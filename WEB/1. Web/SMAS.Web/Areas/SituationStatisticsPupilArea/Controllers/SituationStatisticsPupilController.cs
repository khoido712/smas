﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.SituationStatisticsPupilArea.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using System.Configuration;


namespace SMAS.Web.Areas.SituationStatisticsPupilArea.Controllers
{
    public class SituationStatisticsPupilController : BaseController
    {
        //
        // GET: /SituationStatisticsPupilArea/SituationStatisticsPupil/

        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly ILeavingReasonBusiness LeavingReasonBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly IPupilOfSchoolBusiness PupilOfSchoolBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;

        public SituationStatisticsPupilController(IProvinceBusiness ProvinceBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IDistrictBusiness DistrictBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            IClassMovementBusiness ClassMovementBusiness,
            ILeavingReasonBusiness LeavingReasonBusiness,
            IPupilLeavingOffBusiness PupilLeavingOffBusiness,
            IPupilOfSchoolBusiness PupilOfSchoolBusiness,
            ISchoolMovementBusiness SchoolMovementBusiness,
            IMarkStatisticBusiness _MarkStatisticBusiness,
            IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness)
        {
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassMovementBusiness = ClassMovementBusiness;
            this.LeavingReasonBusiness = LeavingReasonBusiness;
            this.PupilLeavingOffBusiness = PupilLeavingOffBusiness;
            this.PupilOfSchoolBusiness = PupilOfSchoolBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.MarkStatisticBusiness = _MarkStatisticBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
        }

        public ActionResult Index()
        {
            GetData();
            return View();
        }

        #region Export Excel By School
        [ValidateAntiForgeryToken]
        public JsonResult GetSituationStatisticReportBySchool(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? monthID = !string.IsNullOrEmpty(frm["MonthID"]) ? Int32.Parse(frm["MonthID"]) : 0;
            int? educationLevel = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? Int32.Parse(frm["EducationLevelID"]) : 0;

            bool showPupilFemale = false;
            bool showPupilEthenic = false;
            bool showFemaleEthenic = false;

            if ("on".Equals(frm["chkPupilFemale"]))
            {
                showPupilFemale = true;
            }
            if ("on".Equals(frm["chkPupilEthenic"]))
            {
                showPupilEthenic = true;
            }
            if ("on".Equals(frm["chkFemaleEthenic"]))
            {
                showFemaleEthenic = true;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SituationStatisticsPupilConstants.HS_THCS_TKBiendongHS_112017);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dic.Add("SchoolID", _globalInfo.SchoolID);
            dic.Add("AppliedLevelID", _globalInfo.AppliedLevel);
            dic.Add("EducationLevelID", educationLevel);
            dic.Add("MonthID", monthID);
            dic.Add("SheetPupilFemale", showPupilFemale);
            dic.Add("SheetPupilEthnic", showPupilEthenic);
            dic.Add("SheetPupilFemaleEthnic", showFemaleEthenic);

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBusiness.GetProcessReportSituationStatisticPupil(dic, SituationStatisticsPupilConstants.HS_THCS_TKBiendongHS_112017);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                IDictionary<string, object> dicExport = new Dictionary<string, object>();
                dicExport.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicExport.Add("SchoolID", _globalInfo.SchoolID);
                dicExport.Add("AppliedLevel", _globalInfo.AppliedLevel);
                dicExport.Add("EducationLevelID", educationLevel);
                dicExport.Add("MonthID", monthID);
                dicExport.Add("SuperVisingDeptName", _globalInfo.SuperVisingDeptName.ToUpper());
                dicExport.Add("SheetPupilFemale", showPupilFemale);
                dicExport.Add("SheetPupilEthnic", showPupilEthenic);
                dicExport.Add("SheetPupilFemaleEthnic", showFemaleEthenic);

                var lstEducation = _globalInfo.EducationLevels.ToList();
                Stream excel = PupilOfClassBusiness.ExportSituationStatisticsBySchool(dicExport, lstEducation);

                processedReport = PupilOfClassBusiness.InsertProcessReportSituationStatisticPupil(dic, excel, SituationStatisticsPupilConstants.HS_THCS_TKBiendongHS_112017, 2);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySchool(FormCollection frm)
        {
            int? monthID = !string.IsNullOrEmpty(frm["MonthID"]) ? Int32.Parse(frm["MonthID"]) : 0;
            int? educationLevel = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? Int32.Parse(frm["EducationLevelID"]) : 0;

            bool showSheetFemale = false;
            bool showSheetEthenic = false;
            bool showSheetFemaleEthenic = false;

            if ("on".Equals(frm["chkPupilFemale"]))
            {
                showSheetFemale = true;
            }
            if ("on".Equals(frm["chkPupilEthenic"]))
            {
                showSheetEthenic = true;
            }
            if ("on".Equals(frm["chkFemaleEthenic"]))
            {
                showSheetFemaleEthenic = true;
            }

            ProcessedReport processedReport = null;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dic.Add("SchoolID", _globalInfo.SchoolID);
            dic.Add("AppliedLevelID", _globalInfo.AppliedLevel);
            dic.Add("EducationLevelID", educationLevel);
            dic.Add("MonthID", monthID);
            dic.Add("SheetPupilFemale", showSheetFemale);
            dic.Add("SheetPupilEthnic", showSheetEthenic);
            dic.Add("SheetPupilFemaleEthnic", showSheetFemaleEthenic);

            IDictionary<string, object> dicExport = new Dictionary<string, object>();
            dicExport.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicExport.Add("SchoolID", _globalInfo.SchoolID);
            dicExport.Add("AppliedLevel", _globalInfo.AppliedLevel);
            dicExport.Add("EducationLevelID", educationLevel);
            dicExport.Add("MonthID", monthID);
            dicExport.Add("SuperVisingDeptName", _globalInfo.SuperVisingDeptName.ToUpper());
            dicExport.Add("SheetPupilFemale", showSheetFemale);
            dicExport.Add("SheetPupilEthnic", showSheetEthenic);
            dicExport.Add("SheetPupilFemaleEthnic", showSheetFemaleEthenic);

            var lstEducation = _globalInfo.EducationLevels.ToList();
            Stream excel = PupilOfClassBusiness.ExportSituationStatisticsBySchool(dicExport, lstEducation);
            processedReport = PupilOfClassBusiness.InsertProcessReportSituationStatisticPupil(dic, excel, SituationStatisticsPupilConstants.HS_THCS_TKBiendongHS_112017, 2);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReportBySchool(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> {
                SituationStatisticsPupilConstants.HS_THCS_TKBiendongHS_112017,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReportBySchool(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> {
                SituationStatisticsPupilConstants.HS_THCS_TKBiendongHS_112017,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        #region Excel Excel By Phong-SGD
        [ValidateAntiForgeryToken]
        public JsonResult LoadMonthByYear(int year)
        {
            DateTime current = DateTime.Now;
            if (year <= 0 || year > current.Year)
                return Json(new { lstMonth = new List<SelectListItem>(), valueSelected = "0" });

            string monthID = current.Year + "" + current.Month;
            List<MonthViewModel> lstMonth = GetListMonth(year);
            MonthViewModel objMonth = lstMonth.Where(x => x.MonthID == monthID).FirstOrDefault();

            var lstMonthItem = lstMonth.Select(u => new SelectListItem { Value = u.MonthID.ToString(), Text = u.Display }).ToList();
            return Json(new { lstMonth = lstMonthItem, valueSelected = objMonth != null ? objMonth.MonthID : "0" });
        }

        public JsonResult CreatedReport(int year, int month, int? districtID, int? appliedLevelID,
            bool chkFemale, bool chkEthenic, bool chkFemaleEthenic)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            //0: so GD, 1: phongGD
            int SupervisingDeptID = _globalInfo.IsSubSuperVisingDeptRole ? 1 : 0;
            IDictionary<string, object> dicExport = new Dictionary<string, object>();
            dicExport.Add("Year", year);
            dicExport.Add("MonthID", month);
            dicExport.Add("DistrictID", districtID.HasValue ? districtID : 0);
            dicExport.Add("AppliedLevel", appliedLevelID?? 0);
            dicExport.Add("ProvinceID", _globalInfo.ProvinceID);
            dicExport.Add("SheetPupilFemale", chkFemale);
            dicExport.Add("SheetPupilEthnic", chkEthenic);
            dicExport.Add("SheetPupilFemaleEthnic", chkFemaleEthenic);
            dicExport.Add("SupervisingDeptID", SupervisingDeptID);
            MarkStatisticBusiness.InsertReportVariablePupilBySuperVising(dicExport);

            return Json(new { Type = "success" });
        }

        public FileResult ExportReport(int year, int month, int? district, int? appliedLevel,
            bool showPupilFemale, bool showPupilEthenic, bool showFemaleEthenic)
        {
            IDictionary<string, object> dicExport = new Dictionary<string, object>();
            //0: so GD, 1: phongGD
            int SupervisingDeptID = _globalInfo.IsSubSuperVisingDeptRole ? 1 : 0;
            dicExport.Add("Year", year);
            dicExport.Add("MonthID", month);
            dicExport.Add("DistrictID", district);
            dicExport.Add("AppliedLevel", appliedLevel);
            dicExport.Add("ProvinceID", _globalInfo.ProvinceID);
            dicExport.Add("SuperVisingDeptName", _globalInfo.SuperVisingDeptName.ToUpper());
            dicExport.Add("SupervisingDeptID", SupervisingDeptID);
            dicExport.Add("SheetPupilFemale", showPupilFemale);
            dicExport.Add("SheetPupilEthnic", showPupilEthenic);
            dicExport.Add("SheetPupilFemaleEthnic", showFemaleEthenic);

            Stream excel = MarkStatisticBusiness.ExportVariablePupilBySuperVising(dicExport);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = string.Format("SGD_TKBienDongHS_{0}.xls", month);
            result.FileDownloadName = fileName;
            return result;
        }
        #endregion

        #region GetData
        private void GetData()
        {
            ViewData[SituationStatisticsPupilConstants.LIST_MONTH] = null;
            DateTime dateNow = DateTime.Now;
            string monthId = string.Format("{0}{1}", dateNow.Year, dateNow.Month);

            if (_globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsSuperVisingDeptRole)
            {
                Dictionary<string, object> dicYear = new Dictionary<string, object>();
                List<AcademicYear> lstAcademicYear = AcademicYearBusiness.Search(dicYear).Where(x => x.IsActive.HasValue && x.IsActive.Value).ToList();
                List<int> lstYear = lstAcademicYear.Where(x => x.Year <= dateNow.Year).Select(x => x.Year).Distinct().OrderBy(x => x).ToList();
                List<YearViewModel> lstYearVM = GetListYear(lstYear);

                string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
                int curSemester = 1;
                int curYear = AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(_globalInfo.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);

                ViewData[SituationStatisticsPupilConstants.LIST_ACADEMIC_YEAER] = new SelectList(lstYearVM, "YearID", "DisplayTitle", curYear);

                if (curYear > 0)
                {
                    List<MonthViewModel> lstMonth = GetListMonth(curYear);
                    MonthViewModel objMonth = lstMonth.Where(x => x.MonthID == monthId).FirstOrDefault();
                    ViewData[SituationStatisticsPupilConstants.LIST_MONTH] = new SelectList(lstMonth, "MonthID", "Display", objMonth != null ? objMonth.MonthID : "");
                }
               
                IDictionary<string, object> dicDis = new Dictionary<string, object>();
                dicDis.Add("ProvinceID", _globalInfo.ProvinceID);
                List<District> lstDistrict = DistrictBusiness.Search(dicDis).Where(x => x.IsActive.HasValue && x.IsActive.Value).ToList();
                ViewData[SituationStatisticsPupilConstants.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName");

                List<AppliedLevelViewModel> lstAppliedLevel = new List<AppliedLevelViewModel>();
                lstAppliedLevel.Add(new AppliedLevelViewModel
                {
                    AppliedLevelID = SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                    DisplayTitle = SituationStatisticsPupilConstants.APPLIED_LEVEL_PRIMARY
                });
                lstAppliedLevel.Add(new AppliedLevelViewModel
                {
                    AppliedLevelID = SystemParamsInFile.APPLIED_LEVEL_SECONDARY,
                    DisplayTitle = SituationStatisticsPupilConstants.APPLIED_LEVEL_SECONDARY
                });
                lstAppliedLevel.Add(new AppliedLevelViewModel
                {
                    AppliedLevelID = SystemParamsInFile.APPLIED_LEVEL_TERTIARY,
                    DisplayTitle = SituationStatisticsPupilConstants.APPLIED_LEVEL_TERTIARY
                });

                ViewData[SituationStatisticsPupilConstants.LIST_APPLIED_LEVEL] = new SelectList(lstAppliedLevel, "AppliedLevelID", "DisplayTitle", false);
            }
            else
            {
                int schoolId = _globalInfo.SchoolID.Value;
                int academicYear = _globalInfo.AcademicYearID.Value;
                AcademicYear objYear = AcademicYearBusiness.Find(academicYear);

                List<MonthViewModel> lstMonth = GetListMonthOfSchool(objYear.FirstSemesterStartDate.Value, objYear.SecondSemesterEndDate.Value);
                MonthViewModel objMonth = lstMonth.Where(x => x.MonthID == monthId).FirstOrDefault();
                ViewData[SituationStatisticsPupilConstants.LIST_MONTH] = new SelectList(lstMonth, "MonthID", "Display", objMonth != null ? objMonth.MonthID : "");

                //SchoolProfile objSchool = SchoolProfileBusiness.Find(schoolId);

                /*IQueryable<EducationLevel> lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 1 || x.Grade == 2 || x.Grade == 3);
                switch (objSchool.EducationGrade)
                {
                    ///Trường chỉ có cấp 1 
                    case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY:
                        lstEducation = lstEducation.Where(x => x.Grade == 1);
                        break;
                    ///Trường chỉ có cấp 2 
                    case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY:
                        lstEducation = lstEducation.Where(x => x.Grade == 2);
                        break;
                    ///Trường chỉ có cấp 3 
                    case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY:
                        lstEducation = lstEducation.Where(x => x.Grade == 3);
                        break;
                    ///Trường có cấp 1,2 
                    case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY:
                        lstEducation = lstEducation.Where(x => x.Grade == 1 || x.Grade == 2);
                        break;
                    ///Trường có cấp 2,3 
                    case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY:
                        lstEducation = lstEducation.Where(x => x.Grade == 2 || x.Grade == 3);
                        break;
                    ///Trường có cả 3 cấp 1,2,3 
                    case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL:
                        break;
                }*/
                var lstEdu = _globalInfo.EducationLevels.ToList();
                ViewData[SituationStatisticsPupilConstants.LIST_EDUCATION_LEVEL] =
                    lstEdu.Select(u => new SelectListItem { Value = u.EducationLevelID.ToString(), Text = u.Resolution, Selected = false }).ToList();
            }
        }

        private List<YearViewModel> GetListYear(List<int> lstYear)
        {
            List<YearViewModel> lstYearVM = new List<YearViewModel>();
            for (int i = 0; i < lstYear.Count(); i++)
            {
                lstYearVM.Add(new YearViewModel { YearID = lstYear[i], DisplayTitle = ((lstYear[i])+ " - " + (lstYear[i] + 1)) });
            }
            return lstYearVM;
        }

        private List<MonthViewModel> GetListMonth(int year)
        {
            List<MonthViewModel> lstMonth = new List<MonthViewModel>();
            MonthViewModel objMonth = null;
            int _length = 0;
            for (int i = 8; i <= 12; i++)
            {
                _length = i.ToString().Length;
                objMonth = new MonthViewModel();
                objMonth.Display = string.Format("{0}/{1}", (_length == 1 ? ("0" + i) : i.ToString()), year);
                objMonth.MonthID = string.Format("{0}{1}", year, i.ToString());
                lstMonth.Add(objMonth);
            }

            for (int i = 1; i <= 7; i++)
            {
                _length = i.ToString().Length;
                objMonth = new MonthViewModel();
                objMonth.Display = string.Format("{0}/{1}", (_length == 1 ? ("0" + i) : i.ToString()), (year + 1));
                objMonth.MonthID = string.Format("{0}{1}", (year + 1), i.ToString());
                lstMonth.Add(objMonth);
            }

            return lstMonth;
        }

        private List<MonthViewModel> GetListMonthOfSchool(DateTime StartDate, DateTime EndDate)
        {
            List<MonthViewModel> lstMonth = new List<MonthViewModel>();
            MonthViewModel objMonth = null;
            StartDate = new DateTime(StartDate.Year, StartDate.Month, 1);
            EndDate = new DateTime(EndDate.Year, EndDate.Month, 1);
            int _length = 0;
            while (StartDate <= EndDate)
            {
                _length = StartDate.Month.ToString().Length;
                objMonth = new MonthViewModel();
                objMonth.Display = string.Format("{0}/{1}", (_length == 1 ? ("0" + StartDate.Month) : StartDate.Month.ToString()), StartDate.Year);
                objMonth.MonthID = string.Format("{0}{1}", StartDate.Year, StartDate.Month);
                lstMonth.Add(objMonth);
                StartDate = StartDate.AddMonths(1);
            }
            return lstMonth;
        }
        #endregion
    }
}
