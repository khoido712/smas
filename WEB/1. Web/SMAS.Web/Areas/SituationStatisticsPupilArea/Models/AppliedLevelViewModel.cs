﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SituationStatisticsPupilArea.Models
{
    public class AppliedLevelViewModel
    {
        public int AppliedLevelID { get; set; }
        public string DisplayTitle { get; set; }
    }
}