﻿using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SituationStatisticsPupilArea.Models
{
    public class SearchViewModel
    {
        [DisplayName("Năm học")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SituationStatisticsPupilConstants.LIST_ACADEMIC_YEAER)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onChangeYear()")]
        public int? cboYear { get; set; }

        [DisplayName("Tháng")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SituationStatisticsPupilConstants.LIST_MONTH)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? cboMonth { get; set; }

        [DisplayName("Quận/Huyện")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SituationStatisticsPupilConstants.LIST_DISTRICT)]
        //[AdditionalMetadata("OnChange", "onChangeDistrict()")]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? cboDistrict { get; set; }

        [DisplayName("Cấp học")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SituationStatisticsPupilConstants.LIST_APPLIED_LEVEL)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? cboAppliedLevel { get; set; }
    }
}