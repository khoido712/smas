﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SituationStatisticsPupilArea.Models
{
    public class MonthViewModel
    {
        public string MonthID { get; set; }
        public string Display {get;set;}
    }
}