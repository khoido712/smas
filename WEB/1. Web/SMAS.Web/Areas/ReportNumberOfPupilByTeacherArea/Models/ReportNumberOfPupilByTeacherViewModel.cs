﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ReportNumberOfPupilByTeacherArea.Models
{
    public class ReportNumberOfPupilByTeacherViewModel
    {

        #region Grid
        
        public int EmployeeID { get; set; }
        [ResourceDisplayName("Employee_Label_FullName")]
        public string EmployeeName { get; set; }
        [ResourceDisplayName("Employee_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }


        public int Semester { get; set; }

        
        public int? FacultyID { get; set; }
        [ResourceDisplayName("Employee_Label_FacultyName")]
        public string FacultyName { get; set; }
        [ResourceDisplayName("Employee_Label_NumberPupil")]
        public int NumberPupil { get; set; }

        #endregion
    }
}