﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.ReportNumberOfPupilByTeacherArea.Models;

namespace SMAS.Web.Areas.ReportNumberOfPupilByTeacherArea.Controllers
{
    public class ReportNumberOfPupilByTeacherController : Controller
    {
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IReportNumberOfPupilByTeacherBusiness ReportNumberOfPupilByTeacherBusiness;
        public ReportNumberOfPupilByTeacherController(ISchoolFacultyBusiness SchoolFacultyBusiness,
            IReportNumberOfPupilByTeacherBusiness ReportNumberOfPupilByTeacherBusiness)
        {
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.ReportNumberOfPupilByTeacherBusiness = ReportNumberOfPupilByTeacherBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();

            return View();
        }

        public ActionResult _Index()
        {
            SetViewData();
            return PartialView();
        }
        public void SetViewData()
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Danh sách tổ bộ môn
            IQueryable<SchoolFaculty> IQSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            List<SchoolFaculty> ListSchoolFaculty = IQSchoolFaculty.OrderBy(o => o.FacultyName).ToList();
            ViewData[ReportNumberOfPupilByTeacherConstants.LS_FACULTY] = ListSchoolFaculty;

            //Danh sách học kỳ
            ViewData[ReportNumberOfPupilByTeacherConstants.LS_SEMESTER] = CommonList.Semester();
            int defaultSemester = (Global.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : Global.Semester.GetValueOrDefault();
            ViewData[ReportNumberOfPupilByTeacherConstants.DF_SEMESTER] = defaultSemester;

            //Grid

            ViewData[ReportNumberOfPupilByTeacherConstants.LIST_PUPILOFTEACHER] = new List<ReportNumberOfPupilByTeacherViewModel>();

        }
        [HttpPost]
        public ActionResult Search(SearchViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            int Semester = frm.Semester;
            int FacultyID = 0;
            
            
            if (frm.FacultyID != null)
            {
                FacultyID = frm.FacultyID.Value;
            }
            ViewData[ReportNumberOfPupilByTeacherConstants.LIST_PUPILOFTEACHER] = new List<ReportNumberOfPupilByTeacherViewModel>();
            IQueryable<ReportNumberOfPupilByTeacherBO> lstReportNumberOfPupilByTeacher = ReportNumberOfPupilByTeacherBusiness.GetNumberOfPupilByTecher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID);
            IQueryable<ReportNumberOfPupilByTeacherViewModel> lst;
            if (lstReportNumberOfPupilByTeacher != null)
            {
                lst = lstReportNumberOfPupilByTeacher.Select(o => new ReportNumberOfPupilByTeacherViewModel
                {
                    EmployeeID = o.EmployeeID,
                    EmployeeName = o.EmployeeName,
                    BirthDate = o.BirthDate,
                    FacultyName = o.FacultyName,
                    NumberPupil = o.NumberPupil
                });
                ViewData[ReportNumberOfPupilByTeacherConstants.LIST_PUPILOFTEACHER] = lst.ToList();
            }

            ReportNumberOfPupilByTeacherViewModel frmReport = new ReportNumberOfPupilByTeacherViewModel();
            frmReport.FacultyID = FacultyID;
            frmReport.Semester = Semester;


            return PartialView("_ListPupilOfTeacher",frmReport);
        }
        [HttpPost]
        public FileResult ExportExcel(FormCollection col, ReportNumberOfPupilByTeacherViewModel reportViewModel)
        {
            GlobalInfo global = new GlobalInfo();
            List<string> listChk = new List<string>();
            foreach (var obj in col.AllKeys)
            {
                if (obj.Contains("chkSatus"))
                {
                    listChk.Add(obj.Replace("chkSatus",""));
                }
            }

            var listid = listChk;
            List<int> lstTeacherID = new List<int>();
            if (listid.Count == 0)
            {
                throw new BusinessException("Common_Validate_NoPupilSelected");
            }

            foreach (var id in listid)
            {
                int ID = int.Parse(id);
                lstTeacherID.Add(ID);
            }
            int Semester = int.Parse(Request["Semester"]);
            int FacultyID = 0;


            if (Request["FacultyID"] != "")
            {
                FacultyID = int.Parse(Request["FacultyID"]);
            }
            Stream excel = ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID,global.AppliedLevel.GetValueOrDefault());
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            string ReportName = "GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester].xls";
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);
            ReportName = ReportName.Replace("[Semester]", semester);
            ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

            result.FileDownloadName = ReportName;

            return result;
        }
    }
}
