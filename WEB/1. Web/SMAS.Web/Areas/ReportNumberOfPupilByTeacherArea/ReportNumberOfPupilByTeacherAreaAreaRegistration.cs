﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportNumberOfPupilByTeacherArea
{
    public class ReportNumberOfPupilByTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportNumberOfPupilByTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportNumberOfPupilByTeacherArea_default",
                "ReportNumberOfPupilByTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
