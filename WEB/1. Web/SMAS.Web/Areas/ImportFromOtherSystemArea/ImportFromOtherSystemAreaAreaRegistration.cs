﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ImportFromOtherSystemArea
{
    public class ImportFromOtherSystemAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ImportFromOtherSystemArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ImportFromOtherSystemArea_default",
                "ImportFromOtherSystemArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
