﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Common;
using System.Globalization;
using SMAS.Web.Areas.ImportPupilArea.Models;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;
using SMAS.Business.Business;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.ImportFromOtherSystemArea
{
    public class ImportFromOtherSystemConstants : BaseController
    {
        public const string LIST_EDUCATION_LEVEL = "List_Education_Level";
        public const string LIST_CLASS = "List_class";
        public const string LIST_SUBJECT = "List_Subject";
        public const string DEFAULT_SEMESTER = "Default_Semester";
        public const string BUTTON_ENABLE = "button_enable";
        public const string SESSION_KEY_PUPIL_PROFILE = "ImportFromOtherSystem_Profile";
        public const string SESSION_KEY_MARK = "ImportFromOtherSystem_Mark";
        public const string SESSION_FILE_NAME_PUPIL = "ImportFromOtherSystem_Profile_FileName";
        public const string SESSION_FILE_NAME_MARK = "ImportFromOtherSystem_Mark_FileName";
        public const int DATA_TYPE_PUPIL_PROFILE = 1;
        public const int DATA_TYPE_MARK = 2;
    }
}
