﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Common;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;
using SMAS.Business.Business;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.ImportFromOtherSystemArea.Models;
using AutoMapper;
using Ionic.Zip;
using System.Web.UI;

namespace SMAS.Web.Areas.ImportFromOtherSystemArea.Controllers
{
    public class ImportFromOtherSystemController : BaseController
    {

        private IClassProfileBusiness ClassProfileBusiness;
        private IClassSubjectBusiness ClassSubjectBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        private IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private IProvinceBusiness ProvinceBusiness;
        private IDistrictBusiness DistrictBusiness;
        private ICommuneBusiness CommuneBusiness;
        private IVillageBusiness VillageBusiness;
        private IEthnicBusiness EthnicBusiness;
        private IPolicyTargetBusiness PolicyTargetBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private IPupilOfClassBusiness PupilOfClassBusiness;
        private IMarkRecordBusiness MarkRecordBusiness;
        private IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private ICodeConfigBusiness CodeConfigBusiness;
        private ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private IQiSubjectMappingBusiness QiSubjectMappingBusiness;
        private IReligionBusiness ReligionBusiness;
        public ImportFromOtherSystemController(IClassProfileBusiness ClassProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            IUserAccountBusiness UserAccountBusiness,
            IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness,
            ITeachingAssignmentBusiness TeachingAssignmentBusiness,
            IProvinceBusiness ProvinceBusiness,
            IDistrictBusiness DistrictBusiness,
            ICommuneBusiness CommuneBusiness,
            IVillageBusiness VillageBusiness,
            IEthnicBusiness EthnicBusiness,
            IPolicyTargetBusiness PolicyTargetBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            IMarkRecordBusiness MarkRecordBusiness,
            IMarkRecordHistoryBusiness MarkRecordHistoryBusiness,
            ICodeConfigBusiness CodeConfigBusiness,
            ISemeterDeclarationBusiness SemeterDeclarationBusiness,
            IQiSubjectMappingBusiness QiSubjectMappingBusiness,
            IReligionBusiness ReligionBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.CommuneBusiness = CommuneBusiness;
            this.VillageBusiness = VillageBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.PolicyTargetBusiness = PolicyTargetBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.MarkRecordHistoryBusiness = MarkRecordHistoryBusiness;
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.SemeterDeclarationBusiness = SemeterDeclarationBusiness;
            this.QiSubjectMappingBusiness = QiSubjectMappingBusiness;
            this.ReligionBusiness = ReligionBusiness;
        }


        public ActionResult Index()
        {
            ViewData[ImportFromOtherSystemConstants.LIST_EDUCATION_LEVEL] = _globalInfo.EducationLevels;
            int defaultEducationLevel = _globalInfo.EducationLevels.FirstOrDefault().EducationLevelID;

            ViewData[ImportFromOtherSystemConstants.LIST_CLASS] = getClassFromEducationLevel(defaultEducationLevel);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            int semester = 1;
            if (aca.SecondSemesterStartDate.Value <= nowDate && nowDate <= aca.SecondSemesterEndDate)
            {
                semester = 2;
            }
            ViewData[ImportFromOtherSystemConstants.DEFAULT_SEMESTER] = semester;

            //Kiem tra quyen button
            bool checkImport = false;

            //Kiem tra quyen them
            int permission = GetMenupermission("ImportFromOtherSystem", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            if (permission > 1 && aca.FirstSemesterStartDate <= nowDate && nowDate <= aca.SecondSemesterEndDate)
            {
                checkImport = true;
            }
            ViewData[ImportFromOtherSystemConstants.BUTTON_ENABLE] = checkImport;

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public ActionResult Import(int SystemType, int DataType, int EducationLevel, int? ClassID, int? Semester, int? SubjectID, List<int> ListSubjectID, HttpPostedFileBase file)
        {
            JsonResult res;

            //Quang ich
            if (SystemType == 1)
            {
                if (DataType == ImportFromOtherSystemConstants.DATA_TYPE_PUPIL_PROFILE)
                {
                    #region Import hoc sinh
                    if (file == null)
                    {
                        res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), "error"));
                        return res;
                    }

                    if (!file.FileName.EndsWith(".xls") && !file.FileName.EndsWith(".xlsx"))
                    {
                        res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                        return res;
                    }
                    int maxSize = 3145728;
                    if (file.ContentLength > maxSize)
                    {
                        //JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                        res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "3 MB"), "error"));
                        return res;
                    }

                    List<Province> lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
                    List<District> lstDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
                    List<Commune> lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
                    IQueryable<Village> lstVillage = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });

                    var lstEthnic = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.EthnicID, u.EthnicName }).ToList().Select(u => new ComboObject(u.EthnicID.ToString(), u.EthnicName.ToLower())).ToList();
                    var lstPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.PolicyTargetID, u.Resolution }).ToList().Select(u => new ComboObject(u.PolicyTargetID.ToString(), u.Resolution.ToLower())).ToList();
                    List<ClassProfile> lstClass = getClassFromEducationLevel(EducationLevel);

                    IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                    if (book == null)
                    {
                        res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                        return res;
                    }
                    List<IVTWorksheet> lstSheet = book.GetSheets();
                    if (lstSheet.Where(o => lstClass.Any(u => u.DisplayName == o.Name)).Count() == 0)
                    {
                        res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                        return res;
                    }

                    bool hasErr = false;
                    List<ImportPupilProfileModel> lstModel = new List<ImportPupilProfileModel>();
                    ImportPupilProfileModel model;
                    List<ClassProfile> lstImportedClass = new List<ClassProfile>();
                    foreach (IVTWorksheet sheet in lstSheet)
                    {
                        sheet.ShiftSheetDown();
                        ClassProfile cp = lstClass.FirstOrDefault(o => o.DisplayName == sheet.Name);
                        if (cp == null)
                        {
                            continue;
                        }

                        lstImportedClass.Add(cp);

                        //kiem tra dinh dang cot trong file
                        int? firstR = sheet.GetFirstUsedRow(1);
                        int firstRow;
                        if (!firstR.HasValue)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }
                        else
                        {
                            firstRow = firstR.Value;
                        }

                        if (!GetCellString(sheet, "A" + firstRow).Equals("STT")
                            || !GetCellString(sheet, "B" + firstRow).Equals("Họ và tên")
                            || !GetCellString(sheet, "C" + firstRow).Equals("Ngày sinh")
                            || !GetCellString(sheet, "D" + firstRow).Equals("Giới tính")
                            || !GetCellString(sheet, "E" + firstRow).Equals("Thôn")
                            || !GetCellString(sheet, "F" + firstRow).Equals("Tỉnh")
                            || !GetCellString(sheet, "G" + firstRow).Equals("Huyện")
                            || !GetCellString(sheet, "H" + firstRow).Equals("Xã")
                            || !GetCellString(sheet, "I" + firstRow).Equals("Nơi sinh")
                            || !GetCellString(sheet, "J" + firstRow).Equals("Quê quán")
                            || !GetCellString(sheet, "K" + firstRow).Equals("Nơi ở hiện nay")
                            || !GetCellString(sheet, "L" + firstRow).Equals("Dân tộc")
                            || !GetCellString(sheet, "M" + firstRow).Equals("SĐT liên hệ")
                            || !GetCellString(sheet, "N" + firstRow).Equals("SĐT nhận tin")
                            || !GetCellString(sheet, "O" + firstRow).Equals("Họ tên cha")
                            || !GetCellString(sheet, "P" + firstRow).Equals("Nghề nghiệp cha")
                            || !GetCellString(sheet, "Q" + firstRow).Equals("Họ tên mẹ")
                            || !GetCellString(sheet, "R" + firstRow).Equals("Nghề nghiệp mẹ")
                            || !GetCellString(sheet, "S" + firstRow).Equals("Hộ khẩu"))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        int curRow = firstRow + 1;
                        int firstCol = 1;
                        int lastCol = 19;
                        int curCol = firstCol;
                        int errCol = lastCol + 1;

                        //add them cot mo ta loi cho sheet
                        sheet.SetCellValue(firstRow, errCol, "Mô tả lỗi");
                        sheet.GetRange(firstRow, errCol, firstRow, errCol).SetFontStyle(true, null, false, null, false, false);
                        sheet.GetRange(firstRow, errCol, firstRow, errCol).WrapText();
                        //sheet.GetRange(firstRow, errCol, firstRow, errCol).AutoFitRowHeight();
                        sheet.SetColumnWidth(errCol, 70);

                        while (!string.IsNullOrEmpty(GetCellString(sheet, "A" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "B" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "C" + curRow)))
                        {
                            string errStr = String.Empty;
                            model = new ImportPupilProfileModel();
                            lstModel.Add(model);
                            model.ClassID = cp.ClassProfileID;
                            //STT
                            model.IOrderNumber = GetCellString(sheet, curRow, curCol);
                            int order;
                            if (!String.IsNullOrEmpty(model.IOrderNumber) && int.TryParse(model.IOrderNumber, out order) && order < 100)
                            {
                                model.OrderNumber = order;
                            }
                            else
                            {
                                errStr = errStr + "STT không được bỏ trống/không hợp lệ; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Ho va ten
                            model.FullName = GetCellString(sheet, curRow, curCol);
                            if (String.IsNullOrEmpty(model.FullName))
                            {
                                errStr = errStr + "Họ và tên không được bỏ trống; ";
                                hasErr = true;
                            }
                            else if (model.FullName.Length > 100)
                            {
                                errStr = errStr + "Họ và tên không được dài quá 100 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Ngay sinh
                            model.IBirthDay = GetCellString(sheet, curRow, curCol);
                            DateTime date;
                            if (String.IsNullOrEmpty(model.IBirthDay))
                            {
                                errStr = errStr + "Ngày sinh không được bỏ trống; ";
                                hasErr = true;
                            }
                            else if (!DateTime.TryParse(model.IBirthDay, out date))
                            {
                                errStr = errStr + "Ngày sinh không hợp lệ; ";
                                hasErr = true;
                            }
                            else
                            {
                                model.BirthDay = date;
                            }
                            curCol++;

                            //Gioi tinh
                            model.Genre = GetCellString(sheet, curRow, curCol);
                            curCol++;

                            //Thon
                            model.IVillage = GetCellString(sheet, curRow, curCol);
                            Village v = lstVillage.FirstOrDefault(o => o.VillageName == model.IVillage);
                            if (v != null)
                            {
                                model.VillageID = v.VillageID;
                            }
                            if (model.IVillage.Length > 50)
                            {
                                errStr = errStr + "Thôn không được dài quá 50 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Tinh
                            model.IProvince = GetCellString(sheet, curRow, curCol);
                            if (String.IsNullOrEmpty(model.IProvince))
                            {
                                errStr = errStr + "Thông tin tỉnh không được bỏ trống; ";
                                hasErr = true;
                            }
                            else if (!lstProvince.Any<Province>(o => o.ProvinceName == model.IProvince))
                            {
                                errStr = errStr + "Thông tin tỉnh không tồn tại trong hệ thống; ";
                                hasErr = true;
                            }
                            else
                            {
                                Province p = lstProvince.FirstOrDefault(o => o.ProvinceName == model.IProvince);
                                model.ProvinceID = p.ProvinceID;
                            }
                            curCol++;

                            //Huyen
                            model.IDistrict = GetCellString(sheet, curRow, curCol);
                            District d = lstDistrict.FirstOrDefault(o => o.DistrictName == model.IDistrict
                                && model.ProvinceID > 0 && o.ProvinceID == model.ProvinceID);
                            if (d != null)
                            {
                                model.DistrictID = d.DistrictID;
                            }
                            curCol++;

                            //Xa
                            model.ICommune = GetCellString(sheet, curRow, curCol);
                            Commune c = lstCommune.FirstOrDefault(o => o.CommuneName == model.ICommune
                                && model.DistrictID.HasValue && o.DistrictID == model.DistrictID.Value);
                            if (c != null)
                            {
                                model.CommuneID = c.CommuneID;
                            }

                            if (!model.CommuneID.HasValue)
                            {
                                model.VillageID = null;
                            }
                            curCol++;

                            //Noi sinh
                            model.BirtPlace = GetCellString(sheet, curRow, curCol);
                            if (model.BirtPlace.Length > 256)
                            {
                                errStr = errStr + "Nơi sinh không được dài quá 256 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Que quan
                            model.HomeTown = GetCellString(sheet, curRow, curCol);
                            if (model.HomeTown.Length > 256)
                            {
                                errStr = errStr + "Quê quán không được dài quá 256 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Noi o hien nay
                            model.LiveAddress = GetCellString(sheet, curRow, curCol);
                            if (model.LiveAddress.Length > 256)
                            {
                                errStr = errStr + "Địa chỉ tạm trú không được dài quá 256 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Dan toc
                            model.IEthnic = GetCellString(sheet, curRow, curCol);
                            ComboObject e = lstEthnic.FirstOrDefault(o => o.value == model.IEthnic.ToLower());
                            if (e != null)
                            {
                                model.EthnicID = int.Parse(e.key);
                            }
                            curCol++;

                            //SDT lien he
                            model.PhoneNumber = GetCellString(sheet, curRow, curCol);
                            if (!String.IsNullOrEmpty(model.PhoneNumber) && !Utils.Utils.CheckMobileNumber(model.PhoneNumber))
                            {
                                errStr = errStr + "SĐT liên lạc không hợp lệ; ";
                                hasErr = true;
                            }
                            curCol++;

                            //SDT nhan tin
                            curCol++;

                            //Ho ten cha
                            model.FatherName = GetCellString(sheet, curRow, curCol);
                            if (model.FatherName.Length > 50)
                            {
                                errStr = errStr + "Họ tên cha không được dài quá 50 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Nghe nghiep cha
                            model.FatherJob = GetCellString(sheet, curRow, curCol);
                            if (model.FatherJob.Length > 50)
                            {
                                errStr = errStr + "Nghề nghiệp cha không được dài quá 50 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Ho ten me
                            model.MotherName = GetCellString(sheet, curRow, curCol);
                            if (model.MotherName.Length > 50)
                            {
                                errStr = errStr + "Họ tên mẹ không được dài quá 50 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Nghe nghiep me
                            model.MotherJob = GetCellString(sheet, curRow, curCol);
                            if (model.MotherJob.Length > 50)
                            {
                                errStr = errStr + "Nghề nghiệp mẹ không được dài quá 50 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            //Ho khau
                            model.HomeAddress = GetCellString(sheet, curRow, curCol);
                            if (model.HomeAddress.Length > 256)
                            {
                                errStr = errStr + "Địa chỉ thường trú không được dài quá 256 ký tự; ";
                                hasErr = true;
                            }
                            curCol++;

                            sheet.SetCellValue(curRow, errCol, errStr);

                            curRow++;
                            curCol = firstCol;
                        }

                        sheet.GetRange(firstRow, firstCol, curRow - 1, errCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                    }

                    if (hasErr)
                    {
                        Session[ImportFromOtherSystemConstants.SESSION_KEY_PUPIL_PROFILE] = book;
                        Session[ImportFromOtherSystemConstants.SESSION_FILE_NAME_PUPIL] = file.FileName;
                        res = Json(new JsonMessage("Lỗi import dữ liệu Hồ sơ học sinh từ eSchool – Quảng Ích: Trong file tồn tại dữ liệu không hợp lệ. Thầy cô có muốn tải file mô tả lỗi?", "validate"));
                        return res;
                    }
                    else
                    {
                        List<OrderInsertBO> lstOrderInsert = new List<OrderInsertBO>();
                        OrderInsertBO objOrderInsert = null;
                        List<PupilProfile> lstPupilInsertPP = new List<PupilProfile>();

                        List<int> lstOrderInClass = new List<int>();


                        bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
                        bool isSchoolModify = CodeConfigBusiness.IsSchoolModify(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
                        string originalPupilCode = string.Empty;
                        int maxOrderNumber = 0;
                        int numberLength = 0;

                        List<int> lstClassID = lstModel.Select(p => p.ClassID).Distinct().ToList();

                        List<PupilOfClassBO> lstpoc = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { })
                                                       join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                                       where lstClassID.Contains(poc.ClassID)
                                                       select new PupilOfClassBO
                                                       {
                                                           PupilID = poc.PupilID,
                                                           ClassID = poc.ClassID,
                                                           PupilCode = pf.PupilCode,
                                                           AssignedDate = poc.AssignedDate,
                                                           IsActive = pf.IsActive,
                                                           OrderInClass = poc.OrderInClass,
                                                           AcademicYearID = poc.AcademicYearID
                                                       }).ToList();
                        List<string> lstPupilCodeOtherYear = new List<string>();
                        List<string> lstPupilCodeCurrentYear = new List<string>();
                        if (isAutoGenCode)
                        {
                            CodeConfigBusiness.GetPupilCodeForImport(_globalInfo.AcademicYearID.Value, EducationLevel, _globalInfo.SchoolID.Value, out originalPupilCode, out maxOrderNumber, out numberLength);
                        }

                        DateTime enrolmentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

                        for (int i = 0; i < lstModel.Count; i++)
                        {
                            ImportPupilProfileModel m = lstModel[i];
                            ClassProfile cp = lstClass.FirstOrDefault(o => o.ClassProfileID == m.ClassID);
                            //Insert PupilProfile
                            var pupilProfile = new PupilProfile
                            {
                                CurrentAcademicYearID = _globalInfo.AcademicYearID.Value,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CurrentSchoolID = _globalInfo.SchoolID.Value

                            };

                            string pupilCode = originalPupilCode + ToStringWithFixedLength(maxOrderNumber + 1 + i, numberLength);
                            if (isAutoGenCode)
                            {
                                if (isSchoolModify)
                                {
                                    pupilProfile.PupilCode = pupilCode;
                                }
                                else
                                {

                                    pupilProfile.PupilCode = pupilCode;

                                }
                            }
                            else
                            {
                                AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                                string classNameTrim = SMAS.Business.Common.Utils.StripVNSign(cp.DisplayName);
                                classNameTrim = Regex.Replace(classNameTrim, "[^0-9a-zA-Z]+", "");
                                if (classNameTrim.Length > 20)
                                {
                                    classNameTrim = classNameTrim.Substring(0, 20);
                                }
                                string code = String.Format("QI{0}{1}{2}", aca.Year.ToString().Substring(2), classNameTrim, m.OrderNumber.ToString().PadLeft(4, '0'));

                                pupilProfile.PupilCode = code;
                            }

                            pupilProfile.FullName = m.FullName;
                            pupilProfile.Name = GetPupilName(pupilProfile.FullName);
                            pupilProfile.BirthDate = m.BirthDay;
                            if (m.Genre.Equals("Nữ"))
                            {
                                pupilProfile.Genre = 0;
                            }
                            else
                            {
                                pupilProfile.Genre = 1;
                            }
                            pupilProfile.VillageID = m.VillageID;
                            pupilProfile.ProvinceID = m.ProvinceID;
                            pupilProfile.DistrictID = m.DistrictID;
                            pupilProfile.CommuneID = m.CommuneID;
                            pupilProfile.BirthPlace = m.BirtPlace;
                            pupilProfile.HomeTown = m.HomeTown;
                            pupilProfile.TempResidentalAddress = m.LiveAddress;
                            pupilProfile.EthnicID = m.EthnicID;
                            pupilProfile.Telephone = m.PhoneNumber;
                            pupilProfile.FatherFullName = m.FatherName;
                            pupilProfile.FatherJob = m.FatherJob;
                            pupilProfile.MotherFullName = m.MotherName;
                            pupilProfile.MotherJob = m.MotherJob;
                            pupilProfile.PermanentResidentalAddress = m.HomeAddress;
                            pupilProfile.CurrentClassID = m.ClassID;
                            pupilProfile.CreatedDate = DateTime.Now.Date;
                            pupilProfile.IsActive = true;
                            pupilProfile.EnrolmentDate = enrolmentDate;
                            pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                            pupilProfile.ForeignLanguageTraining = 0;

                            lstPupilInsertPP.Add(pupilProfile);
                            objOrderInsert = new OrderInsertBO();
                            objOrderInsert.ClassID = m.ClassID;
                            objOrderInsert.PupilCode = pupilProfile.PupilCode;
                            objOrderInsert.OrderNumber = m.OrderNumber;
                            lstOrderInsert.Add(objOrderInsert);

                            //kiem tra co trung STT hay ko
                            bool isExistOrder = (from pf in lstpoc
                                                 where
                                                 pf.AcademicYearID == _globalInfo.AcademicYearID
                                                 && (pf.OrderInClass == m.OrderNumber)
                                                 && pf.ClassID == m.ClassID
                                                 && pf.IsActive
                                                 select pf).Count() > 0
                                                 ||
                                                 lstOrderInsert.Where(o => o.ClassID == m.ClassID && o.OrderNumber == m.OrderNumber).Count() > 1;
                            if (isExistOrder)
                            {
                                res = Json(new JsonMessage("STT không được trùng với HS khác", "error"));
                                return res;
                            }

                            //Kiem tra co trung Code hay ko
                            bool isExistCode = (from pf in lstpoc
                                                where pf.PupilCode == pupilProfile.PupilCode
                                                && pf.IsActive
                                                select pf).Count() > 0
                                                ||
                                                lstPupilInsertPP.Where(o => o.PupilCode == pupilProfile.PupilCode).Count() > 1;

                            if (isExistCode)
                            {
                                res = Json(new JsonMessage("Mã học sinh đã tồn tại", "error"));
                                return res;
                            }
                        }

                        if (lstPupilInsertPP.Count > 0)
                        {
                            PupilProfileBusiness.ImportFromOtherSystem(_globalInfo.UserAccountID, lstPupilInsertPP, lstOrderInsert, lstClassID);
                        }

                        string strLstClass = "";
                        for (int i = 0; i < lstImportedClass.Count; i++)
                        {
                            strLstClass = strLstClass + lstImportedClass[i].DisplayName;
                            if (i < lstImportedClass.Count - 1)
                            {
                                strLstClass = strLstClass + ", ";
                            }
                        }

                        ViewData[CommonKey.AuditActionKey.Description] = "Import dữ liệu từ hệ thống khác";
                        ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT;
                        ViewData[CommonKey.AuditActionKey.userFunction] = "Import dữ liệu từ hệ thống khác";
                        ViewData[CommonKey.AuditActionKey.userDescription] = String.Format("Import dữ liệu hồ sơ học sinh từ Hệ thống eSchool – Quảng Ích cho các lớp {0}", strLstClass);

                        res = Json(new JsonMessage(String.Format("Import thành công lớp {0}.", strLstClass), "success"));
                        return res;
                    }
                    #endregion
                }
                else
                {
                    #region import diem
                    if (!ClassID.HasValue)
                    {
                        res = Json(new JsonMessage("Thầy cô chưa chọn lớp", "error"));
                        return res;
                    }

                    if (file == null)
                    {
                        res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), "error"));
                        return res;
                    }

                    //Import tat ca mon
                    if (!SubjectID.HasValue)
                    {
                        if (!file.FileName.EndsWith(".zip"))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        int maxSize = 10485760;
                        if (file.ContentLength > maxSize)
                        {
                            //JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                            res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "10 MB"), "error"));
                            return res;
                        }

                        //Lay danh sach hoc sinh cua lop
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        dic["ClassID"] = ClassID.Value;
                        dic["SchoolID"] = _globalInfo.SchoolID;
                        dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                        dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                        List<PupilProfileBO> lstPP = PupilOfClassBusiness.Search(dic)
                            .Select(o => new PupilProfileBO
                            {
                                FullName = o.PupilProfile.FullName,
                                BirthDate = o.PupilProfile.BirthDate,
                                OrderInClass = o.OrderInClass,
                                PupilProfileID = o.PupilID,
                                PupilCode = o.PupilProfile.PupilCode
                            }).ToList();
                        ;

                        //Danh sach mon hoc
                        List<SubjectCatBO> lstCs = GetListSubjectByClass(ClassID.Value, Semester.Value);
                        List<int> lstSubjectId = lstCs.Select(o => o.SubjectCatID).ToList();

                        //Danh sach mon hoc co trong file zip
                        List<SubjectMappingModel> lstSubjectMapping = new List<SubjectMappingModel>();
                        using (ZipFile zip = ZipFile.Read(file.InputStream))
                        {
                            foreach (ZipEntry e in zip)
                            {
                                IVTWorkbook book;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    e.Extract(stream);
                                    book = VTExport.OpenWorkbook(stream);
                                }

                                if (book != null)
                                {
                                    IVTWorksheet sheet = null;

                                    try
                                    {
                                        sheet = book.GetSheet(1);
                                    }
                                    catch
                                    {
                                        sheet = null;
                                    }

                                    if (sheet != null)
                                    {
                                        SubjectMappingModel sm = new SubjectMappingModel();

                                        //Lay ten mon
                                        var subjectName = GetCellString(sheet, "F7") ?? "";
                                        string[] arr = subjectName.Split('-');
                                        subjectName = arr[0].Replace("Môn học:", string.Empty).Trim();
                                        sm.OriginalSubjectName = subjectName;

                                        QiSubjectMapping qSubject = QiSubjectMappingBusiness.All.FirstOrDefault(o => o.QiSubjectName == subjectName && o.AppliedLevel == _globalInfo.AppliedLevel
                                            && lstSubjectId.Contains(o.MappedSubjectID));

                                        if (qSubject != null)
                                        {
                                            sm.MappedSubjectId = qSubject.MappedSubjectID;
                                            sm.IsChecked = true;
                                        }

                                        lstSubjectMapping.Add(sm);
                                    }
                                }
                            }
                        }

                        if (lstSubjectMapping.Count == 0)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        //Lay danh sach mon hoc
                        ViewData[ImportFromOtherSystemConstants.LIST_SUBJECT] = lstCs;

                        return PartialView("_SubjectSelection", lstSubjectMapping);
                    }
                    else
                    {
                        if (!file.FileName.EndsWith(".xls") && !file.FileName.EndsWith(".xlsx"))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }
                        int maxSize = 3145728;
                        if (file.ContentLength > maxSize)
                        {
                            //JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                            res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "3 MB"), "error"));
                            return res;
                        }

                        bool hasErr = false;
                        List<ImportMarkModel> lstModel = new List<ImportMarkModel>();
                        ImportMarkModel model;
                        IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                        if (book == null)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }
                        IVTWorksheet sheet = book.GetSheet(1);


                        int? firstR = sheet.GetFirstUsedRow(1);
                        int firstRow;
                        if (!firstR.HasValue)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }
                        else
                        {
                            firstRow = firstR.Value;
                        }

                        int headerRow = firstRow + 6;
                        int startRow = firstRow + 10;
                        int curRow = startRow;
                        int firstCol = 1;
                        int lastCol = 31;
                        int curCol = firstCol;
                        int errCol = lastCol + 1;

                        //kiem tra dinh dang cot trong file
                        var A = GetCellString(sheet, "A" + headerRow) ?? "";
                        var B = GetCellString(sheet, "B" + headerRow) ?? "";
                        var C = GetCellString(sheet, "C" + headerRow) ?? "";
                        var D = GetCellString(sheet, "D" + headerRow) ?? "";
                        var Q = GetCellString(sheet, "Q" + headerRow) ?? "";
                        var AA = GetCellString(sheet, "AA" + headerRow) ?? "";
                        var AB = GetCellString(sheet, "AB" + headerRow) ?? "";
                        var AE = GetCellString(sheet, "AE" + headerRow) ?? "";

                        if (Semester == 1 && (!A.ToLower().Equals("tt")
                                        || !B.ToLower().Equals("họ tên")
                                        || !C.ToLower().Equals("ngày sinh")
                                        || !D.ToLower().Equals("điểm hệ số 1")
                                        || !Q.ToLower().Equals("điểm hệ số 2")
                                        || !AA.ToLower().Equals("hk")
                                        || !AB.ToLower().Equals("tb\nk1")
                                        || !AE.ToLower().Equals("mã học sinh")))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        if (Semester == 2 && (!A.ToLower().Equals("tt")
                            || !B.ToLower().Equals("họ tên")
                            || !C.ToLower().Equals("ngày sinh")
                            || !D.ToLower().Equals("điểm hệ số 1")
                            || !Q.ToLower().Equals("điểm hệ số 2")
                            || !AA.ToLower().Equals("hk")
                            || !AB.ToLower().Equals("tb\nmk1")
                            || !AE.ToLower().Equals("mã học sinh")))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        //Kiem tra truong chua khai bao so con diem
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                        dic["SchoolID"] = _globalInfo.SchoolID.Value;
                        dic["Semester"] = Semester.Value;
                        SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
                        if (SemeterDeclaration == null)
                        {
                            res = Json(new JsonMessage(Res.Get("Validate_School_NotMark"), "error"));
                            return res;
                        }

                        //add them cot mo ta loi cho sheet
                        sheet.SetCellValue(headerRow, errCol, "Mô tả lỗi");
                        sheet.GetRange(headerRow, errCol, headerRow, errCol).SetFontStyle(true, null, false, null, false, false);
                        sheet.SetColumnWidth(errCol, 70);

                        //Lay danh sach hoc sinh cua lop
                        dic = new Dictionary<string, object>();
                        dic["ClassID"] = ClassID.Value;
                        dic["SchoolID"] = _globalInfo.SchoolID;
                        dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                        dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                        List<PupilProfileBO> lstPP = PupilOfClassBusiness.Search(dic)
                            .Select(o => new PupilProfileBO
                            {
                                FullName = o.PupilProfile.FullName,
                                BirthDate = o.PupilProfile.BirthDate,
                                OrderInClass = o.OrderInClass,
                                PupilProfileID = o.PupilID,
                                PupilCode = o.PupilProfile.PupilCode
                            }).ToList();
                        ;

                        List<SubjectCatBO> lstCs = GetListSubjectByClass(ClassID.Value, Semester.Value);
                        SubjectCatBO sc = lstCs.FirstOrDefault(o => o.SubjectCatID == SubjectID.Value);
                        bool isComment = sc.IsCommenting.HasValue && sc.IsCommenting.Value == 1 ? true : false;

                        while (!string.IsNullOrEmpty(GetCellString(sheet, "A" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "B" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "C" + curRow)))
                        {
                            string errStr = String.Empty;
                            model = new ImportMarkModel();
                            lstModel.Add(model);
                            model.ClassID = ClassID.Value;
                            model.SubjectID = SubjectID.Value;
                            //Kiem tra ton tai hoc sinh
                            //Ma hoc sinh
                            string pupilCode = GetCellString(sheet, curRow, lastCol);
                            PupilProfileBO pp;
                            pp = lstPP.FirstOrDefault(o => o.PupilCode.Equals(pupilCode));

                            if (pp == null)
                            {
                                errStr = errStr + "Mã học sinh không hợp lệ; ";
                                hasErr = true;
                            }
                            else
                            {
                                model.PupilID = pp.PupilProfileID;
                            }

                            //STT
                            int order;
                            if (!int.TryParse(GetCellString(sheet, curRow, curCol), out order))
                            {
                                order = -1;
                            }
                            curCol++;

                            //Ho ten
                            string fullName = GetCellString(sheet, curRow, curCol);
                            curCol++;

                            // Ngay sinh
                            DateTime birthDay;
                            if (!DateTime.TryParse(GetCellString(sheet, curRow, curCol), out birthDay))
                            {
                                birthDay = DateTime.MinValue;
                            }

                            //Kiem tra hoc sinh co hop le

                            curCol++;

                            #region Mon tinh diem
                            if (!isComment)
                            {
                                //diem mieng
                                int m1, m2, m3, m4, m5;
                                string strM1 = GetCellString(sheet, curRow, curCol);
                                string strM2 = GetCellString(sheet, curRow, curCol + 1);
                                string strM3 = GetCellString(sheet, curRow, curCol + 2);
                                string strM4 = GetCellString(sheet, curRow, curCol + 3);
                                string strM5 = GetCellString(sheet, curRow, curCol + 4);

                                if ((int.TryParse(strM1, out m1) || String.IsNullOrEmpty(strM1))
                                    && (int.TryParse(strM2, out m2) || String.IsNullOrEmpty(strM2))
                                    && (int.TryParse(strM3, out m3) || String.IsNullOrEmpty(strM3))
                                    && (int.TryParse(strM4, out m4) || String.IsNullOrEmpty(strM4))
                                    && (int.TryParse(strM5, out m5) || String.IsNullOrEmpty(strM5)))
                                {
                                    if (0 <= m1 && m1 <= 10
                                        && 0 <= m2 && m2 <= 10
                                        && 0 <= m3 && m3 <= 10
                                        && 0 <= m4 && m4 <= 10
                                        && 0 <= m5 && m5 <= 10)
                                    {
                                        if (!String.IsNullOrEmpty(strM1)) model.M1 = m1;
                                        if (!String.IsNullOrEmpty(strM2)) model.M2 = m2;
                                        if (!String.IsNullOrEmpty(strM3)) model.M3 = m3;
                                        if (!String.IsNullOrEmpty(strM4)) model.M4 = m4;
                                        if (!String.IsNullOrEmpty(strM5)) model.M5 = m5;

                                    }
                                    else
                                    {
                                        errStr = errStr + "Giá trị điểm miệng không hợp lệ; ";
                                        hasErr = true;
                                    }
                                }
                                else
                                {
                                    errStr = errStr + "Giá trị điểm miệng không hợp lệ; ";
                                    hasErr = true;
                                }

                                curCol = curCol + 5;

                                //Diem 15phut
                                decimal p1, p2, p3, p4, p5;
                                string strP1 = GetCellString(sheet, curRow, curCol).Replace(',', '.');
                                string strP2 = GetCellString(sheet, curRow, curCol + 1).Replace(',', '.');
                                string strP3 = GetCellString(sheet, curRow, curCol + 2).Replace(',', '.');
                                string strP4 = GetCellString(sheet, curRow, curCol + 3).Replace(',', '.');
                                string strP5 = GetCellString(sheet, curRow, curCol + 4).Replace(',', '.');

                                if ((decimal.TryParse(strP1, NumberStyles.Any, CultureInfo.InvariantCulture, out p1) || String.IsNullOrEmpty(strP1))
                                    && (decimal.TryParse(strP2, NumberStyles.Any, CultureInfo.InvariantCulture, out p2) || String.IsNullOrEmpty(strP2))
                                    && (decimal.TryParse(strP3, NumberStyles.Any, CultureInfo.InvariantCulture, out p3) || String.IsNullOrEmpty(strP3))
                                    && (decimal.TryParse(strP4, NumberStyles.Any, CultureInfo.InvariantCulture, out p4) || String.IsNullOrEmpty(strP4))
                                    && (decimal.TryParse(strP5, NumberStyles.Any, CultureInfo.InvariantCulture, out p5) || String.IsNullOrEmpty(strP5)))
                                {
                                    if (0 <= p1 && p1 <= 10
                                        && 0 <= p2 && p2 <= 10
                                        && 0 <= p3 && p3 <= 10
                                        && 0 <= p4 && p4 <= 10
                                        && 0 <= p5 && p5 <= 10)
                                    {
                                        if (!String.IsNullOrEmpty(strP1)) model.P1 = Decimal.Round(p1, 1);
                                        if (!String.IsNullOrEmpty(strP2)) model.P2 = Decimal.Round(p2, 1);
                                        if (!String.IsNullOrEmpty(strP3)) model.P3 = Decimal.Round(p3, 1);
                                        if (!String.IsNullOrEmpty(strP4)) model.P4 = Decimal.Round(p4, 1);
                                        if (!String.IsNullOrEmpty(strP5)) model.P5 = Decimal.Round(p5, 1);

                                    }
                                    else
                                    {
                                        errStr = errStr + "Giá trị điểm 15P không hợp lệ; ";
                                        hasErr = true;
                                    }
                                }
                                else
                                {
                                    errStr = errStr + "Giá trị điểm 15P không hợp lệ; ";
                                    hasErr = true;
                                }

                                curCol = curCol + 8;

                                //Diem 1 tiet
                                decimal v1, v2, v3, v4, v5, v6;
                                string strV1 = GetCellString(sheet, curRow, curCol).Replace(',', '.');
                                string strV2 = GetCellString(sheet, curRow, curCol + 1).Replace(',', '.');
                                string strV3 = GetCellString(sheet, curRow, curCol + 2).Replace(',', '.');
                                string strV4 = GetCellString(sheet, curRow, curCol + 3).Replace(',', '.');
                                string strV5 = GetCellString(sheet, curRow, curCol + 4).Replace(',', '.');
                                string strV6 = GetCellString(sheet, curRow, curCol + 5).Replace(',', '.');

                                if ((decimal.TryParse(strV1, NumberStyles.Any, CultureInfo.InvariantCulture, out v1) || String.IsNullOrEmpty(strV1))
                                    && (decimal.TryParse(strV2, NumberStyles.Any, CultureInfo.InvariantCulture, out v2) || String.IsNullOrEmpty(strV2))
                                    && (decimal.TryParse(strV3, NumberStyles.Any, CultureInfo.InvariantCulture, out v3) || String.IsNullOrEmpty(strV3))
                                    && (decimal.TryParse(strV4, NumberStyles.Any, CultureInfo.InvariantCulture, out v4) || String.IsNullOrEmpty(strV4))
                                    && (decimal.TryParse(strV5, NumberStyles.Any, CultureInfo.InvariantCulture, out v5) || String.IsNullOrEmpty(strV5))
                                    && (decimal.TryParse(strV6, NumberStyles.Any, CultureInfo.InvariantCulture, out v6) || String.IsNullOrEmpty(strV6)))
                                {
                                    if (0 <= v1 && v1 <= 10
                                        && 0 <= v2 && v2 <= 10
                                        && 0 <= v3 && v3 <= 10
                                        && 0 <= v4 && v4 <= 10
                                        && 0 <= v5 && v5 <= 10
                                        && 0 <= v6 && v6 <= 10)
                                    {
                                        if (!String.IsNullOrEmpty(strV1)) model.V1 = Decimal.Round(v1, 1);
                                        if (!String.IsNullOrEmpty(strV2)) model.V2 = Decimal.Round(v2, 1);
                                        if (!String.IsNullOrEmpty(strV3)) model.V3 = Decimal.Round(v3, 1);
                                        if (!String.IsNullOrEmpty(strV4)) model.V4 = Decimal.Round(v4, 1);
                                        if (!String.IsNullOrEmpty(strV5)) model.V5 = Decimal.Round(v5, 1);
                                        if (!String.IsNullOrEmpty(strV6)) model.V6 = Decimal.Round(v6, 1);

                                    }
                                    else
                                    {
                                        errStr = errStr + "Giá trị điểm 1T không hợp lệ; ";
                                        hasErr = true;
                                    }
                                }
                                else
                                {
                                    errStr = errStr + "Giá trị điểm 1T không hợp lệ; ";
                                    hasErr = true;
                                }

                                curCol = curCol + 10;

                                //Diem HK
                                decimal hk;
                                string strHK = GetCellString(sheet, curRow, curCol).Replace(',', '.');

                                if ((decimal.TryParse(strHK, NumberStyles.Any, CultureInfo.InvariantCulture, out hk) || String.IsNullOrEmpty(strHK)))
                                {
                                    if (0 <= hk && hk <= 10)
                                    {
                                        if (!String.IsNullOrEmpty(strHK)) model.HK = Decimal.Round(hk, 1);

                                    }
                                    else
                                    {
                                        errStr = errStr + "Giá trị điểm HK không hợp lệ; ";
                                        hasErr = true;
                                    }
                                }
                                else
                                {
                                    errStr = errStr + "Giá trị điểm HK không hợp lệ; ";
                                    hasErr = true;
                                }

                                curCol++;

                                //Diem TBHK1, TBHK2
                                if (Semester == 1)
                                {
                                    decimal tb1;
                                    string strTB1 = GetCellString(sheet, curRow, curCol).Replace(',', '.');

                                    if (decimal.TryParse(strTB1, NumberStyles.Any, CultureInfo.InvariantCulture, out tb1) || String.IsNullOrEmpty(strTB1))
                                    {
                                        if (0 <= tb1 && tb1 <= 10)
                                        {
                                            if (!String.IsNullOrEmpty(strTB1)) model.TBK1 = Decimal.Round(tb1, 1);

                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else
                                    {
                                        errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                        hasErr = true;
                                    }
                                }
                                else
                                {
                                    decimal tb1, tb2;
                                    string strTB1 = GetCellString(sheet, curRow, curCol).Replace(',', '.');
                                    string strTB2 = GetCellString(sheet, curRow, curCol + 1).Replace(',', '.');

                                    if ((decimal.TryParse(strTB1, NumberStyles.Any, CultureInfo.InvariantCulture, out tb1) || String.IsNullOrEmpty(strTB1))
                                        && (decimal.TryParse(strTB2, NumberStyles.Any, CultureInfo.InvariantCulture, out tb2) || String.IsNullOrEmpty(strTB2)))
                                    {
                                        if (0 <= tb1 && tb1 <= 10
                                            && 0 <= tb2 && tb2 <= 10)
                                        {
                                            if (!String.IsNullOrEmpty(strTB1)) model.TBK1 = Decimal.Round(tb1, 1);
                                            if (!String.IsNullOrEmpty(strTB2)) model.TBK2 = Decimal.Round(tb2, 1);

                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else
                                    {
                                        errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                        hasErr = true;
                                    }
                                }
                            }
                            #endregion

                            #region Mon nhan xet
                            else
                            {
                                //Diem mieng
                                string strM1 = GetCellString(sheet, curRow, curCol);
                                string strM2 = GetCellString(sheet, curRow, curCol + 1);
                                string strM3 = GetCellString(sheet, curRow, curCol + 2);
                                string strM4 = GetCellString(sheet, curRow, curCol + 3);
                                string strM5 = GetCellString(sheet, curRow, curCol + 4);
                                if ((String.IsNullOrEmpty(strM1) || strM1.Equals("Đ") || strM1.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strM2) || strM2.Equals("Đ") || strM2.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strM3) || strM3.Equals("Đ") || strM3.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strM4) || strM4.Equals("Đ") || strM4.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strM5) || strM5.Equals("Đ") || strM5.Equals("CĐ")))
                                {
                                    if (!string.IsNullOrEmpty(strM1)) model.M1 = strM1;
                                    if (!string.IsNullOrEmpty(strM2)) model.M2 = strM2;
                                    if (!string.IsNullOrEmpty(strM3)) model.M3 = strM3;
                                    if (!string.IsNullOrEmpty(strM4)) model.M4 = strM4;
                                    if (!string.IsNullOrEmpty(strM5)) model.M5 = strM5;
                                }
                                else
                                {
                                    errStr = errStr + "Giá trị điểm miệng không hợp lệ;  ";
                                    hasErr = true;
                                }
                                curCol = curCol + 5;

                                //Diem 15P
                                string strP1 = GetCellString(sheet, curRow, curCol);
                                string strP2 = GetCellString(sheet, curRow, curCol + 1);
                                string strP3 = GetCellString(sheet, curRow, curCol + 2);
                                string strP4 = GetCellString(sheet, curRow, curCol + 3);
                                string strP5 = GetCellString(sheet, curRow, curCol + 4);
                                if ((String.IsNullOrEmpty(strP1) || strP1.Equals("Đ") || strP1.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strP2) || strP2.Equals("Đ") || strP2.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strP3) || strP3.Equals("Đ") || strP3.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strP4) || strP4.Equals("Đ") || strP4.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strP5) || strP5.Equals("Đ") || strP5.Equals("CĐ")))
                                {
                                    if (!string.IsNullOrEmpty(strP1)) model.P1 = strP1;
                                    if (!string.IsNullOrEmpty(strP2)) model.P2 = strP2;
                                    if (!string.IsNullOrEmpty(strP3)) model.P3 = strP3;
                                    if (!string.IsNullOrEmpty(strP4)) model.P4 = strP4;
                                    if (!string.IsNullOrEmpty(strP5)) model.P5 = strP5;
                                }
                                else
                                {
                                    errStr = errStr + "Giá trị điểm 15P không hợp lệ;  ";
                                    hasErr = true;
                                }
                                curCol = curCol + 8;

                                //Diem 1T
                                string strV1 = GetCellString(sheet, curRow, curCol);
                                string strV2 = GetCellString(sheet, curRow, curCol + 1);
                                string strV3 = GetCellString(sheet, curRow, curCol + 2);
                                string strV4 = GetCellString(sheet, curRow, curCol + 3);
                                string strV5 = GetCellString(sheet, curRow, curCol + 4);
                                string strV6 = GetCellString(sheet, curRow, curCol + 5);

                                if ((String.IsNullOrEmpty(strV1) || strV1.Equals("Đ") || strV1.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strV2) || strV2.Equals("Đ") || strV2.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strV3) || strV3.Equals("Đ") || strV3.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strV4) || strV4.Equals("Đ") || strV4.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strV5) || strV5.Equals("Đ") || strV5.Equals("CĐ"))
                                   && (String.IsNullOrEmpty(strV6) || strV5.Equals("Đ") || strV5.Equals("CĐ")))
                                {
                                    if (!string.IsNullOrEmpty(strV1)) model.V1 = strV1;
                                    if (!string.IsNullOrEmpty(strV2)) model.V2 = strV2;
                                    if (!string.IsNullOrEmpty(strV3)) model.V3 = strV3;
                                    if (!string.IsNullOrEmpty(strV4)) model.V4 = strV4;
                                    if (!string.IsNullOrEmpty(strV5)) model.V5 = strV5;
                                    if (!string.IsNullOrEmpty(strV6)) model.V6 = strV6;
                                }
                                else
                                {
                                    errStr = errStr + "Giá trị điểm 1T không hợp lệ;  ";
                                    hasErr = true;
                                }
                                curCol = curCol + 10;

                                //Diem HK
                                string strHK = GetCellString(sheet, curRow, curCol);
                                if ((String.IsNullOrEmpty(strHK) || strHK.Equals("Đ") || strHK.Equals("CĐ")))
                                {
                                    if (!string.IsNullOrEmpty(strHK)) model.HK = strHK;
                                }
                                else
                                {
                                    errStr = errStr + "Giá trị điểm HK không hợp lệ;  ";
                                    hasErr = true;
                                }
                                curCol++;

                                //Diem TB
                                if (Semester == 1)
                                {
                                    string strTB1 = GetCellString(sheet, curRow, curCol);
                                    if (String.IsNullOrEmpty(strTB1) || strTB1.Equals("Đ") || strTB1.Equals("CĐ"))
                                    {
                                        if (!string.IsNullOrEmpty(strTB1)) model.TBK1 = strTB1;
                                    }
                                    else
                                    {
                                        errStr = errStr + "Giá trị điểm TBHK không hợp lệ;  ";
                                        hasErr = true;
                                    }
                                }
                                else
                                {
                                    string strTB1 = GetCellString(sheet, curRow, curCol);
                                    string strTB2 = GetCellString(sheet, curRow, curCol);
                                    if ((String.IsNullOrEmpty(strTB1) || strTB1.Equals("Đ") || strTB1.Equals("CĐ"))
                                        && (String.IsNullOrEmpty(strTB2) || strTB2.Equals("Đ") || strTB2.Equals("CĐ")))
                                    {
                                        if (!string.IsNullOrEmpty(strTB1)) model.TBK1 = strTB1;
                                        if (!string.IsNullOrEmpty(strTB2)) model.TBK2 = strTB2;
                                    }
                                    else
                                    {
                                        errStr = errStr + "Giá trị điểm TBHK không hợp lệ;  ";
                                        hasErr = true;
                                    }
                                }
                            }


                            #endregion

                            sheet.SetCellValue(curRow, errCol, errStr);

                            curRow++;
                            curCol = firstCol;
                        }

                        sheet.GetRange(headerRow, firstCol, curRow - 1, errCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);

                        if (hasErr)
                        {
                            Session[ImportFromOtherSystemConstants.SESSION_KEY_MARK] = book;
                            Session[ImportFromOtherSystemConstants.SESSION_FILE_NAME_MARK] = file.FileName;
                            res = Json(new JsonMessage("Lỗi import dữ liệu Điểm từ eSchool – Quảng Ích: Trong file tồn tại dữ liệu không hợp lệ. Thầy cô có muốn tải file mô tả lỗi?", "validate"));
                            return res;
                        }
                        else
                        {
                            List<ImportFromOtherSystemBO> lstBO = new List<ImportFromOtherSystemBO>();
                            ImportFromOtherSystemBO bo;
                            for (int i = 0; i < lstModel.Count; i++)
                            {
                                ImportMarkModel m = lstModel[i];
                                bo = new ImportFromOtherSystemBO();
                                bo.ClassID = m.ClassID;
                                bo.HK = m.HK;
                                bo.IsComment = m.IsComment;
                                bo.M1 = m.M1;
                                bo.M2 = m.M2;
                                bo.M3 = m.M3;
                                bo.M4 = m.M4;
                                bo.M5 = m.M5;
                                bo.P1 = m.P1;
                                bo.P2 = m.P2;
                                bo.P3 = m.P3;
                                bo.P4 = m.P4;
                                bo.P5 = m.P5;
                                bo.PupilID = m.PupilID;
                                bo.SubjectID = m.SubjectID;
                                bo.TBK1 = m.TBK1;
                                bo.TBK2 = m.TBK2;
                                bo.V1 = m.V1;
                                bo.V2 = m.V2;
                                bo.V3 = m.V3;
                                bo.V4 = m.V4;
                                bo.V5 = m.V5;
                                bo.V6 = m.V6;
                                bo.Semester = Semester.Value;
                                lstBO.Add(bo);
                            }

                            if (lstBO.Count > 0)
                            {
                                AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                                if (UtilsBusiness.IsMoveHistory(aca))
                                {
                                    MarkRecordHistoryBusiness.ImportFromOtherSystem(lstBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                        _globalInfo.AppliedLevel.Value, ClassID.Value, SubjectID.Value, Semester.Value, isComment, _globalInfo.EmployeeID);
                                }
                                else
                                {
                                    MarkRecordBusiness.ImportFromOtherSystem(lstBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                        _globalInfo.AppliedLevel.Value, ClassID.Value, SubjectID.Value, Semester.Value, isComment, _globalInfo.EmployeeID);
                                }
                            }

                            ViewData[CommonKey.AuditActionKey.Description] = "Import dữ liệu từ hệ thống khác";
                            ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT;
                            ViewData[CommonKey.AuditActionKey.userFunction] = "Import dữ liệu từ hệ thống khác";
                            ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                            ViewData[CommonKey.AuditActionKey.userDescription] = String.Format("Import dữ liệu điểm từ Hệ thống {0} cho lớp {1}, Môn {2}", "eSchool – Quảng Ích", cp.DisplayName, sc.DisplayName);

                            res = Json(new JsonMessage("Import thành công.", "success"));
                            return res;


                        }
                    }

                    #endregion
                }
            }
            else
            {
                if (DataType == ImportFromOtherSystemConstants.DATA_TYPE_PUPIL_PROFILE)
                {
                    #region Import hoc sinh

                    List<PupilProfile> lstPP = (from pf in PupilProfileBusiness.All
                                                join poc in PupilOfClassBusiness.All on pf.PupilProfileID equals poc.PupilID
                                                where poc.SchoolID == _globalInfo.SchoolID.Value
                                                && pf.IsActive
                                                select pf).ToList();

                    if (file == null)
                    {
                        res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), "error"));
                        return res;
                    }

                    if (!file.FileName.EndsWith(".xls") && !file.FileName.EndsWith(".xlsx"))
                    {
                        res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                        return res;
                    }
                    int maxSize = 3145728;
                    if (file.ContentLength > maxSize)
                    {
                        //JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                        res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "3 MB"), "error"));
                        return res;
                    }

                    List<Province> lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
                    List<District> lstDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
                    List<Commune> lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
                    IQueryable<Village> lstVillage = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });

                    var lstEthnic = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.EthnicID, u.EthnicName }).ToList().Select(u => new ComboObject(u.EthnicID.ToString(), u.EthnicName.ToLower())).ToList();
                    var lstPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.PolicyTargetID, u.Resolution }).ToList().Select(u => new ComboObject(u.PolicyTargetID.ToString(), u.Resolution.ToLower())).ToList();
                    List<ClassProfile> lstClass = getClassFromEducationLevel(EducationLevel);
                    var listReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ReligionID, u.Resolution }).ToList().Select(u => new ComboObject(u.ReligionID.ToString(), u.Resolution.ToLower())).ToList();

                    IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                    if (book == null)
                    {
                        res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                        return res;
                    }
                    IVTWorksheet sheet = book.GetSheet(1);


                    bool hasErr = false;
                    List<ImportPupilProfileModel> lstModel = new List<ImportPupilProfileModel>();
                    ImportPupilProfileModel model;
                    List<ClassProfile> lstImportedClass = new List<ClassProfile>();
                    List<Village> lstVillageInsert = new List<Village>();

                    sheet.ShiftSheetDown();

                    //kiem tra dinh dang cot trong file

                    int firstRow = 8;

                    if (!GetCellString(sheet, "A" + firstRow).Equals("STT")
                        || !GetCellString(sheet, "B" + firstRow).Equals("Lớp học")
                        || !GetCellString(sheet, "C" + firstRow).Equals("Mã học sinh")
                        || !GetCellString(sheet, "D" + firstRow).Equals("Mã VEMIS")
                        || !GetCellString(sheet, "E" + firstRow).Equals("Sổ danh bộ")
                        || !GetCellString(sheet, "F" + firstRow).Equals("Họ và tên")
                        || !GetCellString(sheet, "G" + firstRow).Equals("Ngày sinh")
                        || !GetCellString(sheet, "H" + firstRow).Equals("Giới tính")
                        || !GetCellString(sheet, "I" + firstRow).Equals("Chỗ ở hiện nay")
                        || !GetCellString(sheet, "M" + firstRow).Equals("Hộ khẩu thường trú")
                        || !GetCellString(sheet, "Q" + firstRow).Equals("Nơi sinh")
                        || !GetCellString(sheet, "R" + firstRow).Equals("Quê quán")
                        || !GetCellString(sheet, "S" + firstRow).Equals("Chứng minh thư")
                        || !GetCellString(sheet, "T" + firstRow).Equals("Ngày cấp CMT")
                        || !GetCellString(sheet, "U" + firstRow).Equals("Nơi cấp CMT")
                        || !GetCellString(sheet, "V" + firstRow).Equals("Dân tộc")
                        || !GetCellString(sheet, "W" + firstRow).Equals("Tôn giáo")
                        || !GetCellString(sheet, "X" + firstRow).Equals("Diện chính sách")
                        || !GetCellString(sheet, "Y" + firstRow).Equals("Cận nghèo")
                        || !GetCellString(sheet, "Z" + firstRow).Equals("Đoàn viên")
                        || !GetCellString(sheet, "AA" + firstRow).Equals("Đội viên")
                        || !GetCellString(sheet, "AB" + firstRow).Equals("Con giáo viên")
                        || !GetCellString(sheet, "AC" + firstRow).Equals("Tên cha")
                        || !GetCellString(sheet, "AD" + firstRow).Equals("Nghề nghiệp cha")
                        || !GetCellString(sheet, "AE" + firstRow).Equals("Năm sinh cha")
                        || !GetCellString(sheet, "AF" + firstRow).Equals("Tên mẹ")
                        || !GetCellString(sheet, "AG" + firstRow).Equals("Nghề nghiệp mẹ")
                        || !GetCellString(sheet, "AH" + firstRow).Equals("Năm sinh mẹ")
                        || !GetCellString(sheet, "AI" + firstRow).Equals("Điện thoại DĐ")
                        || !GetCellString(sheet, "AJ" + firstRow).Equals("Email")
                        || !GetCellString(sheet, "AK" + firstRow).Equals("Điện thoại bố")
                        || !GetCellString(sheet, "AL" + firstRow).Equals("Điện thoại mẹ")
                        || !GetCellString(sheet, "AM" + firstRow).Equals("Ghi chú"))
                    {
                        res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                        return res;
                    }

                    int curRow = firstRow + 2;
                    int firstCol = 1;
                    int lastCol = 39;
                    int curCol = firstCol;
                    int errCol = lastCol + 1;

                    //add them cot mo ta loi cho sheet
                    sheet.SetCellValue(firstRow, errCol, "Mô tả lỗi");
                    sheet.GetRange(firstRow, errCol, firstRow, errCol).SetFontStyle(true, null, false, null, false, false);
                    sheet.GetRange(firstRow, errCol, firstRow, errCol).WrapText();
                    //sheet.GetRange(firstRow, errCol, firstRow, errCol).AutoFitRowHeight();
                    sheet.SetColumnWidth(errCol, 70);

                    while (!string.IsNullOrEmpty(GetCellString(sheet, "A" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "B" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "C" + curRow))
                        || !string.IsNullOrEmpty(GetCellString(sheet, "F" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "G" + curRow)))
                    {
                        string errStr = String.Empty;
                        model = new ImportPupilProfileModel();
                        lstModel.Add(model);

                        //STT
                        curCol++;

                        //Lop
                        string className = GetCellString(sheet, curRow, curCol);
                        ClassProfile cp = lstClass.FirstOrDefault(o => o.DisplayName == className);
                        model.ClassName = className;
                        if (cp != null)
                        {
                            model.ClassID = cp.ClassProfileID;
                        }
                        else
                        {
                            errStr = errStr + "Lớp học không tồn tại; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Ma hoc sinh
                        string pupilCode = GetCellString(sheet, curRow, curCol);
                        model.PupilCode = pupilCode;
                        if (string.IsNullOrEmpty(pupilCode))
                        {
                            errStr = errStr + "Mã học sinh không được bỏ trống; ";
                            hasErr = true;
                        }
                        else if (pupilCode.Length > 30)
                        {
                            errStr = errStr + "Mã học sinh không được dài quá 30 ký tự; ";
                            hasErr = true;
                        }
                        else if (lstPP.Any(o => o.PupilCode == pupilCode))
                        {
                            errStr = errStr + "Mã học sinh đã tồn tại; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Ma vemis
                        curCol++;

                        //So danh bo
                        model.SDB = GetCellString(sheet, curRow, curCol);
                        if (model.SDB.Length > 30)
                        {
                            errStr = errStr + "Sổ danh bộ không được dài quá 30 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Ho va ten
                        model.FullName = GetCellString(sheet, curRow, curCol);
                        if (String.IsNullOrEmpty(model.FullName))
                        {
                            errStr = errStr + "Họ và tên không được bỏ trống; ";
                            hasErr = true;
                        }
                        else if (model.FullName.Length > 100)
                        {
                            errStr = errStr + "Họ và tên không được dài quá 100 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Ngay sinh
                        model.IBirthDay = GetCellString(sheet, curRow, curCol);
                        DateTime date;
                        string format = "d/M/yyyy";
                        if (String.IsNullOrEmpty(model.IBirthDay))
                        {
                            errStr = errStr + "Ngày sinh không được bỏ trống; ";
                            hasErr = true;
                        }
                        else if (!DateTime.TryParseExact(model.IBirthDay, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        {
                            errStr = errStr + "Ngày sinh không hợp lệ; ";
                            hasErr = true;
                        }
                        else
                        {
                            model.BirthDay = date;
                        }
                        curCol++;

                        //Gioi tinh
                        model.Genre = GetCellString(sheet, curRow, curCol);
                        if (model.Genre != "Nam" && model.Genre != "Nữ")
                        {
                            errStr = errStr + "Giới tính không hợp lệ; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Tinh
                        model.IProvince = GetCellString(sheet, curRow, curCol + 3);
                        if (String.IsNullOrEmpty(model.IProvince))
                        {
                            errStr = errStr + "Thông tin tỉnh không được bỏ trống; ";
                            hasErr = true;
                        }
                        else if (!lstProvince.Any<Province>(o => o.ProvinceName == model.IProvince))
                        {
                            errStr = errStr + "Thông tin tỉnh không tồn tại trong hệ thống; ";
                            hasErr = true;
                        }
                        else
                        {
                            Province p = lstProvince.FirstOrDefault(o => o.ProvinceName == model.IProvince);
                            model.ProvinceID = p.ProvinceID;
                        }

                        //Huyen
                        model.IDistrict = GetCellString(sheet, curRow, curCol + 2);
                        District d = lstDistrict.FirstOrDefault(o => o.DistrictName == model.IDistrict
                            && model.ProvinceID > 0 && o.ProvinceID == model.ProvinceID);
                        if (d != null)
                        {
                            model.DistrictID = d.DistrictID;
                        }

                        //Xa
                        model.ICommune = GetCellString(sheet, curRow, curCol + 1);
                        Commune c = lstCommune.FirstOrDefault(o => o.CommuneName == model.ICommune
                            && model.DistrictID.HasValue && o.DistrictID == model.DistrictID.Value);
                        if (c != null)
                        {
                            model.CommuneID = c.CommuneID;
                        }

                        if (!model.CommuneID.HasValue)
                        {
                            model.VillageID = null;
                        }

                        //Thon
                        model.IVillage = GetCellString(sheet, curRow, curCol);
                        Village v = lstVillage.FirstOrDefault(o => o.VillageName.Trim().ToUpper() == model.IVillage.Trim().ToUpper() && o.CommuneID == model.CommuneID);

                        if (v != null)
                        {
                            model.VillageID = v.VillageID;
                        }
                        else if (model.IVillage.Length > 50)
                        {
                            errStr = errStr + "Thôn không được dài quá 50 ký tự; ";
                            hasErr = true;
                        }
                        else
                        {
                            Village vil = new Village();
                            vil.VillageName = model.IVillage;
                            vil.ShortName = model.IVillage;
                            vil.CommuneID = model.CommuneID;
                            vil.CreateDate = DateTime.Now;
                            vil.IsActive = true;
                            lstVillageInsert.Add(vil);
                        }


                        curCol = curCol + 4;

                        //Ho khau thuong tru
                        string addrVillage = GetCellString(sheet, curRow, curCol);
                        string addrCommune = GetCellString(sheet, curRow, curCol + 1);
                        string addrDistrict = GetCellString(sheet, curRow, curCol + 2);
                        string addrProvince = GetCellString(sheet, curRow, curCol + 3);
                        var lstAddr = (new List<string>() { addrVillage, addrCommune, addrDistrict, addrProvince }).Where(o => !string.IsNullOrEmpty(o));
                        string addr = string.Join(", ", lstAddr);
                        model.HomeAddress = addr;
                        if (model.HomeAddress.Length > 256)
                        {
                            errStr = errStr + "Hộ khẩu thường trú không được dài quá 256 ký tự; ";
                            hasErr = true;
                        }

                        curCol = curCol + 4;

                        //Noi sinh
                        model.BirtPlace = GetCellString(sheet, curRow, curCol);
                        if (model.BirtPlace.Length > 256)
                        {
                            errStr = errStr + "Nơi sinh không được dài quá 256 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Que quan
                        model.HomeTown = GetCellString(sheet, curRow, curCol);
                        if (model.HomeTown.Length > 256)
                        {
                            errStr = errStr + "Quê quán không được dài quá 256 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //chung minh thu
                        model.IdentifyNumber = GetCellString(sheet, curRow, curCol);
                        if (model.IdentifyNumber.Length > 15)
                        {
                            errStr = errStr + "Chứng minh thư không được dài quá 15 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //ngay cap cmt
                        curCol++;

                        //noi cap cmt
                        curCol++;

                        //Dan toc
                        model.IEthnic = GetCellString(sheet, curRow, curCol);
                        ComboObject e = lstEthnic.FirstOrDefault(o => o.value == model.IEthnic.ToLower());
                        if (e != null)
                        {
                            model.EthnicID = int.Parse(e.key);
                        }
                        curCol++;

                        //Ton giao
                        model.IReligion = GetCellString(sheet, curRow, curCol);
                        ComboObject r = listReligion.FirstOrDefault(o => o.value == model.IReligion.ToLower());
                        if (r != null)
                        {
                            model.ReligionID = int.Parse(r.key);
                        }
                        curCol++;

                        //dien chinh sach
                        model.IPolicyTarget = GetCellString(sheet, curRow, curCol);
                        if (model.IPolicyTarget.ToLower() == "con liệt sĩ")
                        {
                            model.ProvinceID = 25;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con thương binh nặng 1/4")
                        {
                            model.ProvinceID = 43;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con thương binh 2/4")
                        {
                            model.ProvinceID = 44;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con thương binh 3/4")
                        {
                            model.ProvinceID = 45;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con thương binh 4/4")
                        {
                            model.ProvinceID = 56;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "kinh vùng cao")
                        {
                            model.ProvinceID = 46;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con dân tộc vùng cao")
                        {
                            model.ProvinceID = 47;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con dân tộc vùng thấp")
                        {
                            model.ProvinceID = 48;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con bệnh binh 1/4")
                        {
                            model.ProvinceID = 49;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con bệnh binh 2/4")
                        {
                            model.ProvinceID = 50;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con bệnh binh 3/4")
                        {
                            model.ProvinceID = 51;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con bệnh binh 4/4")
                        {
                            model.ProvinceID = 52;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "bộ đội xuất ngũ")
                        {
                            model.ProvinceID = 53;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con anh hùng")
                        {
                            model.ProvinceID = 38;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "con gia đình có công CM")
                        {
                            model.ProvinceID = 54;
                        }
                        else if (model.IPolicyTarget.Trim().ToLower() == "vùng khó khăn 135")
                        {
                            model.ProvinceID = 28;
                        }
                        curCol++;

                        //can ngheo
                        curCol++;

                        //Doan vien
                        string doanvien = GetCellString(sheet, curRow, curCol);
                        if (doanvien == "1")
                        {
                            model.IsYouthLeageMember = true;
                        }
                        curCol++;

                        //Doi vien
                        string doivien = GetCellString(sheet, curRow, curCol);
                        if (doivien == "1")
                        {
                            model.IsYoungPioneerMember = true;
                        }
                        curCol++;

                        //con giao vien
                        curCol++;

                        //Ho ten cha
                        model.FatherName = GetCellString(sheet, curRow, curCol);
                        if (model.FatherName.Length > 50)
                        {
                            errStr = errStr + "Họ tên cha không được dài quá 50 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Nghe nghiep cha
                        model.FatherJob = GetCellString(sheet, curRow, curCol);
                        if (model.FatherJob.Length > 50)
                        {
                            errStr = errStr + "Nghề nghiệp cha không được dài quá 50 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Nam sinh cha
                        model.IFatherBirthYear = GetCellString(sheet, curRow, curCol);
                        int fatherBirthYear;
                        if (int.TryParse(model.IFatherBirthYear, out fatherBirthYear))
                        {
                            if (fatherBirthYear >= 1900 && fatherBirthYear <= DateTime.Now.Year)
                            {
                                model.FatherBirthYear = fatherBirthYear;
                            }
                            else
                            {
                                errStr = errStr + "Năm sinh cha không hợp lệ; ";
                                hasErr = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(model.IFatherBirthYear))
                        {
                            errStr = errStr + "Năm sinh cha không hợp lệ; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Ho ten me
                        model.MotherName = GetCellString(sheet, curRow, curCol);
                        if (model.MotherName.Length > 50)
                        {
                            errStr = errStr + "Họ tên mẹ không được dài quá 50 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Nghe nghiep me
                        model.MotherJob = GetCellString(sheet, curRow, curCol);
                        if (model.MotherJob.Length > 50)
                        {
                            errStr = errStr + "Nghề nghiệp mẹ không được dài quá 50 ký tự; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Nam sinh me
                        model.IMotherBirthYear = GetCellString(sheet, curRow, curCol);
                        int motherBirthYear;
                        if (int.TryParse(model.IMotherBirthYear, out motherBirthYear))
                        {
                            if (motherBirthYear >= 1900 && motherBirthYear <= DateTime.Now.Year)
                            {
                                model.MotherBirthYear = motherBirthYear;
                            }
                            else
                            {
                                errStr = errStr + "Năm sinh mẹ không hợp lệ; ";
                                hasErr = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(model.IMotherBirthYear))
                        {
                            errStr = errStr + "Năm sinh mẹ không hợp lệ; ";
                            hasErr = true;
                        }
                        curCol++;

                        //SDT lien he
                        model.PhoneNumber = GetCellString(sheet, curRow, curCol);
                        if (!String.IsNullOrEmpty(model.PhoneNumber) && !Utils.Utils.CheckMobileNumber(model.PhoneNumber))
                        {
                            errStr = errStr + "Điện thoại DĐ không hợp lệ; ";
                            hasErr = true;
                        }
                        curCol++;

                        //Email
                        model.Email = GetCellString(sheet, curRow, curCol);
                        if (!String.IsNullOrEmpty(model.Email))
                        {
                            if (!string.IsNullOrEmpty(model.Email))
                            {
                                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                                Match match = Regex.Match(model.Email.Trim(), pattern, RegexOptions.IgnoreCase);

                                if (!match.Success)
                                {
                                    errStr = errStr + "Email không hợp lệ; ";
                                    hasErr = true;
                                }
                                else if (model.Email.Length > 50)
                                {
                                    errStr = errStr + "Email không được nhập quá 50 ký tự; ";
                                    hasErr = true;
                                }
                            }
                        }
                        curCol++;

                        //dien thoai bo
                        model.FatherMobile = GetCellString(sheet, curRow, curCol);
                        if (!String.IsNullOrEmpty(model.FatherMobile) && !Utils.Utils.CheckMobileNumber(model.FatherMobile))
                        {
                            errStr = errStr + "Điện thoại bố không hợp lệ; ";
                            hasErr = true;
                        }
                        curCol++;

                        //dien thoai me
                        model.MotherMobile = GetCellString(sheet, curRow, curCol);
                        if (!String.IsNullOrEmpty(model.MotherMobile) && !Utils.Utils.CheckMobileNumber(model.MotherMobile))
                        {
                            errStr = errStr + "Điện thoại mẹ không hợp lệ; ";
                            hasErr = true;
                        }
                        curCol++;

                        sheet.SetCellValue(curRow, errCol, errStr);

                        curRow++;
                        curCol = firstCol;
                    }

                    sheet.GetRange(firstRow, firstCol, curRow - 1, errCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);


                    if (hasErr)
                    {
                        Session[ImportFromOtherSystemConstants.SESSION_KEY_PUPIL_PROFILE] = book;
                        Session[ImportFromOtherSystemConstants.SESSION_FILE_NAME_PUPIL] = file.FileName;
                        res = Json(new JsonMessage("Lỗi import dữ liệu Hồ sơ học sinh từ Vnedu: Trong file tồn tại dữ liệu không hợp lệ. Thầy cô có muốn tải file mô tả lỗi?", "validate"));
                        return res;
                    }
                    else
                    {
                        List<OrderInsertBO> lstOrderInsert = new List<OrderInsertBO>();
                        OrderInsertBO objOrderInsert = null;
                        List<PupilProfile> lstPupilInsertPP = new List<PupilProfile>();

                        List<int> lstClassID = lstModel.Select(p => p.ClassID).Distinct().ToList();
                        List<PupilOfClassBO> lstpoc = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { })
                                                       join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                                       where lstClassID.Contains(poc.ClassID)
                                                       select new PupilOfClassBO
                                                       {
                                                           PupilID = poc.PupilID,
                                                           ClassID = poc.ClassID,
                                                           PupilCode = pf.PupilCode,
                                                           AssignedDate = poc.AssignedDate,
                                                           IsActive = pf.IsActive,
                                                           OrderInClass = poc.OrderInClass,
                                                           AcademicYearID = poc.AcademicYearID
                                                       }).ToList();

                        //Xu ly gan ordernumber theo từng lớp
                        int curClassId = 0;
                        int orderNumber = 1;
                        for (int i = 0; i < lstModel.Count; i++)
                        {
                            ImportPupilProfileModel m = lstModel[i];

                            if (m.ClassID != curClassId || curClassId == 0)
                            {
                                int startOrderNumber = lstpoc.Where(o => o.ClassID == m.ClassID
                                    && o.AcademicYearID == _globalInfo.AcademicYearID && o.IsActive).Count() > 0 ? lstpoc.Where(o => o.ClassID == m.ClassID
                                    && o.AcademicYearID == _globalInfo.AcademicYearID && o.IsActive).Max(o => o.OrderInClass.HasValue ? o.OrderInClass.Value : 0) + 1 : 1;

                                orderNumber = startOrderNumber;
                            }

                            m.OrderNumber = orderNumber++;
                            curClassId = m.ClassID;
                        }





                        DateTime enrolmentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

                        //Insert village
                        for (int i = 0; i < lstVillageInsert.Count; i++)
                        {
                            if (lstVillageInsert[i].CommuneID.HasValue && !string.IsNullOrEmpty(lstVillageInsert[i].VillageName))
                            {
                                VillageBusiness.Insert(lstVillageInsert[i]);
                            }
                        }
                        if (lstVillageInsert.Count > 0)
                        {
                            VillageBusiness.Save();
                        }

                        //Lay lai danh sach village
                        lstVillage = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });

                        for (int i = 0; i < lstModel.Count; i++)
                        {
                            ImportPupilProfileModel m = lstModel[i];

                            //Insert PupilProfile
                            var pupilProfile = new PupilProfile
                            {
                                CurrentAcademicYearID = _globalInfo.AcademicYearID.Value,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CurrentSchoolID = _globalInfo.SchoolID.Value
                            };

                            pupilProfile.PupilCode = m.PupilCode;
                            pupilProfile.FullName = m.FullName;
                            pupilProfile.Name = GetPupilName(pupilProfile.FullName);
                            pupilProfile.BirthDate = m.BirthDay;
                            if (m.Genre.Equals("Nữ"))
                            {
                                pupilProfile.Genre = 0;
                            }
                            else
                            {
                                pupilProfile.Genre = 1;
                            }

                            Village v = lstVillage.FirstOrDefault(o => o.VillageName.Trim().ToUpper() == m.IVillage.Trim().ToUpper() && o.CommuneID == m.CommuneID);

                            if (v != null)
                            {
                                pupilProfile.VillageID = v.VillageID;
                            }

                            pupilProfile.ProvinceID = m.ProvinceID;
                            pupilProfile.DistrictID = m.DistrictID;
                            pupilProfile.CommuneID = m.CommuneID;
                            pupilProfile.PermanentResidentalAddress = m.HomeAddress;
                            pupilProfile.BirthPlace = m.BirtPlace;
                            pupilProfile.HomeTown = m.HomeTown;
                            pupilProfile.IdentifyNumber = m.IdentifyNumber;
                            pupilProfile.EthnicID = m.EthnicID;
                            pupilProfile.ReligionID = m.ReligionID;
                            pupilProfile.PolicyTargetID = m.PolicyTargetID;
                            pupilProfile.IsYouthLeageMember = m.IsYouthLeageMember;
                            pupilProfile.IsYoungPioneerMember = m.IsYoungPioneerMember;
                            pupilProfile.FatherFullName = m.FatherName;
                            pupilProfile.FatherJob = m.FatherJob;
                            pupilProfile.FatherBirthDate = m.FatherBirthYear.HasValue ? (DateTime?)new DateTime(m.FatherBirthYear.Value, 1, 1) : null;
                            pupilProfile.MotherFullName = m.MotherName;
                            pupilProfile.MotherJob = m.MotherJob;
                            pupilProfile.MotherBirthDate = m.MotherBirthYear.HasValue ? (DateTime?)new DateTime(m.MotherBirthYear.Value, 1, 1) : null;
                            pupilProfile.Mobile = m.PhoneNumber;
                            pupilProfile.Email = m.Email;
                            pupilProfile.FatherMobile = m.FatherMobile;
                            pupilProfile.MotherMobile = m.MotherMobile;
                            pupilProfile.StorageNumber = m.SDB;
                            pupilProfile.CurrentClassID = m.ClassID;
                            pupilProfile.CreatedDate = DateTime.Now.Date;
                            pupilProfile.IsActive = true;
                            pupilProfile.EnrolmentDate = enrolmentDate;
                            pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                            pupilProfile.ForeignLanguageTraining = 0;

                            lstPupilInsertPP.Add(pupilProfile);
                            objOrderInsert = new OrderInsertBO();
                            objOrderInsert.ClassID = m.ClassID;
                            objOrderInsert.PupilCode = pupilProfile.PupilCode;
                            objOrderInsert.OrderNumber = m.OrderNumber;
                            lstOrderInsert.Add(objOrderInsert);

                            //kiem tra co trung STT hay ko
                            bool isExistOrder = (from pf in lstpoc
                                                 where
                                                 pf.AcademicYearID == _globalInfo.AcademicYearID
                                                 && (pf.OrderInClass == m.OrderNumber)
                                                 && pf.ClassID == m.ClassID
                                                 && pf.IsActive
                                                 select pf).Count() > 0
                                                 ||
                                                 lstOrderInsert.Where(o => o.ClassID == m.ClassID && o.OrderNumber == m.OrderNumber).Count() > 1;
                            if (isExistOrder)
                            {
                                res = Json(new JsonMessage("STT không được trùng với HS khác", "error"));
                                return res;
                            }

                            //Kiem tra co trung Code hay ko
                            bool isExistCode = (from pf in lstpoc
                                                where pf.PupilCode == pupilProfile.PupilCode
                                                && pf.IsActive
                                                select pf).Count() > 0
                                                ||
                                                lstPupilInsertPP.Where(o => o.PupilCode == pupilProfile.PupilCode).Count() > 1;

                            if (isExistCode)
                            {
                                res = Json(new JsonMessage("Mã học sinh đã tồn tại", "error"));
                                return res;
                            }

                            ClassProfile cp = lstClass.FirstOrDefault(o => o.ClassProfileID == m.ClassID);
                            lstImportedClass.Add(cp);
                        }

                        if (lstPupilInsertPP.Count > 0)
                        {
                            PupilProfileBusiness.ImportFromOtherSystem(_globalInfo.UserAccountID, lstPupilInsertPP, lstOrderInsert, lstClassID);
                        }

                        string strLstClass = string.Join(", ", lstImportedClass.Select(o => o.DisplayName).Distinct().ToList());

                        ViewData[CommonKey.AuditActionKey.Description] = "Import dữ liệu từ hệ thống khác";
                        ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT;
                        ViewData[CommonKey.AuditActionKey.userFunction] = "Import dữ liệu từ hệ thống khác";
                        ViewData[CommonKey.AuditActionKey.userDescription] = String.Format("Import dữ liệu hồ sơ học sinh từ Hệ thống Vnedu cho các lớp {0}", strLstClass);

                        res = Json(new JsonMessage(String.Format("Import thành công lớp {0}.", strLstClass), "success"));
                        return res;
                    }
                    #endregion
                }
                else
                {
                    #region import diem
                    if (!ClassID.HasValue)
                    {
                        res = Json(new JsonMessage("Thầy cô chưa chọn lớp", "error"));
                        return res;
                    }

                    if (file == null)
                    {
                        res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), "error"));
                        return res;
                    }

                    //Import tat ca mon
                    if (!SubjectID.HasValue)
                    {
                        if (!file.FileName.EndsWith(".xls") && !file.FileName.EndsWith(".xlsx"))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        int maxSize = 10485760;
                        if (file.ContentLength > maxSize)
                        {
                            //JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                            res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "10 MB"), "error"));
                            return res;
                        }

                        //Danh sach mon hoc
                        List<SubjectCatBO> lstCs = GetListSubjectByClass(ClassID.Value, Semester.Value);
                        List<int> lstSubjectId = lstCs.Select(o => o.SubjectCatID).ToList();

                        //Danh sach mon hoc co trong file zip
                        List<SubjectMappingModel> lstSubjectMapping = new List<SubjectMappingModel>();
                        IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);

                        foreach (IVTWorksheet sheet in book.GetSheets())
                        {
                            SubjectMappingModel sm = new SubjectMappingModel();
                            sheet.ShiftSheetDown();
                            //Lay ten mon
                            var subjectName = GetCellString(sheet, "A4") ?? "";
                            string[] arr = subjectName.Split('-');

                            if (arr.Length >= 2 && subjectName.Contains("MÔN"))
                            {
                                subjectName = arr[1].Replace("MÔN", string.Empty).Trim();
                                sm.OriginalSubjectName = subjectName;

                                QiSubjectMapping qSubject = QiSubjectMappingBusiness.All.FirstOrDefault(o => o.QiSubjectName == subjectName && o.AppliedLevel == _globalInfo.AppliedLevel
                                    && lstSubjectId.Contains(o.MappedSubjectID));

                                if (qSubject != null)
                                {
                                    sm.MappedSubjectId = qSubject.MappedSubjectID;
                                    sm.IsChecked = true;
                                }

                                lstSubjectMapping.Add(sm);
                            }
                        }


                        if (lstSubjectMapping.Count == 0)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        //Lay danh sach mon hoc
                        ViewData[ImportFromOtherSystemConstants.LIST_SUBJECT] = lstCs;

                        return PartialView("_SubjectSelection", lstSubjectMapping);
                    }
                    else
                    {
                        if (!file.FileName.EndsWith(".xls") && !file.FileName.EndsWith(".xlsx"))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }
                        int maxSize = 3145728;
                        if (file.ContentLength > maxSize)
                        {
                            //JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                            res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "3 MB"), "error"));
                            return res;
                        }

                        bool hasErr = false;
                        List<ImportMarkModel> lstModel = new List<ImportMarkModel>();
                        ImportMarkModel model;
                        IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                        if (book == null)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }
                        IVTWorksheet sheet = book.GetSheet(1);
                        sheet.ShiftSheetDown();

                        int? firstR = sheet.GetFirstUsedRow(1);

                        int firstRow;
                        if (!firstR.HasValue)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }
                        else
                        {
                            firstRow = firstR.Value;
                        }

                        int headerRow = firstRow + 5;
                        int startRow = firstRow + 7;
                        int curRow = startRow;
                        int firstCol = 1;


                        //kiem tra dinh dang cot trong file
                        var A = GetCellString(sheet, "A" + headerRow) ?? "";
                        var C = GetCellString(sheet, "C" + headerRow) ?? "";
                        var E = GetCellString(sheet, "E" + headerRow) ?? "";

                        if (!A.ToLower().Equals("stt")
                                        || !C.ToLower().Equals("họ và tên")
                                        || !E.ToLower().Equals("miệng"))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        //Tim cot cuoi cung
                        int lastCol = 0;
                        string lastColHeader = Semester == 1 ? "điểm\ntbhk" : "cn";
                        for (int i = 1; i < 200; i++)
                        {
                            var cellVal = GetCellString(sheet, headerRow, i);
                            if (cellVal.ToLower().Equals(lastColHeader))
                            {
                                lastCol = i;
                                break;
                            }
                        }

                        if (lastCol == 0)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        int curCol = firstCol;
                        int errCol = lastCol + 1;

                        //Kiem tra truong chua khai bao so con diem
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                        dic["SchoolID"] = _globalInfo.SchoolID.Value;
                        dic["Semester"] = Semester.Value;
                        SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
                        if (SemeterDeclaration == null)
                        {
                            res = Json(new JsonMessage(Res.Get("Validate_School_NotMark"), "error"));
                            return res;
                        }

                        //add them cot mo ta loi cho sheet
                        sheet.SetCellValue(headerRow, errCol, "Mô tả lỗi");
                        sheet.GetRange(headerRow, errCol, headerRow, errCol).SetFontStyle(true, null, false, null, false, false);
                        sheet.SetColumnWidth(errCol, 70);

                        //Lay danh sach hoc sinh cua lop
                        dic = new Dictionary<string, object>();
                        dic["ClassID"] = ClassID.Value;
                        dic["SchoolID"] = _globalInfo.SchoolID;
                        dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                        dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                        List<PupilProfileBO> lstPP = PupilOfClassBusiness.Search(dic)
                            .Select(o => new PupilProfileBO
                            {
                                FullName = o.PupilProfile.FullName,
                                BirthDate = o.PupilProfile.BirthDate,
                                OrderInClass = o.OrderInClass,
                                PupilProfileID = o.PupilID,
                                PupilCode = o.PupilProfile.PupilCode
                            }).ToList();
                        ;

                        List<SubjectCatBO> lstCs = GetListSubjectByClass(ClassID.Value, Semester.Value);
                        SubjectCatBO sc = lstCs.FirstOrDefault(o => o.SubjectCatID == SubjectID.Value);
                        bool isComment = sc.IsCommenting.HasValue && sc.IsCommenting.Value == 1 ? true : false;

                        while (!string.IsNullOrEmpty(GetCellString(sheet, "B" + curRow)))
                        {
                            string errStr = String.Empty;
                            model = new ImportMarkModel();
                            lstModel.Add(model);
                            model.ClassID = ClassID.Value;
                            model.SubjectID = SubjectID.Value;
                            //Kiem tra ton tai hoc sinh
                            //Ma hoc sinh
                            string pupilCode = GetCellString(sheet, curRow, 2);
                            PupilProfileBO pp;
                            pp = lstPP.FirstOrDefault(o => o.PupilCode.Equals(pupilCode));

                            if (pp == null)
                            {
                                errStr = errStr + "Mã học sinh không hợp lệ; ";
                                hasErr = true;
                            }
                            else
                            {
                                model.PupilID = pp.PupilProfileID;
                            }

                            //STT
                            curCol = curCol + 2;

                            //Ho ten
                            curCol = curCol + 2;

                            model.ListM = new List<object>();
                            model.ListP = new List<object>();
                            model.ListV = new List<object>();

                            #region Mon tinh diem
                            if (!isComment)
                            {
                                while (curCol <= lastCol)
                                {
                                    string cellVal = GetCellString(sheet, curRow, curCol);
                                    string header = GetCellString(sheet, headerRow, curCol);
                                    string subheader = GetCellString(sheet, headerRow + 1, curCol);
                                    int mmark;
                                    decimal pvmark;

                                    bool mError = false;
                                    bool pError = false;
                                    bool vError = false;

                                    var matchM = Regex.Match(subheader, "^M[1-9]$", RegexOptions.None);
                                    var matchP = Regex.Match(subheader, "^P[1-9]$", RegexOptions.None);
                                    var matchV = Regex.Match(subheader, "^V[1-9]$", RegexOptions.None);


                                    if (matchM.Success)
                                    {
                                        if (int.TryParse(cellVal, out mmark))
                                        {
                                            if (0 <= mmark && mmark <= 10)
                                            {
                                                model.ListM.Add(mmark);
                                            }
                                            else
                                            {
                                                if (!mError)
                                                {
                                                    errStr = errStr + "Giá trị điểm miệng không hợp lệ; ";
                                                    hasErr = true;
                                                    mError = true;
                                                }
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.ListM.Add(null);
                                        }
                                        else
                                        {
                                            if (!mError)
                                            {
                                                errStr = errStr + "Giá trị điểm miệng không hợp lệ; ";
                                                hasErr = true;
                                                mError = true;
                                            }
                                        }
                                    }
                                    else if (matchP.Success)
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                model.ListP.Add(Decimal.Round(pvmark, 1));
                                            }
                                            else
                                            {
                                                if (!pError)
                                                {
                                                    errStr = errStr + "Giá trị điểm 15 phút không hợp lệ; ";
                                                    hasErr = true;
                                                    pError = true;
                                                }
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.ListP.Add(null);
                                        }
                                        else
                                        {
                                            if (!pError)
                                            {
                                                errStr = errStr + "Giá trị điểm 15 phút không hợp lệ; ";
                                                hasErr = true;
                                                pError = true;
                                            }
                                        }
                                    }
                                    else if (matchV.Success)
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                model.ListV.Add(Decimal.Round(pvmark, 1));
                                            }
                                            else
                                            {
                                                if (!vError)
                                                {
                                                    errStr = errStr + "Giá trị điểm 1 tiết không hợp lệ; ";
                                                    hasErr = true;
                                                    vError = true;
                                                }
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.ListV.Add(null);
                                        }
                                        else
                                        {
                                            if (!vError)
                                            {
                                                errStr = errStr + "Giá trị điểm 1 tiết không hợp lệ; ";
                                                hasErr = true;
                                                vError = true;
                                            }
                                        }
                                    }
                                    else if (header.Equals("Học\nkỳ"))
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                model.HK = Decimal.Round(pvmark, 1);
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm học kỳ không hợp lệ; ";
                                                hasErr = true;
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.HK = null;
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm học kỳ không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else if (header.Equals("Điểm\nTBHK"))
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                if (Semester == 1)
                                                {
                                                    model.TBK1 = Decimal.Round(pvmark, 1);
                                                }
                                                else
                                                {
                                                    model.TBK2 = Decimal.Round(pvmark, 1);
                                                }
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                                hasErr = true;
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            if (Semester == 1)
                                            {
                                                model.TBK1 = null;
                                            }
                                            else
                                            {
                                                model.TBK2 = null;
                                            }
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else if (header.Equals("CN"))
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                model.TBCN = Decimal.Round(pvmark, 1);
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm CN không hợp lệ; ";
                                                hasErr = true;
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.TBCN = null;
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm CN không hợp lệ; ";
                                            hasErr = true;
                                            vError = true;
                                        }
                                    }

                                    curCol++;
                                }

                            }
                            #endregion

                            #region Mon nhan xet
                            else
                            {
                                while (curCol <= lastCol)
                                {
                                    string cellVal = GetCellString(sheet, curRow, curCol);
                                    string header = GetCellString(sheet, headerRow, curCol);

                                    bool mError = false;
                                    bool pError = false;
                                    bool vError = false;

                                    var matchM = Regex.Match(header, "^M[1-9]$", RegexOptions.None);
                                    var matchP = Regex.Match(header, "^P[1-9]$", RegexOptions.None);
                                    var matchV = Regex.Match(header, "^V[1-9]$", RegexOptions.None);


                                    if (matchM.Success)
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal)) model.ListM.Add(cellVal);
                                        }
                                        else
                                        {
                                            if (!mError)
                                            {
                                                errStr = errStr + "Giá trị điểm miệng không hợp lệ;  ";
                                                hasErr = true;
                                                mError = true;
                                            }
                                        }
                                    }
                                    else if (matchP.Success)
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal)) model.ListP.Add(cellVal);
                                        }
                                        else
                                        {
                                            if (!pError)
                                            {
                                                errStr = errStr + "Giá trị điểm 15 phút không hợp lệ; ";
                                                hasErr = true;
                                                pError = true;
                                            }
                                        }
                                    }
                                    else if (matchV.Success)
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal)) model.ListV.Add(cellVal);
                                        }
                                        else
                                        {
                                            if (!vError)
                                            {
                                                errStr = errStr + "Giá trị điểm 1 tiết không hợp lệ; ";
                                                hasErr = true;
                                                vError = true;
                                            }
                                        }
                                    }
                                    else if (header.Equals("Học\nkỳ"))
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal)) model.HK = cellVal;
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm học kỳ không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else if (header.Equals("Điểm\nTBHK"))
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal))
                                            {
                                                if (Semester == 1)
                                                {
                                                    model.TBK1 = cellVal;
                                                }
                                                else
                                                {
                                                    model.TBK2 = cellVal;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else if (header.Equals("CN"))
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal)) model.TBCN = cellVal;
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm CN không hợp lệ;  ";
                                            hasErr = true;
                                        }
                                    }

                                    curCol++;
                                }
                            }

                            #endregion

                            sheet.SetCellValue(curRow, errCol, errStr);

                            curRow++;
                            curCol = firstCol;
                        }

                        sheet.GetRange(headerRow, firstCol, curRow - 1, errCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);

                        if (hasErr)
                        {
                            Session[ImportFromOtherSystemConstants.SESSION_KEY_MARK] = book;
                            Session[ImportFromOtherSystemConstants.SESSION_FILE_NAME_MARK] = file.FileName;
                            res = Json(new JsonMessage("Lỗi import dữ liệu Điểm từ Vnedu: Trong file tồn tại dữ liệu không hợp lệ. Thầy cô có muốn tải file mô tả lỗi?", "validate"));
                            return res;
                        }
                        else
                        {
                            List<ImportFromOtherSystemBO> lstBO = new List<ImportFromOtherSystemBO>();
                            ImportFromOtherSystemBO bo;
                            for (int i = 0; i < lstModel.Count; i++)
                            {
                                ImportMarkModel m = lstModel[i];
                                bo = new ImportFromOtherSystemBO();
                                bo.ClassID = m.ClassID;
                                bo.HK = m.HK;
                                bo.IsComment = m.IsComment;
                                bo.M1 = m.M1;
                                bo.M2 = m.M2;
                                bo.M3 = m.M3;
                                bo.M4 = m.M4;
                                bo.M5 = m.M5;
                                bo.P1 = m.P1;
                                bo.P2 = m.P2;
                                bo.P3 = m.P3;
                                bo.P4 = m.P4;
                                bo.P5 = m.P5;
                                bo.PupilID = m.PupilID;
                                bo.SubjectID = m.SubjectID;
                                bo.TBK1 = m.TBK1;
                                bo.TBK2 = m.TBK2;
                                bo.V1 = m.V1;
                                bo.V2 = m.V2;
                                bo.V3 = m.V3;
                                bo.V4 = m.V4;
                                bo.V5 = m.V5;
                                bo.V6 = m.V6;
                                bo.Semester = Semester.Value;
                                bo.lstM = m.ListM;
                                bo.lstP = m.ListP;
                                bo.lstV = m.ListV;
                                bo.TBCN = m.TBCN;
                                lstBO.Add(bo);
                            }

                            if (lstBO.Count > 0)
                            {
                                AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                                if (UtilsBusiness.IsMoveHistory(aca))
                                {
                                    MarkRecordHistoryBusiness.ImportFromOtherSystem(lstBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                        _globalInfo.AppliedLevel.Value, ClassID.Value, SubjectID.Value, Semester.Value, isComment, _globalInfo.EmployeeID);
                                }
                                else
                                {
                                    MarkRecordBusiness.ImportFromOtherSystem(lstBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                        _globalInfo.AppliedLevel.Value, ClassID.Value, SubjectID.Value, Semester.Value, isComment, _globalInfo.EmployeeID);
                                }
                            }

                            ViewData[CommonKey.AuditActionKey.Description] = "Import dữ liệu từ hệ thống khác";
                            ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT;
                            ViewData[CommonKey.AuditActionKey.userFunction] = "Import dữ liệu từ hệ thống khác";
                            ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                            ViewData[CommonKey.AuditActionKey.userDescription] = String.Format("Import dữ liệu điểm từ Hệ thống {0} cho lớp {1}, Môn {2}", "Vnedu", cp.DisplayName, sc.DisplayName);

                            res = Json(new JsonMessage("Import thành công.", "success"));
                            return res;
                        }
                    }

                    #endregion
                }
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        [ActionAudit]
        public ActionResult ImportMultiSubject(int SystemType, int DataType, int EducationLevel, int? ClassID, int? Semester, string ArrSubjectID, HttpPostedFileBase file)
        {
            JsonResult res;


            if (SystemType == 1)
            {
                #region import diem
                if (!ClassID.HasValue)
                {
                    res = Json(new JsonMessage("Thầy cô chưa chọn lớp", "error"));
                    return res;
                }

                if (file == null)
                {
                    res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), "error"));
                    return res;
                }


                if (!file.FileName.EndsWith(".zip"))
                {
                    res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                    return res;
                }

                //List<int> ListSubjectID = this.GetIDsFromString(ArrSubjectID);

                List<QiSubjectMapping> lstMappedSubject = new List<QiSubjectMapping>();
                string[] arrMappedSubject = ArrSubjectID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (arrMappedSubject.Length == 0)
                {
                    res = Json(new JsonMessage("Thầy cô chưa chọn môn học", "error"));
                    return res;
                }
                for (int i = 0; i < arrMappedSubject.Length; i++)
                {
                    string[] arr = arrMappedSubject[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    string qisubjectName = arr[0];
                    int subjectId = int.Parse(arr[1]);
                    string mappedSubjectName = arr[2];
                    lstMappedSubject.Add(new QiSubjectMapping { QiSubjectName = qisubjectName, MappedSubjectID = subjectId, MappedSubjectName = mappedSubjectName });
                }

                List<int> lstSubjectId = lstMappedSubject.Select(o => o.MappedSubjectID).Distinct().ToList();

                for (int i = 0; i < lstSubjectId.Count; i++)
                {
                    int count = lstMappedSubject.Where(o => o.MappedSubjectID == lstSubjectId[i]).Count();
                    if (count > 1)
                    {
                        QiSubjectMapping firstMapped = lstMappedSubject.Where(o => o.MappedSubjectID == lstSubjectId[i]).FirstOrDefault();
                        res = Json(new JsonMessage(string.Format("{0} được chọn nhiều hơn 1 lần", firstMapped.MappedSubjectName), "error"));
                        return res;
                    }

                }

                int maxSize = 10485760;
                if (file.ContentLength > maxSize)
                {
                    //JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                    res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "10 MB"), "error"));
                    return res;
                }

                //Lay danh sach hoc sinh cua lop
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = ClassID.Value;
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                List<PupilProfileBO> lstPP = PupilOfClassBusiness.Search(dic)
                    .Select(o => new PupilProfileBO
                    {
                        FullName = o.PupilProfile.FullName,
                        BirthDate = o.PupilProfile.BirthDate,
                        OrderInClass = o.OrderInClass,
                        PupilProfileID = o.PupilID,
                        PupilCode = o.PupilProfile.PupilCode
                    }).ToList();
                ;

                //Danh sach mon hoc cua lop
                List<SubjectCatBO> lstCs = GetListSubjectByClass(ClassID.Value, Semester.Value);

                List<ImportMarkModel> lstModel = new List<ImportMarkModel>();
                ImportMarkModel model;

                List<ErrorViewModel> lstError = new List<ErrorViewModel>();
                //Danh sach mon hoc co trong file zip
                List<QiSubjectMapping> lstQiSubject = new List<QiSubjectMapping>();

                //Kiem tra truong chua khai bao so con diem
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["Semester"] = Semester.Value;
                SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
                if (SemeterDeclaration == null)
                {
                    res = Json(new JsonMessage(Res.Get("Validate_School_NotMark"), "error"));
                    return res;
                }

                List<QiSubjectMapping> lstQiSubjectAll = QiSubjectMappingBusiness.All.ToList();
                using (ZipFile zip = ZipFile.Read(file.InputStream))
                {
                    foreach (ZipEntry e in zip)
                    {
                        IVTWorkbook book;
                        using (MemoryStream stream = new MemoryStream())
                        {
                            e.Extract(stream);
                            book = VTExport.OpenWorkbook(stream);
                        }

                        if (book != null)
                        {
                            IVTWorksheet sheet = null;

                            try
                            {
                                sheet = book.GetSheet(1);
                            }
                            catch
                            {
                                sheet = null;
                            }

                            if (sheet != null)
                            {
                                //Lay ten mon
                                var subjectName = GetCellString(sheet, "F7") ?? "";
                                string[] arr = subjectName.Split('-');
                                subjectName = arr[0].Replace("Môn học:", string.Empty).Trim();
                                QiSubjectMapping qSubject = lstMappedSubject.FirstOrDefault(o => o.QiSubjectName == subjectName);

                                if (qSubject != null)
                                {
                                    int? firstR = sheet.GetFirstUsedRow(1);
                                    int firstRow;
                                    if (!firstR.HasValue)
                                    {
                                        res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                                        return res;
                                    }
                                    else
                                    {
                                        firstRow = firstR.Value;
                                    }

                                    int headerRow = firstRow + 6;
                                    int startRow = firstRow + 10;
                                    int curRow = startRow;
                                    int firstCol = 1;
                                    int lastCol = 31;
                                    int curCol = firstCol;
                                    int errCol = lastCol + 1;

                                    //kiem tra dinh dang cot trong file
                                    var A = GetCellString(sheet, "A" + headerRow) ?? "";
                                    var B = GetCellString(sheet, "B" + headerRow) ?? "";
                                    var C = GetCellString(sheet, "C" + headerRow) ?? "";
                                    var D = GetCellString(sheet, "D" + headerRow) ?? "";
                                    var Q = GetCellString(sheet, "Q" + headerRow) ?? "";
                                    var AA = GetCellString(sheet, "AA" + headerRow) ?? "";
                                    var AB = GetCellString(sheet, "AB" + headerRow) ?? "";
                                    var AE = GetCellString(sheet, "AE" + headerRow) ?? "";

                                    if (Semester == 1 && (!A.ToLower().Equals("tt")
                                        || !B.ToLower().Equals("họ tên")
                                        || !C.ToLower().Equals("ngày sinh")
                                        || !D.ToLower().Equals("điểm hệ số 1")
                                        || !Q.ToLower().Equals("điểm hệ số 2")
                                        || !AA.ToLower().Equals("hk")
                                        || !AB.ToLower().Equals("tb\nk1")
                                        || !AE.ToLower().Equals("mã học sinh")))
                                    {
                                        continue;
                                    }

                                    if (Semester == 2 && (!A.ToLower().Equals("tt")
                                        || !B.ToLower().Equals("họ tên")
                                        || !C.ToLower().Equals("ngày sinh")
                                        || !D.ToLower().Equals("điểm hệ số 1")
                                        || !Q.ToLower().Equals("điểm hệ số 2")
                                        || !AA.ToLower().Equals("hk")
                                        || !AB.ToLower().Equals("tb\nmk1")
                                        || !AE.ToLower().Equals("mã học sinh")))
                                    {
                                        continue;
                                    }

                                    SubjectCatBO sc = lstCs.FirstOrDefault(o => o.SubjectCatID == qSubject.MappedSubjectID);
                                    bool isComment = sc.IsCommenting.HasValue && sc.IsCommenting.Value == 1 ? true : false;


                                    while (!string.IsNullOrEmpty(GetCellString(sheet, "A" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "B" + curRow)) || !string.IsNullOrEmpty(GetCellString(sheet, "C" + curRow)))
                                    {
                                        string errStr = String.Empty;
                                        bool hasErr = false;
                                        model = new ImportMarkModel();
                                        lstModel.Add(model);
                                        model.ClassID = ClassID.Value;
                                        model.SubjectID = qSubject.MappedSubjectID;
                                        model.IsComment = isComment;

                                        //Kiem tra ton tai hoc sinh
                                        //Ma hoc sinh
                                        string pupilCode = GetCellString(sheet, curRow, lastCol);
                                        PupilProfileBO pp;
                                        pp = lstPP.FirstOrDefault(o => o.PupilCode.Equals(pupilCode));

                                        if (pp == null)
                                        {
                                            errStr = errStr + "Mã học sinh không hợp lệ; ";
                                            hasErr = true;
                                        }
                                        else
                                        {
                                            model.PupilID = pp.PupilProfileID;
                                        }

                                        //STT
                                        int order;
                                        if (!int.TryParse(GetCellString(sheet, curRow, curCol), out order))
                                        {
                                            order = -1;
                                        }
                                        curCol++;

                                        //Ho ten
                                        string fullName = GetCellString(sheet, curRow, curCol);
                                        curCol++;

                                        // Ngay sinh
                                        DateTime birthDay;
                                        if (!DateTime.TryParse(GetCellString(sheet, curRow, curCol), out birthDay))
                                        {
                                            birthDay = DateTime.MinValue;
                                        }

                                        //Kiem tra hoc sinh co hop le

                                        curCol++;

                                        #region Mon tinh diem
                                        if (!isComment)
                                        {
                                            //diem mieng
                                            int m1, m2, m3, m4, m5;
                                            string strM1 = GetCellString(sheet, curRow, curCol);
                                            string strM2 = GetCellString(sheet, curRow, curCol + 1);
                                            string strM3 = GetCellString(sheet, curRow, curCol + 2);
                                            string strM4 = GetCellString(sheet, curRow, curCol + 3);
                                            string strM5 = GetCellString(sheet, curRow, curCol + 4);

                                            if ((int.TryParse(strM1, out m1) || String.IsNullOrEmpty(strM1))
                                                && (int.TryParse(strM2, out m2) || String.IsNullOrEmpty(strM2))
                                                && (int.TryParse(strM3, out m3) || String.IsNullOrEmpty(strM3))
                                                && (int.TryParse(strM4, out m4) || String.IsNullOrEmpty(strM4))
                                                && (int.TryParse(strM5, out m5) || String.IsNullOrEmpty(strM5)))
                                            {
                                                if (0 <= m1 && m1 <= 10
                                                    && 0 <= m2 && m2 <= 10
                                                    && 0 <= m3 && m3 <= 10
                                                    && 0 <= m4 && m4 <= 10
                                                    && 0 <= m5 && m5 <= 10)
                                                {
                                                    if (!String.IsNullOrEmpty(strM1)) model.M1 = m1;
                                                    if (!String.IsNullOrEmpty(strM2)) model.M2 = m2;
                                                    if (!String.IsNullOrEmpty(strM3)) model.M3 = m3;
                                                    if (!String.IsNullOrEmpty(strM4)) model.M4 = m4;
                                                    if (!String.IsNullOrEmpty(strM5)) model.M5 = m5;

                                                }
                                                else
                                                {
                                                    errStr = errStr + "Giá trị điểm miệng không hợp lệ; ";
                                                    hasErr = true;
                                                }
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm miệng không hợp lệ; ";
                                                hasErr = true;
                                            }

                                            curCol = curCol + 5;

                                            //Diem 15phut
                                            decimal p1, p2, p3, p4, p5;
                                            string strP1 = GetCellString(sheet, curRow, curCol).Replace(',', '.');
                                            string strP2 = GetCellString(sheet, curRow, curCol + 1).Replace(',', '.');
                                            string strP3 = GetCellString(sheet, curRow, curCol + 2).Replace(',', '.');
                                            string strP4 = GetCellString(sheet, curRow, curCol + 3).Replace(',', '.');
                                            string strP5 = GetCellString(sheet, curRow, curCol + 4).Replace(',', '.');

                                            if ((decimal.TryParse(strP1, NumberStyles.Any, CultureInfo.InvariantCulture, out p1) || String.IsNullOrEmpty(strP1))
                                                && (decimal.TryParse(strP2, NumberStyles.Any, CultureInfo.InvariantCulture, out p2) || String.IsNullOrEmpty(strP2))
                                                && (decimal.TryParse(strP3, NumberStyles.Any, CultureInfo.InvariantCulture, out p3) || String.IsNullOrEmpty(strP3))
                                                && (decimal.TryParse(strP4, NumberStyles.Any, CultureInfo.InvariantCulture, out p4) || String.IsNullOrEmpty(strP4))
                                                && (decimal.TryParse(strP5, NumberStyles.Any, CultureInfo.InvariantCulture, out p5) || String.IsNullOrEmpty(strP5)))
                                            {
                                                if (0 <= p1 && p1 <= 10
                                                    && 0 <= p2 && p2 <= 10
                                                    && 0 <= p3 && p3 <= 10
                                                    && 0 <= p4 && p4 <= 10
                                                    && 0 <= p5 && p5 <= 10)
                                                {
                                                    if (!String.IsNullOrEmpty(strP1)) model.P1 = Decimal.Round(p1, 1);
                                                    if (!String.IsNullOrEmpty(strP2)) model.P2 = Decimal.Round(p2, 1);
                                                    if (!String.IsNullOrEmpty(strP3)) model.P3 = Decimal.Round(p3, 1);
                                                    if (!String.IsNullOrEmpty(strP4)) model.P4 = Decimal.Round(p4, 1);
                                                    if (!String.IsNullOrEmpty(strP5)) model.P5 = Decimal.Round(p5, 1);

                                                }
                                                else
                                                {
                                                    errStr = errStr + "Giá trị điểm 15P không hợp lệ; ";
                                                    hasErr = true;
                                                }
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm 15P không hợp lệ; ";
                                                hasErr = true;
                                            }

                                            curCol = curCol + 8;

                                            //Diem 1 tiet
                                            decimal v1, v2, v3, v4, v5, v6;
                                            string strV1 = GetCellString(sheet, curRow, curCol).Replace(',', '.');
                                            string strV2 = GetCellString(sheet, curRow, curCol + 1).Replace(',', '.');
                                            string strV3 = GetCellString(sheet, curRow, curCol + 2).Replace(',', '.');
                                            string strV4 = GetCellString(sheet, curRow, curCol + 3).Replace(',', '.');
                                            string strV5 = GetCellString(sheet, curRow, curCol + 4).Replace(',', '.');
                                            string strV6 = GetCellString(sheet, curRow, curCol + 5).Replace(',', '.');

                                            if ((decimal.TryParse(strV1, NumberStyles.Any, CultureInfo.InvariantCulture, out v1) || String.IsNullOrEmpty(strV1))
                                                && (decimal.TryParse(strV2, NumberStyles.Any, CultureInfo.InvariantCulture, out v2) || String.IsNullOrEmpty(strV2))
                                                && (decimal.TryParse(strV3, NumberStyles.Any, CultureInfo.InvariantCulture, out v3) || String.IsNullOrEmpty(strV3))
                                                && (decimal.TryParse(strV4, NumberStyles.Any, CultureInfo.InvariantCulture, out v4) || String.IsNullOrEmpty(strV4))
                                                && (decimal.TryParse(strV5, NumberStyles.Any, CultureInfo.InvariantCulture, out v5) || String.IsNullOrEmpty(strV5))
                                                && (decimal.TryParse(strV6, NumberStyles.Any, CultureInfo.InvariantCulture, out v6) || String.IsNullOrEmpty(strV6)))
                                            {
                                                if (0 <= v1 && v1 <= 10
                                                    && 0 <= v2 && v2 <= 10
                                                    && 0 <= v3 && v3 <= 10
                                                    && 0 <= v4 && v4 <= 10
                                                    && 0 <= v5 && v5 <= 10
                                                    && 0 <= v6 && v6 <= 10)
                                                {
                                                    if (!String.IsNullOrEmpty(strV1)) model.V1 = Decimal.Round(v1, 1);
                                                    if (!String.IsNullOrEmpty(strV2)) model.V2 = Decimal.Round(v2, 1);
                                                    if (!String.IsNullOrEmpty(strV3)) model.V3 = Decimal.Round(v3, 1);
                                                    if (!String.IsNullOrEmpty(strV4)) model.V4 = Decimal.Round(v4, 1);
                                                    if (!String.IsNullOrEmpty(strV5)) model.V5 = Decimal.Round(v5, 1);
                                                    if (!String.IsNullOrEmpty(strV6)) model.V6 = Decimal.Round(v6, 1);

                                                }
                                                else
                                                {
                                                    errStr = errStr + "Giá trị điểm 1T không hợp lệ; ";
                                                    hasErr = true;
                                                }
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm 1T không hợp lệ; ";
                                                hasErr = true;
                                            }

                                            curCol = curCol + 10;

                                            //Diem HK
                                            decimal hk;
                                            string strHK = GetCellString(sheet, curRow, curCol).Replace(',', '.');

                                            if ((decimal.TryParse(strHK, NumberStyles.Any, CultureInfo.InvariantCulture, out hk) || String.IsNullOrEmpty(strHK)))
                                            {
                                                if (0 <= hk && hk <= 10)
                                                {
                                                    if (!String.IsNullOrEmpty(strHK)) model.HK = Decimal.Round(hk, 1);

                                                }
                                                else
                                                {
                                                    errStr = errStr + "Giá trị điểm HK không hợp lệ; ";
                                                    hasErr = true;
                                                }
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm HK không hợp lệ; ";
                                                hasErr = true;
                                            }

                                            curCol++;

                                            //Diem TBHK1, TBHK2
                                            if (Semester == 1)
                                            {
                                                decimal tb1;
                                                string strTB1 = GetCellString(sheet, curRow, curCol).Replace(',', '.');

                                                if (decimal.TryParse(strTB1, NumberStyles.Any, CultureInfo.InvariantCulture, out tb1) || String.IsNullOrEmpty(strTB1))
                                                {
                                                    if (0 <= tb1 && tb1 <= 10)
                                                    {
                                                        if (!String.IsNullOrEmpty(strTB1)) model.TBK1 = Decimal.Round(tb1, 1);

                                                    }
                                                    else
                                                    {
                                                        errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                                        hasErr = true;
                                                    }
                                                }
                                                else
                                                {
                                                    errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                                    hasErr = true;
                                                }
                                            }
                                            else
                                            {
                                                decimal tb1, tb2;
                                                string strTB1 = GetCellString(sheet, curRow, curCol).Replace(',', '.');
                                                string strTB2 = GetCellString(sheet, curRow, curCol + 1).Replace(',', '.');

                                                if ((decimal.TryParse(strTB1, NumberStyles.Any, CultureInfo.InvariantCulture, out tb1) || String.IsNullOrEmpty(strTB1))
                                                    && (decimal.TryParse(strTB2, NumberStyles.Any, CultureInfo.InvariantCulture, out tb2) || String.IsNullOrEmpty(strTB2)))
                                                {
                                                    if (0 <= tb1 && tb1 <= 10
                                                        && 0 <= tb2 && tb2 <= 10)
                                                    {
                                                        if (!String.IsNullOrEmpty(strTB1)) model.TBK1 = Decimal.Round(tb1, 1);
                                                        if (!String.IsNullOrEmpty(strTB2)) model.TBK2 = Decimal.Round(tb2, 1);

                                                    }
                                                    else
                                                    {
                                                        errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                                        hasErr = true;
                                                    }
                                                }
                                                else
                                                {
                                                    errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                                    hasErr = true;
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Mon nhan xet
                                        else
                                        {
                                            //Diem mieng
                                            string strM1 = GetCellString(sheet, curRow, curCol);
                                            string strM2 = GetCellString(sheet, curRow, curCol + 1);
                                            string strM3 = GetCellString(sheet, curRow, curCol + 2);
                                            string strM4 = GetCellString(sheet, curRow, curCol + 3);
                                            string strM5 = GetCellString(sheet, curRow, curCol + 4);
                                            if ((String.IsNullOrEmpty(strM1) || strM1.Equals("Đ") || strM1.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strM2) || strM2.Equals("Đ") || strM2.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strM3) || strM3.Equals("Đ") || strM3.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strM4) || strM4.Equals("Đ") || strM4.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strM5) || strM5.Equals("Đ") || strM5.Equals("CĐ")))
                                            {
                                                if (!string.IsNullOrEmpty(strM1)) model.M1 = strM1;
                                                if (!string.IsNullOrEmpty(strM2)) model.M2 = strM2;
                                                if (!string.IsNullOrEmpty(strM3)) model.M3 = strM3;
                                                if (!string.IsNullOrEmpty(strM4)) model.M4 = strM4;
                                                if (!string.IsNullOrEmpty(strM5)) model.M5 = strM5;
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm miệng không hợp lệ;  ";
                                                hasErr = true;
                                            }
                                            curCol = curCol + 5;

                                            //Diem 15P
                                            string strP1 = GetCellString(sheet, curRow, curCol);
                                            string strP2 = GetCellString(sheet, curRow, curCol + 1);
                                            string strP3 = GetCellString(sheet, curRow, curCol + 2);
                                            string strP4 = GetCellString(sheet, curRow, curCol + 3);
                                            string strP5 = GetCellString(sheet, curRow, curCol + 4);
                                            if ((String.IsNullOrEmpty(strP1) || strP1.Equals("Đ") || strP1.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strP2) || strP2.Equals("Đ") || strP2.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strP3) || strP3.Equals("Đ") || strP3.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strP4) || strP4.Equals("Đ") || strP4.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strP5) || strP5.Equals("Đ") || strP5.Equals("CĐ")))
                                            {
                                                if (!string.IsNullOrEmpty(strP1)) model.P1 = strP1;
                                                if (!string.IsNullOrEmpty(strP2)) model.P2 = strP2;
                                                if (!string.IsNullOrEmpty(strP3)) model.P3 = strP3;
                                                if (!string.IsNullOrEmpty(strP4)) model.P4 = strP4;
                                                if (!string.IsNullOrEmpty(strP5)) model.P5 = strP5;
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm 15P không hợp lệ;  ";
                                                hasErr = true;
                                            }
                                            curCol = curCol + 8;

                                            //Diem 1T
                                            string strV1 = GetCellString(sheet, curRow, curCol);
                                            string strV2 = GetCellString(sheet, curRow, curCol + 1);
                                            string strV3 = GetCellString(sheet, curRow, curCol + 2);
                                            string strV4 = GetCellString(sheet, curRow, curCol + 3);
                                            string strV5 = GetCellString(sheet, curRow, curCol + 4);
                                            string strV6 = GetCellString(sheet, curRow, curCol + 5);

                                            if ((String.IsNullOrEmpty(strV1) || strV1.Equals("Đ") || strV1.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strV2) || strV2.Equals("Đ") || strV2.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strV3) || strV3.Equals("Đ") || strV3.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strV4) || strV4.Equals("Đ") || strV4.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strV5) || strV5.Equals("Đ") || strV5.Equals("CĐ"))
                                               && (String.IsNullOrEmpty(strV6) || strV5.Equals("Đ") || strV5.Equals("CĐ")))
                                            {
                                                if (!string.IsNullOrEmpty(strV1)) model.V1 = strV1;
                                                if (!string.IsNullOrEmpty(strV2)) model.V2 = strV2;
                                                if (!string.IsNullOrEmpty(strV3)) model.V3 = strV3;
                                                if (!string.IsNullOrEmpty(strV4)) model.V4 = strV4;
                                                if (!string.IsNullOrEmpty(strV5)) model.V5 = strV5;
                                                if (!string.IsNullOrEmpty(strV6)) model.V6 = strV6;
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm 1T không hợp lệ;  ";
                                                hasErr = true;
                                            }
                                            curCol = curCol + 10;

                                            //Diem HK
                                            string strHK = GetCellString(sheet, curRow, curCol);
                                            if ((String.IsNullOrEmpty(strHK) || strHK.Equals("Đ") || strHK.Equals("CĐ")))
                                            {
                                                if (!string.IsNullOrEmpty(strHK)) model.HK = strHK;
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm HK không hợp lệ;  ";
                                                hasErr = true;
                                            }
                                            curCol++;

                                            //Diem TB
                                            if (Semester == 1)
                                            {
                                                string strTB1 = GetCellString(sheet, curRow, curCol);
                                                if (String.IsNullOrEmpty(strTB1) || strTB1.Equals("Đ") || strTB1.Equals("CĐ"))
                                                {
                                                    if (!string.IsNullOrEmpty(strTB1)) model.TBK1 = strTB1;
                                                }
                                                else
                                                {
                                                    errStr = errStr + "Giá trị điểm TBHK không hợp lệ;  ";
                                                    hasErr = true;
                                                }
                                            }
                                            else
                                            {
                                                string strTB1 = GetCellString(sheet, curRow, curCol);
                                                string strTB2 = GetCellString(sheet, curRow, curCol);
                                                if ((String.IsNullOrEmpty(strTB1) || strTB1.Equals("Đ") || strTB1.Equals("CĐ"))
                                                    && (String.IsNullOrEmpty(strTB2) || strTB2.Equals("Đ") || strTB2.Equals("CĐ")))
                                                {
                                                    if (!string.IsNullOrEmpty(strTB1)) model.TBK1 = strTB1;
                                                    if (!string.IsNullOrEmpty(strTB2)) model.TBK2 = strTB2;
                                                }
                                                else
                                                {
                                                    errStr = errStr + "Giá trị điểm TBHK không hợp lệ;  ";
                                                    hasErr = true;
                                                }
                                            }
                                        }


                                        #endregion

                                        if (hasErr)
                                        {
                                            ErrorViewModel err = lstError.FirstOrDefault(o => o.PupilCode.Equals(pupilCode) && o.PupilFullName.Equals(fullName));
                                            if (err != null)
                                            {
                                                err.Error = err.Error + string.Format("- {0}: {1}\n", qSubject.MappedSubjectName, errStr);
                                            }
                                            else
                                            {
                                                err = new ErrorViewModel();
                                                err.PupilCode = pupilCode;
                                                err.PupilFullName = fullName;
                                                err.Error = string.Format("- {0}: {1}\n", qSubject.MappedSubjectName, errStr);
                                                lstError.Add(err);
                                            }
                                        }

                                        curRow++;
                                        curCol = firstCol;
                                    }

                                }

                            }
                        }
                    }
                }

                if (lstError.Count > 0)
                {

                    return PartialView("_ViewError", lstError);
                }
                else
                {
                    List<ImportFromOtherSystemBO> lstBO = new List<ImportFromOtherSystemBO>();
                    ImportFromOtherSystemBO bo;
                    for (int i = 0; i < lstModel.Count; i++)
                    {
                        ImportMarkModel m = lstModel[i];
                        bo = new ImportFromOtherSystemBO();
                        bo.ClassID = m.ClassID;
                        bo.HK = m.HK;
                        bo.IsComment = m.IsComment;
                        bo.M1 = m.M1;
                        bo.M2 = m.M2;
                        bo.M3 = m.M3;
                        bo.M4 = m.M4;
                        bo.M5 = m.M5;
                        bo.P1 = m.P1;
                        bo.P2 = m.P2;
                        bo.P3 = m.P3;
                        bo.P4 = m.P4;
                        bo.P5 = m.P5;
                        bo.PupilID = m.PupilID;
                        bo.SubjectID = m.SubjectID;
                        bo.IsComment = m.IsComment;
                        bo.TBK1 = m.TBK1;
                        bo.TBK2 = m.TBK2;
                        bo.V1 = m.V1;
                        bo.V2 = m.V2;
                        bo.V3 = m.V3;
                        bo.V4 = m.V4;
                        bo.V5 = m.V5;
                        bo.V6 = m.V6;
                        bo.Semester = Semester.Value;
                        lstBO.Add(bo);
                    }

                    if (lstBO.Count > 0)
                    {
                        AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

                        if (UtilsBusiness.IsMoveHistory(aca))
                        {
                            MarkRecordHistoryBusiness.ImportFromOtherSystem(lstBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                _globalInfo.AppliedLevel.Value, ClassID.Value, 0, Semester.Value, false, _globalInfo.EmployeeID);
                        }
                        else
                        {
                            MarkRecordBusiness.ImportFromOtherSystem(lstBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                _globalInfo.AppliedLevel.Value, ClassID.Value, 0, Semester.Value, false, _globalInfo.EmployeeID);
                        }

                    }

                    ViewData[CommonKey.AuditActionKey.Description] = "Import dữ liệu từ hệ thống khác";
                    ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT;
                    ViewData[CommonKey.AuditActionKey.userFunction] = "Import dữ liệu từ hệ thống khác";
                    ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                    ViewData[CommonKey.AuditActionKey.userDescription] = String.Format("Import dữ liệu điểm từ Hệ thống {0} cho lớp {1}, Môn {2}", "eSchool – Quảng Ích", cp.DisplayName, "[Tất cả]");

                    res = Json(new JsonMessage("Import thành công.", "success"));
                    return res;
                }

                #endregion
            }
            else
            {
                #region import diem
                if (!ClassID.HasValue)
                {
                    res = Json(new JsonMessage("Thầy cô chưa chọn lớp", "error"));
                    return res;
                }

                if (file == null)
                {
                    res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), "error"));
                    return res;
                }


                if (!file.FileName.EndsWith(".xls") && !file.FileName.EndsWith(".xlsx"))
                {
                    res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                    return res;
                }

                //List<int> ListSubjectID = this.GetIDsFromString(ArrSubjectID);

                List<QiSubjectMapping> lstMappedSubject = new List<QiSubjectMapping>();
                string[] arrMappedSubject = ArrSubjectID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (arrMappedSubject.Length == 0)
                {
                    res = Json(new JsonMessage("Thầy cô chưa chọn môn học", "error"));
                    return res;
                }
                for (int i = 0; i < arrMappedSubject.Length; i++)
                {
                    string[] arr = arrMappedSubject[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    string qisubjectName = arr[0];
                    int subjectId = int.Parse(arr[1]);
                    string mappedSubjectName = arr[2];
                    lstMappedSubject.Add(new QiSubjectMapping { QiSubjectName = qisubjectName, MappedSubjectID = subjectId, MappedSubjectName = mappedSubjectName });
                }

                List<int> lstSubjectId = lstMappedSubject.Select(o => o.MappedSubjectID).Distinct().ToList();

                for (int i = 0; i < lstSubjectId.Count; i++)
                {
                    int count = lstMappedSubject.Where(o => o.MappedSubjectID == lstSubjectId[i]).Count();
                    if (count > 1)
                    {
                        QiSubjectMapping firstMapped = lstMappedSubject.Where(o => o.MappedSubjectID == lstSubjectId[i]).FirstOrDefault();
                        res = Json(new JsonMessage(string.Format("{0} được chọn nhiều hơn 1 lần", firstMapped.MappedSubjectName), "error"));
                        return res;
                    }

                }

                int maxSize = 10485760;
                if (file.ContentLength > maxSize)
                {
                    //JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                    res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "10 MB"), "error"));
                    return res;
                }

                //Lay danh sach hoc sinh cua lop
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = ClassID.Value;
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                List<PupilProfileBO> lstPP = PupilOfClassBusiness.Search(dic)
                    .Select(o => new PupilProfileBO
                    {
                        FullName = o.PupilProfile.FullName,
                        BirthDate = o.PupilProfile.BirthDate,
                        OrderInClass = o.OrderInClass,
                        PupilProfileID = o.PupilID,
                        PupilCode = o.PupilProfile.PupilCode
                    }).ToList();
                ;

                //Danh sach mon hoc cua lop
                List<SubjectCatBO> lstCs = GetListSubjectByClass(ClassID.Value, Semester.Value);

                List<ImportMarkModel> lstModel = new List<ImportMarkModel>();
                ImportMarkModel model;

                List<ErrorViewModel> lstError = new List<ErrorViewModel>();
                //Danh sach mon hoc co trong file zip
                List<QiSubjectMapping> lstQiSubject = new List<QiSubjectMapping>();

                //Kiem tra truong chua khai bao so con diem
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["Semester"] = Semester.Value;
                SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
                if (SemeterDeclaration == null)
                {
                    res = Json(new JsonMessage(Res.Get("Validate_School_NotMark"), "error"));
                    return res;
                }

                List<QiSubjectMapping> lstQiSubjectAll = QiSubjectMappingBusiness.All.ToList();

                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);

                foreach (IVTWorksheet sheet in book.GetSheets())
                {
                    sheet.ShiftSheetDown();

                    //Lay ten mon
                    var subjectName = GetCellString(sheet, "A4") ?? "";
                    string[] arr = subjectName.Split('-');
                    if (arr.Length >= 2 && subjectName.Contains("MÔN"))
                    {
                        subjectName = arr[1].Replace("MÔN", string.Empty).Trim();
                    }
                    else
                    {
                        subjectName = string.Empty;
                    }
                    QiSubjectMapping qSubject = lstMappedSubject.FirstOrDefault(o => o.QiSubjectName == subjectName);

                    if (qSubject != null)
                    {
                        int? firstR = sheet.GetFirstUsedRow(1);

                        int firstRow;
                        if (!firstR.HasValue)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }
                        else
                        {
                            firstRow = firstR.Value;
                        }

                        int headerRow = firstRow + 5;
                        int startRow = firstRow + 7;
                        int curRow = startRow;
                        int firstCol = 1;


                        //kiem tra dinh dang cot trong file
                        var A = GetCellString(sheet, "A" + headerRow) ?? "";
                        var C = GetCellString(sheet, "C" + headerRow) ?? "";
                        var E = GetCellString(sheet, "E" + headerRow) ?? "";

                        if (!A.ToLower().Equals("stt")
                                        || !C.ToLower().Equals("họ và tên")
                                        || !E.ToLower().Equals("miệng"))
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        //Tim cot cuoi cung
                        int lastCol = 0;
                        string lastColHeader = Semester == 1 ? "điểm\ntbhk" : "cn";
                        for (int i = 1; i < 200; i++)
                        {
                            var cellVal = GetCellString(sheet, headerRow, i);
                            if (cellVal.ToLower().Equals(lastColHeader))
                            {
                                lastCol = i;
                                break;
                            }
                        }

                        if (lastCol == 0)
                        {
                            res = Json(new JsonMessage("File dữ liệu không hợp lệ", "error"));
                            return res;
                        }

                        int curCol = firstCol;

                        SubjectCatBO sc = lstCs.FirstOrDefault(o => o.SubjectCatID == qSubject.MappedSubjectID);
                        bool isComment = sc.IsCommenting.HasValue && sc.IsCommenting.Value == 1 ? true : false;

                        while (!string.IsNullOrEmpty(GetCellString(sheet, "B" + curRow)))
                        {
                            string errStr = String.Empty;
                            bool hasErr = false;
                            model = new ImportMarkModel();
                            lstModel.Add(model);
                            model.ClassID = ClassID.Value;
                            model.SubjectID = qSubject.MappedSubjectID;
                            model.IsComment = isComment;
                            //Kiem tra ton tai hoc sinh
                            //Ma hoc sinh
                            string pupilCode = GetCellString(sheet, curRow, 2);
                            PupilProfileBO pp;
                            pp = lstPP.FirstOrDefault(o => o.PupilCode.Equals(pupilCode));

                            if (pp == null)
                            {
                                errStr = errStr + "Mã học sinh không hợp lệ; ";
                                hasErr = true;
                            }
                            else
                            {
                                model.PupilID = pp.PupilProfileID;
                            }

                            //STT
                            curCol = curCol + 2;

                            //Ho ten
                            string fullName = GetCellString(sheet, curRow, curCol) + " " + GetCellString(sheet, curRow, curCol + 1);
                            curCol = curCol + 2;

                            model.ListM = new List<object>();
                            model.ListP = new List<object>();
                            model.ListV = new List<object>();

                            #region Mon tinh diem
                            if (!isComment)
                            {
                                while (curCol <= lastCol)
                                {
                                    string cellVal = GetCellString(sheet, curRow, curCol);
                                    string header = GetCellString(sheet, headerRow, curCol);
                                    string subheader = GetCellString(sheet, headerRow + 1, curCol);
                                    int mmark;
                                    decimal pvmark;

                                    bool mError = false;
                                    bool pError = false;
                                    bool vError = false;

                                    var matchM = Regex.Match(subheader, "^M[1-9]$", RegexOptions.None);
                                    var matchP = Regex.Match(subheader, "^P[1-9]$", RegexOptions.None);
                                    var matchV = Regex.Match(subheader, "^V[1-9]$", RegexOptions.None);


                                    if (matchM.Success)
                                    {
                                        if (int.TryParse(cellVal, out mmark))
                                        {
                                            if (0 <= mmark && mmark <= 10)
                                            {
                                                model.ListM.Add(mmark);
                                            }
                                            else
                                            {
                                                if (!mError)
                                                {
                                                    errStr = errStr + "Giá trị điểm miệng không hợp lệ; ";
                                                    hasErr = true;
                                                    mError = true;
                                                }
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.ListM.Add(null);
                                        }
                                        else
                                        {
                                            if (!mError)
                                            {
                                                errStr = errStr + "Giá trị điểm miệng không hợp lệ; ";
                                                hasErr = true;
                                                mError = true;
                                            }
                                        }
                                    }
                                    else if (matchP.Success)
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                model.ListP.Add(Decimal.Round(pvmark, 1));
                                            }
                                            else
                                            {
                                                if (!pError)
                                                {
                                                    errStr = errStr + "Giá trị điểm 15 phút không hợp lệ; ";
                                                    hasErr = true;
                                                    pError = true;
                                                }
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.ListP.Add(null);
                                        }
                                        else
                                        {
                                            if (!pError)
                                            {
                                                errStr = errStr + "Giá trị điểm 15 phút không hợp lệ; ";
                                                hasErr = true;
                                                pError = true;
                                            }
                                        }
                                    }
                                    else if (matchV.Success)
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                model.ListV.Add(Decimal.Round(pvmark, 1));
                                            }
                                            else
                                            {
                                                if (!vError)
                                                {
                                                    errStr = errStr + "Giá trị điểm 1 tiết không hợp lệ; ";
                                                    hasErr = true;
                                                    vError = true;
                                                }
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.ListV.Add(null);
                                        }
                                        else
                                        {
                                            if (!vError)
                                            {
                                                errStr = errStr + "Giá trị điểm 1 tiết không hợp lệ; ";
                                                hasErr = true;
                                                vError = true;
                                            }
                                        }
                                    }
                                    else if (header.Equals("Học\nkỳ"))
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                model.HK = Decimal.Round(pvmark, 1);
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm học kỳ không hợp lệ; ";
                                                hasErr = true;
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.HK = null;
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm học kỳ không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else if (header.Equals("Điểm\nTBHK"))
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                if (Semester == 1)
                                                {
                                                    model.TBK1 = Decimal.Round(pvmark, 1);
                                                }
                                                else
                                                {
                                                    model.TBK2 = Decimal.Round(pvmark, 1);
                                                }
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                                hasErr = true;
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            if (Semester == 1)
                                            {
                                                model.TBK1 = null;
                                            }
                                            else
                                            {
                                                model.TBK2 = null;
                                            }
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else if (header.Equals("CN"))
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (decimal.TryParse(cellVal, NumberStyles.Any, CultureInfo.InvariantCulture, out pvmark))
                                        {
                                            if (0 <= pvmark && pvmark <= 10)
                                            {
                                                model.TBCN = Decimal.Round(pvmark, 1);
                                            }
                                            else
                                            {
                                                errStr = errStr + "Giá trị điểm CN không hợp lệ; ";
                                                hasErr = true;
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(cellVal))
                                        {
                                            model.TBCN = null;
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm CN không hợp lệ; ";
                                            hasErr = true;
                                            vError = true;
                                        }
                                    }

                                    curCol++;
                                }

                            }
                            #endregion

                            #region Mon nhan xet
                            else
                            {
                                while (curCol <= lastCol)
                                {
                                    string cellVal = GetCellString(sheet, curRow, curCol);
                                    string header = GetCellString(sheet, headerRow, curCol);
                                    string subheader = GetCellString(sheet, headerRow + 1, curCol);

                                    bool mError = false;
                                    bool pError = false;
                                    bool vError = false;

                                    var matchM = Regex.Match(subheader, "^M[1-9]$", RegexOptions.None);
                                    var matchP = Regex.Match(subheader, "^P[1-9]$", RegexOptions.None);
                                    var matchV = Regex.Match(subheader, "^V[1-9]$", RegexOptions.None);


                                    if (matchM.Success)
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            model.ListM.Add(cellVal);
                                        }
                                        else
                                        {
                                            if (!mError)
                                            {
                                                errStr = errStr + "Giá trị điểm miệng không hợp lệ;  ";
                                                hasErr = true;
                                                mError = true;
                                            }
                                        }
                                    }
                                    else if (matchP.Success)
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            model.ListP.Add(cellVal);
                                        }
                                        else
                                        {
                                            if (!pError)
                                            {
                                                errStr = errStr + "Giá trị điểm 15 phút không hợp lệ; ";
                                                hasErr = true;
                                                pError = true;
                                            }
                                        }
                                    }
                                    else if (matchV.Success)
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            model.ListV.Add(cellVal);
                                        }
                                        else
                                        {
                                            if (!vError)
                                            {
                                                errStr = errStr + "Giá trị điểm 1 tiết không hợp lệ; ";
                                                hasErr = true;
                                                vError = true;
                                            }
                                        }
                                    }
                                    else if (header.Equals("Học\nkỳ"))
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal)) model.HK = cellVal;
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm học kỳ không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else if (header.Equals("Điểm\nTBHK"))
                                    {
                                        cellVal = cellVal.Replace(',', '.');

                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal))
                                            {
                                                if (Semester == 1)
                                                {
                                                    model.TBK1 = cellVal;
                                                }
                                                else
                                                {
                                                    model.TBK2 = cellVal;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm TBHK không hợp lệ; ";
                                            hasErr = true;
                                        }
                                    }
                                    else if (header.Equals("CN"))
                                    {
                                        if (String.IsNullOrEmpty(cellVal) || cellVal.Equals("Đ") || cellVal.Equals("CĐ"))
                                        {
                                            if (!string.IsNullOrEmpty(cellVal)) model.TBCN = cellVal;
                                        }
                                        else
                                        {
                                            errStr = errStr + "Giá trị điểm CN không hợp lệ;  ";
                                            hasErr = true;
                                        }
                                    }

                                    curCol++;
                                }
                            }

                            #endregion

                            if (hasErr)
                            {
                                ErrorViewModel err = lstError.FirstOrDefault(o => o.PupilCode.Equals(pupilCode) && o.PupilFullName.Equals(fullName));
                                if (err != null)
                                {
                                    err.Error = err.Error + string.Format("- {0}: {1}\n", qSubject.MappedSubjectName, errStr);
                                }
                                else
                                {
                                    err = new ErrorViewModel();
                                    err.PupilCode = pupilCode;
                                    err.PupilFullName = fullName;
                                    err.Error = string.Format("- {0}: {1}\n", qSubject.MappedSubjectName, errStr);
                                    lstError.Add(err);
                                }
                            }

                            curRow++;
                            curCol = firstCol;
                        }
                    }
                }


                if (lstError.Count > 0)
                {

                    return PartialView("_ViewError", lstError);
                }
                else
                {
                    List<ImportFromOtherSystemBO> lstBO = new List<ImportFromOtherSystemBO>();
                    ImportFromOtherSystemBO bo;
                    for (int i = 0; i < lstModel.Count; i++)
                    {
                        ImportMarkModel m = lstModel[i];
                        bo = new ImportFromOtherSystemBO();
                        bo.ClassID = m.ClassID;
                        bo.HK = m.HK;
                        bo.IsComment = m.IsComment;
                        bo.M1 = m.M1;
                        bo.M2 = m.M2;
                        bo.M3 = m.M3;
                        bo.M4 = m.M4;
                        bo.M5 = m.M5;
                        bo.P1 = m.P1;
                        bo.P2 = m.P2;
                        bo.P3 = m.P3;
                        bo.P4 = m.P4;
                        bo.P5 = m.P5;
                        bo.PupilID = m.PupilID;
                        bo.SubjectID = m.SubjectID;
                        bo.TBK1 = m.TBK1;
                        bo.TBK2 = m.TBK2;
                        bo.V1 = m.V1;
                        bo.V2 = m.V2;
                        bo.V3 = m.V3;
                        bo.V4 = m.V4;
                        bo.V5 = m.V5;
                        bo.V6 = m.V6;
                        bo.Semester = Semester.Value;
                        bo.lstM = m.ListM;
                        bo.lstP = m.ListP;
                        bo.lstV = m.ListV;
                        bo.TBCN = m.TBCN;
                        lstBO.Add(bo);
                    }

                    if (lstBO.Count > 0)
                    {
                        AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

                        if (UtilsBusiness.IsMoveHistory(aca))
                        {
                            MarkRecordHistoryBusiness.ImportFromOtherSystem(lstBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                _globalInfo.AppliedLevel.Value, ClassID.Value, 0, Semester.Value, false, _globalInfo.EmployeeID);
                        }
                        else
                        {
                            MarkRecordBusiness.ImportFromOtherSystem(lstBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                _globalInfo.AppliedLevel.Value, ClassID.Value, 0, Semester.Value, false, _globalInfo.EmployeeID);
                        }

                    }

                    ViewData[CommonKey.AuditActionKey.Description] = "Import dữ liệu từ hệ thống khác";
                    ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT;
                    ViewData[CommonKey.AuditActionKey.userFunction] = "Import dữ liệu từ hệ thống khác";
                    ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                    ViewData[CommonKey.AuditActionKey.userDescription] = String.Format("Import dữ liệu điểm từ Hệ thống {0} cho lớp {1}, Môn {2}", "eSchool – Quảng Ích", cp.DisplayName, "[Tất cả]");

                    res = Json(new JsonMessage("Import thành công.", "success"));
                    return res;
                }

                #endregion
            }

        }

        private string ToStringWithFixedLength(int number, int length)
        {
            if (number < 0) return string.Empty;

            if (number.ToString().Length > length) return string.Empty;

            return number.ToString().PadLeft(length, '0');
        }

        private string GetPupilName(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
                return string.Empty;

            fullName = fullName.Trim();

            int index = fullName.LastIndexOf(' ');

            return index > 0 ? fullName.Substring(index + 1) : fullName;
        }

        public FileResult DownloadErrorFile(int type)
        {
            if (type == ImportFromOtherSystemConstants.DATA_TYPE_PUPIL_PROFILE)
            {
                IVTWorkbook book = (IVTWorkbook)Session[ImportFromOtherSystemConstants.SESSION_KEY_PUPIL_PROFILE];
                if (book != null)
                {
                    Stream s = book.ToStream();
                    FileStreamResult result = new FileStreamResult(s, "application/octet-stream");
                    result.FileDownloadName = SMAS.Business.Common.Utils.StripVNSign((string)Session[ImportFromOtherSystemConstants.SESSION_FILE_NAME_PUPIL]);

                    return result;
                }
            }
            else if (type == ImportFromOtherSystemConstants.DATA_TYPE_MARK)
            {
                IVTWorkbook book = (IVTWorkbook)Session[ImportFromOtherSystemConstants.SESSION_KEY_MARK];
                if (book != null)
                {
                    Stream s = book.ToStream();
                    FileStreamResult result = new FileStreamResult(s, "application/octet-stream");
                    result.FileDownloadName = SMAS.Business.Common.Utils.StripVNSign((string)Session[ImportFromOtherSystemConstants.SESSION_FILE_NAME_MARK]);

                    return result;
                }
            }
            return null;
        }

        public JsonResult AjaxLoadClass(int educationLevel)
        {
            List<ClassProfile> lstClass = getClassFromEducationLevel(educationLevel);

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult AjaxLoadSubject(int? classId, int semester)
        {
            List<SubjectCatBO> lstSc;
            if (!classId.HasValue)
            {
                lstSc = new List<SubjectCatBO>();
            }
            else
            {
                lstSc = GetListSubjectByClass(classId.Value, semester);
            }

            return Json(new SelectList(lstSc, "SubjectCatID", "DisplayName"));
        }

        private string GetCellString(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }
        private string GetCellString(IVTWorksheet sheet, int row, int col)
        {
            object o = sheet.GetRange(row, col, row, col).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }
        /// <summary>
        /// Lấy danh sách các lớp học theo khối
        /// Nếu cấp học bằng ) thì lấy tất cả
        /// Nếu người dùng không phải là admin trường thì lấy các lớp người này dạy
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        private List<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };

            //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
            if (!global.IsAdminSchoolRole && !global.IsViewAll && !UtilsBusiness.IsBGH(global.UserAccountID))
            {
                dic["UserAccountID"] = global.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING;
            }

            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).OrderBy(o => o.OrderNumber).ThenBy(u => u.DisplayName);
            return lsCP.ToList();
        }

        private List<SubjectCatBO> GetListSubjectByClass(int classId, int semester)
        {
            //cho phep tai khoan xem tat ca sanh sach mon hoc
            bool ViewAll = false;

            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            //Chiendd1: 06/07/2015
            //Bo sung dieu kien partition cho bang TeachingAssignmentBusiness
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();

            lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value,
                                                                semester, classId, ViewAll)
                                                                .Where(u => (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                    ? u.SectionPerWeekFirstSemester > 0
                                                                    : u.SectionPerWeekSecondSemester > 0)
                                                                    && (!u.IsSubjectVNEN.HasValue || (u.IsSubjectVNEN.HasValue && u.IsSubjectVNEN.Value == false)))
                              join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                              where s.IsActive == true
                              && s.IsApprenticeshipSubject == false
                              select new SubjectCatBO()
                              {
                                  SubjectCatID = s.SubjectCatID,
                                  DisplayName = s.DisplayName,
                                  IsCommenting = cs.IsCommenting,
                                  OrderInSubject = s.OrderInSubject,
                                  SubjectIdInCrease = cs.SubjectIDIncrease,
                                  ClassID = cs.ClassID
                              }
                                ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();


            List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();
            int UserAccountID = _globalInfo.UserAccountID;

            // Neu la admin truong thi hien thi het
            if (new GlobalInfo().IsAdminSchoolRole)
            {

            }
            else
            {
                int? TeacherID = UserAccountBusiness.Find(UserAccountID).EmployeeID;
                List<int> listPermitSubjectID = null; ;
                if (TeacherID.HasValue)
                {
                    // Phan cong giao vu la giao vien bo mon
                    if (ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == classId &&
                                                            o.AcademicYearID == _globalInfo.AcademicYearID &&
                                                            o.SchoolID == _globalInfo.SchoolID &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER).Count() > 0
                                                            )
                    {
                        listPermitSubjectID = lsClassSubject.Select(o => o.SubjectCatID).ToList();
                    }
                    else
                    {
                        // Khong phai lai giao vien phan cong giao vu thi kiem tra la giao vien bo mon
                        IDictionary<string, object> searchInfo = new Dictionary<string, object>();
                        searchInfo["ClassID"] = classId;
                        searchInfo["TeacherID"] = TeacherID;
                        searchInfo["Semester"] = semester;
                        searchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                        searchInfo["IsActive"] = true;
                        listPermitSubjectID = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfo).Select(o => o.SubjectID).ToList();


                    }
                }
                else
                {
                    listPermitSubjectID = new List<int>();
                }
                lsClassSubject = lsClassSubject.Where(o => listPermitSubjectID.Contains(o.SubjectCatID)).ToList();
            }

            return lsClassSubject;
        }

        private string RenderPartialToString(string controlName, object viewData)
        {
            ViewPage viewPage = new ViewPage() { ViewContext = new ViewContext() };

            viewPage.ViewData = new ViewDataDictionary(viewData);
            viewPage.Controls.Add(viewPage.LoadControl(controlName));

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter tw = new HtmlTextWriter(sw))
                {
                    viewPage.RenderControl(tw);
                }
            }

            return sb.ToString();
        }

        private List<int> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                IDArr = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                IDArr = new string[] { };
            }
            List<int> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return listID;
        }
    }
}
