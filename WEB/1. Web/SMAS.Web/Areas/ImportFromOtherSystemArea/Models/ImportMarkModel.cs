﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Common;
using System.Globalization;
using SMAS.Web.Areas.ImportPupilArea.Models;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;
using SMAS.Business.Business;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.ImportFromOtherSystemArea.Models
{
    public class ImportMarkModel
    {
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public bool IsComment { get; set; }
        public int PupilID { get; set; }
        public object M1 { get; set; }
        public object M2 { get; set; }
        public object M3 { get; set; }
        public object M4 { get; set; }
        public object M5 { get; set; }
        public object P1 { get; set; }
        public object P2 { get; set; }
        public object P3 { get; set; }
        public object P4 { get; set; }
        public object P5 { get; set; }
        public object V1 { get; set; }
        public object V2 { get; set; }
        public object V3 { get; set; }
        public object V4 { get; set; }
        public object V5 { get; set; }
        public object V6 { get; set; }
        public object HK { get; set; }
        public object TBK1 { get; set; }
        public object TBK2 { get; set; }
        public object TBCN { get; set; }
        public List<object> ListM { get; set; }
        public List<object> ListP { get; set; }
        public List<object> ListV { get; set; }
    }
}
