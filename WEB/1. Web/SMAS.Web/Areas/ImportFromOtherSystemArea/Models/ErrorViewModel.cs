﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ImportFromOtherSystemArea.Models
{
    public class ErrorViewModel
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string PupilFullName { get; set; }
        public string Error { get; set; }
    }
}