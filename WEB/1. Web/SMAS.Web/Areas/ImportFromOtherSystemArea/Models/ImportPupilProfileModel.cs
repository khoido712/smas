﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Common;
using System.Globalization;
using SMAS.Web.Areas.ImportPupilArea.Models;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;
using SMAS.Business.Business;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.ImportFromOtherSystemArea.Models
{
    public class ImportPupilProfileModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public string IOrderNumber { get; set; }
        public int OrderNumber { get; set; }
        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public string SDB { get; set; }
        public string IdentifyNumber { get; set; }
        public string IBirthDay { get; set; }
        public DateTime BirthDay { get; set; }
        public string Genre { get; set; }
        public string IVillage { get; set; }
        public int? VillageID { get; set; }
        public string IProvince { get; set; }
        public int ProvinceID {get;set;}
        public string IDistrict {get;set;}
        public int? DistrictID { get; set; }
        public string ICommune { get; set; }
        public int? CommuneID { get; set; }
        public string BirtPlace { get; set; }
        public string HomeTown { get; set; }
        public string LiveAddress { get; set; }
        public string IEthnic { get; set; }
        public int? EthnicID { get; set; }
        public string IReligion { get; set; }
        public int? ReligionID { get; set; }
        public string PhoneNumber { get; set; }
        public string FatherName { get; set; }
        public string FatherJob { get; set; }
        public string MotherName { get; set; }
        public string MotherJob { get; set; }
        public string HomeAddress { get; set; }
        public bool? IsYouthLeageMember { get; set; }
        public bool? IsYoungPioneerMember { get; set; }
        public int? FatherBirthYear { get; set; }
        public string IFatherBirthYear { get; set; }
        public int? MotherBirthYear { get; set; }
        public string IMotherBirthYear { get; set; }
        public string Email { get; set; }
        public string FatherMobile { get; set; }
        public string MotherMobile { get; set; }
        public string IPolicyTarget { get; set; }
        public int? PolicyTargetID { get; set; }
    }
}
