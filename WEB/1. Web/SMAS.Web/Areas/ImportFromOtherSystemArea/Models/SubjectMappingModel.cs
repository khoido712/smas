﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ImportFromOtherSystemArea.Models
{
    public class SubjectMappingModel
    {
        public string OriginalSubjectName { get; set; }
        public int MappedSubjectId { get; set; }
        public bool IsChecked { get; set; }
    }
}