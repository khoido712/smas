/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Threading;
using System.Globalization;
namespace SMAS.Web.Areas.PriorityTypeArea.Models
{
    public class PriorityTypeViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 PriorityTypeID { get; set; }

        [ResourceDisplayName("PriorityType_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Resolution { get; set; }

        [ResourceDisplayName("PriorityType_Label_AppliedLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PriorityTypeConstants.APPLIEDLEVEL)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Nullable<System.Int32> AppliedLevel { get; set; }

        [ResourceDisplayName("PriorityType_Label_Type")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PriorityTypeConstants.TYPE)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 Type { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PriorityType_Label_Type")]
        public string TypeName { get; set; }

        [ResourceDisplayName("PriorityType_Label_Mark")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Range(1, 9, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public System.Nullable<System.Decimal> Mark { get; set; }

        [ResourceDisplayName("PriorityType_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }
    }

}


