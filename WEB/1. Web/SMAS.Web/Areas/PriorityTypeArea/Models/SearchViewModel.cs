/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PriorityTypeArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PriorityType_Label_AppliedLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PriorityTypeConstants.APPLIEDLEVEL)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> AppliedLevel { get; set; }

        [ResourceDisplayName("PriorityType_Label_Type")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PriorityTypeConstants.TYPE)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> Type { get; set; }

        [ResourceDisplayName("PriorityType_Label_Resolution")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Resolution { get; set; }
    }
}