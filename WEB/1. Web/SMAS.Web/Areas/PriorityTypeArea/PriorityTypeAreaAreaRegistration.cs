﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PriorityTypeArea
{
    public class PriorityTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PriorityTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PriorityTypeArea_default",
                "PriorityTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
