﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.PriorityTypeArea.Models;

using SMAS.Models.Models;
using System.Globalization;
using System.Threading;

namespace SMAS.Web.Areas.PriorityTypeArea.Controllers
{
    public class PriorityTypeController : BaseController
    {
        private readonly IPriorityTypeBusiness PriorityTypeBusiness;

        public PriorityTypeController(IPriorityTypeBusiness prioritytypeBusiness)
        {
            this.PriorityTypeBusiness = prioritytypeBusiness;
        }

        // GET: /PriorityType/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            var listAppliedLevels = GetAppliedLevels();
            var listTypes = Types();
            ViewData[PriorityTypeConstants.APPLIEDLEVEL] = new SelectList(listAppliedLevels, "Key", "Value");
            ViewData[PriorityTypeConstants.TYPE] = new SelectList(listTypes, "Key", "Value");
            IEnumerable<PriorityTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[PriorityTypeConstants.LIST_PRIORITYTYPE] = lst;
            return View();
        }

        //
        // GET: /PriorityType/Search
        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["AppliedLevel"] = frm.AppliedLevel;
            SearchInfo["Type"] = frm.Type;
            SearchInfo["Resolution"] = frm.Resolution;

            IEnumerable<PriorityTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[PriorityTypeConstants.LIST_PRIORITYTYPE] = lst;

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            PriorityType prioritytype = new PriorityType();
            TryUpdateModel(prioritytype);
            Utils.Utils.TrimObject(prioritytype);

            this.PriorityTypeBusiness.Insert(prioritytype);
            this.PriorityTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int PriorityTypeID, FormCollection col)
        {
            PriorityType prioritytype = this.PriorityTypeBusiness.Find(PriorityTypeID);
            TryUpdateModel(prioritytype);
            Utils.Utils.TrimObject(prioritytype);
            prioritytype.Mark = Convert.ToDecimal(col["Mark"], CultureInfo.CreateSpecificCulture("en-US"));
            this.PriorityTypeBusiness.Update(prioritytype);
            this.PriorityTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.PriorityTypeBusiness.Delete(id);
            this.PriorityTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PriorityTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<PriorityType> query = this.PriorityTypeBusiness.Search(SearchInfo);
            IQueryable<PriorityTypeViewModel> lst = query.Select(o => new PriorityTypeViewModel
            {
                PriorityTypeID = o.PriorityTypeID,
                Resolution = o.Resolution,
                AppliedLevel = o.AppliedLevel,
                Mark = o.Mark,
                Type = o.Type,
                Description = o.Description
            });
            IEnumerable<PriorityTypeViewModel> listRes = lst.ToList();
            foreach (var i in listRes)
            {
                if (i.Type == 1)
                    i.TypeName = Res.Get("PriorityType_Lable_Type1");
                else if (i.Type == 2)
                    i.TypeName = Res.Get("PriorityType_Lable_Type2");
            }
            return listRes.OrderBy(o=>o.Resolution);
        }

        private int[] levels = { 1, 2, 3 };
        private Dictionary<int, string> GetAppliedLevels()
        {
            Dictionary<int, string> Levels = new Dictionary<int, string>();
            foreach (int i in levels)
            {
                Levels.Add(i, i.ToString());
            }
            return Levels;
        }

        private Dictionary<int, string> Types()
        {
            Dictionary<int, string> types = new Dictionary<int, string>();
            types.Add(1, Res.Get("PriorityType_Lable_Type1"));// "Diện ưu tiên
            types.Add(2, Res.Get("PriorityType_Lable_Type2"));// "Diện khuyến khích
            return types;
        }
    }
}





