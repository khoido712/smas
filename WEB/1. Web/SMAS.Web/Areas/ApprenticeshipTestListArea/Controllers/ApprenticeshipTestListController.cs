﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ApprenticeshipTestListArea.Models;
using Telerik.Web.Mvc;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ApprenticeshipTestListArea.Controllers
{
    public class ApprenticeshipTestListController : BaseController
    {
        private readonly IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness;
        private readonly IApprenticeshipClassBusiness ApprenticeshipClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;

        public ApprenticeshipTestListController(IApprenticeshipTrainingBusiness apprenticeshiptrainingBusiness, ISummedUpRecordBusiness SummedUpRecordBusiness,
            IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness, IApprenticeshipClassBusiness ApprenticeshipClassBusiness, ISchoolProfileBusiness SchoolProfileBusiness,
            IPupilProfileBusiness PupilProfileBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ApprenticeshipTrainingBusiness = apprenticeshiptrainingBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.ApprenticeshipSubjectBusiness = ApprenticeshipSubjectBusiness;
            this.ApprenticeshipClassBusiness = ApprenticeshipClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
        }

        public decimal Mark_G = 8.0m;
        public decimal Mark_K = 6.5m;
        public decimal Mark_TB = 5.0m;

        #region Page Action
        public ActionResult Index()
        {
            SearchViewModel abc = new SearchViewModel();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();

            // Lay thong tin nam hoc
            IQueryable<AcademicYear> IQAcademicYear = AcademicYearBusiness.SearchBySchool(globalInfo.SchoolID.Value).OrderByDescending(o => o.Year);
            SelectList ListAcademicYear = new SelectList((IEnumerable<AcademicYear>)IQAcademicYear.ToList(), "AcademicYearID", "DisplayTitle", globalInfo.AcademicYearID);
            ViewData[ApprenticeshipTestListConstants.LIST_ACADEMICYEAR] = ListAcademicYear;

            // Lay du lieu lop nghe pho thong
            IQueryable<ApprenticeshipClass> IQApprenticeshipClass = ApprenticeshipClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo);
            SelectList ListApprenticeshipClass = new SelectList((IEnumerable<ApprenticeshipClass>)IQApprenticeshipClass.Where(o => o.ApprenticeshipSubject.SubjectCat.AppliedLevel == globalInfo.AppliedLevel).ToList(), "ApprenticeshipClassID", "ClassName");
            ViewData[ApprenticeshipTestListConstants.LIST_CLASS] = ListApprenticeshipClass;
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            List<ApprenticeshipTestListViewModel> paging = this._Search(frm);

            ViewData[ApprenticeshipTestListConstants.LIST_APPRENTICESHIPTRAINING] = paging;

            foreach (ApprenticeshipTestListViewModel item in paging)
            {
                // Xep loai
                if (item.SummedUpMark >= Mark_G) // loai gioi
                {
                    item.XL = "G";
                }
                if (item.SummedUpMark >= Mark_K && item.SummedUpMark < Mark_G) // loai kha
                {
                    item.XL = "K";
                }
                if (item.SummedUpMark >= Mark_TB && item.SummedUpMark < Mark_K) // loai Trung binh
                {
                    item.XL = "TB";
                }
                if (item.SummedUpMark < (decimal)Mark_TB) // loai Yeu
                {
                    item.XL = "Y";
                }
            }
            return PartialView("_List");
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? AcademicYearID)
        {
            if (AcademicYearID.HasValue)
            {
                IQueryable<ApprenticeshipClass> lsCP = getClassFromAcademicYear(AcademicYearID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP.ToList(), "ApprenticeshipClassID", "ClassName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion

        #region Extent Method

        private List<ApprenticeshipTestListViewModel> _Search(SearchViewModel frm)
        {
            if (frm == null)
            {
                frm = new SearchViewModel();
            }

            Utils.Utils.TrimObject(frm);
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            if (frm.ClassID.HasValue)
            {
                SearchInfo["ApprenticeshipClassID"] = frm.ClassID.Value;
                ViewData[ApprenticeshipTestListConstants.IS_PAGEABLE] = false;
            }
            else
            {
                ViewData[ApprenticeshipTestListConstants.IS_PAGEABLE] = true;
            }

            if (frm.AcademicYearID.HasValue)
            {
                SearchInfo["AcademicYearID"] = frm.AcademicYearID.Value;
            }

            SearchInfo["PupilCode"] = frm.PupilCode == null ? "" : frm.PupilCode.Trim().ToLower();
            SearchInfo["FullName"] = frm.FullName == null ? "" : frm.FullName.Trim().ToLower();
            // Lay danh sach hoc sinh hoc nghe
            IQueryable<ApprenticeshipTraining> lstFirstApprenticeshipTraining = ApprenticeshipTrainingBusiness
                .SearchBySchool(globalInfo.SchoolID.Value, SearchInfo);

            // Lay danh sach tong ket diem
            if (globalInfo.SchoolID.HasValue)
            {
                SearchInfo["SchoolID"] = globalInfo.SchoolID.Value;
            }
            // Lay thong tin mon hoc
            if (frm.ClassID.HasValue)
            {
                ApprenticeshipClass ApprenticeshipClass = ApprenticeshipClassBusiness.Find(frm.ClassID.Value);
                if (ApprenticeshipClass != null && ApprenticeshipClass.ApprenticeshipSubject != null)
                {
                    int SubjectID = ApprenticeshipClassBusiness.Find(frm.ClassID).ApprenticeshipSubject.SubjectID;
                    SearchInfo["SubjectID "] = SubjectID;
                }
            }

            decimal NomalMark = SMAS.Business.Common.GlobalConstants.NORMAL_MARK;
            // Bo va them cac dieu kien ko tim kiem tuong ung
            SearchInfo["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            SearchInfo["SummedUpFromMark"] = NomalMark;
            SearchInfo.Remove("PupilCode");
            SearchInfo.Remove("FullName");
            //Gia tri nam nu
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");

            // Lay dung theo diem ca nam
            IQueryable<SummedUpRecord> ListSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo)
                                                                                    .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
            // Lay danh sach hoc sinh
            IQueryable<PupilProfileBO> LstPupilProfile = PupilProfileBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo);
            //Lay day du thong tin fill len Gridview
            IEnumerable<ApprenticeshipTestListViewModel> LstApprenticeshipTraining = from at in lstFirstApprenticeshipTraining.AsEnumerable()
                                                                                     join su in ListSummedUpRecord.AsEnumerable() on new { at.PupilID, at.ApprenticeshipSubject.SubjectID } equals new { su.PupilID, su.SubjectID }
                                                                                     join pf in LstPupilProfile.AsEnumerable() on at.PupilID equals pf.PupilProfileID
                                                                                     where su.SummedUpMark.HasValue && su.SummedUpMark >= NomalMark
                                                                                     select new ApprenticeshipTestListViewModel
                                                                                     {
                                                                                         PupilID = at.PupilID,
                                                                                         PupilCode = at.PupilCode,
                                                                                         FullName = pf.FullName,
                                                                                         BirthDate = pf.BirthDate,
                                                                                         GenreName = (pf.Genre == (int)SystemParamsInFile.GENRE_MALE) ? male : female,
                                                                                         BirthPlace = pf.BirthPlace,
                                                                                         ClassID = at.ClassID,
                                                                                         ClassName = at.ClassProfile.DisplayName,
                                                                                         NPTSubject = at.ApprenticeshipSubject.SubjectName,
                                                                                         AcademicYearID = at.AcademicYearID,
                                                                                         ApprenticeshipSubjectID = at.ApprenticeshipSubjectID,
                                                                                         ApprenticeshipTrainingID = at.ApprenticeshipTrainingID,
                                                                                         ApprenticeshipClassID = at.ApprenticeShipClassID,
                                                                                         ApprenticeshipClassName = at.ApprenticeshipClass.ClassName,
                                                                                         SummedUpMark = su.SummedUpMark,
                                                                                         Theory_Mark = at.TheoryMark,
                                                                                         Action_Mark = at.ActionMark
                                                                                     };
            List<ApprenticeshipTestListViewModel> listModel = LstApprenticeshipTraining.ToList();
            return listModel;
        }

        public FileResult ExportExcel(int? academicYearID, int? apprenticeshipClassID, string pupilCode, string fullName)
        {
            SearchViewModel frm = new SearchViewModel();
            frm.AcademicYearID = academicYearID;
            frm.ClassID = apprenticeshipClassID;
            frm.FullName = fullName;
            frm.PupilCode = pupilCode;
            List<ApprenticeshipTestListViewModel> listApprenticeshipTest = this._Search(frm);
            #region lay cac sheet ra
            GlobalInfo global = new GlobalInfo();
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", ApprenticeshipTestListConstants.TEMPLATE_EXCEL);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet templateSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet, "M7");
            #endregion

            #region fill
            IVTRange firstRange = templateSheet.GetRange("A8", "M8");
            IVTRange middleRange = templateSheet.GetRange("A9", "M9");
            IVTRange lastRange = templateSheet.GetRange("A10", "M10");
            IVTRange infoRange = templateSheet.GetRange("A12", "M13");
            int startDataRow = 8;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            DateTime now = DateTime.Now;
            dic["MonthYear"] = now.Month + "/" + now.Year;
            SchoolProfile school = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value);
            string shoolName = school.SchoolName;
            dic["SchoolName"] = shoolName;
            string province = school.Province != null ? school.Province.ProvinceName + ", " : string.Empty;
            dic["ProvinceNameAndDate"] = province + "Ngày " + now.Day + " Tháng " + now.Month + " Năm " + now.Year;
            List<object> listData = new List<object>();
            for (int i = 0; i < listApprenticeshipTest.Count(); i++)
            {
                Dictionary<string, object> dicDetail = new Dictionary<string, object>();
                ApprenticeshipTestListViewModel item = listApprenticeshipTest[i];
                dicDetail["STT"] = i + 1;
                dicDetail["PupilCode"] = item.PupilCode;
                dicDetail["FullName"] = item.FullName;
                dicDetail["BirthDate"] = item.BirthDate;
                dicDetail["BirthPlace"] = item.BirthPlace;
                dicDetail["SchoolName"] = shoolName;
                dicDetail["Subject"] = item.NPTSubject;
                dicDetail["LTMark"] = item.Theory_Mark;
                dicDetail["THMark"] = item.Action_Mark;
                dicDetail["Result"] = item.SummedUpMark;
                //Hungnd 01/07/2013 generate Capacity by Result
                if (item.SummedUpMark >= Mark_G) // loai gioi
                {
                    dicDetail["Capacity"] = "G";
                }
                if (item.SummedUpMark >= Mark_K && item.SummedUpMark < Mark_G) // loai kha
                {
                    dicDetail["Capacity"] = "K";
                }
                if (item.SummedUpMark >= Mark_TB && item.SummedUpMark < Mark_K) // loai Trung binh
                {
                    dicDetail["Capacity"] = "TB";
                }
                if (item.SummedUpMark < (decimal)Mark_TB) // loai Yeu
                {
                    dicDetail["Capacity"] = "Y";
                }
                dicDetail["Note"] = "";
                listData.Add(dicDetail);
                if ((i + 1) % 5 == 0 || i == listApprenticeshipTest.Count - 1)
                {
                    sheet.CopyPaste(lastRange, startDataRow + i, 1);
                }
                else if ((i + 1) % 5 == 1)
                {
                    sheet.CopyPaste(firstRange, startDataRow + i, 1);
                }
                else
                {
                    sheet.CopyPaste(middleRange, startDataRow + i, 1);
                }
            }
            dic.Add("Rows", listData);
            // copy thong tin o cuoi
            sheet.CopyPaste(infoRange, startDataRow + listApprenticeshipTest.Count + 1, 1);

            sheet.FillVariableValue(dic);
            // copy lai style

            templateSheet.Delete();
            #endregion

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = "HS_THPT_DSThiSinhDuThiNPT_" + school.SchoolName;
            if (apprenticeshipClassID.HasValue)
            {
                fileName += "_" + ApprenticeshipClassBusiness.Find(apprenticeshipClassID.Value).ClassName;
            }
            fileName = ReportUtils.StripVNSign(fileName);
            result.FileDownloadName = fileName + ".xls";
            return result;
        }

        private IQueryable<ApprenticeshipClass> getClassFromAcademicYear(int AcademicYearID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID}
                };
            IQueryable<ApprenticeshipClass> lsCP = ApprenticeshipClassBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return lsCP;
        }

        #endregion
    }
}
