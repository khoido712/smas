﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ApprenticeshipTestListArea
{
    public class ApprenticeshipTestListAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ApprenticeshipTestListArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ApprenticeshipTestListArea_default",
                "ApprenticeshipTestListArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
