﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.ApprenticeshipTestListArea.Models
{
    public class ApprenticeshipTestListViewModel
    {
        public System.Int32 ApprenticeshipTrainingID { get; set; }
        public System.Nullable<System.Int32> ClassID { get; set; }
        public System.Nullable<System.Int32> ApprenticeshipClassID { get; set; }
        public System.Nullable<System.Int32> SchoolID { get; set; }
        public System.Nullable<System.Int32> AcademicYearID { get; set; }
        public System.Int32 PupilID { get; set; }
        public System.Nullable<System.Int32> EducationLevelID { get; set; }
        public System.Int32 ApprenticeshipSubjectID { get; set; }
        [ResourceDisplayName("ApprenticeshipTest_Label_SummedUpMark")]
        public System.Nullable<System.Decimal> SummedUpMark { get; set; }
        [ResourceDisplayName("ApprenticeshipTest_Label_Name")]
        public System.String FullName { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_PupilCode")]
        public System.String PupilCode { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_ApprenticeshipClass")]
        public System.String ClassName { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_Class")]
        public System.String ApprenticeshipClassName { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_NPTSubject")]
        public System.String NPTSubject { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_GenreName")]
        public System.String GenreName { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_BirthPlace")]
        public string BirthPlace { get; set; }

        public System.Decimal? Theory_Mark { get; set; }

        public System.Decimal? Action_Mark { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_XL")]
        public System.String XL { get; set; }
    }
}