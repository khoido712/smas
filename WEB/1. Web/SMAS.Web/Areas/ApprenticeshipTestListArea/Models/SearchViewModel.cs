﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ApprenticeshipTestListArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ApprenticeshipTest_Label_Year")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipTestListConstants.LIST_ACADEMICYEAR)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? AcademicYearID { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipTestListConstants.LIST_CLASS)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_Name")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }
    }
}