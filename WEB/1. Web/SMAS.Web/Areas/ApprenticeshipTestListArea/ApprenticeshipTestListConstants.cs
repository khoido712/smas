﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ApprenticeshipTestListArea
{
    public class ApprenticeshipTestListConstants
    {
        public const string LIST_ACADEMICYEAR = "listAcademicYear";
        public const string LIST_CLASS = "listClass";
        public const string LIST_APPRENTICESHIPTRAINING = "listApprenticeshipTraining";
        public const string IS_PAGEABLE = "isPageAble";
        public const string TEMPLATE_EXCEL = "HS_THPT_DSThiSinhDuThiNPT.xls";
    }
}