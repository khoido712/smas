﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportPupilSynthesisArea
{
    public class ReportPupilSynthesisConstant
    {
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_SUBJECT = "LIST_SUBJECT";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_PROPERTYCLASS = "LIST_PROPERTYCLASS";
    }
}