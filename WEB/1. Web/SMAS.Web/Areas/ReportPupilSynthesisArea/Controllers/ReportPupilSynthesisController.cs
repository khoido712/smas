﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Constants;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Web.Areas.ReportPupilSynthesisArea;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.ReportPupilSynthesisArea.Controllers
{
    public class ReportPupilSynthesisController : BaseController
    {
        //
        // GET: /ReportPupilSynthesisArea/ReportPupilSynthesis/

        GlobalInfo GlobalInfo = new GlobalInfo();

        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IPropertyOfClassBusiness PropertyOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IReportPupilSynthesisBusiness ReportPupilSynthesisBusiness;
        private readonly IClassPropertyCatBusiness ClassPropertyCatBusiness;


        public ReportPupilSynthesisController(IUserAccountBusiness useraccountBusiness, ISchoolFacultyBusiness schoolFacultyBusiness, IGroupCatBusiness groupCatBusiness, IPropertyOfClassBusiness PropertyOfClassBusiness, IEmployeeBusiness employeeBusiness, ISchoolProfileBusiness schoolProfileBusiness, IClassProfileBusiness ClassProfileBusiness, IClassSubjectBusiness ClassSubjectBusiness, IReportPupilSynthesisBusiness ReportPupilSynthesisBusiness, IClassPropertyCatBusiness ClassPropertyCatBusiness)
        {
            this.UserAccountBusiness = useraccountBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.GroupCatBusiness = groupCatBusiness;
            this.PropertyOfClassBusiness = PropertyOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ReportPupilSynthesisBusiness = ReportPupilSynthesisBusiness;
            this.ClassPropertyCatBusiness = ClassPropertyCatBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }


        /// <summary>
        /// GetViewData
        /// </summary>
        ///<author>Nam ta</author>
        ///<Date>11/27/2012</Date>
        public void GetViewData()
        {
            int EducationLevelID = 0;
            List<EducationLevel> lstEducationLevel = GlobalInfo.EducationLevels;

            if (lstEducationLevel != null && lstEducationLevel.Count > 0)
            {
                EducationLevelID = lstEducationLevel.FirstOrDefault().EducationLevelID;
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            List<ClassProfile> listClassProfile = ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();
            ViewData[ReportPupilSynthesisConstant.LIST_CLASS] = listClassProfile;

            List<ComboObject> listSemesterAll = CommonList.SemesterAndAll();
            // Neu la cap 1 thi bo ca nam di
            if (GlobalInfo.AppliedLevel.HasValue && GlobalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                listSemesterAll = CommonList.Semester();
            }
            List<SelectListItem> listSemester = new List<SelectListItem>();
            foreach (ComboObject item in listSemesterAll)
            {
                if (GlobalInfo.Semester.HasValue && GlobalInfo.Semester.Value.ToString().Equals(item.key))
                {
                    listSemester.Add(new SelectListItem { Value = item.key, Text = item.value, Selected = true });
                }
                else
                {
                    listSemester.Add(new SelectListItem { Value = item.key, Text = item.value, Selected = false });
                }
            }
            ViewData[ReportPupilSynthesisConstant.LIST_SEMESTER] = listSemester;

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            dic["EducationLevelID"] = EducationLevelID;
            IEnumerable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic);

            IEnumerable<SubjectCat> listSubjectCat = listClassSubject.Select(u => u.SubjectCat).Distinct()
                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName);

            //DANH SACH MON HOC
            List<ViettelCheckboxList> listSubject = new List<ViettelCheckboxList>();
            foreach (SubjectCat ClassSubject in listSubjectCat)
            {
                ViettelCheckboxList item = new ViettelCheckboxList();
                item.Value = ClassSubject.SubjectCatID;
                item.Label = ClassSubject.DisplayName;
                item.cchecked = true;
                listSubject.Add(item);
            }

            ViewData[ReportPupilSynthesisConstant.LIST_SUBJECT] = listSubject;


            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            //DANH SACH THUOC TINH CUA LOP
            IEnumerable<PropertyOfClass> listPropertyOfClass = PropertyOfClassBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic);
            //IEnumerable<ClassPropertyCat> listClassPropertyCat = listPropertyOfClass.Select(u => u.ClassPropertyCat).Distinct();
            var listClassPropertyCat = ClassPropertyCatBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", GlobalInfo.AppliedLevel } });
            List<ViettelCheckboxList> listClassProperty = new List<ViettelCheckboxList>();
            ViettelCheckboxList item1 = new ViettelCheckboxList();
            //Thêm các tính chất không có trong danh mục them truc tiep vao db

            //item1 = new ViettelCheckboxList();
            //item1.Value = -1;
            //item1.Label = "Học ngoại ngữ 1";
            //item1.cchecked = true;
            //listClassProperty.Add(item1);
            //item1 = new ViettelCheckboxList();
            //item1.Value = -2;
            //item1.Label = "Học ngoại ngữ 2";
            //item1.cchecked = true;
            //listClassProperty.Add(item1);
            //item1 = new ViettelCheckboxList();
            //item1.Value = -3;
            //item1.Label = "Học nghề PT";
            //item1.cchecked = true;
            //listClassProperty.Add(item1);
            //item1 = new ViettelCheckboxList();
            //item1.Value = -4;
            //item1.Label = "Lớp ghép";
            //item1.cchecked = true;
            //listClassProperty.Add(item1);
            //item1 = new ViettelCheckboxList();
            //item1.Value = -5;
            //item1.Label = "Học tin học";
            //item1.cchecked = true;
            //listClassProperty.Add(item1);
            foreach (ClassPropertyCat PropertyOfClass in listClassPropertyCat)
            {
                ViettelCheckboxList item = new ViettelCheckboxList();
                item.Value = PropertyOfClass.ClassPropertyCatID;
                item.Label = PropertyOfClass.Resolution;
                item.cchecked = true;
                listClassProperty.Add(item);
            }
            ViewData[ReportPupilSynthesisConstant.LIST_PROPERTYCLASS] = listClassProperty;
        }

        /// <summary>
        /// GetcboClass
        /// </summary>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>11/28/2012</Date>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboClass(int? EducationLevelID)
        {

            //Load combobox lop hoc theo khoi hoc
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            if (EducationLevelID.HasValue)
                dic["EducationLevelID"] = EducationLevelID.Value;
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).ToList();
            return Json(new SelectList(listClass, "ClassProfileID", "DisplayName"));

        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadPropertyOfClass(int? ClassID, int? EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            if (ClassID.HasValue)
                dic["ClassID"] = ClassID.Value;
            if (EducationLevelID.HasValue)
                dic["EducationLevelID"] = EducationLevelID.Value;
            //DANH SACH THUOC TINH CUA LOP
            IEnumerable<PropertyOfClass> listPropertyOfClass = PropertyOfClassBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic);
            //Sửa đổi: Không load tính chất theo lớp mà load tất cả tính chất
            //var list = listPropertyOfClass.Select(u => u.ClassPropertyCat).Distinct();
            var list = ClassPropertyCatBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", GlobalInfo.AppliedLevel } });
            ViettelCheckboxList item = new ViettelCheckboxList();
            List<ViettelCheckboxList> listClassProperty = new List<ViettelCheckboxList>();
            //Thêm các tính chất không có trong danh mục
            item = new ViettelCheckboxList();
            item.Value = -1;
            item.Label = "Học ngoại ngữ 1";
            item.cchecked = true;
            listClassProperty.Add(item);
            item = new ViettelCheckboxList();
            item.Value = -2;
            item.Label = "Học ngoại ngữ 2";
            item.cchecked = true;
            listClassProperty.Add(item);
            item = new ViettelCheckboxList();
            item.Value = -3;
            item.Label = "Học nghề PT";
            item.cchecked = true;
            listClassProperty.Add(item);
            item = new ViettelCheckboxList();
            item.Value = -4;
            item.Label = "Lớp ghép";
            item.cchecked = true;
            listClassProperty.Add(item);
            item = new ViettelCheckboxList();
            item.Value = -5;
            item.Label = "Học tin học";
            item.cchecked = true;
            listClassProperty.Add(item);
            //Các tính chất trong danh mục
            foreach (ClassPropertyCat PropertyOfClass in list)
            {
                item = new ViettelCheckboxList();
                item.Value = PropertyOfClass.ClassPropertyCatID;
                item.Label = PropertyOfClass.Resolution;
                item.cchecked = true;
                listClassProperty.Add(item);
            }

            ViewData[ReportPupilSynthesisConstant.LIST_PROPERTYCLASS] = listClassProperty;

            return PartialView("_PropertyClass");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadSubjectOfClass(int? ClassID, int? EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            if (ClassID.HasValue)
                dic["ClassID"] = ClassID.Value;
            if (EducationLevelID.HasValue)
                dic["EducationLevelID"] = EducationLevelID.Value;
            //DANH SACH MON HOC CUA LOP
            IEnumerable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic);

            IEnumerable<SubjectCat> listSubjectCat = listClassSubject.Select(u => u.SubjectCat).Distinct()
                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName);

            //DANH SACH MON HOC
            List<ViettelCheckboxList> listSubject = new List<ViettelCheckboxList>();
            foreach (SubjectCat ClassSubject in listSubjectCat)
            {
                ViettelCheckboxList item = new ViettelCheckboxList();
                item.Value = ClassSubject.SubjectCatID;
                item.Label = ClassSubject.DisplayName;
                item.cchecked = true;
                listSubject.Add(item);
            }
            ViewData[ReportPupilSynthesisConstant.LIST_SUBJECT] = listSubject;
            return PartialView("_SubjectClass");
        }


        [HttpPost]
        
        public FileResult ExportFile(FormCollection form)
        {
            int EducationLevelID = form["cboEducationLevel"].Equals("") ? 0 : SMAS.Business.Common.Utils.GetInt(form["cboEducationLevel"]);
            int ClassID = form["cboClass"].Equals("") ? 0 : SMAS.Business.Common.Utils.GetInt(form["cboClass"]);
            int Semester = form["cboSemester"].Equals("") ? 0 : SMAS.Business.Common.Utils.GetInt(form["cboSemester"]);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            //dua tat ca thong tin tu form vao dictionary
            foreach (String key in form.AllKeys)
            {
                dic.Add(key, form[key]);
            }
            String filename = "";
            Stream excel = this.ReportPupilSynthesisBusiness.ExportPupilSynthesis(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, Semester, _globalInfo.AppliedLevel.Value, EducationLevelID, ClassID, dic,_globalInfo.IsSuperVisingDeptRole,_globalInfo.IsSubSuperVisingDeptRole, out filename);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;
            return result;
        }



    }
}
