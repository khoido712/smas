﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportPupilSynthesisArea
{
    public class ReportPupilSynthesisAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportPupilSynthesisArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportPupilSynthesisArea_default",
                "ReportPupilSynthesisArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
