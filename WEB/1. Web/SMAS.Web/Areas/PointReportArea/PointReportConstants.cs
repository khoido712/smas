﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PointReportArea
{
    public class PointReportConstants
    {
        public const string LS_EDUCATION_LEVEL = "listEducationLevel";
        public const string LS_CLASS = "listClass";
        public const string LS_SEMESTER = "listSemester";
        public const string DF_SEMESTER = "defaultSemester";
        public const string LS_PERIOD = "listPeriod";
        public const string LS_PAPER_SIZE = "listPaperSize";
    }
}