﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PointReportArea
{
    public class PointReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PointReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PointReportArea_default",
                "PointReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
