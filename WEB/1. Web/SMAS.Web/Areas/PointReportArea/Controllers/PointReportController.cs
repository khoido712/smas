﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.PointReportArea.Controllers
{
    public class PointReportController : BaseController
    {
        //
        // GET: /PointReportArea/PointReport/

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IPointReportBusiness PointReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;

        public PointReportController(IClassProfileBusiness classProfileBusiness
            , IEducationLevelBusiness educationLevelBusiness
            , IPeriodDeclarationBusiness periodDeclarationBusiness
            , IPointReportBusiness pointReportBusiness
            , IReportDefinitionBusiness reportDefinitionBusiness
            , IProcessedReportBusiness processedReportBusiness
            , IAcademicYearBusiness academicYearBusiness
            , IMarkRecordBusiness markRecordBusiness
            , IMarkRecordHistoryBusiness markRecordHistoryBusiness
            , IJudgeRecordBusiness judgeRecordBusiness
            , IJudgeRecordHistoryBusiness judgeRecordHistoryBusiness
            , ISummedUpRecordBusiness summedUpRecordBusiness
            , ISummedUpRecordHistoryBusiness summedUpRecordHistoryBusiness
            , IMarkTypeBusiness markTypeBusiness)
        {
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PeriodDeclarationBusiness = periodDeclarationBusiness;
            this.PointReportBusiness = pointReportBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.MarkRecordBusiness = markRecordBusiness;
            this.MarkRecordHistoryBusiness = markRecordHistoryBusiness;
            this.JudgeRecordBusiness = judgeRecordBusiness;
            this.JudgeRecordHistoryBusiness = judgeRecordHistoryBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.SummedUpRecordHistoryBusiness = summedUpRecordHistoryBusiness;
            this.MarkTypeBusiness = markTypeBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();

            //Danh sách khối học
            ViewData[PointReportConstants.LS_EDUCATION_LEVEL] = Global.EducationLevels;

            //Danh sách lớp học
            ViewData[PointReportConstants.LS_CLASS] = new List<ClassProfile>();
            int Edu = Global.EducationLevels.FirstOrDefault().EducationLevelID;
            if (Global.EducationLevels.Count > 0)
            {
                ViewData[PointReportConstants.LS_CLASS] = ClassProfileBusiness.All.Where(o=>o.EducationLevelID == Edu && o.SchoolID ==Global.SchoolID && o.AcademicYearID == Global.AcademicYearID && o.IsActive.Value).ToList();
            }
            //Danh sách học kỳ
            ViewData[PointReportConstants.LS_SEMESTER] = CommonList.Semester();
            int defaultSemester = (Global.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : Global.Semester.GetValueOrDefault();
            ViewData[PointReportConstants.DF_SEMESTER] = defaultSemester;

            //Danh sách đợt
            ViewData[PointReportConstants.LS_PERIOD] = new List<PeriodDeclaration>();
            AjaxLoadPeriod(defaultSemester);
            //Danh sách kích thước giấy in
            List<ComboObject> listPaperSize = new List<ComboObject>();
            listPaperSize.Add(new ComboObject(SystemParamsInFile.PAPER_SIZE_A4.ToString(), "A4"));
            listPaperSize.Add(new ComboObject(SystemParamsInFile.PAPER_SIZE_A5.ToString(), "A5"));
            ViewData[PointReportConstants.LS_PAPER_SIZE] = listPaperSize;

            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? idEducationLevel)
        {
            ViewData[PointReportConstants.LS_CLASS] = new List<ClassProfile>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = idEducationLevel;
            dicClass["AcademicYearID"] = GlobalInfo.AcademicYearID;
            if (!GlobalInfo.IsAdminSchoolRole)
            {
                dicClass["UserAccountID"] = GlobalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dicClass);
            
            if (lsClass.Count() != 0)
            {
                ViewData[PointReportConstants.LS_CLASS] = lsClass;
            }
            List<ClassProfile> lstClass = lsClass.OrderBy(o => o.DisplayName).ToList();
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPeriod(int? idSemester)
        {
            ViewData[PointReportConstants.LS_PERIOD] = new List<PeriodDeclaration>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dicPeriod = new Dictionary<string, object>();
            dicPeriod["Semester"] = idSemester;
            dicPeriod["AcademicYearID"] = GlobalInfo.AcademicYearID;
            IQueryable<PeriodDeclaration> lsPeriod = PeriodDeclarationBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dicPeriod);
            if (lsPeriod.Count() != 0)
            {
                ViewData[PointReportConstants.LS_PERIOD] = lsPeriod;
            }
            return Json(new SelectList(lsPeriod.ToList(), "PeriodDeclarationID", "Resolution"), JsonRequestBehavior.AllowGet);
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetReportByPeriod(PointReportBO pointReportBO)
        {
            // Neu khong co dot bao loi
            if (pointReportBO.PeriodDeclarationID == 0)
            {
                throw new BusinessException("Common_Null_PeriodID");
            }
            GlobalInfo GlobalInfo = new GlobalInfo();
            pointReportBO.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            pointReportBO.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            pointReportBO.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
            pointReportBO.UserAccountID = GlobalInfo.UserAccountID;
            ReportDefinition reportDef = PointReportBusiness.GetReportDefinitionOfPeriodReport(pointReportBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PointReportBusiness.GetPointReportByPeriod(pointReportBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = PointReportBusiness.CreatePointReportByPeriod(pointReportBO);
                processedReport = PointReportBusiness.InsertPointReportByPeriod(pointReportBO, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportByPeriod(PointReportBO pointReportBO)
        {
            // Neu khong co dot bao loi
            if (pointReportBO.PeriodDeclarationID == 0)
            {
                throw new BusinessException("Common_Null_PeriodID");
            }
            GlobalInfo GlobalInfo = new GlobalInfo();
            pointReportBO.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            pointReportBO.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            pointReportBO.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
            pointReportBO.UserAccountID = GlobalInfo.UserAccountID;
            Stream excel = PointReportBusiness.CreatePointReportByPeriod(pointReportBO);
            ProcessedReport processedReport = PointReportBusiness.InsertPointReportByPeriod(pointReportBO, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetReportBySemester(PointReportBO pointReportBO)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            pointReportBO.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            pointReportBO.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            pointReportBO.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
            pointReportBO.UserAccountID = GlobalInfo.UserAccountID;
            ReportDefinition reportDef = PointReportBusiness.GetReportDefinitionOfSemesterReport(pointReportBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PointReportBusiness.GetPointReportBySemester(pointReportBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                List<MarkRecordBO> lstMarkRecord;
                List<JudgeRecordBO> lstJudgeRecord;
                List<SummedUpRecordBO> lstSummedUpRecord;
                AcademicYear acaYear = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID.Value);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

                if (isMovedHistory)
                {
                     //Lấy danh sách thông tin điểm môn tính điểm
                    lstMarkRecord = this.getMarkRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                    //Lấy danh sách thông tin điểm môn nhận xét
                    lstJudgeRecord = this.getJudgeRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                    //Lấy danh sách thông tin điểm tổng kết
                    lstSummedUpRecord = this.getSummedUpRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                }
                else
                {
                    //Lấy danh sách thông tin điểm môn tính điểm
                    lstMarkRecord = this.getMarkRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                    //Lấy danh sách thông tin điểm môn nhận xét
                    lstJudgeRecord = this.getJudgeRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                    //Lấy danh sách thông tin điểm tổng kết
                    lstSummedUpRecord = this.getSummedUpRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                }
                Stream excel = PointReportBusiness.CreatePointReportBySemester(pointReportBO, lstMarkRecord, lstJudgeRecord, lstSummedUpRecord);
                processedReport = PointReportBusiness.InsertPointReportBySemester(pointReportBO, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySemester(PointReportBO pointReportBO)
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            pointReportBO.AcademicYearID = globalInfo.AcademicYearID.GetValueOrDefault();
            pointReportBO.SchoolID = globalInfo.SchoolID.GetValueOrDefault();
            pointReportBO.AppliedLevel = globalInfo.AppliedLevel.GetValueOrDefault();
            pointReportBO.UserAccountID = globalInfo.UserAccountID;
            List<MarkRecordBO> lstMarkRecord;
            List<JudgeRecordBO> lstJudgeRecord;
            List<SummedUpRecordBO> lstSummedUpRecord;
            AcademicYear acaYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            if (isMovedHistory)
            {
                 //Lấy danh sách thông tin điểm môn tính điểm
                lstMarkRecord = this.getMarkRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                    , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                //Lấy danh sách thông tin điểm môn nhận xét
                lstJudgeRecord = this.getJudgeRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                    , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                //Lấy danh sách thông tin điểm tổng kết
                lstSummedUpRecord = this.getSummedUpRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                    , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
            }
            else
            {
                //Lấy danh sách thông tin điểm môn tính điểm
                lstMarkRecord = this.getMarkRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                    , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                //Lấy danh sách thông tin điểm môn nhận xét
                lstJudgeRecord = this.getJudgeRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                    , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                //Lấy danh sách thông tin điểm tổng kết
                lstSummedUpRecord = this.getSummedUpRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                    , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
            }
            Stream excel = PointReportBusiness.CreatePointReportBySemester(pointReportBO,lstMarkRecord,lstJudgeRecord ,lstSummedUpRecord);
            ProcessedReport processedReport = PointReportBusiness.InsertPointReportBySemester(pointReportBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        private List<MarkRecordBO> getMarkRecord(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
             //Lấy danh sách thông tin điểm môn tính điểm
                Dictionary<string, object> dicMarkRecord = new Dictionary<string, object> 
                {
                    {"AcademicYearID", AcademicYearID}, 
                    {"SchoolID", SchoolID},
                    {"EducationLevelID", EducationLevelID},
                    {"ClassID", ClassID},
                    {"Semester", Semester},
                    {"Year", Year}
                };
                List<MarkRecordBO> lstMarkRecord = (from p in this.MarkRecordBusiness.SearchBySchool(SchoolID, dicMarkRecord)
                                join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                select new MarkRecordBO
                                {
                                    PupilID = p.PupilID,
                                    ClassID = p.ClassID,
                                    SubjectID = p.SubjectID,
                                    Title = q.Title,
                                    Mark = p.Mark,
                                    Semester = p.Semester,
                                    OrderNumber = p.OrderNumber
                                })
                                .ToList();
            return lstMarkRecord;
        }
        private List<MarkRecordBO> getMarkRecordHistory(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
             //Lấy danh sách thông tin điểm môn tính điểm
                Dictionary<string, object> dicMarkRecord = new Dictionary<string, object> 
                {
                    {"AcademicYearID", AcademicYearID}, 
                    {"SchoolID", SchoolID},
                    {"EducationLevelID", EducationLevelID},
                    {"ClassID", ClassID},
                    {"Semester", Semester},
                    {"Year", Year}
                };
                List<MarkRecordBO> lstMarkRecord = (from p in this.MarkRecordHistoryBusiness.SearchMarkRecordHistory(dicMarkRecord)
                                join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                select new MarkRecordBO
                                {
                                    PupilID = p.PupilID,
                                    ClassID = p.ClassID,
                                    SubjectID = p.SubjectID,
                                    Title = q.Title,
                                    Mark = p.Mark,
                                    Semester = p.Semester,
                                    OrderNumber = p.OrderNumber
                                })
                                .ToList();
            return lstMarkRecord;
        }
        private List<JudgeRecordBO> getJudgeRecord(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm môn nhận xét
            Dictionary<string, object> dicJudgeRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"SchoolID", SchoolID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID},
                {"Semester", Semester},
                {"checkWithClassMovement", "checkWithClassMovement"},
                {"Year", Year}
            };
            List<JudgeRecordBO> lstJudgeRecord = (from p in JudgeRecordBusiness.SearchBySchool(SchoolID, dicJudgeRecord)
                                                 join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                 select new JudgeRecordBO
               {
                   PupilID = p.PupilID,
                   ClassID = p.ClassID,
                   SubjectID = p.SubjectID,
                   Title = q.Title,
                   Judgement = p.Judgement,
                   Semester = p.Semester,
                   OrderNumber = p.OrderNumber
               })
                .ToList();
            return lstJudgeRecord;
        }
        private List<JudgeRecordBO> getJudgeRecordHistory(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm môn nhận xét
            Dictionary<string, object> dicJudgeRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"SchoolID", SchoolID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID},
                {"Semester", Semester},
                {"checkWithClassMovement", "checkWithClassMovement"},
                {"Year", Year}
            };
            List<JudgeRecordBO> lstJudgeRecord = (from p in JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(dicJudgeRecord)
                                                 join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                 select new JudgeRecordBO
               {
                   PupilID = p.PupilID,
                   ClassID = p.ClassID,
                   SubjectID = p.SubjectID,
                   Title = q.Title,
                   Judgement = p.Judgement,
                   Semester = p.Semester,
                   OrderNumber = p.OrderNumber
               })
                .ToList();
            return lstJudgeRecord;
        }
        private List<SummedUpRecordBO> getSummedUpRecord(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm tổng kết
            Dictionary<string, object> dicSummedUpRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"SchoolID", SchoolID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID}
            };
            List<SummedUpRecordBO> lstSummedUpRecord = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, dicSummedUpRecord)
                 .Select(o => new SummedUpRecordBO
                 {
                     PupilID = o.PupilID,
                     ClassID = o.ClassID,
                     SubjectID = o.SubjectID,
                     JudgementResult = o.JudgementResult,
                     SummedUpMark = o.SummedUpMark,
                     ReTestMark = o.ReTestMark,
                     ReTestJudgement = o.ReTestJudgement,
                     Semester = o.Semester
                 }).ToList();
            return lstSummedUpRecord;
        }
        private List<SummedUpRecordBO> getSummedUpRecordHistory(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm tổng kết
            Dictionary<string, object> dicSummedUpRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"SchoolID", SchoolID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID}
            };
            List<SummedUpRecordBO> lstSummedUpRecord = this.SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, dicSummedUpRecord)
                 .Select(o => new SummedUpRecordBO
                 {
                     PupilID = o.PupilID,
                     ClassID = o.ClassID,
                     SubjectID = o.SubjectID,
                     JudgementResult = o.JudgementResult,
                     SummedUpMark = o.SummedUpMark,
                     ReTestMark = o.ReTestMark,
                     ReTestJudgement = o.ReTestJudgement,
                     Semester = o.Semester
                 }).ToList();
            return lstSummedUpRecord;
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel},
                {"UserAccountID", GlobalInfo.UserAccountID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A4,
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A5,
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A4,
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A5,
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A4,
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A5
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}
