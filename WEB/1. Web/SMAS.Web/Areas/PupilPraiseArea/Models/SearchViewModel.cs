/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PupilPraiseArea.Models
{
    public class SearchViewModel
    {
        public Nullable<int> EducationLevelID1 { get; set; }
        public Nullable<int> ClassID1 { get; set; }

        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public String FullName { get; set; }
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public String PupilCode { get; set; }
        public Nullable<int> PraiseTypeID1 { get; set; }
        public Nullable<int> PraiseLevel1 { get; set; }
    }
}
