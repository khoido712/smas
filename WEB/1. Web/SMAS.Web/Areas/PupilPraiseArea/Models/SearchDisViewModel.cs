/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PupilPraiseArea.Models
{
    public class SearchDisViewModel
    {
        public Nullable<int> EducationLevelID2 { get; set; }
        public Nullable<int> ClassID2 { get; set; }
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public String FullName2 { get; set; }
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public String PupilCode2 { get; set; }
        public Nullable<int> DisciplineTypeID2 { get; set; }
        public Nullable<int> DisciplineLevel2 { get; set; }
    }
}
