/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using System.ComponentModel;
using SMAS.Models.CustomAttribute;
namespace SMAS.Web.Areas.PupilPraiseArea.Models
{
    public class PupilPraiseViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 PupilPraiseID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]    
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_EducationLevel_Create)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this, this.options[this.selectedIndex].value);")]
        public System.Int32 EducationLevelID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]    
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_Class_Create)]
        [AdditionalMetadata("OnChange", "AjaxLoadPupil(this, this.options[this.selectedIndex].value);")]     
        public System.Int32 ClassID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_Pupil")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_Pupil_Create)]        
        public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_PraiseType_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 PraiseTypeID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_PraiseLevel_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 PraiseLevel { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseDate")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public DateTime? PraiseDate { get; set; }

        [ResourceDisplayName("PupilPraise_Label_RecordedInSchoolReport")]                
        public bool IsRecordedInSchoolReport { get; set; }

        [ResourceDisplayName("PupilPraise_Label_RecordedInSchoolReport")]
        public bool IsRecordedInSchoolReport1 { get; set; }

        [ResourceDisplayName("PupilPraise_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
		public System.String Description { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.Int32> Semester { get; set; }						

        [ScaffoldColumn(false)]
        public System.Int32 AcademicYearID { get; set; }			

        [ScaffoldColumn(false)]
        public System.Int32 SchoolID { get; set; }

        [ScaffoldColumn(false)]
        public int? ClassOrderNumber { get; set; }

        [ScaffoldColumn(false)]
        public int? OrderInClass { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_FullName")]
        public System.String FullName { get; set; }

        [ScaffoldColumn(false)]
        public String Name { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_PupilCode")]
        public System.String PupilCode { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_BirthDate")]        
        public System.DateTime BirthDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_Genre")]        
        public int Genre { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_Genre")]
        public string GT { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_ClassName")]
        public string ClassName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_PraiseTotal")]
        public int PraiseTotal { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_PraiseType")]    
        public string PraiseType { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilPraise_Label_PraiseLevel")]
        public string PraiseLevelString { get; set; }

        [ScaffoldColumn(false)]
        public bool CanEdit { get; set; }
      
    }
}


