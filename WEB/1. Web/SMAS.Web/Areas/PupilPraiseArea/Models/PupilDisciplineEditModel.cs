using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.PupilPraiseArea.Models
{
    public class PupilDisciplineEditModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 PupilDisciplineID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_EducationLevel")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_EducationLevel_Create)]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 EducationLevelID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_Class")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_Class_Create)]
        //[AdditionalMetadata("OnChange", "AjaxLoadPupil(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 ClassID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_Pupil")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_Pupil_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineType")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_DisciplineType_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 DisciplineTypeID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineLevel")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_DisciplineLevel_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 DisciplineLevel { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineDate")]
        //[UIHint("DateTimePicker")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public DateTime? DisciplineDateEdit { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_RecordedInSchoolReport")]
        public bool IsRecordedInSchoolReport { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String Description { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 AcademicYearID { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 SchoolID { get; set; }
    }
}
