using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using System.ComponentModel;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.PupilPraiseArea.Models
{
    public class PupilPraiseEditModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 PupilPraiseID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_EducationLevel_Create)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 EducationLevelID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_Class_Create)]
        [AdditionalMetadata("OnChange", "AjaxLoadPupil(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 ClassID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_Pupil")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_Pupil_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_PraiseType_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 PraiseTypeID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilPraiseConstants.LIST_PraiseLevel_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 PraiseLevel { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseDate")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public DateTime? PraiseDateEdit { get; set; }

        [ResourceDisplayName("PupilPraise_Label_RecordedInSchoolReport")]
        public bool IsRecordedInSchoolReport { get; set; }

        [ResourceDisplayName("PupilPraise_Label_RecordedInSchoolReport")]
        public bool IsRecordedInSchoolReport1 { get; set; }

        [ResourceDisplayName("PupilPraise_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String Description { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.Int32> Semester { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 AcademicYearID { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 SchoolID { get; set; }		
    }
}
