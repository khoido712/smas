/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PupilPraiseArea.Models
{
    public class PupilDisciplineViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 PupilDisciplineID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_EducationLevel")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_EducationLevel_Create)]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 EducationLevelID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_Class")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_Class_Create)]
        //[AdditionalMetadata("OnChange", "AjaxLoadPupil(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 ClassID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_Pupil")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_Pupil_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineType")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_DisciplineType_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 DisciplineTypeID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineLevel")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", PupilDisciplineConstants.LIST_DisciplineLevel_Create)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 DisciplineLevel { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineDate")]
        //[UIHint("DateTimePicker")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public DateTime? DisciplineDate { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_RecordedInSchoolReport")]
        public bool IsRecordedInSchoolReport { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String Description { get; set; }     

        [ScaffoldColumn(false)]
        public System.Int32 AcademicYearID { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 SchoolID { get; set; }

        [ScaffoldColumn(false)]
        public int? ClassOrderNumber { get; set; }

        [ScaffoldColumn(false)]
        public int? OrderInClass { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_FullName")]
        public System.String FullName { get; set; }

        [ScaffoldColumn(false)]
        public String Name { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_PupilCode")]
        public System.String PupilCode { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_BirthDate")]
        public System.DateTime BirthDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_Genre")]
        public int Genre { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_Genre")]
        public string GT { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_ClassName")]
        public string ClassName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_DisciplineTotal")]
        public int DisciplineTotal { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_DisciplineType")]
        public string DisciplineType { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilDiscipline_Label_DisciplineLevel")]
        public string DisciplineLevelString { get; set; }

        [ScaffoldColumn(false)]
        public bool CanEdit { get; set; }

    }
}


