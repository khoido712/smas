﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilPraiseArea
{
    public class PupilPraiseAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilPraiseArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilPraiseArea_default",
                "PupilPraiseArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
