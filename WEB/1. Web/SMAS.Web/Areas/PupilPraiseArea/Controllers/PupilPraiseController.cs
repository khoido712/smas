﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PupilPraiseArea.Models;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.Business.Common;
using SMAS.Business.SearchForm;


namespace SMAS.Web.Areas.PupilPraiseArea.Controllers
{
    public class PupilPraiseController : BaseController
    {
        private readonly IPupilPraiseBusiness PupilPraiseBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPraiseTypeBusiness PraiseTypeBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IDisciplineTypeBusiness DisciplineTypeBusiness;
        private readonly IPupilDisciplineBusiness PupilDisciplineBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public PupilPraiseController(IPupilPraiseBusiness pupilpraiseBusiness, IEducationLevelBusiness educationLevelBusiness,
            IClassProfileBusiness classProfileBusiness, IPraiseTypeBusiness praiseTypeBusiness,
            IPupilProfileBusiness pupilProfileBusiness, IPupilOfClassBusiness pupilOfClassBusiness, IDisciplineTypeBusiness pisciplineTypeBusiness,
            IPupilDisciplineBusiness pupilDisciplineBusiness, IAcademicYearBusiness academicYearBusiness)
        {
            this.PupilPraiseBusiness = pupilpraiseBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PraiseTypeBusiness = praiseTypeBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.DisciplineTypeBusiness = pisciplineTypeBusiness;
            this.PupilDisciplineBusiness = pupilDisciplineBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }

        //
        // GET: /PupilPraise/

        public ActionResult Index()
        {

            SetViewData();
            return View();
        }
        #region setViewData
        private void SetViewData()
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = new GlobalInfo().AppliedLevel;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).ToList();
            ViewData[PupilPraiseConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_EducationLevel_Create] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_EducationLevel_Discipline] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_EducationLevel_Create_Discipline] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            //Đưa dữ liệu ra combobox lớp = rỗng
            ViewData[PupilPraiseConstants.LIST_Class] = new SelectList(new string[] { });
            ViewData[PupilPraiseConstants.LIST_Class_Create] = new SelectList(new string[] { });
            ViewData[PupilPraiseConstants.LIST_Class_Discipline] = new SelectList(new string[] { });
            ViewData[PupilPraiseConstants.LIST_Class_Create_Discipline] = new SelectList(new string[] { });

            //Đưa dữ liệu ra combobox học sinh = rỗng
            ViewData[PupilPraiseConstants.LIST_Pupil_Create] = new SelectList(new string[] { });
            ViewData[PupilPraiseConstants.LIST_Pupil_Create_Discipline] = new SelectList(new string[] { });

            //Đưa dữ liệu ra combobox hình thức khen thưởng
            IDictionary<string, object> PraiseTypeInfo = new Dictionary<string, object>();
            PraiseTypeInfo["IsActive"] = true;
            IQueryable<PraiseType> LstPraiseTypeFormDB = PraiseTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, PraiseTypeInfo);
            List<PraiseType> LstPraiseType = LstPraiseTypeFormDB.ToList();
            ViewData[PupilPraiseConstants.LIST_PraiseType] = new SelectList(LstPraiseType, "PraiseTypeID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_PraiseType_Create] = new SelectList(LstPraiseType, "PraiseTypeID", "Resolution");

            //Đưa dữ liệu ra combobox hình thức kỷ luật
            IDictionary<string, object> DisTypeInfo = new Dictionary<string, object>();
            DisTypeInfo["IsActive"] = true;
            List<DisciplineType> LstDisciplineType = DisciplineTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, DisTypeInfo).ToList();
            ViewData[PupilPraiseConstants.LIST_DisciplineType] = new SelectList(LstDisciplineType, "DisciplineTypeID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_DisciplineType_Create] = new SelectList(LstDisciplineType, "DisciplineTypeID", "Resolution");

            //Đưa dữ liệu ra combobox nơi khen thưởng
            List<ComboObject> ListPraiseLevel = new List<ComboObject>();
            ListPraiseLevel.Add(new ComboObject("1", Res.Get("Education_Hierachy_Level_Ministry")));
            ListPraiseLevel.Add(new ComboObject("2", Res.Get("Education_Hierachy_Level_Province_Office")));
            ListPraiseLevel.Add(new ComboObject("3", Res.Get("Education_Hierachy_Level_District_Office")));
            ListPraiseLevel.Add(new ComboObject("4", Res.Get("Education_Hierachy_Level_School")));
            ListPraiseLevel.Add(new ComboObject("5", Res.Get("Education_Hierachy_Level_Class")));
            ViewData[PupilPraiseConstants.LIST_PraiseLevel] = new SelectList(ListPraiseLevel, "key", "value");
            ViewData[PupilPraiseConstants.LIST_PraiseLevel_Create] = new SelectList(ListPraiseLevel, "key", "value");
            ViewData[PupilPraiseConstants.LIST_DisciplineLevel] = new SelectList(ListPraiseLevel, "key", "value");
            ViewData[PupilPraiseConstants.LIST_DisciplineLevel_Create] = new SelectList(ListPraiseLevel, "key", "value");

            //Đưa ra danh sách học sinh khen thuong
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            IEnumerable<PupilPraiseViewModel> lst = this._Search(SearchInfo);

            List<PupilPraiseViewModel> lst1 = new List<PupilPraiseViewModel>();
            foreach (var item in lst)
            {
                bool Check = false;
                foreach (var item1 in lst1)
                {
                    if ((item1.PupilID == item.PupilID) && (item1.ClassID == item.ClassID))
                    {
                        Check = true;
                        item1.PraiseTotal = item1.PraiseTotal + 1;
                    }
                }
                if (Check == false)
                {
                    if (item.Genre == 0)
                    {
                        item.GT = Res.Get("Common_Label_Female");
                    }
                    else { item.GT = Res.Get("Common_Label_Male"); }
                    item.PraiseTotal = 1;
                    lst1.Add(item);
                }
            }

            //Đưa ra danh sách học sinh ky luat
            IDictionary<string, object> SearchDisInfo = new Dictionary<string, object>();
            SearchDisInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            IEnumerable<PupilDisciplineViewModel> lstDis = this._SearchDis(SearchInfo);

            List<PupilDisciplineViewModel> lstDis1 = new List<PupilDisciplineViewModel>();
            foreach (var item in lstDis)
            {
                bool Check = false;
                foreach (var item1 in lstDis1)
                {
                    if ((item1.PupilID == item.PupilID) && (item1.ClassID == item.ClassID))
                    {
                        Check = true;
                        item1.DisciplineTotal = item1.DisciplineTotal + 1;
                    }
                }
                if (Check == false)
                {
                    if (item.Genre == 0)
                    {
                        item.GT = Res.Get("Common_Label_Female");
                    }
                    else { item.GT = Res.Get("Common_Label_Male"); }
                    item.DisciplineTotal = 1;
                    lstDis1.Add(item);
                }
            }
            SetPermission();
            ViewData[PupilPraiseConstants.LIST_PUPILPRAISE] = lst1;
            ViewData[PupilPraiseConstants.LIST_PUPILDISCIPLINE] = lstDis1;

        }
        #endregion
        //
        // GET: /PupilPraise/Search

        //phuongh1 08/08/2013
        private void SetPermission()
        {
            GlobalInfo global = new GlobalInfo();
            int permission = GetMenupermission("PupilPraise", global.UserAccountID, global.IsAdmin);
            ViewData[SMAS.Web.Constants.GlobalConstants.PER_CREATE] = !global.IsCurrentYear
                || permission < SystemParamsInFile.PER_CREATE;
            ViewData[SMAS.Web.Constants.GlobalConstants.PER_UPDATE] = !global.IsCurrentYear
                || permission < SystemParamsInFile.PER_UPDATE;
            ViewData[SMAS.Web.Constants.GlobalConstants.PER_DELETE] = !global.IsCurrentYear
                || permission < SystemParamsInFile.PER_DELETE;

        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["PraiseTypeID"] = frm.PraiseTypeID1;
            SearchInfo["PraiseLevel"] = frm.PraiseLevel1;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID1;
            SearchInfo["ClassID"] = frm.ClassID1;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["ListClassID"] = null;

            IEnumerable<PupilPraiseViewModel> lst = this._Search(SearchInfo);

            List<PupilPraiseViewModel> lst1 = new List<PupilPraiseViewModel>();
            foreach (var item in lst)
            {
                bool Check = false;
                foreach (var item1 in lst1)
                {
                    if ((item1.PupilID == item.PupilID) && (item1.ClassID == item.ClassID))
                    {
                        Check = true;
                        item1.PraiseTotal = item1.PraiseTotal + 1;
                    }
                }
                if (Check == false)
                {
                    if (item.Genre == 0)
                    {
                        item.GT = Res.Get("Common_Label_Female");
                    }
                    else { item.GT = Res.Get("Common_Label_Male"); }
                    item.PraiseTotal = 1;
                    lst1.Add(item);
                }
            }
            SetPermission();
            ViewData[PupilPraiseConstants.LIST_PUPILPRAISE] = lst1;

            //Get view data here

            return PartialView("_List");
        }

        public PartialViewResult SearchDis(SearchDisViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["FullName"] = frm.FullName2;
            SearchInfo["PupilCode"] = frm.PupilCode2;
            SearchInfo["DisciplineTypeID"] = frm.DisciplineTypeID2;
            SearchInfo["DisciplineLevel"] = frm.DisciplineLevel2;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID2;
            SearchInfo["ClassID"] = frm.ClassID2;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["ListClassID"] = null;

            IEnumerable<PupilDisciplineViewModel> lst = this._SearchDis(SearchInfo);

            List<PupilDisciplineViewModel> lst1 = new List<PupilDisciplineViewModel>();
            foreach (var item in lst)
            {
                bool Check = false;
                foreach (var item1 in lst1)
                {
                    if ((item1.PupilID == item.PupilID) && (item1.ClassID == item.ClassID))
                    {
                        Check = true;
                        item1.DisciplineTotal = item1.DisciplineTotal + 1;
                    }
                }
                if (Check == false)
                {
                    if (item.Genre == 0)
                    {
                        item.GT = Res.Get("Common_Label_Female");
                    }
                    else { item.GT = Res.Get("Common_Label_Male"); }
                    item.DisciplineTotal = 1;
                    lst1.Add(item);
                }
            }
            SetPermission();
            ViewData[PupilPraiseConstants.LIST_PUPILDISCIPLINE] = lst1;

            //Get view data here

            return PartialView("_ListDis");
        }
        /// <summary>
        /// Create praise
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(PupilPraiseViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            if (frm.EducationLevelID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_EducationLevelRepuired"), "error"));
            }
            if (frm.ClassID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_ClassRepuired"), "error"));
            }
            if (frm.PupilID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PupilRepuired"), "error"));
            }
            if (frm.PraiseTypeID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PraiseTypeRepuired"), "error"));
            }
            if (frm.PraiseLevel == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PraiseLevelRepuired"), "error"));
            }
            if (!frm.PraiseDate.HasValue)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PraiseDateRepuired"), "error"));
            }
            if (frm.Description == null || frm.Description.Trim() == "")
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DescriptionRepuired"), "error"));
            }
            if (frm.Description.Length > 256)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DescriptionMaxLength"), "error"));
            }
            using (TransactionScope scope = new TransactionScope())
            {
                int semester = 0;
                AcademicYear ac = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
                if (ac.FirstSemesterStartDate < frm.PraiseDate.Value.Date && ac.FirstSemesterEndDate > frm.PraiseDate.Value.Date)
                {
                    semester = 1;
                }
                if (ac.SecondSemesterStartDate < frm.PraiseDate.Value.Date && ac.SecondSemesterEndDate > frm.PraiseDate.Value.Date)
                {
                    semester = 2;
                }
                PupilPraise PupilPraise = new PupilPraise();
                //if (ModelState.IsValid)
                //{

                PupilPraise.PupilID = frm.PupilID;
                PupilPraise.ClassID = frm.ClassID;
                PupilPraise.EducationLevelID = frm.EducationLevelID;
                PupilPraise.SchoolID = new GlobalInfo().SchoolID.Value;
                PupilPraise.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                PupilPraise.PraiseTypeID = frm.PraiseTypeID;
                PupilPraise.PraiseDate = frm.PraiseDate.Value.Date;
                PupilPraise.PraiseLevel = frm.PraiseLevel;
                PupilPraise.Description = frm.Description;
                PupilPraise.IsRecordedInSchoolReport = frm.IsRecordedInSchoolReport;
                PupilPraise.Semester = semester;
                Utils.Utils.TrimObject(PupilPraise);
                this.PupilPraiseBusiness.InsertPupilPraise(new GlobalInfo().UserAccountID, PupilPraise);
                this.PupilPraiseBusiness.Save();
                scope.Complete();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                //}
                //string jsonErrList = Res.GetJsonErrorMessage(ModelState);
                //return Json(new JsonMessage(jsonErrList, "error"));
            }
        }
        public PartialViewResult _Create()
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = new GlobalInfo().AppliedLevel;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).ToList();
            //ViewData[PupilPraiseConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_EducationLevel_Create] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            //Đưa dữ liệu ra combobox lớp = rỗng
            //ViewData[PupilPraiseConstants.LIST_Class] = new SelectList(new string[] { });
            ViewData[PupilPraiseConstants.LIST_Class_Create] = new SelectList(new string[] { });

            //Đưa dữ liệu ra combobox học sinh = rỗng
            ViewData[PupilPraiseConstants.LIST_Pupil_Create] = new SelectList(new string[] { });

            //Đưa dữ liệu ra combobox hình thức khen thưởng
            IDictionary<string, object> PraiseTypeInfo = new Dictionary<string, object>();
            PraiseTypeInfo["IsActive"] = true;
            List<PraiseType> LstPraiseType = PraiseTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, PraiseTypeInfo).ToList();
            //ViewData[PupilPraiseConstants.LIST_PraiseType] = new SelectList(LstPraiseType, "PraiseTypeID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_PraiseType_Create] = new SelectList(LstPraiseType, "PraiseTypeID", "Resolution");

            //Đưa dữ liệu ra combobox nơi khen thưởng
            List<ComboObject> ListPraiseLevel = new List<ComboObject>();
            ListPraiseLevel.Add(new ComboObject("1", Res.Get("Education_Hierachy_Level_Ministry")));
            ListPraiseLevel.Add(new ComboObject("2", Res.Get("Education_Hierachy_Level_Province_Office")));
            ListPraiseLevel.Add(new ComboObject("3", Res.Get("Education_Hierachy_Level_District_Office")));
            ListPraiseLevel.Add(new ComboObject("4", Res.Get("Education_Hierachy_Level_School")));
            ListPraiseLevel.Add(new ComboObject("5", Res.Get("Education_Hierachy_Level_Class")));
            //ViewData[PupilPraiseConstants.LIST_PraiseLevel] = new SelectList(ListPraiseLevel, "key", "value");
            ViewData[PupilPraiseConstants.LIST_PraiseLevel_Create] = new SelectList(ListPraiseLevel, "key", "value");

            return PartialView("_Create");
        }
        /// <summary>
        /// Create Discipline
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult CreateDis(PupilDisciplineViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            if (frm.EducationLevelID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_EducationLevelRepuired"), "error"));
            }
            if (frm.ClassID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_ClassRepuired"), "error"));
            }
            if (frm.PupilID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PupilRepuired"), "error"));
            }
            if (frm.DisciplineTypeID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DisciplineTypeRepuired"), "error"));
            }
            if (frm.DisciplineLevel == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DisciplineLevelRepuired"), "error"));
            }
            if (!frm.DisciplineDate.HasValue)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DisciplineDateRepuired"), "error"));
            }
            if (frm.Description == null || frm.Description.Trim() == "")
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DescriptionDisRepuired"), "error"));
            }
            if (frm.Description.Length > 256)
            {
                return Json(new JsonMessage(Res.Get("PupilDiscpline_Label_DescriptionMaxLength"), "error"));
            }
            using (TransactionScope scope = new TransactionScope())
            {
                PupilDiscipline PupilDiscipline = new PupilDiscipline();
                PupilDiscipline.PupilID = frm.PupilID;
                PupilDiscipline.ClassID = frm.ClassID;
                PupilDiscipline.EducationLevelID = frm.EducationLevelID;
                PupilDiscipline.SchoolID = new GlobalInfo().SchoolID.Value;
                PupilDiscipline.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                PupilDiscipline.DisciplineTypeID = frm.DisciplineTypeID;
                PupilDiscipline.DisciplinedDate = frm.DisciplineDate.Value.Date;
                PupilDiscipline.DisciplineLevel = frm.DisciplineLevel;
                PupilDiscipline.Description = frm.Description;
                PupilDiscipline.IsRecordedInSchoolReport = frm.IsRecordedInSchoolReport;
                Utils.Utils.TrimObject(PupilDiscipline);
                this.PupilDisciplineBusiness.InsertPupilDiscipline(new GlobalInfo().UserAccountID, PupilDiscipline);
                this.PupilDisciplineBusiness.Save();
                scope.Complete();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }
        public PartialViewResult _CreateDis()
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = new GlobalInfo().AppliedLevel;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).ToList();
            //ViewData[PupilPraiseConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_EducationLevel_Create_Discipline] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            //Đưa dữ liệu ra combobox lớp = rỗng
            //ViewData[PupilPraiseConstants.LIST_Class] = new SelectList(new string[] { });
            ViewData[PupilPraiseConstants.LIST_Class_Create_Discipline] = new SelectList(new string[] { });

            //Đưa dữ liệu ra combobox học sinh = rỗng
            ViewData[PupilPraiseConstants.LIST_Pupil_Create_Discipline] = new SelectList(new string[] { });

            //Đưa dữ liệu ra combobox hình thức kỷ luật
            IDictionary<string, object> DisTypeInfo = new Dictionary<string, object>();
            DisTypeInfo["IsActive"] = true;
            List<DisciplineType> LstDisciplineType = DisciplineTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, DisTypeInfo).ToList();
            ViewData[PupilPraiseConstants.LIST_DisciplineType] = new SelectList(LstDisciplineType, "DisciplineTypeID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_DisciplineType_Create] = new SelectList(LstDisciplineType, "DisciplineTypeID", "Resolution");

            //Đưa dữ liệu ra combobox nơi Kỷ luật
            List<ComboObject> ListPraiseLevel = new List<ComboObject>();
            ListPraiseLevel.Add(new ComboObject("1", Res.Get("Education_Hierachy_Level_Ministry")));
            ListPraiseLevel.Add(new ComboObject("2", Res.Get("Education_Hierachy_Level_Province_Office")));
            ListPraiseLevel.Add(new ComboObject("3", Res.Get("Education_Hierachy_Level_District_Office")));
            ListPraiseLevel.Add(new ComboObject("4", Res.Get("Education_Hierachy_Level_School")));
            ListPraiseLevel.Add(new ComboObject("5", Res.Get("Education_Hierachy_Level_Class")));
            //ViewData[PupilPraiseConstants.LIST_PraiseLevel] = new SelectList(ListPraiseLevel, "key", "value");
            ViewData[PupilPraiseConstants.LIST_DisciplineLevel_Create] = new SelectList(ListPraiseLevel, "key", "value");

            return PartialView("_CreateDis");
        }
        /// <summary>
        /// Update Praise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int PupilPraiseID, PupilPraiseEditModel frm)
        {
            CheckPermissionForAction(PupilPraiseID, "PupilPraise");
            Utils.Utils.TrimObject(frm);
            if (frm.PraiseTypeID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PraiseTypeRepuired"), "error"));
            }
            if (frm.PraiseLevel == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PraiseLevelRepuired"), "error"));
            }
            if (!frm.PraiseDateEdit.HasValue)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PraiseDateRepuired"), "error"));
            }
            if (frm.Description == null || frm.Description.Trim() == "")
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DescriptionRepuired"), "error"));
            }
            if (frm.Description.Length > 256)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DescriptionMaxLength"), "error"));
            }
            using (TransactionScope scope = new TransactionScope())
            {
                PupilPraise PupilPraise = PupilPraiseBusiness.Find(PupilPraiseID);
                TryUpdateModel(PupilPraise);
                PupilPraise.PraiseDate = frm.PraiseDateEdit.Value;
                Utils.Utils.TrimObject(PupilPraise);
                this.PupilPraiseBusiness.UpdatePupilPraise(new GlobalInfo().UserAccountID, PupilPraise);
                this.PupilPraiseBusiness.Save();
                scope.Complete();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
        }

        public PartialViewResult EditPraise(int id)
        {
            CheckPermissionForAction(id, "PupilPraise");
            return PartialView("_Edit", PrepareEdit(id));
        }

        public PartialViewResult EditDiscipline(int id)
        {
            CheckPermissionForAction(id, "PupilDiscipline");
            return PartialView("_EditDis", PrepareEditDis(id));
        }
        private PupilPraiseEditModel PrepareEdit(int id)
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = new GlobalInfo().AppliedLevel;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).ToList();
            ViewData[PupilPraiseConstants.LIST_EducationLevel_Create] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");

            //Đưa dữ liệu ra combobox hình thức khen thưởng
            IDictionary<string, object> PraiseTypeInfo = new Dictionary<string, object>();
            PraiseTypeInfo["IsActive"] = true;
            List<PraiseType> LstPraiseType = PraiseTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, PraiseTypeInfo).ToList();
            //ViewData[PupilPraiseConstants.LIST_PraiseType] = new SelectList(LstPraiseType, "PraiseTypeID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_PraiseType_Create] = new SelectList(LstPraiseType, "PraiseTypeID", "Resolution");

            //Đưa dữ liệu ra combobox nơi khen thưởng
            List<ComboObject> ListPraiseLevel = new List<ComboObject>();
            ListPraiseLevel.Add(new ComboObject("1", Res.Get("Education_Hierachy_Level_Ministry")));
            ListPraiseLevel.Add(new ComboObject("2", Res.Get("Education_Hierachy_Level_Province_Office")));
            ListPraiseLevel.Add(new ComboObject("3", Res.Get("Education_Hierachy_Level_District_Office")));
            ListPraiseLevel.Add(new ComboObject("4", Res.Get("Education_Hierachy_Level_School")));
            ListPraiseLevel.Add(new ComboObject("5", Res.Get("Education_Hierachy_Level_Class")));
            //ViewData[PupilPraiseConstants.LIST_PraiseLevel] = new SelectList(ListPraiseLevel, "key", "value");
            ViewData[PupilPraiseConstants.LIST_PraiseLevel_Create] = new SelectList(ListPraiseLevel, "key", "value");

            PupilPraise PupilPraise = this.PupilPraiseBusiness.Find(id);

            // Dua ra combobox Class
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["EducationLevelID"] = PupilPraise.EducationLevelID;
            ClassSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            List<ClassProfile> ListClassProfile = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, ClassSearchInfo).ToList();
            ViewData[PupilPraiseConstants.LIST_Class_Create] = new SelectList(ListClassProfile, "ClassProfileID", "DisplayName");

            // Dua ra combobox Pupil
            IDictionary<string, object> PupilSearchInfo = new Dictionary<string, object>();
            PupilSearchInfo["ClassID"] = PupilPraise.ClassID;
            PupilSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            List<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, PupilSearchInfo).ToList();
            List<PupilProfile> LstPupilProfile = new List<PupilProfile>();
            foreach (var pop in ListPupilOfClass)
            {
                PupilProfile PupilProfile = new PupilProfile();
                PupilProfile.PupilProfileID = pop.PupilID;
                PupilProfile.FullName = pop.PupilProfile.FullName;
                LstPupilProfile.Add(PupilProfile);
            }
            ViewData[PupilPraiseConstants.LIST_Pupil_Create] = new SelectList(LstPupilProfile, "PupilProfileID", "FullName");

            PupilPraiseEditModel frm = new PupilPraiseEditModel();
            Utils.Utils.BindTo(PupilPraise, frm);
            frm.PraiseDateEdit = PupilPraise.PraiseDate;
            return frm;
        }
        private PupilDisciplineEditModel PrepareEditDis(int id)
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = new GlobalInfo().AppliedLevel;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).ToList();
            //ViewData[PupilPraiseConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_EducationLevel_Create_Discipline] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");

            //Đưa dữ liệu ra combobox hình thức kỷ luật
            IDictionary<string, object> DisTypeInfo = new Dictionary<string, object>();
            DisTypeInfo["IsActive"] = true;
            List<DisciplineType> LstDisciplineType = DisciplineTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, DisTypeInfo).ToList();
            ViewData[PupilPraiseConstants.LIST_DisciplineType] = new SelectList(LstDisciplineType, "DisciplineTypeID", "Resolution");
            ViewData[PupilPraiseConstants.LIST_DisciplineType_Create] = new SelectList(LstDisciplineType, "DisciplineTypeID", "Resolution");

            //Đưa dữ liệu ra combobox nơi Kỷ luật
            List<ComboObject> ListPraiseLevel = new List<ComboObject>();
            ListPraiseLevel.Add(new ComboObject("1", Res.Get("Education_Hierachy_Level_Ministry")));
            ListPraiseLevel.Add(new ComboObject("2", Res.Get("Education_Hierachy_Level_Province_Office")));
            ListPraiseLevel.Add(new ComboObject("3", Res.Get("Education_Hierachy_Level_District_Office")));
            ListPraiseLevel.Add(new ComboObject("4", Res.Get("Education_Hierachy_Level_School")));
            ListPraiseLevel.Add(new ComboObject("5", Res.Get("Education_Hierachy_Level_Class")));
            //ViewData[PupilPraiseConstants.LIST_PraiseLevel] = new SelectList(ListPraiseLevel, "key", "value");
            ViewData[PupilPraiseConstants.LIST_DisciplineLevel_Create] = new SelectList(ListPraiseLevel, "key", "value");

            PupilDiscipline PupilDiscipline = this.PupilDisciplineBusiness.Find(id);

            // Dua ra combobox Class
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["EducationLevelID"] = PupilDiscipline.EducationLevelID;
            ClassSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            List<ClassProfile> ListClassProfile = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, ClassSearchInfo).ToList();
            ViewData[PupilPraiseConstants.LIST_Class_Create_Discipline] = new SelectList(ListClassProfile, "ClassProfileID", "DisplayName");

            // Dua ra combobox Pupil
            IDictionary<string, object> PupilSearchInfo = new Dictionary<string, object>();
            PupilSearchInfo["ClassID"] = PupilDiscipline.ClassID;
            PupilSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            List<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, PupilSearchInfo).ToList();
            List<PupilProfile> LstPupilProfile = new List<PupilProfile>();
            foreach (var pop in ListPupilOfClass)
            {
                PupilProfile PupilProfile = new PupilProfile();
                PupilProfile.PupilProfileID = pop.PupilID;
                PupilProfile.FullName = pop.PupilProfile.FullName;
                LstPupilProfile.Add(PupilProfile);
            }
            ViewData[PupilPraiseConstants.LIST_Pupil_Create_Discipline] = new SelectList(LstPupilProfile, "PupilProfileID", "FullName");

            PupilDisciplineEditModel frm = new PupilDisciplineEditModel();
            Utils.Utils.BindTo(PupilDiscipline, frm);
            frm.DisciplineDateEdit = PupilDiscipline.DisciplinedDate;
            return frm;
        }
        /// <summary>
        /// Update Discipline
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult EditDis(int PupilDisciplineID, PupilDisciplineEditModel frm)
        {
            CheckPermissionForAction(PupilDisciplineID, "PupilDiscipline");
            Utils.Utils.TrimObject(frm);
            if (frm.DisciplineTypeID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DisciplineTypeRepuired"), "error"));
            }
            if (frm.DisciplineLevel == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DisciplineLevelRepuired"), "error"));
            }
            if (!frm.DisciplineDateEdit.HasValue)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DisciplineDateRepuired"), "error"));
            }
            if (frm.Description == null || frm.Description.Trim() == "")
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DescriptionDisRepuired"), "error"));
            }
            if (frm.Description.Length > 256)
            {
                return Json(new JsonMessage(Res.Get("PupilDiscipline_Label_DescriptionMaxLength"), "error"));
            }
            using (TransactionScope scope = new TransactionScope())
            {
                PupilDiscipline PupilDiscipline = PupilDisciplineBusiness.Find(PupilDisciplineID);
                TryUpdateModel(PupilDiscipline);
                if (ModelState.IsValid)
                {
                    Utils.Utils.TrimObject(PupilDiscipline);
                    PupilDiscipline.DisciplinedDate = frm.DisciplineDateEdit.Value;
                    this.PupilDisciplineBusiness.UpdatePupilDiscipline(new GlobalInfo().UserAccountID, PupilDiscipline);
                    this.PupilDisciplineBusiness.Save();
                    scope.Complete();
                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                }
                string jsonErrList = Res.GetJsonErrorMessage(ModelState);
                return Json(new JsonMessage(jsonErrList, "error"));
            }
        }
        /// <summary>
        /// Delete praise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            CheckPermissionForAction(id, "PupilPraise");
            this.PupilPraiseBusiness.DeletePupilPraise(new GlobalInfo().UserAccountID, id, new GlobalInfo().SchoolID.Value);
            this.PupilPraiseBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        /// <summary>
        /// Delete Discipline
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult DeleteDis(int id)
        {
            CheckPermissionForAction(id, "PupilDiscipline");
            this.PupilDisciplineBusiness.DeletePupilDiscipline(new GlobalInfo().UserAccountID, id, new GlobalInfo().SchoolID.Value);
            this.PupilDisciplineBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));

        }
        /// <summary>
        /// Search praise
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PupilPraiseViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            int Grade = (int)new GlobalInfo().AppliedLevel.Value;
            IQueryable<PupilPraise> query = this.PupilPraiseBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo).Where(o => o.EducationLevel.Grade == Grade);

            List<PupilPraiseViewModel> lst = query.Select(o => new PupilPraiseViewModel
            {
                PupilPraiseID = o.PupilPraiseID,
                PupilID = o.PupilID,
                ClassID = o.ClassID,
                EducationLevelID = o.EducationLevelID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                PraiseTypeID = o.PraiseTypeID,
                PraiseDate = o.PraiseDate,
                PraiseLevel = o.PraiseLevel,
                IsRecordedInSchoolReport = o.IsRecordedInSchoolReport.Value,
                Semester = o.Semester,
                Description = o.Description,
                FullName = o.PupilProfile.FullName,
                Name = o.PupilProfile.Name,
                PupilCode = o.PupilProfile.PupilCode,
                ClassName = o.ClassProfile.DisplayName,
                ClassOrderNumber = o.ClassProfile.OrderNumber,
                Genre = o.PupilProfile.Genre,
                PraiseType = o.PraiseType != null ? o.PraiseType.Resolution : string.Empty,
                BirthDate = o.PupilProfile.BirthDate
            }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            // Kiem tra phan quyen cua user doi voi tung lop
            List<int> listClassID = lst.Select(o => o.ClassID).Distinct().ToList();
            Dictionary<int, bool> dicPermitClass = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, listClassID);
            foreach (var item in lst)
            {
                if (item.PraiseLevel == 1)
                {
                    item.PraiseLevelString = Res.Get("Education_Hierachy_Level_Ministry");
                }
                if (item.PraiseLevel == 2)
                {
                    item.PraiseLevelString = Res.Get("Education_Hierachy_Level_Province_Office");
                }
                if (item.PraiseLevel == 3)
                {
                    item.PraiseLevelString = Res.Get("Education_Hierachy_Level_District_Office");
                }
                if (item.PraiseLevel == 4)
                {
                    item.PraiseLevelString = Res.Get("Education_Hierachy_Level_School");
                }
                if (item.PraiseLevel == 5)
                {
                    item.PraiseLevelString = Res.Get("Education_Hierachy_Level_Class");
                }
                // Lay phan quyen
                item.CanEdit = dicPermitClass[item.ClassID];
            }
            return lst;
        }
        private IEnumerable<PupilDisciplineViewModel> _SearchDis(IDictionary<string, object> SearchDisInfo)
        {
            int Grade = (int)new GlobalInfo().AppliedLevel.Value;
            IQueryable<PupilDiscipline> query = this.PupilDisciplineBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchDisInfo).Where(o => o.EducationLevel.Grade == Grade);
            List<PupilDisciplineViewModel> lstDis = query.Select(o => new PupilDisciplineViewModel
            {
                PupilDisciplineID = o.PupilDisciplineID,
                PupilID = o.PupilID,
                ClassID = o.ClassID,
                EducationLevelID = o.EducationLevelID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                DisciplineTypeID = o.DisciplineTypeID,
                DisciplineDate = o.DisciplinedDate,
                DisciplineLevel = o.DisciplineLevel,
                IsRecordedInSchoolReport = o.IsRecordedInSchoolReport,
                Description = o.Description,
                FullName = o.PupilProfile.FullName,
                Name = o.PupilProfile.Name,
                PupilCode = o.PupilProfile.PupilCode,
                ClassName = o.ClassProfile.DisplayName,
                ClassOrderNumber = o.ClassProfile.OrderNumber,
                Genre = o.PupilProfile.Genre,
                DisciplineType = o.DisciplineType != null ? o.DisciplineType.Resolution : string.Empty,
                BirthDate = o.PupilProfile.BirthDate
            }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            // Kiem tra phan quyen cua user doi voi tung lop
            List<int> listClassID = lstDis.Select(o => o.ClassID).Distinct().ToList();
            Dictionary<int, bool> dicPermitClass = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, listClassID);
            foreach (var item in lstDis)
            {
                if (item.DisciplineLevel == 1)
                {
                    item.DisciplineLevelString = Res.Get("Education_Hierachy_Level_Ministry");
                }
                if (item.DisciplineLevel == 2)
                {
                    item.DisciplineLevelString = Res.Get("Education_Hierachy_Level_Province_Office");
                }
                if (item.DisciplineLevel == 3)
                {
                    item.DisciplineLevelString = Res.Get("Education_Hierachy_Level_District_Office");
                }
                if (item.DisciplineLevel == 4)
                {
                    item.DisciplineLevelString = Res.Get("Education_Hierachy_Level_School");
                }
                if (item.DisciplineLevel == 5)
                {
                    item.DisciplineLevelString = Res.Get("Education_Hierachy_Level_Class");
                }
                item.CanEdit = dicPermitClass[item.ClassID];
            }
            return lstDis;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? educationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            List<ClassProfile> lst = new List<ClassProfile>();
            if (educationLevelID.ToString() != "")
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = educationLevelID;
                dic["AcademicYearID"] = global.AcademicYearID;

                if (global.IsAdminSchoolRole == false)
                {
                    dic["UserAccountID"] = global.UserAccountID;
                    dic["EmployeeID"] = global.EmployeeID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                }
                lst = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass1(int? educationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            List<ClassProfile> lst = new List<ClassProfile>();
            if (educationLevelID.ToString() != "")
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = educationLevelID;
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;

                lst = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPupil(int? classid)
        {
            GlobalInfo gi = new GlobalInfo();
            List<PupilProfile> LstPupilProfile = new List<PupilProfile>();
            if (classid != null)
            {
                LstPupilProfile = PupilOfClassBusiness.SearchBySchool(gi.SchoolID.Value, new PupilOfClassSearchForm()
                {
                    ClassID = classid,
                    AcademicYearID = gi.AcademicYearID,
                    Status = SystemParamsInFile.PUPIL_STATUS_STUDYING
                }).OrderBy(x => x.OrderInClass).Select(x => x.PupilProfile).ToList();
            }
            return Json(new SelectList(LstPupilProfile, "PupilProfileID", "FullName"));
        }

        #region Detail

        public PartialViewResult Detail(int PupilID, int ClassID)
        {
            CheckPermissionForAction(PupilID, "PupilProfile", 3);
            PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilID);
            PupilPraiseViewModel PupilPraise = new PupilPraiseViewModel();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = PupilID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["ListClassID"] = null;

            IEnumerable<PupilPraiseViewModel> lst = this._Search(SearchInfo);
            ViewData[PupilPraiseConstants.LIST_PUPILPRAISE_DETAIL] = lst;

            SetPermission();


            /**/
            //Get view data here
            List<PupilPraiseViewModel> ListPupilPraise = lst.ToList();
            ViewData[PupilPraiseConstants.COUNT_PRAISE] = ListPupilPraise.Count();
            if (ListPupilPraise.Count > 0)
            {
                return PartialView("_Detail", ListPupilPraise[0]);
            }
            else
            {
                PupilPraise.FullName = PupilProfile.FullName;
                return PartialView("_Detail", PupilPraise);
            }
        }

        public PartialViewResult DetailDis(int PupilID, int ClassID)
        {
            CheckPermissionForAction(PupilID, "PupilProfile", 3);
            PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilID);
            PupilDisciplineViewModel PupilDiscipline = new PupilDisciplineViewModel();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = PupilID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["ListClassID"] = null;

            IEnumerable<PupilDisciplineViewModel> lst = this._SearchDis(SearchInfo);
            ViewData[PupilPraiseConstants.LIST_PUPILDISCIPLINE_DETAIL] = lst;
            //Get view data here
            List<PupilDisciplineViewModel> ListPupilDis = lst.ToList();
            int countDis = ListPupilDis != null ? ListPupilDis.Count() : 0;
            ViewData[PupilPraiseConstants.COUNT_DISCIPLINE] = countDis.ToString();
            SetPermission();
            if (ListPupilDis.Count > 0)
            {
                return PartialView("_DetailDis", ListPupilDis[0]);
            }
            else
            {
                PupilDiscipline.FullName = PupilProfile.FullName;
                return PartialView("_DetailDis", PupilDiscipline);
            }

        }

        public bool isHeadTeacher(int classID)
        {
            GlobalInfo global = new GlobalInfo();
            if (!global.IsCurrentYear)
                return false;
            else
            {
                if (UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, classID))
                    return true;
                else
                    return false;
            }
        }
        #endregion
    }
}





