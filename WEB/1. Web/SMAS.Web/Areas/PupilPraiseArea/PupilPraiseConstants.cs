/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilPraiseArea
{
    public class PupilPraiseConstants
    {
        public const string LIST_PUPILPRAISE_DETAIL = "LIST_PUPILPRAISE_DETAIL";
        public const string LIST_PUPILPRAISE = "list LIST_PUPILPRAISE Praise";
        public const string LIST_PUPILDISCIPLINE = "LIST_PUPILDISCIPLINE";
        public const string LIST_EducationLevel = "LIST_EducationLevel";
        public const string LIST_Class = "LIST_Class";
        public const string LIST_PraiseType = "LIST_PraiseType";
        public const string LIST_PraiseLevel = "LIST_PraiseLevel";
        public const string LIST_PUPILDISCIPLINE_CREATE = "LIST_PUPILDISCIPLINE_CREATE";
        public const string LIST_EducationLevel_Create = "LIST_EducationLevel_Create";
        public const string LIST_Class_Create = "LIST_Class_Create";
        public const string LIST_PraiseType_Create = "LIST_PraiseType_Create";
        public const string LIST_PraiseLevel_Create = "LIST_PraiseLevel_Create";
        public const string LIST_Pupil_Create = "LIST_Pupil_Create";

        public const string LIST_PUPILDISCIPLINE_DETAIL = "LIST_PUPILDISCIPLINE_DETAIL";
        public const string LIST_EducationLevel_Discipline = "LIST_EducationLevel_Discipline";
        public const string LIST_Class_Discipline = "LIST_Class_Discipline";
        public const string LIST_DisciplineType = "LIST_DisciplineType";
        public const string LIST_DisciplineLevel = "LIST_DisciplineLevel";
        public const string LIST_EducationLevel_Create_Discipline = "LIST_EducationLevel_Create_Discipline";
        public const string LIST_Class_Create_Discipline = "LIST_Class_Create_Discipline";
        public const string LIST_DisciplineType_Create = "LIST_DisciplineType_Create";
        public const string LIST_DisciplineLevel_Create = "LIST_DisciplineLevel_Create";
        public const string LIST_Pupil_Create_Discipline = "LIST_Pupil_Create_Discipline";
        public const string  COUNT_DISCIPLINE = "count_discipline";
        public const string COUNT_PRAISE = "count_praise";
    }
}