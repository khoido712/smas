﻿using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.VnenClassSubjectArea.Models
{
    public class ListViewModel
    {
        [ResourceDisplayName("VnenClassSubject_Is_Vnen")]
        public long ClassSubjectID { get; set; }
        public int SubjectID { get; set; }
        [ResourceDisplayName("VnenClassSubject_Subject_Name")]
        public string SubjectName { get; set; }
        [ResourceDisplayName("VnenClassSubject_Abreviation")]
        public string Abreviation { get; set; }
        public int? AppliedType { get; set; }
        public int? IsCommenting { get; set; }
        public bool? IsVnenSubject { get; set; }

        [ResourceDisplayName("VnenClassSubject_Subject_Type")]
        public string StrAppliedType
        {
            get
            {
                switch (AppliedType)
                {
                    case 0:
                        return Res.Get("AppliedTypeSubject_Require");
                    case 1:
                        return Res.Get("AppliedTypeSubject_Choice");
                    case 2:
                        return Res.Get("AppliedTypeSubject_ChoiceAddPiority");
                    case 3:
                        return Res.Get("AppliedTypeSubject_ChoiceCalculatorMark");
                    case 4:
                        return Res.Get("AppliedTypeSubject_ApparentShip");
                    default:
                        return String.Empty;
                }
                 
            }
        }
        [ResourceDisplayName("VnenClassSubject_Subject_Kind")]
        public string StrIsCommenting
        {
            get
            {
                switch (IsCommenting)
                {
                    case 0:
                        return Res.Get("Common_Label_Is_Not_CommentingSubject");
                    case 1:
                        return Res.Get("Common_Label_IsCommentingSubject");
                    default:
                        return String.Empty;
                }
            }
        }
    }
}