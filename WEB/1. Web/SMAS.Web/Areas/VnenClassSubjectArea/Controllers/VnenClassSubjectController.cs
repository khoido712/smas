﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.VnenClassSubjectArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.VnenClassSubjectArea.Controllers
{
    public class VnenClassSubjectController:BaseController
    {
        #region properties
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness ;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        
        private List<EducationLevel> lstEducationLevel;
        private List<ClassProfile> lstClass;
        #endregion

        #region Constructor
        public VnenClassSubjectController(IClassSubjectBusiness ClassSubjectBusiness, IEducationLevelBusiness EducationLevelBusiness,
            IClassProfileBusiness ClassProfileBusiness, ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        #endregion

        #region Actions
       
        public ActionResult Index()
        {
            SetViewData();

            //Lay danh sach mon hoc cua lop
            int? educationLevelID = null;
            if (lstEducationLevel.Count > 0)
            {
                educationLevelID = lstEducationLevel.First().EducationLevelID;
            }

            int? classID = null;
            if (lstClass.Count > 0)
            {
                classID = lstClass.First().ClassProfileID;
            }

            ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = new List<ListViewModel>();
            if (educationLevelID != null && classID != null)
            {
                ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = _Search(educationLevelID.Value, classID.Value);
            }
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            ViewData["IsVNEN"] = objSP.IsNewSchoolModel.HasValue && objSP.IsNewSchoolModel.Value;

            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = CheckButtonPermission();

            return View();
        }

        public PartialViewResult Search(SearchViewModel form)
        {
            

            Utils.Utils.TrimObject(form);

            List<ListViewModel> lstResult=new List<ListViewModel>();

            if (form.EducationLevelID != null && form.ClassID != null)
            {
                lstResult = _Search(form.EducationLevelID.Value, form.ClassID.Value);
            }

            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = CheckButtonPermission();
            return PartialView("_List", lstResult);
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic.Add("EducationLevelID", educationLevelID);
            dic.Add("SchoolID", _globalInfo.SchoolID);
            lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).Where(o=>o.IsVnenClass==true)
                .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(string selectedIDs, int? classID)
        {
            if (!IsCurrentYear() || GetMenupermission("VnenClassSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            List<long> lstClassSubjectID = GetListIDFromString(selectedIDs);

            if (lstClassSubjectID.Count == 0)
            {
                throw new BusinessException("VnenClassSubject_Message_NoChoice");
            }

            ClassProfile cp = ClassProfileBusiness.Find(classID);
            if (cp == null || (cp != null && (cp.IsVnenClass == null || cp.IsVnenClass == false)))
            {
                throw new BusinessException("VnenClassSubject_Error_Class_NotVnen");
            }

            //Lay ra danh sach mon hoc cua lop
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.All.Where(o => o.ClassID == classID).ToList();
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                ClassSubject cs = lstClassSubject[i];
                if (lstClassSubjectID.Contains(cs.ClassSubjectID))
                {
                    if (cs.IsSubjectVNEN != true)
                    {
                        cs.IsSubjectVNEN = true;
                        ClassSubjectBusiness.Update(cs);
                    }
                }
                else
                {
                    if (cs.IsSubjectVNEN == true)
                    {
                        cs.IsSubjectVNEN = false;
                        ClassSubjectBusiness.Update(cs);
                    }
                }
                ClassSubjectBusiness.Update(cs);
            }

            ClassSubjectBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_Save")));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveEdu(string selectedIDs, string notSelectedIDs, int? educationLevelID)
        {
            if (!IsCurrentYear() || GetMenupermission("VnenClassSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            List<long> lstSelectedSubjectID = GetListIDFromString(selectedIDs);
            List<long> lstNotSelectedSubjectID = GetListIDFromString(notSelectedIDs);

            if (lstSelectedSubjectID.Count == 0)
            {
                throw new BusinessException("VnenClassSubject_Message_NoChoice");
            }

            //Lay ra cac lop VNEN cua khoi
            List<ClassProfile> lstClass = new List<ClassProfile>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (educationLevelID != null)
            {
                dic.Add("EducationLevelID", educationLevelID);
                dic.Add("SchoolID", _globalInfo.SchoolID);
                lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).Where(o => o.IsVnenClass == true).ToList();
            }

            SaveInMultiClass(lstClass, lstSelectedSubjectID, lstNotSelectedSubjectID);

            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSchool(string selectedIDs, string notSelectedIDs)
        {
            if (!IsCurrentYear() || GetMenupermission("VnenClassSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            List<long> lstSelectedSubjectID = GetListIDFromString(selectedIDs);
            List<long> lstNotSelectedSubjectID = GetListIDFromString(notSelectedIDs);

            if (lstSelectedSubjectID.Count == 0)
            {
                throw new BusinessException("VnenClassSubject_Message_NoChoice");
            }

            //Lay ra cac lop VNEN cua khoi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", _globalInfo.SchoolID);
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).Where(o => o.IsVnenClass == true).ToList();

            SaveInMultiClass(lstClass, lstSelectedSubjectID, lstNotSelectedSubjectID);

            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lay danh sach khoi
            lstEducationLevel = _globalInfo.EducationLevels;

            int? defaultEducationLevelID = null;
            if (lstEducationLevel.Count > 0)
            {
                defaultEducationLevelID = lstEducationLevel.First().EducationLevelID;
            }
            ViewData[VnenClassSubjectConstants.CBO_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            //Lay danh sach lop
            if (defaultEducationLevelID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();

                dic.Add("EducationLevelID", defaultEducationLevelID);
                dic.Add("SchoolID", _globalInfo.SchoolID);
                lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).Where(o=>o.IsVnenClass==true)
                    .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
            }
            else
            {
                lstClass = new List<ClassProfile>();
            }
            ViewData[VnenClassSubjectConstants.CBO_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
        }


        private List<ListViewModel> _Search(int educationLevelID, int classID)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();

            Dictionary<string, object> dic = new Dictionary<string, object>();

            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchByClass(classID, dic).ToList();

            //Lay ra cac mon tang cuong
            List<int> lstIncreaseID = lstClassSubject.Where(o => o.SubjectIDIncrease != null && o.SubjectIDIncrease != 0).Select(o => o.SubjectIDIncrease.Value).ToList();

            lstClassSubject = lstClassSubject.Where(o =>o.IsSpecializedSubject != true &&  (o.SubjectIDIncrease == 0 || o.SubjectIDIncrease == null) && !lstIncreaseID.Contains(o.SubjectID)).ToList();

            lstResult = lstClassSubject.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                                                        .Select(o => new ListViewModel
                                                        {
                                                            SubjectID=o.SubjectID,
                                                            SubjectName=o.SubjectCat.DisplayName,
                                                            Abreviation=o.SubjectCat.Abbreviation,
                                                            ClassSubjectID=o.ClassSubjectID,
                                                            AppliedType = o.AppliedType,
                                                            IsCommenting = o.IsCommenting,
                                                            IsVnenSubject=o.IsSubjectVNEN
                                                        }).ToList();

            return lstResult;
        }

        private List<long> GetListIDFromString(string str)
        {
            string[] idArr;
            if (str != "")
            {
                str = str.Remove(str.Length - 1);
                idArr = str.Split(',');
            }
            else
            {
                idArr = new string[] { };
            }
            List<long> lstID = idArr.Length > 0 ? idArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList() :
                                            new List<long>();

            return lstID;
        }

        private void SaveInMultiClass(List<ClassProfile> lstClass, List<long> lstSelectedSubjectID, List<long> lstNotSelectedSubjectID)
        {
            if (lstClass.Count == 0)
            {
                throw new BusinessException("VnenClassSubject_Error_Not_Have_Vnen_Class");
            }

            List<int> lstClassID = lstClass.Select(o => o.ClassProfileID).ToList();
            List<ClassSubject> lstClassSubjectEdu = ClassSubjectBusiness.All.Where(o => lstClassID.Contains(o.ClassID)).ToList();

            for (int i = 0; i < lstClass.Count; i++)
            {
                int classID = lstClass[i].ClassProfileID;
                List<ClassSubject> lstClassSubject = lstClassSubjectEdu.Where(o => o.ClassID == classID).ToList();
                //Loai ra cac mon chuyen, mon tang cuong
                List<int> lstIncreaseID = lstClassSubject.Where(o => o.SubjectIDIncrease != null && o.SubjectIDIncrease != 0).Select(o => o.SubjectIDIncrease.Value).ToList();
                lstClassSubject = lstClassSubject.Where(o => o.IsSpecializedSubject != true && (o.SubjectIDIncrease == 0 || o.SubjectIDIncrease == null) && !lstIncreaseID.Contains(o.SubjectID)).ToList();

                //Cap nhat cac mon check chon
                for (int j = 0; j < lstSelectedSubjectID.Count; j++)
                {
                    long subjectID = lstSelectedSubjectID[j];
                    ClassSubject cs = lstClassSubject.Where(o=>o.SubjectID == subjectID).FirstOrDefault();
                    if (cs != null && cs.IsSubjectVNEN != true)
                    {
                        cs.IsSubjectVNEN = true;
                        ClassSubjectBusiness.Update(cs);
                    }
                }

                //cap nhat cac mon khong check chon
                for (int j = 0; j < lstNotSelectedSubjectID.Count; j++)
                {
                    long subjectID = lstNotSelectedSubjectID[j];
                    ClassSubject cs = lstClassSubject.Where(o => o.SubjectID == subjectID).FirstOrDefault();
                    if (cs != null && cs.IsSubjectVNEN == true)
                    {
                        cs.IsSubjectVNEN = false;
                        ClassSubjectBusiness.Update(cs);
                    }
                }
            }

            ClassSubjectBusiness.Save();
        }

        private bool CheckButtonPermission()
        {

            return IsCurrentYear() &&
                GetMenupermission("VnenClassSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) >= SystemParamsInFile.PER_CREATE;
        }

        private bool IsCurrentYear()
        {
            bool isCurrentYear = false;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            if ((DateTime.Compare(nowDate, aca.FirstSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.FirstSemesterEndDate.Value) <= 0) ||
                (DateTime.Compare(nowDate, aca.SecondSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.SecondSemesterEndDate.Value) <= 0))
            {
                isCurrentYear = true;
            }
            return isCurrentYear;
        }
        #endregion
    }
}