﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.VnenClassSubjectArea
{
    public class VnenClassSubjectConstants
    {
        public const string CBO_EDUCATION_LEVEL = "cbo_education_level";
        public const string CBO_CLASS = "cbo_class";
        public const string LIST_CLASS_SUBJECT = "list_class_subject";
        public const string PER_CHECK_BUTTON = "PER_CHECK_BUTTON";

    }
}