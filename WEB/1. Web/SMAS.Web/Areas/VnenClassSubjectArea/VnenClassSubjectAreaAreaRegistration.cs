﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.VnenClassSubjectArea
{
    public class VnenClassSubjectAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "VnenClassSubjectArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "VnenClassSubjectArea_default",
                "VnenClassSubjectArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
