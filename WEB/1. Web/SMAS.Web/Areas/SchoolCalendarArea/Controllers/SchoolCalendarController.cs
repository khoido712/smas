﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SchoolCalendarArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Web.Areas.ClassProfileArea;
using System.IO;
using SMAS.Business.Business;
using System.Web.Script.Serialization;
using SMAS.Web;
using SMAS.VTUtils.Excel.Export;
using System.Web;
using System.Text;
using SMAS.Business.BusinessObject;
namespace SMAS.Web.Areas.SchoolCalendarArea.Controllers
{
    public class SchoolCalendarController : BaseController
    {
        //
        // GET: /SchoolCalendarArea/SchoolCalendar/

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassPropertyCatBusiness ClassPropertyCatBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly IPropertyOfClassBusiness PropertyOfClassBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ICalendarBusiness CalendarBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ISendResultBusiness SendResultBusiness;
        private readonly IAssignSubjectConfigBusiness AssignSubjectConfigBusiness; 
        /// <summary>
        /// Contructor
        /// </summary>
        public SchoolCalendarController(IClassProfileBusiness classprofileBusiness, IEducationLevelBusiness EducationLevelBusiness,
            IClassPropertyCatBusiness ClassPropertyCatBusiness, ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness,
            IAcademicYearBusiness AcademicYearBusiness, ISubCommitteeBusiness SubCommitteeBusiness,
            IPropertyOfClassBusiness PropertyOfClassBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness,
            ITeachingAssignmentBusiness teachingAssignmentBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            ISendResultBusiness SendResultBusiness,
            ISubjectCatBusiness SubjectCatBusiness, ICalendarBusiness calendarBusiness,
            IAssignSubjectConfigBusiness AssignSubjectConfigBusiness)
        {
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.CalendarBusiness = calendarBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassPropertyCatBusiness = ClassPropertyCatBusiness;
            this.SchoolSubsidiaryBusiness = SchoolSubsidiaryBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.PropertyOfClassBusiness = PropertyOfClassBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SendResultBusiness = SendResultBusiness;
            this.AssignSubjectConfigBusiness = AssignSubjectConfigBusiness;
        }

        public const int startDay = 2, endDay = 8;
        public const int startNumberOfPeriod = 1, endNumberOfPeriod = 5;

        /// <summary>
        /// First Load View
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //Lay thong tin Request ClassID
            int classID = 0;
            if (!string.IsNullOrEmpty(Request["classID"]))
            {
                int.TryParse(Request["classID"].ToString(), out classID);
                ViewData["requestClassID"] = classID;
            }
            ClassProfile selClass = null;
            if (classID > 0)
            {
                selClass = ClassProfileBusiness.All.Where(a => a.ClassProfileID == classID && a.IsActive.Value).FirstOrDefault();
            }
            // Lấy về danh sách khôi
            List<SelectListItem> lstLevel = new List<SelectListItem>();
            foreach (var itm in _globalInfo.EducationLevels)
            {
                SelectListItem slItem = new SelectListItem();
                slItem.Text = itm.Resolution;
                slItem.Value = itm.EducationLevelID.ToString();
                if (selClass != null && itm.EducationLevelID == selClass.EducationLevelID)
                {
                    slItem.Selected = true;
                }
                lstLevel.Add(slItem);
            }
            SelectListItem allSelect = new SelectListItem();
            allSelect.Text = Res.Get("All");
            allSelect.Value = "";
            lstLevel.Insert(0, allSelect);
            //if (lstLevel.Count > 0 && selClass == null) lstLevel[1].Selected = true;
            ViewData["lstLevel"] = lstLevel;

            // Danh sách học kỳ
            List<ComboObject> lstCombo = CommonList.Semester();
            List<SelectListItem> lstSemester = new List<SelectListItem>();
            foreach (var itm in lstCombo)
            {
                SelectListItem slItem = new SelectListItem();
                slItem.Text = itm.value;
                slItem.Value = itm.key;
                if (itm.key == _globalInfo.Semester.Value.ToString())
                {
                    slItem.Selected = true;
                }
                lstSemester.Add(slItem);
            }
            ViewData["lstSemester"] = lstSemester;
            //List buoi hoc
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (selClass != null)
            {
                dic["EducationLevelID"] = selClass.EducationLevelID;
            }
            else
            {
                //if (lstLevel.Count > 0) dic["EducationLevelID"] = lstLevel[1].Value;
            }

            IEnumerable<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).OrderBy(a => a.DisplayName).ToList();
            // Danh sách lớp
            ViewData["lstClass"] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            //Danh sach section
            int? maxSection = lstClass.Max(a => a.Section);
            if (selClass != null)
            {
                maxSection = lstClass.Where(a => a.ClassProfileID == selClass.ClassProfileID).Max(a => a.Section);
            }
            var lstSection = new List<CalendarBusiness.ClassSection>();
            CalendarBusiness.GetSection((maxSection.HasValue) ? maxSection.Value : 0, ref lstSection);
            ViewData["lstSection"] = new SelectList(lstSection.ToList().OrderBy(u => u.Section), "Section", "SectionName");
            //Danh sách giáo vien trường
            dic["EmploymentStatus"] = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
            dic["CurrentSchoolID"] = _globalInfo.SchoolID.Value;
            List<string> lstTeacherName = EmployeeBusiness.SearchTeacher(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.Name).ThenBy(o => o.FullName).Select(a => a.FullName + " - " + a.EmployeeCode).ToList();
            ViewData["lstTeacher"] = lstTeacherName;

            //danh sach ngay ap dung
            List<int> lstClassID = lstClass.Select(p => p.ClassProfileID).Distinct().ToList();
            IDictionary<string, object> dicSearchCalendar = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"lstClassID",lstClassID}
            };
            IQueryable<Calendar> iquery = CalendarBusiness.Search(dicSearchCalendar);
            List<string> lstAppliedDate = iquery.OrderByDescending(p => p.StartDate).ToList().Select(p => p.StartDate.Value.ToString("dd/MM/yyyy")).Distinct().ToList();
            string appliedDate = string.Empty;
            List<SelectListItem> lstItemAppliedDate = new List<SelectListItem>();
            SelectListItem objItem = null;
            for (int i = 0; i < lstAppliedDate.Count; i++)
            {
                appliedDate = lstAppliedDate[i];
                objItem = new SelectListItem();
                objItem.Value = appliedDate;
                objItem.Text = appliedDate;
                lstItemAppliedDate.Add(objItem);
            }
            ViewData["lstAppliedDate"] = lstItemAppliedDate;
            ViewData["MaxDate"] = iquery.Count() > 0 ? iquery.Max(p => p.StartDate).Value.ToString("dd/MM/yyyy") : "";
            //Return View
            return View();
        }

        public ActionResult AjaxLoadAppliedDate(int? ClassID)
        {
            ClassProfile selClass = null;
            if (ClassID > 0)
            {
                selClass = ClassProfileBusiness.All.Where(a => a.ClassProfileID == ClassID && a.IsActive.Value).FirstOrDefault();
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (selClass != null)
            {
                dic["EducationLevelID"] = selClass.EducationLevelID;
            }
            else
            {
                //if (lstLevel.Count > 0) dic["EducationLevelID"] = lstLevel[1].Value;
            }
            IEnumerable<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).ToList();
            List<int> lstClassID = lstClass.Select(p => p.ClassProfileID).Distinct().ToList();
            IDictionary<string, object> dicSearchCalendar = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"lstClassID",lstClassID}
            };
            IQueryable<Calendar> iquery = CalendarBusiness.Search(dicSearchCalendar);
            List<string> lstAppliedDate = iquery.OrderByDescending(p => p.StartDate).ToList().Select(p => p.StartDate.Value.ToString("dd/MM/yyyy")).Distinct().ToList();
            string appliedDate = string.Empty;
            List<SelectListItem> lstItemAppliedDate = new List<SelectListItem>();
            SelectListItem objItem = null;
            for (int i = 0; i < lstAppliedDate.Count; i++)
            {
                appliedDate = lstAppliedDate[i];
                objItem = new SelectListItem();
                objItem.Value = appliedDate;
                objItem.Text = appliedDate;
                lstItemAppliedDate.Add(objItem);
            }
            return Json(lstItemAppliedDate);
        }

        public PartialViewResult LoadCreateCalendar(int ClassID)
        {
            ClassProfile selClass = null;
            if (ClassID > 0)
            {
                selClass = ClassProfileBusiness.All.Where(a => a.ClassProfileID == ClassID && a.IsActive.Value).FirstOrDefault();
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (selClass != null)
            {
                dic["EducationLevelID"] = selClass.EducationLevelID;
            }
            else
            {
                //if (lstLevel.Count > 0) dic["EducationLevelID"] = lstLevel[1].Value;
            }
            IEnumerable<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).ToList();
            List<int> lstClassID = lstClass.Select(p => p.ClassProfileID).Distinct().ToList();
            IDictionary<string, object> dicSearchCalendar = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"lstClassID",lstClassID}
            };
            IQueryable<Calendar> iquery = CalendarBusiness.Search(dicSearchCalendar);
            ViewData["MaxDate"] = iquery.Count() > 0 ? iquery.Max(p => p.StartDate).Value.ToString("dd/MM/yyyy") : "";
            return PartialView("_CreateDialog");
        }

        /// <summary>
        /// Lay danh sach buoi hoc theo lop
        /// </summary>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public JsonResult LoadSection(int? ClassID, int? EducationLevelID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            if (ClassID.HasValue)
            {
                dic["ClassProfileID"] = ClassID;
            }
            if (EducationLevelID.HasValue)
            {
                dic["EducationLevelID"] = EducationLevelID.Value;
            }
            IEnumerable<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).ToList();
            var lstSection = new List<CalendarBusiness.ClassSection>();
            foreach (ClassProfile cl in lstClass)
            {
                var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                lstSection = lstSection.Union(listSectionOther).ToList();

                if (lstSection.Count >= 3)
                    break;
            }

            //int? maxSection = lstClass.Max(a => a.Section);
            //CalendarBusiness.GetSection((maxSection.HasValue) ? maxSection.Value : 0, ref lstSection);


            return Json(new SelectList(lstSection.OrderBy(o => o.Section), "Section", "SectionName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult DeleteCalendar(SearchModel frm)
        {
            Dictionary<string, object> dicCalendar = new Dictionary<string, object>();
            dicCalendar["SchoolID"] = _globalInfo.SchoolID.Value;
            dicCalendar["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicCalendar["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dicCalendar["AppliedDate"] = Convert.ToDateTime(frm.AppliedDate);
            if (frm.Level.HasValue)
            {
                dicCalendar["EducationLevelID"] = frm.Level.Value;
            }
            if (frm.ClassID.HasValue)
            {
                dicCalendar["ClassID"] = frm.ClassID.Value;
            }
            if (frm.Section.HasValue)
            {
                dicCalendar["Section"] = frm.Section.Value;
            }
            if (frm.Semester.HasValue)
            {
                dicCalendar["Semester"] = frm.Semester.Value;
            }
            if (!string.IsNullOrEmpty(frm.TeacherName))
            {
                dicCalendar["AssignTeacherName"] = frm.TeacherName;
            }
            List<Calendar> lstCalendar = CalendarBusiness.Search(dicCalendar).ToList();
            CalendarBusiness.DeleteAll(lstCalendar);
            CalendarBusiness.Save();
            return Json(new JsonMessage("Xóa thành công", "success"));
        }
        /// <summary>
        /// Lay danh sach lop theo khoi
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int? EducationLevelID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (EducationLevelID.HasValue)
            {
                dic["EducationLevelID"] = EducationLevelID.Value;
            }
            IEnumerable<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).OrderBy(u => u.DisplayName).ToList();
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        #region tranferBO
        public List<SubjectCatBO> TranferSubjectBO(List<SubjectCat> lstSubjectCat, List<SubjectCatBO> lstSubjectCatBO) 
        {
            
            foreach (var item in lstSubjectCat) 
            { 
                SubjectCatBO objSubjectBO = new SubjectCatBO();
                objSubjectBO.SubjectCatID = item.SubjectCatID;
                objSubjectBO.strSubjectAndAssignCatID = item.SubjectCatID.ToString();
                objSubjectBO.SubjectName = item.SubjectName;
                objSubjectBO.OrderInSubject = item.OrderInSubject;
                objSubjectBO.Abbreviation = item.Abbreviation;
                objSubjectBO.AppliedLevel = item.AppliedLevel;
                objSubjectBO.ApprenticeshipGroupID = item.ApprenticeshipGroupID;
                objSubjectBO.AssignSubjectID = null;
                objSubjectBO.Code = item.Code;
                objSubjectBO.Color = item.Color;
                objSubjectBO.CreatedDate = item.CreatedDate;
                objSubjectBO.Description = item.Description;
                objSubjectBO.DisplayName = item.DisplayName;
                objSubjectBO.HasPractice = item.HasPractice;
                objSubjectBO.IsActive = item.IsActive;
                objSubjectBO.IsApprenticeshipSubject = item.IsApprenticeshipSubject;
                objSubjectBO.IsCommenting = item.IsCommenting;
                objSubjectBO.IsCoreSubject = item.IsCoreSubject;
                objSubjectBO.IsEditIsCommentting = item.IsEditIsCommentting;
                objSubjectBO.IsExemptible = item.IsExemptible;
                objSubjectBO.IsForeignLanguage = item.IsForeignLanguage;
                objSubjectBO.MiddleSemesterTest = item.MiddleSemesterTest;
                objSubjectBO.ModifiedDate = item.ModifiedDate;
                objSubjectBO.OrderInSubject = item.OrderInSubject;
                objSubjectBO.ReadAndWriteMiddleTest = item.ReadAndWriteMiddleTest;
                objSubjectBO.SynchronizeID = item.SynchronizeID;
                
                lstSubjectCatBO.Add(objSubjectBO);
            }
            return lstSubjectCatBO;
        }

        public SubjectCatBO TranferObjSubjectBO(SubjectCat item, SubjectCatBO objSubjectBO)
        {          
                objSubjectBO.SubjectCatID = item.SubjectCatID;
                objSubjectBO.strSubjectAndAssignCatID = item.SubjectCatID.ToString();
                objSubjectBO.SubjectName = item.SubjectName;
                objSubjectBO.OrderInSubject = item.OrderInSubject;
                objSubjectBO.Abbreviation = item.Abbreviation;
                objSubjectBO.AppliedLevel = item.AppliedLevel;
                objSubjectBO.ApprenticeshipGroupID = item.ApprenticeshipGroupID;
                objSubjectBO.AssignSubjectID = null;
                objSubjectBO.Code = item.Code;
                objSubjectBO.Color = item.Color;
                objSubjectBO.CreatedDate = item.CreatedDate;
                objSubjectBO.Description = item.Description;
                objSubjectBO.DisplayName = item.DisplayName;
                objSubjectBO.HasPractice = item.HasPractice;
                objSubjectBO.IsActive = item.IsActive;
                objSubjectBO.IsApprenticeshipSubject = item.IsApprenticeshipSubject;
                objSubjectBO.IsCommenting = item.IsCommenting;
                objSubjectBO.IsCoreSubject = item.IsCoreSubject;
                objSubjectBO.IsEditIsCommentting = item.IsEditIsCommentting;
                objSubjectBO.IsExemptible = item.IsExemptible;
                objSubjectBO.IsForeignLanguage = item.IsForeignLanguage;
                objSubjectBO.MiddleSemesterTest = item.MiddleSemesterTest;
                objSubjectBO.ModifiedDate = item.ModifiedDate;
                objSubjectBO.OrderInSubject = item.OrderInSubject;
                objSubjectBO.ReadAndWriteMiddleTest = item.ReadAndWriteMiddleTest;
                objSubjectBO.SynchronizeID = item.SynchronizeID;
                
                return objSubjectBO;
        }

        #endregion
        /// <summary>
        /// Tim kiem thong tin TKB
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public PartialViewResult Search(SearchModel frm)
        {
            int currentPage = 1;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            Dictionary<string, object> dicCalendar = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dicCalendar = dic;
            dicCalendar["AppliedDate"] = Convert.ToDateTime(frm.AppliedDate);
            if (frm.Level.HasValue)
            {
                dic["EducationLevelID"] = frm.Level.Value;
                dic["EducationLevel"] = frm.Level.Value;
                dicCalendar["EducationLevelID"] = frm.Level.Value;
            }
            if (frm.ClassID.HasValue)
            {
                dic["ClassProfileID"] = frm.ClassID.Value;
                dic["ClassID"] = frm.ClassID.Value;
                dicCalendar["ClassID"] = frm.ClassID.Value;
            }
            if (frm.Section.HasValue)
            {
                dicCalendar["Section"] = frm.Section.Value;
                dic["Section"] = frm.Section.Value;
            }
            if (frm.Semester.HasValue)
            {
                dicCalendar["Semester"] = frm.Semester.Value;
                dic["Semester"] = frm.Semester.Value;
            }
            if (!string.IsNullOrEmpty(frm.TeacherName))
            {
                dicCalendar["AssignTeacherName"] = frm.TeacherName;
                dic["AssignTeacherName"] = frm.TeacherName;
            }
            if (frm.CurrentPage.HasValue && frm.CurrentPage.Value > 1)
            {
                currentPage = frm.CurrentPage.Value;
            }
            List<CalendarBusiness.ClassSection> lstSection = new List<CalendarBusiness.ClassSection>();
            if (frm.Section.HasValue)
            {
                lstSection.Add(new CalendarBusiness.ClassSection(frm.Section.Value));
            }
            // Lấy về danh sách khôi
            List<SelectListItem> lstLevel = new List<SelectListItem>();
            foreach (var itm in _globalInfo.EducationLevels)
            {
                SelectListItem slItem = new SelectListItem();
                slItem.Text = itm.Resolution;
                slItem.Value = itm.EducationLevelID.ToString();
                lstLevel.Add(slItem);
            }
            ViewData["lstLevel"] = lstLevel;
            var queryClass = ClassProfileBusiness.Search(dic).OrderBy(a => a.DisplayName)
                .Select(o => new ClassProfileTempBO
                {
                    EducationLevelID = o.EducationLevelID,
                    ClassProfileID = o.ClassProfileID,
                    DisplayName = o.DisplayName,
                    EmployeeName = o.Employee != null ? o.Employee.FullName : string.Empty,
                    OrderNumber = o.OrderNumber,
                    Section = o.Section
                }).ToList();
            int totalClass = queryClass.Count();
            int totalPage = (totalClass + SchoolCalendarConstants.CLASS_PER_PAGE - 1) / SchoolCalendarConstants.CLASS_PER_PAGE;
            var lstClass = queryClass.Skip((currentPage - 1) * SchoolCalendarConstants.CLASS_PER_PAGE).Take(SchoolCalendarConstants.CLASS_PER_PAGE).ToList();
            List<CalendarModel> lstCalendar = CalendarBusiness.Search(dicCalendar)
                .Select(cal => new CalendarModel
                {
                    AcademicYearID = cal.AcademicYearID,
                    ClassID = cal.ClassID,
                    DayOfWeek = cal.DayOfWeek,
                    NumberOfPeriod = cal.NumberOfPeriod.Value,
                    Section = cal.Section,
                    Semester = frm.Semester.Value,
                    SubjectID = cal.SubjectID,
                    SubjectName = cal.SubjectCat.DisplayName,
                    SubjectColor = cal.SubjectCat.Color,
                    CalendarID = cal.CalendarID,
                    AssignSubjectID = cal.AssignmentSubjectID,
                }).ToList();
            List<CalendarModel> lstModel = null;
            
            // Lay danh sach mon hoc theo truong
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            
            
            // Danh sách phân môn
            List<AssignSubjectConfig> lstASC = AssignSubjectConfigBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value 
                                                                                        && x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                        ).ToList();

            List<AssignSubjectConfig> lstASCTemp = new List<AssignSubjectConfig>();
            //tao 2 mon mac dinh la chao co va sinh hoat lop
            //ADD MON CHAO CO VAO
            var Subject_Chao_Co = SubjectCatBusiness.Find(SMAS.Web.Constants.GlobalConstants.CHAO_CO_SUBJECT);
            SubjectCatBO Subject_Chao_Co_BO = new SubjectCatBO();
            TranferObjSubjectBO(Subject_Chao_Co, Subject_Chao_Co_BO);
            var Subject_Sinh_Hoat = SubjectCatBusiness.Find(SMAS.Web.Constants.GlobalConstants.SINH_HOAT_SUBJECT);
            SubjectCatBO Subject_Sinh_Hoat_BO = new SubjectCatBO();
            TranferObjSubjectBO(Subject_Sinh_Hoat, Subject_Sinh_Hoat_BO);
            // Danh sach phan cong giang day cua truong (co the lay theo khoi lop neu co chon tren giao dien)
            var lstTeacherAll = TeachingAssignmentBusiness.GetTeachingAssigment(_globalInfo.SchoolID.Value, dic).ToList();
            List<SubjectCatBO> lstAssignSubject = null;

            if (lstClass != null && lstClass.Count > 0)
            {
                lstModel = new List<CalendarModel>();
                foreach (var cls in lstClass)
                {
                    dic["ClassID"] = cls.ClassProfileID;
                    dic["SubjectID"] = null;
                    List<SubjectCat> listSubject = listClassSubject.Where(o => o.ClassID == cls.ClassProfileID).Select(a => a.SubjectCat).OrderBy(o => o.DisplayName).ToList();
                    List<SubjectCatBO> lstSubjecTCatBO = new List<SubjectCatBO>();
                    List<SubjectCatBO> lstSubjecTCatBOTemp = new List<SubjectCatBO>();
                    TranferSubjectBO(listSubject, lstSubjecTCatBO);
                    
                    if (Subject_Sinh_Hoat != null)
                    {
                        lstSubjecTCatBO.Insert(0, Subject_Sinh_Hoat_BO);
                        lstSubjecTCatBOTemp.Insert(0, Subject_Sinh_Hoat_BO);
                        if (Subject_Chao_Co != null)
                        {
                            lstSubjecTCatBO.Insert(0, Subject_Chao_Co_BO);
                            lstSubjecTCatBOTemp.Insert(0, Subject_Chao_Co_BO);
                        }
                    }
                    lstAssignSubject = new List<SubjectCatBO>();
                    // Lấy phân môn của môn học
                    foreach (var objSubject in listSubject)
                    {
                        SubjectCat objSubjectTemp = new SubjectCat();
                        SubjectCatBO objSubjectBOTemp = new SubjectCatBO();
                        objSubjectTemp = listSubject.Where(x => x.SubjectCatID == objSubject.SubjectCatID).ToList().FirstOrDefault();
                        string stringEducationLevel = cls.EducationLevelID.ToString();
                        lstASCTemp = lstASC.Where(x => x.SubjectID == objSubject.SubjectCatID && x.EducationApplied.Contains(stringEducationLevel)).ToList();
                        
                        TranferObjSubjectBO(objSubject, objSubjectBOTemp);
                        lstSubjecTCatBOTemp.Add(objSubjectBOTemp);
                        
                        foreach (var objLstASCTemp in lstASCTemp)
                        {
                            SubjectCatBO newSubject = new SubjectCatBO();

                            //Xét ID và Name của phân môn
                            newSubject.SubjectCatID = objLstASCTemp.SubjectID;
                            newSubject.strSubjectAndAssignCatID = objSubject.SubjectCatID.ToString() + "*" + objLstASCTemp.AssignSubjectConfigID.ToString();
                            newSubject.SubjectName = objLstASCTemp.AssignSubjectName;
                            newSubject.SubjectCatID = objLstASCTemp.SubjectID;
                            lstSubjecTCatBOTemp.Add(newSubject);
                            //lstAssignSubject.Add(newSubject);
                        }
                    }

                    //lstSubjecTCatBO.AddRange(lstAssignSubject);

                    ViewData["Subject" + cls.ClassProfileID] = lstSubjecTCatBOTemp;
                    List<CalendarModel> clsCalendar = lstCalendar.Where(a => a.ClassID == cls.ClassProfileID).ToList();
                    if (clsCalendar != null && clsCalendar.Count > 0)
                    {
                        foreach (CalendarModel cal in clsCalendar)
                        {
                            var lstTeacher = lstTeacherAll.Where(o => o.ClassID == cls.ClassProfileID && o.SubjectID == cal.SubjectID).ToList();

                            if (cal.SubjectID != Constants.GlobalConstants.SINH_HOAT_SUBJECT && cal.SubjectID != Constants.GlobalConstants.CHAO_CO_SUBJECT)
                                cal.TeacherNames = lstTeacher.Select(a => a.TeacherName).ToList();
                            else if (cal.SubjectID == Constants.GlobalConstants.SINH_HOAT_SUBJECT)
                            {
                                //voi mon sinh hoat lay ten giao vien chu nhiem
                                cal.TeacherNames = !string.IsNullOrWhiteSpace(cls.EmployeeName) ? new List<string> { { cls.EmployeeName } } : new List<string>();
                            }
                            else
                            {
                                cal.TeacherNames = new List<string>();
                            }

                            if (cal.AssignSubjectID != null)
                                cal.AssignSubjectName = lstASC.Find(x => x.AssignSubjectConfigID == cal.AssignSubjectID).AssignSubjectName;

                            lstModel.Add(cal);
                        }
                    }
                }
                if (lstSection.Count == 0)
                {
                    foreach (var cl in lstClass)
                    {
                        var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                        CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                        var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                        lstSection = lstSection.Union(listSectionOther).ToList();

                        if (lstSection.Count >= 3)
                            break;
                    }

                    ////Neu ko tim theo buoi thi load het cac buoi trong danh sach lop hoc
                    //int? maxSection = lstClass.Max(a => a.Section);
                    //CalendarBusiness.GetSection((maxSection.HasValue) ? maxSection.Value : 0, ref lstSection);
                }
                lstSection = (lstSection != null) ? lstSection.OrderBy(a => a.Section).ToList() : new List<CalendarBusiness.ClassSection>();
            }
            //Add view data here
            ViewData["lstSectionSearch"] = lstSection;
            ViewData["lstClassSearch"] = lstClass;
            ViewData["Semester"] = frm.Semester;
            ViewData["TotalPage"] = totalPage;
            ViewData["CurrentPage"] = currentPage;
            //Get view data here
            return PartialView("_List", lstModel);
        }

        /// <summary>
        /// Luu thong tin diem danh cua lop
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [SMAS.Web.Filter.ActionAudit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveData(List<UpdateCalendar> model,string AppliedDate)
        {
            if (string.IsNullOrEmpty(AppliedDate))
            {
                AppliedDate = DateTime.Now.ToString("dd/MM/yyyy");
            }
            DateTime DateApplied = Convert.ToDateTime(AppliedDate);
            var updateCounter = 0;
            model = new JavaScriptSerializer().Deserialize<List<UpdateCalendar>>(Request["model"]);
            Calendar calendar = null;
            foreach (UpdateCalendar cal in model)
            {
                bool updated = false;
                if (cal.CalendarID != 0)
                {
                    calendar = CalendarBusiness.All.Where(a => a.CalendarID == cal.CalendarID).FirstOrDefault();
                    if (calendar != null)
                    {
                        if (cal.stringSubjectAndAssignID == "")
                        {
                            CalendarBusiness.Delete(calendar.CalendarID);
                        }
                        else
                        {
                            //Update Current Calendar
                            calendar.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            calendar.ClassID = cal.ClassID;
                            calendar.DayOfWeek = (byte)cal.DayOfWeek;
                            calendar.NumberOfPeriod = (byte)cal.NumberOfPeriod;
                            calendar.SubjectOrder = (byte)cal.NumberOfPeriod;
                            calendar.Section = (byte)cal.Section;
                            calendar.IsActive = true;
                            int tryParse;
                            bool isAssign = Int32.TryParse(cal.stringSubjectAndAssignID, out tryParse);
                            if (isAssign)
                            {
                                calendar.SubjectID = Int16.Parse(cal.stringSubjectAndAssignID);
                                calendar.AssignmentSubjectID = null;
                            }
                            else
                            {
                                string[] stringSubject = cal.stringSubjectAndAssignID.Split('*');
                                calendar.SubjectID = Int16.Parse(stringSubject[0]);
                                calendar.AssignmentSubjectID = Int16.Parse(stringSubject[1]);
                            }
                            CalendarBusiness.Update(calendar);
                        }
                        updated = true;
                        updateCounter++;
                    }
                }
                if (!updated)
                {
                    //Them mới calendar
                    if (cal.stringSubjectAndAssignID == "") continue;
                    calendar = new Calendar();
                    calendar.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    calendar.ClassID = cal.ClassID;
                    calendar.DayOfWeek = (byte)cal.DayOfWeek;
                    calendar.NumberOfPeriod = (byte)cal.NumberOfPeriod;
                    calendar.SubjectOrder = (byte)cal.NumberOfPeriod;
                    calendar.Section = (byte)cal.Section;
                    int tryParse;
                    bool isAssign = Int32.TryParse(cal.stringSubjectAndAssignID, out tryParse);
                    if (isAssign)
                    {
                        calendar.SubjectID = Int16.Parse(cal.stringSubjectAndAssignID);
                        calendar.AssignmentSubjectID = null;
                    }
                    else
                    {
                        string[] stringSubject = cal.stringSubjectAndAssignID.Split('*');
                        calendar.SubjectID = Int16.Parse(stringSubject[0]);
                        calendar.AssignmentSubjectID = Int16.Parse(stringSubject[1]);
                    }                    
                    calendar.IsActive = true;
                    calendar.StartDate = DateApplied;
                    calendar.Semester = (byte)((cal.Semester.HasValue) ? cal.Semester.Value : _globalInfo.Semester.Value);
                    CalendarBusiness.Insert(calendar);
                    updateCounter++;
                }
            }
            if (updateCounter > 0)
            {
                //Save calendar data
                CalendarBusiness.Save();
            }
            //SetViewDataActionAudit(string.Empty, string.Empty, string.Empty, "Cập nhật thông tin thời khóa biểu.");
            return Json(new JsonMessage(Res.Get("Calendar_Update_OK")));
        }

        /// <summary>
        /// xuat template file option 1
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        public FileResult CreateExcelTemplate(int? EducationLevelID, int? Semester)
        {
            GlobalInfo global = new GlobalInfo();
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/HT" + "/" + SchoolCalendarConstants.TEMPLATE_NAME;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTemplateDropdownList = oBook.GetSheet(2);
            IVTWorksheet sheetTemplateEmpty = oBook.GetSheet(3);
            //IVTWorksheet sheetTemplateData = oBook.GetSheet(4);
            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = EducationLevelID;
            dicClass["AcademicYearID"] = global.AcademicYearID;
            dicClass["AppliedLevel"] = global.AppliedLevel;
            IEnumerable<ClassProfile> listClassProfle = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();

            IVTRange templateRange;
            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = EducationLevelID;
            dic["AcademicYearID"] = global.AcademicYearID;
            dic["Semester"] = Semester;
            dic["SubjectID"] = null;
            List<ClassSubject> listSubjectOfClass = ClassSubjectBusiness.SearchBySchool(global.SchoolID.Value, dic).ToList();


            // danh sách phân môn
            List<AssignSubjectConfig> lstASC = AssignSubjectConfigBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                                                                                        && x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                        ).ToList();
            List<AssignSubjectConfig> lstASCTemp = null;

            var Subject_Chao_Co = SubjectCatBusiness.Find(SMAS.Web.Constants.GlobalConstants.CHAO_CO_SUBJECT);
            SubjectCatBO Subject_Chao_Co_BO = new SubjectCatBO();
            TranferObjSubjectBO(Subject_Chao_Co, Subject_Chao_Co_BO);
            var Subject_Sinh_Hoat = SubjectCatBusiness.Find(SMAS.Web.Constants.GlobalConstants.SINH_HOAT_SUBJECT);
            SubjectCatBO Subject_Sinh_Hoat_BO = new SubjectCatBO();
            TranferObjSubjectBO(Subject_Sinh_Hoat, Subject_Sinh_Hoat_BO);
            int clIndex = 1;
            foreach (ClassProfile classProfile in listClassProfle)
            {
                List<object> lsSS = new List<object>();

                //var iqSSN = listSubject.Where(u => u.ClassID == classProfile.ClassProfileID).Select(o => o.SubjectCat.DisplayName).Distinct().ToList();
                List<SubjectCat> listSubject = listSubjectOfClass.Where(o => o.ClassID == classProfile.ClassProfileID).Select(a => a.SubjectCat).OrderBy(o => o.DisplayName).ToList();
                List<SubjectCatBO> lstSubjecTCatBO = new List<SubjectCatBO>();
                TranferSubjectBO(listSubject, lstSubjecTCatBO);
                List<string> iqSSN = lstSubjecTCatBO.Where(u => u.ClassID == classProfile.ClassProfileID).Select(o => o.DisplayName).Distinct().ToList();
                //tao 2 mon mac dinh la chao co va sinh hoat lop
                //ADD MON CHAO CO VAO
                
                if (Subject_Sinh_Hoat_BO != null)
                {
                    lstSubjecTCatBO.Insert(0, Subject_Chao_Co_BO);
                    if (Subject_Chao_Co_BO != null)
                        lstSubjecTCatBO.Insert(0, Subject_Sinh_Hoat_BO);
                }

                foreach (var item in lstSubjecTCatBO)
                {
                    lsSS.Add(new { SubjectName = item.DisplayName });

                    lstASCTemp = new List<AssignSubjectConfig>();
                    string stringEducationLevel = classProfile.EducationLevelID.ToString();
                    lstASCTemp = lstASC.Where(x => x.SubjectID == item.SubjectCatID && x.EducationApplied.Contains(stringEducationLevel)).ToList();
                    foreach (var objLstASCTemp in lstASCTemp)
                    {
                        lsSS.Add(new { SubjectName = objLstASCTemp.AssignSubjectName });
                    }
                }
                KingDic.Add("ClassSubject" + clIndex.ToString(), lsSS);
                clIndex++;
            }
            SchoolProfile SchoolProfile = SchoolProfileBusiness.Find(global.SchoolID.Value);
            SupervisingDept SupervisingDept = SupervisingDeptBusiness.Find(SchoolProfile.SupervisingDeptID);

            sheet.SetCellValue("A1", SupervisingDept.SupervisingDeptName);
            sheet.SetCellValue("A2", SchoolProfile.SchoolName);
            AcademicYear acad = AcademicYearBusiness.Find(global.AcademicYearID);
            string semesterName = "";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                semesterName = "HỌC KỲ I";
            }
            else
            {
                semesterName = "HỌC KỲ II";
            }
            string TitleFile = "NĂM HỌC " + acad.Year + " - " + (acad.Year + 1).ToString() + " " + semesterName + " KHỐI " + EducationLevelID;
            sheet.SetCellValue("F4", TitleFile);

            IVTRange templateRangeForDrop = sheetTemplateDropdownList.GetRange("A1", "AR65");

            templateRangeForDrop.FillVariableValue(KingDic);


            //lay danh sac buoi cua hoc ky
            List<CalendarBusiness.ClassSection> lstSection = new List<CalendarBusiness.ClassSection>();
            if (lstSection.Count == 0)
            {
                foreach (ClassProfile cl in listClassProfle)
                {
                    var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                    CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                    var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                    lstSection = lstSection.Union(listSectionOther).ToList();

                    if (lstSection.Count >= 3)
                        break;
                }
            }
            lstSection = (lstSection != null) ? lstSection.OrderBy(a => a.Section).ToList() : new List<CalendarBusiness.ClassSection>();         

            //Xoa cac dong ngang
            int startRow = 6;
            int endRow = 113;
            int oldValue = 1;
            int newValue = 1;
            int startMerge = startRow + 1;
            int numberStartClass = 5;
            int maxnumberClass = 43;
            int lengthClass = listClassProfle.Count();
            IVTRange rangeUnlock = sheet.GetRange(startRow + 1, numberStartClass, endRow, numberStartClass + lengthClass - 1);
            rangeUnlock.IsLock = false;
            for (int row = startRow; row < endRow; row++)
            {
                string typeRow = sheet.GetCellValue(row, 1) != null ? sheet.GetCellValue(row, 1).ToString() : "";
                int startCl = 5;
                foreach (ClassProfile cl in listClassProfle)
                {
                    var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                    CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                    if (typeRow != "" && !lstSectionFor.Any(u => u.Section.ToString().Equals(typeRow)))
                    {
                        IVTRange rangeLock = sheet.GetRange(row, startCl, row, startCl);
                        rangeLock.IsLock = true;
                        sheet.GetRange(row, startCl, row, startCl).IsLock = true;
                    }
                    startCl++;
                }

                if (typeRow != "" && !lstSection.Select(o => o.Section).Any(u => u.ToString() == typeRow))
                {
                    sheet.DeleteRow(row);
                    row--;
                }
            }
            oldValue = 2;
            for (int row = startRow + 1; row < endRow; row++)
            {
                string typeRow = sheet.GetCellValue(row, 2) != null ? sheet.GetCellValue(row, 2).ToString().ToUpper().Trim() == "CHỦ NHẬT" ? "9" : sheet.GetCellValue(row, 2).ToString() : "8";
                if (typeRow != "")
                {
                    newValue = int.Parse(typeRow);
                    if (newValue != oldValue)
                    {
                        IVTRange rangeMerge = sheet.GetRange(startMerge, 2, row - 1, 2);
                        if (lstSection.Count > 0)
                            rangeMerge.Merge();
                        startMerge = row;
                    }
                    if (newValue == 8)
                    {
                        sheet.SetCellValue(row, 2, "");
                    }
                    oldValue = newValue;
                }

                sheet.SetCellValue(row, 1, "");
            }

            
            IVTRange rangeClass = sheetTemplateDropdownList.GetRange(startRow, numberStartClass + lengthClass, endRow, numberStartClass + maxnumberClass);

            sheet.CopyPaste(rangeClass, startRow, numberStartClass + lengthClass);
            
            sheet.ProtectSheet("");
            int classIndex = numberStartClass;
            //fill lop 
            foreach (ClassProfile cl in listClassProfle)
            {
                sheet.SetCellValue(startRow, classIndex, cl.DisplayName);
                classIndex++;
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "ThoiKhoaBieu.xls";
            return result;
        }

        /// <summary>
        /// xuat template file option 2
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        /// 



        public FileResult CreateExcelTemplateOp2(int? EducationLevelID, int? Semester, int? ClassID, int? Section, string AppliedDate, string TeacherName)
        {
            GlobalInfo global = new GlobalInfo();
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/HT" + "/" + SchoolCalendarConstants.TEMPLATE_NAME;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTemplateDropdownList = oBook.GetSheet(2);
            IVTWorksheet sheetTemplateEmpty = oBook.GetSheet(3);
            //IVTWorksheet sheetTemplateData = oBook.GetSheet(4);
            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = EducationLevelID;
            dicClass["AcademicYearID"] = global.AcademicYearID;
            dicClass["AppliedLevel"] = global.AppliedLevel;
            dicClass["EducationLevel"] = EducationLevelID;
            dicClass["SchoolID"] = _globalInfo.SchoolID;

            if (EducationLevelID != 0)
            {
                dicClass["EducationLevelID"] = EducationLevelID;
                dicClass["EducationLevel"] = EducationLevelID;
            }
            if (ClassID != 0)
            {
                dicClass["ClassProfileID"] = ClassID;
                dicClass["ClassID"] = ClassID;
            }
            if (Section != 0)
            {
                dicClass["Section"] = Section;
            }
            if (Semester != 0)
            {
                dicClass["Semester"] = Semester;
            }
            if (!string.IsNullOrEmpty(TeacherName))
            {
                dicClass["AssignTeacherName"] = TeacherName;
            }

            List<ClassProfile> listClassProfle = ClassProfileBusiness.Search(dicClass).OrderBy(a => a.DisplayName).ToList();
            List<Calendar> lstCalendar = CalendarBusiness.Search(dicClass).ToList();
            IEnumerable<TeachingAssignmentBO> listTeachingAssignment = TeachingAssignmentBusiness.GetTeachingAssigment(_globalInfo.SchoolID.Value, dicClass).ToList();
            //IEnumerable<ClassProfile> listClassProfle = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();

            IVTRange templateRange;
            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = EducationLevelID;
            dic["AcademicYearID"] = global.AcademicYearID;
            dic["Semester"] = Semester;
            dic["SubjectID"] = null;
            List<ClassSubject> listSubjectOfClass = ClassSubjectBusiness.SearchBySchool(global.SchoolID.Value, dic).ToList();


            // danh sách phân môn
            List<AssignSubjectConfig> lstASC = AssignSubjectConfigBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                                                                                        && x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                        ).ToList();
            List<AssignSubjectConfig> lstASCTemp = null;

            #region fill dữ liệu vào combobox và ghép vào sheet1
            var Subject_Chao_Co = SubjectCatBusiness.Find(SMAS.Web.Constants.GlobalConstants.CHAO_CO_SUBJECT);
            SubjectCatBO Subject_Chao_Co_BO = new SubjectCatBO();
            TranferObjSubjectBO(Subject_Chao_Co, Subject_Chao_Co_BO);
            var Subject_Sinh_Hoat = SubjectCatBusiness.Find(SMAS.Web.Constants.GlobalConstants.SINH_HOAT_SUBJECT);
            SubjectCatBO Subject_Sinh_Hoat_BO = new SubjectCatBO();
            TranferObjSubjectBO(Subject_Sinh_Hoat, Subject_Sinh_Hoat_BO);
            int clIndex = 1;
            foreach (ClassProfile classProfile in listClassProfle)
            {
                List<object> lsSS = new List<object>();

                //var iqSSN = listSubject.Where(u => u.ClassID == classProfile.ClassProfileID).Select(o => o.SubjectCat.DisplayName).Distinct().ToList();
                List<SubjectCat> listSubject = listSubjectOfClass.Where(o => o.ClassID == classProfile.ClassProfileID).Select(a => a.SubjectCat).OrderBy(o => o.DisplayName).ToList();
                List<SubjectCatBO> lstSubjecTCatBO = new List<SubjectCatBO>();
                TranferSubjectBO(listSubject, lstSubjecTCatBO);
                List<string> iqSSN = lstSubjecTCatBO.Where(u => u.ClassID == classProfile.ClassProfileID).Select(o => o.DisplayName).Distinct().ToList();
                //tao 2 mon mac dinh la chao co va sinh hoat lop
                //ADD MON CHAO CO VAO

                if (Subject_Sinh_Hoat_BO != null)
                {
                    lstSubjecTCatBO.Insert(0, Subject_Chao_Co_BO);
                    if (Subject_Chao_Co_BO != null)
                        lstSubjecTCatBO.Insert(0, Subject_Sinh_Hoat_BO);
                }

                foreach (var item in lstSubjecTCatBO)
                {
                    lsSS.Add(new { SubjectName = item.DisplayName });

                    lstASCTemp = new List<AssignSubjectConfig>();
                    string stringEducationLevel = classProfile.EducationLevelID.ToString();
                    lstASCTemp = lstASC.Where(x => x.SubjectID == item.SubjectCatID && x.EducationApplied.Contains(stringEducationLevel)).ToList();
                    foreach (var objLstASCTemp in lstASCTemp)
                    {
                        lsSS.Add(new { SubjectName = objLstASCTemp.AssignSubjectName });
                    }
                }
                KingDic.Add("ClassSubject" + clIndex.ToString(), lsSS);
                clIndex++;
            }
            SchoolProfile SchoolProfile = SchoolProfileBusiness.Find(global.SchoolID.Value);
            SupervisingDept SupervisingDept = SupervisingDeptBusiness.Find(SchoolProfile.SupervisingDeptID);

            sheet.SetCellValue("A1", SupervisingDept.SupervisingDeptName);
            sheet.SetCellValue("A2", SchoolProfile.SchoolName);
            AcademicYear acad = AcademicYearBusiness.Find(global.AcademicYearID);
            string semesterName = "";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                semesterName = "HỌC KỲ I";
            }
            else
            {
                semesterName = "HỌC KỲ II";
            }
            string TitleFile = "NĂM HỌC " + acad.Year + " - " + (acad.Year + 1).ToString() + " " + semesterName + " KHỐI " + EducationLevelID;
            sheet.SetCellValue("F4", TitleFile);

            IVTRange templateRangeForDrop = sheetTemplateDropdownList.GetRange("A1", "AR65");

            templateRangeForDrop.FillVariableValue(KingDic);


            //lay danh sac buoi cua hoc ky
            List<CalendarBusiness.ClassSection> lstSection = new List<CalendarBusiness.ClassSection>();
            if (lstSection.Count == 0)
            {
                foreach (ClassProfile cl in listClassProfle)
                {
                    var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                    CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                    var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                    lstSection = lstSection.Union(listSectionOther).ToList();

                    if (lstSection.Count >= 3)
                        break;
                }
            }
            lstSection = (lstSection != null) ? lstSection.OrderBy(a => a.Section).ToList() : new List<CalendarBusiness.ClassSection>();

            //Xoa cac dong ngang
            int startRow = 6;
            int endRow = 113;
            int oldValue = 1;
            int newValue = 1;
            int startMerge = startRow + 1;
            int numberStartClass = 5;
            int maxnumberClass = 43;
            int lengthClass = listClassProfle.Count();
            IVTRange rangeUnlock = sheet.GetRange(startRow + 1, numberStartClass, endRow, numberStartClass + lengthClass - 1);
            rangeUnlock.IsLock = false;
            for (int row = startRow; row < endRow; row++)
            {
                string typeRow = sheet.GetCellValue(row, 1) != null ? sheet.GetCellValue(row, 1).ToString() : "";
                int startCl = 5;
                foreach (ClassProfile cl in listClassProfle)
                {
                    var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                    CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                    if (typeRow != "" && !lstSectionFor.Any(u => u.Section.ToString().Equals(typeRow)))
                    {
                        IVTRange rangeLock = sheet.GetRange(row, startCl, row, startCl);
                        rangeLock.IsLock = true;
                        sheet.GetRange(row, startCl, row, startCl).IsLock = true;
                    }
                    startCl++;
                }

                if (typeRow != "" && !lstSection.Select(o => o.Section).Any(u => u.ToString() == typeRow))
                {
                    sheet.DeleteRow(row);
                    row--;
                }
            }
            oldValue = 2;
            for (int row = startRow + 1; row < endRow; row++)
            {
                string typeRow = sheet.GetCellValue(row, 2) != null ? sheet.GetCellValue(row, 2).ToString().ToUpper().Trim() == "CHỦ NHẬT" ? "9" : sheet.GetCellValue(row, 2).ToString() : "8";
                if (typeRow != "")
                {
                    newValue = int.Parse(typeRow);
                    if (newValue != oldValue)
                    {
                        IVTRange rangeMerge = sheet.GetRange(startMerge, 2, row - 1, 2);
                        if (lstSection.Count > 0)
                            rangeMerge.Merge();
                        startMerge = row;
                    }
                    if (newValue == 8)
                    {
                        sheet.SetCellValue(row, 2, "");
                    }
                    oldValue = newValue;
                }

                sheet.SetCellValue(row, 1, "");
            }


            IVTRange rangeClass = sheetTemplateDropdownList.GetRange(startRow, numberStartClass + lengthClass, endRow, numberStartClass + maxnumberClass);

            sheet.CopyPaste(rangeClass, startRow, numberStartClass + lengthClass);


            //sheet.SetPrintArea("A" + (startRow - 5) + ":" + "");



            sheet.ProtectSheet("");
            int classIndex = numberStartClass;
            //fill lop 
            foreach (ClassProfile cl in listClassProfle)
            {
                sheet.SetCellValue(startRow, classIndex, cl.DisplayName);
                classIndex++;
            }
            #endregion

            DateTime? dateApplied = null;
            if (AppliedDate != "")
            {
                dateApplied = Convert.ToDateTime(AppliedDate);
            }

            #region fill dữ liệu vào file import
                     
            // Danh sách phân môn
            List<CalendarBusiness.ClassSection> lstSection1 = new List<CalendarBusiness.ClassSection>();

            foreach (ClassProfile cl in listClassProfle)
            {
                var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                var listSectionOther = lstSectionFor.Where(o => !lstSection1.Select(u => u.Section).Contains(o.Section));
                lstSection1 = lstSection1.Union(listSectionOther).ToList();

                if (lstSection1.Count >= 3)
                    break;
            }

            if (Section != 0)
            {
                lstSection1 = lstSection1.Where(u => u.Section == Section.Value).ToList();
            }

            int startRow1 = 6;
            int startCol = 5;
            int tmpRow, tmpCol;
            int sectionCount = lstSection1.Count;

            Dictionary<string, int> listWidth = new Dictionary<string, int>();

            //Duyet qua tung lop de fill TKB
            foreach (var cls in listClassProfle)
            {
                tmpRow = startRow1;
                tmpCol = startCol;

                //Fill calendar
                List<Calendar> classCalendar = lstCalendar.Where(a => a.ClassID == cls.ClassProfileID).ToList();
                if (dateApplied != null)
                    classCalendar = classCalendar.Where(x => x.StartDate == dateApplied).ToList();

                if (classCalendar != null)
                {
                    foreach (Calendar cal in classCalendar)
                    {
                        if (cal.Section.HasValue && cal.NumberOfPeriod.HasValue)
                        {
                            List<TeachingAssignmentBO> listTAClass = listTeachingAssignment.Where(u => u.ClassID == cls.ClassProfileID && u.SubjectID == cal.SubjectID).ToList();

                            StringBuilder strFm = new StringBuilder();
                            int col = startCol;
                            int sectionRow = CalendarBusiness.getExcelRow(cal.Section.Value, lstSection1);
                            //Tinh ra ROW chua mon nay ung voi tiet, thu, buoi
                            // (Thu - 2) x 5 x tong + (buoi - 1) x 5 + tiet + startRow
                            int row = (cal.DayOfWeek - 2) * 5 * lstSection1.Count + (sectionRow - 1) * 5 + cal.NumberOfPeriod.Value + startRow1;

                            string stringSubjectName = cal.SubjectCat.SubjectName;
                            if (cal.AssignmentSubjectID != null)
                                stringSubjectName = lstASC.Find(x => x.AssignSubjectConfigID == cal.AssignmentSubjectID).AssignSubjectName;

                            sheet.SetFormulaValue(row, col, stringSubjectName);
                            //sheet1.SetCellValue(row, col, cal.SubjectCat.SubjectName);
                        }
                    }
                }

                //Tăng column
                startCol++;
            }

            #endregion

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "ThoiKhoaBieu.xls";
            return result;
        }


        /// <summary>
        /// Export to client
        /// </summary>
        /// <returns></returns>
        [SMAS.Web.Filter.ActionAudit]
        public FileResult ExportFile()
        {
            try
            {

                DateTime? dateApplied = null;
                if (Request["AppliedDate"].ToString() != "0")
                {
                    dateApplied = Convert.ToDateTime(Request["AppliedDate"].ToString());
                }
                                                          
                int? semester = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
                int? level = SMAS.Business.Common.Utils.GetInt(Request["level"]);
                int? classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]);
                int? section = SMAS.Business.Common.Utils.GetInt(Request["section"]);
                
                string teacherName = Request["teacherName"];
                semester = (semester == 0) ? null : semester;
                level = (level == 0) ? null : level;
                classID = (classID == 0) ? null : classID;
                section = (section == 0) ? null : section;

                Stream excel = CalendarBusiness.ExportExcel(semester, level, classID, section, teacherName, dateApplied, _globalInfo.SchoolID, _globalInfo.AcademicYearID, level, _globalInfo.AppliedLevel);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = "ThoiKhoaBieu.xls";
                return result;
            }
            catch
            {
                //Do nothing
            }
            return null;
        }

        #region Ham ImportExcel
        
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int? semesterid, int? EducationLevelID, string DateApplied,int TypeID)
        {
            GlobalInfo global = new GlobalInfo();
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            //The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();
            string Error = "";

            if (file != null)
            {
                DateTime StartDate = DateTime.Now;
                if (TypeID == 2)//cap nhat
                {
                    if (!string.IsNullOrEmpty(DateApplied) || DateApplied != "Null")
                    {
                        StartDate = Convert.ToDateTime(DateApplied);
                    }

                    IDictionary<string, object> dicSearchCalendar = new Dictionary<string, object>()
                    {
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"AppliedDate",StartDate}
                    };
                    IQueryable<Calendar> iquery = CalendarBusiness.Search(dicSearchCalendar);
                    if (iquery.Count() == 0)
                    {
                        return Json(new JsonMessage("Ngày áp dụng không tồn tại", "AppliedFail"));
                    }
                }
                else
                {
                    StartDate = Convert.ToDateTime(DateApplied);
                    IDictionary<string, object> dicSearchCalendar = new Dictionary<string, object>()
                    {
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"AppliedDate",StartDate}
                    };
                    IQueryable<Calendar> iquery = CalendarBusiness.Search(dicSearchCalendar);
                    if (iquery.Count() > 0)
                    {
                        return Json(new JsonMessage("Ngày áp dụng đã tồn tại", "AppliedFail"));
                    }
                }
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 1024)
                {
                    throw new BusinessException("JudgeRecord_Validate_FileMaxSize");
                }
                //luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                List<Calendar> list = getDataToFileImport(physicalPath, semesterid, EducationLevelID, StartDate, out Error);
                if (Error.Trim().Length == 0)
                {
                    ImportDataSuccess(list, EducationLevelID.HasValue ? EducationLevelID.Value : 0, semesterid.Value,StartDate);
                    return Json(new JsonMessage("Import dữ liệu thành công","success"));

                }

            }
            return Json(new JsonMessage(Error, "error"));

        }
        #endregion

        public void ImportDataSuccess(List<Calendar> list, int EducationLevelID, int Semester,DateTime StartDate)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicCalendar = new Dictionary<string, object>();
            dicCalendar["Semester"] = Semester;
            dicCalendar["EducationLevelID"] = EducationLevelID;
            dicCalendar["SchoolID"] = glo.SchoolID.Value;
            dicCalendar["AcademicYearID"] = glo.AcademicYearID;
            dicCalendar["AppliedDate"] = StartDate;
            CalendarBusiness.InsertCalendar(list, dicCalendar);
            //Calendar objInsert = null;
            //for (int i = 0; i < list.Count; i++)
                //{
            //    objInsert = list[i];
            //    CalendarBusiness.Insert(objInsert);
                //}
            //CalendarBusiness.Save();
        }

        public List<Calendar> getDataToFileImport(string filename, int? semesterid, int? EducationLevelID,DateTime StartDate, out string Error)
        {
            GlobalInfo glo = new GlobalInfo();
            int SchoolID = glo.SchoolID.Value;
            int AcademicYearID = glo.AcademicYearID.Value;
            int AppliedLevel = glo.AppliedLevel.Value;

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

            string FilePath = filename;
            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            IVTWorksheet sheet = null;
            try
            {
                sheet = oBook.GetSheet(1);
            }
            catch
            {
                Error = "Không phải là file Import Thời khóa biểu";
                return null;
            }



            Error = "";

            var content = sheet.GetCellValue("F3");
            if (content != null)
            {
                if (!content.ToString().Equals("THỜI KHÓA BIỂU"))
                {
                    Error = "Không phải là file Import Thời khóa biểu";
                    return null;
                }
            }
            else
            {
                Error = "Không phải là file Import Thời khóa biểu";
                return null;
            }

            #region Kiem tra du lieu chon dau vao
            string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
            string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
            string SemesterStr = semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "HỌC KỲ I " : "HỌC KỲ II";
            string Title = (string)sheet.GetCellValue("F4");
            string EducaitonName = EducationLevelID.HasValue ? ("KHỐI " + EducationLevelID.ToString()) : "";


            if (!Title.Contains(SemesterStr))
            {
                if (Error == "")
                {
                    //Error = "- " + Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterStr;
                    Error = "- " + string.Format(Res.Get("SchoolCalendar_InvalidImportFile_InvalidSemester")
                        ,semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II");
                }
                else
                {
                    Error = Error + "</br>" + "- " + string.Format(Res.Get("SchoolCalendar_InvalidImportFile_InvalidSemester")
                        , semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II");
                }
            }
            if (!(Title.Contains(AcademicYearName) || Title.Contains(_AcademicYearName)))
            {
                if (Error == "")
                    Error = "- " + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
            }
            if (!Title.Contains(EducaitonName))
            {
                if (Error == "")
                    Error = "- " + Res.Get("SchoolCalendar_Label_ErrorEducationLevel");
                else Error = Error + "</br>" + "- " + Res.Get("SchoolCalendar_Label_ErrorEducationLevel");
            }
            #endregion

            //phan kiem tra du lieu chon ban dau va thong tin tren excel
            int startColMark = 5;
            int startRow = 7;


            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = glo.AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;

            IEnumerable<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, dic).ToList();

            // Danh sách phân môn
            List<AssignSubjectConfig> lstASC = AssignSubjectConfigBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                                                                                        && x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                        ).ToList();

            List<CalendarBusiness.ClassSection> lstSection = new List<CalendarBusiness.ClassSection>();
            if (lstSection.Count == 0)
            {
                foreach (ClassProfile cl in lstClassProfile)
                {
                    var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                    CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                    var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                    lstSection = lstSection.Union(listSectionOther).ToList();

                    if (lstSection.Count >= 3)
                        break;
                }
            }
            int cntClass = lstClassProfile.Count();
            List<ClassProfile> lstClass = new List<ClassProfile>();
            for (int i = startColMark; i < cntClass + startColMark; i++)
            {
                var lstClassHas = lstClassProfile.ToList();
                var className = sheet.GetCellValue(startRow - 1, i);
                if (className != null)
                {
                    var ClassSearch = lstClassHas.Where(u => u.DisplayName.Equals(className.ToString()));
                    if (ClassSearch == null)
                    {
                        if (Error == "")
                            Error = "- " + "Lớp" + className.ToString() + " không hợp lệ";
                        else Error = Error + "</br>" + "Lớp" + className.ToString() + " không hợp lệ";
                        return null;
                    }
                    else
                    {
                        if (ClassSearch.FirstOrDefault() != null)
                        {
                            lstClass.Add(ClassSearch.FirstOrDefault());
                        }
                        lstClassHas.Remove(ClassSearch.FirstOrDefault());
                    }
                }
            }
            //co loi thoat ra ngoai dua ra thong bao loi
            if (Error != "")
            {
                return null;
            }
            dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = EducationLevelID;
            dic["Semester"] = semesterid;
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dic).ToList();
            var Subject_Chao_Co = SubjectCatBusiness.Find(SMAS.Web.Constants.GlobalConstants.CHAO_CO_SUBJECT);
            var Subject_Sinh_Hoat = SubjectCatBusiness.Find(SMAS.Web.Constants.GlobalConstants.SINH_HOAT_SUBJECT);
            List<Calendar> listCalendar = new List<Calendar>();
            int thu, buoi, tiet;
            object thuStr, buoiStr, tietStr;
            thuStr = buoiStr = new object();
            // kiem tra tinh hop le danh sach lop
            //kiem tra cac cot trong bang csdl  xem da chuan chua
            while (sheet.GetCellValue(startRow, 4) != null)
            {
                //lay lan luot cac gia tri
                if (sheet.GetCellValue(startRow, 2) != null)
                {
                    thuStr = sheet.GetCellValue(startRow, 2);
                }
                if (sheet.GetCellValue(startRow, 3) != null)
                {
                    buoiStr = sheet.GetCellValue(startRow, 3);
                }
                tietStr = sheet.GetCellValue(startRow, 4);


                if (thuStr == null || buoiStr == null || tietStr == null)
                {
                    Error = " - Thứ, buổi, hoặc tiết trong file excel sai định dạng";
                    return null;
                }
                else
                {
                    if (thuStr.ToString().ToUpper().Trim() == "CHỦ NHẬT")
                        thuStr = "8";

                    if (int.TryParse(thuStr.ToString(), out thu))
                    {
                        if (!(thu >= 2 && thu <= 8))
                        {
                            Error = " - Thứ phải từ 2 - chủ nhật";
                            return null;
                        }
                    }
                    if (!lstSection.Any(u => u.SectionName.ToLower().Contains(buoiStr.ToString().ToLower())))
                    {
                        Error = " - File excel không có buổi học này";
                        return null;
                    }
                    else
                    {
                        buoi = lstSection.Where(u => u.SectionName.ToLower().Contains(buoiStr.ToString().ToLower())).Select(o => o.Section).FirstOrDefault();
                    }

                    if (int.TryParse(tietStr.ToString(), out tiet))
                    {
                        if (!(tiet >= 1 && tiet <= 5))
                        {
                            Error = " - Tiết phải từ 2 - 7";
                            return null;
                        }
                    }
                }
                int clIndex = 5;
                foreach (ClassProfile cl in lstClass)
                {
                    var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                    CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);

                    if (lstSectionFor.Any(u => u.Section == buoi))
                    {
                        var monStr = sheet.GetCellValue(startRow, clIndex);
                        if (monStr != null)
                        {
                            //lay danh sach mon hoc cua lop
                            List<SubjectCat> listSubjectForClass = listClassSubject.Where(u => u.ClassID == cl.ClassProfileID).Select(u => u.SubjectCat).ToList();
                          
                            if (Subject_Sinh_Hoat != null)
                            {
                                listSubjectForClass.Insert(0, Subject_Sinh_Hoat);
                                if (Subject_Chao_Co != null)
                                    listSubjectForClass.Insert(0, Subject_Chao_Co);
                            }

                            List<AssignSubjectConfig> objASCTemp = new List<AssignSubjectConfig>();
                            var sbCl = listSubjectForClass.Where(u => u.DisplayName.ToLower() == monStr.ToString().ToLower());
                            //objSubjectTemp = listSubject.Where(x => x.SubjectCatID == objSubject.SubjectCatID).ToList().FirstOrDefault();
                            string stringEducationLevel = cl.EducationLevelID.ToString();
                            objASCTemp = lstASC.Where(x => x.AssignSubjectName.ToLower() == monStr.ToString().ToLower() && x.EducationApplied.Contains(stringEducationLevel)).ToList();


                            int SubjectID;
                            int? AssignSubjectID;
                            string SubjectName = "";
                            if (sbCl.Count() > 0 && objASCTemp.Count() > 0) 
                            {
                                SubjectID = sbCl.FirstOrDefault().SubjectCatID;
                                AssignSubjectID = objASCTemp.FirstOrDefault().AssignSubjectConfigID;
                            }
                            else if (objASCTemp.Count() > 0) 
                            {                              
                                AssignSubjectID = objASCTemp.FirstOrDefault().AssignSubjectConfigID;
                                SubjectID = objASCTemp.FirstOrDefault().SubjectID;
                            }
                            else
                            {
                                SubjectID = sbCl.FirstOrDefault().SubjectCatID;
                                AssignSubjectID = null;
                            }



                            if ((sbCl != null && sbCl.Count() > 0) || (objASCTemp != null && objASCTemp.Count() > 0))
                            {
                                SubjectCat sbc = sbCl.FirstOrDefault();
                                Calendar calendar = new Calendar();
                                calendar.AcademicYearID = glo.AcademicYearID.Value;
                                calendar.ClassID = cl.ClassProfileID;
                                calendar.DayOfWeek = thu;
                                calendar.NumberOfPeriod = tiet;
                                calendar.SubjectOrder = tiet;
                                calendar.Section = buoi;
                                calendar.SubjectID = SubjectID;
                                calendar.IsActive = true;
                                calendar.StartDate = StartDate;
                                calendar.Semester = semesterid;
                                calendar.AssignmentSubjectID = AssignSubjectID;

                                listCalendar.Add(calendar);
                            }
                            else
                            {
                                clIndex++;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        //bo qua voi nhung lop khong co buoi
                        clIndex++;
                        continue;
                    }

                    clIndex++;
                }

                startRow++;
            }

            return listCalendar;
        }

        [HttpPost]
        public ActionResult GetViewTeacherSendSMS(string EmployeeCode, int? Level, int? ClassID, int? Semester)
        {
            GlobalInfo glo = new GlobalInfo();
            IEnumerable<Employee> listTeacher = EmployeeBusiness.SearchTeacher(glo.SchoolID.Value, new Dictionary<string, object> { { "EmployeeCode", EmployeeCode } }).ToList();
            Employee employee = new Employee();

            //set du lieu
            #region comboKhoa
            List<SelectListItem> lstLevel = new List<SelectListItem>();
            foreach (var itm in _globalInfo.EducationLevels)
            {
                SelectListItem slItem = new SelectListItem();
                slItem.Text = itm.Resolution;
                slItem.Value = itm.EducationLevelID.ToString();

                lstLevel.Add(slItem);
            }
            SelectListItem allSelect = new SelectListItem();
            allSelect.Text = Res.Get("All");
            allSelect.Value = "";
            lstLevel.Insert(0, allSelect);
            ViewData["lstLevel"] = lstLevel;
            #endregion

            #region ComboLop
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (Semester.HasValue)
            {
                dic["Semester"] = Semester;
            }
            if (Level != null)
            {
                dic["EducationLevelID"] = Level;
            }
            IEnumerable<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).OrderBy(a => a.DisplayName).ToList();
            // Danh sách lớp
            ViewData["lstClass"] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            #endregion



            var lstSection = new List<CalendarBusiness.ClassSection>();
            lstSection.Add(new CalendarBusiness.ClassSection(SystemParamsInFile.SECTION_MORNING));
            lstSection.Add(new CalendarBusiness.ClassSection(SystemParamsInFile.SECTION_AFTERNOON));
            lstSection.Add(new CalendarBusiness.ClassSection(SystemParamsInFile.SECTION_EVENING));

            if (Semester != glo.Semester.Value || !glo.IsCurrentYear)
            {
                return Json(new JsonMessage(Res.Get("SchoolCalendar_Label_NotSemester"), "error"));
            }

            if (listTeacher != null && listTeacher.Count() > 0)
            {
                employee = listTeacher.FirstOrDefault();

                //khong co so dien thoai
                if (employee.Mobile == null)
                {
                    return Json(new JsonMessage(Res.Get("SchoolCalendar_Label_NotNumber"), "error"));
                }

                //Ten giao vien
                ViewData[SchoolCalendarConstants.TEACHER_NAME] = employee.FullName;

                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = glo.AcademicYearID;
                dic["ClassID"] = ClassID;
                dic["EducationLevelID"] = Level;
                dic["EmployeeID"] = employee.EmployeeID;
                dic["AppliedLevel"] = glo.AppliedLevel;
                dic["Semester"] = glo.Semester;
                IEnumerable<Calendar> listCalendar = CalendarBusiness.Search(dic).ToList();
                if (listCalendar != null && listCalendar.Count() > 0)
                {

                    //fix thu chay tu thu 2 den thu 7
                    StringBuilder contentAll = new StringBuilder();
                    for (int thu = startDay; thu <= endDay; thu++)
                    {
                        StringBuilder contentAllThu = new StringBuilder();
                        for (int buoi = SystemParamsInFile.SECTION_MORNING;
                            buoi <= SystemParamsInFile.SECTION_EVENING; buoi++)
                        {

                            //lay tat cac tiet hoc trong buoi
                            List<Calendar> listForSection = listCalendar.Where(u => u.DayOfWeek == thu && u.Section == buoi).OrderBy(u => u.NumberOfPeriod).ToList();

                            List<int> listCalendarAdd = new List<int>();

                            StringBuilder contentAllTiet = new StringBuilder();
                            foreach (Calendar calendar in listForSection)
                            {
                                List<Calendar> listForClass = listForSection.Where(u =>
                                                            !listCalendarAdd.Contains(u.CalendarID) &&
                                                            u.ClassID == calendar.ClassID && u.SubjectID == calendar.SubjectID).ToList();

                                if (listForClass != null && listForClass.Count() > 0)
                                {
                                    //them vao truong hop ngoai le lich hoc
                                    listCalendarAdd.InsertRange(0, listForClass.Select(u => u.CalendarID).ToList());

                                    string tietHoc = string.Join(",", listForClass.Select(u => u.NumberOfPeriod.ToString()).ToList());

                                    string contentTiet = string.Format(Res.Get("SchoolCalendar_Label_TitleSection"), tietHoc, listForClass.First().SubjectCat.DisplayName, listForClass.First().ClassProfile.DisplayName);
                                    contentAllTiet.Append(contentTiet);
                                }
                            }
                            //neu khong co thi bo qua
                            if (contentAllTiet.Length > 0)
                            {
                                string contentBuoi = string.Format(Res.Get("SchoolCalendar_Label_TitleSection2"), lstSection.Where(u => u.Section == buoi).First().SectionName, contentAllTiet.ToString());
                                contentAllThu.Append(contentBuoi);
                            }
                        }
                        if (contentAllThu.Length > 0)
                        {
                            string contentThu = string.Format(Res.Get("SchoolCalendar_Label_TitleSection3"), thu.ToString(), contentAllThu.ToString());
                            contentAll.Append(contentThu);
                        }
                    }

                    //thong tin cua tin nhan 
                    SchoolProfile School = SchoolProfileBusiness.Find(glo.SchoolID);
                    string SchoolName = Business.Common.Utils.StripVNSign(School.SchoolName);
                    string Genre = employee.Genre ? Res.Get("SchoolCalendar_Label_TitleSection4") : Res.Get("SchoolCalendar_Label_TitleSection5");
                    //dinh dang tin nhan
                    string MessageFormat = string.Format(Res.Get("SchoolCalendar_Label_TitleSection6"), SchoolName, Genre, employee.FullName, contentAll);

                    ViewData[SchoolCalendarConstants.CONTENT_SMS] = MessageFormat;

                    //luu lai noi dung tin nhan de kiem tra lai tren server
                    Session[SchoolCalendarConstants.CONTENT_SMS] = MessageFormat;

                    return PartialView("_MessageDialog");
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("SchoolCalendar_Label_TitleSection7"), "error"));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("SchoolCalendar_Label_TitleSection8"), "error"));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendSMS(string EmployeeCode, string Message)
        {
            //Lay noi dung tin nhan
            string smsContent = SMAS.Business.Common.Utils.StripVNSign((string)Session[SchoolCalendarConstants.CONTENT_SMS]).Replace("\\n", "\n");
            if (!Message.Contains(smsContent))
            {
                Json(new JsonMessage(Res.Get("SchoolCalendar_Label_ErrorSendSMS"), "error"));
            }

            //luu vao bang smsresult
            IEnumerable<Employee> listTeacher = EmployeeBusiness.SearchTeacher(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "EmployeeCode", EmployeeCode } }).ToList();
            Employee employee = listTeacher != null && listTeacher.Count() > 0 ? listTeacher.FirstOrDefault() : null;

            if (employee != null)
            {
                int numMsg = Message.Length % 500 == 0 ? Message.Length / 500 : (Message.Length / 500) + 1;
                List<string> listStr = SplitBy(Message, 500).ToList();
                foreach (string str in listStr)
                {
                    SendResult sms = new SendResult();
                    sms.SchoolID = _globalInfo.SchoolID;
                    sms.RetryNum = 0;
                    sms.TimeSend = DateTime.Now.ToShortTimeString();
                    sms.ReceiverMobile = employee.Mobile;
                    sms.Year = DateTime.Now.Year;
                    sms.Status = 0;
                    sms.EmployeeCode = EmployeeCode;
                    sms.CreateUser = _globalInfo.UserAccountID.ToString();
                    sms.Content = str;
                    SendResultBusiness.Insert(sms);
                }
                SendResultBusiness.Save();

            }



            return Json(new JsonMessage(Res.Get("SchoolCalendar_Label_SendSMSSuccess")));
        }

        public string[] SplitBy(string source, int count)
        {
            const string Separator = "╩";
            var byCount = source.Select((c, i) => i % count == 0 ? Separator + c : c.ToString()).ToArray();
            var inString = string.Join(string.Empty, byCount);
            return inString.Split(new[] { Separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        [HttpPost]
        public JsonResult GetInfoSubject(int? ClassID, int? SubjectID, int? Semester)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            Dictionary<string, object> dicCalendar = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;

            if (ClassID.HasValue)
            {
                dic["ClassProfileID"] = ClassID;
                dic["ClassID"] = ClassID;
            }

            if (Semester.HasValue)
            {
                dic["Semester"] = Semester;
            }
            var mdl = new CalendarModel();
            var subject = SubjectCatBusiness.Find(SubjectID);
            //mdl.AssignSubjectID = 0;
            if (subject == null)
            {
                int SubjectIDInAssignSubject = AssignSubjectConfigBusiness.Find(SubjectID).SubjectID;
                subject = SubjectCatBusiness.Find(SubjectIDInAssignSubject);
                //mdl.AssignSubjectID = 1;
            }

            if (SubjectID.HasValue)
            {
                dic["SubjectID"] = subject.SubjectCatID;
            }
            

            var classProfile = ClassProfileBusiness.Find(ClassID);
            var lstTeacher = TeachingAssignmentBusiness.GetTeachingAssigment(_globalInfo.SchoolID.Value, dic);
            

            mdl.SubjectID = subject.SubjectCatID;
            mdl.SubjectName = subject.DisplayName;
            mdl.SubjectColor = subject.Color;
            int order = 0;
            if (lstTeacher.Count() <= 2)
            {
                lstTeacher.ToList().ForEach(u =>
                {
                    string teacher = "<p>" + u.TeacherName + "</p>";
                    mdl.NameTeacher = mdl.NameTeacher + teacher;
                });
            }
            else
            {
                lstTeacher.ToList().ForEach(u =>
                {
                    order++;
                    string teacher = "<p>" + u.TeacherName + "</p>";
                    if (order == 3)
                    {
                        teacher = "<div style='text-align:center;'>...</div>";
                        mdl.NameTeacher = mdl.NameTeacher + teacher;
                    }
                    else if (order < 3)
                    {
                        mdl.NameTeacher = mdl.NameTeacher + teacher;
                    }
                });
            }

            //tooltip
            lstTeacher.ToList().ForEach(u =>
            {
                mdl.TeacherTooltip += string.IsNullOrEmpty(u.TeacherName) ? string.Empty : u.TeacherName + Environment.NewLine;
            });

            if (mdl.SubjectID != Constants.GlobalConstants.SINH_HOAT_SUBJECT && mdl.SubjectID != Constants.GlobalConstants.CHAO_CO_SUBJECT)
                mdl.TeacherNames = lstTeacher.Select(a => a.TeacherName).ToList();
            else if (mdl.SubjectID == Constants.GlobalConstants.SINH_HOAT_SUBJECT)
            {
                //voi mon sinh hoat lay ten giao vien chu nhiem
                string teacher = "<p>" + classProfile.Employee != null && classProfile.Employee != null ? classProfile.Employee.FullName : "" + "</p>";
                mdl.NameTeacher = teacher;
            }
            else
            {
                mdl.TeacherNames = new List<string>();
            }

            return Json(mdl);
        }

        [HttpPost]
        public JsonResult GetInfoSubjectString(int? ClassID, string SubjectID, int? Semester)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            Dictionary<string, object> dicCalendar = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;

            if (ClassID.HasValue)
            {
                dic["ClassProfileID"] = ClassID;
                dic["ClassID"] = ClassID;
            }

            if (Semester.HasValue)
            {
                dic["Semester"] = Semester;
            }
            var mdl = new CalendarModel();
            int tryParse;
            bool isAssign = Int32.TryParse(SubjectID, out tryParse);
            SubjectCat subject = new SubjectCat();
            int SubjectCatID;
            if (isAssign)
            {
                SubjectCatID = Int32.Parse(SubjectID);
                subject = SubjectCatBusiness.Find(SubjectCatID);
                mdl.IsAssignment = 0;
            }
            else
            {
                string[] subjectArr = SubjectID.Split('*');
                SubjectCatID = Int32.Parse(subjectArr[1]);
                int SubjectIDInAssignSubject = AssignSubjectConfigBusiness.Find(SubjectCatID).SubjectID;
                subject = SubjectCatBusiness.Find(SubjectIDInAssignSubject);
                mdl.IsAssignment = 1;
            }

            //if (SubjectCatID != null)
            //{
                dic["SubjectID"] = subject.SubjectCatID;
            //}


            var classProfile = ClassProfileBusiness.Find(ClassID);
            var lstTeacher = TeachingAssignmentBusiness.GetTeachingAssigment(_globalInfo.SchoolID.Value, dic);


            mdl.SubjectID = subject.SubjectCatID;
            mdl.SubjectName = subject.DisplayName;
            mdl.SubjectColor = subject.Color;
            int order = 0;
            if (lstTeacher.Count() <= 2)
            {
                lstTeacher.ToList().ForEach(u =>
                {
                    string teacher = "<p>" + u.TeacherName + "</p>";
                    mdl.NameTeacher = mdl.NameTeacher + teacher;
                });
            }
            else
            {
                lstTeacher.ToList().ForEach(u =>
                {
                    order++;
                    string teacher = "<p>" + u.TeacherName + "</p>";
                    if (order == 3)
                    {
                        teacher = "<div style='text-align:center;'>...</div>";
                        mdl.NameTeacher = mdl.NameTeacher + teacher;
                    }
                    else if (order < 3)
                    {
                        mdl.NameTeacher = mdl.NameTeacher + teacher;
                    }
                });
            }

            //tooltip
            lstTeacher.ToList().ForEach(u =>
            {
                mdl.TeacherTooltip += string.IsNullOrEmpty(u.TeacherName) ? string.Empty : u.TeacherName + Environment.NewLine;
            });

            if (mdl.SubjectID != Constants.GlobalConstants.SINH_HOAT_SUBJECT && mdl.SubjectID != Constants.GlobalConstants.CHAO_CO_SUBJECT)
                mdl.TeacherNames = lstTeacher.Select(a => a.TeacherName).ToList();
            else if (mdl.SubjectID == Constants.GlobalConstants.SINH_HOAT_SUBJECT)
            {
                //voi mon sinh hoat lay ten giao vien chu nhiem
                string teacher = "<p>" + classProfile.Employee != null && classProfile.Employee != null ? classProfile.Employee.FullName : "" + "</p>";
                mdl.NameTeacher = teacher;
            }
            else
            {
                mdl.TeacherNames = new List<string>();
            }

            return Json(mdl);
        }

    }
}
