﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SchoolCalendarArea
{
    public class SchoolCalendarAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SchoolCalendarArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SchoolCalendarArea_default",
                "SchoolCalendarArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
