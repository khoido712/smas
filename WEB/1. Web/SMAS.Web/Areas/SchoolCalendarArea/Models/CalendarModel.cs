﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SchoolCalendarArea;
using SMAS.Models.Models;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Areas.ClassProfileArea;

namespace SMAS.Web.Areas.SchoolCalendarArea.Models
{
    public class CalendarModel
    {
        /// <summary>
        /// ID cua calendar Object
        /// </summary>
        public int CalendarID { get; set; }
        /// <summary>
        /// Nam hoc
        /// </summary>
        public int AcademicYearID { get; set; }
        /// <summary>
        /// ClassID
        /// </summary>
        public int ClassID { get; set; }
        /// <summary>
        /// ID Mon hoc duoc chon
        /// </summary>
        public int SubjectID { get; set; }
        /// <summary>
        /// Thứ
        /// </summary>
        public int DayOfWeek { get; set; }
        /// <summary>
        /// Buổi
        /// </summary>
        public int? Section { get; set; }
        /// <summary>
        /// Học kỳ
        /// </summary>
        public int Semester { get; set; }
        /// <summary>
        /// Mau mon hoc
        /// </summary>
        public string SubjectColor { get; set; }
        /// <summary>
        /// Ten mon hoc
        /// </summary>
        public string SubjectName { get; set; }
        /// <summary>
        /// Tiết học
        /// </summary>
        public int ? NumberOfPeriod { get; set; }
        /// <summary>
        /// Danh sach giao vien duoc phan cong giang day mon nay
        /// </summary>
        public List<string> TeacherNames { get; set; }

        /// <summary>
        /// Ten Giao Vien Kieu String
        /// </summary>
        public string NameTeacher { get; set; }

        /// <summary>
        /// Thong tin chi tiet giao vien 
        /// </summary>
        public string TeacherTooltip { get; set; }

        /// <summary>
        /// Phân môn hay không
        /// </summary>
        public int IsAssignment { get; set; }

        /// <summary>
        /// ID phân môn
        /// </summary>
        public int? AssignSubjectID { get; set; }

        /// <summary>
        /// ID phân môn
        /// </summary>
        public string AssignSubjectName { get; set; }

        /// <summary>
        /// Check neu buoi hoc duoc luu la dung
        /// </summary>
        /// <param name="seperate">in PupilProfile.Section</param>
        /// <param name="secton"></param>
        /// <returns></returns>
        public bool CheckSeperate(int ? seperate, int section)
        {
            if (!seperate.HasValue) return false;

            /*string binary = Convert.ToString(seperate.Value, 2);
            if (this.CheckPosVal(binary, 3 - ClassProfileConstants.MORNING))
            {
                if (ClassProfileConstants.MORNING == section) return true;
            }
            if (this.CheckPosVal(binary, 3 - ClassProfileConstants.AFTERNOON))
            {
                if (ClassProfileConstants.AFTERNOON == section) return true;
            }
            if (this.CheckPosVal(binary, 3 - ClassProfileConstants.NIGHT))
            {
                if (ClassProfileConstants.NIGHT == section) return true;
            }*/
            if (seperate == ClassProfileConstants.MORNING && section == ClassProfileConstants.MORNING)
            {
                return true;
            }
            if (seperate == ClassProfileConstants.AFTERNOON && section == ClassProfileConstants.AFTERNOON)
            {
                return true;
            }
            if (seperate == ClassProfileConstants.NIGHT && section == ClassProfileConstants.NIGHT)
            {
                return true;
            }
            if (seperate == 4 && (section == ClassProfileConstants.MORNING || section==ClassProfileConstants.AFTERNOON))
            {
                return true;
            }
            if (seperate == 5 && (section == ClassProfileConstants.MORNING || section == ClassProfileConstants.NIGHT))
            {
                return true;
            }
            if (seperate == 6 && (section == ClassProfileConstants.AFTERNOON || section == ClassProfileConstants.NIGHT))
            {
                return true;
            }
            if (seperate == 7 && (section == ClassProfileConstants.MORNING||
                                   section == ClassProfileConstants.AFTERNOON || 
                                   section == ClassProfileConstants.NIGHT))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Kiem tra mot ky tu tai vi tri do xem co trung voi 1
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private bool CheckPosVal(string s, int pos)
        {
            if (s == null || s.Equals(string.Empty))
            {
                return false;
            }
            if (s.Length < 3)
            {
                for (int i = s.Length; i < 3; i++)
                {
                    s = "0" + s;
                }
            }
            char c = s[pos];
            if (c.Equals('1'))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Contructor
        /// </summary>
        public CalendarModel()
        {
            SubjectName = SubjectColor = string.Empty;
            TeacherNames = new List<string>();
            DayOfWeek = Semester =  SubjectID = ClassID = AcademicYearID = 0;
            Section = 0;  NumberOfPeriod = 0;
        }
    }
    /// <summary>
    /// Update Model
    /// </summary>
    public class UpdateCalendar
    {
        public int CalendarID { get; set; }
        public int? Semester { get; set; }
        public int ClassID { get; set; }
        public int DayOfWeek { get; set; }
        public int NumberOfPeriod { get; set; }
        public int Section { get; set; }
        public int SubjectID { get; set; }
        public string Sbj { get; set; }
        public string stringSubjectAndAssignID { get; set; }
        public int IsAssign { get; set; }
    }
}