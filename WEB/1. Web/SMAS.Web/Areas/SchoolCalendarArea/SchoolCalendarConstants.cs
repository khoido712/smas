﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SchoolCalendarArea
{
    public class SchoolCalendarConstants
    {
        /// <summary>
        /// So lop tren moi trang
        /// </summary>
        public const int CLASS_PER_PAGE = 6;
        /// <summary>
        /// Ngay bat dau cua tuan (thứ 2)
        /// </summary>
        public const int DAY_IN_WEEK_START = 2;
        /// <summary>
        /// Ngày kết thúc của tuần (thứ 7)
        /// </summary>
        public const int DAY_IN_WEEK_END = 8;
        /// <summary>
        /// Số tiết trên một buổi
        /// </summary>
        public const int NUMBER_OF_TIME_UNIT_IN_SECTION = 5;

        public const string MON_CHAO_CO = "Chào cờ";
        public const string MON_SINH_HOAT_LOP = "Sinh hoạt lớp";

        public const string TEMPLATE_NAME = "Import_ThoiKhoaBieu.xls";

        public const string CONTENT_SMS = "content_sms";

        public const string TEACHER_NAME = "TeacherName";
    }
}