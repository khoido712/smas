﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TranscriptOfClassPeriodArea
{
    public class TranscriptOfClassPeriodAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TranscriptOfClassPeriodArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TranscriptOfClassPeriodArea_default",
                "TranscriptOfClassPeriodArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
