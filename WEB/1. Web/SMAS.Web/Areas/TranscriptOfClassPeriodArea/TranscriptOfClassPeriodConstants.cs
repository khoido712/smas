﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TranscriptOfClassPeriodArea
{
    public class TranscriptOfClassPeriodConstants
    {
        public const string LIST_EDUCATION_LEVEL = "List_Education_Level";
        public const string LIST_CLASS = "List_Class";
        public const string LIST_SUBJECT = "List_Subject";
        public const string LIST_PERIOD_COMMON = "List_Period_Common";
        public const string LIST_PERIOD_SUBJECT = "List_Period_Subject";
        public const string LIST_SEMESTER_COMMON = "List_Semester_Common";
        public const string LIST_SEMESTER_SUBJECT = "List_Semester_Subject";
    }
}