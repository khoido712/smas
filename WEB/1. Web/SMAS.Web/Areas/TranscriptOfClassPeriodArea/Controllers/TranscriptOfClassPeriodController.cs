﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.TranscriptOfClassPeriodArea.Models;
using System.IO;
using SMAS.Web.Areas.TranscriptOfClassPeriodArea;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.TranscriptOfClassPeriodArea.Controllers
{
    public class TranscriptOfClassPeriodController : BaseController
    {
        IClassProfileBusiness ClassProfileBusiness;
        IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        IClassSubjectBusiness ClassSubjectBusiness;
        ITranscriptsByPeriodBusiness TranscriptsByPeriodBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;

        public TranscriptOfClassPeriodController(IClassProfileBusiness classProfileBusiness,
            IPeriodDeclarationBusiness periodDeclarationBusiness,
            IClassSubjectBusiness classSubjectBusiness,
            ITranscriptsByPeriodBusiness transcriptsByPeriodBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.PeriodDeclarationBusiness = periodDeclarationBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.TranscriptsByPeriodBusiness = transcriptsByPeriodBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        [HttpPost]
        public JsonResult ExportCommon(TranscriptOfClassPeriodViewModel model)
        {
            PupilRewardReportBO bo = new PupilRewardReportBO();
            GlobalInfo GlobalInfo = new GlobalInfo();

            string type = JsonReportMessage.NEW;

            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.SchoolID = glo.SchoolID.Value;
            toc.AcademicYearID = glo.AcademicYearID.Value;
            toc.AppliedLevel = glo.AppliedLevel.Value;
            if (model.ClassPeriodID.HasValue)
            {
                toc.ClassID = model.ClassPeriodID.Value;
            }
            toc.EducationLevelID = model.EducationLevelID.Value;
            toc.PeriodDeclarationID = model.PeriodDeclarationID;
            toc.SchoolID = glo.SchoolID.Value;
            toc.Semester = model.Semester.Value;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEODOT);
            ProcessedReport entity = null;

            if (reportDef.IsPreprocessed == true)
            {
                entity = TranscriptsByPeriodBusiness.GetSummariseTranscriptsByPeriod(toc);
                if (entity != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptsByPeriodBusiness.CreateSummariseTranscriptsByPeriod(toc);
                entity = TranscriptsByPeriodBusiness.InsertSummariseTranscriptsByPeriod(toc, excel);
                ProcessedReportBusiness.Save();
                excel.Close();
            }
            return Json(new JsonReportMessage(entity, type));
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult ExportCommonRecreate(TranscriptOfClassPeriodViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.SchoolID = glo.SchoolID.Value;
            toc.AcademicYearID = glo.AcademicYearID.Value;
            toc.AppliedLevel = glo.AppliedLevel.Value;
            if (model.ClassPeriodID.HasValue)
            {
                toc.ClassID = model.ClassPeriodID.Value;
            }
            toc.EducationLevelID = model.EducationLevelID.Value;
            toc.PeriodDeclarationID = model.PeriodDeclarationID;
            toc.SchoolID = glo.SchoolID.Value;
            toc.Semester = model.Semester.Value;

            Stream excel = TranscriptsByPeriodBusiness.CreateSummariseTranscriptsByPeriod(toc);
            ProcessedReport entity = TranscriptsByPeriodBusiness.InsertTranscriptsByPeriod(toc, excel);
            excel.Close();
            return Json(new JsonReportMessage(entity, JsonReportMessage.NEW));
        }

        [HttpPost]
        public JsonResult ExportSubject(TranscriptOfClassPeriodViewModel model)
        {
            PupilRewardReportBO bo = new PupilRewardReportBO();
            GlobalInfo GlobalInfo = new GlobalInfo();

            string type = JsonReportMessage.NEW;

            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.SchoolID = glo.SchoolID.Value;
            toc.AcademicYearID = glo.AcademicYearID.Value;
            toc.AppliedLevel = glo.AppliedLevel.Value;
            toc.ClassID = model.ClassProfileID;
            toc.EducationLevelID = model.EducationLevelID.Value;
            toc.PeriodDeclarationID = model.PeriodDeclarationID;
            toc.SchoolID = glo.SchoolID.Value;
            toc.Semester = model.Semester.Value;
            toc.SubjectID = model.SubjectCatID.HasValue ? model.SubjectCatID.Value : 0;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANGDIEMLOPCAP23THEODOT);
            ProcessedReport entity = null;

            if (reportDef.IsPreprocessed == true)
            {
                entity = TranscriptsByPeriodBusiness.GetTranscriptsByPeriod(toc);
                if (entity != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptsByPeriodBusiness.CreateTranscriptsByPeriod(toc);
                entity = TranscriptsByPeriodBusiness.InsertTranscriptsByPeriod(toc, excel);
                ProcessedReportBusiness.Save();
                excel.Close();
            }
            return Json(new JsonReportMessage(entity, type));
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult ExportSubjectRecreate(TranscriptOfClassPeriodViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.SchoolID = glo.SchoolID.Value;
            toc.AcademicYearID = glo.AcademicYearID.Value;
            toc.AppliedLevel = glo.AppliedLevel.Value;
            toc.ClassID = model.ClassProfileID;
            toc.EducationLevelID = model.EducationLevelID.Value;
            toc.PeriodDeclarationID = model.PeriodDeclarationID;
            toc.Semester = model.Semester.Value;
            toc.SubjectID = model.SubjectCatID.HasValue ? model.SubjectCatID.Value : 0;

            Stream excel = TranscriptsByPeriodBusiness.CreateTranscriptsByPeriod(toc);
            ProcessedReport entity = TranscriptsByPeriodBusiness.InsertTranscriptsByPeriod(toc, excel);
            excel.Close();
            return Json(new JsonReportMessage(entity, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { SystemParamsInFile.REPORT_BANGDIEMLOPCAP23THEODOT, SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEODOT };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int educationLevelID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = glo.AcademicYearID.Value;
            dicClass["EducationLevelID"] = educationLevelID;
            if (!glo.IsAdminSchoolRole)
            {
                dicClass["UserAccountID"] = glo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }

            List<ComboObject> lstClass = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(o => o.DisplayName)
                                                .ToList()
                                                .Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName))
                                                .ToList();

            return Json(lstClass);
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadSubject(int? ClassProfileID)
        {
            GlobalInfo glo = new GlobalInfo();
            List<ComboObject> lstSubject = new List<ComboObject>();
            if (ClassProfileID != null)
            {
                lstSubject  = ClassSubjectBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "ClassID", ClassProfileID } })
                                                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList()
                                                .Select(u => new ComboObject(u.SubjectID.ToString(), u.SubjectCat.DisplayName))
                                                .ToList();
            }
            return Json(lstSubject);
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadPeriod(int semester)
        {
            GlobalInfo glo = new GlobalInfo();
            List<ComboObject> lstSubject = PeriodDeclarationBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", glo.AcademicYearID.Value }, { "Semester", semester } })
                                                .ToList()
                                                .Select(u => new ComboObject(u.PeriodDeclarationID.ToString(), u.Resolution))
                                                .ToList();
            return Json(lstSubject);
        }

        private void GetViewData()
        {
            GlobalInfo glo =new GlobalInfo();
            ViewData[TranscriptOfClassPeriodConstants.LIST_EDUCATION_LEVEL] = new SelectList(glo.EducationLevels, "EducationLevelID", "Resolution");

            EducationLevel educationLevel = glo.EducationLevels.FirstOrDefault();

            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = glo.AcademicYearID.Value;
            dicClass["EducationLevelID"] = educationLevel.EducationLevelID;
            if (!glo.IsAdminSchoolRole) {
                dicClass["UserAccountID"] = glo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }
            var lstClass = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, dicClass).ToList();
            ViewData[TranscriptOfClassPeriodConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            //ClassProfile classProfile = lstClass.FirstOrDefault();

            List<SubjectCat> lstSubject = new List<SubjectCat>();
            //if (classProfile != null)
            //{
            //    lstSubject = ClassSubjectBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "ClassID", classProfile.ClassProfileID } }).Select(u => u.SubjectCat).ToList();
            //}
            ViewData[TranscriptOfClassPeriodConstants.LIST_SUBJECT] = new SelectList(lstSubject, "SubjectCatID", "SubjectName");

            var lstSemesterCommon = CommonList.Semester();
            ViewData[TranscriptOfClassPeriodConstants.LIST_SEMESTER_COMMON] = new SelectList(lstSemesterCommon, "key", "value");

            //hoantv bảng điểm theo đợt semester không có cả năm
            //var lstSemesterSubject = CommonList.SemesterAndAll();
            //ViewData[TranscriptOfClassPeriodConstants.LIST_SEMESTER_SUBJECT] = new SelectList(lstSemesterSubject, "key", "value"); ;

            var semesterCommon = lstSemesterCommon.FirstOrDefault();
            //var semesterSubject = lstSemesterSubject.FirstOrDefault();

            var lstPeriodCommon = PeriodDeclarationBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", glo.AcademicYearID.Value }, { "Semester", Convert.ToInt32(semesterCommon.key) } });
            //var lstPeriodSubject = PeriodDeclarationBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", glo.AcademicYearID.Value }, { "Semester", Convert.ToInt32(semesterSubject.key) } });

            ViewData[TranscriptOfClassPeriodConstants.LIST_PERIOD_COMMON] = new SelectList(lstPeriodCommon, "PeriodDeclarationID", "Resolution");
            ViewData[TranscriptOfClassPeriodConstants.LIST_PERIOD_SUBJECT] = new SelectList(lstPeriodCommon, "PeriodDeclarationID", "Resolution"); 
        }


    }
}
