﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.TranscriptOfClassPeriodArea.Models
{
    public class TranscriptOfClassPeriodViewModel
    {
        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("ClassProfile_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ClassProfileID { get; set; }

        [ResourceDisplayName("ClassProfile_Label_AllTitle")]
        public int? ClassPeriodID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        public int? SubjectCatID {get;set;}

        [ResourceDisplayName("Common_Label_Semester")]
        public int? Semester { get; set; }

        [ResourceDisplayName("PeriodDeclaration_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PeriodDeclarationID { get; set; }

        public bool? ForceCreate { get; set; }
    }
}