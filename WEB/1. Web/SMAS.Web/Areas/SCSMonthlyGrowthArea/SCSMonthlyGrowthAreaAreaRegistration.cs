﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SCSMonthlyGrowthArea
{
    public class SCSMonthlyGrowthAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SCSMonthlyGrowthArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SCSMonthlyGrowthArea_default",
                "SCSMonthlyGrowthArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
