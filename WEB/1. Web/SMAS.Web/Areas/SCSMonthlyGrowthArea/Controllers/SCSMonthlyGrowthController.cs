﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using SMAS.Web.Filter;
using SMAS.Web.Areas.SCSMonthlyGrowthArea;
using SMAS.Web.Areas.SCSMonthlyGrowthArea.Models;
using System.Collections;

namespace SMAS.Web.Areas.SCSMonthlyGrowthArea.Controllers
{
    /// <summary>
    /// monthly growth controller
    /// </summary>
    /// <auth>HaiVT</auth>
    /// <date>17/04/2013</date>
    public class SCSMonthlyGrowthController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private readonly IPhysicalTestBusiness PhysicalTestBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public SCSMonthlyGrowthController
            (
                IClassProfileBusiness classProfileBusiness,
                IMonitoringBookBusiness monitoringBookBusiness,
                IPhysicalTestBusiness physicalTestBusiness,
                IAcademicYearBusiness academicYearBusiness
            )
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.PhysicalTestBusiness = physicalTestBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }
        #region tang truong thang
        /// <summary>
        /// load index with list class and grid
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>23/04/2013</date>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(int classID = 0)
        {
            #region danh sach lop hoc
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EmployeeID"] = _globalInfo.EmployeeID;
            dic["Semester"] = _globalInfo.Semester;
            // Lấy về danh sách khôi
            ViewData[SCSMonthlyGrowthConstants.LIST_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
           
            IQueryable<ClassProfile> lstClass = null;
            if (_globalInfo.IsAdmin == false)
            {
                //chi lay danh sach lop theo quyen gvcn
                lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value);                                
            }
            else if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);
            }
            List<ClassProfile> lstClassProfile = lstClass.OrderBy(p => p.DisplayName).ToList();
            #endregion
            if (lstClassProfile == null || lstClassProfile.Count == 0)
            {
                ViewData[SCSMonthlyGrowthConstants.LIST_CLASS] = new List<ClassProfile>();
                ViewData[SCSMonthlyGrowthConstants.LIST_PUPIL] = new List<MonthlyGrowthViewModel>();
                ViewData[SCSMonthlyGrowthConstants.CLASSID_SELECT] = classID;
            }
            else
            {
                #region danh sach hoc sinh cua lop
                //Lay theo khoi nho nhat truoc + lay lop sap xep theo thu tu ABC
                var currentClass = lstClassProfile.OrderBy(a => a.EducationLevelID).OrderBy(p => p.DisplayName).FirstOrDefault();
                if (classID == 0)
                {
                    classID = currentClass.ClassProfileID;
                    dic["classID"] = classID;
                }
                else
                {
                    dic["classID"] = classID;
                }
                DateTime dateTimeNow = DateTime.Now;
                dic["monthSelect"] = dateTimeNow;
                ViewData[SCSMonthlyGrowthConstants.LIST_CLASS] = lstClassProfile.Where(a => a.EducationLevelID == currentClass.EducationLevelID).ToList();
                ViewData[SCSMonthlyGrowthConstants.FIRST_LEVEL_SELECTED] = currentClass.EducationLevelID;
                ViewData[SCSMonthlyGrowthConstants.LIST_PUPIL] = MonitoringBookBusiness.GetListMonthlyGrowth(dic)
                                                                .Select(p =>
                                                                    new MonthlyGrowthViewModel
                                                                    {
                                                                        MonitoringBookID = p.MonitoringBookID,
                                                                        PupilID = p.PupilID,
                                                                        BirthDate = p.BirthDate,
                                                                        Genre = p.Genre,
                                                                        Image = p.Image,
                                                                        FullName = p.FullName,
                                                                        Height = p.Height,
                                                                        Weight = p.Weight,
                                                                        Status = p.Status,
                                                                        Nutrition = p.Nutrition,
                                                                        Solution = p.Solution,
                                                                        Month = ((dateTimeNow.Year - p.BirthDate.Year) * 12) + dateTimeNow.Month - p.BirthDate.Month
                                                                    }
                                                                ).ToList();
                ViewData[SCSMonthlyGrowthConstants.CLASSID_SELECT] = classID;
                #endregion
            }

            #region list month select            
            ViewData[SCSMonthlyGrowthConstants.LIST_MONTH] = Utils.Utils.GetListMonth();

            #endregion

            return View();
        }

        /// <summary>
        /// reload grid when change class or month
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>23/04/2013</date>
        /// <param name="classID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReloadGrid(int classID, long dateTicks)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["classID"] = classID;
            dic["monthSelect"] = new DateTime(dateTicks);
            DateTime dateTimeNow = DateTime.Now;
            ViewData[SCSMonthlyGrowthConstants.LIST_PUPIL] = MonitoringBookBusiness.GetListMonthlyGrowth(dic).Select(p =>
                                                                new MonthlyGrowthViewModel
                                                                {
                                                                    MonitoringBookID = p.MonitoringBookID,
                                                                    PupilID = p.PupilID,
                                                                    BirthDate = p.BirthDate,
                                                                    Genre = p.Genre,
                                                                    Image = p.Image,
                                                                    FullName = p.FullName,
                                                                    Height = p.Height,
                                                                    Weight = p.Weight,
                                                                    Status = p.Status,
                                                                    Nutrition = p.Nutrition,
                                                                    Solution = p.Solution,
                                                                    Month =((dateTimeNow.Year - p.BirthDate.Year) * 12) + dateTimeNow.Month - p.BirthDate.Month
                                                                }
                                                            ).ToList();
            return PartialView("_ListPupil");
        }
        /// <summary>
        /// Lay danh sach lop hoc
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        [SkipCheckRole]
        public JsonResult LoadClass(int? EducationLevelID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["EducationLevelID"] = (EducationLevelID.HasValue) ? EducationLevelID.Value : 0;           
            var lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).OrderBy(a => a.EducationLevelID).OrderBy(a => a.DisplayName).ToList();
            if (lstClass == null) lstClass = new List<ClassProfile>();
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        /// <summary>
        /// save MonthlyGrowth
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>23/04/2013</date>
        /// <param name="monthlyGrowthBO"></param>
        /// <returns></returns>
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveMonthlyGrowth(List<MonthlyGrowthBO> lstMonthlyGrowth, int? classID, long dateTicks)
        {
            //check thoi gian nam hoc
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime monthSelect = new DateTime(dateTicks);
            if (objAcademicYear != null && monthSelect >= objAcademicYear.FirstSemesterStartDate && monthSelect <= objAcademicYear.SecondSemesterEndDate)
            {

                if (lstMonthlyGrowth != null && lstMonthlyGrowth.Count > 0)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    ClassProfile objClassProfile = ClassProfileBusiness.Find(classID);
                    if (objClassProfile == null)
                    {
                        return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                    }
                    else
                    {
                        dic["educationLevelID"] = objClassProfile.EducationLevelID;
                    }

                    dic["classID"] = classID;
                    dic["monthSelect"] = monthSelect;
                    dic["schoolID"] = _globalInfo.SchoolID;
                    dic["academicYearID"] = _globalInfo.AcademicYearID;
                    

                    ResultBO objResultBO = PhysicalTestBusiness.InsertOrUpdatePhysicalTest(lstMonthlyGrowth, dic);
                    if (objResultBO.isError)
                    {
                        return Json(new { Type = "error", Message = Res.Get(objResultBO.ErrorMsg) });
                    }

                    SetViewDataActionAudit("", objResultBO.LogResult, "", "MonthlyGrowth");

                    return Json(new { Type = "success", Message = Res.Get(objResultBO.Result), data = "[" + objResultBO.JsonResult.Substring(0, objResultBO.JsonResult.Length - 1) + "]" });
                }
                else
                {
                    return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_NoChange") });
                }
            }
            else
            {
                return Json(new { Type = "error", Message = Res.Get("PupilFault_Validate_IsNotCurrentYear") }); 
            }
        }
        #endregion        
    }
}
