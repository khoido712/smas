﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.SCSMonthlyGrowthArea.Models
{
    public class MonthlyGrowthViewModel
    {
        public int? MonitoringBookID { get; set; }
        public int PupilID { get; set; }
        public DateTime BirthDate { get; set; }
        public int Genre { get; set; }
        public byte[] Image { get; set; }
        public string FullName { get; set; }       
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public int Status { get; set; }
        public int? Nutrition { get; set; }
        public string Solution { get; set; }
        public string HistoryYourSelf { get; set; }
        public int Month { get; set; }
    }
}