﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSMonthlyGrowthArea
{
    public class SCSMonthlyGrowthConstants
    {
        public const string LIST_CLASS = "lst class by permission";
        public const string LIST_PUPIL = "lst pupil of class";
        public const string LIST_CHART = "lst monitoring book";
        public const string LIST_CHART_CLASS = "lst monitoring book of class";
        public const string CLASSID_SELECT = "classID select";
        public const string FULL_NAME = "nam of pupil";
        public const string BIRTH_DAY = "birthday of pupil";
        public const string GENRE = "genre of pupil";
        public const string CLASS_NAME = "class of pupil";
        public const string CLASS_ID = "classid of pupil";
        public const string HISTORY_YOUR_SELF = "history yourseft";
        public const string PUPIL_ID = "id of pupil";
        public const string LIST_MONTH = "lst 12 month";
        public const string LIST_EDUCATION_LEVEL = "List of levelid";
        public const string FIRST_LEVEL_SELECTED = "First of level";
    }
}