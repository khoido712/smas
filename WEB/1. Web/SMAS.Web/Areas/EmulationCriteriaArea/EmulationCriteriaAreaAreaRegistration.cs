﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmulationCriteriaArea
{
    public class EmulationCriteriaAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmulationCriteriaArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmulationCriteriaArea_default",
                "EmulationCriteriaArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
