/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.EmulationCriteriaArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.EmulationCriteriaArea.Controllers
{
    public class EmulationCriteriaController : BaseController
    {        
        private readonly IEmulationCriteriaBusiness EmulationCriteriaBusiness;
		
		public EmulationCriteriaController (IEmulationCriteriaBusiness emulationcriteriaBusiness)
		{
			this.EmulationCriteriaBusiness = emulationcriteriaBusiness;
		}
		
		//
        // GET: /EmulationCriteria/

        public ActionResult Index()
        {
            SetViewDataPermission("EmulationCriteria", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here
            SearchInfo["IsActive"] = true;
            IEnumerable<EmulationCriteriaViewModel> lst = this._Search(SearchInfo);
            ViewData[EmulationCriteriaConstants.LIST_EMULATIONCRITERIA] = lst;
            return View();
        }

		//
        // GET: /EmulationCriteria/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["Resolution"] = frm.Resolution;
			//add search info
			//

            IEnumerable<EmulationCriteriaViewModel> lst = this._Search(SearchInfo);
            ViewData[EmulationCriteriaConstants.LIST_EMULATIONCRITERIA] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("EmulationCriteria", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmulationCriteria emulationcriteria = new EmulationCriteria();
           // emulationcriteria.Mark = 
            TryUpdateModel(emulationcriteria);
            GlobalInfo g = new GlobalInfo();
            emulationcriteria.SchoolID = g.SchoolID;
            emulationcriteria.IsActive = true;
            Utils.Utils.TrimObject(emulationcriteria);
            if (emulationcriteria.Mark <= 0 || emulationcriteria.Mark >= 100)
            {
                throw new BusinessException("Common_Validate_MarkEmulation");
            }
            this.EmulationCriteriaBusiness.Insert(emulationcriteria);
            this.EmulationCriteriaBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EmulationCriteriaId)
        {
            this.CheckPermissionForAction(EmulationCriteriaId, "EmulationCriteria");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("EmulationCriteria", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmulationCriteria emulationcriteria = this.EmulationCriteriaBusiness.Find(EmulationCriteriaId);
            TryUpdateModel(emulationcriteria);
            
            Utils.Utils.TrimObject(emulationcriteria);
            if (emulationcriteria.Mark == 0 || emulationcriteria.Mark == 100)
            {
                throw new BusinessException("Common_Validate_MarkEmulation");
            }
            this.EmulationCriteriaBusiness.Update(emulationcriteria);
            this.EmulationCriteriaBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)

        {
            this.CheckPermissionForAction(id, "EmulationCriteria");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("EmulationCriteria", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            int SchoolID = GlobalInfo.SchoolID.Value;
            this.EmulationCriteriaBusiness.Delete(id, SchoolID);
            this.EmulationCriteriaBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EmulationCriteriaViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("EmulationCriteria", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            GlobalInfo g = new GlobalInfo();
            int SchoolID = g.SchoolID.Value;
            IQueryable<EmulationCriteria> query = this.EmulationCriteriaBusiness.SearchBySchool(SchoolID, SearchInfo);
            IQueryable<EmulationCriteriaViewModel> lst = query.Select(o => new EmulationCriteriaViewModel {               
						EmulationCriteriaId = o.EmulationCriteriaId,								
						Resolution = o.Resolution,								
						Mark = o.Mark								
                        //SchoolID = o.SchoolID,								
                        //CreatedDate = o.CreatedDate,								
                        //IsActive = o.IsActive,								
                        //ModifiedDate = o.ModifiedDate								
					
				
            });
            return lst.ToList().OrderBy(p=> SMAS.Business.Common.Utils.SortABC(p.Resolution));
        }        
    }
}





