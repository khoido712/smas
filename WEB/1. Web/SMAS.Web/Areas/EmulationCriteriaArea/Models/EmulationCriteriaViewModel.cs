﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.EmulationCriteriaArea.Models
{
    public class EmulationCriteriaViewModel
    {
         [ScaffoldColumn(false)]
		public System.Int32 EmulationCriteriaId { get; set; }
         [ResourceDisplayName("EmulationCriteria_Label_Resolution")]
         [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
         [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]					
		public System.String Resolution { get; set; }

         //[ResDisplayName("EmulationCriteria_Label_Mark")]
         //[AdditionalMetadata("class", "decimal")]
         //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
         //[Range(0, 100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
         //[RegularExpression(@"^[0-9]+(\,[0-9]{1,2})?$", ErrorMessage = "Price must can't have more than 2 decimal places")]
         [ResourceDisplayName("EmulationCriteria_Label_Mark")]
        // [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
         [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
         //[AdditionalMetadata("class", "validtext")]
        public System.Decimal? Mark { get; set; }
         [ScaffoldColumn(false)]			
       
        
        public System.Nullable<System.Int32> SchoolID { get; set; }								
        //public System.Nullable<System.DateTime> CreatedDate { get; set; }								
        //public System.Boolean IsActive { get; set; }								
        //public System.Nullable<System.DateTime> ModifiedDate { get; set; }								
	       
    }
}


