﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.BookRecordHeadTeachersArea;
using SMAS.Business.BusinessObject;
using System.Threading;
using System.Globalization;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.BookRecordHeadTeachersArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class BookRecordHeadTeachersController : BaseController
    {
        //
        // GET: /BookRecordHeadTeachersArea/BookRecordHeadTeachers/

        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IMonthCommentsBusiness MonthCommentsBusiness;
        private readonly IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        private readonly IBookmarkedFunctionBusiness BookmarkedFunctionBusiness;

        public BookRecordHeadTeachersController(IPupilProfileBusiness pupilProfileBusiness,
                                        ISummedUpRecordBusiness summedUpRecordBusiness,
                                        IAcademicYearBusiness academicYearBusiness,
                                        ISemeterDeclarationBusiness semeterDeclarationBusiness,
                                        IMarkRecordBusiness markrecordBusiness,
                                        ISubjectCatBusiness subjectCatBusiness,
                                        ITeachingAssignmentBusiness teachingAssignmentBusiness,
                                        IClassProfileBusiness classProfileBusiness,
                                        IClassSubjectBusiness classSubjectBusiness,
                                        IPeriodDeclarationBusiness PeriodDeclarationBusiness,
                                        IPupilOfClassBusiness PupilOfClassBusiness,
                                        IMarkTypeBusiness MarkTypeBusiness,
                                        IExemptedSubjectBusiness exemptedSubjectBusiness,
                                        IUserAccountBusiness UserAccountBusiness,
                                        IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness,
                                        IMonthCommentsBusiness monthCommentsBusiness,
                                        IEvaluationCriteriaBusiness EvaluationCriteriaBusiness,
                                        IBookmarkedFunctionBusiness BookmarkedFunctionBusiness)
        {
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.SemesterDeclarationBusiness = semeterDeclarationBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.MarkRecordBusiness = markrecordBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
            this.MonthCommentsBusiness = monthCommentsBusiness;
            this.EvaluationCriteriaBusiness = EvaluationCriteriaBusiness;
            this.BookmarkedFunctionBusiness = BookmarkedFunctionBusiness;
        }

        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            SetViewData(ClassID);
            return View();
        }

        public void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                    semester1 = true;
                else semester2 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[BookRecordHeadTeachersConstant.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[BookRecordHeadTeachersConstant.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[BookRecordHeadTeachersConstant.LIST_CLASS] = listClass;
                var listCP = listClass.ToList();
                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            // Ưu tiên trước đối với giáo viên bộ môn
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                            List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                            {
                                cp = listSubjectTeacher.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                        .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listHead != null && listHead.Count > 0)
                                {
                                    cp = listHead.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            //lay list mon hoc
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (cp != null)
            {
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && cp.ClassProfileID > 0)
                {
                    lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, _globalInfo.Semester.Value, cp.ClassProfileID, _globalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                        //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .ToList();
                }
            }
            else
            {
                ViewData[BookRecordHeadTeachersConstant.CLASS_NULL] = true;
            }
            ViewData[BookRecordHeadTeachersConstant.SubjectID] = lsClassSubject.Select(c => c.SubjectID).FirstOrDefault();

            ViewData[BookRecordHeadTeachersConstant.LIST_SUBJECT] = null;

            ViewData[BookRecordHeadTeachersConstant.ShowList] = false;

            ViewData[BookRecordHeadTeachersConstant.HAS_ERROR_DATA] = false;
            ViewData[BookRecordHeadTeachersConstant.LIST_IMPORTDATA] = null;

            ViewData[BookRecordHeadTeachersConstant.CLASS_12] = false;
            ViewData[BookRecordHeadTeachersConstant.SEMESTER_1] = true;
            ViewData[BookRecordHeadTeachersConstant.ERROR_BASIC_DATA] = false;
            ViewData[BookRecordHeadTeachersConstant.ERROR_IMPORT_MESSAGE] = "";
            ViewData[BookRecordHeadTeachersConstant.ERROR_IMPORT] = true;
            ViewData[BookRecordHeadTeachersConstant.ENABLE_ENTRYMARK] = "true";

            ViewData[BookRecordHeadTeachersConstant.FIRT_SEMESTER_STARTDATE] = objAca.FirstSemesterStartDate;
            ViewData[BookRecordHeadTeachersConstant.FIRT_SEMESTER_ENDDATE] = objAca.FirstSemesterEndDate;
            ViewData[BookRecordHeadTeachersConstant.SECOND_SEMESTER_STARTDATE] = objAca.SecondSemesterStartDate;
            ViewData[BookRecordHeadTeachersConstant.SECOND_SEMESTER_ENDDATE] = objAca.SecondSemesterEndDate;
            int semesterID = semester1 == true ? SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST : SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            List<MonthComments> monthList = GetMonthBySemester(semesterID);
            int monthSelectID = this.GetMonthSelectID(semesterID);
            ViewData[BookRecordHeadTeachersConstant.MONTH_SELECT] = monthSelectID;
            ViewData[BookRecordHeadTeachersConstant.LIST_MONTH] = monthList;
        }

        [HttpPost]
        //[CacheFilter(Duration = 60)]
        public PartialViewResult GetSubjectPanel()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int TypeID = SMAS.Business.Common.Utils.GetInt(Request["TypeSelectID"]);
            int semester = semesterid == null ? 0 : semesterid.Value;
            ViewData[BookRecordHeadTeachersConstant.ClassID] = classid.Value;
            #region LoadSubject Tab Mon hoc va HDGD
            if (TypeID == 1)//Mon hoc va HDGD
            {
                //cho phep tai khoan xem tat ca sanh sach mon hoc
                bool ViewAll = false;

                if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
                {
                    ViewAll = true;
                }
                //Chiendd1: 06/07/2015
                //Bo sung dieu kien partition cho bang TeachingAssignmentBusiness
                int schoolId = _globalInfo.SchoolID.Value;
                List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
                if (classid.HasValue && semesterid.HasValue)
                {
                    lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semesterid.Value, classid.Value, ViewAll).Where(u => semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                      join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                      where s.IsActive == true
                                      && s.IsApprenticeshipSubject == false
                                      select new SubjectCatBO()
                                      {
                                          SubjectCatID = s.SubjectCatID,
                                          DisplayName = s.DisplayName,
                                          IsCommenting = cs.IsCommenting,
                                          OrderInSubject = s.OrderInSubject
                                      }
                                      ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                    Session["ListSubject"] = lsClassSubject;
                }
                List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();
                int UserAccountID = _globalInfo.UserAccountID;

                // Neu la admin truong thi hien thi het
                if (new GlobalInfo().IsAdminSchoolRole)
                {
                    foreach (SubjectCatBO item in lsClassSubject)
                    {
                        listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, false));
                    }
                }
                else
                {
                    int? TeacherID = UserAccountBusiness.Find(UserAccountID).EmployeeID;
                    List<int> listPermitSubjectID = new List<int>();
                    if (TeacherID.HasValue)
                    {
                        // Phan cong giao vu la giao vien bo mon
                        if (ClassSupervisorAssignmentBusiness.All.Where(
                                                                o => o.TeacherID == TeacherID &&
                                                                o.ClassID == classid &&
                                                                o.AcademicYearID == _globalInfo.AcademicYearID &&
                                                                o.SchoolID == _globalInfo.SchoolID &&
                                                                o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER).Count() > 0
                                                                )
                        {
                            listPermitSubjectID = lsClassSubject.Select(o => o.SubjectCatID).ToList();
                        }
                        else
                        {
                            // Khong phai lai giao vien phan cong giao vu thi kiem tra la giao vien bo mon
                            /*listPermitSubjectID = TeachingAssignmentBusiness.All.Where(
                                                                o => o.TeacherID == TeacherID &&
                                                                o.ClassID == classid &&
                                                                o.Semester == semester &&
                                                                o.IsActive).Select(o => o.SubjectID).ToList();*/
                            IDictionary<string, object> searchInfo = new Dictionary<string, object>();
                            searchInfo["ClassID"] = classid;
                            searchInfo["TeacherID"] = TeacherID;
                            searchInfo["Semester"] = semester;
                            searchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                            searchInfo["IsActive"] = true;
                            listPermitSubjectID = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfo).Select(o => o.SubjectID).ToList();
                        }
                    }
                    foreach (SubjectCatBO item in lsClassSubject)
                    {
                        // todo: haivt9 review code, GetTypeSubject su dung Session["ListSubject"] cung chinh la lsClassSubject, mo 2 tab -> sai typeSubject -> fix su dung lsClassSubject
                        bool typeSubject = this.GetTypeSubject(item.SubjectCatID) == SubjectMark;
                        if (listPermitSubjectID.Contains(item.SubjectCatID))
                        {
                            listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, true, typeSubject));
                        }
                        else
                        {
                            listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, typeSubject));
                        }
                    }
                }
                ViewData["ListSubjectComment"] = lsClassSubject;
                ViewData[BookRecordHeadTeachersConstant.LIST_SUBJECT] = listSJ;
                ViewData[BookRecordHeadTeachersConstant.ShowList] = false;
            }
            #endregion
            #region Load Tieu chi Tab Nang luc pham chat
            else if (TypeID == 2)//Tab nang luc
            {
                List<EvaluationCriteria> listEvaluationCriteria = EvaluationCriteriaBusiness.getEvaluationCriteria().Where(p => p.TypeID == 2).ToList();
                ViewData[BookRecordHeadTeachersConstant.LIST_EVALUATION_CRITERIA] = listEvaluationCriteria;
            }
            else//Tab pham chat
            {
                List<EvaluationCriteria> listEvaluationCriteria = EvaluationCriteriaBusiness.getEvaluationCriteria().Where(p => p.TypeID == 3).ToList();
                ViewData[BookRecordHeadTeachersConstant.LIST_EVALUATION_CRITERIA] = listEvaluationCriteria;
            }
            #endregion
            ViewData[BookRecordHeadTeachersConstant.TYPE_SELECT_ID] = TypeID;
            return PartialView("_ViewSubject");
        }

        public const int SubjectMark = 1;
        public const int SubjectJudge = 2;

        [HttpPost]
        public int GetTypeSubject(int? SubjectID)
        {
            int typeSubject = 0;
            //const 1 mon tinh diem va mon tinh diem ket hop nhan xet; 2 mon nhan xet

            if (Session["ListSubject"] != null)
            {
                List<SubjectCatBO> lsClassSubject = Session["ListSubject"] as List<SubjectCatBO>;

                var lclassSubject = lsClassSubject.Where(o => o.SubjectCatID == SubjectID.Value);

                if (lclassSubject != null && lclassSubject.Count() > 0)
                {
                    SubjectCatBO classSubject = lclassSubject.FirstOrDefault();

                    if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        return SubjectMark;
                    }
                    else
                    {
                        return SubjectJudge;
                    }
                }
            }
            return typeSubject;
        }

        public JsonResult AjaxLoadMonthID(int semesterID)
        {
            List<MonthComments> monthList = GetMonthBySemester(semesterID);
            int monthSelectID = this.GetMonthSelectID(semesterID);
            return Json(new SelectList(monthList, "MonthID", "Name",monthSelectID));
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboPeriod(int? SemesterID)
        {
            if (SemesterID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                dic["Semester"] = SemesterID.Value;
                IEnumerable<PeriodDeclaration> listPeriod = PeriodDeclarationBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
                List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
                if (listPeriod != null && listPeriod.Count() > 0)
                {
                    foreach (var item in listPeriod)
                    {
                        PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                        periodDeclaration = item;
                        if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                            periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                        listPeriodDeclaration.Add(periodDeclaration);
                    }
                }
                ViewData[BookRecordHeadTeachersConstant.LIST_SEMESTER] = SemesterID.Value;
                ViewData[BookRecordHeadTeachersConstant.ShowList] = false;
                return Json(new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution"));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Lay thang theo hoc ki
        /// </summary>
        /// <param name="semesterID"></param>
        /// <returns></returns>
        private List<MonthComments> GetMonthBySemester(int semesterID)
        {
            List<MonthComments> monthList = new List<MonthComments>();
            if (semesterID == 1)
            {
                monthList = MonthCommentsBusiness.getMonthCommentsList().Where(p => (p.MonthID >= BookRecordHeadTeachersConstant.MONTH_1 && p.MonthID <= BookRecordHeadTeachersConstant.MONTH_5) || p.MonthID == BookRecordHeadTeachersConstant.MONTH_11).ToList();
            }
            else
            {
                monthList = MonthCommentsBusiness.getMonthCommentsList().Where(p => (p.MonthID >= BookRecordHeadTeachersConstant.MONTH_6 && p.MonthID <= BookRecordHeadTeachersConstant.MONTH_10) || p.MonthID == BookRecordHeadTeachersConstant.MONTH_11).ToList();
            }
            return monthList;
        }

        private int GetMonthSelectID(int semesterID)
        {
            int monthID = 0;
            DateTime dateTimeNow = DateTime.Now.Date;
            int monthCurrent = DateTime.Now.Month;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (semesterID == 1)
            {

                if (dateTimeNow <= objAca.FirstSemesterStartDate)
                {
                    monthID = 1;
                }
                else if (dateTimeNow >= objAca.FirstSemesterEndDate)
                {
                    monthID = 5;
                }
                else if (monthCurrent == 8)
                {
                    monthID = 1;
                }
                else if (monthCurrent == 9)
                {
                    monthID = 2;
                }
                else if (monthCurrent == 10)
                {
                    monthID = 3;
                }
                else if (monthCurrent == 11)
                {
                    monthID = 4;
                }
                else if (monthCurrent == 12)
                {
                    monthID = 5;
                }
                else if (monthCurrent < 8)
                {
                    monthID = 1;
                }
                else
                {
                    monthID = 5;
                }
            }
            else if (semesterID == 2)
            {
                if (dateTimeNow <= objAca.SecondSemesterStartDate)
                {
                    monthID = 6;
                }
                else if (dateTimeNow >= objAca.SecondSemesterEndDate)
                {
                    monthID = 10;
                }
                else if (monthCurrent == 1)
                {
                    monthID = 6;
                }
                else if (monthCurrent == 2)
                {
                    monthID = 7;
                }
                else if (monthCurrent == 3)
                {
                    monthID = 8;
                }
                else if (monthCurrent == 4)
                {
                    monthID = 9;
                }
                else if (monthCurrent == 5)
                {
                    monthID = 10;
                }
                else if (monthCurrent > 5)
                {
                    monthID = 10;
                }
                else
                {
                    monthID = 6;
                }
            }
            return monthID;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddMenuUsual(string url)
        {
            Menu objMenu = SMAS.Business.Common.StaticData.ListMenu.Where(c => c.URL.Contains(url)).FirstOrDefault();

            List<BookmarkedFunction> lstBm = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.GetValueOrDefault(),
                _globalInfo.AcademicYearID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID);

            BookmarkedFunction objDB = lstBm.Where(p => p.MenuID == objMenu.MenuID).FirstOrDefault();

            if (objDB == null)
            {
                BookmarkedFunction bf = new BookmarkedFunction();
                bf.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bf.AcademicYearID = _globalInfo.AcademicYearID;
                bf.LevelID = _globalInfo.AppliedLevel;
                bf.UserID = _globalInfo.UserAccountID;
                bf.MenuID = objMenu.MenuID;
                bf.Semester = _globalInfo.Semester;
                string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", objMenu.MenuName);
                bf.FunctionName = string.IsNullOrEmpty(name) ? objMenu.MenuName : name;
                bf.Order = lstBm.Count + 1;
                bf.CreatedDate = DateTime.Now;

                BookmarkedFunctionBusiness.Insert(bf);
                BookmarkedFunctionBusiness.Save();
            }
            return Json(new JsonMessage("Đã thêm vào danh sách chức năng thường dùng.", "success"));
        }
    }
}
