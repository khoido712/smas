﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.BookRecordHeadTeachersArea
{
    public class BookRecordHeadTeachersAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BookRecordHeadTeachersArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BookRecordHeadTeachersArea_default",
                "BookRecordHeadTeachersArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
