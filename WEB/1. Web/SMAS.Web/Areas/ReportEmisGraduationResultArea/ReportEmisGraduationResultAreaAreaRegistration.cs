﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportEmisGraduationResultArea
{
    public class ReportEmisGraduationResultAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportEmisGraduationResultArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportEmisGraduationResultArea_default",
                "ReportEmisGraduationResultArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
