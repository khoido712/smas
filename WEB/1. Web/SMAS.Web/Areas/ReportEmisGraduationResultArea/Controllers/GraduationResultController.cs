﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using System.Threading;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.ReportEmisGraduationResultArea.Controllers
{
    public class GraduationResultController : ThreadController
    {
        
        public static int CountdownEvent = 0;
        

        public ActionResult Index()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            this.StartThreadForLongAction("thread" + CountdownEvent, new LongAction(LongTimeAction), dic);

            return View();
        }

        private void LongTimeAction(IDictionary<string, object> dic)
        {
            CountdownEvent = 0;
            for (int i = 0; i < 50; i++)
            {
                Thread.Sleep(new TimeSpan(0, 0, 1));
                CountdownEvent = i;
            }
            CountdownEvent = 0;
        }


        [ValidateAntiForgeryToken]
        public JsonResult Status(int hehe)
        {
            List<string> list = new List<string>();

            List<Thread> listThread = ThreadManager.getInstance().GetAllThread();

            foreach (Thread t in listThread)
            {
                list.Add(t.Name);
            }

            string s = string.Join("\n", list);
            return Json(s, JsonRequestBehavior.AllowGet);
        }

    }
}
