﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.GraduationLevelArea
{
    public class GraduationLevelAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "GraduationLevelArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "GraduationLevelArea_default",
                "GraduationLevelArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
