/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.GraduationLevelArea.Models
{
    public class GraduationLevelViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 GraduationLevelID { get; set; }

        [ResourceDisplayName("GraduationLevel_Label_Resolution")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey",GraduationLevelConstants.LIST_Resolution)]        
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
		public System.String Resolution { get; set; }

        [ResourceDisplayName("GraduationLevel_Label_AppliedLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", GraduationLevelConstants.LIST_AppliedLevel)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]				
		public System.Nullable<System.Int32> AppliedLevel { get; set; }

        [ResourceDisplayName("GraduationLevel_Label_FullfilmentStatus")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", GraduationLevelConstants.LIST_FullfilmentStatus)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]					
		public System.Int32 FullfilmentStatus { get; set; }

        [ScaffoldColumn(false)]					
		public System.Nullable<System.DateTime> CreatedDate { get; set; }

        [ScaffoldColumn(false)]	
		public System.Boolean IsActive { get; set; }

        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        //[ScaffoldColumn(false)]
        [ResourceDisplayName("GraduationLevel_Label_AppliedLevel")]        
        public String  AppliedLevelName { get; set; }

        [ResourceDisplayName("GraduationLevel_Label_FullfilmentStatus")]
        public System.String FullfilmentStatusName { get; set; }
    }
}


