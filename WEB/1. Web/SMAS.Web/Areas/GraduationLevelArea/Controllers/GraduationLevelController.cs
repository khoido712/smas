﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.GraduationLevelArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.GraduationLevelArea.Controllers
{
    public class GraduationLevelController : BaseController
    {
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;

        public GraduationLevelController(IGraduationLevelBusiness graduationlevelBusiness)
        {
            this.GraduationLevelBusiness = graduationlevelBusiness;
        }
        #region Index
        //
        // GET: /GraduationLevel/

        public ActionResult Index()
        {
            //Dua du lieu ra combobox Cap hoc
            List<ComboObject> listAppliedLevel = new List<ComboObject>();
            //listSex.Add(new ComboObject("0", Res.Get("Common_Combo_All")));
            listAppliedLevel.Add(new ComboObject("1", Res.Get("Common_Label_Primary")));
            listAppliedLevel.Add(new ComboObject("2", Res.Get("Common_Label_Secondary")));
            listAppliedLevel.Add(new ComboObject("3", Res.Get("Common_Label_Tertiary")));
            listAppliedLevel.Add(new ComboObject("4", Res.Get("Common_Label_Childcare")));
            listAppliedLevel.Add(new ComboObject("5", Res.Get("Common_Label_Children")));
            ViewData[GraduationLevelConstants.LIST_AppliedLevel] = new SelectList(listAppliedLevel, "key", "value");

            //Dua du lieu ra combobox trinh do cap hoc
            List<ComboObject> listResolution = new List<ComboObject>();
            //listSex.Add(new ComboObject("0", Res.Get("Common_Combo_All")));
            listResolution.Add(new ComboObject(Res.Get("Common_Label_Doctoral"), Res.Get("Common_Label_Doctoral")));
            listResolution.Add(new ComboObject(Res.Get("Common_Label_Masters"), Res.Get("Common_Label_Masters")));
            listResolution.Add(new ComboObject(Res.Get("Common_Label_University"), Res.Get("Common_Label_University")));
            listResolution.Add(new ComboObject(Res.Get("Common_Label_College"), Res.Get("Common_Label_College")));
            listResolution.Add(new ComboObject(Res.Get("Common_Label_Intermediate"), Res.Get("Common_Label_Intermediate")));
            listResolution.Add(new ComboObject(Res.Get("Common_Label_Elementary"), Res.Get("Common_Label_Elementary")));
            ViewData[GraduationLevelConstants.LIST_Resolution] = new SelectList(listResolution, "key", "value");

            //Dua du lieu ra combobox loại trình độ đào tạo
            List<ComboObject> listStatus = new List<ComboObject>();
            //listSex.Add(new ComboObject("0", Res.Get("Common_Combo_All")));
            listStatus.Add(new ComboObject("1", Res.Get("Common_Label_Under")));
            listStatus.Add(new ComboObject("2", Res.Get("Common_Label_standard")));
            listStatus.Add(new ComboObject("3", Res.Get("Common_Label_Beyond")));
            ViewData[GraduationLevelConstants.LIST_FullfilmentStatus] = new SelectList(listStatus, "key", "value");

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //Get view data here

            IEnumerable<GraduationLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[GraduationLevelConstants.LIST_GRADUATIONLEVEL] = lst;
            return View();
        }
        #endregion


        //
        // GET: /GraduationLevel/Search

        #region Search
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            SearchInfo["AppliedLevel"] = frm.AppliedLevel;
            SearchInfo["Resolution"] = frm.Resolution;

            IEnumerable<GraduationLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[GraduationLevelConstants.LIST_GRADUATIONLEVEL] = lst;

            //Get view data here

            return PartialView("_List");
        }
        #endregion

        #region Create
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GraduationLevel graduationlevel = new GraduationLevel();
            TryUpdateModel(graduationlevel);
            graduationlevel.IsActive = true;
            Utils.Utils.TrimObject(graduationlevel);

            this.GraduationLevelBusiness.Insert(graduationlevel);
            this.GraduationLevelBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int GraduationLevelID)
        {
            GraduationLevel graduationlevel = this.GraduationLevelBusiness.Find(GraduationLevelID);
            TryUpdateModel(graduationlevel);
            Utils.Utils.TrimObject(graduationlevel);
            this.GraduationLevelBusiness.Update(graduationlevel);
            this.GraduationLevelBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        #region
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.GraduationLevelBusiness.Delete(id);
            this.GraduationLevelBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<GraduationLevelViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<GraduationLevel> query = this.GraduationLevelBusiness.Search(SearchInfo);
            IQueryable<GraduationLevelViewModel> lst = query.Select(o => new GraduationLevelViewModel
            {
                GraduationLevelID = o.GraduationLevelID,
                Resolution = o.Resolution,
                AppliedLevel = o.AppliedLevel,
                FullfilmentStatus = o.FullfilmentStatus,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate
            });

            List<GraduationLevelViewModel> lst1 = lst.ToList();
            foreach (var item in lst1)
            {
                if (item.AppliedLevel == 1)
                {
                    item.AppliedLevelName = Res.Get("Common_Label_Primary");
                }
                if (item.AppliedLevel == 2)
                {
                    item.AppliedLevelName = Res.Get("Common_Label_Secondary");
                }
                if (item.AppliedLevel == 3)
                {
                    item.AppliedLevelName = Res.Get("Common_Label_Tertiary");
                }
                if (item.AppliedLevel == 4)
                {
                    item.AppliedLevelName = Res.Get("Common_Label_Childcare");
                }
                if (item.AppliedLevel == 5)
                {
                    item.AppliedLevelName = Res.Get("Common_Label_Children");
                }
                if (item.FullfilmentStatus == 1)
                {
                    item.FullfilmentStatusName = Res.Get("Common_Label_Under");
                }
                if (item.FullfilmentStatus == 2)
                {
                    item.FullfilmentStatusName = Res.Get("Common_Label_standard");
                }
                if (item.FullfilmentStatus == 3)
                {
                    item.FullfilmentStatusName = Res.Get("Common_Label_Beyond");
                }
            }
            return lst1.ToList();
        }
        #endregion

    }
}




