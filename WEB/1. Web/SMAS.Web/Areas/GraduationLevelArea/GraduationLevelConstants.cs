/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.GraduationLevelArea
{
    public class GraduationLevelConstants
    {
        public const string LIST_GRADUATIONLEVEL = "listGraduationLevel";
        public const string LIST_AppliedLevel = "LIST_AppliedLevel";
        public const string LIST_Resolution = "LIST_Resolution";
        public const string LIST_FullfilmentStatus = "LIST_FullfilmentStatus";
    }
}