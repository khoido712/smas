﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryHeadTeacherArea
{
    public class MarkRecordOfPrimaryHeadTeacherConstant
    {
        public const string LIST_MARKRECORD = "listMarkRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "ListEducationLevel";
        public const string LIST_CLASS = "ListClass";
        public const string LIST_SUBJECT = "ListSubject";
        public const string LABEL_MES = "LabelMessage";
        public const string BOOL_TOAN = "BoolToan";
        public const string BOOL_MONKHAC = "BoolMonKhac";
        public const string BOOL_TIENGVIET = "BoolTiengViet";
        public const string BOOL_HKI = "BoolSemesterI";
        public const string BOOL_HKII = "BoolSemesterII";
        public const string IS_VISIBLED_BUTTON = "IsVisibledButton";


        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";
        public const string DISABLED_BUTTON = "DISABLED_BUTTON";
        public const string LOCK_MARK_TITLE = "LOCK_MARK_TITLE";

        public const string LIST_MONTH = "ListMonth";
        public const string LIST_PUPIL_EVALUATION = "ListPupilProfileEvaluation";
        public const string LIST_EVALUATION_CRITERIA = "ListEvaluationcriteria";

        public const int GVBM = 1;
        public const int GVCN = 2;

        public const int MONTH_EVALUATION_ENDING = 11;

        public const string MONTH_SELECT = "MONTH_SELECT";

        public const int MONTH_1 = 1;
        public const int MONTH_2 = 2;
        public const int MONTH_3 = 3;
        public const int MONTH_4 = 4;
        public const int MONTH_5 = 5;
        public const int MONTH_6 = 6;
        public const int MONTH_7 = 7;
        public const int MONTH_8 = 8;
        public const int MONTH_9 = 9;
        public const int MONTH_10 = 10;
        public const int MONTH_11 = 11;

        public const string SUBJECT_ID = "SubjectID";
        public const string CHECK_ISCOMMENTING = "CheckIsCommenting";
        public const string SEMESTER_ID = "SemesterID";
        public const string EVALUATION_CRITERIA_ID = "EvaluationCriteria";
        public const string CLASS_ID = "classId";
        public const string EDUCATION_LEVEL_ID = "EducationlevelID";

        public const string EVALUATION_ID = "EvaluationID";

        public const int TAB_EDUCATION = 1;
        public const int TAB_CAPACTIES = 2;
        public const int TAB_QUALITIES = 3;

        public const int LIMIT_COMMENTS = 500;

        /// <summary>
        /// tu phuc vu,tu quan
        /// </summary>
        public const int SeftServingManagement = 1;

        /// <summary>
        /// giao tiep, hop tac
        /// </summary>
        public const int CommunicationCoomperation = 2;

        /// <summary>
        ///	Tự học và giải quyết vấn đề
        /// </summary>
        public const int StudyAndSolveProblems = 3;

        /// <summary>
        /// 4	Chăm học, chăm làm, tích cực tham gia hoạt động giáo dục
        /// </summary>
        public const int HandWorking = 4;

        /// <summary>
        /// 5	Tự tin, tự trọng, tự chịu trách nhiệm
        /// </summary>
        public const int Confidence = 5;

        /// <summary>
        /// 6	Trung thực, kỉ luật, đoàn kết
        /// </summary>
        public const int Honesty = 6;

        /// <summary>
        /// 7	Yêu gia đình, bạn và những người khác; yêu trường, lớp, quê hương, đất nước
        /// </summary>
        public const int LoveFamily = 7;

        public const string FINISH = "Hoàn thành";

        public const string NOTFINISH = "Chưa hoàn thành";

        public const string SHORTFINISH = "HT";

        public const string SHORTNOTFINISH = "CHT";

        public const string OK = "Đạt";

        public const string NOT_OK = "Chưa đạt";

        public const string SHORT_OK = "Đ";

        public const string SHORT_NOT_OK = "CĐ";

        /// <summary>
        /// Tieu chi danh gia Tab nang luc
        /// </summary>
        public const int NUM_CRITERIA_CAPACITIES = 4;

        /// <summary>
        /// Tieu chi danh gia Tab pham chat
        /// </summary>
        public const int NUM_CRITERIA_QUALITIES = 5;


        public const string LOCK_MARK = "lockmark";

        public const string STR_DISABLED = "disabled='disabled'";

        public const string STR_M1 = "T1";
        public const string STR_M2 = "T2";
        public const string STR_M3 = "T3";
        public const string STR_M4 = "T4";
        public const string STR_M5 = "T5";
        public const string STR_M6 = "T6";
        public const string STR_M7 = "T7";
        public const string STR_M8 = "T8";
        public const string STR_M9 = "T9";
        public const string STR_M10 = "T10";
        public const string STR_ENDING_FIRST = "CK1";
        public const string STR_ENDING_SECOND = "CK2";
        public const string STR_ENDING_FIRST_SEMESTER = "HK1";
        public const string STR_ENDING_SECOND_SEMESTER = "HK2";

        /// <summary>
        /// quyen ban giam hieu
        /// </summary>
        public const string IS_ROLE_PRINCIPAL = "isRolePrincipal";

        public const string HEAD_TEACHE = "HeadTeacher";

        public const string CHECK_LOCK_SEMESTER = "CheckLockSemester";

        public const string CLASS_NAME = "LỚP";


        public const string TEMPLATE_FILE_MONTH = "BangDanhGiaThangLopK1_1HK1_GVCN.xls";
        public const string TEMPLATE_FILE_SEMESTER = "BangDanhGiaCuoiKyI_LopK1_1_GVCN.xls";
        public const int COLUMN_TITLE = 5;
        public const int ROW_TILTE = 5;
        public const int ROW_START = 6;
        public const int ROW_TILTE_CAPACITIES_HEAD_TEACHER = 5;
        public const int ROW_START_CAPACITIES_HEAD_TEACHER = 7;
        public const int COLUMN_START = 1;
        public const int COLUMN_PULIL_CODE = 2;
        public const int COLUMN_PULIL_NAME = 3;
        public const int COLUMN_BIRTHDAY = 4;
        public const int COLUMN_MONTH_1 = 5;
        public const int COLUMN_MONTH_2 = 6;
        public const int COLUMN_MONTH_3 = 7;
        public const int COLUMN_MONTH_4 = 8;
        public const int COLUMN_MONTH_5 = 9;
        public const int COLUMN_COMMENTS_ENDDING = 10;
        public const int COLUMN_END_MARK = 11;
        public const int COLUMN_ENDDING_EVALUATION = 12;
        public const int COLUMN_CAPACITIES_1 = 5;
        public const int COLUMN_CAPACITIES_2 = 6;
        public const int COLUMN_CAPACITIES_3 = 7;
        public const int COLUMN_CAPACITIES_ENDDING = 8;
        public const int COLUMN_QUALITIES_1 = 5;
        public const int COLUMN_QUALITIES_2 = 6;
        public const int COLUMN_QUALITIES_3 = 7;
        public const int COLUMN_QUALITIES_4 = 8;
        public const int COLUMN_QUALITIES_ENDDING = 9;

        public const string SHEET_NAME_CAPACITIES = "NangLuc";
        public const string SHEET_NAME_QUALITIES = "PhamChat";
        public const string SHEET_NAME_EDUCATION = "MonHoc&HĐGD";
        public const int FILE_UPLOAD_MAX_SIZE = 5120;

        public const int COPY_OVER_WRITE_ALL_PUPIL = 1;
        public const int COPY_OVER_WRITE_ALL_PUPIL_NOT_COMMENT = 2;
        public const int COPY_COMMNET_OF_MONTH = 1;
        public const int COPY_COMMNET_ALL_MONTH_IN_SEMESTER = 2;


        public const string LIST_COMMENT = "listComment";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string LAST_UPDATE_PERIOD_MARK = "LAST_UPDATE_PERIOD_MARK";
    }
}