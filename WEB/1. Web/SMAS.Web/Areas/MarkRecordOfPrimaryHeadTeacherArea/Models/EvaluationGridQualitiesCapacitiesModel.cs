﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryHeadTeacherArea.Models
{
    public class EvaluationGridQualitiesCapacitiesModel
    {
        public long EvaluationCommentsID { get; set; }
        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public string Birthday { get; set; }
        public int OrderInClass { get; set; }
        public long PupilID { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public string Comment4 { get; set; }
        public string Comment5 { get; set; }
        public int? PiriodicEndingMark { get; set; }
        public string EvaluationEnding { get; set; }
        public int? RetestMark { get; set; }
        public string EvaluationReTraining { get; set; }
        public int Status { get; set; }
        public string CommentsEnding { get; set; }
        public int MonthID_1 { get; set; }
        public int MonthID_2 { get; set; }
        public int MonthID_3 { get; set; }
        public int MonthID_4 { get; set; }
        public int MonthID_5 { get; set; }
        public long EvaluationCriteriaID { get; set; }
        public long SummedEvaluationID { get; set; }
        public long SummedEndingEvaluationID { get; set; }
        public bool Error { get; set; }
        public string MessageError { get; set; }


        /// <summary>
        /// cot 1
        /// </summary>
        public string Column_1 { get; set; }

        /// <summary>
        /// cot 1
        /// </summary>
        public string Column_2 { get; set; }

        /// <summary>
        /// cot 1
        /// </summary>
        public string Column_3 { get; set; }

        /// <summary>
        /// cot 1
        /// </summary>
        public string Column_4 { get; set; }

        /// <summary>
        /// cot danh gia
        /// </summary>
        public string Column_5 { get; set; }
    }
}