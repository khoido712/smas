﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryHeadTeacherArea.Models
{
    public class EvaluationGridModel
    {
        public long EvaluationCommentsID { get; set; }
        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public int OrderInClass { get; set; }
        public int PupilID { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public string Comment4 { get; set; }
        public string Comment5 { get; set; }
        public int? PiriodicEndingMark { get; set; }
        public string EvaluationEnding { get; set; }
        public int? RetestMark { get; set; }
        public string EvaluationReTraining { get; set; }
        public int Status { get; set; }
        public string CommentsEnding { get; set; }
        public int MonthID_1 { get; set; }
        public int MonthID_2 { get; set; }
        public int MonthID_3 { get; set; }
        public int MonthID_4 { get; set; }
        public int MonthID_5 { get; set; }
        public int EvaluationCriteriaID { get; set; }
        public long SummedEvaluationID { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public DateTime? MaxUpdateTime { get; set; }
        private bool _ischk1 = true;
        private bool _ischk2 = true;
        private bool _ischk3 = true;
        private bool _ischk4 = true;
        private bool _ischk5 = true;
        public bool ischk1
        {
            get { return _ischk1; }
            set 
            {
                _ischk1 = value;
            }
        }
        public bool ischk2
        {
            get { return _ischk2; }
            set
            {
                _ischk2 = value;
            }
        }
        public bool ischk3
        {
            get { return _ischk3; }
            set
            {
                _ischk3 = value;
            }
        }
        public bool ischk4
        {
            get { return _ischk4; }
            set
            {
                _ischk4 = value;
            }
        }
        public bool ischk5
        {
            get { return _ischk5; }
            set
            {
                _ischk5 = value;
            }
        }

        public long SummedEndingEvaluationID { get; set; }

        public bool Error { get; set; }
        public string MessageError { get; set; }

        /// <summary>
        /// biến dùng xác định là dòng đánh giá khi import (chỉ dùng cho import)
        /// </summary>
        public bool UserEvaluation { get; set; }


        public string StrDisabledPupilStudying { get; set; }
    }
}