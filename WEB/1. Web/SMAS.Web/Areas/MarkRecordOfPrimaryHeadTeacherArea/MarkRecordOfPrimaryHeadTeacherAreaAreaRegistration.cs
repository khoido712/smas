﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryHeadTeacherArea
{
    public class MarkRecordOfPrimaryHeadTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkRecordOfPrimaryHeadTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkRecordOfPrimaryHeadTeacherArea_default",
                "MarkRecordOfPrimaryHeadTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
