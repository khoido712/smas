﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MarkRecordOfPrimaryHeadTeacherArea;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Text;
using System.Configuration;
using SMAS.Web.Areas.MarkRecordOfPrimaryHeadTeacherArea.Models;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryHeadTeacherArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class MarkRecordOfPrimaryHeadTeacherController : BaseController
    {
        //
        // GET: /MarkRecordOfPrimaryHeadTeacherArea/MarkRecordOfPrimaryHeadTeacher/
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IVMarkRecordBusiness VMarkRecordBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly ILockMarkPrimaryBusiness LockMarkPrimaryBusiness;
        private readonly IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IMonthCommentsBusiness MonthCommentsBusiness;
        private readonly IEvaluationCommentsBusiness EvaluationCommentsBusiness;
        private readonly IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        private readonly IEvaluationCommentsHistoryBusiness EvaluationCommentsHistoryBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness;
        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private const string SPACE = "\r\n";


        public MarkRecordOfPrimaryHeadTeacherController(
            IPupilLeavingOffBusiness PupilLeavingOffBusiness,
            IClassMovementBusiness ClassMovementBusiness,
            ISchoolMovementBusiness SchoolMovementBusiness,
            IUserAccountBusiness UserAccountBusiness,
            IMarkRecordBusiness markrecordBusiness,
            IPeriodDeclarationBusiness PeriodDeclarationBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            IEducationLevelBusiness educationLevelBusiness,
            ITeachingAssignmentBusiness teachingAssignmentBusiness,
            ISubjectCatBusiness subjectCatBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IMarkTypeBusiness markTypeBusiness,
            IAcademicYearBusiness academicYearBusiness,
            ISummedUpRecordBusiness summedUpRecordBusiness,
            IExemptedSubjectBusiness exemptedSubjectBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IVMarkRecordBusiness VMarkRecordBusiness,
            IVSummedUpRecordBusiness VSummedUpRecordBusiness,
            ILockMarkPrimaryBusiness LockMarkPrimaryBusiness,
            IMarkRecordHistoryBusiness MarkRecordHistoryBusiness,
            ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness,
            IMonthCommentsBusiness MonthCommentsBusiness,
            IEvaluationCommentsBusiness EvaluationCommentsBusiness,
            IEvaluationCriteriaBusiness EvaluationCriteriaBusiness,
            IEvaluationCommentsHistoryBusiness EvaluationCommentsHistoryBusiness,
            ISummedEvaluationBusiness SummedEvaluationBusiness,
            ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness,
            IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness
            )
        {
            this.PupilLeavingOffBusiness = PupilLeavingOffBusiness;
            this.ClassMovementBusiness = ClassMovementBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.MarkRecordBusiness = markrecordBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.MarkTypeBusiness = markTypeBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.VSummedUpRecordBusiness = VSummedUpRecordBusiness;
            this.VMarkRecordBusiness = VMarkRecordBusiness;
            this.LockMarkPrimaryBusiness = LockMarkPrimaryBusiness;
            this.MarkRecordHistoryBusiness = MarkRecordHistoryBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
            this.MonthCommentsBusiness = MonthCommentsBusiness;
            this.EvaluationCommentsBusiness = EvaluationCommentsBusiness;
            this.EvaluationCriteriaBusiness = EvaluationCriteriaBusiness;
            this.EvaluationCommentsHistoryBusiness = EvaluationCommentsHistoryBusiness;
            this.SummedEvaluationBusiness = SummedEvaluationBusiness;
            this.SummedEndingEvaluationBusiness = SummedEndingEvaluationBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
        }

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetSubjectAndEducationGroup()
        {
            int evalutionID = 1;// tab dau tien
            int typeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN;//giao vienchu nhiem
            int classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]);
            int semester = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int subjectID = 0;
            int monthID = SMAS.Business.Common.Utils.GetInt(Request["monthID"]);
            bool isGetFirstSubject = !string.IsNullOrEmpty(SMAS.Business.Common.Utils.GetString(Request["isFirstSubject"])) ? Convert.ToBoolean(SMAS.Business.Common.Utils.GetString(Request["isFirstSubject"])) : false;
            //lay list thang
            List<MonthComments> monthList = GetMonthBySemester(semester);
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_MONTH] = monthList;
            //lay list mon hoc
            bool viewAll = _globalInfo.IsRolePrincipal ? true : _globalInfo.IsViewAll;
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && classID > 0)
            {
                lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semester, classID, viewAll)
                .Where(o => o.SubjectCat.IsActive == true)
                    //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                .ToList();
            }
            if (lsClassSubject != null && lsClassSubject.Count > 0)
            {
                if (monthID > 0 && monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                {
                    subjectID = SMAS.Business.Common.Utils.GetInt(Request["subjectID"]);
                    if (subjectID == 0 && isGetFirstSubject)
                    {
                        subjectID = lsClassSubject.Select(p => p.SubjectID).FirstOrDefault();
                    }
                }
            }
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_SUBJECT] = lsClassSubject;

            List<EvaluationGridModel> listEvaluationResult = new List<EvaluationGridModel>();

            if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11 && subjectID > 0)
            {
                listEvaluationResult = this.SearchEvaluation(classID, semester, subjectID, typeOfTeacher, evalutionID);
            }
            else if (monthID < MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11 && monthID > 0)
            {
                listEvaluationResult = this.SearchEvaluation(classID, semester, subjectID, typeOfTeacher, evalutionID);
            }
            //check quyen GVCN
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);

            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            DateTime? MaxUpdateTime = listEvaluationResult.Select(p => p.MaxUpdateTime).FirstOrDefault();
            string DateEqual = string.Empty;
            if (MaxUpdateTime.HasValue)
	        {
		        tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span>", new object[]{
                        MaxUpdateTime.Value.Hour.ToString(),
                        MaxUpdateTime.Value.Minute.ToString("D2"),
                        MaxUpdateTime});
                DateEqual = MaxUpdateTime.Value.ToString("dd/MM/yyyy hh:mm");
	        }
            
            strLastUpdate = !string.IsNullOrEmpty(tmpStr) ? string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr) : "";
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LAST_UPDATE_STRING] = strLastUpdate;

            List<int> lstLastUpdatePupilID = listEvaluationResult.Where(p => (p.UpdateTime.HasValue ? p.UpdateTime.Value.ToString("dd/MM/yyyy hh:mm") == DateEqual : p.CreateTime.ToString("dd/MM/yyyy hh:mm") == DateEqual)).Select(p => p.PupilID).Distinct().ToList();

            SetVisbileButton(semester, classID, subjectID);
            int IsCommenting = CheckSubjectCommenting(subjectID, classID);
            if (monthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                listEvaluationResult = this.SetCommentGVBM(listEvaluationResult, classID, evalutionID, semester, monthID);//lay nhan xet danh gia cua GVBM
            }
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_PUPIL_EVALUATION] = listEvaluationResult;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.MONTH_SELECT] = monthID;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.SUBJECT_ID] = subjectID;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.CHECK_ISCOMMENTING] = IsCommenting;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.SEMESTER_ID] = semester;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.CLASS_ID] = classID;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LOCK_MARK] = this.CheckLockMark(classID);
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_ROLE_PRINCIPAL] = _globalInfo.IsRolePrincipal ? 1 : 0;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.HEAD_TEACHE] = isGVCN;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LAST_UPDATE_PERIOD_MARK] = lstLastUpdatePupilID;
            
            //lay thong tin autocompelete
            IDictionary<string, object> dicComment = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",classID},
                //{"SemesterID",semester},
                {"TypeOfTeacherID",typeOfTeacher},
                {"EvaluationID",evalutionID}                
            };
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_COMMENT] = EvaluationCommentsBusiness.getListCommentAutoComplete(dicComment);
            return PartialView("_GetSubjectAndEducationGroup");
        }
        [HttpPost]
        public PartialViewResult GetCapacities()
        {
            int evalutionID = 2;// tab thu 2
            int typeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN;//giao vien bo mon
            int classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]);
            int semester = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int monthID = SMAS.Business.Common.Utils.GetInt(Request["monthID"]);
            int subjectID = 0;
            bool isGetFirstSubject = !string.IsNullOrEmpty(SMAS.Business.Common.Utils.GetString(Request["isFirstSubject"])) ? Convert.ToBoolean(SMAS.Business.Common.Utils.GetString(Request["isFirstSubject"])) : false;
            if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                List<EvaluationCriteria> listEvaluationCriteria = EvaluationCriteriaBusiness.getEvaluationCriteria().Where(p => p.TypeID == 2).ToList();
                subjectID = SMAS.Business.Common.Utils.GetInt(Request["subjectID"]);
                if (subjectID == 0 && isGetFirstSubject)
                {
                    subjectID = listEvaluationCriteria.Select(p => p.EvaluationCriteriaID).FirstOrDefault();
                }
            }
            List<EvaluationGridModel> listEvaluationResult = new List<EvaluationGridModel>();
            if ((monthID > 0 && monthID < MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11 && subjectID == 0) || (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11 && subjectID > 0))
            {
                listEvaluationResult = this.SearchEvaluation(classID, semester, subjectID, typeOfTeacher, evalutionID);
            }
           
            //check quyen GVCN
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);

            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            DateTime? MaxUpdateTime = listEvaluationResult.Select(p => p.MaxUpdateTime).FirstOrDefault();
            if (MaxUpdateTime.HasValue)
            {
                tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span>", new object[]{
                        MaxUpdateTime.Value.Hour.ToString(),
                        MaxUpdateTime.Value.Minute.ToString("D2"),
                        MaxUpdateTime});
            }

            strLastUpdate = !string.IsNullOrEmpty(tmpStr) ? string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr) : "";
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LAST_UPDATE_STRING] = strLastUpdate;

            SetVisbileButton(semester, classID, subjectID);
            if (monthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                listEvaluationResult = this.SetCommentGVBM(listEvaluationResult, classID, evalutionID, semester, monthID);//lay nhan xet danh gia cua GVBM
            }
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_PUPIL_EVALUATION] = listEvaluationResult;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.MONTH_SELECT] = monthID;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.SEMESTER_ID] = semester;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.EVALUATION_CRITERIA_ID] = subjectID;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LOCK_MARK] = this.CheckLockMark(classID);
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_ROLE_PRINCIPAL] = _globalInfo.IsRolePrincipal ? 1 : 0;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.HEAD_TEACHE] = isGVCN;
            //lay thong tin autocompelete
            IDictionary<string, object> dicComment = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",classID},
               // {"SemesterID",semester},
                {"TypeOfTeacherID",typeOfTeacher},
                {"EvaluationID",evalutionID}
            };
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_COMMENT] = EvaluationCommentsBusiness.getListCommentAutoComplete(dicComment);
            return PartialView("_GetCapacities");
        }
        [HttpPost]
        public PartialViewResult GetQualities()
        {
            int evalutionID = 3;// tab thu 3
            int typeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN;//giao vien bo mon
            int classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]);
            int semester = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int monthID = SMAS.Business.Common.Utils.GetInt(Request["monthID"]);
            int subjectID = 0;
            bool isGetFirstSubject = !string.IsNullOrEmpty(SMAS.Business.Common.Utils.GetString(Request["isFirstSubject"])) ? Convert.ToBoolean(SMAS.Business.Common.Utils.GetString(Request["isFirstSubject"])) : false;
            if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                List<EvaluationCriteria> listEvaluationCriteria = EvaluationCriteriaBusiness.getEvaluationCriteria().Where(p => p.TypeID == 3).ToList();
                subjectID = SMAS.Business.Common.Utils.GetInt(Request["subjectID"]);               
                if (subjectID == 0 && isGetFirstSubject)
                {
                    subjectID = listEvaluationCriteria.Select(p => p.EvaluationCriteriaID).FirstOrDefault();
                }
            }

            List<EvaluationGridModel> listEvaluationResult = new List<EvaluationGridModel>();
            if ((monthID > 0 && monthID < MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11 && subjectID == 0) || (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11 && subjectID > 0))
            {
                listEvaluationResult = this.SearchEvaluation(classID, semester, subjectID, typeOfTeacher, evalutionID);
            }

            //check quyen GVCN
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            SetVisbileButton(semester, classID, subjectID);

            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            DateTime? MaxUpdateTime = listEvaluationResult.Select(p => p.MaxUpdateTime).FirstOrDefault();
            if (MaxUpdateTime.HasValue)
            {
                tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span>", new object[]{
                        MaxUpdateTime.Value.Hour.ToString(),
                        MaxUpdateTime.Value.Minute.ToString("D2"),
                        MaxUpdateTime});
            }
            strLastUpdate = !String.IsNullOrEmpty(tmpStr) ? string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr) : "";
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LAST_UPDATE_STRING] = strLastUpdate;
            if (monthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                listEvaluationResult = this.SetCommentGVBM(listEvaluationResult, classID, evalutionID, semester, monthID);//lay nhan xet danh gia cua GVBM
            }
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_PUPIL_EVALUATION] = listEvaluationResult;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.MONTH_SELECT] = monthID;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.SEMESTER_ID] = semester;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.EVALUATION_CRITERIA_ID] = subjectID;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LOCK_MARK] = this.CheckLockMark(classID);
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_ROLE_PRINCIPAL] = _globalInfo.IsRolePrincipal ? 1 : 0;
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.HEAD_TEACHE] = isGVCN;
            //lay thong tin autocompelete
            IDictionary<string, object> dicComment = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",classID},
               // {"SemesterID",semester},
                {"TypeOfTeacherID",typeOfTeacher},
                {"EvaluationID",evalutionID}                
            };
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_COMMENT] = EvaluationCommentsBusiness.getListCommentAutoComplete(dicComment);
            return PartialView("_GetQualities");
        }

        /// <summary>
        /// Lay thong tin hoc sinh duoc danh gia
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="semester"></param>
        /// <param name="subjectID">ID môn học, nếu chọn Năng lực và phẩm chất thì subjectID là ID các mặt tiêu chí</param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="evalutionID"></param>
        /// <returns></returns>
        private List<EvaluationGridModel> SearchEvaluation(int classID, int semester, int subjectID, int typeOfTeacher, int evalutionID)
        {
            List<EvaluationCommentsBO> listEvaluation = EvaluationCommentsBusiness.getListEvaluationCommentsBySchool(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classID, semester, subjectID, typeOfTeacher, evalutionID).ToList();
            List<EvaluationGridModel> listEvaluationResult = (from list in listEvaluation
                                                              select new EvaluationGridModel
                                                              {
                                                                  FullName = list.FullName,
                                                                  Name = list.Name,
                                                                  PupilID = list.PupilID,
                                                                  PupilCode = list.PupilCode,
                                                                  Birthday = list.Birthday,
                                                                  Comment1 = list.Comment1,
                                                                  Comment2 = list.Comment2,
                                                                  Comment3 = list.Comment3,
                                                                  Comment4 = list.Comment4,
                                                                  Comment5 = list.Comment5,
                                                                  MonthID_1 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_1 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_6),
                                                                  MonthID_2 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_2 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_7),
                                                                  MonthID_3 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_3 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_8),
                                                                  MonthID_4 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_4 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_9),
                                                                  MonthID_5 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_5 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_10),
                                                                  PiriodicEndingMark = list.PiriodicEndingMark,
                                                                  EvaluationEnding = list.EvaluationEnding,
                                                                  RetestMark = list.RetestMark,
                                                                  EvaluationReTraining = list.EvaluationReTraining,
                                                                  Status = list.Status,
                                                                  CommentsEnding = list.CommentEnding,
                                                                  EvaluationCommentsID = list.EvaluationCommentsID,
                                                                  EvaluationCriteriaID = list.EvaluationCriteriaID,
                                                                  SummedEvaluationID = list.SummedEvaluationID,
                                                                  SummedEndingEvaluationID = list.SummedEndingEvaluationID,
                                                                  StrDisabledPupilStudying = (list.Status != GlobalConstants.PUPIL_STATUS_STUDYING ? MarkRecordOfPrimaryHeadTeacherConstant.STR_DISABLED : string.Empty),
                                                                  OrderInClass = list.OrderInClass,
                                                                  CreateTime = list.CreateTime,
                                                                  UpdateTime = list.UpdateTime,
                                                                  MaxUpdateTime = list.MaxUpdateTime
                                                              }).OrderBy(p => p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
            return listEvaluationResult;
        }


        private List<EvaluationGridModel> GetEvaluationCommentsExport(int classID, int semester, int subjectID, int typeOfTeacher, int evalutionID)
        {
            List<EvaluationCommentsBO> listEvaluation = EvaluationCommentsBusiness.getListEvaluationCommentsBySchoolExport(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classID, semester, subjectID, typeOfTeacher, evalutionID).ToList();
            List<EvaluationGridModel> listEvaluationResult = (from list in listEvaluation
                                                              select new EvaluationGridModel
                                                              {
                                                                  FullName = list.FullName,
                                                                  Name = list.Name,
                                                                  PupilID = list.PupilID,
                                                                  PupilCode = list.PupilCode,
                                                                  Birthday = list.Birthday,
                                                                  Comment1 = list.Comment1,
                                                                  Comment2 = list.Comment2,
                                                                  Comment3 = list.Comment3,
                                                                  Comment4 = list.Comment4,
                                                                  Comment5 = list.Comment5,
                                                                  MonthID_1 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_1 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_6),
                                                                  MonthID_2 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_2 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_7),
                                                                  MonthID_3 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_3 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_8),
                                                                  MonthID_4 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_4 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_9),
                                                                  MonthID_5 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryHeadTeacherConstant.MONTH_5 : MarkRecordOfPrimaryHeadTeacherConstant.MONTH_10),
                                                                  PiriodicEndingMark = list.PiriodicEndingMark,
                                                                  EvaluationEnding = list.EvaluationEnding,
                                                                  RetestMark = list.RetestMark,
                                                                  EvaluationReTraining = list.EvaluationReTraining,
                                                                  Status = list.Status,
                                                                  CommentsEnding = list.CommentEnding,
                                                                  EvaluationCommentsID = list.EvaluationCommentsID,
                                                                  EvaluationCriteriaID = list.EvaluationCriteriaID,
                                                                  OrderInClass = list.OrderInClass
                                                              }).OrderBy(p => p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
            return listEvaluationResult;
        }

        /// <summary>
        /// Lay thang theo hoc ki
        /// </summary>
        /// <param name="semesterID"></param>
        /// <returns></returns>
        private List<MonthComments> GetMonthBySemester(int semesterID)
        {
            List<MonthComments> monthList = new List<MonthComments>();
            if (semesterID == 1)
            {
                monthList = MonthCommentsBusiness.getMonthCommentsList().Where(p => (p.MonthID >= MarkRecordOfPrimaryHeadTeacherConstant.MONTH_1 && p.MonthID <= MarkRecordOfPrimaryHeadTeacherConstant.MONTH_5) || p.MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11).ToList();
            }
            else
            {
                monthList = MonthCommentsBusiness.getMonthCommentsList().Where(p => (p.MonthID >= MarkRecordOfPrimaryHeadTeacherConstant.MONTH_6 && p.MonthID <= MarkRecordOfPrimaryHeadTeacherConstant.MONTH_10) || p.MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11).ToList();
            }
            return monthList;
        }

        private int CheckSubjectCommenting(int subjectID, int ClassID)
        {
            int isCommenting = -1;
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            if (subjectID > 0 && ClassID > 0)
            {
                ClassSubject obj = ClassSubjectBusiness.All.Where(p => p.ClassID == ClassID && p.SubjectID == subjectID && p.Last2digitNumberSchool==partitionId).FirstOrDefault();
                if (obj != null)
                {
                    return obj.IsCommenting.HasValue ? obj.IsCommenting.Value : -1;
                }
            }
            return isCommenting;
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult SaveOneComments(string comments, int pupilID, int monthID, int subjectID, int classID, int educationLevelID, int semester, int tab)
        {
            EvaluationComments obj = new EvaluationComments();
            obj.PupilID = pupilID;
            obj.ClassID = classID;
            obj.EducationLevelID = educationLevelID;
            obj.AcademicYearID = _globalInfo.AcademicYearID.Value;
            obj.LastDigitSchoolID = _globalInfo.SchoolID.Value % GlobalConstants.PARTITION;
            obj.SchoolID = _globalInfo.SchoolID.Value;
            obj.TypeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN;
            obj.SemesterID = semester;
            if (tab == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
            {
                obj.EvaluationID = MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION;//danh gia CLGD
                obj.EvaluationCriteriaID = 0;
            }
            else if (tab == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES) //nang luc
            {
                obj.EvaluationID = MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES;
                obj.EvaluationCriteriaID = 0;// tieu chi danh gia
            }
            else
            {
                obj.EvaluationID = MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES;
                obj.EvaluationCriteriaID = 0;// tieu chi danh gia
            }
            if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_1 || monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_6)
            {
                obj.CommentsM1M6 = comments;
            }
            else if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_2 || monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_7)
            {
                obj.CommentsM2M7 = comments;
            }
            else if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_3 || monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_8)
            {
                obj.CommentsM3M8 = comments;
            }
            else if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_4 || monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_9)
            {
                obj.CommentsM4M9 = comments;
            }
            else if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_5 || monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_10)
            {
                obj.CommentsM5M10 = comments;
            }
            List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();

            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            if (UtilsBusiness.IsMoveHistory(aca))
            {
                lstActionAuditBO = EvaluationCommentsHistoryBusiness.InsertOrUpdateEvaluationCommentsByPupilIDForHeadTeacher(monthID, obj);
            }
            else
            {

                lstActionAuditBO = EvaluationCommentsBusiness.InsertOrUpdateEvaluationCommentsByPupilIDForHeadTeacher(monthID, obj);
            }


            //Ghi log du lieu:
            WriteLog(lstActionAuditBO, SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);

            return Json(new JsonMessage("Lưu thành công"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        [ActionAudit]
        public JsonResult SaveComentsEducationQuality(FormCollection frm)
        {
            //Ham save dung chung cho 3 tab
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int partitionNumber = _globalInfo.SchoolID.Value % GlobalConstants.PARTITION;
            int evaluationID = Convert.ToInt32(frm["Tab"]);
            int semesterID = Convert.ToInt32(frm["HfSemesterID"]);
            int classID = Convert.ToInt32(frm["HfClassID"]);
            int subjectID = 0;
            int MonthID = Convert.ToInt32(frm["HfMonthID"]);
            if (MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                subjectID = Convert.ToInt32(frm["HfSubjectID"]);
            }
            #region check permision
            bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            if (!(isSchoolAdmin || isGVCN))
            {
                return Json(new JsonMessage("Thầy/cô không có quyền thao tác chức năng này"));
            }
            #endregion
            int educationLevelID = Convert.ToInt32(frm["HfEducationLevelID"]);
            int typeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN;
            List<EvaluationCommentsBO> evaluationList = EvaluationCommentsBusiness.getListEvaluationCommentsBySchool(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classID, semesterID, subjectID, typeOfTeacher, evaluationID).ToList(); ;
            List<EvaluationComments> list = new List<EvaluationComments>();
            List<SummedEvaluation> listSummed = new List<SummedEvaluation>();
            List<SummedEndingEvaluation> listSummedEnding = new List<SummedEndingEvaluation>();
            EvaluationComments evaluationCommentsObj = null;
            SummedEvaluation summedEvaluationObj = null;
            SummedEndingEvaluation summedEvaluationEndingObj = null;
            for (int i = 0; i < evaluationList.Count; i++)
            {
                EvaluationCommentsBO obj = evaluationList[i];
                int pupilID = obj.PupilID;
                string strInsert = string.Empty;
                if ("on".Equals(frm["Chk_" + pupilID]))
                {
                    if (MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                    {
                        summedEvaluationObj = new SummedEvaluation();
                        summedEvaluationObj.PupilID = pupilID;
                        summedEvaluationObj.ClassID = classID;
                        summedEvaluationObj.EducationLevelID = educationLevelID;
                        summedEvaluationObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        summedEvaluationObj.LastDigitSchoolID = partitionNumber;
                        summedEvaluationObj.SchoolID = _globalInfo.SchoolID.Value;
                        summedEvaluationObj.SemesterID = semesterID;
                        summedEvaluationObj.EvaluationCriteriaID = subjectID;
                        summedEvaluationObj.EvaluationID = evaluationID;

                        if (frm["AreaCommentsEnding_" + pupilID] != null && frm["AreaCommentsEnding_" + pupilID] != "")
                        {
                            summedEvaluationObj.EndingComments = frm["AreaCommentsEnding_" + pupilID];// Nhan xet

                        }
                        if (frm["txtPiriodicEndingMark_" + pupilID] != null && frm["txtPiriodicEndingMark_" + pupilID].Trim() != "")
                        {
                            summedEvaluationObj.PeriodicEndingMark = Convert.ToInt32(frm["txtPiriodicEndingMark_" + pupilID]);
                        }

                        //Neu la Tab NL,PC thi insert vao summedEndingEvaluation
                        if (evaluationID == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES || evaluationID == MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES)
                        {
                            
                            summedEvaluationEndingObj = new SummedEndingEvaluation();
                            summedEvaluationEndingObj.PupilID = pupilID;
                            summedEvaluationEndingObj.SemesterID = semesterID;
                            summedEvaluationEndingObj.EvaluationID = evaluationID;
                            summedEvaluationEndingObj.SchoolID = _globalInfo.SchoolID.Value;
                            summedEvaluationEndingObj.LastDigitSchoolID = partitionNumber;
                            summedEvaluationEndingObj.ClassID = classID;
                            summedEvaluationEndingObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            summedEvaluationEndingObj.CreateTime = DateTime.Now;
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                strInsert += "DGI,";
                            }
                            else
                            {
                                strInsert += "DGII,";
                            }
                            summedEvaluationEndingObj.EndingEvaluation = frm["txtEvaluation_" + pupilID];
                            
                            listSummedEnding.Add(summedEvaluationEndingObj);
                        }
                        else
                        {
                            //Neu la tab CLGD thi insert vao summedEvaluation
                            summedEvaluationObj.EndingEvaluation = frm["txtEvaluation_" + pupilID];
                        }
                        listSummed.Add(summedEvaluationObj);
                    }
                    else
                    {
                        evaluationCommentsObj = new EvaluationComments();
                        evaluationCommentsObj.PupilID = pupilID;
                        evaluationCommentsObj.ClassID = classID;
                        evaluationCommentsObj.EducationLevelID = educationLevelID;
                        evaluationCommentsObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        evaluationCommentsObj.LastDigitSchoolID = partitionNumber;
                        evaluationCommentsObj.SchoolID = _globalInfo.SchoolID.Value;
                        evaluationCommentsObj.TypeOfTeacher = typeOfTeacher;
                        evaluationCommentsObj.SemesterID = semesterID;
                        evaluationCommentsObj.EvaluationCriteriaID = 0;//subjectID;
                        evaluationCommentsObj.EvaluationID = evaluationID;

                        //lay thong tin commnet trong thang
                        if (MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_1 || MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_6)
                        {
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                strInsert += "T1,";
                            }
                            else
                            {
                                strInsert += "T6,";
                            }
                            evaluationCommentsObj.CommentsM1M6 = frm["AreaCommentsM16_" + pupilID];
                        }
                        else if (MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_2 || MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_7)
                        {
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                strInsert += "T2,";
                            }
                            else
                            {
                                strInsert += "T7,";
                            }
                            evaluationCommentsObj.CommentsM2M7 = frm["AreaCommentsM27_" + pupilID];
                        }
                        else if (MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_3 || MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_8)
                        {
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                strInsert += "T3,";
                            }
                            else
                            {
                                strInsert += "T8,";
                            }
                            evaluationCommentsObj.CommentsM3M8 = frm["AreaCommentsM38_" + pupilID];
                        }
                        else if (MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_4 || MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_9)
                        {
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                strInsert += "T4,";
                            }
                            else
                            {
                                strInsert += "T9,";
                            }
                            evaluationCommentsObj.CommentsM4M9 = frm["AreaCommentsM49_" + pupilID];
                        }
                        else if (MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_5 || MonthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_10)
                        {
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                strInsert += "T5,";
                            }
                            else
                            {
                                strInsert += "T10,";
                            }
                            evaluationCommentsObj.CommentsM5M10 = frm["AreaCommentsM510_" + pupilID];
                        }

                        evaluationCommentsObj.NoteInsert = strInsert + DateTime.Now.ToString();
                        evaluationCommentsObj.CreateTime = DateTime.Now;

                        if (evaluationID == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES || evaluationID == MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES)
                        {
                            // insert cot danh gia
                            summedEvaluationEndingObj = new SummedEndingEvaluation();
                            summedEvaluationEndingObj.PupilID = pupilID;
                            summedEvaluationEndingObj.SemesterID = semesterID;
                            summedEvaluationEndingObj.EvaluationID = evaluationID;
                            summedEvaluationEndingObj.SchoolID = _globalInfo.SchoolID.Value;
                            summedEvaluationEndingObj.LastDigitSchoolID = partitionNumber;
                            summedEvaluationEndingObj.ClassID = classID;
                            summedEvaluationEndingObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            summedEvaluationEndingObj.CreateTime = DateTime.Now;
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                strInsert += "DGI,";
                            }
                            else
                            {
                                strInsert += "DGII,";
                            }

                            summedEvaluationEndingObj.EndingEvaluation = frm["txtEvaluation_" + pupilID];
                            listSummedEnding.Add(summedEvaluationEndingObj);
                        }
                        list.Add(evaluationCommentsObj);
                    }
                }
            }

            if ((list != null && list.Count > 0) || (listSummed != null && listSummed.Count > 0) || (listSummedEnding != null && listSummedEnding.Count > 0))
            {
                List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();
                if (UtilsBusiness.IsMoveHistory(aca))
                {
                    List<EvaluationCommentsHistory> lstECHistory = list.ConvertAll(e => ConvertToHistoryEvaluationComments(e));
                    List<SummedEvaluationHistory> listSummedHistory = listSummed.ConvertAll(e => ConvertToHistorySummedEvaluation(e));
                    lstActionAuditBO = EvaluationCommentsHistoryBusiness.InsertOrUpdateEvaluationCommentsHistory(MonthID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classID, educationLevelID, semesterID, evaluationID, subjectID, typeOfTeacher, lstECHistory, listSummedHistory, listSummedEnding);
                }
                else
                {
                    lstActionAuditBO = EvaluationCommentsBusiness.InsertOrUpdateEvaluationComments(MonthID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classID, educationLevelID, semesterID, evaluationID, subjectID, typeOfTeacher, list, listSummed, listSummedEnding);
                }

                WriteLog(lstActionAuditBO, SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
            }
            else
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn học sinh"));
                
            }

            return Json(new JsonMessage("Lưu thành công"));
        }

        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEvaluationCommments(string arrayID, string arraySummedID, int semesterID, int classID, int evaluationCriteriaID, int educationLevelID, int evaluationID, int monthID)
        {
            try
            {
                #region check permision
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
                bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
                if (!(isSchoolAdmin || isGVCN))
                {
                    return Json(new JsonMessage("Thầy/cô không có quyền thao tác chức năng này"));
                }
                #endregion
                AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                string[] ArrayID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                List<string> IDStrList = ArrayID.ToList();
                List<long> idList = IDStrList.Select(long.Parse).ToList();
                List<long> idListSummedEnding = null;
                if (!string.IsNullOrWhiteSpace(arraySummedID))
                {
                    string[] ArraySummedEndingID = arraySummedID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    List<string> IDStrListSummedEnding = ArraySummedEndingID.ToList();
                    idListSummedEnding = IDStrListSummedEnding.Select(long.Parse).ToList();
                }

                int typeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN;
                List<EvaluationCommentActionAuditBO> lstActionAudit = new List<EvaluationCommentActionAuditBO>() ;
                if (UtilsBusiness.IsMoveHistory(aca))
                {
                    lstActionAudit = EvaluationCommentsHistoryBusiness.DeleteEvaluationCommentsHistory(idList, idListSummedEnding, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, semesterID, classID, evaluationCriteriaID, typeOfTeacher, educationLevelID, evaluationID, monthID);
                }
                else
                {
                     lstActionAudit = EvaluationCommentsBusiness.DeleteEvaluationComments(idList, idListSummedEnding, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, semesterID, classID, evaluationCriteriaID, typeOfTeacher, educationLevelID, evaluationID, monthID);
                }

                //Ghi log
                WriteLog(lstActionAudit, SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                return Json(new JsonMessage("Xóa nhận xét đánh giá thành công"));
            }
            catch
            {
                return Json(new JsonMessage("Lỗi trong quá trình xử lý, xin thử lại."));
            }
        }

        [ActionAudit]
        public FileResult ExportExcel(int classID, int semester, int evaluationID, int monthID, string className)
        {
            ClassProfile classProfileObj = ClassProfileBusiness.Find(classID);
            className = classProfileObj.DisplayName;
            if (className.ToUpper().StartsWith(MarkRecordOfPrimaryHeadTeacherConstant.CLASS_NAME))
            {
                className = className.Substring(MarkRecordOfPrimaryHeadTeacherConstant.CLASS_NAME.Length, className.Length - MarkRecordOfPrimaryHeadTeacherConstant.CLASS_NAME.Length).Trim();
            }
            int typeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            string templatePath = string.Empty;
            string strLockMark = this.CheckLockMark(classID);
            if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", MarkRecordOfPrimaryHeadTeacherConstant.TEMPLATE_FILE_SEMESTER);
            }
            else
            {
                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", MarkRecordOfPrimaryHeadTeacherConstant.TEMPLATE_FILE_MONTH);
            }
            List<MonthComments> monthCommentsList = this.GetMonthBySemester(semester).ToList();

            //Lay danh sach cac cot da khoa
            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID}
            };
            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            string lockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation;
            VTDataValidation objValidationNumber;
            string[] s = new string[] { MarkRecordOfPrimaryHeadTeacherConstant.FINISH, MarkRecordOfPrimaryHeadTeacherConstant.NOTFINISH };
            string[] cd = new string[] { MarkRecordOfPrimaryHeadTeacherConstant.OK, MarkRecordOfPrimaryHeadTeacherConstant.NOT_OK };
            if (monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                IVTWorksheet sheet = null;
                //Lay list mon hoc
                List<ClassSubject> lstClassSubject = new List<ClassSubject>();
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && classID > 0)
                {
                    lstClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semester, classID, _globalInfo.IsRolePrincipal ? true : _globalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .ToList();
                }

                ClassSubject objClassSubject;
                if (lstClassSubject == null || lstClassSubject.Count == 0)
                {
                    throw new BusinessException("Lớp chưa được khai báo môn học. Thầy/cô vui lòng kiểm tra lại");
                }

                List<int> lstSubject = lstClassSubject.Select(c => c.SubjectID).ToList();
                IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                dicSearch.Add("AcademicYearID", academicYear.AcademicYearID);
                dicSearch.Add("SemesterID", semester);
                dicSearch.Add("TypeOfTeacher", typeOfTeacher);
                dicSearch.Add("SchoolID", academicYear.SchoolID);
                List<EvaluationCommentsReportBO> lstEvaluationComents = EvaluationCommentsBusiness.GetAllEvaluationCommentsbyClassAndHeadTeacher(classID, dicSearch, lstSubject).ToList();
                List<EvaluationCommentsReportBO> lstEvaluationComentsbySubject = new List<EvaluationCommentsReportBO>();
                int endRow = 0;


                int totalSheet = 0;
                for (int c = 0; c < lstClassSubject.Count; c++)
                {
                    objClassSubject = lstClassSubject[c];
                    objValidation = new VTDataValidation();
                    ++totalSheet;
                    lstEvaluationComentsbySubject = lstEvaluationComents.Where(e => e.EvaluationCriteriaID == objClassSubject.SubjectID).ToList();
                    endRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START + lstEvaluationComentsbySubject.Count;
                    if (objClassSubject.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)//Mon tinh diem
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        objValidation.ToColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_ENDDING_EVALUATION;
                        objValidation.FromColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_ENDDING_EVALUATION;
                        //Tao validation cho cot diem
                        objValidationNumber = new VTDataValidation
                        {
                            FromValue = "1",
                            ToValue = "10",
                            SheetIndex = totalSheet,
                            Type = VTValidationType.INTEGER,
                            FromColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK,
                            ToColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK,
                            FromRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START,
                            ToRow = endRow
                        };
                        lstValidation.Add(objValidationNumber);
                    }
                    else
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(2));
                        objValidation.ToColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK;
                        objValidation.FromColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK;
                    }
                    //Tao combox danh gia trong cot 
                    objValidation.FromRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START;
                    objValidation.ToRow = endRow;
                    objValidation.SheetIndex = totalSheet;
                    objValidation.Contrains = s;
                    objValidation.Type = VTValidationType.LIST;
                    lstValidation.Add(objValidation);

                    sheet.Name = Utils.Utils.StripVNSignAndSpace(objClassSubject.SubjectCat.DisplayName);
                    FillExcelEducationQuality(sheet, lstEvaluationComentsbySubject, semester, className, academicYear, objClassSubject, monthCommentsList, lockMarkTitle);


                }
                lstEvaluationComentsbySubject = lstEvaluationComents.Where(e => e.EvaluationCriteriaID == -1).ToList();

                //Sheet nang luc
                sheet = oBook.CopySheetToLast(oBook.GetSheet(3));
                sheet.Name = MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_CAPACITIES;
                FillExcelCapacitiesOrQualities(sheet, lstEvaluationComentsbySubject, semester, className, academicYear, lockMarkTitle, GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY);
                objValidation = new VTDataValidation
                {
                    Contrains = cd,
                    FromColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_ENDDING,
                    FromRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START,
                    SheetIndex = ++totalSheet,
                    ToColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_ENDDING,
                    ToRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START + lstEvaluationComentsbySubject.Count,
                    Type = VTValidationType.LIST
                };
                lstValidation.Add(objValidation);
                //sheet Pham chat 
                sheet = oBook.CopySheetToLast(oBook.GetSheet(4));
                sheet.Name = MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_QUALITIES;
                FillExcelCapacitiesOrQualities(sheet, lstEvaluationComentsbySubject, semester, className, academicYear, lockMarkTitle, GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY);
                objValidation = new VTDataValidation
                {
                    Contrains = cd,
                    FromColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_ENDDING,
                    FromRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START,
                    SheetIndex = ++totalSheet,
                    ToColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_ENDDING,
                    ToRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START + lstEvaluationComentsbySubject.Count,
                    Type = VTValidationType.LIST
                };
                lstValidation.Add(objValidation);
                oBook.GetSheet(1).Delete();
                oBook.GetSheet(1).Delete();
                oBook.GetSheet(1).Delete();
                oBook.GetSheet(1).Delete();
            }
            else
            {
                IVTWorksheet sheetEducation = null;
                IVTWorksheet sheetCapacites = null;
                IVTWorksheet sheetQuaties = null;
                sheetEducation = oBook.GetSheet(1);
                sheetCapacites = oBook.GetSheet(2);
                sheetQuaties = oBook.GetSheet(3);
                monthCommentsList = monthCommentsList.Where(p => p.MonthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11).ToList();
                IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                dicSearch.Add("AcademicYearID", academicYear.AcademicYearID);
                dicSearch.Add("SemesterID", semester);
                dicSearch.Add("TypeOfTeacher", typeOfTeacher);
                dicSearch.Add("SchoolID", academicYear.SchoolID);

                List<EvaluationCommentsReportBO> lstEvaluationComents = EvaluationCommentsBusiness.GetAllEvaluationCommentsMonthbyClassofHeadTeacher(classID, dicSearch).ToList();
                FillDataExcelSheetMonth(sheetEducation, lstEvaluationComents, semester, className, academicYear, monthCommentsList, lockMarkTitle, MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION);
                FillDataExcelSheetMonth(sheetCapacites, lstEvaluationComents, semester, className, academicYear, monthCommentsList, lockMarkTitle, MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES);
                FillDataExcelSheetMonth(sheetQuaties, lstEvaluationComents, semester, className, academicYear, monthCommentsList, lockMarkTitle, MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES);
            }

            className = Utils.Utils.StripVNSignAndSpace(className.Replace(" ", ""));

            //Tao log
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();

            descriptionStr.Append("Sổ theo dõi CLGD (GVCN)").Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            paramsStr.Append("Lớp: " + className).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            userFuntionsStr.Append("Sổ theo dõi CLGD (GVCN)").Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_EXPORT).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            userDescriptionsStr.Append(String.Format("Xuất excel Sổ theo dõi CLGD (GVCN), {0}, Học kỳ {1}, năm học {2}.",
                new string[] { className, semester.ToString(), academicYear.DisplayTitle })).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

            ViewData[CommonKey.AuditActionKey.Description] = descriptionStr.ToString();
            ViewData[CommonKey.AuditActionKey.Parameter] = paramsStr.ToString();
            ViewData[CommonKey.AuditActionKey.userAction] = userActionsStr.ToString();
            ViewData[CommonKey.AuditActionKey.userFunction] = userFuntionsStr.ToString();
            ViewData[CommonKey.AuditActionKey.userDescription] = userDescriptionsStr.ToString();

            if (monthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11 && monthID > 0)
            {
                string fileName = string.Format("BangDanhGiaThang_Lop{0}_HK{1}_GVCN.xls", className, semester);
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                string fileName = string.Format("BangDanhGiaCuoiKy_Lop{0}_HK{1}_GVCN.xls", className, semester);
                Stream excel = oBook.ToStreamValidationData(lstValidation);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
        }
        private void FillDataExcelSheetMonth(IVTWorksheet sheet, List<EvaluationCommentsReportBO> lstEvaluationComments, int semester, string className, AcademicYear objAcademicYear, List<MonthComments> monthCommentsList, string LockMarkTitle, int evaluationId)
        {
            className = className.Replace("Lớp", "");
            className = className.Replace("lớp", "");
            className = className.Replace("LỚP", "");
            className = className.Trim();
            sheet.SetCellValue("A2", objAcademicYear.SchoolProfile.SchoolName.ToUpper());
            if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
            {
                string strTitle = string.Format("BẢNG ĐÁNH GIÁ MÔN HỌC VÀ HOẠT ĐỘNG GIÁO DỤC HỌC KỲ {0} - LỚP {1}", semester, className.ToUpper());
                sheet.SetCellValue("A3", strTitle);

            }
            else if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES)
            {
                string strTitle = string.Format("BẢNG ĐÁNH GIÁ NĂNG LỰC HỌC SINH HỌC KỲ {0} - LỚP {1}", semester, className.ToUpper());
                sheet.SetCellValue("A3", strTitle);
            }
            else
            {
                string strTitle = string.Format("BẢNG ĐÁNH GIÁ PHẨM CHẤT HỌC SINH HỌC KỲ {0} - LỚP {1}", semester, className.ToUpper());
                sheet.SetCellValue("A3", strTitle);
            }
            string strAcademicYear = Res.Get("AcademicYear_Control_Year") + " " + objAcademicYear.DisplayTitle;
            sheet.SetCellValue("A4", strAcademicYear.ToUpper());
            int startColumnTitle = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_TITLE;
            int startRowTitle = MarkRecordOfPrimaryHeadTeacherConstant.ROW_TILTE;
            monthCommentsList = monthCommentsList.Where(p => p.MonthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11).ToList();
            for (int m = 0; m < monthCommentsList.Count; m++)
            {
                sheet.SetCellValue(startRowTitle, (startColumnTitle + m), monthCommentsList[m].Name);
            }

            // Fill du lieu nhan xet trong thang
            int startRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START;
            int startColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_START;
            int columnIndex = 1;
            EvaluationCommentsReportBO obj;
            EvaluationComments objEducation;

            if (lstEvaluationComments == null || lstEvaluationComments.Count == 0)
            {
                return;
            }

            for (int i = 0; i < lstEvaluationComments.Count; i++)
            {
                columnIndex = 1;

                obj = lstEvaluationComments[i];
                if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
                {
                    objEducation = obj.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION];
                }
                else if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES)
                {
                    objEducation = obj.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES];
                }
                else
                {
                    objEducation = obj.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES];
                }
                sheet.SetCellValue(startRow, startColumn, i + 1);//STT
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).IsLock = true;
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_CODE;
                sheet.SetCellValue(startRow, columnIndex, obj.PupilCode);//ma hoc sinh
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;

                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_NAME;
                sheet.SetCellValue(startRow, columnIndex, obj.FullName);// ho ten hoc sinh
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;

                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_BIRTHDAY;
                sheet.SetCellValue(startRow, columnIndex, obj.Birthday.ToString("dd/MM/yyyy"));// ngay thang nam sinh
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;

                //Thang thu 1
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_1;
                sheet.SetCellValue(startRow, columnIndex, (objEducation == null) ? "" : objEducation.CommentsM1M6);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (!string.IsNullOrEmpty(Utils.Utils.CheckLockMarkBySemester(semester, LockMarkTitle, "T1", 1, false, "")))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                //Thang thu 2
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_2;
                sheet.SetCellValue(startRow, columnIndex, (objEducation == null) ? "" : objEducation.CommentsM2M7);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (!string.IsNullOrEmpty(Utils.Utils.CheckLockMarkBySemester(semester, LockMarkTitle, "T2", 1, false, "")))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                //Thang thu 3
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_3;
                sheet.SetCellValue(startRow, columnIndex, (objEducation == null) ? "" : objEducation.CommentsM3M8);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (!string.IsNullOrEmpty(Utils.Utils.CheckLockMarkBySemester(semester, LockMarkTitle, "T3", 1, false, "")))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                //Thang thu 4
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_4;
                sheet.SetCellValue(startRow, columnIndex, (objEducation == null) ? "" : objEducation.CommentsM4M9);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (!string.IsNullOrEmpty(Utils.Utils.CheckLockMarkBySemester(semester, LockMarkTitle, "T4", 1, false, "")))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                //Thang thu 5
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_5;
                sheet.SetCellValue(startRow, columnIndex, (objEducation == null) ? "" : objEducation.CommentsM5M10);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (!string.IsNullOrEmpty(Utils.Utils.CheckLockMarkBySemester(semester, LockMarkTitle, "T5", 1, false, "")))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                //kiem tra hoc sinh khac trang thai dang hoc
                if (obj.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, startColumn, startRow, columnIndex).IsLock = true;
                }

                if ((i + 1) % 5 == 0)
                {
                    sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }
                else
                {
                    sheet.GetRange(startRow, startColumn, startRow, startColumn + 8).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }

                if ((i + 1) == lstEvaluationComments.Count())
                {
                    sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }

                startRow += 1;
            }
            // sheet.ProtectSheet();
        }

        /// <summary>
        /// Fill du lieu chat luong giao duc vao file excel
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="listresult"></param>
        /// <param name="semester"></param>
        /// <param name="className"></param>
        /// <param name="objAcademicYear"></param>
        /// <param name="isCommeting"></param>
        /// <returns></returns>
        private void FillExcelEducationQuality(IVTWorksheet sheet, List<EvaluationCommentsReportBO> lstEvaluationComments, int semester, string className, AcademicYear objAcademicYear, ClassSubject objClassSubject, List<MonthComments> monthCommentsList, string LockMarkTitle)
        {

            className = className.Replace("Lớp", "");
            className = className.Replace("lớp", "");
            className = className.Replace("LỚP", "");
            className = className.Trim();
            ///Thong tin truong            
            string strTitle = string.Format("BẢNG ĐÁNH GIÁ CHẤT LƯỢNG MÔN {0} HỌC KỲ {1} - LỚP {2}", objClassSubject.SubjectCat.DisplayName.ToUpper(), semester, className.ToUpper());
            string strAcademicYear = Res.Get("AcademicYear_Control_Year") + " " + objAcademicYear.DisplayTitle;
            sheet.SetCellValue("A2", objAcademicYear.SchoolProfile.SchoolName.ToUpper());
            sheet.SetCellValue("A3", strTitle);
            sheet.SetCellValue("A4", strAcademicYear.ToUpper());
            string lockCK = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "CK1" : "CK2";
            //fill cac thang tieu de
            int startColumnTitle = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_TITLE;
            int startRowTitle = MarkRecordOfPrimaryHeadTeacherConstant.ROW_TILTE;
            monthCommentsList = monthCommentsList.Where(p => p.MonthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11).ToList();
            for (int m = 0; m < monthCommentsList.Count; m++)
            {
                sheet.SetCellValue(startRowTitle, (startColumnTitle + m), monthCommentsList[m].Name);
            }
            // Fill du lieu nhan xet trong thang
            int startRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START;
            int startColumn = 1;
            EvaluationCommentsReportBO obj;
            EvaluationComments objEducation;
            EvaluationComments objCapcities;
            EvaluationComments objQualities;
            SummedEvaluation objSummedEvaluation;

            if (lstEvaluationComments == null || lstEvaluationComments.Count == 0)
            {
                return;
            }

            string endingEvaluation = string.Empty;
            int columnIndex = 1;
            for (int i = 0; i < lstEvaluationComments.Count; i++)
            {
                columnIndex = 1;
                obj = lstEvaluationComments[i];
                //Lay nhan xet danh gia mon hoc, nang luc, pham chat cua
                objEducation = obj.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION];
                objCapcities = obj.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES];
                objQualities = obj.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES];
                sheet.SetCellValue(startRow, startColumn, i + 1);//STT
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_CODE;
                sheet.SetCellValue(startRow, columnIndex, obj.PupilCode);//ma hoc sinh
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);

                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_NAME;
                sheet.SetCellValue(startRow, columnIndex, obj.FullName);// ho ten hoc sinh
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);

                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_BIRTHDAY;
                sheet.SetCellValue(startRow, columnIndex, obj.Birthday.ToString("dd/MM/yyyy"));// ngay thang nam sinh
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetHAlign(VTHAlign.xlHAlignLeft);
                //Nhan xet thang thu nhat
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_1;
                sheet.SetCellValue(startRow, columnIndex, (objEducation != null) ? objEducation.CommentsM1M6 : "");
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T1", ""))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                //Nhan xet thang thu 2
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_2;
                sheet.SetCellValue(startRow, columnIndex, (objEducation != null) ? objEducation.CommentsM2M7 : "");
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T2", ""))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }
                //Nhan xet thang thu 3
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_3;
                sheet.SetCellValue(startRow, columnIndex, (objEducation != null) ? objEducation.CommentsM3M8 : "");
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();

                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T3", ""))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                //Nhan xet thang thu 4
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_4;
                sheet.SetCellValue(startRow, columnIndex, (objEducation != null) ? objEducation.CommentsM4M9 : "");
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T4", ""))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                //Nhan xet thang thu 5
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_5;
                sheet.SetCellValue(startRow, columnIndex, (objEducation != null) ? objEducation.CommentsM5M10 : "");
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T5", ""))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }

                objSummedEvaluation = obj.DicSummedEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION];

                //Nhan xet cuoi ky
                columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_COMMENTS_ENDDING;
                sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();

                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                {
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                }
                //Cot danh gia
                endingEvaluation = (objSummedEvaluation != null) ? objSummedEvaluation.EndingEvaluation : "";
                //Neu la mon tinhs diem thi dien cot KTCK
                if (objClassSubject.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK) //mon tinh diem
                {
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK;
                    sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.PeriodicEndingMark : null);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }



                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_ENDDING_EVALUATION;
                    sheet.SetCellValue(startRow, columnIndex, "HT".Equals(endingEvaluation) ? MarkRecordOfPrimaryHeadTeacherConstant.FINISH : "CHT".Equals(endingEvaluation) ? MarkRecordOfPrimaryHeadTeacherConstant.NOTFINISH : endingEvaluation);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    //sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }
                    /*//Danh gia sau thi lai hoac danh gia cuoi nam
                    if (!string.IsNullOrEmpty(obj.EvaluationEnding))
                    {
                        sheet.SetCellValue(startRow, startColumn + 11, obj.EvaluationEnding);
                    }
                    else
                    {
                        sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION, "HT".Equals(obj.EvaluationEnding) ? MarkRecordOfPrimaryTeacherConstants.FINISH : "CHT".Equals(obj.EvaluationEnding) ? MarkRecordOfPrimaryTeacherConstants.NOTFINISH : obj.EvaluationEnding);
                    }*/

                    if ((i + 1) % 5 == 0)
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                    else
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    if ((i + 1) == lstEvaluationComments.Count())
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                }
                else //Mon nhat xet
                {
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK;
                    sheet.SetCellValue(startRow, columnIndex, "HT".Equals(endingEvaluation) ? MarkRecordOfPrimaryHeadTeacherConstant.FINISH : "CHT".Equals(endingEvaluation) ? MarkRecordOfPrimaryHeadTeacherConstant.NOTFINISH : endingEvaluation);

                    /*if (!string.IsNullOrEmpty(obj.EvaluationReTraining))
                    {
                        sheet.SetCellValue(startRow, startColumn + 10, "HT".Equals(obj.EvaluationReTraining) ? MarkRecordOfPrimaryTeacherConstants.FINISH : "CHT".Equals(obj.EvaluationReTraining) ? MarkRecordOfPrimaryTeacherConstants.NOTFINISH : obj.EvaluationReTraining);
                    }
                    else
                    {
                        sheet.SetCellValue(startRow, startColumn + 10, "HT".Equals(obj.EvaluationEnding) ? MarkRecordOfPrimaryTeacherConstants.FINISH : "CHT".Equals(obj.EvaluationEnding) ? MarkRecordOfPrimaryTeacherConstants.NOTFINISH : obj.EvaluationEnding);
                    }*/
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }

                    if ((i + 1) % 5 == 0)
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                    else
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    if ((i + 1) == lstEvaluationComments.Count())
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                }
                //học sinh khác đang học tô đỏ ghạch ngang
                if (obj.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, startColumn, startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_ENDDING_EVALUATION).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, startColumn, startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_ENDDING_EVALUATION).WrapText();
                    sheet.GetRange(startRow, startColumn, startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_ENDDING_EVALUATION).IsLock = true;
                }
                startRow += 1;
            }
            sheet.SetFontName("Times New Roman", 11);
            //sheet.ProtectSheet();

        }
        /// <summary>
        /// Fill du lieu cuoi ky vao file excel doi voi sheet Nang luc, pham chat 
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="lstEvaluationComments"></param>
        /// <param name="semester"></param>
        /// <param name="className"></param>
        /// <param name="objAcademicYear"></param>
        /// <param name="LockMarkTitle"></param>
        
        private void FillExcelCapacitiesOrQualities(IVTWorksheet sheet, List<EvaluationCommentsReportBO> lstEvaluationComments, int semester, string className, AcademicYear objAcademicYear, string LockMarkTitle, int evaluationId)
        {
            className = className.Replace("Lớp", "");
            className = className.Replace("lớp", "");
            className = className.Replace("LỚP", "");
            className = className.Trim();
            string strTitle;
            sheet.SetCellValue("A2", objAcademicYear.SchoolProfile.SchoolName.ToUpper());
            if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES)
            {
                strTitle = string.Format("BẢNG ĐÁNH GIÁ NĂNG LỰC HỌC SINH HỌC KỲ {0} - LỚP {1}", semester, className.ToUpper());
            }
            else
            {
                strTitle = string.Format("BẢNG ĐÁNH GIÁ PHẨM CHẤT HỌC SINH HỌC KỲ {0} - LỚP {1}", semester, className.ToUpper());
            }

            string strAcademicYear = Res.Get("AcademicYear_Control_Year").ToUpper() + " " + objAcademicYear.DisplayTitle;

            sheet.SetCellValue("A3", strTitle);
            sheet.SetCellValue("A4", strAcademicYear);
            string lockCK = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "CK1" : "CK2";
            int startRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START_CAPACITIES_HEAD_TEACHER;
            int startColumn = 1;
            EvaluationCommentsReportBO obj;
            SummedEvaluation objSummedEvaluation;
            SummedEndingEvaluation objSummedEndingEvaluation;
            string evaluationEnding;
            if (lstEvaluationComments == null || lstEvaluationComments.Count == 0)
            {
                return;
            }

            string endingEvaluation = string.Empty;
            int columnIndex = 1;
            if (evaluationId == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY)
            {
                #region "Dien du lieu sheet nang luc"
                for (int i = 0; i < lstEvaluationComments.Count; i++)
                {
                    columnIndex = 1;
                    obj = lstEvaluationComments[i];
                    //Lay nhan xet danh gia mon hoc, nang luc, pham chat cua
                    sheet.SetCellValue(startRow, startColumn, i + 1);//STT
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_CODE;
                    sheet.SetCellValue(startRow, columnIndex, obj.PupilCode);//ma hoc sinh
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);

                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_NAME;
                    sheet.SetCellValue(startRow, columnIndex, obj.FullName);// ho ten hoc sinh
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);

                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_BIRTHDAY;
                    sheet.SetCellValue(startRow, columnIndex, obj.Birthday.ToString("dd/MM/yyyy"));// ngay thang nam sinh
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetHAlign(VTHAlign.xlHAlignLeft);
                    //Nhan xet thang thu nhat
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_1;
                    objSummedEvaluation = obj.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.SeftServingManagement];
                    sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }

                    //Nhan xet thang thu 2
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_2;
                    objSummedEvaluation = obj.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.CommunicationCoomperation];
                    sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }
                    //Nhan xet thang thu 3
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_3;
                    objSummedEvaluation = obj.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.StudyAndSolveProblems];
                    sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();

                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }


                    //Nhan xet cuoi ky
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_ENDDING;
                    objSummedEndingEvaluation = obj.DicSummedEndingEvaluation[GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY];
                    evaluationEnding = (objSummedEndingEvaluation == null) ? "" : objSummedEndingEvaluation.EndingEvaluation;
                    if ("Đ".Equals(evaluationEnding))
                    {
                        evaluationEnding = MarkRecordOfPrimaryHeadTeacherConstant.OK;
                    }
                    else if ("CĐ".Equals(evaluationEnding))
                    {
                        evaluationEnding = MarkRecordOfPrimaryHeadTeacherConstant.NOT_OK;
                    }

                    sheet.SetCellValue(startRow, columnIndex, evaluationEnding);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();

                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }
                    //học sinh khác đang học tô đỏ ghạch ngang
                    if (obj.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).IsLock = true;
                    }
                    if ((i + 1) % 5 == 0)
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                    else
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    if ((i + 1) == lstEvaluationComments.Count())
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    startRow += 1;
                }
                #endregion
            }
            else //Dien du lieu sheet pham chat
            {
                #region "Dien du lie sheet nang luc"
                for (int i = 0; i < lstEvaluationComments.Count; i++)
                {
                    columnIndex = 1;
                    obj = lstEvaluationComments[i];
                    sheet.SetCellValue(startRow, startColumn, i + 1);//STT
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_CODE;
                    sheet.SetCellValue(startRow, columnIndex, obj.PupilCode);//ma hoc sinh
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);

                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_NAME;
                    sheet.SetCellValue(startRow, columnIndex, obj.FullName);// ho ten hoc sinh
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);

                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_BIRTHDAY;
                    sheet.SetCellValue(startRow, columnIndex, obj.Birthday.ToString("dd/MM/yyyy"));// ngay thang nam sinh
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetHAlign(VTHAlign.xlHAlignLeft);

                    //Cham hoc cham lam
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_1;
                    objSummedEvaluation = obj.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.HandWorking];
                    sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }

                    //b) Tự trọng, tự tin, tự chịu trách nhiệm
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_2;
                    objSummedEvaluation = obj.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.Confidence];
                    sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }
                    //c) Trung thực, kỉ luật, đoàn kết
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_3;
                    objSummedEvaluation = obj.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.Honesty];
                    sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();

                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }

                    //d) Tình cảm, thái độ
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_4;
                    objSummedEvaluation = obj.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.LoveFamily];
                    sheet.SetCellValue(startRow, columnIndex, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();

                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }

                    //Nhan xet cuoi ky
                    columnIndex = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_ENDDING;
                    objSummedEndingEvaluation = obj.DicSummedEndingEvaluation[GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY];
                    evaluationEnding = (objSummedEndingEvaluation == null) ? "" : objSummedEndingEvaluation.EndingEvaluation;
                    if ("Đ".Equals(evaluationEnding))
                    {
                        evaluationEnding = MarkRecordOfPrimaryHeadTeacherConstant.OK;
                    }
                    else if ("CĐ".Equals(evaluationEnding))
                    {
                        evaluationEnding = MarkRecordOfPrimaryHeadTeacherConstant.NOT_OK;
                    }

                    sheet.SetCellValue(startRow, columnIndex, evaluationEnding);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, columnIndex, startRow, columnIndex).WrapText();
                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, lockCK, "CK"))
                    {
                        sheet.GetRange(startRow, columnIndex, startRow, columnIndex).IsLock = true;
                    }
                    //học sinh khác đang học tô đỏ ghạch ngang
                    if (obj.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).IsLock = true;
                    }
                    if ((i + 1) % 5 == 0)
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                    else
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    if ((i + 1) == lstEvaluationComments.Count())
                    {
                        sheet.GetRange(startRow, startColumn, startRow, columnIndex).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    startRow += 1;
                }
                #endregion
            }
            sheet.SetFontName("Times New Roman", 11);
            //sheet.ProtectSheet();

        }

        [ActionAudit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportEducationQuality(IEnumerable<HttpPostedFileBase> attachmentsEducationQuality, int classID, int semester, int monthID, string className, int educationLevelID, string arrayID, bool importCapacities, bool importQualities)
        {
            
            int partitionNumber = _globalInfo.SchoolID.Value % GlobalConstants.PARTITION;
            int typeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN;
            if (attachmentsEducationQuality == null || attachmentsEducationQuality.Count() <= 0)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            #region check permision
            bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            if (!(isSchoolAdmin || isGVCN))
            {
                return Json(new JsonMessage("Thầy/cô không có quyền thao tác chức năng này"));
            }
            #endregion
            //The Name of the Upload component is "attachments"
            var file = attachmentsEducationQuality.FirstOrDefault();
            if (file == null)
            {
                return Json(new JsonMessage("Lỗi trong quá trình xử lý, Thầy/cô vui lòng thử lại."));
            }
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //kiem tra file extension lan nua
            List<string> excelExtension = new List<string>();
            excelExtension.Add(".XLS");
            excelExtension.Add(".XLSX");
            var extension = Path.GetExtension(file.FileName);
            if (!excelExtension.Contains(extension.ToUpper()))
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
            }
            if (file.ContentLength / 1024 > MarkRecordOfPrimaryHeadTeacherConstant.FILE_UPLOAD_MAX_SIZE)
            {
                //throw new BusinessException("JudgeRecord_Validate_FileMaxSize");
                return Json(new JsonMessage("Dung lượng file excel không được lớn hơn 5MB", JsonMessage.ERROR));
            }

            //luu ra dia
            var fileName = Path.GetFileNameWithoutExtension(file.FileName);
            fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
            var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
            file.SaveAs(physicalPath);
            if (classID > 0 && String.IsNullOrEmpty(className))
            {
                var objClass = ClassProfileBusiness.Find(classID);
                className = (objClass == null) ? "" : objClass.DisplayName;
            }

            IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
            IVTWorksheet sheet = null;
            IVTWorksheet sheetCapacities = null;
            IVTWorksheet sheetQualities = null;
            List<ClassSubject> lstClassSubject = new List<ClassSubject>();
            List<IVTWorksheet> lstSheet = new List<IVTWorksheet>();
            if (monthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                try
                {
                    //sheet = oBook.GetSheet(1);
                    //sheetCapacities = oBook.GetSheet(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_CAPACITIES);
                    //sheetQualities = oBook.GetSheet(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_QUALITIES);
                    lstSheet = oBook.GetSheets();
                    sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_EDUCATION));
                    sheetCapacities = lstSheet.FirstOrDefault(c => c.Name.Equals(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_CAPACITIES));
                    sheetQualities = lstSheet.FirstOrDefault(c => c.Name.Equals(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_QUALITIES));

                    if (sheet == null && sheetCapacities == null && sheetQualities == null)
                    {
                        return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
                    }
                }
                catch
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
                }

                #region Validate thong tin
                //StringBuilder errMessageMonth = new StringBuilder();
                string errMessage;
                if (sheet != null)
                {
                    errMessage = this.ErrValidateFileExcel(sheet, MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION, semester, classID, academicYear, className);
                    if (!string.IsNullOrEmpty(errMessage))
                    {
                        //errMessageMonth.Append(String.Format(" Sheet MonHoc&HĐGD: {0} <br/>", errMessage));
                        //string msg = String.Format("Import dữ liệu không thành công. Sheet MonHoc&HĐGD: {0} Thầy cô vui lòng kiểm tra lại.", errMessage);
                        return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
                    }
                }
                if (sheetCapacities != null)
                {
                    errMessage = this.ErrValidateFileExcel(sheetCapacities, MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES, semester, classID, academicYear, className);
                    if (!string.IsNullOrEmpty(errMessage))
                    {
                        //errMessageMonth.Append(String.Format(" Sheet NangLuc: {0} <br/>", errMessage));
                        //string msg = String.Format("Import dữ liệu không thành công. Sheet NangLuc: {0} Thầy cô vui lòng kiểm tra lại.", errMessage);
                        return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
                    }
                }
                if (sheetQualities != null)
                {
                    errMessage = this.ErrValidateFileExcel(sheetQualities, MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES, semester, classID, academicYear, className);
                    if (!string.IsNullOrEmpty(errMessage))
                    {
                        //errMessageMonth.Append(String.Format(" Sheet PhamChat: {0} <br/>", errMessage));
                        //string msg = String.Format("Import dữ liệu không thành công. Sheet PhamChat: {0} Thầy cô vui lòng kiểm tra lại.", errMessage);
                        return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
                    }
                }

                #endregion
            }
            else
            {
                string[] ArrayID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (ArrayID == null || ArrayID.Count() == 0)
                {
                    return Json(new JsonMessage("Thầy/cô chưa chọn môn học"), JsonMessage.ERROR);
                }
                List<string> IDStrList = ArrayID.ToList();
                List<int> lstSubjectImport = IDStrList.Select(Int32.Parse).ToList();
                string subjectName;
                StringBuilder validateSubjectName = new StringBuilder();

                try
                {
                    lstSheet = oBook.GetSheets();
                }
                catch
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
                }

                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && classID > 0)
                {
                    lstClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semester, classID, _globalInfo.IsRolePrincipal ? true : _globalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true && lstSubjectImport.Contains(o.SubjectID))
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .ToList();
                }

                #region "Validate file excel"

                StringBuilder errMessage = new StringBuilder();
                string msgTitle = string.Empty;
                string sheetName;
                for (int i = 0; i < lstClassSubject.Count; i++)
                {
                    sheetName = Utils.Utils.StripVNSignAndSpace(lstClassSubject[i].SubjectCat.DisplayName);
                    sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(sheetName));
                    if (sheet == null)
                    {
                        continue;
                    }
                    msgTitle = this.ErrValidateFileExcel(sheet, MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION, semester, classID, academicYear, className, lstClassSubject[i].SubjectID, monthID, sheetName);
                    if (string.IsNullOrEmpty(msgTitle))
                    {
                        continue;
                    }

                    errMessage.Append(msgTitle);
                    break;

                }
                //Neu khong co du lieu loi thi kiem tra sheet NangLuc, pham chat
                if (String.IsNullOrEmpty(errMessage.ToString()))
                {
                    sheetCapacities = lstSheet.FirstOrDefault(c => c.Name.Equals(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_CAPACITIES));
                    if (sheetCapacities != null)
                    {
                        msgTitle = this.ErrValidateFileExcel(sheetCapacities, MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES, semester, classID, academicYear, className, 0, monthID);
                        if (!string.IsNullOrEmpty(msgTitle))
                        {
                            errMessage.Append(msgTitle);
                        }

                    }
                }
                if (String.IsNullOrEmpty(errMessage.ToString()))
                {
                    sheetQualities = lstSheet.FirstOrDefault(c => c.Name.Equals(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_QUALITIES));
                    if (sheetQualities != null)
                    {
                        msgTitle = this.ErrValidateFileExcel(sheetQualities, MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES, semester, classID, academicYear, className, 0, monthID);
                        if (!string.IsNullOrEmpty(msgTitle))
                        {
                            errMessage.Append(msgTitle);
                        }
                    }
                }

                if (!String.IsNullOrEmpty(errMessage.ToString()))
                {
                    string msg = String.Format("Import dữ liệu không thành công. {0} Thầy cô vui lòng kiểm tra lại.", errMessage.ToString());
                    return Json(new JsonMessage(msg, JsonMessage.ERROR));
                    //return Json(new JsonMessage(errMessage.ToString()), "error");
                }

                //Kiem tra sheet mon hoc co trong danh sach mon hoc duoc import
                for (int i = 0; i < lstClassSubject.Count; i++)
                {
                    subjectName = lstClassSubject[i].SubjectCat.DisplayName;
                    if (!lstSheet.Select(s => s.Name).Contains(Utils.Utils.StripVNSignAndSpace(subjectName)))
                    {
                        if (String.IsNullOrEmpty(validateSubjectName.ToString()))
                        {
                            validateSubjectName.Append(subjectName);
                        }
                        else
                        {
                            validateSubjectName.Append(", " + subjectName);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(validateSubjectName.ToString()))
                {
                    //string msg = string.Format("Thầy/cô không có quyền import các môn học: {0}", validateSubjectName.ToString());
                    //return Json(new JsonMessage(msg,JsonMessage.ERROR));
                    return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
                }

                #endregion

            }

            int partitionId = UtilsBusiness.GetPartionId(academicYear.SchoolID);
            List<PupilOfClassBO> lstPupilOfClass = PupilOfClassBusiness.GetPupilInClass(classID).ToList().OrderBy(c => c.OrderInClass).ThenBy(c => SMAS.Business.Common.Utils.SortABC(c.Name + " " + c.PupilFullName)).ToList();
            List<EvaluationCommentsReportBO> lstOutPut = new List<EvaluationCommentsReportBO>();

            if (monthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                try
                {
                    if (sheet != null)
                    {
                        this.GetDataFromFileExcel(sheet, lstPupilOfClass, academicYear, partitionId, MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION, monthID, educationLevelID, classID, 0, semester, 0, lstOutPut);
                    }
                    if (sheetCapacities != null)
                    {
                        this.GetDataFromFileExcel(sheetCapacities, lstPupilOfClass, academicYear, partitionId, MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES, monthID, educationLevelID, classID, 0, semester, 0, lstOutPut);
                    }
                    if (sheetQualities != null)
                    {
                        this.GetDataFromFileExcel(sheetQualities, lstPupilOfClass, academicYear, partitionId, MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES, monthID, educationLevelID, classID, 0, semester, 0, lstOutPut);
                    }
                }
                catch (Exception ex)
                {
                    LogExtensions.ErrorExt(logger, DateTime.Now, "ImportEducationQuality", "null", ex);
                    return Json(new JsonMessage("Import không thành công. Thầy cô vui lòng kiểm tra lại"), JsonMessage.ERROR);
                }

            }
            else
            {
                ClassSubject objClassSubject;
                string sheetName;
                for (int i = 0; i < lstClassSubject.Count; i++)
                {
                    objClassSubject = lstClassSubject[i];
                    sheetName = Utils.Utils.StripVNSignAndSpace(lstClassSubject[i].SubjectCat.DisplayName);
                    sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(sheetName));
                    if (sheet == null)
                    {
                        continue;
                    }
                    this.GetDataFromFileExcel(sheet, lstPupilOfClass, academicYear, partitionId, MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION, monthID, educationLevelID, classID, objClassSubject.SubjectID, semester, objClassSubject.IsCommenting.Value, lstOutPut);
                }

                sheetCapacities = lstSheet.FirstOrDefault(c => c.Name.Equals(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_CAPACITIES));
                if (sheetCapacities != null && importCapacities)
                {
                    this.GetDataFromFileExcel(sheetCapacities, lstPupilOfClass, academicYear, partitionId, MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES, monthID, educationLevelID, classID, 0, semester, 0, lstOutPut);
                }
                sheetQualities = lstSheet.FirstOrDefault(c => c.Name.Equals(MarkRecordOfPrimaryHeadTeacherConstant.SHEET_NAME_QUALITIES));
                if (sheetQualities != null && importQualities)
                {
                    this.GetDataFromFileExcel(sheetQualities, lstPupilOfClass, academicYear, partitionId, MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES, monthID, educationLevelID, classID, 0, semester, 0, lstOutPut);
                }
            }
            List<EvaluationCommentsReportBO> excelDataErrorList = lstOutPut.Where(p => p.Error == false && p.MessageError != null && (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == 0)).ToList();
            if (excelDataErrorList != null && excelDataErrorList.Count > 0)
            {
                ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_PUPIL_EVALUATION] = excelDataErrorList;
                return Json(new JsonMessage(RenderPartialViewToString("_ViewErrorEducationQuality", null), "CreateViewError"));
            }
            if (lstOutPut == null || lstOutPut.Count == 0)
            {
                return Json(new JsonMessage("Import dữ liệu không thành công. Thây cô vui lòng kiểm tra lại", JsonMessage.ERROR));
            }
            ImportExcel(lstOutPut, academicYear, classID, educationLevelID, semester, typeOfTeacher, monthID, partitionId);


            //Tao log
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();

            descriptionStr.Append("Sổ theo dõi CLGD (GVCN)").Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            paramsStr.Append("Lớp: " + className).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            userFuntionsStr.Append("Sổ theo dõi CLGD (GVCN)").Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            userDescriptionsStr.Append(String.Format("Import Sổ theo dõi CLGD (GVCN), {0}, Học kỳ {1}, năm học {2}.",
                new string[] { className, semester.ToString(), academicYear.DisplayTitle })).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

            ViewData[CommonKey.AuditActionKey.Description] = descriptionStr.ToString();
            ViewData[CommonKey.AuditActionKey.Parameter] = paramsStr.ToString();
            ViewData[CommonKey.AuditActionKey.userAction] = userActionsStr.ToString();
            ViewData[CommonKey.AuditActionKey.userFunction] = userFuntionsStr.ToString();
            ViewData[CommonKey.AuditActionKey.userDescription] = userDescriptionsStr.ToString();

            return Json(new JsonMessage("Import dữ liệu thành công."));

        }

        private void GetDataFromFileExcel(IVTWorksheet sheet, List<PupilOfClassBO> lstPupilOfClass, AcademicYear objAcademicYear, int partitionId, int evaluationId, int montId, int educationLevelId, int classId, int subjectId, int semester, int isCommenting, List<EvaluationCommentsReportBO> lstOutPut)
        {
            //subjectId = (subjectId == 0) ? -1 : subjectId;
            Regex objHtml = new Regex(@"</?\w+((\s+\w+(\s*=\s*(?:""[^""]*""|'[^']*'|[^'"">\s]+))?)+\s*|\s*)/?>");
            bool IsMatch = false;
            EvaluationCommentsReportBO objEvaluation;
            EvaluationComments objEvaluationEducation = null;
            IDictionary<int, EvaluationComments> dicEvaluation;
            IDictionary<int, SummedEvaluation> dicSummedEvaluation;
            IDictionary<int, SummedEndingEvaluation> dicSummedEnding;
            SummedEvaluation objSummedEvaluationEducation = null;
            SummedEndingEvaluation objSummedEnding;
            List<string> lstPupilCode = new List<string>();
            bool checkDuplicate = false;
            bool pass = true;
            int comment = 0;
            int mark;
            int startRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START;
            //Neu la sheet Nang luc, pham chat cuoi ky thi hang bat dau la 7. nguoc lai thi hang bat dau la 6
            if (evaluationId != MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION && montId == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                startRow = MarkRecordOfPrimaryHeadTeacherConstant.ROW_START_CAPACITIES_HEAD_TEACHER;
            }

            //for qua file excel
            int startColumn = 0;// MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_1;
            int endColumn = 0;
            //Lay cot du lieu cho tung sheet            
            string comment1;
            string comment2;
            string comment3;
            string comment4;
            string comment5;
            int pupilId = 0;
            if (montId == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
            {
                if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
                {
                    startColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_1;
                    if (isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        endColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_ENDDING_EVALUATION;
                    }
                    else
                    {
                        endColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK;
                    }
                }
                else if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES)
                {
                    startColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_1;
                    endColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_ENDDING;
                }
                else
                {
                    startColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_1;
                    endColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_ENDDING;
                }
            }
            else
            {
                startColumn = startColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_1;
                endColumn = MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_5;
            }
            string pupilCode;

            string commentEnding;
            string endingEvaluation;
            while (sheet.GetCellValue(startRow, 1) != null ||
                   sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_CODE) != null ||
                   sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_NAME) != null ||
                   sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_BIRTHDAY) != null)
            {
                pass = true;
                pupilCode = (string)sheet.GetCellValue(startRow, 2);
                objEvaluation = new EvaluationCommentsReportBO();
                objEvaluation.EvaluationId = evaluationId;
                if (lstPupilCode.Count() == 0)
                {
                    // add vao list ma hoc sinh:
                    lstPupilCode.Add(pupilCode);
                }
                else
                {
                    if (lstPupilCode.Contains(pupilCode))
                    {
                        checkDuplicate = true;
                    }
                    else
                    {
                        checkDuplicate = false;
                        lstPupilCode.Add(pupilCode);
                    }
                }
                var poc = lstPupilOfClass.FirstOrDefault(u => u.PupilCode == pupilCode);
                if (poc == null)
                {
                    objEvaluation.MessageError = "- Mã học sinh không hợp lệ \r\n";
                    pass = false;
                }
                else
                {
                    objEvaluation.PupilID = poc.PupilID;
                    objEvaluation.Status = poc.Status;
                }

                // Check trùng dữ liệu:
                if (checkDuplicate)
                {
                    pass = false;
                    objEvaluation.MessageError = "- Trùng mã học sinh.\r\n";
                }
                objEvaluation.PupilCode = pupilCode;
                //Kiem tra cot ho va ten hoc sinh
                string pupilname = (string)sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_PULIL_NAME);
                if (poc != null && !poc.PupilFullName.Equals(pupilname, StringComparison.InvariantCultureIgnoreCase))
                {
                    objEvaluation.MessageError = "- " + Res.Get("MarkRecord_Label_NameError") + "\r\n";
                    pass = false;
                }

                objEvaluation.FullName = pupilname;
                pupilId = (poc != null) ? poc.PupilID : 0;
                #region "Lay du lieu thang"
                // lay du lieu thang cac sheet Mon hoc va hoat dong giao duc, nang luc, pham chat
                if (montId != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                {
                    comment1 = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_1) == null) ? "" :
                                                         sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_1).ToString();
                    comment2 = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_2) == null) ? "" :
                                                          sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_2).ToString();
                    comment3 = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_3) == null) ? "" :
                                                          sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_3).ToString().Trim();
                    comment4 = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_4) == null) ? "" :
                                                          sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_4).ToString().Trim();
                    comment5 = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_5) == null) ? "" :
                                                          sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_MONTH_5).ToString().Trim();
                    objEvaluationEducation = new EvaluationComments
                    {
                        AcademicYearID = objAcademicYear.AcademicYearID,
                        ClassID = classId,
                        CommentsM1M6 = comment1,
                        CommentsM2M7 = comment2,
                        CommentsM3M8 = comment3,
                        CommentsM4M9 = comment4,
                        CommentsM5M10 = comment5,
                        CreateTime = DateTime.Now,
                        EducationLevelID = educationLevelId,
                        EvaluationCriteriaID = subjectId,
                        LastDigitSchoolID = partitionId,
                        PupilID = pupilId,
                        SchoolID = objAcademicYear.SchoolID,
                        SemesterID = semester,
                        TypeOfTeacher = MarkRecordOfPrimaryHeadTeacherConstant.GVCN,
                        UpdateTime = DateTime.Now,
                        EvaluationID = evaluationId
                    };
                    dicEvaluation = new Dictionary<int, EvaluationComments>();
                    dicEvaluation.Add(evaluationId, objEvaluationEducation);
                    objEvaluation.DicEvaluation = dicEvaluation;
                }
                #endregion

                #region "Lay du lieu  cuoi ky cho tung sheet"
                if (montId == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                {
                    //Nhan xet danh gia cuoi ky

                    if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
                    {
                        dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                        objSummedEvaluationEducation = SetValueSummedEvaluation(objAcademicYear, classId, educationLevelId, evaluationId, partitionId, pupilId, semester);
                        objSummedEvaluationEducation.EvaluationCriteriaID = subjectId;
                        objSummedEvaluationEducation.EndingComments = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_COMMENTS_ENDDING) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_COMMENTS_ENDDING).ToString().Trim();
                        if (isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                        {
                            if (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK) != null)
                            {
                                int.TryParse(sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK).ToString().Trim(), out mark);
                                objSummedEvaluationEducation.PeriodicEndingMark = mark;
                            }
                            else
                            {
                                objSummedEvaluationEducation.PeriodicEndingMark = null;
                            }

                        }
                        //Cot danh gia HT, CHT
                        if (sheet.GetCellValue(startRow, endColumn) != null)
                        {
                            if (MarkRecordOfPrimaryHeadTeacherConstant.FINISH.Equals(sheet.GetCellValue(startRow, endColumn)))
                            {
                                objSummedEvaluationEducation.EndingEvaluation = MarkRecordOfPrimaryHeadTeacherConstant.SHORTFINISH;
                            }
                            else if (MarkRecordOfPrimaryHeadTeacherConstant.NOTFINISH.Equals(sheet.GetCellValue(startRow, endColumn)))
                            {
                                objSummedEvaluationEducation.EndingEvaluation = MarkRecordOfPrimaryHeadTeacherConstant.SHORTNOTFINISH;
                            }
                        }
                        else
                        {
                            objSummedEvaluationEducation.EndingEvaluation = "";
                        }
                        dicSummedEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, objSummedEvaluationEducation);
                        objEvaluation.DicSummedEvaluation = dicSummedEvaluation;

                    }
                    else if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES)
                    {
                        dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();

                        objSummedEvaluationEducation = SetValueSummedEvaluation(objAcademicYear, classId, educationLevelId, evaluationId, partitionId, pupilId, semester);
                        objSummedEvaluationEducation.EvaluationCriteriaID = MarkRecordOfPrimaryHeadTeacherConstant.SeftServingManagement;
                        objSummedEvaluationEducation.EndingComments = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_1) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_1).ToString().Trim();
                        dicSummedEvaluation.Add(MarkRecordOfPrimaryHeadTeacherConstant.SeftServingManagement, objSummedEvaluationEducation);


                        objSummedEvaluationEducation = SetValueSummedEvaluation(objAcademicYear, classId, educationLevelId, evaluationId, partitionId, pupilId, semester);
                        objSummedEvaluationEducation.EvaluationCriteriaID = MarkRecordOfPrimaryHeadTeacherConstant.CommunicationCoomperation;
                        objSummedEvaluationEducation.EndingComments = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_2) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_2).ToString().Trim();
                        dicSummedEvaluation.Add(MarkRecordOfPrimaryHeadTeacherConstant.CommunicationCoomperation, objSummedEvaluationEducation);

                        objSummedEvaluationEducation = SetValueSummedEvaluation(objAcademicYear, classId, educationLevelId, evaluationId, partitionId, pupilId, semester);
                        objSummedEvaluationEducation.EvaluationCriteriaID = MarkRecordOfPrimaryHeadTeacherConstant.StudyAndSolveProblems;
                        objSummedEvaluationEducation.EndingComments = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_3) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_3).ToString().Trim();
                        dicSummedEvaluation.Add(MarkRecordOfPrimaryHeadTeacherConstant.StudyAndSolveProblems, objSummedEvaluationEducation);

                        objEvaluation.CapacitesOrQualitiesSemester = dicSummedEvaluation;

                        dicSummedEnding = new Dictionary<int, SummedEndingEvaluation>();
                        objSummedEnding = new SummedEndingEvaluation
                        {
                            AcademicYearID = objAcademicYear.AcademicYearID,
                            ClassID = classId,
                            CreateTime = DateTime.Now,
                            EvaluationID = evaluationId,
                            LastDigitSchoolID = partitionId,
                            PupilID = pupilId,
                            SchoolID = objAcademicYear.SchoolID,
                            SemesterID = semester,
                            UpdateTime = DateTime.Now
                        };
                        commentEnding = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_ENDDING) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_ENDDING).ToString().Trim();
                        if (!string.IsNullOrEmpty(commentEnding))
                        {
                            if (commentEnding.Equals(MarkRecordOfPrimaryHeadTeacherConstant.OK))
                            {
                                objSummedEnding.EndingEvaluation = MarkRecordOfPrimaryHeadTeacherConstant.SHORT_OK;
                            }
                            else
                            {
                                objSummedEnding.EndingEvaluation = MarkRecordOfPrimaryHeadTeacherConstant.SHORT_NOT_OK;
                            }
                        }
                        else
                        {
                            objSummedEnding.EndingEvaluation = "";
                        }
                        dicSummedEnding.Add(evaluationId, objSummedEnding);
                        objEvaluation.DicSummedEndingEvaluation = dicSummedEnding;
                    }
                    else
                    {
                        dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();

                        objSummedEvaluationEducation = SetValueSummedEvaluation(objAcademicYear, classId, educationLevelId, evaluationId, partitionId, pupilId, semester);
                        objSummedEvaluationEducation.EvaluationCriteriaID = MarkRecordOfPrimaryHeadTeacherConstant.HandWorking;
                        objSummedEvaluationEducation.EndingComments = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_1) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_1).ToString().Trim();
                        dicSummedEvaluation.Add(MarkRecordOfPrimaryHeadTeacherConstant.HandWorking, objSummedEvaluationEducation);

                        objSummedEvaluationEducation = SetValueSummedEvaluation(objAcademicYear, classId, educationLevelId, evaluationId, partitionId, pupilId, semester);
                        objSummedEvaluationEducation.EvaluationCriteriaID = MarkRecordOfPrimaryHeadTeacherConstant.Confidence;
                        objSummedEvaluationEducation.EndingComments = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_2) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_2).ToString().Trim();
                        dicSummedEvaluation.Add(MarkRecordOfPrimaryHeadTeacherConstant.Confidence, objSummedEvaluationEducation);

                        objSummedEvaluationEducation = SetValueSummedEvaluation(objAcademicYear, classId, educationLevelId, evaluationId, partitionId, pupilId, semester);
                        objSummedEvaluationEducation.EvaluationCriteriaID = MarkRecordOfPrimaryHeadTeacherConstant.Honesty;
                        objSummedEvaluationEducation.EndingComments = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_3) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_3).ToString().Trim();
                        dicSummedEvaluation.Add(MarkRecordOfPrimaryHeadTeacherConstant.Honesty, objSummedEvaluationEducation);

                        objSummedEvaluationEducation = SetValueSummedEvaluation(objAcademicYear, classId, educationLevelId, evaluationId, partitionId, pupilId, semester);
                        objSummedEvaluationEducation.EvaluationCriteriaID = MarkRecordOfPrimaryHeadTeacherConstant.LoveFamily;
                        objSummedEvaluationEducation.EndingComments = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_4) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_4).ToString().Trim();
                        dicSummedEvaluation.Add(MarkRecordOfPrimaryHeadTeacherConstant.LoveFamily, objSummedEvaluationEducation);
                        objEvaluation.CapacitesOrQualitiesSemester = dicSummedEvaluation;

                        dicSummedEnding = new Dictionary<int, SummedEndingEvaluation>();
                        objSummedEnding = new SummedEndingEvaluation
                        {
                            AcademicYearID = objAcademicYear.AcademicYearID,
                            ClassID = classId,
                            CreateTime = DateTime.Now,
                            EvaluationID = evaluationId,
                            LastDigitSchoolID = partitionId,
                            PupilID = pupilId,
                            SchoolID = objAcademicYear.SchoolID,
                            SemesterID = semester,
                            UpdateTime = DateTime.Now
                        };
                        commentEnding = (sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_ENDDING) == null) ? "" :
                                        sheet.GetCellValue(startRow, MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_ENDDING).ToString().Trim();
                        if (!string.IsNullOrEmpty(commentEnding))
                        {
                            if (commentEnding.Equals(MarkRecordOfPrimaryHeadTeacherConstant.OK))
                            {
                                objSummedEnding.EndingEvaluation = MarkRecordOfPrimaryHeadTeacherConstant.SHORT_OK;
                            }
                            else
                            {
                                objSummedEnding.EndingEvaluation = MarkRecordOfPrimaryHeadTeacherConstant.SHORT_NOT_OK;
                            }
                        }
                        else
                        {
                            objSummedEnding.EndingEvaluation = "";
                        }
                        dicSummedEnding.Add(evaluationId, objSummedEnding);
                        objEvaluation.DicSummedEndingEvaluation = dicSummedEnding;
                    }
                }
                #endregion


                #region "Validate du lieu cho tung sheet"
                //Kiem tra du lieu tung cot du lieu
                for (int i = startColumn; i <= endColumn; i++)
                {

                    if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
                    {
                        #region "Validate sheet Mon hoc va hoat dong giao duc"
                        if (sheet.GetCellValue(startRow, i) != null)
                        {
                            if (isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                            {
                                #region Validate mon tinh diem
                                //Cot diem cuoi ky
                                if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK)
                                {
                                    if (int.TryParse(sheet.GetCellValue(startRow, i).ToString().Trim(), out mark))
                                    {
                                        //evaluationObj.PeriodicEndingMark = mark;
                                        if (mark <= 0 || mark > 10)
                                        {
                                            objEvaluation.MessageError += "- Điểm kiểm tra định kỳ phải được nhập từ 1-10.\r\n";
                                            pass = false;
                                        }
                                    }
                                    else
                                    {
                                        objEvaluation.MessageError += "- Điểm kiểm tra định kỳ không phải là số.\r\n";
                                        pass = false;
                                    }
                                }
                                else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_ENDDING_EVALUATION)//Cot  danh gia
                                {
                                    endingEvaluation = sheet.GetCellValue(startRow, i).ToString().Trim();
                                    if (!endingEvaluation.ToLower().Equals(MarkRecordOfPrimaryHeadTeacherConstant.FINISH.ToLower())
                                       && !endingEvaluation.ToLower().Equals(MarkRecordOfPrimaryHeadTeacherConstant.NOTFINISH.ToLower()))
                                    {
                                        objEvaluation.MessageError += "-Đánh giá không đúng định dạng\r\n";
                                        pass = false;
                                    }
                                }
                                else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_COMMENTS_ENDDING) // Cot nhan xet HK
                                {
                                    IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString()));
                                    if (IsMatch)
                                    {
                                        objEvaluation.MessageError += "- Nhận xét đánh giá chứa ký tự đặc biệt.\r\n";
                                        pass = false;
                                    }

                                    if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                    {
                                        objEvaluation.MessageError += "- Số ký tự nhận xét cuối kỳ bé hơn hoặc bằng 500 ký tự.\r\n";
                                        pass = false;
                                    }
                                }
                                else
                                {
                                    if (sheet.GetCellValue(startRow, i) != null)
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            comment = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? i - 4 : i + 1;
                                            objEvaluation.MessageError += String.Format("- Số ký tự nhận xét tháng thứ {0} phải nhỏ hơn hoặc bằng 500 ký tự.\r\n", comment);
                                            pass = false;
                                        }

                                        IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString()));
                                        if (IsMatch)
                                        {
                                            objEvaluation.MessageError += "- Nhận xét đánh giá chứa ký tự đặc biệt.\r\n";
                                            pass = false;
                                        }
                                    }

                                }
                                #endregion

                            }
                            else
                            {
                                #region Validate mon nhan xet

                                if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_COMMENTS_ENDDING)
                                {
                                    IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString()));
                                    if (IsMatch)
                                    {
                                        objEvaluation.MessageError += "- Nhận xét đánh giá chứa ký tự đặc biệt.\r\n";
                                        pass = false;
                                    }

                                    if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                    {
                                        objEvaluation.MessageError += "- Số ký tự nhận xét cuối kỳ bé hơn hoặc bằng 500 ký tự.\r\n";
                                        pass = false;
                                    }
                                }
                                else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_END_MARK)
                                {
                                    endingEvaluation = sheet.GetCellValue(startRow, i).ToString().Trim();
                                    if (!endingEvaluation.ToLower().Equals(MarkRecordOfPrimaryHeadTeacherConstant.FINISH.ToLower())
                                       && !endingEvaluation.ToLower().Equals(MarkRecordOfPrimaryHeadTeacherConstant.NOTFINISH.ToLower()))
                                    {
                                        objEvaluation.MessageError += "-Đánh giá không đúng định dạng\r\n";
                                        pass = false;
                                    }
                                }
                                else
                                {
                                    if (sheet.GetCellValue(startRow, i) != null)
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            comment = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? i - 4 : i + 1;
                                            objEvaluation.MessageError += String.Format("- Số ký tự nhận xét tháng thứ {0} phải nhỏ hơn hoặc bằng 500 ký tự.\r\n", comment);
                                            pass = false;
                                        }

                                        IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString()));
                                        if (IsMatch)
                                        {
                                            objEvaluation.MessageError += "- Nhận xét đánh giá chứa ký tự đặc biệt.\r\n";
                                            pass = false;
                                        }
                                    }

                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                    else if (evaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES)
                    {
                        #region "Validate sheet Nang luc"
                        if (montId == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                        {
                            #region Chon danh gia cuoi ky
                            if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_1)
                            {
                                //Tu phuc vu, tu quan
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString())))
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            objEvaluation.MessageError += "-a) phải nhỏ hơn hoặc bằng 500 ký tự.\r\n";
                                            pass = false;
                                        }
                                    }
                                    else
                                    {
                                        objEvaluation.MessageError += "-a) không được phép nhập các ký tự nguy hiểm.\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_2)
                            {
                                //Giao tiếp, hợp tác
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString())))
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            objEvaluation.MessageError += "-b) phải nhỏ hơn hoặc bằng 500 ký tự\r\n";
                                            pass = false;
                                        }
                                    }
                                    else
                                    {
                                        objEvaluation.MessageError += "-b) không được phép nhập các ký tự nguy hiểm.\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_3)
                            {

                                //Tự học và giải quyết vấn đề
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString())))
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            objEvaluation.MessageError = "-c) phải nhỏ hơn hoặc bằng 500 ký tự.\r\n";
                                            pass = false;
                                        }
                                    }
                                    else
                                    {
                                        objEvaluation.MessageError += "-c) không được phép nhập các ký tự đặt biệt.\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_CAPACITIES_ENDDING)
                            {
                                //Danh gia
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!sheet.GetCellValue(startRow, i).ToString().Trim().Equals(MarkRecordOfPrimaryHeadTeacherConstant.OK)
                                        && !(sheet.GetCellValue(startRow, i).ToString().Trim().Equals(MarkRecordOfPrimaryHeadTeacherConstant.NOT_OK)))
                                    {
                                        objEvaluation.MessageError += "-Đánh giá không đúng định dạng\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Neu chon Thang
                            if (sheet.GetCellValue(startRow, i) != null)
                            {
                                if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                {
                                    //objEvaluation.MessageError += "- Số ký tự nhận xét tháng thứ phải nhỏ hơn hoặc bằng 500 ký tự.\r\n";
                                    comment = (semester == 1) ? i - 4 : i + 1;
                                    objEvaluation.MessageError += String.Format("- Số ký tự nhận xét tháng thứ {0} phải nhỏ hơn hoặc bằng 500 ký tự.\r\n", comment);
                                    pass = false;
                                }

                                IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString()));
                                if (IsMatch)
                                {
                                    objEvaluation.MessageError += "- Nhận xét đánh giá chứa ký tự đặc biệt.\r\n";
                                    pass = false;
                                }

                            }
                            #endregion
                        }

                        #endregion
                    }
                    else
                    {
                        #region "Validate sheet pham chat"
                        if (montId == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                        {
                            #region Chon danh gia cuoi ky
                            if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_1)
                            {
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString())))
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            objEvaluation.MessageError += "-a) phải nhỏ hơn hoặc bằng 500 ký tự.\r\n";
                                            pass = false;
                                        }
                                    }
                                    else
                                    {
                                        objEvaluation.MessageError += "-a) không được phép nhập các ký tự nguy hiểm.\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_2)
                            {
                                //Giao tiếp, hợp tác
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString())))
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            objEvaluation.MessageError += "-b) phải nhỏ hơn hoặc bằng 500 ký tự\r\n";
                                            pass = false;
                                        }
                                    }
                                    else
                                    {
                                        objEvaluation.MessageError += "-b) không được phép nhập các ký tự nguy hiểm.\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_3)
                            {
                                //Tự học và giải quyết vấn đề
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString())))
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            objEvaluation.MessageError = "-c) phải nhỏ hơn hoặc bằng 500 ký tự.\r\n";
                                            pass = false;
                                        }
                                    }
                                    else
                                    {
                                        objEvaluation.MessageError += "-c) không được phép nhập các ký tự nguy hiểm.\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_4)
                            {
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString())))
                                    {
                                        if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                        {
                                            objEvaluation.MessageError = "-d) phải nhỏ hơn hoặc bằng 500 ký tự.\r\n";
                                            pass = false;
                                        }
                                    }
                                    else
                                    {
                                        objEvaluation.MessageError += "-d) không được phép nhập các ký tự nguy hiểm.\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            else if (i == MarkRecordOfPrimaryHeadTeacherConstant.COLUMN_QUALITIES_ENDDING)
                            {
                                //Danh gia
                                if (sheet.GetCellValue(startRow, i) != null)
                                {
                                    if (!sheet.GetCellValue(startRow, i).ToString().Trim().Equals(MarkRecordOfPrimaryHeadTeacherConstant.OK)
                                        && !(sheet.GetCellValue(startRow, i).ToString().Trim().Equals(MarkRecordOfPrimaryHeadTeacherConstant.NOT_OK)))
                                    {
                                        objEvaluation.MessageError += "-Đánh giá không đúng định dạng\r\n";
                                        pass = false;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Neu chon Thang
                            if (sheet.GetCellValue(startRow, i) != null)
                            {
                                if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryHeadTeacherConstant.LIMIT_COMMENTS)
                                {
                                    comment = (semester == 1) ? i - 4 : i + 1;
                                    objEvaluation.MessageError += String.Format("- Số ký tự nhận xét tháng thứ {0} phải nhỏ hơn hoặc bằng 500 ký tự.\r\n", comment);
                                    pass = false;
                                }
                                IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString()));
                                if (IsMatch)
                                {
                                    objEvaluation.MessageError += "- Nhận xét đánh giá chứa ký tự đặc biệt.\r\n";
                                    pass = false;
                                }

                            }
                            #endregion
                        }
                        #endregion
                    }
                    //objEvaluation.arrComment = arrComment;

                }
                #endregion
                if (!String.IsNullOrEmpty(objEvaluation.MessageError))
                {
                    objEvaluation.MessageError = string.Format("Sheet {0}: {1}", sheet.Name, objEvaluation.MessageError);
                }
                objEvaluation.EvaluationCriteriaID = subjectId;
                objEvaluation.IsCommenting = isCommenting;
                objEvaluation.Error = pass;
                lstOutPut.Add(objEvaluation);
                startRow++;
            }
        }

        private void ImportExcel(List<EvaluationCommentsReportBO> listOutPut, AcademicYear objAcademicYear, int classId, int educationLevelId, int semesterId, int typeOfTeacher, int monthId, int partitionId)
        {
            List<EvaluationComments> lstEvaluationInsertOrUpdate = new List<EvaluationComments>();
            List<SummedEvaluation> lstSummedEvaluation = new List<SummedEvaluation>();
            List<SummedEndingEvaluation> lstSummnedEnding = new List<SummedEndingEvaluation>();
            SummedEvaluation objSummedEvaluation = null;
            SummedEndingEvaluation objSummedEnding = null;
            EvaluationCommentsReportBO sourceData = null;
            EvaluationComments sourceEvaluation;
            for (int i = 0; i < listOutPut.Count; i++)
            {
                sourceData = listOutPut[i];
                //Chi import cac hoc sinh co trang thai dang hoc
                if (sourceData.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                if (monthId != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                {
                    if (sourceData.EvaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
                    {
                        sourceEvaluation = sourceData.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION];
                        if (sourceEvaluation != null)
                        {
                            lstEvaluationInsertOrUpdate.Add(sourceEvaluation);
                        }
                    }
                    else if (sourceData.EvaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES)
                    {
                        sourceEvaluation = sourceData.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES];
                        if (sourceEvaluation != null)
                        {
                            lstEvaluationInsertOrUpdate.Add(sourceEvaluation);
                        }
                    }
                    else
                    {
                        sourceEvaluation = sourceData.DicEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES];
                        if (sourceEvaluation != null)
                        {
                            lstEvaluationInsertOrUpdate.Add(sourceEvaluation);
                        }
                    }
                }
                if (monthId == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                {
                    //Nhan xet danh gia cuoi ky cua tung mat danh gia
                    if (sourceData.EvaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
                    {
                        objSummedEvaluation = sourceData.DicSummedEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION];
                        if (objSummedEvaluation != null)
                        {
                            lstSummedEvaluation.Add(objSummedEvaluation);
                        }
                    }
                    else if (sourceData.EvaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES)
                    {
                        //Tieu chi tu phuc vu
                        objSummedEvaluation = sourceData.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.SeftServingManagement];
                        if (objSummedEvaluation != null)
                        {
                            lstSummedEvaluation.Add(objSummedEvaluation);
                        }

                        //Tieu chi giao tiep
                        objSummedEvaluation = sourceData.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.CommunicationCoomperation];
                        if (objSummedEvaluation != null)
                        {
                            lstSummedEvaluation.Add(objSummedEvaluation);
                        }

                        objSummedEvaluation = sourceData.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.StudyAndSolveProblems];
                        if (objSummedEvaluation != null)
                        {
                            lstSummedEvaluation.Add(objSummedEvaluation);
                        }

                        objSummedEnding = sourceData.DicSummedEndingEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES];
                        if (objSummedEnding != null)
                        {
                            lstSummnedEnding.Add(objSummedEnding);
                        }
                    }
                    else if (sourceData.EvaluationId == MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES)
                    {
                        //Cham chi cham lam
                        objSummedEvaluation = sourceData.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.HandWorking];
                        if (objSummedEvaluation != null)
                        {
                            lstSummedEvaluation.Add(objSummedEvaluation);
                        }

                        //Tu tin tu trong
                        objSummedEvaluation = sourceData.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.Confidence];
                        if (objSummedEvaluation != null)
                        {
                            lstSummedEvaluation.Add(objSummedEvaluation);
                        }

                        // Trung thuc
                        objSummedEvaluation = sourceData.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.Honesty];
                        if (objSummedEvaluation != null)
                        {
                            lstSummedEvaluation.Add(objSummedEvaluation);
                        }

                        // Yeu gia dinh
                        objSummedEvaluation = sourceData.CapacitesOrQualitiesSemester[MarkRecordOfPrimaryHeadTeacherConstant.LoveFamily];
                        if (objSummedEvaluation != null)
                        {
                            lstSummedEvaluation.Add(objSummedEvaluation);
                        }

                        objSummedEnding = sourceData.DicSummedEndingEvaluation[MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES];
                        if (objSummedEnding != null)
                        {
                            lstSummnedEnding.Add(objSummedEnding);
                        }
                    }

                }
            }

            if (lstEvaluationInsertOrUpdate != null && lstEvaluationInsertOrUpdate.Count > 0)
            {
                EvaluationCommentsBusiness.InsertOrUpdatebyClass(objAcademicYear.SchoolID, objAcademicYear.AcademicYearID, classId, educationLevelId, semesterId, typeOfTeacher, lstEvaluationInsertOrUpdate);
            }
            if (lstSummedEvaluation != null && lstSummedEvaluation.Count > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("PartitionId", partitionId);
                dic.Add("SchoolId", objAcademicYear.SchoolID);
                dic.Add("AcademicYearId", objAcademicYear.AcademicYearID);
                dic.Add("ClassId", classId);
                dic.Add("EducationLevelId", educationLevelId);
                dic.Add("SemesterId", semesterId);
                dic.Add("IsRetest", false);//Co cap nhat diem thi lai
                SummedEvaluationBusiness.InsertOrUpdatebyClass(dic, lstSummedEvaluation);
            }
            if (lstSummnedEnding != null && lstSummnedEnding.Count > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("PartitionId", partitionId);
                dic.Add("SchoolID", objAcademicYear.SchoolID);
                dic.Add("AcademicYearID", objAcademicYear.AcademicYearID);
                dic.Add("SemesterID", semesterId);
                SummedEndingEvaluationBusiness.InsertOrUpdatebyClass(classId, dic, lstSummnedEnding);
            }
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private string CheckLockMark(int classID)
        {
            string strLockMark = string.Empty;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("ClassID", classID);
            List<LockMarkPrimary> lockmarlList = LockMarkPrimaryBusiness.SearchLockedMark(dic).ToList();

            if (lockmarlList != null && lockmarlList.Count > 0)
            {
                return lockmarlList.Select(p => p.LockPrimary).FirstOrDefault();
            }
            else
            {
                return string.Empty;
            }
        }

        private string ErrValidateFileExcel(IVTWorksheet sheet, int evaluationID, int semesterID, int classID, AcademicYear objAcademicYear, string className, int? subjectID = 0, int monthID = 0, string subjectName = "")
        {
            try
            {
                string Title = "";
                string semesterName = "";
                string academicYearName = objAcademicYear.DisplayTitle.Replace("-", "").Replace(" ", "");
                string TitleYear = "";
                if (semesterID == 1)
                {
                    semesterName = "HOCKY1";
                }
                else if (semesterID == 2)
                {
                    semesterName = "HOCKY2";
                }

                Title = Utils.Utils.StripVNSign(sheet.GetCellValue(3, 1).ToString().Replace(" ", "")).ToUpper();
                TitleYear = Utils.Utils.StripVNSign(sheet.GetCellValue(4, 1).ToString().Replace("-", "").Replace(" ", ""));
                if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(TitleYear))
                {
                    return "File excel không đúng mẫu.";
                }

                if (!Title.Contains(semesterName))
                {
                    return "Học kỳ trong file excel không đúng.";
                }
                if (!Title.Contains(Utils.Utils.StripVNSignAndSpace(className.ToUpper())))
                {
                    return "Lớp học trong file excel không đúng.";
                }
                if (!TitleYear.Contains(Utils.Utils.StripVNSignAndSpace(academicYearName)))
                {
                    return "Năm học trong file excel không đúng.";
                }
                if (evaluationID == MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION)
                {
                    if (!string.IsNullOrEmpty(subjectName) && monthID == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                    {
                        if (!Title.Contains(subjectName.ToUpper()))
                        {
                            return "Môn học trong file excel không đúng.";
                        }
                    }
                    if (Title.Contains("BANGDANHGIACHATLUONGMON") && monthID != MarkRecordOfPrimaryHeadTeacherConstant.MONTH_11)
                    {

                        return "File dữ liệu không hợp lệ";

                    }

                }

                return "";
            }
            catch
            {
                return "File excel không đúng mẫu.";
            }
        }


        private EvaluationCommentsHistory ConvertToHistoryEvaluationComments(EvaluationComments objA)
        {
            if (objA == null)
            {
                return new EvaluationCommentsHistory();
            }
            EvaluationCommentsHistory objB = new EvaluationCommentsHistory
            {
                EvaluationCommentsID = objA.EvaluationCommentsID,
                AcademicYearID = objA.AcademicYearID,
                ClassID = objA.ClassID,
                CommentsM1M6 = objA.CommentsM1M6,
                CommentsM2M7 = objA.CommentsM2M7,
                CommentsM3M8 = objA.CommentsM3M8,
                CommentsM4M9 = objA.CommentsM4M9,
                CommentsM5M10 = objA.CommentsM5M10,
                CreateTime = objA.CreateTime,
                EducationLevelID = objA.EducationLevelID,
                EvaluationCriteriaID = objA.EvaluationCriteriaID,
                EvaluationID = objA.EvaluationID,
                LastDigitSchoolID = objA.LastDigitSchoolID,
                NoteInsert = objA.NoteInsert,
                NoteUpdate = objA.NoteUpdate,
                PupilID = objA.PupilID,
                SchoolID = objA.SchoolID,
                SemesterID = objA.SemesterID,
                TypeOfTeacher = objA.TypeOfTeacher,
                UpdateTime = objA.UpdateTime
            };
            return objB;
        }

        private SummedEvaluationHistory ConvertToHistorySummedEvaluation(SummedEvaluation objA)
        {
            if (objA == null)
            {
                return new SummedEvaluationHistory();
            }
            SummedEvaluationHistory objB = new SummedEvaluationHistory
            {
                SummedEvaluationID = objA.SummedEvaluationID,
                AcademicYearID = objA.AcademicYearID,
                ClassID = objA.ClassID,
                CreateTime = objA.CreateTime,
                EducationLevelID = objA.EducationLevelID,
                EvaluationCriteriaID = objA.EvaluationCriteriaID,
                EvaluationID = objA.EvaluationID,
                LastDigitSchoolID = objA.LastDigitSchoolID,
                PupilID = objA.PupilID,
                SchoolID = objA.SchoolID,
                SemesterID = objA.SemesterID,
                UpdateTime = objA.UpdateTime,
                EndingComments = objA.EndingComments,
                EndingEvaluation = objA.EndingEvaluation,
                EvaluationReTraining = objA.EvaluationReTraining,
                PeriodicEndingMark = objA.PeriodicEndingMark,
                RetestMark = objA.RetestMark
            };
            return objB;
        }

        private void SetVisbileButton(int semester, int classID, int subjectID)
        {
            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (!(isAdminShcool || isGVCN))
            {
                ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_VISIBLED_BUTTON] = false;
            }
            else if (isAdminShcool)
            {
                ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_VISIBLED_BUTTON] = true;
            }
            else if (isGVCN)
            {
                if (semester == 1)
                {
                    if (dateTimeNow < objAca.FirstSemesterStartDate.Value || dateTimeNow > objAca.FirstSemesterEndDate)
                    {
                        ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_VISIBLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_VISIBLED_BUTTON] = true;
                    }
                }
                else if (semester == 2)
                {
                    if (dateTimeNow < objAca.SecondSemesterStartDate.Value || dateTimeNow > objAca.SecondSemesterEndDate.Value)
                    {
                        ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_VISIBLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[MarkRecordOfPrimaryHeadTeacherConstant.IS_VISIBLED_BUTTON] = true;
                    }
                }
            }
        }


        private string GetEvaluationName(int evaluationID)
        {
            string retName = "";
            switch (evaluationID)
            {
                case MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION:
                    retName = "Môn học và HĐGD GVCN";
                    break;
                case MarkRecordOfPrimaryHeadTeacherConstant.TAB_CAPACTIES:
                    retName = "Năng lực GVCN";
                    break;
                case MarkRecordOfPrimaryHeadTeacherConstant.TAB_QUALITIES:
                    retName = "Phẩm chất GVCN";
                    break;
            }
            return retName;
        }

        /// <summary>
        /// Dua cac thong tin vao log
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="evaluationID"></param>
        /// <param name="semester"></param>
        /// <param name="monthId"></param>
        /// <param name="action"></param>
        /// <param name="functionName"></param>
        private void SetLog(int classId, int evaluationID, int semester, int monthId, int subjectId, string action, string className)
        {
            string monthName = "";
            string subjectName = "";
            if (monthId > 0)
            {
                MonthComments objMonth = MonthCommentsBusiness.Find(monthId);
                monthName = (objMonth == null) ? "" : objMonth.Name;
            }
            if (subjectId > 0)
            {
                SubjectCat objSubject = SubjectCatBusiness.Find(subjectId);
                subjectName = (objSubject == null) ? "" : objSubject.DisplayName;
            }
            if (String.IsNullOrEmpty(className))
            {
                ClassProfile objClass = ClassProfileBusiness.Find(classId);
                className = objClass.DisplayName;
            }
            string evaluationName = GetEvaluationName(evaluationID);

            string desCription = "";
            if (action == SMAS.Business.Common.GlobalConstants.ACTION_ADD || action == SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)
            {
                desCription = String.Format("Lưu nhận xét đánh giá lớp: {0}, môn học:  {1}, học kỳ: {2}, tháng: {3}, mặt đánh giá: {4}", className, subjectName, semester, monthName, evaluationName);
            }
            else if (action == SMAS.Business.Common.GlobalConstants.ACTION_DELETE)
            {
                desCription = String.Format("Xóa nhận xét đánh giá lớp: {0}, môn học: {1}, học kỳ: {2}, tháng: {3}, mặt đánh giá: {4}", className, subjectName, semester, monthName, evaluationName);
            }
            else if (action == SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)
            {
                desCription = String.Format("Import nhận xét đánh giá lớp: {0}, học kỳ: {1}", className, semester);
            }
            string parameter = String.Format("Class_id:{0}, subject_id:{1} Semester:{2}, Thang:{3}, Tab:{4}", classId, subjectId, semester, monthId, evaluationID);
            SetViewDataActionAudit(String.Empty, String.Empty, classId.ToString(), desCription, parameter, "Sổ theo dõi CLGD GVCN", action, desCription);

        }

        private string GetMonthBySemeseter(int semester, int monthIDlocation)
        {
            string strmonth = string.Empty;

            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_1)
                {
                    strmonth = " tháng thứ nhất ";
                }
                else if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_2)
                {
                    strmonth = " tháng thứ hai ";
                }
                else if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_3)
                {
                    strmonth = " tháng thứ ba ";
                }
                else if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_4)
                {
                    strmonth = " tháng thứ tư ";
                }
                else if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_5)
                {
                    strmonth = " tháng thứ năm ";
                }
            }
            else
            {
                if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_1)
                {
                    strmonth = " tháng thứ sáu ";
                }
                else if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_2)
                {
                    strmonth = " tháng thứ bảy ";
                }
                else if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_3)
                {
                    strmonth = " tháng thứ tám ";
                }
                else if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_4)
                {
                    strmonth = " tháng thứ chín ";
                }
                else if (monthIDlocation == MarkRecordOfPrimaryHeadTeacherConstant.MONTH_5)
                {
                    strmonth = " tháng thứ mười ";
                }
            }

            return strmonth;

        }

        private SummedEvaluation SetValueSummedEvaluation(AcademicYear objAcademicYear, int classId, int educationLevelId, int evaluationId, int partitionId, int pupilId, int semesterId)
        {
            SummedEvaluation objSummedEvaluationEducation = new SummedEvaluation
            {
                AcademicYearID = objAcademicYear.AcademicYearID,
                ClassID = classId,
                CreateTime = DateTime.Now,
                EducationLevelID = educationLevelId,
                EvaluationID = evaluationId,
                LastDigitSchoolID = partitionId,
                PupilID = pupilId,
                SchoolID = objAcademicYear.SchoolID,
                SemesterID = semesterId,
                UpdateTime = DateTime.Now
            };
            return objSummedEvaluationEducation;
        }

        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult CopyData(string arrSubjectId, int iCopyObject, int iCopyLocation, int classId, int semesterId, int educationLevelId, int? month, int subjectIdSelect)
        {
            if (string.IsNullOrEmpty(arrSubjectId)) return Json(new { Type = "error", Message = "Thầy cô chưa chọn môn học để sao chép." });
            if (iCopyObject != MarkRecordOfPrimaryHeadTeacherConstant.COPY_OVER_WRITE_ALL_PUPIL
                && iCopyObject != MarkRecordOfPrimaryHeadTeacherConstant.COPY_OVER_WRITE_ALL_PUPIL_NOT_COMMENT
                && iCopyLocation != MarkRecordOfPrimaryHeadTeacherConstant.COPY_COMMNET_OF_MONTH
                && iCopyLocation != MarkRecordOfPrimaryHeadTeacherConstant.COPY_COMMNET_ALL_MONTH_IN_SEMESTER)
                return Json(new { Type = "error", Message = "Dữ liệu không hợp lệ." });

            #region check permission
            bool isHeadTeacher = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classId);
            if (!_globalInfo.IsAdminSchoolRole && !isHeadTeacher)
            {
                return Json(new { Type = "error", Message = "Thầy cô không có quyền thao tác chức năng này." });
            }
            #endregion

            List<int> listSubjectID = arrSubjectId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolId",_globalInfo.SchoolID},
                {"AcademicYearId",_globalInfo.AcademicYearID},
                {"ClassId",classId},
                {"SemesterId",semesterId},
                {"EvaluationId", MarkRecordOfPrimaryHeadTeacherConstant.TAB_EDUCATION},
                {"TypeOfTeacher", MarkRecordOfPrimaryHeadTeacherConstant.GVCN},
                {"EduationLevelId", educationLevelId},
                {"listSubjectId", listSubjectID},
                {"Status", GlobalConstants.PUPIL_STATUS_STUDYING},                
                {"Month", month},                
            };
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();
            if (UtilsBusiness.IsMoveHistory(aca))
            {
                lstActionAuditBO = EvaluationCommentsHistoryBusiness.CopyDataEvaluationHistoryForHeadTeacher(objDic, iCopyObject, iCopyLocation, month, subjectIdSelect);
            }
            else
            {
                lstActionAuditBO = EvaluationCommentsBusiness.CopyDataEvaluationForHeadTeacher(objDic, iCopyObject, iCopyLocation, month, subjectIdSelect);
            }

            WriteLog(lstActionAuditBO, GlobalConstants.ACTION_UPDATE);
            return Json(new { Type = "success", Message = "Sao chép dữ liệu thành công." });
        }

        private void WriteLog(List<EvaluationCommentActionAuditBO> lstActionAuditBO, string action)
        {
            if (lstActionAuditBO != null && lstActionAuditBO.Count > 0)
            {
                // Tạo data ghi log
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();

                for (int i = 0; i < lstActionAuditBO.Count; i++)
                {
                    EvaluationCommentActionAuditBO aab = lstActionAuditBO[i];

                    objectIDStr.Append(aab.ObjectId).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append("Sổ theo dõi CLGD (GVCN)").Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsStr.Append("Lơp: " + aab.ClassName).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectStr.Append(aab.StrOldObjectValue).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectStr.Append(aab.StrNewObjectValue).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append("Sổ theo dõi CLGD (GVCN)").Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(action).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsStr.Append(aab.UserDescription).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    ViewData[CommonKey.AuditActionKey.OldJsonObject] = oldObjectStr.ToString();
                    ViewData[CommonKey.AuditActionKey.NewJsonObject] = newObjectStr.ToString();
                    ViewData[CommonKey.AuditActionKey.ObjectID] = objectIDStr.ToString();
                    ViewData[CommonKey.AuditActionKey.Description] = descriptionStr.ToString();
                    ViewData[CommonKey.AuditActionKey.Parameter] = paramsStr.ToString();
                    ViewData[CommonKey.AuditActionKey.userAction] = userActionsStr.ToString();
                    ViewData[CommonKey.AuditActionKey.userFunction] = userFuntionsStr.ToString();
                    ViewData[CommonKey.AuditActionKey.userDescription] = userDescriptionsStr.ToString();
                }


            }
        }

        private List<EvaluationGridModel> SetCommentGVBM(List<EvaluationGridModel> lstEvaluationGridModel,int classID,int evaluationID,int semesterID,int monthID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"SemesterID",semesterID},
                {"EvaluationID",evaluationID}
            };
            List<EvaluationCommentsBO> lstEV = new List<EvaluationCommentsBO>();
            EvaluationGridModel objEvaluationModel = null;
            lstEV = EvaluationCommentsBusiness.GetListEvaluationComments(dic);
            List<EvaluationCommentsBO> lsttmp = null;
            EvaluationCommentsBO objtmp = null;
            StringBuilder strComment1 = new StringBuilder();
            StringBuilder strComment2 = new StringBuilder();
            StringBuilder strComment3 = new StringBuilder();
            StringBuilder strComment4 = new StringBuilder();
            StringBuilder strComment5 = new StringBuilder();
            for (int i = 0; i < lstEvaluationGridModel.Count; i++)
            {
                strComment1 = new StringBuilder();
                strComment2 = new StringBuilder();
                strComment3 = new StringBuilder();
                strComment4 = new StringBuilder();
                strComment5 = new StringBuilder();
                objEvaluationModel = lstEvaluationGridModel[i];
                lsttmp = lstEV.Where(p => p.PupilID == objEvaluationModel.PupilID).ToList();
                for (int j = 0; j < lsttmp.Count; j++)
                {
                    objtmp = lsttmp[j];
                    if (evaluationID == 1)
                    {
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment1) && !string.IsNullOrEmpty(objtmp.Comment1))
                        {
                            strComment1.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment1).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment2) && !string.IsNullOrEmpty(objtmp.Comment2))
                        {
                            strComment2.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment2).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment3) && !string.IsNullOrEmpty(objtmp.Comment3))
                        {
                            strComment3.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment3).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment4) && !string.IsNullOrEmpty(objtmp.Comment4))
                        {
                            strComment4.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment4).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment5) && !string.IsNullOrEmpty(objtmp.Comment5))
                        {
                            strComment5.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment5).Append(SPACE);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment1) && !string.IsNullOrEmpty(objtmp.Comment1))
                        {
                            strComment1.Append(objtmp.Comment1).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment2) && !string.IsNullOrEmpty(objtmp.Comment2))
                        {
                            strComment2.Append(objtmp.Comment2).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment3) && !string.IsNullOrEmpty(objtmp.Comment3))
                        {
                            strComment3.Append(objtmp.Comment3).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment4) && !string.IsNullOrEmpty(objtmp.Comment4))
                        {
                            strComment4.Append(objtmp.Comment4).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationModel.Comment5) && !string.IsNullOrEmpty(objtmp.Comment5))
                        {
                            strComment5.Append(objtmp.Comment5).Append(SPACE);
                        }
                    }
                }

                if (string.IsNullOrEmpty(objEvaluationModel.Comment1) && !string.IsNullOrEmpty(strComment1.ToString()))
                {
                    objEvaluationModel.Comment1 = strComment1.ToString().Substring(0, strComment1.ToString().Length - 2);
                    objEvaluationModel.ischk1 = false;
                }
                if (string.IsNullOrEmpty(objEvaluationModel.Comment2) && !string.IsNullOrEmpty(strComment2.ToString()))
                {
                    objEvaluationModel.Comment2 = strComment2.ToString().Substring(0, strComment2.ToString().Length - 2);
                    objEvaluationModel.ischk2 = false;
                }
                if (string.IsNullOrEmpty(objEvaluationModel.Comment3) && !string.IsNullOrEmpty(strComment3.ToString()))
                {
                    objEvaluationModel.Comment3 = strComment3.ToString().Substring(0, strComment3.ToString().Length - 2);
                    objEvaluationModel.ischk3 = false;
                }
                if (string.IsNullOrEmpty(objEvaluationModel.Comment4) && !string.IsNullOrEmpty(strComment4.ToString()))
                {
                    objEvaluationModel.Comment4 = strComment4.ToString().Substring(0, strComment4.ToString().Length - 2);
                    objEvaluationModel.ischk4 = false;
                }
                if (string.IsNullOrEmpty(objEvaluationModel.Comment5) && !string.IsNullOrEmpty(strComment5.ToString()))
                {
                    objEvaluationModel.Comment5 = strComment5.ToString().Substring(0, strComment5.ToString().Length - 2);
                    objEvaluationModel.ischk5 = false;
                }

            }
            return lstEvaluationGridModel;
        }

        [HttpPost]
        public PartialViewResult AjaxLoadSubjectImport()
        {
            int classid = SMAS.Business.Common.Utils.GetInt(Request["ClassID"]);
            int semesterid = SMAS.Business.Common.Utils.GetInt(Request["SemesterID"]);
            int TypeID = SMAS.Business.Common.Utils.GetInt(Request["TypeSelectID"]);
            bool ViewAll = false;

            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            if (classid > 0 && semesterid > 0)
            {
                lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semesterid, classid, ViewAll).Where(u => semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                  join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                  where s.IsActive == true
                                  && s.IsApprenticeshipSubject == false
                                  select new SubjectCatBO()
                                  {
                                      SubjectCatID = s.SubjectCatID,
                                      DisplayName = s.DisplayName,
                                      IsCommenting = cs.IsCommenting,
                                      OrderInSubject = s.OrderInSubject
                                  }
                                  ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();
            int UserAccountID = _globalInfo.UserAccountID;

            // Neu la admin truong thi hien thi het
            if (new GlobalInfo().IsAdminSchoolRole)
            {
                foreach (SubjectCatBO item in lsClassSubject)
                {
                    listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, false));
                }
            }
            else
            {
                int? TeacherID = UserAccountBusiness.Find(UserAccountID).EmployeeID;
                List<int> listPermitSubjectID = new List<int>();
                if (TeacherID.HasValue)
                {
                    // Phan cong giao vu la giao vien bo mon
                    if (ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == classid &&
                                                            o.AcademicYearID == _globalInfo.AcademicYearID &&
                                                            o.SchoolID == _globalInfo.SchoolID &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER).Count() > 0
                                                            )
                    {
                        listPermitSubjectID = lsClassSubject.Select(o => o.SubjectCatID).ToList();
                    }
                    else
                    {
                        IDictionary<string, object> searchInfo = new Dictionary<string, object>();
                        searchInfo["ClassID"] = classid;
                        searchInfo["TeacherID"] = TeacherID;
                        searchInfo["Semester"] = semesterid;
                        searchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                        searchInfo["IsActive"] = true;
                        listPermitSubjectID = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfo).Select(o => o.SubjectID).ToList();
                    }
                }
                foreach (SubjectCatBO item in lsClassSubject)
                {
                    bool typeSubject = this.GetTypeSubject(item.SubjectCatID) == 1;
                    if (listPermitSubjectID.Contains(item.SubjectCatID))
                    {
                        listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, true, typeSubject));
                    }
                    else
                    {
                        listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, typeSubject));
                    }
                }
            }
            ViewData[MarkRecordOfPrimaryHeadTeacherConstant.LIST_SUBJECT] = listSJ;
            if (TypeID == 1)
            {
                return PartialView("_SubjectClass");
            }
            else if (TypeID == 2)
            {
                return PartialView("_SubjectClassCapacities");
            }
            else
            {
                return PartialView("_SubjectClassQualities");
            }
        }
        private int GetTypeSubject(int? SubjectID)
        {
            int typeSubject = 0;
            //const 1 mon tinh diem va mon tinh diem ket hop nhan xet; 2 mon nhan xet

            if (Session["ListSubject"] != null)
            {
                List<SubjectCatBO> lsClassSubject = Session["ListSubject"] as List<SubjectCatBO>;

                var lclassSubject = lsClassSubject.Where(o => o.SubjectCatID == SubjectID.Value);

                if (lclassSubject != null && lclassSubject.Count() > 0)
                {
                    SubjectCatBO classSubject = lclassSubject.FirstOrDefault();

                    if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        return 1;
                    }
                    else
                    {
                        return 2;
                    }
                }
            }
            return typeSubject;
        }
    }
}
