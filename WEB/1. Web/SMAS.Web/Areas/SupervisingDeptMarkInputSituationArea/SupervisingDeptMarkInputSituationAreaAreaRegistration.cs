﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SupervisingDeptMarkInputSituationArea
{
    public class SupervisingDeptMarkInputSituationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SupervisingDeptMarkInputSituationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SupervisingDeptMarkInputSituationArea_default",
                "SupervisingDeptMarkInputSituationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
