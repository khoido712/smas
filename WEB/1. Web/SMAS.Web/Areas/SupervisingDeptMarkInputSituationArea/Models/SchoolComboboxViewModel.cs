﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SupervisingDeptMarkInputSituationArea.Models
{
    public class SchoolComboboxViewModel
    {
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string OrderName { get; set; }
    }
}