﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SupervisingDeptMarkInputSituationArea.Models
{
    public class SearchViewModel
    {
        public int ReportType { get; set; }
        public int ProvinceID { get; set; }
        public int DistrictID { get; set; }
        public int AcademicYearID { get; set; }
        public int AppliedLevel { get; set; }
        public int Semester { get; set; }
        public int? SubjectID { get; set; }
        public int? SchoolID { get; set; }
    }
}