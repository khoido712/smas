﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.SupervisingDeptMarkInputSituationArea.Models;
using System.IO;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.SupervisingDeptMarkInputSituationArea.Controllers
{
    public class SupervisingDeptMarkInputSituationController : BaseController
    {
        #region register contructor
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IInputStatisticsBusiness InputStatisticsBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness; 
        public SupervisingDeptMarkInputSituationController(ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IReportDefinitionBusiness ReportDefinitionBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness
            , IDistrictBusiness DistrictBusiness
            , IProvinceBusiness ProvinceBusiness
            , IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IEmployeeBusiness EmployeeBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , IClassAssigmentBusiness ClassAssigmentBusiness
            , ITeachingAssignmentBusiness TeachingAssignmentBusiness
            , IMarkRecordBusiness MarkRecordBusiness
            , IPupilRankingBusiness PupilRankingBusiness
            , IInputStatisticsBusiness InputStatisticsBusiness
            , IProcessedReportBusiness ProcessedReportBusiness
            , ISupervisingDeptBusiness supervisingDeptBusiness
            , IMarkStatisticBusiness markStatisticBusiness)
        {
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportMarkInputSituationBusiness = ReportMarkInputSituationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ClassAssigmentBusiness = ClassAssigmentBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.InputStatisticsBusiness = InputStatisticsBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.MarkStatisticBusiness = markStatisticBusiness;
        }
        #endregion
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        public JsonResult AjaxLoadDistrict(int? provinceId)
        {
            IQueryable<District> lstDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == _globalInfo.ProvinceID && o.IsActive == true);
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                lstDistrict = lstDistrict.Where(o => o.DistrictID == _globalInfo.DistrictID);
            }
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                return Json(new SelectList(lstDistrict.OrderBy(p=>p.DistrictName).ToList(), "DistrictID", "DistrictName", _globalInfo.DistrictID));
            }
            else
            {
                return Json(new SelectList(lstDistrict.OrderBy(p => p.DistrictName).ToList(), "DistrictID", "DistrictName"));
            }
        }
        public JsonResult AjaxLoadSubject(int Year,int AppliedLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = Year;
            dic["ProvinceID"] = _globalInfo.ProvinceID;
            dic["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;
            dic["AppliedLevel"] = AppliedLevelID;
            List<SubjectCatBO> ListSubject = SupervisingDeptBusiness.SearchSubject(dic);
            SubjectCatBO objSCBO = new SubjectCatBO();
            List<string> lstStrSubjectName = ListSubject.Select(p => p.DisplayName).ToList();

            for (int i = 0; i < ListSubject.Count; i++)
            {
                objSCBO = ListSubject[i];
                if (lstStrSubjectName.Count(p=>p == objSCBO.DisplayName) > 1)
                {
                    objSCBO.DisplayName = objSCBO.DisplayName + "(" + (objSCBO.IsCommenting == 0 ? "Môn tính điểm" : "Môn nhận xét") + ")";
                }
            }

            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                objSCBO = new SubjectCatBO();
                objSCBO.SubjectCatID = -1;
                objSCBO.DisplayName = "Năng lực";
                ListSubject.Add(objSCBO);
                objSCBO = new SubjectCatBO();
                objSCBO.SubjectCatID = -2;
                objSCBO.DisplayName = "Phẩm chất";
                ListSubject.Add(objSCBO);
            }
            return Json(new SelectList(ListSubject, "SubjectCatID", "DisplayName"));
        }
        public PartialViewResult AjaxLoadSchool(int DistrictID, int AppliedLevelID)
        {
            List<int> lstEducationGrade = new List<int>();
            if (AppliedLevelID == 1)
            {
                lstEducationGrade.Add(1);
                lstEducationGrade.Add(3);
                lstEducationGrade.Add(7);
            }
            else if (AppliedLevelID == 2)
            {
                lstEducationGrade.Add(2);
                lstEducationGrade.Add(3);
                lstEducationGrade.Add(6);
                lstEducationGrade.Add(7);
            }
            else if (AppliedLevelID == 3)
            {
                lstEducationGrade.Add(4);
                lstEducationGrade.Add(6);
                lstEducationGrade.Add(7);
            }

            List<SchoolComboboxViewModel> lstSchool = (from sp in SchoolProfileBusiness.All
                                                       where sp.ProvinceID == _globalInfo.ProvinceID
                                                       && (sp.DistrictID == DistrictID || DistrictID == 0)
                                                       && lstEducationGrade.Contains(sp.EducationGrade)
                                                       select new SchoolComboboxViewModel
                                                       {
                                                           SchoolID = sp.SchoolProfileID,
                                                           SchoolName = sp.SchoolName,
                                                           OrderName = sp.SchoolName
                                                       }).ToList();
            SchoolComboboxViewModel objSCVM = new SchoolComboboxViewModel();
            objSCVM.SchoolID = 0;
            objSCVM.SchoolName = "[Tất cả]";
            objSCVM.OrderName = "1";
            lstSchool.Add(objSCVM);
            ViewData[SupervisingDeptMarkInputSituationConstants.CBO_SCHOOL] = lstSchool.OrderBy(p => p.OrderName).ToList();
            return PartialView("_SchoolCombobox");
        }
        private int GetRole()
        {
            if (_globalInfo.IsSystemAdmin)
            {
                return 2;
            }
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        private void SetViewData()
        {
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            AcademicYear AcademicYear = null;
            int curSemester = 1;
            int curYear = 0;
            if (_globalInfo.IsSystemAdmin)
            {
                curYear = DateTime.Now.Year;
                int firstYear = 2011;
                for (int i = curYear + 2; i >= firstYear; i--)
                {
                    AcademicYear = new AcademicYear
                    {
                        Year = i - 1,
                        DisplayTitle = String.Format("{0} -{1}", i - 1, i)
                    };
                    lstAcademicYear.Add(AcademicYear);
                }
                if (DateTime.Now.Month <= 8)
                {
                    curYear = curYear - 1;
                }
                curSemester = 1;
                ViewData[SupervisingDeptMarkInputSituationConstants.LS_YEAR] = new SelectList(lstAcademicYear, "Year", "DisplayTitle", curYear);
            }
            else
            {
                // nam hoc cho phong/so
                List<int> lstYear = AcademicYearBusiness.GetListYearForSupervisingDept_Pro(_globalInfo.SupervisingDeptID.Value).ToList();
                for (int i = 0; i < lstYear.Count(); i++)
                {
                    AcademicYear = new AcademicYear();
                    AcademicYear.Year = lstYear[i];
                    AcademicYear.DisplayTitle = lstYear[i] + "-" + (lstYear[i] + 1);
                    lstAcademicYear.Add(AcademicYear);
                }
                // Lay nam hoc hien tai
                string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
                curYear = AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(_globalInfo.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);
                // Nam hoc
                ViewData[SupervisingDeptMarkInputSituationConstants.LS_YEAR] = new SelectList(lstAcademicYear, "Year", "DisplayTitle", curYear);
            }
            ViewData[SupervisingDeptMarkInputSituationConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");

            // cap hoc
            var lstAppliedLevel = CommonList.AppliedLevelName().Where(p => p.key != SystemParamsInFile.APPLIED_LEVEL_CRECHE.ToString()).ToList();
            //Tinh thanh
            List<Province> lstProvince = new List<Province>();
            if (_globalInfo.IsSystemAdmin)
            {
                lstProvince = ProvinceBusiness.All.Where(p => p.IsActive == true).OrderBy(p => p.ProvinceName).ToList();
            }
            else
            {
                var objProvince = ProvinceBusiness.All.Where(p => p.ProvinceID == _globalInfo.ProvinceID).FirstOrDefault();
                lstProvince.Add(objProvince);
            }
            ViewData[SupervisingDeptMarkInputSituationConstants.LIST_PROVINCE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");

            if (_globalInfo.IsSystemAdmin)
            {
                ViewData[SupervisingDeptMarkInputSituationConstants.DISABLE_PROVINCE] = false;
                ViewData[SupervisingDeptMarkInputSituationConstants.DISABLE_DISTRICT] = false;
            }
            else
            {

                ViewData[SupervisingDeptMarkInputSituationConstants.DISABLE_PROVINCE] = true;
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    ViewData[SupervisingDeptMarkInputSituationConstants.DISABLE_DISTRICT] = false;
                }
                else
                {
                    ViewData[SupervisingDeptMarkInputSituationConstants.DISABLE_DISTRICT] = true;
                }

            }
            // quyen huyen
            List<District> lstDictrict = new List<District>();
            if (!_globalInfo.IsSystemAdmin)
            {
                IQueryable<District> iqtDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == _globalInfo.ProvinceID && o.IsActive == true);
                if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    iqtDistrict = iqtDistrict.Where(o => o.DistrictID == _globalInfo.DistrictID);
                    lstAppliedLevel.RemoveAt(2);
                }
                lstDictrict = iqtDistrict.OrderBy(p => p.DistrictName).ToList();
            }
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                ViewData[SupervisingDeptMarkInputSituationConstants.LS_DISTRICT] = new SelectList(lstDictrict, "DistrictID", "DistrictName", _globalInfo.DistrictID);
            }
            else
            {
                ViewData[SupervisingDeptMarkInputSituationConstants.LS_DISTRICT] = new SelectList(lstDictrict, "DistrictID", "DistrictName");
            }
            ViewData[SupervisingDeptMarkInputSituationConstants.LS_EDU] = new SelectList(lstAppliedLevel, "key", "value", GlobalConstants.APPLIED_LEVEL_PRIMARY);

            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();

            MarkStatisticsearchInfo["Year"] = curYear;
            MarkStatisticsearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            MarkStatisticsearchInfo["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;
            MarkStatisticsearchInfo["AppliedLevel"] = GlobalConstants.APPLIED_LEVEL_PRIMARY;
            List<SubjectCatBO> ListSubject = SupervisingDeptBusiness.SearchSubject(MarkStatisticsearchInfo);
            SubjectCatBO objSCBO = new SubjectCatBO();
            List<string> lstStrSubjectName = ListSubject.Select(p => p.DisplayName).ToList();

            for (int i = 0; i < ListSubject.Count; i++)
            {
                objSCBO = ListSubject[i];
                if (lstStrSubjectName.Count(p => p == objSCBO.DisplayName) > 1)
                {
                    objSCBO.DisplayName = objSCBO.DisplayName + "(" + (objSCBO.IsCommenting == 0 ? "Môn tính điểm" : "Môn nhận xét") + ")";
                }
            }
            objSCBO = new SubjectCatBO();
            objSCBO.SubjectCatID = -1;
            objSCBO.DisplayName = "Năng lực";
            ListSubject.Add(objSCBO);
            objSCBO = new SubjectCatBO();
            objSCBO.SubjectCatID = -2;
            objSCBO.DisplayName = "Phẩm chất";
            ListSubject.Add(objSCBO);
            ViewData[SupervisingDeptMarkInputSituationConstants.CBO_SUBJECT] = new SelectList(ListSubject, "SubjectCatID", "DisplayName");
        }
        public FileResult ExportReport(int year, int semester, int appliedLevel, int districtID, int schoolID, int subjectID,string yearName)
        {
            Stream excel = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ProvinceID",_globalInfo.ProvinceID},
                {"SchoolID",schoolID},
                {"Year",year},
                {"AppliedLevelID",appliedLevel},
                {"SemesterID",semester},
                {"SubjectID",subjectID},
                {"UnitID",_globalInfo.SupervisingDeptID},
                {"SupervisingDeptName",_globalInfo.SuperVisingDeptName},
                {"YearName",yearName}
            };
            if (!_globalInfo.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                dic["SupervisingDeptID"] = 1;
                dic["DistrictID"] = _globalInfo.DistrictID;
            }
            else
            {
                //so
                dic["SupervisingDeptID"] = 0;
                dic["DistrictID"] = districtID;
            }
            //lay danh sach mon hoc can chay
            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();
            MarkStatisticsearchInfo["Year"] = year;
            MarkStatisticsearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            MarkStatisticsearchInfo["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;
            MarkStatisticsearchInfo["AppliedLevel"] = appliedLevel;
            if (subjectID != 0)
            {
                MarkStatisticsearchInfo["SubjectID"] = subjectID;
            }
            List<SubjectCatBO> lstSubjectCatBO = SupervisingDeptBusiness.SearchSubject(MarkStatisticsearchInfo);
            SubjectCatBO objSCBO = new SubjectCatBO();
            List<string> lstStrSubjectName = lstSubjectCatBO.Select(p => p.DisplayName).ToList();

            for (int i = 0; i < lstSubjectCatBO.Count; i++)
            {
                objSCBO = lstSubjectCatBO[i];
                if (lstStrSubjectName.Count(p => p == objSCBO.DisplayName) > 1)
                {
                    objSCBO.DisplayName = objSCBO.DisplayName + "(" + (objSCBO.IsCommenting == 0 ? "Môn tính điểm" : "Môn nhận xét") + ")";
                }
            }
            SubjectCatBO objSC = null;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                if (subjectID == -1)
                {
                    objSC = new SubjectCatBO();
                    objSC.SubjectCatID = -1;
                    objSC.DisplayName = "Năng lực";
                    lstSubjectCatBO.Add(objSC);//add năng lực    
                }
                else if (subjectID == -2)
                {
                    objSC = new SubjectCatBO();
                    objSC.SubjectCatID = -2;
                    objSC.DisplayName = "Phẩm chất";
                    lstSubjectCatBO.Add(objSC);
                }
                else if (subjectID == 0)
                {
                    objSC = new SubjectCatBO();
                    objSC.SubjectCatID = -1;
                    objSC.DisplayName = "Năng lực";
                    lstSubjectCatBO.Add(objSC);//add năng lực
                    objSC = new SubjectCatBO();
                    objSC.SubjectCatID = -2;
                    objSC.DisplayName = "Phẩm chất";
                    lstSubjectCatBO.Add(objSC);

                }
            }
            excel = MarkStatisticBusiness.ExportSupervisingDeptMarkInputSituation(dic, lstSubjectCatBO);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string FileDownloadName = string.Empty;
            string appliedName = appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY ? "TH" : appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY ? "THCS" : "THPT";
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                FileDownloadName = string.Format("SGD_{0}_TinhHinhNhapDL_{1}.xls", appliedName, semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII");
            }
            else
            {
                FileDownloadName = string.Format("SGD_{0}_TinhHinhNhapDL_{1}.xls", appliedName, semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII");
            }
            result.FileDownloadName = FileDownloadName;
            return result;
        }
        public JsonResult CreateNewData(int year, int semester, int appliedLevel, int districtID, int schoolID, int subjectID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ProvinceID",_globalInfo.ProvinceID},
                {"SchoolID",schoolID},
                {"Year",year},
                {"AppliedLevelID",appliedLevel},
                {"SemesterID",semester},
                {"SubjectID",subjectID},
                {"UnitID",_globalInfo.SupervisingDeptID}
            };
            if (!_globalInfo.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                dic["SupervisingDeptID"] = 1;
                dic["DistrictID"] = _globalInfo.DistrictID;
            }
            else
            {
                //so
                dic["SupervisingDeptID"] = 0;
                dic["DistrictID"] = districtID;
            }
            //lay danh sach mon hoc can chay
            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();
            MarkStatisticsearchInfo["Year"] = year;
            MarkStatisticsearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            MarkStatisticsearchInfo["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;
            MarkStatisticsearchInfo["AppliedLevel"] = appliedLevel;
            if (subjectID != 0)
            {
                MarkStatisticsearchInfo["SubjectID"] = subjectID;
            }
            List<SubjectCatBO> lstSubjectCatBO = SupervisingDeptBusiness.SearchSubject(MarkStatisticsearchInfo);
            SubjectCatBO objSCBO = new SubjectCatBO();
            List<string> lstStrSubjectName = lstSubjectCatBO.Select(p => p.DisplayName).ToList();

            for (int i = 0; i < lstSubjectCatBO.Count; i++)
            {
                objSCBO = lstSubjectCatBO[i];
                if (lstStrSubjectName.Count(p => p == objSCBO.DisplayName) > 1)
                {
                    objSCBO.DisplayName = objSCBO.DisplayName + "(" + (objSCBO.IsCommenting == 0 ? "Môn tính điểm" : "Môn nhận xét") + ")";
                }
            }
            SubjectCatBO objSC = null;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                if (subjectID == -1)
                {
                    objSC = new SubjectCatBO();
                    objSC.SubjectCatID = -1;
                    objSC.DisplayName = "Năng lực";
                    lstSubjectCatBO.Add(objSC);//add năng lực    
                }
                else if (subjectID == -2)
                {
                    objSC = new SubjectCatBO();
                    objSC.SubjectCatID = -2;
                    objSC.DisplayName = "Phẩm chất";
                    lstSubjectCatBO.Add(objSC);
                }
                else if (subjectID == 0)
                {
                    objSC = new SubjectCatBO();
                    objSC.SubjectCatID = -1;
                    objSC.DisplayName = "Năng lực";
                    lstSubjectCatBO.Add(objSC);//add năng lực
                    objSC = new SubjectCatBO();
                    objSC.SubjectCatID = -2;
                    objSC.DisplayName = "Phẩm chất";
                    lstSubjectCatBO.Add(objSC);

                }
            }
            MarkStatisticBusiness.InsertMarkStatisticBySupervisingDept(dic, lstSubjectCatBO);//day du lieu vao bang MarkStatistic
            return Json(new JsonMessage("Tổng hợp thành công", "success"));
        }
    }
}