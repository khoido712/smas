﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportTertiarySchoolStartYearArea
{
    public class ReportTertiarySchoolStartYearConstants
    {
        public const string LS_DISTRICT = "LS_DISTRICT";
        public const string LS_YEAR = "LS_YEAR";
        public const string LS_STATUS = "LS_STATUS";
        public const string LIST = "LS_STATUS";
    }
}