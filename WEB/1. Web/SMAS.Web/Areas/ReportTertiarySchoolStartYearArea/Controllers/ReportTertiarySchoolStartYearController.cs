﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportTertiarySchoolStartYearArea.Models;

namespace SMAS.Web.Areas.ReportTertiarySchoolStartYearArea.Controllers
{
    public class ReportTertiarySchoolStartYearController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        public ReportTertiarySchoolStartYearController(
            ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IReportDefinitionBusiness ReportDefinitionBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness
            , IDistrictBusiness DistrictBusiness
            , IProvinceBusiness ProvinceBusiness
            , IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IEmployeeBusiness EmployeeBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , IClassAssigmentBusiness ClassAssigmentBusiness
            , ITeachingAssignmentBusiness TeachingAssignmentBusiness
            )
        {
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportMarkInputSituationBusiness = ReportMarkInputSituationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ClassAssigmentBusiness = ClassAssigmentBusiness;
        }

        public ActionResult Index()        
        {
            SetViewData();
            return View();
        }
        private void SetViewData()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            List<District> lstDistrict = new List<District>();
            List<ComboObject> lstAcademicYear = new List<ComboObject>();
            IQueryable<District> lsdistrict = DistrictBusiness.All.Where(o => o.ProvinceID == GlobalInfo.ProvinceID && o.IsActive == true);
            if (GlobalInfo.IsSubSuperVisingDeptRole)
            {
                lsdistrict = lsdistrict.Where(o => o.DistrictID == GlobalInfo.DistrictID);
            }
            if (lsdistrict.Count() > 0)
            {
                lstDistrict = lsdistrict.ToList();
            }
            //Quan huyen
            ViewData[ReportTertiarySchoolStartYearConstants.LS_DISTRICT] = lstDistrict;


            IQueryable<AcademicYearOfProvince> lstAca = AcademicYearOfProvinceBusiness.All.Where(o => o.ProvinceID == GlobalInfo.ProvinceID);
            //Neu so da khai bao nam hoc
            if (lstAca.Count() > 0)
            {
                List<string> lstAcastr = lstAca.Select(o => o.DisplayTitle).Distinct().ToList();
                int i = 1;
                foreach (string aop in lstAcastr)
                {
                    ComboObject co = new ComboObject();
                    co.key = i.ToString();
                    co.value = aop;
                    lstAcademicYear.Add(co);
                    i++;
                }

            }
            else
            {
                //Neu so chua khai bao nam hoc
                IDictionary<string, object> dicSchool = new Dictionary<string, object>();
                dicSchool["ProvinceID"] = GlobalInfo.ProvinceID;
                dicSchool["SupervisingDeptID"] = GlobalInfo.SupervisingDeptID;
                //Neu phong dang nhap
                if (GlobalInfo.IsSubSuperVisingDeptRole)
                {
                    dicSchool["DistrictID"] = GlobalInfo.DistrictID;
                }
                List<SchoolProfile> lstSchool = SchoolProfileBusiness.Search(dicSchool).ToList();
                //danh sach nam hoc
                List<string> lstYear = new List<string>();
                foreach (SchoolProfile sp in lstSchool)
                {
                    lstYear = lstYear.Union(sp.AcademicYears.Select(o => o.DisplayTitle).ToList()).ToList();
                }
                //fill gia tri vao combobox
                lstYear = lstYear.Distinct().ToList();
                int i = 1;
                foreach (string aca in lstYear)
                {
                    ComboObject co = new ComboObject();
                    co.key = i.ToString();
                    co.value = aca;
                    lstAcademicYear.Add(co);
                    i++;
                }
            }
            //Nam hoc
            ViewData[ReportTertiarySchoolStartYearConstants.LS_YEAR] = lstAcademicYear.OrderBy(o => o.value).ToList();


            //Trang thai
            List<ComboObject> lst = new List<ComboObject>();
            lst.Add(new ComboObject("1", Res.Get("Report_Not_Send")));
            lst.Add(new ComboObject("2", Res.Get("Report_Send")));
            ViewData[ReportTertiarySchoolStartYearConstants.LS_STATUS] = lst;
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            GlobalInfo Global = new GlobalInfo();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["DistrictID"] = frm.DistrictID;
            dic["SchoolName"] = frm.SchoolName;
            dic["SupervisingDeptID"] = Global.SupervisingDeptID;
            dic["ProvinceID"] = Global.ProvinceID;
            //Danh sach truong
            List<SchoolProfile> lstSchool = AcademicYearBusiness.All.Where(v => v.DisplayTitle == frm.DisplayTitle && v.IsActive == true).Select(o=>o.SchoolProfile).ToList();
            //Lay ra nhung truong da bao cao hoac chua bao cao
            //danh sach truong bao cao len phong so

            List<ReportModel> lstReport = new List<ReportModel>();
            ReportModel rm = new ReportModel();
            foreach (SchoolProfile sp in lstSchool)
            {
                rm = new ReportModel();
                rm.SchoolName = sp.SchoolName;
                rm.DistrictName = sp.District.DistrictName;
                rm.SchoolType = sp.SchoolType.Resolution;
                
            }
            return PartialView("_List");
        }
    }
}
