﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportTertiarySchoolStartYearArea.Models
{
    public class SearchViewModel
    {
        public string SchoolName { get; set; }
        public int DistrictID { get; set; }
        public string DisplayTitle { get; set; }
        public string Status { get; set; }

    }
}