﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportTertiarySchoolStartYearArea.Models
{
    public class ReportModel
    {
        [ResourceDisplayName("ReportTertiarySchoolStartYear_Label_SchoolName")]
        public string SchoolName { get; set; }
        [ResourceDisplayName("ReportTertiarySchoolStartYear_Label_SchoolType")]
        public string SchoolType { get; set; }
        [ResourceDisplayName("ReportInputMonitoringSituation_Label_District")]
        public string DistrictName { get; set; }
        [ResourceDisplayName("ReportTertiarySchoolStartYear_Label_Status")]
        public string Status { get; set; }
        [ResourceDisplayName("ReportTertiarySchoolStartYear_Label_ReportDate")]
        public DateTime ReportDate { get; set; }
        [ResourceDisplayName("ReportTertiarySchoolStartYear_Label_ReportDetail")]
        public DateTime ReportDetail { get; set; }
    }
}