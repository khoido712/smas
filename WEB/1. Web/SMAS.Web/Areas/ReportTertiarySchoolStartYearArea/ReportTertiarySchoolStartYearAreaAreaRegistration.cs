﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportTertiarySchoolStartYearArea
{
    public class ReportTertiarySchoolStartYearAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportTertiarySchoolStartYearArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportTertiarySchoolStartYearArea_default",
                "ReportTertiarySchoolStartYearArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
