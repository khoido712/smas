﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SituationOfViolationArea
{
    public class SituationOfViolationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SituationOfViolationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SituationOfViolationArea_default",
                "SituationOfViolationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
