﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;
using SMAS.Web.Controllers;


namespace SMAS.Web.Areas.SituationOfViolationArea.Controllers
{
    public class SituationOfViolationController: BaseController
    {
        private readonly IReportSituationOfViolationBusiness ReportSituationOfViolationBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public SituationOfViolationController(IReportSituationOfViolationBusiness situationOfViolationBusiness,
                       IAcademicYearBusiness academicYearID,
                       IProcessedReportBusiness processedReportBusiness)
        {
            this.ReportSituationOfViolationBusiness = situationOfViolationBusiness;
            this.AcademicYearBusiness = academicYearID;
             this.ProcessedReportBusiness = processedReportBusiness;          
        }
        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();
            var lstEducation = Global.EducationLevels;
            ViewData[SituationOfViolationConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetViolationOfPupil(ReportSituationOfViolationBO reportSituationOfViolationBO)
        {
            GlobalInfo global = new GlobalInfo();
            reportSituationOfViolationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            reportSituationOfViolationBO.SchoolID = global.SchoolID.Value;
            reportSituationOfViolationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = ReportSituationOfViolationBusiness.GetReportDefinitionOfPupilFault(reportSituationOfViolationBO);
            
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                processedReport = ReportSituationOfViolationBusiness.ExcelGetPupilFault(reportSituationOfViolationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFault(reportSituationOfViolationBO);
                processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFault(reportSituationOfViolationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewViolationOfPupil(ReportSituationOfViolationBO reportSituationOfViolationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            reportSituationOfViolationBO.AcademicYearID = glo.AcademicYearID.Value;
            reportSituationOfViolationBO.SchoolID = glo.SchoolID.Value;
            reportSituationOfViolationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFault(reportSituationOfViolationBO);
            ProcessedReport processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFault(reportSituationOfViolationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetViolationOfClass(ReportSituationOfViolationBO reportSituationOfViolationBO)
        {
            GlobalInfo global = new GlobalInfo();
            reportSituationOfViolationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            reportSituationOfViolationBO.SchoolID = global.SchoolID.Value;
            reportSituationOfViolationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = ReportSituationOfViolationBusiness.GetReportDefinitionOfPupilFaultByClass(reportSituationOfViolationBO);

            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                processedReport = ReportSituationOfViolationBusiness.ExcelGetPupilFaultByClass(reportSituationOfViolationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFaultByClass(reportSituationOfViolationBO);
                processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFaultByClass(reportSituationOfViolationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewViolationOfClass(ReportSituationOfViolationBO reportSituationOfViolationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            reportSituationOfViolationBO.AcademicYearID = glo.AcademicYearID.Value;
            reportSituationOfViolationBO.SchoolID = glo.SchoolID.Value;
            reportSituationOfViolationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFaultByClass(reportSituationOfViolationBO);
            ProcessedReport processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFaultByClass(reportSituationOfViolationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_BAO_CAO_HS_VI_PHAM,
                SystemParamsInFile.REPORT_THONG_KE_VI_PHAM
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}
