﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ConfigDataLockArea
{
    public class ConfigDataLockConstant
    {
        public const int EVALUATION_4 = 4;
        public const int EVALUATION_5 = 5;
        public const int EVALUATION_6 = 6;
        public const int EVALUATION_7 = 7;
        public const string LIST_EDUCATION_LEVEL = "LIST_EDUCATION_LEVEL";
        public const string LIST_CLASSS = "LIST_CLASSS";
        public const string LIST_DATA_LOCK = "LIST_DATA_LOCK";
        public const string KTDK_GK1 = "DKGK1";
        public const string KTDK_CK1 = "DKCK1";
        public const string KTDK_GK2 = "DKGK2";
        public const string KTDK_CK2 = "DKCK2";
        public const string HK1 = "HK1";
        public const string HK2 = "HK2";
        public const string NXHK1 = "NXHK1";
        public const string NXHK2 = "NXHK2";
        public const string DGHK1 = "DGHK1";
        public const string DGHK2 = "DGHK2";
        public const string RLTH = "RLTH";
        public const string KTHK1 = "KTHK1";
        public const string KTHK2 = "KTHK2";
        public const string DGBS = "DGBS";
        public const string LIST_DATA_LOCK_ALL = "LIST_DATA_LOCK_ALL";
        public const string LIST_MONTH_HK1 = "LIST_MONTH_HK1";
        public const string LIST_MONTH_HK2 = "LIST_MONTH_HK2";
        public const string DATA_LOCK_COMMENT = "DATA_LOCK_COMMENT";
        public const string DATA_LOCK_EVALUATION = "DATA_LOCK_EVALUATION";
        public const string DATA_LOCK_EVAL_GENERAL = "DATA_LOCK_EVAL_GENERAL";
    }
}