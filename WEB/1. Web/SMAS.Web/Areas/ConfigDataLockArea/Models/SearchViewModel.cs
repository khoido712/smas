﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ConfigDataLockArea.Models
{
    public class SearchViewModel
    {
        [DisplayName("Khối: ")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConfigDataLockConstant.LIST_EDUCATION_LEVEL)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onChangeEducation()")]
        public int? cboEducationLevel { get; set; }

        [DisplayName("Lớp: ")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConfigDataLockConstant.LIST_CLASSS)]
        [AdditionalMetadata("OnChange", "onChangeClass()")]
        [AdditionalMetadata("PlaceHolder", "null")]      
        public int? cboClass { get; set; }
    }
}