﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ConfigDataLockArea.Models
{
    public class DataCheckAll
    {
        public int SubjectID { get; set; }
        public bool CheckAll { get; set; }
        public string Month { get; set; }
        public int Semeter { get; set; }
    }
}