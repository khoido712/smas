﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ConfigDataLockArea.Models
{
    public class MonthViewModel
    {
        public List<MonthInYear> ListMonthHK1 { get; set; }
        public List<MonthInYear> ListMonthHK2 { get; set; }
    }

    public class MonthInYear
    {
        public string MonthName { get; set; }
        public int Semeter { get; set; }
    }
}