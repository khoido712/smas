﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ConfigDataLockArea.Models
{
    public class DataLockViewModel
    {
        private int _value_1 = 0;
        private int _value_2 = 0;
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public string MonthByHK { get; set; }
        public List<MonthLocked> ListMonthLocked { get; set; }
        public int ValueCheckCHK1 { get { return _value_1; } set { _value_1 = value; } }
        public int ValueCheckCHK2 { get { return _value_2; } set { _value_2 = value; } }
        public bool ValueCheckDKGK1 { get; set; }
        public bool ValueCheckDKCK1 { get; set; }
        public bool ValueCheckDKGK2 { get; set; }
        public bool ValueCheckDKCK2 { get; set; }
    }

    public class MonthLocked
    {
        private int _value = 0;
        public string MonthCheck { get; set; }
        public int ValueCheck { get { return _value; } set { _value = value; } }
        public int SubjectID { get; set; }
        public int Semeter { get; set; }
    }

    public class DataLockComment
    {
        private int _value_1 = 0;
        private int _value_2 = 0;
        public int ValueCheckHK1 { get { return _value_1; } set { _value_1 = value; } }
        public int ValueCheckHK2 { get { return _value_2; } set { _value_2 = value; } }
    }

    public class DataLockEvaluation
    {
        private int _value_1 = 0;
        private int _value_2 = 0;
        private int _value_3 = 0;
        public int ValueCheckHK1 { get { return _value_1; } set { _value_1 = value; } }
        public int ValueCheckHK2 { get { return _value_2; } set { _value_2 = value; } }
        public int ValueCheckInSummer { get { return _value_3; } set { _value_3 = value; } }
    }

    public class DataLockEvalGeneral
    {
        private int _value_1 = 0;
        private int _value_2 = 0;
        private int _value_3 = 0;
        public int ValueCheckBonusHK1 { get { return _value_1; } set { _value_1 = value; } }
        public int ValueCheckBonusHK2 { get { return _value_2; } set { _value_2 = value; } }
        public int ValueCheckAdditional { get { return _value_3; } set { _value_3 = value; } }
    }
}