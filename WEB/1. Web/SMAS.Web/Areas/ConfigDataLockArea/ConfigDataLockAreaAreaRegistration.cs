﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ConfigDataLockArea
{
    public class ConfigDataLockAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ConfigDataLockArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ConfigDataLockArea_default",
                "ConfigDataLockArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
