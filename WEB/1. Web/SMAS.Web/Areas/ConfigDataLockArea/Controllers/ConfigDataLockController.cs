﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.ConfigDataLockArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ConfigDataLockArea.Controllers
{
    public class ConfigDataLockController : BaseController
    {
        //
        // GET: /ConfigDataLockArea/ConfigDataLock/

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public ConfigDataLockController(IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.LockRatedCommentPupilBusiness = LockRatedCommentPupilBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public void SetViewData()
        {
            int? SchoolID = _globalInfo.SchoolID;
            int? AcademicYearID = _globalInfo.AcademicYearID;

            int educationId = 0;
            IEnumerable<EducationLevel> lsEducationLevel = new GlobalInfo().EducationLevels.ToList();
            ViewData[ConfigDataLockConstant.LIST_EDUCATION_LEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");

            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (lsEducationLevel.FirstOrDefault() != null)
            {
                educationId = lsEducationLevel.FirstOrDefault().EducationLevelID;

                dic["EducationLevelID"] = educationId;

                if (SchoolID.HasValue)
                {
                    dic["SchoolID"] = SchoolID.Value;
                }

                if (AcademicYearID.HasValue)
                {
                    dic["AcademicYearID"] = AcademicYearID.Value;
                }
                lstClass = this.ClassProfileBusiness.Search(dic).Where(x=>x.IsVnenClass.HasValue && x.IsVnenClass.Value == true).ToList();
            }

            ViewData[ConfigDataLockConstant.LIST_CLASSS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
        }

        #region AjaxLoadGrid
        public PartialViewResult AjaxLoadGrid(int? educationID, int? ClassID)
        {
            if (!educationID.HasValue || !ClassID.HasValue)
            {
                if (!educationID.HasValue)
                    educationID = -1;
                if (!ClassID.HasValue)
                    ClassID = -1;
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic["EducationLevelID"] = educationID;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["ClassID"] = ClassID;
            List<int> lstEducationID = new List<int>();
            lstEducationID.Add(educationID.Value);
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.Search(dic)
                                                .Where(x => x.IsSubjectVNEN.HasValue 
                                                    && x.ClassID == ClassID 
                                                    && x.IsSubjectVNEN.Value == true).ToList();
            List<int> lstSubjectId = lstClassSubject.Select(x => x.SubjectID).Distinct().ToList();
            List<SubjectCatBO> lstSubject = ListSubjectByEducation(lstEducationID).Where(x => lstSubjectId.Contains(x.SubjectCatID)).Distinct().ToList();

            dic["lstEvaluationID"] = ListEvaluation();

            List<LockRatedCommentPupil> lstLockRated = new List<LockRatedCommentPupil>();
            lstLockRated = LockRatedCommentPupilBusiness.Search(dic).ToList();

            MonthViewModel obj = ObjectMonthViewModel();

            List<DataLockViewModel> lstResult = new List<DataLockViewModel>();
            DataLockViewModel objResult = null;
            LockRatedCommentPupil objLockRated = null;
            SubjectCatBO objSub = null;
            MonthInYear objMonthHK1 = null;
            MonthInYear objMonthHK2 = null;

            List<MonthLocked> lstMonthLocked = new List<MonthLocked>();
            MonthLocked objMonthLocked = null;

            // Duyệt môn học
            for (int i = 0; i < lstSubject.Count; i++)
            {
                objSub = lstSubject[i];
                objResult = new DataLockViewModel();
                objResult.ListMonthLocked = new List<MonthLocked>();
                objResult.SubjectID = lstSubject[i].SubjectCatID;
                objResult.SubjectName = lstSubject[i].SubjectName;

                // Duyệt HK 1
                for (int j = 0; j < obj.ListMonthHK1.Count; j++)
                {
                    objMonthHK1 = obj.ListMonthHK1[j];
                    objMonthLocked = new MonthLocked();
                    objMonthLocked.SubjectID = objSub.SubjectCatID;
                    objMonthLocked.Semeter = objMonthHK1.Semeter;
                    objMonthLocked.MonthCheck = objMonthHK1.MonthName;

                    objLockRated = lstLockRated.FirstOrDefault(x => x.SubjectID == objSub.SubjectCatID
                                                    && x.LockTitle.Contains((objMonthHK1.MonthName + ConfigDataLockConstant.HK1)));
                    if (objLockRated != null)
                    {
                        objMonthLocked.ValueCheck = 1;
                    }
                    lstMonthLocked.Add(objMonthLocked);
                }
                // Cuối HK 1
                objLockRated = lstLockRated.FirstOrDefault(x => x.SubjectID == objSub.SubjectCatID
                                                  && x.LockTitle.Contains(("C" + ConfigDataLockConstant.HK1)));
                if (objLockRated != null)
                {
                    objResult.ValueCheckCHK1 = 1;
                }

                // Duyệt HK 2
                for (int j = 0; j < obj.ListMonthHK2.Count; j++)
                {
                    objMonthHK2 = obj.ListMonthHK2[j];
                    objMonthLocked = new MonthLocked();
                    objMonthLocked.SubjectID = objSub.SubjectCatID;
                    objMonthLocked.Semeter = objMonthHK2.Semeter;
                    objMonthLocked.MonthCheck = objMonthHK2.MonthName;

                    objLockRated = lstLockRated.FirstOrDefault(x => x.SubjectID == objSub.SubjectCatID
                                                    && x.LockTitle.Contains((objMonthHK2.MonthName + ConfigDataLockConstant.HK2)));
                    if (objLockRated != null)
                    {
                        objMonthLocked.ValueCheck = 1;
                    }
                    lstMonthLocked.Add(objMonthLocked);
                }
                // Cuối HK 2
                objLockRated = lstLockRated.FirstOrDefault(x => x.SubjectID == objSub.SubjectCatID
                                                  && x.LockTitle.Contains(("C" + ConfigDataLockConstant.HK2)));
                if (objLockRated != null)
                {
                    objResult.ValueCheckCHK2 = 1;
                }

                //Kiểm tra định kỳ
                objLockRated = lstLockRated.FirstOrDefault(x => x.SubjectID == objSub.SubjectCatID
                                                 && x.LockTitle.Contains((ConfigDataLockConstant.KTDK_GK1)));
                if (objLockRated != null)
                {
                    objResult.ValueCheckDKGK1 = true;
                }

                objLockRated = lstLockRated.FirstOrDefault(x => x.SubjectID == objSub.SubjectCatID
                                                 && x.LockTitle.Contains((ConfigDataLockConstant.KTDK_CK1)));
                if (objLockRated != null)
                {
                    objResult.ValueCheckDKCK1 = true;
                }

                objLockRated = lstLockRated.FirstOrDefault(x => x.SubjectID == objSub.SubjectCatID
                                                 && x.LockTitle.Contains((ConfigDataLockConstant.KTDK_GK2)));
                if (objLockRated != null)
                {
                    objResult.ValueCheckDKGK2 = true;
                }

                objLockRated = lstLockRated.FirstOrDefault(x => x.SubjectID == objSub.SubjectCatID
                                                 && x.LockTitle.Contains((ConfigDataLockConstant.KTDK_CK2)));
                if (objLockRated != null)
                {
                    objResult.ValueCheckDKCK2 = true;
                }

                objResult.ListMonthLocked.AddRange(lstMonthLocked);
                lstResult.Add(objResult);
            }

            DataLockComment objDataLockComment = new DataLockComment();
            DataLockEvaluation objDataLockEvaluation = new DataLockEvaluation();
            DataLockEvalGeneral objDataLockEvalGeneral = new DataLockEvalGeneral();

            var valDataLockComment = lstLockRated.Where(x => x.EvaluationID == ConfigDataLockConstant.EVALUATION_5).FirstOrDefault();
            if (valDataLockComment != null)
            {
                if (valDataLockComment.LockTitle.Contains(ConfigDataLockConstant.NXHK1))
                {
                    objDataLockComment.ValueCheckHK1 = 1;
                }
                if (valDataLockComment.LockTitle.Contains(ConfigDataLockConstant.NXHK2))
                {
                    objDataLockComment.ValueCheckHK2 = 1;
                }
            }

            var valDataLockEvaluation = lstLockRated.Where(x => x.EvaluationID == ConfigDataLockConstant.EVALUATION_6).FirstOrDefault();
            if (valDataLockEvaluation != null)
            {
                if (valDataLockEvaluation.LockTitle.Contains(ConfigDataLockConstant.DGHK1))
                {
                    objDataLockEvaluation.ValueCheckHK1 = 1;
                }
                if (valDataLockEvaluation.LockTitle.Contains(ConfigDataLockConstant.DGHK2))
                {
                    objDataLockEvaluation.ValueCheckHK2 = 1;
                }
                if (valDataLockEvaluation.LockTitle.Contains(ConfigDataLockConstant.RLTH))
                {
                    objDataLockEvaluation.ValueCheckInSummer = 1;
                }
            }

            var valDataLockEvalGeneral = lstLockRated.Where(x => x.EvaluationID == ConfigDataLockConstant.EVALUATION_7).FirstOrDefault();
            if (valDataLockEvalGeneral != null)
            {
                if (valDataLockEvalGeneral.LockTitle.Contains(ConfigDataLockConstant.KTHK1))
                {
                    objDataLockEvalGeneral.ValueCheckBonusHK1 = 1;
                }
                if (valDataLockEvalGeneral.LockTitle.Contains(ConfigDataLockConstant.KTHK2))
                {
                    objDataLockEvalGeneral.ValueCheckBonusHK2 = 1;
                }
                if (valDataLockEvalGeneral.LockTitle.Contains(ConfigDataLockConstant.DGBS))
                {
                    objDataLockEvalGeneral.ValueCheckAdditional = 1;
                }
            }

            ViewData[ConfigDataLockConstant.LIST_MONTH_HK1] = obj.ListMonthHK1;
            ViewData[ConfigDataLockConstant.LIST_MONTH_HK2] = obj.ListMonthHK2;
            ViewData[ConfigDataLockConstant.LIST_DATA_LOCK] = lstResult;
            ViewData[ConfigDataLockConstant.DATA_LOCK_COMMENT] = objDataLockComment;
            ViewData[ConfigDataLockConstant.DATA_LOCK_EVALUATION] = objDataLockEvaluation;
            ViewData[ConfigDataLockConstant.DATA_LOCK_EVAL_GENERAL] = objDataLockEvalGeneral;

            return PartialView("_GridDataLock");
        }
        #endregion

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int educationId)
        {
            if (educationId < 0)
                return Json(new List<SelectListItem>());

            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<ClassProfile> lstClass = new List<ClassProfile>();

            int? SchoolID = _globalInfo.SchoolID;
            int? AcademicYearID = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationId;
            dic["SchoolID"] = SchoolID.Value;
            dic["AcademicYearID"] = AcademicYearID.Value;
            lstClass = this.ClassProfileBusiness.Search(dic).Where(x => x.IsVnenClass.HasValue && x.IsVnenClass.Value == true).ToList();
            var lstClassSelected = lstClass.Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = true}).ToList();
            return Json(lstClassSelected);
        }

        #region Save
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult Save(FormCollection frm)
        {
            int appliedAllClass = !string.IsNullOrEmpty(frm["appliedClassCurrentOrAll"])
                                    ? int.Parse(frm["appliedClassCurrentOrAll"]) : 0;
            int educationID = !string.IsNullOrEmpty(frm["educationID"]) ? int.Parse(frm["educationID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["classID"]) ? int.Parse(frm["classID"]) : 0;          
            // appliedAllClass = 1: Áp dụng lớp hiện tại
            // appliedAllClass = 2: Áp dụng toàn khối
            // appliedAllClass = 3: Áp dụn toàn trường
            if (educationID > 0 && appliedAllClass > 0 && (appliedAllClass == 1 || appliedAllClass == 2 || appliedAllClass == 3))
            {
                if (appliedAllClass == 1 && classID == 0)
                {
                    throw new BusinessException("Thầy cô vui lòng chọn lớp học");
                }

                var lstEducation = _globalInfo.EducationLevels.ToList();
                List<int> lstEducationID = new List<int>();
                lstEducationID = lstEducation.Select(x => x.EducationLevelID).ToList();

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedAllClass"] = appliedAllClass;
                dic["EducationID"] = educationID;
                dic["ClassID"] = classID;
                dic["lstEducationID"] = lstEducationID;
                dic["lstEvaluationID"] = ListEvaluation();
                                             
                MonthViewModel obj = ObjectMonthViewModel();

                LockRatedCommentPupilBO objLockRatedBO = new LockRatedCommentPupilBO();
                MonthInYearLockBO objMonthLock = null;
                MonthInYear objMonthHK = null;

                List<MonthInYearLockBO> lstMonthLockHK1 = new List<MonthInYearLockBO>();
                List<MonthInYearLockBO> lstMonthLockHK2 = new List<MonthInYearLockBO>();
                for (int k = 0; k < obj.ListMonthHK1.Count; k++)
                {
                    objMonthHK = obj.ListMonthHK1[k];

                    objMonthLock = new MonthInYearLockBO();
                    objMonthLock.MonthName = objMonthHK.MonthName;
                    objMonthLock.Semeter = objMonthHK.Semeter;
                    lstMonthLockHK1.Add(objMonthLock);
                   
                }
                objLockRatedBO.ListMonthHK1 = new List<MonthInYearLockBO>();
                objLockRatedBO.ListMonthHK1 = lstMonthLockHK1;

                for (int k = 0; k < obj.ListMonthHK2.Count; k++)
                {
                    objMonthHK = obj.ListMonthHK2[k];
                    objMonthLock = new MonthInYearLockBO();
                    objMonthLock.MonthName = objMonthHK.MonthName;
                    objMonthLock.Semeter = objMonthHK.Semeter;
                    lstMonthLockHK2.Add(objMonthLock);
                }
                objLockRatedBO.ListMonthHK2 = new List<MonthInYearLockBO>();
                objLockRatedBO.ListMonthHK2 = lstMonthLockHK2;
             
                this.LockRatedCommentPupilBusiness.SaveDataLockVNEN(frm, dic, objLockRatedBO);
                
                if (appliedAllClass == 1)
                {
                    return Json(new JsonMessage(Res.Get("success_Save"), "success"));
                }
                if (appliedAllClass == 2)
                {
                    return Json(new JsonMessage(Res.Get("Applied_All_EducationLevel_Success"), "success"));                   
                }
                return Json(new JsonMessage(Res.Get("Applied_AppliedLevel_Success"), "success"));
            }

            return Json(new JsonMessage(Res.Get("Save_Failed"), "error"));
        }
        #endregion

        private List<int> ListEvaluation()
        {
            List<int> lstEvalutionID = new List<int>();
            lstEvalutionID.Add(4);
            lstEvalutionID.Add(5);
            lstEvalutionID.Add(6);
            lstEvalutionID.Add(7);
            return lstEvalutionID;
        }

        private LockRatedCommentPupil CreateObject(int classID, int schoolId, int academicYear, int subjectId, int lastDigit, int evaluation)
        {
            LockRatedCommentPupil obj = new LockRatedCommentPupil();
            obj.ClassID = classID;
            obj.SchoolID = schoolId;
            obj.AcademicYearID = academicYear;
            obj.SubjectID = subjectId;
            obj.LastDigitSchoolID = lastDigit;
            obj.EvaluationID = evaluation;
            obj.CreateTime = DateTime.Now;
            return obj;
        }

        private List<SubjectCatBO> ListSubjectByEducation(List<int> lstEducationID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            IQueryable<SchoolSubject> lstSchoolSubject = SchoolSubjectBusiness.Search(dic).Where(x => x.EducationLevelID.HasValue
                                                                && lstEducationID.Contains(x.EducationLevelID.Value));

            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            IQueryable<SubjectCat> lstSubjectCat = SubjectCatBusiness.Search(dic).Where(x => x.IsActive);         

            List<SubjectCatBO> lstSubject = new List<SubjectCatBO>();

            lstSubject = (from sc in lstSchoolSubject
                          join sub in lstSubjectCat on sc.SubjectID equals sub.SubjectCatID
                          select new SubjectCatBO()
                          {
                              SubjectCatID = sub.SubjectCatID,
                              SubjectName = sub.SubjectName,
                              OrderInSubject = sub.OrderInSubject
                          }).Distinct().ToList();
            lstSubject = lstSubject.OrderBy(x => x.OrderInSubject).ToList();
            return lstSubject;
        }

        private MonthViewModel ObjectMonthViewModel()
        {
            MonthViewModel lstMonthVM = new MonthViewModel();
            List<MonthInYear> lstMonthHK1 = new List<MonthInYear>();
            List<MonthInYear> lstMonthHK2 = new List<MonthInYear>();

            AcademicYear objYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            lstMonthHK1 = GetListMonth(1, objYear);
            lstMonthHK2 = GetListMonth(2, objYear);

            lstMonthVM.ListMonthHK1 = new List<MonthInYear>();
            lstMonthVM.ListMonthHK1.AddRange(lstMonthHK1);

            lstMonthVM.ListMonthHK2 = new List<MonthInYear>();
            lstMonthVM.ListMonthHK2.AddRange(lstMonthHK2);

            return lstMonthVM;
        }

        private List<MonthInYear> GetListMonth(int semester, AcademicYear objAcademicYear)
        {
            DateTime FDate = new DateTime();
            DateTime EDate = new DateTime();

            if (semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                FDate = objAcademicYear.FirstSemesterStartDate.Value;
                EDate = objAcademicYear.FirstSemesterEndDate.Value;
            }
            else if (semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                FDate = objAcademicYear.SecondSemesterStartDate.Value;
                EDate = objAcademicYear.SecondSemesterEndDate.Value;
            }
            FDate = new DateTime(FDate.Date.Year, FDate.Date.Month, 1);
            EDate = new DateTime(EDate.Date.Year, EDate.Date.Month, 1);

            List<MonthInYear> lstListMonth = new List<MonthInYear>();
            MonthInYear objMonth = null;
            while (FDate <= EDate)
            {
                objMonth = new MonthInYear();
                objMonth.MonthName = "T" + FDate.Date.Month;
                objMonth.Semeter = semester;
                lstListMonth.Add(objMonth);
                FDate = FDate.AddMonths(1);
            }
            return lstListMonth;
        }
    }
}
