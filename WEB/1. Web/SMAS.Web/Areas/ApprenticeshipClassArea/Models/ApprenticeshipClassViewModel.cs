/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Areas.ApprenticeshipClassArea;
namespace SMAS.Web.Areas.ApprenticeshipClassArea.Models
{
    public class ApprenticeshipClassViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 ApprenticeshipClassID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("ApprenticeshipClass_Label_NameClass")]			
		public System.String ClassName { get; set; }

        [UIHint("Combobox")]
        [ResourceDisplayName("ApprenticeshipClass_Label_AcadYear")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipClassConstants.LIST_YEAR)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public System.Int32? AcademicYearID { get; set; }

        
        [UIHint("Combobox")]
        [ResourceDisplayName("ApprenticeshipClass_Label_SchoolFaculty")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipClassConstants.LIST_FACULTYNAME)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacher(this)")]
        public int? FacultyName {get;set;}


        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("ApprenticeshipClass_Label_TeacherName")]		
        [AdditionalMetadata("ViewDataKey", ApprenticeshipClassConstants.LIST_Teacher)]
		public System.Int32? HeadTeacherID { get; set; }

        [ScaffoldColumn(false)]
        	
        public String HeadTeacherFullName { get; set; }

        //[UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[ResDisplayName("Năm học")]
        //[AdditionalMetadata("PlaceHolder", "null")]
        //[AdditionalMetadata("ViewDataKey", ApprenticeshipClassConstants.LIST_YEAR)]
        [ScaffoldColumn(false)]
        public String Year { get; set; }



        [ScaffoldColumn(false)]
        public String SubjectName { get; set; }

        [ResourceDisplayName("ApprenticeshipClass_Label_SubjectName")]			
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipClassConstants.LIST_SUBJECT)]
		public System.Int32? ApprenticeshipSubjectID { get; set; }

        


        [ScaffoldColumn(false)]
		public System.Int32? SchoolID { get; set; }

        [ResourceDisplayName("ApprenticeshipClass_Label_TimeLearn")]			
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipClassConstants.LIST_SEMESTER)]
        [AdditionalMetadata("PlaceHolder", "null")]
		public System.Nullable<System.Int32> Semester { get; set; }

        [ResourceDisplayName("ApprenticeshipClass_Label_TypeSubject")]
		[UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipClassConstants.LIST_SUBJECTTYPE)]
        [AdditionalMetadata("PlaceHolder", "null")]
		public System.Nullable<System.Int32> SubjectType { get; set; }

        [ResourceDisplayName("ApprenticeshipClass_Label_TSubject")]
		[UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipClassConstants.LIST_ISCOMMENTING)]	

		public System.Nullable<System.Int32> IsCommenting { get; set; }								
	       
    }
}


