﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ApprenticeshipClassArea
{
    public class ApprenticeshipClassAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ApprenticeshipClassArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ApprenticeshipClassArea_default",
                "ApprenticeshipClassArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
