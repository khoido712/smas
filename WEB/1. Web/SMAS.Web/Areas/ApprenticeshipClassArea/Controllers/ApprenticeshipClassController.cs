﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ApprenticeshipClassArea.Models;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ApprenticeshipClassArea.Controllers
{
    public class ApprenticeshipClassController : BaseController
    {
        private readonly IApprenticeshipClassBusiness ApprenticeshipClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;

        public ApprenticeshipClassController(IApprenticeshipClassBusiness apprenticeshipclassBusiness, IEmployeeBusiness EmployeeBusiness, IAcademicYearBusiness AcademicYearBusiness, IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness, ISchoolFacultyBusiness SchoolFacultyBusiness, IClassProfileBusiness ClassProfileBusiness)
        {
            this.ApprenticeshipClassBusiness = apprenticeshipclassBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ApprenticeshipSubjectBusiness = ApprenticeshipSubjectBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }
        
        //
        // GET: /ApprenticeshipClass/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo glo = new GlobalInfo();
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            //Get view data here
            string year = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).Year.ToString() + " - " + (AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).Year + 1).ToString();
            List<SelectListItem> cmb = new List<SelectListItem>();
            cmb.Add(new SelectListItem { Text = year, Value = new GlobalInfo().AcademicYearID.ToString(), Selected = true });

            // Lấy dữ liệu lớp học: lấy các lớp 11 đối với khối THPT và lớp 8 của khối THCS
            IDictionary<string, object> SearchClass = new Dictionary<string, object>();
            SearchClass["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchClass["AppliedLevel"] = glo.AppliedLevel;
            if (glo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                SearchClass["EducationLevelID"] = 8; // Khối 8
            }
            if (glo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                SearchClass["EducationLevelID"] = 11; // Khối 11
            }
            ViewData[ApprenticeshipClassConstants.LIST_CLASS] = new SelectList(ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, SearchClass).ToList(), "DisplayName", "DisplayName");

            //-------------------
            ViewData[ApprenticeshipClassConstants.LIST_YEAR] = new SelectList(cmb, "Value", "Text"); ;

            //-----------------------

            ViewData[ApprenticeshipClassConstants.LIST_FACULTYNAME] = new SelectList(SchoolFacultyBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, new Dictionary<string, object>()).ToList(), "SchoolFacultyID", "FacultyName");

            ViewData[ApprenticeshipClassConstants.LIST_Teacher] = new SelectList(new String[] { });


            SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID.Value;
            SearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            SearchInfo["IsActive"] = true;
            ViewData[ApprenticeshipClassConstants.LIST_SUBJECT] = new SelectList(ApprenticeshipSubjectBusiness.Search(SearchInfo).ToList(), "ApprenticeshipSubjectID", "SubjectName");


            //
            cmb = new List<SelectListItem>();
            cmb.Add(new SelectListItem { Text = Res.Get("ApprenticeshipClass_Label_Semester1"), Value = "1", Selected = false });
            cmb.Add(new SelectListItem { Text = Res.Get("ApprenticeshipClass_Label_Semester2"), Value = "2", Selected = false });
            cmb.Add(new SelectListItem { Text = Res.Get("ApprenticeshipClass_Label_Semester3"), Value = "3", Selected = true });
            ViewData[ApprenticeshipClassConstants.LIST_SEMESTER] = new SelectList(cmb, "Value", "Text");


            //
            cmb = new List<SelectListItem>();
            cmb.Add(new SelectListItem { Text = Res.Get("ApprenticeshipClass_Label_SubjectMark"), Value = "0", Selected = true });
            cmb.Add(new SelectListItem { Text = Res.Get("ApprenticeshipClass_Label_SubjectComment"), Value = "1", Selected = false });
            ViewData[ApprenticeshipClassConstants.LIST_SUBJECTTYPE] = new SelectList(cmb, "Value", "Text");


            //
            cmb = new List<SelectListItem>();
            cmb.Add(new SelectListItem { Text = Res.Get("ApprenticeshipClass_Label_Type1"), Value = "0", Selected = true });
            cmb.Add(new SelectListItem { Text = Res.Get("ApprenticeshipClass_Label_Auto"), Value = "1", Selected = false });
            ViewData[ApprenticeshipClassConstants.LIST_ISCOMMENTING] = new SelectList(cmb, "Value", "Text");


            //KIEM TRA THOI GIAN 

            AcademicYear AcademicYear = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value);

            if (DateTime.Now.Date < AcademicYear.FirstSemesterStartDate || DateTime.Now.Date > AcademicYear.SecondSemesterEndDate)
            {
                ViewData[ApprenticeshipClassConstants.VISIBLE_BUTTON] = false;
            }
            else
            {
                ViewData[ApprenticeshipClassConstants.VISIBLE_BUTTON] = true;
            }

            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID.Value;
            IEnumerable<ApprenticeshipClassViewModel> lst = this._Search(SearchInfo);
            ViewData[ApprenticeshipClassConstants.LIST_APPRENTICESHIPCLASS] = lst;



            return View();
        }

        //
        // GET: /ApprenticeshipClass/Search

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetDataTeacher(int? id)
        {
            if (id.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc


                IQueryable<Employee> listTeacher = EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, id.Value);

                return Json(new SelectList(listTeacher, "EmployeeID", "FullName"));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }

        }
        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            //
            AcademicYear AcademicYear = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value);

            if (DateTime.Now.Date < AcademicYear.FirstSemesterStartDate || DateTime.Now.Date > AcademicYear.SecondSemesterEndDate)
            {
                ViewData[ApprenticeshipClassConstants.VISIBLE_BUTTON] = false;
            }
            else
            {
                ViewData[ApprenticeshipClassConstants.VISIBLE_BUTTON] = true;
            }

            IEnumerable<ApprenticeshipClassViewModel> lst = this._Search(SearchInfo);
            ViewData[ApprenticeshipClassConstants.LIST_APPRENTICESHIPCLASS] = lst;

            //Get view data here

            return PartialView("_List");
        }

        #region action
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            ApprenticeshipClass apprenticeshipclass = new ApprenticeshipClass();
            TryUpdateModel(apprenticeshipclass);

            apprenticeshipclass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            apprenticeshipclass.SchoolID = new GlobalInfo().SchoolID.Value;
            //apprenticeshipclass.Semester = new GlobalInfo().Semester.Value;

            Utils.Utils.TrimObject(apprenticeshipclass);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["SchoolID"] = new GlobalInfo().SchoolID.Value;
            dic["ClassName"] = apprenticeshipclass.ClassName;

            IQueryable<ApprenticeshipClass> listApp = ApprenticeshipClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).Where(u => u.ClassName == apprenticeshipclass.ClassName);
            if (listApp.Count() > 0 && listApp != null)
            {
                return Json(new JsonMessage(Res.Get("ApprenticeshipClass_Label_DuplicateClassName"), "ERROR"));
            }

            this.ApprenticeshipClassBusiness.Insert(apprenticeshipclass);
            this.ApprenticeshipClassBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// CreateByClass
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateByClass()
        {
            ApprenticeshipClass apprenticeshipclass = new ApprenticeshipClass();
            TryUpdateModel(apprenticeshipclass);

            apprenticeshipclass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            apprenticeshipclass.SchoolID = new GlobalInfo().SchoolID.Value;
            //apprenticeshipclass.Semester = new GlobalInfo().Semester.Value;

            Utils.Utils.TrimObject(apprenticeshipclass);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["SchoolID"] = new GlobalInfo().SchoolID.Value;


            IQueryable<ApprenticeshipClass> listApp = ApprenticeshipClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).Where(u => u.ClassName == apprenticeshipclass.ClassName);
            if (listApp.Count() > 0 && listApp != null)
            {
                return Json(new JsonMessage(Res.Get("ApprenticeshipClass_Label_DuplicateClassName"), "ERROR"));
            }


            this.ApprenticeshipClassBusiness.Insert(apprenticeshipclass);
            this.ApprenticeshipClassBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Edit(int ApprenticeshipClassID)
        {
            this.CheckPermissionForAction(ApprenticeshipClassID, "ApprenticeshipClass");
            ApprenticeshipClass apprenticeshipclass = this.ApprenticeshipClassBusiness.Find(ApprenticeshipClassID);
            TryUpdateModel(apprenticeshipclass);
            Utils.Utils.TrimObject(apprenticeshipclass);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["SchoolID"] = new GlobalInfo().SchoolID.Value;
            dic["ClassName"] = apprenticeshipclass.ClassName;

            IQueryable<ApprenticeshipClass> listApp = ApprenticeshipClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).Where(u => u.ApprenticeshipClassID != ApprenticeshipClassID).Where(u => u.ClassName == apprenticeshipclass.ClassName);
            if (listApp.Count() > 0 && listApp != null)
            {

                return Json(new JsonMessage(Res.Get("ApprenticeshipClass_Label_DuplicateClassName")));
            }

            this.ApprenticeshipClassBusiness.Update(apprenticeshipclass);
            this.ApprenticeshipClassBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "ApprenticeshipClass");
            ApprenticeshipClassBusiness.DeleteApprenticeshipClass(id, new GlobalInfo().SchoolID.Value);
            this.ApprenticeshipClassBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ApprenticeshipClassViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ApprenticeshipClass> query = this.ApprenticeshipClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<ApprenticeshipClassViewModel> lst = query.Select(o => new ApprenticeshipClassViewModel
            {
                ApprenticeshipClassID = o.ApprenticeshipClassID,
                ClassName = o.ClassName,
                HeadTeacherID = o.HeadTeacherID,
                ApprenticeshipSubjectID = o.ApprenticeshipSubjectID,
                AcademicYearID = o.AcademicYearID,
                SchoolID = o.SchoolID,
                Semester = o.Semester,
                SubjectType = o.SubjectType,
                IsCommenting = o.IsCommenting,
                HeadTeacherFullName = o.Employee.FullName,
                Year = o.AcademicYear.DisplayTitle,
                SubjectName = o.ApprenticeshipSubject.SubjectName,
                FacultyName = o.Employee.SchoolFacultyID
            });

            IEnumerable<ApprenticeshipClassViewModel> listApprenticeshipClassViewModel = lst.ToList();
            return listApprenticeshipClassViewModel;
        }
    }
}





