/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ApprenticeshipClassArea
{
    public class ApprenticeshipClassConstants
    {
        public const string LIST_APPRENTICESHIPCLASS = "listApprenticeshipClass";
        public const string LIST_YEAR = "LIST_YEAR";
        public const string LIST_FACULTYNAME = "LIST_FACULTYNAME";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_SUBJECTTYPE = "LIST_SUBJECTTYPE";
        public const string LIST_SUBJECT = "LIST_SUBJECT";
        public const string LIST_ISCOMMENTING = "LIST_ISCOMMENTING";
        public const string LIST_Teacher = "LIST_Teacher";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string VISIBLE_BUTTON = "VISIBLE_BUTTON";
    }
}