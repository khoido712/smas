﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ReportResultLearningBySemesterArea.Models
{
    public class PeriodSearchViewModel
    {
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Semester")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int Semester { get; set; }
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Period")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int Period { get; set; }
        [ResourceDisplayName("ClassProfile_Control_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int EducationLevel { get; set; }
        public int IsCommenting { get; set; }
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Subject")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int Subject { get; set; }

        public int? AcademicYearID { get; set; }
        public int? AppliedLevel { get; set; }
        public int? SchoolID { get; set; }

        public int rptReport { get; set; }

    }
}