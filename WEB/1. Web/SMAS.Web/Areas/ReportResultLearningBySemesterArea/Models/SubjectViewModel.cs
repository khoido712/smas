using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportResultLearningBySemesterArea.Models
{
    public class SubjectViewModel
    {
        public int SubjectCatID { get; set; }
        public string DisplayName { get; set; }
        public int IsCommenting { get; set; }
        public int? OrderInSubject { get; set; }
    }
}
