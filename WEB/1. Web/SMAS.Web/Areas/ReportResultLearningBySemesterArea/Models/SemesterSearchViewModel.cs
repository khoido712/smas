﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.ReportResultLearningBySemesterArea.Models
{
    public class SemesterSearchViewModel
    {
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Semester")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? Semester { get; set; }
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Period")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? Period { get; set; }
        [ResourceDisplayName("ClassProfile_Control_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? EducationLevel { get; set; }
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Subject")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? Subject { get; set; }
        public int? StatisticLevelReportID { get; set; }
        public int rdoReportBySemester { get; set; }

        public string MarkType { get; set; }

        public string MarkColumn { get; set; }

        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Employee")]
        public int? EmployeeID { get; set; }

        
    }
}