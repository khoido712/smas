﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportResultLearningBySemesterArea.Models
{
    public class EmployeeCboModel
    {
        public int EmployeeID { get; set; }

        public string DisplayName { get; set; }
    }
}