﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportResultLearningBySemesterArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ReportResultLearningBySemesterArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class ReportResultLearningByPeriodController : BaseController
    {
        //
        // GET: /ReportResultLearningBySemesterArea/ReportResultLearningBySemester/
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IReportSynthesisCapacityConductByPeriodBusiness ReportSynthesisCapacityConductByPeriodBusiness;

        public readonly IAverageMarkByPeriodBusiness AverageMarkByPeriodBusiness;
        public readonly IDetailClassifiedSubjectByPeriodBusiness DetailClassifiedSubjectByPeriodBusiness;
        public readonly IClassifiedCapacityByPeriodBusiness ClassifiedCapacityByPeriodBusiness;
        public readonly IClassifiedConductByPeriodBusiness ClassifiedConductByPeriodBusiness;
        public readonly ISynthesisSubjectByPeriodBusiness SynthesisSubjectByPeriodBusiness;
        public readonly ISynthesisClassifiedSubjectByPeriodBusiness SynthesisClassifiedSubjectByPeriodBusiness;

        public ReportResultLearningByPeriodController(IReportSynthesisCapacityConductByPeriodBusiness ReportSynthesisCapacityConductByPeriodBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IPeriodDeclarationBusiness PeriodDeclarationBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            IAverageMarkByPeriodBusiness AverageMarkByPeriodBusiness,
            IDetailClassifiedSubjectByPeriodBusiness DetailClassifiedSubjectByPeriodBusiness,
            IClassifiedCapacityByPeriodBusiness ClassifiedCapacityByPeriodBusiness,
            IClassifiedConductByPeriodBusiness ClassifiedConductByPeriodBusiness,
            ISynthesisSubjectByPeriodBusiness SynthesisSubjectByPeriodBusiness,
            ISynthesisClassifiedSubjectByPeriodBusiness SynthesisClassifiedSubjectByPeriodBusiness
            )
        {
            this.ClassifiedCapacityByPeriodBusiness = ClassifiedCapacityByPeriodBusiness;
            this.DetailClassifiedSubjectByPeriodBusiness = DetailClassifiedSubjectByPeriodBusiness;
            this.AverageMarkByPeriodBusiness = AverageMarkByPeriodBusiness;
            this.ReportSynthesisCapacityConductByPeriodBusiness = ReportSynthesisCapacityConductByPeriodBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassifiedConductByPeriodBusiness = ClassifiedConductByPeriodBusiness;
            this.SynthesisSubjectByPeriodBusiness = SynthesisSubjectByPeriodBusiness;
            this.SynthesisClassifiedSubjectByPeriodBusiness = SynthesisClassifiedSubjectByPeriodBusiness;
        }
        private GlobalInfo GlobalInfo = new GlobalInfo();
        public ActionResult IndexPeriod()
        {
            SetViewData();
            return View();
        }

        public void SetViewData()
        {
            GlobalInfo Global = new GlobalInfo();
            //cbb hoc ky

            ViewData[ReportResultLearningBySemesterConstants.CBO_SEMESTER] = CommonList.Semester();
            ViewData[ReportResultLearningBySemesterConstants.CBO_PERIOD] = new List<PeriodDeclaration>();
            ViewData[ReportResultLearningBySemesterConstants.CBO_SUBJECT] = new List<SubjectCat>();
            ViewData[ReportResultLearningBySemesterConstants.CBO_EDUCATIONLEVEL] = Global.EducationLevels;

            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();


            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningByPeriod_Label_SubjectAverageMark");
            viettelradio.cchecked = true;
            viettelradio.disabled = false;
            viettelradio.Value = 1;
            lsradio.Add(viettelradio);

            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningByPeriod_Label_DetailBySubject");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 2;
            lsradio.Add(viettelradio);

            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 3;
            lsradio.Add(viettelradio);

            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningByPeriod_Label_ConductLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 4;
            lsradio.Add(viettelradio);

            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityLevelBySubject");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 5;
            lsradio.Add(viettelradio);
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityConductByEducationLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 6;
            lsradio.Add(viettelradio);

            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningByPeriod_Label_SubjectRankingStatistic");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 7;
            lsradio.Add(viettelradio);
            ViewData[ReportResultLearningBySemesterConstants.LIST_RADIOREPORT] = lsradio;

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["AcademicYearID"] = Global.AcademicYearID.Value;
            //-	cboSubject (Môn học): Lấy các thông tin 
            //-	EducationLevelID = cboEducationLevel.Value
            //-	IsCommenting = cboIsCommenting.Value
            //Map tương ứng vào đối tượng Dictionary<string, object> rồi gọi hàm:
            //ClassSubjectBusiness.SearchSubjectByAcademicYear(AcademicYearID = UserInfo.AcademicYearID, Dictionary<string,object>)
            IQueryable<SubjectCat> lsClassSubject = ClassSubjectBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary)
                                                                         .Select(s => s.SubjectCat)
                                                                         .Where(o => o.IsActive == true);

            if (lsClassSubject.Count() > 0)
            {
                ViewData[ReportResultLearningBySemesterConstants.CBO_SUBJECT] = lsClassSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }

            ViewData[ReportResultLearningBySemesterConstants.CBO_EDUCATIONLEVEL] = new GlobalInfo().EducationLevels;
        }

        #region Load combo Period

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPeriod(int? idSemester)
        {
            if (idSemester == null)
            {
                return Json(new SelectList(new List<PeriodDeclaration>(), "PeriodDeclarationID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            if (idSemester == 0)
            {
                idSemester = 3;
            }
            //-	cboPeriod: Lấy ra danh sách các đợt bằng cách gọi hàm PeriodDeclarationBusiness.SearchBySemester() với các tham số truyền vào:
            //+ AcadmicYearID: UserInfo.AcademicYear
            //+ SchoolID: UserInfo.SchoolID
            //+ SemesterID: cboSemester.SelectedValue
            //Kết quả thu được lstPeriodBySemester. Giá trị mặc định: Kiểm tra ngày hiện tại với ngày bắt đầu và ngày kết thúc của đợt để mặc định đợt hiện tại.

            GlobalInfo Global = new GlobalInfo();
            //cbo Periord
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["SchoolID"] = Global.SchoolID;
            Dictionary["Semester"] = idSemester;

            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySemester(Dictionary);
            int periodSelected = 0;
            for (int i = 0; i < lstPeriod.Count; i++)
            {
                PeriodDeclaration pd = lstPeriod[i];
                if (pd.FromDate.HasValue && pd.EndDate.HasValue)
                {
                    if (pd.FromDate < DateTime.Now && pd.EndDate > DateTime.Now)
                    {
                        periodSelected = pd.PeriodDeclarationID;
                        break;
                    }
                }
            }
            return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", periodSelected), JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Load combo Subject

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? idEducationLevel)
        {
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            //-	cboSubject (Môn học): Lấy các thông tin 
            //-	EducationLevelID = cboEducationLevel.Value
            //-	IsCommenting = cboIsCommenting.Value
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["IsCommenting"] = 0;
            Dictionary["AcademicYearID"] = GlobalInfo.AcademicYearID.Value;
            //Map tương ứng vào đối tượng Dictionary<string, object> rồi gọi hàm:
            //ClassSubjectBusiness.SearchSubjectByAcademicYear(AcademicYearID = UserInfo.AcademicYearID, Dictionary<string,object>)
            IQueryable<SubjectCat> lsClassSubject = ClassSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, Dictionary)
                                                                        .Select(s => s.SubjectCat)
                                                                        .Where(o => o.IsActive == true);

            if (lsClassSubject.Count() > 0)
            {
                ViewData[ReportResultLearningBySemesterConstants.CBO_SUBJECT] = lsClassSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            return Json(new SelectList(lsClassSubject.ToList(), "SubjectCatID", "SubjectName"), JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult Export(PeriodSearchViewModel frm, int? GetNew)
        {
            frm.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            frm.AppliedLevel = GlobalInfo.AppliedLevel.Value;
            frm.SchoolID = GlobalInfo.SchoolID.Value;

            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;


            return Json(new JsonReportMessage(processedReport, type));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        #region ExportExcel

        [ValidateAntiForgeryToken]
        public JsonResult GetReportByPeriod(PeriodSearchViewModel form)
        {
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            //Danh sách khen thưởng
            if (form.rptReport == 6)
            {
                TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();

                TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
                TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
                TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                TranscriptOfClass.PeriodDeclarationID = form.Period;
                TranscriptOfClass.Semester = form.Semester;
                string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM_THEO_DOT; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.ReportSynthesisCapacityConductByPeriodBusiness.GetSynthesisCapacityConductByPeriod(TranscriptOfClass);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.ReportSynthesisCapacityConductByPeriodBusiness.CreateSynthesisCapacityConductByPeriod(TranscriptOfClass);
                    processedReport = this.ReportSynthesisCapacityConductByPeriodBusiness.InsertSynthesisCapacityConductByPeriod(TranscriptOfClass, excel);
                    excel.Close();

                }
            }

            #region Thống kê điểm trung bình môn - AuNH
            if (form.rptReport == 1)
            {
                type = JsonReportMessage.NEW;
                TranscriptOfClass entity = new TranscriptOfClass();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.SubjectID = form.Subject;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodDeclarationID = form.Period;
                entity.Semester = form.Semester;

                processedReport = AverageMarkByPeriodBusiness.GetAverageMarkByPeriod(entity);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = AverageMarkByPeriodBusiness.CreateAverageMarkByPeriod(entity);
                    processedReport = AverageMarkByPeriodBusiness.InsertAverageMarkByPeriod(entity, excel);
                    excel.Close();
                }

            }
            #endregion

            #region Thống kê xếp loại chi tiết theo môn - AuNH
            if (form.rptReport == 2)
            {
                type = JsonReportMessage.NEW;
                TranscriptOfClass entity = new TranscriptOfClass();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.EducationLevelID = form.EducationLevel;
                entity.PeriodDeclarationID = form.Period;
                entity.Semester = form.Semester;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;

                processedReport = DetailClassifiedSubjectByPeriodBusiness.GetDetailClassifiedSubjectByPeriod(entity);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = DetailClassifiedSubjectByPeriodBusiness.CreateDetailClassifiedSubjectByPeriod(entity);
                    processedReport = DetailClassifiedSubjectByPeriodBusiness.InsertDetailClassifiedSubjectByPeriod(entity, excel);
                    excel.Close();
                }

            }
            #endregion

            #region Thống kê xếp loại học lực - AuNH
            if (form.rptReport == 3)
            {
                type = JsonReportMessage.NEW;
                TranscriptOfClass entity = new TranscriptOfClass();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodDeclarationID = form.Period;
                entity.Semester = form.Semester;

                processedReport = ClassifiedCapacityByPeriodBusiness.GetClassifiedCapacityByPeriod(entity);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ClassifiedCapacityByPeriodBusiness.CreateClassifiedCapacityByPeriod(entity);
                    processedReport = ClassifiedCapacityByPeriodBusiness.InsertClassifiedCapacityByPeriod(entity, excel);
                    excel.Close();
                }

            }
            #endregion

            #region Thống kê xếp loại hạnh kiểm - QuangLM
            if (form.rptReport == 4)
            {
                type = JsonReportMessage.NEW;
                ClassifiedConductByPeriod entity = new ClassifiedConductByPeriod();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodID = form.Period;

                processedReport = ClassifiedConductByPeriodBusiness.GetClassifiedConductByPeriod(entity);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ClassifiedConductByPeriodBusiness.CreateClassifiedConductByPeriod(entity);
                    processedReport = ClassifiedConductByPeriodBusiness.InsertClassifiedConductByPeriod(entity, excel);
                    excel.Close();
                }

            }
            #endregion

            #region Thống kê tổng hợp các môn - QuangLM
            if (form.rptReport == 5)
            {
                type = JsonReportMessage.NEW;
                SynthesisSubjectByPeriod entity = new SynthesisSubjectByPeriod();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodID = form.Period;


                processedReport = SynthesisSubjectByPeriodBusiness.GetSynthesisSubjectByPeriod(entity);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = SynthesisSubjectByPeriodBusiness.CreateSynthesisSubjectByPeriod(entity);
                    processedReport = SynthesisSubjectByPeriodBusiness.InsertSynthesisSubjectByPeriod(entity, excel);
                    excel.Close();
                }

            }
            #endregion

            #region Thống kê tổng hợp xếp loại các môn - QuangLM
            if (form.rptReport == 7)
            {
                type = JsonReportMessage.NEW;
                SynthesisClassifiedSubjectByPeriod entity = new SynthesisClassifiedSubjectByPeriod();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodID = form.Period;
                entity.SemesterID = form.Semester;


                processedReport = SynthesisClassifiedSubjectByPeriodBusiness.GetSynthesisClassifiedSubjectByPeriod(entity);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = SynthesisClassifiedSubjectByPeriodBusiness.CreateSynthesisClassifiedSubjectByPeriod(entity);
                    processedReport = SynthesisClassifiedSubjectByPeriodBusiness.InsertSynthesisClassifiedSubjectByPeriod(entity, excel);
                    excel.Close();
                }

            }
            #endregion


            return Json(new JsonReportMessage(processedReport, type));
        }




        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportByPeriod(PeriodSearchViewModel form)
        {
            ProcessedReport processedReport = null;
            if (form.rptReport == 6)
            {
                TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();


                TranscriptOfClass.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                TranscriptOfClass.SchoolID = GlobalInfo.SchoolID.Value;
                TranscriptOfClass.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                TranscriptOfClass.PeriodDeclarationID = form.Period;
                TranscriptOfClass.Semester = form.Semester;
                Stream excel = this.ReportSynthesisCapacityConductByPeriodBusiness.CreateSynthesisCapacityConductByPeriod(TranscriptOfClass);
                processedReport = this.ReportSynthesisCapacityConductByPeriodBusiness.InsertSynthesisCapacityConductByPeriod(TranscriptOfClass, excel);
                excel.Close();
            }

            #region Thống kê điểm trung bình môn - AuNH
            if (form.rptReport == 1)
            {

                TranscriptOfClass entity = new TranscriptOfClass();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.SubjectID = form.Subject;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodDeclarationID = form.Period;
                entity.Semester = form.Semester;
                Stream excel = AverageMarkByPeriodBusiness.CreateAverageMarkByPeriod(entity);
                processedReport = AverageMarkByPeriodBusiness.InsertAverageMarkByPeriod(entity, excel);
                excel.Close();


            }
            #endregion

            #region Thống kê xếp loại chi tiết theo môn - AuNH
            if (form.rptReport == 2)
            {
                TranscriptOfClass entity = new TranscriptOfClass();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.EducationLevelID = form.EducationLevel;
                entity.PeriodDeclarationID = form.Period;
                entity.Semester = form.Semester;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                Stream excel = DetailClassifiedSubjectByPeriodBusiness.CreateDetailClassifiedSubjectByPeriod(entity);
                processedReport = DetailClassifiedSubjectByPeriodBusiness.InsertDetailClassifiedSubjectByPeriod(entity, excel);
                excel.Close();


            }
            #endregion

            #region Thống kê xếp loại học lực - AuNH
            if (form.rptReport == 3)
            {

                TranscriptOfClass entity = new TranscriptOfClass();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodDeclarationID = form.Period;
                entity.Semester = form.Semester;
                Stream excel = ClassifiedCapacityByPeriodBusiness.CreateClassifiedCapacityByPeriod(entity);
                processedReport = ClassifiedCapacityByPeriodBusiness.InsertClassifiedCapacityByPeriod(entity, excel);
                excel.Close();


            }
            #endregion

            #region Thống kê xếp loại hạnh kiểm - QuangLM
            if (form.rptReport == 4)
            {

                ClassifiedConductByPeriod entity = new ClassifiedConductByPeriod();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodID = form.Period;
                entity.SemesterID = form.Semester;
                processedReport = ClassifiedConductByPeriodBusiness.GetClassifiedConductByPeriod(entity);
                Stream excel = ClassifiedConductByPeriodBusiness.CreateClassifiedConductByPeriod(entity);
                processedReport = ClassifiedConductByPeriodBusiness.InsertClassifiedConductByPeriod(entity, excel);
                excel.Close();


            }
            #endregion

            #region Thống kê tổng hợp các môn - QuangLM
            if (form.rptReport == 5)
            {
                SynthesisSubjectByPeriod entity = new SynthesisSubjectByPeriod();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodID = form.Period;
                entity.SemesterID = form.Semester;
                Stream excel = SynthesisSubjectByPeriodBusiness.CreateSynthesisSubjectByPeriod(entity);
                processedReport = SynthesisSubjectByPeriodBusiness.InsertSynthesisSubjectByPeriod(entity, excel);
                excel.Close();

            }
            #endregion

            #region Thống kê tổng hợp xếp loại các môn - QuangLM
            if (form.rptReport == 7)
            {
                SynthesisClassifiedSubjectByPeriod entity = new SynthesisClassifiedSubjectByPeriod();
                entity.SchoolID = GlobalInfo.SchoolID.Value;
                entity.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                entity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                entity.PeriodID = form.Period;
                entity.SemesterID = form.Semester;
                processedReport = SynthesisClassifiedSubjectByPeriodBusiness.GetSynthesisClassifiedSubjectByPeriod(entity);

                Stream excel = SynthesisClassifiedSubjectByPeriodBusiness.CreateSynthesisClassifiedSubjectByPeriod(entity);
                processedReport = SynthesisClassifiedSubjectByPeriodBusiness.InsertSynthesisClassifiedSubjectByPeriod(entity, excel);
                excel.Close();

            }
            #endregion

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        #endregion
    }
}
