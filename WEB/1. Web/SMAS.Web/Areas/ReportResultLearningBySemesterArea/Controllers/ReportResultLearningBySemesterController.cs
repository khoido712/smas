﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportResultLearningBySemesterArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ReportResultLearningBySemesterArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class ReportResultLearningBySemesterController : BaseController
    {
        //
        // GET: /ReportResultLearningBySemesterArea/ReportResultLearningBySemester/
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        public readonly IClassSubjectBusiness ClassSubjectBusiness;
        public readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        public readonly IPupilRewardReportBusiness PupilRewardReportBusiness;
        public readonly IProcessedReportBusiness ProcessedReportBusiness;
        public readonly IPupilRetestRegistrationBusiness PupilRetestRegistrationBusiness;
        public readonly IPupilRetestBusiness PupilRetestBusiness;
        public readonly IMarkOfSemesterBusiness MarkOfSemesterBusiness;
        public readonly IMarkOfSemesterBySubjectBusiness MarkOfSemesterBySubjectBusiness;
        public readonly IMarkOfSemesterByJudgeSubjectBusiness MarkOfSemesterByJudgeSubjectBusiness;
        public readonly IAverageMarkBySubjectBusiness AverageMarkBySubjectBusiness;
        public readonly IDetailClassifiedBySubjectBusiness DetailClassifiedBySubjectBusiness;
        public readonly ISubjectCatBusiness SubjectCatBusiness;
        public readonly IReportResultEndOfYearBusiness ReportResultEndOfYearBusiness;
        public readonly IClassifiedCapacityBusiness ClassifiedCapacityBusiness;
        public readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        public readonly IAcademicYearBusiness AcademicYearBusiness;
        public readonly IStatisticLevelReportBusiness StatisticLevelReportBusiness;
        //----minhh added
        public readonly IClassifiedConductBusiness ClassifiedConductBusiness;
        public readonly IClassifiedCapacityBySubjectBusiness ClassifiedCapacityBySubjectBusiness;
        public readonly ISynthesisCapacityConductBusiness SynthesisCapacityConductBusiness;
        public readonly ISynthesisSubjectByEducationLevelBusiness SynthesisSubjectByEducationLevelBusiness;
        public readonly IEmulationTitleBusiness EmulationTitleBusiness;
        public readonly IStatisticLevelConfigBusiness StatisticLevelConfigBusiness;
        //----end

        //Start: 2014/11/25 viethd4 Sua doi chuc nang Thống kê điểm TBM
        public readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        public readonly IEmployeeBusiness EmployeeBusiness;
        public readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        //End: 2014/11/25 viethd4 Sua doi chuc nang Thống kê điểm TBM

        public ReportResultLearningBySemesterController(IEmulationTitleBusiness EmulationTitleBusiness, IReportResultEndOfYearBusiness ReportResultEndOfYearBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness, IPupilRewardReportBusiness pupilRewardReportBusiness, IProcessedReportBusiness processedReportBusiness,
            IPupilRetestRegistrationBusiness pupilRetestRegistrationBusiness, IPupilRetestBusiness pupilRetestBusiness, IMarkOfSemesterBusiness markOfSemesterBusiness,
            IMarkOfSemesterBySubjectBusiness markOfSemesterBySubjectBusiness, IMarkOfSemesterByJudgeSubjectBusiness markOfSemesterByJudgeSubjectBusiness,
            IAverageMarkBySubjectBusiness averageMarkBySubjectBusiness, IDetailClassifiedBySubjectBusiness detailClassifiedBySubjectBusiness, IClassifiedCapacityBusiness classifiedcapacityBusiness,
            IClassifiedConductBusiness ClassifiedConductBusiness, IClassifiedCapacityBySubjectBusiness ClassifiedCapacityBySubjectBusiness, ISynthesisCapacityConductBusiness SynthesisCapacityConductBusiness,
            ISynthesisSubjectByEducationLevelBusiness SynthesisSubjectByEducationLevelBusiness, ISubjectCatBusiness subjectCatBusiness, ISchoolSubjectBusiness schoolSubjectBusiness, IAcademicYearBusiness academicYearBusiness, IStatisticLevelReportBusiness statisticLevelReportBusiness, IStatisticLevelConfigBusiness StatisticLevelConfigBusiness,
            ISemeterDeclarationBusiness SemesterDeclarationBusiness, IEmployeeBusiness EmployeeBusiness, ITeachingAssignmentBusiness TeachingAssignmentBusiness)
        {
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassifiedCapacityBusiness = classifiedcapacityBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.PupilRewardReportBusiness = pupilRewardReportBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ClassifiedConductBusiness = ClassifiedConductBusiness;
            this.ClassifiedCapacityBySubjectBusiness = ClassifiedCapacityBySubjectBusiness;
            this.SynthesisCapacityConductBusiness = SynthesisCapacityConductBusiness;
            this.SynthesisSubjectByEducationLevelBusiness = SynthesisSubjectByEducationLevelBusiness;
            this.PupilRetestRegistrationBusiness = pupilRetestRegistrationBusiness;
            this.PupilRetestBusiness = pupilRetestBusiness;
            this.MarkOfSemesterBusiness = markOfSemesterBusiness;
            this.MarkOfSemesterBySubjectBusiness = markOfSemesterBySubjectBusiness;
            this.MarkOfSemesterByJudgeSubjectBusiness = markOfSemesterByJudgeSubjectBusiness;
            this.AverageMarkBySubjectBusiness = averageMarkBySubjectBusiness;
            this.DetailClassifiedBySubjectBusiness = detailClassifiedBySubjectBusiness;
            this.EmulationTitleBusiness = EmulationTitleBusiness;
            this.ReportResultEndOfYearBusiness = ReportResultEndOfYearBusiness;
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.StatisticLevelReportBusiness = statisticLevelReportBusiness;
            this.StatisticLevelConfigBusiness = StatisticLevelConfigBusiness;
            this.SemesterDeclarationBusiness = SemesterDeclarationBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
        }


        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult _index()
        {
            SetViewData();
            return View();
        }
        public void SetViewData()
        {
            GlobalInfo Global = new GlobalInfo();
            //cbb hoc ky
            AcademicYear acaYear = AcademicYearBusiness.Find(Global.AcademicYearID.Value);

            int Semester = DateTime.Now < acaYear.SecondSemesterStartDate.Value
                                ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                : (DateTime.Now < acaYear.SecondSemesterEndDate.Value
                                        ? SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        : SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
            ViewData[ReportResultLearningBySemesterConstants.DEFAULT_SEMESTER] = Semester;

            ViewData[ReportResultLearningBySemesterConstants.CBO_SEMESTER] = CommonList.SemesterAndAll();
            ViewData[ReportResultLearningBySemesterConstants.CBO_PERIOD] = new List<PeriodDeclaration>();

            // Neu co thong tin ve hoc ky thi lay luon thong tin ve dot
            if (Global.Semester.HasValue)
            {
                IDictionary<string, object> PeriodSearchInfo = new Dictionary<string, object>();
                PeriodSearchInfo["AcademicYearID"] = Global.AcademicYearID;
                PeriodSearchInfo["Semester"] = Global.Semester.Value;
                List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(Global.SchoolID.Value, PeriodSearchInfo).ToList();

                int PeriodDeclarationIDSelected = 0;
                if (lstPeriod != null && lstPeriod.Count() > 0)
                {
                    foreach (var item in lstPeriod)
                    {
                        if (item.FromDate.HasValue && item.EndDate.HasValue)
                        {
                            if (item.FromDate < DateTime.Now && item.EndDate > DateTime.Now)
                            {
                                PeriodDeclarationIDSelected = item.PeriodDeclarationID;
                                break;
                            }
                        }
                    }
                }
                ViewData[ReportResultLearningBySemesterConstants.CBO_PERIOD_SELECTED] = PeriodDeclarationIDSelected;
                ViewData[ReportResultLearningBySemesterConstants.CBO_PERIOD] = lstPeriod;
            }

            ViewData[ReportResultLearningBySemesterConstants.CBO_EDUCATIONLEVEL] = Global.EducationLevels;

            IQueryable<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(Global.AcademicYearID.Value,
                                                                                new Dictionary<string, object> { 
                                                                                    { "AppliedLevel", Global.AppliedLevel.Value }
                                                                                });

            List<SubjectCat> lstClassSubject = listSchoolSubject.Select(o => o.SubjectCat).Distinct().ToList();
            lstClassSubject = lstClassSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            List<SubjectViewModel> lstSchoolSubjectModel = listSchoolSubject
                                                            .Select(u => new SubjectViewModel
                                                            {
                                                                SubjectCatID = u.SubjectID,
                                                                DisplayName = u.SubjectCat.DisplayName,
                                                                OrderInSubject = u.SubjectCat.OrderInSubject,
                                                                IsCommenting = u.IsCommenting.Value
                                                            }).Distinct()
                                                            .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName)
                                                            .ToList();

            ViewData[ReportResultLearningBySemesterConstants.List_SUBJECT] = lstSchoolSubjectModel;
            ViewData[ReportResultLearningBySemesterConstants.CBO_SUBJECT] = lstClassSubject;
            //lấy ra mức thống kê
            List<StatisticLevelReport> lstStatisticLevel = StatisticLevelReportBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            lstStatisticLevel = lstStatisticLevel.OrderBy(o => o.Resolution).ToList();
            if (lstStatisticLevel == null)
            {
                lstStatisticLevel = new List<StatisticLevelReport>();
            }
            ViewData[ReportResultLearningBySemesterConstants.CBO_STATISTIC_LEVEL] = lstStatisticLevel;
            StatisticLevelReport default_statistic_level = lstStatisticLevel.Where(o => o.Resolution.ToLower().Contains("chuẩn")).FirstOrDefault();
            int statictic_id_default = default_statistic_level != null ? default_statistic_level.StatisticLevelReportID : 0;
            ViewData[ReportResultLearningBySemesterConstants.DEFAULT_STATSICTIC_LEVEL] = statictic_id_default;

            //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
            //Lay danh sach loai diem
            List<ComboObject> lstMarkTypes = new List<ComboObject>();
            lstMarkTypes.Add(new ComboObject(ReportResultLearningBySemesterConstants.MARK_TYPE_PERIODIC.ToString(), Res.Get("ReportResultLearningBySemester_Label_MarkTypePeriod")));
            lstMarkTypes.Add(new ComboObject(ReportResultLearningBySemesterConstants.MARK_TYPE_AVERAGE.ToString(), Res.Get("ReportResultLearningBySemester_Label_MarkTypeAverage")));
            ViewData[ReportResultLearningBySemesterConstants.CBO_MARK_TYPE]=lstMarkTypes;

            
            //Lay danh sach cot diem
            //Lay ra SemesterDeclaration
            Dictionary<string, object> dicToGetSD=new Dictionary<string,object>();
            dicToGetSD["SchoolID"]=Global.SchoolID.Value;
            dicToGetSD["AcademicYearID"] = Global.AcademicYearID.Value;
            dicToGetSD["Semester"]=Semester;

            IQueryable<SemeterDeclaration> lstSD = SemesterDeclarationBusiness.Search(dicToGetSD).OrderByDescending(o => o.AppliedDate);
            SemeterDeclaration sd = lstSD.FirstOrDefault();
            //So con diem mieng
            int interviewMarkNum = 0;
            //So con diem viet he so 1
            int writingMarkNum = 0;
            //So con diem viet he so 2
            int twiceCoeffiecientMarkNum = 0;
            if (sd != null)
            {
                //So con diem mieng
                interviewMarkNum = sd.InterviewMark;
                //So con diem viet he so 1
                writingMarkNum = sd.WritingMark;
                //So con diem viet he so 2
                twiceCoeffiecientMarkNum = sd.TwiceCoeffiecientMark;
            }
            List<ComboObject> lstMarkColumn = new List<ComboObject>();
            string markColumn = string.Empty;

            for (int i = 1; i <= interviewMarkNum; i++)
            {
                markColumn = "M" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            for (int i = 1; i <= writingMarkNum; i++)
            {
                markColumn = "P" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            for (int i = 1; i <= twiceCoeffiecientMarkNum; i++)
            {
                markColumn = "V" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            lstMarkColumn.Add(new ComboObject("HK","KTHK"));


            ViewData[ReportResultLearningBySemesterConstants.CBO_MARK_COLUMN] = lstMarkColumn;

            if (lstSchoolSubjectModel != null && lstSchoolSubjectModel.Count > 0)
            {
                //Lay danh sach giao vien trong truong
                Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
                dicToGetTeacher["CurrentSchoolID"] = Global.SchoolID;
                IQueryable<Employee> query = EmployeeBusiness.Search(dicToGetTeacher);

                //Lay danh sach giao vien duong phan cong day mon duoc chon trong hoc ky duoc chon
                IDictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
                dicToGetAssignment["AcademicYearID"] = Global.AcademicYearID;
                dicToGetAssignment["SubjectID"] = lstSchoolSubjectModel[0].SubjectCatID;
                if (Semester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    dicToGetAssignment["Semester"] = Semester;
                }
                IQueryable<TeachingAssignment> lstTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, dicToGetAssignment);

                IQueryable<EmployeeCboModel> lstComboEmployee = from q in query
                                                                where lstTeachingAssignment.Any(o => o.TeacherID == q.EmployeeID)
                                                                select new EmployeeCboModel
                                                                {
                                                                    EmployeeID = q.EmployeeID,
                                                                    DisplayName = q.FullName + " - " + q.EmployeeCode

                                                                };

                ViewData[ReportResultLearningBySemesterConstants.CBO_EMPLOYEE] = lstComboEmployee;
            }
            else
            {
                ViewData[ReportResultLearningBySemesterConstants.CBO_EMPLOYEE] = new List<EmployeeCboModel>();
            }
            //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM

            #region Radio Report Semester
            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();

            //Khen thưởng
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_EmulationList");
            viettelradio.cchecked = true;
            viettelradio.disabled = false;
            viettelradio.Value = 1;
            lsradio.Add(viettelradio);

            //Danh sách thi lại
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_PupilRetest");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 2;
            lsradio.Add(viettelradio);

            //Danh sách đăng ký thi lại
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_PupilRetestRegistration");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 3;
            lsradio.Add(viettelradio);

            //Thống kê điểm thi học kỳ
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_MarkOfSemester");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 4;
            lsradio.Add(viettelradio);

            //Thông kê điểm thi học kỳ theo môn tính điểm và nhận xét
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_MarkOfSemesterBySubject");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 5;
            lsradio.Add(viettelradio);

            //Thống kê điểm trung bình môn
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_Mark");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 6;
            lsradio.Add(viettelradio);

            //Thống kê xếp loại chi tiết theo môn   
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_DetailBySubject");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 7;
            lsradio.Add(viettelradio);

            //Thống kê xếp loại học lực
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_CapacityLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 8;
            lsradio.Add(viettelradio);

            //Thống kê xếp loại hạnh kiểm
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_ConductLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 9;
            lsradio.Add(viettelradio);

            //Thông kê xếp loại học lực môn
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_CapacityLevelBySubject");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 10;
            lsradio.Add(viettelradio);

            //Thống kê tổng hợp các môn theo khối
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_SubjectByEducationLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 11;
            lsradio.Add(viettelradio);

            //Thống kê tổng hợp học lực hạnh kiểm
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_CapacityConductByEducationLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 12;
            lsradio.Add(viettelradio);

            //Thống kê danh hiệu thi đua
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_Emulation");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 13;
            lsradio.Add(viettelradio);

            //Thông kê kết quả cuối năm
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_ResultAtTheEndOfTheYear");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 14;
            lsradio.Add(viettelradio);
            ViewData[ReportResultLearningBySemesterConstants.LIST_RADIOREPORTSEMESTER] = lsradio;
            #endregion

            #region Radio Report Period
            List<ViettelCheckboxList> lsradioPeriod = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradioPeriod = new ViettelCheckboxList();

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_SubjectAverageMark");
            viettelradioPeriod.cchecked = true;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 1;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_DetailBySubject");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 2;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityLevel");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 3;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_ConductLevel");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 4;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityLevelBySubject");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 5;
            lsradioPeriod.Add(viettelradioPeriod);
            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityConductByEducationLevel");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 6;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_SubjectRankingStatistic");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 7;
            lsradioPeriod.Add(viettelradioPeriod);
            ViewData[ReportResultLearningBySemesterConstants.LIST_RADIOREPORT] = lsradioPeriod;
            #endregion
        }

        #region Load combo Subject

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? idEducationLevel, int? id)
        {

            GlobalInfo Global = new GlobalInfo();
            List<SubjectViewModel> lstSchoolSubject = new List<SubjectViewModel>();
            if (id.HasValue && id == 6)
            {
                lstSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(Global.AcademicYearID.Value, new Dictionary<string, object> {
                                                                                    { "AppliedLevel", Global.AppliedLevel.Value }, 
                                                                                    { "EducationLevelID", idEducationLevel }
                                                                                    
                                                                            })
                                                                            .Where(o => o.IsCommenting.HasValue && o.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                                                           .Select(u => new SubjectViewModel
                                                                           {
                                                                               SubjectCatID = u.SubjectID,
                                                                               DisplayName = u.SubjectCat.DisplayName,
                                                                               IsCommenting = u.IsCommenting.Value,
                                                                               OrderInSubject = u.SubjectCat.OrderInSubject
                                                                           })
                                                                           .Distinct()
                                                                           .ToList();
            }
            else
            {
                lstSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(Global.AcademicYearID.Value, new Dictionary<string, object> {
                                                                                    { "AppliedLevel", Global.AppliedLevel.Value }, 
                                                                                    { "EducationLevelID", idEducationLevel }
                                                                            })
                                                                          .Select(u => new SubjectViewModel
                                                                          {
                                                                              SubjectCatID = u.SubjectID,
                                                                              DisplayName = u.SubjectCat.DisplayName,
                                                                              IsCommenting = u.IsCommenting.Value,
                                                                              OrderInSubject = u.SubjectCat.OrderInSubject
                                                                          })
                                                                          .Distinct()
                                                                          .ToList();
            }
            lstSchoolSubject = lstSchoolSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).Distinct().ToList();           
            ViewData[ReportResultLearningBySemesterConstants.List_SUBJECT] = lstSchoolSubject;
            return Json(new SelectList(lstSchoolSubject, "SubjectCatID", "DisplayName"));
        }
        #endregion

        #region Load combo MarkColumn

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadMarkColum(int paraSemester)
        {
            GlobalInfo Global = new GlobalInfo();
            //Lay ra SemesterDeclaration
            Dictionary<string, object> dicToGetSD = new Dictionary<string, object>();
            dicToGetSD["SchoolID"] = Global.SchoolID.Value;
            dicToGetSD["AcademicYearID"] = Global.AcademicYearID.Value;
            dicToGetSD["Semester"] = paraSemester;

            IQueryable<SemeterDeclaration> lstSD = SemesterDeclarationBusiness.Search(dicToGetSD).OrderByDescending(o => o.AppliedDate);
            SemeterDeclaration sd = lstSD.FirstOrDefault();

            //So con diem mieng
            int interviewMarkNum = 0;
            //So con diem viet he so 1
            int writingMarkNum = 0;
            //So con diem viet he so 2
            int twiceCoeffiecientMarkNum = 0;
            if (sd != null)
            {
                //So con diem mieng
                interviewMarkNum = sd.InterviewMark;
                //So con diem viet he so 1
                writingMarkNum = sd.WritingMark;
                //So con diem viet he so 2
                twiceCoeffiecientMarkNum = sd.TwiceCoeffiecientMark;
            }

            List<ComboObject> lstMarkColumn = new List<ComboObject>();
            string markColumn = string.Empty;

            for (int i = 1; i <= interviewMarkNum; i++)
            {
                markColumn = "M" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            for (int i = 1; i <= writingMarkNum; i++)
            {
                markColumn = "P" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            for (int i = 1; i <= twiceCoeffiecientMarkNum; i++)
            {
                markColumn = "V" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }
            lstMarkColumn.Add(new ComboObject("HK", "KTHK"));

            ViewData[ReportResultLearningBySemesterConstants.CBO_MARK_COLUMN] = lstMarkColumn;
            return Json(new SelectList(lstMarkColumn, "key", "value"));
        }
        #endregion

        #region Load combo Giao vien theo

        public PartialViewResult AjaxLoadTeacher(int paraSubjectId, int paraSemester)
        {
            GlobalInfo Global = new GlobalInfo();
            //Lay danh sach giao vien trong truong
            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = Global.SchoolID;
            IQueryable<Employee> query = EmployeeBusiness.Search(dicToGetTeacher);

            //Lay danh sach giao vien duong phan cong day mon duoc chon
            IDictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
            dicToGetAssignment["AcademicYearID"] = Global.AcademicYearID;
            dicToGetAssignment["SubjectID"] = paraSubjectId;
            if (paraSemester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                dicToGetAssignment["Semester"] = paraSemester;
            }
            IQueryable<TeachingAssignment> lstTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, dicToGetAssignment);

            IQueryable<EmployeeCboModel> lstComboEmployee = from q in query
                                                            where lstTeachingAssignment.Any(o => o.TeacherID == q.EmployeeID)
                                                            select new EmployeeCboModel
                                                            {
                                                                EmployeeID = q.EmployeeID,
                                                                DisplayName = q.FullName + " - " + q.EmployeeCode

                                                            };

            ViewData[ReportResultLearningBySemesterConstants.CBO_EMPLOYEE] = lstComboEmployee;

            return PartialView("_TeacherCombobox");
        }
        #endregion

        #region ExportExcel

        [ValidateAntiForgeryToken]
        public JsonResult GetReportBySemester(SemesterSearchViewModel form)
        {
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            GlobalInfo global = new GlobalInfo();
            List<StatisticLevelConfig> lstStatisticLevelConfig = new List<StatisticLevelConfig>();
            if (form.rdoReportBySemester == 4 || form.rdoReportBySemester == 5 || form.rdoReportBySemester == 6 || form.rdoReportBySemester == 10 || form.rdoReportBySemester == 11)
            {
                if (form.StatisticLevelReportID.HasValue)
                {
                    lstStatisticLevelConfig = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { {"StatisticLevelReportID",form.StatisticLevelReportID.Value}}).ToList();
                }
            }
            #region 1 Danh sách khen thưởng
            if (form.rdoReportBySemester == 1)
            {
                PupilRewardReportBO PupilRewardReportBO = new PupilRewardReportBO();
                if (form.Semester.HasValue)
                {
                    PupilRewardReportBO.Semester = form.Semester.Value;
                }
                else
                {
                    PupilRewardReportBO.Semester = 0;
                }
                if (form.EducationLevel.HasValue)
                {
                    PupilRewardReportBO.EducationLevelID = form.EducationLevel.Value;
                }
                else
                { PupilRewardReportBO.EducationLevelID = 0; }
                PupilRewardReportBO.AppliedLevel = global.AppliedLevel.Value;
                PupilRewardReportBO.SchoolID = global.SchoolID.Value;
                PupilRewardReportBO.AcademicYearID = global.AcademicYearID.Value;
                PupilRewardReportBO.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
                PupilRewardReportBO.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
                string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.PupilRewardReportBusiness.GetPupilReward(PupilRewardReportBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.PupilRewardReportBusiness.CreatePupilReward(PupilRewardReportBO);
                    processedReport = this.PupilRewardReportBusiness.InsertPupilReward(PupilRewardReportBO, excel);
                    excel.Close();

                }
            }
            #endregion

            #region 2 Danh sách thi lại
            if (form.rdoReportBySemester == 2)
            {
                PupilRetestBO PupilRetestBO = new PupilRetestBO();
                PupilRetestBO.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                PupilRetestBO.EducationLevelID = 0;
                PupilRetestBO.AppliedLevel = global.AppliedLevel.Value;
                PupilRetestBO.SchoolID = global.SchoolID.Value;
                PupilRetestBO.AcademicYearID = global.AcademicYearID.Value;
                string reportCode = SystemParamsInFile.REPORT_DS_HS_THI_LAI; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.PupilRetestBusiness.GetPupilRetest(PupilRetestBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.PupilRetestBusiness.CreatePupilRetest(PupilRetestBO);
                    processedReport = this.PupilRetestBusiness.InsertPupilRetest(PupilRetestBO, excel);
                    excel.Close();

                }
            }
            #endregion

            #region 3 Danh sách đăng ký thi lại
            if (form.rdoReportBySemester == 3)
            {
                PupilRetestRegistrationBO PupilRetestRegistrationBO = new PupilRetestRegistrationBO();
                PupilRetestRegistrationBO.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                PupilRetestRegistrationBO.EducationLevelID = 0;
                if (form.Subject.HasValue)
                {
                    PupilRetestRegistrationBO.SubjectID = form.Subject.Value;
                }

                PupilRetestRegistrationBO.AppliedLevel = global.AppliedLevel.Value;
                PupilRetestRegistrationBO.SchoolID = global.SchoolID.Value;
                PupilRetestRegistrationBO.AcademicYearID = global.AcademicYearID.Value;
                string reportCode = SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    //string InputParameterHashKey = PupilRetestRegistrationBusiness.GetHashKey(PupilRetestRegistrationBO);
                    //processedReport = ProcessedReportBusiness.GetProcessedReport(InputParameterHashKey, reportCode);
                    processedReport = this.PupilRetestRegistrationBusiness.GetPupilRetest(PupilRetestRegistrationBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.PupilRetestRegistrationBusiness.CreatePupilRetestRegistrationSecondary(PupilRetestRegistrationBO);
                    processedReport = this.PupilRetestRegistrationBusiness.InsertPupilRetestRegistrationSecondary(PupilRetestRegistrationBO, excel);
                    excel.Close();

                }
            }
            #endregion

            #region 4 Thống kê điểm thi học kỳ
            if (form.rdoReportBySemester == 4)
            {
                if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                {
                    throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                }
                MarkOfSemesterBO MarkOfSemesterBO = new MarkOfSemesterBO();
                if (form.Semester.HasValue)
                {
                    MarkOfSemesterBO.Semester = form.Semester.Value;
                }
                if (form.StatisticLevelReportID.HasValue)
                {
                    MarkOfSemesterBO.StatisticReportID = form.StatisticLevelReportID.Value;
                }
                MarkOfSemesterBO.AppliedLevel = global.AppliedLevel.Value;
                MarkOfSemesterBO.SchoolID = global.SchoolID.Value;
                MarkOfSemesterBO.AcademicYearID = global.AcademicYearID.Value;

                string reportCode = SystemParamsInFile.REPORT_TKDiemThiHocKy; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.MarkOfSemesterBusiness.GetMarkOfSemester(MarkOfSemesterBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.MarkOfSemesterBusiness.CreateMarkOfSemester(MarkOfSemesterBO);
                    processedReport = this.MarkOfSemesterBusiness.InsertMarkOfSemester(MarkOfSemesterBO, excel);
                    excel.Close();

                }
            }
            #endregion

            #region 5 Thống kê điểm thi học kỳ theo môn
            if (form.rdoReportBySemester == 5)
            {
                GlobalInfo glo = global;
                if (!form.Subject.HasValue)
                {
                    throw new BusinessException("Please_Choice_Subject");
                }
                SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { 
                    { "AcademicYearID", glo.AcademicYearID.Value } ,
                    { "SubjectID", form.Subject.Value }
                }).FirstOrDefault();
                
                if (schoolSubject.IsCommenting.Value != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }
                    //Thống kê điểm thi học kỳ theo môn tính điểm
                    MarkOfSemesterBySubjectBO MarkOfSemesterBySubjectBO = new MarkOfSemesterBySubjectBO();
                    if (form.Semester.HasValue)
                    {
                        MarkOfSemesterBySubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.Subject.HasValue)
                    {
                        MarkOfSemesterBySubjectBO.SubjectID = form.Subject.Value;
                    }
                    if (form.StatisticLevelReportID.HasValue)
                    {
                        MarkOfSemesterBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    }
                    MarkOfSemesterBySubjectBO.AppliedLevel = global.AppliedLevel.Value;
                    MarkOfSemesterBySubjectBO.SchoolID = global.SchoolID.Value;
                    MarkOfSemesterBySubjectBO.AcademicYearID = global.AcademicYearID.Value;


                    string reportCode = SystemParamsInFile.REPORT_TKDiemThiHocKyTheoMonTinhDiem; ;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = this.MarkOfSemesterBySubjectBusiness.GetMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = this.MarkOfSemesterBySubjectBusiness.CreateMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO);
                        processedReport = this.MarkOfSemesterBySubjectBusiness.InsertMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO, excel);
                        excel.Close();
                    }
                }
                else
                {
                    //Thống kê điểm thi học kỳ theo môn nhận xét
                    MarkOfSemesterByJudgeSubjectBO MarkOfSemesterByJudgeSubjectBO = new MarkOfSemesterByJudgeSubjectBO();
                    if (form.Semester.HasValue)
                    {
                        MarkOfSemesterByJudgeSubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.Subject.HasValue)
                    {
                        MarkOfSemesterByJudgeSubjectBO.SubjectID = form.Subject.Value;
                    }
                    MarkOfSemesterByJudgeSubjectBO.AppliedLevel = global.AppliedLevel.Value;
                    MarkOfSemesterByJudgeSubjectBO.SchoolID = global.SchoolID.Value;
                    MarkOfSemesterByJudgeSubjectBO.AcademicYearID = global.AcademicYearID.Value;


                    string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK_THEOMON_NHANXET; ;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = this.MarkOfSemesterByJudgeSubjectBusiness.GetMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = this.MarkOfSemesterByJudgeSubjectBusiness.CreateMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO);
                        processedReport = this.MarkOfSemesterByJudgeSubjectBusiness.InsertMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO, excel);
                        excel.Close();

                    }
                }
            }
            #endregion

            #region 6 Thống kê điểm trung bình môn
            if (form.rdoReportBySemester == 6)
            {
               
                AverageMarkBySubjectBO AverageMarkBySubjectBO = new AverageMarkBySubjectBO();
                if (!form.Subject.HasValue)
                {
                    throw new BusinessException("ReportResultLearningBySemester_Validate_Subject");
                }
                SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { 
                    { "AcademicYearID", global.AcademicYearID.Value } ,
                    { "SubjectID", form.Subject.Value }
                }).FirstOrDefault();
                if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }
                }
                if (Convert.ToInt16(form.MarkType) == ReportResultLearningBySemesterConstants.MARK_TYPE_PERIODIC && String.IsNullOrEmpty(form.MarkColumn))
                {
                    throw new BusinessException("ReportResultLearningBySemester_Validate_MarkColumn");
                }
                if (form.Semester.HasValue)
                {
                    AverageMarkBySubjectBO.Semester = form.Semester.Value;
                }
                if (form.Subject.HasValue)
                {
                    AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                }
                if (form.StatisticLevelReportID.HasValue)
                {
                    AverageMarkBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                }
                //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                if (form.EmployeeID.HasValue)
                {
                    AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                }
                if (!String.IsNullOrEmpty(form.MarkType))
                {
                    AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                }
                else
                {
                    AverageMarkBySubjectBO.MarkType = ReportResultLearningBySemesterConstants.MARK_TYPE_AVERAGE;
                }

                AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                AverageMarkBySubjectBO.AppliedLevel = global.AppliedLevel.Value;
                AverageMarkBySubjectBO.SchoolID = global.SchoolID.Value;
                AverageMarkBySubjectBO.AcademicYearID = global.AcademicYearID.Value;

                string reportCode = SystemParamsInFile.REPORT_ThongKeDiemTBM; 
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        processedReport = this.AverageMarkBySubjectBusiness.GetAverageMarkBySubject(AverageMarkBySubjectBO);
                    }
                    else
                    {
                        processedReport = this.AverageMarkBySubjectBusiness.GetAverageMarkBySubjectJudge(AverageMarkBySubjectBO);
                    }
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubject(AverageMarkBySubjectBO);
                        processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubject(AverageMarkBySubjectBO, excel);
                        excel.Close();
                    }
                    else
                    {
                        Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectJudge(AverageMarkBySubjectBO);
                        processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectJudge(AverageMarkBySubjectBO, excel);
                        excel.Close();
                    }

                }
            }
            #endregion

            #region 7 Thống kê xếp loại chi tiết môn
            if (form.rdoReportBySemester == 7)
            {
                DetailClassifiedBySubjectBO DetailClassifiedBySubjectBO = new DetailClassifiedBySubjectBO();
                if (form.Semester.HasValue)
                {
                    DetailClassifiedBySubjectBO.Semester = form.Semester.Value;
                }
                if (form.EducationLevel.HasValue)
                {
                    DetailClassifiedBySubjectBO.EducationLevelID = form.EducationLevel.Value;
                }
                DetailClassifiedBySubjectBO.SchoolID = global.SchoolID.Value;
                DetailClassifiedBySubjectBO.AcademicYearID = global.AcademicYearID.Value;
                DetailClassifiedBySubjectBO.AppliedLevel = global.AppliedLevel.Value;
                string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_CHITIET_MON; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.DetailClassifiedBySubjectBusiness.GetDetailClassifiedBySubject(DetailClassifiedBySubjectBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.DetailClassifiedBySubjectBusiness.CreateDetailClassifiedBySubject(DetailClassifiedBySubjectBO);
                    processedReport = this.DetailClassifiedBySubjectBusiness.InsertDetailClassifiedBySubject(DetailClassifiedBySubjectBO, excel);
                    excel.Close();

                }
            }
            #endregion

            #region 8 Thống kê xếp loại học lực
            if (form.rdoReportBySemester == 8)
            {
                ClassifiedCapacityBO ClassifiedCapacityBO = new ClassifiedCapacityBO();
                if (form.Semester.HasValue)
                {
                    ClassifiedCapacityBO.Semester = form.Semester.Value;
                }
                ClassifiedCapacityBO.SchoolID = global.SchoolID.Value;
                ClassifiedCapacityBO.AcademicYearID = global.AcademicYearID.Value;
                ClassifiedCapacityBO.AppliedLevel = global.AppliedLevel.Value;

                string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_HOC_LUC; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.ClassifiedCapacityBusiness.GetClassifiedCapacity(ClassifiedCapacityBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.ClassifiedCapacityBusiness.CreateClassifiedCapacity(ClassifiedCapacityBO);
                    processedReport = this.ClassifiedCapacityBusiness.InsertClassifiedCapacity(ClassifiedCapacityBO, excel);
                    excel.Close();

                }
            }
            #endregion

            #region 9 Thống kê xếp loại hạnh kiểm - minhh
            if (form.rdoReportBySemester == 9)
            {
                ClassifiedConduct entity = new ClassifiedConduct();
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;
                string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HANH_KIEM;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                    processedReport = ClassifiedConductBusiness.GetClassifiedConduct(entity);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ClassifiedConductBusiness.CreateClassifiedConduct(entity);
                    processedReport = ClassifiedConductBusiness.InsertClassifiedConduct(entity, excel);
                    excel.Close();
                }
            }

            #endregion

            #region 10 Thống kê tổng hợp xếp loại các môn - minhh
            if (form.rdoReportBySemester == 10)
            {
                if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                {
                    throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                }
                ClassifiedCapacityBySubject entity = new ClassifiedCapacityBySubject();
                entity.Semester = form.Semester.Value;
                entity.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;
                string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HOC_LUC_MON;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ClassifiedCapacityBySubjectBusiness.GetClassifiedCapacityBySubject(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ClassifiedCapacityBySubjectBusiness.CreateClassifiedCapacityBySubject(entity);
                    processedReport = ClassifiedCapacityBySubjectBusiness.InsertClassifiedCapacityBySubject(entity, excel);
                    excel.Close();
                }
            }
            #endregion

            #region 11 Thống kê tổng hợp xếp loại mon hoc theo khoi - minhh
            if (form.rdoReportBySemester == 11)
            {
                if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                {
                    throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                }
                SynthesisSubjectByEducationLevel entity = new SynthesisSubjectByEducationLevel();
                entity.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;
                string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_CAC_MON_THEO_KHOI;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = SynthesisSubjectByEducationLevelBusiness.GetSynthesisSubjectByEducationLevel(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = SynthesisSubjectByEducationLevelBusiness.CreateSynthesisSubjectByEducationLevel(entity);
                    processedReport = SynthesisSubjectByEducationLevelBusiness.InsertSynthesisSubjectByEducationLevel(entity, excel);
                    excel.Close();
                }
            }
            #endregion

            #region 12 Thống kê tổng hợp xếp loại hoc luc hanh kiem - minhh
            if (form.rdoReportBySemester == 12)
            {
                SynthesisCapacityConduct entity = new SynthesisCapacityConduct();
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;
                string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = SynthesisCapacityConductBusiness.GetSynthesisCapacityConduct(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = SynthesisCapacityConductBusiness.CreateSynthesisCapacityConduct(entity);
                    processedReport = SynthesisCapacityConductBusiness.InsertSynthesisCapacityConduct(entity, excel);
                    excel.Close();
                }
            }
            #endregion

            #region 13 Thống kê danh hieu thi dua - minhh
            if (form.rdoReportBySemester == 13)
            {
                EmulationTitle entity = new EmulationTitle();
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                string reportCode = SystemParamsInFile.REPORT_THONG_KE_DANH_HIEU_THI_DUA;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = EmulationTitleBusiness.GetEmulationTitle(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = EmulationTitleBusiness.CreateEmulationTitle(entity);
                    processedReport = EmulationTitleBusiness.InsertEmulationTitle(entity, excel);
                    excel.Close();
                }
            }
            #endregion

            #region 14 Thống kê kết quả cuối năm
            if (form.rdoReportBySemester == 14)
            {
                GlobalInfo Global = new GlobalInfo();
                TranscriptOfClass ResultEndOfYear = new TranscriptOfClass();
                ResultEndOfYear.AppliedLevel = Global.AppliedLevel.Value;
                ResultEndOfYear.AcademicYearID = Global.AcademicYearID.Value;
                ResultEndOfYear.SchoolID = Global.SchoolID.Value;

                string reportCode = SystemParamsInFile.REPORT_THONG_KE_KET_QUA_CUOI_NAM; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.ReportResultEndOfYearBusiness.GetReportResultEndOfYear(ResultEndOfYear);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.ReportResultEndOfYearBusiness.CreateReportResultEndOfYear(ResultEndOfYear);
                    processedReport = this.ReportResultEndOfYearBusiness.InsertResultEndOfYear(ResultEndOfYear, excel);
                    excel.Close();

                }
            }
            #endregion

            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySemester(SemesterSearchViewModel form)
        {
            ProcessedReport processedReport = null;
            GlobalInfo global = new GlobalInfo();

            #region Danh sách khen thưởng
            if (form.rdoReportBySemester == 1)
            {
                PupilRewardReportBO PupilRewardReportBO = new PupilRewardReportBO(); 

                if (form.Semester.HasValue)
                {
                    PupilRewardReportBO.Semester = form.Semester.Value;
                }
                else
                {
                    PupilRewardReportBO.Semester = 0;
                }
                if (form.EducationLevel.HasValue)
                {
                    PupilRewardReportBO.EducationLevelID = form.EducationLevel.Value;
                }
                else
                { PupilRewardReportBO.EducationLevelID = 0; }
                PupilRewardReportBO.AppliedLevel = global.AppliedLevel.Value;
                PupilRewardReportBO.SchoolID = global.SchoolID.Value;
                PupilRewardReportBO.AcademicYearID = global.AcademicYearID.Value;
                PupilRewardReportBO.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
                PupilRewardReportBO.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
                Stream excel = this.PupilRewardReportBusiness.CreatePupilReward(PupilRewardReportBO);
                processedReport = this.PupilRewardReportBusiness.InsertPupilReward(PupilRewardReportBO, excel);
                excel.Close();
            }
            #endregion

            #region Danh sách học sinh thi lại
            if (form.rdoReportBySemester == 2)
            {
                PupilRetestBO PupilRetestBO = new PupilRetestBO();
                PupilRetestBO.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                PupilRetestBO.EducationLevelID = 0;
                PupilRetestBO.AppliedLevel = global.AppliedLevel.Value;
                PupilRetestBO.SchoolID = global.SchoolID.Value;
                PupilRetestBO.AcademicYearID = global.AcademicYearID.Value;

                Stream excel = this.PupilRetestBusiness.CreatePupilRetest(PupilRetestBO);
                processedReport = this.PupilRetestBusiness.InsertPupilRetest(PupilRetestBO, excel);
                excel.Close();
            }
            #endregion

            #region Danh sách học sinh đăng kí thi lại
            if (form.rdoReportBySemester == 3)
            {
                PupilRetestRegistrationBO PupilRetestRegistrationBO = new PupilRetestRegistrationBO();
                PupilRetestRegistrationBO.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                PupilRetestRegistrationBO.EducationLevelID = 0;
                PupilRetestRegistrationBO.SubjectID = form.Subject.Value;
                PupilRetestRegistrationBO.AppliedLevel = global.AppliedLevel.Value;
                PupilRetestRegistrationBO.SchoolID = global.SchoolID.Value;
                PupilRetestRegistrationBO.AcademicYearID = global.AcademicYearID.Value;

                Stream excel = this.PupilRetestRegistrationBusiness.CreatePupilRetestRegistrationSecondary(PupilRetestRegistrationBO);
                processedReport = this.PupilRetestRegistrationBusiness.InsertPupilRetestRegistrationSecondary(PupilRetestRegistrationBO, excel);
                excel.Close();

            }
            #endregion

            #region Thống kê điểm thi học kì
            if (form.rdoReportBySemester == 4)
            {
                MarkOfSemesterBO MarkOfSemesterBO = new MarkOfSemesterBO();
                
                if (form.Semester.HasValue)
                {
                    MarkOfSemesterBO.Semester = form.Semester.Value;
                }
                if (form.StatisticLevelReportID.HasValue)
                {
                    MarkOfSemesterBO.StatisticReportID = form.StatisticLevelReportID.Value;
                }
                MarkOfSemesterBO.AppliedLevel = global.AppliedLevel.Value;
                MarkOfSemesterBO.SchoolID = global.SchoolID.Value;
                MarkOfSemesterBO.AcademicYearID = global.AcademicYearID.Value;

                Stream excel = this.MarkOfSemesterBusiness.CreateMarkOfSemester(MarkOfSemesterBO);
                processedReport = this.MarkOfSemesterBusiness.InsertMarkOfSemester(MarkOfSemesterBO, excel);
                excel.Close();

            }
            #endregion

            #region Thống kê điểm thi học kì theo môn
            if (form.rdoReportBySemester == 5)
            {
                SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { 
                    { "AcademicYearID", global.AcademicYearID.Value } ,
                    { "SubjectID", form.Subject.Value }
                }).FirstOrDefault();
                if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    MarkOfSemesterBySubjectBO MarkOfSemesterBySubjectBO = new MarkOfSemesterBySubjectBO();
                    if (form.Semester.HasValue)
                    {
                        MarkOfSemesterBySubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.Subject.HasValue)
                    {
                        MarkOfSemesterBySubjectBO.SubjectID = form.Subject.Value;
                    }
                    if (form.StatisticLevelReportID.HasValue)
                    {
                        MarkOfSemesterBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    }
                    MarkOfSemesterBySubjectBO.AppliedLevel = global.AppliedLevel.Value;
                    MarkOfSemesterBySubjectBO.SchoolID = global.SchoolID.Value;
                    MarkOfSemesterBySubjectBO.AcademicYearID = global.AcademicYearID.Value;

                    Stream excel = this.MarkOfSemesterBySubjectBusiness.CreateMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO);
                    processedReport = this.MarkOfSemesterBySubjectBusiness.InsertMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO, excel);
                    excel.Close();
                }
                else
                {
                    MarkOfSemesterByJudgeSubjectBO MarkOfSemesterByJudgeSubjectBO = new MarkOfSemesterByJudgeSubjectBO();
                    if (form.Semester.HasValue)
                    {
                        MarkOfSemesterByJudgeSubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.Subject.HasValue)
                    {
                        MarkOfSemesterByJudgeSubjectBO.SubjectID = form.Subject.Value;
                    }
                    if (form.StatisticLevelReportID.HasValue)
                    {
                        MarkOfSemesterByJudgeSubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    }
                    MarkOfSemesterByJudgeSubjectBO.AppliedLevel = global.AppliedLevel.Value;
                    MarkOfSemesterByJudgeSubjectBO.SchoolID = global.SchoolID.Value;
                    MarkOfSemesterByJudgeSubjectBO.AcademicYearID = global.AcademicYearID.Value;

                    Stream excel = this.MarkOfSemesterByJudgeSubjectBusiness.CreateMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO);
                    processedReport = this.MarkOfSemesterByJudgeSubjectBusiness.InsertMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO, excel);
                    excel.Close();
                }
            }
            #endregion

            #region Thống kê điểm trung bình môn
            if (form.rdoReportBySemester == 6)
            {
                AverageMarkBySubjectBO AverageMarkBySubjectBO = new AverageMarkBySubjectBO();
                SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { 
                    { "AcademicYearID", global.AcademicYearID.Value } ,
                    { "SubjectID", form.Subject.Value }
                }).FirstOrDefault();
                if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    if (form.Semester.HasValue)
                    {
                        AverageMarkBySubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.Subject.HasValue)
                    {
                        AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                    }
                    if (form.StatisticLevelReportID.HasValue)
                    {
                        AverageMarkBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    }
                    //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                   
                    if (form.EmployeeID.HasValue)
                    {
                        AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                    }
                    if (!String.IsNullOrEmpty(form.MarkType))
                    {
                        AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                    }
                    else
                    {
                        AverageMarkBySubjectBO.MarkType = ReportResultLearningBySemesterConstants.MARK_TYPE_AVERAGE;
                    }

                    AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                    //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                    AverageMarkBySubjectBO.AppliedLevel = global.AppliedLevel.Value;
                    AverageMarkBySubjectBO.SchoolID = global.SchoolID.Value;
                    AverageMarkBySubjectBO.AcademicYearID = global.AcademicYearID.Value;

                    Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubject(AverageMarkBySubjectBO);
                    processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubject(AverageMarkBySubjectBO, excel);
                    excel.Close();
                }
                else
                {
                    if (form.Semester.HasValue)
                    {
                        AverageMarkBySubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.Subject.HasValue)
                    {
                        AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                    }
                    //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                    if (form.EmployeeID.HasValue)
                    {
                        AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                    }
                    if (!String.IsNullOrEmpty(form.MarkType))
                    {
                        AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                    }
                    else
                    {
                        AverageMarkBySubjectBO.MarkType = ReportResultLearningBySemesterConstants.MARK_TYPE_AVERAGE;
                    }
                    AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                    //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                    AverageMarkBySubjectBO.AppliedLevel = global.AppliedLevel.Value;
                    AverageMarkBySubjectBO.SchoolID = global.SchoolID.Value;
                    AverageMarkBySubjectBO.AcademicYearID = global.AcademicYearID.Value;

                    Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectJudge(AverageMarkBySubjectBO);
                    processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectJudge(AverageMarkBySubjectBO, excel);
                    excel.Close();
                }

            }
            #endregion

            #region Thống kê xếp loại chi tiết theo môn
            if (form.rdoReportBySemester == 7)
            {
                DetailClassifiedBySubjectBO DetailClassifiedBySubjectBO = new DetailClassifiedBySubjectBO();
                if (form.Semester.HasValue)
                {
                    DetailClassifiedBySubjectBO.Semester = form.Semester.Value;
                }
                if (form.EducationLevel.HasValue)
                {
                    DetailClassifiedBySubjectBO.EducationLevelID = form.EducationLevel.Value;
                }
                DetailClassifiedBySubjectBO.SchoolID = global.SchoolID.Value;
                DetailClassifiedBySubjectBO.AcademicYearID = global.AcademicYearID.Value;
                DetailClassifiedBySubjectBO.AppliedLevel = global.AppliedLevel.Value;

                Stream excel = this.DetailClassifiedBySubjectBusiness.CreateDetailClassifiedBySubject(DetailClassifiedBySubjectBO);
                processedReport = this.DetailClassifiedBySubjectBusiness.InsertDetailClassifiedBySubject(DetailClassifiedBySubjectBO, excel);
                excel.Close();

            }
            #endregion

            #region Thống kê xếp loại học lực
            if (form.rdoReportBySemester == 8)
            {
                ClassifiedCapacityBO ClassifiedCapacityBO = new ClassifiedCapacityBO();
                if (form.Semester.HasValue)
                {
                    ClassifiedCapacityBO.Semester = form.Semester.Value;
                }
                ClassifiedCapacityBO.SchoolID = global.SchoolID.Value;
                ClassifiedCapacityBO.AcademicYearID = global.AcademicYearID.Value;
                ClassifiedCapacityBO.AppliedLevel = global.AppliedLevel.Value;

                Stream excel = this.ClassifiedCapacityBusiness.CreateClassifiedCapacity(ClassifiedCapacityBO);
                processedReport = this.ClassifiedCapacityBusiness.InsertClassifiedCapacity(ClassifiedCapacityBO, excel);
                excel.Close();

            }
            #endregion

            #region Thống kê tổng hợp xếp loại học lực - hạnh kiểm
            if (form.rdoReportBySemester == 14)
            {
                GlobalInfo Global = global;
                TranscriptOfClass ResultEndOfYear = new TranscriptOfClass();
                ResultEndOfYear.AppliedLevel = Global.AppliedLevel.Value;
                ResultEndOfYear.AcademicYearID = Global.AcademicYearID.Value;
                ResultEndOfYear.SchoolID = Global.SchoolID.Value;


                Stream excel = ReportResultEndOfYearBusiness.CreateReportResultEndOfYear(ResultEndOfYear);
                processedReport = ReportResultEndOfYearBusiness.InsertResultEndOfYear(ResultEndOfYear, excel);
                excel.Close();

            }
            #endregion

            #region Thống kê xếp loại hạnh kiểm - minhh
            if (form.rdoReportBySemester == 9)
            {
                ClassifiedConduct entity = new ClassifiedConduct();
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;
                Stream excel = ClassifiedConductBusiness.CreateClassifiedConduct(entity);
                processedReport = ClassifiedConductBusiness.InsertClassifiedConduct(entity, excel);
                excel.Close();
            }
            #endregion

            #region  Thống kê tổng hợp xếp loại các môn  - minhh
            if (form.rdoReportBySemester == 10)
            {
                ClassifiedCapacityBySubject entity = new ClassifiedCapacityBySubject();
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;
                entity.StatisticLevelReportID = form.StatisticLevelReportID.Value;

                Stream excel = ClassifiedCapacityBySubjectBusiness.CreateClassifiedCapacityBySubject(entity);
                processedReport = ClassifiedCapacityBySubjectBusiness.InsertClassifiedCapacityBySubject(entity, excel);
                excel.Close();
            }
            #endregion

            #region  Thống kê tổng hợp xếp loại hoc luc hanh kiem  - minhh
            if (form.rdoReportBySemester == 12)
            {
                SynthesisCapacityConduct entity = new SynthesisCapacityConduct();
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;

                Stream excel = SynthesisCapacityConductBusiness.CreateSynthesisCapacityConduct(entity);
                processedReport = SynthesisCapacityConductBusiness.InsertSynthesisCapacityConduct(entity, excel);
                excel.Close();
            }
            #endregion

            #region  Thống kê tổng hợp xếp loại mon hoc theo khoi  - minhh
            if (form.rdoReportBySemester == 11)
            {
                SynthesisSubjectByEducationLevel entity = new SynthesisSubjectByEducationLevel();
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;
                entity.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                Stream excel = SynthesisSubjectByEducationLevelBusiness.CreateSynthesisSubjectByEducationLevel(entity);
                processedReport = SynthesisSubjectByEducationLevelBusiness.InsertSynthesisSubjectByEducationLevel(entity, excel);
                excel.Close();
            }
            #endregion

            #region  Thống kê danh hieu thi dua  - minhh
            if (form.rdoReportBySemester == 13)
            {
                EmulationTitle entity = new EmulationTitle();
                entity.Semester = form.Semester.Value;
                entity.AppliedLevel = global.AppliedLevel.Value;
                entity.SchoolID = global.SchoolID.Value;
                entity.AcademicYearID = global.AcademicYearID.Value;

                Stream excel = EmulationTitleBusiness.CreateEmulationTitle(entity);
                processedReport = EmulationTitleBusiness.InsertEmulationTitle(entity, excel);
                excel.Close();
            }
            #endregion


            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion

        public JsonResult AjaxLoadingSubject(int? check)
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
             dic["AppliedLevel"] = Global.AppliedLevel.Value ;
             if (check == 1)
             {
                 dic["IsCommenting"] = SystemParamsInFile.ISCOMMENTING_TYPE_MARK;
             }
             
            IQueryable<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(Global.AcademicYearID.Value,dic);
            if (check == 2)
            {
                listSchoolSubject = listSchoolSubject.Where(o=>o.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_MARK);
            }
            List<SubjectCat> lstClassSubject = listSchoolSubject.Select(o => o.SubjectCat).Distinct().ToList();
            lstClassSubject = lstClassSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            List<SubjectViewModel> lstSchoolSubjectModel = lstClassSubject
                                                            .Select(u => new SubjectViewModel
                                                            {
                                                                SubjectCatID = u.SubjectCatID,
                                                                DisplayName = u.DisplayName,
                                                                OrderInSubject = u.OrderInSubject,
                                                                IsCommenting = u.IsCommenting
                                                            }).Distinct()
                                                            .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName)
                                                            .ToList();
            ViewData[ReportResultLearningBySemesterConstants.List_SUBJECT] = lstSchoolSubjectModel;
            return Json(new SelectList(lstSchoolSubjectModel, "SubjectCatID", "DisplayName"));
        }

        public bool CheckIsCommentting(int? subjectID)
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = Global.AppliedLevel.Value;
            dic["SubjectID"] = subjectID;

            IQueryable<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(Global.AcademicYearID.Value, dic);
            int? ss = listSchoolSubject.FirstOrDefault().IsCommenting;
            if (ss == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                return false;
            else return true;
        }
    }
}
