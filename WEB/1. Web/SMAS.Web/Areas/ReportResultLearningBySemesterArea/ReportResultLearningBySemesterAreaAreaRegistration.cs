﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportResultLearningBySemesterArea
{
    public class ReportResultLearningBySemesterAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportResultLearningBySemesterArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportResultLearningBySemesterArea_default",
                "ReportResultLearningBySemesterArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
