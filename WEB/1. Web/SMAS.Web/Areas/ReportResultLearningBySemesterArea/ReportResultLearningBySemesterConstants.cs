﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportResultLearningBySemesterArea
{
    public class ReportResultLearningBySemesterConstants
    {
        public const string CBO_SEMESTER = "CboSemeseter";
        public const string CBO_PERIOD = "CboPeriod";
        public const string CBO_PERIOD_SELECTED = "CboPeriodSelected";
        public const string CBO_EDUCATIONLEVEL = "CBoEducationLevel";
        public const string CBO_ISCOMMENTING = "CboCommenting";
        public const string CBO_SUBJECT = "CboSubject";
        public const string List_SUBJECT = "List_SUBJECT";
        public const string LIST_RADIOREPORT = "ListRadioReport";
        public const string LIST_RADIOREPORTSEMESTER = "ListRadioReportSemester";
        public const string DEFAULT_SEMESTER = "DEFAULT_SEMESTER";
        public const string DEFAULT_STATSICTIC_LEVEL = "DEFAULT_STATSICTIC_LEVEL";
        public const string CBO_STATISTIC_LEVEL = "CBO_STATISTIC_LEVEL";
        public const string CBO_MARK_TYPE = "listMarkType";
        public const string CBO_MARK_COLUMN = "listMarkColumn";
        public const string CBO_EMPLOYEE = "cboEmployee";
        public const int MARK_TYPE_PERIODIC = 1; //Kiểm tra định kỳ
        public const int MARK_TYPE_AVERAGE = 2; //Trung bình môn
    }
}