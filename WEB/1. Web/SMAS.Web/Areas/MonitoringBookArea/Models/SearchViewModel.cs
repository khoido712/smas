/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Models.CustomAttribute;
namespace SMAS.Web.Areas.MonitoringBookArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("MonitoringBook_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public string EducationLevel { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public string Class { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_Fullname")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookConstants.LIST_FULLNAME)]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string Fullname { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookConstants.DATEFIRSTACAD)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.GREATER, "FirstDayOfSemester", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DataConstraint(DataConstraintAttribute.LESS, "ToDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? FromDate { get; set; }

         [ScaffoldColumn(false)]
         [ResourceDisplayName("MonitoringBook_Label_FirstDayOfSemester")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FirstDayOfSemester { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_ToDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookConstants.DATEENDACAD)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "EndDayOfSemester", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DataConstraint(DataConstraintAttribute.GREATER, "FromDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        
        public DateTime? ToDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("MonitoringBook_Label_EndDayOfSemester")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDayOfSemester { get; set; }
    }
}