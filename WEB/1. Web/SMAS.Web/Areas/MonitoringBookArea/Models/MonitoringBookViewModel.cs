/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Models.CustomAttribute;
namespace SMAS.Web.Areas.MonitoringBookArea.Models
{
    public class MonitoringBookViewModel
    {
         [ScaffoldColumn(false)]
        public System.Int32 MonitoringBookID { get; set; }
         [ScaffoldColumn(false)]				
		public System.Int32 AcademicYearID { get; set; }
         [ScaffoldColumn(false)]
		public System.Int32 SchoolID { get; set; }

         public int Create_PupilID { get; set; }
         
         [ScaffoldColumn(false)]
		public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_CreateDate")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [AdditionalMetadata("ViewDataKey", MonitoringBookConstants.CREATE_MONITORINGDATE)]
		public System.Nullable<System.DateTime> MonitoringDate { get; set; }

        [ScaffoldColumn(false)]
        public string MonitoringDateString { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_CreateDate")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [DataConstraint(DataConstraintAttribute.LESS, "FirstDayOfSemesterCreate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        //[AdditionalMetadata("ViewDataKey", MonitoringBookConstants.CREATE_MONITORINGDATE)]
        public System.Nullable<System.DateTime> MonitoringDateForCreate { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_EducationLevel")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", MonitoringBookConstants.LIST_EDUCATIONLEVEL)]
        //[AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String EducationLevelName { get; set; }
        [ResourceDisplayName("MonitoringBook_Label_Class")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", MonitoringBookConstants.CREATE_CLASSNAME)]
        //[AdditionalMetadata("Placeholder", "All")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Int32 ClassID { get; set; }
        [ResourceDisplayName("MonitoringBook_Label_Class")]
        
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        [ScaffoldColumn(false)]
        public System.String ClassName { get; set; }
        [ResourceDisplayName("MonitoringBook_Label_CreateFullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String FullName { get; set; }
        [ResourceDisplayName("MonitoringBook_Label_CreateFullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String FullNameEdit { get; set; }
        [ResourceDisplayName("MonitoringBook_Label_CreateSymptoms")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String Symptoms { get; set; }
        [ResourceDisplayName("MonitoringBook_Label_CreateSolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
		public System.String Solution { get; set; }
         [ScaffoldColumn(false)]
         [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
		public System.String UnitName { get; set; }
         [ScaffoldColumn(false)]
		public System.Nullable<System.Int32> MonitoringType { get; set; }
         [ScaffoldColumn(false)]
		public System.Nullable<System.Int32> HealthPeriodID { get; set; }
         [ScaffoldColumn(false)]
		public System.DateTime CreateDate { get; set; }
         [ScaffoldColumn(false)]
         [ResourceDisplayName("MonitoringBook_Label_EducationLevel")]
         [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
		public System.Int32 EducationLevelID { get; set; }
         [ScaffoldColumn(false)]
         public System.String PupilCode { get; set; }
         [ScaffoldColumn(false)]
         public System.Boolean AbleEdit { get; set; }

         [ScaffoldColumn(false)]
         [ResourceDisplayName("MonitoringBook_Label_FirstDayOfSemester")]
         [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
         public DateTime? FirstDayOfSemesterCreate { get; set; }
    }
}


