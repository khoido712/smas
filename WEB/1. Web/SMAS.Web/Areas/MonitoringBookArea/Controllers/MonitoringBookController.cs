﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MonitoringBookArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Web.Areas.MonitoringBookArea.Controllers
{
    public class MonitoringBookController : BaseController
    {
        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        public MonitoringBookController(IMonitoringBookBusiness monitoringbookBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IPupilProfileBusiness PupilProfileBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IEmployeeBusiness EmployeeBusiness)
        {
            this.MonitoringBookBusiness = monitoringbookBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        //
        // GET: /MonitoringBook/

        public ActionResult Index()
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime dateFirst = objAcademicYear.FirstSemesterStartDate.Value;

            ViewData[MonitoringBookConstants.DATEFIRSTACAD] = dateFirst;
            ViewData["objAcademicYear"] = objAcademicYear;

            if (_globalInfo.IsCurrentYear)
            {
                ViewData[MonitoringBookConstants.DATEENDACAD] = DateTime.Now.Date;
            }
            else
            {
                DateTime dateEnd = objAcademicYear.SecondSemesterEndDate.Value;
                ViewData[MonitoringBookConstants.DATEENDACAD] = dateEnd;
            }
            int classID;
            if (int.TryParse(Request.QueryString["ClassID"], out classID))
            {
                ClassProfile selectedClass = this.ClassProfileBusiness.Find(classID);
                if (selectedClass != null)
                {
                    ViewData["SELECTED_CLASSID"] = selectedClass.ClassProfileID;
                    ViewData["EDUCATION_OF_SELECTED_CLASSID"] = selectedClass.EducationLevelID;
                }
            }

            //Get view data here
            ViewData[MonitoringBookConstants.GRID_EMPTY] = false;
            setViewData();
            ViewData[MonitoringBookConstants.PERMISSION_ALLOW] = _globalInfo.IsCurrentYear;
            return View();
        }

        //
        // GET: /MonitoringBook/Search

        public void setViewData()
        {
            //-	cboEducationLevel: UserInfo.EducationLevels
            if (_globalInfo.EducationLevels.Count > 0)
            {
                ViewData[MonitoringBookConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[MonitoringBookConstants.LIST_EDUCATIONLEVEL] = new SelectList(new string[] { });
            }

            List<string> lstFullName = new List<string>();
            lstFullName.Add("");
            ViewData[MonitoringBookConstants.CREATE_FULLNAME] = lstFullName;
            ViewData[MonitoringBookConstants.LIST_CLASS] = new SelectList(new string[] { });
            //ViewData[MonitoringBookConstants.USER_PERMISSION] = GetPermissionOfUser();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
        }
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(global.AcademicYearID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //add search info
            if (global.AppliedLevel.HasValue)
            {
                SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            }
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["ClassID"] = frm.Class;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["FromMonitoringDate"] = frm.FromDate.HasValue ? frm.FromDate : objAcademicYear.FirstSemesterStartDate;
            SearchInfo["ToMonitoringDate"] = frm.ToDate.HasValue ? frm.ToDate : objAcademicYear.SecondSemesterEndDate;

            IEnumerable<MonitoringBookViewModel> lst = this._Search(SearchInfo);
            ViewData[MonitoringBookConstants.LIST_MONITORINGBOOK] = lst.OrderByDescending(o => o.CreateDate).ToList();

            //Get view data here
            ViewData[MonitoringBookConstants.GRID_EMPTY] = lst.Count() > 0;
            ViewData[MonitoringBookConstants.PERMISSION_ALLOW] = _globalInfo.IsCurrentYear;
            //ViewData[MonitoringBookConstants.USER_PERMISSION] = GetPermissionOfUser();    
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(DateTime? MonitoringBookIDCreate)
        {
            if (!CheckActionPermissionMinAddReturn())
            {
                return Json(new JsonMessage("Thầy/cô chưa được phân quyền ở chức năng này."));
            }

            GlobalInfo global = new GlobalInfo();
            MonitoringBookViewModel monitoringbook = new MonitoringBookViewModel();
            TryUpdateModel(monitoringbook);
            Utils.Utils.TrimObject(monitoringbook);
            monitoringbook.MonitoringDateForCreate = MonitoringBookIDCreate.Value;
            monitoringbook.AcademicYearID = global.AcademicYearID.Value;
            monitoringbook.SchoolID = global.SchoolID.Value;
            string strFullName = monitoringbook.FullName;
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime SecondEndDate = academicYear.SecondSemesterEndDate.Value;
            DateTime FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime FirstStartdate = academicYear.FirstSemesterStartDate.Value;
            if (Convert.ToDateTime(MonitoringBookIDCreate.Value) < FirstStartdate || Convert.ToDateTime(MonitoringBookIDCreate.Value) > SecondEndDate)
            {
                string msg = string.Format(Res.Get("Validate_Date_MonitoringBook"), FirstStartdate.ToShortDateString(), SecondEndDate.ToShortDateString());
                return Json(new JsonMessage(msg, "error"));
            }

            //if (str[1].Equals(null)) {
            //    return Json(new JsonMessage(Res.Get("Common_Label_ErrorAddNewMessage")));
            //}
            int pupilProfileID = monitoringbook.Create_PupilID;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("CurrentClassID", monitoringbook.ClassID);
            dic.Add("EducationLevelID", monitoringbook.EducationLevelID);
            PupilProfile pupilProfileOBJ = PupilProfileBusiness.Find(pupilProfileID);
            if (pupilProfileOBJ != null)
            {
                monitoringbook.PupilID = pupilProfileOBJ.PupilProfileID;
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ErrorAddNewMessage")));
            }
            //monitoringbook.ClassID = cboClass;
            //monitoringbook.PupilID = hdfPupilID; // (set giá trị khi nhập tên bằng Autocomplete)
            //monitoringbook.MonitoringDate = dtpMonitoringDate;
            //monitoringbook.EducationLevelID = cbo EducationLevel;
            monitoringbook.CreateDate = DateTime.Now;
            //monitoringbook.Symptoms = txtSymptoms;
            //monitoringbook.Solution = txtSolution;
            monitoringbook.MonitoringType = 1; //: TYPE_DATE;

            MonitoringBook mbInsert = new MonitoringBook();
            mbInsert.AcademicYearID = monitoringbook.AcademicYearID;
            mbInsert.SchoolID = monitoringbook.SchoolID;
            mbInsert.ClassID = monitoringbook.ClassID;
            mbInsert.PupilID = monitoringbook.PupilID;
            mbInsert.MonitoringDate = monitoringbook.MonitoringDateForCreate.Value;
            mbInsert.EducationLevelID = monitoringbook.EducationLevelID;
            mbInsert.CreateDate = DateTime.Now;
            mbInsert.Symptoms = monitoringbook.Symptoms;
            mbInsert.Solution = monitoringbook.Solution;
            mbInsert.MonitoringType = 1;
            this.MonitoringBookBusiness.Insert(mbInsert);
            this.MonitoringBookBusiness.Save();
           
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int MonitoringBookID, string FullNameEdit)
        {
            if (!CheckActionPermissionMinAddReturn())
            {
                return Json(new JsonMessage("Thầy/cô chưa được phân quyền ở chức năng này."));
            }

            MonitoringBook monitoringbook = this.MonitoringBookBusiness.Find(MonitoringBookID);

            TryUpdateModel(monitoringbook);
            Utils.Utils.TrimObject(monitoringbook);
            this.MonitoringBookBusiness.Update(monitoringbook);
            this.MonitoringBookBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            if (!CheckActionPermissionMinAddReturn())
            {
                return Json(new JsonMessage("Thầy/cô chưa được phân quyền ở chức năng này."));
            }

            this.MonitoringBookBusiness.Delete(id);
            this.MonitoringBookBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<MonitoringBookViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            int? schoolID = _globalInfo.SchoolID;
            IQueryable<MonitoringBook> query = this.MonitoringBookBusiness.SearchBySchool(schoolID.Value, SearchInfo).Where(o => o.MonitoringType == 1);
            IQueryable<MonitoringBookViewModel> lst = query.Select(o => new MonitoringBookViewModel
            {
                MonitoringBookID = o.MonitoringBookID,
                AcademicYearID = o.AcademicYearID,
                SchoolID = o.SchoolID,
                ClassID = o.ClassID,
                PupilID = o.PupilID,
                MonitoringDate = o.MonitoringDate,
                Symptoms = o.Symptoms,
                Solution = o.Solution,
                UnitName = o.UnitName,
                MonitoringType = o.MonitoringType,
                HealthPeriodID = o.HealthPeriodID,
                CreateDate = o.CreateDate,
                EducationLevelID = o.EducationLevelID,
                FullName = o.PupilProfile.FullName,
                PupilCode = o.PupilProfile.PupilCode,
            });

            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int roleOfUser = GetMenupermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<MonitoringBookViewModel> lst1 = lst.ToList();
            MonitoringBookViewModel item = null;
            List<MonitoringBookViewModel> nList = new List<MonitoringBookViewModel>();
            for (int i = 0, templst1 = lst1.Count; i < templst1; i++)
            {
                item = lst1[i];
                item.MonitoringDateString = item.MonitoringDate.Value.ToString("dd/MM/yyyy");
                item.ClassName = ClassProfileBusiness.Find(item.ClassID).DisplayName;
                if (item.MonitoringDate != null)
                {
                    DateTime dt1 = item.MonitoringDate.Value;
                    DateTime dt2 = item.CreateDate;
                    TimeSpan d = dt1.Subtract(dt2);
                    int days = d.Days;
                    int hours = d.Hours;
                    if (days >= 2 && hours > 0)
                    {
                        item.AbleEdit = false;
                    }
                    else
                    {
                        if (roleOfUser > SystemParamsInFile.PER_VIEW)
                        item.AbleEdit = true;
                        else item.AbleEdit = false;
                    }
                }
                else
                {
                    item.AbleEdit = false;
                }
                nList.Add(item);
            }
            return nList;
        }

        #region Load Data by Ajax      
        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID", _globalInfo.AcademicYearID.Value}
                };
            return ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            IQueryable<ClassProfile> lsCP = null;
            if (EducationLevelID.HasValue)
            {
                lsCP = getClassFromEducationLevel(EducationLevelID.Value);
            }
            if (lsCP != null && lsCP.Count() != 0)
            {
                return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
            }
            else
            {
                return Json(new SelectList(new string[] {}));
            }
        }
        #endregion

        #region Detail

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult loadViewDialog(int? id)
        {
            MonitoringBookViewModel mbv = new MonitoringBookViewModel();
            MonitoringBook mb = MonitoringBookBusiness.Find(id);
            mbv.MonitoringDate = mb.MonitoringDate;
            mbv.Symptoms = mb.Symptoms;
            mbv.Solution = mb.Solution;
            mbv.ClassName = ClassProfileBusiness.Find(mb.ClassID).DisplayName;
            mbv.FullName = mb.PupilProfile.FullName + " - " + mb.PupilProfile.PupilCode;
            return PartialView("_LinkView", mbv);
        }

        #endregion Detail

        //[ValidateAntiForgeryToken]
        //public PartialViewResult AjaxLoadPupil(int? classID)
        //{
        //    IDictionary<string, object> dic = new Dictionary<string, object>();
        //    dic["ClassID"] = classID.Value;
        //    //IEnumerable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic);
        //    List<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).ToList();

        //    List<string> listName = new List<string>();
        //    PupilOfClass pupil = null;
        //    for (int i = 0, tempPupil = listPupil.Count(); i < tempPupil; i++)
        //    {
        //        pupil = listPupil[i];
        //        //}
        //        //foreach (PupilOfClass pupil in listPupil)
        //        //{
        //        string namePupil = pupil.PupilProfile.FullName + " - " + pupil.PupilProfile.PupilProfileID;
        //        listName.Add(namePupil);

        //    }

        //    ViewData[MonitoringBookConstants.CREATE_FULLNAME] = listName;
        //    //ViewData[MonitoringBookConstants.LIST_PUPIL_OF_CLASS] = listPupil;
        //    return PartialView("_FullNameCreate");
        //}

        //[ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadPupil(int? EducationLevelID, int? ClassID)
        {
            ViewData[MonitoringBookConstants.CREATE_FULLNAME] = getAutoCompletePupilData(EducationLevelID, ClassID);
            return PartialView("_FullNameCreate");
        }

        private List<string> getAutoCompletePupilData(int? EducationLevelID, int? ClassID)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<PupilOfClass> iqPoC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"ClassID",ClassID}
            });
            List<string> lsCO = new List<string>();
            string fullName = string.Empty;
            foreach (var o in iqPoC)
            {
                fullName = o.PupilProfile.FullName + "<span style=\"display:none;\">©" + o.PupilProfile.PupilProfileID + "</span>";
                lsCO.Add(fullName);
            }
            return lsCO;
        }

        #region export Excel

        public FileResult ExportExcel(int? EducationLevel, int? Class, string Fullname, string FromDateStr, string ToDateStr)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", MonitoringBookConstants.TEMPLATE_THEODOIHANGNGAY);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTRange templateRange = sheet.GetRange("A1", "G75");

            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            List<object> QueenDic = new List<object>();

            //Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo gi = new GlobalInfo();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //add search info
            //SearchInfo["AcademicYearID"] = gi.AcademicYearID.Value;
            //SearchInfo["AppliedLevel"] = gi.AppliedLevel.Value;
            //SearchInfo["SchoolID"] = gi.SchoolID.Value;
            if (_globalInfo.AppliedLevel.HasValue)
            {
                SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            }
            SearchInfo["EducationLevelID"] = EducationLevel;
            SearchInfo["ClassID"] = Class;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            SearchInfo["FullName"] = Fullname;
            if (string.IsNullOrEmpty(FromDateStr))
            {
                SearchInfo["FromMonitoringDate"] = objAcademicYear.FirstSemesterStartDate;
            }
            else
            {
                DateTime FromDate = Convert.ToDateTime(FromDateStr);
                SearchInfo["FromMonitoringDate"] = FromDate;
            }
            if (string.IsNullOrEmpty(ToDateStr))
            {
                SearchInfo["ToMonitoringDate"] = objAcademicYear.SecondSemesterEndDate;
            }
            else
            {
                DateTime ToDate = Convert.ToDateTime(ToDateStr);                
                SearchInfo["ToMonitoringDate"] = ToDate;
            }

            List<MonitoringBookViewModel> lst = this._Search(SearchInfo).OrderByDescending(o=>o.CreateDate).ToList();
            for (int i = 0; i < lst.Count(); i++)
            {
                MonitoringBookViewModel thisModel = lst[i];
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["FullName"] = thisModel.FullName;
                princeDic["MonitoringDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.MonitoringDate);

                princeDic["Class"] = thisModel.ClassName;
                princeDic["Symptoms"] = (thisModel.Symptoms != null) ? thisModel.Symptoms.Trim() : "";
                princeDic["Solution"] = (thisModel.Solution != null) ? thisModel.Solution.Trim() : "";

                QueenDic.Add(princeDic);
            }

            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string year = "Năm học {0} - {1}";
            string date = sp.Province.ProvinceName + ", ngày {0} tháng {1} năm {2}";
            string dateTitle = string.Format(date, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string yearTitle = string.Format(year, ay.Year, (ay.Year + 1));
            EducationLevel el = EducationLevel != null ? EducationLevelBusiness.Find(EducationLevel) : null;
            ClassProfile cp = Class != null ? ClassProfileBusiness.Find(Class) : null;
            string templateTitle = "SỔ THEO DÕI SỨC KHOẺ HÀNG NGÀY";
            string labelForEducation = ((GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                                            || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN))
                                            ? Res.Get("PupilProfile_Label_EducationLevel_MN") : Res.Get("MonitoringBook_Label_EducationLevel");
            string templateTitle_1 = labelForEducation.ToUpper() + ": {0} - LỚP: {1}";

            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SupervisingDeptName", sp.SupervisingDept.SupervisingDeptName);
            KingDic.Add("SchoolName", sp.SchoolName);
            KingDic.Add("TitleYear", yearTitle);
            KingDic.Add("ProvinceNameAndDate", dateTitle);
            KingDic.Add("TitleTemplate", templateTitle);
            KingDic.Add("TitleTemplate_1", string.Format(templateTitle_1, (el != null) ? el.Resolution.ToUpper() : "TẤT CẢ", (cp != null) ? cp.DisplayName.ToUpper() : "TẤT CẢ"));

            templateRange.FillVariableValue(KingDic);

            Stream excel = oBook.ToStream();
            String temp = "";
            if (_globalInfo.AppliedLevel == 1) {
                temp = "TH";
            }
            else if (_globalInfo.AppliedLevel == 2)
            {
                temp = "THCS";
            }
            else if (_globalInfo.AppliedLevel == 3)
            {
                temp = "THPT";
            }
            else if (_globalInfo.AppliedLevel == 4)
            {
                temp = "NT";
            }
            else if (_globalInfo.AppliedLevel == 5)
            {
                temp = "MG";
            }
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = string.Format("HS_" + MonitoringBookConstants.TEMPLATE_THEODOIHANGNGAY_FORMAT, 
                (el != null && cp == null) ? "_" + Utils.Utils.StripVNSign(el.Resolution).Replace(" ", "") : "", 
                (cp != null) ? "_" + Utils.Utils.StripVNSign(cp.DisplayName).Replace(" ", "") : "", 
                sp.SchoolName.Replace(" ", "_")).Replace("Template", temp);
            return result;
        }

        #endregion export Excel


        #region Role permission
        /// <summary>
        /// Lấy quyền của người dùng
        /// </summary>
        /// <returns></returns>
        private int GetPermissionOfUser()
        {
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsEmployeeManager)
                return SystemParamsInFile.PER_DELETE;
            else
            {
                // Kiem tra co la Nhan vien Y te
                Employee emp = EmployeeBusiness.Find(_globalInfo.EmployeeID);
                if (emp.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE &&
                        emp.WorkType.Resolution.ToLower().Equals(MonitoringBookConstants.EMPLOYEE_MEDICAL.ToLower()))
                    return SystemParamsInFile.PER_DELETE;

                // Nếu ko chỉ có quyền xem/xuất excel
                return SystemParamsInFile.PER_VIEW;
            }
        }
        #endregion
    }
}





