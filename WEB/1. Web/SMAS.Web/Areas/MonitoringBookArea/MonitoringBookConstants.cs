﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.MonitoringBookArea
{
    public class MonitoringBookConstants
    {
        public const string LIST_MONITORINGBOOK = "listMonitoringBook";
        // For SearchViewModel
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_FULLNAME = "LIST_FULLNAME";
        public const string LIST_FROMDATE = "LIST_FROMDATE";
        public const string LIST_TODATE = "LIST_TODATE";
        // For Create View
        public const string CREATE_MONITORINGDATE = "CREATE_MONITORINGDATE";
        public const string CREATE_EDUCATIONLEVELNAME = "CREATE_EDUCATIONLEVELNAME";
        public const string CREATE_CLASSNAME = "CREATE_CLASSNAME";
        public const string CREATE_FULLNAME = "CREATE_FULLNAME";
        public const string GRID_EMPTY = "GRID_EMPTY";
        public const string TEMPLATE_THEODOIHANGNGAY = "Template_Theodoiskhangngay.xls";
        public const string LIST_PUPIL_OF_CLASS = "LIST_PUPIL_OF_CLASS";

        public const string TEMPLATE_THEODOIHANGNGAY_FORMAT = "Template_TheoDoiSKHangNgay{0}{1}.xls";
        public const string DATEFIRSTACAD = "DateFirstAcad";
        public const string DATEENDACAD = "DateEndAcad";
        public const string PERMISSION_ALLOW = "PERMISSION_ALLOW";

        public const string USER_PERMISSION = "permissiom of user";
        public const string EMPLOYEE_MEDICAL = "Nhân viên Y Tế";

    }
}