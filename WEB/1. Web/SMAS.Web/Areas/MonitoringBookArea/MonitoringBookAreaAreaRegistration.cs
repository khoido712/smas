﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MonitoringBookArea
{
    public class MonitoringBookAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MonitoringBookArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MonitoringBookArea_default",
                "MonitoringBookArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
