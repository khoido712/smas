﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.DefaultGroupMenuArea.Controllers
{   
    public class DefaultGroupMenuController : BaseController
    {        
        private readonly IDefaultGroupMenuBusiness DefaultGroupMenuBusiness;
		
		public DefaultGroupMenuController (IDefaultGroupMenuBusiness defaultgroupmenuBusiness)
		{
			this.DefaultGroupMenuBusiness = defaultgroupmenuBusiness;
		}
		
		//
        // GET: /DefaultGroupMenu/

        public ViewResult Index()
        {
            
			return View(this.DefaultGroupMenuBusiness.All.ToList());
        }

        //
        // GET: /DefaultGroupMenu/Details/5

        public ViewResult Details(int id)
        {            
            DefaultGroupMenu defaultgroupmenu = this.DefaultGroupMenuBusiness.Find(id);
			return View(defaultgroupmenu);
        }

        //
        // GET: /DefaultGroupMenu/Create
        
        public ActionResult Create()
        {

            return View();
        } 

        //
        // POST: /DefaultGroupMenu/Create

        [HttpPost]
		[ValidateAntiForgeryToken]
        
        public ActionResult Create(DefaultGroupMenu defaultgroupmenu)
        {
            if (ModelState.IsValid)
            {				
				this.DefaultGroupMenuBusiness.Insert(defaultgroupmenu);
                return RedirectToAction("Index");  
            }


            return View(defaultgroupmenu);
        }
        
        //
        // GET: /DefaultGroupMenu/Edit/5
        
        public ActionResult Edit(int id)
        {
            DefaultGroupMenu defaultgroupmenu = this.DefaultGroupMenuBusiness.Find(id);

            return View(defaultgroupmenu);
        }

        //
        // POST: /DefaultGroupMenu/Edit/5

        [HttpPost]
		[ValidateAntiForgeryToken]
        
        public ActionResult Edit(DefaultGroupMenu defaultgroupmenu)
        {
            if (ModelState.IsValid)
            {
				this.DefaultGroupMenuBusiness.Update(defaultgroupmenu);                
                return RedirectToAction("Index");
            }

            return View(defaultgroupmenu);
        }

        //
        // GET: /DefaultGroupMenu/Delete/5
        

        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            DefaultGroupMenu defaultgroupmenu = this.DefaultGroupMenuBusiness.Find(id);
            return View(defaultgroupmenu);
        }

        //
        // POST: /DefaultGroupMenu/Delete/5

        [HttpPost, ActionName("Delete")]		

        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this.DefaultGroupMenuBusiness.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
