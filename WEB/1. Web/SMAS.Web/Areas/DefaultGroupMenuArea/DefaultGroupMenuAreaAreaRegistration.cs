﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DefaultGroupMenuArea
{
    public class DefaultGroupMenuAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DefaultGroupMenuArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DefaultGroupMenuArea_default",
                "DefaultGroupMenuArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
