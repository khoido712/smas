﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Web.Areas.HealthTestReportForSchoolArea.Models;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.HealthTestReportForSchoolArea.Controllers
{
    public class HealthTestReportForSchoolController : BaseController
    {

        private readonly IHealthTestReportBusiness HealthTestReportBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public HealthTestReportForSchoolController(
                                               IHealthTestReportBusiness HealthTestReportBusiness,
                                               IAcademicYearBusiness AcademicYearBusiness,
                                               IProcessedReportBusiness processedreportBusiness,
                                               IReportDefinitionBusiness reportdefinitionBusiness)
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.HealthTestReportBusiness = HealthTestReportBusiness;
            this.ProcessedReportBusiness = processedreportBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
        }


        private void SetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            // load combobox EducationLevel

            ViewData[HealthTestReportForSchoolConstants.LIST_EDUCATIONLEVEL] = new SelectList(glo.EducationLevels, "EducationLevelID", "Resolution");
            

            //load combobox Period
            List<ComboObject> lstPeriod = new List<ComboObject>();
            lstPeriod.Add(new ComboObject("1", SMAS.Web.Constants.GlobalConstants.PERIOD_EXAMINATION_1));
            lstPeriod.Add(new ComboObject("2", SMAS.Web.Constants.GlobalConstants.PERIOD_EXAMINATION_2));
            ViewData[HealthTestReportForSchoolConstants.LIST_Period] = new SelectList(lstPeriod, "key", "value", 1);


            // list radio button
            List<ViettelCheckboxList> ListType = new List<ViettelCheckboxList>();
            ListType.Add(new ViettelCheckboxList(
                Res.Get("HealthTestReport_Label_ReportType_Health"),
                "1",
                false,
                false
            ));
            ListType.Add(new ViettelCheckboxList(
                Res.Get("HealthTestReport_Label_ReportType_Eye"),
                "2",
                false,
                false
            ));
            ViewData[HealthTestReportForSchoolConstants.LIST_REPORTTYPE] = ListType;


        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetReport(HealthTestReportForSchoolViewModel form)
        {

            ExcelHealthTestReportBO ExcelHealthTestReportBO = new ExcelHealthTestReportBO();

            ExcelHealthTestReportBO.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            ExcelHealthTestReportBO.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            ExcelHealthTestReportBO.EducationLevelID = form.EducationLevelID == null ? 0 : form.EducationLevelID.Value;
            ExcelHealthTestReportBO.HealthPeriod = form.HealthPeriod.Value;
            ExcelHealthTestReportBO.SchoolID = new GlobalInfo().SchoolID.Value;


            string reportCode = "";

            if (form.ReportType.Value == 1)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_TRUONG;
            }

            if (form.ReportType.Value == 2)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_MAT;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = HealthTestReportBusiness.GetHealthTestReportForSchool(ExcelHealthTestReportBO,form.ReportType.Value);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = HealthTestReportBusiness.CreateHealthTestReportForSchool(ExcelHealthTestReportBO, form.ReportType.Value);
                processedReport = HealthTestReportBusiness.InsertHealthTestReportForSchool(ExcelHealthTestReportBO, excel, form.ReportType.Value);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(HealthTestReportForSchoolViewModel form)
        {
            
            ExcelHealthTestReportBO ExcelHealthTestReportBO = new ExcelHealthTestReportBO();

            ExcelHealthTestReportBO.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            ExcelHealthTestReportBO.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            ExcelHealthTestReportBO.EducationLevelID = form.EducationLevelID == null ? 0 : form.EducationLevelID.Value;
            ExcelHealthTestReportBO.HealthPeriod = form.HealthPeriod.Value;
            ExcelHealthTestReportBO.SchoolID = new GlobalInfo().SchoolID.Value;


                Stream excel = HealthTestReportBusiness.CreateHealthTestReportForSchool(ExcelHealthTestReportBO, form.ReportType.Value);
                ProcessedReport processedReport = HealthTestReportBusiness.InsertHealthTestReportForSchool(ExcelHealthTestReportBO, excel, form.ReportType.Value);
                excel.Close(); 

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

    }
}
