﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;

namespace SMAS.Web.Areas.HealthTestReportForSchoolArea.Models
{
    public class HealthTestReportForSchoolViewModel 
    {
       

        [ResourceDisplayName("HealthTestReport_Label_Period")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", HealthTestReportForSchoolConstants.LIST_Period)]
        public Nullable<int> HealthPeriod { get; set; }


        [ResourceDisplayName("HealthTestReport_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", HealthTestReportForSchoolConstants.LIST_EDUCATIONLEVEL)]
        public Nullable<int> EducationLevelID { get; set; }


        [ResourceDisplayName("")]
        [UIHint("ListRadio")]
        [AdditionalMetadata("ViewDataKey", HealthTestReportForSchoolConstants.LIST_REPORTTYPE)]
        public Nullable<int> ReportType { get; set; }

    }
}
