﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HealthTestReportForSchoolArea
{
    public class HealthTestReportForSchoolAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HealthTestReportForSchoolArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HealthTestReportForSchoolArea_default",
                "HealthTestReportForSchoolArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
