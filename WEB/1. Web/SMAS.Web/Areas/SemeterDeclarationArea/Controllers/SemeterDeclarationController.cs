﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Web.Areas.SemeterDeclarationArea.Models;
using SMAS.Business.Common;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.SemeterDeclarationArea.Controllers
{
    public class SemeterDeclarationController : BaseController
    {
        #region bien,tham so
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness; //business hoc ky
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBusiness;    //business khoa con diem
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        public const int DEFAULT_COEFFICIENTINTERVIEW = 1;
        public const int DEFAULT_COEFFICIENTWRITING = 1;
        public const int DEFAULT_COEFFICIENTTWICE = 2;
        public const int DEFAULT_COEFFICIENTSEMESTER = 3;
        public const int DEFAULT_INTERVIEWMARK = 5;
        public const int DEFAULT_WRITINGMARK = 5;
        public const int DEFAULT_TWICECOEFFIECIENTMARK = 5;
        public const int DEFAULT_SEMESTERMARK = 1;

        #endregion

        #region ham khoi tao
        public SemeterDeclarationController(ISemeterDeclarationBusiness semeterdeclarationBusiness, ILockedMarkDetailBusiness LockedMarkDetailBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , IAcademicYearBusiness AcademicYearBusiness,
            IPeriodDeclarationBusiness PeriodDeclarationBusiness)
        {
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.SemeterDeclarationBusiness = semeterdeclarationBusiness;
            this.LockedMarkDetailBusiness = LockedMarkDetailBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        #endregion

        #region index
        //
        // GET: /SemeterDeclaration/

        public ActionResult Index()
        {
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                if (_globalInfo.ProductVersionID == GlobalConstants.PRODUCT_VERSION_ID_FULL)
                {
                    return Redirect("/CodeConfigArea/CodeConfig/Index");
                }
                else if (_globalInfo.ProductVersionID == GlobalConstants.PRODUCT_VERSION_ID_ADVANCE)
                {
                    return Redirect("/ConductConfigArea/ConductConfig/Index");
                }

            }
            SetViewData();


            SetViewDataPermission("SemeterDeclaration", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return View();
        }

        #endregion

        #region old

        //#region _SelectAjaxEditing
        /////// <summary>
        /////// ham khoi tao truyen gia tri mac dinh cho grid khi load page
        /////// </summary>
        /////// <returns>list du lieu dua vao grid</returns>
        ////[GridAction]
        ////public ActionResult _SelectAjaxEditing()
        ////{

        ////    SetViewData();

        ////    IQueryable<SemeterDeclaration> listPeriod = (IQueryable<SemeterDeclaration>)ViewData[SemeterDeclarationConstant.LS_SEMETERDECLARATION];

        ////    // Convert du lieu sang doi tuong SupervisingDeptForm
        ////    IQueryable<SemeterDeclarationObject> res = ConvertToObject(listPeriod);
        ////    return View(new GridModel(res.ToList()));
        ////}


        //#endregion

        //#region _SaveAjaxEditing
        /////// <summary>
        /////// ham xu ly su kien nguoi dung an nut edit
        /////// </summary>
        /////// <param name="id">id cua doi tuong can edit</param>
        /////// <returns></returns>
        ////[AcceptVerbs(HttpVerbs.Post)]
        ////[GridAction]
        ////public ActionResult _SaveAjaxEditing(int id)
        ////{

        ////    //object new
        ////    SemeterDeclarationObject SemeterDeclarationTest = new SemeterDeclarationObject();
        ////    //truyen du lieu tren form vao SemeterDeclarationObject
        ////    if (TryUpdateModel(SemeterDeclarationTest))
        ////    {
        ////        //lay du lieu tu session do vao obj
        ////        GlobalInfo globalInfo = new GlobalInfo();

        ////        int? SchoolID = globalInfo.SchoolID;
        ////        int? AcademicYearID = globalInfo.AcademicYearID;
        ////        //periodObject.SchoolID = (int?)Session["SchoolID"];
        ////        //periodObject.AcademicYearID = (int)Session["AcademicYearID"];

        ////        SemeterDeclarationTest.SchoolID = SchoolID.Value;
        ////        SemeterDeclarationTest.AcademicYearID = AcademicYearID.Value;
        ////        SemeterDeclarationTest.SemeterDeclarationID = id;
        ////        AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID);
        ////        SemeterDeclarationTest.Year = ay.Year;

        ////        if (id != 0)
        ////        {
        ////            //du lieu hop le thi convert data va update
        ////            SemeterDeclaration periodModel = SemeterDeclarationBusiness.Find(id);
        ////            Utils.Utils.BindTo(SemeterDeclarationTest, periodModel);
        ////            //Update
        ////            this.SemeterDeclarationBusiness.Update(periodModel);
        ////            this.SemeterDeclarationBusiness.Save();
        ////        }
        ////        else
        ////        {
        ////            //du lieu hop le thi convert data va update
        ////            SemeterDeclaration periodModel = new SemeterDeclaration();
        ////            Utils.Utils.BindTo(SemeterDeclarationTest, periodModel);
        ////            //Update
        ////            this.SemeterDeclarationBusiness.Insert(periodModel);
        ////            this.SemeterDeclarationBusiness.Save();
        ////        }

        ////    }
        ////    else
        ////    {
        ////        throw new BusinessException("Common_Error_DataInputError");
        ////    }


        ////    SetViewData();
        ////    IQueryable<SemeterDeclaration> listPeriod = (IQueryable<SemeterDeclaration>)ViewData[SemeterDeclarationConstant.LS_SEMETERDECLARATION];
        ////    // Convert du lieu sang doi tuong SupervisingDeptForm
        ////    IQueryable<SemeterDeclarationObject> res = ConvertToObject(listPeriod);
        ////    return View(new GridModel(res.ToList()));

        ////}


        //#endregion


        //#region Convert from List model to List view model
        ////private List<SemeterDeclarationObject> Convert(List<SemeterDeclaration> lsSemeterDeclaration)
        ////{
        ////    #region convert to view model
        ////    //tu lsSemeterDeclaration dua ra ngoai man hinh,9 phan tu co dinh
        ////    SemeterDeclaration hk1 = lsSemeterDeclaration.Where(o => o.Semester == 1).FirstOrDefault();
        ////    SemeterDeclaration hk2 = lsSemeterDeclaration.Where(o => o.Semester == 2).FirstOrDefault();

        ////    List<SemeterDeclarationObject> lsSDO = new List<SemeterDeclarationObject>();
        ////    //diem mieng
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.DIEM_MIENG;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_InterviewMark");
        ////        sdo.SemeterDeclarationValueHK1 = hk1.InterviewMark;
        ////        sdo.SemeterDeclarationValueHK2 = hk2.InterviewMark;
        ////        lsSDO.Add(sdo);
        ////    }
        ////    //hs diem mieng
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.HS_DIEM_MIENG;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_CoefficientInterview");
        ////        sdo.SemeterDeclarationValueHK1 = hk1.CoefficientInterview;
        ////        sdo.SemeterDeclarationValueHK2 = hk2.CoefficientInterview;
        ////        lsSDO.Add(sdo);
        ////    }

        ////    //diem 15
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.DIEM_15;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_WritingMark");
        ////        sdo.SemeterDeclarationValueHK1 = hk1.WritingMark;
        ////        sdo.SemeterDeclarationValueHK2 = hk2.WritingMark;
        ////        lsSDO.Add(sdo);
        ////    }
        ////    //hs diem 15
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.HS_DIEM_15;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_CoefficientWriting");
        ////        sdo.SemeterDeclarationValueHK1 = hk1.CoefficientWriting;
        ////        sdo.SemeterDeclarationValueHK2 = hk2.CoefficientWriting;
        ////        lsSDO.Add(sdo);
        ////    }

        ////    //diem 1t
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.DIEM_1T;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_TwiceCoefficientWritingMark");
        ////        sdo.SemeterDeclarationValueHK1 = hk1.TwiceCoeffiecientMark;
        ////        sdo.SemeterDeclarationValueHK2 = hk2.TwiceCoeffiecientMark;
        ////        lsSDO.Add(sdo);
        ////    }
        ////    //hs diem 1t
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.HS_DIEM_1T;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_CoefficientTwice");
        ////        sdo.SemeterDeclarationValueHK1 = hk1.CoefficientTwice;
        ////        sdo.SemeterDeclarationValueHK2 = hk2.CoefficientTwice;
        ////        lsSDO.Add(sdo);
        ////    }

        ////    //diem hk
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.DIEM_HK;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_SemesterMark");
        ////        sdo.SemeterDeclarationValueHK1 = hk1.SemesterMark;
        ////        sdo.SemeterDeclarationValueHK2 = hk2.SemesterMark;
        ////        lsSDO.Add(sdo);
        ////    }
        ////    //hs diem hk
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.HS_DIEM_HK;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_CoefficientSemester");
        ////        sdo.SemeterDeclarationValueHK1 = hk1.CoefficientSemester;
        ////        sdo.SemeterDeclarationValueHK2 = hk2.CoefficientSemester;
        ////        lsSDO.Add(sdo);
        ////    }

        ////    //khoa
        ////    {
        ////        SemeterDeclarationObject sdo = new SemeterDeclarationObject();
        ////        sdo.SemeterDeclarationKind = SemeterDeclarationConstant.KHOA;
        ////        sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_Discontinued");
        ////        sdo.SemeterDeclarationLockHK1 = hk1.IsLock.HasValue ? hk1.IsLock.Value : false;
        ////        sdo.SemeterDeclarationLockHK2 = hk2.IsLock.HasValue ? hk2.IsLock.Value : false;
        ////        lsSDO.Add(sdo);
        ////    }
        //    //#endregion

        //    //return lsSDO;
        //}
        //#endregion

        //#region check in database
        ////private List<SemeterDeclaration> CheckInDatabase()
        ////{
        ////    GlobalInfo globalInfo = new GlobalInfo();
        ////    List<SemeterDeclaration> lsSemeterDeclaration = SemeterDeclarationBusiness.All
        ////       .Where(o => (o.SchoolID == globalInfo.SchoolID.Value))
        ////       .Where(o => (o.AcademicYearID == globalInfo.AcademicYearID.Value)).ToList()
        ////       ;
        ////    if (lsSemeterDeclaration.Count() != 2)
        ////    {
        ////        //xoa truoc
        ////        SemeterDeclarationBusiness.DeleteAll(lsSemeterDeclaration.ToList());
        ////        AcademicYear ay = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);

        ////        SemeterDeclaration temp1 = new SemeterDeclaration();
        ////        temp1.AcademicYearID = globalInfo.AcademicYearID.Value;
        ////        temp1.SchoolID = globalInfo.SchoolID.Value;
        ////        temp1.Semester = 1;
        ////        temp1.Resolution = Res.Get("Common_Label_FirstSemester");
        ////        temp1.Year = ay.Year;
        ////        temp1.SemesterMark = 1;
        ////        temp1.IsLock = false;

        ////        SemeterDeclaration temp2 = new SemeterDeclaration();
        ////        temp2.AcademicYearID = globalInfo.AcademicYearID.Value;
        ////        temp2.SchoolID = globalInfo.SchoolID.Value;
        ////        temp2.Semester = 2;
        ////        temp2.SemesterMark = 1;
        ////        temp2.Resolution = Res.Get("Common_Label_SecondSemester");
        ////        temp2.Year = ay.Year;
        ////        temp2.IsLock = false;

        ////        lsSemeterDeclaration = new List<SemeterDeclaration>();
        ////        lsSemeterDeclaration.Add(temp1);
        ////        lsSemeterDeclaration.Add(temp2);

        ////        this.SemeterDeclarationBusiness.Insert(temp1);
        ////        this.SemeterDeclarationBusiness.Insert(temp2);
        ////        this.SemeterDeclarationBusiness.Save();
        ////    }

        ////    return lsSemeterDeclaration;
        ////}
        //#endregion

        //#region Save
        // //public JsonResult Save(int?[] SemeterDeclarationKind, int?[] SemeterDeclarationValueHK1, int?[] SemeterDeclarationValueHK2, bool? SemeterDeclarationLockHK1, bool? SemeterDeclarationLockHK2)
        ////{
        ////    GlobalInfo global = new GlobalInfo();
        ////    List<SemeterDeclaration> lsSemeterDeclaration = CheckInDatabase();

        ////    SemeterDeclaration hk1 = lsSemeterDeclaration.Where(o => o.Semester == 1).FirstOrDefault();
        ////    SemeterDeclaration hk2 = lsSemeterDeclaration.Where(o => o.Semester == 2).FirstOrDefault();

        ////    #region fill du lieu de update

        ////    //diem mieng
        ////    hk1.InterviewMark = SemeterDeclarationValueHK1[0].HasValue ? (int)SemeterDeclarationValueHK1[0].Value : (int)0;
        ////    hk2.InterviewMark = SemeterDeclarationValueHK2[0].HasValue ? (int)SemeterDeclarationValueHK2[0].Value : (int)0;

        ////    //hs diem mieng
        ////    hk1.CoefficientInterview = SemeterDeclarationValueHK1[1].HasValue ? (int)SemeterDeclarationValueHK1[1].Value : (int)0;
        ////    hk2.CoefficientInterview = SemeterDeclarationValueHK2[1].HasValue ? (int)SemeterDeclarationValueHK2[1].Value : (int)0;

        ////    //diem 15
        ////    hk1.WritingMark = SemeterDeclarationValueHK1[2].HasValue ? (int)SemeterDeclarationValueHK1[2].Value : (int)0;
        ////    hk2.WritingMark = SemeterDeclarationValueHK2[2].HasValue ? (int)SemeterDeclarationValueHK2[2].Value : (int)0;

        ////    //hs diem 15
        ////    hk1.CoefficientWriting = SemeterDeclarationValueHK1[3].HasValue ? (int)SemeterDeclarationValueHK1[3].Value : (int)0;
        ////    hk2.CoefficientWriting = SemeterDeclarationValueHK2[3].HasValue ? (int)SemeterDeclarationValueHK2[3].Value : (int)0;

        ////    //diem 1t
        ////    hk1.TwiceCoeffiecientMark = SemeterDeclarationValueHK1[4].HasValue ? (int)SemeterDeclarationValueHK1[4].Value : (int)0;
        ////    hk2.TwiceCoeffiecientMark = SemeterDeclarationValueHK2[4].HasValue ? (int)SemeterDeclarationValueHK2[4].Value : (int)0;

        ////    //hs diem 1t
        ////    hk1.CoefficientTwice = SemeterDeclarationValueHK1[5].HasValue ? (int)SemeterDeclarationValueHK1[5].Value : (int)0;
        ////    hk2.CoefficientTwice = SemeterDeclarationValueHK2[5].HasValue ? (int)SemeterDeclarationValueHK2[5].Value : (int)0;

        ////    //diem hk
        ////    //hk1.SemesterMark = SemeterDeclarationValueHK1[6].HasValue ? (int)SemeterDeclarationValueHK1[6].Value : (int)0;
        ////    //hk2.SemesterMark = SemeterDeclarationValueHK2[6].HasValue ? (int)SemeterDeclarationValueHK2[6].Value : (int)0;
        ////    hk1.SemesterMark = 1;
        ////    hk2.SemesterMark = 1;

        ////    //hs diemhk
        ////    hk1.CoefficientSemester = SemeterDeclarationValueHK1[6].HasValue ? (int)SemeterDeclarationValueHK1[6].Value : (int)0;
        ////    hk2.CoefficientSemester = SemeterDeclarationValueHK2[6].HasValue ? (int)SemeterDeclarationValueHK2[6].Value : (int)0;

        ////    //khoa
        ////    hk1.IsLock = SemeterDeclarationLockHK1.HasValue;
        ////    hk2.IsLock = SemeterDeclarationLockHK2.HasValue;
        ////    #endregion

        ////    SemeterDeclarationBusiness.Update(hk1);
        ////    SemeterDeclarationBusiness.Update(hk2);
        ////    SemeterDeclarationBusiness.Save();

        ////    return Json(new JsonMessage(Res.Get("success_Save")));
        ////}
        //#endregion

        #endregion

        #region khoi tao du lieu

        /// <summary>
        /// Khoi tao du lieu ban dau cho page
        /// </summary>
        private void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[SemeterDeclarationConstant.ENABLE_BUTTON] = global.IsCurrentYear;
            var lsSD = SemeterDeclarationBusiness.Search(new Dictionary<string, object>() 
            { 
            {"AcademicYearID",global.AcademicYearID}
            ,{"SchoolID",global.SchoolID}
            })
                //.Where(o=>DateTime.Compare(DateTime.Now,o.AppliedDate)>=0)
            .OrderByDescending(o => o.AppliedDate);

            if (lsSD.Count() > 0)
            {
                //lay ra cac ngay dua len combobox
                var lsAppliedDate = lsSD.Select(o => o.AppliedDate).Distinct().OrderBy(o => o).ToList();
                List<ComboObject> lsComboAppliedDate = new List<ComboObject>();
                int i = 0;
                foreach (var item in lsAppliedDate)
                {
                    ComboObject co = new ComboObject();
                    co.value = string.Format("{0:dd/MM/yyyy}", item);
                    co.key = string.Format("{0:dd/MM/yyyy}", item);
                    lsComboAppliedDate.Add(co);
                    if (item.Date < DateTime.Now)
                    {
                        i++;

                    }
                }

                DateTime dt = lsAppliedDate.Where(o => DateTime.Compare(DateTime.Now, o) >= 0).LastOrDefault();
                if (dt.Year == 1)
                {
                    dt = lsAppliedDate.FirstOrDefault();
                }
                var lsSDDefault = lsSD.Where(o => o.AppliedDate.CompareTo(dt) == 0).ToList();

                ViewData[SemeterDeclarationConstant.LS_APPLIEDDATE] = new SelectList(lsComboAppliedDate, "key", "value");
                if (i > 0)
                {
                    ViewData[SemeterDeclarationConstant.LS_DEFAULT_APPLIEDDATE] = lsComboAppliedDate[i - 1];
                }

                ViewData[SemeterDeclarationConstant.LS_SEMETERDECLARATION] = Convert(lsSDDefault);

            }
            else
            {
                List<SemeterDeclaration> lsSDDefault = new List<SemeterDeclaration>();
                //add new 
                SemeterDeclaration sdHk1 = new SemeterDeclaration();
                sdHk1.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;

                sdHk1.CoefficientInterview = DEFAULT_COEFFICIENTINTERVIEW;
                sdHk1.CoefficientWriting = DEFAULT_COEFFICIENTWRITING;
                sdHk1.CoefficientTwice = DEFAULT_COEFFICIENTTWICE;
                sdHk1.CoefficientSemester = DEFAULT_COEFFICIENTSEMESTER;

                sdHk1.InterviewMark = DEFAULT_INTERVIEWMARK;
                sdHk1.WritingMark = DEFAULT_WRITINGMARK;
                sdHk1.TwiceCoeffiecientMark = DEFAULT_TWICECOEFFIECIENTMARK;
                sdHk1.SemesterMark = DEFAULT_SEMESTERMARK;

                SemeterDeclaration sdHk2 = new SemeterDeclaration();
                sdHk2.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;

                sdHk2.CoefficientInterview = DEFAULT_COEFFICIENTINTERVIEW;
                sdHk2.CoefficientWriting = DEFAULT_COEFFICIENTWRITING;
                sdHk2.CoefficientTwice = DEFAULT_COEFFICIENTTWICE;
                sdHk2.CoefficientSemester = DEFAULT_COEFFICIENTSEMESTER;

                sdHk2.InterviewMark = DEFAULT_INTERVIEWMARK;
                sdHk2.WritingMark = DEFAULT_WRITINGMARK;
                sdHk2.TwiceCoeffiecientMark = DEFAULT_TWICECOEFFIECIENTMARK;
                sdHk2.SemesterMark = DEFAULT_SEMESTERMARK;

                lsSDDefault.Add(sdHk1);
                lsSDDefault.Add(sdHk2);
                //
                ViewData[SemeterDeclarationConstant.LS_SEMETERDECLARATION] = Convert(lsSDDefault);
                ViewData[SemeterDeclarationConstant.LS_APPLIEDDATE] = new SelectList(new string[] { });
                ViewData[SemeterDeclarationConstant.LS_DEFAULT_APPLIEDDATE] = null;
            }
            ViewData[SemeterDeclarationConstant.SHOW_MESSAGE] = lsSD.Count() == 0;

        }
        #endregion

        #region Convert from List model to List view model
        private List<SemeterDeclarationObject> Convert(List<SemeterDeclaration> lsSemeterDeclaration)
        {
            #region convert to view model
            //tu lsSemeterDeclaration dua ra ngoai man hinh,9 phan tu co dinh
            SemeterDeclaration hk1 = lsSemeterDeclaration.FirstOrDefault();
            // SemeterDeclaration hk2 = lsSemeterDeclaration.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
            DateTime DefaultDateTime = new DateTime();
            List<SemeterDeclarationObject> lsSDO = new List<SemeterDeclarationObject>();
            //diem mieng
            {
                SemeterDeclarationObject sdo = new SemeterDeclarationObject();
                sdo.SemeterDeclarationKind = SemeterDeclarationConstant.DIEM_MIENG;
                sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_InterviewMark");
                sdo.SemeterDeclarationValueSCD = hk1.InterviewMark;
                sdo.SemeterDeclarationValueHS = hk1.CoefficientInterview;
                if (hk1.AppliedDate.CompareTo(DefaultDateTime) != 0)
                {
                    sdo.AppliedDate = hk1.AppliedDate;
                }
                lsSDO.Add(sdo);
            }
            //diem 15
            {
                SemeterDeclarationObject sdo = new SemeterDeclarationObject();
                sdo.SemeterDeclarationKind = SemeterDeclarationConstant.DIEM_15;
                sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_WritingMark");
                sdo.SemeterDeclarationValueSCD = hk1.WritingMark;
                sdo.SemeterDeclarationValueHS = hk1.CoefficientWriting;
                if (hk1.AppliedDate.CompareTo(DefaultDateTime) != 0)
                {
                    sdo.AppliedDate = hk1.AppliedDate;
                }
                lsSDO.Add(sdo);
            }
            //diem 1t
            {
                SemeterDeclarationObject sdo = new SemeterDeclarationObject();
                sdo.SemeterDeclarationKind = SemeterDeclarationConstant.DIEM_1T;
                sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_TwiceCoefficientWritingMark");
                sdo.SemeterDeclarationValueSCD = hk1.TwiceCoeffiecientMark;
                sdo.SemeterDeclarationValueHS = hk1.CoefficientTwice;
                if (hk1.AppliedDate.CompareTo(DefaultDateTime) != 0)
                {
                    sdo.AppliedDate = hk1.AppliedDate;
                }
                lsSDO.Add(sdo);
            }

            //diem hk
            {
                SemeterDeclarationObject sdo = new SemeterDeclarationObject();
                sdo.SemeterDeclarationKind = SemeterDeclarationConstant.DIEM_HK;
                sdo.SemeterDeclarationKindName = Res.Get("PeriodDeclaration_Label_SemesterMark");
                sdo.SemeterDeclarationValueSCD = hk1.SemesterMark;
                sdo.SemeterDeclarationValueHS = hk1.CoefficientSemester;
                if (hk1.AppliedDate.CompareTo(DefaultDateTime) != 0)
                {
                    sdo.AppliedDate = hk1.AppliedDate;
                }
                lsSDO.Add(sdo);
            }
            #endregion

            return lsSDO;
        }
        #endregion

        #region Save - luu thong tin vao db
        

        [ValidateAntiForgeryToken]
        public JsonResult Save(int?[] SemeterDeclarationKind, int?[] SemeterDeclarationValueHS, int?[] SemeterDeclarationValueSCD, DateTime? AppliedDate)
        {
            GlobalInfo global = new GlobalInfo();
            if (!AppliedDate.HasValue)
            {
                throw new BusinessException("SemesterDeclaration_Label_NoAppliedDate");
            }
            DateTime FromDate = AppliedDate.Value;
            DateTime ToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //tim trong csdl neu chua co thi insert
            var lsSD = (SemeterDeclarationBusiness.Search(new Dictionary<string, object>() 
            { 
            {"AcademicYearID",global.AcademicYearID}
            ,{"SchoolID",global.SchoolID}
            })).ToList();

            if (lsSD.Count > 0 && lsSD != null)
            {
                SemeterDeclaration obj = lsSD.OrderByDescending(p => p.AppliedDate).FirstOrDefault();
                if (FromDate < obj.AppliedDate)
                {
                   throw new BusinessException("Ngày áp dụng phải lớn hơn ngày áp dụng của lần khai báo loại điểm gần nhất " + obj.AppliedDate.ToString("dd/MM/yyyy"));
                }
                else if (FromDate > academicYear.SecondSemesterEndDate.Value)
                {
                    throw new BusinessException("Ngày áp dụng phải nhỏ hơn ngày kết thúc năm học hiện tại " + academicYear.SecondSemesterEndDate.Value.ToString("dd/MM/yyyy"));
                }

                lsSD = lsSD.Where(o => DateTime.Compare(o.AppliedDate, FromDate) > 0 && DateTime.Compare(o.AppliedDate, ToDate) <= 0).ToList();
            }
            if (lsSD.Count() == 0)
            {
                if (GetMenupermission("SemeterDeclaration", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                SemeterDeclaration sd = new SemeterDeclaration();
                sd.InterviewMark = SemeterDeclarationValueSCD[0].HasValue ? (int)SemeterDeclarationValueSCD[0].Value : (int)0;
                sd.CoefficientInterview = SemeterDeclarationValueHS[0].HasValue ? (int)SemeterDeclarationValueHS[0].Value : (int)0;
                sd.WritingMark = SemeterDeclarationValueSCD[1].HasValue ? (int)SemeterDeclarationValueSCD[1].Value : (int)0;
                sd.CoefficientWriting = SemeterDeclarationValueHS[1].HasValue ? (int)SemeterDeclarationValueHS[1].Value : (int)0;
                sd.TwiceCoeffiecientMark = SemeterDeclarationValueSCD[2].HasValue ? (int)SemeterDeclarationValueSCD[2].Value : (int)0;
                sd.CoefficientTwice = SemeterDeclarationValueHS[2].HasValue ? (int)SemeterDeclarationValueHS[2].Value : (int)0;

                sd.CoefficientSemester = SemeterDeclarationValueHS[3].HasValue ? (int)SemeterDeclarationValueHS[3].Value : (int)0;
                sd.SemesterMark = 1;
                sd.AppliedDate = AppliedDate.Value;
                sd.AcademicYearID = global.AcademicYearID.Value;
                sd.SchoolID = global.SchoolID.Value;
                sd.Year = AcademicYearBusiness.Find(global.AcademicYearID.Value).Year;

                sd.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                sd.Resolution = Res.Get("Common_Label_FirstSemester");
                // Kiem tra so con diem
                VaidateData(sd);
                SemeterDeclarationBusiness.Insert(sd);
                //hk2 giong hk1
                SemeterDeclaration sd2 = new SemeterDeclaration();
                Utils.Utils.BindTo(sd, sd2);
                sd2.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                sd2.Resolution = Res.Get("Common_Label_SecondSemester");
                // Kiem tra so con diem
                VaidateData(sd2);
                SemeterDeclarationBusiness.Insert(sd2);

                SemeterDeclarationBusiness.Save();
            }
            else   //co thi update thoi
            {
                //Kiem tra tong so con diem theo dot da khai bao

                List<PeriodDeclaration> lsPeriod = PeriodDeclarationBusiness.All.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && o.SchoolID == global.SchoolID && o.AcademicYearID == global.AcademicYearID).ToList();
                if (lsPeriod.Count > 0)
                {
                    int SumInter = 0;
                    int SumWriting = 0;
                    int SumTwiceCoeffiecient = 0;
                    foreach (PeriodDeclaration pd in lsPeriod)
                    {
                        if (pd.InterviewMark.HasValue)
                        {
                            SumInter += pd.InterviewMark.Value;
                        }
                        if (pd.WritingMark.HasValue)
                        {
                            SumWriting += pd.WritingMark.Value;
                        }
                        if (pd.TwiceCoeffiecientMark.HasValue)
                        {
                            SumTwiceCoeffiecient = pd.TwiceCoeffiecientMark.Value;
                        }
                    }

                    if (SemeterDeclarationValueSCD[0].HasValue && SemeterDeclarationValueSCD[0].Value < SumInter)
                    {
                        throw new BusinessException("Validate_Sum_InterviewMark");
                    }
                    if (SemeterDeclarationValueSCD[1].HasValue && SemeterDeclarationValueSCD[1].Value < SumWriting)
                    {
                        throw new BusinessException("Validate_Sum_WritingMark");
                    }
                    if (SemeterDeclarationValueSCD[2].HasValue && SemeterDeclarationValueSCD[2].Value < SumTwiceCoeffiecient)
                    {
                        throw new BusinessException("Validate_Sum_TwiceCoeffiecientMark");
                    }

                }
                if (GetMenupermission("SemeterDeclaration", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                if (lsSD.Count() != 2)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                SemeterDeclaration sd1 = lsSD.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                SemeterDeclaration sd2 = lsSD.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();

                sd1.InterviewMark = SemeterDeclarationValueSCD[0].HasValue ? (int)SemeterDeclarationValueSCD[0].Value : (int)0;
                sd1.CoefficientInterview = SemeterDeclarationValueHS[0].HasValue ? (int)SemeterDeclarationValueHS[0].Value : (int)0;
                sd1.WritingMark = SemeterDeclarationValueSCD[1].HasValue ? (int)SemeterDeclarationValueSCD[1].Value : (int)0;
                sd1.CoefficientWriting = SemeterDeclarationValueHS[1].HasValue ? (int)SemeterDeclarationValueHS[1].Value : (int)0;
                sd1.TwiceCoeffiecientMark = SemeterDeclarationValueSCD[2].HasValue ? (int)SemeterDeclarationValueSCD[2].Value : (int)0;
                sd1.CoefficientTwice = SemeterDeclarationValueHS[2].HasValue ? (int)SemeterDeclarationValueHS[2].Value : (int)0;

                sd1.CoefficientSemester = SemeterDeclarationValueHS[3].HasValue ? (int)SemeterDeclarationValueHS[3].Value : (int)0;
                sd1.SemesterMark = DEFAULT_SEMESTERMARK;
                sd1.AppliedDate = AppliedDate.Value;
                sd1.AcademicYearID = global.AcademicYearID.Value;
                sd1.SchoolID = global.SchoolID.Value;
                sd1.Year = AcademicYearBusiness.Find(global.AcademicYearID.Value).Year;

                sd1.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                sd1.Resolution = Res.Get("Common_Label_FirstSemester");
                SemeterDeclarationBusiness.Update(sd1);
                //hk2 giong hk1
                sd2.InterviewMark = SemeterDeclarationValueSCD[0].HasValue ? (int)SemeterDeclarationValueSCD[0].Value : (int)0;
                sd2.CoefficientInterview = SemeterDeclarationValueHS[0].HasValue ? (int)SemeterDeclarationValueHS[0].Value : (int)0;
                sd2.WritingMark = SemeterDeclarationValueSCD[1].HasValue ? (int)SemeterDeclarationValueSCD[1].Value : (int)0;
                sd2.CoefficientWriting = SemeterDeclarationValueHS[1].HasValue ? (int)SemeterDeclarationValueHS[1].Value : (int)0;
                sd2.TwiceCoeffiecientMark = SemeterDeclarationValueSCD[2].HasValue ? (int)SemeterDeclarationValueSCD[2].Value : (int)0;
                sd2.CoefficientTwice = SemeterDeclarationValueHS[2].HasValue ? (int)SemeterDeclarationValueHS[2].Value : (int)0;

                sd2.CoefficientSemester = SemeterDeclarationValueHS[3].HasValue ? (int)SemeterDeclarationValueHS[3].Value : (int)0;
                sd2.SemesterMark = DEFAULT_SEMESTERMARK;
                sd2.AppliedDate = AppliedDate.Value;
                sd2.AcademicYearID = global.AcademicYearID.Value;
                sd2.SchoolID = global.SchoolID.Value;
                sd2.Year = AcademicYearBusiness.Find(global.AcademicYearID.Value).Year;

                sd2.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                sd2.Resolution = Res.Get("Common_Label_SecondSemester");
                SemeterDeclarationBusiness.Update(sd2);
                SemeterDeclarationBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("success_Save")));
        }
        #endregion

        #region SetAppliedDate - dat ngay ap dung

        [ValidateAntiForgeryToken]
        public PartialViewResult SetAppliedDate(DateTime? AppliedDate)
        {
            GlobalInfo global = new GlobalInfo();
            if (!AppliedDate.HasValue)
            {
                throw new BusinessException("Thầy/cô chưa nhập Ngày áp dụng");
            }
            else
            {
                DateTime FromDate = AppliedDate.Value;
                DateTime ToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);
                //kiem tra xem co ngay nay trong csdl chua
                var lsSD = SemeterDeclarationBusiness.Search(new Dictionary<string, object>() 
            { 
            {"AcademicYearID",global.AcademicYearID}
            ,{"SchoolID",global.SchoolID}
            }).Where(o => DateTime.Compare(o.AppliedDate, FromDate) >= 0)
            .Where(o => DateTime.Compare(o.AppliedDate, ToDate) <= 0);
                //ton tai =>exception
                if (lsSD.Count() > 0)
                {
                    throw new BusinessException("SemesterDeclaration_Label_DupAppliedDate");
                }
                List<SemeterDeclaration> lsSDDefault = new List<SemeterDeclaration>();
                //add new 
                SemeterDeclaration sdHk1 = new SemeterDeclaration();
                sdHk1.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;

                sdHk1.CoefficientInterview = DEFAULT_COEFFICIENTINTERVIEW;
                sdHk1.CoefficientWriting = DEFAULT_COEFFICIENTWRITING;
                sdHk1.CoefficientTwice = DEFAULT_COEFFICIENTTWICE;
                sdHk1.CoefficientSemester = DEFAULT_COEFFICIENTSEMESTER;

                sdHk1.InterviewMark = DEFAULT_INTERVIEWMARK;
                sdHk1.WritingMark = DEFAULT_WRITINGMARK;
                sdHk1.TwiceCoeffiecientMark = DEFAULT_TWICECOEFFIECIENTMARK;
                sdHk1.SemesterMark = DEFAULT_SEMESTERMARK;
                sdHk1.AppliedDate = AppliedDate.Value;

                SemeterDeclaration sdHk2 = new SemeterDeclaration();
                sdHk2.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;

                sdHk2.CoefficientInterview = DEFAULT_COEFFICIENTINTERVIEW;
                sdHk2.CoefficientWriting = DEFAULT_COEFFICIENTWRITING;
                sdHk2.CoefficientTwice = DEFAULT_COEFFICIENTTWICE;
                sdHk2.CoefficientSemester = DEFAULT_COEFFICIENTSEMESTER;

                sdHk2.InterviewMark = DEFAULT_INTERVIEWMARK;
                sdHk2.WritingMark = DEFAULT_WRITINGMARK;
                sdHk2.TwiceCoeffiecientMark = DEFAULT_TWICECOEFFIECIENTMARK;
                sdHk2.SemesterMark = DEFAULT_SEMESTERMARK;
                sdHk2.AppliedDate = AppliedDate.Value;

                lsSDDefault.Add(sdHk1);
                lsSDDefault.Add(sdHk2);
                //
                ViewData[SemeterDeclarationConstant.LS_SEMETERDECLARATION] = Convert(lsSDDefault);
                ViewData[SemeterDeclarationConstant.ENABLE_BUTTON] = true;
                ViewData[SemeterDeclarationConstant.SHOW_MESSAGE] = false;
            }
            SetViewDataPermission("SemeterDeclaration", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return PartialView("_List");
        }
        #endregion

        #region DeleteSemester - xoa dot
        

        [ValidateAntiForgeryToken]
        public JsonResult DeleteSemester(DateTime? AppliedDate)
        {
            GlobalInfo global = new GlobalInfo();
            if (!AppliedDate.HasValue)
            {
                throw new BusinessException("SemesterDeclaration_Label_NoAppliedDate");
            }
            if (GetMenupermission("SemeterDeclaration", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            DateTime FromDate = AppliedDate.Value;
            DateTime ToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);
            var lstSD = SemeterDeclarationBusiness.Search(new Dictionary<string, object>() 
            { 
            {"AcademicYearID",global.AcademicYearID}
            ,{"SchoolID",global.SchoolID}
            }).ToList();
            //kiem tra xem co ngay nay trong csdl chua
            var lsSD = lstSD.Where(o => DateTime.Compare(o.AppliedDate, FromDate) >= 0)
             .Where(o => DateTime.Compare(o.AppliedDate, ToDate) <= 0).ToList();
            //neu khong ton tai =>exception
            if (lsSD.Count() == 0)
            {
                throw new BusinessException("SemesterDeclaration_Label_NoSemesterOfAppliedDate");
            }
            var listSD = lstSD.Where(o => o.AppliedDate != AppliedDate).ToList();
            if (listSD.Count > 0)
            {
                //Kiem tra so con diem xem co > so con diem da khai bao hay ko
                SemeterDeclaration sd = listSD.OrderByDescending(o => o.AppliedDate).FirstOrDefault();

                AcademicYear ac = AcademicYearBusiness.Find(global.AcademicYearID.Value);
                int semester = 1;
                if (ac.SecondSemesterStartDate < sd.AppliedDate && ac.SecondSemesterEndDate > sd.AppliedDate)
                {
                    semester = 2;
                }

                List<PeriodDeclaration> listpd = PeriodDeclarationBusiness.All.Where(o => o.Semester == semester && o.SchoolID == global.SchoolID && o.AcademicYearID == global.AcademicYearID).ToList();

                int SumI = 0;
                int SumW = 0;
                int SumT = 0;
                foreach (PeriodDeclaration pd in listpd)
                {
                    if (pd.InterviewMark.HasValue)
                    {
                        SumI += pd.InterviewMark.Value;
                    }
                    if (pd.WritingMark.HasValue)
                    {
                        SumW += pd.WritingMark.Value;
                    }
                    if (pd.TwiceCoeffiecientMark.HasValue)
                    {
                        SumT += pd.TwiceCoeffiecientMark.Value;
                    }
                }
                if (SumI > sd.InterviewMark || SumW > sd.WritingMark || SumT > sd.TwiceCoeffiecientMark)
                {
                    List<object> Params = new List<object>();
                    Params.Add(sd.AppliedDate.ToString("dd/MM/yyyy"));
                    throw new BusinessException("SemesterDeclaration_Validate_AppliedDate", Params);
                }


            }

            SemeterDeclarationBusiness.DeleteAll(lsSD);
            SemeterDeclarationBusiness.Save();

            return Json(new JsonMessage(Res.Get("success_del")));
        }
        #endregion

        #region ChangeAppliedDate - doi ngay ap dung

        [ValidateAntiForgeryToken]
        public PartialViewResult ChangeAppliedDate(DateTime? AppliedDate)
        {
            GlobalInfo global = new GlobalInfo();
            if (!AppliedDate.HasValue)
            {
                List<SemeterDeclaration> lsSDDefault = new List<SemeterDeclaration>();
                //add new 
                SemeterDeclaration sdHk1 = new SemeterDeclaration();
                sdHk1.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;

                sdHk1.CoefficientInterview = DEFAULT_COEFFICIENTINTERVIEW;
                sdHk1.CoefficientWriting = DEFAULT_COEFFICIENTWRITING;
                sdHk1.CoefficientTwice = DEFAULT_COEFFICIENTTWICE;
                sdHk1.CoefficientSemester = DEFAULT_COEFFICIENTSEMESTER;

                sdHk1.InterviewMark = DEFAULT_INTERVIEWMARK;
                sdHk1.WritingMark = DEFAULT_WRITINGMARK;
                sdHk1.TwiceCoeffiecientMark = DEFAULT_TWICECOEFFIECIENTMARK;
                sdHk1.SemesterMark = DEFAULT_SEMESTERMARK;

                SemeterDeclaration sdHk2 = new SemeterDeclaration();
                sdHk2.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;

                sdHk2.CoefficientInterview = DEFAULT_COEFFICIENTINTERVIEW;
                sdHk2.CoefficientWriting = DEFAULT_COEFFICIENTWRITING;
                sdHk2.CoefficientTwice = DEFAULT_COEFFICIENTTWICE;
                sdHk2.CoefficientSemester = DEFAULT_COEFFICIENTSEMESTER;

                sdHk2.InterviewMark = DEFAULT_INTERVIEWMARK;
                sdHk2.WritingMark = DEFAULT_WRITINGMARK;
                sdHk2.TwiceCoeffiecientMark = DEFAULT_TWICECOEFFIECIENTMARK;
                sdHk2.SemesterMark = DEFAULT_SEMESTERMARK;

                lsSDDefault.Add(sdHk1);
                lsSDDefault.Add(sdHk2);
                //
                ViewData[SemeterDeclarationConstant.LS_SEMETERDECLARATION] = Convert(lsSDDefault);
                ViewData[SemeterDeclarationConstant.LS_APPLIEDDATE] = new SelectList(new string[] { });
                ViewData[SemeterDeclarationConstant.LS_DEFAULT_APPLIEDDATE] = null;
                ViewData[SemeterDeclarationConstant.ENABLE_BUTTON] = false;
                ViewData[SemeterDeclarationConstant.SHOW_MESSAGE] = true;
                ViewData[SemeterDeclarationConstant.APPLIEDDATE_DEFAULT] = (AppliedDate != null ? AppliedDate.Value.ToString("dd/MM/yyyy"):"");
            }
            else
            {

                DateTime FromDate = AppliedDate.Value;
                DateTime ToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);
                //kiem tra xem co ngay nay trong csdl chua
                var lsSD = SemeterDeclarationBusiness.Search(new Dictionary<string, object>() 
            { 
            {"AcademicYearID",global.AcademicYearID}
            ,{"SchoolID",global.SchoolID}
            }).Where(o => DateTime.Compare(o.AppliedDate, FromDate) >= 0)
            .Where(o => DateTime.Compare(o.AppliedDate, ToDate) <= 0);
                //khong ton tai =>exception
                if (lsSD.Count() == 0)
                {
                    throw new BusinessException("SemesterDeclaration_Label_NotExistAppliedDate");
                }
                ViewData[SemeterDeclarationConstant.LS_SEMETERDECLARATION] = Convert(lsSD.ToList());
                ViewData[SemeterDeclarationConstant.ENABLE_BUTTON] = true;
                ViewData[SemeterDeclarationConstant.SHOW_MESSAGE] = lsSD.Count() == 0;
                ViewData[SemeterDeclarationConstant.APPLIEDDATE_DEFAULT] = (AppliedDate != null ? AppliedDate.Value.ToString("dd/MM/yyyy") : "");
            }
            SetViewDataPermission("SemeterDeclaration", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return PartialView("_List");
        }
        #endregion

        /// <summary>
        /// Quanglm1
        /// Kiem tra khong vuot qua con diem quy dinh
        /// </summary>
        /// <param name="sd"></param>
        private void VaidateData(SemeterDeclaration sd)
        {
            if (sd.InterviewMark > SMAS.Web.Constants.GlobalConstants.MAX_COUNT_MARK_M)
            {
                List<object> listParams = new List<object>();
                listParams.Add("M");
                listParams.Add(SMAS.Web.Constants.GlobalConstants.MAX_COUNT_MARK_M);
                throw new BusinessException("SemesterDeclaration_Label_MaxCountMark", listParams);
            }

            if (sd.WritingMark > SMAS.Web.Constants.GlobalConstants.MAX_COUNT_MARK_P)
            {
                List<object> listParams = new List<object>();
                listParams.Add("15P");
                listParams.Add(SMAS.Web.Constants.GlobalConstants.MAX_COUNT_MARK_P);
                throw new BusinessException("SemesterDeclaration_Label_MaxCountMark", listParams);
            }

            if (sd.TwiceCoeffiecientMark > SMAS.Web.Constants.GlobalConstants.MAX_COUNT_MARK_V)
            {
                List<object> listParams = new List<object>();
                listParams.Add("V");
                listParams.Add(SMAS.Web.Constants.GlobalConstants.MAX_COUNT_MARK_V);
                throw new BusinessException("SemesterDeclaration_Label_MaxCountMark", listParams);
            }

            if (sd.SemesterMark > SMAS.Web.Constants.GlobalConstants.MAX_COUNT_MARK_HK)
            {
                List<object> listParams = new List<object>();
                listParams.Add("HK");
                listParams.Add(SMAS.Web.Constants.GlobalConstants.MAX_COUNT_MARK_HK);
                throw new BusinessException("SemesterDeclaration_Label_MaxCountMark", listParams);
            }
        }
    }
}
