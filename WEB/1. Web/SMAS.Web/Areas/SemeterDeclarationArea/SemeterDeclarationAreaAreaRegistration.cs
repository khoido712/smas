﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SemeterDeclarationArea
{
    public class SemeterDeclarationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SemeterDeclarationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SemeterDeclarationArea_default",
                "SemeterDeclarationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
