using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SemeterDeclarationArea.Models
{
    public class SemeterDeclarationObject
    {

        public int SemeterDeclarationID
        {
            get;
            set;
        }

         #region get from model

        //[ResDisplayName("PeriodDeclaration_Label_Discontinued")]
        //public bool IsLock
        //{
        //    get;
        //    set;
        //}

        //public string Resolution { get; set; }

        //public int AcademicYearID { get; set; }
        //public Nullable<int> SchoolID { get; set; }

        //public Nullable<int> Year { get; set; }

        //public int Semester { get; set; }


        //[ResDisplayName("PeriodDeclaration_Label_InterviewMark")]
        //public int InterviewMark { get; set; }

        //[ResDisplayName("PeriodDeclaration_Label_WritingMark")]
        //public int WritingMark { get; set; }

        //[ResDisplayName("PeriodDeclaration_Label_TwiceCoefficientWritingMark")]
        //public int TwiceCoeffiecientMark { get; set; }

        //[ResDisplayName("PeriodDeclaration_Label_CoefficientInterview")]
        //public int CoefficientInterview { get; set; }

        //[ResDisplayName("PeriodDeclaration_Label_CoefficientWriting")]
        //public int CoefficientWriting { get; set; }

        //[ResDisplayName("PeriodDeclaration_Label_CoefficientTwice")]
        //public int CoefficientTwice { get; set; }

        //[ResDisplayName("PeriodDeclaration_Label_SemesterMark")]
        //public int SemesterMark { get; set; }

        //[ResDisplayName("PeriodDeclaration_Label_CoefficientSemester")]
        //public int CoefficientSemester { get; set; }

       
        #endregion
     
         #region grid

        [ResourceDisplayName("PeriodDeclaration_Label_SemeterDeclarationKind")]
        public int SemeterDeclarationKind { get; set; }

         [ResourceDisplayName("PeriodDeclaration_Label_SemeterDeclarationKind")]
         public string SemeterDeclarationKindName { get; set; }

         [ResourceDisplayName("PeriodDeclaration_Label_HS")]
         public int SemeterDeclarationValueHS { get; set; }

         [ResourceDisplayName("PeriodDeclaration_Label_SCD")]
         public int SemeterDeclarationValueSCD { get; set; }

         [ResourceDisplayName("PeriodDeclaration_Label_AppliedDate")]
         public DateTime? AppliedDate { get; set; }

         #endregion


    }
}
