﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SemeterDeclarationArea
{
    public class SemeterDeclarationConstant
    {
        public const string LS_SEMETERDECLARATION = "LISTSEMETERDECLARATION";
        public const string LS_SEMESTER = "LS_SEMESTER";

        public const int DIEM_MIENG = 1;
        public const int HS_DIEM_MIENG = 2;
        public const int DIEM_15 = 3;
        public const int HS_DIEM_15 = 4;
        public const int DIEM_1T = 5;
        public const int HS_DIEM_1T = 6;
        public const int DIEM_HK = 7;
        public const int HS_DIEM_HK = 8;
        public const int KHOA = 9;

        public const string LS_APPLIEDDATE = "LS_APPLIEDDATE";
        public const string LS_DEFAULT_APPLIEDDATE = "LS_DEFAULT_APPLIEDDATE";
        public const string ENABLE_BUTTON = "ENABLE_BUTTON";

        public const string SHOW_MESSAGE = "SHOW_MESSAGE";

        public const string APPLIEDDATE_DEFAULT = "Applieddate_Default";

    }
}