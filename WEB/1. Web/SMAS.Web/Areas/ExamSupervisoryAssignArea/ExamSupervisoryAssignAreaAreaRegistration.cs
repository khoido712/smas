﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea
{
    public class ExamSupervisoryAssignAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamSupervisoryAssignArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamSupervisoryAssignArea_default",
                "ExamSupervisoryAssignArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
