﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Models
{
    public class CreateSwapReplaceContentViewModel
    {
        public List<ExamSupervisoryViewModel> listExamSupervisory { get; set; }
        public List<SupervisoryAssignmentViewModel> listAssignedOfSupervisory { get; set; }
        public long ExamSupervisoryAssignmentID { get; set; }
        public string ExamSupervisoryName { get; set; }

        //Phuc vu cho chuc nang them moi giam thi
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public long SubjectID { get; set; }
        public long ExamRoomID { get; set; }
        public string ExamRoomCode { get; set; }
    }
}