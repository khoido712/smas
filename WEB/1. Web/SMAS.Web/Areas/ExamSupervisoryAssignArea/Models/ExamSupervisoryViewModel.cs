﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Models
{
    public class ExamSupervisoryViewModel
    {
        public long ExamSupervisoryAssignmentID { get; set; }
        public long ExamSupervisoryID { get; set; }
        public int TeacherID { get; set; }
        public int? SchoolFacultyID { get; set; }
        public long ExamRoomID { get; set; }

        [ResourceDisplayName("ExamSupervisoryAssign_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_EmployeeFullName")]
        public string EmployeeFullName { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_SchoolFacultyName")]
        public string SchoolFacultyName { get; set; }
        public string EthnicCode { get; set; }
        public string ExamRoomCode { get; set; }
        public int ExamRoomSettedNumber { get; set; }

        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamRoomDisplay")]
        public string ExamRoomDisplayString
        {
            get
            {
                return ExamRoomCode + " (Số thí sinh: " + ExamRoomSettedNumber + ")";
            }
        }
    }
}