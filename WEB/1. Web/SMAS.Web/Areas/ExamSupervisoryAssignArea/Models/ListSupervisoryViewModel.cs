﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Models
{
    public class ListSupervisoryViewModel
    {
        [ResourceDisplayName("ExamSupervisoryAssign_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_EmployeeFullName")]
        public string EmployeeFullName { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_SchoolFacultyName")]
        public string SchoolFacultyName { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamGroupID")]
        public long ExamGroupID { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamGroupID")]
        public string ExamGroupName { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamSubjectID")]
        public int SubjectID { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamSubjectID")]
        public string SubjectName { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamRoomID")]
        public long ExamRoomID { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamRoomID")]
        public string ExamRoomName { get; set; }
        public DateTime? CreateDate { get; set; }
        [ResourceDisplayName("Ghi chú")]
        public string Note 
        {
            get {
                if (CreateDate != null)
                {
                    string a = CreateDate.Value.ToShortDateString();
                    string b = CreateDate.Value.Hour.ToString();
                    string c = CreateDate.Value.Minute.ToString();
                    return b +":" + c + " ngày " + a;
                }
                    
                else
                    return "";
            }
        }
        public long ExamSupervisoryAssignmentID { get; set; }

    }
}