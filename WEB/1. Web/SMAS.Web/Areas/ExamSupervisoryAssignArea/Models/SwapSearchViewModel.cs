﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Models
{
    public class SwapSearchViewModel
    {
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryAssignConstants.CBO_EXAM_SUBJECT_NEW)]
        public long? SubjectIDNew { get; set; }

        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryAssignConstants.CBO_EXAM_GROUP_NEW)]
        [AdditionalMetadata("OnChange", "onExamGroupChangeNew()")]
        public long? ExamGroupIDNew { get; set; }

        public long ExaminationsIDNew { get; set; }

        public long examSupervisoryAssignmentIDOld { get; set; }

        public int EmployeeID { get; set; }

    }
}