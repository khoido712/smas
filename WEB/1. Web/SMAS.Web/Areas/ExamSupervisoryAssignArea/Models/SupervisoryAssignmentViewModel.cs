﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Models
{
    public class SupervisoryAssignmentViewModel
    {
        public long ExamSupervisoryAssignmentID {get;set;}
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public int? OrderInSubject { get; set; }
        public string ExamGroupCode { get; set; }
        public string ExamGroupName { get; set; }
        public long ExamRoomID { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamRoomCode")]
        public string ExamRoomCode { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_Note")]
        public string Note { get; set; }
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamSubjectID")]
        public string DisplaySubject
        {
            get
            {
                return SubjectName + " - " + ExamGroupName;
            }
        }
    }
}