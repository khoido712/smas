﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Models
{
    public class AutoAssignViewModel
    {

        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [StringLength(1, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Range(1, 9, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamSupervisoryAssign_Validate_PosInteger_SupervisoryPerRoom")]
        [ResourceDisplayName("ExamSupervisoryAssign_Label_SupervisoryPerRoom")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamSupervisoryAssign_Validate_Required")]
        public string SupervisoryPerRoom { get; set; }

        public int AssignScale { get; set; }
        public long _ExaminationsID { get; set; }
        public long _ExamGroupID { get; set; }
        public int _SubjectID { get; set; }
    }
}