﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryAssignConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public long ExaminationsID { get; set; }

        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryAssignConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public long? ExamGroupID { get; set; }      

        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryAssignConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("OnChange", "onExamSubjectChange()")]
        public long? SubjectID { get; set; }
       

        [ResourceDisplayName("ExamSupervisoryAssign_Label_ExamRoomID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryAssignConstants.CBO_EXAM_ROOM)]
        [AdditionalMetadata("OnChange", "onExamRoomChange()")]
        public long? ExamRoomID { get; set; }
    }
}