﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Models
{
    public class TeacherComboboxViewModel
    {
        public int EmployeeID { get; set; }
        public string DisplayName { get; set; }
    }
}