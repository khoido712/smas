﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Web.Areas.ExamSupervisoryAssignArea;
using SMAS.Web.Areas.ExamSupervisoryAssignArea.Models;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea.Controller
{
    public class ExamSupervisoryAssignController : BaseController
    {
        #region properties
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IExamSupervisoryBusiness ExamSupervisoryBusiness;
        private readonly IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamSubjectBO> listExamSubject;
        private List<ExamRoom> listExamRoom;

        #endregion

        #region Constructor
        public ExamSupervisoryAssignController(IExamGroupBusiness examGroupBusiness, IExaminationsBusiness examinationsBusiness, IExamSubjectBusiness examSubjectBusiness,
            IExamRoomBusiness examRoomBusiness, IExamSupervisoryBusiness examSupervisoryBusiness, IExamSupervisoryAssignmentBusiness examSupervisoryAssignmentBusiness,
            ISubjectCatBusiness subjectCatBusiness, IExamPupilBusiness examPupilBusiness, IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness)
        {
            this.ExamGroupBusiness = examGroupBusiness;
            this.ExaminationsBusiness = examinationsBusiness;
            this.ExamSubjectBusiness = examSubjectBusiness;
            this.ExamRoomBusiness = examRoomBusiness;
            this.ExamSupervisoryBusiness = examSupervisoryBusiness;
            this.ExamSupervisoryAssignmentBusiness = examSupervisoryAssignmentBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ExamPupilBusiness = examPupilBusiness;
            this.ExamSupervisoryViolateBusiness = ExamSupervisoryViolateBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamSupervisoryAssignArea/ExamSupervisoryAssign/

        public ActionResult Index()
        {

            SetViewData();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Lay ky thi mac dinh
            long? defaultExamID = null;
            if (listExaminations.Count > 0)
            {
                defaultExamID = listExaminations.First().ExaminationsID;
            }

            //Lay nhom thi mac dinh
            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0)
            {
                defaultExamGroupID = listExamGroup.First().ExamGroupID;
            }

            //Lay mon thi mac dinh
            int? defaultSubjectID = null;
            if (listExamSubject.Count > 0)
            {
                defaultSubjectID = listExamSubject.First().SubjectID;
            }

            //Lay so giam thi hien co cua ky thi
            int totalSupervisory = 0;
            if (defaultExamID != null && defaultExamGroupID != null)
                totalSupervisory = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminationsCustom(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, defaultExamID.Value, defaultExamGroupID.Value).Count();

            if (defaultExamID == null || defaultExamGroupID == null || defaultSubjectID == null)
            {
                ViewData[ExamSupervisoryAssignConstants.LIST_EXAM_SUPERVISORY] = new List<ExamSupervisoryViewModel>();
                ViewData[ExamSupervisoryAssignConstants.INFO_TOTAL_ROOM] = listExamRoom.Count;
                ViewData[ExamSupervisoryAssignConstants.INFO_TOTAL_SUPERVISORY] = totalSupervisory;
                CheckCommandPermision(this.ExaminationsBusiness.Find(defaultExamID), this.ExamGroupBusiness.Find(defaultExamGroupID));
                return View();
            }

            List<ExamSupervisoryViewModel> listResult = this._Search(defaultExamID.Value, defaultExamGroupID.Value, defaultSubjectID.Value, dic).ToList();

            ViewData[ExamSupervisoryAssignConstants.LIST_EXAM_SUPERVISORY] = listResult;
            ViewData[ExamSupervisoryAssignConstants.INFO_TOTAL_ROOM] = listExamRoom.Count;
            ViewData[ExamSupervisoryAssignConstants.INFO_TOTAL_SUPERVISORY] = totalSupervisory;

            CheckCommandPermision(this.ExaminationsBusiness.Find(defaultExamID), this.ExamGroupBusiness.Find(defaultExamGroupID));
            return View();
        }

        //
        // GET: /ExamSupervisoryAssignArea/Search/
        public PartialViewResult Search(SearchViewModel form)
        {
            Utils.Utils.TrimObject(form);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExamRoomID"] = form.ExamRoomID;

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;


            if (examinationsID == null || examGroupID == null || subjectID == null)
            {
                ViewData[ExamSupervisoryAssignConstants.INFO_TOTAL_ROOM] = 0;
                ViewData[ExamSupervisoryAssignConstants.INFO_TOTAL_SUPERVISORY] = 0;
                CheckCommandPermision(null, null);
                return PartialView("_List", new List<ExamSupervisoryViewModel>());
            }
            //Add search info - Navigate to Search function in business
            List<ExamSupervisoryViewModel> listResult = _Search(examinationsID.Value, examGroupID.Value, subjectID.Value, SearchInfo).ToList();

            ViewData[ExamSupervisoryAssignConstants.INFO_TOTAL_ROOM] = this.ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID.Value)
                                                                                                   .Where(o => o.ExamGroupID == examGroupID.Value).Count();

            ViewData[ExamSupervisoryAssignConstants.INFO_TOTAL_SUPERVISORY] = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminationsCustom(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID.Value, examGroupID.Value).Count();

            CheckCommandPermision(this.ExaminationsBusiness.Find(examinationsID.Value), this.ExamGroupBusiness.Find(examGroupID.Value));

            return PartialView("_List", listResult);
        }

        [HttpPost]
        public JsonResult ValidateBeforeAssign(AutoAssignViewModel form)
        {

            Utils.Utils.TrimObject(form);

            int assignScale = form.AssignScale;
            int supervisoryNumPerRoom = Convert.ToInt32(form.SupervisoryPerRoom);
            long examinationsID = form._ExaminationsID;
            long examGroupID = form._ExamGroupID;
            int subjectID = form._SubjectID;

            //Lay danh sach giam thi trong ky thi
            List<ExamSupervisoryBO> listExamSupervisory = ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID)
                                                                        .OrderBy(o => o.TeacherName)
                                                                        .ToList();
            List<ExamSupervisoryAssignment> listAssignOfExam = this.ExamSupervisoryAssignmentBusiness.All
                                                   .Where(o => o.ExaminationsID == examinationsID)
                                                   .ToList();
            List<ExamRoom> listExamRoomOfExam = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                .ToList();

            //Phan cong giam thi theo mon thi hien tai hoac Phan cong cho tat ca cac mon cua nhom thi hien tai
            if (assignScale == ExamSupervisoryAssignConstants.ASSIGN_SCALE_CURRENT_SUBJECT || assignScale == ExamSupervisoryAssignConstants.ASSIGN_SCALE_ALL_SUBJECT)
            {
                CheckExamSupervisoryPerRoomNum(form, listExamSupervisory, listAssignOfExam, listExamRoomOfExam);
            }
            //Phan cong cho mon thi hien tai cua tat ca nhom thi
            else if (assignScale == ExamSupervisoryAssignConstants.ASSIGN_SCALE_ALL_ROOM)
            {

                //Lay danh sach nhom thi cua ky thi
                List<ExamGroup> listExamGroup = this.ExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                        .OrderBy(o => o.ExamGroupCode).ToList();

                //Phan cong
                for (int iGroup = 0; iGroup < listExamGroup.Count; iGroup++)
                {

                    ExamGroup examGroup = listExamGroup[iGroup];
                    //Kiem tra nhom thi co hoc mon thi hay khong
                    if (this.ExamSubjectBusiness.All.Where(o => o.ExamGroupID == examGroup.ExamGroupID).Where(o => o.SubjectID == subjectID).Count() > 0)
                    {
                        form._ExamGroupID = examGroup.ExamGroupID;
                        CheckExamSupervisoryPerRoomNum(form, listExamSupervisory, listAssignOfExam, listExamRoomOfExam);
                    }
                }
            }

            return Json(new JsonMessage());

        }

        /// <summary>
        /// Auto Asign
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult AutoAssign(AutoAssignViewModel form)
        {
            if (ModelState.IsValid)
            {
                if (GetMenupermission("ExamSupervisoryAssign", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }

                Utils.Utils.TrimObject(form);


                int assignScale = form.AssignScale;
                int supervisoryNumPerRoom = Convert.ToInt32(form.SupervisoryPerRoom);
                long examinationsID = form._ExaminationsID;
                long examGroupID = form._ExamGroupID;
                int subjectID = form._SubjectID;

                //Lay danh sach giam thi trong ky thi
                List<ExamSupervisoryBO> listExamSupervisory = ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID)
                                                                            //.Where(x => x.ExamGroupID == examGroupID || x.ExamGroupID == 0)
                                                                            .OrderBy(o => o.TeacherName)
                                                                            .ToList();

                List<ExamSupervisoryAssignment> listAssignOfExam = this.ExamSupervisoryAssignmentBusiness.All
                                                       .Where(o => o.ExaminationsID == examinationsID)
                                                       .ToList();
                List<ExamRoom> listExamRoomOfExam = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                    .ToList();
                List<ExamSupervisoryAssignment> listToInsert = new List<ExamSupervisoryAssignment>();
                List<ExamSupervisoryAssignment> listToDelete = new List<ExamSupervisoryAssignment>();

                //Bien dem so phong thi da phan cong
                int assignedRoomNum = 0;

                //Phan cong giam thi theo mon thi hien tai
                if (assignScale == ExamSupervisoryAssignConstants.ASSIGN_SCALE_CURRENT_SUBJECT)
                {
                    listExamSupervisory = listExamSupervisory.Where(x => x.ExamGroupID == examGroupID || x.ExamGroupID == 0).ToList();
                    AutoAssignHandle(form, listExamSupervisory, listToInsert, listToDelete, listAssignOfExam, listExamRoomOfExam, ref assignedRoomNum);
                }
                //Phan cong cho tat ca cac mon cua nhom thi hien tai
                else if (assignScale == ExamSupervisoryAssignConstants.ASSIGN_SCALE_ALL_SUBJECT)
                {
                    //Lay danh sach mon thi cua nhom thi
                    List<ExamSubject> listExamSubject = this.ExamSubjectBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                                  .Where(o => o.ExamGroupID == examGroupID)
                                                                                  .ToList();
                    listExamSupervisory = listExamSupervisory.Where(x => x.ExamGroupID == examGroupID || x.ExamGroupID == 0).ToList();
                    //Phan cong
                    for (int iSubject = 0; iSubject < listExamSubject.Count; iSubject++)
                    {
                        ExamSubject examSubject = listExamSubject[iSubject];
                        form._SubjectID = examSubject.SubjectID;
                        AutoAssignHandle(form, listExamSupervisory, listToInsert, listToDelete, listAssignOfExam, listExamRoomOfExam, ref assignedRoomNum);
                    }
                }
                //Phan cong cho mon thi hien tai cua tat ca nhom thi
                else if (assignScale == ExamSupervisoryAssignConstants.ASSIGN_SCALE_ALL_ROOM)
                {

                    //Lay danh sach nhom thi cua ky thi
                    List<ExamGroup> listExamGroup = this.ExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                            .OrderBy(o => o.ExamGroupCode).ToList();

                    //Phan cong
                    List<ExamSupervisoryBO> listExamSupervisoryTemp = new List<ExamSupervisoryBO>();
                    for (int iGroup = 0; iGroup < listExamGroup.Count; iGroup++)
                    {
                        listExamSupervisoryTemp = listExamSupervisory.Where(x => x.ExamGroupID == listExamGroup[iGroup].ExamGroupID || x.ExamGroupID == 0).ToList();
                        ExamGroup examGroup = listExamGroup[iGroup];
                        //Kiem tra nhom thi co hoc mon thi hay khong
                        if (this.ExamSubjectBusiness.All.Where(o => o.ExamGroupID == examGroup.ExamGroupID).Where(o => o.SubjectID == subjectID).Count() > 0)
                        {
                            form._ExamGroupID = examGroup.ExamGroupID;
                            AutoAssignHandle(form, listExamSupervisoryTemp, listToInsert, listToDelete, listAssignOfExam, listExamRoomOfExam, ref assignedRoomNum);
                        }
                    }
                }

                this.ExamSupervisoryAssignmentBusiness.DeleteList(listToDelete);
                this.ExamSupervisoryAssignmentBusiness.InsertList(listToInsert);

                return Json(new JsonMessage(String.Format(Res.Get("ExamSupervisoryAssign_Message_AssignSuccess"), assignedRoomNum)));
            }
            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }

        //
        //GET: /ExamSupervisoryAssignArea/Create/
        public ActionResult Create(long examinationsID, long examGroupID, int subjectID, long examRoomID)
        {
            ExamRoom examRoom = this.ExamRoomBusiness.Find(examRoomID);
            //Danh sach giam thi trong ky thi
            List<ExamSupervisoryBO> listExamSupervisory = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID).ToList();
            listExamSupervisory = listExamSupervisory.Where(x => x.ExamGroupID == examGroupID || x.ExamGroupID == 0).ToList();

            //Danh sach cac giam thi da duoc phan cong cho nhom thi va mon thi
            List<long> listAssignedSupervisoryID = this.ExamSupervisoryAssignmentBusiness.All
                                                       .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID.Value)
                                                       .Where(o => o.ExaminationsID == examinationsID)
                                                       .Where(o => o.ExamGroupID == examGroupID)
                                                       .Where(o => o.SubjectID == subjectID)
                                                       .Select(o => o.ExamSupervisoryID).Distinct().ToList();
            //Lay danh sach giam thi chua duoc phan cong
            List<ExamSupervisoryViewModel> listExamSupervisoryModel = listExamSupervisory.Where(o => !listAssignedSupervisoryID.Contains(o.ExamSupervisoryID))
                                                                .Select(o => new ExamSupervisoryViewModel
                                                                {
                                                                    EmployeeCode = o.EmployeeCode,
                                                                    EmployeeFullName = o.TeacherName,
                                                                    SchoolFacultyName = o.SchoolFacultyName,
                                                                    ExamSupervisoryID = o.ExamSupervisoryID,
                                                                    EthnicCode = o.EthnicCode,
                                                                }).OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeFullName.getOrderingName(o.EthnicCode))).ToList();


            CreateSwapReplaceContentViewModel content = new CreateSwapReplaceContentViewModel();
            content.listExamSupervisory = listExamSupervisoryModel;
            content.ExamRoomID = examRoomID;
            content.ExamRoomCode = examRoom.ExamRoomCode;
            content.ExaminationsID = examinationsID;
            content.ExamGroupID = examGroupID;
            content.SubjectID = subjectID;

            return PartialView("_Create", content);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(long examinationsID, long examGroupID, int subjectID, long examRoomID, string arrRoomID)
        {
            //Kiem tra quyen insert
            if (GetMenupermission("ExamSupervisoryAssign", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            string[] examSupervisoryIDDArr;
            if (arrRoomID != "")
            {
                arrRoomID = arrRoomID.Remove(arrRoomID.Length - 1);
                examSupervisoryIDDArr = arrRoomID.Split(',');
            }
            else
            {
                examSupervisoryIDDArr = new string[] { };
            }
            if (examSupervisoryIDDArr.Length == 0)
            {
                throw new BusinessException("ExamSupervisoryAssign_Validate_Create_NotChoosen");
            }
            List<long> listExamSupervisoryID = examSupervisoryIDDArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList();

            //Phân công giám thị vào phòng thi
            for (int i = 0; i < listExamSupervisoryID.Count; i++)
            {
                ExamSupervisoryAssignment entity = new ExamSupervisoryAssignment();
                entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                entity.AssignedDate = DateTime.Now;
                entity.CreateTime = DateTime.Now;
                entity.ExamGroupID = examGroupID;
                entity.ExaminationsID = examinationsID;
                entity.ExamRoomID = examRoomID;
                entity.ExamSupervisoryAssignmentID = this.ExamSupervisoryAssignmentBusiness.GetNextSeq<long>("EXAMSUPERVISORYASSIGN_SEQ");
                entity.ExamSupervisoryID = listExamSupervisoryID[i];
                entity.SubjectID = subjectID;
                entity.UpdateTime = null;

                this.ExamSupervisoryAssignmentBusiness.CheckDuplicateData(entity, 0);
                this.ExamSupervisoryAssignmentBusiness.Insert(entity);
                this.ExamSupervisoryAssignmentBusiness.Save();
            }

            return Json(new JsonMessage(Res.Get("ExamSupervisoryAssign_Message_CreateSuccess")));
        }

        public ActionResult SwapNew(long examinationsID, long examGroupID, long examSupervisoryAssignmentID)
        {
            #region LoadCB
            //CB nhom thi
            ViewData["ExaminationsNew"] = examinationsID;
            List<ExamGroup> lstExamGroup = null;
            if (examinationsID != null)
            {
                lstExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                lstExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamSupervisoryAssignConstants.CBO_EXAM_GROUP_NEW] = new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (lstExamGroup.Count > 0)
                defaultExamGroupID = lstExamGroup.First().ExamGroupID;

            //CB mon thi
            List<ExamSubjectBO> lstExamSubject = null;
            if (defaultExamGroupID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = examinationsID;
                //dic["ExamGroupID"] = defaultExamGroupID;
                lstExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();
            }
            else
            {
                lstExamSubject = new List<ExamSubjectBO>();
            }
            ViewData[ExamSupervisoryAssignConstants.CBO_EXAM_SUBJECT_NEW] = new SelectList(lstExamSubject, "SubjectID", "SubjectName");
            #endregion
            ExamSupervisoryAssignment assignment = this.ExamSupervisoryAssignmentBusiness.Find(examSupervisoryAssignmentID);
            long examSupervisoryID = assignment.ExamSupervisoryID;
            ViewData["examSupervisoryAssignmentIDOld"] = examSupervisoryAssignmentID;

            ViewData["TeacherName"] = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID)
                .Where(o => o.ExamSupervisoryID == examSupervisoryID).ToList().FirstOrDefault().TeacherName;

            //Danh sach cac giam thi cua ki thi
            List<ExamSupervisoryBO> listExamSupervisory = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID).ToList();
            //Danh sach cac giam thi da duoc phan cong trong ky thi
            List<long> listAssignedSupervisoryID = this.ExamSupervisoryAssignmentBusiness.All
                                                       .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID.Value)
                                                       .Where(o => o.ExaminationsID == examinationsID)
                                                       .Select(o => o.ExamSupervisoryID).Distinct().ToList();

            // CB giáo viên
            List<ExamSupervisoryViewModel> listExamSupervisoryModel = listExamSupervisory.Where(o => listAssignedSupervisoryID.Contains(o.ExamSupervisoryID))
                                                                .Where(o => o.ExamSupervisoryID != examSupervisoryID)
                                                                .Select(o => new ExamSupervisoryViewModel
                                                                {
                                                                    EmployeeCode = o.EmployeeCode,
                                                                    TeacherID = o.TeacherID,
                                                                    EmployeeFullName = o.TeacherName,
                                                                    SchoolFacultyName = o.SchoolFacultyName,
                                                                    ExamSupervisoryID = o.ExamSupervisoryID,
                                                                    EthnicCode = o.EthnicCode
                                                                }).OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeFullName.getOrderingName(o.EthnicCode))).ToList();

            List<TeacherComboboxViewModel> lstTeacher = new List<TeacherComboboxViewModel>();
            TeacherComboboxViewModel objTeacher = new TeacherComboboxViewModel();
            objTeacher.EmployeeID = 0;
            objTeacher.DisplayName = "[Tât cả]";
            lstTeacher.Add(objTeacher);

            List<TeacherComboboxViewModel> query = (from q in listExamSupervisoryModel
                                                    select new TeacherComboboxViewModel
                                                    {
                                                        EmployeeID = q.TeacherID,
                                                        DisplayName = q.EmployeeFullName + " - " + q.EmployeeCode + " - " + q.SchoolFacultyName
                                                    }).ToList();
            lstTeacher.AddRange(query);
            ViewData[ExamSupervisoryAssignConstants.LIST_TEACHER] = lstTeacher;
            
            return PartialView("_SwapNew");
        }

        [HttpPost]
        public PartialViewResult SwapSearchNew(SwapSearchViewModel form)
        {
            //Lay danh sach phan cong cua giam thi trong ky thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ExaminationsID"] = form.ExaminationsIDNew;           
            dic["SubjectID"] = form.SubjectIDNew;
            dic["ExamGroupID"] = form.ExamGroupIDNew;
            dic["EmployeeID"] = form.EmployeeID;

            long MinusExamSupervisoryAssign = SMAS.Business.Common.Utils.GetLong(form.examSupervisoryAssignmentIDOld);
            ExamSupervisoryAssignment assignment = this.ExamSupervisoryAssignmentBusiness.Find(MinusExamSupervisoryAssign);
            long examSupervisoryID = assignment.ExamSupervisoryID;

            List<ListSupervisoryViewModel> listResult = this.ExamSupervisoryAssignmentBusiness.AdditionSearch(dic).Where(x => x.ExamSupervisoryID != examSupervisoryID).ToList()
                                                                .Select(o => new ListSupervisoryViewModel
                                                                {
                                                                    EmployeeCode = o.EmployeeCode,                                                                   
                                                                    EmployeeFullName = o.EmployeeFullName,
                                                                    SchoolFacultyName = o.SchoolFacultyName,
                                                                    ExamGroupName = o.ExamGroupName,
                                                                    SubjectName = o.SubjectName,
                                                                    ExamRoomName = o.ExamRoomCode,                                                                   
                                                                    SubjectID = o.SubjectID,
                                                                    ExamSupervisoryAssignmentID = o.ExamSupervisoryAssignmentID,
                                                                    CreateDate = o.CreateDate
                                                                    //OrderInSubject = o.OrderInSubject
                                                                })//.OrderBy(o => o.OrderInSubject)
                                                                .ToList();
            int totalRecord = listResult.Count();
            ViewData["Total"] = totalRecord;
            listResult = listResult.Take(15).ToList();

            return PartialView("_ListExamSupervisoryNew", listResult);

        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult PagingSwapSearchNew(SwapSearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);

            long MinusExamSupervisoryAssign = SMAS.Business.Common.Utils.GetLong(form.examSupervisoryAssignmentIDOld);
            ExamSupervisoryAssignment assignment = this.ExamSupervisoryAssignmentBusiness.Find(MinusExamSupervisoryAssign);
            long examSupervisoryID = assignment.ExamSupervisoryID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ExaminationsID"] = form.ExaminationsIDNew;
            dic["SubjectID"] = form.SubjectIDNew;
            dic["ExamGroupID"] = form.ExamGroupIDNew;
            dic["EmployeeID"] = form.EmployeeID;

            List<ListSupervisoryViewModel> listResult = this.ExamSupervisoryAssignmentBusiness.AdditionSearch(dic).Where(x=>x.ExamSupervisoryID != examSupervisoryID).ToList()
                                                                .Select(o => new ListSupervisoryViewModel
                                                                {
                                                                    EmployeeCode = o.EmployeeCode,
                                                                    EmployeeFullName = o.EmployeeFullName,
                                                                    SchoolFacultyName = o.SchoolFacultyName,
                                                                    ExamGroupName = o.ExamGroupName,
                                                                    SubjectName = o.SubjectName,
                                                                    ExamRoomName = o.ExamRoomCode,
                                                                    SubjectID = o.SubjectID,
                                                                    ExamSupervisoryAssignmentID = o.ExamSupervisoryAssignmentID,
                                                                    CreateDate = o.CreateDate
                                                                    //OrderInSubject = o.OrderInSubject
                                                                })//.OrderBy(o => o.OrderInSubject)
                                                                 .ToList();
            int totalRecord = listResult.Count();
            ViewData["Total"] = totalRecord;

            //listResult = listResult.Skip((currentPage - 1) * pageSize).Take(15).ToList();

            return View(new GridModel<ListSupervisoryViewModel>
            {
                Total = totalRecord,
                Data = listResult.Skip((currentPage - 1) * pageSize).Take(15)
            });
        }

        [HttpPost]
        public ActionResult SwapSearch(long examinationsID, long examSupervisoryID)
        {
            //Lay danh sach phan cong cua giam thi trong ky thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ExaminationsID"] = examinationsID;
            dic["ExamSupervisoryID"] = examSupervisoryID;

            List<SupervisoryAssignmentViewModel> listResult = this.ExamSupervisoryAssignmentBusiness.AdditionSearch(dic).ToList()
                                                                .Select(o => new SupervisoryAssignmentViewModel
                                                                {
                                                                    SubjectID = o.SubjectID,
                                                                    ExamSupervisoryAssignmentID = o.ExamSupervisoryAssignmentID,
                                                                    SubjectName = o.SubjectName,
                                                                    ExamGroupCode = o.ExamGroupCode,
                                                                    ExamGroupName = o.ExamGroupName,
                                                                    ExamRoomCode = o.ExamRoomCode,
                                                                    Note = o.SchedulesExam,
                                                                    OrderInSubject = o.OrderInSubject
                                                                }).OrderBy(o => o.OrderInSubject).ToList();

            //Lay danh sach mon thi
            ViewData[ExamSupervisoryAssignConstants.CBO_SWAP_EXAM_SUBJECT] = new SelectList(listResult, "ExamSupervisoryAssignmentID", "DisplaySubject");

            return PartialView("_SwapRightList", listResult);

        }

        public JsonResult SwapNewAjax(long examSupervisoryAssignmentID1, long? examSupervisoryAssignmentID2)
        {
            if (GetMenupermission("ExamSupervisoryAssign", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
           
            if (examSupervisoryAssignmentID2 == null)
            {
                throw new BusinessException(Res.Get("ExamSupervisoryAssign_Validate_NotSubjectToSwap"));
            }

            ExamSupervisoryAssignment assignment1 = this.ExamSupervisoryAssignmentBusiness.Find(examSupervisoryAssignmentID1);
            ExamSupervisoryAssignment assignment2 = this.ExamSupervisoryAssignmentBusiness.Find(examSupervisoryAssignmentID2);

            if (assignment1.ExamGroupID == assignment2.ExamGroupID && assignment1.ExamRoomID == assignment2.ExamRoomID)
            {
                throw new BusinessException(Res.Get("Thầy/cô không thể hoán đổi giám thị ở cùng phòng thi"));
            }

            long temp = assignment1.ExamSupervisoryID;
            assignment1.ExamSupervisoryID = assignment2.ExamSupervisoryID;
            assignment2.ExamSupervisoryID = temp;
            assignment1.AssignedDate = DateTime.Now;
            assignment2.AssignedDate = DateTime.Now;

            //Kiem tra rang buoc
            this.ExamSupervisoryAssignmentBusiness.CheckDuplicateData(assignment1, assignment2.ExamSupervisoryAssignmentID);
            this.ExamSupervisoryAssignmentBusiness.CheckDuplicateData(assignment2, assignment1.ExamSupervisoryAssignmentID);

            //Update
            this.ExamSupervisoryAssignmentBusiness.Update(assignment1);
            this.ExamSupervisoryAssignmentBusiness.Update(assignment2);

            this.ExamSupervisoryAssignmentBusiness.Save();

            return Json(new JsonMessage(Res.Get("ExamSupervisoryAssign_Message_SwapSuccess")));
        }


        //
        // GET: /ExamSupervisoryAssignArea/Swap/
        public ActionResult Swap(long examinationsID, long examGroupID, long examSupervisoryAssignmentID)
        {
            ExamSupervisoryAssignment assignment = this.ExamSupervisoryAssignmentBusiness.Find(examSupervisoryAssignmentID);
            long examSupervisoryID = assignment.ExamSupervisoryID;

            //Danh sach cac giam thi cua ki thi
            List<ExamSupervisoryBO> listExamSupervisory = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID).ToList();
            //Danh sach cac giam thi da duoc phan cong trong ky thi
            List<long> listAssignedSupervisoryID = this.ExamSupervisoryAssignmentBusiness.All
                                                       .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID.Value)
                                                       .Where(o => o.ExaminationsID == examinationsID)
                                                       .Select(o => o.ExamSupervisoryID).Distinct().ToList();
            //Lay danh sach giam thi cho phan ben trai
            List<ExamSupervisoryViewModel> listExamSupervisoryModel = listExamSupervisory.Where(o => listAssignedSupervisoryID.Contains(o.ExamSupervisoryID))
                                                                .Where(o => o.ExamSupervisoryID != examSupervisoryID)
                                                                .Select(o => new ExamSupervisoryViewModel
                                                                {
                                                                    EmployeeCode = o.EmployeeCode,
                                                                    EmployeeFullName = o.TeacherName,
                                                                    SchoolFacultyName = o.SchoolFacultyName,
                                                                    ExamSupervisoryID = o.ExamSupervisoryID,
                                                                    EthnicCode = o.EthnicCode
                                                                }).OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeFullName.getOrderingName(o.EthnicCode))).ToList();

            CreateSwapReplaceContentViewModel content = new CreateSwapReplaceContentViewModel();
            content.listExamSupervisory = listExamSupervisoryModel;
            content.listAssignedOfSupervisory = new List<SupervisoryAssignmentViewModel>();
            content.ExamSupervisoryAssignmentID = examSupervisoryAssignmentID;
            content.ExamSupervisoryName = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID)
                .Where(o => o.ExamSupervisoryID == examSupervisoryID).ToList().FirstOrDefault().TeacherName;

            ViewData[ExamSupervisoryAssignConstants.CBO_SWAP_EXAM_SUBJECT] = new SelectList(new List<ExamSubjectBO>(), "SubjectID", "SubjectName");

            return PartialView("_Swap", content);
        }
     
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Swap(long examSupervisoryAssignmentID1, long? examSupervisoryAssignmentID2, long? examSupervisoryID)
        {
            if (GetMenupermission("ExamSupervisoryAssign", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (examSupervisoryID == null)
            {
                throw new BusinessException(Res.Get("ExamSupervisoryAssign_Validate_NotSupervisoryToSwap"));
            }
            else if (examSupervisoryAssignmentID2 == null)
            {
                throw new BusinessException(Res.Get("ExamSupervisoryAssign_Validate_NotSubjectToSwap"));
            }

            ExamSupervisoryAssignment assignment1 = this.ExamSupervisoryAssignmentBusiness.Find(examSupervisoryAssignmentID1);
            ExamSupervisoryAssignment assignment2 = this.ExamSupervisoryAssignmentBusiness.Find(examSupervisoryAssignmentID2);

            if (assignment1.ExamGroupID == assignment2.ExamGroupID) 
            {
                throw new BusinessException(Res.Get("ExamSupervisoryAssign_Validate_NotSubjectToSwap"));
            }

            long temp = assignment1.ExamSupervisoryID;
            assignment1.ExamSupervisoryID = assignment2.ExamSupervisoryID;
            assignment2.ExamSupervisoryID = temp;
            assignment1.AssignedDate = DateTime.Now;
            assignment2.AssignedDate = DateTime.Now;
          
            //Kiem tra rang buoc
            this.ExamSupervisoryAssignmentBusiness.CheckDuplicateData(assignment1, assignment2.ExamSupervisoryAssignmentID);
            this.ExamSupervisoryAssignmentBusiness.CheckDuplicateData(assignment2, assignment1.ExamSupervisoryAssignmentID);

            //Update
            this.ExamSupervisoryAssignmentBusiness.Update(assignment1);
            this.ExamSupervisoryAssignmentBusiness.Update(assignment2);

            this.ExamSupervisoryAssignmentBusiness.Save();

            return Json(new JsonMessage(Res.Get("ExamSupervisoryAssign_Message_SwapSuccess")));
        }

        //
        // GET: /ExamSupervisoryAssignArea/Replace/
        public ActionResult Replace(long examinationsID, long examSupervisoryAssignmentID)
        {
            ExamSupervisoryAssignment assignment = this.ExamSupervisoryAssignmentBusiness.Find(examSupervisoryAssignmentID);
            long examSupervisoryID = assignment.ExamSupervisoryID;

            //Danh sach cac giam thi cua ki thi
            List<ExamSupervisoryBO> listExamSupervisory = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID).ToList();

            //Danh sach cac giam thi da duoc phan cong trong ky thi
            List<long> listAssignedSupervisoryID = this.ExamSupervisoryAssignmentBusiness.All
                                                       .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID.Value)
                                                       .Where(o => o.ExaminationsID == examinationsID)
                                                       .Select(o => o.ExamSupervisoryID).Distinct().ToList();
            //Lay danh sach giam thi chua duoc phan cong
            List<ExamSupervisoryViewModel> listExamSupervisoryModel = listExamSupervisory.Where(o => !listAssignedSupervisoryID.Contains(o.ExamSupervisoryID))
                                                                .Select(o => new ExamSupervisoryViewModel
                                                                {
                                                                    EmployeeCode = o.EmployeeCode,
                                                                    EmployeeFullName = o.TeacherName,
                                                                    SchoolFacultyName = o.SchoolFacultyName,
                                                                    ExamSupervisoryID = o.ExamSupervisoryID,
                                                                    EthnicCode = o.EthnicCode
                                                                }).OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeFullName.getOrderingName(o.EthnicCode))).ToList();


            CreateSwapReplaceContentViewModel content = new CreateSwapReplaceContentViewModel();
            content.listExamSupervisory = listExamSupervisoryModel;
            content.ExamSupervisoryAssignmentID = examSupervisoryAssignmentID;
            content.ExamSupervisoryName = this.ExamSupervisoryBusiness.GetExamSupervisoryOfExaminations(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID)
                .Where(o => o.ExamSupervisoryID == examSupervisoryID).ToList().FirstOrDefault().TeacherName;

            return PartialView("_Replace", content);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Replace(long examSupervisoryAssignmentID, long? examSupervisoryID)
        {
            if (GetMenupermission("ExamSupervisoryAssign", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (examSupervisoryID == null)
            {
                throw new BusinessException(Res.Get("ExamSupervisoryAssign_Validate_NotSupervisoryToReplace"));
            }

            ExamSupervisoryAssignment assigned = this.ExamSupervisoryAssignmentBusiness.Find(examSupervisoryAssignmentID);

            assigned.ExamSupervisoryID = examSupervisoryID.Value;
            this.ExamSupervisoryAssignmentBusiness.CheckDuplicateData(assigned, assigned.ExamSupervisoryAssignmentID);

            this.ExamSupervisoryAssignmentBusiness.Update(assigned);
            this.ExamSupervisoryAssignmentBusiness.Save();

            return Json(new JsonMessage(Res.Get("ExamSupervisoryAssign_Message_ReplaceSuccess")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Delete(int id)
        {
            if (GetMenupermission("ExamSupervisoryAssign", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ExamSupervisoryAssignment entity = this.ExamSupervisoryAssignmentBusiness.Find(id);
            if (entity != null)
            {
                //Kiem tra giam thi da co vi pham hay chua
                if (ExamSupervisoryViolateBusiness.All.Where(o => o.ExaminationsID == entity.ExaminationsID)
                                                     .Where(o => o.ExamGroupID == entity.ExamGroupID)
                                                     .Where(o => o.SubjectID == entity.SubjectID)
                                                     .Where(o => o.ExamRoomID == entity.ExamRoomID)
                                                     .Where(o => o.ExamSupervisoryID == entity.ExamSupervisoryID)
                                                    .Count() > 0)
                {
                    throw new BusinessException("ExamSupervisoryAssign_Validate_ViolatedTeacher");
                }

                this.ExamSupervisoryAssignmentBusiness.Delete(id);
                this.ExamSupervisoryAssignmentBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamSubject(long examinationsID, long? examGroupID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();

            return Json(new SelectList(listExamSubject, "SubjectID", "SubjectName"));
        }

        public JsonResult AjaxLoadExamSubjectNew(long examinationsID, long? examGroupID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            List<SelectListItem> listExamSubjectNew = new List<SelectListItem>();
            listExamSubjectNew = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList()
                        .Select(u => new SelectListItem { Value = u.SubjectID.ToString(), Text = u.SubjectName, Selected = false })
                        .ToList();

            return Json(listExamSubjectNew);
        }

        public JsonResult AjaxLoadExamRoom(long examinationsID, long? examGroupID)
        {
            listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                  .Where(o => o.ExamGroupID == examGroupID)
                                                  .OrderBy(o => o.ExamRoomCode)
                                                  .ToList();

            return Json(new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode"));
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamSupervisoryAssignConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamSupervisoryAssignConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;

            //Lay danh sach mon thi
            if (defaultExamGroupID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = defaultExamID;
                dic["ExamGroupID"] = defaultExamGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();
            }
            else
            {
                listExamSubject = new List<ExamSubjectBO>();
            }
            ViewData[ExamSupervisoryAssignConstants.CBO_EXAM_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");

            //Lay danh sach phong thi
            if (defaultExamGroupID != null)
            {
                listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == defaultExamID.Value)
                                                 .Where(o => o.ExamGroupID == defaultExamGroupID.Value)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            }
            else
            {
                listExamRoom = new List<ExamRoom>();
            }
            ViewData[ExamSupervisoryAssignConstants.CBO_EXAM_ROOM] = new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode");
        }


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ExamSupervisoryViewModel> _Search(long examinationsID, long examGroupID, long subjectID, IDictionary<string, object> SearchInfo)
        {
            List<ExamSupervisoryAssignmentBO> listAssignment = ExamSupervisoryAssignmentBusiness.SearchByExamSubject
                                                        (_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID, examGroupID, subjectID, SearchInfo).ToList();

            //Lay danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = this.ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                   .Where(o => o.ExamGroupID == examGroupID)
                                                                   .OrderBy(o => o.ExamRoomCode).ToList();
            long examRoomID = SMAS.Business.Common.Utils.GetLong(SearchInfo, "ExamRoomID");
            if (examRoomID > 0)
            {
                listExamRoom = listExamRoom.Where(o => o.ExamRoomID == examRoomID).ToList();
            }
            //Lay so thi sinh da duoc sap xep vao cac phong
            int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<ExamPupil> listExamPupil = this.ExamPupilBusiness.GetExamPupilOfExamGroup(examinationsID, examGroupID, _globalInfo.SchoolID.GetValueOrDefault()
                                            , _globalInfo.AcademicYearID.GetValueOrDefault(), partitionID).ToList();

            var listCountByRoom = (from q in listExamPupil
                                   group q by q.ExamRoomID into qg
                                   select new
                                   {
                                       ExamRoomID = qg.Key,
                                       ExamPupilCount = qg.Count()
                                   }).ToList();
            var examRoomWithCount = (from er in listExamRoom
                                     join cbr in listCountByRoom
                                         on er.ExamRoomID equals cbr.ExamRoomID into des
                                     from x in des.DefaultIfEmpty()
                                     select new
                                     {
                                         ExamRoomID = er.ExamRoomID,
                                         SettedNumber = x != null ? x.ExamPupilCount : 0,
                                         ExamRoomCode = er.ExamRoomCode
                                     }).ToList();


            List<ExamSupervisoryViewModel> ret = (from erwc in examRoomWithCount
                                                  join la in listAssignment
                                                      on erwc.ExamRoomID equals la.ExamRoomID into des
                                                  from x in des.DefaultIfEmpty()
                                                  select new ExamSupervisoryViewModel
                                                  {
                                                      EmployeeCode = x != null ? x.EmployeeCode : String.Empty,
                                                      EmployeeFullName = x != null ? x.EmployeeFullName : String.Empty,
                                                      EthnicCode = x != null ? x.EthnicCode : null,
                                                      ExamRoomCode = erwc.ExamRoomCode,
                                                      ExamRoomID = erwc.ExamRoomID,
                                                      ExamSupervisoryID = x != null ? x.ExamSupervisoryID : 0,
                                                      SchoolFacultyID = x != null ? x.SchoolFacultyID : 0,
                                                      SchoolFacultyName = x != null ? x.SchoolFacultyName : String.Empty,
                                                      ExamSupervisoryAssignmentID = x != null ? x.ExamSupervisoryAssignmentID : 0,
                                                      TeacherID = x != null ? x.TeacherID : 0,
                                                      ExamRoomSettedNumber = erwc.SettedNumber
                                                  }).OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeFullName.getOrderingName(o.EthnicCode))).ToList();
            return ret;
        }

        private void AutoAssignHandle(AutoAssignViewModel form, List<ExamSupervisoryBO> listExamSupervisory, List<ExamSupervisoryAssignment> listToInsert,
         List<ExamSupervisoryAssignment> listToDelete, List<ExamSupervisoryAssignment> listAssignOfExam, List<ExamRoom> listRoomOfExam, ref int assignedRoomNum)
        {
            int assignScale = form.AssignScale;
            int supervisoryNumPerRoom = Convert.ToInt32(form.SupervisoryPerRoom);
            long examinationsID = form._ExaminationsID;
            long examGroupID = form._ExamGroupID;
            int subjectID = form._SubjectID;

            //Lay danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = listRoomOfExam.Where(o => o.ExamGroupID == examGroupID)
                                                            .OrderBy(o => o.ExamRoomCode).ToList();

            //Kiem tra du lieu
            int minSupervisoryNum = listExamRoom.Count * supervisoryNumPerRoom;
            if (minSupervisoryNum > listExamSupervisory.Count)
            {
                throw new BusinessException(String.Format(@Res.Get("ExamSupervisoryAssign_Validate_NotEnoughSupervisory"), minSupervisoryNum));
            }

            //Xoa du lieu phan cong cu
            List<ExamSupervisoryAssignment> oldAssignData = listAssignOfExam.Where(o => o.ExamGroupID == examGroupID)
                                                .Where(o => o.SubjectID == subjectID)
                                                .ToList();

            listToDelete.AddRange(oldAssignData);

            //Phan cong
            List<ExamSupervisoryBO> tempListExamSupervisory = new List<ExamSupervisoryBO>(listExamSupervisory.Where(x=>x.ExamGroupID == examGroupID).ToList());
            List<ExamSupervisoryBO> tempListExamSupervisoryGeneral = null;
            #region option 1, option 2           
            //if(tempListExamSupervisory.Count > 0)
            //{
                List<long> lstID = tempListExamSupervisory.Select(x => x.ExamSupervisoryID).Distinct().ToList();
                tempListExamSupervisoryGeneral = new List<ExamSupervisoryBO>(listExamSupervisory.Where(x => !lstID.Contains(x.ExamSupervisoryID) && x.ExamGroupID == 0).ToList());
            //}
            //else
            //    tempListExamSupervisoryGeneral = new List<ExamSupervisoryBO>(listExamSupervisory);

            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom room = listExamRoom[i];
                //Lay ngau nhien so giam thi bang so giam thi moi phong thi
                List<ExamSupervisoryBO> listESForRoom = new List<ExamSupervisoryBO>();
                List<ExamSupervisoryBO> listESForRoomTemp = new List<ExamSupervisoryBO>();
                Random rnd = new Random();
                for (int j = 0; j < supervisoryNumPerRoom; j++)
                {
                    if (tempListExamSupervisory.Count() > 0)
                    {
                        int index = rnd.Next(tempListExamSupervisory.Count);
                        ExamSupervisoryBO esb = tempListExamSupervisory[index];
                        tempListExamSupervisory.Remove(esb);
                        listESForRoom.Add(esb);
                        listESForRoomTemp.Add(esb);
                    }
                    else 
                    {
                        int index = rnd.Next(tempListExamSupervisoryGeneral.Count);
                        ExamSupervisoryBO esb = tempListExamSupervisoryGeneral[index];
                        tempListExamSupervisoryGeneral.Remove(esb);
                        listESForRoom.Add(esb);
                        listESForRoomTemp.Add(esb);
                    }                    
                }

                //Tao record phan cong cho phong va mon
                ExamSupervisoryAssignment entity;
                for (int j = 0; j < listESForRoom.Count; j++)
                {
                    int index = rnd.Next(listESForRoomTemp.Count);
                    ExamSupervisoryBO es = listESForRoomTemp[index];
                    listESForRoomTemp.Remove(es);
                    //ExamSupervisoryBO es = listESForRoom[j];
                    
                    entity = new ExamSupervisoryAssignment();
                    entity.ExamSupervisoryAssignmentID = ExamSupervisoryAssignmentBusiness.GetNextSeq<long>("EXAMSUPERVISORYASSIGN_SEQ");
                    entity.ExamSupervisoryID = es.ExamSupervisoryID;
                    entity.ExaminationsID = examinationsID;
                    entity.ExamGroupID = examGroupID;
                    entity.SubjectID = subjectID;
                    entity.ExamRoomID = room.ExamRoomID;
                    entity.AssignedDate = DateTime.Now;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.CreateTime = DateTime.Now;
                    entity.UpdateTime = null;

                    listToInsert.Add(entity);
                }
                assignedRoomNum++;
            }
            #endregion
            

        }

        /// <summary>
        /// Kiem tra disable/enable, an/hien cac button
        /// </summary>
        /// <param name="listResult"></param>
        private void CheckCommandPermision(Examinations examinations, ExamGroup examGroup)
        {
            SetViewDataPermission("ExamSupervisoryAssign", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin, _globalInfo.RoleID);
            bool checkAssign = false;
            bool checkAdd = false;
            bool checkEdit = false;
            bool checkDelete = false;
            if (examinations != null && examGroup != null)
            {
                if (examinations.MarkInput.GetValueOrDefault() != true && examinations.MarkClosing.GetValueOrDefault() != true && _globalInfo.IsCurrentYear)
                {
                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE])
                    {
                        checkAssign = true;
                        checkAdd = true;
                    }

                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_UPDATE])
                    {
                        checkEdit = true;
                    }

                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE])
                    {
                        checkDelete = true;
                    }
                }
            }

            ViewData[ExamSupervisoryAssignConstants.PER_CHECK_ASSIGN] = checkAssign;
            ViewData[ExamSupervisoryAssignConstants.PER_CHECK_ADD] = checkAdd;
            ViewData[ExamSupervisoryAssignConstants.PER_CHECK_EDIT] = checkEdit;
            ViewData[ExamSupervisoryAssignConstants.PER_CHECK_DELETE] = checkDelete;
        }

        private void CheckExamSupervisoryPerRoomNum(AutoAssignViewModel form, List<ExamSupervisoryBO> listExamSupervisory, List<ExamSupervisoryAssignment> listAssignOfExam, List<ExamRoom> listRoomOfExam)
        {
            int supervisoryNumPerRoom = Convert.ToInt32(form.SupervisoryPerRoom);
            long examGroupID = form._ExamGroupID;

            //Lay danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = listRoomOfExam.Where(o => o.ExamGroupID == examGroupID)
                                                            .OrderBy(o => o.ExamRoomCode).ToList();

            //Kiem tra du lieu
            int minSupervisoryNum = listExamRoom.Count * supervisoryNumPerRoom;
            if (minSupervisoryNum > listExamSupervisory.Count)
            {
                throw new BusinessException(String.Format(@Res.Get("ExamSupervisoryAssign_Validate_NotEnoughSupervisory"), minSupervisoryNum));
            }
        }
        #endregion

    }
}
