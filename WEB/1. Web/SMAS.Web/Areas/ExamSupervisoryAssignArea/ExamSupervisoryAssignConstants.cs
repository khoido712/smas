﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryAssignArea
{
    public class ExamSupervisoryAssignConstants
    {
        //Viewdata
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string CBO_EXAM_GROUP_NEW = "cbo_exam_group_new";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string CBO_EXAM_SUBJECT = "cbo_exam_subject";
        public const string CBO_EXAM_SUBJECT_NEW = "cbo_exam_subject_new";
        public const string CBO_EXAMINATIONS_NEW = "cbo_examinations_new";
        public const string CBO_EXAM_ROOM = "cbo_exam_room";
        public const string LIST_EXAM_SUPERVISORY = "list_exam_supervisory";
        public const string INFO_TOTAL_ROOM = "info_total_room";
        public const string INFO_TOTAL_SUPERVISORY = "info_total_supervisory";
        public const string CBO_SWAP_EXAM_SUBJECT = "cbo_swap_exam_subject";
        public const string LIST_TEACHER = "list_teacher";

        public const int ASSIGN_SCALE_CURRENT_SUBJECT = 1;
        public const int ASSIGN_SCALE_ALL_SUBJECT = 2;
        public const int ASSIGN_SCALE_ALL_ROOM = 3;

        //Command permission
        public const string PER_CHECK_ASSIGN = "per_check_assign";
        public const string PER_CHECK_ADD = "per_check_add";
        public const string PER_CHECK_EDIT = "per_check_edit";
        public const string PER_CHECK_DELETE = "per_check_delete";
    }
}