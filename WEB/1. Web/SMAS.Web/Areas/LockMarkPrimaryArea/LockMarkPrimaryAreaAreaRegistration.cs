﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LockMarkPrimaryArea
{
    public class LockMarkPrimaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LockMarkPrimaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LockMarkPrimaryArea_default",
                "LockMarkPrimaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
