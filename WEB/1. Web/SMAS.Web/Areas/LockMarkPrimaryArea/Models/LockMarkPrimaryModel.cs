﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LockMarkPrimaryArea.Models
{
    public class LockMarkPrimaryModel
    {
        public int ClassID { get; set; }
        [ResourceDisplayName("Candidate_Label_Class")]
        public string ClassName { get; set; }
        
        public string StrLockMark { get; set; }
    }
}