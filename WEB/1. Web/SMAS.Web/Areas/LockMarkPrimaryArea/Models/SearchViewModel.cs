﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LockMarkPrimaryArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("LockedMarkDetail_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LockMarkPrimaryConstants.LS_EducationLevel)]
        [AdditionalMetadata("OnChange", "AjaxLoadClassByEduLevel()")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? cboEducationLevel { get; set; }
    }
}