﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Data.Objects.SqlClient;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using SMAS.Web.Areas.LockMarkPrimaryArea.Models;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.Web.Filter;
using SMAS.Business.Common;


namespace SMAS.Web.Areas.LockMarkPrimaryArea.Controllers
{
    public class LockMarkPrimaryController : BaseController
    {
        //
        // GET: /LockMarkPrimaryArea/LockMarkPrimary/
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ILockMarkPrimaryBusiness LockMarkPrimaryBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        public LockMarkPrimaryController(IClassProfileBusiness classProfileBusiness,
                                          ILockMarkPrimaryBusiness LockMarkPrimaryBusiness,
                                          IClassSubjectBusiness classSubjectBusiness,
                                          IMarkTypeBusiness markTypeBusiness,
                                          ISemeterDeclarationBusiness semeterDeclarationBusiness,
                                          IEducationLevelBusiness educationLevelBusiness,
                                          ISchoolProfileBusiness schoolProfileBusiness
                                         )
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.LockMarkPrimaryBusiness = LockMarkPrimaryBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.MarkTypeBusiness = markTypeBusiness;
            this.SemeterDeclarationBusiness = semeterDeclarationBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }
        public ActionResult Index()
        {
            SetViewData();
            GlobalInfo Global = new GlobalInfo();
            SetViewDataPermission("LockMarkPrimary", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return View();
        }

        public void SetViewData()
        {
            //Đổ dữ liệu vào EducationLevelcombobox
            IEnumerable<EducationLevel> lsEducationLevel = new GlobalInfo().EducationLevels;
            ViewData[LockMarkPrimaryConstants.LS_EducationLevel] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");



        }

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(int educationLevelID)
        {
            if (educationLevelID > 2)
            {
                ViewData[LockMarkPrimaryConstants.showColumn] = true;
            }
            else
            {
                ViewData[LockMarkPrimaryConstants.showColumn] = false;
            }
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Global.SchoolID;
            dic["AcademicYearID"] = Global.AcademicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["AppliedLevel"] = Global.AppliedLevel;
            //Danh sach khoa mon tinh diem
            List<LockMarkPrimary> lstLockMark = LockMarkPrimaryBusiness.SearchLockedMark(dic).Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK).ToList();
            //Danh sach khoa mon nhan xet
            List<LockMarkPrimary> lstLockJud = LockMarkPrimaryBusiness.SearchLockedMark(dic).Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();

            //Do du lieu len Grid
            List<LockMarkPrimaryModel> lstMark = new List<LockMarkPrimaryModel>();
            List<LockMarkPrimaryModel> lstJud = new List<LockMarkPrimaryModel>();
            //Danh sach lop
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dic).ToList();
            LockMarkPrimaryModel item = new LockMarkPrimaryModel();
            foreach (ClassProfile Class in lstClass)
            {
                item = new LockMarkPrimaryModel();
                item.ClassID = Class.ClassProfileID;
                item.ClassName = Class.DisplayName;
                item.StrLockMark = "";
                LockMarkPrimary lmp = lstLockMark.Where(o => o.ClassID == Class.ClassProfileID).FirstOrDefault();
                if (lmp != null && lmp.LockPrimary != null)
                {
                    item.StrLockMark = lmp.LockPrimary;
                }


                lstMark.Add(item);
            }
            ViewData[LockMarkPrimaryConstants.LIST_TD] = lstMark;

            //Mon nhan xet
            /*foreach (ClassProfile Class in lstClass)
            {
                item = new LockMarkPrimaryModel();
                item.ClassID = Class.ClassProfileID;
                item.ClassName = Class.DisplayName;
                item.StrLockMark = "";
                LockMarkPrimary lmp = lstLockJud.Where(o => o.ClassID == Class.ClassProfileID).FirstOrDefault();
                if (lmp != null && lmp.LockPrimary != null)
                {
                    item.StrLockMark = lmp.LockPrimary;
                }

                lstJud.Add(item);
            }
            ViewData[LockMarkPrimaryConstants.LIST_NX] = lstJud.ToList();*/
            return PartialView("_GridLockedMark");
        }

        [SkipCheckRole]
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult SaveLockedMarkDetail(int cboEducationLevel, string[] LockMarkCheckTD)
        {
            //var objWatch = new System.Diagnostics.Stopwatch();
           // objWatch.Start();
            // Kiem tra phan quyen giao vien
            if (GetMenupermission("LockMarkPrimary", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            // Kiem tra nam hoc hien tai
            if (!_globalInfo.IsCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }
            List<string> listTD = new List<string>();
            List<string> listNX = new List<string>();
            if (LockMarkCheckTD != null)
            {
                listTD = LockMarkCheckTD.ToList();
            }
            
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Global.SchoolID;
            dic["AcademicYearID"] = Global.AcademicYearID;
            dic["EducationLevelID"] = cboEducationLevel;
            dic["AppliedLevel"] = Global.AppliedLevel;
            //long lockId = LockMarkPrimaryBusiness.GetNextSeq<long>();
            //Xoa danh sach khoa diem cu
            List<LockMarkPrimary> lstLockOld = LockMarkPrimaryBusiness.SearchLockedMark(dic).ToList();
            /*if (lstLockOld != null && lstLockOld.Count > 0)
            {
                LockMarkPrimaryBusiness.DeleteAll(lstLockOld);
                LockMarkPrimaryBusiness.Save();
            }*/
            //Danh sach lop
            List<int> lstClass = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dic).Select(o => o.ClassProfileID).ToList();
            //Danh sach khoa diem
            List<LockMarkPrimary> lstLockMark = new List<LockMarkPrimary>();
            LockMarkPrimary lmpMark = new LockMarkPrimary();
            LockMarkPrimary lmpJud = new LockMarkPrimary();
            foreach (int ClassID in lstClass)
            {
                //TD
                lmpMark = new LockMarkPrimary();
                lmpMark.LockMarkPrimaryID=LockMarkPrimaryBusiness.GetNextSeq<int>();
                lmpMark.ClassID = ClassID;
                lmpMark.AcademicYearID = Global.AcademicYearID.Value;
                lmpMark.IsCommenting = SystemParamsInFile.ISCOMMENTING_TYPE_MARK;
                lmpMark.SchoolID = Global.SchoolID.Value;
                lmpMark.CreatedDate = DateTime.Now;


                string strClass = "_" + ClassID.ToString();
                //Mon tinh diem
                List<string> lstClassMark = listTD.Where(o => o.Contains(strClass)).ToList();
                foreach (string item in lstClassMark)
                {
                    string[] temp = item.Split('_');
                    if (temp != null && temp.Count() > 0)
                    {
                        if (temp.Count() <= 2)
                        {
                            lmpMark.LockPrimary += temp[0] + ",";
                        }
                        else
                        {
                            lmpMark.LockPrimary += temp[0] + "_" + temp[1] + ",";
                        }
                    }
                }

               
                // Neu co chon thon tin con diem khoa thi moi insert
                if (!string.IsNullOrWhiteSpace(lmpMark.LockPrimary))
                {
                    lstLockMark.Add(lmpMark);
                }
                
            }
            LockMarkPrimaryBusiness.InsertAndDelete(lstLockMark, lstLockOld);
            
            //objWatch.Stop();            
            return Json(new JsonMessage(Res.Get("LockedMarkDetail_Label_SaveSuc")));
        }
    }
}
