﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LockMarkPrimaryArea
{
    public class LockMarkPrimaryConstants
    {
        public const string LS_EducationLevel = "ListEducationLevel";
        public const string LIST_TD = "LIST_TD";
        public const string LIST_NX = "LIST_NX";
        public const string showColumn = "showColumn";
    }
}