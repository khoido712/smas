﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.SubCommitteeArea.Models;

namespace SMAS.Web.Areas.SubCommitteeArea.Controllers
{
    public class SubCommitteeController : BaseController
    {        
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
		
		public SubCommitteeController (ISubCommitteeBusiness subcommitteeBusiness)
		{
			this.SubCommitteeBusiness = subcommitteeBusiness;
		}
		
		//
        // GET: /SubCommittee/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            
            //Get view data here

            IEnumerable<SubCommitteeViewModel> lst = this._Search(SearchInfo);
            ViewData[SubCommitteeConstants.LIST_SUBCOMMITTEE] = lst;
           
            return View();
        }

		//
        // GET: /SubCommittee/Search

        
        public PartialViewResult Search(SubCommitteeSearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["Resolution"] = frm.Resolution;
			//add search info
			//

            IEnumerable<SubCommitteeViewModel> lst = this._Search(SearchInfo);
            ViewData[SubCommitteeConstants.LIST_SUBCOMMITTEE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(SubCommitteeViewModel subcommitteeViewModel)
        {
            SubCommittee subcommittee = new SubCommittee();
            TryUpdateModel(subcommittee); 
            Utils.Utils.TrimObject(subcommittee);

            subcommittee.SubCommitteeID = subcommitteeViewModel.SubCommitteeID;
            subcommittee.Resolution = subcommitteeViewModel.Resolution;
            subcommittee.Description = subcommitteeViewModel.Description;
            //subcommittee.Abbreviation =  subcommitteeViewModel.Abbreviation;
            subcommittee.IsActive =  true;

            this.SubCommitteeBusiness.Insert(subcommittee);
            this.SubCommitteeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int SubCommitteeID)
        {
            SubCommittee subcommittee = this.SubCommitteeBusiness.Find(SubCommitteeID);
            TryUpdateModel(subcommittee);
            Utils.Utils.TrimObject(subcommittee);
            this.SubCommitteeBusiness.Update(subcommittee);
            this.SubCommitteeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.SubCommitteeBusiness.Delete(id);
            this.SubCommitteeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<SubCommitteeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<SubCommittee> query = this.SubCommitteeBusiness.Search(SearchInfo).OrderBy(o => o.Resolution);
            IQueryable<SubCommitteeViewModel> lst = query.Select(o => new SubCommitteeViewModel {               
						SubCommitteeID = o.SubCommitteeID,								
						Resolution = o.Resolution,								
						Abbreviation = o.Abbreviation,								
						Description = o.Description,								
						CreatedDate = o.CreatedDate,								
						IsActive = o.IsActive,								
						ModifiedDate = o.ModifiedDate								
					
				
            });
            return lst.ToList();
        }        
    }
}





