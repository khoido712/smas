/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.SubCommitteeArea.Models
{
    public class SubCommitteeViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 SubCommitteeID { get; set; }
		
		[ResourceDisplayName("SubCommittee_Column_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Resolution { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("SubCommittee_Column_Abbreviation")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String Abbreviation { get; set; }

        [ResourceDisplayName("SubCommittee_Column_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		[DataType(DataType.MultilineText)]	
		public System.String Description { get; set; }	
							
        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> CreatedDate { get; set; }

        [ScaffoldColumn(false)]
		public System.Boolean IsActive { get; set; }

        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }								
	       
    }
}


