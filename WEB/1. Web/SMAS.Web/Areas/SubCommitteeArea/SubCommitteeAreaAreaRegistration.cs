﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SubCommitteeArea
{
    public class SubCommitteeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SubCommitteeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SubCommitteeArea_default",
                "SubCommitteeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
