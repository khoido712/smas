/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;
namespace SMAS.Web.Areas.PupilGraduationArea.Models
{
    public class GrantCertificationViewModel
    {
	public System.Int32 PupilGraduationID { get; set; }								
	public System.Int32 PupilID { get; set; }								
	public System.Int32 ClassID { get; set; }
        public string ShortName { get; set; }
        public int OrderInClass { get; set; }
        public int Status { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_DisplayName")]
        public System.String DisplayName { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_BirthDate")]
        public System.Nullable<System.DateTime> BirthDay { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_Genre")]
        public System.String GenreDisplay { get; set; }

        public System.Int32 Genre { get; set; }	

		public System.Int32 SchoolID { get; set; }								
		public System.Int32 AcademicYearID { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_Year")]				
		public System.Nullable<System.Int32> Year { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_Year")]
        public string YearDisplay { get; set; }

		public System.Int32 GraduationLevel { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_GraduationGrade")]			
		public System.Int32 GraduationGrade { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_IssuedNumber")]						
		public System.String IssuedNumber { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_RecordNumber")]				
		public System.String RecordNumber { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_IssuedDate")]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
		public System.Nullable<System.DateTime> IssuedDate { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_ReceivedDate")]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
		public System.Nullable<System.DateTime> ReceivedDate { get; set; }
								
		public System.Nullable<System.Int32> IsReceived { get; set; }								
		public System.String PriorityReason { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_Description")]
        public System.String Description { get; set; }

        [ResourceDisplayName("PupilGraduation_Label_Receiver")]
        public System.String Receiver { get; set; }		
    }
}
