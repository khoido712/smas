﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilGraduationArea.Models
{
    public class TemporaryGraduationPaperViewModel
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string ClassName { get; set; }
        public string GraduationType { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }
    }
}