/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PupilGraduationArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ClassProfile_Label_DisplayName")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilGraduationConstants.LIST_CLASS)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }
    }
}