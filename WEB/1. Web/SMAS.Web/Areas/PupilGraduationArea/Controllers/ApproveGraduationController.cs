using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.PupilGraduationArea.Models;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using Telerik.Web.Mvc;
using SMAS.VTUtils.Pdf;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace SMAS.Web.Areas.PupilGraduationArea.Controllers
{
    // Xét duyệt tốt nghiệp chỉ dành cho cấp 2 lớp 9
    public class ApproveGraduationController : BaseController
    {
        private IClassProfileBusiness ClassProfileBu;
        private IPupilOfClassBusiness PupilOfClassBu;
        private IPupilRankingBusiness PupilRankingBu;
        private IPupilGraduationBusiness PupilGraduationBu;
        private ISummedUpRecordBusiness SummedUpRecordBu;
        private IAcademicYearBusiness AcademicYearBu;
        private IReportDefinitionBusiness ReportDefinitionBu;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        private IGraduationApprovalBusiness GraduationApprovalBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        private ISupervisingDeptBusiness SupervisingDeptBusiness;
        private IReportDefinitionBusiness ReportDefinitionBusiness;
        public ApproveGraduationController(IClassProfileBusiness classProfileBu, IPupilOfClassBusiness pupilOfClassBu
                                            , IPupilRankingBusiness pupilRankingBu, IPupilGraduationBusiness pupilGraduationBu
                                            , ISummedUpRecordBusiness summedUpRecordBu, IAcademicYearBusiness academicYearBu
                                            , IReportDefinitionBusiness reportDefinitionBu
                                            , ISchoolProfileBusiness SchoolProfileBusiness
                                            , IPupilProfileBusiness PupilProfileBusiness,IGraduationApprovalBusiness GraduationApprovalBusiness,
                                              IUserAccountBusiness UserAccountBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness)
        {
            this.ClassProfileBu = classProfileBu;
            this.PupilOfClassBu = pupilOfClassBu;
            this.PupilRankingBu = pupilRankingBu;
            this.PupilGraduationBu = pupilGraduationBu;
            this.SummedUpRecordBu = summedUpRecordBu;
            this.AcademicYearBu = academicYearBu;
            this.ReportDefinitionBu = reportDefinitionBu;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
             this.GraduationApprovalBusiness = GraduationApprovalBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }

        public ActionResult _Index()
        {
            GlobalInfo glo = new GlobalInfo();
            if (glo.AppliedLevel.Value != SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                throw new BusinessException(Res.Get("PupilGraduation_Label_MustBeAppliedLevelSecondary"));
            GetViewData();
            return PartialView();
        }

        [HttpPost]
        public PartialViewResult Search(SearchViewModel model)
        {
            //Utils.Utils.TrimObject(model);
            //ViewData[PupilGraduationConstants.LIST_APPROVE_GRADUATION] = SearchData(model, true);
            //ViewData[PupilGraduationConstants.APPROVE_GRADUATION_PERMISSION] = this.GetMenupermission("ApproveGraduation", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            //return PartialView("_List", model);
			Utils.Utils.TrimObject(model);
            var dataSearch = SearchData(model,null, true);
            if (dataSearch != null && dataSearch.Count > 0)
            {
                var year = dataSearch.FirstOrDefault().Year;               
                GlobalInfo glo = new GlobalInfo();
                var objGraduationApproval = GraduationApprovalBusiness.All
                                                                      .Where(x => x.SchoolID == glo.SchoolID.Value
                                                                                 && x.AcademicYearID == glo.AcademicYearID
                                                                                 && x.Year == year)
                                                                      .FirstOrDefault();
                if (objGraduationApproval != null)
                {
                    var _NotSubmit = Res.Get("Graduation_NotStubmit");
                    var _WaitingApprove = Res.Get("Graduation_WaitingApprove");
                    var _CancelApprove = Res.Get("Graduation_CancelApprove");
                    var _Approved = Res.Get("Graduation_Approved");
                    dataSearch[0].GraduationApprovalStatus = objGraduationApproval.Status;
                    dataSearch[0].GraduationApprovalUserApprove = objGraduationApproval.UserApprove;
                    UserAccount SupervisingDept = UserAccountBusiness.All.Where(x => x.UserAccountID == objGraduationApproval.UserApprove && x.IsActive == true).FirstOrDefault();
                    if (SupervisingDept != null)
                    {
                        dataSearch[0].GraduationApprovalUserApproveName = SupervisingDept.aspnet_Users.UserName;
                    }
                    dataSearch[0].GraduationApprovalStatusName = objGraduationApproval == null || objGraduationApproval.Status == 0 ? _NotSubmit
                        : (objGraduationApproval.Status == SystemParamsInFile.GRADUCATION_STATUS_WAITING ? _WaitingApprove : (objGraduationApproval.Status == SystemParamsInFile.GRADUCATION_STATUS_APPROVED
                        ? _Approved : objGraduationApproval.Status == SystemParamsInFile.GRADUCATION_STATUS_CANCEL ? _CancelApprove 
                        : _NotSubmit));
                }
            }
            foreach (var item in dataSearch)
            {
                if ((item.CapacityLevelID != null && item.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK) && (item.ConductLevelID == null || item.ConductLevelID == 0 || item.ConductLevelID != SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY))
                    item.EnableCheckBoxPriority = true;
            }

            string SubName = "";
            var schoolProfile = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            var SubSuperVising = SupervisingDeptBusiness.All.Where(x => x.ParentID == schoolProfile.SupervisingDeptID && x.DistrictID == schoolProfile.DistrictID).FirstOrDefault();
            if (SubSuperVising != null)
            {
                SubName = SubSuperVising.SupervisingDeptName;
            }
            ViewData["SubName"] = SubName;
            ViewData[PupilGraduationConstants.LIST_APPROVE_GRADUATION] = dataSearch;
            ViewData[PupilGraduationConstants.APPROVE_GRADUATION_PERMISSION] = this.GetMenupermission("ApproveGraduation", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            return PartialView("_List", model);
        }

        [HttpPost]
        public PartialViewResult SearchTemporaryGraduationPaper(SearchViewModel model)
        {
            Utils.Utils.TrimObject(model);           
            List<TemporaryGraduationPaperViewModel> lstResult = SearchDataTemporaryGraduationPaper(model, true);
            Session[PupilGraduationConstants.LIST_RESULT_TEMPORARY_GRADUATION_PAPER] = lstResult;

            if (model.ClassID.HasValue)
            {
                ViewData[PupilGraduationConstants.LIST_TEMPORARY_GRADUATION_PAPER] = lstResult;
            }
            else
            {
                List<TemporaryGraduationPaperViewModel> lstPage = lstResult.Skip(0).Take(15).ToList();
                ViewData[PupilGraduationConstants.LIST_TEMPORARY_GRADUATION_PAPER] = lstResult;
            }

            ViewData[PupilGraduationConstants.TOTAL] = lstResult.Count;
            ViewData[PupilGraduationConstants.PAGE_NUMBER] = 1;          

            return PartialView("_ListTemporaryGraduation", model);
        }

        private List<TemporaryGraduationPaperViewModel> SearchDataTemporaryGraduationPaper(SearchViewModel frm, bool check)
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = frm.ClassID;
            dic["AcademicYearID"] = glo.AcademicYearID.Value;
            dic["SchoolID"] = glo.SchoolID.Value;
            dic["PupilCode"] = frm.PupilCode;
            dic["FullName"] = frm.FullName;
            dic["Check"] = "Check";
            dic["GraduationLevel"] = 2; // Cap tot nghiep
            dic.Add("EducationLevelID", SystemParamsInFile.EDUCATION_LEVEL_NINTH); //Lop 9
            List<PupilGraduationBO> lstPupilGraduation = PupilGraduationBu.SearchPupilGraduation(glo.SchoolID.Value, glo.AcademicYearID.Value, 2, dic);
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(glo.SchoolID);

            List<TemporaryGraduationPaperViewModel> lstResult = new List<TemporaryGraduationPaperViewModel>();
            TemporaryGraduationPaperViewModel objResult = null;
            foreach (var pg in lstPupilGraduation)
            {
                objResult = new TemporaryGraduationPaperViewModel();
                objResult.PupilID = pg.PupilID;
                objResult.FullName = pg.FullName;
                objResult.Name = pg.Name;
                objResult.OrderInClass = pg.OrderInClass;
                objResult.PupilCode = pg.PupilCode;
                objResult.ClassName = pg.ClassName;
                objResult.GraduationType = pg.GraduationGrade == 1 ? "Giỏi" : pg.GraduationGrade == 2 ? "Khá" : pg.GraduationGrade == 3 ? "Trung bình khá" : pg.GraduationGrade == 4 ? "Trung bình" : "";
                lstResult.Add(objResult);
            }

            if (lstResult.Count > 0 && lstResult != null)
            {
                if (frm.ClassID > 0)
                {
                    lstResult = lstResult.OrderBy(p => p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                }
                else
                {
                    lstResult = lstResult.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                }
            }

            return lstResult;
        }

        /// <summary>
        /// Tìm kiếm có phân trang
        /// </summary>
        /// 
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(GridCommand command)
        {
            int Page = 1;
            if (command != null)
                Page = command.Page;

            List<TemporaryGraduationPaperViewModel> lst;
            if (Session["LIST_RESULT_TEMPORARY_GRADUATION_PAPER"] != null)
            {
                lst = (List<TemporaryGraduationPaperViewModel>)Session["LIST_RESULT_TEMPORARY_GRADUATION_PAPER"];
            }
            else
            {
                SearchViewModel model = new SearchViewModel();
                lst = this.SearchDataTemporaryGraduationPaper(model, true);
            }

            List<TemporaryGraduationPaperViewModel> lstAtCurrentPage = lst.Skip((Page - 1) * 15).Take(15).ToList();
            ViewData[PupilGraduationConstants.LIST_TEMPORARY_GRADUATION_PAPER] = lstAtCurrentPage;
            GridModel<TemporaryGraduationPaperViewModel> gm = new GridModel<TemporaryGraduationPaperViewModel>(lstAtCurrentPage);
            gm.Total = lst.Count();
            return View(gm);
        }

        #region ExcelTemporaryGraduationPaper
        [ValidateAntiForgeryToken]
        public JsonResult GetTemporaryGraduationPaperReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            string pupilCode = !string.IsNullOrEmpty(frm["PupilCode"]) ? frm["PupilCode"] : "";
            string fullName = !string.IsNullOrEmpty(frm["FullName"]) ? frm["FullName"] : "";
            string content = !string.IsNullOrEmpty(frm["idContent"]) ? frm["idContent"] : "";
            string arrayPupilID = !string.IsNullOrEmpty(frm["ArrayPupilID"]) ? frm["ArrayPupilID"] : "";
            int valueCheckAllOnePage = !string.IsNullOrEmpty(frm["checkAllOnePage"]) ? int.Parse(frm["checkAllOnePage"]) : 0;
            int valueCheckAllClass = !string.IsNullOrEmpty(frm["checkAllClass"]) ? int.Parse(frm["checkAllClass"]) : 0;

            bool checkAllOnePage = valueCheckAllOnePage == 1 ? true: false;
            bool checkAllClass = valueCheckAllClass == 1? true: false;

            List<int> lstPupilID = new List<int>();
            if (!checkAllClass && (!string.IsNullOrEmpty(arrayPupilID)))
            {
                lstPupilID = arrayPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Int32.Parse(x)).ToList();             
            }        

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.GiayTotNghiepTamThoi);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"Content", content},
                {"ClassID", classID},
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"SchoolID", _globalInfo.SchoolID},
                {"PupilCode", pupilCode},
                {"FullName", fullName},
                {"Check", "Check"},
                {"checkAllOnePage", valueCheckAllOnePage},
                {"checkAllClass", valueCheckAllClass},
                {"lstPupilID", lstPupilID},
            };           

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBu.GetTemporaryGraduationPaperReport(dic, SystemParamsInFile.GiayTotNghiepTamThoi);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = PupilOfClassBu.ExportTemporaryGraduationPaper(dic);
                processedReport = PupilOfClassBu.InsertTemporaryGraduationPaperReport(dic, excel, SystemParamsInFile.GiayTotNghiepTamThoi);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;

            int? classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            string pupilCode = !string.IsNullOrEmpty(frm["PupilCode"]) ? frm["PupilCode"] : "";
            string fullName = !string.IsNullOrEmpty(frm["FullName"]) ? frm["FullName"] : "";
            string content = !string.IsNullOrEmpty(frm["idContent"]) ? frm["idContent"] : "";
            string arrayPupilID = !string.IsNullOrEmpty(frm["ArrayPupilID"]) ? frm["ArrayPupilID"] : "";
            int valueCheckAllOnePage = !string.IsNullOrEmpty(frm["checkAllOnePage"]) ? int.Parse(frm["checkAllOnePage"]) : 0;
            int valueCheckAllClass = !string.IsNullOrEmpty(frm["checkAllClass"]) ? int.Parse(frm["checkAllClass"]) : 0;

            bool checkAllOnePage = valueCheckAllOnePage == 1 ? true : false;
            bool checkAllClass = valueCheckAllClass == 1 ? true : false;

            List<int> lstPupilID = new List<int>();
            if (!checkAllClass && (!string.IsNullOrEmpty(arrayPupilID)))
            {
                lstPupilID = arrayPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Int32.Parse(x)).ToList();
            }
          
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"Content", content},
                {"ClassID", classID},
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"SchoolID", _globalInfo.SchoolID},
                {"PupilCode", pupilCode},
                {"FullName", fullName},
                {"Check", "Check"},
                {"checkAllOnePage", valueCheckAllOnePage},
                {"checkAllClass", valueCheckAllClass},
                {"lstPupilID", lstPupilID},
            };   

            Stream excel = PupilOfClassBu.ExportTemporaryGraduationPaper(dic);
            processedReport = PupilOfClassBu.InsertTemporaryGraduationPaperReport(dic, excel, SystemParamsInFile.GiayTotNghiepTamThoi);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.GiayTotNghiepTamThoi,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.GiayTotNghiepTamThoi,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Save(SearchViewModel model, FormCollection form)
        {
            GlobalInfo glo = new GlobalInfo();
            if (!glo.IsCurrentYear)
            {
                throw new BusinessException(Res.Get("Common_Validate_NotCurrentYear"));
            }
            int permission = this.GetMenupermission("ApproveGraduation", glo.UserAccountID, glo.IsAdmin);

            if (permission < SystemParamsInFile.PERMISSION_LEVEL_CREATE)
                return Json(new JsonMessage(Res.Get("Validate_Permission_Teacher"), JsonMessage.ERROR));

            if (glo.AppliedLevel.Value != SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                throw new BusinessException(Res.Get("PupilGraduation_Label_MustBeAppliedLevelSecondary"));

            //Kiem tra dieu kien cua cac hoc sinh
            List<PupilGraduation> lstPupilGraduation = new List<PupilGraduation>();

            // Danh sách học sinh thuộc trường - là cấp 2 và lớp 9
            IEnumerable<PupilGraduationBO> lst = SearchData(model,null, false);

            // Danh sách được xét duyệt tốt nghiệp
            // Học sinh chưa tốt nghiệp
            // Học sinh chưa nhận bằng
            var validPupil = lst.Where(u => (!u.GraduationStatus.HasValue || u.GraduationStatus.Value != SystemParamsInFile.GRADUATION_STATUS_GRADUATED)
                                            && (!u.IsReceived.HasValue || u.IsReceived.Value == 0));

            int year = this.AcademicYearBu.Find(glo.AcademicYearID).Year;

            if (validPupil.Any(u => !string.IsNullOrEmpty(form.Get("chkPriority_" + u.PupilID)) && string.IsNullOrEmpty(form.Get("PriorityReason_" + u.PupilID))))
                return Json(new JsonMessage(string.Format(Res.Get("Common_Validate_Required"), Res.Get("PupilGraduation_Label_PriorityReason"))), JsonMessage.ERROR);

            #region Check dieu kien xet duyet cua hoc sinh
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(glo.SchoolID);
            bool checkGDTX = schoolProfile.TrainingTypeID.HasValue ? schoolProfile.TrainingType.Resolution == "GDTX" : false;
            List<string> lstNotification = null;
            foreach (PupilGraduationBO pupil in validPupil)
            {
                if (!string.IsNullOrEmpty(form.Get("Pupil_" + pupil.PupilID)))//Hoc sinh duoc tich chon
                {
                    lstNotification = new List<string>();

                    // Check co phai la GDTX ko
                    //Kiem tra hoc sinh theo schoolID - kieu truong se co GDTX
                    // Tim kiem trong SchoolProfile de biet duoc truong co day chuong trinh GDTX hay khong?

                    // Dùng chung 2 điều kiện
                    // Học sinh học hết chương trình bổ túc từ 15 tuổi trở lên và THCS - Bo dieu kien nay
                    //if (checkGDTX && DateTime.Now.AddYears(-15) < pupil.BirthDate)
                    //    lstNotification.Add(Res.Get("PupilGraduation_Error_PupilNotOldEnough"));
                    // Học sinh có số buổi nghỉ học trong lớp 9 không quá 45 buổi
                    if (pupil.AbsentDays.HasValue && pupil.AbsentDays.Value > 45)
                        lstNotification.Add(Res.Get("PupilGraduation_Error_PupilAbsentExceed"));

                    if (PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, glo.AppliedLevel.Value, pupil.PupilLearningType, pupil.BirthDate)) //Học sinh thuộc diện phải xếp loại hạnh kiểm
                    {
                        if (!pupil.CapacityLevelID.HasValue)
                            lstNotification.Add(Res.Get("PupilGraduation_Error_CapacityNotRanked"));
                    }
                    else
                    {
                        // học sinh chưa được xếp loại hạnh kiểm và học lực
                        if (!pupil.ConductLevelID.HasValue && !pupil.CapacityLevelID.HasValue)
                        {
                            lstNotification.Add(Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked"));
                        }
                        else
                        {
                            if (!pupil.ConductLevelID.HasValue)
                                lstNotification.Add(Res.Get("PupilGraduation_Error_ConductNotRanked"));

                            if (!pupil.CapacityLevelID.HasValue)
                                lstNotification.Add(Res.Get("PupilGraduation_Error_CapacityNotRanked"));
                        }

                        if (pupil.ConductLevelID.HasValue && pupil.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY)
                            lstNotification.Add(Res.Get("PupilGraduation_Error_ConductNotSatisfy"));
                    }

                    // Dùng chung điều kiện cho các học sinh thuộc trường THCS và GDTX
                    // Xếp loại học lực cả năm lớp 9 từ trung bình trở lên, nếu không thuộc đối tượng được hưởng chính sách ưu tiên, khuyến khích
                    // Xếp loại học lực yếu nhưng các môn học tính điểm TBM đều đạt từ 3,5 điểm trở lên, trong đó môn toán hoặc môn ngữ văn đạt từ 5,0 điểm trở lên,
                    // nếu thuộc đối tượng được hưởng chính sách ưu tiên, khuyến khích
                    if (pupil.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                    {
                        lstNotification.Add(Res.Get("PupilGraduation_Error_CapacityNotSatisfy"));
                    }
                    else
                    {
                        if (pupil.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK)
                        {
                            if (!string.IsNullOrEmpty(form.Get("chkPriority_" + pupil.PupilID))) //Hoc sinh duoc huong uu tien
                            {
                                var lstSummedUpMark = this.SummedUpRecordBu.All
                                                                    .Where(u => u.SchoolID == glo.SchoolID.Value
                                                                            && u.AcademicYearID == glo.AcademicYearID.Value
                                                                            && u.ClassID == pupil.ClassID
                                                                            && u.PupilID == pupil.PupilID
                                                                            && u.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                                            && u.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                                                    .Select(u => new { SummedUpMark = u.SummedUpMark, DisplayName = u.SubjectCat.DisplayName, IsCheck = u.SubjectCat.IsCoreSubject })
                                                                    .ToList();

                                List<string> coreSubjects = new List<string>();
                                List<string> notCoreSubjects = new List<string>();

                                if (!lstSummedUpMark.Where(u => u.IsCheck).Any(u => u.SummedUpMark.HasValue && u.SummedUpMark >= 5)) //Neu khong co mon chinh nao lon hon 5 diem
                                    lstSummedUpMark.Where(u => u.IsCheck && (!u.SummedUpMark.HasValue || u.SummedUpMark < 5)).ToList().ForEach(u => coreSubjects.Add(u.DisplayName));

                                if (lstSummedUpMark.Any(u => !u.SummedUpMark.HasValue || u.SummedUpMark.Value < (decimal)3.5)) //neu co mon binh thuong diem duoi 3.5
                                    lstSummedUpMark.Where(u => (!u.SummedUpMark.HasValue || u.SummedUpMark < (decimal)3.5)).ToList().ForEach(u => notCoreSubjects.Add(u.DisplayName));

                                if (coreSubjects.Count > 0)
                                    lstNotification.Add(string.Format(Res.Get("PupilGraduation_Error_SummedMarkNotSatisfy"), string.Join(",", coreSubjects)));

                                else if (notCoreSubjects.Count > 0)
                                    lstNotification.Add(string.Format(Res.Get("PupilGraduation_Error_SummedMarkNotSatisfy"), string.Join(",", notCoreSubjects)));
                            }
                            else
                            {
                                lstNotification.Add(Res.Get("PupilGraduation_Error_CapacityNotSatisfy"));
                            }
                        }
                    }

                    pupil.Notification = string.Join(",", lstNotification);
                    pupil.GraduationStatus = string.IsNullOrEmpty(pupil.Notification) ? SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT : SystemParamsInFile.GRADUATION_STATUS_APPROVE_INSUFFICIENT;

                    PupilGraduation pg = new PupilGraduation();
                    pg.AcademicYearID = glo.AcademicYearID.Value;
                    pg.ClassID = pupil.ClassID;
                    pg.GraduationGrade = pupil.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT ? GetGraduationGrade(pupil.ConductLevelID.HasValue ? pupil.ConductLevelID.Value : 0, pupil.CapacityLevelID.Value, checkGDTX) : (int)0;
                    pg.Status = pupil.GraduationStatus;
                    pg.GraduationLevel = (int)glo.AppliedLevel.Value;
                    pupil.PriorityReason = pg.PriorityReason = !string.IsNullOrEmpty(form.Get("chkPriority_" + pupil.PupilID)) && !string.IsNullOrEmpty(form.Get("PriorityReason_" + pupil.PupilID)) ? form.Get("PriorityReason_" + pupil.PupilID).Trim() : "";
                    pg.PupilID = pupil.PupilID;
                    pg.SchoolID = glo.SchoolID.Value;
                    pg.Year = year;
                    pg.Description = pupil.Notification;

                    pupil.GraduationGrade = pg.GraduationGrade;
                    lstPupilGraduation.Add(pg);
                }
            }
            #endregion

            if (lstPupilGraduation.Count > 0)
            {
                this.PupilGraduationBu.InsertPupilGraduation(lstPupilGraduation);
                this.PupilGraduationBu.Save();
            }

            return Json(new JsonMessage(Res.Get("ApproveGraduation_Label_ApproveSuccess")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int[] PupilIDs)
        {
            GlobalInfo glo = new GlobalInfo();
            int permission = this.GetMenupermission("ApproveGraduation", glo.UserAccountID, glo.IsAdmin);

            if (permission < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (PupilIDs == null || PupilIDs.Length == 0)
                return Json(new JsonMessage(Res.Get("Common_Validate_NoPupilSelected"), JsonMessage.ERROR));

            if (!glo.IsCurrentYear)
            {
                throw new BusinessException(Res.Get("Common_Validate_NotCurrentYear"));
            }
            IEnumerable<PupilGraduationBO> lst = SearchData(new SearchViewModel(),PupilIDs.ToList(), false);
            if (lst.Any(u =>                                                                                            //neu ton tai hoc sinh
                            PupilIDs.Any(v => v == u.PupilID)                                                           //Duoc tick chon
                            && (!u.PupilGraduationID.HasValue || u.PupilGraduationID.Value == 0                         //Nhung chua duoc xep hang
                                || (u.PupilGraduationID.HasValue && u.IsReceived.HasValue && u.IsReceived.Value == SystemParamsInFile.GRADUAION_RECEIVED))  //Hoac da xep hang va da nhan bang
                ))
            {
                return Json(new JsonMessage(Res.Get("ApproveGraduation_Label_DeletePupilNotApproved"), JsonMessage.ERROR));
            }
            else
            {
                List<int> listPupilGraduationID = new List<int>();
                foreach (PupilGraduationBO pupil in lst)
                {
                    if (pupil.PupilGraduationID.HasValue && pupil.PupilGraduationID.Value > 0 && (!pupil.IsReceived.HasValue || pupil.IsReceived.Value != SystemParamsInFile.GRADUAION_RECEIVED) && PupilIDs.Any(v => v == pupil.PupilID))//Hoc sinh duoc tich chon
                    {
                        listPupilGraduationID.Add(pupil.PupilGraduationID.Value);
                    }
                }
                if (listPupilGraduationID.Count > 0)
                {
                    List<PupilGraduation> listPupilGraduation = PupilGraduationBu.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object>(){
                        {"AcademicYearID",glo.AcademicYearID}
                    }).Where(o => listPupilGraduationID.Contains(o.PupilGraduationID)).ToList();
                    PupilGraduationBu.DeletePupilGraduation(listPupilGraduation);
                    PupilGraduationBu.Save();
                }
            }
            return Json(new JsonMessage(Res.Get("ApproveGraduation_Label_DeleteSuccess")));
        }

        private List<PupilGraduationBO> SearchData(SearchViewModel frm,List<int> lstPupilID, bool check)
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = frm.ClassID;
            dic["AcademicYearID"] = glo.AcademicYearID.Value;
            dic["PupilCode"] = frm.PupilCode;
            dic["FullName"] = frm.FullName;
            dic["Check"] = "Check";
            dic["lstPupilID"] = lstPupilID;
            dic.Add("EducationLevelID", SystemParamsInFile.EDUCATION_LEVEL_NINTH); //Lop 9
            List<PupilGraduationBO> lstApproveGrd = PupilGraduationBu.GetApprovePupil(glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, dic);
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(glo.SchoolID);
            bool checkGDTX = schoolProfile.TrainingTypeID.HasValue ? schoolProfile.TrainingType.Resolution == "GDTX" : false;
            foreach (var pg in lstApproveGrd)
            {
                // Trường hợp thỏa mãn các điều kiện thì được hiển thị ô lựa chọn, set giá trị disabled checkbox = false
                pg.chkDisabled = false;
                pg.Notification = pg.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT || pg.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_GRADUATED ? string.Empty : pg.Description;
                // End set disabled
                if (check)
                {
                    if (PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, glo.AppliedLevel.Value, pg.PupilLearningType, pg.BirthDate)) //Hoc sinh khong phai xet hanh kiem
                    {
                        if (!pg.CapacityLevelID.HasValue)
                        {
                            pg.Notification = Res.Get("PupilGraduation_Error_CapacityNotRanked") + ", ";
                            pg.chkDisabled = true;
                        }
                    }
                    else //Hoc sinh phai xet hanh kiem
                    {
                        if (!pg.ConductLevelID.HasValue && !pg.CapacityLevelID.HasValue)
                        {
                            pg.Notification = Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked") + ", ";
                            pg.chkDisabled = true;
                        }
                        else
                        {
                            if (!pg.ConductLevelID.HasValue)
                            {
                                pg.Notification = Res.Get("PupilGraduation_Error_ConductNotRanked") + ", ";
                                pg.chkDisabled = true;
                            }

                            if (!pg.CapacityLevelID.HasValue)
                            {
                                pg.Notification = Res.Get("PupilGraduation_Error_CapacityNotRanked") + ", ";
                                pg.chkDisabled = true;
                            }
                        }
                    }
                }
                pg.StatusString = pg.GraduationStatus.HasValue ? CommonList.GraduationStatus().Where(u => u.key.Equals(pg.GraduationStatus.Value.ToString())).SingleOrDefault().value : string.Empty;
            }
            if (lstApproveGrd.Count > 0 && lstApproveGrd != null)
            {
                if (frm.ClassID > 0)
                {
                    lstApproveGrd = lstApproveGrd.OrderBy(p => p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                }
                else
                {
                    lstApproveGrd = lstApproveGrd.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                }
            }

            return lstApproveGrd;
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            var lstClass = ClassProfileBu.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", glo.AcademicYearID.Value }, { "EducationLevelID", SystemParamsInFile.EDUCATION_LEVEL_NINTH } });
            ViewData[PupilGraduationConstants.LIST_CLASS] = new SelectList(lstClass.OrderBy(u => u.DisplayName).ToList(), "ClassProfileID", "DisplayName");
        }

        private int GetGraduationGrade(int? conductLevel, int capacityLevel, bool checkGDTX)
        {
            int retVal = 0;


            if (checkGDTX && conductLevel.Value == 0)
            {

                //Loại giỏi: hạnh kiểm loại tốt, học lực loại giỏi
                if (capacityLevel == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT)
                    retVal = SystemParamsInFile.GRADUATION_GRADE_EXCELLENT;
                //Loại khá: hạnh kiểm từ loại khá trở lên, học lực loại khá hoặc hạnh kiểm loại khá, học lực loại giỏi;
                else if (
                    //hoc luc kha hoac gioi
                    (capacityLevel == SystemParamsInFile.CAPACITY_TYPE_GOOD)
                    //Hoc luc kha  kha hoac gioi
                    || capacityLevel == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT)
                {
                    retVal = SystemParamsInFile.GRADUATION_GRADE_GOOD;
                }
                else retVal = SystemParamsInFile.GRADUATION_GRADE_FAIR;
            }
            else
            {
                //Loại giỏi: hạnh kiểm loại tốt, học lực loại giỏi
                if (conductLevel.Value == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY && capacityLevel == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT)
                    retVal = SystemParamsInFile.GRADUATION_GRADE_EXCELLENT;
                //Loại khá: hạnh kiểm từ loại khá trở lên, học lực loại khá hoặc hạnh kiểm loại khá, học lực loại giỏi;
                else if (
                    //Hanh kiem kha va hoc luc kha hoac gioi
                    (conductLevel.Value == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY
                        && (capacityLevel == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT
                            || capacityLevel == SystemParamsInFile.CAPACITY_TYPE_GOOD))
                    //Hoc luc kha va hanh kiem kha hoac gioi
                    || (capacityLevel == SystemParamsInFile.CAPACITY_TYPE_GOOD
                        && (conductLevel.Value == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY
                            || conductLevel.Value == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY))
                    )
                {
                    retVal = SystemParamsInFile.GRADUATION_GRADE_GOOD;
                }
                else retVal = SystemParamsInFile.GRADUATION_GRADE_FAIR;
            }

            return retVal;
        }

        public FileStreamResult Export(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            Utils.Utils.TrimObject(model);

            ClassProfile classProfile = ClassProfileBu.Find(model.ClassID);
            AcademicYear acaYear = AcademicYearBu.Find(glo.AcademicYearID);
            Province province = acaYear.SchoolProfile.Province;
            SchoolProfile objSP = SchoolProfileBusiness.Find(glo.SchoolID);

            List<PupilGraduationBO> listApprovePupil = SearchData(model,null, true);

            ReportDefinition reportDefinition = ReportDefinitionBu.GetByCode(SystemParamsInFile.HS_THCS_DSXETDUYETTOTNGHIEP);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            string reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet oSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            IVTRange topRow = tempSheet.GetRange("A1", "M1");
            IVTRange midRow = tempSheet.GetRange("A3", "M3");
            IVTRange botRow = tempSheet.GetRange("A5", "M5");

            int START_ROW_INDEX = 10;
            for (int i = 0; i < listApprovePupil.Count; i++)
            {
                IVTRange range = null;
                if (i % 5 == 4) range = botRow;
                else if (i % 5 == 0) range = topRow;
                else range = midRow;

                oSheet.CopyPaste(range, START_ROW_INDEX + i, 1, true);
                if ((i + 1) == listApprovePupil.Count)
                {
                    range = botRow;
                    oSheet.CopyPaste(range, START_ROW_INDEX + i, 1, true);
                }
            }

            oSheet.CopyPaste(botRow, START_ROW_INDEX + listApprovePupil.Count - 1, 1, true);

            Dictionary<string, object> dicVarable = new Dictionary<string, object>();
            List<object> listRow = new List<object>();

            int index = 1;
            listApprovePupil.ForEach(u =>
            {
                ComboObject gg = u.GraduationGrade.HasValue ? CommonList.GraduationGrade().Where(v => v.key == u.GraduationGrade.Value.ToString()).SingleOrDefault() : null;
                ComboObject gs = u.GraduationStatus.HasValue ? CommonList.GraduationStatus().Where(v => v.key == u.GraduationStatus.Value.ToString()).SingleOrDefault() : null;
                listRow.Add(new
                {
                    Index = index++,
                    FullName = u.FullName,
                    Genre = u.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female"),
                    BirthDate = u.BirthDate.ToString("dd/MM/yyyy"),
                    BirthPlace = u.BirthPlace,
                    EthnicName = u.EthnicName,
                    ClassName = u.ClassName,
                    CapacityLevel = u.CapacityLevel,
                    ConductLevel = u.ConductLevel,
                    AbsentDays = u.AbsentDays,
                    PriorityReason = u.PriorityReason,
                    GraduationGrade = gg != null ? gg.value : "",
                    Notification = u.Notification
                });
            });

            dicVarable["Rows"] = listRow;
            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(glo.SchoolID.Value,glo.AppliedLevel.Value);
            dicVarable["SchoolName"] = glo.SchoolName;
            dicVarable["ProvinceDate"] = ((objSP.District != null ? objSP.District.DistrictName + ", " : "")) + "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["ClassName"] = classProfile != null ? "Lớp " + classProfile.DisplayName + " - " : "";
            oSheet.FillVariableValue(dicVarable);

            if (listRow.Count == 0)
                oSheet.DeleteRow(START_ROW_INDEX);

            tempSheet.Delete();

            reportName = classProfile != null ? reportName.Replace("[ClassName]", classProfile.DisplayName) : reportName.Replace("_[ClassName]", "");
            Stream stream = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
            result.FileDownloadName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            return result;
        }

        public FileStreamResult ExportApproveGraduationProfile(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            Utils.Utils.TrimObject(model);

            ClassProfile classProfile = ClassProfileBu.Find(model.ClassID);
            AcademicYear acaYear = AcademicYearBu.Find(glo.AcademicYearID);
            Province province = acaYear.SchoolProfile.Province;
            SchoolProfile objSP = SchoolProfileBusiness.Find(glo.SchoolID);

            List<PupilGraduationBO> listApprovePupil = SearchData(model,null, true);
            List<PupilGraduationBO> listApprovePupilPriority = new List<PupilGraduationBO>();
            listApprovePupilPriority = listApprovePupil.Where(x => !string.IsNullOrEmpty(x.PriorityReason)).ToList();
            listApprovePupil = listApprovePupil.Where(x => !listApprovePupilPriority.Select(y => y.PupilID).Contains(x.PupilID)).ToList();
            ReportDefinition reportDefinition = ReportDefinitionBu.GetByCode(SystemParamsInFile.REPORT_GRADUATION_APPROVAL);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            string reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = model.ClassID;
            Stream stream = GraduationApprovalBusiness.ExportApproveGraduationProfile(oBook, listApprovePupil, listApprovePupilPriority, objSP, acaYear, dic);
            FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
            result.FileDownloadName = reportDefinition.TemplateName;
            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateOrUpdateApproval(int Year, bool IsCancel)
        {
            if (Year == 0)
            {
                return Json(new JsonMessage(Res.Get("Graduation_Stubmit_Error")));
            }
            GlobalInfo glo = new GlobalInfo();
            var GraduationApprovalItem = new GraduationApproval();
            GraduationApprovalItem.Year = Year;
            GraduationApprovalItem.AcademicYearID = glo.AcademicYearID.Value;
            GraduationApprovalItem.SchoolID = (int)glo.SchoolID;
            GraduationApprovalItem.Status = 1;
            GraduationApprovalItem.UserCreate = glo.UserAccountID;
            if (IsCancel)
            {
                GraduationApprovalItem.Status = -1;
            }
            var status = GraduationApprovalBusiness.CreateOrUpdateGraduationApproval(GraduationApprovalItem);
            if (string.IsNullOrEmpty(status))
            {
                if (IsCancel)
                    status = "Graduation_Cancel_Error";
                else
                    status = "Graduation_Stubmit_Error";
            }
            return Json(new JsonMessage(Res.Get(status)));
        }
    }
}
