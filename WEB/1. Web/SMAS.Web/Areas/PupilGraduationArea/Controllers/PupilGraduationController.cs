/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PupilGraduationArea.Models;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;


namespace SMAS.Web.Areas.PupilGraduationArea.Controllers
{
    // Điều kiện dự thi tốt nghiệp THPT - Chỉ dành cho cấp 3 lớp 12
    public class PupilGraduationController : BaseController
    {
        private readonly IPupilGraduationBusiness PupilGraduationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;

        public PupilGraduationController(ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IPupilGraduationBusiness pupilgraduationBusiness, IClassProfileBusiness classProfileBusiness
            , IPupilOfClassBusiness pupilOfClassBusiness, IPupilRankingBusiness pupilRankingBusiness, IPupilAbsenceBusiness pupilAbsenceBusiness, IPupilProfileBusiness pupilProfileBusiness)
        {
            this.PupilGraduationBusiness = pupilgraduationBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.PupilRankingBusiness = pupilRankingBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
        }

        const int MaxBuoi = 45;

        public ActionResult Index()
        {
            SetViewData();
            ViewData[PupilGraduationConstants.APPROVE_GRADUATION_PERMISSION] = this.GetMenupermission("ApproveGraduation", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData[PupilGraduationConstants.CONDITIONAL_GRADUATION_PERMISSION] = this.GetMenupermission("ConditionalGraduation", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData[PupilGraduationConstants.GRANT_CERTIFICATION_PERMISSION] = this.GetMenupermission("GrantCertification", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            return View();
        }

        private void SetViewData()
        {
            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["IsActive"] = true;
            ClassSearchInfo["AcademicYearID"] = glo.AcademicYearID;
            if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                ClassSearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TWELFTH; //Lop 12
            else if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                ClassSearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_NINTH; //Lop 9
            else
                throw new BusinessException(Res.Get("ApproveGraduation_Error_AppliedLevel"));

            ClassSearchInfo["GraduationLevel"] = glo.AppliedLevel.Value;

            List<ClassProfile> LstClassProfile = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, ClassSearchInfo).OrderBy(o => o.DisplayName).ToList();
            ViewData[PupilGraduationConstants.LIST_CLASS] = new SelectList(LstClassProfile, "ClassProfileID", "DisplayName");

            string txtContent = "Đã được công nhận tốt nghiệp trung học cơ sở tại hội đồng xét công nhận tốt nghiệp {0}. Ngày {1} tháng {2} năm {3}";
            SchoolProfile objSchool = SchoolProfileBusiness.Find(glo.SchoolID);
            if (objSchool != null)
            {
                string schoolName = objSchool.SchoolName;
                List<string> lstSplitSchoolName = schoolName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
                if (lstSplitSchoolName[0].ToString().ToUpper() == "TRƯỜNG")
                {
                    lstSplitSchoolName.RemoveAt(0);
                    schoolName = string.Join(" ", lstSplitSchoolName.ToArray());
                }

                DateTime currentDate = DateTime.Now;
                txtContent = string.Format(txtContent, schoolName, currentDate.Day, currentDate.Month, currentDate.Year);
            }
            else
            {
                txtContent = string.Empty;
            }
            ViewData["Content_Default"] = txtContent;
        }

        //[HttpPost]
        //
        //public JsonResult Approve(SearchViewModel model, FormCollection form, int[] PupilGraduations, int[] chkExemptions)
        //{
        //    int permission = this.GetMenupermission("PupilGraduation", _globalInfo.UserAccountID, _globalInfo.IsAdmin);

        //    if (permission < SystemParamsInFile.PERMISSION_LEVEL_CREATE)
        //        return Json(new JsonMessage(Res.Get("Validate_Permission_Teacher"), JsonMessage.ERROR));

        //    GlobalInfo glo = new GlobalInfo();

        //    if (PupilGraduations == null || PupilGraduations.Length == 0)
        //        return Json(new JsonMessage(Res.Get("Common_Validate_NoPupilSelected"), JsonMessage.ERROR));

        //    if (chkExemptions != null && chkExemptions.Length > 0)
        //    {
        //        foreach (int pupilID in PupilGraduations)
        //            if (chkExemptions.Contains(pupilID) && string.IsNullOrEmpty(form.Get("Exemption_" + pupilID)))
        //                return Json(new JsonMessage(Res.Get("PupilGraduation_Label_InvalidExemptionReason"), JsonMessage.ERROR));
        //    }

        //    List<PupilGraduationBO> lstPupilGraduationGV = SearchData(model, false);
        //    List<PupilGraduation> lstPupilGraduation = new List<PupilGraduation>();

        //    int? year = AcademicYearBusiness.Find(glo.AcademicYearID.Value).Year;
        //    SchoolProfile schoolProfile = SchoolProfileBusiness.Find(glo.SchoolID);
        //    bool checkGDTX = schoolProfile.TrainingTypeID.HasValue ? schoolProfile.TrainingType.Resolution == "GDTX" : false;
        //    foreach (PupilGraduationBO pgv in lstPupilGraduationGV)
        //    {
        //        if (PupilGraduations.Contains(pgv.PupilID)) //Hoc sinh duoc tich chon
        //        {
        //            PupilGraduation pg = new PupilGraduation();
        //            List<string> lstDescription = new List<string>();

        //            pg.AcademicYearID = pgv.AcademicYearID;
        //            pg.ClassID = pgv.ClassID;
        //            pg.Exemption = chkExemptions != null && chkExemptions.Contains(pgv.PupilID) && !string.IsNullOrEmpty(form.Get("Exemption_" + pgv.PupilID)) ? form.Get("Exemption_" + pgv.PupilID) : "";
        //            pg.PupilID = pgv.PupilID;
        //            pg.SchoolID = glo.SchoolID.Value;
        //            pg.Year = year;
        //            pg.GraduationLevel = (int)glo.AppliedLevel.Value;

        //            /* Dùng chung điều kiện */
        //            // Số buổi nghỉ trong năm không được quá 45 buổi
        //            if (pgv.AbsentDays > 45)
        //                lstDescription.Add(Res.Get("PupilGraduation_Error_PupilAbsentExceed"));
        //            // Xếp loại học lực cả năm từ Yếu trở lên
        //            if (pgv.CapacityLevelID.HasValue && (pgv.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR))
        //                lstDescription.Add(Res.Get("PupilGraduation_Error_CapacityNotSatisfy"));
        //            /* End điều kiện dùng chung */

        //            if (checkGDTX)
        //            {
        //                // Trường hợp học sinh thuộc chương trình giáo dục thường xuyên - GDTX
        //                // Chỉ Kiểm tra việc xếp loại hạnh kiểm và học lực của các học sinh dưới 20 với cấp 2 và dưới 25 với cấp 3
        //                if (!PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, glo.AppliedLevel.Value, pgv.PupilLearningType, pgv.BirthDate)) //Học sinh thuộc diện phải xếp loại hạnh kiểm
        //                {
        //                    if (!pgv.ConductLevelID.HasValue && !pgv.CapacityLevelID.HasValue)
        //                    {
        //                        lstDescription.Add(Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked"));
        //                    }
        //                    else
        //                    {
        //                        if (!pgv.ConductLevelID.HasValue)
        //                            lstDescription.Add(Res.Get("PupilGraduation_Error_ConductNotRanked"));

        //                        if (!pgv.CapacityLevelID.HasValue)
        //                            lstDescription.Add(Res.Get("PupilGraduation_Error_CapacityNotRanked"));
        //                    }

        //                    if (pgv.ConductLevelID.HasValue && pgv.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY)
        //                        lstDescription.Add(Res.Get("PupilGraduation_Error_ConductNotSatisfy"));
        //                }
        //            }
        //            else
        //            {
        //                // Trường hợp học sinh thuộc các trường THCS
        //                // Xếp loại hạnh kiểm cả năm từ TB trở lên
        //                if (!pgv.ConductLevelID.HasValue && !pgv.CapacityLevelID.HasValue)
        //                {
        //                    lstDescription.Add(Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked"));
        //                }
        //                else
        //                {
        //                    if (!pgv.ConductLevelID.HasValue)
        //                        lstDescription.Add(Res.Get("PupilGraduation_Error_ConductNotRanked"));

        //                    if (!pgv.CapacityLevelID.HasValue)
        //                        lstDescription.Add(Res.Get("PupilGraduation_Error_CapacityNotRanked"));
        //                }

        //                if (pgv.ConductLevelID.HasValue && pgv.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY)
        //                    lstDescription.Add(Res.Get("PupilGraduation_Error_ConductNotSatisfy"));
        //            }

        //            if (lstDescription.Count > 0)
        //                pg.Description = string.Join(",", lstDescription);
        //            pg.MeritPoint = Int32.Parse(pgv.MeritPoints.ToString());
        //            pg.Status = lstDescription.Count == 0 ? SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_SUFFICIENT : SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_INSUFFICIENT;
        //            pg.Description = string.Join(",", lstDescription);

        //            Utils.Utils.TrimObject(pg);
        //            lstPupilGraduation.Add(pg);
        //        }
        //    }
        //    this.PupilGraduationBusiness.InsertPupilGraduation(lstPupilGraduation);
        //    this.PupilGraduationBusiness.Save();
        //    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        //}

        //[HttpPost]
        //
        //public JsonResult Delete(int[] PupilIDs)
        //{
        //    if (PupilIDs == null || PupilIDs.Length == 0)
        //        return Json(new JsonMessage(Res.Get("Common_Validate_NoPupilSelected"), JsonMessage.ERROR));

        //    List<PupilGraduationBO> lstPupilGraduationGV = SearchData(new SearchViewModel(), false);

        //    if (lstPupilGraduationGV.Any(u =>                                                                               //neu ton tai hoc sinh
        //                        PupilIDs.Contains(u.PupilID)                                                                //Duoc tick chon
        //                        && (!u.PupilGraduationID.HasValue || u.PupilGraduationID.Value == 0                         //Nhung chua duoc xep hang
        //                            || (u.PupilGraduationID.HasValue && u.IsReceived.HasValue && u.IsReceived.Value == 1))  //Hoac da xep hang va da nhan bang
        //        ))
        //    {
        //        return Json(new JsonMessage(Res.Get("ApproveGraduation_Label_DeletePupilNotApproved"), JsonMessage.ERROR));
        //    }

        //    GlobalInfo glo = new GlobalInfo();
        //    foreach (PupilGraduationBO pgv in lstPupilGraduationGV)
        //    {
        //        if (PupilIDs.Contains(pgv.PupilID) && pgv.PupilGraduationID.HasValue && (!pgv.IsReceived.HasValue || pgv.IsReceived.Value != 1)) //Hoc sinh duoc tich chon
        //        {
        //            PupilGraduation pg = PupilGraduationBusiness.Find(pgv.PupilGraduationID.Value);
        //            this.PupilGraduationBusiness.DeletePupilGraduation(glo.UserAccountID, pg);
        //        }
        //    }

        //    this.PupilGraduationBusiness.Save();
        //    return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        //}

        //public PartialViewResult Search(SearchViewModel model)
        //{
        //    Utils.Utils.TrimObject(model);
        //    ViewData[PupilGraduationConstants.LIST_PUPILGRADUATION] = SearchData(model, true);
        //    ViewData[PupilGraduationConstants.GRADUATION_PERMISSION] = this.GetMenupermission("PupilGraduation", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
        //    return PartialView("_List", model);
        //}

        //private List<PupilGraduationBO> SearchData(SearchViewModel frm, bool check)
        //{
        //    GlobalInfo glo = new GlobalInfo();
        //    IDictionary<string, object> pocDic = new Dictionary<string, object>();
        //    pocDic["AcademicYearID"] = glo.AcademicYearID;
        //    pocDic["ClassID"] = frm.ClassID;
        //    pocDic["PupilCode"] = frm.PupilCode;
        //    pocDic["FullName"] = frm.FullName;
        //    pocDic["Check"] = "Check";
        //    pocDic["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TWELFTH; //Lop 12
        //    List<PupilGraduationBO> lstPG = PupilGraduationBusiness.GetGraduationPupil(glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, pocDic);
        //    SchoolProfile schoolProfile = SchoolProfileBusiness.Find(glo.SchoolID);
        //    bool checkGDTX = schoolProfile.TrainingTypeID.HasValue ? schoolProfile.TrainingType.Resolution == "GDTX" : false;
        //    foreach (PupilGraduationBO pg in lstPG)
        //    {
        //        // Trường hợp thỏa mãn các điều kiện thì được hiển thị ô lựa chọn, set giá trị disabled checkbox = false
        //        pg.chkDisabled = false;
        //        // End set disabled
        //        if (check)
        //        {
        //            if (checkGDTX)
        //            {
        //                // Học sinh thuộc diện phải xếp loại hạnh kiểm của hệ GDTX
        //                // Có tuổi dưới 20 với THCS và dưới 25 với THPT
        //                // Trường hợp này dưới 20 tuổi mới hiển thị thông báo lên cột học lực và hạnh kiểm
        //                if (!PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, glo.AppliedLevel.Value, pg.PupilLearningType, pg.BirthDate))
        //                {
        //                    if (!pg.ConductLevelID.HasValue && !pg.CapacityLevelID.HasValue)
        //                    {
        //                        pg.Description = Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked");
        //                        pg.chkDisabled = true;
        //                    }
        //                    else
        //                    {
        //                        if (!pg.ConductLevelID.HasValue)
        //                        {
        //                            pg.Description = Res.Get("PupilGraduation_Error_ConductNotRanked");
        //                            pg.chkDisabled = true;
        //                        }
        //                        if (!pg.CapacityLevelID.HasValue)
        //                        {
        //                            pg.Description = Res.Get("PupilGraduation_Error_CapacityNotRanked");
        //                            pg.chkDisabled = true;
        //                        }
        //                    }
        //                }

        //            }
        //            else
        //            {
        //                // Kiểm tra học sinh chưa được xếp loại học lực & hạnh kiểm của các trường THCS

        //                if (!pg.ConductLevelID.HasValue && !pg.CapacityLevelID.HasValue)
        //                {
        //                    pg.Description = Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked");
        //                    pg.chkDisabled = true;
        //                }
        //                else
        //                {
        //                    if (!pg.ConductLevelID.HasValue)
        //                    {
        //                        pg.Description = Res.Get("PupilGraduation_Error_ConductNotRanked");
        //                        pg.chkDisabled = true;
        //                    }
        //                    if (!pg.CapacityLevelID.HasValue)
        //                    {
        //                        pg.Description = Res.Get("PupilGraduation_Error_CapacityNotRanked");
        //                        pg.chkDisabled = true;
        //                    }
        //                }
        //            }
        //        }

        //        pg.ApprenticeshipPoints = pg.ApprenticeshipRank.HasValue
        //                                        ? (pg.ApprenticeshipRank.Value == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_EXPERT //Loai gioi +2
        //                                                ? 2
        //                                                : (pg.ApprenticeshipRank.Value == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_GOOD //Loai kha +1.5
        //                                                        ? (decimal)1.5
        //                                                        : (pg.ApprenticeshipRank.Value == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_NORMAL //Loai TB  +1
        //                                                                ? 1
        //                                                                : new Nullable<decimal>())))
        //                                        : new Nullable<decimal>();

        //        decimal certificatePoints = glo.TrainingType == SystemParamsInFile.TRAINING_TYPE_VOCATIONAL && pg.CertificatePoints.HasValue ? pg.CertificatePoints.Value : 0;

        //        pg.MeritPoints = GetMeritPoints(pg.ApprenticeshipPoints, pg.PraisePoints, certificatePoints);
        //        pg.StatusString = pg.GraduationStatus.HasValue ? CommonList.GraduationStatus().Where(u => u.key.Equals(pg.GraduationStatus.Value.ToString())).SingleOrDefault().value : string.Empty;
        //    }

        //    return lstPG;
        //}

        //public FileResult ExportExcel(SearchViewModel model)
        //{
        //    GlobalInfo glo = new GlobalInfo();
        //    IDictionary<string, object> pocDic = new Dictionary<string, object>();
        //    pocDic["AcademicYearID"] = glo.AcademicYearID;
        //    pocDic["ClassID"] = model.ClassID;
        //    pocDic["PupilCode"] = model.PupilCode;
        //    pocDic["FullName"] = model.FullName;
        //    pocDic["Check"] = "Check";
        //    pocDic["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TWELFTH; //Lop 12
        //    List<PupilGraduationBO> lstPG = SearchData(model, true);

        //    AcademicYear ay = AcademicYearBusiness.Find(glo.AcademicYearID);
        //    SchoolProfile sp = SchoolProfileBusiness.Find(glo.SchoolID);
        //    ClassProfile classProfile = ClassProfileBusiness.Find(model.ClassID);

        //    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THPT_DKDTTOTNGHIEP);
        //    string templatePath = ReportUtils.GetTemplatePath(reportDef);
        //    string fileName = reportDef.OutputNamePattern;

        //    List<object> lstRows = new List<object>();
        //    int index = 1;
        //    lstPG.ForEach(u =>
        //    {
        //        lstRows.Add(new
        //        {
        //            Index = index++,
        //            PupilCode = u.PupilCode,
        //            FullName = u.FullName,
        //            Genre = u.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female"),
        //            BirthDate = u.BirthDate.ToString("dd/MM/yyyy"),
        //            ClassName = u.ClassName,
        //            Capacity = u.CapacityLevel,
        //            Conduct = u.ConductLevel,
        //            AbsentDays = u.AbsentDays,
        //            Exemption = u.ExemptionResolution,
        //            Notification = u.Notification,
        //            PraisePoints = u.PraisePoints,
        //            ApprenticeshipPoints = u.ApprenticeshipPoints,
        //            MeritPoints = u.MeritPoints
        //        });
        //    });
        //    IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
        //    IVTWorksheet sheet = oBook.GetSheet(1);
        //    IVTWorksheet temp = oBook.GetSheet(2);

        //    IVTRange topRange = temp.GetRange("A1", "L1");
        //    IVTRange midRange = temp.GetRange("A2", "L2");
        //    IVTRange botRange = temp.GetRange("A5", "L5");

        //    int START_ROW_INDEX = 9;
        //    int rowIndex = START_ROW_INDEX;
        //    for (int i = 0; i < lstPG.Count - 5; i++)
        //    {
        //        IVTRange range = null;
        //        if (i % 5 == 4) range = botRange;
        //        else if (i % 5 == 0) range = topRange;
        //        else range = midRange;
        //        sheet.CopyPaste(range, rowIndex + i + 5, 1, true);
        //    }
        //    temp.CopyPaste(botRange, START_ROW_INDEX + lstPG.Count - 1, 1, true);

        //    Dictionary<string, object> sheetData = new Dictionary<string, object>();
        //    sheetData["AcademicYear"] = ay.DisplayTitle;
        //    sheetData["SchoolName"] = sp.SchoolName;
        //    sheetData["ProvinceName"] = sp.Province.ProvinceName;
        //    sheetData["SupervisingDeptName"] = sp.SupervisingDept.SupervisingDeptName;
        //    sheetData["ReportDate"] = string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
        //    sheetData["Rows"] = lstRows;

        //    sheet.FillVariableValue(sheetData);
        //    temp.Delete();

        //    fileName = fileName.Replace("[ClassName]", classProfile != null ? classProfile.DisplayName : "");

        //    Stream excel = oBook.ToStream();
        //    FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

        //    result.FileDownloadName = ReportUtils.RemoveSpecialCharacters(fileName) + "." + reportDef.OutputFormat;
        //    return result;
        //}

        //private decimal? GetMeritPoints(decimal? appPoint, decimal? praisePoints, decimal? certificatePoints)
        //{
        //    decimal meritPoints = 0;

        //    if (appPoint.HasValue) meritPoints = meritPoints + appPoint.Value;
        //    if (praisePoints.HasValue) meritPoints = meritPoints + praisePoints.Value;
        //    if (certificatePoints.HasValue) meritPoints = meritPoints + certificatePoints.Value;

        //    if (meritPoints > 4) meritPoints = 4;

        //    return meritPoints;
        //}
    
    }
}





