﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.PupilGraduationArea.Models;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;


namespace SMAS.Web.Areas.PupilGraduationArea.Controllers
{
    public class GrantCertificationController : BaseController
    {
        // Dành cho 2 cấp 2 &3 
        private readonly IPupilGraduationBusiness PupilGraduationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;

        public GrantCertificationController(IPupilGraduationBusiness pupilgraduationBusiness,
                                            IClassProfileBusiness classprofileBusiness,
                                            IAcademicYearBusiness academicyearBusiness,
                                            IPupilOfClassBusiness pupilOfclassBusiness,
                                            IPupilProfileBusiness pupilprofileBusiness,
                                            IReportDefinitionBusiness reportDefinitionBusiness)
        {
            this.PupilGraduationBusiness = pupilgraduationBusiness;
            this.PupilOfClassBusiness = pupilOfclassBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.PupilProfileBusiness = pupilprofileBusiness;
            this.AcademicYearBusiness = academicyearBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return PartialView();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            GlobalInfo glo = new GlobalInfo();
            ViewData[PupilGraduationConstants.LIST_GRADUATIONGRADE] = CommonList.GraduationGrade();
            Utils.Utils.TrimObject(frm);

            AcademicYear academicyear = this.AcademicYearBusiness.Find(glo.AcademicYearID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["GraduationLevel"] = glo.AppliedLevel.Value;
            List<PupilOfClass> pocList = PupilOfClassBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID.Value && p.AcademicYearID == _globalInfo.AcademicYearID.Value && p.ClassID == frm.ClassID).ToList();
            List<GrantCertificationViewModel> list = this._Search(SearchInfo)
                                                        .Select(o => new GrantCertificationViewModel
                                                        {
                                                            PupilGraduationID = o.PupilGraduationID,
                                                            PupilID = o.PupilID,
                                                            ClassID = o.ClassID,
                                                            DisplayName = o.ClassProfile.DisplayName,
                                                            PupilCode = o.PupilProfile.PupilCode,
                                                            FullName = o.PupilProfile.FullName,
                                                            BirthDay = o.PupilProfile.BirthDate,
                                                            Year = o.Year,
                                                            YearDisplay = o.Year.HasValue ? (o.Year.Value + " - " + (o.Year.Value + 1)) : string.Empty,
                                                            GenreDisplay = o.PupilProfile.Genre == 0 ? Res.Get("Common_Label_Female") : Res.Get("Common_Label_Male"),
                                                            GraduationGrade = o.GraduationGrade == 0 ? (int)SystemParamsInFile.GRADUATION_GRADE_FAIR : o.GraduationGrade,
                                                            IssuedNumber = o.IssuedNumber,
                                                            IssuedDate = o.IssuedDate,
                                                            IsReceived = o.IsReceived,
                                                            Description = o.Description,
                                                            Genre = o.PupilProfile.Genre,
                                                            GraduationLevel = o.GraduationLevel,
                                                            ReceivedDate = o.ReceivedDate,
                                                            RecordNumber = o.RecordNumber,
                                                            SchoolID = glo.SchoolID.Value,
                                                            Receiver = o.Receiver,
                                                            ShortName = o.PupilProfile.Name,
                                                            OrderInClass = pocList.Where(p=>p.PupilID == o.PupilID).Select(p=>p.OrderInClass).FirstOrDefault() ?? 0
                                                        }).ToList();

            if (list.Count > 0 && list != null)
            {
                if (frm.ClassID > 0)
                {
                    list = list.OrderBy(p => p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.ShortName + " " + p.FullName)).ToList();
                }
                else
                {
                list = list.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.ShortName + " " + p.FullName)).ToList();
            }
            }
            ViewData[PupilGraduationConstants.LIST_PUPILGRADUATION] = list;
            ViewData[PupilGraduationConstants.GRANT_CERTIFICATION_PERMISSION] = this.GetMenupermission("GrantCertification", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            return PartialView("_GridGrantCertification");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Update(SearchViewModel frm, FormCollection form)
        {
            GlobalInfo glo = new GlobalInfo();

            int permission = this.GetMenupermission("GrantCertification", glo.UserAccountID, glo.IsAdmin);

            if (permission < SystemParamsInFile.PERMISSION_LEVEL_CREATE)
                return Json(new JsonMessage(Res.Get("Validate_Permission_Teacher"), JsonMessage.ERROR));

            Utils.Utils.TrimObject(frm);

            AcademicYear academicyear = this.AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            int year = academicyear.Year;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfo["Year"] = year;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.FullName;
            List<PupilGraduation> lstSearch = this._Search(SearchInfo);

            var checkedPupil = lstSearch.Where(u => !string.IsNullOrEmpty(form.Get("check_" + u.PupilID)));

            if (checkedPupil.Count() == 0)
                return Json(new JsonMessage(Res.Get("GrantCertification_Label_ErrorCheck"), "error"));

            List<PupilGraduation> lstPupilGraduation = new List<PupilGraduation>();
            foreach (var item in checkedPupil)
            {
                string IssuedDate = TrimString(form.Get("IssuedDate_" + item.PupilID));
                string ReceivedDate = TrimString(form.Get("ReceivedDate_" + item.PupilID));
                string issuedNumber = TrimString(form.Get("IssuedNumber_" + item.PupilID));
                string recordNumber = TrimString(form.Get("RecordNumber_" + item.PupilID));
                string description = TrimString(form.Get("Description_" + item.PupilID));
                string receiver = TrimString(form.Get("Receiver_" + item.PupilID));
                string graduationGradeStr = TrimString(form.Get("GraduationGrade_" + item.PupilID));

                if (IssuedDate == null || IssuedDate == "")
                    return Json(new JsonMessage(string.Format(Res.Get("Common_Validate_Required"), Res.Get("PupilGraduation_Label_IssuedDate")), "error"));

                bool checkConvert = true;
                DateTime issuedDate = new DateTime();
                checkConvert = DateTime.TryParse(IssuedDate, out issuedDate);
                if (!checkConvert)
                    return Json(new JsonMessage(Res.Get("GrantCertification_Label_IssuedDateError"), "error"));

                if (ReceivedDate == null || ReceivedDate == "")
                    return Json(new JsonMessage(string.Format(Res.Get("Common_Validate_Required"), Res.Get("PupilGraduation_Label_ReceivedDate")), "error"));

                DateTime receivedDate = new DateTime();
                checkConvert = DateTime.TryParse(ReceivedDate, out receivedDate);
                if (!checkConvert)
                    return Json(new JsonMessage(Res.Get("GrantCertification_Label_ReceivedDateError"), "error"));

                if (issuedDate > receivedDate)
                    return Json(new JsonMessage(Res.Get("GrantCertification_Label_DateTimeErrorIssuedDate"), "error"));

                if (receiver == null || string.IsNullOrEmpty(receiver.Trim()))
                    return Json(new JsonMessage(string.Format(Res.Get("Common_Validate_Required"), Res.Get("PupilGraduation_Label_Receiver")), "error"));

                item.PupilGraduationID = item.PupilGraduationID;
                item.AcademicYearID = item.AcademicYearID;
                item.Year = item.Year;
                item.PupilID = item.PupilID;
                item.ClassID = item.ClassID;
                item.SchoolID = item.SchoolID;
                if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                    item.GraduationGrade = Convert.ToByte(graduationGradeStr);
                item.IssuedDate = issuedDate;
                item.ReceivedDate = receivedDate;
                item.Receiver = receiver;
                item.IssuedNumber = issuedNumber;
                item.RecordNumber = recordNumber;
                item.GraduationLevel = item.GraduationLevel;
                item.PriorityReason = item.PriorityReason;
                item.Description = description;
                item.IsReceived = 1;
                item.Status = SystemParamsInFile.GRADUATION_STATUS_GRADUATED;
                lstPupilGraduation.Add(item);
            }

            this.PupilGraduationBusiness.UpdatePupilGraduation(lstPupilGraduation);
            this.PupilGraduationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int[] PupilGraduationIDs)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("GrantCertification", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (PupilGraduationIDs == null || PupilGraduationIDs.Length == 0)
                return Json(new JsonMessage(Res.Get("Common_Validate_NoPupilSelected"), JsonMessage.ERROR));

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            List<PupilGraduation> lstSearch = this._Search(SearchInfo);
            List<PupilGraduation> lstPupilGraduation = new List<PupilGraduation>();

            //Neu chon xoa hoc sinh chua duoc nhan bang
            if (lstSearch.Any(u => PupilGraduationIDs.Any(v => v == u.PupilGraduationID) && (!u.IsReceived.HasValue || u.IsReceived == SystemParamsInFile.GRADUAION_NOT_RECEIVED)))
                return Json(new JsonMessage(Res.Get("GrantCertification_Label_PupilNotReceived"), "error"));

            GlobalInfo glo = new GlobalInfo();
            foreach (var item in lstSearch.Where(u => PupilGraduationIDs.Any(v => v == u.PupilGraduationID)))
            {
                item.GraduationGrade = 0;
                item.IssuedNumber = null;
                item.RecordNumber = null;
                item.IssuedDate = null;
                item.ReceivedDate = null;
                item.Receiver = null;
                item.Description = null;
                item.PriorityReason = item.PriorityReason;
                item.IsReceived = SystemParamsInFile.GRADUAION_NOT_RECEIVED;
                item.Status = glo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY
                                    ? SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_SUFFICIENT
                                    : SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT;
                lstPupilGraduation.Add(item);
            }

            this.PupilGraduationBusiness.UpdatePupilGraduation(lstPupilGraduation);
            this.PupilGraduationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        private List<PupilGraduation> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo glo = new GlobalInfo();
            List<PupilGraduation> lst = this.PupilGraduationBusiness
                                                        .SearchBySchool(glo.SchoolID.Value, SearchInfo)
                                                        .Where(u => u.Status.HasValue && (u.Status.Value == SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT
                                                                                            || u.Status.Value == SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_SUFFICIENT
                                                                                            || u.Status.Value == SystemParamsInFile.GRADUATION_STATUS_GRADUATED))
                                                        .ToList();
            return lst;
        }

        private void SetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = glo.AcademicYearID.Value;

            if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                dic["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TWELFTH; //Lop 12
            else if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                dic["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_NINTH; //Lop 9
            else
                throw new BusinessException(Res.Get("ApproveGraduation_Error_AppliedLevel"));

            dic["GraduationLevel"] = glo.AppliedLevel.Value;
            List<ClassProfile> lstClass = this.ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            ViewData[PupilGraduationConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            ViewData[PupilGraduationConstants.LIST_GRADUATIONGRADE] = CommonList.GraduationGrade();
        }

        public FileResult ExportExcel(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = model.ClassID;
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchInfo["PupilCode"] = model.PupilCode;
            SearchInfo["FullName"] = model.FullName;
            SearchInfo["GraduationLevel"] = glo.AppliedLevel.Value;

            if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                SearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TWELFTH; //Lop 12
            else if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                SearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_NINTH; //Lop 9
            else
                throw new BusinessException(Res.Get("ApproveGraduation_Error_AppliedLevel"));

            List<PupilGraduation> lstPG = _Search(SearchInfo);

            if (lstPG.Count > 0 && lstPG != null)
            {
                if (model.ClassID > 0)
                {
                    List<PupilOfClass> pocList = PupilOfClassBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID && p.AcademicYearID == _globalInfo.AcademicYearID && p.ClassID == model.ClassID).ToList();
                    lstPG = (from a in lstPG
                             join b in pocList on a.ClassID equals b.ClassID
                             where a.PupilID == b.PupilID
                            orderby b.OrderInClass,SMAS.Business.Common.Utils.SortABC(a.PupilProfile.Name + " " + a.PupilProfile.FullName)
                            select a).ToList();
                }
                else
                {
                    lstPG = lstPG.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilProfile.Name + " " + p.PupilProfile.FullName)).ToList();
                }
            }

            AcademicYear ay = AcademicYearBusiness.Find(glo.AcademicYearID);
            SchoolProfile sp = ay.SchoolProfile;
            ClassProfile classProfile = ClassProfileBusiness.Find(model.ClassID);

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THPT_CAPBANGTN);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string fileName = reportDef.OutputNamePattern;

            List<object> lstRows = new List<object>();
            int index = 1;
            lstPG.ForEach(u =>
            {
                lstRows.Add(new
                {
                    Index = index++,
                    FullName = u.PupilProfile.FullName,
                    BirthDate = u.PupilProfile.BirthDate.ToString("dd/MM/yyyy"),
                    Genre = u.PupilProfile.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female"),
                    ClassName = u.ClassProfile.DisplayName,
                    AcademicYear = (ay.Year + " - " + (ay.Year + 1)),
                    GraduationGrade = GetGraduationGrade(u.GraduationGrade),
                    RecordNumber = u.RecordNumber,
                    IssuedDate = u.IssuedDate.HasValue ? u.IssuedDate.Value.ToString("dd/MM/yyyy"):null,
                    IssuedNumber = u.IssuedNumber,
                    ReceivedDate = u.ReceivedDate.HasValue ? u.ReceivedDate.Value.ToString("dd/MM/yyyy") : null,
                    Receiver = u.Receiver,
                    Description = u.Description
                });
            });

           

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet temp = oBook.GetSheet(2);

            IVTRange topRange = temp.GetRange("A1", "N1");
            IVTRange midRange = temp.GetRange("A2", "N2");
            IVTRange botRange = temp.GetRange("A5", "N5");

            int START_ROW_INDEX = 10;
            int rowIndex = START_ROW_INDEX;
            for (int i = 0; i < lstPG.Count; i++)
            {
                IVTRange range = null;
                if (i % 5 == 4) range = botRange;
                else if (i % 5 == 0) range = topRange;
                else range = midRange;
                sheet.CopyPaste(range, rowIndex + i, 1, true);
                if ((i + 1) == lstPG.Count)
                {
                    range = botRange;
                    sheet.CopyPaste(range, rowIndex + i, 1, true);
                }
            }
            if (lstPG.Count > 5)
                sheet.CopyPaste(botRange, START_ROW_INDEX + lstPG.Count - 1, 1, true);

            Dictionary<string, object> sheetData = new Dictionary<string, object>();
            sheetData["AcademicYear"] = ay.DisplayTitle;
            sheetData["SchoolName"] = sp.SchoolName;
            sheetData["ProvinceName"] = sp.Province.ProvinceName;
            sheetData["SupervisingDeptName"] = sp.SupervisingDept.SupervisingDeptName;
            sheetData["ReportDate"] = "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            sheetData["Rows"] = lstRows;

            if (lstPG.Count == 0)
                sheet.DeleteRow(START_ROW_INDEX);

            sheet.FillVariableValue(sheetData);
            temp.Delete();

            fileName = fileName.Replace("[AppliedLevel]", glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_SECONDARY ? "THCS" : "THPT")
                                .Replace("[ClassName]", classProfile != null ? classProfile.DisplayName : "");

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            result.FileDownloadName = ReportUtils.RemoveSpecialCharacters(fileName) + "." + reportDef.OutputFormat;
            return result;
        }

        private string GetGraduationGrade(int GraduationGrade)
        {
            string s = "";
            if (GraduationGrade == 0)
            {
                return s;
            }
            else
            {

                ComboObject co = CommonList.GraduationGrade().SingleOrDefault(u => u.key == GraduationGrade.ToString());

                return co.value;
            }
        }

        private string TrimString(string o)
        {
            if (o == null) return o;

            return o.Trim();
        }
    }
}
