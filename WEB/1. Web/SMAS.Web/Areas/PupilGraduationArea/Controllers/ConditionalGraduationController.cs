﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.PupilGraduationArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.PupilGraduationArea.Controllers
{
    public class ConditionalGraduationController : BaseController
    {
        private readonly IPupilGraduationBusiness PupilGraduationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilPraiseBusiness PupilPraiseBusiness;

        public ConditionalGraduationController(ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IPupilGraduationBusiness pupilgraduationBusiness, IClassProfileBusiness classProfileBusiness
            , IPupilOfClassBusiness pupilOfClassBusiness, IPupilRankingBusiness pupilRankingBusiness, IPupilAbsenceBusiness pupilAbsenceBusiness, IPupilProfileBusiness pupilProfileBusiness, IPupilPraiseBusiness pupilPraiseBusiness)
        {
            this.PupilGraduationBusiness = pupilgraduationBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.PupilRankingBusiness = pupilRankingBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.PupilPraiseBusiness = pupilPraiseBusiness;
        }

        const int MaxBuoi = 45;

        private void SetViewData()
        {
            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["IsActive"] = true;
            ClassSearchInfo["AcademicYearID"] = glo.AcademicYearID;
            if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                ClassSearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TWELFTH; //Lop 12
            else if (glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                ClassSearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_NINTH; //Lop 9
            else
                throw new BusinessException(Res.Get("ApproveGraduation_Error_AppliedLevel"));

            ClassSearchInfo["GraduationLevel"] = glo.AppliedLevel.Value;

            List<ClassProfile> LstClassProfile = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, ClassSearchInfo).OrderBy(o => o.DisplayName).ToList();
            ViewData[PupilGraduationConstants.LIST_CLASS] = new SelectList(LstClassProfile, "ClassProfileID", "DisplayName");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Approve(SearchViewModel model, FormCollection form, int[] PupilGraduations, int[] chkExemptions)
        {
            int permission = this.GetMenupermission("PupilGraduation", _globalInfo.UserAccountID, _globalInfo.IsAdmin);

            if (permission < SystemParamsInFile.PERMISSION_LEVEL_CREATE)
                return Json(new JsonMessage(Res.Get("Validate_Permission_Teacher"), JsonMessage.ERROR));

            GlobalInfo glo = new GlobalInfo();

            if (PupilGraduations == null || PupilGraduations.Length == 0)
                return Json(new JsonMessage(Res.Get("Common_Validate_NoPupilSelected"), JsonMessage.ERROR));

            if (chkExemptions != null && chkExemptions.Length > 0)
            {
                foreach (int pupilID in PupilGraduations)
                    if (chkExemptions.Contains(pupilID) && string.IsNullOrEmpty(form.Get("Exemption_" + pupilID)))
                        return Json(new JsonMessage(Res.Get("PupilGraduation_Label_InvalidExemptionReason"), JsonMessage.ERROR));
            }

            List<PupilGraduationBO> lstPupilGraduationGV = SearchData(model, false);
            List<PupilGraduation> lstPupilGraduation = new List<PupilGraduation>();

            int? year = AcademicYearBusiness.Find(glo.AcademicYearID.Value).Year;
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(glo.SchoolID);
            bool checkGDTX = schoolProfile.TrainingTypeID.HasValue ? schoolProfile.TrainingType.Resolution == "GDTX" : false;
            foreach (PupilGraduationBO pgv in lstPupilGraduationGV)
            {
                if (PupilGraduations.Contains(pgv.PupilID)) //Hoc sinh duoc tich chon
                {
                    PupilGraduation pg = new PupilGraduation();
                    List<string> lstDescription = new List<string>();

                    pg.AcademicYearID = pgv.AcademicYearID;
                    pg.ClassID = pgv.ClassID;
                    pg.Exemption = chkExemptions != null && chkExemptions.Contains(pgv.PupilID) && !string.IsNullOrEmpty(form.Get("Exemption_" + pgv.PupilID)) ? form.Get("Exemption_" + pgv.PupilID) : "";
                    pg.PupilID = pgv.PupilID;
                    pg.SchoolID = glo.SchoolID.Value;
                    pg.Year = year;
                    pg.GraduationLevel = (int)glo.AppliedLevel.Value;

                    /* Dùng chung điều kiện */
                    // Số buổi nghỉ trong năm không được quá 45 buổi
                    if (pgv.AbsentDays > 45)
                        lstDescription.Add(Res.Get("PupilGraduation_Error_PupilAbsentExceed"));
                    // Xếp loại học lực cả năm từ Yếu trở lên
                    if (pgv.CapacityLevelID.HasValue && (pgv.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR))
                        lstDescription.Add(Res.Get("PupilGraduation_Error_CapacityNotSatisfy"));
                    /* End điều kiện dùng chung */

                    if (PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, glo.AppliedLevel.Value, pgv.PupilLearningType, pgv.BirthDate)) //Học sinh thuộc diện không phải xếp loại hạnh kiểm
                    {
                        if (!pgv.CapacityLevelID.HasValue)
                            lstDescription.Add(Res.Get("PupilGraduation_Error_CapacityNotRanked"));
                    }
                    else //Hoc sinh phai xet hanh kiem
                    {
                        if (!pgv.ConductLevelID.HasValue && !pgv.CapacityLevelID.HasValue)
                        {
                            lstDescription.Add(Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked"));
                        }
                        else
                        {
                            if (!pgv.ConductLevelID.HasValue)
                                lstDescription.Add(Res.Get("PupilGraduation_Error_ConductNotRanked"));

                            if (!pgv.CapacityLevelID.HasValue)
                                lstDescription.Add(Res.Get("PupilGraduation_Error_CapacityNotRanked"));
                        }

                        if (pgv.ConductLevelID.HasValue && pgv.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY)
                            lstDescription.Add(Res.Get("PupilGraduation_Error_ConductNotSatisfy"));
                    }

                    if (lstDescription.Count > 0)
                        pg.Description = string.Join(",", lstDescription);
                    pg.MeritPoint = pgv.MeritPoints;
                    pg.Status = lstDescription.Count == 0 ? SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_SUFFICIENT : SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_INSUFFICIENT;
                    pg.Description = string.Join(",", lstDescription);

                    Utils.Utils.TrimObject(pg);
                    lstPupilGraduation.Add(pg);
                }
            }
            this.PupilGraduationBusiness.InsertPupilGraduation(lstPupilGraduation);
            this.PupilGraduationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int[] PupilIDs)
        {
            if (PupilIDs == null || PupilIDs.Length == 0)
                return Json(new JsonMessage(Res.Get("Common_Validate_NoPupilSelected"), JsonMessage.ERROR));

            List<PupilGraduationBO> lstPupilGraduationGV = SearchData(new SearchViewModel(), false);

            if (lstPupilGraduationGV.Any(u =>                                                                               //neu ton tai hoc sinh
                                PupilIDs.Contains(u.PupilID)                                                                //Duoc tick chon
                                && (!u.PupilGraduationID.HasValue || u.PupilGraduationID.Value == 0                         //Nhung chua duoc xep hang
                                    || (u.PupilGraduationID.HasValue && u.IsReceived.HasValue && u.IsReceived.Value == 1))  //Hoac da xep hang va da nhan bang
                ))
            {
                return Json(new JsonMessage(Res.Get("ApproveGraduation_Label_DeletePupilNotApproved"), JsonMessage.ERROR));
            }

            GlobalInfo glo = new GlobalInfo();
            List<int> PupilGraduationIDList = lstPupilGraduationGV.Select(pp => pp.PupilGraduationID.HasValue ? pp.PupilGraduationID.Value : 0).ToList();
            var lstPupilGraduation = PupilGraduationBusiness.All.Where(p => PupilGraduationIDList.Contains(p.PupilGraduationID));
            foreach (PupilGraduationBO pgv in lstPupilGraduationGV)
            {
                int PupilGraduationID = pgv.PupilGraduationID.HasValue ? pgv.PupilGraduationID.Value : 0;
                if (PupilIDs.Contains(pgv.PupilID) && pgv.PupilGraduationID.HasValue && (!pgv.IsReceived.HasValue || pgv.IsReceived.Value != 1)) //Hoc sinh duoc tich chon
                {
                    PupilGraduation pg = lstPupilGraduation.Where(p => p.PupilGraduationID == PupilGraduationID).FirstOrDefault();
                    if (pg != null)
                    {
                        this.PupilGraduationBusiness.DeletePupilGraduation(glo.UserAccountID, pg);
                    }
                }
            }

            this.PupilGraduationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public PartialViewResult Search(SearchViewModel model)
        {
            Utils.Utils.TrimObject(model);
            ViewData[PupilGraduationConstants.LIST_PUPILGRADUATION] = SearchData(model, true);
            ViewData[PupilGraduationConstants.CONDITIONAL_GRADUATION_PERMISSION] = this.GetMenupermission("ConditionalGraduation", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            return PartialView("_List", model);
        }

        private List<PupilGraduationBO> SearchData(SearchViewModel frm, bool check)
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> pocDic = new Dictionary<string, object>();
            pocDic["AcademicYearID"] = glo.AcademicYearID;
            pocDic["ClassID"] = frm.ClassID;
            pocDic["PupilCode"] = frm.PupilCode;
            pocDic["FullName"] = frm.FullName;
            pocDic["Check"] = "Check";
            pocDic["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TWELFTH; //Lop 12
            List<PupilGraduationBO> lstPG = PupilGraduationBusiness.GetGraduationPupil(glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, pocDic);
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(glo.SchoolID);
            bool checkGDTX = schoolProfile.TrainingTypeID.HasValue ? schoolProfile.TrainingType.Resolution == "GDTX" : false;
            foreach (PupilGraduationBO pg in lstPG)
            {
                // Trường hợp thỏa mãn các điều kiện thì được hiển thị ô lựa chọn, set giá trị disabled checkbox = false
                pg.chkDisabled = false;
                pg.Description = pg.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_SUFFICIENT || pg.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_GRADUATED ? string.Empty : pg.Description;
                // End set disabled
                if (check)
                {
                    // Học sinh thuộc diện phải xếp loại hạnh kiểm của hệ GDTX
                    // Có tuổi dưới 20 với THCS và dưới 25 với THPT
                    // Trường hợp này dưới 20 tuổi mới hiển thị thông báo lên cột học lực và hạnh kiểm
                    if (PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, glo.AppliedLevel.Value, pg.PupilLearningType, pg.BirthDate)) //Hoc sinh khong phai xet hanh kiem
                    {
                        if (!pg.CapacityLevelID.HasValue)
                        {
                            pg.Description = Res.Get("PupilGraduation_Error_CapacityNotRanked");
                            pg.chkDisabled = true;
                        }
                    }
                    else
                    {
                        if (!pg.ConductLevelID.HasValue && !pg.CapacityLevelID.HasValue)
                        {
                            pg.Description = Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked");
                            pg.chkDisabled = true;
                        }
                        else
                        {
                            if (!pg.ConductLevelID.HasValue)
                            {
                                pg.Description = Res.Get("PupilGraduation_Error_ConductNotRanked");
                                pg.chkDisabled = true;
                            }
                            if (!pg.CapacityLevelID.HasValue)
                            {
                                pg.Description = Res.Get("PupilGraduation_Error_CapacityNotRanked");
                                pg.chkDisabled = true;
                            }
                        }
                    }
                }

                pg.ApprenticeshipPoints = pg.ApprenticeshipRank.HasValue
                                                ? (pg.ApprenticeshipRank.Value == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_EXPERT //Loai gioi +2
                                                        ? 2
                                                        : (pg.ApprenticeshipRank.Value == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_GOOD //Loai kha +1.5
                                                                ? (decimal)1.5
                                                                : (pg.ApprenticeshipRank.Value == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_NORMAL //Loai TB  +1
                                                                        ? 1
                                                                        : new Nullable<decimal>())))
                                                : new Nullable<decimal>();

                decimal certificatePoints = 0;
                if (checkGDTX)
                {
                    if (pg.ItCertificatePoint.HasValue) certificatePoints += pg.ItCertificatePoint.Value;
                    if (pg.LanguageCertificatePoint.HasValue) certificatePoints += pg.LanguageCertificatePoint.Value;
                }
                pg.CertificatePoints = certificatePoints > 0 ? certificatePoints : new Nullable<decimal>();
                pg.MeritPoints = GetMeritPoints(pg.ApprenticeshipPoints, pg.PraisePoints, certificatePoints);
                pg.StatusString = pg.GraduationStatus.HasValue ? CommonList.GraduationStatus().Where(u => u.key.Equals(pg.GraduationStatus.Value.ToString())).SingleOrDefault().value : string.Empty;
            }

            if (lstPG.Count > 0 && lstPG != null)
            {
                if (frm.ClassID > 0)
                {
                    lstPG = lstPG.OrderBy(p=>p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                }
                else
                {
                lstPG = lstPG.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " "+ p.FullName)).ToList();
            }
            }

            return lstPG;
        }

        public FileResult ExportExcel(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> pocDic = new Dictionary<string, object>();
            pocDic["AcademicYearID"] = glo.AcademicYearID;
            pocDic["ClassID"] = model.ClassID;
            pocDic["PupilCode"] = model.PupilCode;
            pocDic["FullName"] = model.FullName;
            pocDic["Check"] = "Check";
            pocDic["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TWELFTH; //Lop 12
            List<PupilGraduationBO> lstPG = SearchData(model, true);

            var lstPraise = PupilPraiseBusiness.SearchBySchool(glo.SchoolID.Value, pocDic)
                                                .Select(u => new { u.PupilID, u.Description, u.PraiseType.GraduationMark })
                                                .Where(u => u.GraduationMark.HasValue)
                                                .ToList();

            foreach (PupilGraduationBO pg in lstPG)
            {
                List<string> addPoints = new List<string>();
                if (pg.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_SUFFICIENT || pg.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_GRADUATED)
                {
                    var lstPupilPraise = lstPraise.Where(u => u.PupilID == pg.PupilID);
                    if (pg.ExemptionResolution != null)
                    {
                        pg.Description = Res.Get("ConditionalGraduation_Exempt")+ " " + pg.ExemptionResolution;
                    }
                    else if (lstPupilPraise.Any())
                    {
                        pg.Description = string.Join(", ", lstPupilPraise.Select(u => u.Description));
                    }
                }
            }

            AcademicYear ay = AcademicYearBusiness.Find(glo.AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(glo.SchoolID);
            ClassProfile classProfile = ClassProfileBusiness.Find(model.ClassID);

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THPT_DKDTTOTNGHIEP);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string fileName = reportDef.OutputNamePattern;

            List<object> lstRows = new List<object>();
            int index = 1;
            lstPG.ForEach(u =>
            {
                lstRows.Add(new
                {
                    Index = index++,
                    PupilCode = u.PupilCode,
                    FullName = u.FullName,
                    Genre = u.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female"),
                    BirthDate = u.BirthDate.ToString("dd/MM/yyyy"),
                    ClassName = u.ClassName,
                    Capacity = u.CapacityLevel,
                    Conduct = u.ConductLevel,
                    AbsentDays = u.AbsentDays,
                    Exemption = u.ExemptionResolution,
                    Notification = u.Description,
                    PraisePoints = u.PraisePoints,
                    ApprenticeshipPoints = u.ApprenticeshipPoints,
                    CertificatePoints = u.CertificatePoints,
                    MeritPoints = u.MeritPoints
                });
            });
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet temp = oBook.GetSheet(2);

            bool checkGDTX = sp.TrainingTypeID.HasValue ? sp.TrainingType.Resolution == "GDTX" : false;
            if (!checkGDTX) sheet.HideColumn(11);

            IVTRange topRange = temp.GetRange("A1", "M1");
            IVTRange midRange = temp.GetRange("A2", "M2");
            IVTRange botRange = temp.GetRange("A5", "M5");

            int START_ROW_INDEX = 9;
            int rowIndex = START_ROW_INDEX;
            for (int i = 0; i < lstPG.Count; i++)
            {
                IVTRange range = null;
                if (i % 5 == 4) range = botRange;
                else if (i % 5 == 0) range = topRange;
                else range = midRange;
                sheet.CopyPaste(range, rowIndex + i, 1, true);
                if ((i + 1) == lstPG.Count)
                {
                    range = botRange;
                    sheet.CopyPaste(range, rowIndex + i, 1, true);
                }
            }
            sheet.CopyPaste(botRange, START_ROW_INDEX + lstPG.Count - 1, 1, true);

            Dictionary<string, object> sheetData = new Dictionary<string, object>();
            sheetData["AcademicYear"] = ay.DisplayTitle;
            sheetData["SchoolName"] = sp.SchoolName;
            sheetData["ProvinceName"] = (sp.District != null ? sp.District.DistrictName : "");
            sheetData["SupervisingDeptName"] = UtilsBusiness.GetSupervisingDeptName(sp.SchoolProfileID,_globalInfo.AppliedLevel.Value);
            sheetData["ReportDate"] = string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            sheetData["Rows"] = lstRows;

            sheet.FillVariableValue(sheetData);
            temp.Delete();

            if (classProfile != null)
            {
                fileName = fileName.Replace("[ClassName]", classProfile.DisplayName);
            }
            else
            {
                fileName = fileName.Replace("_[ClassName]","");
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            result.FileDownloadName = ReportUtils.RemoveSpecialCharacters(fileName) + "." + reportDef.OutputFormat;
            return result;
        }

        private decimal? GetMeritPoints(decimal? appPoint, decimal? praisePoints, decimal? certificatePoints)
        {
            decimal meritPoints = 0;

            if (appPoint.HasValue) meritPoints = meritPoints + appPoint.Value;
            if (praisePoints.HasValue) meritPoints = meritPoints + praisePoints.Value;
            if (certificatePoints.HasValue) meritPoints = meritPoints + certificatePoints.Value;

            if (meritPoints > 4) meritPoints = 4;

            return meritPoints > 0 ? meritPoints : new Nullable<decimal>();
        }
    }
}
