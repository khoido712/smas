﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilGraduationArea
{
    public class PupilGraduationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilGraduationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilGraduationArea_default",
                "PupilGraduationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
