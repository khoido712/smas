/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilGraduationArea
{
    public class PupilGraduationConstants
    {
        public const string TOTAL = "TOTAL";
        public const string PAGE_NUMBER = "PAGE_NUMBER";
        public const string LIST_RESULT_TEMPORARY_GRADUATION_PAPER = "LIST_RESULT_TEMPORARY_GRADUATION_PAPER";
        public const string LIST_TEMPORARY_GRADUATION_PAPER = "LIST_TEMPORARY_GRADUATION_PAPER";
        public const string TEMPORARY_GRADUATION_PAPER_PERMISSION = "TEMPORARY_GRADUATION_PAPER_PERMISSION";

        public const string LIST_APPROVE_GRADUATION = "listApproveGraduation";
        public const string LIST_PUPILGRADUATION = "listPupilGraduation";
        public const string LIST_GRADUATIONGRADE = "ListGraduationGrade";
        public const string LIST_CLASS = "listClass";
        public const string APPROVE_GRADUATION_PERMISSION = "APPROVE_GRADUATION_PERMISSION";
        public const string CONDITIONAL_GRADUATION_PERMISSION = "CONDITIONAL_GRADUATION_PERMISSION";
        public const string GRANT_CERTIFICATION_PERMISSION = "GRANT_CERTIFICATION_PERMISSION";
    }
}