﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.PupilProfileArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Filter;
using Telerik.Web.Mvc;
using System.Text;
using System.Transactions;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Configuration;
using System.Web.Routing;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Log;
using System.Globalization;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;

namespace SMAS.Web.Areas.PupilProfileArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilProfileController : BaseController
    {
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPolicyTargetBusiness PolicyTargetBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly IAreaBusiness AreaBusiness;
        private readonly IPriorityTypeBusiness PriorityTypeBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDisabledTypeBusiness DisabledTypeBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPolicyRegimeBusiness PolicyRegimeBusiness;
        private readonly IPupilPraiseBusiness PupilPraiseBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilDisciplineBusiness PupilDisciplineBusiness;
        private readonly ICapacityLevelBusiness CapacityLevelBusiness;
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IVPupilRankingBusiness VPupilRankingBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly ILeavingReasonBusiness LeavingReasonBusiness;
        private readonly IParticularPupilBusiness ParticularPupilBusiness;
        private readonly IParticularPupilTreatmentBusiness ParticularPupilTreatmentBusiness;
        private readonly IParticularPupilCharacteristicBusiness ParticularPupilCharacteristicBusiness;
        private readonly IExemptedObjectTypeBusiness ExemptedObjectTypeBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IMTBusiness MTBusiness;
        private readonly IMovementAcceptanceBusiness MovementAcceptanceBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IVMarkRecordBusiness VMarkRecordBusiness;
        private readonly IVJudgeRecordBusiness VJudgeRecordBusiness;
        private readonly IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        private readonly ILocationBusiness LocationBusiness;
        private readonly IPupilFaultBusiness PupilFaultBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private readonly IHealthPeriodBusiness HealthPeriodBusiness;
        private readonly IEyeTestBusiness EyeTestBusiness;
        private readonly IPhysicalTestBusiness PhysicalTestBusiness;
        private readonly IENTTestBusiness ENTTestBusiness;
        private readonly ISpineTestBusiness SpineTestBusiness;
        private readonly IDentalTestBusiness DentalTestBusiness;
        private readonly IOverallTestBusiness OverallTestBusiness;
        private readonly IPraiseTypeBusiness PraiseTypeBusiness;
        private readonly IDisciplineTypeBusiness DisciplineTypeBusiness;
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;
        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private readonly IVillageBusiness VillageBusiness;
        private readonly IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness;
        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly IReviewBookPupilBusiness ReviewBookPupilBusiness;
        private readonly IEvaluationCommentsBusiness EvaluationCommentsBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness;
        private readonly IRewardCommentFinalBusiness RewardCommentFinalBusiness;
        private readonly ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness;
        private readonly ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness;
        private readonly IUpdateRewardBusiness UpdateRewardBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IPupilEmulationBusiness PupilEmulationBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        private readonly ISMSParentContractInClassDetailBusiness SMSParentContractInClassDetailBusiness;
        private readonly ISMSParentContractBusiness SMSParentContractBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IOtherEthnicBusiness OtherEthnicBusiness;

        private const string TEMPLATE_PUPILPROFILE = "BM_DanhSachHocSinhPT.xls";
        private const string TEMPLATE_PUPILPROFILE_2018 = "BM_DanhSachHocSinh_2018.xls";
        private const string TEMPLATE_PUPILPROFILE_FORMAT = "BM_DanhSachHocSinh{0}{1}{2}.xls";//KHOI,LOP,TRUONG
        private const string Log_Space = "; ";
        private const string SEMICOLON_SPACE = "; ";

        public PupilProfileController(IPupilProfileBusiness pupilprofileBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IPolicyTargetBusiness PolicyTargetBusiness
            , IGraduationLevelBusiness GraduationLevelBusiness
            , IEthnicBusiness EthnicBusiness
            , IReligionBusiness ReligionBusiness
            , ICodeConfigBusiness CodeConfigBusiness
            , IDistrictBusiness DistrictBusiness
            , ICommuneBusiness CommuneBusiness
            , IAreaBusiness AreaBusiness
            , IPriorityTypeBusiness PriorityTypeBusiness
            , IProvinceBusiness ProvinceBusiness
            , IDisabledTypeBusiness DisabledTypeBusiness
            , IFamilyTypeBusiness FamilyTypeBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IPolicyRegimeBusiness PolicyRegimeBusiness
            , IPupilPraiseBusiness PupilPraiseBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IPupilDisciplineBusiness PupilDisciplineBusiness
            , ICapacityLevelBusiness capacityLevelBusiness
            , IConductLevelBusiness conductLevelBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IClassMovementBusiness classMovementBusiness
            , ISchoolMovementBusiness schoolMovementBusiness
            , IPupilLeavingOffBusiness pupilLeavingOffBusiness
            , ILeavingReasonBusiness leavingReasonBusiness
            , IParticularPupilCharacteristicBusiness particularpupilcharacteristicBusiness
            , IParticularPupilBusiness particularPupilBusiness
            , IParticularPupilTreatmentBusiness particularPupilTreatmentBusiness
            , IExemptedObjectTypeBusiness exemptedObjectTypeBusiness
            , IExemptedSubjectBusiness exemptedSubjectBusiness
            , IClassSubjectBusiness classSubjectBusiness
            , IUserAccountBusiness userAccountBusiness
            , IMTBusiness mTBusiness
            , IMovementAcceptanceBusiness movementAcceptanceBusiness
            , IMarkRecordBusiness markRecordBusiness
            , IJudgeRecordBusiness judgeRecordBusiness
            , ISummedUpRecordBusiness summedUpRecordBusiness
            , ILocationBusiness locationBusiness
            , IPupilFaultBusiness pupilFaultBusiness
            , IPupilAbsenceBusiness pupilAbsenceBusiness
            , IMonitoringBookBusiness monitoringBookBusiness
            , IHealthPeriodBusiness healthPeriodBusiness
            , IEyeTestBusiness eyeTestBusiness
            , IPhysicalTestBusiness physicalTestBusiness
            , IENTTestBusiness eNTTestBusiness
            , ISpineTestBusiness spineTestBusiness
            , IDentalTestBusiness dentalTestBusiness
            , IOverallTestBusiness overallTestBusiness
            , IPraiseTypeBusiness praiseTypeBusiness
            , IDisciplineTypeBusiness disciplineTypeBusiness
            , IITQualificationLevelBusiness iTQualificationLevelBusiness
            , IForeignLanguageGradeBusiness foreignLanguageGradeBusiness
            , ISemeterDeclarationBusiness semesterDeclarationBusiness
            , IVillageBusiness villageBusiness
            , IVPupilRankingBusiness vPupilRankingBusiness
            , IVMarkRecordBusiness vMarkRecordBusiness
            , IVJudgeRecordBusiness vJudgeRecordBusiness
            , IVSummedUpRecordBusiness vSummedUpRecordBusiness
            , IRegisterSubjectSpecializeBusiness registerSubjectSpecializeBusiness,
            ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness,
            IReviewBookPupilBusiness ReviewBookPupilBusiness,
            IEvaluationCommentsBusiness EvaluationCommentsBusiness,
            ISummedEvaluationBusiness SummedEvaluationBusiness,
            IRewardCommentFinalBusiness RewardCommentFinalBusiness,
            ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness,
            ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness,
            IUpdateRewardBusiness UpdateRewardBusiness,
            IPupilRankingBusiness PupilRankingBusiness,
            IPupilEmulationBusiness PupilEmulationBusiness,
            IRestoreDataBusiness RestoreDataBusiness,
            IRestoreDataDetailBusiness RestoreDataDetailBusiness
            , ISMSParentContractInClassDetailBusiness SMSParentContractInClassDetailBusiness
            , ISMSParentContractBusiness SMSParentContractBusiness
            , IReportDefinitionBusiness ReportDefinitionBusiness
            , IOtherEthnicBusiness OtherEthnicBusiness)
        {
            this.OtherEthnicBusiness = OtherEthnicBusiness;
            this.PupilProfileBusiness = pupilprofileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PolicyTargetBusiness = PolicyTargetBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.ReligionBusiness = ReligionBusiness;
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.CommuneBusiness = CommuneBusiness;
            this.AreaBusiness = AreaBusiness;
            this.PriorityTypeBusiness = PriorityTypeBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.DisabledTypeBusiness = DisabledTypeBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PolicyRegimeBusiness = PolicyRegimeBusiness;
            this.PupilPraiseBusiness = PupilPraiseBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilDisciplineBusiness = PupilDisciplineBusiness;
            this.CapacityLevelBusiness = capacityLevelBusiness;
            this.ConductLevelBusiness = conductLevelBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassMovementBusiness = classMovementBusiness;
            this.SchoolMovementBusiness = schoolMovementBusiness;
            this.PupilLeavingOffBusiness = pupilLeavingOffBusiness;
            this.LeavingReasonBusiness = leavingReasonBusiness;
            this.ParticularPupilCharacteristicBusiness = particularpupilcharacteristicBusiness;
            this.ParticularPupilBusiness = particularPupilBusiness;
            this.ParticularPupilTreatmentBusiness = particularPupilTreatmentBusiness;
            this.ExemptedObjectTypeBusiness = exemptedObjectTypeBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.MTBusiness = mTBusiness;
            this.SchoolMovementBusiness = schoolMovementBusiness;
            this.MarkRecordBusiness = markRecordBusiness;
            this.JudgeRecordBusiness = judgeRecordBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.LocationBusiness = locationBusiness;
            this.PupilFaultBusiness = pupilFaultBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.HealthPeriodBusiness = healthPeriodBusiness;
            this.EyeTestBusiness = eyeTestBusiness;
            this.PhysicalTestBusiness = physicalTestBusiness;
            this.ENTTestBusiness = eNTTestBusiness;
            this.DentalTestBusiness = dentalTestBusiness;
            this.SpineTestBusiness = spineTestBusiness;
            this.OverallTestBusiness = overallTestBusiness;
            this.MovementAcceptanceBusiness = movementAcceptanceBusiness;
            this.PraiseTypeBusiness = praiseTypeBusiness;
            this.DisciplineTypeBusiness = disciplineTypeBusiness;
            this.ITQualificationLevelBusiness = iTQualificationLevelBusiness;
            this.ForeignLanguageGradeBusiness = foreignLanguageGradeBusiness;
            this.SemesterDeclarationBusiness = semesterDeclarationBusiness;
            this.VillageBusiness = villageBusiness;
            this.VPupilRankingBusiness = vPupilRankingBusiness;
            this.VMarkRecordBusiness = vMarkRecordBusiness;
            this.VJudgeRecordBusiness = vJudgeRecordBusiness;
            this.VSummedUpRecordBusiness = vSummedUpRecordBusiness;
            this.RegisterSubjectSpecializeBusiness = registerSubjectSpecializeBusiness;
            this.SummedEndingEvaluationBusiness = SummedEndingEvaluationBusiness;
            this.ReviewBookPupilBusiness = ReviewBookPupilBusiness;
            this.EvaluationCommentsBusiness = EvaluationCommentsBusiness;
            this.SummedEvaluationBusiness = SummedEvaluationBusiness;
            this.RewardCommentFinalBusiness = RewardCommentFinalBusiness;
            this.TeacherNoteBookMonthBusiness = TeacherNoteBookMonthBusiness;
            this.TeacherNoteBookSemesterBusiness = TeacherNoteBookSemesterBusiness;
            this.UpdateRewardBusiness = UpdateRewardBusiness;
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.PupilEmulationBusiness = PupilEmulationBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
            this.RestoreDataDetailBusiness = RestoreDataDetailBusiness;
            this.SMSParentContractInClassDetailBusiness = SMSParentContractInClassDetailBusiness;
            this.SMSParentContractBusiness = SMSParentContractBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
        }
        #region Page Action
        //
        // GET: /PupilProfileArea/
        //[CacheFilter(Duration = 60)]
        [CompressFilter(Order = 1)]
        public ActionResult Index(int? ClassID)
        {


            CheckActionPermissionMinView();
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            SetViewDataPermission("PupilProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            CheckPermissionForAction(ClassID, "ClassProfile");
            SetViewData(ClassID); //Dong nay o tren cung
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            Dictionary<string, bool> dicPermission = new Dictionary<string, bool>();
            dicPermission["PupilProfile"] = this.GetMenupermission("PupilProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole) > 0;
            dicPermission["ExemptedSubject"] = this.GetMenupermission("ExemptedSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole) > 0;
            dicPermission["ParticularPupil"] = this.GetMenupermission("ParticularPupil", _globalInfo.UserAccountID, _globalInfo.IsAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole) > 0;
            dicPermission["MovementAcceptance"] = (UtilsBusiness.IsBGH(_globalInfo.UserAccountID) || _globalInfo.IsAdmin || _globalInfo.IsAdminSchoolRole || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole);
            dicPermission["PupilPraise"] = this.GetMenupermission("PupilPraise", _globalInfo.UserAccountID, _globalInfo.IsAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole) > 0;
            ViewData[PupilProfileConstants.DICTIONARY_TABS_PERMISSION] = dicPermission;

            ViewData[PupilProfileConstants.IS_NAVIGATE_FROM_OTHER_PAGE] = ClassID.HasValue && ClassID.Value > 0;
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID.HasValue ? ClassID.Value : 0);
            bool isLockPupil = objAca.IsLockPupilProfile.HasValue && objAca.IsLockPupilProfile.Value;
            bool isLockSTT = acaYear.IsLockNumOrdinal.HasValue && acaYear.IsLockNumOrdinal.Value;
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;
                SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
                SearchInfo["CurrentClassID"] = ClassID;
                Paginate<PupilProfileViewModel> lst = this.SearchPaging(SearchInfo, !ClassID.HasValue || ClassID.Value <= 0, false);
                ViewData[PupilProfileConstants.LIST_PUPILPROFILE] = lst;
                //Get view data here
                if (_globalInfo.IsAdmin || (isGVCN && !isLockSTT))
                {
                    ViewData[PupilProfileConstants.VISIBLE_ORDER] = lst.List.Any();
                    ViewData[PupilProfileConstants.ENABLE_PAGING] = false;
                }
                else
                {
                    ViewData[PupilProfileConstants.VISIBLE_ORDER] = false;
                    ViewData[PupilProfileConstants.ENABLE_PAGING] = true;
                }
                ViewData[PupilProfileConstants.IS_SHOW_AVATAR] = acaYear.IsShowAvatar.HasValue && acaYear.IsShowAvatar.Value;
            }

            List<DisciplineType> lstDisciplineType = DisciplineTypeBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<PraiseType> LstPraiseType = PraiseTypeBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();

            ViewData[PupilProfileConstants.LIST_PRAISE_TYPE] = new SelectList(LstPraiseType, "PraiseTypeID", "Resolution");
            ViewData[PupilProfileConstants.LIST_DISCIPLINE_TYPE] = new SelectList(lstDisciplineType, "DisciplineTypeID", "Resolution");
            ViewData[PupilProfileConstants.IS_ENABLE_NOTE_PRAISE] = LstPraiseType.Count > 0 ? false : true;
            ViewData[PupilProfileConstants.IS_ENABLE_NOTE_DISCIPLINE] = lstDisciplineType.Count > 0 ? false : true;
            ViewData[PupilProfileConstants.LIST_DISCIPLINE_LEVEL] = ViewData[PupilProfileConstants.LIST_PRAISE_LEVEL] = new SelectList(CommonList.DisciplineLevel(), "value", "key");
            bool isCurrentYear = UtilsBusiness.IsCurrentAcademicYear(acaYear);
            ViewData[PupilProfileConstants.ENABLE_CREATE_DELETE] = ((_globalInfo.IsAdmin && isCurrentYear) || (isGVCN && isCurrentYear && !isLockPupil));

            return PartialView();
        }

        public int CheckSeePermission(string Controller, int UserAccountID, bool IsAdmin, int RolID = 0)
        {
            // Lay quyen nguoi dung
            return GetMenupermission(Controller, UserAccountID, IsAdmin, RolID);
        }

        //
        // GET: /PupilProfileArea/Search
        //[CacheFilter(Duration = 60)]
        [CompressFilter(Order = 1)]
        public PartialViewResult Search(SearchViewModel frm, int page = 1, string orderBy = "")
        {
            Utils.Utils.TrimObject(frm);
            Session[PupilProfileConstants.SEARCHFORM] = frm;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            ViewData[PupilProfileConstants.IS_SHOW_AVATAR] = acaYear.IsShowAvatar.HasValue && acaYear.IsShowAvatar.Value;

            int isEnableRedirect = CheckSeePermission("PupilSummaryReport", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData["isEnableRedirect"] = isEnableRedirect;

            //add search info
            SearchInfo["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["CurrentClassID"] = frm.Class;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["ProfileStatus"] = frm.Status;
            SearchInfo["EthnicID"] = frm.Ethnic;

            Paginate<PupilProfileViewModel> lst = this.SearchPaging(SearchInfo, !frm.Class.HasValue || frm.Class <= 0, false, page, orderBy, true);
            ViewData[PupilProfileConstants.LIST_PUPILPROFILE] = lst;
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, frm.Class.HasValue ? frm.Class.Value : 0);

            bool isLockPupil = acaYear.IsLockPupilProfile.HasValue && acaYear.IsLockPupilProfile.Value;
            bool isLockSTT = acaYear.IsLockNumOrdinal.HasValue && acaYear.IsLockNumOrdinal.Value;
            bool isCurrentYear = UtilsBusiness.IsCurrentAcademicYear(acaYear);
            ViewData[PupilProfileConstants.ENABLE_CREATE_DELETE] = ((_globalInfo.IsAdmin && isCurrentYear) || (isGVCN && isCurrentYear && !isLockPupil));

            //Get view data here
            if (frm.EducationLevel.HasValue && frm.Class.HasValue && (_globalInfo.IsAdmin || (isGVCN && !isLockSTT)))
            {
                ViewData[PupilProfileConstants.VISIBLE_ORDER] = lst.List.Any();
                ViewData[PupilProfileConstants.ENABLE_PAGING] = false;
            }
            else
            {
                ViewData[PupilProfileConstants.VISIBLE_ORDER] = false;
                ViewData[PupilProfileConstants.ENABLE_PAGING] = true;
            }
            SetViewDataPermission("PupilProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            return PartialView("_List");
        }

        public PartialViewResult SearchExempted(SearchViewModel frm)
        {
            GlobalInfo glo = new GlobalInfo();
            Utils.Utils.TrimObject(frm);

            List<int> lstClassID = getClassFromEducationLevel(0).Select(u => u.ClassProfileID).ToList();
            Dictionary<int, bool> dicPermission = UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, lstClassID);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = glo.AppliedLevel.Value;
            dic["ClassID"] = frm.Class;
            dic["EducationLevelID"] = frm.EducationLevel;
            dic["FullName"] = frm.Fullname;
            dic["PupilCode"] = frm.PupilCode;
            dic["Genre"] = frm.Genre;
            dic["AcademicYearID"] = glo.AcademicYearID;
            dic["CheckWithClass"] = "CheckWithClass";

            var data = (from poc in PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value, dic)
                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                        join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                        join edu in EducationLevelBusiness.All on cp.EducationLevelID equals edu.EducationLevelID
                        join ex in ExemptedSubjectBusiness.All on new { poc.ClassID, poc.PupilID } equals new { ex.ClassID, ex.PupilID }
                        where lstClassID.Contains(poc.ClassID)
                        && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        select new ExemptedSubjectViewModel
                        {
                            ExemptedObjectID = ex.ExemptedObjectID,
                            EducationLevelID = edu.EducationLevelID,
                            EducationLevelName = edu.Resolution,
                            ExemptedSubjectID = ex.ExemptedSubjectID,
                            ClassID = poc.ClassID,
                            ClassName = cp.DisplayName,
                            PupilID = poc.PupilID,
                            FullName = pp.FullName,
                            Name = pp.Name,
                            OrderInClass = poc.OrderInClass,
                            Status = poc.Status,
                            PupilCode = pp.PupilCode,
                            SubjectName = ex.SubjectCat.DisplayName,
                            FirstSemesterExemptType = ex.FirstSemesterExemptType,
                            SecondSemesterExemptType = ex.SecondSemesterExemptType,
                            FirstSemesterExemptTypeName = string.Empty,
                            SecondSemesterExemptTypeName = string.Empty,
                            ClassOrderNumber = cp.OrderNumber
                        });

            if (frm.Class.HasValue && frm.Class.Value > 0)
            {
                data = data.OrderBy(u => u.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName);
            }
            else
            {
                data = data.OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(u => u.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName);
            }

            var lstSemesterExemptType = CommonList.ExemptType();
            var listData = data.ToList();
            foreach (var item in listData)
            {
                item.EnableEdit = glo.IsCurrentYear && (dicPermission[item.ClassID] || glo.IsAdminSchoolRole) && item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING;
                item.FirstSemesterExemptTypeName = GetSemesterExemptTypeName(item.FirstSemesterExemptType, lstSemesterExemptType);
                item.SecondSemesterExemptTypeName = GetSemesterExemptTypeName(item.SecondSemesterExemptType, lstSemesterExemptType);
            }

            ViewData[PupilProfileConstants.LIST_EXEMPTED_SUBJECT] = listData;

            return PartialView("_ListExempted");
        }

        public PartialViewResult SearchParticular(SearchViewModel frm)
        {
            GlobalInfo glo = new GlobalInfo();
            Utils.Utils.TrimObject(frm);

            List<int> lstClassID = getClassFromEducationLevel(0).Select(u => u.ClassProfileID).ToList();
            Dictionary<int, bool> dicPermission = UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, lstClassID);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = glo.AppliedLevel;
            SearchInfo["SchoolID"] = glo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchInfo["ClassID"] = frm.Class;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["CheckWithClass"] = "CheckWithClass";

            //Theo nghiệp vụ mới, mỗi ParticularPupil sẽ chỉ có 1 ParticularPupilCharacteristic do đó khi join sang bảng ParticularPupilCharacteristic 
            //tuy là liên kết 1 - n nhưng thực tế là liên kết 1 - 1. Theo lý thuyết thì danh sách học sinh sẽ không bị duplicate. 
            //Nếu bị thì do chức năng thêm mới học sinh cá biệt.
            List<ParticularPupilViewModel> lst = (from poc in PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo)
                                                  join pr in ParticularPupilBusiness.All on new { poc.ClassID, poc.PupilID } equals new { pr.ClassID, pr.PupilID }
                                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                  join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                  join ppc in ParticularPupilCharacteristicBusiness.All on pr.ParticularPupilID equals ppc.ParticularPupilID into g1
                                                  from j1 in g1.DefaultIfEmpty()
                                                  where lstClassID.Contains(poc.ClassID)
                                                  && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                  select new ParticularPupilViewModel
                                                  {
                                                      ClassID = poc.ClassID,
                                                      DisplayName = cp.DisplayName,
                                                      EducationLevelID = cp.EducationLevelID,
                                                      FullName = pp.FullName,
                                                      ParticularPupilID = pr.ParticularPupilID,
                                                      PupilCode = pp.PupilCode,
                                                      PupilID = poc.PupilID,
                                                      RealUpdatedDate = pr.RealUpdatedDate,
                                                      PsychologyCharacteristic = j1.PsychologyCharacteristic,
                                                      ChangesNoteOfPupil = j1.FamilyCharacteristic,
                                                      Name = pp.Name,
                                                      OrderInClass = poc.OrderInClass,
                                                      Status = poc.Status,
                                                      ClassOrderNumber = cp.OrderNumber
                                                  }).OrderBy(o => o.EducationLevelID)
                                                  .ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            foreach (ParticularPupilViewModel item in lst)
                item.EnableEdit = glo.IsCurrentYear && (dicPermission[item.ClassID] || glo.IsAdminSchoolRole) && item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING;

            ViewData[PupilProfileConstants.LIST_PARTICULARPUPIL_TREATMENT] = lst.OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.DisplayName).ThenBy(u => u.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            return PartialView("_ListParticular");
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSchoolMovement(SearchViewModel frm)
        {
            GlobalInfo glo = new GlobalInfo();

            Dictionary<int, List<ComboObject>> dicClassByEdu = new Dictionary<int, List<ComboObject>>();
            var lstClassProfile = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> {
                                                                                            { "AcademicYearID", glo.AcademicYearID }
                                                                                        }).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName)
                                                                                        .Select(u => new { u.ClassProfileID, u.EducationLevelID, u.DisplayName })
                                                                                        .ToList();
            glo.EducationLevels.ForEach(u =>
            {
                dicClassByEdu[u.EducationLevelID] = lstClassProfile.Where(v => v.EducationLevelID == u.EducationLevelID).Select(v => new ComboObject { key = v.ClassProfileID.ToString(), value = v.DisplayName }).ToList();
            });

            ViewData[PupilProfileConstants.DIC_CLASS_BY_EDUCATION_LEVEL] = dicClassByEdu;

            AcademicYear acaYear = AcademicYearBusiness.Find(glo.AcademicYearID);
            int curYear = acaYear.Year;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = glo.AppliedLevel;
            SearchInfo["MovedToSchoolID"] = glo.SchoolID.Value;
            SearchInfo["MovedToClassID"] = frm.Class;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["Genre"] = frm.Genre;

            var query = from sm in SchoolMovementBusiness.Search(SearchInfo)
                        join pp in PupilProfileBusiness.All on sm.PupilID equals pp.PupilProfileID
                        join sp in SchoolProfileBusiness.All on sm.SchoolID equals sp.SchoolProfileID
                        join edu in EducationLevelBusiness.All on sm.EducationLevelID equals edu.EducationLevelID
                        join aca in AcademicYearBusiness.All on sm.AcademicYearID equals aca.AcademicYearID
                        join cp in ClassProfileBusiness.All on sm.MovedToClassID equals cp.ClassProfileID into g1
                        from j1 in g1.DefaultIfEmpty()
                        where sm.MovedDate >= acaYear.FirstSemesterStartDate && sm.MovedDate <= acaYear.SecondSemesterEndDate && !sm.MovedToClassID.HasValue
                        && (!j1.IsActive.HasValue || (j1.IsActive.HasValue && j1.IsActive.Value))
                        select new SchoolMovementViewModel
                        {
                            SchoolMovementID = sm.SchoolMovementID,
                            EducationLevelID = sm.EducationLevelID,
                            EducationLevel = edu.Resolution,
                            PupilID = sm.PupilID,
                            Semester = sm.Semester,
                            FullName = pp.FullName,
                            Name = pp.Name,
                            PupilCode = pp.PupilCode,
                            BirthDate = pp.BirthDate,
                            Genre = pp.Genre,
                            FromSchoolID = sm.SchoolID,
                            FromSchoolName = sp.SchoolName,
                            ClassName = j1.DisplayName,
                            ClassOrderNumber = j1.OrderNumber
                        };

            List<SchoolMovementViewModel> lstSchoolMovement = query.OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            lstSchoolMovement.ForEach(u =>
            {
                u.GenreDisplay = u.Genre == SystemParamsInFile.GENRE_MALE
                                                ? Res.Get("Common_Label_Male")
                                                : (u.Genre == SystemParamsInFile.GENRE_FEMALE ? Res.Get("Common_Label_Female") : string.Empty);
                u.SemesterDisplay = u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                ? Res.Get("Semester_Of_Year_First")
                                                : (u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND ? Res.Get("Semester_Of_Year_Second") : string.Empty);
            });

            ViewData[PupilProfileConstants.LIST_SCHOOL_MOVEMENT] = lstSchoolMovement;

            return PartialView("_ListSchoolMovement");
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchPraise(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            GlobalInfo globalInfo = new GlobalInfo();
            List<int> lstClassID = getClassFromEducationLevel(0).Select(u => u.ClassProfileID).ToList();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = globalInfo.AcademicYearID;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["PraiseTypeID"] = frm.PraiseTypeID;
            SearchInfo["PraiseLevel"] = frm.PraiseLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["AppliedLevel"] = globalInfo.AppliedLevel;
            SearchInfo["ClassID"] = frm.Class;
            if (frm.Class.HasValue)
            {
                SearchInfo["Check"] = "Check";
            }
            SearchInfo["Genre"] = frm.Genre;

            var data = (from poc in PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo)
                        join ppr in PupilPraiseBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo) on new { poc.ClassID, poc.PupilID } equals new { ppr.ClassID, ppr.PupilID }
                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                        join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                        where lstClassID.Contains(poc.ClassID)
                        && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        select new { cp.EducationLevelID, poc.ClassID, cp.DisplayName, cp.OrderNumber, poc.PupilID, poc.OrderInClass, pp.FullName, pp.Name, pp.BirthDate, pp.Genre, pp.PupilCode })
                        .GroupBy(u => new { u.EducationLevelID, u.ClassID, u.DisplayName, u.OrderNumber, u.PupilID, u.OrderInClass, u.FullName, u.Name, u.BirthDate, u.Genre, u.PupilCode })
                        .Select(u => new PraisePupilViewModel
                        {
                            ClassID = u.Key.ClassID,
                            ClassName = u.Key.DisplayName,
                            PupilID = u.Key.PupilID,
                            OrderInClass = u.Key.OrderInClass,
                            FullName = u.Key.FullName,
                            Name = u.Key.Name,
                            BirthDate = u.Key.BirthDate,
                            Genre = u.Key.Genre,
                            PupilCode = u.Key.PupilCode,
                            ClassOrderNumber = u.Key.OrderNumber,
                            EducationLevelID = u.Key.EducationLevelID,
                            PraiseTotal = u.Count()
                        });
            List<PraisePupilViewModel> lstPupilPraise = data.OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            lstPupilPraise.ForEach(u =>
            {
                u.GenreDisplay = u.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female");
            });

            ViewData[PupilProfileConstants.LIST_PUPIL_PRAISE] = lstPupilPraise;

            return PartialView("_ListPraisePupil");
        }

        public PartialViewResult DetailPraisePupil(int pupilID, int classID)
        {
            CheckPermissionForAction(pupilID, "PupilOfClass", 3);
            CheckPermissionForAction(classID, "ClassProfile");

            GlobalInfo globalInfo = new GlobalInfo();

            AcademicYear acaYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            PupilProfile pupilProfile = PupilProfileBusiness.Find(pupilID);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = globalInfo.AcademicYearID;
            SearchInfo["PupilID"] = pupilID;
            SearchInfo["ClassID"] = classID;

            List<PupilPraiseViewModel> lstPupilPraiseDetail = new List<PupilPraiseViewModel>();

            lstPupilPraiseDetail = PupilPraiseBusiness
                                        .SearchBySchool(globalInfo.SchoolID.Value, SearchInfo)
                                        .Select(u => new PupilPraiseViewModel
                                        {
                                            ClassID = u.ClassID,
                                            ClassName = u.ClassProfile.DisplayName,
                                            Description = u.Description,
                                            EducationLevelID = u.ClassProfile.EducationLevelID,
                                            IsRecordedInSchoolReport = u.IsRecordedInSchoolReport,
                                            LocationName = string.Empty,
                                            PraiseDate = u.PraiseDate,
                                            PraiseLevel = u.PraiseLevel,
                                            PraiseResolution = u.PraiseType.Resolution,
                                            PraiseTypeID = u.PraiseTypeID,
                                            PupilID = u.PupilID,
                                            PupilPraiseID = u.PupilPraiseID,
                                            Semester = u.Semester,
                                        })
                                        .ToList();

            lstPupilPraiseDetail.ForEach(u =>
            {
                u.AcademicYearID = globalInfo.AcademicYearID.Value;
                u.AcademicYearName = acaYear.DisplayTitle;
                u.SchoolID = globalInfo.SchoolID.Value;
                u.SemesterName = GetSemesterName(u.Semester);
                u.LocationName = GetLocationName(u.PraiseLevel);
            });

            ViewData[PupilProfileConstants.LIST_DETAIL_PUPIL_PRAISE] = lstPupilPraiseDetail;
            ViewData[PupilProfileConstants.DETAIL_FULL_NAME] = pupilProfile.FullName;
            ViewData[PupilProfileConstants.HAS_RIGHT_PERMISSION] = globalInfo.IsCurrentYear && UtilsBusiness.HasHeadTeacherPermission(globalInfo.UserAccountID, classID);

            return PartialView("_ListDetailPraisePupil");
        }

        public PartialViewResult DetailDisciplinePupil(int pupilID, int classID)
        {
            CheckPermissionForAction(pupilID, "PupilProfile", 3);
            CheckPermissionForAction(classID, "ClassProfile");

            GlobalInfo globalInfo = new GlobalInfo();
            AcademicYear acaYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            PupilProfile pupilProfile = PupilProfileBusiness.Find(pupilID);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = globalInfo.AcademicYearID;
            SearchInfo["PupilID"] = pupilID;
            SearchInfo["ClassID"] = classID;

            List<PupilDisciplineViewModel> lstPupilDisciplineDetail = new List<PupilDisciplineViewModel>();

            lstPupilDisciplineDetail = PupilDisciplineBusiness
                                        .SearchBySchool(globalInfo.SchoolID.Value, SearchInfo)
                                        .Select(u => new PupilDisciplineViewModel
                                        {
                                            ClassID = u.ClassID,
                                            ClassName = u.ClassProfile.DisplayName,
                                            Description = u.Description,
                                            EducationLevelID = u.ClassProfile.EducationLevelID,
                                            IsRecordedInSchoolReport = u.IsRecordedInSchoolReport,
                                            LocationName = string.Empty,
                                            DisciplinedDate = u.DisciplinedDate,
                                            DisciplineLevel = u.DisciplineLevel,
                                            DisciplineResolution = u.DisciplineType.Resolution,
                                            DisciplineTypeID = u.DisciplineTypeID,
                                            PupilID = u.PupilID,
                                            PupilDisciplineID = u.PupilDisciplineID
                                        })
                                        .ToList();

            lstPupilDisciplineDetail.ForEach(u =>
            {
                u.AcademicYearID = globalInfo.AcademicYearID.Value;
                u.AcademicYearName = acaYear.DisplayTitle;
                u.SchoolID = globalInfo.SchoolID.Value;
                u.LocationName = GetLocationName(u.DisciplineLevel);
            });

            ViewData[PupilProfileConstants.LIST_DETAIL_PUPIL_DISCIPLINE] = lstPupilDisciplineDetail;
            ViewData[PupilProfileConstants.DETAIL_FULL_NAME] = pupilProfile.FullName;
            ViewData[PupilProfileConstants.HAS_RIGHT_PERMISSION] = globalInfo.IsCurrentYear && UtilsBusiness.HasHeadTeacherPermission(globalInfo.UserAccountID, classID);

            return PartialView("_ListDetailDisciplinePupil");
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchDiscipline(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            GlobalInfo globalInfo = new GlobalInfo();
            List<int> lstClassID = getClassFromEducationLevel(0).Select(u => u.ClassProfileID).ToList();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = globalInfo.AcademicYearID;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["DisciplineTypeID"] = frm.DisciplineTypeID;
            SearchInfo["DisciplineLevel"] = frm.DisciplineLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["AppliedLevel"] = globalInfo.AppliedLevel;
            SearchInfo["ClassID"] = frm.Class;
            if (frm.Class.HasValue)
            {
                SearchInfo["Check"] = "Check";
            }
            SearchInfo["Genre"] = frm.Genre;

            var data = (from poc in PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo)
                        join ppr in PupilDisciplineBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo) on new { poc.ClassID, poc.PupilID } equals new { ppr.ClassID, ppr.PupilID }
                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                        join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                        where lstClassID.Contains(poc.ClassID)
                        && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        select new { cp.EducationLevelID, poc.ClassID, cp.DisplayName, cp.OrderNumber, poc.PupilID, poc.OrderInClass, pp.FullName, pp.Name, pp.BirthDate, pp.Genre, pp.PupilCode })
                        .GroupBy(u => new { u.EducationLevelID, u.ClassID, u.DisplayName, u.OrderNumber, u.PupilID, u.OrderInClass, u.FullName, u.Name, u.BirthDate, u.Genre, u.PupilCode })
                        .Select(u => new DisciplinePupilViewModel
                        {
                            ClassID = u.Key.ClassID,
                            ClassName = u.Key.DisplayName,
                            PupilID = u.Key.PupilID,
                            OrderInClass = u.Key.OrderInClass,
                            FullName = u.Key.FullName,
                            Name = u.Key.Name,
                            BirthDate = u.Key.BirthDate,
                            Genre = u.Key.Genre,
                            PupilCode = u.Key.PupilCode,
                            EducationLevelID = u.Key.EducationLevelID,
                            ClassOrderNumber = u.Key.OrderNumber,
                            DisciplineTotal = u.Count()
                        });
            List<DisciplinePupilViewModel> lstPupilDiscipline = data.OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            lstPupilDiscipline.ForEach(u =>
            {
                u.GenreDisplay = u.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female");
            });

            ViewData[PupilProfileConstants.LIST_PUPIL_DISCIPLINE] = lstPupilDiscipline;

            return PartialView("_ListDisciplinePupil");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SearchReserve(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //Lay nam hoc truoc
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            int prevYear = aca.Year - 1;
            AcademicYear prevAca = AcademicYearBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value && o.Year == prevYear && o.IsActive == true).FirstOrDefault();

            int prevAcademicYearID = -1;
            if (prevAca != null)
            {
                prevAcademicYearID = prevAca.AcademicYearID;
            }

            //Lay nam hoc truoc nua
            int prev2Year = aca.Year - 2;
            AcademicYear prev2Aca = AcademicYearBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value && o.Year == prev2Year && o.IsActive == true).FirstOrDefault();
            int prev2AcademicYearID = -1;
            if (prev2Aca != null)
            {
                prev2AcademicYearID = prev2Aca.AcademicYearID;
            }

            //Lay hoc sinh thoi hoc trong HK1 va HK2 cua nam truoc
            SearchInfo["AcademicYearID"] = prevAcademicYearID;
            SearchInfo["FullName"] = frm.ReserveFullname;
            SearchInfo["PupilCode"] = frm.ReservePupilCode;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;

            IQueryable<ReservePupilViewModel> data1 = null;
            if (prevAcademicYearID > 0)
            {
                data1 = from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo)
                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                        join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                        join plo in PupilLeavingOffBusiness.All on new { poc.PupilID, poc.ClassID } equals new { plo.PupilID, plo.ClassID }
                        where plo.AcademicYearID == prevAcademicYearID
                       && plo.IsReserve == true
                       && plo.SchoolID == _globalInfo.SchoolID
                       && (plo.Semester == 1 || plo.Semester == 2)
                       && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        select new ReservePupilViewModel
                        {
                            PupilOfClassID = poc.PupilOfClassID,
                            PupilID = poc.PupilID,
                            ClassID = poc.ClassID,
                            Name = poc.PupilProfile.Name,
                            FullName = poc.PupilProfile.FullName,
                            PupilCode = poc.PupilProfile.PupilCode,
                            Genre = poc.PupilProfile.Genre,
                            BirthDate = poc.PupilProfile.BirthDate,
                            ClassName = poc.ClassProfile.DisplayName,
                            PupilLeaveOffID = plo.PupilLeavingOffID,
                            LeavingDate = plo.LeavingDate,
                            LeavingReason = plo.LeavingReason.Resolution,
                            AcceptEducationLevel = poc.ClassProfile.EducationLevelID,
                            AcceptClassID = plo.AcceptClassID,
                            IsDataInherit = plo.Semester == 2 ? true : false,
                            IsVnenClass = poc.ClassProfile.IsVnenClass == true ? true : false

                        };
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = prev2AcademicYearID;
            SearchInfo["FullName"] = frm.ReserveFullname;
            SearchInfo["PupilCode"] = frm.ReservePupilCode;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;

            //Lay hoc sinh thoi hoc trong he cua nam truoc nua
            IQueryable<ReservePupilViewModel> data2 = null;

            //Neu la cap 1
            if (prev2AcademicYearID > 0)
            {
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {

                    int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);

                    data2 = from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo)
                            join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                            join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                            join plo in PupilLeavingOffBusiness.All on new { poc.PupilID, poc.ClassID } equals new { plo.PupilID, plo.ClassID }
                            join see in SummedEndingEvaluationBusiness.All.Where(o => o.AcademicYearID == prev2AcademicYearID && o.LastDigitSchoolID == partition
                                && o.SchoolID == _globalInfo.SchoolID && o.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT && o.EvaluationID == 4)
                            on new { poc.PupilID, poc.ClassID } equals new { see.PupilID, see.ClassID } into des
                            from x in des.DefaultIfEmpty()
                            where plo.AcademicYearID == prev2AcademicYearID
                            && plo.IsReserve == true
                            && plo.SchoolID == _globalInfo.SchoolID
                            && plo.Semester == 6
                            && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                            select new ReservePupilViewModel
                            {
                                PupilOfClassID = poc.PupilOfClassID,
                                PupilID = poc.PupilID,
                                ClassID = poc.ClassID,
                                Name = poc.PupilProfile.Name,
                                FullName = poc.PupilProfile.FullName,
                                PupilCode = poc.PupilProfile.PupilCode,
                                Genre = poc.PupilProfile.Genre,
                                BirthDate = poc.PupilProfile.BirthDate,
                                ClassName = poc.ClassProfile.DisplayName,
                                PupilLeaveOffID = plo.PupilLeavingOffID,
                                LeavingDate = plo.LeavingDate,
                                LeavingReason = plo.LeavingReason.Resolution,
                                AcceptEducationLevel = (x != null && (x.EndingEvaluation == "HT" || x.RateAdd == 1)) ? poc.ClassProfile.EducationLevelID + 1 : poc.ClassProfile.EducationLevelID,
                                AcceptClassID = plo.AcceptClassID,
                                IsDataInherit = false,
                                IsVnenClass = false
                            };
                }
                else
                {
                    IDictionary<string, object> dic1 = new Dictionary<string, object>();
                    dic1["AcademicYearID"] = prev2AcademicYearID;
                    dic1["SchoolID"] = _globalInfo.SchoolID;
                    dic1["AppliedLevel"] = _globalInfo.AppliedLevel;
                    dic1["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                    dic1["StudyingJudgementID"] = 1;


                    IDictionary<string, object> dic2 = new Dictionary<string, object>();
                    dic2["SchoolID"] = _globalInfo.SchoolID;
                    dic2["AcademicYearID"] = prev2AcademicYearID;
                    dic2["SemesterID"] = GlobalConstants.SEMESTER_OF_YEAR_SECOND;


                    data2 = from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo)
                            join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                            join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                            join plo in PupilLeavingOffBusiness.All on new { poc.PupilID, poc.ClassID } equals new { plo.PupilID, plo.ClassID }
                            join pr in VPupilRankingBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic1) on new { poc.PupilID, poc.ClassID } equals new { pr.PupilID, pr.ClassID } into des1
                            from x in des1.DefaultIfEmpty()
                            join rbp in ReviewBookPupilBusiness.Search(dic2).Where(o => o.RateAndYear == 1 || o.RateAdd == 1) on new { poc.PupilID, poc.ClassID } equals new { rbp.PupilID, rbp.ClassID } into des2
                            from y in des2.DefaultIfEmpty()
                            where plo.AcademicYearID == prev2AcademicYearID
                            && plo.IsReserve == true
                            && plo.SchoolID == _globalInfo.SchoolID
                            && plo.Semester == 6
                            && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                            select new ReservePupilViewModel
                            {
                                PupilOfClassID = poc.PupilOfClassID,
                                PupilID = poc.PupilID,
                                ClassID = poc.ClassID,
                                Name = poc.PupilProfile.Name,
                                FullName = poc.PupilProfile.FullName,
                                PupilCode = poc.PupilProfile.PupilCode,
                                Genre = poc.PupilProfile.Genre,
                                BirthDate = poc.PupilProfile.BirthDate,
                                ClassName = poc.ClassProfile.DisplayName,
                                PupilLeaveOffID = plo.PupilLeavingOffID,
                                LeavingDate = plo.LeavingDate,
                                LeavingReason = plo.LeavingReason.Resolution,
                                AcceptEducationLevel = poc.ClassProfile.IsVnenClass == true ? (y != null ? poc.ClassProfile.EducationLevelID + 1 : poc.ClassProfile.EducationLevelID) :
                                (x != null ? poc.ClassProfile.EducationLevelID + 1 : poc.ClassProfile.EducationLevelID),
                                AcceptClassID = plo.AcceptClassID,
                                IsDataInherit = false,
                                IsVnenClass = poc.ClassProfile.IsVnenClass == true ? true : false
                            };

                }
            }

            List<ReservePupilViewModel> lstReserveModel = new List<ReservePupilViewModel>();

            if (data1 != null && data2 != null)
            {
                lstReserveModel = data1.Union(data2).OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            }
            else if (data1 != null)
            {
                lstReserveModel = data1.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            }
            else if (data2 != null)
            {
                lstReserveModel = data2.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            }

            //Lay danh sach lop tiep nhan
            List<ClassProfile> lstClassProfile = this.GetListClass();
            List<int> lstClassID = lstClassProfile.Select(o => o.ClassProfileID).ToList();
            lstReserveModel = lstReserveModel.Where(o => !o.AcceptClassID.HasValue || (o.AcceptClassID.HasValue && lstClassID.Contains(o.AcceptClassID.Value))).ToList();

            ViewData[PupilProfileConstants.LIST_PUPIL_RESERVE] = lstReserveModel;

            ViewData[PupilProfileConstants.LIST_ACCEPT_CLASS] = lstClassProfile;


            return PartialView("_ListReserve");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AcceptReserve(int? classId, int pupilId, int pupilLeavingOffID, int oldClassId, bool isVnenClass, bool isDataInherit)
        {
            if (classId == null || classId == 0)
            {
                throw new BusinessException("Thầy cô phải chọn lớp tiếp nhận");
            }
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //Lay nam hoc truoc
            int prevYear = aca.Year - 1;
            AcademicYear prevAca = AcademicYearBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value && o.Year == prevYear && o.IsActive == true).FirstOrDefault();
            int prevAcademicYearID = prevAca.AcademicYearID;
            //Them hoc sinh vao lop tiep nhan
            PupilOfClass poc = new PupilOfClass();
            poc.AcademicYearID = _globalInfo.AcademicYearID.Value;
            poc.AssignedDate = DateTime.Now.Date;
            poc.ClassID = classId.Value;
            poc.PupilID = pupilId;
            poc.SchoolID = _globalInfo.SchoolID.Value;
            poc.Status = GlobalConstants.PUPIL_STATUS_STUDYING;
            poc.SynchronizeID = 1;
            poc.Year = aca.Year;
            poc.NoConductEstimation = false;

            PupilOfClassBusiness.Insert(poc);

            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            if (isDataInherit)
            {
                #region Cap 1
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    //Lay nhan xet chi tiet cac thang
                    List<EvaluationComments> lstEc = EvaluationCommentsBusiness.All
                        .Where(o => o.AcademicYearID == prevAcademicYearID
                        && o.ClassID == oldClassId
                        && o.LastDigitSchoolID == partition
                        && o.PupilID == pupilId
                        && o.SchoolID == _globalInfo.SchoolID.Value
                        && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                        ).ToList();
                    foreach (EvaluationComments e in lstEc)
                    {
                        e.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        e.ClassID = classId.Value;
                        e.CreateTime = DateTime.Now;
                        e.EducationLevelID = cp.EducationLevelID;
                        e.UpdateTime = null;
                        e.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>("EVALUATION_COMMENTS_SEQ");

                        EvaluationCommentsBusiness.Insert(e);
                    }

                    //chuyen kiem tra cuoi ky
                    List<SummedEvaluation> lstSe = SummedEvaluationBusiness.All.Where
                        (o => o.AcademicYearID == prevAcademicYearID
                        && o.ClassID == oldClassId
                        && o.LastDigitSchoolID == partition
                        && o.PupilID == pupilId
                        && o.SchoolID == _globalInfo.SchoolID
                        && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                        ).ToList();
                    foreach (SummedEvaluation se in lstSe)
                    {
                        se.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        se.ClassID = classId.Value;
                        se.CreateTime = DateTime.Now;
                        se.EducationLevelID = cp.EducationLevelID;
                        se.SummedEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<long>("SUMMED_EVALUATION_SEQ");
                        se.UpdateTime = null;

                        SummedEvaluationBusiness.Insert(se);
                    }

                    //chuyen danh gia cuoi ky
                    List<SummedEndingEvaluation> lstSee = SummedEndingEvaluationBusiness.All
                        .Where(o => o.AcademicYearID == prevAcademicYearID
                        && o.ClassID == oldClassId
                        && o.LastDigitSchoolID == partition
                        && o.PupilID == pupilId
                        && o.SchoolID == _globalInfo.SchoolID
                        && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                        ).ToList();

                    foreach (SummedEndingEvaluation see in lstSee)
                    {
                        see.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        see.ClassID = classId.Value;
                        see.CreateTime = DateTime.Now;
                        see.SummedEndingEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<long>("SUMMED_ENDING_EVALUATION_SEQ");
                        see.UpdateTime = null;

                        SummedEndingEvaluationBusiness.Insert(see);
                    }

                    //chuyen khen thuong
                    List<RewardCommentFinal> lstRcf = RewardCommentFinalBusiness.All
                        .Where(o => o.AcademicYearID == prevAcademicYearID
                        && o.ClassID == oldClassId
                        && o.LastDigitSchoolID == partition
                        && o.PupilID == pupilId
                        && o.SchoolID == _globalInfo.SchoolID
                        && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                        ).ToList();

                    foreach (RewardCommentFinal rcf in lstRcf)
                    {
                        rcf.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        rcf.ClassID = classId.Value;
                        rcf.CreateTime = DateTime.Now;
                        rcf.RewardCommentFinalID = RewardCommentFinalBusiness.GetNextSeq<int>("REWARD_COMMENT_FINAL_SEQ");
                        rcf.UpdateTime = null;

                        RewardCommentFinalBusiness.Insert(rcf);
                    }
                }
                #endregion
                #region cap 2,3
                else
                {
                    //	Điểm chi tiết của các môn học của năm học hiện tại
                    List<VMarkRecord> lstVmr = VMarkRecordBusiness.All
                        .Where(o => o.AcademicYearID == prevAcademicYearID
                        && o.ClassID == oldClassId
                        && o.Last2digitNumberSchool == partition
                        && o.PupilID == pupilId
                        && o.SchoolID == _globalInfo.SchoolID
                        && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST

                        ).ToList();

                    MarkRecord mr;
                    foreach (VMarkRecord vmr in lstVmr)
                    {
                        mr = new MarkRecord();
                        foreach (var prop in vmr.GetType().GetProperties())
                        {
                            mr.GetType().GetProperty(prop.Name).SetValue(mr, prop.GetValue(vmr));
                        }
                        mr.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        mr.ClassID = classId.Value;
                        mr.CreatedDate = DateTime.Now.Date;
                        mr.CreatedAcademicYear = aca.Year;
                        mr.MarkRecordID = 0;

                        MarkRecordBusiness.Insert(mr);
                    }

                    List<VJudgeRecord> lstVjr = VJudgeRecordBusiness.All
                        .Where(o => o.AcademicYearID == prevAcademicYearID
                        && o.ClassID == oldClassId
                        && o.Last2digitNumberSchool == partition
                        && o.PupilID == pupilId
                        && o.SchoolID == _globalInfo.SchoolID
                        && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                        ).ToList();

                    JudgeRecord jr;
                    foreach (VJudgeRecord vjr in lstVjr)
                    {
                        jr = new JudgeRecord();
                        foreach (var prop in vjr.GetType().GetProperties())
                        {
                            jr.GetType().GetProperty(prop.Name).SetValue(jr, prop.GetValue(vjr));
                        }
                        jr.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        jr.ClassID = classId.Value;
                        jr.CreatedDate = DateTime.Now.Date;
                        jr.CreatedAcademicYear = aca.Year;
                        jr.JudgeRecordID = 0;

                        JudgeRecordBusiness.Insert(jr);
                    }

                    List<VSummedUpRecord> lstVsur = VSummedUpRecordBusiness.All
                        .Where(o => o.AcademicYearID == prevAcademicYearID
                        && o.ClassID == oldClassId
                        && o.Last2digitNumberSchool == partition
                        && o.PupilID == pupilId
                        && o.SchoolID == _globalInfo.SchoolID
                        && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                        ).ToList();

                    SummedUpRecord sur;
                    foreach (VSummedUpRecord vsur in lstVsur)
                    {
                        sur = new SummedUpRecord();
                        foreach (var prop in vsur.GetType().GetProperties())
                        {
                            sur.GetType().GetProperty(prop.Name).SetValue(sur, prop.GetValue(vsur));
                        }
                        sur.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sur.ClassID = classId.Value;
                        sur.CreatedAcademicYear = aca.Year;
                        sur.SummedUpRecordID = 0;

                        SummedUpRecordBusiness.Insert(sur);

                    }

                    if (isVnenClass)
                    {
                        //	Nhận xét chi tiết các tháng trong Sổ tay giáo viên
                        List<int> lstMonth = GetListMonthFirstSemester(prevAcademicYearID);
                        List<TeacherNoteBookMonth> lstTnbm = TeacherNoteBookMonthBusiness.All
                            .Where(o => o.AcademicYearID == prevAcademicYearID
                            && o.ClassID == oldClassId
                            && o.PartitionID == partition
                            && o.PupilID == pupilId
                            && o.SchoolID == _globalInfo.SchoolID
                            && lstMonth.Contains(o.MonthID)).ToList();

                        foreach (TeacherNoteBookMonth tnbm in lstTnbm)
                        {
                            tnbm.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            tnbm.ClassID = classId.Value;
                            tnbm.CreateTime = DateTime.Now;
                            tnbm.TeacherNoteBookMonthID = 0;
                            tnbm.UpdateTime = null;
                            if (tnbm.MonthID != 15)
                            {
                                int oldYear = Convert.ToInt32(tnbm.MonthID.ToString().Substring(0, 4));
                                int monthID = Convert.ToInt32(tnbm.MonthID.ToString().Replace(oldYear.ToString(), (oldYear + 1).ToString()));
                                tnbm.MonthID = monthID;

                            }

                            TeacherNoteBookMonthBusiness.Insert(tnbm);
                        }

                        //	Nhận xét trong Sổ đánh giá học sinh
                        List<TeacherNoteBookSemester> lstTnbs = TeacherNoteBookSemesterBusiness.All
                            .Where(o => o.AcademicYearID == prevAcademicYearID
                            && o.ClassID == oldClassId
                            && o.PartitionID == partition
                            && o.PupilID == pupilId
                            && o.SchoolID == _globalInfo.SchoolID
                            && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                            ).ToList();

                        foreach (TeacherNoteBookSemester tnbs in lstTnbs)
                        {
                            tnbs.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            tnbs.ClassID = classId.Value;
                            tnbs.CreateTime = DateTime.Now;
                            tnbs.TeacherNoteBookSemesterID = 0;
                            tnbs.UpdateTime = null;

                            TeacherNoteBookSemesterBusiness.Insert(tnbs);
                        }

                        //	Đánh giá cuối kỳ
                        List<ReviewBookPupil> lstRbp = ReviewBookPupilBusiness.All
                            .Where(o => o.AcademicYearID == prevAcademicYearID
                            && o.ClassID == oldClassId
                            && o.PartitionID == partition
                            && o.PupilID == pupilId
                            && o.SchoolID == _globalInfo.SchoolID
                            && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                            ).ToList();

                        foreach (ReviewBookPupil rbp in lstRbp)
                        {
                            rbp.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            rbp.ClassID = classId.Value;
                            rbp.CreateTime = DateTime.Now;
                            rbp.ReviewBookPupilID = 0;
                            rbp.UpdateTime = null;

                            ReviewBookPupilBusiness.Insert(rbp);
                        }

                        //	Kết quả nhận xét khen thưởng cuối kỳ
                        List<UpdateReward> lstUr = UpdateRewardBusiness.All
                            .Where(o => o.AcademicYearID == prevAcademicYearID
                            && o.ClassID == oldClassId
                            && o.PartitionID == partition
                            && o.PupilID == pupilId
                            && o.SchoolID == _globalInfo.SchoolID
                            && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                            ).ToList();

                        foreach (UpdateReward ur in lstUr)
                        {
                            ur.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            ur.ClassID = classId.Value;
                            ur.CreateTime = DateTime.Now;
                            ur.UpdateRewardID = 0;
                            ur.UpdateTime = null;

                            UpdateRewardBusiness.Insert(ur);
                        }
                    }
                    else
                    {
                        List<VPupilRanking> lstVpr = VPupilRankingBusiness.All
                            .Where(o => o.AcademicYearID == prevAcademicYearID
                            && o.ClassID == oldClassId
                            && o.Last2digitNumberSchool == partition
                            && o.PupilID == pupilId
                            && o.SchoolID == _globalInfo.SchoolID
                            && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                            ).ToList();

                        PupilRanking pr;
                        foreach (VPupilRanking vpr in lstVpr)
                        {
                            pr = new PupilRanking();
                            foreach (var prop in vpr.GetType().GetProperties())
                            {
                                pr.GetType().GetProperty(prop.Name).SetValue(pr, prop.GetValue(vpr));
                            }
                            pr.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            pr.EducationLevelID = cp.EducationLevelID;
                            pr.ClassID = classId.Value;
                            pr.CreatedAcademicYear = aca.Year;
                            pr.PupilRankingID = 0;


                            PupilRankingBusiness.Insert(pr);
                        }

                        List<PupilEmulation> lstPe = PupilEmulationBusiness.All
                            .Where(o => o.AcademicYearID == prevAcademicYearID
                            && o.ClassID == oldClassId
                            && o.Last2digitNumberSchool == partition
                            && o.PupilID == pupilId
                            && o.SchoolID == _globalInfo.SchoolID
                            && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                            ).ToList();

                        PupilEmulation objPe;
                        foreach (PupilEmulation pe in lstPe)
                        {
                            objPe = new PupilEmulation();
                            objPe.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objPe.ClassID = classId.Value;
                            objPe.CreatedDate = DateTime.Now;
                            objPe.HonourAchivementTypeID = pe.HonourAchivementTypeID;
                            objPe.Last2digitNumberSchool = pe.Last2digitNumberSchool;
                            objPe.M_Category = pe.M_Category;
                            objPe.M_OldID = pe.M_OldID;
                            objPe.M_ProvinceID = pe.M_ProvinceID;
                            objPe.MSourcedb = pe.MSourcedb;
                            objPe.PupilID = pe.PupilID;
                            objPe.SchoolID = pe.SchoolID;
                            objPe.Semester = pe.Semester;
                            objPe.SynchronizeID = pe.SynchronizeID;

                            PupilEmulationBusiness.Insert(objPe);
                        }
                    }

                }
                #endregion

            }

            PupilLeavingOff plo = PupilLeavingOffBusiness.Find(pupilLeavingOffID);
            plo.AcceptClassID = classId;
            PupilLeavingOffBusiness.Update(plo);
            PupilLeavingOffBusiness.Save();

            return Json(new JsonMessage("Tiếp nhận thành công"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UnAcceptReserve(int pupilId, int classId, int pupilLeaveOffId)
        {
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            //Xoa hoc sinh khoi lop
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = pupilId;
            dic["EducationLevelID"] = cp.EducationLevelID;
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            List<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            PupilOfClassBusiness.DeleteAll(lstPoc);

            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                List<EvaluationComments> lstEc = EvaluationCommentsBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.EducationLevelID == cp.EducationLevelID
                    && o.LastDigitSchoolID == partition
                    && o.PupilID == pupilId
                    && o.SchoolID == _globalInfo.SchoolID.Value
                    ).ToList();
                EvaluationCommentsBusiness.DeleteAll(lstEc);

                List<SummedEvaluation> lstSe = SummedEvaluationBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.PupilID == pupilId
                    && o.LastDigitSchoolID == partition
                    ).ToList();
                SummedEvaluationBusiness.DeleteAll(lstSe);

                List<SummedEndingEvaluation> lstSee = SummedEndingEvaluationBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.LastDigitSchoolID == partition
                    && o.PupilID == pupilId
                    ).ToList();
                SummedEndingEvaluationBusiness.DeleteAll(lstSee);

                List<RewardCommentFinal> lstRcf = RewardCommentFinalBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.LastDigitSchoolID == partition
                    && o.PupilID == pupilId
                    ).ToList();
                RewardCommentFinalBusiness.DeleteAll(lstRcf);
            }
            else
            {
                //Xoa diem
                List<MarkRecord> lstMr = MarkRecordBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.Last2digitNumberSchool == partition
                    && o.PupilID == pupilId
                    ).ToList();
                MarkRecordBusiness.DeleteAll(lstMr);

                List<JudgeRecord> lstJr = JudgeRecordBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.Last2digitNumberSchool == partition
                    && o.PupilID == pupilId
                    ).ToList();
                JudgeRecordBusiness.DeleteAll(lstJr);

                List<SummedUpRecord> lstSur = SummedUpRecordBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.Last2digitNumberSchool == partition
                    && o.PupilID == pupilId
                    ).ToList();
                SummedUpRecordBusiness.DeleteAll(lstSur);

                //Xoa het qua diem danh
                List<PupilAbsence> lstPa = PupilAbsenceBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.EducationLevelID == cp.EducationLevelID
                    && o.Last2digitNumberSchool == partition
                    && o.PupilID == pupilId
                    ).ToList();
                PupilAbsenceBusiness.DeleteAll(lstPa);

                if (cp.IsVnenClass == true)
                {
                    List<TeacherNoteBookMonth> lstTnbm = TeacherNoteBookMonthBusiness.All
                        .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                        && o.ClassID == classId
                        && o.PartitionID == partition
                        && o.PupilID == pupilId
                        ).ToList();
                    TeacherNoteBookMonthBusiness.DeleteAll(lstTnbm);

                    List<TeacherNoteBookSemester> lstTnbs = TeacherNoteBookSemesterBusiness.All
                        .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                        && o.ClassID == classId
                        && o.PartitionID == partition
                        && o.PupilID == pupilId).ToList();
                    TeacherNoteBookSemesterBusiness.DeleteAll(lstTnbs);

                    List<ReviewBookPupil> lstRbp = ReviewBookPupilBusiness.All
                        .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                        && o.ClassID == classId
                        && o.PartitionID == partition
                        && o.PupilID == pupilId
                       ).ToList();
                    ReviewBookPupilBusiness.DeleteAll(lstRbp);

                    List<UpdateReward> lstUr = UpdateRewardBusiness.All
                        .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                        && o.ClassID == classId
                        && o.PartitionID == partition
                        && o.PupilID == pupilId
                        ).ToList();
                    UpdateRewardBusiness.DeleteAll(lstUr);
                }
                else
                {
                    List<PupilRanking> lstPr = PupilRankingBusiness.All
                        .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                        && o.ClassID == classId
                        && o.EducationLevelID == cp.EducationLevelID
                        && o.Last2digitNumberSchool == partition
                        && o.PupilID == pupilId
                        ).ToList();
                    PupilRankingBusiness.DeleteAll(lstPr);

                    List<PupilEmulation> lstPe = PupilEmulationBusiness.All
                        .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                        && o.ClassID == classId
                        && o.Last2digitNumberSchool == partition
                        && o.PupilID == pupilId
                        ).ToList();
                    PupilEmulationBusiness.DeleteAll(lstPe);
                }

            }

            //cap nhat lai tinh trang bao luu
            PupilLeavingOff plo = PupilLeavingOffBusiness.Find(pupilLeaveOffId);
            plo.AcceptClassID = null;
            PupilLeavingOffBusiness.Update(plo);
            PupilLeavingOffBusiness.Save();

            return Json(new JsonMessage("Hủy tiếp nhận thành công"));
        }

        [SkipCheckRole]
        [GridAction(EnableCustomBinding = true)]

        [ValidateAntiForgeryToken]
        public ActionResult _GridSelect(SearchViewModel frm, int page = 1, string orderBy = "")
        {
            GlobalInfo global = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["CurrentClassID"] = frm.Class;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["ProfileStatus"] = frm.Status;
            SearchInfo["EthnicID"] = frm.Ethnic;
            bool alowPaging = (!frm.Class.HasValue || frm.Class.Value == 0);

            Paginate<PupilProfileViewModel> paging = this.SearchPaging(SearchInfo, alowPaging, false, page, orderBy);
            GridModel<PupilProfileViewModel> gm = new GridModel<PupilProfileViewModel>(paging.List);
            gm.Total = paging.total;

            return View(gm);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Create()
        {
            CheckActionPermissionMinView();
            GlobalInfo global = new GlobalInfo();
            PupilProfileViewModel PupilProfileViewModel = new PupilProfileViewModel();
            TryUpdateModel(PupilProfileViewModel);

            PupilProfile pupilprofile = new PupilProfile();

            Utils.Utils.BindTo(PupilProfileViewModel, pupilprofile);
            Utils.Utils.TrimObject(pupilprofile);

            //chuyen doi nam sinh bo,me,ng bao ho
            //VALIDATE nam sinh,doi du lieu tu nam sinh object sang nam sinh cua entity
            if (PupilProfileViewModel.iFatherBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_FatherBirthDate" });
                }
                pupilprofile.FatherBirthDate = new DateTime(PupilProfileViewModel.iFatherBirthDate.Value, 1, 1);
            }

            if (PupilProfileViewModel.iMotherBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_MotherBirthDate" });
                }
                pupilprofile.MotherBirthDate = new DateTime(PupilProfileViewModel.iMotherBirthDate.Value, 1, 1);
            }

            if (PupilProfileViewModel.iSponsorBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iSponsorBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_SponsorBirthDate" });
                }
                pupilprofile.SponsorBirthDate = new DateTime(PupilProfileViewModel.iSponsorBirthDate.Value, 1, 1);
            }

            int space = pupilprofile.FullName.LastIndexOf(" ");
            pupilprofile.Name = space == -1 ? pupilprofile.FullName : pupilprofile.FullName.Substring(space + 1);
            // AnhVD Hotfix - Tên học sinh không vượt quá 30 ký tự
            if (!string.IsNullOrWhiteSpace(pupilprofile.Name) && pupilprofile.Name.Length > 30)
                throw new BusinessException("PupilProfile_Validate_MaxLength30");
            // End 20140919
            bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            bool isSchoolModify = CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);

            if (!isAutoGenCode && PupilProfileViewModel.AutoGenCode)
                throw new BusinessException("CodeConfig_Message_ProvinceNotConfigYet");

            //Neu chon autogencode hoac nha truong cau hinh ma tu dong nhung khong duoc sua ma
            //if (PupilProfileViewModel.AutoGenCode && isSchoolModify)
            //    pupilprofile.PupilCode = CodeConfigBusiness.CreatePupilCode(global.AcademicYearID.Value, (int)PupilProfileViewModel.EducationLevel, global.SchoolID.Value);
            if (PupilProfileViewModel.AutoGenCode)
            {
                pupilprofile.PupilCode = CodeConfigBusiness.CreatePupilCode(global.AcademicYearID.Value, (int)PupilProfileViewModel.EducationLevel, global.SchoolID.Value);
            }
            else
            {
                //truong hop autogencode bi check va disable thi ko submut dc,ta phai tu check
                if (isAutoGenCode)
                {
                    if (!isSchoolModify)
                    {
                        pupilprofile.PupilCode = CodeConfigBusiness.CreatePupilCode(global.AcademicYearID.Value, (int)PupilProfileViewModel.EducationLevel, global.SchoolID.Value);
                    }
                }
            }

            //check neu PupilCode chua co thi ban ra exception
            if (pupilprofile.PupilCode == null || pupilprofile.PupilCode.Trim().Length == 0)
                throw new BusinessException("PupilProfile_Label_PupilCodeBlankError");

            if (!PupilProfileViewModel.EnrolmentType.HasValue)
                PupilProfileViewModel.EnrolmentType = 0;

            //check Email
            if (!string.IsNullOrEmpty(PupilProfileViewModel.Email))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.Email.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), "Email"));

                pupilprofile.Email = PupilProfileViewModel.Email;
            }

            pupilprofile.IsSwimming = PupilProfileViewModel.IsSwimming;
            pupilprofile.IdentifyNumber = PupilProfileViewModel.IdentifyNumber;

            pupilprofile.HomePlace = PupilProfileViewModel.HomePlace;

            //current academicyear and school
            pupilprofile.CurrentAcademicYearID = global.AcademicYearID.Value;
            pupilprofile.CurrentSchoolID = global.SchoolID.Value;
            pupilprofile.StorageNumber = PupilProfileViewModel.StorageNumber;

            //dua image vao pupilprofile
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                pupilprofile.Image = imageBytes;
            }

            pupilprofile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
            pupilprofile.ForeignLanguageTraining = PupilProfileViewModel.ForeignLanguageTraining.HasValue ? PupilProfileViewModel.ForeignLanguageTraining.Value : (int)0;

            //if (global.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY || global.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            //    if (!PupilProfileViewModel.ForeignLanguageTraining.HasValue || PupilProfileViewModel.ForeignLanguageTraining == 0)
            //        throw new BusinessException("requireResourceKey", new List<object>() { "PupilProfile_Label_ForeignLanguageTraining" });

            //#region Congnv: 27.11.2013 - Thêm mới thôn/xóm vào CSDL hoặc Lấy thôn xóm từ CSDL

            if (!string.IsNullOrWhiteSpace(PupilProfileViewModel.VillageName))
            {
                Village village;

                // Kiểm tra xem người dùng có nhập Commune
                if (PupilProfileViewModel.CommuneID != null)
                {
                    var existVillage =
                        this.VillageBusiness.All.FirstOrDefault(
                            p =>
                            p.VillageName.Trim().ToLower() == PupilProfileViewModel.VillageName.Trim().ToLower() &&
                            p.CommuneID == PupilProfileViewModel.CommuneID && p.IsActive == true);

                    if (existVillage != null)
                    {
                        //pupilprofile.
                        village = existVillage;
                    }
                    else
                    {
                        var newVillage = new Village
                        {
                            VillageName = PupilProfileViewModel.VillageName,
                            ShortName = PupilProfileViewModel.VillageName,
                            CreateDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            CommuneID = PupilProfileViewModel.CommuneID,
                            IsActive = true
                        };
                        village = VillageBusiness.Insert(newVillage);
                        VillageBusiness.Save();
                        //pupilprofile.VillageID = villageInserted.VillageID;
                    }
                    pupilprofile.VillageID = village.VillageID;
                }
                //else // Người dùng không nhập Commune
                //{
                //    var newVillage = new Village
                //    {
                //        VillageName = PupilProfileViewModel.VillageName,
                //        ShortName = PupilProfileViewModel.VillageName,
                //        CreateDate = DateTime.Now,
                //        ModifiedDate = DateTime.Now,
                //        //CommuneID = (int) PupilProfileViewModel.CommuneID,
                //        IsActive = true
                //    };
                //    village = VillageBusiness.Insert(newVillage);
                //    VillageBusiness.Save();
                //}
                // Cập nhật VillageID cho pupilProfile
                //pupilprofile.VillageID = village.VillageID;
            }

            //#endregion

            //Hệ thống lưu vào cơ sở dữ liệu bằng cách gọi hàm InsertPupilProfile
            this.PupilProfileBusiness.InsertPupilProfile(global.UserAccountID, pupilprofile, PupilProfileViewModel.EnrolmentType.Value);
            this.PupilProfileBusiness.Save();
            ViewData[CommonKey.AuditActionKey.OldJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.NewJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.ObjectID] = pupilprofile.PupilProfileID;
            ViewData[CommonKey.AuditActionKey.Description] = "Thêm mới HS";
            ViewData[CommonKey.AuditActionKey.Parameter] = pupilprofile.PupilProfileID;
            ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_ADD;
            ViewData[CommonKey.AuditActionKey.userFunction] = "Hồ sơ học sinh";
            ViewData[CommonKey.AuditActionKey.userDescription] = "Thêm mới HS " + pupilprofile.FullName + ", mã " + pupilprofile.PupilCode;
            #region HaiVT 04/06/2013 - tao ParentAccount - Bo di vi khong dung nua
            /*
            //so dien thoai cua cha, cua me, nguoi giam ho khong duoc phep giong nhau
            if ((PupilProfileViewModel.FatherMobile == PupilProfileViewModel.MotherMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.FatherMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.MotherMobile))
                || (PupilProfileViewModel.FatherMobile == PupilProfileViewModel.SponsorMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.FatherMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.SponsorMobile))
                || (PupilProfileViewModel.SponsorMobile == PupilProfileViewModel.MotherMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.SponsorMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.MotherMobile)))
            {
                throw new BusinessException("PupilProfile_Label_DuplicateMobileError");
            }
            //tai khoan theo so dt cha
            if (PupilProfileViewModel.IsFatherSMS)
            {
                if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.FatherMobile))
                {
                    CreateParentAccount(PupilProfileViewModel.FatherMobile, pupilprofile);
                }
                else
                {
                    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_FatherMobile_Empty" });
                }
            }

            //tai khoan theo so dt me
            if (PupilProfileViewModel.IsMotherSMS)
            {
                if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.MotherMobile))
                {
                    CreateParentAccount(PupilProfileViewModel.MotherMobile, pupilprofile);
                }
                else
                {
                    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_MotherMobile_Empty" });
                }
            }

            //tai khoan theo so dt nguoi giam ho
            if (PupilProfileViewModel.IsSponsorSMS)
            {
                if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.SponsorMobile))
                {
                    CreateParentAccount(PupilProfileViewModel.SponsorMobile, pupilprofile);
                }
                else
                {
                    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_SponsorMobile_Empty" });
                }
            }
             * */
            #endregion
            Session["ImagePath"] = null;
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Edit(int PupilProfileID)
        {
            CheckActionPermissionMinView();
            CheckPermissionForAction(PupilProfileID, "PupilProfile", 3);
            GlobalInfo global = new GlobalInfo();
            PupilProfile pupilprofile = PupilProfileBusiness.Find(PupilProfileID);
            string oldPupilCode = pupilprofile.PupilCode;


            List<PupilOfClass> lstpoc = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value,
                  new Dictionary<string, object>()
                    {
                        {"AcademicYearID",global.AcademicYearID},
                        {"PupilID",PupilProfileID}
                    }).ToList();
            PupilOfClass poc = lstpoc.Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).FirstOrDefault();
            int OldClassID = poc.ClassID;

            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, poc.ClassID))
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }

            PupilProfileViewModel PupilProfileViewModel = new PupilProfileViewModel();
            TryUpdateModel(PupilProfileViewModel);

            #region Tao du lieu ghi log chi tiet
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            StringBuilder oldObject = new StringBuilder();
            StringBuilder newObject = new StringBuilder();

            objectIDStr.Append(pupilprofile.PupilProfileID);
            descriptionStr.Append("Sửa hồ sơ học sinh");
            paramsStr.Append(pupilprofile.PupilProfileID);
            userFuntionsStr.Append("Hồ sơ học sinh");
            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
            userDescriptionsStr.Append("Cập nhật HS ").Append(pupilprofile.FullName + ", mã ").Append(pupilprofile.PupilCode).Append(", ");
            if (pupilprofile.CurrentClassID != PupilProfileViewModel.CurrentClassID)
            {
                oldObjectStr.Append("Lớp: " + pupilprofile.ClassProfile.DisplayName).Append(Log_Space);
                ClassProfile objOldClass = ClassProfileBusiness.Find(PupilProfileViewModel.CurrentClassID);
                if (objOldClass != null)
                {
                    newObjectStr.Append("Lớp: " + objOldClass.DisplayName).Append(Log_Space);
                }
            }

            if (pupilprofile.FullName != PupilProfileViewModel.FullName)
            {
                oldObjectStr.Append("Họ tên: " + pupilprofile.FullName).Append(Log_Space);
                newObjectStr.Append("Họ tên: " + PupilProfileViewModel.FullName).Append(Log_Space);
            }

            if (pupilprofile.Genre != PupilProfileViewModel.Genre)
            {
                oldObjectStr.Append("Giới tính: " + (pupilprofile.Genre == 0 ? "Nam" : "Nứ")).Append(Log_Space);
                newObjectStr.Append("Giới tính: " + (PupilProfileViewModel.Genre == 0 ? "Nam" : "Nữ")).Append(Log_Space);
            }

            if (pupilprofile.BirthDate.Date != PupilProfileViewModel.BirthDate.Date)
            {
                oldObjectStr.Append("Ngày sinh: " + pupilprofile.BirthDate).Append(Log_Space);
                newObjectStr.Append("Ngày sinh: " + PupilProfileViewModel.BirthDate).Append(Log_Space);
            }

            if (pupilprofile.ProvinceID != PupilProfileViewModel.ProvinceID)
            {
                oldObjectStr.Append("Tỉnh thành: " + pupilprofile.Province.ProvinceName).Append(Log_Space);
                newObjectStr.Append("Tỉnh thành: " + ProvinceBusiness.Find(PupilProfileViewModel.ProvinceID).ProvinceName).Append(Log_Space);
            }

            if (pupilprofile.EnrolmentDate != PupilProfileViewModel.EnrolmentDate)
            {
                oldObjectStr.Append("Ngày vào trường: " + pupilprofile.EnrolmentDate).Append(Log_Space);
                newObjectStr.Append("Ngày vào trường: " + PupilProfileViewModel.EnrolmentDate).Append(Log_Space);
            }

            #endregion

            Utils.Utils.BindTo(PupilProfileViewModel, pupilprofile);
            Utils.Utils.TrimObject(pupilprofile);
            pupilprofile.ClassType = PupilProfileViewModel.ClassType;
            //chuyen doi nam sinh bo,me,ng bao ho
            //VALIDATE nam sinh,doi du lieu tu nam sinh object sang nam sinh cua entity
            if (PupilProfileViewModel.iFatherBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_FatherBirthDate" });
                }
                pupilprofile.FatherBirthDate = new DateTime(PupilProfileViewModel.iFatherBirthDate.Value, 1, 1);
            }
            if (PupilProfileViewModel.iMotherBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_MotherBirthDate" });
                }
                pupilprofile.MotherBirthDate = new DateTime(PupilProfileViewModel.iMotherBirthDate.Value, 1, 1);
            }
            if (PupilProfileViewModel.iSponsorBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iSponsorBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_SponsorBirthDate" });
                }
                pupilprofile.SponsorBirthDate = new DateTime(PupilProfileViewModel.iSponsorBirthDate.Value, 1, 1);
            }

            if (!PupilProfileViewModel.EnrolmentType.HasValue)
                PupilProfileViewModel.EnrolmentType = 0;

            bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            bool isSchoolModify = CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);

            if (!isAutoGenCode && PupilProfileViewModel.AutoGenCode)
                throw new BusinessException("CodeConfig_Message_ProvinceNotConfigYet");
            pupilprofile.IdentifyNumber = PupilProfileViewModel.IdentifyNumber;
            if (!string.IsNullOrEmpty(PupilProfileViewModel.Email))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.Email.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), "Email"));
                pupilprofile.Email = PupilProfileViewModel.Email;
            }

            if (!string.IsNullOrEmpty(PupilProfileViewModel.MotherEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.MotherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_MotherEmail")));
                pupilprofile.MotherEmail = PupilProfileViewModel.MotherEmail;
            }

            if (!string.IsNullOrEmpty(PupilProfileViewModel.FatherEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.FatherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_FatherEmail")));
                pupilprofile.FatherEmail = PupilProfileViewModel.FatherEmail;
            }

            if (!string.IsNullOrEmpty(PupilProfileViewModel.SponsorEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.SponsorEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_SponsorEmail")));
                pupilprofile.SponsorEmail = PupilProfileViewModel.SponsorEmail;
            }

            pupilprofile.HomePlace = PupilProfileViewModel.HomePlace;

            //Neu chon autogencode va nha truong duoc phep sua ma
            if (PupilProfileViewModel.AutoGenCode && isSchoolModify)
            {
                pupilprofile.PupilCode = CodeConfigBusiness.CreatePupilCode(global.AcademicYearID.Value, (int)PupilProfileViewModel.EducationLevel, global.SchoolID.Value);
            }
            //Neu sinh ma tu dong va truong khong duoc sua thi giu nguyen code cu
            if (isAutoGenCode && !isSchoolModify)
                pupilprofile.PupilCode = oldPupilCode;


            //check neu PupilCode chua co thi ban ra exception
            if (pupilprofile.PupilCode == null || pupilprofile.PupilCode.Trim().Length == 0)
                throw new BusinessException("PupilProfile_Label_PupilCodeBlankError");

            if (pupilprofile.PupilCode != oldPupilCode)
            {
                oldObjectStr.Append("Mã học sinh: " + pupilprofile.PupilCode).Append(Log_Space);
                newObjectStr.Append("Mã học sinh: " + oldPupilCode).Append(Log_Space);
            }
            //de phong name loi
            int space = pupilprofile.FullName.LastIndexOf(" ");
            pupilprofile.Name = space == -1 ? pupilprofile.FullName : pupilprofile.FullName.Substring(space + 1);
            // AnhVD Hotfix - Tên học sinh không vượt quá 30 ký tự
            if (!string.IsNullOrWhiteSpace(pupilprofile.Name) && pupilprofile.Name.Length > 30)
                throw new BusinessException("PupilProfile_Validate_MaxLength30");
            // End 20140919

            //current academicyear and school
            pupilprofile.CurrentAcademicYearID = global.AcademicYearID.Value;
            pupilprofile.CurrentSchoolID = global.SchoolID.Value;
            pupilprofile.IsSwimming = PupilProfileViewModel.IsSwimming;
            //dua image vao pupilprofile
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                pupilprofile.Image = imageBytes;
            }

            //status
            if (pupilprofile.ProfileStatus == 0)
                pupilprofile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;

            pupilprofile.ForeignLanguageTraining = PupilProfileViewModel.ForeignLanguageTraining.HasValue ? PupilProfileViewModel.ForeignLanguageTraining.Value : (int)0;

            #region Congnv - 03.12.2013: Cập nhật Village
            if (!string.IsNullOrWhiteSpace(PupilProfileViewModel.VillageName))
            {
                PupilProfileViewModel.VillageName = PupilProfileViewModel.VillageName.Trim();
                Village village;
                // Kiểm tra xem người dùng có nhập Commune
                if (PupilProfileViewModel.CommuneID != null)
                {
                    // Tim xem ten thon xom da co hay chua
                    var existVillage =
                        this.VillageBusiness.All.FirstOrDefault(
                            p =>
                            p.VillageName.Trim().ToLower() == PupilProfileViewModel.VillageName.ToLower() &&
                            p.CommuneID == PupilProfileViewModel.CommuneID && p.IsActive == true);
                    // Neu co roi thi update lai (truong hop chuyen chu thuong thanh chu hoa hoac nguoc lai)
                    if (existVillage != null)
                    {
                        village = existVillage;
                        if (existVillage.VillageName != PupilProfileViewModel.VillageName) // Truong hop doi chu thuong thanh chu hoa thi luu lai
                        {
                            existVillage.VillageName = PupilProfileViewModel.VillageName;
                            VillageBusiness.Update(existVillage);
                            VillageBusiness.Save();
                        }
                    }
                    else
                    {
                        var newVillage = new Village
                        {
                            VillageName = PupilProfileViewModel.VillageName,
                            ShortName = PupilProfileViewModel.VillageName,
                            CreateDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            CommuneID = PupilProfileViewModel.CommuneID,
                            IsActive = true
                        };
                        village = VillageBusiness.Insert(newVillage);
                        VillageBusiness.Save();
                    }
                    pupilprofile.VillageID = village.VillageID;
                }
            }
            else
            {
                pupilprofile.VillageID = null;
            }
            #endregion

            this.PupilProfileBusiness.UpdatePupilProfile(global.UserAccountID, pupilprofile, PupilProfileViewModel.EnrolmentType.Value, OldClassID);


            // Tạo giá trị ghi log
            string newObj = string.Empty;
            newObj = newObjectStr.Length > 0 ? newObjectStr.ToString().Substring(0, newObjectStr.Length - 2) : "";
            oldObject.Append("(Giá trị trước khi sửa): ").Append(oldObjectStr);
            newObject.Append("(Giá trị sau khi sửa): ").Append(newObj);
            userDescriptionsStr.Append("Giá trị trước khi thay đổi: ").Append(oldObjectStr);
            userDescriptionsStr.Append("Giá trị sau khi thay đổi: ").Append(newObj);
            ViewData[CommonKey.AuditActionKey.OldJsonObject] = oldObject;
            ViewData[CommonKey.AuditActionKey.NewJsonObject] = newObject;
            ViewData[CommonKey.AuditActionKey.ObjectID] = objectIDStr;
            ViewData[CommonKey.AuditActionKey.Description] = descriptionStr;
            ViewData[CommonKey.AuditActionKey.Parameter] = paramsStr;
            ViewData[CommonKey.AuditActionKey.userAction] = userActionsStr;
            ViewData[CommonKey.AuditActionKey.userFunction] = userFuntionsStr;
            ViewData[CommonKey.AuditActionKey.userDescription] = userDescriptionsStr;

            Session["ImagePath"] = null;
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            CheckActionPermissionMinView();
            CheckPermissionForAction(id, "PupilProfile", 3);
            PupilProfile pupilProfile = PupilProfileBusiness.Find(id);

            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            this.PupilProfileBusiness.DeletePupilProfile(_globalInfo.UserAccountID, id, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, userInfo);
            //this.PupilProfileBusiness.Save();
            // Tạo giá trị lưu log action_audit
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            objectIDStr.Append(pupilProfile.PupilProfileID);
            descriptionStr.Append("Xóa HS");
            paramsStr.Append(pupilProfile.PupilProfileID);
            userFuntionsStr.Append("Hồ sơ học sinh");
            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
            userDescriptionsStr.Append("Xóa hồ sơ HS ").Append(pupilProfile.FullName + ", mã ").Append(pupilProfile.PupilCode);
            ViewData[CommonKey.AuditActionKey.OldJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.NewJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.ObjectID] = objectIDStr;
            ViewData[CommonKey.AuditActionKey.Description] = descriptionStr;
            ViewData[CommonKey.AuditActionKey.Parameter] = paramsStr;
            ViewData[CommonKey.AuditActionKey.userAction] = userActionsStr;
            ViewData[CommonKey.AuditActionKey.userFunction] = userFuntionsStr;
            ViewData[CommonKey.AuditActionKey.userDescription] = userDescriptionsStr;
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        private Paginate<PupilProfileViewModel> SearchPaging(IDictionary<string, object> SearchInfo, bool allowPaging, bool sort, int page = 1, string orderBy = "", bool isFirst = false)
        {
            List<int> lstClassID = getClassFromEducationLevel(0).Select(u => u.ClassProfileID).ToList();
            if (!_globalInfo.IsAdminSchoolRole)
            {
                SearchInfo["ListNullableClassID"] = lstClassID;
            }
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<PupilLeavingOff> lstPupilLeavingOff = new List<PupilLeavingOff>();
            lstPupilLeavingOff = PupilLeavingOffBusiness.All.Where(p => p.AcademicYearID == _globalInfo.AcademicYearID && p.SchoolID == _globalInfo.SchoolID).ToList();

            Paginate<PupilProfileViewModel> Paging = null;

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            string no = Res.Get("Common_Label_NoChoice");

            IQueryable<PupilProfileViewModel> lsPupilProfileViewModel = this.PupilProfileBusiness
                                                                        .SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo)
                                                                        .Select(o => new PupilProfileViewModel
                                                                        {
                                                                            PupilProfileID = o.PupilProfileID,
                                                                            CurrentClassID = o.CurrentClassID,
                                                                            ClassName = o.CurrentClassName,
                                                                            CurrentSchoolID = o.CurrentSchoolID,
                                                                            CurrentAcademicYearID = o.CurrentAcademicYearID,
                                                                            AreaID = o.AreaID,
                                                                            AreaName = o.AreaName,
                                                                            ProvinceID = o.ProvinceID,
                                                                            ProvinceName = o.ProvinceName,
                                                                            DistrictID = o.DistrictID,
                                                                            DistrictName = o.DistrictName,
                                                                            CommuneID = o.CommuneID,
                                                                            CommuneName = o.CommuneName,
                                                                            IdentifyNumber = o.IdentifyNumber,
                                                                            EthnicID = o.EthnicID,
                                                                            OtherEthnicID = o.OtherEthnicID,
                                                                            EthnicName = o.EthnicName,
                                                                            ReligionID = o.ReligionID,
                                                                            ReligionName = o.ReligionName,
                                                                            PolicyTargetID = o.PolicyTargetID,
                                                                            PolicyTargetName = o.PolicyTargetName,
                                                                            FamilyTypeID = o.FamilyTypeID,
                                                                            FamilyTypeName = o.FamilyName,
                                                                            PriorityTypeID = o.PriorityTypeID,
                                                                            PriorityTypeName = o.PriorityTypeName,
                                                                            PreviousGraduationLevelID = o.PreviousGraduationLevelID,
                                                                            PupilCode = o.PupilCode,
                                                                            EducationLevel = o.EducationLevelID,
                                                                            FullName = o.FullName,
                                                                            Name = o.Name,
                                                                            Genre = o.Genre,
                                                                            BirthDate = o.BirthDate,
                                                                            BirthPlace = o.BirthPlace,
                                                                            HomeTown = o.HomeTown,
                                                                            HomePlace = o.HomePlace,
                                                                            Telephone = o.Telephone,
                                                                            Mobile = o.Mobile,
                                                                            Email = o.Email,
                                                                            TempResidentalAddress = o.TempResidentalAddress,
                                                                            PermanentResidentalAddress = o.PermanentResidentalAddress,
                                                                            IsSwimming = (o.IsSwimming.HasValue && o.IsSwimming.Value),
                                                                            IsResidentIn = o.IsResidentIn,
                                                                            IsDisabled = (o.IsDisabled.HasValue) && o.IsDisabled.Value,
                                                                            DisabledTypeID = o.DisabledTypeID,
                                                                            DisabledTypeName = o.DisabledTypeName,
                                                                            DisabledSeverity = o.DisabledSeverity,
                                                                            BloodType = o.BloodType,
                                                                            EnrolmentMark = o.EnrolmentMark,
                                                                            EnrolmentDate = o.EnrolmentDate,
                                                                            ForeignLanguageTraining = o.ForeignLanguageTraining,                                                                       
                                                                            IsYoungPioneerMember = (o.IsYoungPioneerMember.HasValue) && o.IsYoungPioneerMember.Value,
                                                                            YoungPioneerJoinedDate = o.YoungPioneerJoinedDate,
                                                                            YoungPioneerJoinedPlace = o.YoungPioneerJoinedPlace,
                                                                            IsYouthLeageMember = (o.IsYouthLeageMember.HasValue) && o.IsYouthLeageMember.Value,
                                                                            YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
                                                                            YouthLeagueJoinedPlace = o.YouthLeagueJoinedPlace,
                                                                            IsCommunistPartyMember = (o.IsCommunistPartyMember.HasValue) && o.IsCommunistPartyMember.Value,
                                                                            CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
                                                                            CommunistPartyJoinedPlace = o.CommunistPartyJoinedPlace,
                                                                            FatherFullName = o.FatherFullName,
                                                                            FatherBirthDate = o.FatherBirthDate,
                                                                            FatherJob = o.FatherJob,
                                                                            FatherMobile = o.FatherMobile,
                                                                            FatherEmail = o.FatherEmail,
                                                                            MotherFullName = o.MotherFullName,
                                                                            MotherBirthDate = o.MotherBirthDate,
                                                                            MotherJob = o.MotherJob,
                                                                            MotherMobile = o.MotherMobile,
                                                                            MotherEmail = o.MotherEmail,
                                                                            SponsorFullName = o.SponsorFullName,
                                                                            SponsorBirthDate = o.SponsorBirthDate,
                                                                            SponsorJob = o.SponsorJob,
                                                                            SponsorMobile = o.SponsorMobile,
                                                                            SponsorEmail = o.SponsorEmail,
                                                                            ProfileStatus = o.ProfileStatus,
                                                                            IsActive = o.IsActive,
                                                                            GenreName = (o.Genre == (int)SystemParamsInFile.GENRE_MALE) ? male : female,
                                                                            EnrolmentType = o.EnrolmentType,
                                                                            OrderInClass = o.OrderInClass,
                                                                            PolicyRegimeID = o.PolicyRegimeID,
                                                                            IsSupportForLearning = o.IsSupportForLearning,
                                                                            IsFatherSMS = (o.IsFatherSMS.HasValue && o.IsFatherSMS.Value),
                                                                            IsMotherSMS = (o.IsMotherSMS.HasValue && o.IsMotherSMS.Value),
                                                                            IsSponsorSMS = (o.IsSponsorSMS.HasValue && o.IsSponsorSMS.Value),
                                                                            PupilLearningType = o.PupilLearningType,
                                                                            ItCertificate = o.ItCertificate,
                                                                            LanguageCertificate = o.LanguageCertificate,
                                                                            ItCertificateID = o.ItCertificateID,
                                                                            LanguageCertificateID = o.LanguageCertificateID,
                                                                            SubCommitteeID = o.SubCommitteeID,
                                                                            SubCommitteeName = o.SubCommitteeName,
                                                                            VillageID = o.VillageID,
                                                                            VillageName = o.VillageName,
                                                                            StorageNumber = o.StorageNumber,
                                                                            ClassOrderNumber = o.ClassOrderNumber,
                                                                            AppliedLevelID = _globalInfo.AppliedLevel,
                                                                            SupportingPolicy = o.SupportingPolicy,
                                                                            SupportingPolicyName = o.SupportingPolicy == SystemParamsInFile.POLICY_122003_GOVERNMENT_DECISION ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPENOITRU :
                                                                                    (o.SupportingPolicy == SystemParamsInFile.POLICY_2123_GOVERNMENT_DECISION ? GlobalConstants.POLICY_2123_GOVERNMENT_DECISION :
                                                                                        (o.SupportingPolicy == SystemParamsInFile.POLICY_1672_GOVERNMENT_DECISION ? GlobalConstants.POLICY_1672_GOVERNMENT_DECISION :
                                                                                            (o.SupportingPolicy == SystemParamsInFile.POLICY_01_UBND_LCU_GOVERNMENT_DECISION ? GlobalConstants.POLICY_01_UBND_LCU_GOVERNMENT_DECISION : ""))),
                                                                            IsReceiveRiceSubsidy = o.IsReceiveRiceSubsidy.HasValue ? o.IsReceiveRiceSubsidy.Value : false,
                                                                            IsResettlementTarget = o.IsResettlementTarget.HasValue ? o.IsResettlementTarget.Value : false,
                                                                            ClassTypeName = o.ClassType == SystemParamsInFile.CLASSTYPE_NOITRU ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPENOITRU :
                                                                                     (o.ClassType == SystemParamsInFile.CLASSTYPE_BANTRU ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPEBANTRU :
                                                                                     (o.ClassType == SystemParamsInFile.CLASSTYPE_BANTRUDANNUOI ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPEBANTRUDANNUOI :
                                                                                     (o.ClassType == SystemParamsInFile.CLASSTYPE_NOITRUDANNUOI ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPENOITRUDANNUOI : ""))),
                                                                            MinorityFather = (o.MinorityFather.HasValue && o.MinorityFather.Value),
                                                                            MinorityMother = (o.MinorityMother.HasValue && o.MinorityMother.Value),
                                                                            UsedMoetProgram = (o.UsedMoetProgram.HasValue && o.UsedMoetProgram.Value),
                                                                            IsUsedMoetProgram = o.IsUsedMoetProgram,
                                                                            IsMinorityFather = o.IsMinorityFather,
                                                                            IsMinorityMother = o.IsMinorityMother,
                                                                        });

            int ClassID = SMAS.Business.Common.Utils.GetInt(SearchInfo, "CurrentClassID");

            var lstPupilInfo = lsPupilProfileViewModel.ToList();

            if (string.IsNullOrEmpty(orderBy))
            {
                if (sort)
                    lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                else
                {
                    if (ClassID > 0)
                        lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(o => o.FullName);
                    else
                        lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.EducationLevel).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                }
            }
            else
            {
                int indexMinus = orderBy.LastIndexOf('-');
                string orderByName = indexMinus < 0 ? "" : orderBy.Substring(0, indexMinus);
                string orderByPriority = indexMinus < 0 ? "" : orderBy.Substring(indexMinus + 1);

                switch (orderByName)
                {
                    case "FullName":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    case "PupilCode":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    case "BirthDate":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    case "ClassName":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    case "StatusName":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    default:
                        lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                }
            }

            Paging = new Paginate<PupilProfileViewModel>(lsPupilProfileViewModel);
            Paging.page = page;

            if (!allowPaging)
                Paging.size = lsPupilProfileViewModel.Count();

            Paging.paginate();

            //Thỏa theo nguyện vọng muốn order theo tên tiếng việt trong khi chưa tìm ra cách order trực tiếp trong db.
            if (!allowPaging && ClassID > 0)
            {
                // Sap xep lai cho dung thu tu chon tren grid
                if (!string.IsNullOrEmpty(orderBy))
                {
                    Paging.List = Paging.List.OrderBy(u => u.Name).ThenBy(u => u.FullName).ToList();
                    int indexMinus = orderBy.LastIndexOf('-');
                    string orderByName = indexMinus < 0 ? "" : orderBy.Substring(0, indexMinus);
                    string orderByPriority = indexMinus < 0 ? "" : orderBy.Substring(indexMinus + 1);

                    switch (orderByName)
                    {
                        case "FullName":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        case "PupilCode":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        case "BirthDate":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        case "ClassName":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        case "StatusName":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        default:
                            Paging.List = Paging.List.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                    }
                }
                else
                {
                    Paging.List = Paging.List.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.FullName).ToList();
                }
            }
            #region set data

            string divtool = string.Empty;
            List<int> listHidden = new List<int>();

            Dictionary<int, bool> dicHeadTeacherPermission = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, lstClassID);
            //Dictionary<int, bool> dicSubjectTeacherPermission = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, lstClassID, 0);
            //Dictionary<int, bool> dicSupervisiorAssignmentPermission = UtilsBusiness.HasOverseeingTeacherPermission(global.UserAccountID, lstClassID);
            //bool isBGH = UtilsBusiness.IsBGH(global.UserAccountID);

            foreach (var item in Paging.List)
            {
                item.EnableCheck = _globalInfo.IsCurrentYear
                                        && item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                        && (dicHeadTeacherPermission[item.CurrentClassID] //|| dicSubjectTeacherPermission[item.CurrentClassID] || dicSupervisiorAssignmentPermission[item.CurrentClassID] || isBGH
                                        );

                item.iMotherBirthDate = item.MotherBirthDate.HasValue ? item.MotherBirthDate.Value.Year : new Nullable<int>();
                item.iFatherBirthDate = item.FatherBirthDate.HasValue ? item.FatherBirthDate.Value.Year : new Nullable<int>();
                item.iSponsorBirthDate = item.SponsorBirthDate.HasValue ? item.SponsorBirthDate.Value.Year : new Nullable<int>();

                #region switch case data
                switch (item.ProfileStatus)
                {
                    case SystemParamsInFile.PUPIL_STATUS_STUDYING:
                        item.StatusName = Res.Get("Common_Label_PupilStatusStuding");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_GRADUATED:
                        item.StatusName = Res.Get("Common_Label_PupilStatusGraduated");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMoved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF:
                        item.StatusName = Res.Get("Common_Label_PupilStatusLeaved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMovedToOtherClass");
                        break;
                }

                //PreviousGraduationLevelID
                if (item.PreviousGraduationLevelID.HasValue)
                {
                    switch (item.PreviousGraduationLevelID.Value)
                    {
                        case SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL1:
                            item.PreviousGraduationLevelName = Res.Get("PupilProfile_Label_PreviousGraduationLevel1");
                            break;
                        case SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL2:
                            item.PreviousGraduationLevelName = Res.Get("PupilProfile_Label_PreviousGraduationLevel2");
                            break;
                        case SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL3:
                            item.PreviousGraduationLevelName = Res.Get("PupilProfile_Label_PreviousGraduationLevel3");
                            break;
                        default:
                            item.PreviousGraduationLevelName = "";
                            break;
                    }
                }
                else
                {
                    item.PreviousGraduationLevelName = no;
                }

                if (item.BloodType.HasValue)
                {
                    //BloodType
                    switch (item.BloodType.Value)
                    {
                        case SystemParamsInFile.BLOOD_TYPE_A:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeA");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_B:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeB");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_AB:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeAB");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_O:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeO");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_OTHER:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeOther");
                            break;

                        default:
                            item.BloodTypeName = no;
                            break;
                    }
                }
                //else
                //{
                //    item.BloodTypeName = no;
                //}

                if (item.EnrolmentType.HasValue)
                {
                    switch (item.EnrolmentType.Value)
                    {
                        case SystemParamsInFile.ENROLMENT_TYPE_PASSED_EXAMINATION:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypePassExamination");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeMoveFromOtherSchool");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_SELECTED:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeSelected");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_OTHER:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeOther");
                            break;

                        default:
                            item.EnrolmentTypeName = no;
                            break;
                    }
                }
                //else
                //{
                //    item.EnrolmentTypeName = no;
                //}

                if (item.ForeignLanguageTraining.HasValue)
                {
                    switch (item.ForeignLanguageTraining.Value)
                    {
                        case SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_3_YEARS:
                            item.ForeignLanguageTrainingName = Res.Get("Common_Label_ForeignLanguageTraining3");
                            break;

                        case SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_7_YEARS:
                            item.ForeignLanguageTrainingName = Res.Get("Common_Label_ForeignLanguageTraining7");
                            break;

                        case SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_10_YEARS:
                            item.ForeignLanguageTrainingName = Res.Get("Common_Label_ForeignLanguageTraining10");
                            break;

                        case SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED:
                            item.ForeignLanguageTrainingName = Res.Get("");
                            break;

                        default:
                            item.ForeignLanguageTrainingName = no;
                            break;
                    }
                }
                //else
                //{
                //    item.ForeignLanguageTrainingName = no;
                //}

                if (item.PupilLearningType.HasValue && item.PupilLearningType.Value > 0)
                {
                    ComboObject cbo = CommonList.PupilLearningType().SingleOrDefault(u => u.key.Equals(item.PupilLearningType.ToString()));
                    if (cbo != null) item.PupilLearningTypeName = cbo.value;
                }
                #endregion

                #region divtool namedv
                //0.Xem hồ sơ
                //1.Sửa hồ sơ
                //2.Xóa hồ sơ
                //3.Chuyển lớp
                //4.Chuyển trường
                //5.Cập nhật cá biệt
                //6.Thôi học
                //7.Cập nhật miễn giảm
                //8.Cập nhật thôi học
                //9.Cập nhật khen thưởng
                //10.Cập nhật kỷ luật
                listHidden = new List<int>();

                if (_globalInfo.IsCurrentYear)
                {
                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        listHidden.Add(8);
                        //Neu khong phai giao vien chu nhiem 
                        if (!dicHeadTeacherPermission[item.CurrentClassID]) //BGH, GV
                        {
                            listHidden.Add(1);
                            listHidden.Add(2);
                            listHidden.Add(3);
                            listHidden.Add(4);
                            listHidden.Add(5);
                            listHidden.Add(6);
                            listHidden.Add(7);
                            listHidden.Add(8);
                            listHidden.Add(9);
                            listHidden.Add(10);
                        }
                    }

                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        listHidden.Add(1);
                        listHidden.Add(2);
                        listHidden.Add(3);
                        listHidden.Add(4);
                        listHidden.Add(5);
                        listHidden.Add(6);
                        listHidden.Add(7);
                        listHidden.Add(8);
                        listHidden.Add(9);
                        listHidden.Add(10);
                    }

                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                    {
                        listHidden.Add(1);
                        listHidden.Add(2);
                        listHidden.Add(3);
                        listHidden.Add(4);
                        listHidden.Add(5);
                        listHidden.Add(6);
                        listHidden.Add(7);
                        listHidden.Add(9);
                        listHidden.Add(10);

                        //Neu khong phai giao vien chu nhiem 
                        if (!dicHeadTeacherPermission[item.CurrentClassID]) //BGH, GV
                        {
                            listHidden.Add(8);
                        }
                    }

                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    {
                        listHidden.Add(1);
                        listHidden.Add(2);
                        listHidden.Add(3);
                        listHidden.Add(5);
                        listHidden.Add(6);
                        listHidden.Add(7);
                        listHidden.Add(8);
                        listHidden.Add(9);
                        listHidden.Add(10);

                        //Neu khong phai giao vien chu nhiem 
                        if (!dicHeadTeacherPermission[item.CurrentClassID]) //BGH, GV
                        {
                            listHidden.Add(4);
                        }
                    }

                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                    {
                        listHidden.Add(1);
                        listHidden.Add(2);
                        listHidden.Add(4);
                        listHidden.Add(5);
                        listHidden.Add(6);
                        listHidden.Add(7);
                        listHidden.Add(8);
                        listHidden.Add(9);
                        listHidden.Add(10);
                        //Neu khong phai giao vien chu nhiem 
                        if (!dicHeadTeacherPermission[item.CurrentClassID])
                        {
                            listHidden.Add(3);
                        }
                    }
                }
                else
                {
                    PupilLeavingOff objPupilLeavingOff = lstPupilLeavingOff.Where(p => p.PupilID == item.PupilProfileID).FirstOrDefault();
                    listHidden.Add(1);
                    listHidden.Add(2);
                    listHidden.Add(3);
                    listHidden.Add(4);
                    listHidden.Add(5);
                    listHidden.Add(6);
                    listHidden.Add(7);
                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                       || item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                       || item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                       || (objPupilLeavingOff != null && objPupilLeavingOff.Semester != 6)
                       || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole) // Neu la nguoi dung phong so dang xem thi cung an di
                    {

                        listHidden.Add(8);
                    }
                    listHidden.Add(9);
                    listHidden.Add(10);
                }
                bool isAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
                bool isLockPupil = objAca.IsLockPupilProfile.HasValue && objAca.IsLockPupilProfile.Value;

                if (!isAdmin && isLockPupil)
                {
                    listHidden.Add(1);
                    listHidden.Add(2);
                }

                item.HiddenLink = string.Join(",", listHidden);
                #endregion
            }
            #endregion
            if (isFirst)
            {

                Session["lstPupilIDBySearch"] = lsPupilProfileViewModel.Where(x => x.ProfileStatus == 1 || x.ProfileStatus == 2).Select(x => x.PupilProfileID).ToList();
                //var a = PupilProfileBusiness.All.Take(2000).Select(x=>x.PupilProfileID).ToList();
                //var b = PupilProfileBusiness.All.Take(4000).Select(x => x.PupilProfileID).ToList();
                //var c = PupilProfileBusiness.All.Take(8000).Select(x => x.PupilProfileID).ToList();
                //var d = PupilProfileBusiness.All.Select(x => x.PupilProfileID).ToList();
            }
            return Paging;
        }
        #endregion

        #region Extent Method
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<PupilProfileViewModel> _Search(IDictionary<string, object> SearchInfo, int x, bool sort)
        {
            GlobalInfo global = new GlobalInfo();
            List<PupilProfileBO> lstPupilBO = this.PupilProfileBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).ToList();
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            string no = Res.Get("Common_Label_NoChoice");
            List<PupilProfileViewModel> lsPupilProfileViewModel = lstPupilBO.Select(o => new PupilProfileViewModel
            {
                PupilProfileID = o.PupilProfileID,
                CurrentClassID = o.CurrentClassID,
                ClassName = o.CurrentClassName,
                CurrentSchoolID = o.CurrentSchoolID,
                CurrentAcademicYearID = o.CurrentAcademicYearID,
                AreaID = o.AreaID,
                AreaName = o.AreaName,
                ProvinceID = o.ProvinceID,
                ProvinceName = o.ProvinceName,
                DistrictID = o.DistrictID,
                DistrictName = o.DistrictName,
                CommuneID = o.CommuneID,
                CommuneName = o.CommuneName,
                EthnicID = o.EthnicID,
                EthnicName = o.EthnicName,
                ReligionID = o.ReligionID,
                ReligionName = o.ReligionName,
                PolicyTargetID = o.PolicyTargetID,
                PolicyTargetName = o.PolicyTargetName,
                FamilyTypeID = o.FamilyTypeID,
                FamilyTypeName = o.FamilyName,
                PriorityTypeID = o.PriorityTypeID,
                PriorityTypeName = o.PriorityTypeName,
                PreviousGraduationLevelID = o.PreviousGraduationLevelID,
                PupilCode = o.PupilCode,
                EducationLevel = o.EducationLevelID,
                FullName = o.FullName,
                Name = o.Name,
                Genre = o.Genre,
                BirthDate = o.BirthDate,
                BirthPlace = o.BirthPlace,
                HomeTown = o.HomeTown,
                Telephone = o.Telephone,
                Mobile = o.Mobile,
                Email = o.Email,
                TempResidentalAddress = o.TempResidentalAddress,
                PermanentResidentalAddress = o.PermanentResidentalAddress,
                IsResidentIn = o.IsResidentIn,
                IsDisabled = (o.IsDisabled.HasValue) ? o.IsDisabled.Value : false,
                DisabledTypeID = o.DisabledTypeID,
                DisabledTypeName = o.DisabledTypeName,
                DisabledSeverity = o.DisabledSeverity,
                BloodType = o.BloodType,
                EnrolmentMark = o.EnrolmentMark,
                EnrolmentDate = o.EnrolmentDate,
                ForeignLanguageTraining = o.ForeignLanguageTraining,
                IsYoungPioneerMember = (o.IsYoungPioneerMember.HasValue) ? o.IsYoungPioneerMember.Value : false,
                YoungPioneerJoinedDate = o.YoungPioneerJoinedDate,
                YoungPioneerJoinedPlace = o.YoungPioneerJoinedPlace,
                IsYouthLeageMember = (o.IsYouthLeageMember.HasValue) ? o.IsYouthLeageMember.Value : false,
                YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
                YouthLeagueJoinedPlace = o.YouthLeagueJoinedPlace,
                IsCommunistPartyMember = (o.IsCommunistPartyMember.HasValue) ? o.IsCommunistPartyMember.Value : false,
                CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
                CommunistPartyJoinedPlace = o.CommunistPartyJoinedPlace,
                FatherFullName = o.FatherFullName,
                FatherBirthDate = o.FatherBirthDate,
                FatherJob = o.FatherJob,
                FatherMobile = o.FatherMobile,
                MotherFullName = o.MotherFullName,
                MotherBirthDate = o.MotherBirthDate,
                MotherJob = o.MotherJob,
                MotherMobile = o.MotherMobile,
                SponsorFullName = o.SponsorFullName,
                SponsorBirthDate = o.SponsorBirthDate,
                SponsorJob = o.SponsorJob,
                SponsorMobile = o.SponsorMobile,
                ProfileStatus = o.ProfileStatus,
                IsActive = o.IsActive,
                GenreName = (o.Genre == (int)SystemParamsInFile.GENRE_MALE) ? male : female,
                EnrolmentType = o.EnrolmentType,
                OrderInClass = o.OrderInClass,
                PolicyRegimeID = o.PolicyRegimeID,
                IsSupportForLearning = o.IsSupportForLearning,
                IsSwimming = o.IsSwimming.HasValue && o.IsSwimming.Value,
                IsFatherSMS = o.IsFatherSMS.HasValue ? o.IsFatherSMS.Value : false,
                IsMotherSMS = o.IsMotherSMS.HasValue ? o.IsMotherSMS.Value : false,
                IsSponsorSMS = o.IsSponsorSMS.HasValue ? o.IsSponsorSMS.Value : false,
                SupportingPolicy = o.SupportingPolicy,
                SupportingPolicyName = o.SupportingPolicy == SystemParamsInFile.POLICY_122003_GOVERNMENT_DECISION ? GlobalConstants.POLICY_122003_GOVERNMENT_DECISION :
                                                                                    (o.SupportingPolicy == SystemParamsInFile.POLICY_2123_GOVERNMENT_DECISION ? GlobalConstants.POLICY_2123_GOVERNMENT_DECISION :
                                                                                        (o.SupportingPolicy == SystemParamsInFile.POLICY_1672_GOVERNMENT_DECISION ? GlobalConstants.POLICY_1672_GOVERNMENT_DECISION :
                                                                                            (o.SupportingPolicy == SystemParamsInFile.POLICY_01_UBND_LCU_GOVERNMENT_DECISION ? GlobalConstants.POLICY_01_UBND_LCU_GOVERNMENT_DECISION : ""))),

                IsReceiveRiceSubsidy = o.IsReceiveRiceSubsidy.HasValue ? o.IsReceiveRiceSubsidy.Value : false,
                IsResettlementTarget = o.IsResettlementTarget.HasValue ? o.IsResettlementTarget.Value : false,
                MinorityFather = o.MinorityFather.HasValue ? o.MinorityFather.Value : false,
                MinorityMother = o.MinorityMother.HasValue ? o.MinorityMother.Value : false,
                UsedMoetProgram = o.UsedMoetProgram.HasValue ? o.MinorityMother.Value : false,
            }).ToList();

            if (sort)
            {
                lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            }
            else
            {
                lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.EducationLevel).ThenBy(o => o.CurrentClassID).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ToList();
            }

            #region set data

            string divtool = string.Empty;
            foreach (var item in lsPupilProfileViewModel)
            {
                #region switch case data
                switch (item.ProfileStatus)
                {
                    case SystemParamsInFile.PUPIL_STATUS_STUDYING:
                        item.StatusName = Res.Get("Common_Label_PupilStatusStuding");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_GRADUATED:
                        item.StatusName = Res.Get("Common_Label_PupilStatusGraduated");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMoved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF:
                        item.StatusName = Res.Get("Common_Label_PupilStatusLeaved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMovedToOtherClass");
                        break;
                }

                //PreviousGraduationLevelID
                if (item.PreviousGraduationLevelID.HasValue)
                {
                    switch (item.PreviousGraduationLevelID.Value)
                    {
                        case SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL1:
                            item.PreviousGraduationLevelName = Res.Get("PupilProfile_Label_PreviousGraduationLevel1");
                            break;
                        case SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL2:
                            item.PreviousGraduationLevelName = Res.Get("PupilProfile_Label_PreviousGraduationLevel2");
                            break;
                        case SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL3:
                            item.PreviousGraduationLevelName = Res.Get("PupilProfile_Label_PreviousGraduationLevel3");
                            break;
                        default:
                            item.PreviousGraduationLevelName = "";
                            break;
                    }
                }
                else
                {
                    item.PreviousGraduationLevelName = no;
                }

                if (item.BloodType.HasValue)
                {
                    //BloodType
                    switch (item.BloodType.Value)
                    {
                        case SystemParamsInFile.BLOOD_TYPE_A:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeA");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_B:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeB");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_AB:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeAB");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_O:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeO");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_OTHER:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeOther");
                            break;

                        default:
                            item.BloodTypeName = no;
                            break;
                    }
                }
                else
                {
                    item.BloodTypeName = no;
                }

                if (item.EnrolmentType.HasValue)
                {
                    switch (item.EnrolmentType.Value)
                    {
                        case SystemParamsInFile.ENROLMENT_TYPE_PASSED_EXAMINATION:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypePassExamination");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeMoveFromOtherSchool");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_SELECTED:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeSelected");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_OTHER:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeOther");
                            break;

                        default:
                            item.EnrolmentTypeName = no;
                            break;
                    }
                }
                else
                {
                    item.EnrolmentTypeName = no;
                }

                if (item.ForeignLanguageTraining.HasValue)
                {
                    switch (item.ForeignLanguageTraining.Value)
                    {
                        case SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_3_YEARS:
                            item.ForeignLanguageTrainingName = Res.Get("Common_Label_ForeignLanguageTraining3");
                            break;

                        case SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_7_YEARS:
                            item.ForeignLanguageTrainingName = Res.Get("Common_Label_ForeignLanguageTraining7");
                            break;

                        case SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED:
                            item.ForeignLanguageTrainingName = Res.Get("Common_ForeignLanguageTrainingUnIdentified");
                            break;

                        default:
                            item.ForeignLanguageTrainingName = no;
                            break;
                    }
                }
                else
                {
                    item.ForeignLanguageTrainingName = no;
                }
                #endregion

                #region divtool namedv
                divtool = string.Empty;
                divtool += "<div class=\"NameSection\" data-id=\"" + item.PupilProfileID + "\" data-show=\"0\" style=\"display:none;margin-right:-92px;margin-top:-9px;\" id=\"" + ("tool_" + item.PupilProfileID) + "\">";
                divtool += "<ul id=\"" + ("ul_" + item.PupilProfileID) + "\" class=\"ResetList MenuSectionList\">";
                divtool += "<li><a href=\"javascript:DetailPupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item1\">Xem hồ sơ</span></a></li>";
                if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                {
                    divtool += "<li><a href=\"javascript:EditPupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item2\">Sửa hồ sơ</span></a></li>";
                    divtool += "<li><a href=\"javascript:DeletePupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item3\">Xóa hồ sơ</span></a></li>";
                    divtool += "<li><a href=\"javascript:ClassMovementPupil(" + item.PupilProfileID + "," + item.CurrentClassID + ");\"><span class=\"Sprite1 Item4\">Chuyển lớp</span></a></li>";
                    divtool += "<li><a href=\"javascript:SchoolMovementPupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item5\">Chuyển trường</span></a></li>";
                    divtool += "<li><a href=\"javascript:ParticularPupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item6\">Cập nhật cá biệt</span></a></li>";
                    divtool += "<li><a href=\"javascript:LeavingOffPupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item7\">Thôi học</span></a></li>";
                    divtool += "<li><a href=\"javascript:ExemptedPupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item8\">Cập nhật miễn giảm</span></a></li>";
                }
                else if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                {
                    divtool += "<li><a href=\"javascript:LeavingOffPupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item7\">Cập nhật thôi học</span></a></li>";
                }
                else if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                {
                    divtool += "<li><a href=\"javascript:SchoolMovementPupil(" + item.PupilProfileID + ");\"><span class=\"Sprite1 Item5\">Cập nhật chuyển trường</span></a></li>";
                }
                else if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                {
                    divtool += "<li><a href=\"javascript:ClassMovementPupil(" + item.PupilProfileID + "," + item.CurrentClassID + ");\"><span class=\"Sprite1 Item4\">Cập nhật chuyển lớp</span></a></li>";
                }
                divtool += " </ul></div>";
                item.HtmlDivTool = divtool;
                #endregion
            }
            #endregion

            return lsPupilProfileViewModel;
        }

        /// <summary>
        /// Lay danh sach lop theo quyen
        /// </summary>
        /// <returns></returns>
        private List<ClassProfile> GetListClass()
        {
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.getClassAdminOrHeadTeacher(_globalInfo.AcademicYearID.GetValueOrDefault()
                , _globalInfo.SchoolID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID).ToList();

            return lstClassProfile;
        }
        #endregion

        #region Menu context
        [HttpGet]
        public PartialViewResult ClassMovement(int id, int classid)
        {
            CheckActionPermissionMinView();
            CheckPermissionForAction(id, "PupilProfile", 3);
            CheckPermissionForAction(classid, "ClassProfile");
            GlobalInfo globalInfo = new GlobalInfo();
            ClassMovementModel model = new ClassMovementModel();

            PupilProfile pupilProfile = PupilProfileBusiness.Find(id);
            ClassProfile classProfile = ClassProfileBusiness.Find(classid);
            bool check = (pupilProfile.CurrentClassID == classid) ? true : false;
            AcademicYear aca = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);


            model.PupilID = id;
            model.FromClassID = classid;
            model.ClassName = classProfile.DisplayName;
            model.EducationLevelID = classProfile.EducationLevelID;
            model.FullName = pupilProfile.FullName;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", globalInfo.AcademicYearID.Value);
            dic.Add("EducationLevelID", model.EducationLevelID);
            List<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).Where(o => o.ClassProfileID != model.FromClassID).OrderBy(o => o.DisplayName).ToList();
            ViewData["LISTCLASSTO"] = new SelectList(lsClass, "ClassProfileID", "DisplayName");
            ViewData["LISTSEMESTER"] = new SelectList(CommonList.Semester(), "key", "value");
            ViewData["SEMESTER_ID"] = _globalInfo.Semester.HasValue ? _globalInfo.Semester.Value : -1;
            IDictionary<string, object> dicPupilOfClass = new Dictionary<string, object>();
            dicPupilOfClass["PupilID"] = id;
            dicPupilOfClass["Status"] = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS;
            dicPupilOfClass.Add("AcademicYearID", globalInfo.AcademicYearID.Value);
            dicPupilOfClass.Add("ClassID", classid);
            PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, dicPupilOfClass).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (poc != null && check == false)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = poc.ClassProfile.EducationLevelID;
                SearchInfo["FromClassID"] = poc.ClassID;
                SearchInfo["PupilID"] = poc.PupilID;

                ClassMovement classMovement = ClassMovementBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo).OrderByDescending(u => u.MovedDate).FirstOrDefault();
                if (classMovement != null) //neu hoc sinh nay da chuyen lop
                {
                    model.ClassMovementID = classMovement.ClassMovementID;
                    model.MovedDate1 = classMovement.MovedDate;
                    if (model.MovedDate1 > aca.FirstSemesterStartDate)
                    {
                        model.Semester = 1;
                    }
                    if (model.MovedDate1 > aca.SecondSemesterStartDate)
                    {
                        model.Semester = 2;
                    }
                    model.Reason = classMovement.Reason;
                    model.ToClassID = classMovement.ToClassID;
                }
            }

            if (model.ClassMovementID <= 0)
                model.MovedDate1 = DateTime.Now;
            if (model.ClassMovementID <= 0 || check == true)
            {
                return PartialView("_ClassMovement", model);
            }
            else
            {
                return PartialView("_ClassMovementEdit", model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult ClassMovement(ClassMovementModel model)
        {
            CheckActionPermissionMinView();
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            List<ClassMovement> lstClassMovement = new List<ClassMovement>();
            Utils.Utils.TrimObject(model);

            ClassMovement classMovement = null;
            IDictionary<string, object> dicPupilOfClass = new Dictionary<string, object>();
            dicPupilOfClass["PupilID"] = model.PupilID;
            dicPupilOfClass["Status"] = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS;
            dicPupilOfClass.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dicPupilOfClass.Add("ClassID", model.FromClassID);
            PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicPupilOfClass).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (poc != null)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = ClassProfileBusiness.AllNoTracking.First(o => o.ClassProfileID == poc.ClassID).EducationLevelID;
                SearchInfo["FromClassID"] = poc.ClassID;
                SearchInfo["PupilID"] = poc.PupilID;
                classMovement = ClassMovementBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).OrderByDescending(u => u.MovedDate).FirstOrDefault();
            }

            if (classMovement == null)
                classMovement = new ClassMovement();

            Utils.Utils.BindTo(model, classMovement);

            classMovement.AcademicYearID = _globalInfo.AcademicYearID.Value;
            classMovement.SchoolID = _globalInfo.SchoolID.Value;
            classMovement.MovedDate = model.MovedDate1;
            lstClassMovement.Add(classMovement);
            List<int> lstClassID = new List<int>();
            lstClassID.Add(classMovement.FromClassID);
            lstClassID.Add(classMovement.ToClassID);
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    if (classMovement.ClassMovementID > 0) //Update
                        this.ClassMovementBusiness.UpdateClassMovement(_globalInfo.UserAccountID, classMovement);
                    else //Create
                    {
                        this.ClassMovementBusiness.InsertClassMovement(_globalInfo.UserAccountID, lstClassMovement, model.isClassMovement);
                    }
                    //cap nhat lai ClassID trong bang SMSParentContract
                    SMSParentContractBusiness.UpdateClassIDToSMSParentContract(classMovement.ToClassID, classMovement.FromClassID, _globalInfo.Semester.Value
                        , _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, classMovement.PupilID);
                    SMSParentContractInClassDetailBusiness.Save();
                    SMSParentContractBusiness.Save();
                    tran.Complete();
                }
                //Tinh lai quy tin cho lop
                SMSParentContractBusiness.GetListSMSParentContractClass(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.Semester.Value, lstClassID);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return Json(new JsonMessage(Res.Get("ClassMovement_Label_AddNewMessage")));

        }

        [HttpGet]
        public PartialViewResult SchoolMovement(int id)
        {
            CheckPermissionForAction(id, "PupilProfile", 3);

            //GlobalInfo glo = new GlobalInfo();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["PupilID"] = id;
            SchoolMovement schoolMovement = SchoolMovementBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).OrderByDescending(u => u.MovedDate).FirstOrDefault();

            SchoolMovementModel model = new SchoolMovementModel();

            if (schoolMovement != null)
            {
                Utils.Utils.BindTo(schoolMovement, model);
                model.FullName = schoolMovement.PupilProfile.FullName;
            }
            else
            {
                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value
                                                                , new Dictionary<string, object> {
                                                                    {"PupilID", id },
                                                                    {"SchoolID", _globalInfo.SchoolID.Value},
                                                                    {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                                                                    {"Status", (new int[] {SystemParamsInFile.PUPIL_STATUS_STUDYING } ).ToList() }
                                                                }).FirstOrDefault();
                if (poc == null)
                    throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");

                model.PupilID = poc.PupilID;
                model.ClassID = poc.ClassID;
                model.EducationLevelID = poc.ClassProfile.EducationLevelID;
                model.FullName = poc.PupilProfile.FullName;
                model.MovedDate = DateTime.Now;
            }

            ViewData[PupilProfileConstants.LISTPROVINCE] = new SelectList(ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).Select(u => new { u.ProvinceID, u.ProvinceName }).ToList(), "ProvinceID", "ProvinceName", model.MovedToProvinceID);
            ViewData[PupilProfileConstants.LISTSEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
            ViewData[PupilProfileConstants.SEMESTER_CHECKED] = this.GetSemesterChecked(objAcademicYear);

            return PartialView("_SchoolMovement", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult SchoolMovement(SchoolMovementModel model)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            GlobalInfo glo = new GlobalInfo();
            SchoolMovement sm = new SchoolMovement();
            if (model.SchoolMovementID.HasValue && model.SchoolMovementID.Value > 0)
            {
                sm = SchoolMovementBusiness.Find(model.SchoolMovementID.Value);

                if (sm.PupilID != model.PupilID)
                    return Json(new JsonMessage(Res.Get("Common_Validate_NotCompatible"), JsonMessage.ERROR));

                if (sm.MovedToClassID.HasValue)
                    return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_PupilIsMoved"), JsonMessage.ERROR));
            }

            sm.AcademicYearID = glo.AcademicYearID.Value;
            sm.SchoolID = glo.SchoolID.Value;
            sm.Description = model.Description;
            sm.MovedDate = model.MovedDate;
            sm.Semester = model.Semester;
            sm.EducationLevelID = model.EducationLevelID;
            sm.ClassID = model.ClassID;
            sm.PupilID = model.PupilID;

            if (model.SchoolType == PupilProfileConstants.SCHOOL_MOVEMENT_IN_SYSTEM)
            {
                sm.MovedToProvinceID = model.MovedToProvinceID;
                sm.MovedToDistrictID = model.MovedToDistrictID;
                sm.MovedToSchoolID = model.MovedToSchoolID;
                sm.MovedToClassID = null;

                sm.MovedToClassName = null;
                sm.MovedToSchoolAddress = null;
                sm.MovedToSchoolName = null;
            }
            else
            {
                sm.MovedToProvinceID = null;
                sm.MovedToDistrictID = null;
                sm.MovedToSchoolID = null;
                sm.MovedToClassID = null;

                sm.MovedToClassName = model.MovedToClassName;
                sm.MovedToSchoolAddress = model.MovedToSchoolAddress;
                sm.MovedToSchoolName = model.MovedToSchoolName;
            }

            if (model.SchoolMovementID.HasValue && model.SchoolMovementID.Value > 0)
            {
                SchoolMovementBusiness.UpdateSchoolMovement(glo.UserAccountID, sm);
            }
            else
            {
                SchoolMovementBusiness.InsertSchoolMovement(glo.UserAccountID, sm);
            }

            SchoolMovementBusiness.Save();


            return Json(new JsonMessage(Res.Get("SchoolMovement_Label_UpdateSuccess")));
        }

        [HttpGet]

        public PartialViewResult LeavingOffPupil(int id)
        {
            CheckPermissionForAction(id, "PupilProfile", 3);
            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = glo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchInfo["PupilID"] = id;

            AcademicYear objAcademicYear = AcademicYearBusiness.Find(glo.AcademicYearID);
            DateTime DateTimeNow = DateTime.Now;

            PupilLeavingOff pupilLeavingOff = PupilLeavingOffBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo).OrderByDescending(u => u.LeavingDate).FirstOrDefault();

            LeavingOffModel model = new LeavingOffModel();

            if (pupilLeavingOff != null)
            {
                Utils.Utils.BindTo(pupilLeavingOff, model);
                model.FullName = pupilLeavingOff.PupilProfile.FullName;
            }
            else
            {
                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value
                                                                , new Dictionary<string, object> {
                                                                    {"PupilID", id },
                                                                    {"SchoolID", glo.SchoolID.Value},
                                                                    {"AcademicYearID", glo.AcademicYearID.Value},
                                                                    {"Status", (new int[] {SystemParamsInFile.PUPIL_STATUS_STUDYING } ).ToList() }
                                                                }).SingleOrDefault();
                if (poc == null)
                    throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");

                model.PupilID = poc.PupilID;
                model.ClassID = poc.ClassID;
                model.EducationLevelID = poc.ClassProfile.EducationLevelID;
                model.FullName = poc.PupilProfile.FullName;
                model.LeavingDate = DateTime.Now;
            }

            List<ComboObject> lstSemester = new List<ComboObject>();
            if (DateTimeNow.Date <= objAcademicYear.SecondSemesterEndDate.Value.Date)
            {
                lstSemester = CommonList.Semester();
            }

            lstSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_IN_SUMMER.ToString(), Res.Get("Common_Label_InSumerSemester")));

            ViewData[PupilProfileConstants.LISTLEAVINGREASON] = new SelectList(LeavingReasonBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.LeavingReasonID, u.Resolution }).ToList(), "LeavingReasonID", "Resolution", model.LeavingReasonID);
            ViewData[PupilProfileConstants.LISTSEMESTER] = new SelectList(lstSemester, "key", "value");
            ViewData[PupilProfileConstants.ACADEMIC_YEAR] = objAcademicYear;

            ViewData[PupilProfileConstants.SEMESTER_CHECKED] = pupilLeavingOff != null ? pupilLeavingOff.Semester : this.GetSemesterChecked(objAcademicYear);
            return PartialView("_LeavingOffPupil", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult LeavingOffPupil(LeavingOffModel model)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            PupilLeavingOff pOff = new PupilLeavingOff();
            if (model.PupilLeavingOffID.HasValue && model.PupilLeavingOffID.Value > 0)
            {
                pOff = PupilLeavingOffBusiness.Find(model.PupilLeavingOffID.Value);

                if (pOff.PupilID != model.PupilID)
                    return Json(new JsonMessage(Res.Get("Common_Validate_NotCompatible"), JsonMessage.ERROR));
            }

            PupilProfile objPF = PupilProfileBusiness.Find(model.PupilID);
            LeavingReason objLR = LeavingReasonBusiness.Find(model.LeavingReasonID);

            pOff.AcademicYearID = _globalInfo.AcademicYearID.Value;
            pOff.SchoolID = _globalInfo.SchoolID.Value;
            pOff.Description = model.Description;
            pOff.LeavingDate = model.LeavingDate;
            pOff.Semester = model.Semester;
            pOff.EducationLevelID = model.EducationLevelID;
            pOff.ClassID = model.ClassID;
            pOff.PupilID = model.PupilID;
            pOff.LeavingReasonID = model.LeavingReasonID;
            pOff.IsReserve = model.IsReserve;

            if (model.PupilLeavingOffID.HasValue && model.PupilLeavingOffID.Value > 0)
            {
                PupilLeavingOffBusiness.UpdatePupilLeavingOff(_globalInfo.UserAccountID, pOff);
            }
            else
            {
                PupilLeavingOffBusiness.InsertPupilLeavingOff(_globalInfo.UserAccountID, pOff);
            }
            //luu log
            StringBuilder UserDescription = new StringBuilder();
            UserDescription.Append("Cập nhật thôi học cho HS ").Append(objPF.FullName).Append(", mã ").Append(objPF.PupilCode).Append(", ");
            UserDescription.Append("Ngày ").Append(model.LeavingDate.ToString("dd-MM-yyyy")).Append("/Học kỳ ").Append(model.Semester).Append("/").Append("Lý do: ").Append(objLR.Resolution);
            ViewData[CommonKey.AuditActionKey.OldJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.NewJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.ObjectID] = model.PupilID;
            ViewData[CommonKey.AuditActionKey.Description] = "Cập nhật thôi học";
            ViewData[CommonKey.AuditActionKey.Parameter] = model.PupilID;
            ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE;
            ViewData[CommonKey.AuditActionKey.userFunction] = "Hồ sơ học sinh";
            ViewData[CommonKey.AuditActionKey.userDescription] = UserDescription;
            PupilLeavingOffBusiness.Save();

            return Json(new JsonMessage(Res.Get("PupilLeavingOff_Label_UpdateSuccess")));
        }

        [HttpGet]

        public PartialViewResult ExemptedPupil(int id)
        {
            CheckPermissionForAction(id, "PupilProfile", 3);
            if (id <= 0) throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");

            GlobalInfo glo = new GlobalInfo();
            PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value
                                                                , new Dictionary<string, object> {
                                                                    {"PupilID", id },
                                                                    {"SchoolID", glo.SchoolID.Value},
                                                                    {"AcademicYearID", glo.AcademicYearID.Value},
                                                                    {"Status", (new int[] {SystemParamsInFile.PUPIL_STATUS_STUDYING } ).ToList() }
                                                                }).SingleOrDefault();
            if (poc == null)
                throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");

            PupilExemptedModel model = new PupilExemptedModel();
            model.PupilID = poc.PupilID;
            model.ClassID = poc.ClassID;
            model.FullName = poc.PupilProfile.FullName;
            model.ClassName = poc.ClassProfile.DisplayName;
            model.EducationLevelID = poc.ClassProfile.EducationLevelID;

            //[tanla4] - them ClassID vao dic khi search ExemptedSubject
            List<ExemptedSubject> lstExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(glo.SchoolID.Value,
                                                                            new Dictionary<string, object> {
                                                                            { "AcademicYearID", glo.AcademicYearID.Value },
                                                                            { "ClassID", poc.ClassID},
                                                                            { "PupilID", id } }).ToList();
            ViewData[PupilProfileConstants.LIST_EXEMPTED_SUBJECT] = lstExemptedSubject;

            //Kiểm tra học sinh nằm trong danh sách đăng ký môn chuyên/môn tự chọn
            //đối với môn miễn giảm thì xóa học sinh đó khỏi danh sách đăng ký



            if (lstExemptedSubject.Count > 0)
                model.ExemptedObjectID = lstExemptedSubject.FirstOrDefault().ExemptedObjectID;

            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["AcademicYearID"] = glo.AcademicYearID.Value;
            dicClassSubject["ClassID"] = poc.ClassID;
            dicClassSubject["IsExemptible"] = true;
            dicClassSubject["AppliedLevel"] = glo.AppliedLevel.Value;

            var lstSubject = this.ClassSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicClassSubject).ToList();

            List<GridExemptedSubjectModel> lstExemptedSubjectModel = new List<GridExemptedSubjectModel>();
            foreach (var subject in lstSubject)
            {
                GridExemptedSubjectModel item = new GridExemptedSubjectModel();
                var exemptedSubject = lstExemptedSubject.Where(u => u.SubjectID == subject.SubjectID).FirstOrDefault();
                item.SubjectCatID = subject.SubjectID;
                item.DisplayName = subject.SubjectCat.DisplayName;
                item.HasPractice = subject.SubjectCat.HasPractice;
                if (exemptedSubject != null)
                {
                    item.ExemptedSubjectID = exemptedSubject.ExemptedSubjectID;
                    item.FirstSemesterExemptType = exemptedSubject.FirstSemesterExemptType;
                    item.SecondSemesterExemptType = exemptedSubject.SecondSemesterExemptType;
                }
                lstExemptedSubjectModel.Add(item);
            }

            ViewData[PupilProfileConstants.LIST_SUBJECT_GRID] = lstExemptedSubjectModel;

            var lstExemptedObject = this.ExemptedObjectTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ExemptedObjectTypeID, u.Resolution }).ToList();
            ViewData[PupilProfileConstants.LIST_EXEMPTED_OBJECT] = new SelectList(lstExemptedObject, "ExemptedObjectTypeID", "Resolution", model.ExemptedObjectID);
            ViewData[PupilProfileConstants.LIST_EXEMPTED_TYPE] = CommonList.ExemptType();
            ViewData[PupilProfileConstants.LIST_EXEMPTED_TYPE_PRACTICE] = CommonList.ExemptType().Where(u => u.key == SystemParamsInFile.EXEMPT_TYPE_PRACTICE.ToString()).ToList();
            ViewData[PupilProfileConstants.LIST_EXEMPTED_TYPE_WITHOUT_OTHER] = CommonList.ExemptType().Where(u => u.key != SystemParamsInFile.EXEMPT_TYPE_OTHER.ToString()).ToList();

            return PartialView("_ExemptedPupil", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult ExemptedPupil(PupilExemptedModel model, FormCollection form)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dicRegisterSubjectSpecialize = new Dictionary<string, object>();
            {
                dicRegisterSubjectSpecialize["SchoolID"] = glo.SchoolID;
                dicRegisterSubjectSpecialize["AcademicYearID"] = glo.AcademicYearID;
                dicRegisterSubjectSpecialize["ClassID"] = model.ClassID;
            };
            //List ds dky mon chuyen mon tu chon cua hoc sinh
            List<RegisterSubjectSpecializeBO> lstRegisterSubjectSpecializeBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegisterSubjectSpecialize).Where(p => p.PupilID == model.PupilID).ToList();
            RegisterSubjectSpecializeBO objRegisterBO = null;
            List<RegisterSubjectSpecializeBO> lstObjRegisterBO = new List<RegisterSubjectSpecializeBO>();
            //lst xoa trong mon chuyen mon tu chon neu co trong mien giam
            List<long> lstDelete = new List<long>();

            if (!UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, model.ClassID))
                return Json(new JsonMessage(Res.Get("Common_Label_HasHeadTeacherPermissionError")), JsonMessage.ERROR);
            if (!glo.IsCurrentYear)
                return Json(new JsonMessage(Res.Get("Common_IsCurrentYear_Err")), JsonMessage.ERROR);
            if (ModelState.IsValid && glo.AcademicYearID.HasValue)
            {
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject["AcademicYearID"] = glo.AcademicYearID.Value;
                dicClassSubject["ClassID"] = model.ClassID;
                dicClassSubject["IsExemptible"] = true;
                dicClassSubject["AppliedLevel"] = glo.AppliedLevel.Value;
                var lstSubject = this.ClassSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicClassSubject).ToList();

                List<ExemptedSubject> lstExemptedSubject = new List<ExemptedSubject>();
                int curYear = (int)this.AcademicYearBusiness.Find(glo.AcademicYearID.Value).Year;

                foreach (var subject in lstSubject)
                {
                    string first = form.Get("FirstSemesterExemptType_" + subject.SubjectID);
                    string second = form.Get("SecondSemesterExemptType_" + subject.SubjectID);

                    int? firstValue = first != null && first.Trim() != "" ? Convert.ToByte(first) : new Nullable<int>();
                    int? secondValue = second != null && second.Trim() != "" ? Convert.ToByte(second) : new Nullable<int>();

                    if (firstValue.HasValue || secondValue.HasValue)
                    {
                        //ds mon chuyen mon tu chon  ma ton tai trong mon mien giam thi dua vao list Delete
                        //[tanla4] - sua loi tim mon o danh sach mon chuyen mon tu chon
                        //tim theo ma mon
                        lstObjRegisterBO = lstRegisterSubjectSpecializeBO.Where(p => p.SubjectID == subject.SubjectID).ToList();
                        if (lstObjRegisterBO.Count > 0)
                        {
                            //[tanla4] - sua loi xoa mon chuyen
                            //xet loai cua mon hoc: mon chuyen hay tu chon
                            objRegisterBO = lstObjRegisterBO.First();
                            if (objRegisterBO.SubjectTypeID == 0)//la mon chuyen
                            {
                                lstDelete.Add(objRegisterBO.RegisterSubjectSpecializeID);//vi mon chuyen thi chi luu duy nhat mot dong, khong phan biet hoc ki
                            }
                            else//la mon tu chon
                            {
                                //xet cho tung truong hop ung voi hoc ki 1 va hoc ki 2
                                if (firstValue.HasValue)
                                {
                                    objRegisterBO = lstObjRegisterBO.Where(p => p.SemesterID == 1).FirstOrDefault();
                                    if (objRegisterBO != null)
                                    {
                                        lstDelete.Add(objRegisterBO.RegisterSubjectSpecializeID);
                                    }
                                }
                                if (secondValue.HasValue)
                                {
                                    objRegisterBO = lstObjRegisterBO.Where(p => p.SemesterID == 2).FirstOrDefault();
                                    if (objRegisterBO != null)
                                    {
                                        lstDelete.Add(objRegisterBO.RegisterSubjectSpecializeID);
                                    }
                                }
                            }
                        }
                        ExemptedSubject exemptedSubject = new ExemptedSubject();
                        exemptedSubject.AcademicYearID = glo.AcademicYearID.Value;
                        exemptedSubject.ClassID = model.ClassID;
                        exemptedSubject.Description = string.Empty;
                        exemptedSubject.EducationLevelID = model.EducationLevelID;
                        exemptedSubject.ExemptedObjectID = model.ExemptedObjectID;
                        exemptedSubject.FirstSemesterExemptType = firstValue;
                        exemptedSubject.PupilID = model.PupilID;
                        exemptedSubject.SchoolID = glo.SchoolID.Value;
                        exemptedSubject.SecondSemesterExemptType = secondValue;
                        exemptedSubject.SubjectID = subject.SubjectID;
                        exemptedSubject.Year = curYear;
                        lstExemptedSubject.Add(exemptedSubject);
                    }
                }
                ActionAuditDataBO objAC = new ActionAuditDataBO();
                this.ExemptedSubjectBusiness.UpdateExemptedSubject(glo.UserAccountID, model.PupilID, model.ClassID, model.ExemptedObjectID, lstExemptedSubject, ref objAC);
                this.ExemptedSubjectBusiness.Save();
                ViewData[CommonKey.AuditActionKey.OldJsonObject] = "";
                ViewData[CommonKey.AuditActionKey.NewJsonObject] = "";
                ViewData[CommonKey.AuditActionKey.ObjectID] = objAC.ObjID;
                ViewData[CommonKey.AuditActionKey.Description] = objAC.Description;
                ViewData[CommonKey.AuditActionKey.Parameter] = objAC.Parameter;
                ViewData[CommonKey.AuditActionKey.userAction] = objAC.UserAction;
                ViewData[CommonKey.AuditActionKey.userFunction] = objAC.UserFunction;
                ViewData[CommonKey.AuditActionKey.userDescription] = objAC.UserDescription;

                //Neu ds mon hoc mien giam ma hs co dang ky mon chuyen mon tu chon thi xoa trong mon chuyen mon tu chon
                if (lstDelete.Count > 0)
                {
                    RegisterSubjectSpecializeBusiness.DeleteRegisterByListID(lstDelete);
                }
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        private IEnumerable<ParticularPupilTreatmentModel> _SearchTreatment(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ParticularPupilTreatment> query = this.ParticularPupilTreatmentBusiness.SearchParticularPupilTreatment(SearchInfo);
            List<ParticularPupilTreatmentModel> lst = query.Select(o => new ParticularPupilTreatmentModel
            {
                ParticularPupilTreatmentID = o.ParticularPupilTreatmentID,
                ParticularPupilID = o.ParticularPupilID,
                TreatmentResolution = o.TreatmentResolution,
                Result = o.Result,
                AppliedDate = o.AppliedDate
            }).OrderBy(u => u.AppliedDate).ToList();
            int rowIndex = 1;
            lst.ForEach(u => u.RowIndex = rowIndex++);
            return lst;
        }

        [SkipCheckRole]
        [GridAction(EnableCustomBinding = true)]

        [ValidateAntiForgeryToken]
        public ActionResult _SelectAjaxEditing(int particularPupilID)
        {
            CheckPermissionForAction(particularPupilID, "ParticularPupil");
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ParticularPupilID"] = particularPupilID;
            IEnumerable<ParticularPupilTreatmentModel> listParticularPupilTreatment = this._SearchTreatment(SearchInfo);
            GridModel<ParticularPupilTreatmentModel> gm = new GridModel<ParticularPupilTreatmentModel>(listParticularPupilTreatment);
            return View(gm);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _InsertAjaxEditing(int particularPupilID, ParticularPupilTreatmentModel model) //ParticularPupilID
        {
            CheckPermissionForAction(particularPupilID, "ParticularPupil");
            GlobalInfo globalInfo = new GlobalInfo();
            Utils.Utils.TrimObject(model);

            ParticularPupil particularPupil = ParticularPupilBusiness.Find(particularPupilID);

            if (particularPupil == null)
                throw new BusinessException("PupilProfile_Label_PupilIsNotParticular");

            if (ModelState.IsValid)
            {
                ParticularPupilTreatment particularpupiltreatment = new ParticularPupilTreatment();

                Utils.Utils.BindTo(model, particularpupiltreatment);

                particularpupiltreatment.ParticularPupilID = particularPupilID;
                particularpupiltreatment.PupilID = particularPupil.PupilID;
                particularpupiltreatment.UpdatedDate = DateTime.Now;

                this.ParticularPupilTreatmentBusiness.InsertParticularPupilTreatment(globalInfo.UserAccountID, particularpupiltreatment);
                this.ParticularPupilTreatmentBusiness.Save();

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["ParticularPupilID"] = particularPupilID;
                IEnumerable<ParticularPupilTreatmentModel> listParticularPupilTreatment = this._SearchTreatment(SearchInfo);
                return View(new GridModel(listParticularPupilTreatment.ToList()));
            }
            else
            {
                throw new BusinessException(string.Join(",", ModelState.Values.SelectMany(u => u.Errors).Select(u => u.ErrorMessage).ToArray()));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]

        [ValidateAntiForgeryToken]
        public ActionResult _UpdateAjaxEditing(int id, int particularPupilID, ParticularPupilTreatmentModel model)
        {
            CheckPermissionForAction(particularPupilID, "ParticularPupil");
            GlobalInfo globalInfo = new GlobalInfo();
            Utils.Utils.TrimObject(model);

            model.ParticularPupilTreatmentID = id;

            ParticularPupil particularPupil = ParticularPupilBusiness.Find(particularPupilID);

            if (particularPupil == null)
                throw new BusinessException("PupilProfile_Label_PupilIsNotParticular");

            ParticularPupilTreatment particularpupiltreatment = this.ParticularPupilTreatmentBusiness.Find(model.ParticularPupilTreatmentID);

            if (particularpupiltreatment == null)
                throw new BusinessException("Common_Error_DataInputError");

            if (ModelState.IsValid)
            {
                Utils.Utils.BindTo(model, particularpupiltreatment);

                particularpupiltreatment.ParticularPupilID = particularPupilID;
                particularpupiltreatment.PupilID = particularPupil.PupilID;
                particularpupiltreatment.UpdatedDate = DateTime.Now;

                this.ParticularPupilTreatmentBusiness.UpdateParticularPupilTreatment(globalInfo.UserAccountID, particularpupiltreatment);
                this.ParticularPupilTreatmentBusiness.Save();

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["ParticularPupilID"] = particularPupilID;
                IEnumerable<ParticularPupilTreatmentModel> listParticularPupilTreatment = this._SearchTreatment(SearchInfo);
                return View(new GridModel(listParticularPupilTreatment.ToList()));
            }
            else
            {
                Response.StatusCode = 500;
                return Content(string.Join(",", ModelState.Values.SelectMany(u => u.Errors).Select(u => u.ErrorMessage).ToArray()));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]

        [ValidateAntiForgeryToken]
        public ActionResult _DeleteAjaxEditing(int id)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            ParticularPupilTreatment particularPupilTreatment = ParticularPupilTreatmentBusiness.Find(id);

            if (particularPupilTreatment == null)
            {
                Response.StatusCode = 500;
                return Content(string.Join(",", ModelState.Values.SelectMany(u => u.Errors).Select(u => u.ErrorMessage).ToArray()));
            }

            int particularPupilID = particularPupilTreatment.ParticularPupilID;

            this.ParticularPupilTreatmentBusiness.DeleteParticularPupilTreatment(globalInfo.UserAccountID, id, globalInfo.SchoolID.Value);
            this.ParticularPupilTreatmentBusiness.Save();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ParticularPupilID"] = particularPupilID;
            IEnumerable<ParticularPupilTreatmentModel> listParticularPupilTreatment = this._SearchTreatment(SearchInfo);

            return View(new GridModel(listParticularPupilTreatment.ToList()));
        }

        [HttpGet]

        public PartialViewResult ParticularPupil(int id)
        {
            CheckPermissionForAction(id, "PupilProfile", 3);
            GlobalInfo globalInfo = new GlobalInfo();
            PupilParticularModel model = new PupilParticularModel();
            model.PupilID = id;

            PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object> {
                                                                                                        { "AcademicYearID", globalInfo.AcademicYearID.Value },
                                                                                                        { "PupilID", model.PupilID },
                                                                                                        { "Check", "Check" } })
                                                        .Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                                                        .SingleOrDefault();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = poc.ClassID;
            SearchInfo["PupilID"] = poc.PupilID;
            ParticularPupil particularPupil = ParticularPupilBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo).SingleOrDefault();

            model.FullName = poc.PupilProfile.FullName;
            model.ClassName = poc.ClassProfile.DisplayName;

            if (particularPupil != null)
            {
                model.ParticularPupilID = particularPupil.ParticularPupilID;
                model.ClassName = particularPupil.ClassProfile.DisplayName;

                ParticularPupilCharacteristic particularPupilCharacteristic = particularPupil.ParticularPupilCharacteristics.OrderByDescending(u => u.RealUpdatedDate).FirstOrDefault();

                if (particularPupilCharacteristic != null)
                {
                    model.ParticularPupilCharacteristicID = particularPupilCharacteristic.ParticularPupilCharacteristicID;

                    model.FamilyCharacteristic = particularPupilCharacteristic.FamilyCharacteristic;
                    model.PsychologyCharacteristic = particularPupilCharacteristic.PsychologyCharacteristic;
                    model.FamilyCharacteristic = particularPupilCharacteristic.FamilyCharacteristic;
                    model.RealUpdatedDate = particularPupilCharacteristic.RealUpdatedDate;

                    List<ParticularPupilTreatmentModel> lstPTreatment = particularPupil.ParticularPupilTreatments.Select(u =>
                                                                                        new ParticularPupilTreatmentModel
                                                                                        {
                                                                                            ParticularPupilID = u.ParticularPupilID,
                                                                                            ParticularPupilTreatmentID = u.ParticularPupilTreatmentID,
                                                                                            Result = u.Result,
                                                                                            TreatmentResolution = u.TreatmentResolution,
                                                                                            AppliedDate = u.AppliedDate
                                                                                        }).OrderBy(u => u.AppliedDate).ToList();
                    int rowIndex = 1;
                    lstPTreatment.ForEach(u => u.RowIndex = rowIndex++);

                    ViewData[PupilProfileConstants.LIST_PARTICULARPUPIL_TREATMENT] = lstPTreatment.ToList();
                }
            }
            return PartialView("_ParticularPupil", model);
        }

        [HttpPost]

        public JsonResult ParticularPupil(PupilParticularModel model)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            if (ModelState.IsValid)
            {
                GlobalInfo globalInfo = new GlobalInfo();

                Utils.Utils.TrimObject(model);

                ParticularPupil particularPupil = null;
                ParticularPupilCharacteristic particularPupilCharacteristic = null;

                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object> {
                                                                                                        { "AcademicYearID", globalInfo.AcademicYearID.Value },
                                                                                                        { "PupilID", model.PupilID },
                                                                                                        { "Check", "Check" } })
                                                        .Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                                                        .SingleOrDefault();

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo = new Dictionary<string, object>();
                SearchInfo["ClassID"] = poc.ClassID;
                SearchInfo["PupilID"] = poc.PupilID;
                particularPupil = ParticularPupilBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo).SingleOrDefault();

                if (particularPupil == null)
                {
                    particularPupil = new ParticularPupil();

                    particularPupil.PupilID = model.PupilID;

                    particularPupil.AcademicYearID = globalInfo.AcademicYearID.Value;
                    particularPupil.EducationLevelID = poc.ClassProfile.EducationLevelID;
                    particularPupil.SchoolID = globalInfo.SchoolID.Value;
                    particularPupil.ClassID = poc.ClassID;
                    particularPupil.RecordedDate = DateTime.Now;
                }
                else
                {
                    particularPupilCharacteristic = particularPupil.ParticularPupilCharacteristics.OrderByDescending(u => u.RealUpdatedDate).FirstOrDefault();
                }

                if (particularPupilCharacteristic == null)
                {
                    particularPupilCharacteristic = new ParticularPupilCharacteristic();
                    particularPupilCharacteristic.RecordedDate = DateTime.Now;
                }

                particularPupil.RealUpdatedDate = model.RealUpdatedDate;
                particularPupil.UpdatedDate = DateTime.Now;

                particularPupilCharacteristic.UpdatedDate = DateTime.Now;
                particularPupilCharacteristic.RealUpdatedDate = model.RealUpdatedDate;
                particularPupilCharacteristic.PsychologyCharacteristic = model.PsychologyCharacteristic;
                particularPupilCharacteristic.FamilyCharacteristic = model.FamilyCharacteristic;

                //Neu them moi dac diem
                if (particularPupilCharacteristic.ParticularPupilCharacteristicID == 0)
                    particularPupil.ParticularPupilCharacteristics.Add(particularPupilCharacteristic);
                else this.ParticularPupilCharacteristicBusiness.Update(particularPupilCharacteristic);

                //Neu la cap nhat
                if (particularPupil.ParticularPupilID > 0)
                    this.ParticularPupilBusiness.UpdateParticularPupil(globalInfo.UserAccountID, particularPupil);
                else
                    ParticularPupilBusiness.InsertParticularPupil(globalInfo.UserAccountID, particularPupil);

                this.ParticularPupilBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }

            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DeleteParticularPupil(int id) //ParticularPupilID
        {
            CheckPermissionForAction(id, "ParticularPupil");
            GlobalInfo glo = new GlobalInfo();
            this.ParticularPupilBusiness.DeleteParticularPupil(glo.UserAccountID, id, glo.SchoolID.Value);
            this.ParticularPupilBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        [HttpGet]

        public PartialViewResult PraisePupil(int? id, int pupilID)
        {
            CheckPermissionForAction(pupilID, "PupilProfile", 3);
            GlobalInfo globalInfo = new GlobalInfo();

            PupilProfile pupil = PupilProfileBusiness.Find(pupilID);
            PupilPraise pupilPraise = PupilPraiseBusiness.Find(id);

            if (pupil == null)
                throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");

            PraisePupilModel model = new PraisePupilModel();

            if (pupilPraise != null)
            {
                if (pupil.PupilProfileID != pupilPraise.PupilID)
                    throw new BusinessException("PupilPraise_Label_NotBelongToPupil");

                model.PupilPraiseID = pupilPraise.PupilPraiseID;
                model.PupilID = pupil.PupilProfileID;
                model.FullName = pupil.FullName;
                model.IsRecordedInSchoolReport = pupilPraise.IsRecordedInSchoolReport.HasValue && pupilPraise.IsRecordedInSchoolReport.Value;
                model.PraiseDate = pupilPraise.PraiseDate;
                model.PraiseLevel = pupilPraise.PraiseLevel;
                model.PraiseTypeID = pupilPraise.PraiseTypeID;
                model.Description = pupilPraise.Description;
            }
            else
            {
                model.PupilID = pupilID;
                model.PraiseDate = DateTime.Now;
                model.FullName = pupil.FullName;
            }

            List<PraiseType> LstPraiseType = PraiseTypeBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            ViewData[PupilProfileConstants.LIST_PRAISE_TYPE] = new SelectList(LstPraiseType, "PraiseTypeID", "Resolution");
            ViewData[PupilProfileConstants.IS_ENABLE_NOTE_PRAISE] = LstPraiseType.Count > 0 ? false : true;

            ViewData[PupilProfileConstants.LIST_PRAISE_LEVEL] = new SelectList(CommonList.DisciplineLevel(), "value", "key");

            return PartialView("_PraisePupil", model);
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult PraisePupil(PraisePupilModel model)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            Utils.Utils.TrimObject(model);

            if (ModelState.IsValid)
            {
                GlobalInfo globalInfo = new GlobalInfo();

                PupilProfile pupilProfile = PupilProfileBusiness.Find(model.PupilID);
                if (pupilProfile == null)
                    return Json(Res.Get("PupilProfile_Validate_NotExistOrStudying"), JsonMessage.ERROR);

                PupilPraise pupilPraise = null;
                if (model.PupilPraiseID.HasValue && model.PupilPraiseID.Value > 0)
                {
                    pupilPraise = PupilPraiseBusiness.Find(model.PupilPraiseID);

                    if (pupilPraise == null)
                        return Json(Res.Get("PupilPraise_Label_NotExists"), JsonMessage.ERROR);

                    if (pupilProfile.PupilProfileID != pupilPraise.PupilID)
                        return Json(Res.Get("PupilPraise_Label_NotBelongToPupil"), JsonMessage.ERROR);

                    pupilPraise.PraiseTypeID = model.PraiseTypeID;
                    pupilPraise.PraiseDate = model.PraiseDate.Date;
                    pupilPraise.PraiseLevel = model.PraiseLevel;
                    pupilPraise.Description = model.Description;
                    pupilPraise.IsRecordedInSchoolReport = model.IsRecordedInSchoolReport;
                    //pupilPraise.Semester = semester;
                    this.PupilPraiseBusiness.UpdatePupilPraise(globalInfo.UserAccountID, pupilPraise);
                }
                else
                {
                    pupilPraise = new PupilPraise();
                    pupilPraise.PupilID = model.PupilID;
                    pupilPraise.ClassID = pupilProfile.CurrentClassID;
                    pupilPraise.EducationLevelID = pupilProfile.ClassProfile.EducationLevelID;
                    pupilPraise.SchoolID = globalInfo.SchoolID.Value;
                    pupilPraise.AcademicYearID = globalInfo.AcademicYearID.Value;
                    pupilPraise.PraiseTypeID = model.PraiseTypeID;
                    pupilPraise.PraiseDate = model.PraiseDate.Date;
                    pupilPraise.PraiseLevel = model.PraiseLevel;
                    pupilPraise.Description = model.Description;
                    pupilPraise.IsRecordedInSchoolReport = model.IsRecordedInSchoolReport;
                    //pupilPraise.Semester = semester;
                    this.PupilPraiseBusiness.InsertPupilPraise(globalInfo.UserAccountID, pupilPraise);
                }

                this.PupilPraiseBusiness.Save();

                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PraisePupilSuccess")));
            }

            return Json(string.Join(",", ModelState.Values.Where(u => u.Errors.Count > 0).Select(u => string.Join(",", u.Errors.Select(v => v.ErrorMessage)))), JsonMessage.ERROR);
        }

        [HttpGet]

        public PartialViewResult DisciplinePupil(int? id, int pupilID)
        {
            CheckPermissionForAction(pupilID, "PupilProfile", 3);
            GlobalInfo globalInfo = new GlobalInfo();
            PupilProfile pupil = PupilProfileBusiness.Find(pupilID);
            PupilDiscipline pupilDiscipline = PupilDisciplineBusiness.Find(id);

            if (pupil == null)
                throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");

            DisciplinePupilModel model = new DisciplinePupilModel();

            if (pupilDiscipline != null)
            {
                if (pupil.PupilProfileID != pupilDiscipline.PupilID)
                    throw new BusinessException("PupilDiscipline_Label_NotBelongToPupil");

                model.PupilDisciplineID = pupilDiscipline.PupilDisciplineID;
                model.PupilID = pupil.PupilProfileID;
                model.FullName = pupil.FullName;
                model.IsRecordedInSchoolReport = pupilDiscipline.IsRecordedInSchoolReport;
                model.DisciplineDate = pupilDiscipline.DisciplinedDate;
                model.DisciplineLevel = pupilDiscipline.DisciplineLevel;
                model.DisciplineTypeID = pupilDiscipline.DisciplineTypeID;
                model.Description = pupilDiscipline.Description;
            }
            else
            {
                model.PupilID = pupilID;
                model.DisciplineDate = DateTime.Now;
                model.FullName = pupil.FullName;
            }

            List<DisciplineType> lstDisciplineType = DisciplineTypeBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            ViewData[PupilProfileConstants.LIST_DISCIPLINE_TYPE] = new SelectList(lstDisciplineType, "DisciplineTypeID", "Resolution");
            ViewData[PupilProfileConstants.IS_ENABLE_NOTE_DISCIPLINE] = lstDisciplineType.Count > 0 ? false : true;

            ViewData[PupilProfileConstants.LIST_DISCIPLINE_LEVEL] = new SelectList(CommonList.DisciplineLevel(), "value", "key");

            return PartialView("_DisciplinePupil", model);
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DisciplinePupil(DisciplinePupilModel model)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            Utils.Utils.TrimObject(model);

            if (ModelState.IsValid)
            {
                GlobalInfo globalInfo = new GlobalInfo();

                PupilProfile pupilProfile = PupilProfileBusiness.Find(model.PupilID);
                if (pupilProfile == null)
                    return Json(Res.Get("PupilProfile_Validate_NotExistOrStudying"), JsonMessage.ERROR);

                PupilDiscipline pupilDiscipline = null;
                if (model.PupilDisciplineID.HasValue && model.PupilDisciplineID.Value > 0)
                {
                    pupilDiscipline = PupilDisciplineBusiness.Find(model.PupilDisciplineID);

                    if (pupilProfile.PupilProfileID != pupilDiscipline.PupilID)
                        throw new BusinessException("PupilDiscipline_Label_NotBelongToPupil");

                    pupilDiscipline.DisciplineTypeID = model.DisciplineTypeID;
                    pupilDiscipline.DisciplinedDate = model.DisciplineDate;
                    pupilDiscipline.DisciplineLevel = model.DisciplineLevel;
                    pupilDiscipline.Description = model.Description;
                    pupilDiscipline.IsRecordedInSchoolReport = model.IsRecordedInSchoolReport;
                    this.PupilDisciplineBusiness.UpdatePupilDiscipline(globalInfo.UserAccountID, pupilDiscipline);
                }
                else
                {
                    pupilDiscipline = new PupilDiscipline();
                    pupilDiscipline.PupilID = model.PupilID;
                    pupilDiscipline.ClassID = pupilProfile.CurrentClassID;
                    pupilDiscipline.EducationLevelID = pupilProfile.ClassProfile.EducationLevelID;
                    pupilDiscipline.SchoolID = globalInfo.SchoolID.Value;
                    pupilDiscipline.AcademicYearID = globalInfo.AcademicYearID.Value;
                    pupilDiscipline.DisciplineTypeID = model.DisciplineTypeID;
                    pupilDiscipline.DisciplinedDate = model.DisciplineDate;
                    pupilDiscipline.DisciplineLevel = model.DisciplineLevel;
                    pupilDiscipline.Description = model.Description;
                    pupilDiscipline.IsRecordedInSchoolReport = model.IsRecordedInSchoolReport;
                    this.PupilDisciplineBusiness.InsertPupilDiscipline(globalInfo.UserAccountID, pupilDiscipline);
                }

                this.PupilDisciplineBusiness.Save();

                return Json(new JsonMessage(Res.Get("PupilDiscipline_Label_DisciplinePupilSuccess")));
            }

            return Json(string.Join(",", ModelState.Values.Where(u => u.Errors.Count > 0).Select(u => string.Join(",", u.Errors.Select(v => v.ErrorMessage)))), JsonMessage.ERROR);
        }
        #endregion

        #region Tabs Actions
        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteExempted(int[] chkExemptedSubject)
        {
            GlobalInfo glo = new GlobalInfo();

            if (chkExemptedSubject == null || chkExemptedSubject.Length == 0)
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteFailureMessage"), JsonMessage.ERROR));

            foreach (int exemptedSubjectID in chkExemptedSubject)
            {
                ExemptedSubjectBusiness.DeleteExemptedSubject(glo.UserAccountID, exemptedSubjectID, glo.SchoolID.Value);
            }

            ExemptedSubjectBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), JsonMessage.SUCCESS));
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteParticular(int[] chkParticular)
        {
            GlobalInfo glo = new GlobalInfo();

            if (chkParticular == null || chkParticular.Length == 0)
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteFailureMessage"), JsonMessage.ERROR));

            foreach (int particularPupilID in chkParticular)
            {
                this.ParticularPupilBusiness.DeleteParticularPupil(glo.UserAccountID, particularPupilID, glo.SchoolID.Value);
            }
            this.ParticularPupilBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), JsonMessage.SUCCESS));
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePraise(int id)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            this.PupilPraiseBusiness.DeletePupilPraise(globalInfo.UserAccountID, id, globalInfo.SchoolID.Value);
            this.PupilPraiseBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteDiscipline(int id)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            this.PupilDisciplineBusiness.DeletePupilDiscipline(globalInfo.UserAccountID, id, globalInfo.SchoolID.Value);
            this.PupilDisciplineBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult ApproveSchoolMovement(FormCollection form)
        {
            GlobalInfo glo = new GlobalInfo();

            if (!UtilsBusiness.IsBGH(glo.UserAccountID) && !glo.IsAdminSchoolRole)
                return Json(new JsonMessage(Res.Get("Common_Label_NotHasPermission"), JsonMessage.ERROR));

            IEnumerable<string> lstCbo = form.AllKeys.Where(u => u.StartsWith("cboSchoolMovement_"));

            var lstClassProfile = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", glo.AcademicYearID } })
                                                    .Select(u => new { u.ClassProfileID, u.EducationLevelID, u.DisplayName })
                                                    .ToList();
            int classID = 0;
            int schoolMovementID = 0;


            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);

            using (TransactionScope transactionScope = new TransactionScope())
            {
                MovementAcceptanceBusiness.SetAutoDetectChangesEnabled(false);
                foreach (string sltClass in lstCbo)
                {
                    if (int.TryParse(sltClass.Replace("cboSchoolMovement_", "").Trim(), out schoolMovementID) && schoolMovementID > 0 //Lay SchoolMovementID
                        && int.TryParse(form.Get(sltClass), out classID) && classID > 0)                                              //Lay ClassID - Lop nhan hoc sinh nay
                    {
                        SchoolMovement schoolMovement = SchoolMovementBusiness.Find(schoolMovementID);
                        MovementAcceptance movementAcceptance = new MovementAcceptance();
                        movementAcceptance.AcademicYearID = glo.AcademicYearID.Value;
                        movementAcceptance.ClassID = classID;
                        movementAcceptance.Description = string.Empty;
                        movementAcceptance.EducationLevelID = lstClassProfile.Where(u => u.ClassProfileID == classID).FirstOrDefault().EducationLevelID;
                        movementAcceptance.MovedDate = DateTime.Now;
                        movementAcceptance.MovedFromClassID = schoolMovement.ClassID;
                        movementAcceptance.MovedFromClassName = string.Empty;
                        movementAcceptance.MovedFromSchoolID = schoolMovement.SchoolID;
                        movementAcceptance.MovedFromSchoolName = string.Empty;
                        movementAcceptance.PupilID = schoolMovement.PupilID;
                        movementAcceptance.SchoolID = glo.SchoolID.Value;
                        movementAcceptance.Semester = schoolMovement.Semester;
                        this.MovementAcceptanceBusiness.InsertMovementAcceptance(glo.UserAccountID, movementAcceptance, isMovedHistory);
                        schoolMovement.MovedToClassID = classID;
                        SchoolMovementBusiness.Update(schoolMovement);
                    }
                }

                this.SchoolMovementBusiness.Save();
                transactionScope.Complete();
                MovementAcceptanceBusiness.SetAutoDetectChangesEnabled(true);
            }
            return Json(new JsonMessage(Res.Get("PupilProfile_Label_MovementAcceptanceSuccess")));
        }
        #endregion

        #region setviewdata

        public void SetViewData(int? ClassID, int PupilProfileID = 0)
        {
            CheckPermissionForAction(ClassID, "ClassProfile");
            ViewData[PupilProfileConstants.VISIBLE_ORDER] = false;
            ViewData[PupilProfileConstants.ENABLE_PAGING] = true;
            int isEnableRedirect = CheckSeePermission("PupilSummaryReport", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData["isEnableRedirect"] = isEnableRedirect;
            //Get view data here
            GlobalInfo global = new GlobalInfo();

            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(global.SchoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            ViewData[PupilProfileConstants.FirstStartDate] = academicYear.FirstSemesterStartDate.Value;

            int selectedEducationLevel = 0;
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                ClassProfile clsProfile = ClassProfileBusiness.Find(ClassID);
                if (clsProfile != null)
                {
                    ViewData[PupilProfileConstants.LISTCLASS] = new SelectList(getClassFromEducationLevel(clsProfile.EducationLevelID).ToList(), "ClassProfileID", "DisplayName", ClassID.Value);
                    selectedEducationLevel = clsProfile.EducationLevelID;
                }
                else
                    ViewData[PupilProfileConstants.LISTCLASS] = new SelectList(new string[] { });
            }
            else ViewData[PupilProfileConstants.LISTCLASS] = new SelectList(new string[] { });

            ViewData[PupilProfileConstants.LISTEDUCATIONLEVEL] = global.EducationLevels != null ? new SelectList(global.EducationLevels, "EducationLevelID", "Resolution", selectedEducationLevel) : new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            ViewData[PupilProfileConstants.LISTGENRE] = new SelectList(CommonList.GenreAndAll(), "key", "value");
            ViewData[PupilProfileConstants.LISTSTATUS] = new SelectList(CommonList.PupilStatus(), "key", "value");
            ViewData[PupilProfileConstants.LISTPREVIOUSGRADUATIONLEVEL] = new SelectList(CommonList.PreviousGraduationLevel(), "key", "value");
            ViewData[PupilProfileConstants.LISTDISTRICT] = new SelectList(new string[] { });
            ViewData[PupilProfileConstants.LISTCOMMUNE] = new SelectList(new string[] { });

            List<ComboObject> lstSupportingPolicy = CommonList.SupportingPolicy();
            if (_globalInfo.ProvinceID == SystemParamsInFile.PROVINCEID_LAICHAU) lstSupportingPolicy.Add(new ComboObject(SystemParamsInFile.POLICY_01_UBND_LCU_GOVERNMENT_DECISION.ToString(), Res.Get("Chế độ theo QĐ QĐ 01-UBND Tỉnh Lai Châu")));
            ViewData[PupilProfileConstants.LISTSUPPORTINGPOLICY] = new SelectList(lstSupportingPolicy, "key", "value");

            IQueryable<PolicyTarget> lsPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object>() { });
            ViewData[PupilProfileConstants.LISTPOLICYTARGET] = new SelectList(lsPolicyTarget.ToList(), "PolicyTargetID", "Resolution");

            IQueryable<Ethnic> lsEthnic = EthnicBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.EthnicName);
            ViewData[PupilProfileConstants.LISTETHNIC] = new SelectList(lsEthnic.ToList(), "EthnicID", "EthnicName");

            List<OtherEthnic> lstOtherEthnic = new List<OtherEthnic>();
            if (lsEthnic.Count() != 0 && PupilProfileID != null && PupilProfileID != 0)
            {
                int? ethnicIDOfPupil = PupilProfileBusiness.Find(PupilProfileID).EthnicID;
                IQueryable<OtherEthnic> queryOE = OtherEthnicBusiness.All.Where(x => x.EthnicID == ethnicIDOfPupil);
                lstOtherEthnic = queryOE.ToList();
            }
            ViewData[PupilProfileConstants.LISTOTHERETHNIC] = new SelectList(lstOtherEthnic, "OtherEthnicID", "OtherEthnicName", 0);

            List<Religion> lsReligion = ReligionBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            Religion religion = lsReligion.Where(u => u.Resolution != null && u.Resolution.ToLower().Trim().Equals("không")).FirstOrDefault();
            ViewData[PupilProfileConstants.LISTRELIGION] = new SelectList(lsReligion.ToList(), "ReligionID", "Resolution", religion != null ? religion.ReligionID : 0);

            IQueryable<Area> lsArea = AreaBusiness.Search(new Dictionary<string, object>() { });
            ViewData[PupilProfileConstants.LISTAREA] = new SelectList(lsArea.ToList(), "AreaID", "AreaName");

            IQueryable<PriorityType> lsPriorityType = PriorityTypeBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", global.AppliedLevel } });
            ViewData[PupilProfileConstants.LISTPRIORITYTYPE] = new SelectList(lsPriorityType.ToList(), "PriorityTypeID", "Resolution");

            IQueryable<Province> lsProvince = ProvinceBusiness.Search(new Dictionary<string, object>() { }).OrderBy(o => o.ProvinceName);
            ViewData[PupilProfileConstants.LISTPROVINCE] = new SelectList(lsProvince.ToList(), "ProvinceID", "ProvinceName");

            IQueryable<DisabledType> lsDisabledType = DisabledTypeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
            ViewData[PupilProfileConstants.LISTDISABLEDTYPE] = new SelectList(lsDisabledType.ToList(), "DisabledTypeID", "Resolution");

            IQueryable<FamilyType> lsFamilyType = FamilyTypeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
            ViewData[PupilProfileConstants.LISTFAMILYTYPE] = new SelectList(lsFamilyType.ToList(), "FamilyTypeID", "Resolution");

            IQueryable<PolicyRegime> lsPolicyRegime = PolicyRegimeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
            ViewData[PupilProfileConstants.LISTPOLICYREGIME] = new SelectList(lsPolicyRegime, "PolicyRegimeID", "Resolution");

            //ViewData[PupilProfileConstants.LIST_IT_CERTIFICATE] = new SelectList(ITQualificationLevelBusiness.Search(new Dictionary<string, object> { { "IsActive", true }, { "Type", SystemParamsInFile.TYPE_CERTIFICATE } }).ToList(), "ITQualificationLevelID", "Resolution");
            ViewData[PupilProfileConstants.LIST_IT_CERTIFICATE] = new SelectList(ITQualificationLevelBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Where(w => w.ITQualificationLevelID == 2 || w.ITQualificationLevelID == 3 || w.ITQualificationLevelID == 4).ToList(), "ITQualificationLevelID", "Resolution");//2,3,4
            //ViewData[PupilProfileConstants.LIST_FOREIGN_LANGUAGE_CERTIFICATE] = new SelectList(ForeignLanguageGradeBusiness.Search(new Dictionary<string, object> { { "IsActive", true }, { "Type", SystemParamsInFile.TYPE_CERTIFICATE } }).ToList(), "ForeignLanguageGradeID", "Resolution");//16,17,18
            ViewData[PupilProfileConstants.LIST_FOREIGN_LANGUAGE_CERTIFICATE] = new SelectList(ForeignLanguageGradeBusiness.Search(new Dictionary<string, object> { { "IsActive", true }, { "Type", SystemParamsInFile.TYPE_CERTIFICATE } }).Where(w => w.ForeignLanguageGradeID == 16 || w.ForeignLanguageGradeID == 17 || w.ForeignLanguageGradeID == 18).ToList(), "ForeignLanguageGradeID", "Resolution");//16,17,18

            ViewData[PupilProfileConstants.ENABLE_CKB_POLICYREGIME] = lsPolicyRegime.Count() > 0;
            ViewData[PupilProfileConstants.AUTO_GEN_CODE_CHECK] = CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            ViewData[PupilProfileConstants.IS_SCHOOL_MODIFIED] = CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            ViewData[PupilProfileConstants.IS_GDTX_SCHOOL] = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
            ViewData[PupilProfileConstants.LIST_PUPIL_LEARNING_TYPE] = new SelectList(CommonList.PupilLearningType(), "key", "value");
            ViewData[PupilProfileConstants.LISTBLOODTYPE] = new SelectList(CommonList.BloodType(), "key", "value");
            ViewData[PupilProfileConstants.LISTFOREIGNLANGUAGETRAINING] = new SelectList(CommonList.ForeignLanguageTraining(), "key", "value");
            ViewData[PupilProfileConstants.LISTENROLMENTTYPE] = new SelectList(CommonList.EnrolmentType(), "key", "value");
            ViewData[PupilProfileConstants.LISTCLASSTYPE] = new SelectList(CommonList.ClassType(), "key", "value");
            ViewData[PupilProfileConstants.LISTHOMEPLACE] = new SelectList(CommonList.HomePlace(), "key", "value");
        }

        #endregion setviewdata

        #region ajax load combobox

        /// <summary>
        /// Lấy danh sách các lớp học theo khối
        /// Nếu cấp học bằng ) thì lấy tất cả
        /// Nếu người dùng không phải là admin trường thì lấy các lớp người này dạy
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };

            //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
            if (!global.IsAdminSchoolRole && !global.IsViewAll && !UtilsBusiness.IsBGH(global.UserAccountID))
            {
                dic["UserAccountID"] = global.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING;
            }

            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).OrderBy(u => u.DisplayName);
            return lsCP;
        }

        private IQueryable<District> getDistrictFromProvide(int ProvinceID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",ProvinceID},
                    {"IsActive", true}
                };
            IQueryable<District> lsD = DistrictBusiness.Search(dic).OrderBy(u => u.DistrictName);
            return lsD;
        }

        private IQueryable<Commune> getCommuneFromDistrict(int DistrictID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"DistrictID",DistrictID},
                    {"IsActive", true}
                };
            IQueryable<Commune> lsC = CommuneBusiness.Search(dic).OrderBy(o => o.CommuneName);
            return lsC;
        }

        private IQueryable<HealthPeriod> getHealthPeriodFromAcademicYear(int AcademicYearID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            IQueryable<HealthPeriod> lstPeriodic = this.MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dic).Where(o => o.HealthPeriod != null)
                .Select(o => o.HealthPeriod).Distinct();

            return lstPeriodic;
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClassOfHeadteacher(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                GlobalInfo global = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };

                //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
                if (!global.IsAdminSchoolRole)
                {
                    dic["UserAccountID"] = global.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                }

                IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).OrderBy(u => u.DisplayName);
                return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        public JsonResult AjaxLoadOtherEthnic(int? EthnicID)
        {
            if (EthnicID.HasValue)
            {
                IQueryable<OtherEthnic> lsC = OtherEthnicBusiness.All.Where(x => x.EthnicID == EthnicID);
                if (lsC.Count() != 0)
                {
                    List<OtherEthnic> lstOtherEthnic = lsC.ToList();
                    lstOtherEthnic = lstOtherEthnic.OrderBy(o => o.OtherEthnicName).ToList();
                    return Json(new SelectList(lstOtherEthnic, "OtherEthnicID", "OtherEthnicName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadDistrict(int? ProvinceID)
        {
            if (ProvinceID.HasValue)
            {
                IQueryable<District> lsD = getDistrictFromProvide(ProvinceID.Value);
                if (lsD.Count() != 0)
                {
                    return Json(new SelectList(lsD.ToList(), "DistrictID", "DistrictName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadCommune(int? DistrictID)
        {
            if (DistrictID.HasValue)
            {
                IQueryable<Commune> lsC = getCommuneFromDistrict(DistrictID.Value);
                if (lsC.Count() != 0)
                {
                    return Json(new SelectList(lsC.ToList(), "CommuneID", "CommuneName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSchool(int? DistrictId)
        {
            List<SchoolProfile> lstSchool = new List<SchoolProfile>();
            if (DistrictId.HasValue && DistrictId.Value > 0)
            {
                GlobalInfo glo = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["DistrictID"] = DistrictId.Value;
                dic["IsActive"] = true;
                dic["AppliedLevel"] = glo.AppliedLevel.Value;
                //dungnt fix bug 168941
                lstSchool = this.SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName).Where(o => o.SchoolProfileID != glo.SchoolID).ToList();
            }
            return Json(new SelectList(lstSchool, "SchoolProfileID", "SchoolName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPeriod(int? AcademicYearID)
        {
            List<HealthPeriod> lstPeriodic = new List<HealthPeriod>();
            if (AcademicYearID.HasValue && AcademicYearID.Value > 0)
            {
                lstPeriodic = getHealthPeriodFromAcademicYear(AcademicYearID.Value).ToList();
            }
            return Json(new SelectList(lstPeriodic, "HealthPeriodID", "Resolution"));
        }
        #endregion ajax load combobox

        #region redirect create,edit,detail

        public ActionResult RedirectCreatePage()
        {
            CheckActionPermissionMinView();
            SetViewData(null);

            //Kiểm tra UserInfo. HasHeadTeacherPermission (0) = false:
            //Trả về trang thông báo lỗi “Bạn không có quyền truy cập vào dữ liệu này”
            GlobalInfo global = new GlobalInfo();
            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, 0))
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }

            bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            bool isSchoolModified = CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);

            PupilProfileViewModel model = new PupilProfileViewModel();
            //model.EnrolmentDate = DateTime.Now.Date;
            model.AutoGenCode = isAutoGenCode && !isSchoolModified;

            return PartialView("_Create", model);
        }


        public ActionResult GetEditPupilProfile(int PupilProfileID, int NextPre = 0)
        {
            CheckPermissionForAction(PupilProfileID, "PupilProfile", 3);
            GlobalInfo global = new GlobalInfo();

            bool isEnd = false;
            bool isFirst = false;
            List<int> lstInt = new List<int>();
            if (Session["lstPupilIDBySearch"] != null)
            {
                lstInt = (List<int>)Session["lstPupilIDBySearch"];
            }
            //List<int> lstInt = new List<int>();
            //var abc = (SearchViewModel)Session[PupilProfileChildrenConstants.SEARCHFORM];
            int position = lstInt.FindIndex(X => X == PupilProfileID);
            if (position == 0)
                isFirst = true;
            if (position == lstInt.Count - 1)
                isEnd = true;
            if (NextPre != 0 && lstInt.Count != 0)
            {
                if (NextPre == 1)
                {
                    PupilProfileID = lstInt[position - 1];
                    if (position - 1 == 0)
                        isFirst = true;
                    if (position == lstInt.Count - 1)
                        isEnd = false;
                }
                else
                {
                    PupilProfileID = lstInt[position + 1];
                    if (position + 1 == lstInt.Count - 1)
                        isEnd = true;

                    if (position == 0)
                        isFirst = false;
                }
            }

            List<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", global.AcademicYearID } }).ToList();
            if (lstPoc != null && !lstPoc.Where(o => o.PupilID == PupilProfileID).Any())
            {
                return new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "Index" }));
            }
            SetViewData(null, PupilProfileID);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["PupilProfileID"] = PupilProfileID;
            SearchInfo["ProfileStatus"] = SystemParamsInFile.PUPIL_STATUS_STUDYING;

            PupilProfileViewModel model = SearchPaging(SearchInfo, false, false).List.FirstOrDefault();
            PupilProfile pp = PupilProfileBusiness.Find(PupilProfileID);
            model.PupilProfileID = PupilProfileID;
            if (pp.FatherBirthDate.HasValue)
            {
                model.iFatherBirthDate = pp.FatherBirthDate.Value.Year;
            }
            if (pp.MotherBirthDate.HasValue)
            {
                model.iMotherBirthDate = pp.MotherBirthDate.Value.Year;
            }
            if (pp.SponsorBirthDate.HasValue)
            {
                model.iSponsorBirthDate = pp.SponsorBirthDate.Value.Year;
            }

            //load combobox
            IQueryable<ClassProfile> lsCp = getClassFromEducationLevel(model.EducationLevel);
            if (lsCp.Count() > 0)
            {
                ViewData[PupilProfileConstants.LISTCLASS] = new SelectList(lsCp.ToList(), "ClassProfileID", "DisplayName");
            }
            else
            {
                ViewData[PupilProfileConstants.LISTCLASS] = new SelectList(new string[] { });
            }

            IQueryable<District> lsD = getDistrictFromProvide(model.ProvinceID);
            if (lsD.Count() > 0)
            {
                ViewData[PupilProfileConstants.LISTDISTRICT] = new SelectList(lsD.ToList(), "DistrictID", "DistrictName");
            }
            else
            {
                ViewData[PupilProfileConstants.LISTDISTRICT] = new SelectList(new string[] { });
            }

            if (model.DistrictID.HasValue)
            {
                IQueryable<Commune> lsC = getCommuneFromDistrict(model.DistrictID.Value);
                if (lsC.Count() > 0)
                {
                    ViewData[PupilProfileConstants.LISTCOMMUNE] = new SelectList(lsC.ToList(), "CommuneID", "CommuneName");
                }
                else
                {
                    ViewData[PupilProfileConstants.LISTCOMMUNE] = new SelectList(new string[] { });
                }
            }
            if (model.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)
            {
                ViewData[PupilProfileConstants.PUPIL_NOT_STUDY] = false;
            }
            else
            {
                ViewData[PupilProfileConstants.PUPIL_NOT_STUDY] = true;
            }
            model.ClassType = pp.ClassType;
            model.isFirst = isFirst;
            model.isEnd = isEnd;
            if (pp.MinorityMother.HasValue)
                model.MinorityMother = pp.MinorityMother.Value;
            if (pp.MinorityFather.HasValue)
                model.MinorityFather = pp.MinorityFather.Value;
            if (pp.UsedMoetProgram.HasValue)
                model.UsedMoetProgram = pp.UsedMoetProgram.Value;
            return View("_Edit", model);
        }


        public ActionResult GetDetailPupilProfile(int PupilProfileID, int ClassID, string typeOfActionResult = "view")
        {
            CheckPermissionForAction(PupilProfileID, "PupilProfile", 3);
            GlobalInfo global = new GlobalInfo();
            List<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", global.AcademicYearID } }).ToList();
            if (lstPoc != null && !lstPoc.Where(o => o.PupilID == PupilProfileID).Any())
            {
                return new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "Index" }));
            }
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(global.SchoolID);
            ViewData[PupilProfileConstants.IS_GDTX_SCHOOL] = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["PupilProfileID"] = PupilProfileID;
            SearchInfo["CurrentClassID"] = ClassID;
            //SearchInfo["ProfileStatus"] = SystemParamsInFile.PUPIL_STATUS_STUDYING;
            PupilProfileViewModel model = new PupilProfileViewModel();
            model = SearchPaging(SearchInfo, false, false).List.FirstOrDefault();
            if (model == null)
            {
                model = new PupilProfileViewModel();
            }
            PupilProfile pp = PupilProfileBusiness.Find(PupilProfileID);
            if (pp.FatherBirthDate.HasValue)
            {
                model.iFatherBirthDate = pp.FatherBirthDate.Value.Year;
            }
            if (pp.MotherBirthDate.HasValue)
            {
                model.iMotherBirthDate = pp.MotherBirthDate.Value.Year;
            }
            if (pp.SponsorBirthDate.HasValue)
            {
                model.iSponsorBirthDate = pp.SponsorBirthDate.Value.Year;
            }

            if (pp.PolicyRegimeID.HasValue)
            {
                model.PolicyRegimeName = pp.PolicyRegime.Resolution;
            }
            string ClassTypeName = "";
            model.ClassType = pp.ClassType;
            if (pp.ClassType.HasValue)
            {
                switch (pp.ClassType.Value)
                {
                    case SystemParamsInFile.CLASSTYPE_NOITRU:
                        ClassTypeName = Res.Get("PupilProfile_Label_ClassTypeNoiTru");
                        break;
                    case SystemParamsInFile.CLASSTYPE_BANTRU:
                        ClassTypeName = Res.Get("PupilProfile_Label_ClassTypeBanTru");
                        break;
                    case SystemParamsInFile.CLASSTYPE_BANTRUDANNUOI:
                        ClassTypeName = Res.Get("PupilProfile_Label_ClassTypeBanTruDanNuoi");
                        break;
                    case SystemParamsInFile.CLASSTYPE_NOITRUDANNUOI:
                        ClassTypeName = Res.Get("PupilProfile_Label_ClassTypeNoiTruDanNuoi");
                        break;
                    default:
                        ClassTypeName = Res.Get("Common_Label_NoChoice");
                        break;
                }
                model.ClassTypeName = ClassTypeName;
            }
            else
            {
                model.ClassTypeName = Res.Get("Common_Label_NoChoice");
            }
            ViewData[PupilProfileConstants.PUPILPROFILE_MODEL_FOR_DETAIL] = model;

            if (model.OtherEthnicID != null)
            {
                OtherEthnic otherEthnic = OtherEthnicBusiness.Find(model.OtherEthnicID);
                model.OtherEthnicCode = otherEthnic.OtherEthnicCode;
                model.OtherEthnicName = otherEthnic.OtherEthnicName;
            }

            // Location 
            IQueryable<Location> iqLocation = LocationBusiness.Search(new Dictionary<string, object>());

            //thanh tich khen thuong PupilPraiseBusiness
            #region Khen thuong
            var iqPupilPraise = PupilPraiseBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"PupilID",PupilProfileID}
                ,{"AcademicYearID",global.AcademicYearID}
            }).OrderByDescending(o => o.PraiseDate).ToList();

            List<PupilPraiseViewModel> lsPPrVM = new List<PupilPraiseViewModel>();
            List<ComboObject> ListPraiseLevel = new List<ComboObject>();
            ListPraiseLevel.Add(new ComboObject("1", Res.Get("Education_Hierachy_Level_Ministry")));
            ListPraiseLevel.Add(new ComboObject("2", Res.Get("Education_Hierachy_Level_Province_Office")));
            ListPraiseLevel.Add(new ComboObject("3", Res.Get("Education_Hierachy_Level_District_Office")));
            ListPraiseLevel.Add(new ComboObject("4", Res.Get("Education_Hierachy_Level_School")));
            ListPraiseLevel.Add(new ComboObject("5", Res.Get("Education_Hierachy_Level_Class")));
            foreach (var ppr in iqPupilPraise)
            {
                PupilPraiseViewModel ppvm = new PupilPraiseViewModel();
                Utils.Utils.BindTo(ppr, ppvm);

                ppvm.AcademicYearName = string.Format("{0}-{1}", ppr.AcademicYear.Year, (ppr.AcademicYear.Year + 1).ToString());
                ppvm.ClassName = ppr.ClassProfile.DisplayName;
                if (ppvm.Semester.HasValue)
                {
                    ppvm.SemesterName = ppvm.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                ? Res.Get("Common_Label_FirstSemester")
                                                : Res.Get("Common_Label_SecondSemester");
                }
                else
                {
                    ppvm.SemesterName = "";
                }
                var lstPre = ListPraiseLevel.Where(o => o.key == ppvm.PraiseLevel.ToString()).Select(o => o.value);
                if (lstPre != null && lstPre.Count() > 0)
                {
                    ppvm.LocationName = lstPre.FirstOrDefault();
                }
                ppvm.PraiseResolution = ppr.PraiseType != null ? ppr.PraiseType.Resolution : string.Empty;
                lsPPrVM.Add(ppvm);
            }
            ViewData[PupilProfileConstants.LIST_PUPIL_PRAISE] = lsPPrVM;
            #endregion

            //thanh tich ky luat
            #region Ky luat
            var lsPupilDiscipline = PupilDisciplineBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                 {"PupilID",PupilProfileID}
                ,{"AcademicYearID",global.AcademicYearID}
            }).OrderByDescending(o => o.DisciplinedDate).ToList();

            List<PupilDisciplineViewModel> lsPDVM = new List<PupilDisciplineViewModel>();

            foreach (var pd in lsPupilDiscipline)
            {
                PupilDisciplineViewModel pdvm = new PupilDisciplineViewModel();
                Utils.Utils.BindTo(pd, pdvm);
                //them nhung truong tren grid
                AcademicYear ay = pd.AcademicYear;
                pdvm.AcademicYearName = string.Format("{0}-{1}", ay.Year.ToString(), (ay.Year + 1).ToString());
                pdvm.ClassName = pd.ClassProfile.DisplayName;
                if (IsDateInsideDateRange(pd.DisciplinedDate, ay.FirstSemesterStartDate.Value, ay.FirstSemesterEndDate.Value))
                {
                    pdvm.SemesterName = Res.Get("Common_Label_FirstSemester");
                }
                else if (IsDateInsideDateRange(pd.DisciplinedDate, ay.SecondSemesterStartDate.Value, ay.SecondSemesterEndDate.Value))
                {
                    pdvm.SemesterName = Res.Get("Common_Label_SecondSemester");
                }
                else
                {
                    pdvm.SemesterName = "";
                }
                var lstPre = ListPraiseLevel.Where(o => o.key == pdvm.DisciplineLevel.ToString()).Select(o => o.value);
                if (lstPre != null && lstPre.Count() > 0)
                {
                    pdvm.LocationName = lstPre.FirstOrDefault();
                }

                pdvm.DisciplineResolution = pd.DisciplineType.Resolution;
                lsPDVM.Add(pdvm);
            }
            ViewData[PupilProfileConstants.LIST_PUPIL_DISCIPLINE] = lsPDVM;
            #endregion

            #region cac lop da hoc
            //NamTA Compare
            List<PupilOfClass> lsPoc = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() {
                                                                                        { "PupilID", PupilProfileID }
                                                            }).Distinct().OrderByDescending(o => o.AssignedDate).ThenByDescending(o => o.PupilOfClassID).ToList();

            List<PupilOfClassViewModel> lsPocVM = new List<PupilOfClassViewModel>();
            List<PupilOfClassViewModel> lsPocVMAll = new List<PupilOfClassViewModel>();
            //lay tung nam
            int academicYearID = 0;
            foreach (var itemForLDH in lsPoc)
            {
                PupilOfClassViewModel pocvm = new PupilOfClassViewModel();
                Utils.Utils.BindTo(itemForLDH, pocvm);
                pocvm.AcademicYearName = itemForLDH.AcademicYear.Year + " - " + (itemForLDH.AcademicYear.Year + 1);
                pocvm.Year = itemForLDH.AcademicYear.Year;
                pocvm.ClassName = itemForLDH.ClassProfile.DisplayName;

                //loai bo cac lop hoc sinh co cung nam hoc lay lop hoc som nhat
                if (academicYearID != itemForLDH.AcademicYearID)
                {
                    academicYearID = itemForLDH.AcademicYearID;
                    lsPocVM.Add(pocvm);
                }

                // Doi voi vi pham, nghi hoc, mien giam thi se lay tat ca
                lsPocVMAll.Add(pocvm);
            }
            ViewData[PupilProfileConstants.LIST_PUPIL_CLDH] = lsPocVM;
            ViewData[PupilProfileConstants.LIST_PUPIL_CLDH_VNM] = lsPocVMAll;
            #endregion cac lop da hoc

            //qua trinh hoc tap ren luyen
            #region qua trinh hoc tap ren luyen
            PupilOfClassViewModel pocvmFirst = lsPocVM.FirstOrDefault();
            if (pocvmFirst != null)
                GetDetailHLHKByAcademicYear(pocvmFirst.AcademicYearID, pocvmFirst.ClassID, PupilProfileID);
            #endregion qua trinh hoc tap ren luyen

            #region qua trinh len lop - Comment By AnhVD 19/6/2013 - Not use
            //List<PupilRankingViewModel> lsPRVMForQTLL = new List<PupilRankingViewModel>();
            //var lstPupilRanking = PupilRankingBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            //{
            //    {"PupilID",PupilProfileID}
            //}).Where(o => o.StudyingJudgementID != null).Where(o => lsEdu.Contains(o.EducationLevelID)).OrderByDescending(o => o.AcademicYearID);

            //var lsAcademicYearForQTLL = lstPupilRanking.Select(o => o.AcademicYearID).Distinct();
            //foreach (var item in lsAcademicYearForQTLL)
            //{
            //    PupilRankingViewModel prvmForQTLL = new PupilRankingViewModel();

            //    //lay ra pupilRanking co semester max
            //    int? maxSemester = lstPupilRanking.Where(o => o.AcademicYearID == item).Max(o => o.Semester);
            //    if (global.AppliedLevel.Value == 1)
            //    {
            //        if (maxSemester > 2)
            //        {
            //            maxSemester = 2;
            //        }
            //    }
            //    PupilRanking pr = lstPupilRanking.Where(o => o.AcademicYearID == item).Where(o => o.Semester == maxSemester).FirstOrDefault();
            //    Utils.Utils.BindTo(pr, prvmForQTLL);

            //    AcademicYear ayForQTLL = AcademicYearBusiness.Find(item);
            //    prvmForQTLL.AcademicYearName = string.Format("{0}-{1}", ayForQTLL.Year.ToString(), (ayForQTLL.Year + 1).ToString());
            //    ClassProfile cpForQTLL = ClassProfileBusiness.Find(pr.ClassID);
            //    prvmForQTLL.ClassName = cpForQTLL != null ? cpForQTLL.DisplayName : "";
            //    prvmForQTLL.StudyingJudgementName = pr.StudyingJudgement != null ? pr.StudyingJudgement.Resolution : "";

            //    lsPRVMForQTLL.Add(prvmForQTLL);
            //}
            //ViewData[PupilProfileConstants.LIST_PUPIL_QTLL] = lsPRVMForQTLL;
            #endregion qua trinh len lop

            ViewData[PupilProfileConstants.LIST_ACADEMICYEAR] = new SelectList(lsPocVM, "AcademicYearID", "AcademicYearName");

            #region Vi pham, nghi hoc, mien giam
            PupilOfClassViewModel pocvmAllFirst = lsPocVM.FirstOrDefault();
            if (pocvmAllFirst != null)
                GetDetailVio_Abs_ExSByAcademicYear(pocvmAllFirst.AcademicYearID, pocvmAllFirst.ClassID, PupilProfileID);
            #endregion

            #region Suc khoe dinh ky
            int FirstHealthPeriodID = 0;
            if (pocvmAllFirst != null)
            {
                List<HealthPeriod> lstHealthPeriod = getHealthPeriodFromAcademicYear(pocvmAllFirst.AcademicYearID).ToList();
                if (lstHealthPeriod != null && lstHealthPeriod.Count > 0)
                {
                    FirstHealthPeriodID = lstHealthPeriod.FirstOrDefault().HealthPeriodID;
                }
                ViewData[PupilProfileConstants.LIST_HEALTH_PERIODIC] = new SelectList(getHealthPeriodFromAcademicYear(pocvmAllFirst.AcademicYearID).ToList(), "HealthPeriodID", "Resolution");
                GetPupilHealthPeriodic(pocvmAllFirst.AcademicYearID, FirstHealthPeriodID, PupilProfileID);
            }
            #endregion

            // Tạo giá trị lưu log action_audit
            SetViewDataActionAudit(String.Empty, String.Empty, PupilProfileID.ToString(), "View pupil_profile_id:" + PupilProfileID, PupilProfileID.ToString(), "Hồ sơ học sinh", GlobalConstants.ACTION_VIEW, "Xem hồ sơ học sinh " + pp.FullName + " mã " + pp.PupilCode);

            if (typeOfActionResult == "view") return View("Detail");
            else return PartialView("DetailPartialView");
        }

        /// <summary>
        /// Lấy thông tin về Học lực, hạnh kiểm
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult GetDetailHLHKByAcademicYear(int AcademicYearID, int ClassID, int PupilProfileID)
        {
            CheckPermissionForAction(PupilProfileID, "PupilProfile", 3);
            //qua trinh hoc tap ren luyen
            List<PupilRankingViewModel> lsPRVMForHLHK = new List<PupilRankingViewModel>();
            List<int> lsEdu = _globalInfo.EducationLevels.Select(o => o.EducationLevelID).ToList();
            var iqPupilRankingByCapacity = VPupilRankingBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
            {
                {"PupilID", PupilProfileID},
                {"ClassID", ClassID},
                {"AcademicYearID", AcademicYearID}
            }).Where(o => o.CapacityLevelID != null || o.ConductLevelID != null).Where(o => lsEdu.Contains(o.EducationLevelID));

            var lstPupilRankingByCapacity = (from p in iqPupilRankingByCapacity
                                             join ca in CapacityLevelBusiness.All on p.CapacityLevelID equals ca.CapacityLevelID into g
                                             from j in g.DefaultIfEmpty()
                                             join co in ConductLevelBusiness.All on p.ConductLevelID equals co.ConductLevelID into g1
                                             from j1 in g1.DefaultIfEmpty()
                                             select new
                                             {
                                                 p.AcademicYearID,
                                                 p.ClassID,
                                                 p.AverageMark,
                                                 p.Semester,
                                                 CapRes = j.CapacityLevel1,
                                                 ConducRes = j1.Resolution
                                             }).OrderByDescending(o => o.AcademicYearID).ToList();

            var lsAcademicYear = lstPupilRankingByCapacity.Select(o => o.AcademicYearID).Distinct();

            foreach (var academicYear in lsAcademicYear)
            {
                PupilRankingViewModel prvm = new PupilRankingViewModel();
                var lsPupilRankingInAcademicYear = lstPupilRankingByCapacity.Where(o => o.AcademicYearID == academicYear).OrderBy(o => o.Semester).ToList(); ;
                int classID = lstPupilRankingByCapacity.Select(o => o.ClassID).FirstOrDefault();

                AcademicYear ay = AcademicYearBusiness.Find(academicYear);
                prvm.AcademicYearName = string.Format("{0}-{1}", ay.Year.ToString(), (ay.Year + 1).ToString());
                ClassProfile cp = ClassProfileBusiness.Find(classID);
                prvm.ClassName = cp.DisplayName;


                foreach (var item in lsPupilRankingInAcademicYear)
                {
                    Utils.Utils.BindTo(item, prvm);
                    if (item.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        prvm.AverageMarkHK1 = item.AverageMark.HasValue ? string.Format("{0:0.0}", item.AverageMark.Value) : "";
                        prvm.CapacityLevelHK1 = item.CapRes;
                        prvm.ConductLevelHK1 = item.ConducRes;
                    }
                    else if (item.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        prvm.AverageMarkHK2 = item.AverageMark.HasValue ? string.Format("{0:0.0}", item.AverageMark.Value) : "";
                        prvm.CapacityLevelHK2 = item.CapRes;
                        prvm.ConductLevelHK2 = item.ConducRes;

                        if (_globalInfo.AppliedLevel == 1)
                        {
                            prvm.AverageMarkCN = item.AverageMark.HasValue ? string.Format("{0:0.0}", item.AverageMark.Value) : "";
                            prvm.CapacityLevelCN = item.CapRes;
                            prvm.ConductLevelCN = item.ConducRes;
                        }
                    }
                    else
                    {
                        prvm.AverageMarkCN = item.AverageMark.HasValue ? string.Format("{0:0.0}", item.AverageMark.Value) : "";
                        prvm.CapacityLevelCN = item.CapRes;
                        prvm.ConductLevelCN = item.ConducRes;
                        if (_globalInfo.AppliedLevel == 1)
                        {
                            // Neu la cap 1 thi ky 2 se lay la diem thi lai neu co thi lai
                            prvm.AverageMarkHK2 = item.AverageMark.HasValue ? string.Format("{0:0.0}", item.AverageMark.Value) : "";
                            prvm.CapacityLevelHK2 = item.CapRes;
                            prvm.ConductLevelHK2 = item.ConducRes;
                        }
                    }
                }
                lsPRVMForHLHK.Add(prvm);
            }
            ViewData[PupilProfileConstants.LIST_PUPIL_HLHK] = lsPRVMForHLHK.Where(o => o.AcademicYearID == AcademicYearID);
            GetDetailTranscriptBySemester(AcademicYearID, ClassID, PupilProfileID, _globalInfo.Semester ?? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST);

            //end qua trinh hoc tap ren luyen
            return PartialView("_DetailHistoryLearn");
        }

        /// <summary>
        /// Lấy thông tin bảng điểm - AnhVD 19/06/2013
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilProfileID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>

        public PartialViewResult GetDetailTranscriptBySemester(int AcademicYearID, int ClassID, int PupilProfileID, int Semester)
        {
            CheckPermissionForAction(PupilProfileID, "PupilProfile", 3);
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            //Danh sach mon hoc cua lop
            List<ClassSubjectBO> lstSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>(){
                                                                                                    {"AcademicYearID", AcademicYearID},
                                                                                                    {"Semester", Semester},
                                                                                                    {"SchooID", academicYear.SchoolID}
                                                                                                })
                                                                                                .Select(o => new ClassSubjectBO
                                                                                                {
                                                                                                    SubjectID = o.SubjectID,
                                                                                                    ClassID = o.ClassID,
                                                                                                    DisplayName = o.SubjectCat.DisplayName,
                                                                                                    IsCommenting = o.IsCommenting,
                                                                                                    OrderInSubject = o.SubjectCat.OrderInSubject
                                                                                                })
                                                                        .OrderBy(u => u.OrderInSubject).ToList();
            PupilTranscriptModel ppTranscript;
            StringBuilder markTMP;
            int CntM = 0;
            int CntP = 0;
            int CntV = 0;

            List<PupilTranscriptModel> lstPupilTranscript = new List<PupilTranscriptModel>();

            //lay thong tin bang diem
            var lstMarkRecord = VMarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                                                                            {
                                                                                {"PupilID", PupilProfileID},
                                                                                {"ClassID", ClassID},
                                                                                {"AcademicYearID", AcademicYearID},
                                                                                {"Semester", Semester},
                                                                                {"Year", academicYear.Year}
                                                                            }).ToList();
            var lstJudgeRecord = VJudgeRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                                                                            {
                                                                                {"PupilID", PupilProfileID},
                                                                                {"ClassID", ClassID},
                                                                                {"AcademicYearID", AcademicYearID},
                                                                                {"Semester", Semester},
                                                                                {"Year", academicYear.Year}
                                                                            }).ToList();
            var lstSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                                                                            {
                                                                                {"PupilID", PupilProfileID},
                                                                                {"ClassID", ClassID},
                                                                                {"AcademicYearID", AcademicYearID}
                                                                            }).ToList();

            foreach (var subject in lstSubject)
            {
                ppTranscript = new PupilTranscriptModel();
                ppTranscript.Mon = subject.DisplayName;

                #region Mon tinh diem
                if (subject.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK || subject.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                {
                    // Diem mieng
                    List<decimal> lstDiemMieng = lstMarkRecord.Where(o => o.SubjectID == subject.SubjectID && o.Title.ToUpper().Contains(GlobalConstants.MARK_TYPE_M)).OrderBy(o => o.Title).Select(o => o.Mark).ToList();
                    if (lstDiemMieng.Count > CntM)
                        CntM = lstDiemMieng.Count;
                    markTMP = new StringBuilder();
                    foreach (var diem in lstDiemMieng)
                        markTMP.Append(string.Format("{0:0}", diem)).Append(" ");
                    ppTranscript.DiemMieng = markTMP.ToString();

                    // Diem 15 phut
                    List<decimal> lstDiem15 = lstMarkRecord.Where(o => o.SubjectID == subject.SubjectID && o.Title.ToUpper().Contains(GlobalConstants.MARK_TYPE_P)).OrderBy(o => o.Title).Select(o => o.Mark).ToList();
                    if (lstDiem15.Count > CntP)
                        CntP = lstDiem15.Count;
                    markTMP = new StringBuilder();
                    foreach (var diem in lstDiem15)
                        markTMP.Append(string.Format("{0:0}", diem)).Append(" ");
                    ppTranscript.Diem15Phut = markTMP.ToString();

                    // Diem 1 tiet
                    List<decimal> lstDiem1Tiet = lstMarkRecord.Where(o => o.SubjectID == subject.SubjectID && o.Title.ToUpper().Contains(GlobalConstants.MARK_TYPE_V)).OrderBy(o => o.Title).Select(o => o.Mark).ToList();
                    if (lstDiem1Tiet.Count > CntV)
                        CntV = lstDiem1Tiet.Count;
                    markTMP = new StringBuilder();
                    foreach (var diem in lstDiem1Tiet)
                        markTMP.Append(string.Format("{0:0.0}", diem)).Append(" ");
                    ppTranscript.Diem1Tiet = markTMP.ToString();

                    // Diem cuoi ky
                    List<decimal> lstDiemCuoiKy = lstMarkRecord.Where(o => o.SubjectID == subject.SubjectID && o.Title.ToUpper().Contains(GlobalConstants.MARK_TYPE_HK)).Select(o => o.Mark).ToList();
                    markTMP = new StringBuilder();
                    foreach (var diem in lstDiemCuoiKy)
                        markTMP.Append(string.Format("{0:0.0}", diem)).Append(" ");
                    ppTranscript.DiemCuoiKy = markTMP.ToString();
                }
                #endregion

                #region Mon nhan xet

                if (subject.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                {
                    // Diem mieng
                    List<string> lstDiemMieng = lstJudgeRecord.Where(o => o.SubjectID == subject.SubjectID && o.Title.ToUpper().Contains(GlobalConstants.MARK_TYPE_M)).OrderBy(o => o.Title).Select(o => o.Judgement).ToList();
                    markTMP = new StringBuilder();
                    if (lstDiemMieng.Count > CntM)
                        CntM = lstDiemMieng.Count;
                    foreach (var diem in lstDiemMieng)
                        markTMP.Append(string.Format("{0:0}", diem)).Append(" ");
                    ppTranscript.DiemMieng = markTMP.ToString();

                    // Diem 15 phut
                    List<string> lstDiem15 = lstJudgeRecord.Where(o => o.SubjectID == subject.SubjectID && o.Title.ToUpper().Contains(GlobalConstants.MARK_TYPE_P)).OrderBy(o => o.Title).Select(o => o.Judgement).ToList();
                    if (lstDiem15.Count > CntP)
                        CntP = lstDiem15.Count;
                    markTMP = new StringBuilder();
                    foreach (var diem in lstDiem15)
                        markTMP.Append(string.Format("{0:0}", diem)).Append(" ");
                    ppTranscript.Diem15Phut = markTMP.ToString();

                    // Diem 1 tiet
                    List<string> lstDiem1Tiet = lstJudgeRecord.Where(o => o.SubjectID == subject.SubjectID && o.Title.ToUpper().Contains(GlobalConstants.MARK_TYPE_V)).OrderBy(o => o.Title).Select(o => o.Judgement).ToList();
                    if (lstDiem1Tiet.Count > CntV)
                        CntV = lstDiem1Tiet.Count;
                    markTMP = new StringBuilder();
                    foreach (var diem in lstDiem1Tiet)
                        markTMP.Append(string.Format("{0:0.0}", diem)).Append(" ");
                    ppTranscript.Diem1Tiet = markTMP.ToString();

                    // Diem cuoi ky
                    List<string> lstDiemCuoiKy = lstJudgeRecord.Where(o => o.SubjectID == subject.SubjectID && o.Title.ToUpper().Contains(GlobalConstants.MARK_TYPE_HK)).Select(o => o.Judgement).ToList();
                    markTMP = new StringBuilder();
                    foreach (var diem in lstDiemCuoiKy)
                        markTMP.Append(string.Format("{0:0.0}", diem)).Append(" ");
                    ppTranscript.DiemCuoiKy = markTMP.ToString();
                }
                #endregion

                #region diem trung binh
                VSummedUpRecord surHKI = lstSummedUpRecord.Where(o => o.SubjectID == subject.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !o.PeriodID.HasValue).FirstOrDefault();
                if (surHKI != null)
                {
                    if (surHKI.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        ppTranscript.DiemTBHK1 = string.Format("{0:0.0}", surHKI.SummedUpMark);
                    }
                    else if (surHKI.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        ppTranscript.DiemTBHK1 = string.Format("{0:0.0}", surHKI.JudgementResult);
                    }
                }

                // TB HKII
                VSummedUpRecord surHKII = lstSummedUpRecord.Where(o => o.SubjectID == subject.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !o.PeriodID.HasValue).FirstOrDefault();
                if (surHKII != null)
                {
                    if (surHKII.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        ppTranscript.DiemTBHK2 = string.Format("{0:0.0}", surHKII.SummedUpMark);
                    }
                    else if (surHKII.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        ppTranscript.DiemTBHK2 = string.Format("{0:0.0}", surHKII.JudgementResult);
                    }
                }

                // TB CN
                var surTBCNIII = lstSummedUpRecord.Where(o => o.SubjectID == subject.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                var surTBCNRetest = lstSummedUpRecord.Where(o => o.SubjectID == subject.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_RETEST).FirstOrDefault();
                VSummedUpRecord surTBCN = surTBCNRetest ?? surTBCNIII;
                if (surTBCN != null)
                {
                    if (surTBCN.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        ppTranscript.DiemTBCN = string.Format("{0:0.0}", surTBCN.ReTestMark ?? surTBCN.SummedUpMark);
                    }
                    else if (surTBCN.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        ppTranscript.DiemTBCN = string.Format("{0:0.0}", surTBCN.ReTestJudgement ?? surTBCN.JudgementResult);
                    }
                }
                #endregion

                lstPupilTranscript.Add(ppTranscript);
            }

            ViewData["PupilProfile_DetailTranscript_Semester"] = Semester;
            ViewData["CountNumberMark"] = new int[] { CntM, CntP, CntV };
            ViewData[PupilProfileConstants.LIST_PUPIL_TRANSCRIPT] = lstPupilTranscript;
            // end qua trinh hoc tap ren luyen
            return PartialView("_DetailTranscript");
        }

        /// <summary>
        /// Lấy thông tin về Vi phạm, nghỉ học, miễn giảm - AnhVD 19/06/2013
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>

        public PartialViewResult GetDetailVio_Abs_ExSByAcademicYear(int AcademicYearID, int ClassID, int PupilProfileID)
        {
            CheckPermissionForAction(PupilProfileID, "PupilProfile", 3);
            #region Vi pham, nghi hoc, mien giam
            // Vi pham
            IEnumerable<PupilFault> iqPupilFault = PupilFaultBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() {
                                                                                        { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                                                                                        { "PupilID", PupilProfileID },
                                                                                        { "ClassID", ClassID }
                                                            }).OrderByDescending(o => o.ViolatedDate).ToList();
            List<PupilViolationViewModel> lstPPViolationViewModel = new List<PupilViolationViewModel>();
            foreach (var itemViolation in iqPupilFault)
            {
                PupilViolationViewModel ppvvm = new PupilViolationViewModel();
                Utils.Utils.BindTo(itemViolation, ppvvm);
                ppvvm.FaultName = itemViolation.FaultCriteria.Resolution;
                lstPPViolationViewModel.Add(ppvvm);
            }
            ViewData[PupilProfileConstants.LIST_PUPIL_VIOLATION] = lstPPViolationViewModel.Where(o => o.AcademicYearID == AcademicYearID);

            // Nghi hoc
            IEnumerable<PupilAbsence> iqPupilAbsence = PupilAbsenceBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() {
                                                                                        { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                                                                                        { "PupilID", PupilProfileID },
                                                                                        { "ClassID", ClassID }
                                                            }).OrderByDescending(o => o.AbsentDate).ToList();
            List<PupilAbsenceViewModel> lstPPAbsentViewModel = new List<PupilAbsenceViewModel>();
            foreach (var itemAbsent in iqPupilAbsence)
            {
                PupilAbsenceViewModel ppAbsentvm = new PupilAbsenceViewModel();
                if (lstPPAbsentViewModel.Any(u => u.AbsentDate == itemAbsent.AbsentDate))
                    continue;

                var listAbseceDay = iqPupilAbsence.Where(u => u.AbsentDate == itemAbsent.AbsentDate);

                Utils.Utils.BindTo(itemAbsent, ppAbsentvm);

                foreach (var itemChild in listAbseceDay)
                {
                    if (itemChild.Section == Constants.GlobalConstants.CALENDAR_SECTION_MORNING)
                    {
                        ppAbsentvm.SectionName = Res.Get("ClassProfile_Control_Separate_Morning");
                        ppAbsentvm.Morning = itemChild.IsAccepted.HasValue ? (itemChild.IsAccepted.Value ? "P" : "K") : "K";
                    }
                    else if (itemChild.Section == Constants.GlobalConstants.CALENDAR_SECTION_AFTERNOON)
                    {
                        ppAbsentvm.SectionName = Res.Get("ClassProfile_Control_Separate_Afternoon");
                        ppAbsentvm.Afternoon = itemChild.IsAccepted.HasValue ? (itemChild.IsAccepted.Value ? "P" : "K") : "K";
                    }
                    else if (itemChild.Section == Constants.GlobalConstants.CALENDAR_SECTION_EVENING)
                    {
                        ppAbsentvm.SectionName = Res.Get("ClassProfile_Control_Separate_Night");
                        ppAbsentvm.Evening = itemChild.IsAccepted.HasValue ? (itemChild.IsAccepted.Value ? "P" : "K") : "K";
                    }
                }
                lstPPAbsentViewModel.Add(ppAbsentvm);
            }
            ViewData[PupilProfileConstants.HAS_MORNING] = true;
            ViewData[PupilProfileConstants.HAS_AFTERNOON] = true;
            ViewData[PupilProfileConstants.HAS_EVENING] = true;
            ViewData[PupilProfileConstants.LIST_PUPIL_ABSENCE] = lstPPAbsentViewModel.Where(o => o.AcademicYearID == AcademicYearID);

            // Mien giam
            IQueryable<ExemptedSubject> iqExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() {
                                                                                        { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                                                                                        { "PupilID", PupilProfileID },
                                                                                        { "ClassID", ClassID }
                                                            });
            List<SubjectRemissionViewModel> lstExemptedSubjectViewModel = new List<SubjectRemissionViewModel>();
            foreach (var itemExemptedSubj in iqExemptedSubject)
            {
                SubjectRemissionViewModel ExemptedSubj = new SubjectRemissionViewModel();
                Utils.Utils.BindTo(itemExemptedSubj, ExemptedSubj);
                ExemptedSubj.SubjectRemission = itemExemptedSubj.SubjectCat.DisplayName;
                if (itemExemptedSubj.SecondSemesterExemptType.HasValue)
                {
                    ExemptedSubj.SemesterName = Res.Get("Common_Label_AllYear");
                }
                else if (itemExemptedSubj.FirstSemesterExemptType.HasValue)
                {
                    ExemptedSubj.SemesterName = Res.Get("Common_Label_Combo_FirstSemester");
                }
                lstExemptedSubjectViewModel.Add(ExemptedSubj);
            }
            ViewData[PupilProfileConstants.LIST_EXEMPTED_SUBJECT] = lstExemptedSubjectViewModel.Where(o => o.AcademicYearID == AcademicYearID);
            #endregion
            return PartialView("_DetailViolationAbsenceRemission");
        }

        /// <summary>
        /// lấy thông tin khám sức khỏe định kỳ
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="HealthPeriodID"></param>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>

        public PartialViewResult GetPupilHealthPeriodic(int AcademicYearID, int HealthPeriodID, int PupilProfileID)
        {
            CheckPermissionForAction(PupilProfileID, "PupilProfile", 3);
            IQueryable<MonitoringBook> iqMonitoringBook = MonitoringBookBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() {
                                                                                        { "AcademicYearID", AcademicYearID },
                                                                                        { "PupilID", PupilProfileID },
                                                                                        { "MonitoringType", GlobalConstants.MONITORINGBOOK_TYPE_PERIOD },
                                                                                        { "HealthPeriodID", HealthPeriodID }
                                                            }).OrderByDescending(o => o.MonitoringDate);
            PupilHealthPeriodicViewModel model = new PupilHealthPeriodicViewModel();
            if (iqMonitoringBook.Count() > 0)
            {
                MonitoringBook monitoringBook = iqMonitoringBook.FirstOrDefault();

                if (monitoringBook != null)
                {
                    model = new PupilHealthPeriodicViewModel();
                    model.PupilID = monitoringBook.PupilID;
                    model.ClassID = monitoringBook.ClassID;
                    model.BirthDate = monitoringBook.PupilProfile.BirthDate.ToString("dd/MM/yyyy");
                    model.FullName = monitoringBook.PupilProfile.FullName;
                    model.ClassName = monitoringBook.ClassProfile.DisplayName;
                    model.GenreName = CommonList.GenreAndSelect().Where(u => u.key.Equals(monitoringBook.PupilProfile.Genre.ToString())).FirstOrDefault().value;
                    model.DentalTest = monitoringBook.DentalTests.FirstOrDefault();
                    model.ENTTest = monitoringBook.ENTTests.FirstOrDefault();
                    model.EyeTest = monitoringBook.EyeTests.FirstOrDefault();
                    model.OverallTest = monitoringBook.OverallTests.FirstOrDefault();
                    model.PhysicalTest = monitoringBook.PhysicalTests.FirstOrDefault();
                    model.SpineTest = monitoringBook.SpineTests.FirstOrDefault();
                }
            }
            ViewData[PupilProfileConstants.DETAIL_HEALTH_PERIODIC_PUPIL] = model;

            return PartialView("_DetailPeriodicHealth");
        }

        #endregion redirect create,edit,detail

        #region test image


        [ValidateAntiForgeryToken]
        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        {
            CheckActionPermissionMinView();
            GlobalInfo global = new GlobalInfo();

            if (ImageUploader == null || ImageUploader.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = ImageUploader.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> imageExtension = new List<string>();
                imageExtension.Add(".JPG");
                imageExtension.Add(".JPEG");
                imageExtension.Add(".PNG");
                imageExtension.Add(".BMP");
                imageExtension.Add(".ICO");
                imageExtension.Add(".GIF");
                var extension = Path.GetExtension(file.FileName);
                if (!imageExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                    //return Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
                file.SaveAs(physicalPath);
                Session["ImagePath"] = physicalPath;

                string relativePath = "/Uploads/Photos/" + fileName;
                JsonResult res2 = Json(new JsonMessage(fileName));
                res2.ContentType = "text/plain";
                return res2;
            }

            // Return an empty string to signify success
            JsonResult res3 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res3.ContentType = "text/plain";
            return res3;
            //return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        #region action result

        //public ActionResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        //{
        //    GlobalInfo global = new GlobalInfo();

        //    if (ImageUploader == null || ImageUploader.Count() <= 0) return Content(Res.Get("Common_Label_ImageNullError"));

        //    // The Name of the Upload component is "attachments"
        //    var file = ImageUploader.FirstOrDefault();

        //    if (file != null)
        //    {
        //        //kiem tra file extension lan nua
        //        List<string> imageExtension = new List<string>();
        //        imageExtension.Add(".JPG");
        //        imageExtension.Add(".JPEG");
        //        imageExtension.Add(".PNG");
        //        imageExtension.Add(".BMP");
        //        imageExtension.Add(".ICO");
        //        imageExtension.Add(".GIF");
        //        var extension = Path.GetExtension(file.FileName);
        //        if (!imageExtension.Contains(extension.ToUpper()))
        //        {
        //            return Content(Res.Get("Common_Label_ImageExtensionError"));
        //        }

        //        // luu ra dia
        //        var fileName = Path.GetFileNameWithoutExtension(file.FileName);
        //        fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
        //        var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
        //        file.SaveAs(physicalPath);
        //        Session["ImagePath"] = physicalPath;

        //        string relativePath = "/Uploads/Photos/" + fileName;
        //        return Content(fileName);
        //    }

        //    // Return an empty string to signify success
        //    return Content("");
        //}

        #endregion action result


        [ValidateAntiForgeryToken]
        public ActionResult RemoveImage(string[] fileNames)
        {
            //// The parameter of the Remove action must be called "fileNames"
            //foreach (var fullName in fileNames)
            //{
            //    var fileName = Path.GetFileName(fullName);
            //    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

            //    // TODO: Verify user permissions
            //    if (System.IO.File.Exists(physicalPath))
            //    {
            //        // The files are not actually removed in this demo
            //        System.IO.File.Delete(physicalPath);
            //    }
            //}

            // Return an empty string to signify success
            return Content("");
        }

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }

        #endregion test image

        #region delete pupil

        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCheckPupil(int[] checkedDelete)
        {
            CheckActionPermissionMinView();
            // Tạo giá trị ghi log
            List<int> listID = checkedDelete.ToList();
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int schoolId = _globalInfo.SchoolID.Value;

            List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => listID.Contains(o.PupilProfileID)).ToList();


            //Luu thong tin phuc vu viec phuc hoi du lieu

            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = academicYearId,
                SCHOOL_ID = schoolId,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_PUPIL,
                SHORT_DESCRIPTION = "Hồ sơ học sinh, "
            };


            List<RESTORE_DATA_DETAIL> lstRestoreDetail = PupilProfileBusiness.DeleteListPupilProfile(_globalInfo.UserAccountID, listID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, objRes);
            PupilProfileBusiness.Save();

            // Tạo giá trị ghi log
            StringBuilder oldObject = new StringBuilder();
            StringBuilder objectID = new StringBuilder();
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            for (int i = 0, size = listPupilProfile.Count; i < size; i++)
            {
                oldObject.Append("{\"PupilProfileID\":\"" + listPupilProfile[i].PupilProfileID.ToString() + "\",\"PupilCode\":\"" + listPupilProfile[i].PupilCode + "\"}");
                objectID.Append(listPupilProfile[i].PupilProfileID.ToString());
                descriptionObject.Append("Delete pupil_profile_id:" + listPupilProfile[i].PupilProfileID.ToString());
                paramObject.Append(listPupilProfile[i].PupilProfileID.ToString());
                userFunctions.Append("Hồ sơ học sinh");
                userActions.Append(GlobalConstants.ACTION_DELETE);
                userDescriptions.Append("Xóa học sinh " + listPupilProfile[i].FullName + " mã " + listPupilProfile[i].PupilCode);
                if (i < size - 1)
                {
                    oldObject.Append(GlobalConstants.WILD_LOG);
                    descriptionObject.Append(GlobalConstants.WILD_LOG);
                    paramObject.Append(GlobalConstants.WILD_LOG);
                    objectID.Append(GlobalConstants.WILD_LOG);
                    userFunctions.Append(GlobalConstants.WILD_LOG);
                    userActions.Append(GlobalConstants.WILD_LOG);
                    userDescriptions.Append(GlobalConstants.WILD_LOG);
                }

                objRes.SHORT_DESCRIPTION += string.Format(" HS: {0}, mã: {1},", listPupilProfile[i].FullName, listPupilProfile[i].PupilCode);
            }

            if (lstRestoreDetail != null && lstRestoreDetail.Count > 0)
            {
                if (objRes.SHORT_DESCRIPTION.Length > 1000)
                {
                    objRes.SHORT_DESCRIPTION = objRes.SHORT_DESCRIPTION.Substring(0, 1000) + "...";
                }
                RestoreDataBusiness.Insert(objRes);
                RestoreDataBusiness.Save();
                RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
            }


            SetViewDataActionAudit(oldObject.ToString(), String.Empty, objectID.ToString(), descriptionObject.ToString(), paramObject.ToString(), userFunctions.ToString(), userActions.ToString(), userDescriptions.ToString());
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePupil(int pupilid)
        {
            CheckActionPermissionMinView();
            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            PupilProfileBusiness.DeletePupilProfile(_globalInfo.UserAccountID, pupilid, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, userInfo);
            //PupilProfileBusiness.Save();

            // Tạo dữ liệu ghi log 
            PupilProfile pupilProfile = PupilProfileBusiness.Find(pupilid);
            // Tạo giá trị lưu log action_audit
            SetViewDataActionAudit("{\"PupilProfileID\":\"" + pupilid + "\",\"PupilCode\":\"" + pupilProfile.PupilCode + "\"}", String.Empty, pupilid.ToString(), "Delete pupil_profile_id:" + pupilid, pupilid.ToString(), "Hồ sơ học sinh", GlobalConstants.ACTION_DELETE, "Xóa học sinh " + pupilProfile.FullName + " mã " + pupilProfile.PupilCode);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion delete pupil

        #region sort
        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        [ValidateAntiForgeryToken]
        public JsonResult OrderPupil(int ClassID, List<int> lstOrderedPupilID)
        {
            CheckActionPermissionMinView();
            if (ClassID <= 0)
                return Json(new JsonMessage(Res.Get("PupilProfile_Label_ClassIDRequiredToSort"), JsonMessage.ERROR));

            GlobalInfo global = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Check"] = "Check";

            List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).OrderBy(u => u.PupilProfile.Name).ToList();

            if (lstOrderedPupilID == null)
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));

            if (lstPOC.Count != lstOrderedPupilID.Count)
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));

            List<int> lstPupilID = lstPOC.Select(u => u.PupilID).ToList();
            List<int> lstOrder = lstPupilID.Select(u => lstOrderedPupilID.IndexOf(u) + 1).ToList();

            PupilOfClassBusiness.UpdateOrder(global.UserAccountID, ClassID, lstPupilID, lstOrder);
            PupilOfClassBusiness.Save();

            return Json(new JsonMessage(Res.Get("PupilProfile_Label_OrderSuccess")));
        }


        public PartialViewResult ListSortClass(int ClassID)
        {
            CheckActionPermissionMinView();
            if (ClassID <= 0)
                throw new BusinessException("PupilProfile_Label_ClassIDRequiredToSort");

            CheckPermissionForAction(ClassID, "ClassProfile");

            GlobalInfo global = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Check"] = "Check";

            var query = from poc in PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo)
                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID

                        select new PupilProfileViewModel
                        {
                            PupilProfileID = pp.PupilProfileID,
                            FullName = pp.FullName,
                            PupilCode = pp.PupilCode,
                            BirthDate = pp.BirthDate,
                            Genre = pp.Genre,
                            ProfileStatus = poc.Status,
                            OrderInClass = poc.OrderInClass,
                            Name = pp.Name,
                            EthnicID = pp.EthnicID,
                            EthnicName = pp.Ethnic != null ? pp.Ethnic.EthnicName : null,
                            EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null,
                        };

            List<PupilProfileViewModel> lst = query.ToList()
                .OrderBy(p => p.OrderInClass)
                .ThenBy(u => SMAS.Business.Common.Utils.SortABC(u.FullName.getOrderingName(u.EthnicCode)))
                .ToList();

            lst.ForEach(u =>
            {
                u.StatusName = CommonList.PupilStatus().SingleOrDefault(v => v.key == u.ProfileStatus.ToString()).value;
                u.GenreName = u.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female");
            });

            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            ViewData[PupilProfileConstants.IS_SHOW_AVATAR] = classProfile.AcademicYear.IsShowAvatar;
            ViewData[PupilProfileConstants.LIST_PUPILPROFILE] = lst;
            ViewData[PupilProfileConstants.CLASS_PROFILE_NAME] = classProfile.DisplayName;
            return PartialView("_ListSortClass", ClassID);
        }

        [SkipCheckRole]
        public ViewResult SortClass(int ClassID)
        {
            CheckActionPermissionMinView();
            CheckPermissionForAction(ClassID, "ClassProfile");
            if (ClassID <= 0)
                throw new BusinessException("PupilProfile_Label_ClassIDRequiredToSort");

            ListSortClass(ClassID);
            return View("SortClass");
        }


        [ValidateAntiForgeryToken]
        public JsonResult SortByAlphabet(int ClassID)
        {
            CheckActionPermissionMinView();
            CheckPermissionForAction(ClassID, "ClassProfile");
            if (ClassID <= 0)
                return Json(new JsonMessage(Res.Get("PupilProfile_Label_ClassIDRequiredToSort"), JsonMessage.ERROR));

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Check"] = "Check";

            /*var lstPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo)
                                                .Select(u => new { u.PupilID, u.PupilProfile.Name, NameSort = Utils.Utils.ConvertVN(u.Name), FullNameSort = Utils.Utils.ConvertVN(u.full) })
                                                .ToList()
                                                .OrderBy(u => Utils.Utils.ConvertVN(u.Name))
                                                .ToList();*/
            //Chiendd: 14/08/2014: Sua lai thuat toan sap xep tieng viet
            var lstPOC = (from pc in PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo)
                          join pf in PupilProfileBusiness.All on pc.PupilID equals pf.PupilProfileID
                          select new
                          {
                              PupilID = pc.PupilID,
                              Name = pf.Name,
                              FullName = pf.FullName,
                              EthnicCode = pf.Ethnic != null ? pf.Ethnic.EthnicCode : null
                          })
                          .ToList()
                          .OrderBy(u => SMAS.Business.Common.Utils.SortABC(u.FullName.getOrderingName(u.EthnicCode)))
                          .ToList();
            int orderNum = 1;
            List<int> lstPupilID = lstPOC.Select(u => u.PupilID).ToList();
            List<int> lstOrder = lstPupilID.Select(u => orderNum++).ToList();

            PupilOfClassBusiness.UpdateOrder(global.UserAccountID, ClassID, lstPupilID, lstOrder);
            PupilOfClassBusiness.Save();

            return Json(new JsonMessage(Res.Get("PupilProfile_Label_OrderSuccess"), JsonMessage.SUCCESS));
        }

        #endregion sort

        #region ho tro

        public bool IsDateInsideDateRange(DateTime dateToCheck, DateTime fromDate, DateTime toDate)
        {
            if (dateToCheck >= fromDate && dateToCheck <= toDate)
                return true;
            else
                return false;
        }

        private string GetSemesterExemptTypeName(int? semesterExemptType, List<ComboObject> lst)
        {
            if (!semesterExemptType.HasValue) return string.Empty;
            var value = lst.Where(u => u.key.Equals(semesterExemptType.Value.ToString())).FirstOrDefault();
            return value != null ? value.value : string.Empty;
        }

        private string GetSemesterName(int? semester)
        {
            if (!semester.HasValue) return string.Empty;

            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                return Res.Get("Semester_Of_Year_First");
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                return Res.Get("Semester_Of_Year_Second");
            return string.Empty;
        }

        private string GetLocationName(int praiseLevel)
        {
            ComboObject cbObj = CommonList.DisciplineLevel().SingleOrDefault(u => u.value == praiseLevel.ToString());
            return cbObj != null ? cbObj.key : string.Empty;
        }

        private int GetSemesterChecked(AcademicYear objAcademicYear)
        {
            int SemesterChecked = 0;
            //AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime DateTimeNow = DateTime.Now;
            if (DateTimeNow.Date <= objAcademicYear.SecondSemesterEndDate.Value.Date)
            {
                if ((objAcademicYear.FirstSemesterStartDate.Value.Date <= DateTimeNow.Date && DateTimeNow.Date <= objAcademicYear.FirstSemesterEndDate.Value.Date)
                    || objAcademicYear.FirstSemesterEndDate.Value.Date <= DateTimeNow.Date && DateTimeNow.Date < objAcademicYear.SecondSemesterStartDate.Value.Date)
                {
                    SemesterChecked = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                }
                else
                {
                    SemesterChecked = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                }
            }
            else
            {
                SemesterChecked = SystemParamsInFile.SEMESTER_IN_SUMMER;
            }
            return SemesterChecked;
        }
        #endregion ho tro

        #region bao luu
        private List<int> GetListMonthFirstSemester(int academicYearID)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
            DateTime FDate = objAcademicYear.FirstSemesterStartDate.Value;
            DateTime EDate = objAcademicYear.FirstSemesterEndDate.Value;

            FDate = new DateTime(FDate.Date.Year, FDate.Date.Month, 1);
            EDate = new DateTime(EDate.Date.Year, EDate.Date.Month, 1);

            List<int> lstMonth = new List<int>();
            int objMonth;
            while (FDate <= EDate)
            {

                objMonth = Int32.Parse(FDate.Date.Year.ToString() + FDate.Date.Month.ToString());

                lstMonth.Add(objMonth);
                FDate = FDate.AddMonths(1);
            }

            lstMonth.Add(15);
            return lstMonth;
        }
        #endregion

        #region Excel

        public PartialViewResult PatialViewImportExcel()
        {
            SetViewDataPermission("ImportPupil", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData["LIST_EDUCATION_LEVEL_IMPORT"] = new SelectList(_globalInfo.EducationLevels.ToList(), "EducationLevelID", "Resolution");
            return PartialView("_FormImportExcel");
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int? EducationLevelID, FormCollection frm)
        {
            try
            {
                EducationLevelID = EducationLevelID.HasValue ? EducationLevelID : 0;

                bool isAddPupil = "on".Equals(frm["rdoAddPupil"]);
                bool isUpdatePupil = "on".Equals(frm["rdoUpdatePupil"]);
                HttpPostedFileBase file = Request.Files.Count > 0 ? Request.Files[0] : null;

                if (file == null)
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), "notSuccess"));
                    return res;
                }

                if (!file.FileName.EndsWith(".xls") && !file.FileName.EndsWith(".xlsx"))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExtensionError"), "notSuccess"));
                    return res;
                }
                int maxSize = 3145728;
                if (file.ContentLength > maxSize)
                {
                    JsonResult res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "3 MB"), "notSuccess"));
                    return res;
                }

                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                IVTWorksheet sheet = book.GetSheet(1);
                string SchoolName = sheet.GetCellValue(2, VTVector.dic['A']) != null ? (string)sheet.GetCellValue(2, VTVector.dic['A']) : "";
                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                if (school.SchoolName.Trim().ToUpper() != SchoolName.Trim().ToUpper())
                {
                    JsonResult res = Json(new JsonMessage("Tên trường trong file excel không đúng với tên trường trong hệ thống", "notSuccess"));
                    return res;
                }
                List<Village> lstVillageImport = new List<Village>();

                List<ImportPupilViewModel> lstPupilImported = this.ReadExcelFile(sheet, EducationLevelID.Value, isAddPupil, isUpdatePupil, ref lstVillageImport);
                Session["LIST_IMPORTED_PUPIL"] = lstPupilImported;
                Session["lstVillageImport"] = lstVillageImport;

                if (lstPupilImported.Count == 0)
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("ImportPupil_Label_NoPupilImported"), "notSuccess"));
                    return res;
                }

                int countAllPupil = lstPupilImported.Count;
                int countPupilValid = lstPupilImported.Count(p => p.IsValid);
                string retMsg;
                if (countAllPupil > 0 && countPupilValid > 0 && countAllPupil == countPupilValid)
                {
                    SaveData(isAddPupil);
                    retMsg = String.Format("{0} {1}/{2} học sinh", Res.Get("Common_Label_ImportSuccessMessage"), countPupilValid, countAllPupil);
                    return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS), "text/html");
                }
                JsonResult resx = Json(new JsonMessage(Res.Get("ImportPupil_Label_ImportFailure"), JsonMessage.ERROR), "text/html");
                return resx;
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "ImportExcel", "EducationLevelID=" + EducationLevelID, ex);
                JsonResult res = Json(new JsonMessage("Không đọc được file excel", "Permisstion"), "text/html");
                return res;
            }
        }

        private void SaveData(bool IsAddPupil)
        {
            List<ImportPupilViewModel> listPupil = (List<ImportPupilViewModel>)Session["LIST_IMPORTED_PUPIL"];
            if (listPupil == null)
            {
                return;
            }
            listPupil = listPupil.Where(p => p.IsValid).ToList();
            List<Village> lstVillageImport = (List<Village>)Session["lstVillageImport"];
            if (listPupil == null || !listPupil.Any())
            {
                return;
            }

            List<string> lstPupilCode = listPupil.Select(o => o.PupilCode).ToList();
            List<PupilProfile> lstPupilUpdatePP = null;
            List<string> listPupilCodeUpdate = new List<string>();
            List<ImportPupilViewModel> lstPupilUpdate = null;

            if (!IsAddPupil)
            {
                lstPupilUpdatePP = PupilProfileBusiness.All
                                                   .Where(o => o.CurrentSchoolID == _globalInfo.SchoolID.Value
                                                       && o.IsActive
                                                       && lstPupilCode.Contains(o.PupilCode)).ToList();

                listPupilCodeUpdate = lstPupilUpdatePP.Select(o => o.PupilCode).ToList();
                lstPupilUpdate = listPupil.Where(o => listPupilCodeUpdate.Any(t => t.ToLower().Equals(o.PupilCode.ToLower()))).ToList();
            }

            //Lay danh sach lop
            int academicYearId = _globalInfo.AcademicYearID.Value;
            List<int> lstClassID = listPupil.Select(o => o.CurrentClassID).Distinct().ToList();
            List<PupilProfile> lstPupilInsertPP = new List<PupilProfile>();
            List<PupilProfile> lstPupilUpdateAll = new List<PupilProfile>();
            List<int> lstEnrollmentTypeInsert = new List<int>();
            List<int> lstEnrollmentTypeUpdate = new List<int>();
            List<int> lstOrderInsert = new List<int>();
            List<int> lstOrderUpdate = new List<int>();
            List<int> lstClassIDUpdate = new List<int>();
            List<ImportPupilViewModel> lstPupilUpdateClass = new List<ImportPupilViewModel>();
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = academicYearId;
            if (lstVillageImport != null && lstVillageImport.Count > 0)
            {
                for (int i = 0; i < lstVillageImport.Count; i++)
                {
                    VillageBusiness.Insert(lstVillageImport[i]);
                }
                VillageBusiness.Save();
            }

            List<string> lstVilliageName = listPupil.Where(p => p.VillageName != "").Select(p => p.VillageName).Distinct().ToList();
            List<Village> lstvil = new List<Village>();
            if (lstVilliageName.Count > 0)
            {
                lstvil = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Where(p => lstVilliageName.Contains(p.VillageName)).ToList();
            }

            if (IsAddPupil)
            {
                foreach (ImportPupilViewModel pupil in listPupil)
                {

                    var pupilProfile = new PupilProfile
                    {
                        CurrentAcademicYearID = academicYearId,
                        IsActive = true,
                        CreatedDate = DateTime.Now
                    };
                    if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                    {
                        Village vil = lstvil.FirstOrDefault(p => p.VillageName.Equals(pupil.VillageName) && p.CommuneID == pupil.CommuneID);
                        if (vil != null)
                        {
                            pupil.VillageID = vil.VillageID;
                        }
                    }
                    else
                    {
                        pupil.VillageID = null;
                    }

                    CopyValue(pupilProfile, pupil);
                    lstPupilInsertPP.Add(pupilProfile);
                    lstEnrollmentTypeInsert.Add(pupil.EnrolmentType);
                    lstOrderInsert.Add(pupil.Index);
                }
            }
            else
            {
                //lstPupilUpdateClass = lstPupilUpdate.Where(o => o.CurrentClassID == ClassID).ToList();
                foreach (ImportPupilViewModel pupil in lstPupilUpdate)
                {
                    PupilProfile pupilProfile = lstPupilUpdatePP.FirstOrDefault(o => o.PupilCode == pupil.PupilCode);
                    if (pupilProfile != null)
                    {
                        int currentClassID = pupilProfile.CurrentClassID;
                        pupilProfile.CurrentAcademicYearID = academicYearId;
                        pupilProfile.IsActive = true;
                        if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                        {
                            Village vil = lstvil.FirstOrDefault(p => p.VillageName.Equals(pupil.VillageName) && p.CommuneID == pupil.CommuneID);
                            if (vil != null)
                            {
                                pupil.VillageID = vil.VillageID;
                            }
                        }
                        else
                        {
                            pupil.VillageID = null;

                        }
                        CopyValue(pupilProfile, pupil);
                        lstPupilUpdateAll.Add(pupilProfile);
                        lstEnrollmentTypeUpdate.Add(pupil.EnrolmentType);
                        lstClassIDUpdate.Add(currentClassID);
                    }
                    lstOrderUpdate.Add(pupil.Index);
                }
            }

            if (lstPupilInsertPP.Count > 0)
            {
                PupilProfileBusiness.ImportPupilProfile(_globalInfo.UserAccountID, lstPupilInsertPP, lstEnrollmentTypeInsert, lstOrderInsert, lstClassID);
            }

            if (lstPupilUpdateAll.Count > 0)
            {
                PupilProfileBusiness.UpdateImportPupilProfile(_globalInfo.UserAccountID, lstPupilUpdateAll, lstEnrollmentTypeUpdate, lstOrderUpdate, lstClassIDUpdate, lstClassID);
            }
        }

        private List<ImportPupilViewModel> ReadExcelFile(IVTWorksheet sheet, int educationLevelID, bool isAddPupil, bool isUpdatePupil, ref List<Village> lstVillageImport)
        {
            Session["EducationLevelID"] = educationLevelID;
            List<ImportPupilViewModel> listPupil = new List<ImportPupilViewModel>();
            List<ImportPupilViewModel> listPupilTemp = new List<ImportPupilViewModel>();
            // Congnv: Mẫu cũ
            //int rowIndex = 10;
            int rowIndex = 5;

            #region // const
            // Congnv: Thêm các tên cột trong bảng Excel (Hỗ trợ sửa đổi mẫu sau này)
            //DucPT1: sua bieu mau 08/2016 bieu mau: hs_import_thongtinhocsinh_2016
            const string indexCol = "A";      // STT
            const string pupilCodeCol = "B";  // Mã học sinh
            const string fullNameCol = "C";   // Tên học sinh
            const string birthDateCol = "D"; // Ngày sinh
            const string genreNameCol = "E"; // Giới tính
            const string classNameCol = "F"; // Lớp
            const string enrolmentTypeNameCol = "G"; // Hình thức trúng tuyển
            const string enrolmentDateCol = "H"; // Ngày vào trường
            const string profileStatusNameCol = "I"; // trạng thái học sinh
            const string provinceNameCol = "J"; //  Tỉnh thành
            const string districtNameCol = "K"; // Quận huyện
            const string communeNameCol = "L"; // Xã phường
            const string villageNameCol = "M"; // Thôn xóm
            const string StorageNumberCol = "N"; // số đăng bộ
            const string birthPlaceCol = "O"; // nơi sinh
            const string homeTownCol = "P"; // quê quán
            const string permanentResidentalAddressCol = "Q"; // địa chỉ thường trú
            const string tempResidentalAddressCol = "R";  // địa chỉ tạm trú
            const string foreignLanguageTrainingNameCol = "S"; // hệ học ngoại ngữ
            const string LearningTypeNameCol = "T"; // Hình thức học

            string youngPioneerJoinedDate = string.Empty; // Ngày vào đội
            string youngLeagueJoinedDate = string.Empty; // Ngày vào đoàn
            string communistPartyJoinedDate = string.Empty; // Ngày vào đảng

            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                youngPioneerJoinedDate = "U";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                youngPioneerJoinedDate = "U";
                youngLeagueJoinedDate = "V";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                youngPioneerJoinedDate = "U";
                youngLeagueJoinedDate = "V";
                communistPartyJoinedDate = "W";
            }

            const string ethnicNameCol = "X"; // Dân tộc
            const string religionNameCol = "Y"; // Tôn giáo
            const string policyTargetNameCol = "Z"; // đối tượng chính sách
            const string policyRegimeCol = "AA"; // Chế độ chính sách
            const string isSupportForLearningCol = "AB"; //  Được hỗ trợ kinh phí học tập
            const string isResettlementTargetCol = "AC"; // Thuộc diện tái định cư
            const string areaNameCol = "AD"; // Khu vực
            const string classTypeCol = "AE"; // Diện học sinh
            const string disabilityTypeCol = "AF"; // Loại khuyết tật
            const string mobileCol = "AG"; // sdt di động
            const string identifyNumberCol = "AH"; // CMND
            const string emailCol = "AI"; // Email
            const string bloodTypeNameCol = "AJ"; // nhóm máu
            const string IsUsedMoetProgram = "AK"; // học chương trình giá dục của Bộ
            const string IsSwimmingCol = "AL"; // Biết bơi
            const string fatherFullNameCol = "AM"; // Tên cha
            const string fatherBirthDateCol = "AN"; // Năm sinh của cha 
            const string fatherJobCol = "AO"; // Nghề nghiệp của cha
            const string fatherMobileCol = "AP"; // SDt của cha
            const string fatherEmailCol = "AQ"; // Email của cha
            const string motherFullNameCol = "AR"; // Tên mẹ
            const string motherBirthDateStrCol = "AS"; // Năm sinh của mẹ
            const string motherJobCol = "AT"; // Nghề nghiệp của mẹ
            const string motherMobileCol = "AU"; // SDT của mẹ
            const string motherEmailCol = "AV"; // Email của mẹ
            const string sponsorFullNameCol = "AW"; // Tên người bảo hộ
            const string sponsorBirthDateStrCol = "AX"; // Ngày sinh người bảo hộ
            const string sponsorJobCol = "AY"; // Nghề nghiepj người bảo hộ
            const string sponsorMobilCol = "AZ"; // SDT người bảo hộ
            const string sponsorEmailCol = "BA"; // Email người bảo hộ
            const string isMinorityFatherCol = "BB"; // bố là dân tộc
            const string isMinorityMotherCol = "BC"; //Mẹ là dân tộc
            const string pupilCodeNew = "BD"; // Mã học sinh mới
            #endregion

            #region // get data
            // Khu vực
            var lstArea = AreaBusiness.All.Where(x => x.IsActive.HasValue && x.IsActive.Value == true)
                                                        .Select(x => new { x.AreaID, x.AreaName }).ToList()
                                                        .Select(x => new ComboObject(x.AreaID.ToString(), x.AreaName)).ToList();
            // Chế độ chính sách
            var lstPolicyRegimes = PolicyRegimeBusiness.All.Where(x => x.IsActive).Select(x => new { x.PolicyRegimeID, x.Resolution }).ToList()
                                                        .Select(x => new ComboObject(x.PolicyRegimeID.ToString(), x.Resolution)).ToList();
            // Loại khuyết tật
            var lstDisabilityType = DisabledTypeBusiness.All.Where(x => x.IsActive).Select(x => new { x.DisabledTypeID, x.Resolution }).ToList()
                                                        .Select(x => new ComboObject(x.DisabledTypeID.ToString(), x.Resolution)).ToList();

            // Danh sách Tỉnh/thành, Quận/Huyện, Xã/Phường
            List<Province> lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            List<District> lstDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            List<Commune> listCom = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            IQueryable<Village> lstVillage = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });

            var listClass = ClassProfileBusiness.Search(new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                                                        { "EducationLevelID", educationLevelID }, { "IsActive", true } })
                                                        .Where(x => x.EducationLevel.Grade == _globalInfo.AppliedLevel)
                                                .Select(u => new { u.ClassProfileID, u.DisplayName }).ToList()
                                                .Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName.ToLower())).ToList();
            var listEthnic = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.EthnicID, u.EthnicName }).ToList().Select(u => new ComboObject(u.EthnicID.ToString(), u.EthnicName.ToLower())).ToList();
            var listPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.PolicyTargetID, u.Resolution }).ToList().Select(u => new ComboObject(u.PolicyTargetID.ToString(), u.Resolution.ToLower())).ToList();
            //List<string> lstPolicyTargetNameTemp = listPolicyTarget.Select(o => o.value).ToList();
            var listReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ReligionID, u.Resolution }).ToList().Select(u => new ComboObject(u.ReligionID.ToString(), u.Resolution.ToLower())).ToList();
            int tempCount = 0;
            bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            bool isSchoolModify = CodeConfigBusiness.IsSchoolModify(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            string originalPupilCode = string.Empty;
            int maxOrderNumber = 0;
            int numberLength = 0;
            //int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<PupilOfClassBO> lstpoc = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", _globalInfo.AcademicYearID.Value } })
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               ClassID = poc.ClassID,
                                               PupilCode = pf.PupilCode,
                                               AssignedDate = poc.AssignedDate,
                                               Status = poc.Status
                                           }).ToList();
            //List<int> lstPupilID = lstpoc.Select(p => p.PupilID).Distinct().ToList();
            List<int> LstClassIDInCurrentYear = new List<int>();
            List<int> LstClassIDNotInCurrentYear = new List<int>();
            List<string> lstPupilCodeOtherYear = new List<string>();
            List<string> lstPupilCodeCurrentYear = new List<string>();

            var lstEducationLevel = _globalInfo.EducationLevels.Select(x => x.EducationLevelID).ToList();
            if (educationLevelID > 0)
            {
                lstEducationLevel = lstEducationLevel.Where(x => x == educationLevelID).ToList();
            }

            var lstEducationLevelWithClass = ClassProfileBusiness.All.Where(x => lstEducationLevel.Contains(x.EducationLevelID))
                                            .Where(x=>x.SchoolID == _globalInfo.SchoolID.Value)
                                            .Where(x=>x.AcademicYearID == _globalInfo.AcademicYearID)
                                            .Select(x => new { EducationLevelID = x.EducationLevelID, ClassName = x.DisplayName }).Distinct().ToList();

            List<AutoGenericCode> lstOutGenericCode = new List<AutoGenericCode>();
            if (isAutoGenCode)
            {
                AutoGenericCode objAutoGen = null;
                foreach (var item in lstEducationLevel)
                {
                    CodeConfigBusiness.GetPupilCodeForImport(_globalInfo.AcademicYearID.Value, item, _globalInfo.SchoolID.Value, out originalPupilCode, out maxOrderNumber, out numberLength);

                    objAutoGen = new AutoGenericCode();
                    objAutoGen.Index = 0;
                    objAutoGen.OriginalPupilCode = originalPupilCode;
                    objAutoGen.MaxOrderNumber = maxOrderNumber;
                    objAutoGen.NumberLength = numberLength;
                    objAutoGen.EducationLevelID = item;
                    lstOutGenericCode.Add(objAutoGen);
                }
            }

            // Thông tin Mã HS của các năm học phải là năm hiện tại
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel);
            dic.Add("SchoolID", _globalInfo.SchoolID);
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID); // Năm hiện tại
            // Danh sách ClassId trong năm hiện tại
            LstClassIDInCurrentYear = ClassProfileBusiness.Search(dic).Select(o => o.ClassProfileID).ToList();
            // Danh sách lớp trong các năm khác - chỉ xét trong cùng cấp
            LstClassIDNotInCurrentYear = this.ClassProfileBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID && o.EducationLevel.Grade == _globalInfo.AppliedLevel
                                                            && !LstClassIDInCurrentYear.Contains(o.ClassProfileID) && o.IsActive.Value).Select(o => o.ClassProfileID).ToList();
            // Lấy danh sách HS trong các năm khác
            lstPupilCodeOtherYear = lstpoc.Where(p => LstClassIDNotInCurrentYear.Contains(p.ClassID)).Select(p => p.PupilCode).ToList();
            lstPupilCodeCurrentYear = lstpoc.Where(p => LstClassIDInCurrentYear.Contains(p.ClassID)).Select(p => p.PupilCode).ToList();

            var listCheckIndex = new List<string>();

            //Tìm kiếm học sinh trong năm học
            List<PupilProfile> lstPP = null;

            lstPP = (from pf in PupilProfileBusiness.All
                     join poc in PupilOfClassBusiness.All on pf.PupilProfileID equals poc.PupilID
                     where poc.SchoolID == _globalInfo.SchoolID.Value
                     && pf.IsActive
                     select pf).ToList();

            //lstPP = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } }).ToList();

            int modSchoolID = _globalInfo.SchoolID.Value % 100;
            int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value).Year;
            #endregion

            // Biến lưu dữ liệu tận dụng lại để giảm thời gian import
            // Tỉnh thành trước đó
            string ProviceNameBefore = string.Empty;
            int ProvinceIdBefore = 0;
            // Quận/Huyện trước đó
            List<District> lstDistrictInProvince = new List<District>();
            string DistrictNameBefore = string.Empty;
            int DistrictIdBefore = 0;

            var culture = new CultureInfo("vi-VN");
            StringBuilder ErrorMessage = new StringBuilder();
            bool chkCheck = true;
            List<string> lstPupilCodeNew = new List<string>();

            while (!string.IsNullOrEmpty(GetCellString(sheet, indexCol + rowIndex))
                || !string.IsNullOrEmpty(GetCellString(sheet, fullNameCol + rowIndex))
                || !string.IsNullOrEmpty(GetCellString(sheet, classNameCol + rowIndex)))
            {
                ErrorMessage = new StringBuilder();
                ImportPupilViewModel pupil = new ImportPupilViewModel();
                pupil.checkInsertVilliage = false;
                pupil.IsValid = true;
                //STT
                pupil.IndexStr = GetCellString(sheet, indexCol + rowIndex);
                int? index = GetCellIndex(pupil.IndexStr);

                if (index.HasValue)
                {
                    pupil.Index = index.Value;
                }
                else
                {
                    pupil.Index = 0;
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_STTNotNumber")).Append(SEMICOLON_SPACE);
                }

                #region // Thông tin học sinh
                //Mã HS
                pupil.PupilCode = GetCellString(sheet, pupilCodeCol + rowIndex);
                // Ho va ten
                pupil.FullName = GetCellString(sheet, fullNameCol + rowIndex);
                pupil.Name = GetPupilName(pupil.FullName);
                if (string.IsNullOrEmpty(pupil.FullName))
                {
                    ErrorMessage.Append(Res.Get("Họ và tên học sinh không được để trống")).Append(SEMICOLON_SPACE);
                }
                // Ngay sinh
                pupil.BirthDateStr = GetCellString(sheet, birthDateCol + rowIndex);
                string birthDateTemp = GetCellString(sheet, birthDateCol + rowIndex);
                DateTime birthDate;
                #region // check ngày sinh
                if (!string.IsNullOrEmpty(birthDateTemp))
                {
                    if (DateTime.TryParse(birthDateTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out birthDate))
                    {
                        pupil.BirthDate = birthDate;
                        if (pupil.BirthDate.Value > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidBirthDate_DateTimeNow")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.BirthDate.Value <= new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidBirthDate")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else
                    {
                        ErrorMessage.Append("Ngày sinh không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                else
                {
                    ErrorMessage.Append("Ngày sinh không được để trống").Append(SEMICOLON_SPACE);
                }
                #endregion

                // Gioi tinh
                pupil.GenreName = GetCellString(sheet, genreNameCol + rowIndex);
                pupil.GenreNameReal = pupil.GenreName == "" ? SystemParamsInFile.REPORT_LOOKUPINFO_MALE : pupil.GenreName.ToUpper() == "X" ? SystemParamsInFile.REPORT_LOOKUPINFO_FEMALE : pupil.GenreName;
                pupil.Genre = pupil.GenreName == "" ? SystemParamsInFile.GENRE_MALE : SystemParamsInFile.GENRE_FEMALE;
                if (pupil.GenreName != "" && pupil.GenreName.ToUpper() != "X")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidGenre")).Append(SEMICOLON_SPACE);
                }
                // Lớp
                string className = GetCellString(sheet, classNameCol + rowIndex);
                int classID = SetFromList(listClass, className);
                pupil.ClassName = className;
                pupil.CurrentClassID = classID;

                if (string.IsNullOrEmpty(pupil.ClassName))
                {
                    ErrorMessage.Append(Res.Get("Tên lớp không được để trống")).Append(SEMICOLON_SPACE);
                }
                else
                {
                    if (classID <= 0)
                    {
                        ErrorMessage.Append(Res.Get("Tên lớp không hợp lệ")).Append(SEMICOLON_SPACE);
                    }
                }

                // Hình thức trúng tuyển
                pupil.EnrolmentTypeName = GetCellString(sheet, enrolmentTypeNameCol + rowIndex);
                pupil.EnrolmentType = SetEnrolementType(pupil.EnrolmentTypeName);
                // Ngày vào trường
                pupil.EnrolmentDateStr = GetCellString(sheet, enrolmentDateCol + rowIndex);
                string enrolmentDateTemp = GetCellString(sheet, enrolmentDateCol + rowIndex);
                DateTime enrolmentDate;
                #region // check ngày vào trường
                if (!String.IsNullOrEmpty(enrolmentDateTemp))
                {
                    if (DateTime.TryParse(enrolmentDateTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out enrolmentDate))
                    {
                        pupil.EnrolmentDate = enrolmentDate;
                        if (pupil.EnrolmentDate.Value.Date > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidEnrolmentDate_DateTimaNow")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.EnrolmentDate.Value < new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidEnrolmentDate")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else
                    {

                        ErrorMessage.Append("Ngày vào trường không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                else
                {
                    ErrorMessage.Append("Ngày vào trường không được để trống").Append(SEMICOLON_SPACE);
                }
                #endregion
                // Trạng thái học sinh
                pupil.ProfileStatusName = GetCellString(sheet, profileStatusNameCol + rowIndex);
                pupil.ProfileStatus = pupil.ProfileStatusName == "" ? SystemParamsInFile.PUPIL_STATUS_STUDYING : SetProfileStatus(pupil.ProfileStatusName);
                // Tỉnh/thành
                pupil.ProvinceName = GetCellString(sheet, provinceNameCol + rowIndex);
                #region // Cho phép nhập Tên hoặc Mã Tỉnh thành
                if (!string.IsNullOrEmpty(pupil.ProvinceName))
                {
                    if (pupil.ProvinceName.Equals(ProviceNameBefore))
                    {
                        pupil.ProvinceID = ProvinceIdBefore;
                    }
                    else if (lstProvince.Exists(o => o.ProvinceName.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())))
                    {
                        Province provinceObj = lstProvince.Where(o => o.ProvinceName.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())).FirstOrDefault();
                        if (provinceObj != null)
                        {
                            pupil.ProvinceID = provinceObj.ProvinceID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID).ToList();
                            ProviceNameBefore = pupil.ProvinceName;
                            ProvinceIdBefore = provinceObj.ProvinceID;
                        }
                    }
                    else if (lstProvince.Exists(o => o.ProvinceCode.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())))
                    {
                        Province provinceObj = lstProvince.Where(o => o.ProvinceCode.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())).FirstOrDefault();
                        if (provinceObj != null)
                        {
                            pupil.ProvinceID = provinceObj.ProvinceID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID).ToList();
                            ProviceNameBefore = pupil.ProvinceName;
                            ProvinceIdBefore = provinceObj.ProvinceID;
                        }
                    }
                }
                #endregion

                if (string.IsNullOrEmpty(pupil.ProvinceName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidProvinceNameNull")).Append(SEMICOLON_SPACE);
                }

                if (pupil.ProvinceName != null && pupil.ProvinceName.Trim() != "" && pupil.ProvinceID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidProvinceName")).Append(SEMICOLON_SPACE);
                }
                // Quận/Huyện
                pupil.DistrictName = GetCellString(sheet, districtNameCol + rowIndex);
                #region // Check Quận/Huyện
                if (!string.IsNullOrEmpty(pupil.DistrictName))
                {
                    if (pupil.DistrictName.Equals(DistrictNameBefore))
                    {
                        pupil.DistrictID = DistrictIdBefore;
                    }
                    else if (lstDistrictInProvince.Exists(o => o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())))
                    {
                        District obj = lstDistrictInProvince.Where(o => o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())).FirstOrDefault();
                        if (obj != null)
                        {
                            pupil.DistrictID = obj.DistrictID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            DistrictNameBefore = pupil.DistrictName;
                            DistrictIdBefore = obj.DistrictID;
                        }
                    }
                    else if (lstDistrict.Exists(o => o.ProvinceID == pupil.ProvinceID && (o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower()))))
                    {
                        District obj = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID && ((o.DistrictName != null && o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower())) || (o.DistrictCode != null && o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower()))))
                                        .FirstOrDefault();
                        if (obj != null)
                        {
                            pupil.DistrictID = obj.DistrictID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.DistrictID == pupil.DistrictID).ToList();
                            DistrictNameBefore = pupil.DistrictName;
                            DistrictIdBefore = pupil.DistrictID.Value;
                        }
                    }
                }

                if (pupil.DistrictName != null && pupil.DistrictName.Trim() != "" && pupil.DistrictID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrictName")).Append(SEMICOLON_SPACE);
                }
                #endregion

                // Xã/Phường
                pupil.CommuneName = GetCellString(sheet, communeNameCol + rowIndex);
                #region // Xã/Phường
                if (!string.IsNullOrEmpty(pupil.CommuneName))
                {
                    if (listCom.Exists(o => o.DistrictID == pupil.DistrictID && ((o.CommuneName != null && o.CommuneName.ToLower().Equals(pupil.CommuneName.ToLower())) || (o.CommuneCode != null && o.CommuneCode.ToLower().Equals(pupil.CommuneName.ToLower())))))
                    {
                        Commune comObj = listCom.Where(o => o.DistrictID == pupil.DistrictID && ((o.CommuneName != null && o.CommuneName.ToLower().Equals(pupil.CommuneName.ToLower())) || (o.CommuneCode != null && o.CommuneCode.ToLower().Equals(pupil.CommuneName.ToLower()))))
                                        .FirstOrDefault();
                        if (comObj != null)
                        {
                            pupil.CommuneID = comObj.CommuneID;
                        }
                    }
                }
                #endregion
                // Thôm/Xóm
                pupil.VillageName = GetCellString(sheet, villageNameCol + rowIndex);
                bool checkDistrict = checkCompareDistrict(pupil.ProvinceID, pupil.DistrictID, lstDistrict);
                bool checkCommune = checkCompareCommune(pupil.DistrictID, pupil.CommuneID, listCom);
                //Check Nhập tỉnh thành trước khi nhập quận huyện
                if (pupil.DistrictID != null && pupil.ProvinceID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrictProvince")).Append(SEMICOLON_SPACE);
                }
                if (pupil.CommuneID != null && (pupil.DistrictID == null || pupil.ProvinceID == null))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommuneDistrict")).Append(SEMICOLON_SPACE);
                }
                if (!checkDistrict)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrict")).Append(SEMICOLON_SPACE);
                }
                if (pupil.CommuneName != null && pupil.CommuneName.Trim() != "" && pupil.CommuneID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommuneName")).Append(SEMICOLON_SPACE);
                }

                if (!checkCommune)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommune")).Append(SEMICOLON_SPACE);
                }

                if (!checkCompareVillage(pupil.VillageID, pupil.CommuneID))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliageName")).Append(SEMICOLON_SPACE);
                }

                if (pupil.CommuneID == null || pupil.DistrictID == null || pupil.ProvinceID == null)
                {
                    if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                    {
                        ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliage")).Append(SEMICOLON_SPACE);
                    }
                }
                if (pupil.VillageName != null && pupil.VillageName.Trim().Length > 50)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliageLength")).Append(SEMICOLON_SPACE);
                }

                // Số đăng bộ
                pupil.StorageNumber = GetCellString(sheet, StorageNumberCol + rowIndex);
                //Nơi sinh
                pupil.BirthPlace = GetCellString(sheet, birthPlaceCol + rowIndex);
                // Quê quán
                pupil.HomeTown = GetCellString(sheet, homeTownCol + rowIndex);
                // Địa chỉ thường trú
                pupil.PermanentResidentalAddress = GetCellString(sheet, permanentResidentalAddressCol + rowIndex);
                // Địa chỉ tạm trú
                pupil.TempResidentalAddress = GetCellString(sheet, tempResidentalAddressCol + rowIndex);
                // Hệ học ngoại ngữ
                pupil.ForeignLanguageTrainingName = GetCellString(sheet, foreignLanguageTrainingNameCol + rowIndex);
                pupil.ForeignLanguageTraining = SetForeignLanguageTraining(pupil.ForeignLanguageTrainingName);
                // Hình thức học
                pupil.PupilLearningTypeName = GetCellString(sheet, LearningTypeNameCol + rowIndex);
                pupil.PupilLearningType = SetPupilLearningType(pupil.PupilLearningTypeName);
                // Ngày vào đội, đoàn, đảng
                string youngPioneerTemp = string.Empty;
                string youngLeagueTemp = string.Empty;
                string communistPartyTemp = string.Empty;
                DateTime youngPioneerDate;
                DateTime youngLeagueDate;
                DateTime communistPartyDate;

                if (_globalInfo.AppliedLevel <= SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    youngPioneerTemp = GetCellString(sheet, youngPioneerJoinedDate + rowIndex);
                }
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                    || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    youngLeagueTemp = GetCellString(sheet, youngLeagueJoinedDate + rowIndex);
                }
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    communistPartyTemp = GetCellString(sheet, communistPartyJoinedDate + rowIndex);
                }
                #region // kiểm tra thông tin ngày đội cấp 1, 2, 3
                if (_globalInfo.AppliedLevel <= SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    if (DateTime.TryParse(youngPioneerTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out youngPioneerDate))
                    {
                        pupil.YoungPioneerJoinedDate = youngPioneerDate;
                        pupil.YoungPioneerJoinedDateStr = youngPioneerDate.ToShortDateString();
                        if (pupil.YoungPioneerJoinedDate.Value > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đội phải nhỏ hơn ngày hiện tại.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungPioneerJoinedDate.Value < new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đội phải lớn hơn hoặc bằng ngày 01/01/1900.")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else if (youngPioneerTemp != string.Empty)
                    {
                        ErrorMessage.Append("Ngày vào đội không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                #region //kiểm tra ngày vào đoàn cấp 2,3
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    if (DateTime.TryParse(youngLeagueTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out youngLeagueDate))
                    {
                        pupil.YoungLeagueJoinedDate = youngLeagueDate;
                        pupil.YoungLeagueJoinedDateStr = youngLeagueDate.ToShortDateString();
                        if (pupil.YoungLeagueJoinedDate.Value > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đoàn phải nhỏ hơn ngày hiện tại.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungLeagueJoinedDate.Value < new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đoàn phải lớn hơn hoặc bằng ngày 01/01/1900.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungPioneerJoinedDate >= youngLeagueDate)//Nhập ngày vào đội > ngày vào đoàn
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đoàn phải lớn hơn ngày vào đội.")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else if (youngLeagueTemp != string.Empty)
                    {
                        ErrorMessage.Append("Ngày vào đoàn không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                #region //kiểm tra ngày vào đảng cấp 3
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    if (DateTime.TryParse(communistPartyTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out communistPartyDate))
                    {
                        pupil.CommunistPartyJoinedDate = communistPartyDate;
                        pupil.CommunistPartyJoinedDateStr = communistPartyDate.ToShortDateString();
                        if (pupil.CommunistPartyJoinedDate.Value > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đảng phải nhỏ hơn ngày hiện tại.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.CommunistPartyJoinedDate.Value < new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đảng phải lớn hơn hoặc bằng ngày 01/01/1900.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungPioneerJoinedDate >= communistPartyDate)//Ngày vào đội > ngày vào đảng
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đảng phải lớn hơn ngày vào đội")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungLeagueJoinedDate >= communistPartyDate)//Ngày vào đoàn > ngày vào đảng
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đảng phải lớn hơn ngày vào đoàn")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else if (communistPartyTemp != string.Empty)
                    {
                        ErrorMessage.Append("Ngày vào đảng không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                // Dân tộc
                pupil.EthnicName = GetCellString(sheet, ethnicNameCol + rowIndex);
                pupil.EthnicID = (int?)SetNulableFromList(listEthnic, pupil.EthnicName);//mac dinh dan toc kinh
                if (pupil.EthnicID == -1 && pupil.EthnicName != null && !string.IsNullOrEmpty(pupil.EthnicName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_EthnicExisted")).Append(SEMICOLON_SPACE);
                }
                // Tôn giáo
                pupil.ReligionName = GetCellString(sheet, religionNameCol + rowIndex);
                pupil.ReligionID = (int?)SetNulableFromList(listReligion, pupil.ReligionName);
                if (pupil.ReligionID == -1 && pupil.ReligionName != null && !string.IsNullOrEmpty(pupil.ReligionName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_ReligionExisted")).Append(SEMICOLON_SPACE);
                }
                // Đối tượng chính sách
                pupil.PolicyTargetName = GetCellString(sheet, policyTargetNameCol + rowIndex);
                pupil.PolicyTargetID = (int?)SetNulableFromList(listPolicyTarget, pupil.PolicyTargetName);
                // Chế độ chính sách
                if (!string.IsNullOrEmpty(pupil.PolicyTargetName) && pupil.PolicyTargetID > 0)
                {
                    pupil.PolicyRegimeName = GetCellString(sheet, policyRegimeCol + rowIndex);
                    if (!string.IsNullOrEmpty(pupil.PolicyRegimeName))
                    {
                        pupil.PolicyRegimeID = (int?)SetNulableFromList(lstPolicyRegimes, pupil.PolicyRegimeName);
                    }
                }
                //Ho tro chi phi học tap
                pupil.IsSupportForLearning = GetCellString(sheet, isSupportForLearningCol + rowIndex).ToUpper() == "X" ? true : false;
                //tai dinh cu
                pupil.IsResettlementTarget = GetCellString(sheet, isResettlementTargetCol + rowIndex).ToUpper() == "X" ? true : false;
                // Khu vực
                pupil.AreaName = GetCellString(sheet, areaNameCol + rowIndex);
                pupil.AreaID = (int?)SetNulableFromList(lstArea, pupil.AreaName);
                //dien hoc sinh;
                pupil.ClassTypeName = GetCellString(sheet, classTypeCol + rowIndex);
                pupil.ClassType = SetNulableFromList(CommonList.ClassType(), GetCellString(sheet, classTypeCol + rowIndex));
                if (pupil.ClassTypeName != "" && pupil.ClassType == -1)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidClassType")).Append(SEMICOLON_SPACE);
                }
                //Loại khuyết tật
                pupil.DisabilityTypeName = GetCellString(sheet, disabilityTypeCol + rowIndex);
                if (!string.IsNullOrEmpty(pupil.DisabilityTypeName))
                {
                    pupil.DisabledTypeID = (int?)SetNulableFromList(lstDisabilityType, pupil.DisabilityTypeName);
                }

                if (!string.IsNullOrEmpty(pupil.DisabilityTypeName) && pupil.DisabledTypeID > 0)
                {
                    pupil.IsDisabled = true;
                }
                else
                {
                    pupil.IsDisabled = false;
                }
                // Số điện thoại
                pupil.Mobile = GetCellString(sheet, mobileCol + rowIndex);
                // CMND
                pupil.IdentityNumber = GetCellString(sheet, identifyNumberCol + rowIndex);
                // Email
                pupil.Email = GetCellString(sheet, emailCol + rowIndex);
                #region //check Email
                if (!string.IsNullOrEmpty(pupil.Email))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.Email.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(Res.Get("Common_Validate_Email")).Append(SEMICOLON_SPACE);
                    else if (pupil.Email.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "")).Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                // nhóm máu
                pupil.BloodTypeName = GetCellString(sheet, bloodTypeNameCol + rowIndex);
                pupil.BloodType = SetBloodType(pupil.BloodTypeName);
                // học chương trình giá dục của bộ
                pupil.IsUsedMoetProgram = GetCellString(sheet, IsUsedMoetProgram + rowIndex).ToUpper() == "X" ? true : false;
                // Biết bơi
                pupil.IsSwimming = GetCellString(sheet, IsSwimmingCol + rowIndex).ToUpper() == "X" ? true : false;
                // Mã mới học sinh
                pupil.PupilCodeNew = GetCellString(sheet, pupilCodeNew + rowIndex);
                // Trường hiện tại
                pupil.CurrentSchoolID = _globalInfo.SchoolID.Value;
                #endregion

                #region // Thông tin của cha
                // Họ tên cha 
                pupil.FatherFullName = GetCellString(sheet, fatherFullNameCol + rowIndex);
                // Năm sinh của cha
                int result = 0;
                DateTime? fatherBirthDate = new DateTime?();
                string fatherDateTemp = "";
                string fatherDate = GetCellString(sheet, fatherBirthDateCol + rowIndex);

                if ((int.TryParse(fatherDate, out result) == false || fatherDate.Trim().Length != 4) && fatherDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidFatherBirthDate")).Append(SEMICOLON_SPACE);
                }

                if (fatherDate != null && fatherDate.Trim() != "" && int.TryParse(fatherDate, out result) && fatherDate.Trim().Length == 4)
                {
                    fatherDateTemp = "01/01/" + fatherDate;
                }
                if (fatherDateTemp != null && fatherDateTemp.Trim() != "")
                {
                    fatherBirthDate = DateTime.Parse(fatherDateTemp);
                }
                pupil.FatherBirthDateStr = fatherDate;
                pupil.FatherBirthDate = fatherBirthDate;
                // Nghề nghiệp của cha
                pupil.FatherJob = GetCellString(sheet, fatherJobCol + rowIndex);
                // SDT của cha
                pupil.FatherMobile = GetCellString(sheet, fatherMobileCol + rowIndex);
                // Email của cha
                pupil.FatherEmail = GetCellString(sheet, fatherEmailCol + rowIndex);
                #region //check Email Father
                if (!string.IsNullOrEmpty(pupil.FatherEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.FatherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "cha")).Append(SEMICOLON_SPACE);
                    else if (pupil.FatherEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "cha")).Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                // Bố là dân tộc
                pupil.IsMinorityFather = GetCellString(sheet, isMinorityFatherCol + rowIndex).ToUpper() == "X" ? true : false;
                #endregion

                #region // Thông tin của mẹ
                // Họ và tên mẹ
                pupil.MotherFullName = GetCellString(sheet, motherFullNameCol + rowIndex);
                // Năm sinh của mẹ
                DateTime? motherBirthDate = new DateTime?();
                string motherDateTemp = "";
                string motherDate = GetCellString(sheet, motherBirthDateStrCol + rowIndex);

                if ((int.TryParse(motherDate, out result) == false || motherDate.Trim().Length != 4) && motherDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidMotherBirthDate")).Append(SEMICOLON_SPACE);
                }
                if (motherDate != null && motherDate.Trim() != "" && int.TryParse(motherDate, out result) && motherDate.Trim().Length == 4)
                {
                    motherDateTemp = "01/01/" + motherDate;
                }
                if (motherDateTemp != null && motherDateTemp.Trim() != "")
                {
                    motherBirthDate = DateTime.Parse(motherDateTemp);
                }
                pupil.MotherBirthDateStr = motherDate;
                pupil.MotherBirthDate = motherBirthDate;
                // Nghề nghiệp của mẹ
                pupil.MotherJob = GetCellString(sheet, motherJobCol + rowIndex);
                // SDT của mẹ
                pupil.MotherMobile = GetCellString(sheet, motherMobileCol + rowIndex);
                // Email của mẹ
                pupil.MotherEmail = GetCellString(sheet, motherEmailCol + rowIndex);
                #region //check Email Mother
                if (!string.IsNullOrEmpty(pupil.MotherEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.MotherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "mẹ")).Append(SEMICOLON_SPACE);
                    else if (pupil.MotherEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "mẹ")).Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                // Mẹ là dân tộc
                pupil.IsMinorityMother = GetCellString(sheet, isMinorityMotherCol + rowIndex).ToUpper() == "X" ? true : false;
                #endregion

                #region // Thông tin của người bảo hộ
                // Họ tên người bảo hộ
                pupil.SponsorFullName = GetCellString(sheet, sponsorFullNameCol + rowIndex);
                // Năm sinh người bảo hộ
                DateTime? sponsorBirthDate = new DateTime?();
                string sponsorDateTemp = "";
                string sponsorDate = GetCellString(sheet, sponsorBirthDateStrCol + rowIndex);
                if ((int.TryParse(sponsorDate, out result) == false || sponsorDate.Trim().Length != 4) && sponsorDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidSponsorBirthDate")).Append(SEMICOLON_SPACE);
                }
                if (sponsorDate != null && sponsorDate.Trim() != "" && int.TryParse(sponsorDate, out result) && sponsorDate.Trim().Length == 4)
                {
                    sponsorDateTemp = "01/01/" + sponsorDate;
                }
                if (sponsorDateTemp != null && sponsorDateTemp.Trim() != "")
                {
                    sponsorBirthDate = DateTime.Parse(sponsorDateTemp);
                }
                pupil.SponsorBirthDateStr = sponsorDate;
                pupil.SponsorBirthDate = sponsorBirthDate;
                // NGhề nghiệp người bảo hộ
                pupil.SponsorJob = GetCellString(sheet, sponsorJobCol + rowIndex);
                // SDt người bảo hộ
                pupil.SponsorMobile = GetCellString(sheet, sponsorMobilCol + rowIndex);
                // Email người bảo hộ
                pupil.SponsorEmail = GetCellString(sheet, sponsorEmailCol + rowIndex);
                #region //check Email Sponsor
                if (!string.IsNullOrEmpty(pupil.SponsorEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.SponsorEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "người bảo hộ")).Append(SEMICOLON_SPACE);
                    else if (pupil.SponsorEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "người bảo hộ")).Append(SEMICOLON_SPACE);
                    }
                }
                if (!string.IsNullOrEmpty(pupil.StorageNumber) && pupil.StorageNumber.Length > 30)
                {
                    ErrorMessage.Append(Res.Get("Common_Label_Maxlength_StorageNumber")).Append(SEMICOLON_SPACE);
                }
                #endregion
                #endregion


                #region
                #region validate du lieu Update hoặc Insert
                if (isUpdatePupil)
                {
                    if (lstPP.Any(o => o.PupilCode == pupil.PupilCode))
                    {
                        PupilProfile pupilProfile = lstPP.Where(o => o.PupilCode == pupil.PupilCode).OrderBy(o => o.EnrolmentDate).LastOrDefault();
                        List<PupilOfClassBO> lstpoc_temp = lstpoc.Where(o => o.PupilID == pupilProfile.PupilProfileID).ToList();
                        if (lstpoc_temp.Count() >= 2)
                        {
                            PupilOfClassBO pocAssignMax = lstpoc_temp.OrderBy(o => o.AssignedDate).LastOrDefault();
                            if (pupil.EnrolmentDate >= pocAssignMax.AssignedDate)
                            {
                                ErrorMessage.Append(Res.Get("PupilProfile_Validate_AssignDate")).Append(SEMICOLON_SPACE);
                            }
                        }
                        if (lstpoc_temp.Any(pp => pp.Status != GlobalConstants.PUPIL_STATUS_STUDYING && pp.ClassID == classID))
                        {
                            ErrorMessage.Append("Học sinh ở trạng thái khác đang học ").Append(SEMICOLON_SPACE);
                        }
                        if (!string.IsNullOrEmpty(pupil.PupilCodeNew))
                        {
                            lstPupilCodeNew.Add(pupil.PupilCodeNew);
                            if (lstPP.Any(o => o.PupilCode == pupil.PupilCodeNew))
                            {
                                ErrorMessage.Append(string.Format("Mã học sinh {0} đã tồn tại.", pupil.PupilCodeNew)).Append(SEMICOLON_SPACE);
                            }
                            if (lstPupilCodeNew.Count(p => p == pupil.PupilCodeNew) > 1)
                            {
                                ErrorMessage.Append(string.Format("Mã học sinh mới {0} không được trùng nhau.", pupil.PupilCodeNew)).Append(SEMICOLON_SPACE);
                            }
                        }

                        if (pupilProfile.CurrentClassID != classID)
                        {
                            Dictionary<string, object> dicSearch = new Dictionary<string, object>()
                        {
                            {"SchoolID",_globalInfo.SchoolID},
                            {"AcademicYearID",_globalInfo.AcademicYearID},
                            {"Last2digitNumberSchool",modSchoolID},
                            {"Year",year}
                        };
                            IQueryable<MarkRecord> lstMarkRecord = null;
                            IQueryable<JudgeRecord> lstJudgeRecord = null;
                            IQueryable<PupilRanking> lstPupilRanking = null;
                            IQueryable<SummedUpRecord> lstSUR = null;
                            IQueryable<PupilAbsence> lstPA = null;
                            IQueryable<PupilFault> lstPF = null;
                            if (chkCheck)
                            {
                                //list diem mon tinh diem cua hoc sinh
                                lstMarkRecord = MarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list diem mon nhan xet cua hoc sinh
                                lstJudgeRecord = JudgeRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list danh gia xep loai
                                lstPupilRanking = PupilRankingBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list TBM
                                lstSUR = SummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list diem danh
                                lstPA = PupilAbsenceBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list vi pham
                                lstPF = PupilFaultBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                chkCheck = false;
                            }

                            bool MarkRecordContrainst = lstMarkRecord != null && lstMarkRecord.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Any();
                            if (MarkRecordContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_MarkRecordExist")).Append(SEMICOLON_SPACE);
                            }
                            bool JudgeRecordContrainst = lstJudgeRecord != null && lstJudgeRecord.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Any();
                            if (!MarkRecordContrainst && JudgeRecordContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_MarkRecordExist")).Append(SEMICOLON_SPACE);
                            }
                            bool PupilRankingContrainst = lstPupilRanking != null && lstPupilRanking.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Count() > 0;
                            if (PupilRankingContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_PupilRankingExist")).Append(SEMICOLON_SPACE);
                            }
                            bool SummedUpRecordContrainst = lstSUR != null && lstSUR.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Count() > 0;
                            if (SummedUpRecordContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_SummedUpRecordExist")).Append(SEMICOLON_SPACE);
                            }
                            bool PupilAbsenceContrainst = lstPA != null && lstPA.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Count() > 0;
                            if (PupilAbsenceContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_PupilAbsenceExist")).Append(SEMICOLON_SPACE);
                            }
                            bool PupilFaultContrainst = lstPF != null && lstPF.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Count() > 0;
                            if (PupilFaultContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_PupilFaultExist")).Append(SEMICOLON_SPACE);
                            }
                        }
                    }
                    else
                    {
                        ErrorMessage.Append(Res.Get("Học sinh không tồn tại để cập nhật thông tin.")).Append(SEMICOLON_SPACE);
                        pupil.IsValid = false;
                        pupil.ErrorMessage += ErrorMessage.ToString();
                        listPupil.Add(pupil);
                        rowIndex++;
                        tempCount++;
                        continue;
                    }
                }
                else if (isAddPupil)
                {
                    pupil.PupilCodeNew = string.Empty;
                    if (lstPP.Any(o => o.PupilCode == pupil.PupilCode))
                    {
                        ErrorMessage.Append(Res.Get("Đã tồn tại học sinh này, Thầy/cô không thể thêm."));
                        pupil.IsValid = false;
                        pupil.ErrorMessage += ErrorMessage.ToString();
                        listPupil.Add(pupil);
                        rowIndex++;
                        tempCount++;
                        continue;
                    }
                }
                #endregion

                #region AnhVD9 20140918 - Kiểm tra không cho Import trùng mã trong năm học trước. Nếu mã đã có trong năm học này thì vẫn cho cập nhật
                if (!string.IsNullOrWhiteSpace(pupil.PupilCode))
                {
                    if (lstPupilCodeOtherYear.Contains(pupil.PupilCode)) // năm học cũ đã tồn tại mã
                    {
                        if (!lstPupilCodeCurrentYear.Contains(pupil.PupilCode)) // năm học hiện tại chưa có mã này
                        {
                            ErrorMessage.Append(Res.Get("ImportPupill_PupilCode_InOtherYear")).Append(SEMICOLON_SPACE);
                        }
                    }
                }
                #endregion

                #region // isAutoGenCode

                AutoGenericCode objAutoGenericCode = new AutoGenericCode();               
                var tempt = lstEducationLevelWithClass.Where(x => x.ClassName.ToUpper().Equals(className.ToUpper())).FirstOrDefault();
                if (tempt == null && educationLevelID != 0)
                {
                    ErrorMessage.Append(className + " không thuộc khối đã chọn");
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                    listPupil.Add(pupil);
                    rowIndex++;
                    tempCount++;
                    continue;
                }
                else if (tempt != null)
                {
                    if (isAutoGenCode)
                    {
                        objAutoGenericCode = lstOutGenericCode.Where(x => x.EducationLevelID == tempt.EducationLevelID).FirstOrDefault();
                        originalPupilCode = objAutoGenericCode.OriginalPupilCode;
                        maxOrderNumber = objAutoGenericCode.MaxOrderNumber;
                        numberLength = objAutoGenericCode.NumberLength;
                        tempCount = objAutoGenericCode.Index;
                        string pupilCode = originalPupilCode + ToStringWithFixedLength(maxOrderNumber + 1 + tempCount, numberLength);
                        if (isSchoolModify)
                        {
                            if (string.IsNullOrEmpty(pupil.PupilCode))
                            {
                                pupil.PupilCode = pupilCode;
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(pupil.PupilCode))
                            {
                                pupil.PupilCode = pupilCode;
                            }
                            else
                            {
                                int temp = 0;
                                if (pupil.PupilCode.Length - numberLength > 0)
                                {
                                    int.TryParse(pupil.PupilCode.Substring((pupil.PupilCode.Length - numberLength), numberLength), out temp);
                                }
                                if (!pupil.PupilCode.StartsWith(originalPupilCode) || pupil.PupilCode.Length != pupilCode.Length || temp == 0)
                                {
                                    ErrorMessage.Append(Res.Get("ImportPupil_Label_OriginalPupilCode")).Append(SEMICOLON_SPACE);
                                }
                            }
                        }
                    }
                }

                #endregion

                ValidateImportedPupil(pupil);

                if (listPupil.Any(u => u.PupilCode.ToLower().Equals(pupil.PupilCode.ToLower())))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_PupilCodeExisted"), pupil.PupilCode)).Append(SEMICOLON_SPACE);
                }

                if (pupil.Index > 0 && pupil.CurrentClassID > 0 && listPupil.Any(u => u.Index == pupil.Index && u.CurrentClassID == pupil.CurrentClassID))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_IndexClassExisted"), pupil.ClassName, pupil.Index)).Append(SEMICOLON_SPACE);
                }

                if (!string.IsNullOrEmpty(pupil.ClassName) && !listClass.Any(u => u.key.Equals(pupil.CurrentClassID.ToString())))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_ClassNotExist"), pupil.ClassName)).Append(SEMICOLON_SPACE);
                }

                if (pupil.PolicyTargetID.HasValue && !listPolicyTarget.Any(o => pupil.PolicyTargetID != null && o.key == pupil.PolicyTargetID.Value.ToString()))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidPolicyTargetName")).Append(SEMICOLON_SPACE);
                }

                if (string.IsNullOrEmpty(pupil.ClassName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidClassNameNull")).Append(SEMICOLON_SPACE);
                }

                //Kiểm tra thôn xóm có chưa?
                if (checkDistrict == true && checkCommune == true && pupil.IsValid == true)
                {
                    if (pupil.CommuneID != null && pupil.DistrictID != null && pupil.ProvinceID != null)
                    {
                        bool isCheck = listPupilTemp.Where(o => o.VillageName.Trim().ToUpper() == pupil.VillageName.Trim().ToUpper() && o.CommuneID == pupil.CommuneID).Count() > 0 ? true : false;

                        if (pupil.VillageID == null && pupil.VillageName != null && pupil.VillageName.Trim() != "" && !isCheck)
                        {
                            bool isCheck1 = lstVillage.Where(o => o.VillageName.Trim().ToUpper() == pupil.VillageName.Trim().ToUpper() && o.CommuneID == pupil.CommuneID).Count() > 0 ? true : false;
                            if (!isCheck1)
                            {
                                pupil.checkInsertVilliage = true;
                                //them doi tuong thon xom de insert
                                Village objvl = new Village();
                                objvl.VillageName = pupil.VillageName;
                                objvl.ShortName = pupil.VillageName;
                                objvl.CommuneID = pupil.CommuneID;
                                objvl.CreateDate = DateTime.Now;
                                objvl.IsActive = true;
                                lstVillageImport.Add(objvl);
                            }
                        }
                    }
                }
                #endregion

                // Gán lỗi
                if (ErrorMessage.Length > 0)
                {
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                }
                if (pupil.IsValid)
                    listPupilTemp.Add(pupil);
                else // Xóa bỏ ký tự ; cuối cùng
                {
                    if (!String.IsNullOrEmpty(pupil.ErrorMessage) && pupil.ErrorMessage.Length > 2)
                    {
                        pupil.ErrorMessage = pupil.ErrorMessage.Remove(pupil.ErrorMessage.Length - 2);
                    }
                }

                listPupil.Add(pupil);
                rowIndex++;

                if (tempt != null)
                {
                    objAutoGenericCode.Index++;
                    lstOutGenericCode.RemoveAll(x => x.EducationLevelID == objAutoGenericCode.EducationLevelID);
                    lstOutGenericCode.Add(objAutoGenericCode);
                }
                tempCount++;
            }

            return listPupil;
        }

        public PartialViewResult LoadImportedGrid()
        {
            return PartialView("__GridImported");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ContinueSave(FormCollection frm)
        {
            bool isAddPupil = frm["hdfIsAddPupil"] != null && "1".Equals(frm["hdfIsAddPupil"]) ? true : false;
            List<ImportPupilViewModel> listPupil = (List<ImportPupilViewModel>)Session["LIST_IMPORTED_PUPIL"];
            SaveData(isAddPupil);

            int countAllPupil = listPupil != null ? listPupil.Count : 0;
            int countPupilValid = listPupil != null ? listPupil.Count(p => p.IsValid) : 0;
            string retMsg = String.Format("{0} {1}/{2} học sinh", Res.Get("Common_Label_ImportSuccessMessage"), countPupilValid, countAllPupil);
            return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS), "text/html");
        }

        public FileResult DowloadFileErr(int? EducationLevelID)
        {
            string reportName = string.Empty;
            Stream excel = this.SetDataToFileExcel(EducationLevelID, ref reportName, true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = reportName;
            return result;
        }

        public FileResult ExportTemplate(int? EducationLevelID)
        {
            string reportName = string.Empty;
            Stream excel = this.SetDataToFileExcel(EducationLevelID, ref reportName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = reportName;
            return result;
        }

        private Stream SetDataToFileExcel(int? EducationLevelID, ref string ReportName, bool isErrFile = false)
        {
            EducationLevel edu = EducationLevelID > 0 ? EducationLevelBusiness.Find(EducationLevelID) : new EducationLevel();
            int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;

            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.IMPORT_THONGTINHOCSINH_2018);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            ReportName = ReportUtils.RemoveSpecialCharacters(reportDefinition.OutputNamePattern) + "." + reportDefinition.OutputFormat;

            IVTWorkbook book = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = book.GetSheet(1);
            IVTWorksheet refSheet = book.GetSheet(2); // Tỉnh thành
            IVTWorksheet refSheetDis = book.GetSheet(3); // Quận Huyện
            IVTWorksheet refSheetComm = book.GetSheet(4); // Xã phường

            IVTWorksheet refTemp = book.GetSheet(5);
            IVTWorksheet refHDC = book.GetSheet(6);

            IVTWorksheet sheetProvince = book.CopySheetToLast(refSheet, "C3");
            IVTWorksheet sheetDistrict = book.CopySheetToLast(refSheetDis, "D3");
            IVTWorksheet sheetCommune = book.CopySheetToLast(refSheetComm, "E3");

            IVTRange rangeProvince = refSheet.GetRange("A4", "C4");
            IVTRange rangeDistrict = refSheetDis.GetRange("A4", "D4");

            IVTRange rangeProOfCommune = refSheetComm.GetRange("A4", "E4");
            IVTRange rangeCommune = refSheetComm.GetRange("A5", "E5");

            IVTWorksheet sheetHDC = book.CopySheetToLast(refHDC, "A20");

            #region fill data for sheet
            Dictionary<string, object> dicSheet = new Dictionary<string, object>();
            dicSheet["SuperVisingDeptName"] = _globalInfo.SuperVisingDeptName;
            dicSheet["SchoolName"] = _globalInfo.SchoolName.ToUpper();
            dicSheet["DateTime"] = string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            //dicSheet["EducationLevel"] = edu.Resolution;
            sheet.FillVariableValue(dicSheet);
            #endregion

            #region fill data for combobox values
            Dictionary<string, object> dicRef = new Dictionary<string, object>();
            // Khối - Lớp
            List<object> listClass = new List<object>();
            ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                                                new Dictionary<string, object>
                                                    {
                                                        {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                                                        {"EducationLevelID", EducationLevelID},
                                                        {"IsActive", true}
                                                    }).Where(x => x.EducationLevel.Grade == _globalInfo.AppliedLevel).ToList().ForEach(u => listClass.Add(new { u.DisplayName }));
            dicRef["Classes"] = listClass;

            // Diện học sinh
            List<object> listClassType = new List<object>();
            listClassType.Add(new { Value = "Nội trú" });
            listClassType.Add(new { Value = "Bán trú" });
            listClassType.Add(new { Value = "Nội trú dân nuôi" });
            listClassType.Add(new { Value = "Bán trú dân nuôi" });
            dicRef["ClassTypes"] = listClassType;

            // Hình thức trúng tuyển
            List<object> listEnrolmentType = new List<object>();
            CommonList.EnrolmentTypeAvailable().ForEach(u => listEnrolmentType.Add(new { Value = u.value }));
            dicRef["EnrolmentTypes"] = listEnrolmentType;


            // Sheet Tỉnh/Thành
            List<Province> lstPro = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
            List<object> listProvince_Temp = new List<object>();
            int i = 1;
            int startRow = 4;
            foreach (var pr in lstPro)
            {
                sheetProvince.CopyPasteSameSize(rangeProvince, startRow, 1);
                listProvince_Temp.Add(new Dictionary<string, object>
                    {
                        {"Order", i++},
                        {"ProvinceCode", pr.ProvinceCode},
                        {"ProvinceName", pr.ProvinceName},

                    });
                startRow++;
            }
            // Fill Tỉnh/Thành
            Dictionary<string, object> dicSheetPro = new Dictionary<string, object>();
            dicSheetPro.Add("list", listProvince_Temp);
            sheetProvince.FillVariableValue(dicSheetPro);


            // Sheet Quận/Huyện           
            var lstDis = (from p in DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } })
                          join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                          select new
                          {
                              DistrictID = p.DistrictID,
                              DistrictName = p.DistrictName,
                              DistrictCode = p.DistrictCode,
                              ProvinceID = q.ProvinceID,
                              ProvinceName = q.ProvinceName
                          }).ToList();
            // Fill Quận/Huyện
            int startRow_Dis = 4;
            int order = 1;
            int beginRow_Dis = 4;
            foreach (var pr in lstPro)
            {
                int rowFirstByDis = 1;
                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count();
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        sheetDistrict.CopyPasteSameSize(rangeDistrict, startRow_Dis, 1);
                        sheetDistrict.SetCellValue(startRow_Dis, 1, order++);
                        if (rowFirstByDis == 1)
                        {
                            sheetDistrict.SetCellValue(startRow_Dis, 2, dis.ProvinceName);
                        }
                        sheetDistrict.SetCellValue(startRow_Dis, 3, dis.DistrictCode);
                        sheetDistrict.SetCellValue(startRow_Dis, 4, dis.DistrictName);
                        if (rowFirstByDis == numberOfDis)
                        {
                            IVTRange range = sheetDistrict.GetRange(beginRow_Dis, 2, beginRow_Dis + numberOfDis - 1, 2);
                            range.Merge();
                        }
                        startRow_Dis++;
                        rowFirstByDis++;
                    }
                    beginRow_Dis += numberOfDis;
                }
            }


            // Sheet Xã/Phường
            var lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            // Fill Xã/Phường
            int numProvince = 1;
            int startRow_Pro = 4;

            int startRow_Com = 5;
            order = 1;

            int beginRow_Com = 5;
            foreach (var pr in lstPro)
            {
                // Thông tin tỉnh
                sheetCommune.CopyPasteSameSize(rangeProOfCommune, startRow_Pro, 1);
                sheetCommune.SetCellValue(startRow_Pro, 1, numProvince++);
                sheetCommune.SetCellValue(startRow_Pro, 2, pr.ProvinceName);

                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count;
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        int rowFirstOfDis = 1;
                        var lstCommByDis = lstCommune.Where(o => o.DistrictID == dis.DistrictID).OrderBy(o => o.CommuneName).ToList();
                        int numOfComm = lstCommByDis.Count;
                        if (numOfComm > 0)
                        {
                            foreach (var cm in lstCommByDis)
                            {
                                sheetCommune.CopyPasteSameSize(rangeCommune, startRow_Com, 1);
                                sheetCommune.SetCellValue(startRow_Com, 1, order++);
                                if (rowFirstOfDis == 1)
                                {
                                    sheetCommune.SetCellValue(startRow_Com, 2, dis.DistrictCode);
                                    sheetCommune.SetCellValue(startRow_Com, 3, dis.DistrictName);
                                }
                                sheetCommune.SetCellValue(startRow_Com, 4, cm.CommuneCode);
                                sheetCommune.SetCellValue(startRow_Com, 5, cm.CommuneName);
                                if (rowFirstOfDis == numOfComm)
                                {
                                    sheetCommune.GetRange(beginRow_Com, 2, beginRow_Com + numOfComm - 1, 2).Merge();
                                    sheetCommune.GetRange(beginRow_Com, 3, beginRow_Com + numOfComm - 1, 3).Merge();
                                }
                                startRow_Com++;
                                rowFirstOfDis++;
                            }
                        }
                        beginRow_Com += numOfComm; // dòng tiến hành merge
                    }
                }
                // set lại biến đếm cho tỉnh tiếp theo
                startRow_Pro += order;
                startRow_Com++;
                beginRow_Com++;
                order = 1;
            }

            // Đối tượng chính sách  
            List<object> listPolicyTarget = new List<object>();
            var lstPOT = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPOT.ForEach(u => listPolicyTarget.Add(new { u.Resolution }));
            dicRef["PolicyTargets"] = listPolicyTarget;
            // Chế độ chính sách
            List<object> lstPolicyRegimes = new List<object>();
            var lstPR = PolicyRegimeBusiness.All.Where(x => x.IsActive).OrderBy(o => o.Resolution).ToList();
            lstPR.ForEach(u => lstPolicyRegimes.Add(new { u.Resolution }));
            dicRef["PolicyRegimes"] = lstPolicyRegimes;
            // Khu vực
            List<object> lstAreas = new List<object>();
            var listArea = AreaBusiness.All.Where(x => x.IsActive.HasValue && x.IsActive.Value == true).ToList();
            listArea.ForEach(u => lstAreas.Add(new { u.AreaName }));
            dicRef["Areas"] = lstAreas;
            // Loại khuyết tật
            List<object> lstDisabilityType = new List<object>();
            var listDisabilityType = DisabledTypeBusiness.All.Where(x => x.IsActive).OrderBy(o => o.Resolution).ToList();
            listDisabilityType.ForEach(u => lstDisabilityType.Add(new { u.Resolution }));
            dicRef["DisabilityType"] = lstDisabilityType;

            // Dân tộc
            List<object> listEthnic = new List<object>();
            List<Ethnic> lst1 = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.EthnicName).ToList();
            Ethnic e1 = lst1.FirstOrDefault(o => o.EthnicName.Contains("Kinh"));
            lst1.Remove(e1);
            lst1.Insert(0, e1);
            lst1.ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["Ethnics"] = listEthnic;

            // Tôn giáo
            List<object> listReligionObj = new List<object>();
            List<Religion> lstReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            Religion r1 = lstReligion.FirstOrDefault(o => o.Resolution.Contains("Không"));
            lstReligion.Remove(r1);
            lstReligion.Insert(0, r1);
            lstReligion.ForEach(u => listReligionObj.Add(new { u.Resolution }));
            dicRef["Religions"] = listReligionObj;

            List<object> listBloodType = new List<object>();
            CommonList.BloodType().ForEach(u => listBloodType.Add(new { Value = u.value }));
            dicRef["BloodTypes"] = listBloodType;

            List<object> listForeignLanguageTraining = new List<object>();
            CommonList.ForeignLanguageTraining().ForEach(u => listForeignLanguageTraining.Add(new { Value = u.value }));
            dicRef["ForeignLanguageTrainings"] = listForeignLanguageTraining;

            List<object> listProfileStatus = new List<object>();
            CommonList.PupilStatus().Where(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString()).ToList().ForEach(u => listProfileStatus.Add(new { Value = u.value }));
            dicRef["ProfileStatuses"] = listProfileStatus;

            List<object> listGenres = new List<object>();
            CommonList.GenreAndSelect().ForEach(u => listGenres.Add(new { Value = u.value }));
            dicRef["Genres"] = listGenres;

            refTemp.FillVariableValue(dicRef);
            // Set lai ten cac Sheet
            sheetProvince.Name = "TinhThanh";
            refSheet.Delete();

            sheetDistrict.Name = "QuanHuyen";
            refSheetDis.Delete();

            sheetCommune.Name = "XaPhuong";
            refSheetComm.Delete();

            sheetHDC.Name = "HuongDanChung";
            refHDC.Delete();

            #endregion

            //Neu la cap 1 thi an cot he hoc ngoai ngu
            //if (_globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)

            //an cot mo ta neu nhu download template mau                
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                ReportName = "Mau_Import_TH_ThongTinHocSinh.xls";
                sheet.HideColumn(19);// he ngoai ngu
                sheet.HideColumn(22);// doan
                sheet.HideColumn(23);// dang
                sheet.HideColumn(34);//cmnd
                sheet.HideColumn(35);//email
            }
            else
            {
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    ReportName = "Mau_Import_THCS_ThongTinHocSinh.xls";
                    sheet.HideColumn(23);// dang
                    sheet.HideColumn(34);//cmnd
                    sheet.HideColumn(35);//email
                }
                else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    ReportName = "Mau_Import_THPT_ThongTinHocSinh.xls";
                }
            }
            if (_globalInfo.TrainingType != 3)
            {
                sheet.HideColumn(20);// hinh thuc hoc
            }

            if (isErrFile)
            {
                List<ImportPupilViewModel> lstPupilImportedErr = new List<ImportPupilViewModel>();
                lstPupilImportedErr = (List<ImportPupilViewModel>)Session["LIST_IMPORTED_PUPIL"];
                if (lstPupilImportedErr != null)
                {
                    lstPupilImportedErr = lstPupilImportedErr.Where(p => p.IsValid == false).ToList();
                    ImportPupilViewModel objPupilErr = null;
                    int startRowErr = 5;
                    int startcol = 1;
                    for (int j = 0; j < lstPupilImportedErr.Count; j++)
                    {
                        objPupilErr = lstPupilImportedErr[j];
                        startcol = 1;
                        //STT
                        sheet.SetCellValue(startRowErr, startcol, j + 1);
                        //Ma HS
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PupilCode);
                        //Ho ten
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FullName);
                        //Ngay sinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.BirthDateStr);
                        //Gioi tinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.GenreName);
                        //Lop
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ClassName);
                        //Hinh thuc trung tuyen
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.EnrolmentTypeName);
                        //Ngay vao truong
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.EnrolmentDateStr);
                        //Trang thai hoc sinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ProfileStatusName);
                        //Tinh thanh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ProvinceName);
                        //Quan huyen
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.DistrictName);
                        //Xa,Phuong
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.CommuneName);
                        //Thon xom
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.VillageName);
                        //So dang bo
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.StorageNumber);
                        //Noi sinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.BirthPlace);
                        //Que quan
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.HomeTown);
                        //Dia chi thuong tru
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PermanentResidentalAddress);
                        //Dia chi tam tru
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PermanentResidentalAddress);
                        //He hoc ngoai ngu              
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ForeignLanguageTrainingName);
                        //Hinh thuc hoc               
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PupilLearningTypeName);
                        //Ngay vao doi
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.YoungPioneerJoinedDate);
                        //Ngay vao doan
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.YoungLeagueJoinedDate);
                        //Ngay vao dang
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.CommunistPartyJoinedDate);
                        //Dan toc
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.EthnicName);
                        //Ton giao
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ReligionName);
                        //Doi tuong chinh sach
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PolicyTargetName);
                        // Che do chinh sach
                        sheet.SetCellValue(startRowErr, ++startcol, "");
                        //Ho tro chi phi hoc tap
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsSupportForLearning == true ? "x" : "");
                        //Thuoc tai dinh cu
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsResettlementTarget == true ? "x" : "");
                        // Khu vuc
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.AreaName);
                        //Dien hoc sinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ClassTypeName);
                        //Loai khuyet tat
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.DisabilityTypeName);
                        //So dien thoai di dong
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.Mobile);
                        //So CMND
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IdentityNumber);
                        //Email
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.Email);
                        //Nhom mau
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.BloodTypeName);
                        //Hoc chuong trinh bo gia duc cua Bo
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsUsedMoetProgram == true ? "x" : "");
                        //Biet boi
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsSwimming == true ? "x" : "");
                        //Ho ten cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherFullName);
                        //Nam sinh cua cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherBirthDateStr);
                        //Nghe nghiep cua cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherJob);
                        //SDT cua cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherMobile);
                        //Email cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherEmail);
                        //Ho ten me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherFullName);
                        //Nam sinh cua me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherBirthDateStr);
                        //Nghe nghiep cua me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherJob);
                        //SDT cua me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherMobile);
                        //Email me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherEmail);
                        //Ho ten ng bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorFullName);
                        //Nam sinh cua ng bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorBirthDateStr);
                        //Nghe nghiep cua ng bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorJob);
                        //SDT cua ng bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorMobile);
                        //Email nguoi bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorEmail);
                        // co bo dan toc
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsMinorityFather == true ? "x" : "");
                        // co me dan toc
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsMinorityMother == true ? "x" : "");
                        //Ma HS moi
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PupilCodeNew);
                        //Mo ta
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ErrorMessage);

                        if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                        {
                            sheet.HideColumn(19);// he ngoai ngu
                            sheet.HideColumn(22);// doan
                            sheet.HideColumn(23);// dang
                            sheet.HideColumn(34);//cmnd
                            sheet.HideColumn(35);//email
                        }
                        else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                        {
                            sheet.HideColumn(23);// dang
                            sheet.HideColumn(34);//cmnd
                            sheet.HideColumn(35);//email
                        }
                        startRowErr++;
                    }
                    sheet.UnHideColumn(57);
                    sheet.GetRange(5, 1, 5 + lstPupilImportedErr.Count - 1, 57).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
            }

            return book.ToStream();
        }

        public FileResult ExportExcel(SearchViewModel frm)
        {
            if (!_globalInfo.IsSuperVisingDeptRole && !_globalInfo.IsSubSuperVisingDeptRole)
            {
                CheckActionPermissionMinView();
            }

            #region lay cac sheet ra
            GlobalInfo global = new GlobalInfo();
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", TEMPLATE_PUPILPROFILE_2018);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTinhThanh = oBook.GetSheet(2);
            IVTWorksheet sheetQuanHuyen = oBook.GetSheet(3);
            IVTWorksheet sheetXaPhuong = oBook.GetSheet(4);
            IVTWorksheet sheetref = oBook.GetSheet(6);
            #endregion

            #region lay ra cac lop
            var lsCP = new List<int>();
            //
            if (frm.Class.HasValue)
            {
                lsCP = new List<int>() { frm.Class.Value };
            }
            else
            {
                if (frm.EducationLevel.HasValue)
                {
                    lsCP = getClassFromEducationLevel(frm.EducationLevel.Value).Select(o => o.ClassProfileID).ToList();
                }
                else
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"AcademicYearID",global.AcademicYearID.Value}
                };

                    //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
                    if (!global.IsAdminSchoolRole)
                    {
                        dic["UserAccountID"] = global.UserAccountID;
                        dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                    }
                    var lsEducation = global.EducationLevels.Select(o => o.EducationLevelID).ToList();

                    lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).Where(o => lsEducation.Contains(o.EducationLevelID)).OrderBy(o => o.EducationLevelID).Select(o => o.ClassProfileID).ToList();
                }
            }
            #endregion

            #region dien du lieu cho sheet tinh thanh, quan huyen, xa phuong
            var lsProvince = ProvinceBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();

            var lstDis = (from p in DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } })
                          join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                          select new
                          {
                              DistrictID = p.DistrictID,
                              DistrictName = p.DistrictName,
                              DistrictCode = p.DistrictCode,
                              ProvinceID = q.ProvinceID,
                              ProvinceName = q.ProvinceName
                          }).ToList();

            var lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();

            IVTRange rangeDistrict = sheetQuanHuyen.GetRange("A4", "D4");
            int startRow_Dis = 4;
            int order = 1;
            int beginRow_Dis = 4;
            Province objProvince = null;

            IVTRange rangeProOfCommune = sheetXaPhuong.GetRange("A4", "E4");
            IVTRange rangeCommune = sheetXaPhuong.GetRange("A5", "E5");

            // Fill Xã/Phường
            int numProvince = 1;
            int startRow_Pro = 4;

            int startRow_Com = 5;
            int orderCom = 1;

            int beginRow_Com = 5;
            for (int i = 0; i < lsProvince.Count(); i++)
            {
                objProvince = lsProvince[i];

                sheetTinhThanh.CopyPaste(sheetTinhThanh.GetRange("A4", "C4"), i + 4, 1);
                sheetTinhThanh.SetCellValue("A" + (i + 4), (i + 1));
                sheetTinhThanh.SetCellValue("B" + (i + 4), objProvince.ProvinceCode);
                sheetTinhThanh.SetCellValue("C" + (i + 4), objProvince.ProvinceName);

                sheetXaPhuong.CopyPasteSameSize(rangeProOfCommune, startRow_Pro, 1);
                sheetXaPhuong.SetCellValue(startRow_Pro, 1, numProvince++);
                sheetXaPhuong.SetCellValue(startRow_Pro, 2, objProvince.ProvinceName);

                int rowFirstByDis = 1;
                var lstDisByPro = lstDis.Where(o => o.ProvinceID == objProvince.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count();
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        sheetQuanHuyen.CopyPasteSameSize(rangeDistrict, startRow_Dis, 1);
                        sheetQuanHuyen.SetCellValue(startRow_Dis, 1, order++);
                        sheetQuanHuyen.SetCellValue(startRow_Dis, 2, dis.ProvinceName);
                        sheetQuanHuyen.SetCellValue(startRow_Dis, 3, dis.DistrictCode);
                        sheetQuanHuyen.SetCellValue(startRow_Dis, 4, dis.DistrictName);

                        startRow_Dis++;
                        rowFirstByDis++;

                        int rowFirstOfDis = 1;
                        var lstCommByDis = lstCommune.Where(o => o.DistrictID == dis.DistrictID).OrderBy(o => o.CommuneName).ToList();
                        int numOfComm = lstCommByDis.Count;
                        if (numOfComm > 0)
                        {
                            foreach (var cm in lstCommByDis)
                            {
                                sheetXaPhuong.CopyPasteSameSize(rangeCommune, startRow_Com, 1);
                                sheetXaPhuong.SetCellValue(startRow_Com, 1, orderCom++);
                                sheetXaPhuong.SetCellValue(startRow_Com, 2, dis.DistrictCode);
                                sheetXaPhuong.SetCellValue(startRow_Com, 3, dis.DistrictName);
                                sheetXaPhuong.SetCellValue(startRow_Com, 4, cm.CommuneCode);
                                sheetXaPhuong.SetCellValue(startRow_Com, 5, cm.CommuneName);
                                startRow_Com++;
                                rowFirstOfDis++;
                            }
                        }
                        beginRow_Com += numOfComm; // dòng tiến hành merge

                    }
                    beginRow_Dis += numberOfDis;
                }
                startRow_Pro += orderCom;
                startRow_Com++;
                beginRow_Com++;
                orderCom = 1;
            }
            #endregion

            #region dien du lieu cho sheet dan toc
            //var lsEthnic = EthnicBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            //for (int i = 0; i < lsEthnic.Count(); i++)
            //{
            //    sheetDanToc.CopyPaste(sheetDanToc.GetRange("A4", "C4"), i + 4, 1);
            //    Ethnic e = lsEthnic[i];
            //    sheetDanToc.SetCellValue("A" + (i + 4), e.EthnicCode);
            //    sheetDanToc.SetCellValue("B" + (i + 4), e.EthnicName);
            //    sheetDanToc.SetCellValue("C" + (i + 4), e.Description);
            //}
            List<string> listEthnic = new List<string>();
            List<Ethnic> lst1 = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.EthnicName).ToList();
            Ethnic e1 = lst1.FirstOrDefault(o => o.EthnicName.Contains("Kinh"));
            lst1.Remove(e1);
            lst1.Insert(0, e1);
            lst1.ForEach(u =>
            {
                if (!string.IsNullOrEmpty(u.EthnicName))
                {
                    listEthnic.Add(u.EthnicName);
                }
            });
            #endregion

            #region dien du lieu cho sheet ton giao
            //var lsReligion = ReligionBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            //for (int i = 0; i < lsReligion.Count(); i++)
            //{
            //    sheetTonGiao.CopyPaste(sheetTonGiao.GetRange("A7", "B7"), i + 7, 1);
            //    Religion r = lsReligion[i];
            //    sheetTonGiao.SetCellValue("A" + (i + 7), r.Resolution);
            //    sheetTonGiao.SetCellValue("B" + (i + 7), r.Description);
            //}
            List<string> listReligionObj = new List<string>();
            List<Religion> lstReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            Religion r1 = lstReligion.FirstOrDefault(o => o.Resolution.Contains("Không"));
            lstReligion.Remove(r1);
            lstReligion.Insert(0, r1);
            lstReligion.ForEach(u =>
            {
                if (!string.IsNullOrEmpty(u.Resolution))
                {
                    listReligionObj.Add(u.Resolution);
                }
            });
            #endregion

            #region dien du lieu cho sheet doi tuong chinh sach
            //var lsPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            //for (int i = 0; i < lsPolicyTarget.Count(); i++)
            //{
            //    sheetDoiTuongChinhSach.CopyPaste(sheetDoiTuongChinhSach.GetRange("A4", "B4"), i + 4, 1);
            //    PolicyTarget p = lsPolicyTarget[i];
            //    sheetDoiTuongChinhSach.SetCellValue("A" + (i + 4), (i + 1));
            //    sheetDoiTuongChinhSach.SetCellValue("B" + (i + 4), p.Resolution);
            //}
            List<string> listPolicyTarget = new List<string>();
            var lstPOT = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPOT.ForEach(u =>
            {
                if (!string.IsNullOrEmpty(u.Resolution))
                {
                    listPolicyTarget.Add(u.Resolution);
                }
            });
            #endregion

            #region sheet du lieu hoc sinh tung lop

            #region fill thông tin học sinh
            //tuy theo tung lop ma fill excel theo cac tab
            IVTRange templateRange = sheet.GetRange("A1", "AE75");

            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            // Danh sách combobox
            KingDic.Add("PolicyTargets", listPolicyTarget);
            KingDic.Add("Ethnics", listEthnic);
            KingDic.Add("Religions", listReligionObj);

            //fill combobox doi tuong chinh sach,dan toc,ton giao
            int startrowref = 4;
            for (int i = 0; i < listPolicyTarget.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 1, listPolicyTarget[i]);
                startrowref++;
            }
            startrowref = 4;
            for (int i = 0; i < listEthnic.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 2, listEthnic[i]);
                startrowref++;
            }
            startrowref = 4;
            for (int i = 0; i < listReligionObj.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 3, listReligionObj[i]);
                startrowref++;
            }

            List<string> lstPolicyRemgimeType = new List<string> { "Giảm học phí", "Miễn học phí", "Khác" };
            startrowref = 4;
            for (int i = 0; i < lstPolicyRemgimeType.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 5, lstPolicyRemgimeType[i]);
                startrowref++;
            }
            List<string> lstArea = new List<string> { "Đô thị", "Đồng bằng", "Miền núi - vùng sâu", "Vùng cao – hải đảo" };
            startrowref = 4;
            for (int i = 0; i < lstArea.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 6, lstArea[i]);
                startrowref++;
            }

            List<object> QueenDic = new List<object>();
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["CurrentClassID"] = frm.Class;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["ProfileStatus"] = frm.Status;
            SearchInfo["EthnicID"] = frm.Ethnic;

            List<PupilProfileViewModel> lst = this.SearchPupilExport(SearchInfo, false, false, 1);
            PupilProfileViewModel thisModel = new PupilProfileViewModel();
            int totalPupil = lst.Count;
            for (int i = 0; i < totalPupil; i++)
            {
                thisModel = lst[i];
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["PupilCode"] = thisModel.PupilCode;
                princeDic["FullName"] = thisModel.FullName;
                princeDic["BirthDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.BirthDate);
                princeDic["Genre"] = thisModel.Genre == SystemParamsInFile.GENRE_MALE ? "" : "x";
                princeDic["Class"] = thisModel.ClassName;
                princeDic["SubCommitteeName"] = thisModel.SubCommitteeName;
                princeDic["ForeignLanguageTrainingName"] = thisModel.ForeignLanguageTraining.HasValue ? 
                                                        (thisModel.ForeignLanguageTraining == SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_3_YEARS ? Res.Get("Common_Label_ForeignLanguageTraining3")
                                                        : (thisModel.ForeignLanguageTraining == SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_7_YEARS ? Res.Get("Common_Label_ForeignLanguageTraining7") 
                                                        : (thisModel.ForeignLanguageTraining == SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_10_YEARS ? Res.Get("Common_Label_ForeignLanguageTraining10")
                                                        : (thisModel.ForeignLanguageTraining == SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED ? "" : "")))) 
                                                        : "";
                princeDic["PupilLearningType"] = thisModel.PupilLearningType.HasValue ? (thisModel.PupilLearningType.Value == SystemParamsInFile.PUPIL_LEARNING_TYPE_CENTRALIZING ? "Tập trung" :
                    thisModel.PupilLearningType.Value == SystemParamsInFile.PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING ? "Vừa học vừa làm" : thisModel.PupilLearningType.Value == SystemParamsInFile.PUPIL_LEARNING_TYPE_SELF_LEARNING ? "Tự học có hướng dẫn" : "") : "";

                princeDic["EnrolmentType"] = thisModel.EnrolmentType.HasValue ? (thisModel.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_PASSED_EXAMINATION ? Res.Get("Common_Label_EnrolmentTypePassExamination") 
                    : (thisModel.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL ? Res.Get("Common_Label_EnrolmentTypeMoveFromOtherSchool") 
                    : (thisModel.EnrolmentType ==  SystemParamsInFile.ENROLMENT_TYPE_SELECTED ? Res.Get("Common_Label_EnrolmentTypeSelected") 
                    : (thisModel.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_OTHER ? Res.Get("Common_Label_EnrolmentTypeOther") : "")))) 
                    : "";

                princeDic["EnrolmentDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.EnrolmentDate);
                princeDic["YoungPioneerJoinedDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.YoungPioneerJoinedDate);
                princeDic["YoungLeagueJoinedDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.YouthLeagueJoinedDate);
                princeDic["CommunistPartyJoinedDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.CommunistPartyJoinedDate);
                princeDic["Province"] = thisModel.ProvinceName;
                princeDic["District"] = thisModel.DistrictName;
                princeDic["Commune"] = thisModel.CommuneName;
                princeDic["Village"] = thisModel.VillageName;
                princeDic["StorageNumber"] = thisModel.StorageNumber;

                princeDic["PolicyTarget"] = thisModel.PolicyTargetName;
                princeDic["Ethnic"] = thisModel.EthnicName;
                princeDic["Religion"] = thisModel.ReligionName;
                princeDic["BirthPlace"] = thisModel.BirthPlace;
                princeDic["HomeTown"] = thisModel.HomeTown;

                princeDic["PermanentResidentalAddress"] = thisModel.PermanentResidentalAddress;
                princeDic["TempResidentalAddress"] = thisModel.TempResidentalAddress;
                princeDic["Mobile"] = thisModel.Mobile;
                princeDic["IdentifyNumber"] = thisModel.IdentifyNumber;
                princeDic["Email"] = thisModel.Email;
                princeDic["FatherFullName"] = thisModel.FatherFullName;
                princeDic["iFatherBirthDate"] = thisModel.FatherBirthDateHas.HasValue ? thisModel.FatherBirthDateHas.Value.Year.ToString() : "";
                princeDic["FatherBirthDate"] = thisModel.FatherBirthDate;
                princeDic["FatherJob"] = thisModel.FatherJob;
                princeDic["FatherMobile"] = thisModel.FatherMobile;
                princeDic["FatherEmail"] = thisModel.FatherEmail;
                princeDic["MotherFullName"] = thisModel.MotherFullName;
                princeDic["iMotherBirthDate"] = thisModel.MotherBirthDateHas.HasValue ? thisModel.MotherBirthDateHas.Value.Year.ToString() : "";
                princeDic["MotherBirthDate"] = thisModel.MotherBirthDate;
                princeDic["MotherJob"] = thisModel.MotherJob;
                princeDic["MotherMobile"] = thisModel.MotherMobile;
                princeDic["MotherEmail"] = thisModel.MotherEmail;
                princeDic["BloodType"] = thisModel.BloodType.HasValue ? (thisModel.BloodType == SystemParamsInFile.BLOOD_TYPE_A ? Res.Get("Common_Label_BloodTypeA")
                    : (thisModel.BloodType == SystemParamsInFile.BLOOD_TYPE_B ? Res.Get("Common_Label_BloodTypeB")
                    : (thisModel.BloodType == SystemParamsInFile.BLOOD_TYPE_AB ? Res.Get("Common_Label_BloodTypeAB")
                    : (thisModel.BloodType == SystemParamsInFile.BLOOD_TYPE_O ? Res.Get("Common_Label_BloodTypeO")
                    : (thisModel.BloodType == SystemParamsInFile.BLOOD_TYPE_OTHER ? Res.Get("Common_Label_BloodTypeOther")
                    : ""))))) 
                    : "";
                princeDic["IsSwimming"] = thisModel.IsSwimming == true ? "x" : "";
                princeDic["Status"] = thisModel.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING ? Res.Get("Common_Label_PupilStatusStuding")
                    : (thisModel.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED ? Res.Get("Common_Label_PupilStatusGraduated")
                    : thisModel.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL ? Res.Get("Common_Label_PupilStatusMoved")
                    : thisModel.ProfileStatus ==  SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF ? Res.Get("Common_Label_PupilStatusLeaved")
                    : thisModel.ProfileStatus ==  SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS ? Res.Get("Common_Label_PupilStatusMovedToOtherClass")
                    : "");

                //DucPT bo sung 082016
                princeDic["IsSupportForLearning"] = thisModel.IsSupportForLearning == true ? "x" : "";
                princeDic["IsSwimming"] = thisModel.IsSwimming == true ? "x" : "";
                princeDic["IsResettlementTarget"] = thisModel.IsResettlementTarget == true ? "x" : "";
                princeDic["ClassType"] = thisModel.ClassType.HasValue ? thisModel.ClassType == SystemParamsInFile.CLASSTYPE_NOITRU ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPENOITRU 
                    : (thisModel.ClassType == SystemParamsInFile.CLASSTYPE_BANTRU ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPEBANTRU 
                    : (thisModel.ClassType == SystemParamsInFile.CLASSTYPE_BANTRUDANNUOI ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPEBANTRUDANNUOI 
                    : (thisModel.ClassType == SystemParamsInFile.CLASSTYPE_NOITRUDANNUOI ? GlobalConstants.PUPILPROFILE_LABEL_CLASSTYPENOITRUDANNUOI : ""))) 
                    : "";

                princeDic["SponsorFullName"] = thisModel.SponsorFullName;
                princeDic["iSponsorBirthDate"] = thisModel.SponsorBirthDateHas.HasValue ? thisModel.SponsorBirthDateHas.Value.Year.ToString() : "";
                princeDic["SponsorBirthDate"] = thisModel.SponsorBirthDate;
                princeDic["SponsorJob"] = thisModel.SponsorJob;
                princeDic["SponsorMobile"] = thisModel.SponsorMobile;
                princeDic["SponsorEmail"] = thisModel.SponsorEmail;

                princeDic["PoylicyRegime"] = thisModel.PolicyRegimeID.HasValue ? (thisModel.PolicyRegimeID == 1 ? "Khác" : (thisModel.PolicyRegimeID == 2 ? "Miễn học phí" : "Giảm học phí")) : "";
                princeDic["AreaName"] = thisModel.AreaName;
                princeDic["DisabilityType"] = thisModel.DisabledTypeName;
                princeDic["UsedMoetProgram"] = thisModel.IsUsedMoetProgram.HasValue ? (thisModel.IsUsedMoetProgram.Value == true ? "x" : "") : "";
                princeDic["IsMinorityFather"] = thisModel.IsMinorityFather.HasValue ? (thisModel.IsMinorityFather.Value == true ? "x" : "") : "";
                princeDic["IsMinorityMother"] = thisModel.IsMinorityMother.HasValue ? (thisModel.IsMinorityMother.Value == true ? "x" : "") : "";

                QueenDic.Add(princeDic);
            }

            if (lst.Count > 2)
                for (int i = 0; i < lst.Count - 2; i++)
                    sheet.CopyAndInsertARow(6, 7);

            if (lst.Count == 1)
                sheet.DeleteRow(6);

            SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID);
            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SchoolName", sp.SchoolName);

            if (_globalInfo.AppliedLevel == 1)
            {
                sheet.HideColumn(19);// he ngoai ngu
                sheet.HideColumn(22);// doan
                sheet.HideColumn(23);// dang
                sheet.HideColumn(34);//cmnd
                sheet.HideColumn(35);//email
            }
            else
            {
                if (_globalInfo.AppliedLevel == 2)
                {
                    sheet.HideColumn(23);// dang
                    sheet.HideColumn(34);//cmnd
                    sheet.HideColumn(35);//email
                }
            }
            if (_globalInfo.TrainingType != 3)
            {
                sheet.HideColumn(20);// hinh thuc hoc
            }
            sheet.FillVariableValue(KingDic);
            #endregion

            #region Fill Thong tin combobox trong excel
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation = null;
            //Nhom mau
            string[] arrBloodType = new string[] { Res.Get("Blood_Type_A"), Res.Get("Blood_Type_B"), Res.Get("Blood_Type_AB"), Res.Get("Blood_Type_O") };
            objValidation = new VTDataValidation
            {
                Contrains = arrBloodType,
                FromColumn = 34,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 34,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);
            //He hoc ngoai ngu
            string[] arrLanguageMode = new string[] { "", Res.Get("Language_Mode_Three_Year"), Res.Get("Language_Mode_Seven_Year"), Res.Get("Language_Mode_Ten_Year") };
            objValidation = new VTDataValidation
            {
                Contrains = arrLanguageMode,
                FromColumn = 19,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 19,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);
            //Hinh thuc hoc
            string[] arrLearningType = new string[] { "", Res.Get("PupilLearningType_Centralizing"), Res.Get("PupilLearningType_Working_And_Learning"), Res.Get("PupilLearningType_Self_Learning") };
            objValidation = new VTDataValidation
            {
                Contrains = arrLearningType,
                FromColumn = 20,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 20,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);
            //Trang thai cua hoc sinh           
            List<string> listStatus = CommonList.PupilStatus().Select(p => p.value).ToList();
            string[] arrStatusPupil = listStatus.ToArray();
            objValidation = new VTDataValidation
            {
                Contrains = arrStatusPupil,
                FromColumn = 9,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 9,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };

            lstValidation.Add(objValidation);
            //Hinh thuc
            string[] arrEnrolmentType = new string[] { Res.Get("Common_Label_EnrolmentTypePassExamination"), Res.Get("Common_Label_EnrolmentTypeMoveFromOtherSchool"), Res.Get("Common_Label_EnrolmentTypeSelected"), Res.Get("Common_Label_EnrolmentTypeOther") };
            objValidation = new VTDataValidation
            {
                Contrains = arrEnrolmentType,
                FromColumn = 7,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 7,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);

            //Chế độ chính sách
            string[] arrPolicyRemgimeType = new string[] { "Giảm học phí", "Miễn học phí", "Khác" };
            objValidation = new VTDataValidation
            {
                Contrains = arrPolicyRemgimeType,
                FromColumn = 27,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 27,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);
            //Khu vực
            string[] arrArea = new string[] { "Đô thị", "Đồng bằng", "Miền núi - vùng sâu", "Vùng cao – hải đảo" };
            objValidation = new VTDataValidation
            {
                Contrains = arrArea,
                FromColumn = 30,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 30,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);

            //Loại khuyết tật
            string[] arrDisabilityType = new string[] { "Khiếm thính", "Khiếm thị", "Khó khăn về hoạt động", "Khó khăn về trí tuệ", "Đa tật", "Khác" };
            objValidation = new VTDataValidation
            {
                Contrains = arrDisabilityType,
                FromColumn = 32,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 32,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);

            //Học chương trình giá dục của Bộ
            string[] arrUsedMoetProgram = new string[] { "x" };
            objValidation = new VTDataValidation
            {
                Contrains = arrUsedMoetProgram,
                FromColumn = 37,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 37,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);
            //Bố là dân tộc
            string[] arrMinorityFather = new string[] { "x" };
            objValidation = new VTDataValidation
            {
                Contrains = arrMinorityFather,
                FromColumn = 54,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 54,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);
            //Mẹ là dân tộc
            string[] arrMinorityMother = new string[] { "x" };
            objValidation = new VTDataValidation
            {
                Contrains = arrMinorityMother,
                FromColumn = 55,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 55,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);
            #endregion

            Stream excel = oBook.ToStreamValidationData(lstValidation);
            #endregion

            SchoolProfile sp1 = SchoolProfileBusiness.Find(global.SchoolID);
            EducationLevel el = EducationLevelBusiness.Find(frm.EducationLevel);
            ClassProfile cp = ClassProfileBusiness.Find(frm.Class);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportUtils.StripVNSign(string.Format(TEMPLATE_PUPILPROFILE_FORMAT, (el != null ? "_" + SMAS.Business.Common.Utils.RemoveUnprintableChar(el.Resolution) : ""), (cp != null ? "_" + SMAS.Business.Common.Utils.RemoveUnprintableChar(cp.DisplayName) : ""), "_" + SMAS.Business.Common.Utils.RemoveUnprintableChar(sp1.SchoolName)));

            // Tạo dữ liệu ghi log
            String param = "{\"EducationLevel\":\"" + frm.EducationLevel + "\",\"Class\":\"" + frm.Class + "\", \"Genre\":\"" + frm.Genre + "\",\"PupilCode\":\"" + frm.PupilCode + "\",\"Fullname\":\"" + frm.Fullname + "\",\"Status\":\"" + frm.Status + "\"}";
            SetViewDataActionAudit(String.Empty, String.Empty, String.Empty, "Export excel pupil_profile", param, "Hồ sơ học sinh", GlobalConstants.ACTION_EXPORT, "Xuất danh sách học sinh");

            return result;
        }

        private List<PupilProfileViewModel> SearchPupilExport(IDictionary<string, object> SearchInfo, bool allowPaging, bool sort, int page = 1)
        {
            List<int> lstClassID = getClassFromEducationLevel(0).Select(u => u.ClassProfileID).ToList();
            if (!_globalInfo.IsAdminSchoolRole)
            {
                SearchInfo["ListNullableClassID"] = lstClassID;
            }
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<PupilLeavingOff> lstPupilLeavingOff = new List<PupilLeavingOff>();
            lstPupilLeavingOff = PupilLeavingOffBusiness.All.Where(p => p.AcademicYearID == _globalInfo.AcademicYearID && p.SchoolID == _globalInfo.SchoolID).ToList();

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            string no = Res.Get("Common_Label_NoChoice");

            IQueryable<PupilProfileViewModel> lsPupilProfileViewModel = this.PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo)
                                                                        .Select(o => new PupilProfileViewModel
                                                                        {
                                                                            ClassName = o.CurrentClassName,
                                                                            AreaID = o.AreaID,
                                                                            AreaName = o.AreaName,
                                                                            ProvinceID = o.ProvinceID,
                                                                            ProvinceName = o.ProvinceName,
                                                                            DistrictID = o.DistrictID,
                                                                            DistrictName = o.DistrictName,
                                                                            CommuneID = o.CommuneID,
                                                                            CommuneName = o.CommuneName,
                                                                            IdentifyNumber = o.IdentifyNumber,
                                                                            EthnicID = o.EthnicID,
                                                                            OtherEthnicID = o.OtherEthnicID,
                                                                            EthnicName = o.EthnicName,
                                                                            ReligionID = o.ReligionID,
                                                                            ReligionName = o.ReligionName,
                                                                            PolicyTargetID = o.PolicyTargetID,
                                                                            PolicyTargetName = o.PolicyTargetName,
                                                                            FamilyTypeID = o.FamilyTypeID,
                                                                            FamilyTypeName = o.FamilyName,
                                                                            PriorityTypeID = o.PriorityTypeID,
                                                                            PriorityTypeName = o.PriorityTypeName,
                                                                            PreviousGraduationLevelID = o.PreviousGraduationLevelID,                                                                          
                                                                            PupilCode = o.PupilCode,
                                                                            EducationLevel = o.EducationLevelID,
                                                                            FullName = o.FullName,
                                                                            Name = o.Name,
                                                                            Genre = o.Genre,
                                                                            BirthDate = o.BirthDate,
                                                                            BirthPlace = o.BirthPlace,
                                                                            HomeTown = o.HomeTown,
                                                                            HomePlace = o.HomePlace,
                                                                            Telephone = o.Telephone,
                                                                            Mobile = o.Mobile,
                                                                            Email = o.Email,
                                                                            TempResidentalAddress = o.TempResidentalAddress,
                                                                            PermanentResidentalAddress = o.PermanentResidentalAddress,
                                                                            IsSwimming = (o.IsSwimming.HasValue && o.IsSwimming.Value),
                                                                            IsResidentIn = o.IsResidentIn,
                                                                            IsDisabled = (o.IsDisabled.HasValue) && o.IsDisabled.Value,
                                                                            DisabledTypeID = o.DisabledTypeID,
                                                                            DisabledTypeName = o.DisabledTypeName,
                                                                            DisabledSeverity = o.DisabledSeverity,
                                                                            BloodType = o.BloodType,                                                                           
                                                                            EnrolmentMark = o.EnrolmentMark,
                                                                            EnrolmentDate = o.EnrolmentDate,
                                                                            ForeignLanguageTraining = o.ForeignLanguageTraining,             
                                                                            IsYoungPioneerMember = (o.IsYoungPioneerMember.HasValue) && o.IsYoungPioneerMember.Value,
                                                                            YoungPioneerJoinedDate = o.YoungPioneerJoinedDate,
                                                                            YoungPioneerJoinedPlace = o.YoungPioneerJoinedPlace,
                                                                            IsYouthLeageMember = (o.IsYouthLeageMember.HasValue) && o.IsYouthLeageMember.Value,
                                                                            YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
                                                                            YouthLeagueJoinedPlace = o.YouthLeagueJoinedPlace,
                                                                            IsCommunistPartyMember = (o.IsCommunistPartyMember.HasValue) && o.IsCommunistPartyMember.Value,
                                                                            CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
                                                                            CommunistPartyJoinedPlace = o.CommunistPartyJoinedPlace,
                                                                            FatherFullName = o.FatherFullName,
                                                                            FatherBirthDate = o.FatherBirthDate,
                                                                            FatherJob = o.FatherJob,
                                                                            FatherMobile = o.FatherMobile,
                                                                            FatherEmail = o.FatherEmail,
                                                                            MotherFullName = o.MotherFullName,
                                                                            MotherBirthDate = o.MotherBirthDate,
                                                                            MotherJob = o.MotherJob,
                                                                            MotherMobile = o.MotherMobile,
                                                                            MotherEmail = o.MotherEmail,
                                                                            SponsorFullName = o.SponsorFullName,
                                                                            SponsorBirthDate = o.SponsorBirthDate,
                                                                            SponsorJob = o.SponsorJob,
                                                                            SponsorMobile = o.SponsorMobile,
                                                                            SponsorEmail = o.SponsorEmail,
                                                                            ProfileStatus = o.ProfileStatus,
                                                                            EnrolmentType = o.EnrolmentType,
                                                                            OrderInClass = o.OrderInClass,
                                                                            PolicyRegimeID = o.PolicyRegimeID,
                                                                            IsSupportForLearning = o.IsSupportForLearning,
                                                                            IsFatherSMS = (o.IsFatherSMS.HasValue && o.IsFatherSMS.Value),
                                                                            IsMotherSMS = (o.IsMotherSMS.HasValue && o.IsMotherSMS.Value),
                                                                            IsSponsorSMS = (o.IsSponsorSMS.HasValue && o.IsSponsorSMS.Value),
                                                                            PupilLearningType = o.PupilLearningType,                                                                         
                                                                            ItCertificate = o.ItCertificate,
                                                                            LanguageCertificate = o.LanguageCertificate,
                                                                            ItCertificateID = o.ItCertificateID,
                                                                            LanguageCertificateID = o.LanguageCertificateID,
                                                                            SubCommitteeID = o.SubCommitteeID,
                                                                            SubCommitteeName = o.SubCommitteeName,
                                                                            VillageID = o.VillageID,
                                                                            VillageName = o.VillageName,
                                                                            StorageNumber = o.StorageNumber,
                                                                            ClassOrderNumber = o.ClassOrderNumber,
                                                                            AppliedLevelID = _globalInfo.AppliedLevel,
                                                                            SupportingPolicy = o.SupportingPolicy,                                                                                                                                                                                                                        
                                                                            IsUsedMoetProgram = o.IsUsedMoetProgram,
                                                                            IsMinorityFather = o.IsMinorityFather,
                                                                            IsMinorityMother = o.IsMinorityMother,
                                                                            IsResettlementTarget = (o.IsResettlementTarget.HasValue && o.IsResettlementTarget.Value),
                                                                            FatherBirthDateHas = o.FatherBirthDate,
                                                                            MotherBirthDateHas = o.MotherBirthDate,
                                                                            SponsorBirthDateHas = o.SponsorBirthDate,
                                                                            ClassType = o.ClassType
                                                                        });

            int ClassID = SMAS.Business.Common.Utils.GetInt(SearchInfo, "CurrentClassID");
            if (ClassID > 0)
                lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(o => o.FullName);
            else
                lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.EducationLevel)
                                                                    .ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0)
                                                                    .ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass)
                                                                    .ThenBy(o => o.Name).ThenBy(o => o.FullName);
            var lstPupilInfo = lsPupilProfileViewModel.ToList();
            return lstPupilInfo;
        }

        #region

        private void CopyValue(PupilProfile pupilProfile, ImportPupilViewModel pupil)
        {
            pupilProfile.BirthDate = pupil.BirthDate.Value;
            pupilProfile.BirthPlace = pupil.BirthPlace;
            pupilProfile.BloodType = pupil.BloodType;
            pupilProfile.CreatedDate = DateTime.Now;
            pupilProfile.CurrentClassID = pupil.CurrentClassID;
            pupilProfile.CurrentSchoolID = pupil.CurrentSchoolID;
            pupilProfile.EnrolmentDate = pupil.EnrolmentDate.Value;
            pupilProfile.EthnicID = pupil.EthnicID;
            pupilProfile.FatherBirthDate = pupil.FatherBirthDate;
            pupilProfile.FatherEmail = pupil.FatherEmail;
            pupilProfile.FatherFullName = pupil.FatherFullName;
            pupilProfile.FatherJob = pupil.FatherJob;
            pupilProfile.FatherMobile = pupil.FatherMobile;
            pupilProfile.ForeignLanguageTraining = pupil.ForeignLanguageTraining;
            pupilProfile.PupilLearningType = pupil.PupilLearningType;
            pupilProfile.FullName = pupil.FullName;
            pupilProfile.Genre = pupil.Genre;
            pupilProfile.HomeTown = pupil.HomeTown;
            pupilProfile.IdentifyNumber = pupil.IdentityNumber;
            pupilProfile.Email = pupil.Email;
            pupilProfile.MotherBirthDate = pupil.MotherBirthDate;
            pupilProfile.MotherEmail = pupil.MotherEmail;
            pupilProfile.MotherFullName = pupil.MotherFullName;
            pupilProfile.MotherJob = pupil.MotherJob;
            pupilProfile.MotherMobile = pupil.MotherMobile;
            pupilProfile.Mobile = pupil.Mobile;
            pupilProfile.Name = pupil.Name;
            pupilProfile.StorageNumber = pupil.StorageNumber;
            pupilProfile.PermanentResidentalAddress = pupil.PermanentResidentalAddress;
            pupilProfile.PolicyTargetID = pupil.PolicyTargetID;
            pupilProfile.ProfileStatus = pupil.ProfileStatus;
            if (pupil.ProvinceID != null) pupilProfile.ProvinceID = (int)pupil.ProvinceID;
            pupilProfile.PupilCode = !string.IsNullOrEmpty(pupil.PupilCodeNew) ? pupil.PupilCodeNew : pupil.PupilCode;
            pupilProfile.ReligionID = pupil.ReligionID;
            pupilProfile.TempResidentalAddress = pupil.TempResidentalAddress;
            //Do import moi cap nhat lai ProvinceID nen de DistrictID và CommuneID la null
            pupilProfile.CommuneID = pupil.CommuneID;
            pupilProfile.DistrictID = pupil.DistrictID;
            pupilProfile.VillageID = pupil.VillageID;
            //them doi, doan, dang
            if (pupil.YoungPioneerJoinedDate != null)
            {
                pupilProfile.YoungPioneerJoinedDate = pupil.YoungPioneerJoinedDate;
                pupilProfile.IsYoungPioneerMember = true;
            }
            if (pupil.YoungLeagueJoinedDate != null)
            {
                pupilProfile.YouthLeagueJoinedDate = pupil.YoungLeagueJoinedDate;
                pupilProfile.IsYouthLeageMember = true;
            }
            if (pupil.CommunistPartyJoinedDate != null)
            {
                pupilProfile.CommunistPartyJoinedDate = pupil.CommunistPartyJoinedDate;
                pupilProfile.IsCommunistPartyMember = true;
            }
            //Cap nhat 08/2016
            pupilProfile.ClassType = pupil.ClassType;
            pupilProfile.IsSupportForLearning = pupil.IsSupportForLearning;
            pupilProfile.IsResettlementTarget = pupil.IsResettlementTarget;
            pupilProfile.IsSwimming = pupil.IsSwimming;

            pupilProfile.SponsorBirthDate = pupil.SponsorBirthDate;
            pupilProfile.SponsorFullName = pupil.SponsorFullName;
            pupilProfile.SponsorJob = pupil.SponsorJob;
            pupilProfile.SponsorMobile = pupil.SponsorMobile;
            pupilProfile.SponsorEmail = pupil.SponsorEmail;

            pupilProfile.MinorityFather = pupil.IsMinorityFather;
            pupilProfile.MinorityMother = pupil.IsMinorityMother;
            pupilProfile.UsedMoetProgram = pupil.IsUsedMoetProgram;
            pupilProfile.IsDisabled = pupil.IsDisabled;
            pupilProfile.DisabledTypeID = pupil.DisabledTypeID;
            pupilProfile.AreaID = pupil.AreaID;
            pupilProfile.PolicyRegimeID = pupil.PolicyRegimeID;
        }

        private void ValidateImportedPupil(ImportPupilViewModel pupil)
        {
            ValidationContext vc = new ValidationContext(pupil, null, null);
            List<string> messages = new List<string>();
            foreach (PropertyInfo pi in pupil.GetType().GetProperties())
            {
                string resKey = string.Empty;
                if (pi.Name == "FatherBirthDate" || pi.Name == "MotherBirthDate" || pi.Name == "BirthDate" || pi.Name == "EnrolmentDate"
                    || pi.Name == "YoungPioneerJoinedDate" || pi.Name == "YoungLeagueJoinedDate" || pi.Name == "CommunistPartyJoinedDate"
                    || pi.Name == "Email" || pi.Name == "FatherEmail" || pi.Name == "MotherEmail" || pi.Name == "SponsorEmail" || pi.Name == "ForeignLanguageTraining")
                {
                    continue;
                }
                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is DisplayAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }

                    if (att is DisplayNameAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }
                }

                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is ValidationAttribute)
                    {
                        ValidationAttribute attV = (ValidationAttribute)att;
                        vc.MemberName = pi.Name;
                        if (attV.GetValidationResult(pi.GetValue(pupil, null), vc) != ValidationResult.Success)
                        {
                            List<object> Params = new List<object>();
                            Params.Add(Res.Get(resKey));

                            if (att is StringLengthAttribute)
                            {
                                StringLengthAttribute attS = (StringLengthAttribute)att;
                                Params.Add(attS.MaximumLength);
                            }

                            messages.Add(string.Format(Res.Get(attV.ErrorMessageResourceName), Params.ToArray()));
                        }
                    }
                }
            }

            if (messages.Count > 0)
            {
                pupil.IsValid = false;
                pupil.ErrorMessage += string.Join(SEMICOLON_SPACE, messages);
                pupil.ErrorMessage += SEMICOLON_SPACE;
            }
        }

        private bool checkCompareDistrict(int? provinceID, int? districtID, List<District> lstDistrict)
        {
            if (districtID == null) return true;
            if (districtID != null && provinceID == null) return false;
            int check = lstDistrict.Where(p => p.ProvinceID == provinceID && p.DistrictID == districtID).Count();
            if (check > 0)
                return true;
            return false;
        }
        private bool checkCompareCommune(int? districtID, int? communeID, List<Commune> lstCommune)
        {
            if (communeID == null || districtID == null)
                return true;
            if (communeID != null && districtID == null) return false;
            int check = lstCommune.Where(p => p.DistrictID == districtID && p.CommuneID == communeID).Count();
            if (check > 0)
                return true;
            return false;
        }
        private bool checkCompareVillage(int? villiageID, int? communeID)
        {
            if (villiageID == null || communeID == null) return true;
            if (villiageID != null && communeID == null) return false;
            var check = VillageBusiness.Search(new Dictionary<string, object> { { "CommuneID", communeID }, { "Villiage", villiageID }, { "IsActive", true } });
            if (check.Count() > 0)
                return true;
            return false;
        }

        private string ToStringWithFixedLength(int number, int length)
        {
            if (number < 0) return string.Empty;

            if (number.ToString().Length > length) return string.Empty;

            return number.ToString().PadLeft(length, '0');
        }

        private int? SetNulableFromList(List<ComboObject> list, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty) return null;

            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToInt32(c.key);
        }

        private int SetForeignLanguageTraining(string value)
        {
            GlobalInfo glo = new GlobalInfo();
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty)
                return (int)SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED;
            ComboObject c = CommonList.ForeignLanguageTraining().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetPupilLearningType(string value)
        {
            GlobalInfo glo = new GlobalInfo();
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty)
                return 0;
            ComboObject c = CommonList.PupilLearningType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetProfileStatus(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = CommonList.PupilStatus().SingleOrDefault(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString() && u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetEnrolementType(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            ComboObject c = CommonList.EnrolmentType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? 0 : Convert.ToByte(c.key);
        }

        private int? SetBloodType(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty) return null;

            ComboObject c = CommonList.BloodType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private string GetPupilName(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
                return string.Empty;

            fullName = fullName.Trim();

            int index = fullName.LastIndexOf(' ');

            return index > 0 ? fullName.Substring(index + 1) : fullName;
        }

        private int? GetCellIndex(string value)
        {
            int val = 0;
            if (string.IsNullOrEmpty(value) || value.Trim() == "" || !int.TryParse(value.Trim(), out val) || !Regex.IsMatch(value.Trim(), "^\\d+$"))
                return null;
            if (val > 0 && val < 10000)
            {
                return val;
            }
            return null;
        }

        private string GetCellString(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }

        private int SetFromList(List<ComboObject> list, string value)
        {
            value = (value == null ? string.Empty : value.Trim().ToLower());
            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? 0 : Convert.ToInt32(c.key);
        }

        private DateTime? GetCellDate(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            var culture = new CultureInfo("vi-VN");
            if (o != null)
            {
                try
                {
                    if (o is DateTime)
                        return (DateTime)o;
                    var oStr = o.ToString().Trim();
                    if (oStr.EndsWith(".00"))
                        oStr = oStr.Remove(oStr.Length - 4, 3);
                    if (oStr.EndsWith(".0"))
                        oStr = oStr.Remove(oStr.Length - 3, 2);

                    DateTime dt;

                    DateTime.TryParse(oStr, out dt);
                    return dt;
                }
                catch { }
            }
            return null;
        }
        #endregion
        #endregion
    }
}
