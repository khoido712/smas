﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilProfileArea
{
    public class PupilProfileAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilProfileArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilProfileArea_default",
                "PupilProfileArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}