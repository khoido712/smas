/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.PupilProfileArea
{
    public class PupilProfileConstants
    {
        public const string BOOL_IS_FIRST_TIME = "BOOL_IS_FIRST_TIME";
        public const string DETAIL_FULL_NAME = "DETAIL_FULL_NAME";
        public const string LIST_PUPILPROFILE = "listPupilProfile";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTCLASS = "LISTCLASS";
        public const string LISTGENRE = "LISTGENRE";
        public const string LISTSTATUS = "LISTSTATUS";

        public const string LIST_ACADEMICYEAR = "LIST_ACADEMICYEAR";
        public const string LIST_HEALTH_PERIODIC = "LIST_HEALTH_PERIODIC";
        public const string SEMESTER_CHECKED = "SEMESTER_CHECKED";
        public const string ACADEMIC_YEAR = "ACADEMIC_YEAR";
        public const string LISTSEMESTER = "LISTSEMESTER";
        public const string LISTETHNIC = "LISTETHNIC";//
        public const string LISTOTHERETHNIC = "LISTOTHERETHNIC";
        public const string LISTRELIGION = "LISTRELIGION";//
        public const string LISTPOLICYTARGET = "LISTPOLICYTARGET";//
        public const string LISTAREA = "LISTAREA";//all
        public const string LISTPRIORITYTYPE = "LISTPRIORITYTYPE";//all
        public const string LISTPROVINCE = "LISTPROVINCE";//all
        public const string LISTDISTRICT = "LISTDISTRICT";//dua vao provide
        public const string LISTCOMMUNE = "LISTCOMMUNE";//dua vao district
        public const string LISTDISABLEDTYPE = "LISTDISABLEDTYPE";//all
        public const string LISTLEAVINGREASON = "LISTLEAVINGREASON";// ly do thoi hoc
        public const string LIST_EXEMPTED_OBJECT = "LIST_EXEMPTED_OBJECT"; //Hinh thuc mien giam
        public const string LIST_EXEMPTED_SUBJECT = "LIST_EXEMPTED_SUBJECT"; //Cac mon duoc mien giam
        public const string LIST_SUBJECT_GRID = "LIST_SUBJECT_GRID"; //Cac mon hoc cua lop
        public const string LIST_EXEMPTED_TYPE = "LIST_EXEMPTED_TYPE"; //Cac mon hoc cua lop
        public const string LIST_EXEMPTED_TYPE_PRACTICE = "LIST_EXEMPTED_TYPE_PRACTICE"; //Cac mon hoc cua lop
        public const string LIST_EXEMPTED_TYPE_WITHOUT_OTHER = "LIST_EXEMPTED_TYPE_WITHOUT_OTHER"; //Cac mon hoc cua lop

        public const string LISTBLOODTYPE = "LISTBLOODTYPE";//
        public const string LISTFAMILYTYPE = "LISTFAMILYTYPE";//all
        public const string LISTENROLMENTTYPE = "LISTENROLMENTTYPE";
        public const string LISTCLASSTYPE = "LISTCLASSTYPE";
        public const string LISTHOMEPLACE = "LISTHOMEPLACE";
        public const string LISTSUPPORTINGPOLICY = "LISTSUPPORTINGPOLICY";
        public const string LISTPREVIOUSGRADUATIONLEVEL = "LISTPREVIOUSGRADUATIONLEVEL";//
        public const string LISTFOREIGNLANGUAGETRAINING = "LISTFOREIGNLANGUAGETRAINING";//
        public const string LIST_FOREIGN_LANGUAGE_CERTIFICATE = "LIST_FOREIGN_LANGUAGE_CERTIFICATE";//
        public const string LIST_IT_CERTIFICATE = "LIST_IT_CERTIFICATE";//
        public const string LIST_PUPIL_LEARNING_TYPE = "LIST_PUPIL_LEARNING_TYPE";//

        public const string LIST_PARTICULARPUPIL_TREATMENT = "LIST_PARTICULARPUPIL_TREATMENT";//
        public const string LIST_SCHOOL_MOVEMENT = "LIST_SCHOOL_MOVEMENT";//
        public const string DIC_CLASS_BY_EDUCATION_LEVEL = "DIC_CLASS_BY_EDUCATION_LEVEL";//
        public const string LIST_PUPIL_TRANSCRIPT = "LIST_PUPIL_TRANSCRIPT";//bang diem hoc sinh
        public const string LIST_PUPIL_VIOLATION = "LIST_PUPIL_VIOLATION";
        public const string LIST_PUPIL_ABSENCE = "LIST_PUPIL_ABSENCE";

        public const string CLASS_PROFILE_NAME = "CLASS_PROFILE_NAME";
        
        //an hien autogencode
        public const string AUTO_GEN_CODE_CHECK = "AUTO_GEN_CODE_CHECK";

        public const string IS_SCHOOL_MODIFIED = "IS_SCHOOL_MODIFIED";
        public const string PUPILPROFILE_MODEL_FOR_DETAIL = "PUPILPROFILE_MODEL_FOR_DETAIL";
        //
        public const string SEARCHFORM = "SEARCHFORM";

        public const string VISIBLE_ORDER = "VISIBLE_ORDER";

        public const string ENABLE_PAGING = "ENABLE_PAGING";
        public const string PUPIL_NOT_STUDY = "PUPIL_NOT_STUDY";
        public const string LISTPOLICYREGIME = "LISTPOLICYREGIME";
        public const string ENABLE_CKB_POLICYREGIME = "ENABLE_CKB_POLICYREGIME";

        public const string LIST_PUPIL_PRAISE = "LIST_PUPIL_PRAISE";
        public const string LIST_PUPIL_DISCIPLINE = "LIST_PUPIL_DISCIPLINE";
        public const string LIST_DETAIL_PUPIL_PRAISE = "LIST_DETAIL_PUPIL_PRAISE";
        public const string LIST_DETAIL_PUPIL_DISCIPLINE = "LIST_DETAIL_PUPIL_DISCIPLINE";
        public const string LIST_PUPIL_HLHK = "LIST_PUPIL_HLHK";
        public const string LIST_PUPIL_QTLL = "LIST_PUPIL_QTLL";
        public const string LIST_PUPIL_CLDH = "LIST_PUPIL_CLDH";
        public const string LIST_PUPIL_CLDH_VNM = "LIST_PUPIL_CLDH_VNM";
        public const string LIST_PRAISE_TYPE = "LIST_PRAISE_TYPE";
        public const string LIST_PRAISE_LEVEL = "LIST_PRAISE_LEVEL";
        public const string LIST_DISCIPLINE_TYPE = "LIST_DISCIPLINE_TYPE";
        public const string LIST_DISCIPLINE_LEVEL = "LIST_DISCIPLINE_LEVEL";
        public const string LIST_PUPIL_RESERVE = "LIST_PUPIL_RESERVE";
        public const string LIST_ACCEPT_CLASS = "LIST_ACCEPT_CLASS";
        public const string IS_ENABLE_NOTE_PRAISE = "IsEnableNotePraise";
        public const string IS_ENABLE_NOTE_DISCIPLINE = "IsEnableNoteDiscipline";
        //School movement
        public const int SCHOOL_MOVEMENT_IN_SYSTEM = 1;
        public const int SCHOOL_MOVEMENT_OUT_SYSTEM = 2;
        public const string IS_SHOW_AVATAR = "IS_SHOW_AVATAR";
        public const string IS_NAVIGATE_FROM_OTHER_PAGE = "IS_NAVIGATE_FROM_OTHER_PAGE";
        //Absence
        public const string HAS_MORNING = "HAS_MORNING";
        public const string HAS_AFTERNOON = "HAS_AFTERNOON";
        public const string HAS_EVENING = "HAS_EVENING";

        public const string DETAIL_HEALTH_PERIODIC_PUPIL = "DETAIL_HEALTH_PERIODIC_PUPIL";
        public const string HAS_RIGHT_PERMISSION = "HAS_RIGHT_PERMISSION";

        public const string IS_GDTX_SCHOOL = "IS_GDTX_SCHOOL";
        public const string ENABLE_CREATE_DELETE = "ENABLE_CREATE_DELETE";
        public const string DICTIONARY_TABS_PERMISSION = "DICTIONARY_TABS_PERMISSION";
        public const string FirstStartDate = "FirstStartDate";
        public const string ENABLE_IMPORT_BUTTON_TEACHER = "ENABLE_IMPORT_BUTTON_TEACHER";
    }
}