﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilParticularModel
    { 
        public int ParticularPupilID { get; set; }

        public int ParticularPupilCharacteristicID { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_ListPupilName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PupilID { get; set; }
        
        [ResourceDisplayName("ParticularPupil_Column_RealUpdatedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]	
        public DateTime RealUpdatedDate { get; set; }

        [ResourceDisplayName("ParticularPupilCharacteristic_Label_ChangesNoteOfPupil")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.MultilineText)]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string FamilyCharacteristic { get; set; }

        [ResourceDisplayName("ParticularPupilCharacteristic_Label_PsychologyCharacteristic")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.MultilineText)]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string PsychologyCharacteristic { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_PupilName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_Class")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ClassName { get; set; }
    }
}