﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PraisePupilViewModel
    {
        [ResourceDisplayName("ClassProfile_Label_ClassProfileID")]
        public int ClassID { get; set; }

        public int? ClassOrderNumber { get; set; }
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("ClassProfile_Label_DisplayName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilProfileID")]
        public int PupilID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Name")]
        public string Name { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime BirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public int Genre { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreDisplay { get; set; }

        [ResourceDisplayName("PupilProfile_Label_OrderInClass")]
        public int? OrderInClass { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseTotal")]
        public int PraiseTotal { get; set; }
    }
}