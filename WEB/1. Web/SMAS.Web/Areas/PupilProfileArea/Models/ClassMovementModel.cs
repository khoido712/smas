﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class ClassMovementModel
    {
        public int ClassMovementID { get; set; }

        public int PupilID { get; set; }

        public int EducationLevelID { get; set; }

        [ResourceDisplayName("ClassMovement_Label_FromClass")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int FromClassID { get; set; }

        [ResourceDisplayName("ClassMovement_Label_ToClass")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ToClassID { get; set; }

        [ResourceDisplayName("ClassMovement_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("ClassMovement_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("ClassMovement_Label_MovedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]	
        public DateTime MovedDate1 { get; set; }

        [ResourceDisplayName("ClassMovement_Label_Semester")]
        public int? Semester { get; set; }

        [ResourceDisplayName("ClassMovement_Label_Reason")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Reason { get; set; }
        public bool? isClassMovement { get; set; }
    }
}