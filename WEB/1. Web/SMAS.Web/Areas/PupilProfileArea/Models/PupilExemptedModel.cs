﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilExemptedModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LISTEDUCATIONLEVEL)]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ClassID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassID { get; set; }

        [ResourceDisplayName("ClassProfile_Label_DisplayName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_PupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PupilID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ExemptedObjectID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LIST_EXEMPTED_OBJECT)]
        public int ExemptedObjectID { get; set; }
    }

    public class GridExemptedSubjectModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_ExemptedSubjectID")]
        public int ExemptedSubjectID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SubjectCatID")]
        public int SubjectCatID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_DisplayName")]
        public string DisplayName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SecondSemesterExemptType")]
        public int? FirstSemesterExemptType { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SecondSemesterExemptType")]
        public int? SecondSemesterExemptType { get; set; }

        public bool HasPractice { get; set; }
    }
}