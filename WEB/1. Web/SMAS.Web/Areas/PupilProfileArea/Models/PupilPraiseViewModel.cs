using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilPraiseViewModel
    {
        public int PupilPraiseID { get; set; }

        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int EducationLevelID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        public int PraiseTypeID { get; set; }

        public int PraiseLevel { get; set; }

        public Nullable<bool> IsRecordedInSchoolReport { get; set; }

        public Nullable<int> Semester { get; set; }

        //grid
        [ResourceDisplayName("PupilProfile_Label_PraiseLocation")]
        public string LocationName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_AcademicYear")]
        public string AcademicYearName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Semester")]
        public string SemesterName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PraiseResolution")]
        public string PraiseResolution { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PraiseDescription")]
        public string Description { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PraiseDate")]
        public System.DateTime PraiseDate { get; set; }
    }
}
