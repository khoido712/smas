﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using Resources;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class ParticularPupilViewModel
    {
        [ScaffoldColumn(false)]
        public int ParticularPupilID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ParticularPupil_Column_ListPupilName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PupilID { get; set; }

        [ScaffoldColumn(false)]
        public int ClassID { get; set; }

        [ScaffoldColumn(false)]
        public int Status { get; set; }

        [ScaffoldColumn(false)]
        public int? OrderInClass { get; set; }

        [ScaffoldColumn(false)]
        public int? ClassOrderNumber { get; set; }

        [ScaffoldColumn(false)]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_RealUpdatedDate")]
        public DateTime RealUpdatedDate { get; set; }

        [ResourceDisplayName("ParticularPupilCharacteristic_Label_ChangesNoteOfPupil_HTML")]
        public string ChangesNoteOfPupil { get; set; }

        [ResourceDisplayName("ParticularPupilCharacteristic_Label_PsychologyCharacteristic")]
        public string PsychologyCharacteristic { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Name")]
        public string Name { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_Class")]
        public string DisplayName { get; set; }

        public bool EnableEdit { get; set; }
    }
}