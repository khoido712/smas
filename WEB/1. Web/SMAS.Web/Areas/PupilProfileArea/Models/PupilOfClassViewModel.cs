using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilOfClassViewModel
    {
        public int PupilOfClassID { get; set; }

        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        public Nullable<int> Year { get; set; }

        public Nullable<int> OrderInClass { get; set; }

        public string Description { get; set; }

        public Nullable<bool> NoConductEstimation { get; set; }

        public int Status { get; set; }

        //grid
        [ResourceDisplayName("PupilProfile_Label_AcademicYear")]
        public string AcademicYearName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_AssignedDate")]
        public Nullable<System.DateTime> AssignedDate { get; set; }
    }
}
