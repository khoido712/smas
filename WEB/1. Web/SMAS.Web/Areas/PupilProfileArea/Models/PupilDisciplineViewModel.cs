using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilDisciplineViewModel
    {
        public int PupilDisciplineID { get; set; }

        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int EducationLevelID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        public int DisciplineTypeID { get; set; }

        public bool IsRecordedInSchoolReport { get; set; }

        public int DisciplineLevel { get; set; }

        //gird
        [ResourceDisplayName("PupilProfile_Label_DisciplineLocation")]
        public string LocationName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_AcademicYear")]
        public string AcademicYearName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Semester")]
        public string SemesterName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_DisciplineResolution")]
        public string DisciplineResolution { get; set; }

        [ResourceDisplayName("PupilProfile_Label_DisciplineDescription")]
        public string Description { get; set; }

        [ResourceDisplayName("PupilProfile_Label_DisciplineDate")]
        public System.DateTime DisciplinedDate { get; set; }
    }
}
