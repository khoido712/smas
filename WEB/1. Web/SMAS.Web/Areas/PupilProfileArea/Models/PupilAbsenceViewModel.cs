﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilAbsenceViewModel
    {
        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int EducationLevelID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        [ResourceDisplayName("EmployeeWorkMovement_Label_RitireDate")]
        public System.DateTime AbsentDate { get; set; }

        [ResourceDisplayName("Home_Label_Section")]
        public int Section { get; set; }

        [ResourceDisplayName("Home_Label_Section")]
        public string SectionName { get; set; }

        [ResourceDisplayName("ClassProfile_Control_Separate_Morning")]
        public string Morning { get; set; }

        [ResourceDisplayName("ClassProfile_Control_Separate_Afternoon")]
        public string Afternoon { get; set; }

        [ResourceDisplayName("ClassProfile_Control_Separate_Night")]
        public string Evening { get; set; }

        public bool? HasMorning { get; set; }
        public bool? HasAfternoon { get; set; }
        public bool? HasEvening { get; set; }
    }
}