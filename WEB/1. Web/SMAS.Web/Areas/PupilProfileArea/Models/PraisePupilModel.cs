﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PraisePupilModel
    {
        [ResourceDisplayName("PupilPraise_Label_PupilPraiseID")]
        public int? PupilPraiseID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilProfileID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PupilID { get; set; }

        public string FullName { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PraiseTypeID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PraiseLevel { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime PraiseDate { get; set; }

        [ResourceDisplayName("PupilPraise_Label_RecordedInSchoolReport")]
        public bool IsRecordedInSchoolReport { get; set; }

        [ResourceDisplayName("PupilPraise_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string Description { get; set; }
    }
}