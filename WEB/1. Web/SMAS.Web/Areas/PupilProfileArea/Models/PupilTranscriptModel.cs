﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilTranscriptModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        public int PupilID { get; set; }
        [ResourceDisplayName("Common_Label_SubjectTitle")]
        public string Mon { get; set; }

        [ResourceDisplayName("LockedMarkDetail_Column_M")]
        public string DiemMieng { get; set; }

        [ResourceDisplayName("JudgeRecord_Label_P")]
        public string Diem15Phut { get; set; }

        [ResourceDisplayName("JudgeRecord_Label_V")]
        public string Diem1Tiet { get; set; }

        [ResourceDisplayName("PupilProfile_DetailTranscript_KTHK")]
        public string DiemCuoiKy { get; set; }

        [ResourceDisplayName("JudgeRecordSemester_Label_HKI")]
        public string DiemTBHK1 { get; set; }

        [ResourceDisplayName("JudgeRecordSemester_Label_HKII")]
        public string DiemTBHK2 { get; set; }

        [ResourceDisplayName("MarkRecordCareer_Label_AllYear")]
        public string DiemTBCN { get; set; }


    }
}