using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilRankingViewModel
    {
        public int PupilRankingID { get; set; }

        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int AcademicYearID { get; set; }

        public int EducationLevelID { get; set; }

        public int Year { get; set; }

        public Nullable<int> Semester { get; set; }

        public Nullable<int> PeriodID { get; set; }

        public Nullable<decimal> AverageMark { get; set; }

        public Nullable<int> TotalAbsentDaysWithPermission { get; set; }

        public Nullable<int> TotalAbsentDaysWithoutPermission { get; set; }

        public Nullable<int> CapacityLevelID { get; set; }

        public Nullable<int> ConductLevelID { get; set; }

        public Nullable<int> Rank { get; set; }

        public Nullable<System.DateTime> RankingDate { get; set; }

        public Nullable<int> StudyingJudgementID { get; set; }

        public int SchoolID { get; set; }

        //grid
        [ResourceDisplayName("PupilProfile_Label_AcademicYear")]
        public string AcademicYearName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_AverageMarkHK1")]
        public string AverageMarkHK1 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_AverageMarkHK2")]
        public string AverageMarkHK2 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_AverageMarkCN")]
        public string AverageMarkCN { get; set; }

        //hoc luc
        [ResourceDisplayName("PupilProfile_Label_CapacityLevelHK1")]
        public string CapacityLevelHK1 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_CapacityLevelHK2")]
        public string CapacityLevelHK2 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_CapacityLevelCN")]
        public string CapacityLevelCN { get; set; }

        //hanh kiem
        [ResourceDisplayName("PupilProfile_Label_ConductLevelHK1")]
        public string ConductLevelHK1 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ConductLevelHK2")]
        public string ConductLevelHK2 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ConductLevelCN")]
        public string ConductLevelCN { get; set; }

        [ResourceDisplayName("PupilRankingModel_Label_StudyingJudgement")]
        public string StudyingJudgementName { get; set; }
    }
}
