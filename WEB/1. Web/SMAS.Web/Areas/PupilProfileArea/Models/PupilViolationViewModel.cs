﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilViolationViewModel
    {
        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int EducationLevelID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        [ResourceDisplayName("PupilFault_Label_ViolatedDate")]
        public System.DateTime ViolatedDate { get; set; }

        public int FaultID { get; set; }
        [ResourceDisplayName("Fault_Name_Title")]
        public string FaultName { get; set; }

        [ResourceDisplayName("Fault_Count_Title")]
        public int NumberOfFault { get; set; }

    }
}