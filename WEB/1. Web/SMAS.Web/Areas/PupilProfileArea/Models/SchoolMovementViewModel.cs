﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class SchoolMovementViewModel
    {
        public int SchoolMovementID { get; set; }

        public int FromSchoolID { get; set; }

        public int? ClassOrderNumber { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_MoveFromSchool")]
        public string FromSchoolName { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_EducationLevelID")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_EducationLevel")]
        public string EducationLevel { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public int PupilID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        public string Name { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime BirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public int Genre { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreDisplay { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_SemesterMoved")]
        public int Semester { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_SemesterMoved")]
        public string SemesterDisplay { get; set; }
    }
}