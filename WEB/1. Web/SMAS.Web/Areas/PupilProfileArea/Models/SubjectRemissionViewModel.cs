﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class SubjectRemissionViewModel
    {
        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int EducationLevelID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_AcademicYear")]
        public string AcademicYearName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Semester")]
        public string SemesterName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SubjectName")]
        public string SubjectRemission { get; set; }

     
    }
}