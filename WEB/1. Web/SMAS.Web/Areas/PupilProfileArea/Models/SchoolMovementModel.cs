﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class SchoolMovementModel
    {
        public int? SchoolMovementID { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassID { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_Pupil")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PupilID { get; set; }

        public string FullName { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_MovedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]	
        public DateTime MovedDate { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_Semester")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int Semester { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_MovedToClassName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string MovedToClassName { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_MovedToSchoolName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string MovedToSchoolName { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_MovedToSchoolAddress")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string MovedToSchoolAddress { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("SchoolMovement_Label_MovedToProvinceID")]
        public int MovedToProvinceID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("SchoolMovement_Label_MovedToDistrictID")]
        public int MovedToDistrictID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("SchoolMovement_Label_MovedToSchoolID")]
        public int? MovedToSchoolID { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_MovedToClassID")]
        public int? MovedToClassID { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_SchoolType")]
        public int SchoolType { get; set; }

        [ResourceDisplayName("SchoolMovement_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}