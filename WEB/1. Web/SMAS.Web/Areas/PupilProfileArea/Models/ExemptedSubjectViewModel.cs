﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class ExemptedSubjectViewModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ExemptedSubjectID")]
        public int ExemptedSubjectID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ExemptedObjectID")]
        public int ExemptedObjectID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilProfileID")]
        public int PupilID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        public string EducationLevelName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ClassProfile_Label_AllTitle")]
        public string ClassName { get; set; }

        [ResourceDisplayName("ClassProfile_Column_ClassProfileID")]
        public int ClassID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SubjectName")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_FirstSemesterExemptType")]
        public int? FirstSemesterExemptType { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_FirstSemesterExemptType")]
        public string FirstSemesterExemptTypeName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SecondSemesterExemptType")]
        public int? SecondSemesterExemptType { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SecondSemesterExemptType")]
        public string SecondSemesterExemptTypeName { get; set; }

        public int Status { get; set; }

        public int? OrderInClass { get; set; }

        public int? ClassOrderNumber { get; set; }

        public string Name { get; set; }

        public bool EnableEdit { get; set; }
    }
}