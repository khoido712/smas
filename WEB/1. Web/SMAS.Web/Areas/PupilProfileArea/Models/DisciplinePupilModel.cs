﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class DisciplinePupilModel
    {
        [ResourceDisplayName("PupilDiscipline_Label_PupilDisciplineID")]
        public int? PupilDisciplineID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_Pupil")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PupilID { get; set; }

        public string FullName { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int DisciplineTypeID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int DisciplineLevel { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime DisciplineDate { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_RecordedInSchoolReport")]
        public bool IsRecordedInSchoolReport { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string Description { get; set; }
    }
}