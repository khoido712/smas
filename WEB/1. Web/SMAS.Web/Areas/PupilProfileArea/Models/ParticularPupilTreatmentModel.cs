﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class ParticularPupilTreatmentModel
    {
        public int? RowIndex { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_ParticularPupilTreatmentID")]
        public int? ParticularPupilTreatmentID { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_ParticularPupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ParticularPupilID { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_TreatmentResolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        public string TreatmentResolution { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_Result")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        public string Result { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_UpdatedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? AppliedDate { get; set; }
    }
}