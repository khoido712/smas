﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.Web;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class PupilProfileViewModel
    {
        #region tu sinh

        public System.Int32 PupilProfileID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public System.Int32 CurrentClassID { get; set; }

        public System.Int32 CurrentSchoolID { get; set; }

        public System.Int32 CurrentAcademicYearID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Area")]
        public string AreaName { get; set; }

        public System.Nullable<System.Int32> AreaID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Province")]
        public string ProvinceName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("PupilProfile_Label_Province")]
        public System.Int32 ProvinceID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_District")]
        public string DistrictName { get; set; }

        public System.Nullable<System.Int32> DistrictID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Commune")]
        public string CommuneName { get; set; }

        public System.Nullable<System.Int32> CommuneID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Ethnic")]
        public string EthnicName { get; set; }

        public System.Nullable<System.Int32> EthnicID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Religion")]
        public string ReligionName { get; set; }

        public System.Nullable<System.Int32> ReligionID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PolicyTarget")]
        public string PolicyTargetName { get; set; }

        public System.Nullable<System.Int32> PolicyTargetID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FamilyType")]
        public string FamilyTypeName { get; set; }

        public System.Nullable<System.Int32> FamilyTypeID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PriorityType")]
        public string PriorityTypeName { get; set; }

        public System.Nullable<System.Int32> PriorityTypeID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PreviousGraduationLevel")]
        public string PreviousGraduationLevelName { get; set; }

        public System.Nullable<System.Int32> PreviousGraduationLevelID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Name")]
        public System.String Name { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthPlace")]
        public System.String BirthPlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_HomeTown")]
        public System.String HomeTown { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Telephone")]
        public System.String Telephone { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Email")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String Email { get; set; }

        [ResourceDisplayName("PupilProfile_Label_TempResidentalAddress")]
        public System.String TempResidentalAddress { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PermanentResidentalAddress")]
        public System.String PermanentResidentalAddress { get; set; }

        public System.Nullable<System.Boolean> IsResidentIn { get; set; }

        [ResourceDisplayName("PupilProfile_Label_IsDisabled")]
        public bool IsDisabled { get; set; }

        [ResourceDisplayName("PupilProfile_Label_IsSwimming")]
        public bool IsSwimming { get; set; }

        public bool FatherIsEthnic { get; set; }
        [ResourceDisplayName("Có bố dân tộc")]
        public bool MinorityFather { get; set; }

        public bool MotherIsEthnic { get; set; }
        [ResourceDisplayName("Có mẹ dân tộc")]
        public bool MinorityMother { get; set; }

        [ResourceDisplayName("Học chương trình GD của bộ")]
        public bool UsedMoetProgram { get; set; }

        public bool IsProgramCarriculum { get; set; }

        [ResourceDisplayName("PupilProfile_Label_DisabledType")]
        public string DisabledTypeName { get; set; }

        public System.Nullable<System.Int32> DisabledTypeID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_DisabledSeverity")]
        public System.String DisabledSeverity { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BloodType")]
        public string BloodTypeName { get; set; }

        public System.Nullable<System.Int32> BloodType { get; set; }

        [ResourceDisplayName("")]
        public System.Nullable<System.Decimal> EnrolmentMark { get; set; }

        [ResourceDisplayName("PupilProfile_Label_EnrolmentDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime EnrolmentDate { get; set; }

        public System.Int32? ForeignLanguageTraining { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ForeignLanguageTraining")]
        public string ForeignLanguageTrainingName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_IsYoungPioneerMember")]
        public bool IsYoungPioneerMember { get; set; }

        [ResourceDisplayName("PupilProfile_Label_YoungPioneerJoinedDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.Nullable<System.DateTime> YoungPioneerJoinedDate { get; set; }

        public System.String YoungPioneerJoinedPlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_IsYouthLeageMember")]
        public bool IsYouthLeageMember { get; set; }

        [ResourceDisplayName("PupilProfile_Label_YouthLeagueJoinedDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.Nullable<System.DateTime> YouthLeagueJoinedDate { get; set; }

        public System.String YouthLeagueJoinedPlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_IsCommunistPartyMember")]
        public bool IsCommunistPartyMember { get; set; }

        [ResourceDisplayName("PupilProfile_Label_CommunistPartyJoinedDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.Nullable<System.DateTime> CommunistPartyJoinedDate { get; set; }

        [ResourceDisplayName("")]
        public System.String CommunistPartyJoinedPlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FatherFullName")]
        public System.String FatherFullName { get; set; }

        public System.Nullable<System.DateTime> FatherBirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FatherJob")]
        public System.String FatherJob { get; set; }

        [ResourceDisplayName("PupilProfile_Label_MotherFullName")]
        public System.String MotherFullName { get; set; }

        public System.Nullable<System.DateTime> MotherBirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_MotherJob")]
        public System.String MotherJob { get; set; }

        [ResourceDisplayName("PupilProfile_Label_SponsorFullName")]
        public System.String SponsorFullName { get; set; }

        public System.Nullable<System.DateTime> SponsorBirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_SponsorJob")]
        public System.String SponsorJob { get; set; }

        [ResourceDisplayName("PupilProfile_Label_SponsorMobile")]
        public System.String SponsorMobile { get; set; }

        [ResourceDisplayName("PupilProfile_Label_RegisterSMS")]
        public bool IsFatherSMS { get; set; }


        [ResourceDisplayName("PupilProfile_Label_RegisterSMS")]
        public bool IsMotherSMS { get; set; }

        [ResourceDisplayName("PupilProfile_Label_RegisterSMS")]
        public bool IsSponsorSMS { get; set; }

        public int? OrderInClass { get; set; }

        public System.Boolean IsActive { get; set; }

        public Nullable<int> VillageID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_VillageName")]
        public string VillageName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_StorageName")]
        public string StorageNumber { get; set; }

        public Nullable<int> ClassOrderNumber { get; set; }

        #endregion tu sinh

        #region truong tren grid tren grid

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Int32 Genre { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Mobile")]
        public System.String Mobile { get; set; }

        [ResourceDisplayName("Pupil_Label_IdentityNumber")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String IdentifyNumber { get; set; }

        [ResourceDisplayName("Pupil_Label_HomePlace")]
        public System.Int32? HomePlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FatherMobile")]
        public System.String FatherMobile { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FatherEmail")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String FatherEmail { get; set; }

        [ResourceDisplayName("PupilProfile_Label_MotherMobile")]
        public System.String MotherMobile { get; set; }

        [ResourceDisplayName("PupilProfile_Label_MotherEmail")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String MotherEmail { get; set; }

        [ResourceDisplayName("PupilProfile_Label_SponsorEmail")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String SponsorEmail { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Status")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Int32 ProfileStatus { get; set; }

        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Int32 EducationLevel { get; set; }

        [ResourceDisplayName("PupilProfile_Label_EnrolmentType")]
        public string EnrolmentTypeName { get; set; }

        public int? EnrolmentType { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassType")]
        public int? ClassType { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassType")]
        public string ClassTypeName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_SupportingPolicy")]
        public int? SupportingPolicy { get; set; }

        [ResourceDisplayName("PupilProfile_Label_SupportingPolicy")]
        public string SupportingPolicyName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_IsReceiveRiceSubsidy")]
        public bool IsReceiveRiceSubsidy { get; set; }

        [ResourceDisplayName("PupilProfile_Label_InResettlementProgram")]
        public bool IsResettlementTarget { get; set; }
        public bool AutoGenCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Status")]
        public string StatusName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_SponsorBirthDate")]
        public int? iSponsorBirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_MotherBirthDate")]
        public int? iMotherBirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FatherBirthDate")]
        public int? iFatherBirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PolicyRegime")]
        public Nullable<int> PolicyRegimeID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_IsSupportForLearning")]
        public bool IsSupportForLearning { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PolicyRegime")]
        public string PolicyRegimeName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilLearningType")]
        public int? PupilLearningType { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilLearningType")]
        public string PupilLearningTypeName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_LanguageCertificate")]
        public string LanguageCertificate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ItCertificate")]
        public string ItCertificate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_LanguageCertificateID")]
        public int? LanguageCertificateID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ITCertificateID")]
        public int? ItCertificateID { get; set; }

        public string HtmlDivTool { get; set; }
        #endregion truong tren grid tren grid

        public string HiddenLink { get; set; }

        public bool EnableCheck { get; set; }

        [ResourceDisplayName("SubCommittee_Label_SubCommitteeID")]
        public int? SubCommitteeID { get; set; }

        [ResourceDisplayName("SubCommittee_Label_Resolution")]
        public string SubCommitteeName { get; set; }

        public string EthnicCode { get; set; }

        public int? AppliedLevelID { get; set; }
        public bool? IsUsedMoetProgram { get; set; }
        public bool isFirst { get; set; }
        public bool? IsMinorityFather { get; set; }

        public bool? IsMinorityMother { get; set; }
        public bool isEnd { get; set; }
        
        [ResourceDisplayName("Tên gọi khác (Dân tộc)")]
        public string OtherEthnicName { get; set; }
        public System.Nullable<System.Int32> OtherEthnicID { get; set; }
        public string OtherEthnicCode { get; set; }

        public DateTime? FatherBirthDateHas { get; set; }
        public DateTime? MotherBirthDateHas { get; set; }
        public DateTime? SponsorBirthDateHas { get; set; }
    }
}
