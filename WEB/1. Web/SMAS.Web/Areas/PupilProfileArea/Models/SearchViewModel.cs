/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LISTCLASS)]
        [AdditionalMetadata("OnChange", "OnCboClassChanged(this)")]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Class { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LISTGENRE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Genre { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        [UIHint("Textbox")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [UIHint("Textbox")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Fullname { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Status")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LISTSTATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Status { get; set; }

        [ResourceDisplayName("Ethnic_Label_EthnicID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LISTETHNIC)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Ethnic { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LIST_PRAISE_TYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? PraiseTypeID { get; set; }

        [ResourceDisplayName("PupilPraise_Label_PraiseLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LIST_PRAISE_LEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? PraiseLevel { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LIST_DISCIPLINE_TYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? DisciplineTypeID { get; set; }

        [ResourceDisplayName("PupilDiscipline_Label_DisciplineLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileConstants.LIST_DISCIPLINE_LEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? DisciplineLevel { get; set; }

        [ScaffoldColumn(false)]
        public string ReservePupilCode { get; set; }

        [ScaffoldColumn(false)]
        public string ReserveFullname { get; set; }

    }
}
