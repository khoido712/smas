﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilProfileArea.Models
{
    public class ReservePupilViewModel
    {
        public int PupilOfClassID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int PupilLeaveOffID { get; set; }
        public string Name { get; set; }
        [ResourceDisplayName("Pupil_Profile_ReservePupil_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("Pupil_Profile_ReservePupil_PupilCode")]
        public string PupilCode { get; set; }
        public int Genre { get; set; }
        [ResourceDisplayName("Pupil_Profile_ReservePupil_BirthDate")]
        public DateTime BirthDate { get; set; }
        [ResourceDisplayName("Pupil_Profile_ReservePupil_ClassName")]
        public string ClassName { get; set; }
        [ResourceDisplayName("Pupil_Profile_ReservePupil_LeavingDate")]
        public DateTime LeavingDate { get; set; }
        [ResourceDisplayName("Pupil_Profile_ReservePupil_LeavingReason")]
        public string LeavingReason { get; set; }
        [ResourceDisplayName("Pupil_Profile_ReservePupil_Genre")]
        public string StrGenre
        {
            get
            {
                string str = "";
                if (Genre == 1)
                {
                    str = "Nam";
                }
                else
                {
                    str = "Nữ";
                }
                return str;
            }
        }
        public int? AcceptClassID { get; set; }
        public int AcceptEducationLevel { get; set; }
        public bool IsDataInherit { get; set; }
        public bool IsVnenClass { get; set; }
    }
}