﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExperienceTypeArea
{
    public class ExperienceTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExperienceTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExperienceTypeArea_default",
                "ExperienceTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
