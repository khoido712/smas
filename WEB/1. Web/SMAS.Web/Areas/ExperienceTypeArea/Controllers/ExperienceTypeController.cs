/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ExperienceTypeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.ExperienceTypeArea.Controllers
{
    public class ExperienceTypeController : BaseController
    {
        private readonly IExperienceTypeBusiness ExperienceTypeBusiness;
        private readonly ITeachingExperienceBusiness TeachingExperienceBusiness;

        public ExperienceTypeController(IExperienceTypeBusiness experiencetypeBusiness, ITeachingExperienceBusiness teachingExperienceBusiness)
        {
            this.ExperienceTypeBusiness = experiencetypeBusiness;
            this.TeachingExperienceBusiness = teachingExperienceBusiness;
        }

        //
        // GET: /ExperienceType/

        public ActionResult Index()
        {
            SetViewDataPermission("ExperienceType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            IEnumerable<ExperienceTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[ExperienceTypeConstants.LIST_EXPERIENCETYPE] = lst;
            return View();
        }

        //
        // GET: /ExperienceType/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            SearchInfo["Resolution"] = frm.Resolution;
            IEnumerable<ExperienceTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[ExperienceTypeConstants.LIST_EXPERIENCETYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ExperienceType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ExperienceType experiencetype = new ExperienceType();
            TryUpdateModel(experiencetype);
            experiencetype.IsActive = true;
            GlobalInfo g = new GlobalInfo();
            experiencetype.SchoolID = g.SchoolID.Value;
            Utils.Utils.TrimObject(experiencetype);

            this.ExperienceTypeBusiness.Insert(experiencetype);
            this.ExperienceTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ExperienceTypeID)
        {
            this.CheckPermissionForAction(ExperienceTypeID, "ExperienceType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ExperienceType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ExperienceType experiencetype = this.ExperienceTypeBusiness.Find(ExperienceTypeID);
            TryUpdateModel(experiencetype);
            GlobalInfo g = new GlobalInfo();

            experiencetype.SchoolID = g.SchoolID.Value;
            experiencetype.IsActive = true;
            Utils.Utils.TrimObject(experiencetype);
            this.ExperienceTypeBusiness.Update(experiencetype);
            this.ExperienceTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "ExperienceType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ExperienceType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ExperienceTypeID",id}
            };
            List<TeachingExperience> lstTE = TeachingExperienceBusiness.Search(dic).ToList();
            if (lstTE.Count > 0)
            {
                throw new BusinessException("Validate_ExperienceType");
            }
            this.ExperienceTypeBusiness.Delete(id);
            this.ExperienceTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ExperienceTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("ExperienceType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<ExperienceType> query = this.ExperienceTypeBusiness.Search(SearchInfo);
            IQueryable<ExperienceTypeViewModel> lst = query.Select(o => new ExperienceTypeViewModel
            {
                ExperienceTypeID = o.ExperienceTypeID,
                Resolution = o.Resolution,
                SchoolID = o.SchoolID,
                Description = o.Description,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate
            });

            return lst.ToList().OrderBy(o=> SMAS.Business.Common.Utils.SortABC(o.Resolution));
        }
    }
}





