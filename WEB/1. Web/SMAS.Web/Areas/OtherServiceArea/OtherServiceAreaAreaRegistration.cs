﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.OtherServiceArea
{
    public class OtherServiceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "OtherServiceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "OtherServiceArea_default",
                "OtherServiceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
