﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
namespace SMAS.Web.Areas.OtherServiceArea.Models
{
    public class OtherServiceViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 OtherServiceID { get; set; }
        [ResourceDisplayName("OtherService_Label_OtherServiceName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String OtherServiceName { get; set; }
        //[ScaffoldColumn(false)]
        [ResourceDisplayName("OtherService_Label_EffectDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public System.DateTime EffectDate { get; set; }
        [ResourceDisplayName("OtherService_LabelForm_Price")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        //[RegularExpression(@"^(0|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "OtherService_Price_Error")]
        public System.Int32 Price { get; set; }
        [ResourceDisplayName("OtherService_Label_Note")]
        [DataType(DataType.MultilineText)]
        public System.String Note { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("OtherService_Label_SchoolID")]
        public System.Int32 SchoolID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("OtherService_Label_CreatedDate")]
        public System.DateTime CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        [ResourceDisplayName("OtherService_Label_IsActive")]
        public System.Boolean IsActive { get; set; }
        [ScaffoldColumn(false)]
        [ResourceDisplayName("OtherService_Label_ModifiedDate")]
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }

    }
}


