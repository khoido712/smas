﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.OtherServiceArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.OtherServiceArea.Controllers
{
    public class OtherServiceController : BaseController
    {
        private readonly IOtherServiceBusiness OtherServiceBusiness;

        public OtherServiceController(IOtherServiceBusiness otherserviceBusiness)
        {
            this.OtherServiceBusiness = otherserviceBusiness;
        }

        //
        // GET: /OtherService/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            int? schoolID = global.SchoolID;
            //Get view data here
            SearchInfo["IsActive"] = true;            
            SearchInfo["SchoolID"] = schoolID;
            IEnumerable<OtherServiceViewModel> lst = this._Search(SearchInfo);
            ViewData[OtherServiceConstants.LIST_OTHERSERVICE] = lst;
            return View();
        }

        //
        // GET: /OtherService/Search


        public PartialViewResult Search()
        {
            //Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            int? schoolID = global.SchoolID;
            //add search info
            SearchInfo["IsActive"] = true;
            SearchInfo["SchoolID"] = schoolID;

            IEnumerable<OtherServiceViewModel> lst = this._Search(SearchInfo);
            ViewData[OtherServiceConstants.LIST_OTHERSERVICE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo glo = new GlobalInfo();
            if (GetMenupermission("OtherService", glo.UserAccountID, glo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            GlobalInfo global = new GlobalInfo();
            OtherService otherservice = new OtherService();
            TryUpdateModel(otherservice);
            Utils.Utils.TrimObject(otherservice);
            //DateTime date = DateTime.Now;
            //otherservice.EffectDate = date;
            otherservice.SchoolID = global.SchoolID.Value;
            this.OtherServiceBusiness.Insert(otherservice);
            this.OtherServiceBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int OtherServiceID)
        {
            this.CheckPermissionForAction(OtherServiceID, "OtherService");
            GlobalInfo glo = new GlobalInfo();
            if (GetMenupermission("OtherService", glo.UserAccountID, glo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            OtherService otherservice = this.OtherServiceBusiness.Find(OtherServiceID);
            //otherservice.ModifiedDate = DateTime.Now;
            TryUpdateModel(otherservice);
            Utils.Utils.TrimObject(otherservice);

            this.OtherServiceBusiness.Update(otherservice);
            this.OtherServiceBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "OtherService");
            GlobalInfo glo = new GlobalInfo();
            if (GetMenupermission("OtherService", glo.UserAccountID, glo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            GlobalInfo global = new GlobalInfo();
            int? schoolID = global.SchoolID;
            this.OtherServiceBusiness.Delete(id, schoolID.Value);
            this.OtherServiceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<OtherServiceViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("OtherService", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<OtherService> query = this.OtherServiceBusiness.Search(SearchInfo);
            IQueryable<OtherServiceViewModel> lst = query.Select(o => new OtherServiceViewModel
            {
                OtherServiceID = o.OtherServiceID,
                OtherServiceName = o.OtherServiceName,
                Note = o.Note,
                Price = o.Price,
                SchoolID = o.SchoolID,
                EffectDate = o.EffectDate,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate
            });
            return lst.OrderBy(p => p.OtherServiceName).ToList();
        }
    }
}





