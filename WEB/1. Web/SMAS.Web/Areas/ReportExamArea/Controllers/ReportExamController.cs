﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;


namespace SMAS.Web.Areas.ReportExamArea.Controllers
{
    public class ReportExamController : Controller
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        private readonly IReportExamBusiness ReportExamBusiness;
        private readonly IDetachableHeadBagBusiness DetachableHeadBagBusiness;
        public ReportExamController(IClassProfileBusiness classProfileBusiness,
            IEducationLevelBusiness educationLevelBusiness,

            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IExaminationBusiness examinationBusiness,
            IExaminationSubjectBusiness examinationSubjectBusiness,
            ISubjectCatBusiness subjectCatBusiness,
             IExaminationRoomBusiness examinationRoomBusiness,
            IReportExamBusiness reportExamBusiness,
            IDetachableHeadBagBusiness detachableHeadBagBusiness)
        {
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;

            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ExaminationBusiness = examinationBusiness;
            this.ExaminationSubjectBusiness = examinationSubjectBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ExaminationRoomBusiness = examinationRoomBusiness;
            this.ReportExamBusiness = reportExamBusiness;
            this.DetachableHeadBagBusiness = detachableHeadBagBusiness;
        }

        public void setViewData()
        {
            GlobalInfo Global = new GlobalInfo();

            //Danh sách khối học            
            ViewData[ReportExamConstants.LIST_EDUCATION_LEVEL] = new List<EducationLevel>();
            //Danh sách kỳ thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Global.AcademicYearID.GetValueOrDefault();
            dic["AppliedLevel"] = Global.AppliedLevel.GetValueOrDefault();
            int schoolID = Global.SchoolID.Value;
            List<Examination> lstExamination = ExaminationBusiness.SearchBySchool(schoolID, dic).OrderByDescending(o => o.ToDate).ToList();
            ViewData[ReportExamConstants.LIST_EXAMINATION] = lstExamination;
            ViewData[ReportExamConstants.LIST_EXAMINATION_SUBJECT] = new List<ExaminationSubjectBO>();
            //Danh sách phòng thi
            IDictionary<string, object> dicRoom = new Dictionary<string, object>();
            ViewData[ReportExamConstants.LIST_EXAMINATION_ROOM] = new List<ExaminationRoom>();
            ViewData[ReportExamConstants.LIST_DETACHABLE_HEAD_BAG] = new List<DetachableHeadBag>();

        }

        public PartialViewResult ControlCombo()
        {
            setViewData();

            return PartialView("_ListControl");

        }


        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();
            setViewData();
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationRoom(int? idExaminationSubjectID, int? idExaminationID, int? idEducationLevelID, int? ExaminationSubjectHidden)
        {
            ViewData[ReportExamConstants.LIST_EXAMINATION_ROOM] = new List<ExaminationRoom>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            int? ExaminationSubjectHiddenID = ExaminationSubjectHidden;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (Request["rdoReportType"] == "1" && ExaminationBusiness.Find(idExaminationID).UsingSeparateList == 0)
            {
                dic["ExaminationSubjectID"] = ExaminationSubjectHiddenID;
            }
            else
            {
                dic["ExaminationSubjectID"] = idExaminationSubjectID;
            }
            // dic["ExaminationSubjectID"] = idExaminationSubjectID;
            dic["ExaminationID"] = idExaminationID;
            dic["EducationLevelID"] = idEducationLevelID;
            if (idExaminationSubjectID == null) return null;
            //if(ExaminationBusiness.Find(idExaminationID).UsingSeparateList == 0)
            IQueryable<ExaminationRoom> lsRoom = ExaminationRoomBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic);

            // Sap xep cac phong thi
            var orderedListRoom = lsRoom.OrderBy(o => o.RoomTitle).ToList();
            ViewData[ReportExamConstants.LIST_EXAMINATION_ROOM] = orderedListRoom;

            return Json(new SelectList(orderedListRoom, "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
        }



        [ValidateAntiForgeryToken]
        public JsonResult LoadDisableControl(int? idExaminationID)
        {

            Examination Examination = ExaminationBusiness.Find(idExaminationID);
            bool disable = false;
            if (Examination.UsingSeparateList == 0)
            {
                disable = true;
            }
            return Json(disable);

        }



        [ValidateAntiForgeryToken]
        public JsonResult LoadDisableDetachableHeadBag(int? idExaminationID)
        {
            if (!idExaminationID.HasValue)
            {
                return Json(true);
            }
            Examination Examination = ExaminationBusiness.Find(idExaminationID);
            bool disable = true;
            if (Examination.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD)
            {
                disable = false;
            }
            return Json(disable);

        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadEducationLevel(int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            if (examinationID.HasValue && examinationID.Value > 0)
            {
                dicExaminationSubject.Add("ExaminationID", examinationID.Value);
            }
            else
            {
                return Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            var lstEdu = lstExamSubject.Select(u => new { u.EducationLevelID, u.EducationLevel.Resolution }).Distinct()
                .OrderBy(o => o.EducationLevelID).ToList();

            return Json(new SelectList(lstEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationSubject(int? idExaminationID, int? idEducationLevelID)
        {
            ViewData[ReportExamConstants.LIST_EXAMINATION_SUBJECT] = new List<ExaminationSubject>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["ExaminationID"] = idExaminationID;
            dic["EducationLevelID"] = idEducationLevelID;
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID.GetValueOrDefault();
            dic["SchoolID"] = GlobalInfo.SchoolID.GetValueOrDefault();
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel.GetValueOrDefault();

            IQueryable<ExaminationSubject> qExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).Distinct();

            List<ExaminationSubjectBO> lstExaminationSubject = (from es in qExaminationSubject
                                                                join sc in SubjectCatBusiness.All on es.SubjectID equals sc.SubjectCatID
                                                                orderby sc.OrderInSubject, sc.DisplayName
                                                                select new ExaminationSubjectBO
                                                                   {
                                                                       ExaminationSubjectID = es.ExaminationSubjectID,
                                                                       DisplayName = sc.DisplayName
                                                                   }).ToList();

            if (lstExaminationSubject.Count() != 0)
            {
                ViewData[ReportExamConstants.LIST_EXAMINATION_SUBJECT] = lstExaminationSubject;
            }
            return Json(new SelectList(lstExaminationSubject.ToList(), "ExaminationSubjectID", "DisplayName"), JsonRequestBehavior.AllowGet);
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadDetachableHeadBag(int? idExaminationSubjectID)
        {


            //ViewData[ReportExamConstants.LIST_EXAMINATION_SUBJECT] = new List<ExaminationSubject>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["ExaminationSubjectID"] = idExaminationSubjectID;
            if (idExaminationSubjectID == 0) return null;
            List<DetachableHeadBag> lstDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();


            return Json(new SelectList(lstDetachableHeadBag.ToList(), "DetachableHeadBagID", "BagTitle"), JsonRequestBehavior.AllowGet);
        }

        //Xuất báo cáo
        [HttpPost]
        public FileResult ExportExcel(FormCollection col)
        {
            int ExaminationID = 0;
            int EducationLevelID = 0;
            if (col["ExaminationID"] == null || col["ExaminationID"].Trim().Equals(string.Empty)
                || !int.TryParse(col["ExaminationID"], out ExaminationID))
            {
                throw new BusinessException("Examination_Label_SetExaminationStageErrorMessage");
            }
            if (col["EducationLevelID"] == null || col["EducationLevelID"].Trim().Equals(string.Empty)
                || !int.TryParse(col["EducationLevelID"].Trim(), out EducationLevelID))
            {
                throw new BusinessException("InputExaminationMark_Label_EducationLevelNotAvailable");
            }
            GlobalInfo global = new GlobalInfo();
            ExaminationID = Convert.ToInt32(col["ExaminationID"]);
            EducationLevelID = Convert.ToInt32(col["EducationLevelID"]);
            int? ExaminationSubjectHiddenID;
            if (col["ExaminationSubjectHidden"] == "")

                ExaminationSubjectHiddenID = 0;
            else
                ExaminationSubjectHiddenID = Convert.ToInt32(col["ExaminationSubjectHidden"]);
            int? ExaminationSubjectID;
            if (col["ExaminationSubjectID"] == "")
            {
                ExaminationSubjectID = 0;
            }
            else
            {
                ExaminationSubjectID = Convert.ToInt32(col["ExaminationSubjectID"]);
            }

            int? ExaminationRoomID;
            if (col["ExaminationRoomID"] == "")
            {
                ExaminationRoomID = 0;
            }
            else
            {
                ExaminationRoomID = Convert.ToInt32(col["ExaminationRoomID"]);
            }
            int? DetachableHeadBagID;
            if (col["DetachableHeadBagID"] == "")
            {
                DetachableHeadBagID = 0;
            }
            else
            {
                DetachableHeadBagID = Convert.ToInt32(col["DetachableHeadBagID"]);
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID.GetValueOrDefault();
            dic["AcademicYearID"] = global.AcademicYearID.GetValueOrDefault();
            dic["AppliedLevel"] = global.AppliedLevel.GetValueOrDefault();
            dic["ExaminationID"] = ExaminationID;
            dic["EducationLevelID"] = EducationLevelID;

            if (col["rdoReportType"] == "1" && ExaminationBusiness.Find(ExaminationID).UsingSeparateList == 0)
            {
                dic["ExaminationSubjectID"] = ExaminationSubjectHiddenID;
            }
            else
            {
                dic["ExaminationSubjectID"] = ExaminationSubjectID;
            }
            dic["ExaminationRoomID"] = ExaminationRoomID;
            dic["DetachableHeadBagID"] = DetachableHeadBagID;
            if (col["rdoReportType"] == "1")
            {
                Stream excel = ReportExamBusiness.CreateReportList(dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

                string ReportName = "THI_[SchoolLevel]_DSThiSinh[SubjectName].xls";
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);
                if (ExaminationSubjectID != 0)
                {
                    ReportName = ReportName.Replace("[SubjectName]", "_" + ExaminationSubjectBusiness.Find(ExaminationSubjectID).SubjectCat.DisplayName);
                }
                else
                {
                    ReportName = ReportName.Replace("[SubjectName]", "");
                }
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

                result.FileDownloadName = ReportName;

                return result;
            }
            else if (col["rdoReportType"] == "2")
            {
                if (ExaminationSubjectID == 0)
                {
                    throw new BusinessException("Chưa chọn môn thi");
                }
                Stream excel = ReportExamBusiness.CreateRollReport(dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

                string ReportName = "THI_[SchoolLevel]_DiemDanhThiSinhTheoPhong_[SubjectName].xls";
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);

                ReportName = ReportName.Replace("[SubjectName]", ExaminationSubjectBusiness.Find(ExaminationSubjectID).SubjectCat.DisplayName);
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

                result.FileDownloadName = ReportName;

                return result;
            }
            else if (col["rdoReportType"] == "3")
            {
                if (ExaminationSubjectID == 0)
                {
                    throw new BusinessException("Chưa chọn môn thi");
                }
                //Bo sung 12/12/2013
                if (ExaminationSubjectID == 0)
                {
                    int type = ExaminationBusiness.Find(ExaminationID).MarkImportType;
                    Examination exam = ExaminationBusiness.Find(ExaminationID);
                    if (exam.UsingSeparateList != 0)
                    {
                        throw new BusinessException("ExaminationSubject_Label_ErrorCheckMessage");
                    }

                    Stream excel = ReportExamBusiness.CreateInputMarkFileBySBD(dic);
                    FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                    if (type == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER)
                    {
                        string ReportName = "Thi_[SchoolLevel]_NhapDiemTheoSBD.xls";
                        string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                        //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                        ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);

                        //ReportName = ReportName.Replace("[SubjectName]", ExaminationSubjectBusiness.Find(ExaminationSubjectID).SubjectCat.DisplayName);
                        ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

                        result.FileDownloadName = ReportName;

                    }
                    return result;
                }
                else
                {
                    int type = ExaminationBusiness.Find(ExaminationID).MarkImportType;

                    if (type == SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE)
                    {
                        Stream excel = ReportExamBusiness.CreateInputMarkFile(dic);
                        FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

                        string ReportName = "Thi_[SchoolLevel]_NhapDiemTheoMHS_[SubjectName].xls";
                        string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                        //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                        ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);

                        ReportName = ReportName.Replace("[SubjectName]", ExaminationSubjectBusiness.Find(ExaminationSubjectID).SubjectCat.DisplayName);
                        ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

                        result.FileDownloadName = ReportName;

                        return result;
                    }
                    else if (type == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER)
                    {
                        Stream excel = ReportExamBusiness.CreateInputMarkFile(dic);
                        FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

                        string ReportName = "Thi_[SchoolLevel]_NhapDiemTheoSBD_[SubjectName].xls";
                        string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                        //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                        ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);

                        ReportName = ReportName.Replace("[SubjectName]", ExaminationSubjectBusiness.Find(ExaminationSubjectID).SubjectCat.DisplayName);
                        ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

                        result.FileDownloadName = ReportName;

                        return result;
                    }
                    else
                    {
                        Stream excel = ReportExamBusiness.CreateInputMarkFile(dic);
                        FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

                        string ReportName = "Thi_[SchoolLevel]_NhapDiemTheoSoPhach_[SubjectName].xls";
                        string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                        //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                        ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);

                        ReportName = ReportName.Replace("[SubjectName]", ExaminationSubjectBusiness.Find(ExaminationSubjectID).SubjectCat.DisplayName);
                        ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

                        result.FileDownloadName = ReportName;

                        return result;
                    }
                }
            }
            else
            {
                Stream excel = ReportExamBusiness.CreateAssignReport(dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

                string ReportName = "THI_[SchoolLevel]_DSPhanCongGiamThi_[EducationLevelName].xls";
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);

                ReportName = ReportName.Replace("[EducationLevelName]", EducationLevelBusiness.Find(EducationLevelID).Resolution);
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

                result.FileDownloadName = ReportName;

                return result;
            }

        }

        //Bo sung 12/12
        public JsonResult checkExam(int? idExaminationID)
        {

            Examination Examination = ExaminationBusiness.Find(idExaminationID);
            string TypeMark = "0";
            if (Examination != null)
            {
                TypeMark = Examination.MarkImportType.ToString();
            }
            return Json(TypeMark, JsonRequestBehavior.AllowGet);

        }

    }
}
