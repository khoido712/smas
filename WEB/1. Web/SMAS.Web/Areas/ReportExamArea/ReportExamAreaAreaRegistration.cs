﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportExamArea
{
    public class ReportExamAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportExamArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportExamArea_default",
                "ReportExamArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
