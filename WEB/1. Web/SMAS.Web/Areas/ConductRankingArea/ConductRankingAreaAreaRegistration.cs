﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ConductRankingArea
{
    public class ConductRankingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ConductRankingArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ConductRankingArea_default",
                "ConductRankingArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
