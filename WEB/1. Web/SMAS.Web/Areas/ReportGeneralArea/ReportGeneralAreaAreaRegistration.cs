﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportGeneralArea
{
    public class ReportGeneralAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportGeneralArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportGeneralArea_default",
                "ReportGeneralArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
