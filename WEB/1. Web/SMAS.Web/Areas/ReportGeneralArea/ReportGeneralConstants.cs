﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportGeneralArea
{
    public class ReportGeneralConstants
    {
        public const string LIST_EDUCATION_LEVEL = "LIST_EducationLevel";
        public const string LIST_CLASS = "LIST_Class";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_ROW_EXCEL = "LIST_ROW_EXCEL";
        public const string LIST_PROFILE_TEMPLATE = "LIST_PROFILE_TEMPLATE";
        public const string LIST_COLUMN_TEMPLATE_PUPIL = "LIST_COLUMN_TEMPLATE_PUPIL";
        public const string LIST_COLUMN_TEMPLATE_HEALTH = "LIST_COLUMN_TEMPLATE_HEALTH";
        public const string LIST_COLUMN_TEMPLATE_SUBJECT = "LIST_COLUMN_TEMPLATE_SUBJECT";
        public const string LIST_COLUMN_TEMPLATE_MARK_RESULT = "LIST_COLUMN_TEMPLATE_MARK_RESULT";
        public const string LIST_COLUMN_TEMPLATE_CAPQUA = "LIST_COLUMN_TEMPLATE_CAPQUA";
        public const string TYPE_SELECTED = "TYPE_SELECTED";
        public const string IS_SYSTEM_TEMPLATE = "IS_SYSTEM_TEMPLATE";
    }
}