﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportGeneralArea;
using SMAS.Web.Areas.ReportGeneralArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Constants;
using System.IO;
using SMAS.Business.Common;
using System.Text.RegularExpressions;

namespace SMAS.Web.Areas.ReportGeneralArea
{
    public class ReportGeneralController : BaseController
    {
        private readonly IProfileTemplateBusiness ProfileTemplateBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IColumnDescriptionBusiness ColumnDescriptionBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness; 
        public ReportGeneralController(IProfileTemplateBusiness profileTemplateBusiness, IAcademicYearBusiness academicYearBusiness,
            IClassProfileBusiness classProfileBusiness, IColumnDescriptionBusiness columnDescriptionBusiness,
            ISchoolProfileBusiness schoolProfileBusiness, IClassSubjectBusiness classSubjectBusiness)
        {
            this.ProfileTemplateBusiness = profileTemplateBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ColumnDescriptionBusiness = columnDescriptionBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
        }
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (educationLevelID > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dic["EducationLevelID"] = educationLevelID;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }
        [HttpPost]
        public PartialViewResult LoadTemplate(int id)
        {
            SetProfileTemplate(id);
            SetRowExcel();
            List<ColumnDescriptionModel> lstVal = GetColumnDescription(id);
            List<ColumnDescriptionModel> lstSubjectModel = new List<ColumnDescriptionModel>();
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_PUPIL] = lstVal.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_5
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_6
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_7));
                if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    lstSubjectModel = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11
                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_25).ToList();
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = this.GetListSubjectByListCDPrimary(lstSubjectModel);
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_15);
                }
                else if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    lstSubjectModel = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12
                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26).ToList();
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = this.GetListSubjectByListCD(lstSubjectModel);
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14);
                }
                else
                {
                    lstSubjectModel = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13
                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27).ToList();
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = this.GetListSubjectByListCD(lstSubjectModel);
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14);
                }
            }
            else
            {
                ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_PUPIL] = lstVal.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_8
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_9
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_10
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_17));
                ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_HEALTH] = lstVal.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_16));
            }
            return PartialView("_GridTemplate");
        }
        [ValidateAntiForgeryToken]
        public JsonResult SaveTemplate(FormCollection frm)
        {
            int templapteId = !string.IsNullOrEmpty(frm["ProfileTemplateId"]) ? Convert.ToInt32(frm["ProfileTemplateId"]) : 0;
            string templateName = frm["ProfileTemplateName"];
            int TypeSelected = !string.IsNullOrEmpty(frm["hdfTypeID"]) ? int.Parse(frm["hdfTypeID"]) : 0;
            if (string.IsNullOrEmpty(templateName))
            {
                return Json(new JsonMessage("Thầy/cô chưa nhập tên mẫu", JsonMessage.ERROR));
            }
            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(templapteId);
            List<int> lstReportTypeID = null;
            int ReportTypeID = 0;
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TEMPLATE_PST;
                lstReportTypeID = new List<int>
                {
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_5,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_6,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_7,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_15,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_25,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27
                };
            }
            else
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TEMPLATE_CHILDREN;
                lstReportTypeID = new List<int>
                {
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_8,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_9,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_10,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_16,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_17
                };
            }
            bool existstName = false;
            if (templapteId > 0)
            {
                existstName = ProfileTemplateBusiness.All.Any(s => s.ProfileTemplateId != templapteId && s.ProfileTemplateName.ToLower().Equals(templateName.ToLower())
                && s.ReportType == ReportTypeID && s.SchoolId == _globalInfo.SchoolID && s.AppliedLevelID == _globalInfo.AppliedLevel);
            }
            else
            {
                existstName = ProfileTemplateBusiness.All.Any(s => s.ProfileTemplateName.ToLower().Equals(templateName.ToLower())
                && s.ReportType == ReportTypeID && s.SchoolId == _globalInfo.SchoolID && s.AppliedLevelID == _globalInfo.AppliedLevel);
            }
            
            if (existstName)
            {
                return Json(new JsonMessage("Tên mẫu đã được khai báo", JsonMessage.ERROR));
            }

            if (profileTemplate != null && profileTemplate.SchoolId == 0 && profileTemplate.UnitID == 0)
            {
                return Json(new JsonMessage("Thầy/cô không được sửa mẫu của hệ thống", JsonMessage.ERROR));
            }

            IQueryable<ColumnDescription> iquery = ColumnDescriptionBusiness.All
                                        .Where(x => lstReportTypeID.Contains(x.ReportType.Value))
                                        .OrderBy(c => c.OrderID).ThenBy(c => c.ColumnOrder);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string appliedLevel = _globalInfo.AppliedLevel.ToString();
            if (objSP.TrainingTypeID == 3)//truong GDTX
            {
                iquery = iquery.Where(p => p.AppliedLevelID.Contains("6"));
            }
            else
            {
                iquery = iquery.Where(p => p.AppliedLevelID.Contains(appliedLevel));
            }
            List<ColumnDescription> lstColumnDescription = iquery.ToList();
            List<string> lstError = new List<string>();
            List<EmployeeColumnTemplateBO> listTemplateBO = new List<EmployeeColumnTemplateBO>();
            EmployeeColumnTemplateBO objTemplateBO = null;
            for (int i = 0; i < lstColumnDescription.Count; i++)
            {
                ColumnDescription columnDescription = lstColumnDescription[i];
                string chk = frm["chk_" + columnDescription.ColumnDescriptionId];
                if ((chk == null || chk != "on") && !columnDescription.RequiredColumn)
                {
                    continue;
                }
                string excelName = frm["ExcelName_" + columnDescription.ColumnDescriptionId];
                string colExcel = frm["ColExcel_" + columnDescription.ColumnDescriptionId];

                objTemplateBO = new EmployeeColumnTemplateBO
                {
                    ColDesId = columnDescription.ColumnDescriptionId,
                    ColExcel = colExcel,
                    ExcelName = excelName
                };

                if (string.IsNullOrEmpty(colExcel))
                {
                    return Json(new JsonMessage("Thầy/cô chưa nhập cột excel", JsonMessage.ERROR));
                }

                if (string.IsNullOrEmpty(excelName))
                {
                    return Json(new JsonMessage("Thầy/cô chưa nhập tên cột excel", JsonMessage.ERROR));
                }

                string pattern = @"^-*[0-9,\.]+$";
                Match match = Regex.Match(objTemplateBO.ColExcel, pattern, RegexOptions.IgnoreCase);

                if (match.Success)
                    return Json(new JsonMessage(String.Format("Tên cột excel {0} không hợp lệ", objTemplateBO.ColExcel), JsonMessage.ERROR));

                VTVector vTVector = new VTVector(objTemplateBO.ColExcel + "1");
                if (vTVector.X == 0)
                {
                    return Json(new JsonMessage(String.Format("Tên cột excel {0} không hợp lệ", objTemplateBO.ColExcel), JsonMessage.ERROR));
                }
                if (listTemplateBO.Any(s => s.ColExcel == objTemplateBO.ColExcel))
                {
                    return Json(new JsonMessage(String.Format("Tên cột excel {0} đã được sử dụng", objTemplateBO.ColExcel), JsonMessage.ERROR));
                }

                listTemplateBO.Add(objTemplateBO);
            }
            objTemplateBO = new EmployeeColumnTemplateBO()
            {
                ColDesId = -1,
                ColExcel = TypeSelected.ToString(),
                ExcelName = "KeyMH"
            };
            listTemplateBO.Add(objTemplateBO);
            //Validate cot excel;
            if (lstError.Count > 0)
            {
                string msg = string.Join(",", lstError);
                return Json(new JsonMessage(msg, JsonMessage.ERROR));
            }
            if (templapteId > 0)
            {
                if (!string.IsNullOrEmpty(templateName))
                {
                    profileTemplate.ProfileTemplateName = templateName;    
                }
                profileTemplate.ColDescriptionIds = Newtonsoft.Json.JsonConvert.SerializeObject(listTemplateBO);
                profileTemplate.ModifiedDate = DateTime.Now;
                profileTemplate.SchoolId = _globalInfo.SchoolID.Value;
                profileTemplate.ReportType = ReportTypeID;
                ProfileTemplateBusiness.Update(profileTemplate);
                ProfileTemplateBusiness.Save();
                return Json(new { Type = "success", Message = Res.Get("Common_Label_UpdateSuccessMessage"), profiletemplateID = profileTemplate.ProfileTemplateId });
            }
            else
            {
                ProfileTemplate profileTemplateInsert = new ProfileTemplate
                {
                    ProfileTemplateName = templateName,
                    SchoolId = _globalInfo.SchoolID.Value,
                    ColDescriptionIds = Newtonsoft.Json.JsonConvert.SerializeObject(listTemplateBO),
                    ReportType = ReportTypeID,
                    AppliedLevelID = _globalInfo.AppliedLevel,
                    CreatedDate = DateTime.Now,
                };

                int length = profileTemplateInsert.ColDescriptionIds.Length;
                ProfileTemplateBusiness.Insert(profileTemplateInsert);
                ProfileTemplateBusiness.Save();
                return Json(new { Type = "success", Message = Res.Get("Common_Label_AddNewMessage"), profiletemplateID = profileTemplateInsert.ProfileTemplateId });
            }
        }
        [HttpPost]
        public JsonResult DeleteTemplate(int profileTemplateId)
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            ProfileTemplate profile = ProfileTemplateBusiness.Find(profileTemplateId);
            if (profile == null)
            {
                return Json(new JsonMessage("Mẫu excel không hợp lệ", JsonMessage.ERROR));
            }
            if (profile.SchoolId == 0 && profile.UnitID == 0)
            {
                return Json(new JsonMessage("Thầy/cô không xóa được mẫu excel của hệ thống", JsonMessage.ERROR));
            }
            ProfileTemplateBusiness.Delete(profileTemplateId);
            ProfileTemplateBusiness.Save();
            return Json(new { Type = "success", Message = Res.Get("Xóa mẫu excel thành công"), profiletemplateID = 0 });
        }
        public JsonResult ReLoadTemplate(int profileTemplateID)
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            int ReportTypeID = 0;
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TEMPLATE_PST;
            }
            else
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TEMPLATE_CHILDREN;
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", schoolId);
            dic.Add("ReportType", ReportTypeID);
            dic.Add("AppliedLevelID", _globalInfo.AppliedLevel);
            List<ProfileTemplate> lstProfileTemplate = ProfileTemplateBusiness.Search(dic).ToList();

            if (lstProfileTemplate == null)
            {
                lstProfileTemplate = new List<ProfileTemplate>();
            }
            int defaultId = profileTemplateID;
            if (profileTemplateID == 0)
            {
                ProfileTemplate objProfileTempOfSchool = lstProfileTemplate.FirstOrDefault(s => s.SchoolId == schoolId);
                if (objProfileTempOfSchool != null)
                {
                    defaultId = objProfileTempOfSchool.ProfileTemplateId;
                }
                else
                {
                    ProfileTemplate objProfileTemp = lstProfileTemplate.FirstOrDefault(p => p.ProfileTemplateId > 0);
                    if (objProfileTemp != null)
                    {
                        defaultId = objProfileTemp.ProfileTemplateId;
                    }
                }
            }
            

            return Json(new SelectList(lstProfileTemplate, "ProfileTemplateId", "ProfileTemplateName", defaultId));
        }
        public PartialViewResult OrderTemplate(string strtemplateId,int TypeSelected)
        {
            List<int> lstTemplateID = strtemplateId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
            List<int> lstReportTypeID = new List<int>();
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                lstReportTypeID = new List<int>
                {
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_5,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_6,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_7,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_15,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_25,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27
                };
            }
            else
            {
                lstReportTypeID = new List<int>
                {
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_8,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_9,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_10,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_16,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_17
                };
            }
            IQueryable<ColumnDescription> iquery = ColumnDescriptionBusiness.All
                                        .Where(x => lstReportTypeID.Contains(x.ReportType.Value))
                                        .OrderBy(c => c.OrderID).ThenBy(c=>c.ColumnOrder);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string appliedLevel = _globalInfo.AppliedLevel.ToString();
            if (objSP.TrainingTypeID == 3)//truong GDTX
            {
                iquery = iquery.Where(p => p.AppliedLevelID.Contains("6"));
            }
            else
            {
                iquery = iquery.Where(p => p.AppliedLevelID.Contains(appliedLevel));
            }
            List<ColumnDescription> lstColumnDescription = iquery.ToList();

            List<ColumnDescriptionModel> lstColumnModel = new List<ColumnDescriptionModel>();
            VTExport export = new VTExport();
            int count = 1;
            for (int i = 0; i < lstColumnDescription.Count; i++)
            {

                ColumnDescription column = lstColumnDescription[i];
                ColumnDescriptionModel objColumnModel = new ColumnDescriptionModel
                {
                    ColumnDescriptionId = column.ColumnDescriptionId,
                    ColumnName = column.ColumnName,
                    RequiredColumn = column.RequiredColumn,
                    Resolution = column.Resolution,
                    Description = column.Description,
                    ReportTypeID = column.ReportType.HasValue ? column.ReportType.Value : 0
                };
                if (lstTemplateID.Contains(column.ColumnDescriptionId))
                {
                    string columnExcel = SMAS.Business.Common.UtilsBusiness.GetExcelColumnName(count);
                    objColumnModel.ExcelName = column.Resolution;
                    objColumnModel.ColExcel = columnExcel;
                    objColumnModel.CheckValue = true;
                    count++;
                }
                else
                {
                    string columnExcel = VTVector.ColumnIntToString(column.ColumnIndex);
                    objColumnModel.ColExcel = columnExcel;
                    objColumnModel.ExcelName = column.Resolution;
                    objColumnModel.CheckValue = false;
                    objColumnModel.ColumnIndex = column.ColumnIndex;
                }
                lstColumnModel.Add(objColumnModel);
            }
            List<ColumnDescriptionModel> lstSubjectModel = new List<ColumnDescriptionModel>();
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_PUPIL] = lstColumnModel.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_5
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_6
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_7));
                if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    lstSubjectModel = lstColumnModel.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11
                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_25).ToList();
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = SetSubjectWithOrderTemplate(lstSubjectModel);
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstColumnModel.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_15);
                }
                else if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    lstSubjectModel = lstColumnModel.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12
                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26).ToList();
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = SetSubjectWithOrderTemplate(lstSubjectModel);
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstColumnModel.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14);
                }
                else
                {
                    lstSubjectModel = lstColumnModel.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13
                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27).ToList();
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = SetSubjectWithOrderTemplate(lstSubjectModel);
                    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstColumnModel.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14);
                }
            }
            else
            {
                ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_PUPIL] = lstColumnModel.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_8
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_9
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_10
                                                                                || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_17));
                ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_HEALTH] = lstColumnModel.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_16));
            }
            ViewData[ReportGeneralConstants.TYPE_SELECTED] = TypeSelected;
            return PartialView("_GridTemplate");
        }
        public FileResult ExportExcel(int profileTemplateId, int rowId, int EducationLevelID, int ClassID, int SemesterID, int ReportTypeID)
        {
            List<int> lstReportTypeID = new List<int>();
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                lstReportTypeID = new List<int>
                {
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_5,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_6,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_7,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_15,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_25,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27
                };
            }
            else
            {
                lstReportTypeID = new List<int>
                {
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_8,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_9,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_10,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_16,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_17
                };
            }
            Stream excel = ProfileTemplateBusiness.ExportPupilInfo(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, profileTemplateId, rowId,lstReportTypeID,_globalInfo.AppliedLevel.Value,EducationLevelID,ClassID,SemesterID,ReportTypeID);
            // Fix chrome file type
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string FileName = string.Empty;
            if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                FileName = "MN_BaoCaoTongHop.xls";
            }
            else if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                FileName = "TH_BaoCaoTongHop_" + (SemesterID == 1 ? "GiuakyI" : SemesterID == 2 ? "CuoikyI" : SemesterID == 3 ? "GiuakyII" : "CuoikyII") + ".xls";
            }
            else if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                FileName = "THCS_BaoCaoTongHop_" + (SemesterID == 1 ? "HK" + SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST : 
                    SemesterID == 2 ? "HK" + SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND : "Canam") + ".xls";
            }
            else if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                FileName = "THPT_BaoCaoTongHop_" + (SemesterID == 1 ? "HK" + SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST :
                    SemesterID == 2 ? "HK" + SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND : "Canam") + ".xls";
            }
            result.FileDownloadName = ReportUtils.StripVNSign(FileName);
            return result;
        }
        private void SetViewData()
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                List<ComboObject> lstSemester = CommonList.SemesterAndAll();
                ComboObject defaultSemester;
                if (datenow >= FirstStarDate && datenow <= FirstEndDate)
                {
                    defaultSemester = lstSemester[0];
                }
                else
                {
                    defaultSemester = lstSemester[1];
                }
                ViewData[ReportGeneralConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester.key);
            }
            else if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                
                int semesterSelected = 0;
                if (datenow < SecondStartDate)
                {
                    semesterSelected = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI;
                }
                else
                {
                    semesterSelected = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII;
                }
                List<ComboObject> lstSemester = CommonList.Semester_CommunicationNoteReport();
                ViewData[ReportGeneralConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", semesterSelected);
            }
            

            ViewData[ReportGeneralConstants.LIST_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            //Lop
            var lstClass = new List<ClassProfile>();

            ViewData[ReportGeneralConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            this.SetProfileTemplate(0);
            this.SetRowExcel();
            //List<ColumnDescriptionModel> lstVal = GetColumnDescription(0);
            //List<ColumnDescriptionModel> lstSubjectModel = new List<ColumnDescriptionModel>();
            //if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
            //    || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
            //    || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            //{
            //    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_PUPIL] = lstVal.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_5
            //                                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_6
            //                                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_7));
            //    if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY)
            //    {
            //        lstSubjectModel = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11
            //                                        || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_25).ToList();
            //        ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = this.GetListSubjectByListCDPrimary(lstSubjectModel);
            //        ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_15);
            //    }
            //    else if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY)
            //    {
            //        lstSubjectModel = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12
            //                                        || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26).ToList();
            //        ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = this.GetListSubjectByListCD(lstSubjectModel);
            //        ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14);    
            //    }
            //    else
            //    {
            //        lstSubjectModel = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13
            //                                        || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27).ToList();
            //        ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_SUBJECT] = this.GetListSubjectByListCD(lstSubjectModel);
            //        ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_MARK_RESULT] = lstVal.Where(p => p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14);
            //    }
            //}
            //else
            //{
            //    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_PUPIL] = lstVal.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_8
            //                                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_9
            //                                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_10
            //                                                                    || p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_17));
            //    ViewData[ReportGeneralConstants.LIST_COLUMN_TEMPLATE_HEALTH] = lstVal.Where(p => (p.ReportTypeID == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_16));
            //}
            
        }
        private void SetProfileTemplate(int profileTemplateId)
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            int ReportTypeID = 0;
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TEMPLATE_PST;
            }
            else
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TEMPLATE_CHILDREN;
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("TemplateID", profileTemplateId);
            dic.Add("SchoolID", schoolId);
            dic.Add("ReportType", ReportTypeID);
            dic.Add("AppliedLevelID", _globalInfo.AppliedLevel);

            List<ProfileTemplate> lstProfileTemplate = ProfileTemplateBusiness.Search(dic).ToList();
            if (profileTemplateId == 0)
            {
                if (lstProfileTemplate == null)
                {
                    lstProfileTemplate = new List<ProfileTemplate>();
                }
                lstProfileTemplate.Insert(0, new ProfileTemplate { ProfileTemplateName = "[Thêm mới]", ProfileTemplateId = 0 });
            }
            int defaultId = 0;
            ProfileTemplate objProfileTempOfSchool = lstProfileTemplate.FirstOrDefault(s => s.SchoolId == schoolId);
            if (objProfileTempOfSchool != null)
            {
                defaultId = objProfileTempOfSchool.ProfileTemplateId;
            }
            else
            {
                ProfileTemplate objProfileTemp = lstProfileTemplate.FirstOrDefault(p => p.ProfileTemplateId > 0);
                if (objProfileTemp != null)
                {
                    defaultId = objProfileTemp.ProfileTemplateId;
                }
            }
            ViewData[ReportGeneralConstants.LIST_PROFILE_TEMPLATE] = new SelectList(lstProfileTemplate, "ProfileTemplateId", "ProfileTemplateName", defaultId);
        }
        private void SetRowExcel()
        {
            List<RowExcelModel> lstRow = new List<RowExcelModel>();
            for (int i = 1; i <= 20; i++)
            {
                lstRow.Add(new RowExcelModel { RowId = i, RowName = i.ToString() });
            }

            ViewData[ReportGeneralConstants.LIST_ROW_EXCEL] = new SelectList(lstRow, "RowId", "RowName", 1);
        }
        private List<ColumnDescriptionModel> GetColumnDescription(int profileTemplateId)
        {
            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(profileTemplateId);
            List<EmployeeColumnTemplateBO> lstTemplate = new List<EmployeeColumnTemplateBO>();
            if (profileTemplate != null && !string.IsNullOrEmpty(profileTemplate.ColDescriptionIds))
            {
                lstTemplate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeColumnTemplateBO>>(profileTemplate.ColDescriptionIds);
            }
            EmployeeColumnTemplateBO objECT = lstTemplate.Where(p => p.ColDesId == -1).FirstOrDefault();
            ViewData[ReportGeneralConstants.TYPE_SELECTED] = objECT != null ? Int32.Parse(objECT.ColExcel) : 0;
            ViewData[ReportGeneralConstants.IS_SYSTEM_TEMPLATE] = profileTemplate != null && profileTemplate.SchoolId == -1;
            List<int> lstReportTypeID = new List<int>();
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY
                || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                lstReportTypeID = new List<int>
                {
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_5,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_6,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_7,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_15,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_25,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27
                };
            }
            else
            {
                lstReportTypeID = new List<int>
                {
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_8,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_9,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_10,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_16,
                    SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_17
                };
            }
            IQueryable<ColumnDescription> iquery = ColumnDescriptionBusiness.All
                                        .Where(x => lstReportTypeID.Contains(x.ReportType.Value))
                                        .OrderBy(c => c.OrderID).ThenBy(c => c.ColumnOrder);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string appliedLevel = _globalInfo.AppliedLevel.ToString(); 
            if (objSP.TrainingTypeID == 3)//truong GDTX
            {
                iquery = iquery.Where(p => p.AppliedLevelID.Contains("6"));
            }
            else
            {
                iquery = iquery.Where(p => p.AppliedLevelID.Contains(appliedLevel));
            }
            List<ColumnDescription> lstColumnDescription = iquery.ToList();
            List<ColumnDescriptionModel> lstColumnModel = new List<ColumnDescriptionModel>();
            VTExport export = new VTExport();
            for (int i = 0; i < lstColumnDescription.Count; i++)
            {

                ColumnDescription column = lstColumnDescription[i];
                ColumnDescriptionModel objColumnModel = new ColumnDescriptionModel
                {
                    ColumnDescriptionId = column.ColumnDescriptionId,
                    ColumnName = column.ColumnName,
                    Description = column.Description,
                    RequiredColumn = column.RequiredColumn,
                    Resolution = column.Resolution,
                    ReportTypeID = column.ReportType.HasValue ? column.ReportType.Value : 0
                };
                EmployeeColumnTemplateBO employeeColumnTemplateBO = lstTemplate.FirstOrDefault(s => s.ColDesId == column.ColumnDescriptionId);
                if (employeeColumnTemplateBO != null)
                {
                    VTVector vTVector = new VTVector(employeeColumnTemplateBO.ColExcel + "1");
                    objColumnModel.ExcelName = employeeColumnTemplateBO.ExcelName;
                    objColumnModel.ColExcel = employeeColumnTemplateBO.ColExcel;
                    objColumnModel.CheckValue = true;
                    objColumnModel.ColumnIndex = vTVector.Y;
                }
                else
                {
                    string columnExcel = VTVector.ColumnIntToString(column.ColumnIndex);
                    objColumnModel.ColExcel = columnExcel;
                    objColumnModel.ExcelName = column.Resolution;
                    objColumnModel.CheckValue = false;
                    objColumnModel.ColumnIndex = column.ColumnIndex;

                }
                lstColumnModel.Add(objColumnModel);
            }
            //lstColumnModel = lstColumnModel.OrderBy(s => s.ColumnIndex).ToList();
            return lstColumnModel;

        }
        private List<ColumnDescriptionModel> GetListSubjectByListCD(List<ColumnDescriptionModel> lstColumnDescriptionModel)
        {
            ColumnDescriptionModel objResult = null;
            List<int> lstSubjectID = lstColumnDescriptionModel.Where(p => p.Description != null
                                        && !"Ngoại ngữ".Equals(p.Description) && !"Ngoại ngữ 2".Equals(p.Description))
                                        .Select(p => Int32.Parse(p.Description)).Distinct().ToList();
            List<SubjectCat> lstSubjectCat = (from cs in ClassSubjectBusiness.All
                                              join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                              where cp.SchoolID == _globalInfo.SchoolID
                                              && cp.AcademicYearID == _globalInfo.AcademicYearID
                                              && cp.EducationLevel.Grade == _globalInfo.AppliedLevel
                                              && (cs.SectionPerWeekFirstSemester > 0 || cs.SectionPerWeekSecondSemester > 0)
                                              select cs.SubjectCat).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).Distinct().ToList();
            List<int> lstSubjectCatID = lstSubjectCat.Select(p => p.SubjectCatID).Distinct().ToList();
            List<SubjectCat> lstSubjectCatNotinCD = lstSubjectCat.Where(p => !lstSubjectID.Contains(p.SubjectCatID)).ToList();
            List<SubjectCat> lstSubjectForeignLanguage = lstSubjectCat.Where(p => p.IsForeignLanguage).OrderBy(p => p.OrderInSubject).Take(2).Distinct().ToList();
            SubjectCat objSC = null;
            ColumnDescription objInsert = new ColumnDescription();
            int ReportTypeID = 0;
            int ReportTypeInsertID = 0;
            int OrderID = 0;
            if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12;
                ReportTypeInsertID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26;
                OrderID = 9;
            }
            else
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13;
                ReportTypeInsertID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27;
                OrderID = 10;
            }
            List<ColumnDescription> lstCDDB = null;
            ColumnDescription objCDDB = null;
            if (lstSubjectForeignLanguage.Count > 0)
            {
                lstCDDB = ColumnDescriptionBusiness.All.Where(p => p.ReportType == ReportTypeID
                    && (p.Resolution.Equals("Ngoại ngữ") || p.Resolution.Equals("Ngoại ngữ 2"))).ToList();
                for (int i = 0; i < lstSubjectForeignLanguage.Count; i++)
                {
                    if (i == 0)
                    {
                        objCDDB = lstCDDB.Where(p => p.Resolution.Equals("Ngoại ngữ")).FirstOrDefault();
                        objCDDB.Description = lstSubjectForeignLanguage[i].SubjectCatID.ToString();
                        ColumnDescriptionBusiness.Update(objCDDB);
                    }
                    else
                    {
                        objCDDB = lstCDDB.Where(p => p.Resolution.Equals("Ngoại ngữ 2")).FirstOrDefault();
                        objCDDB.Description = lstSubjectForeignLanguage[i].SubjectCatID.ToString();
                        ColumnDescriptionBusiness.Update(objCDDB);
                        break;
                    }
                }
            }
            else
            {
                lstCDDB = ColumnDescriptionBusiness.All.Where(p => p.ReportType == ReportTypeID
                    && (p.Resolution.Equals("Ngoại ngữ") || p.Resolution.Equals("Ngoại ngữ 2"))).ToList();
                for (int i = 0; i < lstCDDB.Count; i++)
                {
                    lstCDDB[i].Description = "0";
                    ColumnDescriptionBusiness.Update(lstCDDB[i]);
                }
            }

            lstSubjectCatNotinCD = lstSubjectCatNotinCD.Except(lstSubjectForeignLanguage).ToList();
            int ColumnOrder = lstColumnDescriptionModel.Where(p => p.Description != null).Count() + 1;
            for (int i = 0; i < lstSubjectCatNotinCD.Count; i++)
            {
                objSC = lstSubjectCatNotinCD[i];
                objResult = new ColumnDescriptionModel();
                objResult.ColumnName = "SUBJECT_NAME";
                objResult.TableName = "SUBJECT_CAT";
                objResult.ColumnOrder = ColumnOrder;
                objResult.ReportTypeID = ReportTypeInsertID;
                objResult.Description = objSC.SubjectCatID.ToString();
                objResult.Resolution = objSC.SubjectName;
                objResult.ExcelName = objSC.SubjectName;
                lstColumnDescriptionModel.Add(objResult);
                //Tao doi tuong insert vao bang ColumnDescription
                objInsert = new ColumnDescription();
                objInsert.ColumnName = objResult.ColumnName;
                objInsert.TableName = objResult.TableName;
                objInsert.Description = objSC.SubjectCatID.ToString();
                objInsert.Resolution = objSC.SubjectName;
                objInsert.FieldName = "SUBJECT_CAT";
                objInsert.ColumnSize = 20;
                objInsert.ColumnType = "String";
                objInsert.CreatedDate = DateTime.Now;
                objInsert.ModifiedDate = DateTime.Now;
                objInsert.ColumnIndex = 0;
                objInsert.ColumnOrder = ColumnOrder;
                objInsert.OrderID = OrderID;
                objInsert.RequiredColumn = false;
                objInsert.ReportType = ReportTypeInsertID;
                objInsert.AppliedLevelID = "1,2,3,6";
                ColumnDescriptionBusiness.Insert(objInsert);
                ColumnOrder++;
            }
            ColumnDescriptionBusiness.Save();
            List<ColumnDescriptionModel> lstResult = new List<ColumnDescriptionModel>();
            List<ColumnDescriptionModel> lstResult1 = new List<ColumnDescriptionModel>();
            List<ColumnDescriptionModel> lstResult2 = new List<ColumnDescriptionModel>();

            List<int> lstSubjectForeignLanguageID = lstSubjectForeignLanguage.Select(p => p.SubjectCatID).Distinct().ToList();
            lstSubjectCatID = lstSubjectCatID.Except(lstSubjectForeignLanguageID).ToList();

            lstResult1 = lstColumnDescriptionModel.Where(p => p.ReportTypeID == ReportTypeID).ToList();
            lstResult2 = lstColumnDescriptionModel.Where(p => p.ReportTypeID == ReportTypeInsertID && lstSubjectCatID.Contains(int.Parse(p.Description))).ToList();
            lstResult = lstResult1.Union(lstResult2).ToList();
            return lstResult;
        }
        private List<ColumnDescriptionModel> GetListSubjectByListCDPrimary(List<ColumnDescriptionModel> lstColumnDescriptionModel)
        {
            ColumnDescriptionModel objResult = null;
            List<int> lstSubjectID = lstColumnDescriptionModel.Where(p => p.Description != null
                                        && !"Ngoại ngữ - Mức đạt được".Equals(p.Resolution) && !"Ngoại ngữ - Điểm KTĐK".Equals(p.Resolution)).Select(p => Int32.Parse(p.Description)).ToList();
            int ColumnOrder = lstColumnDescriptionModel.Where(p => p.Description != null).Count() + 1;
            lstSubjectID = lstSubjectID.Select(p => p).Distinct().ToList();
            List<SubjectCat> lstSubjectCat = (from cs in ClassSubjectBusiness.All
                                              join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                              where cp.SchoolID == _globalInfo.SchoolID
                                              && cp.AcademicYearID == _globalInfo.AcademicYearID
                                              && cp.EducationLevel.Grade == _globalInfo.AppliedLevel
                                              && (cs.SectionPerWeekFirstSemester > 0 || cs.SectionPerWeekSecondSemester > 0)
                                              select cs.SubjectCat).OrderBy(p=>p.OrderInSubject).ThenBy(p=>p.SubjectName).Distinct().ToList();
            List<int> lstSubjectCatID = lstSubjectCat.Select(p => p.SubjectCatID).Distinct().ToList();

            List<SubjectCat> lstSubjectCatNotinCD = lstSubjectCat.Where(p => !lstSubjectID.Contains(p.SubjectCatID)).ToList();

            SubjectCat objSubjectForeignLanguage = lstSubjectCat.Where(p => p.IsForeignLanguage).OrderBy(p => p.OrderInSubject).FirstOrDefault();
            ColumnDescription objInsert = new ColumnDescription();
            int ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11;
            int ReportTypeInsertID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_25;
            int OrderID = 8;
            List<ColumnDescription> lstCDDB = null;
            ColumnDescription objCDDB = null;
            if (objSubjectForeignLanguage != null)
            {
                lstCDDB = ColumnDescriptionBusiness.All.Where(p => p.ReportType == ReportTypeID && (p.Resolution.Equals("Ngoại ngữ - Mức đạt được")
                    || p.Resolution.Equals("Ngoại ngữ - Điểm KTĐK"))).ToList();
                for (int i = 0; i < lstCDDB.Count; i++)
                {
                    objCDDB = lstCDDB[i];
                    objCDDB.Description = objSubjectForeignLanguage.SubjectCatID.ToString();
                    ColumnDescriptionBusiness.Update(objCDDB);
                }
            }
            else
            {
                lstCDDB = ColumnDescriptionBusiness.All.Where(p => p.ReportType == ReportTypeID && (p.Resolution.Equals("Ngoại ngữ - Mức đạt được")
                    || p.Resolution.Equals("Ngoại ngữ - Điểm KTĐK"))).ToList();
                for (int i = 0; i < lstCDDB.Count; i++)
                {
                    objCDDB = lstCDDB[i];
                    objCDDB.Description = "0";
                    ColumnDescriptionBusiness.Update(objCDDB);
                }
            }

            lstSubjectCatNotinCD = lstSubjectCatNotinCD.Where(p => p.SubjectCatID != objSubjectForeignLanguage.SubjectCatID).ToList();
            SubjectCat objSC = null;
            for (int i = 0; i < lstSubjectCatNotinCD.Count; i++)
            {
                objSC = lstSubjectCatNotinCD[i];
                objResult = new ColumnDescriptionModel();
                objResult.ColumnName = "SUBJECT_NAME_EVALUATION";
                objResult.TableName = "SUBJECT_CAT";
                objResult.ColumnOrder = ColumnOrder;
                objResult.ReportTypeID = ReportTypeInsertID;
                objResult.Resolution = objSC.SubjectName + " - Mức đạt được";
                objResult.Description = objSC.SubjectCatID.ToString();
                lstColumnDescriptionModel.Add(objResult);

                objInsert = new ColumnDescription();
                objInsert.ColumnName = objResult.ColumnName;
                objInsert.TableName = objResult.TableName;
                objInsert.Description = objSC.SubjectCatID.ToString();
                objInsert.Resolution = objSC.SubjectName + " - Mức đạt được";
                objInsert.FieldName = "SUBJECT_CAT";
                objInsert.ColumnSize = 20;
                objInsert.ColumnType = "String";
                objInsert.CreatedDate = DateTime.Now;
                objInsert.ModifiedDate = DateTime.Now;
                objInsert.ColumnIndex = 0;
                objInsert.ColumnOrder = ColumnOrder;
                objInsert.OrderID = OrderID;
                objInsert.RequiredColumn = false;
                objInsert.ReportType = ReportTypeInsertID;
                objInsert.AppliedLevelID = "1,2,3,6";
                ColumnDescriptionBusiness.Insert(objInsert);

                if (objSC.IsCommenting == 0)
                {
                    ColumnOrder++;
                    objResult = new ColumnDescriptionModel();
                    objResult.ColumnName = "SUBJECT_NAME_MARK";
                    objResult.TableName = "SUBJECT_CAT";
                    objResult.ColumnOrder = ColumnOrder;
                    objResult.ReportTypeID = ReportTypeInsertID;
                    objResult.Resolution = objSC.SubjectName + " - Điểm KTĐK";
                    lstColumnDescriptionModel.Add(objResult);

                    objInsert = new ColumnDescription();
                    objInsert.ColumnName = objResult.ColumnName;
                    objInsert.TableName = objResult.TableName;
                    objInsert.Description = objSC.SubjectCatID.ToString();
                    objInsert.Resolution = objSC.SubjectName + " - Điểm KTĐK";
                    objInsert.FieldName = "SUBJECT_CAT";
                    objInsert.ColumnSize = 20;
                    objInsert.ColumnType = "String";
                    objInsert.CreatedDate = DateTime.Now;
                    objInsert.ModifiedDate = DateTime.Now;
                    objInsert.ColumnIndex = 0;
                    objInsert.ColumnOrder = ColumnOrder;
                    objInsert.OrderID = OrderID;
                    objInsert.RequiredColumn = false;
                    objInsert.ReportType = ReportTypeInsertID;
                    objInsert.AppliedLevelID = "1,2,3,6";
                    ColumnDescriptionBusiness.Insert(objInsert);
                }
                ColumnOrder++;
            }
            ColumnDescriptionBusiness.Save();

            List<ColumnDescriptionModel> lstResult = new List<ColumnDescriptionModel>();
            List<ColumnDescriptionModel> lstResult1 = new List<ColumnDescriptionModel>();
            List<ColumnDescriptionModel> lstResult2 = new List<ColumnDescriptionModel>();

            lstSubjectCatID = lstSubjectCatID.Where(p => p != objSubjectForeignLanguage.SubjectCatID).ToList();

            lstResult1 = lstColumnDescriptionModel.Where(p => p.ReportTypeID == ReportTypeID).ToList();
            lstResult2 = lstColumnDescriptionModel.Where(p => p.ReportTypeID == ReportTypeInsertID && lstSubjectCatID.Contains(int.Parse(p.Description))).ToList();
            lstResult = lstResult1.Union(lstResult2).ToList();

            return lstResult;
        }
        private List<ColumnDescriptionModel> SetSubjectWithOrderTemplate(List<ColumnDescriptionModel> lstColumnDescriptionModel)
        {
            List<SubjectCat> lstSubjectCat = (from cs in ClassSubjectBusiness.All
                                              join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                              where cp.SchoolID == _globalInfo.SchoolID
                                              && cp.AcademicYearID == _globalInfo.AcademicYearID
                                              && cp.EducationLevel.Grade == _globalInfo.AppliedLevel
                                              && (cs.SectionPerWeekFirstSemester > 0 || cs.SectionPerWeekSecondSemester > 0)
                                              select cs.SubjectCat).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).Distinct().ToList();
            List<SubjectCat> lstSubjectForeignLanguage = lstSubjectCat.Where(p => p.IsForeignLanguage).OrderBy(p => p.OrderInSubject).Take(2).Distinct().ToList();
            List<int> lstSubjectCatID = lstSubjectCat.Select(p => p.SubjectCatID).Distinct().ToList();
            int ReportTypeID = 0;
            int ReportTypeInsertID = 0;
            if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12;
                ReportTypeInsertID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_26;
            }
            else
            {
                ReportTypeID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13;
                ReportTypeInsertID = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_27;
            }

            List<ColumnDescriptionModel> lstResult = new List<ColumnDescriptionModel>();
            List<ColumnDescriptionModel> lstResult1 = new List<ColumnDescriptionModel>();
            List<ColumnDescriptionModel> lstResult2 = new List<ColumnDescriptionModel>();

            List<int> lstSubjectForeignLanguageID = lstSubjectForeignLanguage.Select(p => p.SubjectCatID).Distinct().ToList();
            lstSubjectCatID = lstSubjectCatID.Except(lstSubjectForeignLanguageID).ToList();

            lstResult1 = lstColumnDescriptionModel.Where(p => p.ReportTypeID == ReportTypeID).ToList();
            lstResult2 = lstColumnDescriptionModel.Where(p => p.ReportTypeID == ReportTypeInsertID && lstSubjectCatID.Contains(int.Parse(p.Description))).ToList();
            lstResult = lstResult1.Union(lstResult2).ToList();
            return lstResult;
        }
    }
}