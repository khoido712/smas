﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SMSDepartmentArea
{
    public class SMSDepartmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SMSDepartmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SMSDepartmentArea_default",
                "SMSDepartmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
