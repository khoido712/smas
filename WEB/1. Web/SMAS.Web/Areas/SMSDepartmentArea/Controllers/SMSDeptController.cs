﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  BaLX
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.SMSDepartmentArea.Models;

namespace SMAS.Web.Areas.SMSDepartmentArea.Controllers
{
    public class SMSDeptController : BaseController
    {
        #region variable

        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ISendInfoBusiness SendInfoBusiness;
        private readonly IHistorySMSBusiness HistorySMSBusiness;
        private readonly ISMSTeacherContractBusiness SMSTeacherContractBusiness;
        private readonly ICallDetailRecordBusiness CallDetailRecordBusiness;

        #endregion variable

        #region contructor

        public SMSDeptController(IEmployeeBusiness employeeBusiness,
            IUserAccountBusiness UserAccountBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            ISendInfoBusiness SendInfoBusiness,
            IHistorySMSBusiness HistorySMSBusiness,
            ISMSTeacherContractBusiness SMSTeacherContractBusiness,
            ICallDetailRecordBusiness CallDetailRecordBusiness)
        {
            this.EmployeeBusiness = employeeBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SendInfoBusiness = SendInfoBusiness;
            this.HistorySMSBusiness = HistorySMSBusiness;
            this.SMSTeacherContractBusiness = SMSTeacherContractBusiness;
            this.CallDetailRecordBusiness = CallDetailRecordBusiness;
        }

        #endregion contructor

        #region index

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #endregion index

        #region search

        
        public PartialViewResult Search(DeptSearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["FullName"] = frm.Fullname;

            IEnumerable<SMSDeptViewModel> lst = this._Search(SearchInfo);
            ViewData[SMSDepartmentConstants.LIST_EMPLOYEE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        private IEnumerable<SMSDeptViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            GlobalInfo global = new GlobalInfo();
            IQueryable<Employee> query = this.EmployeeBusiness.SearchEmployee(global.SupervisingDeptID.Value, SearchInfo);
            IQueryable<SMSDeptViewModel> lst = query.Select(o => new SMSDeptViewModel
            {
                EmployeeID = o.EmployeeID,
                FullName = o.FullName,
                BirthDate = o.BirthDate,
                Mobile = o.Mobile,
                PermanentResidentalAddress = o.PermanentResidentalAddress,
            });

            return lst;
        }

        #endregion search

        #region setViewData

        public void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            //Get view data here
            if (hasSMSLeft())
            {
                IEnumerable<SMSDeptViewModel> lst = this._Search(SearchInfo);
                ViewData[SMSDepartmentConstants.LIST_EMPLOYEE] = lst;
            }

            List<SupervisingDept> lstDept = new List<SupervisingDept>();
            lstDept.Add(SupervisingDeptBusiness.Find(global.SupervisingDeptID));
            ViewData[SMSDepartmentConstants.LS_SUPERVISINGDEPT] = new SelectList(lstDept, "SupervisingDeptID", "SupervisingDeptName");
        }

        private bool hasSMSLeft()
        {
            GlobalInfo global = new GlobalInfo();

            // Gọi WS để lấy thông tin hợp đồng SMS Teacher
            SMSTeacherContractBO contract = new SMSTeacherContractBO();
            contract.SupervisingDeptCode = SupervisingDeptBusiness.Find(global.SupervisingDeptID.Value).SupervisingDeptCode;
            contract.Status = -1;
            contract.MaxSMS = -1;
            List<SMSTeacherContractBO> lstContract = SMSTeacherContractBusiness.GetSMSTeacherContract(contract);

            // Nếu chưa đăng ký SMS Teacher
            if (lstContract == null || lstContract.Count() == 0)
            {
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = Res.Get("SMSTeacher_Warning_Unregistered");
                ViewData[SMSDepartmentConstants.SEND_INFO] = "";
                return false;
            }

            // Nếu có đăng ký SMS Teacher nhưng đang bị chặn cắt
            if (lstContract[0].Status == SystemParamsInFile.SMS_TEACHER_BLOCK_STATUS)
            {
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = Res.Get("SMSTeacher_Warning_Closed");
                ViewData[SMSDepartmentConstants.SEND_INFO] = "";
                return false;
            }

            // Nếu đang hoạt động bình thường
            if (lstContract[0].Status == SystemParamsInFile.SMS_TEACHER_ACTIVE_STATUS)
            {
                Session["PackageID"] = lstContract[0].PackageID;
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = "";

                // Gọi WS để lấy số SMS đã gửi
                List<CallDetailRecordBO> lstCDR = CallDetailRecordBusiness.GetCDROfSMSTeacherContract(contract);
                int numSentInMonth = lstCDR.Where(o => o.CallDetailRecord.StaDatetime.ToString("MM/yyyy").Equals(DateTime.Now.ToString("MM/yyyy"))).Count();
                int numSentViettelInMonth = lstCDR.Where(o => o.CallDetailRecord.StaDatetime.ToString("MM/yyyy").Equals(DateTime.Now.ToString("MM/yyyy"))
                                                              && o.CallDetailRecord.Supplier == (int)SystemParamsInFile.NetworkProviders.VIETTEL).Count();
                ViewData[SMSDepartmentConstants.SEND_INFO] = String.Format(Res.Get("SMSTeacher_Warning_SendInfo"),
                                DateTime.Now.ToString("MM/yyyy"), numSentInMonth, numSentViettelInMonth, numSentInMonth - numSentViettelInMonth);
                return true;
            }

            return false;
        }

        #endregion setViewData

        #region send SMS
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendSMS(int[] checkedToSend, String message)
        {
            if (message.Trim().Length == 0)
                return Json(new JsonMessage(Res.Get("Message_Label_ErrorContentEmpty")));

            GlobalInfo global = new GlobalInfo();

            List<SendInfo> lstSendInfo = new List<SendInfo>();
            foreach (var item in checkedToSend)
            {
                // Tao doi tuong SendInfo
                Employee employee = EmployeeBusiness.Find(item);
                SendInfo s = new SendInfo();
                //s.SupervisingDeptCode = school.SupervisingDept.SupervisingDeptCode;
                s.EmployeeCode = employee.EmployeeCode;
                s.InfoType = SystemParamsInFile.INFO_TYPE_EMPLOYEE_UNEXPECTED;
                s.TypeOfReceiver = SystemParamsInFile.RECEIVE_TYPE_SUB_EMPLOYEE;
                s.SchedularType = SystemParamsInFile.SCHEDULE_UNEXPECTED;
                s.PackageID = Convert.ToInt16(Session["PackageID"]);
                s.Title = Res.Get("SMSDept_Title_Message");
                s.Content = SupervisingDeptBusiness.Find(global.SupervisingDeptID).SupervisingDeptName + " " + message;
                s.SendType = SystemParamsInFile.SEND_TYPE_MOBILE;
                UserAccount UserAccount = this.UserAccountBusiness.Find(global.UserAccountID);
                s.CreateUser = UserAccount.aspnet_Users.UserName;
                s.ReceiverMobile = employee.Mobile;
                s.ReceiverName = employee.FullName;
                lstSendInfo.Add(s);
            }

            SendInfoBusiness.InsertSendInfo(lstSendInfo);
            SendInfoBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion send SMS
    }
}





