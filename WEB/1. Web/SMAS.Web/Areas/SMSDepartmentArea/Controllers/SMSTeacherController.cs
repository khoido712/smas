﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  BaLX 
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.SMSDepartmentArea.Models;

namespace SMAS.Web.Areas.SMSDepartmentArea.Controllers
{
    public class SMSTeacherController : BaseController
    {
        #region variable

        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISendInfoBusiness SendInfoBusiness;
        private readonly IHistorySMSBusiness HistorySMSBusiness;
        private readonly ISMSTeacherContractBusiness SMSTeacherContractBusiness;
        private readonly ICallDetailRecordBusiness CallDetailRecordBusiness;

        #endregion variable

        #region contructor

        public SMSTeacherController(IEmployeeBusiness employeeBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            IWorkTypeBusiness WorkTypeBusiness,
            IContractTypeBusiness ContractTypeBusiness,
            ITrainingTypeBusiness TrainingTypeBusiness,
            IDistrictBusiness DistrictBusiness,
            IUserAccountBusiness UserAccountBusiness,
            ISendInfoBusiness SendInfoBusiness,
            IHistorySMSBusiness HistorySMSBusiness,
            ISMSTeacherContractBusiness SMSTeacherContractBusiness,
            ICallDetailRecordBusiness CallDetailRecordBusiness)
        {
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.WorkTypeBusiness = WorkTypeBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.TrainingTypeBusiness = TrainingTypeBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.SendInfoBusiness = SendInfoBusiness;
            this.HistorySMSBusiness = HistorySMSBusiness;
            this.SMSTeacherContractBusiness = SMSTeacherContractBusiness;
            this.CallDetailRecordBusiness = CallDetailRecordBusiness;
        }

        #endregion contructor

        #region index

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #endregion index

        #region search

        
        public PartialViewResult Search(TeacherSearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
            SearchInfo["DistrictID"] = frm.District;
            SearchInfo["TrainingTypeID"] = frm.TrainingType;
            SearchInfo["TraversalPath"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).TraversalPath;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["Genre"] = frm.Sex;
            SearchInfo["EmployeeStatus"] = frm.EmploymentStatus;
            SearchInfo["SchoolID"] = frm.School;
            if (frm.EducationGrade.HasValue)
            {
                if (frm.EducationGrade == 1)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_PRIMARY;
                }
                if (frm.EducationGrade == 2)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_SECONDARY;
                }
                if (frm.EducationGrade == 3)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_TERTIARY;
                }
            }

            IEnumerable<SMSTeacherViewModel> lst = this._Search(SearchInfo);
            ViewData[SMSDepartmentConstants.LIST_EMPLOYEE] = lst;

            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<SMSTeacherViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            GlobalInfo global = new GlobalInfo();

            SearchInfo["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
            IQueryable<Employee> query = this.EmployeeBusiness.SearchByArea(SearchInfo);
            IQueryable<SMSTeacherViewModel> lst = query.Select(o => new SMSTeacherViewModel
            {
                EmployeeID = o.EmployeeID,
                EmployeeStatus = o.EmployeeHistoryStatus.FirstOrDefault().EmployeeStatus,
                SchoolID = o.SchoolID,
                FullName = o.FullName,
                BirthDate = o.BirthDate,
                Mobile = o.Mobile,
                Genre = o.Genre,
                WorkTypeID = o.WorkTypeID,
                ContractTypeID = o.ContractTypeID,
                IsActive = o.IsActive,
                Sex = o.Genre ? male : female                
            });

            List<SMSTeacherViewModel> lsSMSTeacherViewModel = lst.ToList();
            foreach (SMSTeacherViewModel item in lsSMSTeacherViewModel)
            {
                if (item.EmployeeStatus.HasValue)
                {
                    if (item.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
                    {
                        item.EmployeeStatusName = Res.Get("Employee_Label_EmployeeStatusWorking");
                    }
                    else if (item.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_MOVED)
                    {
                        item.EmployeeStatusName = Res.Get("Employee_Label_EmployeeStatusWorkingChange");
                    }
                    else if (item.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_RETIRED)
                    {
                        item.EmployeeStatusName = Res.Get("Employee_Label_EmployeeStatusRetired");
                    }
                }

                if (item.WorkTypeID.HasValue)
                {
                    WorkType workType = WorkTypeBusiness.All.Where(o => (o.WorkTypeID == item.WorkTypeID.Value)).FirstOrDefault();
                    item.WorkTypeName = workType.Resolution;
                }

                if (item.ContractTypeID.HasValue)
                {
                    item.ContractTypeName = ContractTypeBusiness.Find(item.ContractTypeID).Resolution;
                }
            }
            return lsSMSTeacherViewModel;
        }

        #endregion search

        #region setViewData

        public void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            //Get view data here
            SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
            SearchInfo["TraversalPath"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).TraversalPath;
            if (hasSMSLeft())
            {
                IEnumerable<SMSTeacherViewModel> lst = this._Search(SearchInfo);
                ViewData[SMSDepartmentConstants.LIST_EMPLOYEE] = lst;
            }

            // Trang thai
            ViewData[SMSDepartmentConstants.LISTEMPLOYMENTSTATUS] = new SelectList(CommonList.EmployeeStatus(), "key", "value");

            // Gioi tinh
            ViewData[SMSDepartmentConstants.LISTSEX] = new SelectList(CommonList.GenreAndAll(), "key", "value");

            // Quan huyen
            SearchInfo = new Dictionary<string, object>();
            List<District> lstDistrict = new List<District>();
            if (global.IsSuperVisingDeptRole)
            {
                SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
                lstDistrict = DistrictBusiness.Search(SearchInfo).ToList();
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                District district = SupervisingDeptBusiness.Find(global.SupervisingDeptID).District;
                lstDistrict.Add(district);
            }
            ViewData[SMSDepartmentConstants.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName");

            // Loai hinh dao tao
            SearchInfo = new Dictionary<string, object>();
            IEnumerable<TrainingType> lstTrainingType = TrainingTypeBusiness.Search(SearchInfo);
            ViewData[SMSDepartmentConstants.LIST_TRAINING_TYPE] = new SelectList(lstTrainingType, "TrainingTypeID", "Resolution");

            // Cap hoc
            ViewData[SMSDepartmentConstants.LS_APPLIEDLEVEL] = new SelectList(CommonList.AppliedLevel(), "key", "value");

            // Truong
            SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
            SearchInfo["TraversalPath"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).TraversalPath;

            IEnumerable<SchoolProfile> lstSchoolProfile = SchoolProfileBusiness.Search(SearchInfo);
            ViewData[SMSDepartmentConstants.LIST_SCHOOL] = new SelectList(lstSchoolProfile, "SchoolProfileID", "SchoolName");
        }

        private bool hasSMSLeft()
        {
            GlobalInfo global = new GlobalInfo();

            // Gọi WS để lấy thông tin hợp đồng SMS Teacher
            SMSTeacherContractBO contract = new SMSTeacherContractBO();
            contract.SupervisingDeptCode = SupervisingDeptBusiness.Find(global.SupervisingDeptID.Value).SupervisingDeptCode;
            contract.Status = -1;
            contract.MaxSMS = -1;
            List<SMSTeacherContractBO> lstContract = SMSTeacherContractBusiness.GetSMSTeacherContract(contract);

            // Nếu chưa đăng ký SMS Teacher
            if (lstContract == null || lstContract.Count() == 0)
            {
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = Res.Get("SMSTeacher_Warning_Unregistered");
                ViewData[SMSDepartmentConstants.SEND_INFO] = "";
                return false;
            }

            // Nếu có đăng ký SMS Teacher nhưng đang bị chặn cắt
            if (lstContract[0].Status == SystemParamsInFile.SMS_TEACHER_BLOCK_STATUS)
            {
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = Res.Get("SMSTeacher_Warning_Closed");
                ViewData[SMSDepartmentConstants.SEND_INFO] = "";
                return false;
            }

            // Nếu đang hoạt động bình thường
            if (lstContract[0].Status == SystemParamsInFile.SMS_TEACHER_ACTIVE_STATUS)
            {
                Session["PackageID"] = lstContract[0].PackageID;
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = "";

                // Gọi WS để lấy số SMS đã gửi
                List<CallDetailRecordBO> lstCDR = CallDetailRecordBusiness.GetCDROfSMSTeacherContract(contract);
                int numSentInMonth = lstCDR.Where(o => o.CallDetailRecord.StaDatetime.ToString("MM/yyyy").Equals(DateTime.Now.ToString("MM/yyyy"))).Count();
                int numSentViettelInMonth = lstCDR.Where(o => o.CallDetailRecord.StaDatetime.ToString("MM/yyyy").Equals(DateTime.Now.ToString("MM/yyyy"))
                                                              && o.CallDetailRecord.Supplier == (int)SystemParamsInFile.NetworkProviders.VIETTEL).Count();
                ViewData[SMSDepartmentConstants.SEND_INFO] = String.Format(Res.Get("SMSTeacher_Warning_SendInfo"),
                                DateTime.Now.ToString("MM/yyyy"), numSentInMonth, numSentViettelInMonth, numSentInMonth - numSentViettelInMonth);
                return true;
            }

            return false;
        }

        #endregion setViewData
                
        #region reload grid


        [ValidateAntiForgeryToken]
        public JsonResult ReloadGrid(int? DistrictID, int? TrainingTypeID, int? AppliedLevel)
        {
            GlobalInfo global = new GlobalInfo();
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
            SearchInfo["DistrictID"] = DistrictID;
            SearchInfo["TrainingTypeID"] = TrainingTypeID;
            SearchInfo["TraversalPath"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).TraversalPath;
            if (AppliedLevel.HasValue)
            {
                if (AppliedLevel == 1)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_PRIMARY;
                }
                if (AppliedLevel == 2)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_SECONDARY;
                }
                if (AppliedLevel == 3)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_TERTIARY;
                }
            }

            IEnumerable<SchoolProfile> lstSchoolProfile = SchoolProfileBusiness.Search(SearchInfo);
            if (lstSchoolProfile.Count() != 0)
            {
                return Json(new SelectList(lstSchoolProfile.ToList(), "SchoolProfileID", "SchoolName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion reload grid

        #region send SMS
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendSMS(int[] checkedToSend, String message)
        {
            if (message.Trim().Length == 0)
                return Json(new JsonMessage(Res.Get("Message_Label_ErrorContentEmpty")));

            GlobalInfo global = new GlobalInfo();

            List<SendInfo> lstSendInfo = new List<SendInfo>();
            foreach (var item in checkedToSend)
            {
                // Tao doi tuong SendInfo
                Employee employee = EmployeeBusiness.Find(item);
                SendInfo s = new SendInfo();
                //s.SupervisingDeptCode = school.SupervisingDept.SupervisingDeptCode;
                s.SchoolID = employee.SchoolID;
                s.EmployeeCode = employee.EmployeeCode;
                s.InfoType = SystemParamsInFile.INFO_TYPE_TEACHER_UNEXPECTED;
                s.TypeOfReceiver = SystemParamsInFile.RECEIVE_TYPE_TEACHER;
                s.SchedularType = SystemParamsInFile.SCHEDULE_UNEXPECTED;
                s.PackageID = Convert.ToInt16(Session["PackageID"]);
                s.Title = Res.Get("SMSTeacher_Title_Message");
                s.Content = SupervisingDeptBusiness.Find(global.SupervisingDeptID).SupervisingDeptName + " " + message;
                s.SendType = SystemParamsInFile.SEND_TYPE_MOBILE;
                UserAccount UserAccount = this.UserAccountBusiness.Find(global.UserAccountID);
                s.CreateUser = UserAccount.aspnet_Users.UserName;
                s.ReceiverMobile = employee.Mobile;
                s.ReceiverName = employee.FullName;
                lstSendInfo.Add(s);
            }

            SendInfoBusiness.InsertSendInfo(lstSendInfo);
            SendInfoBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion send SMS
    }
}





