﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  BaLX
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.SMSDepartmentArea.Models;

namespace SMAS.Web.Areas.SMSDepartmentArea.Controllers
{
    public class SMSSchoolController : BaseController
    {
        #region private member variable
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAreaBusiness AreaBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly ISchoolTypeBusiness SchoolTypeBusiness;
        private readonly ISchoolPropertyCatBusiness SchoolPropertyCatBusiness;
        private readonly ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        private readonly IPropertyOfSchoolBusiness PropertyOfSchoolBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly Iaspnet_UsersBusiness aspnet_UsersBusiness;
        private readonly ISendInfoBusiness SendInfoBusiness;
        private readonly IHistorySMSBusiness HistorySMSBusiness;
        private readonly ISMSTeacherContractBusiness SMSTeacherContractBusiness;
        private readonly ICallDetailRecordBusiness CallDetailRecordBusiness;

        #endregion

        #region Constructors
        public SMSSchoolController(ISchoolProfileBusiness schoolprofileBusiness,
            IAreaBusiness areabusiness,
            IProvinceBusiness provincebusiness,
            IDistrictBusiness districtbusiness,
            ICommuneBusiness communeBusiness,
            ISupervisingDeptBusiness supervisingDeptBusiness,
            ITrainingTypeBusiness trainingTypeBusiness,
            ISchoolTypeBusiness schoolTypeBusiness,
            ISchoolPropertyCatBusiness schoolPropertyCatBusiness,
            ISchoolSubsidiaryBusiness schoolSubsidiaryBusiness,
            IPropertyOfSchoolBusiness propertyOfSchoolBusiness,
            IUserAccountBusiness userAccountBusiness,
            Iaspnet_UsersBusiness Aspnet_UsersBusiness,
            ISendInfoBusiness SendInfoBusiness,
            IHistorySMSBusiness HistorySMSBusiness,
            ISMSTeacherContractBusiness SMSTeacherContractBusiness,
            ICallDetailRecordBusiness CallDetailRecordBusiness)
        {
            this.SchoolProfileBusiness = schoolprofileBusiness;
            this.AreaBusiness = areabusiness;
            this.ProvinceBusiness = provincebusiness;
            this.DistrictBusiness = districtbusiness;
            this.CommuneBusiness = communeBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.SchoolTypeBusiness = schoolTypeBusiness;
            this.SchoolPropertyCatBusiness = schoolPropertyCatBusiness;
            this.SchoolSubsidiaryBusiness = schoolSubsidiaryBusiness;
            this.PropertyOfSchoolBusiness = propertyOfSchoolBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.aspnet_UsersBusiness = Aspnet_UsersBusiness;
            this.SendInfoBusiness = SendInfoBusiness;
            this.HistorySMSBusiness = HistorySMSBusiness;
            this.SMSTeacherContractBusiness = SMSTeacherContractBusiness;
            this.CallDetailRecordBusiness = CallDetailRecordBusiness;
        }
        #endregion

        #region index

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #endregion index

        #region search
        public PartialViewResult Search(SchoolSearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["SchoolCode"] = frm.SchoolCode;
            SearchInfo["SchoolName"] = frm.SchoolName;
            SearchInfo["HeadMasterName"] = frm.HeadMasterName;
            SearchInfo["DistrictID"] = frm.District;
            SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
            
            SearchInfo["TrainingTypeID"] = frm.TrainingType;
            SearchInfo["EducationGrade"] = frm.EducationGrade;

            IEnumerable<SMSSchoolViewModel> lst = this._Search(SearchInfo);
            ViewData[SMSDepartmentConstants.LIST_SCHOOL] = lst;

            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SchoolProfile"></param>
        /// <returns></returns>
        private IEnumerable<SMSSchoolViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            GlobalInfo global = new GlobalInfo();
            IQueryable<SchoolProfile> query = this.SchoolProfileBusiness.Search(SearchInfo);
            IQueryable<SMSSchoolViewModel> lst = query.Select(o => new SMSSchoolViewModel
            {
                DistrictName = o.District.DistrictName,
                HeadMasterName = o.HeadMasterName,
                HeadMasterPhone = o.HeadMasterPhone,
                SchoolCode = o.SchoolCode,
                SchoolID = o.SchoolProfileID,
                SchoolName = o.SchoolName,
                TrainingTypeID = o.TrainingTypeID
            });

            List<SMSSchoolViewModel> lsSMSSchoolViewModel = lst.ToList();
            foreach (SMSSchoolViewModel item in lsSMSSchoolViewModel)
            {
                if (item.TrainingTypeID.HasValue)
                {
                    item.TrainingTypeName = TrainingTypeBusiness.Find(item.TrainingTypeID).Resolution;
                }
            }

            return lsSMSSchoolViewModel;
        }

        #endregion search

        #region setViewData

        public void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            //Get view data here
            if (hasSMSLeft())
            {
                SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
                IEnumerable<SMSSchoolViewModel> lst = this._Search(SearchInfo);
                ViewData[SMSDepartmentConstants.LIST_SCHOOL] = lst;
            }

            // Quan huyen
            SearchInfo = new Dictionary<string, object>();
            List<District> lstDistrict = new List<District>();
            if (global.IsSuperVisingDeptRole)
            {
                SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
                lstDistrict = DistrictBusiness.Search(SearchInfo).ToList();
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                District district = SupervisingDeptBusiness.Find(global.SupervisingDeptID).District;
                lstDistrict.Add(district);
            }
            ViewData[SMSDepartmentConstants.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName");

            // Loai hinh dao tao
            SearchInfo = new Dictionary<string, object>();
            IEnumerable<TrainingType> lstTrainingType = TrainingTypeBusiness.Search(SearchInfo);
            ViewData[SMSDepartmentConstants.LIST_TRAINING_TYPE] = new SelectList(lstTrainingType, "TrainingTypeID", "Resolution");

            // Cap hoc
            ViewData[SMSDepartmentConstants.LS_APPLIEDLEVEL] = new SelectList(CommonList.AppliedLevel(), "key", "value");
        }

        private bool hasSMSLeft()
        {
            GlobalInfo global = new GlobalInfo();

            // Gọi WS để lấy thông tin hợp đồng SMS Teacher
            SMSTeacherContractBO contract = new SMSTeacherContractBO();
            contract.SupervisingDeptCode = SupervisingDeptBusiness.Find(global.SupervisingDeptID.Value).SupervisingDeptCode;
            contract.Status = -1;
            contract.MaxSMS = -1;
            List<SMSTeacherContractBO> lstContract = SMSTeacherContractBusiness.GetSMSTeacherContract(contract);

            // Nếu chưa đăng ký SMS Teacher
            if (lstContract == null || lstContract.Count() == 0)
            {
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = Res.Get("SMSTeacher_Warning_Unregistered");
                ViewData[SMSDepartmentConstants.SEND_INFO] = "";
                return false;
            }

            // Nếu có đăng ký SMS Teacher nhưng đang bị chặn cắt
            if (lstContract[0].Status == SystemParamsInFile.SMS_TEACHER_BLOCK_STATUS)
            {
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = Res.Get("SMSTeacher_Warning_Closed");
                ViewData[SMSDepartmentConstants.SEND_INFO] = "";
                return false;
            }

            // Nếu đang hoạt động bình thường
            if (lstContract[0].Status == SystemParamsInFile.SMS_TEACHER_ACTIVE_STATUS)
            {
                Session["PackageID"] = lstContract[0].PackageID;
                ViewData[SMSDepartmentConstants.WARNING_MESSAGE] = "";

                // Gọi WS để lấy số SMS đã gửi
                List<CallDetailRecordBO> lstCDR = CallDetailRecordBusiness.GetCDROfSMSTeacherContract(contract);
                int numSentInMonth = lstCDR.Where(o => o.CallDetailRecord.StaDatetime.ToString("MM/yyyy").Equals(DateTime.Now.ToString("MM/yyyy"))).Count();
                int numSentViettelInMonth = lstCDR.Where(o => o.CallDetailRecord.StaDatetime.ToString("MM/yyyy").Equals(DateTime.Now.ToString("MM/yyyy"))
                                                              && o.CallDetailRecord.Supplier == (int)SystemParamsInFile.NetworkProviders.VIETTEL).Count();
                ViewData[SMSDepartmentConstants.SEND_INFO] = String.Format(Res.Get("SMSTeacher_Warning_SendInfo"),
                                DateTime.Now.ToString("MM/yyyy"), numSentInMonth, numSentViettelInMonth, numSentInMonth - numSentViettelInMonth);
                return true;
            }

            return false;
        }

        #endregion setViewData

        #region send SMS
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendSMS(int[] checkedToSend, String message)
        {
            if (message.Trim().Length == 0)
                return Json(new JsonMessage(Res.Get("Message_Label_ErrorContentEmpty")));

            GlobalInfo global = new GlobalInfo();

            List<SendInfo> lstSendInfo = new List<SendInfo>();
            foreach (var item in checkedToSend)
            {
                // Tao doi tuong SendInfo
                SchoolProfile school = SchoolProfileBusiness.Find(item);
                SendInfo s = new SendInfo();
                s.SchoolID = s.SchoolID;
                //s.SupervisingDeptCode = school.SupervisingDept.SupervisingDeptCode;
                s.InfoType = SystemParamsInFile.INFO_TYPE_SCHOOL_UNEXPECTED;
                s.SchedularType = SystemParamsInFile.SCHEDULE_UNEXPECTED;
                s.TypeOfReceiver = SystemParamsInFile.RECEIVE_TYPE_SCHOOL;
                s.PackageID = Convert.ToInt16(Session["PackageID"]);
                s.Title = Res.Get("SMSSchool_Title_Message");
                s.Content = SupervisingDeptBusiness.Find(global.SupervisingDeptID).SupervisingDeptName + " " + message;
                s.SendType = SystemParamsInFile.SEND_TYPE_MOBILE;
                UserAccount UserAccount = this.UserAccountBusiness.Find(global.UserAccountID);
                s.CreateUser = UserAccount.aspnet_Users.UserName;
                s.ReceiverMobile = school.HeadMasterPhone;
                s.ReceiverName = school.HeadMasterName;
                lstSendInfo.Add(s);
            }

            SendInfoBusiness.InsertSendInfo(lstSendInfo);
            SendInfoBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion send SMS
    }
}





