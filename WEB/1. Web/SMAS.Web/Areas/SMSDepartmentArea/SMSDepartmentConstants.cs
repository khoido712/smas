/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.SMSDepartmentArea
{
    public class SMSDepartmentConstants
    {
        public const string LIST_EMPLOYEE = "listEmployee";
        public const string LIST_SCHOOL = "listSchool";

        public const string LIST_DISTRICT = "listDistrict";
        public const string LIST_TRAINING_TYPE = "listTrainingType";
        public const string LIST_EDUCATION_GRADE = "listEducationGrade";

        public const string LISTSCHOOLFACULTY = "LISTSCHOOLFACULTY";
        public const string LISTWORKTYPE = "LISTWORKTYPE";
        public const string LISTEMPLOYMENTSTATUS = "LISTEMPLOYMENTSTATUS";
        public const string LISTSEX = "LISTSEX";
        public const string LISTGRADUATIONLEVEL = "LISTGRADUATIONLEVEL";
        public const string LISTCONTRACTTYPE = "LISTCONTRACTTYPE";

        public const string LS_RELIGION = "LISTRELIGION";
        public const string LS_ETHNIC = "LISTETHNIC";
        public const string LS_FAMILYTYPE = "LISTFAMILYTYPE";
        public const string LS_QUALIFICATIONTYPE = "LISTQUALIFICATIONTYPE";

        public const string LS_SPECIALITYCAT = "LISTSPECIALITYCAT";
        public const string LS_QUALIFICATIONLEVEL = "LISTQUALIFICATIONLEVEL";
        public const string LS_FOREIGNLANGUAGEGRADE = "LISTFOREIGNLANGUAGEGRADE";

        public const string LS_ITQUALIFICATIONLEVEL = "LISTITQUALIFICATIONLEVEL";
        public const string LS_STATEMANAGEMENTGRADE = "LISTSTATEMANAGEMENTGRADE";

        public const string LS_POLITICALGRADE = "LISTPOLITICALGRADE";

        public const string LS_EDUCATIONALMANAGEMENTGRADE = "LISTEDUCATIONALMANAGEMENTGRADE";

        public const string LS_CONTRACT = "LISTCONTRACT";

        public const string LS_STAFFPOSITION = "LISTSTAFFPOSITION";

        public const string LS_SUPERVISINGDEPT = "LISTSUPERVISINGDEPT";

        public const string LS_GROUPCAT = "LISTGROUPCAT";

        public const string LS_GRADUATIONLEVEL = "LS_GRADUATIONLEVEL";

        public const string LS_WORKGROUPTYPE = "LS_WORKGROUPTYPE";

        public const string LS_WORKTYPE = "LS_WORKTYPE";

        public const string LS_PRIMARILYASSIGNEDSUBJECT = "LS_PRIMARILYASSIGNEDSUBJECT";

        public const int EMPLOYMENT_STATUS_WORKING = 1;
        public const int EMPLOYMENT_STATUS_WORK_CHANGED = 2;
        public const int EMPLOYMENT_STATUS_RETIRED = 3;

        public const string CHOSENEMPLOYEE = "CHOSENEMPLOYEE";

        public const string LS_APPLIEDLEVEL = "LS_APPLIEDLEVEL";

        public const string AUTO_GEN_CODE_CHECK = "AUTO_GEN_CODE_CHECK";
        public const string AUTO_GEN_CODE_CHECK_AND_NO_DISABLE = "AUTO_GEN_CODE_CHECK_AND_NO_DISABLE";
        public const string AUTO_GEN_CODE_CHECK_AND_DISABLE = "AUTO_GEN_CODE_CHECK_AND_DISABLE";
        public const string IS_SCHOOL_MODIFIED = "IS_SCHOOL_MODIFIED";
        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";

        public const string WARNING_MESSAGE = "WARNING_MESSAGE";
        public const string SEND_INFO = "SEND_INFO";
    }
}
