/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SMSDepartmentArea.Models
{
    public class SchoolSearchViewModel
    {
        [ResourceDisplayName("SchoolProfile_Label_SchoolName")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolCode")]
        public string SchoolCode { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_HeadMasterName")]
        public string HeadMasterName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LIST_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        public Nullable<int> District { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_TrainingType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LIST_TRAINING_TYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? TrainingType { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_EducationGrade")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LS_APPLIEDLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        public Nullable<int> EducationGrade { get; set; }
    }
}
