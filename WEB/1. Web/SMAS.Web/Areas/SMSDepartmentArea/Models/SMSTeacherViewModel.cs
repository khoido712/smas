/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.SMSDepartmentArea.Models
{
    public class SMSTeacherViewModel
    {
        public int EmployeeID { get; set; }
        public int? EmployeeStatus { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> ContractID { get; set; }
        public Nullable<int> WorkTypeID { get; set; }
        public bool IsActive { get; set; }
        public int AppliedLevel { get; set; }
        public int? ContractTypeID { get; set; }

        [ResourceDisplayName("Employee_Label_Mobile")]
        public string Mobile { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        public bool Genre { get; set; }

        [ResourceDisplayName("Employee_Label_ContractType")]
        public string ContractTypeName { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        public Nullable<System.DateTime> BirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        public string Sex { get; set; }

        [ResourceDisplayName("Employee_Label_WorkType")]
        public string WorkTypeName { get; set; }

        [ResourceDisplayName("Employee_Label_EmploymentStatus")]
        public string EmployeeStatusName { get; set; }
    }								
}


