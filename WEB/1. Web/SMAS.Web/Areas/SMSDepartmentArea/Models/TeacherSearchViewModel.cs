/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SMSDepartmentArea.Models
{
    public class TeacherSearchViewModel
    {
        [ResourceDisplayName("Employee_Label_FullName")]
        [UIHint("Textbox")]
        public string Fullname { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LISTSEX)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Sex { get; set; }

        [ResourceDisplayName("Employee_Label_EmploymentStatus")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LISTEMPLOYMENTSTATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? EmploymentStatus { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LIST_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "onDistrictChange(this)")]
        public int? District { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_TrainingType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LIST_TRAINING_TYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "onTrainingTypeChange(this)")]
        public int? TrainingType { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_EducationGrade")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LS_APPLIEDLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "onAppliedLevelChange(this)")]
        public int? EducationGrade { get; set; }

        [ResourceDisplayName("ClassProfile_Control_School")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LIST_SCHOOL)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? School { get; set; }
    }
}