/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.SMSDepartmentArea.Models
{
    public class SMSDeptViewModel
    {
        public int EmployeeID { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        public Nullable<System.DateTime> BirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_PermanentResidentalAddress")]
        public string PermanentResidentalAddress { get; set; }

        [ResourceDisplayName("Employee_Label_Mobile")]
        public string Mobile { get; set; }
    }								
	       
}


