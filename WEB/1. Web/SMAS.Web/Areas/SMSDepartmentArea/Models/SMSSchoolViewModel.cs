/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.SMSDepartmentArea.Models
{
    public class SMSSchoolViewModel
    {
        public int SchoolID { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_DistrictName")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolName")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolCode")]
        public string SchoolCode { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_EducationGrade")]
        public Nullable<int> EducationGrade { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_HeadMasterName")]
        public string HeadMasterName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_HeadMasterPhone")]
        public string HeadMasterPhone { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_TrainingType")]
        public string TrainingTypeName { get; set; }

        public int? TrainingTypeID { get; set; }
    }								
	       
}


