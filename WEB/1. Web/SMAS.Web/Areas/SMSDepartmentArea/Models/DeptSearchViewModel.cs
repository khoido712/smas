/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SMSDepartmentArea.Models
{
    public class DeptSearchViewModel
    {
        [ResourceDisplayName("Employee_Label_FullName")]
        [UIHint("Textbox")]
        public string Fullname { get; set; }

        [ResourceDisplayName("Employee_Label_SupervisingDept")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SMSDepartmentConstants.LS_SUPERVISINGDEPT)]
        [AdditionalMetadata("Placeholder", "null")]
        public string SupervisingDept { get; set; }        
    }
}