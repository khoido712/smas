﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ITQualificationLevelArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.ITQualificationLevelArea.Controllers
{
    public class ITQualificationLevelController : BaseController
    {
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;

        public ITQualificationLevelController(IITQualificationLevelBusiness itqualificationlevelBusiness)
        {
            this.ITQualificationLevelBusiness = itqualificationlevelBusiness;
        }

        //
        // GET: /ITQualificationLevel/

        public ActionResult Index()
        {
            SetViewDataPermission("ITQualificationLevel", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here

            IEnumerable<ITQualificationLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[ITQualificationLevelConstants.LIST_ITQUALIFICATIONLEVEL] = lst;
            return View();
        }

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["Resolution"] = frm.Resolution;
            SearchInfo["IsActive"] = true;

            IEnumerable<ITQualificationLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[ITQualificationLevelConstants.LIST_ITQUALIFICATIONLEVEL] = lst;

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ITQualificationLevel", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ITQualificationLevel itqualificationlevel = new ITQualificationLevel();
            itqualificationlevel.IsActive = true;
            TryUpdateModel(itqualificationlevel);
            Utils.Utils.TrimObject(itqualificationlevel);

            this.ITQualificationLevelBusiness.Insert(itqualificationlevel);
            this.ITQualificationLevelBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ITQualificationLevelID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ITQualificationLevel", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CheckPermissionForAction(ITQualificationLevelID, "ITQualificationLevel");
            ITQualificationLevel itqualificationlevel = this.ITQualificationLevelBusiness.Find(ITQualificationLevelID);
            TryUpdateModel(itqualificationlevel);
            Utils.Utils.TrimObject(itqualificationlevel);
            this.ITQualificationLevelBusiness.Update(itqualificationlevel);
            this.ITQualificationLevelBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ITQualificationLevel", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CheckPermissionForAction(id, "ITQualificationLevel");
            ITQualificationLevelBusiness.Delete(id);
            this.ITQualificationLevelBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ITQualificationLevelViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("ITQualificationLevel", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<ITQualificationLevel> query = this.ITQualificationLevelBusiness.Search(SearchInfo);
            IQueryable<ITQualificationLevelViewModel> lst = query.Select(o => new ITQualificationLevelViewModel
            {
                ITQualificationLevelID = o.ITQualificationLevelID,
                Resolution = o.Resolution,
                Description = o.Description,
                EncouragePoint = o.EncouragePoint
            });

            return lst.ToList();
        }
    }
}





