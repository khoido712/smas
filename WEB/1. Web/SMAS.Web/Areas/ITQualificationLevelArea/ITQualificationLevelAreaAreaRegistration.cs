﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ITQualificationLevelArea
{
    public class ITQualificationLevelAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ITQualificationLevelArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ITQualificationLevelArea_default",
                "ITQualificationLevelArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
