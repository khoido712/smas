﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SummaryArea
{
    public class SummaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SummaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SummaryArea_default",
                "SummaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
