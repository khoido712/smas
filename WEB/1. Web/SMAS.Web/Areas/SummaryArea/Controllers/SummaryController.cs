﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.SummaryArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class SummaryController : BaseController
    {
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public SummaryController(IConductLevelBusiness conductLevelBusiness,
                                IPeriodDeclarationBusiness PeriodDeclarationBusiness,
                                IClassProfileBusiness ClassProfileBusiness,
                                IAcademicYearBusiness AcademicYearBusiness)
        {
            
            this.ConductLevelBusiness = conductLevelBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        //
        // GET: /SummaryArea/Summary/

        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            SetViewData(ClassID);
            return PartialView();
        }
        
        
        private void SetViewData(int? ClassID)
        {

            bool semester1 = false;
            bool semester2 = false;
            bool semester3 = false;
            if (_globalInfo.Semester.Value == 1)
            {
                semester1 = true;
            }
            else if (_globalInfo.Semester.Value == 2)
            {
                semester2 = true;
            }
            else
            {
                semester3 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("ConductRanking_Label_SemesterFirst"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("ConductRanking_Label_SemesterSecond"), 2, semester2, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("ConductRanking_Label_SemesterAllOfYear"), 3, semester3, false));
            ViewData[SummaryConstants.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in _globalInfo.EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[SummaryConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["Semester"] = _globalInfo.Semester.Value;
            IEnumerable<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
            List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
            if (lstPeriod != null && lstPeriod.Count() > 0)
            {
                foreach (var item in lstPeriod)
                {
                    PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                    periodDeclaration = item;
                    if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";

                    listPeriodDeclaration.Add(periodDeclaration);
                }
            }
            int period = 0;
            if (listPeriodDeclaration != null && listPeriodDeclaration.Count() != 0)
            {
                period = listPeriodDeclaration.FirstOrDefault().PeriodDeclarationID;
            }
            foreach (PeriodDeclaration pd in listPeriodDeclaration)
            {
                if (DateTime.Now >= pd.FromDate && DateTime.Now <= pd.EndDate)
                {
                    period = pd.PeriodDeclarationID;
                }
            }
            ViewData[SummaryConstants.LIST_PERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", period);
            ViewData[SummaryConstants.LIST_APPLIEDLEVEL] = _globalInfo.AppliedLevel;

            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                // Hiển thị danh sách menu bên trái
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                Dictionary<string, object> dictionary = new Dictionary<string, object> {
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"AppliedLevel", _globalInfo.AppliedLevel},
                {"IsVNEN",true}
            };
                if (!_globalInfo.IsViewAll && !_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                {
                    dictionary["UserAccountID"] = _globalInfo.UserAccountID;
                    dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                List<ClassProfile> listCP = ClassProfileBusiness.SearchBySchool(
                    _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[SummaryConstants.LIST_CLASS] = listCP;

                ClassProfile cp = null;
                if (listCP != null && listCP.Count > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        if (!_globalInfo.IsAdminSchoolRole)
                        {
                            dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                            List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(
                                _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                                .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listHead != null && listHead.Count > 0)
                            {
                                cp = listHead.First();
                            }
                            else
                            {
                                dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                                List<ClassProfile> listOverSee = ClassProfileBusiness.SearchBySchool(
                                    _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listOverSee != null && listOverSee.Count > 0)
                                {
                                    cp = listOverSee.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            ViewData[SummaryConstants.LIST_SUBJECT] = null;

            ViewData[SummaryConstants.ShowList] = false;

            ViewData[SummaryConstants.HAS_ERROR_DATA] = false;
            ViewData[SummaryConstants.LIST_IMPORTDATA] = null;

            ViewData[SummaryConstants.CLASS_12] = false;
            ViewData[SummaryConstants.SEMESTER_1] = true;
            ViewData[SummaryConstants.ERROR_BASIC_DATA] = false;
            ViewData[SummaryConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[SummaryConstants.ERROR_IMPORT] = true;
            ViewData[SummaryConstants.ENABLE_ENTRYMARK] = "true";
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboPeriod(int? SemesterID)
        {
            if (SemesterID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["Semester"] = SemesterID.Value;
                IEnumerable<PeriodDeclaration> listPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
                List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
                if (listPeriod != null && listPeriod.Count() > 0)
                {
                    foreach (var item in listPeriod)
                    {
                        PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                        periodDeclaration = item;
                        if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                            periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                        listPeriodDeclaration.Add(periodDeclaration);
                    }
                }
                int period = 0;
                foreach (PeriodDeclaration pd in listPeriodDeclaration)
                {
                    if (DateTime.Now >= pd.FromDate && DateTime.Now <= pd.EndDate)
                    {
                        period = pd.PeriodDeclarationID;
                    }
                }
                ViewData[SummaryConstants.LIST_SEMESTER] = SemesterID.Value;
                ViewData[SummaryConstants.ShowList] = false;
                return Json(new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", period));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }
    }
}
