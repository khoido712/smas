﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DeclareEvaluationIndexArea
{
    public class DeclareEvaluationIndexAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DeclareEvaluationIndexArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DeclareEvaluationIndexArea_default",
                "DeclareEvaluationIndexArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
