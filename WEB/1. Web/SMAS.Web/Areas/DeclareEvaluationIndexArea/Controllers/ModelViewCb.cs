﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Web.Areas.DeclareEvaluationIndexArea.Controllers
{
    class ModelViewCb
    {
        public int EvaluationDevelopmentGroupID { get; set; }

        public string EvaluationDevelopmentGroupName { get; set; }
    }
}
