﻿using Newtonsoft.Json;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.DeclareEvaluationIndexArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SMAS.Web.Areas.DeclareEvaluationIndexArea.Controllers
{
    public class DeclareEvaluationIndexController : BaseController
    {
        #region properties
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IGrowthEvaluationBusiness GrowthEvaluationBusiness; 


        private List<EducationLevel> lstEducationLevel;
        private List<EvaluationDevelopmentGroup> lstEDG = null;
        private bool IsHidden = false;
        private bool IsNotChecked = false;
        private bool selectedAll = true;
        #endregion

        #region Constructor
        public DeclareEvaluationIndexController(
            IClassSubjectBusiness ClassSubjectBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness,
            IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness,
            IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IGrowthEvaluationBusiness GrowthEvaluationBusiness
            )
        {
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.DeclareEvaluationGroupBusiness = DeclareEvaluationGroupBusiness;
            this.EvaluationDevelopmentGroupBusiness = EvaluationDevelopmentGroupBusiness;
            this.EvaluationDevelopmentBusiness = EvaluationDevelopmentBusiness;
            this.DeclareEvaluationIndexBusiness = DeclareEvaluationIndexBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.GrowthEvaluationBusiness = GrowthEvaluationBusiness;
        }
        #endregion

        #region list EvaluationGroup
        [ValidateAntiForgeryToken]
        public JsonResult LoadEvaluationGroup(int eduId)
        {
            //Lay danh sach linh vuc
            if (eduId <= 0)
                return Json(new List<SelectListItem>());
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = eduId;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            lstEDG = new List<EvaluationDevelopmentGroup>();
            lstEDG = DeclareEvaluationIndexBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic)
                .OrderBy(p => p.OrderID.HasValue ? p.OrderID : 0)/*.ThenBy(o => o.DisplayName)*/.ToList();
            lstEDG = (from oik in lstEDG
                      join b in DeclareEvaluationGroupBusiness.All on oik.EvaluationDevelopmentGroupID equals b.EvaluationGroupID
                      where b.AcademicYearID == _globalInfo.AcademicYearID && b.SchoolID == _globalInfo.SchoolID && b.EducationLevelID == eduId
                      select oik).ToList();
            List<SelectListItem> lstClassJson = null;
            lstClassJson = lstEDG.Select(u => new SelectListItem { Value = u.EvaluationDevelopmentGroupID.ToString(), Text = u.EvaluationDevelopmentGroupName, Selected = false }).ToList();
            return Json(lstClassJson);
        }
        #endregion

        public ActionResult Index()
        {
            SetViewData();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            //Lay danh sach mon hoc cua lop
            int? educationLevelID = null;
            if (lstEducationLevel.Count > 0)
            {
                ViewData["Evaluation"] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");
                educationLevelID = lstEducationLevel.First().EducationLevelID;
                dic["EducationLevelID"] = educationLevelID;
                // lay du lieu cho cbox evaluationGroupk
                ViewBag.ClassID = new SelectList(DeclareEvaluationIndexBusiness.SearchForCb2(Int32.Parse(educationLevelID.ToString()), dic).ToList(),
                    "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName");
            }
            // list _class_subject // viet linq trong _Search ViewData se la du lieu bang?
            ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = new List<ListViewModel>();
            if (educationLevelID != null)
            {
                var lstResult = _Search(educationLevelID.Value, null);
                ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = lstResult;
                List<string> lstResultCode = lstResult.Select(x => x.EvaluationDevelomentIndexCode).ToList();
                int resultCodeMax = FindResultCode(lstResultCode);
                ViewData["maxCode"] = resultCodeMax;
            }
            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = CheckButtonPermission();
            return View();
        }

        private List<ListViewModel> _Search(int educationLevelID, string eduEvaId = null)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dic["EvaluationGroupID"] = eduEvaId;
            
            IQueryable<DeclareEvaluationIndexBO> iquery = DeclareEvaluationIndexBusiness.SearchDEI(dic);

            var lstModel = (from iq in iquery.ToList()
                           select new ListViewModel
                           {
                                EvaluationGroupID = iq.EvaluationGroupID,
                                EvaluationIndexID = iq.EvaluationIndexID,
                                DeclareEducatioGroupID = iq.DeclareEvaluationGroupID,
                                EvaluationDevelopmentGroupName = iq.EvaluationDevelopmentGroupName,
                                EvaluationDevelopmentIndexName = iq.EvaluationDevelopmentIndexName,
                                EvaluationGroupOrderID = iq.EvaluationGroupOrderID,
                                EvaluationDevelopmentOrderID = iq.EvaluationDevelopmentOrderID,
                                EvaluationGroupOfDEI = iq.EvaluationGroupOfDEI,
                                EvaluationDevelomentIndexCode = iq.EvaluationDevelomentIndexCode,
                                CreatedBySchool = iq.CreatedBySchool,
                           }).ToList();

            int countIsCheck = 0;
            foreach (var item in lstModel) 
            {
                if (item.EvaluationGroupID == item.EvaluationGroupOfDEI || item.EvaluationGroupOfDEI == null) 
                {
                    lstResult.Add(item);
                    if(item.isCheck == true)
                        countIsCheck++;
                }
            }
            ViewData["countIsCheck"] = countIsCheck;

            IDictionary<string, object> dicc = new Dictionary<string, object>();
            dicc["SchoolID"] = _globalInfo.SchoolID;
            dicc["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicc["EducationLevelID"] = educationLevelID;
            dicc["AppliedLevel"] = _globalInfo.AppliedLevel;
            lstEDG = new List<EvaluationDevelopmentGroup>();
            lstEDG = DeclareEvaluationIndexBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dicc)
                .OrderBy(p => p.OrderID.HasValue ? p.OrderID : 0)/*.ThenBy(o => o.DisplayName)*/.ToList();
            var lstEDGTemp = (from oik in lstEDG
                              join b in DeclareEvaluationGroupBusiness.All on oik.EvaluationDevelopmentGroupID equals b.EvaluationGroupID
                              where b.AcademicYearID == _globalInfo.AcademicYearID &&
                                      b.SchoolID == _globalInfo.SchoolID &&
                                      b.EducationLevelID == educationLevelID
                              select new { IsHidden = oik.IsLocked, IsChecked = b.IsLocked }).FirstOrDefault();
            IsHidden = lstEDGTemp == null ? true : (lstEDGTemp.IsHidden == null ? true : lstEDGTemp.IsHidden.Value);
            IsNotChecked = lstEDGTemp == null ? true : (lstEDGTemp.IsChecked == null ? true : lstEDGTemp.IsChecked.Value);
            ViewData["chkEmployeeRole"] = IsHidden;
            ViewData["IsNotChecked"] = IsNotChecked;
            return lstResult.OrderByDescending(x=>x.isCheck).ToList();
        }

        public PartialViewResult SearchforCB1(string EducationLevelID)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            if (EducationLevelID != null)
            {
                lstResult = _Search(Int32.Parse(EducationLevelID));
                ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = lstResult;
            }
            ViewData["chkEmployeeRole"] = IsHidden;
            ViewData["IsNotChecked"] = IsNotChecked;
            ViewData["selectedAll"] = selectedAll;
            List<string> lstResultCode = lstResult.Select(x => x.EvaluationDevelomentIndexCode).ToList();
            int resultCodeMax = FindResultCode(lstResultCode);
            ViewData["maxCode"] = resultCodeMax;
            ViewData["maxTT"] = lstResult.Max(x=>x.EvaluationDevelopmentOrderID);
            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = CheckButtonPermission();
            return PartialView("_List", lstResult);
        }

        public int FindResultCode(List<string> lstResultCode) 
        {
            int result = 0;
            bool TF;
            char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            
                foreach (var item in lstResultCode)
                {
                    int index = item.ToLower().LastIndexOfAny(alphabet);
                    if (index == item.Length - 1) 
                    {
                        continue;
                    }
                    string maxCode = item.Substring(index + 1).Trim();
                    if (!string.IsNullOrEmpty(maxCode) && maxCode.Length > 0)
                    {
                        int resultTemp;
                        TF = Int32.TryParse(maxCode, out resultTemp);
                        if (TF && resultTemp > result)
                            result = resultTemp;
                    }
                }
                return result;
                      
        }

        public PartialViewResult SearchforCB2(string EducationLevelID, string eduEvaId)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            if (EducationLevelID != null)
            {
                lstResult = _Search(Int32.Parse(EducationLevelID), eduEvaId);
                ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = lstResult;
            }
            if (string.IsNullOrEmpty(eduEvaId))
            {
                ViewData["selectedAll"] = true;
            }
            else 
            {
                ViewData["selectedAll"] = false;
            }
            List<string> lstResultCode = lstResult.Select(x => x.EvaluationDevelomentIndexCode).ToList();
            int resultCodeMax = FindResultCode(lstResultCode);
            ViewData["chkEmployeeRole"] = IsHidden;
            ViewData["IsNotChecked"] = IsNotChecked;
            ViewData["maxCode"] = resultCodeMax;
            ViewData["maxTT"] = lstResult.Max(x => x.EvaluationDevelopmentOrderID);
            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = CheckButtonPermission();
            return PartialView("_List", lstResult);
        }

        [HttpPost]
        public JsonResult Save(string arr, string educationID, string evaluationGroupID)
        {
            List<EvaluationIndex> lstData = JsonConvert.DeserializeObject<List<EvaluationIndex>>(arr).OrderBy(x=>x.EvaluationGroupID).ToList();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EvaluationGroupID"] = evaluationGroupID;
            int EducationID = Int32.Parse(educationID);

            if (evaluationGroupID != "")
            {
                int EvaluationGroupID = Int32.Parse(evaluationGroupID);
                List<DeclareEvaluationGroup> compare = DeclareEvaluationGroupBusiness.
                                                        All.
                                                        Where(x => x.AcademicYearID == _globalInfo.AcademicYearID &&
                                                                x.SchoolID == _globalInfo.SchoolID &&
                                                                x.EducationLevelID == EducationID &&
                                                                x.EvaluationGroupID == EvaluationGroupID).ToList();
                if (compare.Count() == 0)
                    return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }

            List<EvaluationDevelopment> lstCheck = (from a in EvaluationDevelopmentBusiness.All
                                            join b in EvaluationDevelopmentGroupBusiness.All on a.EvaluationDevelopmentGroupID equals b.EvaluationDevelopmentGroupID
                                            where a.EducationLevelID == EducationID && a.IsActive == true &&
                                                  b.AppliedLevel == _globalInfo.AppliedLevel && b.IsActive == true &&
                                                  b.EducationLevelID == EducationID
                                           select a).ToList();

            List<EvaluationDevelopmentGroup> lstCheckDB = (from b in EvaluationDevelopmentGroupBusiness.All 
                                                            where   b.AppliedLevel == _globalInfo.AppliedLevel && b.IsActive == true &&
                                                                  b.EducationLevelID == EducationID
                                                            select b).ToList();
            #region Insert
            // Danh sách lưu vào EvaluationDevelopment và DeclareEvaluationIndex
            // Danh sách có trong DB
            List<EvaluationDevelopment> lstEDITemp = new List<EvaluationDevelopment>();
            EvaluationDevelopment objEDI = null;
            EvaluationDevelopmentGroup objEDG = null;
            int countmax = 1;
            int check = 0;
            string indexErrorName = "";
            foreach (var item in lstData)
            {            
                objEDI = new EvaluationDevelopment();
                if (check == 0)
                {
                    check = item.EvaluationGroupID;
                }
                else
                {
                    if (check != item.EvaluationGroupID)
                    {
                        check = item.EvaluationGroupID;
                        countmax = 1;
                    }
                }   
                
                if (item.EvaluationIndexID == -1 && lstCheck.Where(x => x.EvaluationDevelopmentGroupID == item.EvaluationGroupID 
                                    && x.EvaluationDevelopmentCode == item.EvaluationIndexCode).Count() > 0)
                {
                    return Json(new JsonMessage(Res.Get("Mã không được phép trùng"), "error"));
                }

                if (item.EvaluationIndexID != -1 && lstCheck.Where(x => x.EvaluationDevelopmentGroupID == item.EvaluationGroupID
                                    && x.EvaluationDevelopmentCode == item.EvaluationIndexCode && x.EvaluationDevelopmentID != item.EvaluationIndexID).Count() > 0)
                {
                    return Json(new JsonMessage(Res.Get("Mã không được phép trùng"), "error"));
                }    

                objEDG = lstCheckDB.FirstOrDefault(x => x.EvaluationDevelopmentGroupID == item.EvaluationGroupID);
                if (objEDG != null)
                {
                    objEDI = new EvaluationDevelopment();
                    objEDI.EvaluationDevelopmentID = item.EvaluationIndexID;
                    objEDI.EvaluationDevelopmentCode = item.EvaluationIndexCode;
                    objEDI.EvaluationDevelopmentName = item.EvaluationIndexDes;
                    objEDI.Description = item.EvaluationIndexDes;
                    objEDI.IsRequired = true;
                    objEDI.InputType = 1;
                    objEDI.EducationLevelID = EducationID;
                    objEDI.EvaluationDevelopmentGroupID = item.EvaluationGroupID;
                    objEDI.CreatedDate = DateTime.Now;
                    objEDI.IsActive = true;
                    objEDI.OrderID = item.TT == null ? (lstCheck.Where(x => x.EvaluationDevelopmentGroupID == item.EvaluationGroupID).Max(x => x.OrderID) == null ? 
                        countmax : lstCheck.Where(x => x.EvaluationDevelopmentGroupID == item.EvaluationGroupID).Max(x => x.OrderID) + countmax) : item.TT;
                    objEDI.ProvinceID = _globalInfo.ProvinceID;
                    objEDI.CreatedBySchool = true;
                    lstEDITemp.Add(objEDI);
                    countmax++;
                }            
            }

            if (indexErrorName.Length > 0) 
            {
                return Json(new JsonMessage("Chỉ số " + indexErrorName.Substring(0, indexErrorName.Length - 2) + " đã được sử dụng.", "error"));
            }

            // Danh sách lưu vào DeclareEvaluationIndex
            List<EvaluationIndex> lstDEI = lstData.Where(x => x.EvaluationIndexID != -1).ToList();
            // Danh sách có trong DB
            List<DeclareEvaluationIndex> lstDEITemp = new List<DeclareEvaluationIndex>();
            DeclareEvaluationIndex objDEI = null;
            foreach (var item in lstDEI)
            {
                objEDI = lstCheck.FirstOrDefault(x => x.EvaluationDevelopmentGroupID == item.EvaluationGroupID &&
                                                        x.EvaluationDevelopmentID == item.EvaluationIndexID);
                if (objEDI != null)
                {
                    objDEI = new DeclareEvaluationIndex();
                    objDEI.SchoolID = _globalInfo.SchoolID.Value;
                    objDEI.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objDEI.EducationLevelID = EducationID;
                    objDEI.EvaluationDevelopmentGroID = item.EvaluationGroupID;
                    objDEI.EvaluationDevelopmentID = item.EvaluationIndexID;
                    objDEI.CreateDate = DateTime.Now;
                    lstDEITemp.Add(objDEI);
                }
            }

            Dictionary<string, object> dicIN = new Dictionary<string, object>();
            dicIN["SchoolID"] = _globalInfo.SchoolID;
            dicIN["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicIN["EducationLevelID"] = EducationID;
            dicIN["EvaluationGroupID"] = evaluationGroupID;

            EvaluationDevelopmentBusiness.InsertBySchool(lstEDITemp, lstDEITemp, dicIN);
           
            #endregion

            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }

        [HttpPost]
        public JsonResult Delete(string arr, string educationID, string evaluationGroupID)
        {
            List<EvaluationIndex> lstData = JsonConvert.DeserializeObject<List<EvaluationIndex>>(arr);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EvaluationGroupID"] = evaluationGroupID;
            int EducationID = Int32.Parse(educationID);

            if (evaluationGroupID != "")
            {
                int EvaluationGroupID = Int32.Parse(evaluationGroupID);
                List<DeclareEvaluationGroup> compare = DeclareEvaluationGroupBusiness.
                                                        All.
                                                        Where(x => x.AcademicYearID == _globalInfo.AcademicYearID &&
                                                                x.SchoolID == _globalInfo.SchoolID &&
                                                                x.EducationLevelID == EducationID &&
                                                                x.EvaluationGroupID == EvaluationGroupID).ToList();
                if (compare.Count() == 0)
                    return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }

            List<EvaluationDevelopment> lstCheck = (from a in EvaluationDevelopmentBusiness.All
                                                    join b in EvaluationDevelopmentGroupBusiness.All on a.EvaluationDevelopmentGroupID equals b.EvaluationDevelopmentGroupID
                                                    where a.EducationLevelID == EducationID && a.IsActive == true &&
                                                          b.AppliedLevel == _globalInfo.AppliedLevel && b.IsActive == true &&
                                                          b.EducationLevelID == EducationID
                                                    select a).ToList();

            List<GrowthEvaluation> lstGE = (from ge in GrowthEvaluationBusiness.All
                                                            .Where(x => x.SchoolID == _globalInfo.SchoolID.Value && x.AcademicYearID == _globalInfo.AcademicYearID.Value)
                                            join cp in ClassProfileBusiness.All
                                                            .Where(x => x.IsActive == true && x.SchoolID == _globalInfo.SchoolID && x.AcademicYearID == _globalInfo.AcademicYearID && x.EducationLevel.Grade == _globalInfo.AppliedLevel.Value)
                                                    on ge.ClassID equals cp.ClassProfileID
                                            select ge).ToList();

            #region Delete
            // Danh sách có trong DB
            List<DeclareEvaluationIndex> lstDEITemp = new List<DeclareEvaluationIndex>();
            EvaluationDevelopment objEDI = null;
            DeclareEvaluationIndex objDEI = null;
            GrowthEvaluation objGE = null;
            foreach (var item in lstData)
            {
                objGE = lstGE.FirstOrDefault(x => x.DeclareEvaluationGroupID == item.EvaluationGroupID && x.DeclareEvaluationIndexID == item.EvaluationIndexID);
                if (objGE != null) 
                {
                    return Json(new JsonMessage("Chỉ số '" + item.EvaluationIndexCode + "' đã được sử dụng.", "error"));
                }


                objEDI = lstCheck.FirstOrDefault(x => x.EvaluationDevelopmentGroupID == item.EvaluationGroupID &&
                                                        x.EvaluationDevelopmentID == item.EvaluationIndexID);
                if (objEDI != null)
                {
                    objDEI = new DeclareEvaluationIndex();
                    objDEI.SchoolID = _globalInfo.SchoolID.Value;
                    objDEI.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objDEI.EducationLevelID = EducationID;
                    objDEI.EvaluationDevelopmentGroID = item.EvaluationGroupID;
                    objDEI.EvaluationDevelopmentID = item.EvaluationIndexID;
                    objDEI.CreateDate = DateTime.Now;
                    lstDEITemp.Add(objDEI);
                }
            }

            Dictionary<string, object> dicIN = new Dictionary<string, object>();
            dicIN["SchoolID"] = _globalInfo.SchoolID;
            dicIN["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicIN["EducationLevelID"] = EducationID;
            dicIN["EvaluationGroupID"] = evaluationGroupID;

            EvaluationDevelopmentBusiness.DeleteBySchool(lstDEITemp, dicIN);

            #endregion

            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }

        public FileResult ExportExcel(string educationLevelID, string evaluationGroupID) 
        {
            string fileName = "";
            Stream excel = null;
            Dictionary<string, object> dic = new Dictionary<string,object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dic["EvaluationGroupID"] = evaluationGroupID;
            excel = ReportExcel(dic , out fileName);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        public Stream ReportExcel(Dictionary<string, object> dic, out string fileName) 
        {
            string reportCode = SystemParamsInFile.REPORT_DECLARE_EVALUATION_INDEX;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName + ".xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            string outputName = reportDef.OutputNamePattern;
            fileName = ReportUtils.RemoveSpecialCharacters(outputName) + "." + reportDef.OutputFormat;
            IVTWorksheet sheet = oBook.GetSheet(1);

            IQueryable<DeclareEvaluationIndexBO> iqueryDEI = DeclareEvaluationIndexBusiness.SearchDEI(dic);
            int educationLevelID = Business.Common.Utils.GetInt(dic, "EducationLevelID");
            int evaluationGroupID = Business.Common.Utils.GetInt(dic, "EvaluationGroupID");
            List<DeclareEvaluationIndexBO> lstDEI = iqueryDEI.ToList();
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation = new VTDataValidation();
            objValidation = new VTDataValidation
            {
                Contrains = lstDEI.Select(x => x.EvaluationDevelopmentGroupName).Distinct().ToArray(),
                FromColumn = 2,
                FromRow = 8,
                SheetIndex = 1,
                ToColumn = 2,
                ToRow = 28,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);           
            int startRow = 8;
            int stt = 1;
            lstDEI = lstDEI.Where(x => x.CreatedBySchool == true).ToList();
            foreach(var item in lstDEI)
            {
                sheet.SetCellValue(startRow, 1, stt);
                sheet.SetCellValue(startRow, 2, item.EvaluationDevelopmentGroupName);
                sheet.SetCellValue(startRow, 3, item.EvaluationDevelomentIndexCode);
                sheet.SetCellValue(startRow, 4, item.EvaluationDevelopmentIndexName);               
                stt++;
                startRow++;
            }
            sheet.GetRange(8, 1, startRow - 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(8, 4, startRow - 1, 4).WrapText();
            sheet.GetRange(8, 1, startRow - 1, 1).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(8, 3, startRow - 1, 3).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(8, 2, startRow - 1, 2).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(8, 2, startRow - 1, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.GetRange(8, 1, startRow - 1, 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.GetRange(8, 3, startRow - 1, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

            var objEducation = EducationLevelBusiness.Find(educationLevelID);
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            sheet.SetCellValue("A2", school != null ? school.SupervisingDept.SupervisingDeptName.Trim().ToUpper() : "");
            sheet.SetCellValue("A5", ("NHÓM LỚP " + (objEducation != null ? objEducation.Resolution.Trim() : "")));

            sheet.SetFontName("Times New Roman", 0);
            return oBook.ToStreamValidationData(lstValidation);
        }

        private List<int> GetListIDFromString(string str)
        {
            string[] idArr;
            if (str != "")
            {
                str = str.Remove(str.Length - 1);
                idArr = str.Split(',');
            }
            else
            {
                idArr = new string[] { };
            }
            List<int> lstID = idArr.Length > 0 ? idArr.ToList().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return lstID;
        }

        private void SetViewData()
        {
            lstEducationLevel = _globalInfo.EducationLevels;
            int? defaultEducationLevelID = null;
            if (lstEducationLevel.Count > 0)
            {   // thiet lap combobox
                defaultEducationLevelID = lstEducationLevel.First().EducationLevelID;
            }
            ViewData[VnenClassSubjectConstants.CBO_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");
            //Lay danh sach linh vuc
            if (defaultEducationLevelID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["EducationLevelID"] = defaultEducationLevelID;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                lstEDG = DeclareEvaluationIndexBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic)
                    .OrderBy(p => p.OrderID.HasValue ? p.OrderID : 0)/*.ThenBy(o => o.DisplayName)*/.ToList();
                lstEDG = (from oik in lstEDG
                          join b in DeclareEvaluationGroupBusiness.All on oik.EvaluationDevelopmentGroupID equals b.EvaluationGroupID
                          where b.AcademicYearID == _globalInfo.AcademicYearID &&
                                  b.SchoolID == _globalInfo.SchoolID &&
                                  b.EducationLevelID == defaultEducationLevelID
                          select oik).ToList();
            }
            else
            {
                lstEDG = new List<EvaluationDevelopmentGroup>();
            }
            //IsHidden = lstEDG.FirstOrDefault() == null ? true : (lstEDG.FirstOrDefault().IsLocked == null ? true : lstEDG.FirstOrDefault().IsLocked.Value);
            ViewData["chkEmployeeRole"] = IsHidden;
            ViewData["IsNotChecked"] = IsNotChecked;            
            ViewData["selectedAll"] = selectedAll;
            ViewData[VnenClassSubjectConstants.CBO_CLASS] = new SelectList(lstEDG, "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName");
        }

        private bool CheckButtonPermission()
        {
            return IsCurrentYear() &&
                GetMenupermission("VnenClassSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) >= SystemParamsInFile.PER_CREATE;
        }

        private bool IsCurrentYear()
        {
            bool isCurrentYear = false;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            if ((DateTime.Compare(nowDate, aca.FirstSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.FirstSemesterEndDate.Value) <= 0) ||
                (DateTime.Compare(nowDate, aca.SecondSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.SecondSemesterEndDate.Value) <= 0))
            {
                isCurrentYear = true;
            }
            return isCurrentYear;
        }

        public JsonResult ChangeRole(int? EducationLevelID) 
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;

                var lstDEG = DeclareEvaluationGroupBusiness.GetListBySchoolAndAcademicYear(dic).ToList();
                var status = DeclareEvaluationGroupBusiness.UpdateDEG(lstDEG);
                string message = "";
                if (status == false)
                {
                    message = "Giáo viên 'Được phép' khai báo chỉ số";
                }
                else 
                {
                    message = "Giáo viên 'Không được phép' khai báo chỉ số";
                }
                return Json(new JsonMessage(message));
            }
            catch 
            {
                return Json(new JsonMessage(Res.Get("Lưu thất bại"), "error"));
            }
            
        }

        public ActionResult GetDataBeforeImport(int educationLevelID, int evaluationGroupID) 
        {
            ViewData["EducationLevelID"] = educationLevelID;
            ViewData["EvaluationGroupID"] = evaluationGroupID;
            return PartialView("_ImportExcel");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int educationLevelID, int evaluationGroupID)
        {
            if (attachments == null || attachments.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
                res.ContentType = "text/plain";
                return res;
            }
            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;

                string Error = "";
                bool Pass = false;
                List<ImportViewModel> list = new List<ImportViewModel>();

                list = getDataFromFile(physicalPath, educationLevelID, evaluationGroupID, out Error, out Pass).OrderBy(x=>x.EvaluationDevGroupName).ToList();

                if (Error.Trim().Length == 0 && Pass)
                {
                    List<EvaluationDevelopmentBO> lstMHI = new List<EvaluationDevelopmentBO>();
                    lstMHI = ConvertObjectImport(list, educationLevelID);
                    Dictionary<string, object> dicIN = new Dictionary<string, object>();
                    dicIN["SchoolID"] = _globalInfo.SchoolID;
                    dicIN["AcademicYearID"] = _globalInfo.AcademicYearID;
                    dicIN["EducationLevelID"] = educationLevelID;
                    dicIN["ProvinceID"] = _globalInfo.ProvinceID;
                    dicIN["AppliedLevel"] = _globalInfo.AppliedLevel;

                    EvaluationDevelopmentBusiness.ImportBySchool(lstMHI, dicIN);
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                    res.ContentType = "text/plain";
                    return res;
                }
                else //if (Error.Trim().Length > 0)
                {
                    JsonResult res = Json(new JsonMessage(Error, JsonMessage.ERROR));
                    res.ContentType = "text/plain";
                    return res;
                }
                
            }
            JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_NoFileSelected"), JsonMessage.ERROR));
            res1.ContentType = "text/plain";
            return res1;
            
        }

        public List<ImportViewModel> getDataFromFile(string filename, int educationLevelID, int evaluationGroupID, out string Error, out bool Pass)
        {
            int SchoolID = _globalInfo.SchoolID.Value;
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int AppliedLevel = _globalInfo.AppliedLevel.Value;
            string FilePath = filename;

            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            List<ImportViewModel> ListImport = new List<ImportViewModel>();
            Error = "";
            Pass = false;
            string Title = "";

            #region for
            #region Kiem tra du lieu chon dau vao
            Title = (string)sheet.GetCellValue("A4");

            if (string.IsNullOrEmpty(Title) || !Title.Contains("CHỈ SỐ ĐÁNH GIÁ MẦM NON"))
            {
                Error = "File Excel không hợp lệ.";
                Pass = false;
                return (new List<ImportViewModel>());
            }

            Title = (string)sheet.GetCellValue("A5");
            string educationName = "";
            EducationLevel educationLevel = EducationLevelBusiness.Find(educationLevelID);
            if (educationLevel != null)
            {
                educationName = educationLevel.Resolution;
            }
            else 
            {
                Error = "File Excel không hợp lệ.";
                Pass = false;
                return (new List<ImportViewModel>());
            }

            if (string.IsNullOrEmpty(Title) || !Title.Contains(educationName)) 
            {
                Error = "File Excel không thuộc " + Title;
                Pass = false;
                return (new List<ImportViewModel>());
            }

            IQueryable<EvaluationDevelopmentGroup> iqueryEDG = EvaluationDevelopmentGroupBusiness.All
                                                                .Where(x=>x.IsActive == true && 
                                                                          x.AppliedLevel == _globalInfo.AppliedLevel.Value &&
                                                                          x.ProvinceID == _globalInfo.ProvinceID);
            if (educationLevelID != 0) 
            {
                iqueryEDG = iqueryEDG.Where(x => x.EducationLevelID == educationLevelID);
            }

            if (evaluationGroupID != 0) 
            {
                iqueryEDG = iqueryEDG.Where(x => x.EvaluationDevelopmentGroupID == evaluationGroupID);
            }

            List<EvaluationDevelopmentGroup> lstEDG = iqueryEDG.ToList();
            EvaluationDevelopmentGroup objDEG = new EvaluationDevelopmentGroup();
            if (evaluationGroupID != 0 && lstEDG.Where(x => x.EvaluationDevelopmentGroupID == evaluationGroupID).Count() == 0 ) 
            {
                Error = "Lĩnh vực đánh giá không tồn tại.";
                Pass = false;
                return (new List<ImportViewModel>());
            }


            #endregion

            Pass = true;
            string evaluationDevGroupName = "";
            string evaluationDevIndexCode = "";
            string evaluationDevIndexName = "";
  
            int startRow = 8;
            while (sheet.GetCellValue(startRow, 1) != null)
            {
                evaluationDevGroupName = sheet.GetCellValue(startRow, 2) == null ? "" : sheet.GetCellValue(startRow, 2).ToString();
                if (evaluationDevGroupName == "")
                {
                    Error = "Lĩnh vực đánh giá không được trống.";
                    Pass = false;
                    return (new List<ImportViewModel>());
                }
                objDEG = lstEDG.Where(x=>x.EvaluationDevelopmentGroupName == evaluationDevGroupName).FirstOrDefault();
                if (objDEG == null) 
                {
                    Error = "Lĩnh vực đánh giá " + evaluationDevGroupName + " không tồn tại.";
                    Pass = false;
                    return (new List<ImportViewModel>());
                }

                evaluationDevIndexCode = sheet.GetCellValue(startRow, 3) == null ? "" : sheet.GetCellValue(startRow, 3).ToString();
                evaluationDevIndexName = sheet.GetCellValue(startRow, 4) == null ? "" : sheet.GetCellValue(startRow, 4).ToString();
                if (evaluationDevIndexCode == "") 
                {
                    Error = "Mã chỉ số không được trống.";
                    Pass = false;
                    return (new List<ImportViewModel>());
                }
                if (evaluationDevIndexName == "")
                {
                    Error = "Chỉ số không được trống.";
                    Pass = false;
                    return (new List<ImportViewModel>());
                }
                
                ImportViewModel importViewModel = new ImportViewModel();
                importViewModel.EvaluationDevGroupName = evaluationDevGroupName;
                importViewModel.EvaluationDevIndexCode = evaluationDevIndexCode;
                importViewModel.EvaluationDevIndexName = evaluationDevIndexName;

                ListImport.Add(importViewModel);
                startRow++;
            }
            #endregion

            return ListImport;
        }

        public List<EvaluationDevelopmentBO> ConvertObjectImport(List<ImportViewModel> lstData, int educationLevelID) 
        {
            List<EvaluationDevelopment> lstCheck = (from a in EvaluationDevelopmentBusiness.All
                                                    join b in EvaluationDevelopmentGroupBusiness.All on a.EvaluationDevelopmentGroupID equals b.EvaluationDevelopmentGroupID
                                                    where a.EducationLevelID == educationLevelID && a.IsActive == true &&
                                                          b.AppliedLevel == _globalInfo.AppliedLevel && b.IsActive == true &&
                                                          b.EducationLevelID == educationLevelID && 
                                                          b.ProvinceID == _globalInfo.ProvinceID &&
                                                          a.ProvinceID == _globalInfo.ProvinceID
                                                    select a).ToList();

            List<EvaluationDevelopmentBO> lstResult = new List<EvaluationDevelopmentBO>();
            EvaluationDevelopmentBO objResult = null;
            EvaluationDevelopment objEDI = null;
            int countmax = 1;
            int check = 0;
            foreach (var item in lstData)
            {
                objEDI = new EvaluationDevelopment();
                objEDI = lstCheck.FirstOrDefault(x => x.EvaluationDevelopmentGroup.EvaluationDevelopmentGroupName == item.EvaluationDevGroupName);

                if (objEDI == null)
                {
                    continue;
                }
                
                if (check == 0)
                {
                    check = objEDI.EvaluationDevelopmentGroupID;
                }
                else
                {
                    if (check != objEDI.EvaluationDevelopmentGroupID)
                    {
                        check = objEDI.EvaluationDevelopmentGroupID;
                        countmax = 1;
                    }
                }

                if (objEDI != null)
                {
                    objResult = new EvaluationDevelopmentBO();
                    objResult.EvaluationDevGroupID = objEDI.EvaluationDevelopmentGroupID;
                    objResult.EvaluationDevGroupName = item.EvaluationDevGroupName;
                    objResult.EvaluationDevIndexName = item.EvaluationDevIndexName;
                    objResult.EvaluationDevIndexCode = item.EvaluationDevIndexCode;
                    objResult.Order = lstCheck.Where(x => x.EvaluationDevelopmentGroupID == objEDI.EvaluationDevelopmentGroupID).Max(x => x.OrderID) + countmax;
                    objEDI = lstCheck.Where(x => x.EvaluationDevelopmentGroup.EvaluationDevelopmentGroupName == item.EvaluationDevGroupName
                                               && x.EvaluationDevelopmentCode == item.EvaluationDevIndexCode).FirstOrDefault();
                    objResult.EvaluationDevIndexID = objEDI != null ? objEDI.EvaluationDevelopmentID : -1; // true: insert, false: update                   
                    lstResult.Add(objResult);
                    countmax++;
                }
            }
            return lstResult;
        }
    }
}
