﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareEvaluationIndexArea.Models
{
    public class EvaluationIndex
    {      
        public string EvaluationIndexCode{get;set;}
        public string EvaluationIndexDes{get;set;}
        public int EvaluationIndexID{get;set;}
        public int EvaluationGroupID{get;set;}
        public int? TT { get; set; }
    }
}