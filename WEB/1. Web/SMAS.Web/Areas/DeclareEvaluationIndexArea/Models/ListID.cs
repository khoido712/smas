﻿using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareEvaluationIndexArea.Models
{
    public class ListID
    {
        public int DeclareEvaluationIndex { get; set; }
        public int EvaluationGroup { get; set; }
        public int EvaluationIndex { get; set; }
    }
}