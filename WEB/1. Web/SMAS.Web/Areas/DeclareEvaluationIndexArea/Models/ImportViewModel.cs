﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareEvaluationIndexArea.Models
{
    public class ImportViewModel
    {
        public string EvaluationDevGroupName { get; set; }
        public string EvaluationDevIndexCode { get; set; }
        public string EvaluationDevIndexName { get; set; }
        public int EvaluationDevGroupID { get; set; }
        public bool IsNew { get; set; }
        public int? Order { get; set; }
    }
}