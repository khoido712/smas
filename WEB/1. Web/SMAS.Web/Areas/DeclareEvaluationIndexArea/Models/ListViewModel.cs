﻿using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareEvaluationIndexArea.Models
{
    public class ListViewModel
    {
        public int DeclareEvaluationIndexID { get; set; }
        [ResourceDisplayName("VnenClassSubject_Is_Vnen")]
        public long DeclareEducationID { get; set; }
        public int SchoolID { get; set; }
        [ResourceDisplayName("EvaluationDevelopmentGroupName")] // Ten linh vuc
        public string EvaluationDevelopmentGroupName { get; set; }
        [ResourceDisplayName("EvaluationDevelopmentName")] // Ten chi so
        public string EvaluationDevelopmentIndexName { get; set; }
        [ResourceDisplayName("TT")]
        public int? EvaluationDevelopmentOrderID { get; set; } 
        [ResourceDisplayName("Mã")]
        public string EvaluationDevelomentIndexCode { get; set; }
        //public int? AppliedType { get; set; }
        //public int? IsCommenting { get; set; }
        //public bool? IsVnenSubject { get; set; }
        public int DeclareEducatioGroupID { get; set; }
        public int AcademicID { get; set; }
        public int EducationID { get; set; }

        public int EvaluationGroupID { get; set; }
        public int EvaluationIndexID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? Modified { get; set; }
        public int? EvaluationGroupOrderID { get; set; }             
        public int? EvaluationGroupOfDEI { get; set; }
        public bool? CreatedBySchool { get; set; }
        public bool isCheck {
            get 
            {
                if (EvaluationGroupOfDEI == null)
                    return false;
                return true;
            }
        }



    }
}