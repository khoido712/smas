﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassificationCriteriaArea
{
    public class ClassificationCriteriaAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ClassificationCriteriaArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ClassificationCriteriaArea_default",
                "ClassificationCriteriaArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
