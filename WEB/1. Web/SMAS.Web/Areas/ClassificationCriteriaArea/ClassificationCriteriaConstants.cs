﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ClassificationCriteriaArea
{
    public class ClassificationCriteriaConstants
    {
        public const string LIST_CLASSIFICATIONCRITERIA = "LIST_CLASSIFICATIONCRITERIA";
        public const string LIST_GENRE = "LIST_GENRE";
        public const string LIST_TYPECONFIG = "LIST_TYPECONFIG";
        public const string LIST_LEVEL = "LIST_LEVEL";
        public const string LIST_CLASSIFICATIONCRITERIADETAIL = "LIST_CLASSIFICATIONCRITERIADETAIL";
        public const string LIST_NOTPAGING_CLASSIFICATIONCRITERIADETAIL = "LIST_NOTPAGING_CLASSIFICATIONCRITERIADETAIL";
        public const string LIST_EFFECTDATE = "LIST_EFFECTDATE";
        public const int PAGE_SIZE = 15;
        public const string CURRENT_PAGE = "current of page";
        public const string TOTAL_RECORD = "total page";
    }
}