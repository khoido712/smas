﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassificationCriteriaArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ClassificationCriteria_Label_EffectDate")]        
        [UIHint("Combobox")]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [AdditionalMetadata("ViewDataKey", ClassificationCriteriaConstants.LIST_EFFECTDATE)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "LoadDataGrid()")]        
        public DateTime? EffectDate { get; set; }

        [ResourceDisplayName("ClassificationCriteria_Label_Genre")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassificationCriteriaConstants.LIST_GENRE)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "LoadEffectDate(this)")]//"LoadDataGrid()"
        public int? Genre { get; set; }

        [ResourceDisplayName("ClassificationCriteria_Label_TypeConfig")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassificationCriteriaConstants.LIST_TYPECONFIG)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "LoadDataGrid()")]
        public int? TypeConfig { get; set; }
    }
}