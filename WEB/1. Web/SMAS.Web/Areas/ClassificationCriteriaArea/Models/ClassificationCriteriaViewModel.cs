﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassificationCriteriaArea.Models
{
    public class ClassificationCriteriaViewModel
    {
        [ResourceDisplayName("ClassificationCriteria_Label_EffectDate")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public DateTime? EDate { get; set; }
        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? Genre { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ClassificationCriteria_Label_Month")]        
        public int Month { get; set; }       

        public bool? Pass { get; set; }
        public string Note { get; set; }
    }
}