﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ClassificationCriteriaArea.Models;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using Telerik.Web.Mvc;
using System.Threading.Tasks;

namespace SMAS.Web.Areas.ClassificationCriteriaArea.Controllers
{
    public class ClassificationCriteriaController : Controller
    {
        private readonly ITypeConfigBusiness TypeConfigBusiness;
        private readonly IClassificationCriteriaBusiness ClassificationCriteriaBusiness;
        private readonly IClassificationCriteriaDetailBusiness ClassificationCriteriaDetailBusiness;
        private readonly IClassificationLevelHealthBusiness ClassificationLevelHealthBusiness;
        public ClassificationCriteriaController(ITypeConfigBusiness typeConfigBusiness, IClassificationCriteriaBusiness classificationCriteriaBusiness, IClassificationCriteriaDetailBusiness classificationCriteriaDetailBusiness,
            IClassificationLevelHealthBusiness classificationLevelHealthBusiness)
        {
            this.TypeConfigBusiness = typeConfigBusiness;
            this.ClassificationCriteriaBusiness = classificationCriteriaBusiness;
            this.ClassificationCriteriaDetailBusiness = classificationCriteriaDetailBusiness;
            this.ClassificationLevelHealthBusiness = classificationLevelHealthBusiness;
        }
        // GET: /ClassificationCriteriaArea/ClassificationCriteria/

        public ActionResult Index()
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[ClassificationCriteriaConstants.LIST_GENRE] = new SelectList(CommonList.GenreMN(), "key", "value");
            List<TypeConfig> ListTypeConfig = TypeConfigBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData[ClassificationCriteriaConstants.LIST_TYPECONFIG] = new SelectList(ListTypeConfig, "TypeConfigID", "TypeConfigName");
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["GenreID"] = -1;
            var lstEffectDate = ClassificationCriteriaBusiness.Search(SearchInfo).Select(o => o.EffectDate).Distinct().OrderByDescending(o => o).ToList().Select(p => new { Date = p.ToString("dd/MM/yyyy") }).ToList();
            ViewData[ClassificationCriteriaConstants.LIST_EFFECTDATE] = new SelectList(lstEffectDate, "Date", "Date");
            return View();
        }

        public PartialViewResult Search(DateTime? EffectDate, int? Genre, int? TypeConfig)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EffectDate"] = EffectDate;
            SearchInfo["GenreID"] = Genre;
            SearchInfo["TypeConfigID"] = TypeConfig;
            int totalRecord = 0;

            //add search info	
            IQueryable<ClassificationCriteriaDetail> query = this.ClassificationCriteriaDetailBusiness.GetListClassificationDetail(SearchInfo);
            List<ClassificationLevelHealth> LstClassificationLevelHealth = ClassificationLevelHealthBusiness.All.Where(o => o.IsActive == true && o.TypeConfigID == TypeConfig).OrderByDescending(o => o.Level).ToList();
            ViewData[ClassificationCriteriaConstants.LIST_LEVEL] = LstClassificationLevelHealth;

            totalRecord = LstClassificationLevelHealth.Count > 0 ? (query.Count() / LstClassificationLevelHealth.Count) : 0;//(TypeConfig == 1 || TypeConfig == 2) ? 60 : (228 - 61);
            if (totalRecord > 0)
            {
                ViewData[ClassificationCriteriaConstants.TOTAL_RECORD] = totalRecord;
                ViewData[ClassificationCriteriaConstants.CURRENT_PAGE] = 1;
                List<ClassificationCriteriaDetailBO> lstClassification = query.Select
                                                                        (o => new ClassificationCriteriaDetailBO
                                                                        {
                                                                            ClassificationCriteriaID = o.ClassificationCriteriaID,
                                                                            ClassificationCriteriaDetailID = o.ClassificationCriteriaDetailID,
                                                                            ClassificationLevelHealthID = o.ClassificationLevelHealthID,
                                                                            IndexValue = o.IndexValue,
                                                                            Month = o.Month
                                                                        }).OrderBy(o => o.ClassificationLevelHealthID).ThenBy(o => o.Month).Take(ClassificationCriteriaConstants.PAGE_SIZE).ToList();
                // tinh muc phan loai
                decimal? IndexValue = null;
                List<int> months = lstClassification.Select(p => p.Month).ToList();
                List<int> levelIds = LstClassificationLevelHealth.Select(p => p.ClassificationLevelHealthID).ToList();
                var classifiCationTmp = query.Where(p => months.Contains(p.Month) && levelIds.Contains(p.ClassificationLevelHealthID)).ToList();
                int month = 0;
                int levelId = 0;
                for (int i = lstClassification.Count - 1; i >= 0; i--)
                {
                    month = lstClassification[i].Month;
                    for (int k = levelIds.Count - 1; k >= 0; k--)
                    {
                        levelId = levelIds[k];
                        IndexValue = classifiCationTmp.Where(p => p.Month == month && p.ClassificationLevelHealthID == levelId).Select(p => p.IndexValue).FirstOrDefault();
                        if (IndexValue.HasValue)
                        {
                            lstClassification[i].IndexValueText = lstClassification[i].IndexValueText + "#_" + levelIds[k] + "_" + IndexValue + "#";
                        }
                    }
                }
                ViewData[ClassificationCriteriaConstants.LIST_CLASSIFICATIONCRITERIADETAIL] = lstClassification;
            }
            else
            {
                ViewData[ClassificationCriteriaConstants.TOTAL_RECORD] = 0;
                ViewData[ClassificationCriteriaConstants.CURRENT_PAGE] = 1;
                ViewData[ClassificationCriteriaConstants.LIST_CLASSIFICATIONCRITERIADETAIL] = new List<ClassificationCriteriaDetail>();
            }
            //Get view data here
            return PartialView("_List");
        }
        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchPaging(DateTime? EffectDate, int? Genre, int? TypeConfig, GridCommand command)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EffectDate"] = EffectDate;
            SearchInfo["GenreID"] = Genre;
            SearchInfo["TypeConfigID"] = TypeConfig;
            // tinh muc phan loai
            List<ClassificationLevelHealth> LstClassificationLevelHealth = ClassificationLevelHealthBusiness.All.Where(o => o.IsActive == true && o.TypeConfigID == TypeConfig).ToList();
            int TotalRecord = 0;
            int SkipPage = 0;
            IQueryable<ClassificationCriteriaDetail> lst = this.ClassificationCriteriaDetailBusiness.GetListClassificationDetail(SearchInfo).OrderBy(o => o.ClassificationLevelHealthID).ThenBy(o => o.Month);
            TotalRecord = (lst.Count() / LstClassificationLevelHealth.Count());
            SkipPage = TotalRecord - (command.Page - 1) * ClassificationCriteriaConstants.PAGE_SIZE;
            List<ClassificationCriteriaDetailBO> lstClassification = lst.Select(o => new ClassificationCriteriaDetailBO
                                                                    {
                                                                        ClassificationCriteriaID = o.ClassificationCriteriaID,
                                                                        ClassificationCriteriaDetailID = o.ClassificationCriteriaDetailID,
                                                                        ClassificationLevelHealthID = o.ClassificationLevelHealthID,
                                                                        IndexValue = o.IndexValue,
                                                                        Month = o.Month
                                                                    }).OrderBy(o => o.ClassificationLevelHealthID).ThenBy(o => o.Month).Skip((command.Page - 1) * ClassificationCriteriaConstants.PAGE_SIZE).Take(SkipPage >= ClassificationCriteriaConstants.PAGE_SIZE ? ClassificationCriteriaConstants.PAGE_SIZE : SkipPage).ToList();


            decimal? IndexValue = null;
            List<int> months = lstClassification.Select(p => p.Month).ToList();
            List<int> levelIds = LstClassificationLevelHealth.Select(p => p.ClassificationLevelHealthID).ToList();
            var classifiCationTmp = lst.Where(p => months.Contains(p.Month) && levelIds.Contains(p.ClassificationLevelHealthID)).ToList();
            int month = 0;
            int levelId = 0;
            for (int i = lstClassification.Count - 1; i >= 0; i--)
            {
                month = lstClassification[i].Month;
                for (int k = levelIds.Count - 1; k >= 0; k--)
                {
                    levelId = levelIds[k];
                    IndexValue = classifiCationTmp.Where(p => p.Month == month && p.ClassificationLevelHealthID == levelId).Select(p => p.IndexValue).FirstOrDefault();
                    if (IndexValue.HasValue)
                    {
                        lstClassification[i].IndexValueText = lstClassification[i].IndexValueText + "#_" + levelIds[k] + "_" + IndexValue + "#";
                    }
                }
            }

            return View(new GridModel<ClassificationCriteriaDetailBO>
            {
                Data = lstClassification,
                Total = (lst.Count() / LstClassificationLevelHealth.Count), //(TypeConfig == 1 || TypeConfig == 2) ? 60 : (228 - 61),
            });
        }
        //
        // GET: /ClassificationCriteriaArea/ClassificationCriteria/Details/5


        [ValidateAntiForgeryToken]
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /ClassificationCriteriaArea/ClassificationCriteria/Create
        public ActionResult Create()
        {
            return View();
        }

        public JsonResult AjaxloadEffectDate()
        {
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["GenreID"] = -1;
            var lstEffectDate = ClassificationCriteriaBusiness.Search(SearchInfo).Select(o => o.EffectDate).Distinct().OrderByDescending(o => o).ToList().Select(p => new { Date = p.ToString("dd/MM/yyyy") }).ToList();
            if (lstEffectDate != null && lstEffectDate.Count != 0)
            {
                return Json(new SelectList(lstEffectDate, "Date", "Date"));
            }
            else
            {
                return Json(new SelectList(new string[] { "" }, ""));
            }

        }
        //
        // POST: /ClassificationCriteriaArea/ClassificationCriteria/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DateTime EDate, int Genre)
        {
            this.ClassificationCriteriaBusiness.Insert(EDate, Genre);
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(int[] Month, FormCollection form)
        {
            int ClassificationCriteriaID = SMAS.Business.Common.Utils.GetInt(form["id"]);//TH sửa 1 dòng trên GridView
            if (ClassificationCriteriaID == 0)
            {
                ClassificationCriteriaID = SMAS.Business.Common.Utils.GetInt(form["ClassificationCriteriaID"]);//TH sửa toàn bộ
            }

            int TypeConfig = ClassificationCriteriaBusiness.Find(ClassificationCriteriaID).TypeConfigID;
            List<ClassificationCriteriaDetail> lstCCD = new List<ClassificationCriteriaDetail>();
            List<ClassificationLevelHealth> LstCLHealth = ClassificationLevelHealthBusiness.All.Where(o => o.IsActive == true && o.TypeConfigID == TypeConfig).ToList();
            for (int i = 0; i < Month.Count(); i++)
            {

                foreach (var item in LstCLHealth)
                {
                    ClassificationCriteriaDetail CCD = new ClassificationCriteriaDetail();
                    string IndexValue = form[item.Level.ToString() + "_" + Month[i].ToString()];
                    decimal _IndexValue = 0;
                    bool CheckDecimal = true;
                    if (IndexValue != "")
                    {
                        CheckDecimal = decimal.TryParse(IndexValue, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out _IndexValue);
                    }
                    if (CheckDecimal)
                    {
                        CCD.ClassificationCriteriaID = ClassificationCriteriaID;
                        CCD.ClassificationLevelHealthID = item.ClassificationLevelHealthID;
                        if (_IndexValue != 0)
                        {
                            if (_IndexValue >= 1000)
                                return Json(new JsonMessage(Res.Get("Common_Label_ValidatationIndexValue"), "error"));
                            CCD.IndexValue = _IndexValue;
                            CCD.Month = Month[i];
                            lstCCD.Add(CCD);
                        }

                    }
                    else
                    {
                        return Json(new JsonMessage(Res.Get("Common_Label_ValidatationIndexValue"), "error"));
                    }
                }
            }
            this.ClassificationCriteriaDetailBusiness.Update(lstCCD);
            this.ClassificationCriteriaDetailBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(DateTime EffectDate, int Genre, int TypeConfig)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EffectDate"] = EffectDate;
            SearchInfo["GenreID"] = Genre;
            // khi xoa thi xoa ca 3 type la cannang/chieucao/bmi
            //SearchInfo["TypeConfigID"] = TypeConfig;
            List<ClassificationCriteria> ListCC = ClassificationCriteriaBusiness.Search(SearchInfo).ToList();
            if (ListCC.Count > 0)
            {
                // split task
                int numTask = 3;
                Task[] arrTask = new Task[numTask];
                for (int it = 0; it < numTask; it++)
                {
                    var lstDelete = ListCC.Where(p => p.ClassificationCriteriaID % numTask == it).ToList();
                    arrTask[it] = Task.Factory.StartNew(() =>
                    {
                        SMASEntities contextTask = new SMASEntities();
                        contextTask.Configuration.AutoDetectChangesEnabled = false;
                        contextTask.Configuration.ValidateOnSaveEnabled = false;
                        ClassificationCriteria obj = null;
                        for (int n = lstDelete.Count - 1; n >= 0; n--)
                        {
                            obj = lstDelete[n];
                            obj = contextTask.ClassificationCriteria.Where(p => p.ClassificationCriteriaID == obj.ClassificationCriteriaID).FirstOrDefault();
                            // split task                            
                            int numTaskDelete = 10;
                            Task[] taskDelete = new Task[numTaskDelete];
                            for (int k = 0; k < numTaskDelete; k++)
                            {
                                int taskIndex = k;
                                taskDelete[k] = Task.Factory.StartNew(() =>
                                {
                                    SMASEntities contextDelete = new SMASEntities();
                                    var lstDetail = contextDelete.ClassificationCriteriaDetail.Where(p => p.ClassificationCriteriaID == obj.ClassificationCriteriaID && p.ClassificationCriteriaDetailID % numTask == taskIndex).ToList();
                                    contextDelete.Configuration.AutoDetectChangesEnabled = false;
                                    contextDelete.Configuration.ValidateOnSaveEnabled = false;
                                    for (int i = lstDetail.Count - 1; i >= 0; i--)
                                    {
                                        contextDelete.ClassificationCriteriaDetail.Remove(lstDetail[i]);
                                    }

                                    contextDelete.Configuration.AutoDetectChangesEnabled = true;
                                    contextDelete.Configuration.ValidateOnSaveEnabled = true;
                                    contextDelete.SaveChanges();
                                });
                            }

                            // wait all
                            Task.WaitAll(taskDelete);
                            contextTask.ClassificationCriteria.Remove(obj);
                        }

                        contextTask.Configuration.AutoDetectChangesEnabled = true;
                        contextTask.Configuration.ValidateOnSaveEnabled = true;
                        contextTask.SaveChanges();
                    });
                }

                Task.WaitAll(arrTask);
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteClassificationCriteria"), "error"));
            }

        }

        public FileResult ExportExcel(DateTime EffectDate, int Genre, int TypeConfig)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EffectDate"] = EffectDate;
            SearchInfo["GenreID"] = Genre;
            SearchInfo["TypeConfigID"] = TypeConfig;
            //add search info	
            List<ClassificationCriteriaDetail> ListCCD = this.ClassificationCriteriaDetailBusiness.GetListClassificationDetail(SearchInfo).OrderBy(o => o.Month).ToList();
            List<ClassificationLevelHealth> LstCLHealth = ClassificationLevelHealthBusiness.All.Where(o => o.IsActive == true && o.TypeConfigID == TypeConfig).OrderByDescending(o => o.Level).ToList();
            List<int> Month = ListCCD.Select(o => o.Month).Distinct().ToList();

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "MN" + "/" + "Template_Export_TCPLSK.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);
            string _Genre = (Genre == 1 ? Res.Get("Common_Label_Boys") : Res.Get("Common_Label_Girls"));
            string TypeConfigName = ListCCD.FirstOrDefault().ClassificationCriteria.TypeConfig.TypeConfigName;
            DateTime _EffectDate = ListCCD.FirstOrDefault().ClassificationCriteria.EffectDate.Date;
            //Fill dữ liệu chung            
            sheet.SetCellValue("E4", _Genre);
            sheet.SetCellValue("E5", TypeConfigName);
            sheet.SetCellValue("E6", _EffectDate.ToString("dd/MM/yyyy"));
            IVTRange range = sheet.GetRange(8, 3, 10, 3);
            int d = 0;
            foreach (var item in LstCLHealth)
            {
                d = d + 1;
                //Copy row style
                sheet.CopyPasteSameSize(range, 8, 2 + d);
                sheet.SetCellValue(9, 2 + d, item.Level);
            }
            sheet.MergeRow(8, 3, LstCLHealth.Count + 2);
            sheet.SetCellValue("C8", "Mức phân loại");
            int firstRow = 10;
            range = sheet.GetRange(10, 1, 10, 2 + d);
            int headerStartCol = 0;
            int index = 0;
            decimal? IndexValue = null;
            foreach (var item in Month)
            {
                int currentRow = firstRow + index;
                index++;
                //Cot diem dau tien     
                headerStartCol = 3;
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, currentRow);
                //Fill dữ liệu
                sheet.SetCellValue("A" + currentRow, index);
                sheet.SetCellValue("B" + currentRow, item);

                foreach (ClassificationLevelHealth clhealth in LstCLHealth)
                {
                    var temp = ListCCD.Where(o => o.Month == item && o.ClassificationLevelHealthID == clhealth.ClassificationLevelHealthID);
                    if (temp != null && temp.Count() > 0)
                    {
                        IndexValue = temp.FirstOrDefault().IndexValue;
                    }
                    else
                    {
                        IndexValue = null;
                    }
                    sheet.SetCellValue(currentRow, headerStartCol, IndexValue);
                    headerStartCol = headerStartCol + 1;
                }
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            string ReportName = "TieuChiPhanLoaiSK_" + _Genre + "_" + TypeConfigName + ".xls";
            result.FileDownloadName = ReportName;
            return result;
        }

        #region import

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            GlobalInfo global = new GlobalInfo();

            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                return Json(new JsonMessage(""));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel()
        {
            try
            {
                string FilePath = (string)Session["FilePath"];

                IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
                //Lấy sheet 
                IVTWorksheet sheet = oBook.GetSheet(1);

                GlobalInfo global = new GlobalInfo();
                string _Genre = (sheet.GetCellValue(4, 5) == null) ? "" : sheet.GetCellValue(4, 5).ToString().ToUpper();
                string _TypeConfigName = (sheet.GetCellValue(5, 5) == null) ? "" : sheet.GetCellValue(5, 5).ToString().ToUpper();
                string _EffectDate = (sheet.GetCellValue(6, 5) == null) ? "" : sheet.GetCellValue(6, 5).ToString();
                List<TypeConfig> ListTypeConfig = TypeConfigBusiness.All.Where(o => o.IsActive == true).ToList();

                string Error = "";
                if (_Genre != Res.Get("Common_Label_Girls").ToUpper() && _Genre != Res.Get("Common_Label_Boys").ToUpper())
                {
                    Error = "Đối tượng không đúng hoặc chưa được nhập";
                }
                if (ListTypeConfig.Where(o => o.TypeConfigName.ToUpper() == _TypeConfigName).Count() == 0)
                {
                    if (Error == "")
                        Error = "Loại đánh giá không đúng";
                    else Error = Error + " - Loại đánh giá không đúng";
                }
                DateTime EffectDate;
                if (!DateTime.TryParse(_EffectDate, out EffectDate))
                {
                    if (Error == "")
                        Error = "Ngày áp dụng không đúng";
                    else Error = Error + " - Ngày áp dụng không đúng";
                }
                if (Error != "")
                {
                    return Json(new { success = Error });
                }
                int TypeConfigID = ListTypeConfig.Where(o => o.TypeConfigName.ToUpper() == _TypeConfigName).FirstOrDefault().TypeConfigID;
                List<ClassificationLevelHealth> LstCLHealth = ClassificationLevelHealthBusiness.All.Where(o => o.IsActive == true && o.TypeConfigID == TypeConfigID).OrderByDescending(o => o.Level).ToList();
                for (int i = 3; i <= LstCLHealth.Count + 2; i++)
                {
                    string _Level = (sheet.GetCellValue(9, i) == null) ? "" : sheet.GetCellValue(9, i).ToString();
                    int Level;
                    if (int.TryParse(_Level, out Level))
                    {
                        if (LstCLHealth.Where(o => o.Level == Level).Count() == 0)
                        {
                            return Json(new { success = "Mức phân loại không tồn tại. Vui lòng kiểm tra lại." });
                        }
                    }
                    else
                    {
                        return Json(new { success = "Mức phân loại không tồn tại. Vui lòng kiểm tra lại." });
                    }
                }
                int Genre;
                if (_Genre == Res.Get("Common_Label_Boys").ToUpper())
                {
                    Genre = 1;
                }
                else
                    Genre = 0;
                int ClassificationCriteriaID;
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EffectDate"] = EffectDate;
                SearchInfo["GenreID"] = Genre;
                SearchInfo["TypeConfigID"] = TypeConfigID;
                List<ClassificationCriteria> ListCC = ClassificationCriteriaBusiness.Search(SearchInfo).ToList();
                if (ListCC.Count > 0)
                    ClassificationCriteriaID = ListCC.FirstOrDefault().ClassificationCriteriaID;
                else
                {
                    //Thuc hien Insert vao bang ClassificationCriteria                  
                    this.ClassificationCriteriaBusiness.Insert(EffectDate, Genre);
                    ClassificationCriteriaID = ClassificationCriteriaBusiness.Search(SearchInfo).FirstOrDefault().ClassificationCriteriaID;
                }
                bool PASS = true;
                List<ClassificationCriteriaDetail> lstCCD = new List<ClassificationCriteriaDetail>();
                List<ClassificationCriteriaViewModel> lstMonth = new List<ClassificationCriteriaViewModel>();
                for (int i = 10; i < 200; i++)
                {
                    if (sheet.GetCellValue(i, 2) == null)
                    {
                        break;
                    }
                    ClassificationCriteriaViewModel month = new ClassificationCriteriaViewModel();
                    int Month;
                    if (int.TryParse(sheet.GetCellValue(i, 2).ToString(), out Month))
                    {
                        month.Month = Month;
                        month.Pass = true;
                        if (!CheckMonth(Month, _TypeConfigName))
                        {
                            month.Pass = false;
                            PASS = false;
                            month.Note = "Tháng tuổi không hợp lệ";
                        }
                    }
                    else
                    { return Json(new { success = "Tháng tuổi phải là kiểu số nguyên" }); }
                    for (int j = 0; j < LstCLHealth.Count; j++)
                    {
                        decimal IndexValue;
                        if (sheet.GetCellValue(i, 3 + j) == null)
                        {
                            IndexValue = 0;
                        }
                        else
                        {
                            if (decimal.TryParse(sheet.GetCellValue(i, 3 + j).ToString().Replace(",", "."), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out IndexValue))
                            {
                                if (IndexValue >= 1000 && IndexValue < 0)
                                {
                                    month.Pass = false;
                                    PASS = false;
                                    if (month.Note == null)
                                        month.Note = "Giá trị mức phân loại không hợp lệ";
                                    else month.Note = month.Note + " - Giá trị mức phân loại không hợp lệ";
                                }
                            }
                            else
                            { return Json(new { success = "Giá trị mức phân loại phải là số thập phân" }); }
                        }

                        ClassificationCriteriaDetail CCD = new ClassificationCriteriaDetail();
                        CCD.ClassificationCriteriaID = ClassificationCriteriaID;
                        CCD.ClassificationLevelHealthID = LstCLHealth[j].ClassificationLevelHealthID;
                        CCD.Month = Month;
                        if (IndexValue != 0)
                        {
                            CCD.IndexValue = IndexValue;
                        }
                        lstCCD.Add(CCD);
                    }
                    lstMonth.Add(month);
                }

                if (PASS == true)
                {
                    this.ClassificationCriteriaDetailBusiness.Update(lstCCD);
                    this.ClassificationCriteriaDetailBusiness.Save();
                    return Json(new { success = 1 });
                }
                else
                {
                    Session["lstCCD"] = lstCCD;
                    Session["LstCLHealth"] = LstCLHealth;
                    Session["lstMonth"] = lstMonth;
                    return Json(new { success = 0 });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = "Định dạng file excel không đúng. Bạn sử dụng file export để làm mẫu." });
            }

        }
        public PartialViewResult ViewDataImportExcel()
        {
            List<ClassificationCriteriaDetail> lstCCD = (List<ClassificationCriteriaDetail>)Session["lstCCD"];
            List<ClassificationCriteriaViewModel> lstMonth = (List<ClassificationCriteriaViewModel>)Session["lstMonth"];
            List<ClassificationLevelHealth> LstCLHealth = (List<ClassificationLevelHealth>)Session["LstCLHealth"];
            ViewData[ClassificationCriteriaConstants.LIST_CLASSIFICATIONCRITERIA] = lstMonth;
            ViewData[ClassificationCriteriaConstants.LIST_LEVEL] = LstCLHealth;
            ViewData[ClassificationCriteriaConstants.LIST_CLASSIFICATIONCRITERIADETAIL] = lstCCD;
            return PartialView("ViewDataImport");
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportDataPass()
        {
            List<ClassificationCriteriaDetail> lstCCD = (List<ClassificationCriteriaDetail>)Session["lstCCD"];
            List<ClassificationCriteriaViewModel> lstMonth = (List<ClassificationCriteriaViewModel>)Session["lstMonth"];
            List<int> MonthNoPass = lstMonth.Where(o => o.Pass == false).Select(o => o.Month).ToList();
            List<ClassificationCriteriaDetail> lstCCDPass = lstCCD.Where(o => !MonthNoPass.Contains(o.Month)).ToList();
            this.ClassificationCriteriaDetailBusiness.Update(lstCCDPass);
            this.ClassificationCriteriaDetailBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage")));
        }
        #endregion
        private bool CheckMonth(int month, string TypeConfigName)
        {
            if (TypeConfigName.ToUpper() == "CHIỀU CAO" || TypeConfigName.ToUpper() == "CÂN NẶNG")
            {
                if (month < 0 || month > 60)
                {
                    return false;
                }
            }
            else
            {
                if (month < 61 || month > 228)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
