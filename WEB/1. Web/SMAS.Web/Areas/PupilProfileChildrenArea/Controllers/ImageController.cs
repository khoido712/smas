﻿using System;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.PupilProfileChildrenArea.Controllers
{
    public class ImageController : Controller
    {
        private readonly IPupilProfileBusiness PupilProfileBusiness;

        public ImageController(IPupilProfileBusiness PupilProfileBusiness)
        {
            this.PupilProfileBusiness = PupilProfileBusiness;
        }

        public ActionResult Index()
        {
            return View();
        }


        //[ValidateAntiForgeryToken]
         [SkipCheckRole]
        public ActionResult Show(int id)
        {
            string defaultImagePath = Server.MapPath("~/Content/images/question_mark.png");
            PupilProfile ep = PupilProfileBusiness.Find(id);
            byte[] imageData;
            if (ep != null)
            {
                if (ep.Image != null)
                {
                    imageData = ep.Image;
                    return File(imageData, "image/jpg");
                }
                else
                {
                    imageData = FileToByteArray(defaultImagePath);
                    return File(imageData, "image/jpg");
                }
            }

            //neu ko co tra ve anh default
            imageData = FileToByteArray(defaultImagePath);
            return File(imageData, "image/jpg");
        }

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }
    }
}
