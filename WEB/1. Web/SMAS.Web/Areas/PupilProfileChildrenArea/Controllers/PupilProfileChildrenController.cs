﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PupilProfileChildrenArea.Models;
using SMAS.Web.Areas.PupilProfileChildrenArea;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using System.Transactions;
using Telerik.Web.Mvc;
using Newtonsoft.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;

namespace SMAS.Web.Areas.PupilProfileChildrenArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilProfileChildrenController : BaseController
    {
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPolicyTargetBusiness PolicyTargetBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly IAreaBusiness AreaBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IPriorityTypeBusiness PriorityTypeBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDisabledTypeBusiness DisabledTypeBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IHabitGroupBusiness HabitGroupBusiness;
        private readonly IHabitDetailBusiness HabitDetailBusiness;
        private readonly IHabitOfChildrenBusiness HabitOfChildrenBusiness;
        private readonly ITypeOfDishBusiness TypeOfDishBusiness;
        private readonly IFoodHabitOfChildrenBusiness FoodHabitOfChildrenBusiness;
        private readonly IPolicyRegimeBusiness PolicyRegimeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IMovementAcceptanceBusiness MovementAcceptanceBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly ILeavingReasonBusiness LeavingReasonBusiness;
        private readonly IVillageBusiness VillageBusiness;
        private readonly IOtherEthnicBusiness OtherEthnicBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IDevelopmentOfChildrenBusiness DevelopmentOfChildrenBusiness;
        private readonly IGoodChildrenTicketBusiness GoodChildrenTicketBusiness;

        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private const string TEMPLATE_PUPILPROFILE = "BM_DanhSachHocSinh.xls";
        private const string TEMPLATE_PUPILPROFILE_NEW = "BM_DanhSachHocSinh_Moi.xls";
        private const string SEMICOLON_SPACE = "; ";
        // Format: [KHOI][LOP][TRUONG]
        private const string TEMPLATE_PUPILPROFILE_FORMAT = "BM_DanhSachTre{0}{1}{2}.xls";
        private static bool CHECK_ENABLE_CREATE_DELETE = false;

        public PupilProfileChildrenController(IPupilProfileBusiness pupilprofileBusiness, IClassProfileBusiness ClassProfileBusiness, IPolicyTargetBusiness PolicyTargetBusiness,
            IGraduationLevelBusiness GraduationLevelBusiness, IEthnicBusiness EthnicBusiness, IReligionBusiness ReligionBusiness, ICodeConfigBusiness CodeConfigBusiness
            , IDistrictBusiness DistrictBusiness, ICommuneBusiness CommuneBusiness, IAreaBusiness AreaBusiness, IPriorityTypeBusiness PriorityTypeBusiness
            , IProvinceBusiness ProvinceBusiness, IDisabledTypeBusiness DisabledTypeBusiness, IFamilyTypeBusiness FamilyTypeBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness, IHabitGroupBusiness HabitGroupBusiness, IHabitDetailBusiness HabitDetailBusiness
            , IHabitOfChildrenBusiness HabitOfChildrenBusiness, ITypeOfDishBusiness TypeOfDishBusiness, IFoodHabitOfChildrenBusiness FoodHabitOfChildrenBusiness
            , IEducationLevelBusiness EducationLevelBusiness, IPolicyRegimeBusiness policyRegimeBusiness, ISchoolProfileBusiness schoolProfileBusiness
             , IMonitoringBookBusiness monitoringBookBusiness, ISchoolMovementBusiness SchoolMovementBusiness, IAcademicYearBusiness AcademicYearBusiness,
            IMovementAcceptanceBusiness MovementAcceptanceBusiness, IClassMovementBusiness ClassMovementBusiness, IPupilLeavingOffBusiness PupilLeavingOffBusiness,
            ILeavingReasonBusiness LeavingReasonBusiness, IVillageBusiness VillageBusiness, IOtherEthnicBusiness OtherEthnicBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness, IPupilAbsenceBusiness PupilAbsenceBusiness, IDevelopmentOfChildrenBusiness DevelopmentOfChildrenBusiness,
            IGoodChildrenTicketBusiness GoodChildrenTicketBusiness
            )
        {
            this.GoodChildrenTicketBusiness = GoodChildrenTicketBusiness;
            this.DevelopmentOfChildrenBusiness = DevelopmentOfChildrenBusiness;
            this.PupilAbsenceBusiness = PupilAbsenceBusiness;
            this.PupilProfileBusiness = pupilprofileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PolicyTargetBusiness = PolicyTargetBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.ReligionBusiness = ReligionBusiness;
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.CommuneBusiness = CommuneBusiness;
            this.AreaBusiness = AreaBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.PriorityTypeBusiness = PriorityTypeBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.DisabledTypeBusiness = DisabledTypeBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.HabitGroupBusiness = HabitGroupBusiness;
            this.HabitDetailBusiness = HabitDetailBusiness;
            this.HabitOfChildrenBusiness = HabitOfChildrenBusiness;
            this.TypeOfDishBusiness = TypeOfDishBusiness;
            this.FoodHabitOfChildrenBusiness = FoodHabitOfChildrenBusiness;
            this.PolicyRegimeBusiness = policyRegimeBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.MovementAcceptanceBusiness = MovementAcceptanceBusiness;
            this.ClassMovementBusiness = ClassMovementBusiness;
            this.PupilLeavingOffBusiness = PupilLeavingOffBusiness;
            this.LeavingReasonBusiness = LeavingReasonBusiness;
            this.VillageBusiness = VillageBusiness;
            this.OtherEthnicBusiness = OtherEthnicBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
        }

        #region Index
        [CacheFilter(Duration = 10)]
        [CompressFilter(Order = 1)]
        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            CheckPermissionForAction(ClassID, "ClassProfile");
            SetViewData(ClassID, true);
            ViewData[PupilProfileChildrenConstants.IS_NAVIGATE_FROM_OTHER_PAGE] = ClassID.HasValue && ClassID.Value > 0;
            GlobalInfo global = new GlobalInfo();
            Dictionary<string, bool> dicPermission = new Dictionary<string, bool>();
            dicPermission["PupilProfile"] = this.GetMenupermission("PupilProfile", global.UserAccountID, global.IsAdmin || global.IsSuperVisingDeptRole || global.IsSubSuperVisingDeptRole) > 0;
            dicPermission["MovementAcceptance"] = (UtilsBusiness.IsBGH(global.UserAccountID) || global.IsAdminSchoolRole || global.IsAdmin || global.IsSuperVisingDeptRole || global.IsSubSuperVisingDeptRole);
            ViewData[PupilProfileChildrenConstants.DICTIONARY_TABS_PERMISSION] = dicPermission;
            ViewData[PupilProfileChildrenConstants.Pageable] = Config.PageSize;
            CHECK_ENABLE_CREATE_DELETE = ENABLE_CREATE_DELETE(ClassID);
            ViewData[PupilProfileChildrenConstants.ENABLE_CREATE_DELETE] = CHECK_ENABLE_CREATE_DELETE;
            return PartialView();
        }

        public bool ENABLE_CREATE_DELETE(int? ClassID)
        {
            bool flat = false;
            GlobalInfo global = new GlobalInfo();
            if (global.IsCurrentYear && UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, ClassID.HasValue ? ClassID.Value : 0))
            {
                flat = true;
            }
            return flat;
        }

        public void SetViewData(int? ClassID, bool isFirst = false, int PupilProfileID = 0)
        {
            ViewData[PupilProfileChildrenConstants.VISIBLE_ORDER] = false;
            ViewData[PupilProfileChildrenConstants.ENABLE_PAGING] = true;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //Get view data here
            GlobalInfo global = new GlobalInfo();
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["CurrentClassID"] = ClassID;
            //IEnumerable<PupilProfileChildrenViewModel> lst = this._Search(SearchInfo, false);
            Paginate<PupilProfileChildrenViewModel> lst = this.SearchPaging(SearchInfo, !ClassID.HasValue || ClassID.Value <= 0, false, 1, "", isFirst);
            ViewData[PupilProfileChildrenConstants.LIST_PUPILPROFILE] = lst;
            ViewData[PupilProfileChildrenConstants.ENABLE_PAGING] = !ClassID.HasValue;
            ViewData[PupilProfileChildrenConstants.VISIBLE_ORDER] = ClassID.HasValue && lst.List.Count() > 0;
            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID);
            ViewData[PupilProfileChildrenConstants.IS_SHOW_AVATAR] = acaYear.IsShowAvatar.HasValue && acaYear.IsShowAvatar.Value;
            List<EducationLevel> lsEducationLevel = global.EducationLevels.ToList();

            int selectedEducationLevel = 0;
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                ClassProfile clsProfile = ClassProfileBusiness.Find(ClassID);
                if (clsProfile != null)
                {
                    ViewData[PupilProfileChildrenConstants.LISTCLASS] = new SelectList(getClassFromEducationLevel(clsProfile.EducationLevelID).ToList(), "ClassProfileID", "DisplayName", ClassID.Value);
                    selectedEducationLevel = clsProfile.EducationLevelID;
                }
                else
                    ViewData[PupilProfileChildrenConstants.LISTCLASS] = new SelectList(new string[] { });
            }
            else
                ViewData[PupilProfileChildrenConstants.LISTCLASS] = new SelectList(new string[] { });


            //-	cboEducationLevel: UserInfo.EducationLevels
            //if (lsEducationLevel != null)
            //{
            //    ViewData[PupilProfileChildrenConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            //}
            //else
            //{
            //    ViewData[PupilProfileChildrenConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
            //}

            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);
            if (checkCombineClass)
            {
                lsEducationLevel.Add(new EducationLevel { Resolution = SystemParamsInFile.Combine_Class_Name, EducationLevelID = SystemParamsInFile.Combine_Class_ID });
            }
            ViewData[PupilProfileChildrenConstants.LISTEDUCATIONLEVEL] = lsEducationLevel.Count > 0 ? new SelectList(lsEducationLevel, "EducationLevelID", "Resolution", selectedEducationLevel) : new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            ViewData[PupilProfileChildrenConstants.LIST_EDUCATION_LEVEL] = global.EducationLevels.ToList().Count > 0 ? new SelectList(global.EducationLevels.ToList(), "EducationLevelID", "Resolution", selectedEducationLevel) : new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            //-	cboClass: Giá trị mặc định là [Tất cả]
            //ViewData[PupilProfileChildrenConstants.LISTCLASS] = new SelectList(new string[] { });

            //-	cboSex: CommonList.GenreAndAll
            ViewData[PupilProfileChildrenConstants.LISTGENRE] = new SelectList(CommonList.GenreAndAll(), "key", "value");

            //-	cboStatus: CommonList.PupilStatus


            var listStatus = CommonList.PupilStatus();
            var StatusGraduated = listStatus.FirstOrDefault(c => c.key == SystemParamsInFile.PUPIL_STATUS_GRADUATED.ToString());
            if (StatusGraduated != null)
            {
                listStatus.Remove(StatusGraduated);
            }
            ViewData[PupilProfileChildrenConstants.LISTSTATUS] = new SelectList(listStatus, "key", "value");

            //create,edit load
            //-	cboPolicyTarget: PolicyTargetBusiness.Search()
            IQueryable<PolicyTarget> lsPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object>() { });
            ViewData[PupilProfileChildrenConstants.LISTPOLICYTARGET] = new SelectList(lsPolicyTarget.ToList(), "PolicyTargetID", "Resolution");
            ViewData[PupilProfileChildrenConstants.FirstStartDate] = acaYear.FirstSemesterStartDate.Value;
            //-	cboPreviousGraduationLevel: GraduationLevelBusiness.Search()

            if (global.AppliedLevel.Value == 2 || global.AppliedLevel.Value == 3)
            {
                ViewData[PupilProfileChildrenConstants.PERVIOUSGRADUATION_LEVEL1] = true;
                IQueryable<GraduationLevel> lsGraduationLevel = GraduationLevelBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", (global.AppliedLevel.Value - 1) } });
                ViewData[PupilProfileChildrenConstants.LISTPREVIOUSGRADUATIONLEVEL] = new SelectList(lsGraduationLevel.ToList(), "GraduationLevelID", "Resolution");
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.PERVIOUSGRADUATION_LEVEL1] = false;
                ViewData[PupilProfileChildrenConstants.LISTPREVIOUSGRADUATIONLEVEL] = new SelectList(new string[] { });
            }

            //-	cboEthnic: EthnicBusiness.Search()
            IQueryable<Ethnic> lsEthnic = EthnicBusiness.Search(new Dictionary<string, object>() { });
            List<Ethnic> lstEthnic = lsEthnic.ToList();
            lstEthnic = lstEthnic.OrderBy(o => o.EthnicName).ToList();
            ViewData[PupilProfileChildrenConstants.LISTETHNIC] = new SelectList(lstEthnic, "EthnicID", "EthnicName", 0);

            //-	cboEthnic: EthnicBusiness.Search()          
            List<OtherEthnic> lstOtherEthnic = new List<OtherEthnic>();

            if (lstEthnic.Count != 0 && PupilProfileID != 0)
            {
                int? ethnicIDOfPupil = PupilProfileBusiness.Find(PupilProfileID).EthnicID;
                IQueryable<OtherEthnic> queryOE = OtherEthnicBusiness.All.Where(x => x.EthnicID == ethnicIDOfPupil);
                lstOtherEthnic = queryOE.ToList();
            }

            ViewData[PupilProfileChildrenConstants.LISTOTHERETHNIC] = new SelectList(lstOtherEthnic, "OtherEthnicID", "OtherEthnicName", 0);

            //-	cbo Religion: ReligionBusiness.Search()
            IQueryable<Religion> lsReligion = ReligionBusiness.Search(new Dictionary<string, object>() { });
            List<Religion> lstReligion = lsReligion.ToList();
            lstReligion = lstReligion.OrderBy(o => o.Resolution).ToList();
            ViewData[PupilProfileChildrenConstants.LISTRELIGION] = new SelectList(lstReligion, "ReligionID", "Resolution");

            //o	Nếu UserInfo.AppliedLevel == APPLIED_LEVEL_PRIMARY:
            //cboForeignLanguageTraining, dtpYouthLeagueJoinedDate, dtpCommunistPartyMember,
            //chkIsCommunistPartyMember, chkYouthLeagueJoinedDate: Ẩn đi
            if (global.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                ViewData[PupilProfileChildrenConstants.APPLIED_LEVEL_PRIMARY] = true;
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.APPLIED_LEVEL_PRIMARY] = false;
            }

            //o	Nếu UserInfo.AppliedLevel == APPLIED_LEVEL_SECONDARY
            //dtpCommunistPartyMember, chkIsCommunistPartyMember: Ẩn đi
            if (global.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                ViewData[PupilProfileChildrenConstants.APPLIED_LEVEL_SECONDARY] = true;
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.APPLIED_LEVEL_SECONDARY] = false;
            }

            if (global.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                ViewData[PupilProfileChildrenConstants.APPLIED_LEVEL_TERTIARY] = true;
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.APPLIED_LEVEL_TERTIARY] = false;
            }

            //Nếu  CodeConfigBusiness.IsAutoGenCode(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE thì chkAutoCode được chọn
            if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL))
            {
                ViewData[PupilProfileChildrenConstants.AUTO_GEN_CODE_CHECK] = true;
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.AUTO_GEN_CODE_CHECK] = false;
            }

            //-	Nếu CodeConfigBusiness.IsSchoolModify(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE
            if (CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL))
            {
                ViewData[PupilProfileChildrenConstants.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = true;
                ViewData[PupilProfileChildrenConstants.AUTO_GEN_CODE_CHECK_AND_DISABLE] = false;
                ViewData[PupilProfileChildrenConstants.IS_SCHOOL_MODIFIED] = true;
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = false;
                ViewData[PupilProfileChildrenConstants.AUTO_GEN_CODE_CHECK_AND_DISABLE] = true;
                ViewData[PupilProfileChildrenConstants.IS_SCHOOL_MODIFIED] = false;
            }

            ViewData[PupilProfileChildrenConstants.LISTBLOODTYPE] = new SelectList(CommonList.BloodType(), "key", "value");

            //
            ViewData[PupilProfileChildrenConstants.LISTFOREIGNLANGUAGETRAINING] = new SelectList(CommonList.ForeignLanguageTraining(), "key", "value");

            //
            IQueryable<Area> lsArea = AreaBusiness.Search(new Dictionary<string, object>() { });
            ViewData[PupilProfileChildrenConstants.LISTAREA] = new SelectList(lsArea.ToList(), "AreaID", "AreaName");

            //
            IQueryable<PriorityType> lsPriorityType = PriorityTypeBusiness.Search(new Dictionary<string, object>() { });
            ViewData[PupilProfileChildrenConstants.LISTPRIORITYTYPE] = new SelectList(lsPriorityType.ToList(), "PriorityTypeID", "Resolution");

            //
            IQueryable<Province> lsProvince = ProvinceBusiness.Search(new Dictionary<string, object>() { });
            List<Province> lstProvince = lsProvince.ToList();
            lstProvince = lstProvince.OrderBy(o => o.ProvinceName).ToList();
            ViewData[PupilProfileChildrenConstants.LISTPROVINCE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");

            //Loại chế độ chính sách
            IQueryable<PolicyRegime> lsPolicyRegime = PolicyRegimeBusiness.Search(new Dictionary<string, object>() { });
            ViewData[PupilProfileChildrenConstants.LISTPOLICYREGIME] = new SelectList(lsPolicyRegime, "PolicyRegimeID", "Resolution");
            //district dua vao provide
            ViewData[PupilProfileChildrenConstants.LISTDISTRICT] = new SelectList(new string[] { });

            //commune dua vao district
            ViewData[PupilProfileChildrenConstants.LISTCOMMUNE] = new SelectList(new string[] { });

            //
            IQueryable<DisabledType> lsDisabledType = DisabledTypeBusiness.Search(new Dictionary<string, object>() { });
            ViewData[PupilProfileChildrenConstants.LISTDISABLEDTYPE] = new SelectList(lsDisabledType.ToList(), "DisabledTypeID", "Resolution");

            //
            IQueryable<FamilyType> lsFamilyType = FamilyTypeBusiness.Search(new Dictionary<string, object>() { }).Where(o => o.IsActive == true);
            ViewData[PupilProfileChildrenConstants.LISTFAMILYTYPE] = new SelectList(lsFamilyType.ToList(), "FamilyTypeID", "Resolution");

            //LISTENROLMENTTYPE
            ViewData[PupilProfileChildrenConstants.LISTENROLMENTTYPE] = new SelectList(CommonList.EnrolmentType(), "key", "value");

            //LISTCLASSTYPE
            ViewData[PupilProfileChildrenConstants.LISTCLASSTYPE] = new SelectList(new string[] { });

            //ListHaBitGroup
            IDictionary<string, object> HabitGroupSearchInfo = new Dictionary<string, object>();
            HabitGroupSearchInfo["IsActive"] = true;
            List<HabitGroup> lstHabitGroup = this.HabitGroupBusiness.Search(HabitGroupSearchInfo).ToList();
            ViewData[PupilProfileChildrenConstants.LIST_HABITGROUP] = lstHabitGroup;

            List<HabitDetail> lstHabitDetail = this.HabitDetailBusiness.Search(HabitGroupSearchInfo).ToList();
            ViewData[PupilProfileChildrenConstants.LIST_HABITDETAIL] = lstHabitDetail;

            List<TypeOfDish> lstTypeOfDish = this.TypeOfDishBusiness.Search(HabitGroupSearchInfo).ToList();
            ViewData[PupilProfileChildrenConstants.LIST_TYPEOFDISH] = lstTypeOfDish;

            ViewData[PupilProfileChildrenConstants.LIST_HABITDETAILOFPUPIL] = null;
            ViewData[PupilProfileChildrenConstants.LIST_TYPEOFDISHOFPUPIL] = null;
            ViewData[PupilProfileChildrenConstants.LISTENROLMENTTYPE] = new SelectList(CommonList.EnrolmentType(), "key", "value");
            ViewData[PupilProfileChildrenConstants.LISTENROLMENTDATE] = DateTime.Now;
        }
        #endregion

        #region Search
        [CacheFilter(Duration = 60)]
        [CompressFilter(Order = 1)]
        public PartialViewResult Search(SearchViewModel frm, int page = 1, string orderBy = "")
        {

            Utils.Utils.TrimObject(frm);
            Session[PupilProfileChildrenConstants.SEARCHFORM] = frm;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID);
            ViewData[PupilProfileChildrenConstants.IS_SHOW_AVATAR] = acaYear.IsShowAvatar.HasValue && acaYear.IsShowAvatar.Value;

            //add search info
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["CurrentClassID"] = frm.Class;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["ProfileStatus"] = frm.Status;
            SearchInfo["EthnicID"] = frm.Ethnic;

            Paginate<PupilProfileChildrenViewModel> lst = this.SearchPaging(SearchInfo, string.IsNullOrEmpty(frm.Class), false, page, orderBy, true);
            ViewData[PupilProfileChildrenConstants.LIST_PUPILPROFILE] = lst;
            //Session["lstPupilIDBySearch"] = lst.List.Select(x => x.PupilProfileID).ToList();
            ViewData[PupilProfileChildrenConstants.ENABLE_CREATE_DELETE] = CHECK_ENABLE_CREATE_DELETE;

            //Get view data here
            if (frm.EducationLevel.HasValue && !string.IsNullOrEmpty(frm.Class))
            {
                if (frm.EducationLevel.Value != SystemParamsInFile.Combine_Class_ID)
                {
                    ViewData[PupilProfileChildrenConstants.VISIBLE_ORDER] = lst.List.Count() > 0;
                    ViewData[PupilProfileChildrenConstants.ENABLE_PAGING] = false;
                }
                else
                {
                    ViewData[PupilProfileChildrenConstants.VISIBLE_ORDER] = false;
                    ViewData[PupilProfileChildrenConstants.ENABLE_PAGING] = true;
                }
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.VISIBLE_ORDER] = false;
                ViewData[PupilProfileChildrenConstants.ENABLE_PAGING] = true;
            }
            //ViewData[PupilProfileChildrenConstants.ENABLE_CREATE_DELETE] = global.IsCurrentYear && UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, ClassID.HasValue ? ClassID.Value : 0);
            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PupilProfileChildrenViewModel> _Search(IDictionary<string, object> SearchInfo, bool sort)
        {
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            string no = Res.Get("Common_Label_NoChoice");
            List<int> lstClassID = getClassFromEducationLevel(0).Select(u => u.ClassProfileID).ToList();
            string No = Res.Get("Common_Label_No");
            string Yes = Res.Get("Common_Label_Yes");
            string divtool = string.Empty;
            List<int> listHidden = new List<int>();
            GlobalInfo global = new GlobalInfo();
            Dictionary<int, bool> dicHeadTeacherPermission = UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, lstClassID);
            Dictionary<int, bool> dicSubjectTeacherPermission = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, lstClassID, 0);
            Dictionary<int, bool> dicSupervisiorAssignmentPermission = UtilsBusiness.HasOverseeingTeacherPermission(global.UserAccountID, lstClassID);
            IQueryable<PupilProfileBO> query = this.PupilProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);

            IQueryable<PupilProfileChildrenViewModel> qResult = from pp in query
                                                                join cp in ClassProfileBusiness.All.Where(s => s.AcademicYearID == _globalInfo.AcademicYearID) on pp.CurrentClassID equals cp.ClassProfileID
                                                                join edu in EducationLevelBusiness.All.Where(s => s.IsActive) on cp.EducationLevelID equals edu.EducationLevelID
                                                                join are in AreaBusiness.All.Where(s => s.IsActive == true) on pp.AreaID equals are.AreaID
                                                                join province in ProvinceBusiness.All.Where(p => p.IsActive == true) on pp.ProvinceID equals province.ProvinceID
                                                                join d in DistrictBusiness.All.Where(d => d.IsActive == true) on pp.DistrictID equals d.DistrictID into ppd
                                                                from district in ppd.DefaultIfEmpty()
                                                                join cc in CommuneBusiness.All.Where(s => s.IsActive == true) on pp.CommuneID equals cc.CommuneID into ppcc
                                                                from commune in ppcc.DefaultIfEmpty()
                                                                join e in EthnicBusiness.All.Where(s => s.IsActive == true) on pp.EthnicID equals e.EthnicID into ppe
                                                                from ethnic in ppe.DefaultIfEmpty()
                                                                join oe in OtherEthnicBusiness.All.Where(s => s.IsActive == true) on pp.OtherEthnicID equals oe.OtherEthnicID into oppe
                                                                from otherethnic in oppe.DefaultIfEmpty()
                                                                join re in ReligionBusiness.All.Where(s => s.IsActive == true) on pp.ReligionID equals re.ReligionID into ppre
                                                                from religion in ppre.DefaultIfEmpty()
                                                                join pt in PolicyTargetBusiness.All.Where(s => s.IsActive) on pp.PolicyTargetID equals pt.PolicyTargetID into pppt
                                                                from policy in pppt.DefaultIfEmpty()
                                                                join fam in FamilyTypeBusiness.All.Where(s => s.IsActive == true) on pp.FamilyTypeID equals fam.FamilyTypeID into ppfam
                                                                from family in ppfam.DefaultIfEmpty()
                                                                join pri in PriorityTypeBusiness.All.Where(s => s.IsActive == true) on pp.PriorityTypeID equals pri.PriorityTypeID into pppri
                                                                from priotity in pppri.DefaultIfEmpty()
                                                                join pr in PolicyRegimeBusiness.All.Where(s => s.IsActive) on pp.PolicyRegimeID equals pr.PolicyRegimeID into pppr
                                                                from policyRegime in pppr.DefaultIfEmpty()
                                                                join dis in DisabledTypeBusiness.All.Where(s => s.IsActive) on pp.DisabledTypeID equals dis.DisabledTypeID into ppdis
                                                                from disabled in ppdis.DefaultIfEmpty()

                                                                select new PupilProfileChildrenViewModel
                                                                {
                                                                    EducationLevel = pp.EducationLevelID,
                                                                    PupilProfileID = pp.PupilProfileID,
                                                                    CurrentClassID = pp.CurrentClassID,
                                                                    CurrentSchoolID = pp.CurrentSchoolID,
                                                                    CurrentAcademicYearID = pp.CurrentAcademicYearID,
                                                                    AreaID = pp.AreaID,
                                                                    ProvinceID = pp.ProvinceID,
                                                                    DistrictID = pp.DistrictID,
                                                                    CommuneID = pp.CommuneID,
                                                                    EthnicID = pp.EthnicID,
                                                                    OtherEthnicID = pp.OtherEthnicID,
                                                                    ReligionID = pp.ReligionID,
                                                                    PolicyTargetID = pp.PolicyTargetID,
                                                                    FamilyTypeID = pp.FamilyTypeID,
                                                                    PriorityTypeID = pp.PriorityTypeID,
                                                                    PolicyRegimeID = pp.PolicyRegimeID,
                                                                    PreviousGraduationLevelID = pp.PreviousGraduationLevelID,
                                                                    PupilCode = pp.PupilCode,
                                                                    Name = pp.Name,
                                                                    FullName = pp.FullName,
                                                                    Genre = pp.Genre,
                                                                    BirthDate = pp.BirthDate,
                                                                    BirthPlace = pp.BirthPlace,
                                                                    HomeTown = pp.HomeTown,
                                                                    Telephone = pp.Telephone,
                                                                    Mobile = pp.Mobile,
                                                                    Email = pp.Email,
                                                                    TempResidentalAddress = pp.TempResidentalAddress,
                                                                    PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                                                    IsResidentIn = pp.IsResidentIn.Value,
                                                                    IsDisabled = pp.IsDisabled,
                                                                    DisabledTypeID = pp.DisabledTypeID,
                                                                    DisabledSeverity = pp.DisabledSeverity,
                                                                    BloodType = pp.BloodType,
                                                                    EnrolmentMark = pp.EnrolmentMark,
                                                                    EnrolmentDate = pp.EnrolmentDate,
                                                                    ForeignLanguageTraining = pp.ForeignLanguageTraining,
                                                                    IsYoungPioneerMember = pp.IsYoungPioneerMember,
                                                                    YoungPioneerJoinedDate = pp.YoungPioneerJoinedDate,
                                                                    YoungPioneerJoinedPlace = pp.YoungPioneerJoinedPlace,
                                                                    IsYouthLeageMember = pp.IsYouthLeageMember,
                                                                    YouthLeagueJoinedDate = pp.YouthLeagueJoinedDate,
                                                                    YouthLeagueJoinedPlace = pp.YouthLeagueJoinedPlace,
                                                                    IsCommunistPartyMember = pp.IsCommunistPartyMember,
                                                                    CommunistPartyJoinedDate = pp.CommunistPartyJoinedDate,
                                                                    CommunistPartyJoinedPlace = pp.CommunistPartyJoinedPlace,
                                                                    FatherFullName = pp.FatherFullName,
                                                                    FatherBirthDate = pp.FatherBirthDate,
                                                                    FatherJob = pp.FatherJob,
                                                                    FatherMobile = pp.FatherMobile,
                                                                    MotherFullName = pp.MotherFullName,
                                                                    MotherBirthDate = pp.MotherBirthDate,
                                                                    MotherJob = pp.MotherJob,
                                                                    MotherMobile = pp.MotherMobile,
                                                                    SponsorFullName = pp.SponsorFullName,
                                                                    SponsorBirthDate = pp.SponsorBirthDate,
                                                                    SponsorJob = pp.SponsorJob,
                                                                    SponsorMobile = pp.SponsorMobile,
                                                                    ProfileStatus = pp.ProfileStatus,
                                                                    //CreatedDate = pp.CreatedDate,								
                                                                    IsActive = pp.IsActive,
                                                                    //ModifiedDate = pp.ModifiedDate,		

                                                                    HeightAtBirth = pp.HeightAtBirth,
                                                                    WeightAtBirth = pp.WeightAtBirth,
                                                                    FatherEmail = pp.FatherEmail,
                                                                    MotherEmail = pp.MotherEmail,
                                                                    SponsorEmail = pp.SponsorEmail,
                                                                    ChildOrder = pp.ChildOrder,
                                                                    NumberOfChild = pp.NumberOfChild,
                                                                    EatingHabit = pp.EatingHabit,
                                                                    ReactionTrainingHabit = pp.ReactionTrainingHabit,
                                                                    AttitudeInStrangeScene = pp.AttitudeInStrangeScene,
                                                                    OtherHabit = pp.OtherHabit,
                                                                    FavoriteToy = pp.FavoriteToy,
                                                                    EvaluationConfigID = pp.EvaluationConfigID,
                                                                    GenreName = (pp.Genre == SystemParamsInFile.GENRE_MALE) ? male : female,
                                                                    OrderInClass = pp.OrderInClass,
                                                                    EnrolmentType = pp.EnrolmentType,
                                                                    IsFatherSMS = (pp.IsFatherSMS.HasValue && pp.IsFatherSMS.Value) ? true : false,
                                                                    IsMotherSMS = (pp.IsMotherSMS.HasValue && pp.IsMotherSMS.Value) ? true : false,
                                                                    IsSponsorSMS = (pp.IsSponsorSMS.HasValue && pp.IsSponsorSMS.Value) ? true : false,
                                                                    AreaName = are.AreaName,
                                                                    ClassName = cp.DisplayName,
                                                                    CommuneName = commune.CommuneName,
                                                                    DisabledTypeName = disabled.Resolution,
                                                                    DistrictName = district.DistrictName,
                                                                    EducationLevelName = edu.Resolution,
                                                                    EthnicName = ethnic.EthnicName,
                                                                    OtherEthnicName = otherethnic.OtherEthnicName,
                                                                    PolicyRegimeName = policyRegime.Resolution,
                                                                    FamilyTypeName = family.Resolution,
                                                                    EthnicCode = ethnic.EthnicCode,
                                                                    OtherEthnicCode = otherethnic.OtherEthnicCode,
                                                                    PolicyTargetName = policy.Resolution,
                                                                    PriorityTypeName = priotity.Resolution,
                                                                    ProvinceName = province.ProvinceName,
                                                                    ReligionName = religion.Resolution,
                                                                    MinorityFather = pp.MinorityFather.HasValue ? pp.MinorityFather.Value : false,
                                                                    MinorityMother = pp.MinorityMother.HasValue ? pp.MinorityMother.Value : false,

                                                                };
            //IQueryable<PupilProfileChildrenViewModel> lst = query.Select(o => new PupilProfileChildrenViewModel
            //{
            //    EducationLevel = o.EducationLevelID,
            //    PupilProfileID = o.PupilProfileID,
            //    CurrentClassID = o.CurrentClassID,
            //    CurrentSchoolID = o.CurrentSchoolID,
            //    CurrentAcademicYearID = o.CurrentAcademicYearID,
            //    AreaID = o.AreaID,
            //    ProvinceID = o.ProvinceID,
            //    DistrictID = o.DistrictID,
            //    CommuneID = o.CommuneID,
            //    EthnicID = o.EthnicID,
            //    ReligionID = o.ReligionID,
            //    PolicyTargetID = o.PolicyTargetID,
            //    FamilyTypeID = o.FamilyTypeID,
            //    PriorityTypeID = o.PriorityTypeID,
            //    PolicyRegimeID = o.PolicyRegimeID,
            //    PreviousGraduationLevelID = o.PreviousGraduationLevelID,
            //    PupilCode = o.PupilCode,
            //    Name = o.Name,
            //    FullName = o.FullName,
            //    Genre = o.Genre,
            //    BirthDate = o.BirthDate,
            //    BirthPlace = o.BirthPlace,
            //    HomeTown = o.HomeTown,
            //    Telephone = o.Telephone,
            //    Mobile = o.Mobile,
            //    Email = o.Email,
            //    TempResidentalAddress = o.TempResidentalAddress,
            //    PermanentResidentalAddress = o.PermanentResidentalAddress,
            //    IsResidentIn = o.IsResidentIn.Value,
            //    IsDisabled = o.IsDisabled,
            //    DisabledTypeID = o.DisabledTypeID,
            //    DisabledSeverity = o.DisabledSeverity,
            //    BloodType = o.BloodType,
            //    EnrolmentMark = o.EnrolmentMark,
            //    EnrolmentDate = o.EnrolmentDate,
            //    ForeignLanguageTraining = o.ForeignLanguageTraining,
            //    IsYoungPioneerMember = o.IsYoungPioneerMember,
            //    YoungPioneerJoinedDate = o.YoungPioneerJoinedDate,
            //    YoungPioneerJoinedPlace = o.YoungPioneerJoinedPlace,
            //    IsYouthLeageMember = o.IsYouthLeageMember,
            //    YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
            //    YouthLeagueJoinedPlace = o.YouthLeagueJoinedPlace,
            //    IsCommunistPartyMember = o.IsCommunistPartyMember,
            //    CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
            //    CommunistPartyJoinedPlace = o.CommunistPartyJoinedPlace,
            //    FatherFullName = o.FatherFullName,
            //    FatherBirthDate = o.FatherBirthDate,
            //    FatherJob = o.FatherJob,
            //    FatherMobile = o.FatherMobile,
            //    MotherFullName = o.MotherFullName,
            //    MotherBirthDate = o.MotherBirthDate,
            //    MotherJob = o.MotherJob,
            //    MotherMobile = o.MotherMobile,
            //    SponsorFullName = o.SponsorFullName,
            //    SponsorBirthDate = o.SponsorBirthDate,
            //    SponsorJob = o.SponsorJob,
            //    SponsorMobile = o.SponsorMobile,
            //    ProfileStatus = o.ProfileStatus,
            //    //CreatedDate = o.CreatedDate,								
            //    IsActive = o.IsActive,
            //    //ModifiedDate = o.ModifiedDate,		

            //    HeightAtBirth = o.HeightAtBirth,
            //    WeightAtBirth = o.WeightAtBirth,
            //    FatherEmail = o.FatherEmail,
            //    MotherEmail = o.MotherEmail,
            //    SponsorEmail = o.SponsorEmail,
            //    ChildOrder = o.ChildOrder,
            //    NumberOfChild = o.NumberOfChild,
            //    EatingHabit = o.EatingHabit,
            //    ReactionTrainingHabit = o.ReactionTrainingHabit,
            //    AttitudeInStrangeScene = o.AttitudeInStrangeScene,
            //    OtherHabit = o.OtherHabit,
            //    FavoriteToy = o.FavoriteToy,
            //    EvaluationConfigID = o.EvaluationConfigID,

            //    GenreName = (o.Genre == (int)SystemParamsInFile.GENRE_MALE) ? male : female,
            //    OrderInClass = o.OrderInClass,
            //    EnrolmentType = o.EnrolmentType,
            //    IsFatherSMS = (o.IsFatherSMS.HasValue && o.IsFatherSMS.Value) ? true : false,
            //    IsMotherSMS = (o.IsMotherSMS.HasValue && o.IsMotherSMS.Value) ? true : false,
            //    IsSponsorSMS = (o.IsSponsorSMS.HasValue && o.IsSponsorSMS.Value) ? true : false


            //});
            if (sort)
            {
                qResult = qResult.OrderBy(o => o.FullName);
            }


            List<PupilProfileChildrenViewModel> lsPupilProfileViewModel = qResult.ToList();

            foreach (var item in lsPupilProfileViewModel)
            {
                //ClassProfile cp = ClassProfileBusiness.Find(item.CurrentClassID);
                //if (cp != null)
                //{
                //    item.ClassName = cp.DisplayName;
                //}
                //else
                //{
                //    item.ClassName = "";
                //}

                switch (item.ProfileStatus)
                {
                    case SystemParamsInFile.PUPIL_STATUS_STUDYING:
                        item.StatusName = Res.Get("Common_Label_PupilStatusStuding");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_GRADUATED:
                        item.StatusName = Res.Get("Common_Label_PupilStatusGraduated");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMoved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF:
                        item.StatusName = Res.Get("Common_Label_PupilStatusLeaved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMovedToOtherClass");
                        break;
                }
                //if (item.EducationLevel != 0)
                //{
                //    item.EducationLevelName = EducationLevelBusiness.Find(item.EducationLevel).Resolution;
                //}
                //else
                //{
                //    item.AreaName = no;
                //}
                //if (item.AreaID.HasValue)
                //{
                //    item.AreaName = AreaBusiness.Find(item.AreaID).AreaName;
                //}
                //else
                //{
                //    item.AreaName = no;
                //}

                //if (item.ProvinceID != 0)
                //{
                //    item.ProvinceName = ProvinceBusiness.Find(item.ProvinceID).ProvinceName;
                //}
                //else
                //{
                //    item.ProvinceName = no;
                //}

                //if (item.DistrictID.HasValue)
                //{
                //    item.DistrictName = DistrictBusiness.Find(item.DistrictID).DistrictName;
                //}
                //else
                //{
                //    item.DistrictName = no;
                //}

                ////commune
                //if (item.CommuneID.HasValue)
                //{
                //    item.CommuneName = CommuneBusiness.Find(item.CommuneID).CommuneName;
                //}
                //else
                //{
                //    item.CommuneName = no;
                //}

                ////ethnic
                //if (item.EthnicID.HasValue)
                //{
                //    item.EthnicName = EthnicBusiness.Find(item.EthnicID).EthnicName;
                //}
                //else
                //{
                //    item.EthnicName = no;
                //}

                ////religion
                //if (item.ReligionID.HasValue)
                //{
                //    item.ReligionName = ReligionBusiness.Find(item.ReligionID).Resolution;
                //}
                //else
                //{
                //    item.ReligionName = no;
                //}

                ////PolicyTargetID
                //if (item.PolicyTargetID.HasValue)
                //{
                //    item.PolicyTargetName = PolicyTargetBusiness.Find(item.PolicyTargetID).Resolution;
                //}
                //else
                //{
                //    item.PolicyTargetName = no;
                //}

                ////FamilyTypeID
                //if (item.FamilyTypeID.HasValue)
                //{
                //    item.FamilyTypeName = FamilyTypeBusiness.Find(item.FamilyTypeID).Resolution;
                //}
                //else
                //{
                //    item.FamilyTypeName = no;
                //}

                ////PriorityTypeID
                //if (item.PriorityTypeID.HasValue)
                //{
                //    item.PriorityTypeName = PriorityTypeBusiness.Find(item.PriorityTypeID).Resolution;
                //}
                //else
                //{
                //    item.PriorityTypeName = no;
                //}

                ////Che do chinh sach
                //if (item.PolicyRegimeID.HasValue)
                //{
                //    item.PolicyRegimeName = PolicyRegimeBusiness.Find(item.PolicyRegimeID).Resolution;
                //}
                //else
                //{
                //    item.PolicyRegimeName = no;
                //}
                //////PreviousGraduationLevelID
                ////if (item.PreviousGraduationLevelID.HasValue)
                ////    {
                ////    item.PreviousGraduationLevelName = GraduationLevelBusiness.Find(item.PreviousGraduationLevelID).Resolution;
                ////    }
                ////    else
                ////    {
                ////    item.PreviousGraduationLevelName = no;
                ////    }

                ////DisabledTypeID
                //if (item.DisabledTypeID.HasValue)
                //{
                //    item.DisabledTypeName = DisabledTypeBusiness.Find(item.DisabledTypeID).Resolution;
                //}
                //else
                //{
                //    item.DisabledTypeName = no;
                //}
                if (item.BloodType.HasValue)
                {
                    //BloodType
                    switch (item.BloodType.Value)
                    {
                        case SystemParamsInFile.BLOOD_TYPE_A:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeA");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_B:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeB");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_AB:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeAB");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_O:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeO");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_OTHER:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeOther");
                            break;

                        default:
                            item.BloodTypeName = no;
                            break;
                    }
                }
                else
                {
                    item.BloodTypeName = no;
                }

                if (item.EnrolmentType.HasValue)
                {
                    switch (item.EnrolmentType.Value)
                    {
                        case SystemParamsInFile.ENROLMENT_TYPE_PASSED_EXAMINATION:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypePassExamination");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeMoveFromOtherSchool");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_SELECTED:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeSelected");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_OTHER:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeOther");
                            break;

                        default:
                            item.EnrolmentTypeName = no;
                            break;
                    }
                }
                else
                {
                    item.EnrolmentTypeName = no;
                }
                if (item.IsResidentIn == true)
                {
                    item.ResidentInName = Yes;
                    item.CheckIsResidentIn = true;
                }
                else
                {
                    item.ResidentInName = No;
                    item.CheckIsResidentIn = false;
                }
                if (item.IsDisabled == true)
                {
                    item.IsDisabledName = Yes;
                    item.CheckIsDisabled = true;
                }
                else
                {
                    item.IsDisabledName = No;
                    item.CheckIsDisabled = false;
                }
            }

            return lsPupilProfileViewModel;
        }

        private Paginate<PupilProfileChildrenViewModel> SearchPaging(IDictionary<string, object> SearchInfo, bool allowPaging, bool sort, int page = 1, string orderBy = "", bool isFirst = false)
        {
            GlobalInfo global = new GlobalInfo();
            string No = Res.Get("Common_Label_No");
            string Yes = Res.Get("Common_Label_Yes");
            //Tunning
            List<ClassProfile> lstClass = getClassFromEducationLevel(0).ToList();
            //List<int> lstClassID = getClassFromEducationLevel(0).Select(u => u.ClassProfileID).ToList();
            List<int> lstClassID = (lstClass == null) ? new List<int>() : lstClass.Select(c => c.ClassProfileID).ToList();
            if (!global.IsAdminSchoolRole)
            {
                SearchInfo["ListNullableClassID"] = lstClassID;
            }

            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;

            Paginate<PupilProfileChildrenViewModel> Paging = null;

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            string no = Res.Get("Common_Label_NoChoice");

            IQueryable<PupilProfileBO> lstPupilProfileBO = this.PupilProfileBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);

            IQueryable<PupilProfileChildrenViewModel> lsPupilProfileViewModel = lstPupilProfileBO.Select(o => new PupilProfileChildrenViewModel
                                                                        {
                                                                            EducationLevel = o.EducationLevelID,
                                                                            PupilProfileID = o.PupilProfileID,
                                                                            CurrentClassID = o.CurrentClassID,
                                                                            ClassName = o.CurrentClassName,
                                                                            CurrentSchoolID = o.CurrentSchoolID,
                                                                            CurrentAcademicYearID = o.CurrentAcademicYearID,
                                                                            AreaID = o.AreaID,
                                                                            AreaName = o.AreaName,
                                                                            ProvinceID = o.ProvinceID,
                                                                            DistrictID = o.DistrictID,
                                                                            CommuneID = o.CommuneID,
                                                                            EthnicID = o.EthnicID,
                                                                            OtherEthnicID = o.OtherEthnicID,
                                                                            VillageID = o.VillageID,
                                                                            ReligionID = o.ReligionID,
                                                                            PolicyTargetID = o.PolicyTargetID,
                                                                            FamilyTypeID = o.FamilyTypeID,
                                                                            PriorityTypeID = o.PriorityTypeID,
                                                                            PolicyRegimeID = o.PolicyRegimeID,
                                                                            PreviousGraduationLevelID = o.PreviousGraduationLevelID,
                                                                            ProvinceName = o.ProvinceName,
                                                                            DistrictName = o.DistrictName,
                                                                            CommuneName = o.CommuneName,
                                                                            VillageName = o.VillageName,
                                                                            EthnicName = o.EthnicName,
                                                                            ReligionName = o.ReligionName,
                                                                            PolicyTargetName = o.PolicyTargetName,
                                                                            FamilyTypeName = o.FamilyName,
                                                                            PriorityTypeName = o.PriorityTypeName,
                                                                            PupilCode = o.PupilCode,
                                                                            Name = o.Name,
                                                                            FullName = o.FullName,
                                                                            Genre = o.Genre,
                                                                            BirthDate = o.BirthDate,
                                                                            BirthPlace = o.BirthPlace,
                                                                            HomeTown = o.HomeTown,
                                                                            Telephone = o.Telephone,
                                                                            Mobile = o.Mobile,
                                                                            Email = o.Email,
                                                                            TempResidentalAddress = o.TempResidentalAddress,
                                                                            PermanentResidentalAddress = o.PermanentResidentalAddress,
                                                                            IsResidentIn = o.IsResidentIn.Value,
                                                                            IsDisabled = o.IsDisabled,
                                                                            DisabledTypeID = o.DisabledTypeID,
                                                                            DisabledSeverity = o.DisabledSeverity,
                                                                            BloodType = o.BloodType,
                                                                            EnrolmentMark = o.EnrolmentMark,
                                                                            EnrolmentDate = o.EnrolmentDate,
                                                                            ForeignLanguageTraining = o.ForeignLanguageTraining,
                                                                            IsYoungPioneerMember = o.IsYoungPioneerMember,
                                                                            YoungPioneerJoinedDate = o.YoungPioneerJoinedDate,
                                                                            YoungPioneerJoinedPlace = o.YoungPioneerJoinedPlace,
                                                                            IsYouthLeageMember = o.IsYouthLeageMember,
                                                                            YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
                                                                            YouthLeagueJoinedPlace = o.YouthLeagueJoinedPlace,
                                                                            IsCommunistPartyMember = o.IsCommunistPartyMember,
                                                                            CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
                                                                            CommunistPartyJoinedPlace = o.CommunistPartyJoinedPlace,
                                                                            FatherFullName = o.FatherFullName,
                                                                            FatherBirthDate = o.FatherBirthDate,
                                                                            FatherJob = o.FatherJob,
                                                                            FatherMobile = o.FatherMobile,
                                                                            MotherFullName = o.MotherFullName,
                                                                            MotherBirthDate = o.MotherBirthDate,
                                                                            MotherJob = o.MotherJob,
                                                                            MotherMobile = o.MotherMobile,
                                                                            StorageNumber = o.StorageNumber,
                                                                            SponsorFullName = o.SponsorFullName,
                                                                            SponsorBirthDate = o.SponsorBirthDate,
                                                                            SponsorJob = o.SponsorJob,
                                                                            SponsorMobile = o.SponsorMobile,
                                                                            ProfileStatus = o.ProfileStatus,
                                                                            //CreatedDate = o.CreatedDate,								
                                                                            IsActive = o.IsActive,
                                                                            //ModifiedDate = o.ModifiedDate,		

                                                                            HeightAtBirth = o.HeightAtBirth,
                                                                            WeightAtBirth = o.WeightAtBirth,
                                                                            FatherEmail = o.FatherEmail,
                                                                            MotherEmail = o.MotherEmail,
                                                                            SponsorEmail = o.SponsorEmail,
                                                                            ChildOrder = o.ChildOrder,
                                                                            NumberOfChild = o.NumberOfChild,
                                                                            EatingHabit = o.EatingHabit,
                                                                            ReactionTrainingHabit = o.ReactionTrainingHabit,
                                                                            AttitudeInStrangeScene = o.AttitudeInStrangeScene,
                                                                            OtherHabit = o.OtherHabit,
                                                                            FavoriteToy = o.FavoriteToy,
                                                                            EvaluationConfigID = o.EvaluationConfigID,

                                                                            GenreName = (o.Genre == SystemParamsInFile.GENRE_MALE) ? male : female,
                                                                            OrderInClass = o.OrderInClass,
                                                                            EnrolmentType = o.EnrolmentType,
                                                                            IsFatherSMS = (o.IsFatherSMS.HasValue && o.IsFatherSMS.Value) ? true : false,
                                                                            IsMotherSMS = (o.IsMotherSMS.HasValue && o.IsMotherSMS.Value) ? true : false,
                                                                            IsSponsorSMS = (o.IsSponsorSMS.HasValue && o.IsSponsorSMS.Value) ? true : false,
                                                                            ClassOrderNumber = o.ClassOrderNumber,
                                                                            DisabledTypeName = o.DisabledTypeName,
                                                                            FavouriteTypeOfDish = o.FavouriteTypeOfDish,
                                                                            HateTypeOfDish = o.HateTypeOfDish,
                                                                            AppliedLevel = _globalInfo.AppliedLevel,
                                                                            MinorityFather = (o.MinorityFather.HasValue && o.MinorityFather.Value),
                                                                            MinorityMother = (o.MinorityMother.HasValue && o.MinorityMother.Value),
                                                                        });

            object temClassID = SearchInfo.ContainsKey("CurrentClassID") ? (object)SearchInfo["CurrentClassID"] : null;
            string ClassID = temClassID != null ? temClassID.ToString() : ""; //SMAS.Business.Common.Utils.GetInt(SearchInfo, "CurrentClassID").ToString();

            if (string.IsNullOrEmpty(orderBy))
            {
                if (sort)
                    lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                else
                {
                    if (!string.IsNullOrEmpty(ClassID))
                        lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(o => o.FullName);
                    else
                        lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.EducationLevel).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                }
            }
            else
            {
                int indexMinus = orderBy.LastIndexOf('-');
                string orderByName = indexMinus < 0 ? "" : orderBy.Substring(0, indexMinus);
                string orderByPriority = indexMinus < 0 ? "" : orderBy.Substring(indexMinus + 1);

                switch (orderByName)
                {
                    case "FullName":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    case "PupilCode":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    case "BirthDate":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    case "ClassName":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    case "StatusName":
                        lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                    default:
                        lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                        break;
                }
            }

            Paging = new Paginate<PupilProfileChildrenViewModel>(lsPupilProfileViewModel);
            Paging.page = page;

            if (!allowPaging)
                Paging.size = lsPupilProfileViewModel.Count();

            Paging.paginate();

            //Thỏa theo nguyện vọng muốn order theo tên tiếng việt trong khi chưa tìm ra cách order trực tiếp trong db.
            if (!allowPaging && !string.IsNullOrEmpty(ClassID))
            {
                // Sap xep lai cho dung thu tu chon tren grid
                if (!string.IsNullOrEmpty(orderBy))
                {
                    lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(u => u.Name).ThenBy(u => u.FullName);
                    Paging.List = Paging.List.OrderBy(u => u.Name).ThenBy(u => u.FullName).ToList();
                    int indexMinus = orderBy.LastIndexOf('-');
                    string orderByName = indexMinus < 0 ? "" : orderBy.Substring(0, indexMinus);
                    string orderByPriority = indexMinus < 0 ? "" : orderBy.Substring(indexMinus + 1);

                    switch (orderByName)
                    {
                        case "FullName":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.Name).ThenBy(o => o.FullName);
                            lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        case "PupilCode":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.PupilCode).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        case "BirthDate":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.BirthDate).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        case "ClassName":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        case "StatusName":
                            Paging.List = orderByPriority.ToUpper() == "ASC" ? Paging.List.OrderBy(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName) : Paging.List.OrderByDescending(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            lsPupilProfileViewModel = orderByPriority.ToUpper() == "ASC" ? lsPupilProfileViewModel.OrderBy(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName) : lsPupilProfileViewModel.OrderByDescending(o => o.ProfileStatus).ThenBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                        default:
                            Paging.List = Paging.List.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                            lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                            break;
                    }
                }
                else
                {
                    Paging.List = Paging.List.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.FullName).ToList();
                    lsPupilProfileViewModel = lsPupilProfileViewModel.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.FullName);
                }
            }

            string divtool = string.Empty;
            List<int> listHidden = new List<int>();

            Dictionary<int, bool> dicHeadTeacherPermission = UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, lstClassID);
            Dictionary<int, bool> dicSubjectTeacherPermission = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, lstClassID, 0);
            Dictionary<int, bool> dicSupervisiorAssignmentPermission = UtilsBusiness.HasOverseeingTeacherPermission(global.UserAccountID, lstClassID);
            bool isBGH = UtilsBusiness.IsBGH(global.UserAccountID);
            //Chiendd: Tunning chuc nang tim kiem va xuat excel:
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(c => c.IsActive == true).ToList();
            EducationLevel objEducationLevel;

            List<PolicyRegime> lstPolicyRegime = PolicyRegimeBusiness.All.Where(c => c.IsActive == true).ToList();
            PolicyRegime objPolicyRegime;
            foreach (var item in Paging.List)
            {
                item.EnableCheck = global.IsCurrentYear
                                        && item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                        && (dicHeadTeacherPermission[item.CurrentClassID] || dicSubjectTeacherPermission[item.CurrentClassID] || dicSupervisiorAssignmentPermission[item.CurrentClassID] || isBGH);

                item.iMotherBirthDate = item.MotherBirthDate.HasValue ? item.MotherBirthDate.Value.Year : new Nullable<int>();
                item.iFatherBirthDate = item.FatherBirthDate.HasValue ? item.FatherBirthDate.Value.Year : new Nullable<int>();

                #region switch case data
                //Tunning:
                //ClassProfile cp = ClassProfileBusiness.Find(item.CurrentClassID);
                ClassProfile cp = (lstClass == null) ? null : lstClass.FirstOrDefault(c => c.ClassProfileID == item.CurrentClassID);
                if (cp != null)
                {
                    item.ClassName = cp.DisplayName;
                }
                else
                {
                    item.ClassName = "";
                }

                switch (item.ProfileStatus)
                {
                    case SystemParamsInFile.PUPIL_STATUS_STUDYING:
                        item.StatusName = Res.Get("Common_Label_PupilStatusStuding");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_GRADUATED:
                        item.StatusName = Res.Get("Common_Label_PupilStatusGraduated");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMoved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF:
                        item.StatusName = Res.Get("Common_Label_PupilStatusLeaved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMovedToOtherClass");
                        break;
                }
                if (item.PolicyRegimeID.HasValue && item.PolicyRegimeID > 0)
                {
                    //item.PolicyRegimeName = PolicyRegimeBusiness.Find(item.PolicyRegimeID.Value).Resolution;
                    objPolicyRegime = lstPolicyRegime.FirstOrDefault(p => p.PolicyRegimeID == item.PolicyRegimeID);
                    item.PolicyRegimeName = (objPolicyRegime == null) ? "" : objPolicyRegime.Resolution;
                }
                if (item.EducationLevel > 0)
                {
                    objEducationLevel = lstEducationLevel.FirstOrDefault(e => e.EducationLevelID == item.EducationLevel);
                    //item.EducationLevelName = EducationLevelBusiness.Find(item.EducationLevel).Resolution;
                    item.EducationLevelName = (objEducationLevel == null) ? "" : objEducationLevel.Resolution;
                }

                if (item.BloodType.HasValue)
                {
                    //BloodType
                    switch (item.BloodType.Value)
                    {
                        case SystemParamsInFile.BLOOD_TYPE_A:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeA");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_B:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeB");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_AB:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeAB");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_O:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeO");
                            break;

                        case SystemParamsInFile.BLOOD_TYPE_OTHER:
                            item.BloodTypeName = Res.Get("Common_Label_BloodTypeOther");
                            break;

                        default:
                            item.BloodTypeName = no;
                            break;
                    }
                }

                if (item.EnrolmentType.HasValue)
                {
                    switch (item.EnrolmentType.Value)
                    {
                        case SystemParamsInFile.ENROLMENT_TYPE_PASSED_EXAMINATION:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypePassExamination");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeMoveFromOtherSchool");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_SELECTED:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeSelected");
                            break;

                        case SystemParamsInFile.ENROLMENT_TYPE_OTHER:
                            item.EnrolmentTypeName = Res.Get("Common_Label_EnrolmentTypeOther");
                            break;

                        default:
                            item.EnrolmentTypeName = no;
                            break;
                    }
                }

                if (item.IsResidentIn == true)
                {
                    item.ResidentInName = Yes;
                    item.CheckIsResidentIn = true;
                }
                else
                {
                    item.ResidentInName = No;
                    item.CheckIsResidentIn = false;
                }
                if (item.IsDisabled == true)
                {
                    item.IsDisabledName = Yes;
                    item.CheckIsDisabled = true;
                }
                else
                {
                    item.IsDisabledName = No;
                    item.CheckIsDisabled = false;
                }

                #region divtool namedv
                //0.Xem hồ sơ
                //1.Sửa hồ sơ
                //2.Xóa hồ sơ
                //3.Chuyển lớp
                //4.Chuyển trường
                //5: thôi học
                //6: Cập nhật thôi học
                listHidden = new List<int>();

                if (global.IsCurrentYear)
                {
                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        listHidden.Add(6);
                        //Neu khong phai giao vien chu nhiem 
                        if (!dicHeadTeacherPermission[item.CurrentClassID]) //BGH, GV
                        {
                            listHidden.Add(1);
                            listHidden.Add(2);
                            listHidden.Add(3);
                            listHidden.Add(4);
                            listHidden.Add(5);
                            listHidden.Add(6);
                        }
                    }

                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        listHidden.Add(1);
                        listHidden.Add(2);
                        listHidden.Add(3);
                        listHidden.Add(4);
                        listHidden.Add(5);
                        listHidden.Add(6);
                    }

                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                    {
                        listHidden.Add(1);
                        listHidden.Add(2);
                        listHidden.Add(3);
                        listHidden.Add(4);
                        listHidden.Add(5);


                        //Neu khong phai giao vien chu nhiem 
                        if (!dicHeadTeacherPermission[item.CurrentClassID]) //BGH, GV
                        {
                            listHidden.Add(6);
                        }
                    }

                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    {
                        listHidden.Add(1);
                        listHidden.Add(2);
                        listHidden.Add(3);
                        listHidden.Add(5);
                        listHidden.Add(6);

                        //Neu khong phai giao vien chu nhiem 
                        if (!dicHeadTeacherPermission[item.CurrentClassID]) //BGH, GV
                        {
                            listHidden.Add(4);
                        }
                    }

                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                    {
                        listHidden.Add(1);
                        listHidden.Add(2);
                        listHidden.Add(4);
                        listHidden.Add(5);
                        listHidden.Add(6);
                        //Neu khong phai giao vien chu nhiem 
                        if (!dicHeadTeacherPermission[item.CurrentClassID])
                        {
                            listHidden.Add(3);
                        }
                    }
                }
                else
                {
                    listHidden.Add(1);
                    listHidden.Add(2);
                    listHidden.Add(3);
                    listHidden.Add(4);
                    listHidden.Add(5);
                    listHidden.Add(6);
                }

                item.HiddenLink = string.Join(",", listHidden);
                #endregion
            }
                #endregion
            if (isFirst)
            {

                Session["lstPupilIDBySearch"] = lsPupilProfileViewModel.Where(x => x.ProfileStatus == 1 || x.ProfileStatus == 2).Select(x => x.PupilProfileID).ToList();
                //var a = PupilProfileBusiness.All.Take(2000).Select(x=>x.PupilProfileID).ToList();
                //var b = PupilProfileBusiness.All.Take(4000).Select(x => x.PupilProfileID).ToList();
                //var c = PupilProfileBusiness.All.Take(8000).Select(x => x.PupilProfileID).ToList();
                //var d = PupilProfileBusiness.All.Select(x => x.PupilProfileID).ToList();
            }

            return Paging;
        }

        [SkipCheckRole]
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridSelect(SearchViewModel frm, int page = 1, string orderBy = "")
        {
            GlobalInfo global = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["CurrentClassID"] = frm.Class;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["ProfileStatus"] = frm.Status;
            SearchInfo["EthnicID"] = frm.Ethnic;
            bool alowPaging = string.IsNullOrEmpty(frm.Class);

            Paginate<PupilProfileChildrenViewModel> paging = this.SearchPaging(SearchInfo, alowPaging, false, page, orderBy);
            GridModel<PupilProfileChildrenViewModel> gm = new GridModel<PupilProfileChildrenViewModel>(paging.List);
            gm.Total = paging.total;

            return View(gm);
        }
        #endregion

        #region Sort
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult OrderPupil(int ClassID, List<int> lstOrderedPupilID)
        {
            if (ClassID <= 0)
                return Json(new JsonMessage(Res.Get("PupilProfile_Label_ClassIDRequiredToSort"), JsonMessage.ERROR));

            GlobalInfo global = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Check"] = "Check";

            List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).OrderBy(u => u.PupilProfile.Name).ToList();

            if (lstOrderedPupilID == null)
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));

            if (lstPOC.Count != lstOrderedPupilID.Count)
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));

            List<int> lstPupilID = lstPOC.Select(u => u.PupilID).ToList();
            List<int> lstOrder = lstPupilID.Select(u => lstOrderedPupilID.IndexOf(u) + 1).ToList();

            PupilOfClassBusiness.UpdateOrder(global.UserAccountID, ClassID, lstPupilID, lstOrder);
            PupilOfClassBusiness.Save();

            return Json(new JsonMessage(Res.Get("PupilProfile_Label_OrderSuccess")));
        }

        public PartialViewResult ListSortClass(int ClassID)
        {
            if (ClassID <= 0)
                throw new BusinessException("PupilProfile_Label_ClassIDRequiredToSort");

            CheckPermissionForAction(ClassID, "ClassProfile");

            GlobalInfo global = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Check"] = "Check";

            var query = from poc in PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo)
                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                        select new PupilProfileChildrenViewModel
                        {
                            PupilProfileID = pp.PupilProfileID,
                            FullName = pp.FullName,
                            PupilCode = pp.PupilCode,
                            BirthDate = pp.BirthDate,
                            Genre = pp.Genre,
                            ProfileStatus = poc.Status,
                            OrderInClass = poc.OrderInClass,
                            Name = pp.Name,
                            EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null
                        };

            List<PupilProfileChildrenViewModel> lst = query.ToList()
                .OrderBy(u => u.OrderInClass)
                .ThenBy(u => SMAS.Business.Common.Utils.SortABC(u.Name))
                .ThenBy(u => SMAS.Business.Common.Utils.SortABC(u.FullName))
                .ToList();

            lst.ForEach(u =>
            {
                u.StatusName = CommonList.PupilStatus().SingleOrDefault(v => v.key == u.ProfileStatus.ToString()).value;
                u.GenreName = u.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female");
            });

            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            ViewData[PupilProfileChildrenConstants.IS_SHOW_AVATAR] = classProfile.AcademicYear.IsShowAvatar;
            ViewData[PupilProfileChildrenConstants.LIST_PUPILPROFILE] = lst;
            ViewData[PupilProfileChildrenConstants.CLASS_PROFILE_NAME] = classProfile.DisplayName;
            return PartialView("_ListSortClass", ClassID);
        }

        [SkipCheckRole]
        public ViewResult SortClass(int ClassID)
        {
            CheckPermissionForAction(ClassID, "ClassProfile");
            if (ClassID <= 0)
                throw new BusinessException("PupilProfile_Label_ClassIDRequiredToSort");

            ListSortClass(ClassID);
            return View("SortClass");
        }


        [ValidateAntiForgeryToken]
        public JsonResult SortByAlphabet(int ClassID)
        {
            CheckPermissionForAction(ClassID, "ClassProfile");
            if (ClassID <= 0)
                return Json(new JsonMessage(Res.Get("PupilProfile_Label_ClassIDRequiredToSort"), JsonMessage.ERROR));

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Check"] = "Check";

            /*var lstPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo)
                                                .Select(u => new { u.PupilID, u.PupilProfile.Name,u. })
                                                .ToList()
                                                .OrderBy(u =>Utils.Utils.SortABC(u.Name+ " "+ u.fu))
                                                .ToList();*/

            //Chiendd: 10/09/2014: Sua lai thuat toan sap xep tieng viet
            var lstPOC = (from pc in PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo)
                          join pf in PupilProfileBusiness.All on pc.PupilID equals pf.PupilProfileID
                          select new
                          {
                              PupilID = pc.PupilID,
                              Name = pf.Name,
                              FullName = pf.FullName,
                              EthnicCode = pf.Ethnic != null ? pf.Ethnic.EthnicCode : null
                          })
                          .ToList()
                          .OrderBy(u => SMAS.Business.Common.Utils.SortABC(u.FullName.getOrderingName(u.EthnicCode)))
                          .ToList();

            int orderNum = 1;
            List<int> lstPupilID = lstPOC.Select(u => u.PupilID).ToList();
            List<int> lstOrder = lstPupilID.Select(u => orderNum++).ToList();

            PupilOfClassBusiness.UpdateOrder(global.UserAccountID, ClassID, lstPupilID, lstOrder);
            PupilOfClassBusiness.Save();

            return Json(new JsonMessage(Res.Get("PupilProfile_Label_OrderSuccess"), JsonMessage.SUCCESS));
        }
        #endregion Sort

        #region Create
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create()
        {
            GlobalInfo global = new GlobalInfo();
            PupilProfileChildrenViewModel PupilProfileViewModel = new PupilProfileChildrenViewModel();
            TryUpdateModel(PupilProfileViewModel);

            PupilProfile pupilprofile = new PupilProfile();

            Utils.Utils.BindTo(PupilProfileViewModel, pupilprofile);
            Utils.Utils.TrimObject(pupilprofile);

            if (PupilProfileViewModel.CheckIsDisabled)
            {
                pupilprofile.IsDisabled = true;
            }
            else
            {
                pupilprofile.IsDisabled = false;
            }

            // Kiem tra cac so dien thoai co hop le khong
            try
            {
                this.CheckTelephoneNumber(PupilProfileViewModel);
            }
            catch (Exception ex)
            {
                return Json(new JsonMessage(ex.Message, "error"));
            }

            //chuyen doi nam sinh bo,me,ng bao ho
            //VALIDATE nam sinh,doi du lieu tu nam sinh object sang nam sinh cua entity
            if (PupilProfileViewModel.iFatherBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_FatherBirthDate" });
                }
                if (PupilProfileViewModel.iFatherBirthDate > DateTime.Now.Year)
                {
                    throw new BusinessException(Res.Get("PupilProfileChildren_Valid_FatherBirthDate"));
                }
                pupilprofile.FatherBirthDate = new DateTime(PupilProfileViewModel.iFatherBirthDate.Value, 1, 1);
            }
            if (PupilProfileViewModel.iMotherBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_MotherBirthDate" });
                }
                if (PupilProfileViewModel.iMotherBirthDate > DateTime.Now.Year)
                {
                    throw new BusinessException(Res.Get("PupilProfileChildren_Valid_MotherBirthDate"));
                }
                pupilprofile.MotherBirthDate = new DateTime(PupilProfileViewModel.iMotherBirthDate.Value, 1, 1);
            }
            if (PupilProfileViewModel.iSponsorBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iSponsorBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_SponsorBirthDate" });
                }
                if (PupilProfileViewModel.iSponsorBirthDate > DateTime.Now.Year)
                {
                    throw new BusinessException(Res.Get("PupilProfileChildren_Valid_SponsorBirthDate"));
                }
                pupilprofile.SponsorBirthDate = new DateTime(PupilProfileViewModel.iSponsorBirthDate.Value, 1, 1);
            }
            //Validation Email
            if (!string.IsNullOrEmpty(PupilProfileViewModel.MotherEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.MotherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_MotherEmail")));
                pupilprofile.MotherEmail = PupilProfileViewModel.MotherEmail;
            }

            if (!string.IsNullOrEmpty(PupilProfileViewModel.FatherEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.FatherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_FatherEmail")));
                pupilprofile.FatherEmail = PupilProfileViewModel.FatherEmail;
            }

            if (!string.IsNullOrEmpty(PupilProfileViewModel.SponsorEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.SponsorEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_SponsorEmail")));
                pupilprofile.SponsorEmail = PupilProfileViewModel.SponsorEmail;
            }

            //de phong name loi
            int space = pupilprofile.FullName.LastIndexOf(" ");
            if (space == -1)
            {
                pupilprofile.Name = pupilprofile.FullName;
            }
            else
            {
                pupilprofile.Name = pupilprofile.FullName.Substring(space + 1);
            }

            // AnhVD Hotfix - Tên học sinh không vượt quá 30 ký tự
            if (!string.IsNullOrWhiteSpace(pupilprofile.Name) && pupilprofile.Name.Length > 30)
                throw new BusinessException("PupilProfile_Validate_MaxLength30");
            // End 20140919

            //CodeConfigBusiness.CreatePupilCode(UserInfo.AcademicYearID, cboEducationLevel.Value,UserInfo.SchoolID)
            if (PupilProfileViewModel.AutoGenCode)
            {
                pupilprofile.PupilCode = CodeConfigBusiness.CreatePupilCode(global.AcademicYearID.Value, (int)PupilProfileViewModel.EducationLevel, global.SchoolID.Value);
            }
            else
            {
                //truong hop autogencode bi check va disable thi ko submut dc,ta phai tu check
                if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL))
                {
                    if (!CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL))
                    {
                        pupilprofile.PupilCode = CodeConfigBusiness.CreatePupilCode(global.AcademicYearID.Value, (int)PupilProfileViewModel.EducationLevel, global.SchoolID.Value);
                    }
                }
            }

            //check neu PupilCode chua co thi ban ra exception
            if (pupilprofile.PupilCode == null || pupilprofile.PupilCode.Trim().Length == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilProfile_Label_PupilCodeBlankError"), "error"));
            }

            #region HaiVT 04/06/2013 - tao ParentAccount
            //so dien thoai cua cha, cua me, nguoi giam ho khong duoc phep giong nhau
            if ((PupilProfileViewModel.FatherMobile == PupilProfileViewModel.MotherMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.FatherMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.MotherMobile))
                || (PupilProfileViewModel.FatherMobile == PupilProfileViewModel.SponsorMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.FatherMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.SponsorMobile))
                || (PupilProfileViewModel.SponsorMobile == PupilProfileViewModel.MotherMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.SponsorMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.MotherMobile)))
            {
                throw new BusinessException("PupilProfile_Label_DuplicateMobileError");
            }

            if (PupilProfileViewModel.FatherMobile != null && (PupilProfileViewModel.FatherMobile.Trim().Length < 9 || PupilProfileViewModel.FatherMobile.Trim().Length > 12))
            {
                throw new BusinessException(Res.Get("PupilProfileChildren_Valid_FatherMobile"));
            }
            if (PupilProfileViewModel.MotherMobile != null && (PupilProfileViewModel.MotherMobile.Trim().Length < 9 || PupilProfileViewModel.MotherMobile.Trim().Length > 12))
            {
                throw new BusinessException(Res.Get("PupilProfileChildren_Valid_MotherMobile"));
            }
            if (PupilProfileViewModel.SponsorMobile != null && (PupilProfileViewModel.SponsorMobile.Trim().Length < 9 || PupilProfileViewModel.SponsorMobile.Trim().Length > 12))
            {
                throw new BusinessException(Res.Get("PupilProfileChildren_Valid_SponsorMobile"));
            }
            //tai khoan theo so dt cha

            if (PupilProfileViewModel.IsFatherSMS)
            {
                //if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.FatherMobile))
                //{
                //    CreateParentAccount(PupilProfileViewModel.FatherMobile, pupilprofile);
                //}
                //else
                //{
                //    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_FatherMobile_Empty" });
                //}
            }

            //tai khoan theo so dt me
            if (PupilProfileViewModel.IsMotherSMS)
            {
                //if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.MotherMobile))
                //{
                //    CreateParentAccount(PupilProfileViewModel.MotherMobile, pupilprofile);
                //}
                //else
                //{
                //    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_MotherMobile_Empty" });
                //}
            }

            //tai khoan theo so dt nguoi giam ho
            if (PupilProfileViewModel.IsSponsorSMS)
            {
                //if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.SponsorMobile))
                //{
                //    CreateParentAccount(PupilProfileViewModel.SponsorMobile, pupilprofile);
                //}
                //else
                //{
                //    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_SponsorMobile_Empty" });
                //}
            }
            #endregion
            //if (!PupilProfileViewModel.EnrolmentType.HasValue)
            //{
            //    PupilProfileViewModel.EnrolmentType = 0;
            //}

            //current academicyear and school
            pupilprofile.CurrentAcademicYearID = global.AcademicYearID.Value;
            pupilprofile.CurrentSchoolID = global.SchoolID.Value;

            //dua image vao pupilprofile
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                pupilprofile.Image = imageBytes;
            }

            //status
            pupilprofile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
            if (PupilProfileViewModel.rptEatingHabit.HasValue)
            {
                pupilprofile.EatingHabit = (int)PupilProfileViewModel.rptEatingHabit.Value;
            }
            if (PupilProfileViewModel.rptReactionTraining.HasValue)
            {
                pupilprofile.ReactionTrainingHabit = (int)PupilProfileViewModel.rptReactionTraining.Value;
            }
            if (PupilProfileViewModel.rptAttitudeInStrangeScene.HasValue)
            {
                pupilprofile.AttitudeInStrangeScene = (int)PupilProfileViewModel.rptAttitudeInStrangeScene.Value;
            }

            pupilprofile.IsResidentIn = PupilProfileViewModel.CheckIsResidentIn;
            List<HabitOfChildren> LstHabitOfChildren = new List<HabitOfChildren>();
            List<FoodHabitOfChildren> LstFoodHabitOfChildren = new List<FoodHabitOfChildren>();
            if (PupilProfileViewModel.rptHabitDetail != null)
            {
                for (int i = 0; i <= PupilProfileViewModel.rptHabitDetail.Count() - 1; i++)
                {
                    HabitOfChildren HabitOfChildren = new HabitOfChildren();
                    HabitOfChildren.HabitDetailID = PupilProfileViewModel.rptHabitDetail[i].Value;
                    HabitOfChildren.PupilID = pupilprofile.PupilProfileID;
                    LstHabitOfChildren.Add(HabitOfChildren);
                }
            }
            if (PupilProfileViewModel.rptTypeOfDishHate != null)
            {
                for (int i = 0; i <= PupilProfileViewModel.rptTypeOfDishHate.Count() - 1; i++)
                {
                    FoodHabitOfChildren FoodHabitOfChildren = new FoodHabitOfChildren();
                    FoodHabitOfChildren.TypeOfDishID = PupilProfileViewModel.rptTypeOfDishHate[i].Value;
                    FoodHabitOfChildren.PupilID = pupilprofile.PupilProfileID;
                    FoodHabitOfChildren.HabitType = SystemParamsInFile.FOOD_HABIT_OF_CHILDREN_HATE;
                    LstFoodHabitOfChildren.Add(FoodHabitOfChildren);
                }
            }
            if (PupilProfileViewModel.rptTypeOfDishLike != null)
            {
                for (int i = 0; i <= PupilProfileViewModel.rptTypeOfDishLike.Count() - 1; i++)
                {
                    FoodHabitOfChildren FoodHabitOfChildren = new FoodHabitOfChildren();
                    FoodHabitOfChildren.TypeOfDishID = PupilProfileViewModel.rptTypeOfDishLike[i].Value;
                    FoodHabitOfChildren.PupilID = pupilprofile.PupilProfileID;
                    FoodHabitOfChildren.HabitType = SystemParamsInFile.FOOD_HABIT_OF_CHILDREN_LIKE;
                    LstFoodHabitOfChildren.Add(FoodHabitOfChildren);
                }
            }
            if (PupilProfileViewModel.rptTypeOfDishMain != null)
            {
                for (int i = 0; i <= PupilProfileViewModel.rptTypeOfDishMain.Count() - 1; i++)
                {
                    FoodHabitOfChildren FoodHabitOfChildren = new FoodHabitOfChildren();
                    FoodHabitOfChildren.TypeOfDishID = PupilProfileViewModel.rptTypeOfDishMain[i].Value;
                    FoodHabitOfChildren.PupilID = pupilprofile.PupilProfileID;
                    FoodHabitOfChildren.HabitType = SystemParamsInFile.FOOD_HABIT_OF_CHILDREN_MAIN;
                    LstFoodHabitOfChildren.Add(FoodHabitOfChildren);
                }
            }

            if (!string.IsNullOrWhiteSpace(PupilProfileViewModel.VillageName))
            {
                Village village;

                // Kiểm tra xem người dùng có nhập Commune
                if (PupilProfileViewModel.CommuneID != null)
                {
                    var existVillage =
                        this.VillageBusiness.All.FirstOrDefault(
                            p =>
                            p.VillageName.Trim().ToLower() == PupilProfileViewModel.VillageName.Trim().ToLower() &&
                            p.CommuneID == PupilProfileViewModel.CommuneID && p.IsActive == true);

                    if (existVillage != null)
                    {
                        //pupilprofile.
                        village = existVillage;
                    }
                    else
                    {
                        var newVillage = new Village
                        {
                            VillageName = PupilProfileViewModel.VillageName,
                            ShortName = PupilProfileViewModel.VillageName,
                            CreateDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            CommuneID = PupilProfileViewModel.CommuneID,
                            IsActive = true
                        };
                        village = VillageBusiness.Insert(newVillage);
                        VillageBusiness.Save();
                        //pupilprofile.VillageID = villageInserted.VillageID;
                    }
                    pupilprofile.VillageID = village.VillageID;
                }
                //else // Người dùng không nhập Commune
                //{
                //    var newVillage = new Village
                //    {
                //        VillageName = PupilProfileViewModel.VillageName,
                //        ShortName = PupilProfileViewModel.VillageName,
                //        CreateDate = DateTime.Now,
                //        ModifiedDate = DateTime.Now,
                //        //CommuneID = (int) PupilProfileViewModel.CommuneID,
                //        IsActive = true
                //    };
                //    village = VillageBusiness.Insert(newVillage);
                //    VillageBusiness.Save();
                //}
                // Cập nhật VillageID cho pupilProfile
                //pupilprofile.VillageID = village.VillageID;
            }
            //Hệ thống lưu vào cơ sở dữ liệu bằng cách gọi hàm InsertPupilProfile
            if (!PupilProfileViewModel.EnrolmentType.HasValue)
                PupilProfileViewModel.EnrolmentType = 0;

            this.PupilProfileBusiness.InsertChildren(global.UserAccountID, pupilprofile, LstHabitOfChildren, LstFoodHabitOfChildren, PupilProfileViewModel.EnrolmentType.Value);
            this.PupilProfileBusiness.Save();
            // Tạo giá trị lưu log action_audit
            string newObject = JsonConvert.SerializeObject(SMAS.Business.Common.Utils.BindTo<PupilProfileBO>(pupilprofile), new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }); ;
            SetViewDataActionAudit(String.Empty, newObject, pupilprofile.PupilProfileID.ToString(), "Add pupil_profile_id:" + pupilprofile.PupilProfileID.ToString(), pupilprofile.PupilProfileID.ToString(), "Hồ sơ trẻ", GlobalConstants.ACTION_ADD, "Thêm mới trẻ " + pupilprofile.FullName + " mã " + pupilprofile.PupilCode);
            // end
            Session["ImagePath"] = null;
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        public ActionResult RedirectCreatePage()
        {
            //Kiểm tra UserInfo. HasHeadTeacherPermission (0) = false:
            //Trả về trang thông báo lỗi “Bạn không có quyền truy cập vào dữ liệu này”
            GlobalInfo global = new GlobalInfo();
            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, 0))
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }
            SetViewData(null);
            return PartialView("_Create");
        }
        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(int PupilProfileID)
        {
            GlobalInfo global = new GlobalInfo();

            //PupilProfileID => ClassID
            PupilProfile pupilprofile = PupilProfileBusiness.Find(PupilProfileID);
            var oldpp = PupilProfileBO.createFromPupilProfile(pupilprofile);
            string oldObject = JsonConvert.SerializeObject(oldpp, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects });// convert to json string for old object
            int OldClassID = pupilprofile.CurrentClassID;
            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, pupilprofile.CurrentClassID))
            {
                return Json(new JsonMessage(Res.Get("Common_Label_HasHeadTeacherPermissionError"), "error"));
            }

            PupilProfileChildrenViewModel PupilProfileViewModel = new PupilProfileChildrenViewModel();
            TryUpdateModel(PupilProfileViewModel);



            if (PupilProfileViewModel.AutoGenCode)
            {
                PupilProfileViewModel.PupilCode = pupilprofile.PupilCode;
            }
            else
            {
                //truong hop autogencode bi check va disable thi ko submut dc,ta phai tu check
                if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL))
                {
                    if (!CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL))
                    {
                        PupilProfileViewModel.PupilCode = pupilprofile.PupilCode;
                    }
                }
            }
            Utils.Utils.BindTo(PupilProfileViewModel, pupilprofile);
            Utils.Utils.TrimObject(pupilprofile);


            if (PupilProfileViewModel.CheckIsDisabled)
            {
                pupilprofile.IsDisabled = true;
            }
            else
            {
                pupilprofile.IsDisabled = false;
            }

            // Kiem tra cac so dien thoai co hop le khong
            try
            {
                this.CheckTelephoneNumber(PupilProfileViewModel);
            }
            catch (Exception ex)
            {
                return Json(new JsonMessage(ex.Message, "error"));
            }

            //chuyen doi nam sinh bo,me,ng bao ho
            //VALIDATE nam sinh,doi du lieu tu nam sinh object sang nam sinh cua entity
            if (PupilProfileViewModel.iFatherBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_FatherBirthDate" });
                }
                if (PupilProfileViewModel.iFatherBirthDate > DateTime.Now.Year)
                {
                    throw new BusinessException(Res.Get("PupilProfileChildren_Valid_FatherBirthDate"));
                }
                pupilprofile.FatherBirthDate = new DateTime(PupilProfileViewModel.iFatherBirthDate.Value, 1, 1);
            }
            if (PupilProfileViewModel.iMotherBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_MotherBirthDate" });
                }
                if (PupilProfileViewModel.iMotherBirthDate > DateTime.Now.Year)
                {
                    throw new BusinessException(Res.Get("PupilProfileChildren_Valid_MotherBirthDate"));
                }
                pupilprofile.MotherBirthDate = new DateTime(PupilProfileViewModel.iMotherBirthDate.Value, 1, 1);
            }
            if (PupilProfileViewModel.iSponsorBirthDate.HasValue)
            {
                if (PupilProfileViewModel.iSponsorBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "PupilProfile_Label_SponsorBirthDate" });
                }
                if (PupilProfileViewModel.iSponsorBirthDate > DateTime.Now.Year)
                {
                    throw new BusinessException(Res.Get("PupilProfileChildren_Valid_SponsorBirthDate"));
                }
                pupilprofile.SponsorBirthDate = new DateTime(PupilProfileViewModel.iSponsorBirthDate.Value, 1, 1);
            }

            if (!string.IsNullOrEmpty(PupilProfileViewModel.MotherEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.MotherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_MotherEmail")));
                pupilprofile.MotherEmail = PupilProfileViewModel.MotherEmail;
            }

            if (!string.IsNullOrEmpty(PupilProfileViewModel.FatherEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.FatherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_FatherEmail")));
                pupilprofile.FatherEmail = PupilProfileViewModel.FatherEmail;
            }

            if (!string.IsNullOrEmpty(PupilProfileViewModel.SponsorEmail))
            {
                string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Match match = Regex.Match(PupilProfileViewModel.SponsorEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                    throw new BusinessException(string.Format(Res.Get("Common_Validate_NotEmail"), Res.Get("PupilProfile_Label_SponsorEmail")));
                pupilprofile.SponsorEmail = PupilProfileViewModel.SponsorEmail;
            }

            pupilprofile.StorageNumber = PupilProfileViewModel.StorageNumber;

            //if (!PupilProfileViewModel.EnrolmentType.HasValue)
            //{
            //    PupilProfileViewModel.EnrolmentType = 0;
            //}

            //CodeConfigBusiness.CreatePupilCode(UserInfo.AcademicYearID, cboEducationLevel.Value,UserInfo.SchoolID)
            //if (PupilProfileViewModel.AutoGenCode)
            //{
            //    pupilprofile.PupilCode = CodeConfigBusiness.CreatePupilCode(global.AcademicYearID.Value, (int)PupilProfileViewModel.EducationLevel, global.SchoolID.Value);
            //}
            //else
            //{
            //    //truong hop autogencode bi check va disable thi ko submut dc,ta phai tu check
            //    if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL))
            //    {
            //        if (!CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL))
            //        {
            //            pupilprofile.PupilCode = CodeConfigBusiness.CreatePupilCode(global.AcademicYearID.Value, (int)PupilProfileViewModel.EducationLevel, global.SchoolID.Value);
            //        }
            //    }
            //}

            //check neu PupilCode chua co thi ban ra exception
            if (pupilprofile.PupilCode == null || pupilprofile.PupilCode.Trim().Length == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilProfile_Label_PupilCodeBlankError"), "error"));
            }

            #region HaiVT 04/06/2013 - tao ParentAccount
            if (string.IsNullOrWhiteSpace(PupilProfileViewModel.FatherMobile))
            {
                PupilProfileViewModel.FatherMobile = null;
            }
            else
            {
                PupilProfileViewModel.FatherMobile = PupilProfileViewModel.FatherMobile.Replace(" ", "");
            }
            if (string.IsNullOrWhiteSpace(PupilProfileViewModel.MotherMobile))
            {
                PupilProfileViewModel.MotherMobile = null;
            }
            else
            {
                PupilProfileViewModel.MotherMobile = PupilProfileViewModel.MotherMobile.Replace(" ", "");
            }
            if (string.IsNullOrWhiteSpace(PupilProfileViewModel.SponsorMobile))
            {
                PupilProfileViewModel.SponsorMobile = null;
            }
            else
            {
                PupilProfileViewModel.SponsorMobile = PupilProfileViewModel.SponsorMobile.Replace(" ", "");
            }

            //so dien thoai cua cha, cua me, nguoi giam ho khong duoc phep giong nhau
            if ((PupilProfileViewModel.FatherMobile == PupilProfileViewModel.MotherMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.FatherMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.MotherMobile))
                || (PupilProfileViewModel.FatherMobile == PupilProfileViewModel.SponsorMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.FatherMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.SponsorMobile))
                || (PupilProfileViewModel.SponsorMobile == PupilProfileViewModel.MotherMobile && !string.IsNullOrWhiteSpace(PupilProfileViewModel.SponsorMobile) && !string.IsNullOrWhiteSpace(PupilProfileViewModel.MotherMobile)))
            {
                throw new BusinessException("PupilProfile_Label_DuplicateMobileError");
            }
            if (PupilProfileViewModel.FatherMobile != null && (PupilProfileViewModel.FatherMobile.Trim().Length < 9 || PupilProfileViewModel.FatherMobile.Trim().Length > 12))
            {
                throw new BusinessException(Res.Get("PupilProfileChildren_Valid_FatherMobile"));
            }
            if (PupilProfileViewModel.MotherMobile != null && (PupilProfileViewModel.MotherMobile.Trim().Length < 9 || PupilProfileViewModel.MotherMobile.Trim().Length > 12))
            {
                throw new BusinessException(Res.Get("PupilProfileChildren_Valid_MotherMobile"));
            }
            if (PupilProfileViewModel.SponsorMobile != null && (PupilProfileViewModel.SponsorMobile.Trim().Length < 9 || PupilProfileViewModel.SponsorMobile.Trim().Length > 12))
            {
                throw new BusinessException(Res.Get("PupilProfileChildren_Valid_SponsorMobile"));
            }
            //tai khoan theo so dt cha
            if (PupilProfileViewModel.IsFatherSMS)
            {
                //if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.FatherMobile))
                //{
                //    CreateParentAccount(PupilProfileViewModel.FatherMobile, pupilprofile);
                //}
                //else
                //{
                //    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_FatherMobile_Empty" });
                //}
            }

            //tai khoan theo so dt me
            if (PupilProfileViewModel.IsMotherSMS)
            {
                //if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.MotherMobile))
                //{
                //    CreateParentAccount(PupilProfileViewModel.MotherMobile, pupilprofile);
                //}
                //else
                //{
                //    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_MotherMobile_Empty" });
                //}
            }

            //tai khoan theo so dt nguoi giam ho
            if (PupilProfileViewModel.IsSponsorSMS)
            {
                //if (Utils.Utils.CheckMobileNumber(PupilProfileViewModel.SponsorMobile))
                //{
                //    CreateParentAccount(PupilProfileViewModel.SponsorMobile, pupilprofile);
                //}
                //else
                //{
                //    throw new BusinessException("Common_Validate_NotIsPhoneNumber", new List<object>() { "PupilProfile_Label_SponsorMobile_Empty" });
                //}
            }
            #endregion

            //de phong name loi
            int space = pupilprofile.FullName.LastIndexOf(" ");
            if (space == -1)
            {
                pupilprofile.Name = pupilprofile.FullName;
            }
            else
            {
                pupilprofile.Name = pupilprofile.FullName.Substring(space + 1);
            }

            // AnhVD Hotfix - Tên học sinh không vượt quá 30 ký tự
            if (!string.IsNullOrWhiteSpace(pupilprofile.Name) && pupilprofile.Name.Length > 30)
                throw new BusinessException("PupilProfile_Validate_MaxLength30");
            // End 20140919

            //current academicyear and school
            pupilprofile.CurrentAcademicYearID = global.AcademicYearID.Value;
            pupilprofile.CurrentSchoolID = global.SchoolID.Value;

            //dua image vao pupilprofile
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                pupilprofile.Image = imageBytes;
            }

            //status
            if (pupilprofile.ProfileStatus == 0)
            {
                pupilprofile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
            }
            if (PupilProfileViewModel.rptEatingHabit.HasValue)
            {
                pupilprofile.EatingHabit = (int)PupilProfileViewModel.rptEatingHabit.Value;
            }
            if (PupilProfileViewModel.rptReactionTraining.HasValue)
            {
                pupilprofile.ReactionTrainingHabit = (int)PupilProfileViewModel.rptReactionTraining.Value;
            }
            if (PupilProfileViewModel.rptAttitudeInStrangeScene.HasValue)
            {
                pupilprofile.AttitudeInStrangeScene = (int)PupilProfileViewModel.rptAttitudeInStrangeScene.Value;
            }
            pupilprofile.IsResidentIn = PupilProfileViewModel.CheckIsResidentIn;
            List<HabitOfChildren> LstHabitOfChildren = new List<HabitOfChildren>();
            List<FoodHabitOfChildren> LstFoodHabitOfChildren = new List<FoodHabitOfChildren>();
            if (PupilProfileViewModel.rptHabitDetail != null)
            {
                for (int i = 0; i <= PupilProfileViewModel.rptHabitDetail.Count() - 1; i++)
                {
                    HabitOfChildren HabitOfChildren = new HabitOfChildren();
                    HabitOfChildren.HabitDetailID = PupilProfileViewModel.rptHabitDetail[i].Value;
                    HabitOfChildren.PupilID = pupilprofile.PupilProfileID;
                    LstHabitOfChildren.Add(HabitOfChildren);
                }
            }
            if (PupilProfileViewModel.rptTypeOfDishHate != null)
            {
                for (int i = 0; i <= PupilProfileViewModel.rptTypeOfDishHate.Count() - 1; i++)
                {
                    FoodHabitOfChildren FoodHabitOfChildren = new FoodHabitOfChildren();
                    FoodHabitOfChildren.TypeOfDishID = PupilProfileViewModel.rptTypeOfDishHate[i].Value;
                    FoodHabitOfChildren.PupilID = pupilprofile.PupilProfileID;
                    FoodHabitOfChildren.HabitType = SystemParamsInFile.FOOD_HABIT_OF_CHILDREN_HATE;
                    LstFoodHabitOfChildren.Add(FoodHabitOfChildren);
                }
            }
            if (PupilProfileViewModel.rptTypeOfDishLike != null)
            {
                for (int i = 0; i <= PupilProfileViewModel.rptTypeOfDishLike.Count() - 1; i++)
                {
                    FoodHabitOfChildren FoodHabitOfChildren = new FoodHabitOfChildren();
                    FoodHabitOfChildren.TypeOfDishID = PupilProfileViewModel.rptTypeOfDishLike[i].Value;
                    FoodHabitOfChildren.PupilID = pupilprofile.PupilProfileID;
                    FoodHabitOfChildren.HabitType = SystemParamsInFile.FOOD_HABIT_OF_CHILDREN_LIKE;
                    LstFoodHabitOfChildren.Add(FoodHabitOfChildren);
                }
            }
            if (PupilProfileViewModel.rptTypeOfDishMain != null)
            {
                for (int i = 0; i <= PupilProfileViewModel.rptTypeOfDishMain.Count() - 1; i++)
                {
                    FoodHabitOfChildren FoodHabitOfChildren = new FoodHabitOfChildren();
                    FoodHabitOfChildren.TypeOfDishID = PupilProfileViewModel.rptTypeOfDishMain[i].Value;
                    FoodHabitOfChildren.PupilID = pupilprofile.PupilProfileID;
                    FoodHabitOfChildren.HabitType = SystemParamsInFile.FOOD_HABIT_OF_CHILDREN_MAIN;
                    LstFoodHabitOfChildren.Add(FoodHabitOfChildren);
                }
            }

            if (!string.IsNullOrWhiteSpace(PupilProfileViewModel.VillageName))
            {
                Village village;
                // Kiểm tra xem người dùng có nhập Commune
                if (PupilProfileViewModel.CommuneID != null)
                {
                    var existVillage =
                        this.VillageBusiness.All.FirstOrDefault(
                            p =>
                            p.VillageName.Trim().ToLower() == PupilProfileViewModel.VillageName.Trim().ToLower() &&
                            p.CommuneID == PupilProfileViewModel.CommuneID && p.IsActive == true);

                    if (existVillage != null)
                    {
                        village = existVillage;
                    }
                    else
                    {
                        var newVillage = new Village
                        {
                            VillageName = PupilProfileViewModel.VillageName,
                            ShortName = PupilProfileViewModel.VillageName,
                            CreateDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            CommuneID = PupilProfileViewModel.CommuneID,
                            IsActive = true
                        };
                        village = VillageBusiness.Insert(newVillage);
                        VillageBusiness.Save();
                    }
                    pupilprofile.VillageID = village.VillageID;
                }
            }
            else
            {
                pupilprofile.VillageID = null;
            }

            if (!PupilProfileViewModel.EnrolmentType.HasValue)
                PupilProfileViewModel.EnrolmentType = 0;
            this.PupilProfileBusiness.UpdateChildren(global.UserAccountID, pupilprofile, LstHabitOfChildren, LstFoodHabitOfChildren, OldClassID, PupilProfileViewModel.EnrolmentType.Value);
            this.PupilProfileBusiness.Save();
            var newpp = PupilProfileBO.createFromPupilProfile(pupilprofile);
            string newObject = JsonConvert.SerializeObject(newpp, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }); ;
            SetViewDataActionAudit(oldObject, newObject, pupilprofile.PupilProfileID.ToString(), "Update pupil_profile_id:" + pupilprofile.PupilProfileID.ToString(), pupilprofile.PupilProfileID.ToString(), "Hồ sơ trẻ", GlobalConstants.ACTION_UPDATE, "Cập nhật trẻ " + pupilprofile.FullName + " mã " + pupilprofile.PupilCode);

            if (pupilprofile.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)
            {
                ViewData[PupilProfileChildrenConstants.PUPIL_NOT_STUDY] = false;
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.PUPIL_NOT_STUDY] = true;
            }
            Session["ImagePath"] = null;
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        public ActionResult GetEditPupilProfile(int PupilProfileID, int NextPre = 0)
        {
            GlobalInfo global = new GlobalInfo();
            bool isEnd = false;
            bool isFirst = false;
            List<int> lstInt = new List<int>();
            if (Session["lstPupilIDBySearch"] != null)
            {
                lstInt = (List<int>)Session["lstPupilIDBySearch"];
            }
            //List<int> lstInt = new List<int>();
            //var abc = (SearchViewModel)Session[PupilProfileChildrenConstants.SEARCHFORM];
            int position = lstInt.FindIndex(X => X == PupilProfileID);
            if (position == 0)
                isFirst = true;
            if (position == lstInt.Count - 1)
                isEnd = true;
            if (NextPre != 0 && lstInt.Count != 0)
            {
                if (NextPre == 1)
                {
                    PupilProfileID = lstInt[position - 1];
                    if (position - 1 == 0)
                        isFirst = true;
                    if (position == lstInt.Count - 1)
                        isEnd = false;
                }
                else
                {
                    PupilProfileID = lstInt[position + 1];
                    if (position + 1 == lstInt.Count - 1)
                        isEnd = true;

                    if (position == 0)
                        isFirst = false;
                }
            }

            List<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", global.AcademicYearID } }).ToList();
            //lstPoc = lstPoc.sk
            if (lstPoc != null && !lstPoc.Where(o => o.PupilID == PupilProfileID).Any())
            {
                return new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "Index" }));
            }
            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, 0))
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }
            SetViewData(null, false, PupilProfileID);
            IDictionary<string, object> TypeOfDishOfPupilSearchInfo = new Dictionary<string, object>();
            TypeOfDishOfPupilSearchInfo["PupilID"] = PupilProfileID;
            List<FoodHabitOfChildren> LstFoodHabitOfChildren = this.FoodHabitOfChildrenBusiness.Search(TypeOfDishOfPupilSearchInfo).ToList();
            List<HabitOfChildren> LstHabitOfChildren = this.HabitOfChildrenBusiness.Search(TypeOfDishOfPupilSearchInfo).ToList();
            ViewData[PupilProfileChildrenConstants.LIST_TYPEOFDISHOFPUPIL] = LstFoodHabitOfChildren;
            ViewData[PupilProfileChildrenConstants.LIST_HABITDETAILOFPUPIL] = LstHabitOfChildren;


            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            //IQueryable<PupilProfileChildrenViewModel> lsPupilProfileViewModel = _Search(SearchInfo, false).AsQueryable();
            PupilProfileChildrenViewModel model = SearchPaging(SearchInfo, false, false).List.Where(o => (o.PupilProfileID == PupilProfileID))
                .Where(o => (o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)).FirstOrDefault();
            PupilProfile pp = PupilProfileBusiness.Find(PupilProfileID);
            ViewData[PupilProfileChildrenConstants.LISTENROLMENTDATE] = model.EnrolmentDate;
            model.PupilProfileID = PupilProfileID;
            if (pp.FatherBirthDate.HasValue)
            {
                model.iFatherBirthDate = pp.FatherBirthDate.Value.Year;
            }
            if (pp.MotherBirthDate.HasValue)
            {
                model.iMotherBirthDate = pp.MotherBirthDate.Value.Year;
            }
            if (pp.SponsorBirthDate.HasValue)
            {
                model.iSponsorBirthDate = pp.SponsorBirthDate.Value.Year;
            }
            if (pp.PolicyRegimeID != null)
            {
                model.PolicyRegimeID = pp.PolicyRegimeID;
            }
            if (pp.IsDisabled == true)
            {
                model.CheckIsDisabled = true;
            }
            else
            {
                model.CheckIsDisabled = false;
            }
            //load combobox
            IQueryable<ClassProfile> lsCp = getClassFromEducationLevel(model.EducationLevel);
            if (lsCp.Count() > 0)
            {
                ViewData[PupilProfileChildrenConstants.LISTCLASS] = new SelectList(lsCp.ToList(), "ClassProfileID", "DisplayName");
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.LISTCLASS] = new SelectList(new string[] { });
            }

            IQueryable<District> lsD = getDistrictFromProvide(model.ProvinceID);
            if (lsD.Count() > 0)
            {
                ViewData[PupilProfileChildrenConstants.LISTDISTRICT] = new SelectList(lsD.ToList(), "DistrictID", "DistrictName");
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.LISTDISTRICT] = new SelectList(new string[] { });
            }

            if (model.DistrictID.HasValue)
            {
                IQueryable<Commune> lsC = getCommuneFromDistrict(model.DistrictID.Value);
                if (lsC.Count() > 0)
                {
                    ViewData[PupilProfileChildrenConstants.LISTCOMMUNE] = new SelectList(lsC.ToList(), "CommuneID", "CommuneName");
                }
                else
                {
                    ViewData[PupilProfileChildrenConstants.LISTCOMMUNE] = new SelectList(new string[] { });
                }
            }
            if (model.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)
            {
                ViewData[PupilProfileChildrenConstants.PUPIL_NOT_STUDY] = false;
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.PUPIL_NOT_STUDY] = true;
            }
            if (pp.FullName != null)
                model.FullName = pp.FullName;

            if (pp.ClassProfile.DisplayName != null)
                model.ClassName = pp.ClassProfile.DisplayName;
            model.isFirst = isFirst;
            model.isEnd = isEnd;
            if (pp.MinorityMother.HasValue)
                model.MinorityMother = pp.MinorityMother.Value;
            if (pp.MinorityFather.HasValue)
                model.MinorityFather = pp.MinorityFather.Value;
            return View("_Edit", model);
        }

        public ActionResult GetDetailPupilProfile(int PupilProfileID, string typeOfActionResult = "view")
        {
            GlobalInfo global = new GlobalInfo();

            List<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", global.AcademicYearID } }).ToList();
            if (lstPoc != null && !lstPoc.Where(o => o.PupilID == PupilProfileID).Any())
            {
                return new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "Index" }));
            }
            //ListHaBitGroup
            IDictionary<string, object> HabitSearchInfo = new Dictionary<string, object>();
            HabitSearchInfo["IsActive"] = true;
            List<HabitGroup> lstHabitGroup = this.HabitGroupBusiness.Search(HabitSearchInfo).ToList();
            ViewData[PupilProfileChildrenConstants.LIST_HABITGROUP] = lstHabitGroup;

            List<HabitDetail> lstHabitDetail = this.HabitDetailBusiness.Search(HabitSearchInfo).ToList();
            ViewData[PupilProfileChildrenConstants.LIST_HABITDETAIL] = lstHabitDetail;

            List<TypeOfDish> lstTypeOfDish = this.TypeOfDishBusiness.Search(HabitSearchInfo).ToList();
            ViewData[PupilProfileChildrenConstants.LIST_TYPEOFDISH] = lstTypeOfDish;

            IDictionary<string, object> TypeOfDishOfPupilSearchInfo = new Dictionary<string, object>();
            TypeOfDishOfPupilSearchInfo["PupilID"] = PupilProfileID;
            List<FoodHabitOfChildren> LstFoodHabitOfChildren = this.FoodHabitOfChildrenBusiness.Search(TypeOfDishOfPupilSearchInfo).ToList();
            List<HabitOfChildren> LstHabitOfChildren = this.HabitOfChildrenBusiness.Search(TypeOfDishOfPupilSearchInfo).ToList();
            ViewData[PupilProfileChildrenConstants.LIST_TYPEOFDISHOFPUPIL] = LstFoodHabitOfChildren;
            ViewData[PupilProfileChildrenConstants.LIST_HABITDETAILOFPUPIL] = LstHabitOfChildren;


            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            //IQueryable<PupilProfileChildrenViewModel> lsPupilProfileViewModel = _Search(SearchInfo, false).AsQueryable();
            PupilProfileChildrenViewModel model = SearchPaging(SearchInfo, false, false).List.Where(o => (o.PupilProfileID == PupilProfileID)).FirstOrDefault();
            PupilProfile pp = PupilProfileBusiness.Find(PupilProfileID);

            if (pp.FatherBirthDate.HasValue)
            {
                model.iFatherBirthDate = pp.FatherBirthDate.Value.Year;
            }
            if (pp.MotherBirthDate.HasValue)
            {
                model.iMotherBirthDate = pp.MotherBirthDate.Value.Year;
            }
            if (pp.SponsorBirthDate.HasValue)
            {
                model.iSponsorBirthDate = pp.SponsorBirthDate.Value.Year;
            }
            model.BirthDate_String = model.BirthDate.Date.ToString().Substring(0, 10);

            if (model.OtherEthnicID != null)
            {
                OtherEthnic otherEthnic = OtherEthnicBusiness.Find(model.OtherEthnicID);
                model.OtherEthnicCode = otherEthnic.OtherEthnicCode;
                model.OtherEthnicName = otherEthnic.OtherEthnicName;
            }

            model.EnrolmentDate_string = model.EnrolmentDate.Date.ToString().Substring(0, 10);
            ViewData[PupilProfileChildrenConstants.PUPILPROFILE_MODEL_FOR_DETAIL] = model;

            #region tang truong thang - baolvt-
            // chức năng này bỏ, không dùng nữa
            //ViewGrowthChart(PupilProfileID);
            #endregion

            #region suc khoe dinh ky

            Dictionary<string, object> searchTearm = new Dictionary<string, object>() { { "PupilID", PupilProfileID } };
            IQueryable<AcademicYear> IqLevel = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, searchTearm).Select(poc => poc.AcademicYear).Distinct();
            AcademicYear ay = this.AcademicYearBusiness.Find(global.AcademicYearID);
            if (ay != null)
            {
                IqLevel = IqLevel.Where(a => a.Year <= ay.Year);
            }
            IqLevel = IqLevel.OrderByDescending(o => o.Year);
            List<SelectListItem> lstAcademicYearItem = new List<SelectListItem>();
            SelectListItem item;
            foreach (var academic in IqLevel)
            {
                item = new SelectListItem();
                item.Text = academic.Year + " - " + (academic.Year + 1);
                item.Value = academic.AcademicYearID.ToString();
                lstAcademicYearItem.Add(item);
            }
            ViewData[PupilProfileChildrenConstants.LIST_ACADEMICYEAR] = lstAcademicYearItem;

            int FirstHealthPeriodID = 0;
            List<HealthPeriod> lstHealthPeriod = getHealthPeriodFromAcademicYear(global.AcademicYearID.Value).ToList();
            if (lstHealthPeriod != null && lstHealthPeriod.Count > 0)
            {
                FirstHealthPeriodID = lstHealthPeriod.FirstOrDefault().HealthPeriodID;
            }
            ViewData[PupilProfileChildrenConstants.LIST_HEALTH_PERIODIC] = new SelectList(getHealthPeriodFromAcademicYear(global.AcademicYearID.Value).ToList(), "HealthPeriodID", "Resolution");
            GetPupilHealthPeriodic(global.AcademicYearID.Value, FirstHealthPeriodID, PupilProfileID);
            #endregion

            // Tạo giá trị lưu log action_audit
            SetViewDataActionAudit(String.Empty, String.Empty, PupilProfileID.ToString(), "View pupil_profile_id:" + PupilProfileID, PupilProfileID.ToString(), "Hồ sơ trẻ", GlobalConstants.ACTION_VIEW, "Xem hồ trẻ " + pp.FullName + " mã " + pp.PupilCode);

            if (typeOfActionResult == "view") return View("Detail");
            else return PartialView("DetailPartialView");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateOrder(int[] PupilProfileID, int?[] OrderInClass, int[] ClassID)
        {
            GlobalInfo global = new GlobalInfo();

            List<int> lsPupilID = new List<int>();
            List<int> lsOrder = new List<int>();
            for (int i = 0; i < OrderInClass.Count(); i++)
            {
                if (OrderInClass[i] != null)
                {
                    lsOrder.Add(OrderInClass[i].Value);
                    lsPupilID.Add(PupilProfileID[i]);
                }
                if (OrderInClass[i] == 0)
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_OrderZeroError"), "error"));
                }
            }
            if (lsOrder.Count() > 0)
            {
                PupilOfClassBusiness.UpdateOrder(global.UserAccountID, ClassID[0], lsPupilID, lsOrder);
                SetViewData(ClassID[0]);
                return Json(new JsonMessage(Res.Get("PupilProfile_Label_OrderSuccess")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_NoOrderError"), "error"));
            }
        }
        #endregion

        #region Delete pupil
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DeletePupil(int pupilid)
        {
            PupilProfile pupilProfile = PupilProfileBusiness.Find(pupilid);
            PupilProfileBusiness.DeletePupilProfileChildren(_globalInfo.UserAccountID, pupilid, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            PupilProfileBusiness.Save();
            // Tạo giá trị lưu log action_audit
            SetViewDataActionAudit("{\"PupilProfileID\":\"" + pupilid + "\",\"PupilCode\":\"" + pupilProfile.PupilCode + "\"}", String.Empty, pupilid.ToString(), "Delete pupil_profile_id:" + pupilid, pupilid.ToString(), "Hồ sơ trẻ", GlobalConstants.ACTION_DELETE, "Xóa trẻ " + pupilProfile.FullName + " mã " + pupilProfile.PupilCode);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Xoa cac hoc sinh co check chon
        /// </summary>
        /// <param name="checkedDelete"></param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DeleteCheckPupil(int[] checkedDelete)
        {
            // Tạo giá trị ghi log
            List<int> listID = checkedDelete.ToList();
            List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => listID.Contains(o.PupilProfileID)).ToList();
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int schoolId = _globalInfo.SchoolID.Value;
            //Luu thong tin phuc vu viec phuc hoi du lieu
            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = academicYearId,
                SCHOOL_ID = schoolId,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_PUPIL,
                SHORT_DESCRIPTION = "Xóa học sinh",
            };
            PupilProfileBusiness.DeleteListPupilProfileChildren(_globalInfo.UserAccountID, checkedDelete.ToList(), _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, objRes);
            PupilProfileBusiness.Save();

            // Tạo giá trị ghi log
            StringBuilder oldObject = new StringBuilder();
            StringBuilder objectID = new StringBuilder();
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            for (int i = 0, size = listPupilProfile.Count; i < size; i++)
            {
                oldObject.Append("{\"PupilProfileID\":\"" + listPupilProfile[i].PupilProfileID.ToString() + "\",\"PupilCode\":\"" + listPupilProfile[i].PupilCode + "\"}");
                objectID.Append(listPupilProfile[i].PupilProfileID.ToString());
                descriptionObject.Append("Delete pupil_profile_id:" + listPupilProfile[i].PupilProfileID.ToString());
                paramObject.Append(listPupilProfile[i].PupilProfileID.ToString());
                userFunctions.Append("Hồ sơ trẻ");
                userActions.Append(GlobalConstants.ACTION_DELETE);
                userDescriptions.Append("Xóa trẻ " + listPupilProfile[i].FullName + " mã " + listPupilProfile[i].PupilCode);
                if (i < size - 1)
                {
                    oldObject.Append(GlobalConstants.WILD_LOG);
                    descriptionObject.Append(GlobalConstants.WILD_LOG);
                    paramObject.Append(GlobalConstants.WILD_LOG);
                    objectID.Append(GlobalConstants.WILD_LOG);
                    userFunctions.Append(GlobalConstants.WILD_LOG);
                    userActions.Append(GlobalConstants.WILD_LOG);
                    userDescriptions.Append(GlobalConstants.WILD_LOG);
                }
            }
            SetViewDataActionAudit(oldObject.ToString(), String.Empty, objectID.ToString(), descriptionObject.ToString(), paramObject.ToString(), userFunctions.ToString(), userActions.ToString(), userDescriptions.ToString());

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region Class movement
        [HttpGet]
        public PartialViewResult ClassMovement(int id, int classid)
        {
            CheckPermissionForAction(id, "PupilProfile", 3);
            CheckPermissionForAction(classid, "ClassProfile");
            GlobalInfo globalInfo = new GlobalInfo();
            ClassMovementModel model = new ClassMovementModel();

            PupilProfile pupilProfile = PupilProfileBusiness.Find(id);
            ClassProfile classProfile = ClassProfileBusiness.Find(classid);
            bool check = (pupilProfile.CurrentClassID == classid) ? true : false;
            AcademicYear aca = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);


            model.PupilID = id;
            model.FromClassID = classid;
            model.ClassName = classProfile.DisplayName;
            model.EducationLevelID = classProfile.EducationLevelID;
            model.FullName = pupilProfile.FullName;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", globalInfo.AcademicYearID.Value);
            dic.Add("EducationLevelID", pupilProfile.ClassProfile.EducationLevelID);
            List<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).Where(o => o.ClassProfileID != model.FromClassID).OrderBy(o => o.DisplayName).ToList();
            ViewData["LISTCLASSTO"] = new SelectList(lsClass, "ClassProfileID", "DisplayName");
            ViewData["LISTSEMESTER"] = new SelectList(CommonList.Semester(), "key", "value");
            ViewData["SEMESTER_ID"] = _globalInfo.Semester.HasValue ? _globalInfo.Semester.Value : -1;
            IDictionary<string, object> dicPupilOfClass = new Dictionary<string, object>();
            dicPupilOfClass["PupilID"] = id;
            dicPupilOfClass["Status"] = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS;
            dicPupilOfClass.Add("AcademicYearID", globalInfo.AcademicYearID.Value);
            dicPupilOfClass.Add("ClassID", classid);
            PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, dicPupilOfClass).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (poc != null && check == false)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = poc.ClassProfile.EducationLevelID;
                SearchInfo["FromClassID"] = poc.ClassID;
                SearchInfo["PupilID"] = poc.PupilID;

                ClassMovement classMovement = ClassMovementBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo).OrderByDescending(u => u.MovedDate).FirstOrDefault();
                if (classMovement != null) //neu hoc sinh nay da chuyen lop
                {
                    model.ClassMovementID = classMovement.ClassMovementID;
                    model.MovedDate1 = classMovement.MovedDate;
                    if (model.MovedDate1 > aca.FirstSemesterStartDate)
                    {
                        model.Semester = 1;
                    }
                    if (model.MovedDate1 > aca.SecondSemesterStartDate)
                    {
                        model.Semester = 2;
                    }
                    model.Reason = classMovement.Reason;
                    model.ToClassID = classMovement.ToClassID;
                }
            }

            if (model.ClassMovementID <= 0)
                model.MovedDate1 = DateTime.Now;
            if (model.ClassMovementID <= 0 || check == true)
            {
                return PartialView("_ClassMovement", model);
            }
            else
            {
                return PartialView("_ClassMovementEdit", model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult ClassMovement(ClassMovementModel model)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            List<ClassMovement> lstClassMovement = new List<ClassMovement>();
            Utils.Utils.TrimObject(model);
            GlobalInfo globalInfo = new GlobalInfo();

            ClassMovement classMovement = null;
            IDictionary<string, object> dicPupilOfClass = new Dictionary<string, object>();
            dicPupilOfClass["PupilID"] = model.PupilID;
            dicPupilOfClass["Status"] = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS;
            dicPupilOfClass.Add("AcademicYearID", globalInfo.AcademicYearID.Value);
            dicPupilOfClass.Add("ClassID", model.FromClassID);
            PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, dicPupilOfClass).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (poc != null)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = ClassProfileBusiness.AllNoTracking.First(o => o.ClassProfileID == poc.ClassID && o.IsActive.Value).EducationLevelID;
                SearchInfo["FromClassID"] = poc.ClassID;
                SearchInfo["PupilID"] = poc.PupilID;
                classMovement = ClassMovementBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo).OrderByDescending(u => u.MovedDate).FirstOrDefault();
            }

            if (classMovement == null)
                classMovement = new ClassMovement();

            Utils.Utils.BindTo(model, classMovement);

            classMovement.AcademicYearID = globalInfo.AcademicYearID.Value;
            classMovement.SchoolID = globalInfo.SchoolID.Value;
            classMovement.MovedDate = model.MovedDate1;
            lstClassMovement.Add(classMovement);
            using (TransactionScope tran = new TransactionScope())
            {
                if (classMovement.ClassMovementID > 0) //Update
                    this.ClassMovementBusiness.UpdateClassMovement(globalInfo.UserAccountID, classMovement);
                else //Create
                {
                    this.ClassMovementBusiness.InsertClassMovement(globalInfo.UserAccountID, lstClassMovement, model.isClassMovement);
                }

                this.ClassMovementBusiness.Save();
                tran.Complete();
            }

            return Json(new JsonMessage(Res.Get("ClassMovement_Label_AddNewMessage")));
        }

        [HttpPost]
        public string DisplayAlert(int? pupilID, DateTime? movedDate, int? fromClassID, int? toclassID)
        {
            GlobalInfo global = new GlobalInfo();
            int schoolID = global.SchoolID.Value;
            int academicYearID = global.AcademicYearID.Value;
            //tạo dữ liệu add vào dic 
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = pupilID;
            dic["AcademicYearID"] = academicYearID;
            dic["MovedDate"] = movedDate;
            ClassMovement classMove = ClassMovementBusiness.SearchBySchool(schoolID, dic).FirstOrDefault();

            IDictionary<string, object> search = new Dictionary<string, object>();
            search["PupilID"] = pupilID;
            search["SchoolID"] = schoolID;
            search["ClassID"] = fromClassID;
            search["AcademicYearID"] = academicYearID;
            PupilOfClass item = PupilOfClassBusiness.SearchBySchool(schoolID, search).OrderByDescending(o => o.AssignedDate).FirstOrDefault();
            string output = "true";
            if (classMove == null)
            {
                output = "true";
            }
            else if (classMove.FromClassID != toclassID) //Chuyen trong 1 ngày
            {
                //Từ a => b, b => c

                if (item != null)
                {
                    output = "false";
                }
                else
                {
                    output = "true";
                }
            }
            //Chuyển trong cùng 1 ngày từ a => b, b => a
            else if (classMove.FromClassID == toclassID)
            {
                output = "false";
            }
            // Nghiep vu moi la luon hien thi confirm. Do do o day gan output = true
            output = "true";
            return output;
        }
        #endregion

        #region School Movement
        [HttpGet]
        public PartialViewResult SchoolMovement(int id)
        {
            CheckPermissionForAction(id, "PupilProfile", 3);

            GlobalInfo glo = new GlobalInfo();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(glo.AcademicYearID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = glo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchInfo["PupilID"] = id;
            SchoolMovement schoolMovement = SchoolMovementBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo).OrderByDescending(u => u.MovedDate).FirstOrDefault();

            SchoolMovementModel model = new SchoolMovementModel();

            if (schoolMovement != null)
            {
                Utils.Utils.BindTo(schoolMovement, model);
                model.FullName = schoolMovement.PupilProfile.FullName;
            }
            else
            {
                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value
                                                                , new Dictionary<string, object> {
                                                                    {"PupilID", id },
                                                                    {"SchoolID", glo.SchoolID.Value},
                                                                    {"AcademicYearID", glo.AcademicYearID.Value},
                                                                    {"Status", (new int[] {SystemParamsInFile.PUPIL_STATUS_STUDYING } ).ToList() }
                                                                }).SingleOrDefault();
                if (poc == null)
                    throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");

                model.PupilID = poc.PupilID;
                model.ClassID = poc.ClassID;
                model.EducationLevelID = poc.ClassProfile.EducationLevelID;
                model.FullName = poc.PupilProfile.FullName;
                model.MovedDate = DateTime.Now;
            }

            ViewData[PupilProfileChildrenConstants.LISTPROVINCE] = new SelectList(ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).Select(u => new { u.ProvinceID, u.ProvinceName }).ToList(), "ProvinceID", "ProvinceName", model.MovedToProvinceID);
            ViewData[PupilProfileChildrenConstants.LISTSEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
            ViewData[PupilProfileChildrenConstants.SEMESTER_CHECKED] = this.GetSemesterChecked(objAcademicYear);

            return PartialView("_SchoolMovement", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult SchoolMovement(SchoolMovementModel model)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            GlobalInfo glo = new GlobalInfo();
            SchoolMovement sm = new SchoolMovement();
            if (model.SchoolMovementID.HasValue && model.SchoolMovementID.Value > 0)
            {
                sm = SchoolMovementBusiness.Find(model.SchoolMovementID.Value);

                if (sm.PupilID != model.PupilID)
                    return Json(new JsonMessage(Res.Get("Common_Validate_NotCompatible"), JsonMessage.ERROR));

                if (sm.MovedToClassID.HasValue)
                    return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_PupilIsMoved"), JsonMessage.ERROR));
            }

            sm.AcademicYearID = glo.AcademicYearID.Value;
            sm.SchoolID = glo.SchoolID.Value;
            sm.Description = model.Description;
            sm.MovedDate = model.MovedDate;
            sm.Semester = model.Semester;
            sm.EducationLevelID = model.EducationLevelID;
            sm.ClassID = model.ClassID;
            sm.PupilID = model.PupilID;

            if (model.SchoolType == PupilProfileChildrenConstants.SCHOOL_MOVEMENT_IN_SYSTEM)
            {
                sm.MovedToProvinceID = model.MovedToProvinceID;
                sm.MovedToDistrictID = model.MovedToDistrictID;
                sm.MovedToSchoolID = model.MovedToSchoolID;
                sm.MovedToClassID = null;

                sm.MovedToClassName = null;
                sm.MovedToSchoolAddress = null;
                sm.MovedToSchoolName = null;
            }
            else
            {
                sm.MovedToProvinceID = null;
                sm.MovedToDistrictID = null;
                sm.MovedToSchoolID = null;
                sm.MovedToClassID = null;

                sm.MovedToClassName = model.MovedToClassName;
                sm.MovedToSchoolAddress = model.MovedToSchoolAddress;
                sm.MovedToSchoolName = model.MovedToSchoolName;
            }

            if (model.SchoolMovementID.HasValue && model.SchoolMovementID.Value > 0)
            {
                SchoolMovementBusiness.UpdateSchoolMovement(glo.UserAccountID, sm);
            }
            else
            {
                SchoolMovementBusiness.InsertSchoolMovement(glo.UserAccountID, sm);
            }

            SchoolMovementBusiness.Save();


            return Json(new JsonMessage(Res.Get("SchoolMovement_Label_UpdateSuccess")));
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSchoolMovement(SearchViewModel frm)
        {
            GlobalInfo glo = new GlobalInfo();

            Dictionary<int, List<ComboObject>> dicClassByEdu = new Dictionary<int, List<ComboObject>>();
            var lstClassProfile = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> {
                                                                                            { "AcademicYearID", glo.AcademicYearID }
                                                                                        }).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName)
                                                                                        .Select(u => new { u.ClassProfileID, u.EducationLevelID, u.DisplayName })
                                                                                        .ToList();
            glo.EducationLevels.ForEach(u =>
            {
                dicClassByEdu[u.EducationLevelID] = lstClassProfile.Where(v => v.EducationLevelID == u.EducationLevelID).Select(v => new ComboObject { key = v.ClassProfileID.ToString(), value = v.DisplayName }).ToList();
            });

            ViewData[PupilProfileChildrenConstants.DIC_CLASS_BY_EDUCATION_LEVEL] = dicClassByEdu;

            AcademicYear acaYear = AcademicYearBusiness.Find(glo.AcademicYearID);
            int curYear = acaYear.Year;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = glo.AppliedLevel;
            SearchInfo["MovedToSchoolID"] = glo.SchoolID.Value;
            SearchInfo["MovedToClassID"] = frm.Class;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["Genre"] = frm.Genre;

            var query = from sm in SchoolMovementBusiness.Search(SearchInfo)
                        join pp in PupilProfileBusiness.All on sm.PupilID equals pp.PupilProfileID
                        join sp in SchoolProfileBusiness.All on sm.SchoolID equals sp.SchoolProfileID
                        join edu in EducationLevelBusiness.All on sm.EducationLevelID equals edu.EducationLevelID
                        join aca in AcademicYearBusiness.All on sm.AcademicYearID equals aca.AcademicYearID
                        join cp in ClassProfileBusiness.All on sm.MovedToClassID equals cp.ClassProfileID into g1
                        from j1 in g1.DefaultIfEmpty()
                        where sm.MovedDate >= acaYear.FirstSemesterStartDate && sm.MovedDate <= acaYear.SecondSemesterEndDate && !sm.MovedToClassID.HasValue
                        && (!j1.IsActive.HasValue || (j1.IsActive.HasValue && j1.IsActive.Value))
                        select new SchoolMovementViewModel
                        {
                            SchoolMovementID = sm.SchoolMovementID,
                            EducationLevelID = sm.EducationLevelID,
                            EducationLevel = edu.Resolution,
                            PupilID = sm.PupilID,
                            Semester = sm.Semester,
                            FullName = pp.FullName,
                            Name = pp.Name,
                            PupilCode = pp.PupilCode,
                            BirthDate = pp.BirthDate,
                            Genre = pp.Genre,
                            FromSchoolID = sm.SchoolID,
                            FromSchoolName = sp.SchoolName,
                            ClassName = j1.DisplayName,
                            ClassOrderNumber = j1.OrderNumber
                        };

            List<SchoolMovementViewModel> lstSchoolMovement = query.OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            lstSchoolMovement.ForEach(u =>
            {
                u.GenreDisplay = u.Genre == SystemParamsInFile.GENRE_MALE
                                                ? Res.Get("Common_Label_Male")
                                                : (u.Genre == SystemParamsInFile.GENRE_FEMALE ? Res.Get("Common_Label_Female") : string.Empty);
                u.SemesterDisplay = u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                ? Res.Get("Semester_Of_Year_First")
                                                : (u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND ? Res.Get("Semester_Of_Year_Second") : string.Empty);
            });

            ViewData[PupilProfileChildrenConstants.LIST_SCHOOL_MOVEMENT] = lstSchoolMovement;

            return PartialView("_ListSchoolMovement");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ApproveSchoolMovement()
        {

            GlobalInfo glo = new GlobalInfo();
            if (!UtilsBusiness.IsBGH(glo.UserAccountID) && !glo.IsAdminSchoolRole)
                return Json(new JsonMessage(Res.Get("Common_Label_NotHasPermission"), JsonMessage.ERROR));

            IEnumerable<string> lstCbo = Request.Params.AllKeys.Where(u => u.StartsWith("cboSchoolMovement_"));

            var lstClassProfile = ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> {
                                                                                            { "AcademicYearID", glo.AcademicYearID }
                                                                                        })
                                                                                        .Select(u => new { u.ClassProfileID, u.EducationLevelID, u.DisplayName })
                                                                                        .ToList();

            int classID = 0;
            int schoolMovementID = 0;

            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);

            using (TransactionScope transactionScope = new TransactionScope())
            {
                foreach (string sltClass in lstCbo)
                {
                    if (int.TryParse(sltClass.Replace("cboSchoolMovement_", "").Trim(), out schoolMovementID) && schoolMovementID > 0 //Lay SchoolMovementID
                        && int.TryParse(Request.Params.Get(sltClass), out classID) && classID > 0)                                              //Lay ClassID - Lop nhan hoc sinh nay
                    {
                        SchoolMovement schoolMovement = SchoolMovementBusiness.Find(schoolMovementID);
                        MovementAcceptance movementAcceptance = new MovementAcceptance();
                        movementAcceptance.AcademicYearID = glo.AcademicYearID.Value;
                        movementAcceptance.ClassID = classID;
                        movementAcceptance.Description = string.Empty;
                        movementAcceptance.EducationLevelID = lstClassProfile.Where(u => u.ClassProfileID == classID).FirstOrDefault().EducationLevelID;
                        movementAcceptance.MovedDate = DateTime.Now;
                        movementAcceptance.MovedFromClassID = schoolMovement.ClassID;
                        movementAcceptance.MovedFromClassName = string.Empty;
                        movementAcceptance.MovedFromSchoolID = schoolMovement.SchoolID;
                        movementAcceptance.MovedFromSchoolName = string.Empty;
                        movementAcceptance.PupilID = schoolMovement.PupilID;
                        movementAcceptance.SchoolID = glo.SchoolID.Value;
                        movementAcceptance.Semester = schoolMovement.Semester;
                        this.MovementAcceptanceBusiness.InsertMovementAcceptance(glo.UserAccountID, movementAcceptance, isMovedHistory);
                        schoolMovement.MovedToClassID = classID;
                        SchoolMovementBusiness.Update(schoolMovement);
                    }
                }

                this.SchoolMovementBusiness.Save();
                transactionScope.Complete();
            }
            return Json(new JsonMessage(Res.Get("PupilProfile_Label_MovementAcceptanceSuccess")));
        }
        #endregion

        #region LeavingOff
        [HttpGet]
        public PartialViewResult LeavingOffPupil(int id)
        {
            CheckPermissionForAction(id, "PupilProfile", 3);
            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = glo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchInfo["PupilID"] = id;
            PupilLeavingOff pupilLeavingOff = PupilLeavingOffBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo).OrderByDescending(u => u.LeavingDate).FirstOrDefault();

            LeavingOffModel model = new LeavingOffModel();

            if (pupilLeavingOff != null)
            {
                Utils.Utils.BindTo(pupilLeavingOff, model);
                model.FullName = pupilLeavingOff.PupilProfile.FullName;
            }
            else
            {
                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value
                                                                , new Dictionary<string, object> {
                                                                    {"PupilID", id },
                                                                    {"SchoolID", glo.SchoolID.Value},
                                                                    {"AcademicYearID", glo.AcademicYearID.Value},
                                                                    {"Status", (new int[] {SystemParamsInFile.PUPIL_STATUS_STUDYING } ).ToList() }
                                                                }).SingleOrDefault();
                if (poc == null)
                    throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");

                model.PupilID = poc.PupilID;
                model.ClassID = poc.ClassID;
                model.EducationLevelID = poc.ClassProfile.EducationLevelID;
                model.FullName = poc.PupilProfile.FullName;
                model.LeavingDate = DateTime.Now;
            }
            ViewData[PupilProfileChildrenConstants.LISTLEAVINGREASON] = new SelectList(LeavingReasonBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.LeavingReasonID, u.Resolution }).ToList(), "LeavingReasonID", "Resolution", model.LeavingReasonID);
            // Chon hoc ky cho combobox hoc ky
            AcademicYear ay = this.AcademicYearBusiness.Find(glo.AcademicYearID);
            int selectedSemester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
            if (ay.SecondSemesterStartDate.Value.Date <= model.LeavingDate.Date)
            {
                selectedSemester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            }
            // Tao danh sach hoc ky cho combobox hoc ky
            var listSemesters = new SelectList(CommonList.Semester(), "key", "value", selectedSemester);
            ViewData[PupilProfileChildrenConstants.LISTSEMESTER] = listSemesters;
            return PartialView("_LeavingOffPupil", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult LeavingOffPupil(LeavingOffModel model)
        {
            CheckPermissionForAction(model.PupilID, "PupilProfile", 3);
            GlobalInfo glo = new GlobalInfo();
            PupilLeavingOff pOff = new PupilLeavingOff();
            if (model.PupilLeavingOffID.HasValue && model.PupilLeavingOffID.Value > 0)
            {
                pOff = PupilLeavingOffBusiness.Find(model.PupilLeavingOffID.Value);

                if (pOff.PupilID != model.PupilID)
                    return Json(new JsonMessage(Res.Get("Common_Validate_NotCompatible"), JsonMessage.ERROR));
            }

            pOff.AcademicYearID = glo.AcademicYearID.Value;
            pOff.SchoolID = glo.SchoolID.Value;
            pOff.Description = model.Description;
            pOff.LeavingDate = model.LeavingDate;
            pOff.Semester = model.Semester;
            pOff.EducationLevelID = model.EducationLevelID;
            pOff.ClassID = model.ClassID;
            pOff.PupilID = model.PupilID;
            pOff.LeavingReasonID = model.LeavingReasonID;

            if (model.PupilLeavingOffID.HasValue && model.PupilLeavingOffID.Value > 0)
            {
                PupilLeavingOffBusiness.UpdatePupilLeavingOff(glo.UserAccountID, pOff);
            }
            else
            {
                PupilLeavingOffBusiness.InsertPupilLeavingOff(glo.UserAccountID, pOff);
            }

            PupilLeavingOffBusiness.Save();

            return Json(new JsonMessage(Res.Get("PupilLeavingOff_Label_UpdateSuccess")));
        }
        #endregion

        #region Bieu do tang truong
        /// <summary>
        /// Hien thi bieu do tang truong
        /// </summary>
        /// <param name="pupilID"></param>
        public void ViewGrowthChart(int pupilID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            PupilProfile objPupilProfile = PupilProfileBusiness.Find(pupilID);
            ViewData[PupilProfileChildrenConstants.PUPIL_ID] = pupilID;
            if (objPupilProfile != null && objPupilProfile.CurrentSchoolID == _globalInfo.SchoolID)
            {
                dic["pupilID"] = pupilID;
                dic["birthday"] = objPupilProfile.BirthDate;

                //lay du lieu can nang chieu cao cua tre
                List<MonthlyGrowthBO> lstMonthlyGrowthBO = MonitoringBookBusiness.GetAllMonitoringBookByPupilID(dic).ToList();

                //ViewData[PupilProfileChildrenConstants.LIST_MONITORINGBOOK] = lstMonirotingBook;

                //kiem tra so kham de lay ra tieu su benh ban than gan nhat
                ViewData[PupilProfileChildrenConstants.HISTORY_YOUR_SELF] = string.Empty;
                if (lstMonthlyGrowthBO != null && lstMonthlyGrowthBO.Count > 0)
                {
                    for (int i = lstMonthlyGrowthBO.Count - 1; i > 0; i--)
                    {
                        if (string.IsNullOrEmpty(lstMonthlyGrowthBO[i].HistoryYourSelf))
                        {
                            ViewData[PupilProfileChildrenConstants.HISTORY_YOUR_SELF] = lstMonthlyGrowthBO[i].HistoryYourSelf;
                            break;
                        }
                    }
                }
                //fullname
                ViewData[PupilProfileChildrenConstants.FULL_NAME] = objPupilProfile.FullName;
                //birthday
                ViewData[PupilProfileChildrenConstants.BIRTH_DAY] = objPupilProfile.BirthDate;
                switch (objPupilProfile.Genre)
                {
                    case GlobalConstants.GENRE_FEMALE:
                        {
                            ViewData[PupilProfileChildrenConstants.GENRE] = GlobalConstants.GENRE_FEMALE_TITLE;
                            break;
                        }
                    case GlobalConstants.GENRE_MALE:
                        {
                            ViewData[PupilProfileChildrenConstants.GENRE] = GlobalConstants.GENRE_MALE_TITLE;
                            break;
                        }
                    default:
                        break;
                }
                //className
                ViewData[PupilProfileChildrenConstants.CLASS_NAME] = objPupilProfile.ClassProfile.DisplayName;

                #region tao du lieu ve chart
                //children chi co 60 thang tuoi
                List<MonthlyGrowthBO> lstMonthlyGrowthBOChart = new List<MonthlyGrowthBO>();
                MonthlyGrowthBO objMonthlyGrowthBO = null;
                for (int i = 0; i <= 60; i++)
                {
                    objMonthlyGrowthBO = new MonthlyGrowthBO();
                    for (int j = lstMonthlyGrowthBO.Count - 1; j >= 0; j--)
                    {
                        if (lstMonthlyGrowthBO[j].Month == i)
                        {
                            objMonthlyGrowthBO.Weight = lstMonthlyGrowthBO[j].Weight;
                            objMonthlyGrowthBO.Height = lstMonthlyGrowthBO[j].Height;
                        }
                    }
                    objMonthlyGrowthBO.Month = i;
                    lstMonthlyGrowthBOChart.Add(objMonthlyGrowthBO);
                }
                ViewData[PupilProfileChildrenConstants.CLASS_ID] = objPupilProfile.CurrentClassID;
                ViewData[PupilProfileChildrenConstants.LIST_CHART] = lstMonthlyGrowthBOChart;
                ViewData[PupilProfileChildrenConstants.CLASSID_SELECT] = objPupilProfile.CurrentClassID;
            }
            else
            {
                ViewData[PupilProfileChildrenConstants.HISTORY_YOUR_SELF] = string.Empty;
                //fullname
                ViewData[PupilProfileChildrenConstants.FULL_NAME] = string.Empty;
                //birthday
                ViewData[PupilProfileChildrenConstants.BIRTH_DAY] = string.Empty;
                //ClassID
                ViewData[PupilProfileChildrenConstants.CLASS_ID] = string.Empty;
                //classNaem
                ViewData[PupilProfileChildrenConstants.CLASS_NAME] = string.Empty;
                ViewData[PupilProfileChildrenConstants.LIST_CHART] = new List<MonthlyGrowthBO>();
            }
                #endregion
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult GetPupilHealthPeriodic(int AcademicYearID, int HealthPeriodID, int PupilProfileID)
        {
            IQueryable<MonitoringBook> iqMonitoringBook = MonitoringBookBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() {
                                                                                        { "AcademicYearID", AcademicYearID },
                                                                                        { "PupilID", PupilProfileID },
                                                                                        { "MonitoringType", GlobalConstants.MONITORINGBOOK_TYPE_PERIOD },
                                                                                        { "HealthPeriodID", HealthPeriodID }
                                                            }).OrderByDescending(o => o.MonitoringDate);
            PupilHealthPeriodicViewModel model = new PupilHealthPeriodicViewModel();
            if (iqMonitoringBook.Count() > 0)
            {
                MonitoringBook monitoringBook = iqMonitoringBook.FirstOrDefault();

                if (monitoringBook != null)
                {
                    model = new PupilHealthPeriodicViewModel();
                    model.PupilID = monitoringBook.PupilID;
                    model.ClassID = monitoringBook.ClassID;
                    model.BirthDate = monitoringBook.PupilProfile.BirthDate.ToString("dd/MM/yyyy");
                    model.FullName = monitoringBook.PupilProfile.FullName;
                    model.ClassName = monitoringBook.ClassProfile.DisplayName;
                    model.GenreName = CommonList.GenreAndSelect().Where(u => u.key.Equals(monitoringBook.PupilProfile.Genre.ToString())).FirstOrDefault().value;
                    model.DentalTest = monitoringBook.DentalTests.FirstOrDefault();
                    model.ENTTest = monitoringBook.ENTTests.FirstOrDefault();
                    model.EyeTest = monitoringBook.EyeTests.FirstOrDefault();
                    model.OverallTest = monitoringBook.OverallTests.FirstOrDefault();
                    model.PhysicalTest = monitoringBook.PhysicalTests.FirstOrDefault();
                    model.SpineTest = monitoringBook.SpineTests.FirstOrDefault();
                }
            }
            ViewData[PupilProfileChildrenConstants.DETAIL_HEALTH_PERIODIC_PUPIL] = model;

            return PartialView("_DetailPeriodicHealth");
        }

        #endregion

        #region Ajax - Load Data
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadDistrict(int? ProvinceID)
        {
            if (ProvinceID.HasValue)
            {
                IQueryable<District> lsD = getDistrictFromProvide(ProvinceID.Value);
                if (lsD.Count() != 0)
                {
                    List<District> lstDistrict = lsD.ToList();
                    lstDistrict = lstDistrict.OrderBy(o => o.DistrictName).ToList();
                    return Json(new SelectList(lstDistrict, "DistrictID", "DistrictName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadCommune(int? DistrictID)
        {
            if (DistrictID.HasValue)
            {
                IQueryable<Commune> lsC = getCommuneFromDistrict(DistrictID.Value);
                if (lsC.Count() != 0)
                {
                    List<Commune> lstCommune = lsC.ToList();
                    lstCommune = lstCommune.OrderBy(o => o.CommuneName).ToList();
                    return Json(new SelectList(lstCommune, "CommuneID", "CommuneName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        public JsonResult AjaxLoadOtherEthnic(int? EthnicID)
        {
            if (EthnicID.HasValue)
            {
                IQueryable<OtherEthnic> lsC = OtherEthnicBusiness.All.Where(x => x.EthnicID == EthnicID);
                if (lsC.Count() != 0)
                {
                    List<OtherEthnic> lstOtherEthnic = lsC.ToList();
                    lstOtherEthnic = lstOtherEthnic.OrderBy(o => o.OtherEthnicName).ToList();
                    return Json(new SelectList(lstOtherEthnic, "OtherEthnicID", "OtherEthnicName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSchool(int? DistrictId)
        {
            List<SchoolProfile> lstSchool = new List<SchoolProfile>();
            if (DistrictId.HasValue && DistrictId.Value > 0)
            {
                GlobalInfo glo = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["DistrictID"] = DistrictId.Value;
                dic["IsActive"] = true;
                dic["AppliedLevel"] = glo.AppliedLevel.Value;
                //dungnt fix bug 168941
                lstSchool = this.SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName).Where(o => o.SchoolProfileID != glo.SchoolID).ToList();
            }
            return Json(new SelectList(lstSchool, "SchoolProfileID", "SchoolName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPeriod(int? AcademicYearID)
        {
            List<HealthPeriod> lstPeriodic = new List<HealthPeriod>();
            if (AcademicYearID.HasValue && AcademicYearID.Value > 0)
            {
                lstPeriodic = getHealthPeriodFromAcademicYear(AcademicYearID.Value).ToList();
            }
            return Json(new SelectList(lstPeriodic, "HealthPeriodID", "Resolution"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    if (EducationLevelID.Value == SystemParamsInFile.Combine_Class_ID)
                    {
                        var lstCP = (from lst in lsCP
                                     select new { CombinedClassCode = lst.CombinedClassCode }).Distinct().OrderBy(x => x.CombinedClassCode).ToList();

                        return Json(new SelectList(lstCP, "CombinedClassCode", "CombinedClassCode"));
                    }

                    return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClassCreate(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevelCreate(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }
        #endregion

        #region Utils
        /// <summary>
        /// Kiem tra cac so dien thoai co hop le hay khong
        /// </summary>
        /// <param name="PupilProfileViewModel"></param>
        private void CheckTelephoneNumber(PupilProfileChildrenViewModel PupilProfileViewModel)
        {
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Kiem tra cac so dien thoai chi chua so hay thong
            System.Text.RegularExpressions.Regex regexIsDigetOnly
                = new System.Text.RegularExpressions.Regex("^[0-9]+$", System.Text.RegularExpressions.RegexOptions.Compiled);
            // So dien thoai nha
            if (!string.IsNullOrWhiteSpace(PupilProfileViewModel.Telephone))
            {
                if (!regexIsDigetOnly.IsMatch(PupilProfileViewModel.Telephone))
                {
                    throw new Exception(Res.Get("Invalid_SDT_Nha"));
                }
            }
            if (!string.IsNullOrWhiteSpace(PupilProfileViewModel.FatherMobile))
            {
                if (!regexIsDigetOnly.IsMatch(PupilProfileViewModel.FatherMobile))
                {
                    throw new Exception(Res.Get("Invalid_SDT_Cha"));
                }
            }
            if (!string.IsNullOrWhiteSpace(PupilProfileViewModel.MotherMobile))
            {
                if (!regexIsDigetOnly.IsMatch(PupilProfileViewModel.MotherMobile))
                {
                    throw new Exception(Res.Get("Invalid_SDT_Me"));
                }
            }
            if (!string.IsNullOrWhiteSpace(PupilProfileViewModel.SponsorMobile))
            {
                if (!regexIsDigetOnly.IsMatch(PupilProfileViewModel.SponsorMobile))
                {
                    throw new Exception(Res.Get("Invalid_SDT_Nguoi_bao_ho"));
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        private IQueryable<District> getDistrictFromProvide(int ProvinceID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",ProvinceID},
                };
            IQueryable<District> lsD = DistrictBusiness.Search(dic);
            lsD = lsD.OrderBy(o => o.DistrictName);
            return lsD;
        }

        private IQueryable<Commune> getCommuneFromDistrict(int DistrictID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"DistrictID",DistrictID},
                };
            IQueryable<Commune> lsC = CommuneBusiness.Search(dic);
            lsC = lsC.OrderBy(o => o.CommuneName);
            return lsC;
        }

        private IQueryable<HealthPeriod> getHealthPeriodFromAcademicYear(int AcademicYearID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            IQueryable<HealthPeriod> lstPeriodic = this.MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dic).Where(o => o.HealthPeriod != null)
                .Select(o => o.HealthPeriod).Distinct();

            return lstPeriodic;
        }
        private int GetSemesterChecked(AcademicYear objAcademicYear)
        {
            int SemesterChecked = 0;
            DateTime DateTimeNow = DateTime.Now;
            if (DateTimeNow.Date <= objAcademicYear.SecondSemesterEndDate.Value.Date)
            {
                if ((objAcademicYear.FirstSemesterStartDate.Value.Date <= DateTimeNow.Date && DateTimeNow.Date <= objAcademicYear.FirstSemesterEndDate.Value.Date)
                    || objAcademicYear.FirstSemesterEndDate.Value.Date <= DateTimeNow.Date && DateTimeNow.Date < objAcademicYear.SecondSemesterStartDate.Value.Date)
                {
                    SemesterChecked = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                }
                else
                {
                    SemesterChecked = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                }
            }
            else
            {
                SemesterChecked = SystemParamsInFile.SEMESTER_IN_SUMMER;
            }
            return SemesterChecked;
        }

        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            //: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary)
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                    {"AppliedLevel",global.AppliedLevel.Value}, 
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
            };

            //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
            if (!global.IsAdminSchoolRole && !global.IsViewAll && !UtilsBusiness.IsBGH(global.UserAccountID))
            {
                dic["UserAccountID"] = global.UserAccountID;
                //dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_TEACHING;
            }

            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic);
            //IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return lsCP;
        }
        private IQueryable<ClassProfile> getClassFromEducationLevelCreate(int EducationLevelID)
        {
            //: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary)
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };

            //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
            if (!global.IsAdminSchoolRole)
            {
                dic["UserAccountID"] = global.UserAccountID;
                //dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEADTEACHER;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_TEACHING;
            }

            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic);
            //IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return lsCP;
        }
        #endregion

        #region Avatar
        [ValidateAntiForgeryToken]
        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        {
            GlobalInfo global = new GlobalInfo();

            if (ImageUploader == null || ImageUploader.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = ImageUploader.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> imageExtension = new List<string>();
                imageExtension.Add(".JPG");
                imageExtension.Add(".JPEG");
                imageExtension.Add(".PNG");
                imageExtension.Add(".BMP");
                imageExtension.Add(".ICO");
                imageExtension.Add(".GIF");
                var extension = Path.GetExtension(file.FileName);
                if (!imageExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
                file.SaveAs(physicalPath);
                Session["ImagePath"] = physicalPath;

                string relativePath = "/Uploads/Photos/" + fileName;
                JsonResult res = Json(new JsonMessage(fileName));
                res.ContentType = "text/plain";
                return res;
            }

            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        [ValidateAntiForgeryToken]
        public ActionResult RemoveImage(string[] fileNames)
        {
            //// The parameter of the Remove action must be called "fileNames"
            //foreach (var fullName in fileNames)
            //{
            //    var fileName = Path.GetFileName(fullName);
            //    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

            //    // TODO: Verify user permissions
            //    if (System.IO.File.Exists(physicalPath))
            //    {
            //        // The files are not actually removed in this demo
            //        System.IO.File.Delete(physicalPath);
            //    }
            //}

            // Return an empty string to signify success
            return Content("");
        }

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }
        #endregion Avatar

        #region Export
        /// <summary>
        /// Xuat danh sach excel
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>

        public FileResult ExportExcel(SearchViewModel frm)
        {
            #region lay cac sheet ra
            GlobalInfo global = new GlobalInfo();
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", TEMPLATE_PUPILPROFILE_NEW);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTinhThanh = oBook.GetSheet(2);
            IVTWorksheet sheetQuanHuyen = oBook.GetSheet(3);
            IVTWorksheet sheetXaPhuong = oBook.GetSheet(4);
            IVTWorksheet sheetref = oBook.GetSheet(6);
            //IVTWorksheet sheetDanToc = oBook.GetSheet(5);
            //IVTWorksheet sheetTonGiao = oBook.GetSheet(6);
            //IVTWorksheet sheetDoiTuongChinhSach = oBook.GetSheet(7);
            #endregion

            #region lay ra cac lop
            var lstCP = new List<int>();
            var lstStrCP = new List<string>();

            int classID = 0;
            if (!string.IsNullOrEmpty(frm.Class))
            {
                if (frm.EducationLevel.Value != SystemParamsInFile.Combine_Class_ID)
                {
                    classID = Int32.Parse(frm.Class);
                    lstCP = new List<int>() { classID };
                }
                else
                {
                    lstStrCP = new List<string>() { frm.Class };
                }

            }
            else
            {
                if (frm.EducationLevel.HasValue)
                {
                    if (frm.EducationLevel.Value != SystemParamsInFile.Combine_Class_ID)
                        lstCP = getClassFromEducationLevel(frm.EducationLevel.Value).Select(o => o.ClassProfileID).ToList();
                    else
                        lstStrCP = getClassFromEducationLevel(frm.EducationLevel.Value).Select(o => o.CombinedClassCode).ToList();

                }
                else
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"AcademicYearID",global.AcademicYearID.Value}
                };

                    //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
                    if (!global.IsAdminSchoolRole)
                    {
                        dic["UserAccountID"] = global.UserAccountID;
                        // dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                    }
                    var lsEducation = global.EducationLevels.Select(o => o.EducationLevelID).ToList();

                    if (frm.EducationLevel.HasValue && frm.EducationLevel.Value != SystemParamsInFile.Combine_Class_ID)
                        lstCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic)
                                                    .Where(o => lsEducation.Contains(o.EducationLevelID))
                                                    .OrderBy(o => o.EducationLevelID).Select(o => o.ClassProfileID).ToList();
                    else
                        lstStrCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic)
                                                   .Where(o => lsEducation.Contains(o.EducationLevelID))
                                                   .OrderBy(o => o.EducationLevelID).Select(o => o.CombinedClassCode).ToList();
                }
            }
            #endregion

            #region dien du lieu cho sheet tinh thanh
            var lsProvince = ProvinceBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
            for (int i = 0; i < lsProvince.Count(); i++)
            {
                sheetTinhThanh.CopyPaste(sheetTinhThanh.GetRange("A4", "C4"), i + 4, 1);
                Province p = lsProvince[i];
                sheetTinhThanh.SetCellValue("A" + (i + 4), (i + 1));
                sheetTinhThanh.SetCellValue("B" + (i + 4), p.ProvinceCode);
                sheetTinhThanh.SetCellValue("C" + (i + 4), p.ProvinceName);

            }
            #endregion

            #region quan huyen
            //var lsDistrict = (from p in DistrictBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } })
            //                  join q in ProvinceBusiness.All.Where(o => o.IsActive == true) on p.ProvinceID equals q.ProvinceID
            //                  select new
            //                  {
            //                      DistrictID = p.DistrictID,
            //                      DistrictName = p.DistrictName,
            //                      DistrictCode = p.DistrictCode,
            //                      ProvinceID = q.ProvinceID,
            //                      ProvinceName = q.ProvinceName
            //                  })
            //                 .OrderBy(o => o.ProvinceName).ToList();
            //for (int i = 0; i < lsDistrict.Count(); i++)
            //{
            //    sheetQuanHuyen.CopyPaste(sheetQuanHuyen.GetRange("A4", "D4"), i + 4, 1);
            //    var d = lsDistrict[i];
            //    sheetQuanHuyen.SetCellValue("A" + (i + 4), (i + 1));
            //    sheetQuanHuyen.SetCellValue("B" + (i + 4), d.ProvinceName);
            //    sheetQuanHuyen.SetCellValue("C" + (i + 4), d.DistrictCode);
            //    sheetQuanHuyen.SetCellValue("D" + (i + 4), d.DistrictName);
            //}

            // Fill Quận/Huyện
            IVTRange rangeDistrict = sheetQuanHuyen.GetRange("A4", "D4");
            var lstDis = (from p in DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } })
                          join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                          select new
                          {
                              DistrictID = p.DistrictID,
                              DistrictName = p.DistrictName,
                              DistrictCode = p.DistrictCode,
                              ProvinceID = q.ProvinceID,
                              ProvinceName = q.ProvinceName
                          }).ToList();
            int startRow_Dis = 4;
            int order = 1;
            int beginRow_Dis = 4;
            foreach (var pr in lsProvince)
            {
                int rowFirstByDis = 1;
                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count();
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        sheetQuanHuyen.CopyPasteSameSize(rangeDistrict, startRow_Dis, 1);
                        sheetQuanHuyen.SetCellValue(startRow_Dis, 1, order++);
                        sheetQuanHuyen.SetCellValue(startRow_Dis, 2, dis.ProvinceName);
                        sheetQuanHuyen.SetCellValue(startRow_Dis, 3, dis.DistrictCode);
                        sheetQuanHuyen.SetCellValue(startRow_Dis, 4, dis.DistrictName);

                        startRow_Dis++;
                        rowFirstByDis++;
                    }
                    beginRow_Dis += numberOfDis;
                }
            }
            #endregion

            #region xa phuong
            //var lsCommune = (from p in CommuneBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } })
            //                 join q in DistrictBusiness.All.Where(o => o.IsActive == true) on p.DistrictID equals q.DistrictID
            //                 join r in ProvinceBusiness.All.Where(o => o.IsActive == true) on q.ProvinceID equals r.ProvinceID
            //                 select new
            //                 {
            //                     CommuneID = p.CommuneID,
            //                     CommuneCode = p.CommuneCode,
            //                     CommuneName = p.CommuneName,
            //                     DistrictID = q.DistrictID,
            //                     DistrictCode = q.DistrictCode,
            //                     DistrictName = q.DistrictName
            //                 }).OrderBy(o => o.DistrictName).ToList();


            //for (int i = 0; i < lsCommune.Count(); i++)
            //{
            //    sheetXaPhuong.CopyPaste(sheetXaPhuong.GetRange("A5", "E5"), i + 5, 1);
            //    var c = lsCommune[i];

            //    sheetXaPhuong.SetCellValue("A" + (i + 5), (i + 1));
            //    sheetXaPhuong.SetCellValue("B" + (i + 5), c.DistrictCode);
            //    sheetXaPhuong.SetCellValue("C" + (i + 5), c.DistrictName);
            //    sheetXaPhuong.SetCellValue("D" + (i + 5), c.CommuneCode);
            //    sheetXaPhuong.SetCellValue("E" + (i + 5), c.CommuneName);
            //}
            // Sheet Xã/Phường
            IVTRange rangeProOfCommune = sheetXaPhuong.GetRange("A4", "E4");
            IVTRange rangeCommune = sheetXaPhuong.GetRange("A5", "E5");
            var lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            // Fill Xã/Phường
            int numProvince = 1;
            int startRow_Pro = 4;

            int startRow_Com = 5;
            order = 1;

            int beginRow_Com = 5;
            foreach (var pr in lsProvince)
            {
                // Thông tin tỉnh
                sheetXaPhuong.CopyPasteSameSize(rangeProOfCommune, startRow_Pro, 1);
                sheetXaPhuong.SetCellValue(startRow_Pro, 1, numProvince++);
                sheetXaPhuong.SetCellValue(startRow_Pro, 2, pr.ProvinceName);

                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count;
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        int rowFirstOfDis = 1;
                        var lstCommByDis = lstCommune.Where(o => o.DistrictID == dis.DistrictID).OrderBy(o => o.CommuneName).ToList();
                        int numOfComm = lstCommByDis.Count;
                        if (numOfComm > 0)
                        {
                            foreach (var cm in lstCommByDis)
                            {
                                sheetXaPhuong.CopyPasteSameSize(rangeCommune, startRow_Com, 1);
                                sheetXaPhuong.SetCellValue(startRow_Com, 1, order++);

                                sheetXaPhuong.SetCellValue(startRow_Com, 2, dis.DistrictCode);
                                sheetXaPhuong.SetCellValue(startRow_Com, 3, dis.DistrictName);

                                sheetXaPhuong.SetCellValue(startRow_Com, 4, cm.CommuneCode);
                                sheetXaPhuong.SetCellValue(startRow_Com, 5, cm.CommuneName);

                                startRow_Com++;
                                rowFirstOfDis++;
                            }
                        }
                        beginRow_Com += numOfComm; // dòng tiến hành merge
                    }
                }
                // set lại biến đếm cho tỉnh tiếp theo
                startRow_Pro += order;
                startRow_Com++;
                beginRow_Com++;
                order = 1;
            }
            #endregion

            #region dien du lieu cho sheet dan toc
            List<string> listEthnic = new List<string>();
            List<Ethnic> lst1 = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.EthnicName).ToList();
            Ethnic e1 = lst1.FirstOrDefault(o => o.EthnicName.Contains("Kinh"));
            lst1.Remove(e1);
            lst1.Insert(0, e1);
            lst1.ForEach(u =>
            {
                if (!string.IsNullOrEmpty(u.EthnicName))
                {
                    listEthnic.Add(u.EthnicName);
                }
            });
            #endregion

            #region dien du lieu cho sheet ton giao
            List<string> listReligionObj = new List<string>();
            List<Religion> lstReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            Religion r1 = lstReligion.FirstOrDefault(o => o.Resolution.Contains("Không"));
            lstReligion.Remove(r1);
            lstReligion.Insert(0, r1);
            lstReligion.ForEach(u =>
            {
                if (!string.IsNullOrEmpty(u.Resolution))
                {
                    listReligionObj.Add(u.Resolution);
                }
            });
            #endregion

            #region dien du lieu cho sheet doi tuong chinh sach
            List<string> listPolicyTarget = new List<string>();
            var lstPOT = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPOT.ForEach(u =>
            {
                if (!string.IsNullOrEmpty(u.Resolution))
                {
                    listPolicyTarget.Add(u.Resolution);
                }
            });
            #endregion

            #region dien du lieu cho sheet che do chinh sach
            List<string> listPolicyRegime = new List<string>();
            var lstPR = PolicyRegimeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPR.ForEach(u =>
            {
                if (!string.IsNullOrEmpty(u.Resolution))
                {
                    listPolicyRegime.Add(u.Resolution);
                }
            });
            #endregion

            #region dien du lieu cho sheet loai khuyet tat
            List<string> listDisabledType = new List<string>();
            var lstDT = DisabledTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstDT.ForEach(u =>
            {
                if (!string.IsNullOrEmpty(u.Resolution))
                {
                    listDisabledType.Add(u.Resolution);
                }
            });
            #endregion

            #region sheet du lieu hoc sinh tung lop

            #region new

            //tuy theo tung lop ma fill excel theo cac tab
            IVTRange templateRange = sheet.GetRange("A1", "AE75");

            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            // Danh sách combobox
            KingDic.Add("PolicyTargets", listPolicyTarget);
            KingDic.Add("Ethnics", listEthnic);
            KingDic.Add("Religions", listReligionObj);
            KingDic.Add("PolicyRegimes", listPolicyRegime);
            KingDic.Add("DisabledTypes", listDisabledType);
            int startrowref = 4;
            for (int i = 0; i < listPolicyTarget.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 1, listPolicyTarget[i]);
                startrowref++;
            }
            startrowref = 4;
            for (int i = 0; i < listEthnic.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 2, listEthnic[i]);
                startrowref++;
            }
            startrowref = 4;
            for (int i = 0; i < listReligionObj.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 3, listReligionObj[i]);
                startrowref++;
            }
            startrowref = 4;
            for (int i = 0; i < listPolicyRegime.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 4, listPolicyRegime[i]);
                startrowref++;
            }
            startrowref = 4;
            for (int i = 0; i < listDisabledType.Count; i++)
            {
                sheetref.SetCellValue(startrowref, 5, listDisabledType[i]);
                startrowref++;
            }
            List<object> QueenDic = new List<object>();

            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["CurrentClassID"] = frm.Class;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["EthnicID"] = frm.Ethnic;
            SearchInfo["ProfileStatus"] = frm.Status;

            Paginate<PupilProfileChildrenViewModel> lstPaginate = this.SearchPaging(SearchInfo, false, false, 1, "");
            List<PupilProfileChildrenViewModel> lst = lstPaginate.List.ToList();
            PupilProfileChildrenViewModel thisModel = new PupilProfileChildrenViewModel();
            int totalPupil = lst.Count;
            for (int i = 0; i < totalPupil; i++)
            {
                thisModel = lst[i];
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["PupilCode"] = thisModel.PupilCode;
                princeDic["FullName"] = thisModel.FullName;
                princeDic["BirthDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.BirthDate);
                princeDic["Genre"] = thisModel.GenreName.Equals(Res.Get("Common_Label_Female")) ? "x" : "";
                princeDic["Class"] = thisModel.ClassName;

                princeDic["EnrolmentType"] = thisModel.EnrolmentTypeName;
                princeDic["EnrolmentDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.EnrolmentDate);
                princeDic["Province"] = thisModel.ProvinceName;
                princeDic["District"] = thisModel.DistrictName;
                princeDic["Commune"] = thisModel.CommuneName;
                princeDic["Village"] = thisModel.VillageName;
                princeDic["StorageNumber"] = thisModel.StorageNumber;

                princeDic["PolicyTarget"] = thisModel.PolicyTargetName;
                princeDic["PolicyRegime"] = thisModel.PolicyRegimeName;
                princeDic["DisabledType"] = thisModel.DisabledTypeName;
                princeDic["Ethnic"] = thisModel.EthnicName;
                princeDic["Religion"] = thisModel.ReligionName;
                princeDic["BirthPlace"] = thisModel.BirthPlace;
                princeDic["HomeTown"] = thisModel.HomeTown;

                princeDic["PermanentResidentalAddress"] = thisModel.PermanentResidentalAddress;
                princeDic["TempResidentalAddress"] = thisModel.TempResidentalAddress;
                princeDic["Mobile"] = thisModel.Telephone;
                princeDic["FatherFullName"] = thisModel.FatherFullName;
                princeDic["iFatherBirthDate"] = thisModel.iFatherBirthDate;
                princeDic["FatherBirthDate"] = thisModel.FatherBirthDate;

                princeDic["FatherJob"] = thisModel.FatherJob;
                princeDic["FatherMobile"] = thisModel.FatherMobile;
                princeDic["MotherFullName"] = thisModel.MotherFullName;
                princeDic["iMotherBirthDate"] = thisModel.iMotherBirthDate;
                princeDic["MotherBirthDate"] = thisModel.MotherBirthDate;
                princeDic["MotherJob"] = thisModel.MotherJob;
                princeDic["MotherEmail"] = thisModel.MotherEmail;
                princeDic["FatherEmail"] = thisModel.FatherEmail;

                princeDic["MotherMobile"] = thisModel.MotherMobile;
                princeDic["BloodType"] = thisModel.BloodTypeName;
                princeDic["Status"] = thisModel.StatusName;
                princeDic["MinorityFather"] = thisModel.MinorityFather == true ? "x" : "";
                princeDic["MinorityMother"] = thisModel.MinorityMother == true ? "x" : "";

                QueenDic.Add(princeDic);
            }

            if (lst.Count > 2)
                for (int i = 0; i < lst.Count - 2; i++)
                    sheet.CopyAndInsertARow(6, 7);

            if (lst.Count == 1)
                sheet.DeleteRow(6);

            SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID);
            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SchoolName", sp.SchoolName);
            sheet.FillVariableValue(KingDic);
            //sheet.DeleteColumn(34);
            //sheet.DeleteColumn(8); // Xoa cot ban hoc
            sheet.SetCellValue("A3", "DANH SÁCH TRẺ");
            sheet.SetCellValue("B4", "Mã trẻ");
            sheet.SetCellValue("C4", "Họ và tên trẻ");
            //sheet.SetCellValue("AG4", "Trạng thái của trẻ");
            sheet.Name = "DanhSachTre";
            #endregion
            #region Fill Thong tin combobox trong excel
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation = null;
            //Nhom mau
            string[] arrBloodType = new string[] { Res.Get("Blood_Type_A"), Res.Get("Blood_Type_B"), Res.Get("Blood_Type_AB"), Res.Get("Blood_Type_O") };
            objValidation = new VTDataValidation
            {
                Contrains = arrBloodType,
                FromColumn = 34,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 34,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);

            //Trang thai cua hoc sinh           
            List<string> listStatus = CommonList.PupilStatus().Select(p => p.value).ToList();
            string[] arrStatusPupil = listStatus.ToArray();
            objValidation = new VTDataValidation
            {
                Contrains = arrStatusPupil,
                FromColumn = 35,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 35,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);
            //Gioi tinh
            //List<string> listGenre = CommonList.GenreAndAll().Select(p => p.value).ToList();
            //string[] arrGenre = listGenre.ToArray();
            string[] arrGenre = new string[] { "x" };
            objValidation = new VTDataValidation
            {
                Contrains = arrGenre,
                FromColumn = 5,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 5,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);

            string[] arrMinority = new string[] { Res.Get("x") };
            objValidation = new VTDataValidation
            {
                Contrains = arrMinority,
                FromColumn = 36,
                FromRow = 5,
                SheetIndex = 1,
                ToColumn = 37,
                ToRow = totalPupil + 4,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);


            #endregion
            Stream excel = oBook.ToStreamValidationData(lstValidation);
            #endregion
            SchoolProfile sp1 = SchoolProfileBusiness.Find(global.SchoolID);
            EducationLevel el = EducationLevelBusiness.Find(frm.EducationLevel);
            ClassProfile cp = ClassProfileBusiness.Find(classID);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportUtils.StripVNSign(string.Format(TEMPLATE_PUPILPROFILE_FORMAT, (el != null ? "_" + el.Resolution : ""), (cp != null ? "_" + cp.DisplayName : ""), "_" + sp1.SchoolName));

            // Tạo dữ liệu ghi log
            String param = "{\"EducationLevel\":\"" + frm.EducationLevel + "\",\"Class\":\"" + frm.Class + "\", \"Genre\":\"" + frm.Genre + "\",\"PupilCode\":\"" + frm.PupilCode + "\",\"Fullname\":\"" + frm.Fullname + "\",\"Status\":\"" + frm.Status + "\"}";
            SetViewDataActionAudit(String.Empty, String.Empty, String.Empty, "Export excel pupil_profile", param, "Hồ sơ trẻ", GlobalConstants.ACTION_EXPORT, "Xuất danh sách trẻ");

            return result;
        }
        #endregion

        #region private method
        private bool IsCurrentYear()
        {
            bool isCurrentYear = false;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            if ((DateTime.Compare(nowDate, aca.FirstSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.FirstSemesterEndDate.Value) <= 0) ||
                (DateTime.Compare(nowDate, aca.SecondSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.SecondSemesterEndDate.Value) <= 0))
            {
                isCurrentYear = true;
            }
            return isCurrentYear;
        }
        #endregion

        #region Export Original
        /// <summary>
        /// xuat file excel mau
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>

        public FileResult Export(int EducationLevelID)
        {
            GlobalInfo glo = new GlobalInfo();

            EducationLevel edu = EducationLevelBusiness.Find(EducationLevelID);
            //if (edu == null) return null;

            int year = AcademicYearBusiness.Find(glo.AcademicYearID).Year;

            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.MAU_IMPORT_HO_SO_TRE_MOI);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            string reportName = ReportUtils.RemoveSpecialCharacters(reportDefinition.OutputNamePattern) + "." + reportDefinition.OutputFormat;

            IVTWorkbook book = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = book.GetSheet(1);
            IVTWorksheet refSheet = book.GetSheet(2); // Tỉnh thành
            IVTWorksheet refSheetDis = book.GetSheet(3); // Quận Huyện
            IVTWorksheet refSheetComm = book.GetSheet(4); // Xã phường

            IVTWorksheet refTemp = book.GetSheet(5);
            IVTWorksheet refHDC = book.GetSheet(6);

            IVTWorksheet sheetProvince = book.CopySheetToLast(refSheet, "C3");
            IVTWorksheet sheetDistrict = book.CopySheetToLast(refSheetDis, "D3");
            IVTWorksheet sheetCommune = book.CopySheetToLast(refSheetComm, "E3");

            IVTRange rangeProvince = refSheet.GetRange("A4", "C4");
            IVTRange rangeDistrict = refSheetDis.GetRange("A4", "D4");
            IVTRange rangeProOfCommune = refSheetComm.GetRange("A4", "E4");
            IVTRange rangeCommune = refSheetComm.GetRange("A5", "E5");

            IVTWorksheet sheetHDC = book.CopySheetToLast(refHDC, "A20");

            #region fill data for sheet
            Dictionary<string, object> dicSheet = new Dictionary<string, object>();
            dicSheet["SuperVisingDeptName"] = glo.SuperVisingDeptName;
            dicSheet["SchoolName"] = glo.SchoolName.ToUpper();
            dicSheet["DateTime"] = string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            if (edu != null)
            {
                dicSheet["EducationLevel"] = edu.Resolution;
            }
            sheet.FillVariableValue(dicSheet);
            #endregion

            #region fill data for combobox values
            List<int> lstClassID = getClassFromEducationLevel(EducationLevelID).Select(x => x.ClassProfileID).ToList();
            Dictionary<string, object> dicRef = new Dictionary<string, object>();
            // Khối - Lớp
            List<object> listClass = new List<object>();
            Dictionary<string, object> dicClass = new Dictionary<string, object> { 
                {"AcademicYearID", glo.AcademicYearID.Value},
                {"IsActive", true},
                {"AppliedLevel", glo.AppliedLevel}
            };
            if (EducationLevelID != 0)
            {
                dicClass["EducationLevelID"] = EducationLevelID;
            }
            ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value,
                                                dicClass).Where(x => lstClassID.Contains(x.ClassProfileID)).ToList().ForEach(u => listClass.Add(new { u.DisplayName }));
            dicRef["Classes"] = listClass;

            // Hình thức trúng tuyển
            List<object> listEnrolmentType = new List<object>();
            CommonList.EnrolmentTypeAvailable().ForEach(u => listEnrolmentType.Add(new { Value = u.value }));
            dicRef["EnrolmentTypes"] = listEnrolmentType;


            // Sheet Tỉnh/Thành
            List<Province> lstPro = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
            List<object> listProvince_Temp = new List<object>();
            int i = 1;
            int startRow = 4;
            foreach (var pr in lstPro)
            {
                sheetProvince.CopyPasteSameSize(rangeProvince, startRow, 1);
                listProvince_Temp.Add(new Dictionary<string, object> 
                    {
                        {"Order", i++}, 
                        {"ProvinceCode", pr.ProvinceCode},
                        {"ProvinceName", pr.ProvinceName},
                       
                    });
                startRow++;
            }
            // Fill Tỉnh/Thành
            Dictionary<string, object> dicSheetPro = new Dictionary<string, object>();
            dicSheetPro.Add("list", listProvince_Temp);
            sheetProvince.FillVariableValue(dicSheetPro);


            // Sheet Quận/Huyện           
            var lstDis = (from p in DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } })
                          join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                          select new
                          {
                              DistrictID = p.DistrictID,
                              DistrictName = p.DistrictName,
                              DistrictCode = p.DistrictCode,
                              ProvinceID = q.ProvinceID,
                              ProvinceName = q.ProvinceName
                          }).ToList();
            // Fill Quận/Huyện
            int startRow_Dis = 4;
            int order = 1;
            int beginRow_Dis = 4;
            foreach (var pr in lstPro)
            {
                int rowFirstByDis = 1;
                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count();
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        sheetDistrict.CopyPasteSameSize(rangeDistrict, startRow_Dis, 1);
                        sheetDistrict.SetCellValue(startRow_Dis, 1, order++);
                        if (rowFirstByDis == 1)
                        {
                            sheetDistrict.SetCellValue(startRow_Dis, 2, dis.ProvinceName);
                        }
                        sheetDistrict.SetCellValue(startRow_Dis, 3, dis.DistrictCode);
                        sheetDistrict.SetCellValue(startRow_Dis, 4, dis.DistrictName);
                        if (rowFirstByDis == numberOfDis)
                        {
                            IVTRange range = sheetDistrict.GetRange(beginRow_Dis, 2, beginRow_Dis + numberOfDis - 1, 2);
                            range.Merge();
                        }
                        startRow_Dis++;
                        rowFirstByDis++;
                    }
                    beginRow_Dis += numberOfDis;
                }
            }


            // Sheet Xã/Phường
            var lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            // Fill Xã/Phường
            int numProvince = 1;
            int startRow_Pro = 4;

            int startRow_Com = 5;
            order = 1;

            int beginRow_Com = 5;
            foreach (var pr in lstPro)
            {
                // Thông tin tỉnh
                sheetCommune.CopyPasteSameSize(rangeProOfCommune, startRow_Pro, 1);
                sheetCommune.SetCellValue(startRow_Pro, 1, numProvince++);
                sheetCommune.SetCellValue(startRow_Pro, 2, pr.ProvinceName);

                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count;
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        int rowFirstOfDis = 1;
                        var lstCommByDis = lstCommune.Where(o => o.DistrictID == dis.DistrictID).OrderBy(o => o.CommuneName).ToList();
                        int numOfComm = lstCommByDis.Count;
                        if (numOfComm > 0)
                        {
                            foreach (var cm in lstCommByDis)
                            {
                                sheetCommune.CopyPasteSameSize(rangeCommune, startRow_Com, 1);
                                sheetCommune.SetCellValue(startRow_Com, 1, order++);
                                if (rowFirstOfDis == 1)
                                {
                                    sheetCommune.SetCellValue(startRow_Com, 2, dis.DistrictCode);
                                    sheetCommune.SetCellValue(startRow_Com, 3, dis.DistrictName);
                                }
                                sheetCommune.SetCellValue(startRow_Com, 4, cm.CommuneCode);
                                sheetCommune.SetCellValue(startRow_Com, 5, cm.CommuneName);
                                if (rowFirstOfDis == numOfComm)
                                {
                                    sheetCommune.GetRange(beginRow_Com, 2, beginRow_Com + numOfComm - 1, 2).Merge();
                                    sheetCommune.GetRange(beginRow_Com, 3, beginRow_Com + numOfComm - 1, 3).Merge();
                                }
                                startRow_Com++;
                                rowFirstOfDis++;
                            }
                        }
                        beginRow_Com += numOfComm; // dòng tiến hành merge
                    }
                }
                // set lại biến đếm cho tỉnh tiếp theo
                startRow_Pro += order;
                startRow_Com++;
                beginRow_Com++;
                order = 1;
            }


            // Đối tượng chính sách  
            List<object> listPolicyTarget = new List<object>();
            var lstPOT = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPOT.ForEach(u => listPolicyTarget.Add(new { u.Resolution }));
            dicRef["PolicyTargets"] = listPolicyTarget;

            // Dân tộc
            List<object> listEthnic = new List<object>();
            List<Ethnic> lst1 = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.EthnicName).ToList();
            Ethnic e1 = lst1.FirstOrDefault(o => o.EthnicName.Contains("Kinh"));
            lst1.Remove(e1);
            lst1.Insert(0, e1);
            lst1.ForEach(u => listEthnic.Add(new { u.EthnicName }));
            //EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["Ethnics"] = listEthnic;

            // Chế độ chính sách
            List<object> listPolicyRegimes = new List<object>();
            List<PolicyRegime> lst11 = PolicyRegimeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lst11.ForEach(u => listPolicyRegimes.Add(new { u.Resolution }));
            //EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["PolicyRegimes"] = listPolicyRegimes;

            // Tôn giáo
            List<object> listReligionObj = new List<object>();
            List<Religion> lstReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            Religion r1 = lstReligion.FirstOrDefault(o => o.Resolution.Contains("Không"));
            lstReligion.Remove(r1);
            lstReligion.Insert(0, r1);
            lstReligion.ForEach(u => listReligionObj.Add(new { u.Resolution }));
            //ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listReligion.Add(new { u.Resolution }));
            dicRef["Religions"] = listReligionObj;

            //Loại khuyết tật
            List<object> listDisabledTyeObj = new List<object>();
            List<DisabledType> listDisabledTye = DisabledTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            listDisabledTye.ForEach(u => listDisabledTyeObj.Add(new { u.Resolution }));
            //ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listReligion.Add(new { u.Resolution }));
            dicRef["DisabledTypes"] = listDisabledTyeObj;

            List<object> listBloodType = new List<object>();
            CommonList.BloodType().ForEach(u => listBloodType.Add(new { Value = u.value }));
            dicRef["BloodTypes"] = listBloodType;

            List<object> listProfileStatus = new List<object>();
            CommonList.PupilStatus().Where(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString()).ToList().ForEach(u => listProfileStatus.Add(new { Value = u.value }));
            dicRef["ProfileStatuses"] = listProfileStatus;

            List<object> listGenres = new List<object>();
            CommonList.GenreAndSelect().ForEach(u => listGenres.Add(new { Value = u.value }));
            dicRef["Genres"] = listGenres;



            refTemp.FillVariableValue(dicRef);
            // Set lai ten cac Sheet
            sheetProvince.Name = "TinhThanh";
            refSheet.Delete();

            sheetDistrict.Name = "QuanHuyen";
            refSheetDis.Delete();

            sheetCommune.Name = "XaPhuong";
            refSheetComm.Delete();

            sheetHDC.Name = "HuongDanChung";
            refHDC.Delete();

            #endregion

            //sheet.SetColumnWidth(35, 0);
            FileStreamResult result = new FileStreamResult(book.ToStream(), "application/octet-stream");
            result.FileDownloadName = reportName;
            return result;
        }

        public FileResult DowloadFileErr(int EducationLevelID)
        {
            string reportName = string.Empty;
            Stream excel = this.SetDataToFileExcel(EducationLevelID, ref reportName, true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = reportName;
            return result;
        }

        private Stream SetDataToFileExcel(int EducationLevelID, ref string ReportName, bool isErrFile = false)
        {
            EducationLevel edu = EducationLevelBusiness.Find(EducationLevelID);
            //if (edu == null) return null;

            int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;

            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.MAU_IMPORT_HO_SO_TRE_MOI);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            ReportName = ReportUtils.RemoveSpecialCharacters(reportDefinition.OutputNamePattern) + "." + reportDefinition.OutputFormat;

            IVTWorkbook book = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = book.GetSheet(1);
            IVTWorksheet refSheet = book.GetSheet(2); // Tỉnh thành
            IVTWorksheet refSheetDis = book.GetSheet(3); // Quận Huyện
            IVTWorksheet refSheetComm = book.GetSheet(4); // Xã phường

            IVTWorksheet refTemp = book.GetSheet(5);
            IVTWorksheet refHDC = book.GetSheet(6);

            IVTWorksheet sheetProvince = book.CopySheetToLast(refSheet, "C3");
            IVTWorksheet sheetDistrict = book.CopySheetToLast(refSheetDis, "D3");
            IVTWorksheet sheetCommune = book.CopySheetToLast(refSheetComm, "E3");

            IVTRange rangeProvince = refSheet.GetRange("A4", "C4");
            IVTRange rangeDistrict = refSheetDis.GetRange("A4", "D4");

            IVTRange rangeProOfCommune = refSheetComm.GetRange("A4", "E4");
            IVTRange rangeCommune = refSheetComm.GetRange("A5", "E5");

            IVTWorksheet sheetHDC = book.CopySheetToLast(refHDC, "A20");

            #region fill data for sheet
            Dictionary<string, object> dicSheet = new Dictionary<string, object>();
            dicSheet["SuperVisingDeptName"] = _globalInfo.SuperVisingDeptName;
            dicSheet["SchoolName"] = _globalInfo.SchoolName.ToUpper();
            dicSheet["DateTime"] = string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            if (edu != null)
            {
                dicSheet["EducationLevel"] = edu.Resolution;
            }
            sheet.FillVariableValue(dicSheet);
            #endregion

            #region fill data for combobox values
            Dictionary<string, object> dicRef = new Dictionary<string, object>();
            // Khối - Lớp
            List<int> lstClassID = getClassFromEducationLevel(EducationLevelID).Select(x => x.ClassProfileID).ToList();
            List<object> listClass = new List<object>();
            Dictionary<string, object> dicClass = new Dictionary<string, object> { 
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"IsActive", true},
                {"AppliedLevel", _globalInfo.AppliedLevel}
            };
            if (EducationLevelID != 0)
            {
                dicClass["EducationLevelID"] = EducationLevelID;
            }
            ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                                                dicClass).Where(x => lstClassID.Contains(x.ClassProfileID)).ToList()
                                .ForEach(u => listClass.Add(new { u.DisplayName }));
            dicRef["Classes"] = listClass;

            // Hình thức trúng tuyển
            List<object> listEnrolmentType = new List<object>();
            CommonList.EnrolmentTypeAvailable().ForEach(u => listEnrolmentType.Add(new { Value = u.value }));
            dicRef["EnrolmentTypes"] = listEnrolmentType;


            // Sheet Tỉnh/Thành
            List<Province> lstPro = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
            List<object> listProvince_Temp = new List<object>();
            int i = 1;
            int startRow = 4;
            foreach (var pr in lstPro)
            {
                sheetProvince.CopyPasteSameSize(rangeProvince, startRow, 1);
                listProvince_Temp.Add(new Dictionary<string, object> 
                    {
                        {"Order", i++}, 
                        {"ProvinceCode", pr.ProvinceCode},
                        {"ProvinceName", pr.ProvinceName},
                       
                    });
                startRow++;
            }
            // Fill Tỉnh/Thành
            Dictionary<string, object> dicSheetPro = new Dictionary<string, object>();
            dicSheetPro.Add("list", listProvince_Temp);
            sheetProvince.FillVariableValue(dicSheetPro);


            // Sheet Quận/Huyện           
            var lstDis = (from p in DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } })
                          join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                          select new
                          {
                              DistrictID = p.DistrictID,
                              DistrictName = p.DistrictName,
                              DistrictCode = p.DistrictCode,
                              ProvinceID = q.ProvinceID,
                              ProvinceName = q.ProvinceName
                          }).ToList();
            // Fill Quận/Huyện
            int startRow_Dis = 4;
            int order = 1;
            int beginRow_Dis = 4;
            foreach (var pr in lstPro)
            {
                int rowFirstByDis = 1;
                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count();
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        sheetDistrict.CopyPasteSameSize(rangeDistrict, startRow_Dis, 1);
                        sheetDistrict.SetCellValue(startRow_Dis, 1, order++);
                        if (rowFirstByDis == 1)
                        {
                            sheetDistrict.SetCellValue(startRow_Dis, 2, dis.ProvinceName);
                        }
                        sheetDistrict.SetCellValue(startRow_Dis, 3, dis.DistrictCode);
                        sheetDistrict.SetCellValue(startRow_Dis, 4, dis.DistrictName);
                        if (rowFirstByDis == numberOfDis)
                        {
                            IVTRange range = sheetDistrict.GetRange(beginRow_Dis, 2, beginRow_Dis + numberOfDis - 1, 2);
                            range.Merge();
                        }
                        startRow_Dis++;
                        rowFirstByDis++;
                    }
                    beginRow_Dis += numberOfDis;
                }
            }


            // Sheet Xã/Phường
            var lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            // Fill Xã/Phường
            int numProvince = 1;
            int startRow_Pro = 4;

            int startRow_Com = 5;
            order = 1;

            int beginRow_Com = 5;
            foreach (var pr in lstPro)
            {
                // Thông tin tỉnh
                sheetCommune.CopyPasteSameSize(rangeProOfCommune, startRow_Pro, 1);
                sheetCommune.SetCellValue(startRow_Pro, 1, numProvince++);
                sheetCommune.SetCellValue(startRow_Pro, 2, pr.ProvinceName);

                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count;
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        int rowFirstOfDis = 1;
                        var lstCommByDis = lstCommune.Where(o => o.DistrictID == dis.DistrictID).OrderBy(o => o.CommuneName).ToList();
                        int numOfComm = lstCommByDis.Count;
                        if (numOfComm > 0)
                        {
                            foreach (var cm in lstCommByDis)
                            {
                                sheetCommune.CopyPasteSameSize(rangeCommune, startRow_Com, 1);
                                sheetCommune.SetCellValue(startRow_Com, 1, order++);
                                if (rowFirstOfDis == 1)
                                {
                                    sheetCommune.SetCellValue(startRow_Com, 2, dis.DistrictCode);
                                    sheetCommune.SetCellValue(startRow_Com, 3, dis.DistrictName);
                                }
                                sheetCommune.SetCellValue(startRow_Com, 4, cm.CommuneCode);
                                sheetCommune.SetCellValue(startRow_Com, 5, cm.CommuneName);
                                if (rowFirstOfDis == numOfComm)
                                {
                                    sheetCommune.GetRange(beginRow_Com, 2, beginRow_Com + numOfComm - 1, 2).Merge();
                                    sheetCommune.GetRange(beginRow_Com, 3, beginRow_Com + numOfComm - 1, 3).Merge();
                                }
                                startRow_Com++;
                                rowFirstOfDis++;
                            }
                        }
                        beginRow_Com += numOfComm; // dòng tiến hành merge
                    }
                }
                // set lại biến đếm cho tỉnh tiếp theo
                startRow_Pro += order;
                startRow_Com++;
                beginRow_Com++;
                order = 1;
            }


            // Đối tượng chính sách  
            List<object> listPolicyTarget = new List<object>();
            var lstPOT = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPOT.ForEach(u => listPolicyTarget.Add(new { u.Resolution }));
            dicRef["PolicyTargets"] = listPolicyTarget;

            // Dân tộc
            List<object> listEthnic = new List<object>();
            List<Ethnic> lst1 = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.EthnicName).ToList();
            Ethnic e1 = lst1.FirstOrDefault(o => o.EthnicName.Contains("Kinh"));
            lst1.Remove(e1);
            lst1.Insert(0, e1);
            lst1.ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["Ethnics"] = listEthnic;

            // Chế độ chính sách
            List<object> listPolicyRegimes = new List<object>();
            List<PolicyRegime> lst11 = PolicyRegimeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lst11.ForEach(u => listPolicyRegimes.Add(new { u.Resolution }));
            //EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["PolicyRegimes"] = listPolicyRegimes;

            //Loại khuyết tật
            List<object> listDisabledTyeObj = new List<object>();
            List<DisabledType> listDisabledTye = DisabledTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            listDisabledTye.ForEach(u => listDisabledTyeObj.Add(new { u.Resolution }));
            //ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listReligion.Add(new { u.Resolution }));
            dicRef["DisabledTypes"] = listDisabledTyeObj;

            // Tôn giáo
            List<object> listReligionObj = new List<object>();
            List<Religion> lstReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            Religion r1 = lstReligion.FirstOrDefault(o => o.Resolution.Contains("Không"));
            lstReligion.Remove(r1);
            lstReligion.Insert(0, r1);
            lstReligion.ForEach(u => listReligionObj.Add(new { u.Resolution }));
            dicRef["Religions"] = listReligionObj;

            List<object> listBloodType = new List<object>();
            CommonList.BloodType().ForEach(u => listBloodType.Add(new { Value = u.value }));
            dicRef["BloodTypes"] = listBloodType;

            List<object> listForeignLanguageTraining = new List<object>();
            CommonList.ForeignLanguageTraining().ForEach(u => listForeignLanguageTraining.Add(new { Value = u.value }));
            dicRef["ForeignLanguageTrainings"] = listForeignLanguageTraining;

            List<object> listProfileStatus = new List<object>();
            CommonList.PupilStatus().Where(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString()).ToList().ForEach(u => listProfileStatus.Add(new { Value = u.value }));
            dicRef["ProfileStatuses"] = listProfileStatus;

            List<object> listGenres = new List<object>();
            CommonList.GenreAndSelect().ForEach(u => listGenres.Add(new { Value = u.value }));
            dicRef["Genres"] = listGenres;

            refTemp.FillVariableValue(dicRef);
            // Set lai ten cac Sheet
            sheetProvince.Name = "TinhThanh";
            refSheet.Delete();

            sheetDistrict.Name = "QuanHuyen";
            refSheetDis.Delete();

            sheetCommune.Name = "XaPhuong";
            refSheetComm.Delete();

            sheetHDC.Name = "HuongDanChung";
            refHDC.Delete();

            #endregion

            //Neu la cap 1 thi an cot he hoc ngoai ngu
            if (_globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                sheet.SetColumnWidth(31, 0);

            if (isErrFile)
            {
                List<ImportChildrenViewModel> lstPupilImportedErr = new List<ImportChildrenViewModel>();
                lstPupilImportedErr = (List<ImportChildrenViewModel>)Session["LIST_IMPORTED_PUPIL"];
                lstPupilImportedErr = lstPupilImportedErr.Where(p => p.IsValid == false).ToList();
                ImportChildrenViewModel objPupilErr = null;
                int startRowErr = 5;
                for (int j = 0; j < lstPupilImportedErr.Count; j++)
                {
                    objPupilErr = lstPupilImportedErr[j];
                    //STT
                    sheet.SetCellValue(startRowErr, 1, j + 1);
                    //Ma HS
                    sheet.SetCellValue(startRowErr, 2, objPupilErr.PupilCode);
                    //Ho ten
                    sheet.SetCellValue(startRowErr, 3, objPupilErr.FullName);
                    //Ngay sinh
                    sheet.SetCellValue(startRowErr, 4, objPupilErr.BirthDate);
                    //Gioi tinh
                    sheet.SetCellValue(startRowErr, 5, objPupilErr.GenreName);
                    //Lop
                    sheet.SetCellValue(startRowErr, 6, objPupilErr.ClassName);
                    //Hinh thuc trung tuyen
                    sheet.SetCellValue(startRowErr, 7, objPupilErr.EnrolmentTypeName);
                    //Ngay vao truong
                    sheet.SetCellValue(startRowErr, 8, objPupilErr.EnrolmentDateStr);
                    //Tinh thanh
                    sheet.SetCellValue(startRowErr, 9, objPupilErr.ProvinceName);
                    //Quan huyen
                    sheet.SetCellValue(startRowErr, 10, objPupilErr.DistrictName);
                    //Xa,Phuong
                    sheet.SetCellValue(startRowErr, 11, objPupilErr.CommuneName);
                    //Thon xom
                    sheet.SetCellValue(startRowErr, 12, objPupilErr.VillageName);
                    //So dang bo
                    sheet.SetCellValue(startRowErr, 13, objPupilErr.StorageNumber);
                    //Doi tuong chinh sach
                    sheet.SetCellValue(startRowErr, 14, objPupilErr.PolicyTargetName);
                    sheet.SetCellValue(startRowErr, 15, objPupilErr.PolicyRegimeName);
                    sheet.SetCellValue(startRowErr, 16, objPupilErr.DisabledTypeName);
                    //Dan toc
                    sheet.SetCellValue(startRowErr, 17, objPupilErr.EthnicName);
                    //Ton giao
                    sheet.SetCellValue(startRowErr, 18, objPupilErr.ReligionName);
                    //Noi sinh
                    sheet.SetCellValue(startRowErr, 19, objPupilErr.BirthPlace);
                    //Que quan
                    sheet.SetCellValue(startRowErr, 20, objPupilErr.HomeTown);
                    //Dia chi thuong tru
                    sheet.SetCellValue(startRowErr, 21, objPupilErr.PermanentResidentalAddress);
                    //Dia chi tam tru
                    sheet.SetCellValue(startRowErr, 22, objPupilErr.PermanentResidentalAddress);
                    //So dien thoai di dong
                    sheet.SetCellValue(startRowErr, 23, objPupilErr.Mobile);
                    //Ho ten cha
                    sheet.SetCellValue(startRowErr, 24, objPupilErr.FatherFullName);
                    //Nam sinh cua cha
                    sheet.SetCellValue(startRowErr, 25, objPupilErr.FatherBirthDateStr);
                    //Nghe nghiep cua cha
                    sheet.SetCellValue(startRowErr, 26, objPupilErr.FatherJob);
                    //SDT cua cha
                    sheet.SetCellValue(startRowErr, 27, objPupilErr.FatherMobile);
                    //Email cha
                    sheet.SetCellValue(startRowErr, 28, objPupilErr.FatherEmail);
                    //Ho ten me
                    sheet.SetCellValue(startRowErr, 29, objPupilErr.MotherFullName);
                    //Nam sinh cua me
                    sheet.SetCellValue(startRowErr, 30, objPupilErr.MotherBirthDateStr);
                    //Nghe nghiep cua me
                    sheet.SetCellValue(startRowErr, 31, objPupilErr.MotherJob);
                    //SDT cua me
                    sheet.SetCellValue(startRowErr, 32, objPupilErr.MotherMobile);
                    //Email me
                    sheet.SetCellValue(startRowErr, 33, objPupilErr.MotherEmail);
                    //Nhom mau
                    sheet.SetCellValue(startRowErr, 34, objPupilErr.BloodTypeName);
                    sheet.SetCellValue(startRowErr, 35, objPupilErr.MinorityFatherName = true ? "x" : "");
                    sheet.SetCellValue(startRowErr, 36, objPupilErr.MinorityMotherName = true ? "x" : "");
                    //Trang thai
                    sheet.SetCellValue(startRowErr, 37, objPupilErr.ProfileStatusName);
                    //Ma HS moi
                    sheet.SetCellValue(startRowErr, 38, objPupilErr.PupilCodeNew);
                    //Mo ta
                    sheet.SetCellValue(startRowErr, 39, objPupilErr.ErrorMessage);

                    startRowErr++;
                }
                sheet.UnHideColumn(35);
                sheet.GetRange(5, 1, 5 + lstPupilImportedErr.Count - 1, 35).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            else
            {
                sheet.SetColumnWidth(35, 0);//an cot mo ta neu nhu download template mau
            }

            return book.ToStream();
        }

        public ActionResult OpenImportChidren()
        {

            GlobalInfo glo = new GlobalInfo();
            List<int> lstEducationID = getClassFromEducationLevel(0).Select(x => x.EducationLevelID).Distinct().ToList();
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All
                                                        .Where(x => x.Grade == glo.AppliedLevel && lstEducationID.Contains(x.EducationLevelID)).ToList();
            EducationLevel objAll = new EducationLevel();
            objAll.EducationLevelID = 0;
            objAll.Resolution = "[Tất cả]";
            lstEducationLevel.Add(objAll);
            ViewData["checkFirst"] = 1;
            ViewData["LIST_EDUCATION_LEVEL_IMPORT"] = new SelectList(lstEducationLevel.OrderBy(x => x.EducationLevelID), "EducationLevelID", "Resolution");
            return PartialView("FormImportChildren");
        }

        #endregion

        #region Import
        public JsonResult Import(int? EducationLevelID, FormCollection frm)
        {
            try
            {
                bool isAddPupil = "on".Equals(frm["rdoAddPupil"]);
                bool isUpdatePupil = "on".Equals(frm["rdoUpdatePupil"]);
                Session[PupilProfileChildrenConstants.MESSAGE_ERROR_IMPORT] = null;
                Session[PupilProfileChildrenConstants.MESSAGE_STATUS_SUCCESS] = null;
                HttpPostedFileBase file = Request.Files.Count > 0 ? Request.Files[0] : null;

                if (file == null)
                {
                    throw new BusinessException(Res.Get("InputExaminationMark_Label_NoFileSelected"));
                }

                if (!file.FileName.EndsWith(".xls"))
                {
                    throw new BusinessException(Res.Get("Common_Label_ExtensionError"));
                }

                if (file.ContentLength > 3072000)
                {
                    throw new BusinessException(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"));
                }

                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                IVTWorksheet sheet = book.GetSheet(1);

                if (EducationLevelID == null)
                    EducationLevelID = 0;

                List<ImportChildrenViewModel> lstPupilImported = ReadExcelFile(sheet, EducationLevelID.Value, isAddPupil, isUpdatePupil);
                Session[PupilProfileChildrenConstants.LIST_IMPORTED_PUPIL] = lstPupilImported;

                if (lstPupilImported.Count == 0)
                {
                    throw new BusinessException(Res.Get("ImportPupil_Label_NoPupilImported"));
                }

                bool isAllValid = lstPupilImported.All(u => u.IsValid);
                if (isAllValid)
                    SaveData(isAddPupil);

                int countAllPupil = lstPupilImported.Count;
                int countPupilValid = lstPupilImported.Count(p => p.IsValid);
                string retMsg;
                if (isAllValid)
                {
                    retMsg = String.Format("{0} {1}/{2} trẻ", Res.Get("Common_Label_ImportSuccessMessage"), countPupilValid, countAllPupil);
                    Session[PupilProfileChildrenConstants.MESSAGE_STATUS_SUCCESS] = PupilProfileChildrenConstants.MESSAGE_STATUS_SUCCESS;
                    throw new BusinessException(retMsg);
                }
                JsonResult resx = Json(new JsonMessage(Res.Get("ImportPupil_Label_ImportFailure"), JsonMessage.ERROR), "text/html");
                return resx;
            }
            catch (Exception ex)
            {
                Session[PupilProfileChildrenConstants.MESSAGE_ERROR_IMPORT] = Res.Get(ex.Message);
                JsonResult res = Json(new JsonMessage(Res.Get(ex.Message), JsonMessage.ERROR), "text/html");
                return res;
            }
        }

        public JsonResult LoadImportedGrid()
        {
            if (Session[PupilProfileChildrenConstants.MESSAGE_ERROR_IMPORT] != null)
            {
                if (Session[PupilProfileChildrenConstants.MESSAGE_STATUS_SUCCESS] != null)
                {
                    return Json(new JsonMessage(Session[PupilProfileChildrenConstants.MESSAGE_ERROR_IMPORT].ToString(), JsonMessage.SUCCESS));
                }
                else
                {
                    return Json(new JsonMessage(Session[PupilProfileChildrenConstants.MESSAGE_ERROR_IMPORT].ToString(), JsonMessage.ERROR));
                }
            }
            return Json(new JsonMessage(RenderPartialViewToString("_GridErrorImport", null), "grid"));
            // return PartialView("_gridImported");
        }

        private List<ImportChildrenViewModel> ReadExcelFile(IVTWorksheet sheet, int educationLevelID, bool isAddPupil, bool isUpdatePupil)
        {
            Session["EducationLevelID"] = educationLevelID;
            GlobalInfo glo = new GlobalInfo();
            List<ImportChildrenViewModel> listPupil = new List<ImportChildrenViewModel>();
            List<ImportChildrenViewModel> listPupilTemp = new List<ImportChildrenViewModel>();
            // Congnv: Mẫu cũ
            //int rowIndex = 10;
            int rowIndex = 5;
            // Congnv: Thêm các tên cột trong bảng Excel (Hỗ trợ sửa đổi mẫu sau này)
            const string indexCol = "A";
            const string pupilCodeCol = "B";
            const string fullNameCol = "C";
            const string birthDateCol = "D";
            const string genreNameCol = "E";
            const string classNameCol = "F";
            const string enrolmentTypeNameCol = "G";
            //const string birthDateCol = "H";
            const string enrolmentDateCol = "H";
            const string provinceNameCol = "I";
            const string districtNameCol = "J";
            const string communeNameCol = "K";
            const string villageNameCol = "L";
            const string storageNameCol = "M";
            const string policyTargetNameCol = "N";
            const string policyRegimeNameCol = "O";
            const string disabledTypeNameCol = "P";
            const string ethnicNameCol = "Q";
            const string religionNameCol = "R";
            const string birthPlaceCol = "S";
            const string homeTownCol = "T";
            const string permanentResidentalAddressCol = "U";
            const string tempResidentalAddressCol = "V";
            const string mobileCol = "W";
            const string fatherFullNameCol = "X";
            const string fatherBirthDateCol = "Y";
            const string fatherJobCol = "Z";
            const string fatherMobileCol = "AA";
            const string fatherEmailCol = "AB";
            const string motherFullNameCol = "AC";
            const string motherBirthDateStrCol = "AD";
            const string motherJobCol = "AE";
            const string motherMobileCol = "AF";
            const string motherEmailCol = "AG";
            const string bloodTypeNameCol = "AH";
            const string profileStatusNameCol = "AI";
            const string minoriryFather = "AJ";
            const string minoriryMother = "AK";
            const string pupilCodeNewCol = "AL";

            // Danh sách Tỉnh/thành, Quận/Huyện, Xã/Phường
            List<Province> lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            List<District> lstDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            List<Commune> listCom = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            IQueryable<Village> listVil = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });

            // Lấy các lớp có quyền
            List<int> lstClassID = getClassFromEducationLevel(educationLevelID).Select(x => x.ClassProfileID).ToList();

            Dictionary<string, object> dic1 = new Dictionary<string, object> { 
                { "AcademicYearID", glo.AcademicYearID.Value }, 
                //{ "EducationLevelID", educationLevelID }, 
                { "IsActive", true } 
            };
            if (educationLevelID != 0)
            {
                dic1["EducationLevelID"] = educationLevelID;
            }
            var listClass = ClassProfileBusiness.Search(dic1).Where(x => lstClassID.Contains(x.ClassProfileID)).Select(u => new { u.ClassProfileID, u.DisplayName }).ToList().Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName.ToLower())).ToList();

            var listEthnic = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.EthnicID, u.EthnicName }).ToList().Select(u => new ComboObject(u.EthnicID.ToString(), u.EthnicName.ToLower())).ToList();
            var listPolicyRegime = PolicyRegimeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.PolicyRegimeID, u.Resolution }).ToList().Select(u => new ComboObject(u.PolicyRegimeID.ToString(), u.Resolution.ToLower())).ToList();
            var listDisabledType = DisabledTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.DisabledTypeID, u.Resolution }).ToList().Select(u => new ComboObject(u.DisabledTypeID.ToString(), u.Resolution.ToLower())).ToList();
            var listPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.PolicyTargetID, u.Resolution }).ToList().Select(u => new ComboObject(u.PolicyTargetID.ToString(), u.Resolution.ToLower())).ToList();
            List<string> lstPolicyTargetNameTemp = listPolicyTarget.Select(o => o.value).ToList();
            var listReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ReligionID, u.Resolution }).ToList().Select(u => new ComboObject(u.ReligionID.ToString(), u.Resolution.ToLower())).ToList();
            int tempCount = 0;
            bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(glo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            bool isSchoolModify = CodeConfigBusiness.IsSchoolModify(glo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            string originalPupilCode = string.Empty;
            int maxOrderNumber = 0;
            int numberLength = 0;

            var lstEducationLevel = _globalInfo.EducationLevels.Select(x => x.EducationLevelID).ToList();
            string educationName = "";
            if (educationLevelID > 0)
            {
                educationName = (_globalInfo.EducationLevels.Count > 0 && educationLevelID != 0) ? _globalInfo.EducationLevels.Where(x => x.EducationLevelID == educationLevelID).FirstOrDefault().Resolution : "";
                lstEducationLevel = lstEducationLevel.Where(x => x == educationLevelID).ToList();
            }

            var lstEducationLevelWithClass = ClassProfileBusiness.All.Where(x => lstEducationLevel.Contains(x.EducationLevelID))
                                            .Select(x => new { EducationLevelID = x.EducationLevelID, ClassName = x.DisplayName }).ToList();

            List<AutoGenericCode> lstOutGenericCode = new List<AutoGenericCode>();
            if (isAutoGenCode)
            {
                //CodeConfigBusiness.GetListPupilCodeForImport(_globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, educationLevelID, _globalInfo.SchoolID.Value, out lstResult);               
                AutoGenericCode objAutoGen = null;
                foreach (var item in lstEducationLevel)
                {
                    CodeConfigBusiness.GetPupilCodeForImport(_globalInfo.AcademicYearID.Value, item, _globalInfo.SchoolID.Value, out originalPupilCode, out maxOrderNumber, out numberLength);

                    objAutoGen = new AutoGenericCode();
                    objAutoGen.Index = 0;
                    objAutoGen.OriginalPupilCode = originalPupilCode;
                    objAutoGen.MaxOrderNumber = maxOrderNumber;
                    objAutoGen.NumberLength = numberLength;
                    objAutoGen.EducationLevelID = item;
                    lstOutGenericCode.Add(objAutoGen);
                }
            }
            var listCheckIndex = new List<string>();
            IQueryable<Village> lstVillage = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });
            //Tìm kiếm học sinh trong năm học
            List<PupilProfileBO> lstPP = PupilProfileBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true }, { "CurrentAcademicYearID", glo.AcademicYearID.Value } }).Where(x => x.IsActive).ToList();
            List<PupilOfClass> lstpoc = PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", glo.AcademicYearID.Value } }).ToList();

            // AnhVD9 - 20140829 - Biến lưu dữ liệu tận dụng để giảm thời gian import
            // Tỉnh thành trước đó
            string ProviceNameBefore = string.Empty;
            int ProvinceIdBefore = 0;
            // Quận/Huyện trước đó
            List<District> lstDistrictInProvince = new List<District>();
            string DistrictNameBefore = string.Empty;
            int DistrictIdBefore = 0;

            // AnhVD9 - 20140919 Hotfix -  Thông tin Mã HS không trùng với các năm học khác
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AppliedLevel", glo.AppliedLevel);
            dic.Add("SchoolID", glo.SchoolID);
            dic.Add("AcademicYearID", glo.AcademicYearID); // Năm hiện tại
            // Danh sách ClassId trong năm hiện tại
            List<int> LstClassIDInCurrentYear = ClassProfileBusiness.Search(dic).Select(o => o.ClassProfileID).ToList();
            // Danh sách lớp trong các năm khác - chỉ xét cùng cấp
            List<int> LstClassIDNotInCurrentYear = this.ClassProfileBusiness.All.Where(o => o.SchoolID == glo.SchoolID && o.EducationLevel.Grade == glo.AppliedLevel
                                                            && !LstClassIDInCurrentYear.Contains(o.ClassProfileID) && o.IsActive.Value).Select(o => o.ClassProfileID).ToList();
            // Lấy danh sách HS trong các năm khác trong cùng cấp
            List<string> lstPupilCodeOtherYear = this.PupilOfClassBusiness.All.Where(o => o.SchoolID == glo.SchoolID && LstClassIDNotInCurrentYear.Contains(o.ClassID)).Select(o => o.PupilProfile.PupilCode).ToList();
            List<string> lstPupilCodeCurrentYear = this.PupilOfClassBusiness.All.Where(o => o.SchoolID == glo.SchoolID && LstClassIDInCurrentYear.Contains(o.ClassID)).Select(o => o.PupilProfile.PupilCode).ToList();
            // End Hotfix
            StringBuilder ErrorMessage = new StringBuilder();
            while (!string.IsNullOrEmpty(GetCellString(sheet, indexCol + rowIndex)) || !string.IsNullOrEmpty(GetCellString(sheet, fullNameCol + rowIndex)) || !string.IsNullOrEmpty(GetCellString(sheet, classNameCol + rowIndex)))
            {
                var pupil = new ImportChildrenViewModel();
                pupil.checkInsertVilliage = false;
                pupil.IsValid = true;
                pupil.BirthDateStr = GetCellString(sheet, birthDateCol + rowIndex);
                pupil.EnrolmentDateStr = GetCellString(sheet, enrolmentDateCol + rowIndex);
                string fatherDate = GetCellString(sheet, fatherBirthDateCol + rowIndex);
                string motherDate = GetCellString(sheet, motherBirthDateStrCol + rowIndex);
                string fatherDateTemp = "";
                string motherDateTemp = "";
                int result = 0;
                if (fatherDate != null && fatherDate.Trim() != "" && int.TryParse(fatherDate, out result) && fatherDate.Trim().Length == 4)
                {
                    fatherDateTemp = "01/01/" + fatherDate;
                }

                if (motherDate != null && motherDate.Trim() != "" && int.TryParse(motherDate, out result) && motherDate.Trim().Length == 4)
                {
                    motherDateTemp = "01/01/" + motherDate;
                }

                pupil.FatherBirthDateStr = fatherDate;
                pupil.MotherBirthDateStr = motherDate;

                DateTime? birthDate = GetCellDate(sheet, birthDateCol + rowIndex);
                DateTime? enrolmentDate = GetCellDate(sheet, enrolmentDateCol + rowIndex);
                DateTime? fatherBirthDate = new DateTime?();
                DateTime? motherBirthDate = new DateTime?();
                if (fatherDateTemp != null && fatherDateTemp.Trim() != "")
                {
                    fatherBirthDate = DateTime.Parse(fatherDateTemp);
                }
                if (motherDateTemp != null && motherDateTemp.Trim() != "")
                {
                    motherBirthDate = DateTime.Parse(motherDateTemp);
                }
                string className = GetCellString(sheet, classNameCol + rowIndex);

                pupil.IndexStr = GetCellString(sheet, indexCol + rowIndex);
                int? index = GetCellIndex(pupil.IndexStr);
                var classID = SetFromList(listClass, className);
               
                // Reset data error message
                ErrorMessage = new StringBuilder();
                if (classID == 0)
                {
                    ErrorMessage.Append("Thầy/cô không có quyền với lớp " + className);
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                    listPupil.Add(pupil);
                    rowIndex++;
                    tempCount++;
                    continue;
                }

                if (index.HasValue) pupil.Index = index.Value;
                else
                {
                    pupil.Index = 0;
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_STTNotNumber")).Append(SEMICOLON_SPACE);
                }

                pupil.BirthDate = birthDate;
                pupil.BirthPlace = GetCellString(sheet, birthPlaceCol + rowIndex);
                pupil.BloodTypeName = GetCellString(sheet, bloodTypeNameCol + rowIndex);
                pupil.ClassName = className;
                pupil.EnrolmentDate = enrolmentDate;
                pupil.EnrolmentTypeName = GetCellString(sheet, enrolmentTypeNameCol + rowIndex);
                pupil.EthnicName = GetCellString(sheet, ethnicNameCol + rowIndex);
                pupil.FatherBirthDate = fatherBirthDate;
                pupil.FatherFullName = GetCellString(sheet, fatherFullNameCol + rowIndex);
                pupil.FatherJob = GetCellString(sheet, fatherJobCol + rowIndex);
                pupil.FatherMobile = GetCellString(sheet, fatherMobileCol + rowIndex);
                pupil.FullName = GetCellString(sheet, fullNameCol + rowIndex);
                pupil.Name = GetPupilName(pupil.FullName);
                pupil.GenreName = GetCellString(sheet, genreNameCol + rowIndex);
                pupil.HomeTown = GetCellString(sheet, homeTownCol + rowIndex);
                pupil.Mobile = GetCellString(sheet, mobileCol + rowIndex);
                pupil.MotherBirthDate = motherBirthDate;
                pupil.MotherFullName = GetCellString(sheet, motherFullNameCol + rowIndex);
                pupil.MotherJob = GetCellString(sheet, motherJobCol + rowIndex);
                pupil.MotherMobile = GetCellString(sheet, motherMobileCol + rowIndex);
                pupil.MotherEmail = GetCellString(sheet, motherEmailCol + rowIndex);
                pupil.FatherEmail = GetCellString(sheet, fatherEmailCol + rowIndex);
                pupil.StorageNumber = GetCellString(sheet, storageNameCol + rowIndex);
                pupil.PermanentResidentalAddress = GetCellString(sheet, permanentResidentalAddressCol + rowIndex);
                pupil.PolicyTargetName = GetCellString(sheet, policyTargetNameCol + rowIndex);
                pupil.PupilCode = GetCellString(sheet, pupilCodeCol + rowIndex);
                pupil.PupilCodeNew = GetCellString(sheet, pupilCodeNewCol + rowIndex);
                pupil.ProfileStatusName = GetCellString(sheet, profileStatusNameCol + rowIndex);
                pupil.PolicyRegimeName = GetCellString(sheet, policyRegimeNameCol + rowIndex);
                pupil.DisabledTypeName = GetCellString(sheet, disabledTypeNameCol + rowIndex);
                pupil.ReligionName = GetCellString(sheet, religionNameCol + rowIndex);
                pupil.CurrentSchoolID = glo.SchoolID.Value;
                pupil.TempResidentalAddress = GetCellString(sheet, tempResidentalAddressCol + rowIndex);
                pupil.BloodType = SetBloodType(pupil.BloodTypeName);
                pupil.CurrentClassID = classID;
                pupil.EnrolmentType = SetEnrolementType(pupil.EnrolmentTypeName);
                pupil.MinorityFatherName = GetCellString(sheet, minoriryFather + rowIndex);
                pupil.MinorityMotherName = GetCellString(sheet, minoriryMother + rowIndex);
                pupil.Genre = SetGenreNew(pupil.GenreName);
                pupil.PolicyTargetID = (int?)SetNulableFromList(listPolicyTarget, pupil.PolicyTargetName);
                pupil.ProfileStatus = SetProfileStatus(pupil.ProfileStatusName);

                pupil.EthnicID = (int?)SetNulableFromList(listEthnic, pupil.EthnicName);
                pupil.ReligionID = (int?)SetNulableFromList(listReligion, pupil.ReligionName);
                pupil.PolicyRegimeID = (pupil.PolicyTargetID != -1 && pupil.PolicyTargetID != null) ? (int?)SetNulableFromList(listPolicyRegime, pupil.PolicyRegimeName) : null;
                pupil.DisabledTypeID = (int?)SetNulableFromList(listDisabledType, pupil.DisabledTypeName);
                pupil.IsDisabled = (pupil.DisabledTypeID != -1 && pupil.DisabledTypeID != null) ? 1 : 0;
                #region AnhVD9 20140918 - Kiểm tra không cho Import trùng mã trong năm học trước
                if (!string.IsNullOrWhiteSpace(pupil.PupilCode))
                {
                    if (lstPupilCodeOtherYear.Contains(pupil.PupilCode))
                    {
                        if (!lstPupilCodeCurrentYear.Contains(pupil.PupilCode)) // năm học hiện tại chưa có mã này
                        {
                            ErrorMessage.Append(Res.Get("ImportPupill_PupilCode_InOtherYear")).Append(SEMICOLON_SPACE);
                        }
                    }
                }
                #endregion

                // Tỉnh/thành, Quận/Huyện, Xã/Phường, Thôm/Xóm
                #region Xử lý Tỉnh/thành, Quận/Huyện, Xã/Phường, Thôm/Xóm
                pupil.ProvinceName = GetCellString(sheet, provinceNameCol + rowIndex);
                pupil.DistrictName = GetCellString(sheet, districtNameCol + rowIndex);
                pupil.CommuneName = GetCellString(sheet, communeNameCol + rowIndex);
                pupil.VillageName = GetCellString(sheet, villageNameCol + rowIndex);

                //  Cho phép nhập Tên hoặc Mã Tỉnh thành
                if (!string.IsNullOrEmpty(pupil.ProvinceName))
                {
                    if (pupil.ProvinceName.Equals(ProviceNameBefore))
                    {
                        pupil.ProvinceID = ProvinceIdBefore;
                    }
                    else if (lstProvince.Exists(o => o.ProvinceName.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())))
                    {
                        Province provinceObj = lstProvince.Where(o => o.ProvinceName.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())).FirstOrDefault();
                        if (provinceObj != null)
                        {
                            pupil.ProvinceID = provinceObj.ProvinceID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID).ToList();
                            ProviceNameBefore = pupil.ProvinceName;
                            ProvinceIdBefore = provinceObj.ProvinceID;
                        }
                    }
                    else if (lstProvince.Exists(o => o.ProvinceCode.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())))
                    {
                        Province provinceObj = lstProvince.Where(o => o.ProvinceCode.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())).FirstOrDefault();
                        if (provinceObj != null)
                        {
                            pupil.ProvinceID = provinceObj.ProvinceID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID).ToList();
                            ProviceNameBefore = pupil.ProvinceName;
                            ProvinceIdBefore = provinceObj.ProvinceID;
                        }
                    }
                }

                // Quận/Huyện
                if (!string.IsNullOrEmpty(pupil.DistrictName))
                {
                    if (pupil.DistrictName.Equals(DistrictNameBefore))
                    {
                        pupil.DistrictID = DistrictIdBefore;
                    }
                    else if (lstDistrictInProvince.Exists(o => o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())))
                    {
                        District obj = lstDistrictInProvince.Where(o => o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())).FirstOrDefault();
                        if (obj != null)
                        {
                            pupil.DistrictID = obj.DistrictID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            DistrictNameBefore = pupil.DistrictName;
                            DistrictIdBefore = obj.DistrictID;
                        }

                    }
                    else if (lstDistrict.Exists(o => o.ProvinceID == pupil.ProvinceID && (o.DistrictName != null && o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower())) || (o.DistrictCode != null && o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower()))))
                    {
                        District obj = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID && (o.DistrictName != null && o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower())) || (o.DistrictCode != null && o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())))
                                        .FirstOrDefault();
                        if (obj != null)
                        {
                            pupil.DistrictID = obj.DistrictID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.DistrictID == pupil.DistrictID).ToList();
                            DistrictNameBefore = pupil.DistrictName;
                            DistrictIdBefore = pupil.DistrictID.Value;
                        }
                    }
                }

                // Xã/Phường
                if (!string.IsNullOrEmpty(pupil.CommuneName))
                {
                    if (listCom.Exists(o => o.DistrictID == pupil.DistrictID && (o.CommuneName != null && o.CommuneName.ToLower().Equals(pupil.CommuneName.ToLower())) || (o.CommuneCode != null && o.CommuneCode.ToLower().Equals(pupil.CommuneName.ToLower()))))
                    {
                        Commune comObj = listCom.Where(o => o.DistrictID == pupil.DistrictID && (o.CommuneName != null && o.CommuneName.ToLower().Equals(pupil.CommuneName.ToLower())) || (o.CommuneCode != null && o.CommuneCode.ToLower().Equals(pupil.CommuneName.ToLower())))
                                        .FirstOrDefault();
                        if (comObj != null)
                        {
                            pupil.CommuneID = comObj.CommuneID;
                        }
                    }
                }

                // Thôn/Xóm
                if (!string.IsNullOrEmpty(pupil.VillageName) && pupil.CommuneID != null && pupil.CommuneID != 0)
                {
                    if (listVil.Any(o => o.CommuneID == pupil.CommuneID && ((o.VillageName != null && o.VillageName.ToLower().Equals(pupil.VillageName.ToLower())) || (o.VillageCode != null && o.VillageCode.ToLower().Equals(pupil.VillageName.ToLower())))))
                    {
                        Village villageObj = listVil.Where(o => o.CommuneID == pupil.CommuneID && ((o.VillageName != null && o.VillageName.ToLower().Equals(pupil.VillageName.ToLower())) || (o.VillageCode != null && o.VillageCode.ToLower().Equals(pupil.VillageName.ToLower())))).FirstOrDefault();
                        if (villageObj != null)
                        {
                            pupil.VillageID = villageObj.VillageID;
                        }
                    }
                }
                #endregion End AnhVD20140825
                string pupilCode = originalPupilCode + ToStringWithFixedLength(maxOrderNumber + 1 + tempCount, numberLength);
                #region // isAutoGenCode
                AutoGenericCode objAutoGenericCode = new AutoGenericCode();
                var tempt = lstEducationLevelWithClass.Where(x => x.ClassName.ToUpper().Equals(className.ToUpper())).FirstOrDefault();
                if (tempt == null && educationLevelID != 0)
                {
                    ErrorMessage.Append(className + " không thuộc khối đã chọn");
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                    listPupil.Add(pupil);
                    rowIndex++;
                    tempCount++;
                    continue;
                }
                else if (tempt != null)
                {
                    objAutoGenericCode = lstOutGenericCode.Where(x => x.EducationLevelID == tempt.EducationLevelID).FirstOrDefault();
                    if (isAutoGenCode && objAutoGenericCode != null)
                    {
                        originalPupilCode = objAutoGenericCode.OriginalPupilCode;
                        maxOrderNumber = objAutoGenericCode.MaxOrderNumber;
                        numberLength = objAutoGenericCode.NumberLength;
                        tempCount = objAutoGenericCode.Index;
                        pupilCode = originalPupilCode + ToStringWithFixedLength(maxOrderNumber + 1 + tempCount, numberLength);

                        if (isSchoolModify)
                        {
                            if (string.IsNullOrEmpty(pupil.PupilCode))
                            {
                                pupil.PupilCode = pupilCode;
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(pupil.PupilCode))
                            {
                                pupil.PupilCode = pupilCode;
                            }
                            else
                            {
                                int temp = 0;
                                int.TryParse(pupil.PupilCode.Substring((pupil.PupilCode.Length - numberLength), numberLength), out temp);
                                if (!pupil.PupilCode.StartsWith(originalPupilCode) || pupil.PupilCode.Length != pupilCode.Length || temp == 0)
                                {
                                    ErrorMessage.Append(Res.Get("ImportPupil_Label_OriginalPupilCode")).Append(SEMICOLON_SPACE);
                                }
                            }
                        }
                    }
                }


                #endregion

                if (isUpdatePupil)
                {
                    #region true
                    if (lstPP.Any(o => o.PupilCode == pupil.PupilCode))
                    {
                        PupilProfileBO pupilProfile = lstPP.Where(o => o.PupilCode == pupil.PupilCode).OrderBy(o => o.AssignedDate).LastOrDefault();
                        List<PupilOfClass> lstpoc_temp = lstpoc.Where(o => o.PupilID == pupilProfile.PupilProfileID).ToList();
                        if (lstpoc_temp.Count() >= 2)
                        {
                            PupilOfClass pocAssignMax = lstpoc_temp.OrderBy(o => o.AssignedDate).LastOrDefault();
                            if (pupil.EnrolmentDate >= pocAssignMax.AssignedDate)
                            {
                                ErrorMessage.Append(Res.Get("PupilProfile_Validate_AssignDate")).Append(SEMICOLON_SPACE);
                            }
                        }
                        if (!string.IsNullOrEmpty(pupil.PupilCodeNew))
                        {
                            if (lstPP.Any(o => o.PupilCode == pupil.PupilCodeNew))
                            {
                                ErrorMessage.Append(string.Format("Mã trẻ {0} đã tồn tại.", pupil.PupilCodeNew)).Append(SEMICOLON_SPACE);
                            }
                        }
                        if (pupilProfile.CurrentClassID != classID)
                        {
                            dic = new Dictionary<string, object>();
                            dic["AcademicYearID"] = glo.AcademicYearID.Value;
                            dic["PupilID"] = pupilProfile.PupilProfileID;
                            dic["ClassID"] = pupilProfile.CurrentClassID;

                            bool PupilAbsenseConstraint = PupilAbsenceBusiness.SearchBySchool(glo.SchoolID.Value, dic).Any();
                            if (PupilAbsenseConstraint)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupilChildren_Label_PupilAbsenceExist")).Append(SEMICOLON_SPACE);
                            }
                            bool DevelopmentOfChildrenConstraint = DevelopmentOfChildrenBusiness.SearchBySchool(glo.SchoolID.Value, dic).Any();
                            if (DevelopmentOfChildrenConstraint)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupilChildren_Label_DevelopmentOfChildren_Exist")).Append(SEMICOLON_SPACE);
                            }
                            bool GoodTicketChildrenConstraint = GoodChildrenTicketBusiness.SearchBySchool(glo.SchoolID.Value, dic).Any();
                            if (GoodTicketChildrenConstraint)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupilChildren_Label_GoodTicketChildren_Exist")).Append(SEMICOLON_SPACE);
                            }
                        }
                    }
                    else
                    {
                        ErrorMessage.Append(Res.Get("Trẻ không tồn tại để cập nhật thông tin.")).Append(SEMICOLON_SPACE);
                        pupil.IsValid = false;
                        pupil.ErrorMessage += ErrorMessage.ToString();
                        listPupil.Add(pupil);
                        rowIndex++;
                        tempCount++;
                        continue;
                    }
                    #endregion
                }
                else if (isAddPupil)
                {
                    if (lstPP.Any(o => o.PupilCode == pupil.PupilCode))
                    {
                        ErrorMessage.Append(Res.Get("Đã tồn tại trẻ này, Thầy/cô không thể thêm."));
                        pupil.IsValid = false;
                        pupil.ErrorMessage += ErrorMessage.ToString();
                        listPupil.Add(pupil);
                        rowIndex++;
                        tempCount++;
                        continue;
                    }
                }

                ValidateImportedPupil(pupil);

                //check Email
                if (!string.IsNullOrEmpty(pupil.MotherEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.MotherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "mẹ")).Append(SEMICOLON_SPACE);
                    else if (pupil.MotherEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "mẹ")).Append(SEMICOLON_SPACE);
                    }
                }
                //check Email Father
                if (!string.IsNullOrEmpty(pupil.FatherEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.FatherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "cha")).Append(SEMICOLON_SPACE);
                    else if (pupil.FatherEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "cha")).Append(SEMICOLON_SPACE);
                    }
                }

                if (pupil.ReligionID == -1 && pupil.ReligionName != null && pupil.ReligionName.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_ReligionExisted")).Append(SEMICOLON_SPACE);
                }

                if (pupil.EthnicID == -1 && pupil.EthnicName != null && pupil.EthnicName.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_EthnicExisted")).Append(SEMICOLON_SPACE);
                }

                if (listPupil.Where(x => x.PupilCode != null).Any(u => u.PupilCode.ToLower().Equals(pupil.PupilCode.ToLower())))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportChildren_Label_PupilCodeExisted"), pupil.PupilCode)).Append(SEMICOLON_SPACE);
                }

                if (pupil.Index > 0 && pupil.CurrentClassID > 0 && listPupil.Any(u => u.Index == pupil.Index && u.CurrentClassID == pupil.CurrentClassID))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_IndexClassExisted"), pupil.ClassName, pupil.Index)).Append(SEMICOLON_SPACE);
                }

                // day Lớp 4-5 Ghép 01 không thuộc khối đã chọn;
                if (educationLevelID != 0 && !string.IsNullOrEmpty(pupil.ClassName) && !listClass.Any(u => u.key.Equals(pupil.CurrentClassID.ToString())))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_ClassNotExist"), pupil.ClassName)).Append(SEMICOLON_SPACE);
                }

                if (pupil.PolicyTargetID.HasValue && !listPolicyTarget.Any(o => pupil.PolicyTargetID != null && o.key == pupil.PolicyTargetID.Value.ToString()))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidPolicyTargetName")).Append(SEMICOLON_SPACE);
                }

                if (string.IsNullOrEmpty(pupil.ClassName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidClassNameNull")).Append(SEMICOLON_SPACE);
                }
                if (string.IsNullOrEmpty(pupil.ProvinceName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidProvinceNameNull")).Append(SEMICOLON_SPACE);
                }
                if (pupil.ProvinceName != null && pupil.ProvinceName.Trim() != "" && pupil.ProvinceID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidProvinceName")).Append(SEMICOLON_SPACE);
                }

                if (pupil.DistrictName != null && pupil.DistrictName.Trim() != "" && pupil.DistrictID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrictName")).Append(SEMICOLON_SPACE);
                }
                bool checkDistrict = checkCompareDistrict(pupil.ProvinceID, pupil.DistrictID);
                bool checkCommune = checkCompareCommune(pupil.DistrictID, pupil.CommuneID);
                //Nhập tỉnh thành trước khi nhập quận huyện
                if (pupil.DistrictID != null && pupil.ProvinceID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrictProvince")).Append(SEMICOLON_SPACE);
                }
                if (pupil.CommuneID != null && (pupil.DistrictID == null || pupil.ProvinceID == null))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommuneDistrict")).Append(SEMICOLON_SPACE);
                }
                if (!checkDistrict)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrict")).Append(SEMICOLON_SPACE);
                }
                if (pupil.CommuneName != null && pupil.CommuneName.Trim() != "" && pupil.CommuneID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommuneName")).Append(SEMICOLON_SPACE);
                }
                if (!string.IsNullOrEmpty(pupil.StorageNumber) && pupil.StorageNumber.Length > 30)
                {
                    ErrorMessage.Append(Res.Get("Common_Label_Maxlength_StorageNumber")).Append(SEMICOLON_SPACE);
                }

                if (!checkCommune)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommune")).Append(SEMICOLON_SPACE);
                }

                if (!checkCompareVillage(pupil.VillageID, pupil.CommuneID))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliageName")).Append(SEMICOLON_SPACE);
                }

                if (pupil.CommuneID == null || pupil.DistrictID == null || pupil.ProvinceID == null)
                {
                    if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                    {
                        ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliage")).Append(SEMICOLON_SPACE);
                    }
                }
                if (pupil.VillageName != null && pupil.VillageName.Trim().Length > 50)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliageLength")).Append(SEMICOLON_SPACE);
                }
                if ((int.TryParse(fatherDate, out result) == false || fatherDate.Trim().Length != 4) && fatherDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidFatherBirthDate")).Append(SEMICOLON_SPACE);
                }
                if ((int.TryParse(motherDate, out result) == false || motherDate.Trim().Length != 4) && motherDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidMotherBirthDate")).Append(SEMICOLON_SPACE);
                }
                //Kiểm tra thôn xóm có chưa?
                if (checkDistrict == true && checkCommune == true && pupil.IsValid == true)
                {
                    if (pupil.CommuneID != null && pupil.DistrictID != null && pupil.ProvinceID != null)
                    {
                        bool isCheck = listPupilTemp.Where(o => o.VillageName.Trim().ToUpper() == pupil.VillageName.Trim().ToUpper() && o.CommuneID == pupil.CommuneID).Count() > 0 ? true : false;

                        if (pupil.VillageID == null && pupil.VillageName != null && pupil.VillageName.Trim() != "" && !isCheck)
                        {
                            bool isCheck1 = lstVillage.Where(o => o.VillageName.Trim().ToUpper() == pupil.VillageName.Trim().ToUpper() && o.CommuneID == pupil.CommuneID).Count() > 0 ? true : false;
                            if (!isCheck1)
                            {
                                pupil.checkInsertVilliage = true;
                            }
                        }
                    }
                }

                // Gán lỗi
                if (ErrorMessage.Length > 0)
                {
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                }

                if (pupil.IsValid)
                    listPupilTemp.Add(pupil);
                else // Xóa bỏ ký tự ; cuối cùng
                {
                    if (!String.IsNullOrEmpty(pupil.ErrorMessage) && pupil.ErrorMessage.Length > 2)
                    {
                        pupil.ErrorMessage = pupil.ErrorMessage.Remove(pupil.ErrorMessage.Length - 2);
                    }
                }

                listPupil.Add(pupil);

                rowIndex++;
                if (tempt != null)
                {
                    objAutoGenericCode.Index++;
                    lstOutGenericCode.RemoveAll(x => x.EducationLevelID == objAutoGenericCode.EducationLevelID);
                    lstOutGenericCode.Add(objAutoGenericCode);
                }
                tempCount++;
            } // End while
            return listPupil;
        }


        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult Save(FormCollection frm)
        {
            bool isAddPupil = frm["hdfIsAddPupil"] != null && "1".Equals(frm["hdfIsAddPupil"]) ? true : false;
            List<ImportChildrenViewModel> listPupil = (List<ImportChildrenViewModel>)Session[PupilProfileChildrenConstants.LIST_IMPORTED_PUPIL];
            SaveData(isAddPupil);
            int countAllPupil = listPupil.Count;
            int countPupilValid = listPupil.Count(p => p.IsValid);
            string retMsg = String.Format("{0} {1}/{2} trẻ", Res.Get("Common_Label_ImportSuccessMessage"), countPupilValid, countAllPupil);
            return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS), "text/html");

            //return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        private void SaveData(bool IsAddPupil)
        {
            GlobalInfo glo = new GlobalInfo();

            List<ImportChildrenViewModel> listPupil = (List<ImportChildrenViewModel>)Session[PupilProfileChildrenConstants.LIST_IMPORTED_PUPIL];
            listPupil = listPupil.Where(x => x.IsValid).ToList();
            if (listPupil == null || !listPupil.Any())
            {
                return;
            }
            List<string> lstPupilCode = listPupil.Select(o => o.PupilCode).ToList();
            List<PupilProfileBO> listPupilUpdate = PupilProfileBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "IsActive", true } })
                                                                       .Where(o => o.IsActive && o.CurrentSchoolID == glo.SchoolID.Value && lstPupilCode.Contains(o.PupilCode))
                                                                       .ToList();
            List<string> listPupilCodeUpdate = listPupilUpdate.Select(o => o.PupilCode).ToList();

            List<ImportChildrenViewModel> lstPupilInsert = listPupil.Where(o => !listPupilCodeUpdate.Any(t => t.ToLower().Equals(o.PupilCode.ToLower()))).ToList();
            List<ImportChildrenViewModel> lstPupilUpdate = listPupil.Where(o => listPupilCodeUpdate.Any(t => t.ToLower().Equals(o.PupilCode.ToLower()))).ToList();
            List<int> lstUpdatePupilID = listPupilUpdate.Select(o => o.PupilProfileID).ToList();
            List<PupilProfile> lstPupilUpdatePP = PupilProfileBusiness.All
                                                   .Where(o => o.CurrentSchoolID == glo.SchoolID.Value && lstUpdatePupilID.Contains(o.PupilProfileID)).ToList();
            //Lay danh sach lop
            List<int> lstClassID = listPupil.Select(o => o.CurrentClassID).Distinct().ToList();
            List<PupilProfile> lstPupilInsertPP = new List<PupilProfile>();
            List<PupilProfile> lstPupilUpdateAll = new List<PupilProfile>();
            List<int> lstEnrollmentTypeInsert = new List<int>();
            List<int> lstEnrollmentTypeUpdate = new List<int>();
            List<int> lstOrderInsert = new List<int>();
            List<int> lstOrderUpdate = new List<int>();
            List<int> lstClassIDUpdate = new List<int>();
            List<ImportChildrenViewModel> lstPupilInsertClass = new List<ImportChildrenViewModel>();

            List<ImportChildrenViewModel> lstPupilUpdateClass = new List<ImportChildrenViewModel>();
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = glo.AcademicYearID;
            List<ImportChildrenViewModel> listPupilVilliage = listPupil.Where(o => o.checkInsertVilliage == true).ToList();

            foreach (int ClassID in lstClassID)
            {
                #region Insert
                lstPupilInsertClass = lstPupilInsert.Where(o => o.CurrentClassID == ClassID).ToList();
                foreach (ImportChildrenViewModel pupil in lstPupilInsertClass)
                {
                    if (pupil.IsValid)
                    {
                        var pupilProfile = new PupilProfile
                        {
                            CurrentAcademicYearID = glo.AcademicYearID.Value,
                            IsActive = true
                        };
                        bool isReal = listPupilVilliage.Where(o => o.PupilCode == pupil.PupilCode).Count() > 0 ? true : false;
                        if (isReal)
                        {
                            Village village = new Village();
                            village.VillageName = pupil.VillageName;
                            village.ShortName = pupil.VillageName;
                            village.CommuneID = pupil.CommuneID;
                            village.CreateDate = DateTime.Now;
                            village.IsActive = true;
                            VillageBusiness.Insert(village);
                            VillageBusiness.Save();
                            pupil.VillageID = village.VillageID;
                        }
                        else
                        {
                            if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                            {
                                Village vil = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true }, { "VillageName", pupil.VillageName }, { "CommuneID", pupil.CommuneID } }).FirstOrDefault();
                                if (vil != null)
                                {
                                    pupil.VillageID = vil.VillageID;
                                }
                            }
                            else
                            {
                                pupil.VillageID = null;

                            }
                        }
                        CopyValue(pupilProfile, pupil, true);
                        lstPupilInsertPP.Add(pupilProfile);
                        lstEnrollmentTypeInsert.Add(pupil.EnrolmentType);
                        lstOrderInsert.Add(pupil.Index);
                    }
                }
                #endregion

                #region update
                lstPupilUpdateClass = lstPupilUpdate.Where(o => o.CurrentClassID == ClassID).ToList();
                foreach (ImportChildrenViewModel pupil in lstPupilUpdateClass)
                {
                    PupilProfileBO ppBO = listPupilUpdate.FirstOrDefault(o => o.PupilCode == pupil.PupilCode);
                    if (pupil.IsValid)
                    {

                        PupilProfile pupilProfile = lstPupilUpdatePP.FirstOrDefault(o => o.PupilProfileID == ppBO.PupilProfileID);
                        if (pupilProfile != null)
                        {
                            int currentClassID = pupilProfile.CurrentClassID;
                            pupilProfile.CurrentAcademicYearID = glo.AcademicYearID.Value;
                            pupilProfile.IsActive = true;
                            bool isReal = listPupilVilliage.Where(o => o.PupilCode == pupil.PupilCode).Count() > 0 ? true : false;
                            if (isReal)
                            {
                                Village village = new Village();
                                village.VillageName = pupil.VillageName;
                                village.ShortName = pupil.VillageName;
                                village.CommuneID = pupil.CommuneID;
                                village.CreateDate = DateTime.Now;
                                village.IsActive = true;
                                VillageBusiness.Insert(village);
                                VillageBusiness.Save();
                                pupil.VillageID = village.VillageID;
                            }
                            else
                            {
                                if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                                {
                                    Village vil = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true }, { "VillageName", pupil.VillageName }, { "CommuneID", pupil.CommuneID } }).FirstOrDefault();
                                    if (vil != null)
                                    {
                                        pupil.VillageID = vil.VillageID;
                                    }
                                }
                                else
                                {
                                    pupil.VillageID = null;

                                }
                            }
                            CopyValue(pupilProfile, pupil, false);
                            lstPupilUpdateAll.Add(pupilProfile);
                            lstEnrollmentTypeUpdate.Add(pupil.EnrolmentType);
                            lstClassIDUpdate.Add(currentClassID);
                        }
                        lstOrderUpdate.Add(pupil.Index);
                    }
                }
                #endregion
            }
            if (lstPupilInsertPP.Any() && IsAddPupil)
            {
                PupilProfileBusiness.ImportPupilProfile(glo.UserAccountID, lstPupilInsertPP, lstEnrollmentTypeInsert, lstOrderInsert, lstClassID);
            }
            if (lstPupilUpdateAll.Any() && !IsAddPupil)
            {
                PupilProfileBusiness.UpdateImportPupilProfile(glo.UserAccountID, lstPupilUpdateAll, lstEnrollmentTypeUpdate, lstOrderUpdate, lstClassIDUpdate, lstClassID);
            }

            VillageBusiness.Save();
            PupilProfileBusiness.Save();
        }
        #endregion

        #region Util Functions

        private void CopyValue(PupilProfile pupilProfile, ImportChildrenViewModel pupil, bool isInsert)
        {
            pupilProfile.BirthDate = pupil.BirthDate.Value;
            pupilProfile.BirthPlace = pupil.BirthPlace;
            pupilProfile.BloodType = pupil.BloodType;
            pupilProfile.CreatedDate = DateTime.Now;
            pupilProfile.CurrentClassID = pupil.CurrentClassID;
            pupilProfile.CurrentSchoolID = pupil.CurrentSchoolID;
            pupilProfile.EnrolmentDate = pupil.EnrolmentDate.Value;
            pupilProfile.EthnicID = pupil.EthnicID;
            pupilProfile.FatherBirthDate = pupil.FatherBirthDate;
            pupilProfile.FatherEmail = string.Empty;
            pupilProfile.FatherFullName = pupil.FatherFullName;
            pupilProfile.FatherJob = pupil.FatherJob;
            pupilProfile.FatherMobile = pupil.FatherMobile;
            pupilProfile.ForeignLanguageTraining = pupil.ForeignLanguageTraining;
            pupilProfile.FullName = pupil.FullName;
            pupilProfile.Genre = pupil.Genre;
            pupilProfile.HomeTown = pupil.HomeTown;
            pupilProfile.MotherBirthDate = pupil.MotherBirthDate;
            pupilProfile.MotherEmail = string.Empty;
            pupilProfile.MotherFullName = pupil.MotherFullName;
            pupilProfile.MotherJob = pupil.MotherJob;
            pupilProfile.MotherMobile = pupil.MotherMobile;
            pupilProfile.MotherEmail = pupil.MotherEmail;
            pupilProfile.FatherEmail = pupil.FatherEmail;
            pupilProfile.StorageNumber = pupil.StorageNumber;
            pupilProfile.Mobile = pupil.Mobile;
            pupilProfile.Name = pupil.Name;
            pupilProfile.PermanentResidentalAddress = pupil.PermanentResidentalAddress;
            pupilProfile.PolicyTargetID = pupil.PolicyTargetID;
            pupilProfile.ProfileStatus = pupil.ProfileStatus;
            if (pupil.ProvinceID != null) pupilProfile.ProvinceID = (int)pupil.ProvinceID;
            pupilProfile.PupilCode = (!string.IsNullOrEmpty(pupil.PupilCodeNew) && isInsert == false) ? pupil.PupilCodeNew : pupil.PupilCode;
            pupilProfile.ReligionID = pupil.ReligionID;
            pupilProfile.TempResidentalAddress = pupil.TempResidentalAddress;
            //Do import moi cap nhat lai ProvinceID nen de DistrictID và CommuneID la null
            pupilProfile.CommuneID = pupil.CommuneID;
            pupilProfile.DistrictID = pupil.DistrictID;
            pupilProfile.VillageID = pupil.VillageID;
            pupilProfile.MinorityFather = (pupil.MinorityFatherName != null && pupil.MinorityFatherName.ToString() == "x") ? true : false;
            pupilProfile.MinorityMother = (pupil.MinorityMotherName != null && pupil.MinorityMotherName.ToString() == "x") ? true : false;
            pupilProfile.PolicyRegimeID = pupil.PolicyRegimeID;
            pupilProfile.DisabledTypeID = pupil.DisabledTypeID;
            pupilProfile.IsDisabled = pupil.IsDisabled == 1 ? true : false;
        }

        private string GetCellString(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }

        private DateTime? GetCellDate(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            var culture = new CultureInfo("vi-VN");
            if (o != null)
            {
                try
                {
                    if (o is DateTime)
                        return (DateTime)o;
                    var oStr = o.ToString().Trim();
                    if (oStr.EndsWith(".00"))
                        oStr = oStr.Remove(oStr.Length - 4, 3);
                    if (oStr.EndsWith(".0"))
                        oStr = oStr.Remove(oStr.Length - 3, 2);

                    DateTime dt;

                    DateTime.TryParse(oStr, out dt);
                    return dt;
                }
                catch { }
            }
            return null;
        }

        private int SetGenre(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = CommonList.GenreAndSelect().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? (int)255 : Convert.ToByte(c.key);
        }

        private int SetGenreNew(string value)
        {
            value = value == null ? string.Empty : value.Trim();
            return value == string.Empty ? SystemParamsInFile.GENRE_MALE : SystemParamsInFile.GENRE_FEMALE;
        }

        private int SetEnrolementType(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            ComboObject c = CommonList.EnrolmentType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int? SetBloodType(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty) return null;

            ComboObject c = CommonList.BloodType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetForeignLanguageTraining(string value)
        {
            GlobalInfo glo = new GlobalInfo();
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty)
                return glo.AppliedLevel != null && glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY ? (int)SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_3_YEARS : (int)SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED;

            ComboObject c = CommonList.ForeignLanguageTraining().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetProfileStatus(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = CommonList.PupilStatus().SingleOrDefault(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString() && u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetFromList(List<ComboObject> list, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? 0 : Convert.ToInt32(c.key);
        }

        private int SetFromListCommune(List<Village> listVillage, int? communeID, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            Village village = listVillage.Where(o => o.VillageName.Trim().ToLower() == value && o.CommuneID == communeID).FirstOrDefault();
            if (village != null)
                return village.VillageID;
            else
                return 0;
        }

        private int? SetNulableFromList(List<ComboObject> list, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty) return null;

            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToInt32(c.key);
        }

        private void ValidateImportedPupil(ImportChildrenViewModel pupil)
        {
            ValidationContext vc = new ValidationContext(pupil, null, null);
            List<string> messages = new List<string>();
            foreach (PropertyInfo pi in pupil.GetType().GetProperties())
            {
                string resKey = string.Empty;
                if (pi.Name == "FatherBirthDate" || pi.Name == "MotherBirthDate" || pi.Name == "MotherEmail" || pi.Name == "FatherEmail")
                {
                    continue;
                }
                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is DisplayAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }

                    if (att is DisplayNameAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }
                }

                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is ValidationAttribute)
                    {
                        ValidationAttribute attV = (ValidationAttribute)att;
                        vc.MemberName = pi.Name;
                        if (attV.GetValidationResult(pi.GetValue(pupil, null), vc) != ValidationResult.Success)
                        {
                            List<object> Params = new List<object>();
                            Params.Add(Res.Get(resKey));

                            if (att is StringLengthAttribute)
                            {
                                StringLengthAttribute attS = (StringLengthAttribute)att;
                                Params.Add(attS.MaximumLength);
                            }

                            messages.Add(string.Format(Res.Get(attV.ErrorMessageResourceName), Params.ToArray()));
                        }
                    }
                }
            }

            if (messages.Count > 0)
            {
                pupil.IsValid = false;
                pupil.ErrorMessage += string.Join(SEMICOLON_SPACE, messages);
                pupil.ErrorMessage += SEMICOLON_SPACE;
            }
        }

        private string GetPupilName(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
                return string.Empty;

            fullName = fullName.Trim();

            int index = fullName.LastIndexOf(' ');

            return index > 0 ? fullName.Substring(index + 1) : fullName;
        }

        private int? GetCellIndex(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Trim() == "" || !Regex.IsMatch(value, "^\\d+$"))
                return null;

            return Convert.ToInt32(value.Trim());
        }

        private string ToStringWithFixedLength(int number, int length)
        {
            if (number < 0) return string.Empty;

            if (number.ToString().Length > length) return string.Empty;

            return number.ToString().PadLeft(length, '0');
        }

        private bool checkCompareDistrict(int? provinceID, int? districtID)
        {
            if (districtID == null) return true;
            if (districtID != null && provinceID == null) return false;
            var check = DistrictBusiness.Search(new Dictionary<string, object> { { "ProvinceID", provinceID }, { "DistrictID", districtID }, { "IsActive", true } });
            if (check.Count() > 0)
                return true;
            return false;
        }
        private bool checkCompareCommune(int? districtID, int? communeID)
        {
            if (communeID == null || districtID == null)
                return true;
            if (communeID != null && districtID == null) return false;
            var check = CommuneBusiness.Search(new Dictionary<string, object> { { "DistrictID", districtID }, { "CommuneID", communeID }, { "IsActive", true } });
            if (check.Count() > 0)
                return true;
            return false;
        }
        private bool checkCompareVillage(int? villiageID, int? communeID)
        {
            if (villiageID == null || communeID == null) return true;
            if (villiageID != null && communeID == null) return false;
            var check = VillageBusiness.Search(new Dictionary<string, object> { { "CommuneID", communeID }, { "Villiage", villiageID }, { "IsActive", true } });
            if (check.Count() > 0)
                return true;
            return false;
        }
        #endregion

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}