﻿using System;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using Resources;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.PupilProfileChildrenArea.Models
{
    public class LeavingOffModel
    {
        public int? PupilLeavingOffID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ClassID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Pupil")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PupilID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Fullname")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Semester")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int Semester { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_LeavingDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]	
        public DateTime LeavingDate { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_LeavingReason")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int LeavingReasonID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}