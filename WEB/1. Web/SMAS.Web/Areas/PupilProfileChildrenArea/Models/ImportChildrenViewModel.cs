﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using SMAS.Web.Utils;
using Resources;
using System.Text.RegularExpressions;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.PupilProfileChildrenArea.Models
{
    public class ImportChildrenViewModel
    {
        public int Index { get; set; }

        public string IndexStr { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public int PupilProfileID { get; set; }

        /// <summary>
        /// Ma hoc sinh
        /// </summary>
        [Display(Name = "PupilProfile_Label_PupilCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string PupilCode { get; set; }

        public string PupilCodeNew { get; set; }

        public int IsDisabled { get; set; }

        /// <summary>
        /// Ho va Ten hoc sinh
        /// </summary>
        [Display(Name = "PupilProfile_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string FullName { get; set; }

        /// <summary>
        /// Ten hoc sinh
        /// Validate phu thuoc vao FullName
        /// </summary>
        [Display(Name = "PupilProfile_Label_Name")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Name { get; set; }

        /// <summary>
        /// Ngay sinh
        /// </summary>
        [Display(Name = "PupilProfile_Label_BirthDate")]
        [DateTimeValidation(AllowNull = false, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ImportPupil_Label_InvalidDate")]
        public DateTime? BirthDate { get; set; }

        public string BirthDateStr { get; set; }

        /// <summary>
        /// Gioi tinh
        /// </summary>
        [Display(Name = "PupilProfile_Label_Genre")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [CommonListValidation(AllowNull = false, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Invalid")]
        public int Genre { get; set; }

        /// <summary>
        /// Class ID
        /// </summary>
        public int CurrentClassID { get; set; }

        /// <summary>
        /// School ID
        /// </summary>
        public int CurrentSchoolID { get; set; }

        /// <summary>
        /// Hinh thuc trung tuyen
        /// </summary>
        [Display(Name = "PupilProfile_Label_EnrolmentType")]
        [CommonListValidation(AllowNull = false, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Invalid")]
        public int EnrolmentType { get; set; }

        /// <summary>
        /// Ngay nhap truong
        /// </summary>
        [Display(Name = "PupilProfile_Label_EnrolmentDate")]
        [DateTimeValidation(AllowNull = false, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ImportPupil_Label_InvalidDate")]
        public DateTime? EnrolmentDate { get; set; }

        public string EnrolmentDateStr { get; set; }

        /// <summary>
        /// Tinh thanh
        /// </summary>
        public int? ProvinceID { get; set; }

        /// <summary>
        /// Quan/huyen
        /// </summary>
        public int? DistrictID { get; set; }

        /// <summary>
        /// Xa/Phuong
        /// </summary>
        public int? CommuneID { get; set; }

        /// <summary>
        /// Thon/xom
        /// </summary>
        public int? VillageID { get; set; }

        /// <summary>
        /// Doi tuong chinh sach
        /// </summary>
        public int? PolicyTargetID { get; set; }

        /// <summary>
        /// Dan toc
        /// </summary>
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [Display(Name = "PupilProfile_Label_Ethnic")]
        public int? EthnicID { get; set; }
        public int? DisabledTypeID { get; set; }
        public int? PolicyRegimeID { get; set; }

        /// <summary>
        /// Ton giao
        /// </summary>
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [Display(Name = "PupilProfile_Label_Religion")]
        public int? ReligionID { get; set; }

        /// <summary>
        /// Noi sinh
        /// </summary>
        [Display(Name = "PupilProfile_Label_BirthPlace")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string BirthPlace { get; set; }

        /// <summary>
        /// Que quan
        /// </summary>
        [Display(Name = "PupilProfile_Label_HomeTown")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string HomeTown { get; set; }

        /// <summary>
        /// Dia chi thuong tru
        /// </summary>
        [Display(Name = "PupilProfile_Label_PermanentResidentalAddress")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string PermanentResidentalAddress { get; set; }

        /// <summary>
        /// Dia chi tam tru
        /// </summary>
        [Display(Name = "PupilProfile_Label_TempResidentalAddress")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string TempResidentalAddress { get; set; }

        /// <summary>
        /// So dien thoai
        /// </summary>
        [Display(Name = "PupilProfile_Label_FatherMobile")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [MobileValidation(Pattern = "^\\d{0,15}$", ErrorMessageResourceName = "Common_Validate_NotIsNumber")]
        public string Mobile { get; set; }

        /// <summary>
        /// Ho ten cha
        /// </summary>
        [Display(Name = "PupilProfile_Label_FatherFullName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string FatherFullName { get; set; }

        /// <summary>
        /// Ngay sinh cua cha
        /// </summary>
        [Display(Name = "PupilProfile_Label_FatherBirthDate")]
        [DateTimeValidation(AllowNull = true, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ImportPupil_Label_InvalidDate")]
        public DateTime? FatherBirthDate { get; set; }

        public string FatherBirthDateStr { get; set; }

        /// <summary>
        /// Nghe cua cha
        /// </summary>
        [Display(Name = "PupilProfile_Label_FatherJob")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FatherJob { get; set; }

        /// <summary>
        /// So dien thoai cua cha
        /// </summary>
        [Display(Name = "PupilProfile_Label_FatherMobile")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [MobileValidation(Pattern = "^\\d{0,15}$", ErrorMessageResourceName = "Common_Validate_NotIsNumber")]
        public string FatherMobile { get; set; }

        /// <summary>
        /// Ho ten me
        /// </summary>
        [Display(Name = "PupilProfile_Label_MotherFullName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string MotherFullName { get; set; }

        /// <summary>
        /// Ngay sinh cua me
        /// </summary>
        [Display(Name = "PupilProfile_Label_MotherBirthDate")]
        [DateTimeValidation(AllowNull = true, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ImportPupil_Label_InvalidDate")]
        public DateTime? MotherBirthDate { get; set; }

        public string MotherBirthDateStr { get; set; }

        /// <summary>
        /// Nghe cua me
        /// </summary>
        [Display(Name = "PupilProfile_Label_MotherJob")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string MotherJob { get; set; }

        /// <summary>
        /// So dien thoai cua me
        /// </summary>
        [Display(Name = "PupilProfile_Label_MotherMobile")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [MobileValidation(Pattern = "^\\d{0,15}$", ErrorMessageResourceName = "Common_Validate_NotIsNumber")]
        public string MotherMobile { get; set; }

        public string MotherEmail { get; set; }
        public string FatherEmail { get; set; }
        public string StorageNumber { get; set; }

        /// <summary>
        /// Nhom mau
        /// </summary>
        [Display(Name = "PupilProfile_Label_BloodType")]
        [CommonListValidation(AllowNull = true, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Invalid")]
        public int? BloodType { get; set; }

        /// <summary>
        /// He hoc ngoai ngu
        /// </summary>
        [Display(Name = "PupilProfile_Label_ForeignLanguageTraining")]
        [CommonListValidation(AllowNull = true, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Invalid")]
        public int ForeignLanguageTraining { get; set; }

        /// <summary>
        /// Trang thai hoc sinh
        /// </summary>
        [Display(Name = "PupilProfile_Label_ProfileStatus")]
        [CommonListValidation(AllowNull = false, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Invalid")]
        public int ProfileStatus { get; set; }

        /// <summary>
        /// Hop le
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// CHi tiet loi
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Ten lop
        /// </summary>
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string ClassName { get; set; }

        /// <summary>
        /// Dan toc
        /// </summary>
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string EthnicName { get; set; }

        /// <summary>
        /// TOn giao
        /// </summary>
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string ReligionName { get; set; }

        /// <summary>
        /// Doi tuong chinh sach
        /// </summary>
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string PolicyTargetName { get; set; }

        public string PolicyRegimeName { get; set; }

        public string DisabledTypeName { get; set; }
        
        public string MinorityFatherName { get; set; }

        public string MinorityMotherName { get; set; }
        /// <summary>
        /// Tinh thanh
        /// </summary>
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string ProvinceName { get; set; }

        /// <summary>
        /// Quan/huyen
        /// </summary>
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string DistrictName { get; set; }

        /// <summary>
        /// Xa/Phuong
        /// </summary>
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string CommuneName { get; set; }

        /// <summary>
        /// Thon/xom
        /// </summary>
        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string VillageName { get; set; }

        public string GenreName { get; set; }

        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string EnrolmentTypeName { get; set; }

        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string BloodTypeName { get; set; }

        [DangerCharacterValidation(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string ForeignLanguageTrainingName { get; set; }

        public string ProfileStatusName { get; set; }
        public bool? checkInsertVilliage { get; set; }
    }

    public class DangerCharacterValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null || !Utils.Utils.IsDangerousString(value.ToString()))
            {
                return ValidationResult.Success;
            }
            return new ValidationResult(this.ErrorMessageResourceName);
        }
    }

    public class DateTimeValidation : ValidationAttribute
    {
        public bool AllowNull { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //if ((value == null && AllowNull)
            //        || (value != null && (DateTime)value > DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", CultureInfo.InvariantCulture) && (DateTime)value < DateTime.Now))
            //{
            //    return ValidationResult.Success;
            //}

            var valueDatetime = Convert.ToDateTime(value);
            if ((value == null && AllowNull)
                    || (value != null && valueDatetime > new DateTime(day: 1, month: 1, year: 1900) && valueDatetime < DateTime.Now))
            {
                return ValidationResult.Success;
            }
            return new ValidationResult(this.ErrorMessageResourceName);
        }
    }

    public class CommonListValidation : ValidationAttribute
    {
        public bool AllowNull { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            GlobalInfo glo = new GlobalInfo();

            if (AllowNull && value == null)
                return ValidationResult.Success;

            if (!AllowNull && value == null)
                return new ValidationResult(this.ErrorMessageResourceName);

            //Con lai truong hop value!=null

            List<ComboObject> list = new List<ComboObject>();

            switch (validationContext.MemberName)
            {
                case "BloodType":
                    list = CommonList.BloodType();
                    break;
                case "Genre":
                    list = CommonList.GenreAndSelect();
                    break;
                case "EnrolmentType":
                    list = CommonList.EnrolmentTypeAvailable();
                    break;
                case "ForeignLanguageTraining":
                    list = glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY ? CommonList.ForeignLanguageTraining() : CommonList.ForeignLanguageTrainingUnidentified();
                    break;
                case "ProfileStatus":
                    list = CommonList.PupilStatus().Where(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString()).ToList();
                    break;
            }

            if (list.Any(u => u.key.Equals(value.ToString())))
                return ValidationResult.Success;

            return new ValidationResult(this.ErrorMessageResourceName);
        }
    }

    public class MobileValidation : ValidationAttribute
    {
        public string Pattern { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;

            if (Regex.IsMatch(value.ToString(), Pattern))
                return ValidationResult.Success;

            return new ValidationResult(this.ErrorMessageResourceName);
        }
    }
    
}