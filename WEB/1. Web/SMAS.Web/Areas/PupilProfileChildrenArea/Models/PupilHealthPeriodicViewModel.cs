﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.PupilProfileChildrenArea.Models
{
    public class PupilHealthPeriodicViewModel
    {
        public string FullName { get; set; }

        public string BirthDate { get; set; }

        public string GenreName { get; set; }

        public string ClassName { get; set; }

        public string HistoryYourself { get; set; }

        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public PhysicalTest PhysicalTest { get; set; }

        public EyeTest EyeTest { get; set; }

        public ENTTest ENTTest { get; set; }

        public SpineTest SpineTest { get; set; }

        public DentalTest DentalTest { get; set; }

        public OverallTest OverallTest { get; set; }
    }
}