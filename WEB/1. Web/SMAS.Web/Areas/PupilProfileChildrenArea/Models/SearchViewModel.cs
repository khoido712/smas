/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PupilProfileChildrenArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel_MN")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileChildrenConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class_MN_Edit")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileChildrenConstants.LISTCLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxGetClassID(this)")]
        public string Class { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileChildrenConstants.LISTGENRE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Genre { get; set; }

        [ResourceDisplayName("PupilProfileChildren_Label_PupilCode")]
        [UIHint("Textbox")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [UIHint("Textbox")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Fullname { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Status")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileChildrenConstants.LISTSTATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Status { get; set; }
        [ResourceDisplayName("Ethnic_Label_EthnicID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileChildrenConstants.LISTETHNIC)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Ethnic { get; set; }
    }
}
