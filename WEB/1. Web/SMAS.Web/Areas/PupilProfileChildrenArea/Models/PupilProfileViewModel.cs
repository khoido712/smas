﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Models.Models;
namespace SMAS.Web.Areas.PupilProfileChildrenArea.Models
{
    public class PupilProfileChildrenViewModel
    {
		public System.Int32 PupilProfileID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class_MN")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]					
		public System.Int32 CurrentClassID { get; set; }								
		public System.Int32 CurrentSchoolID { get; set; }								
		public System.Int32 CurrentAcademicYearID { get; set; }
         [ResourceDisplayName("PupilProfile_Label_Area")]
        public string AreaName { get; set; }
		public System.Nullable<System.Int32> AreaID { get; set; }
         [ResourceDisplayName("PupilProfile_Label_Province")]
        public string ProvinceName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Province")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]		
		public System.Int32 ProvinceID { get; set; }
         [ResourceDisplayName("PupilProfile_Label_District")]
        public string DistrictName { get; set; }
		public System.Nullable<System.Int32> DistrictID { get; set; }
        [ResourceDisplayName("PupilProfile_Label_Commune")]
        public string CommuneName { get; set; }
		public System.Nullable<System.Int32> CommuneID { get; set; }
        [ResourceDisplayName("PupilProfile_Label_Ethnic")]
        public string EthnicName { get; set; }					
		public System.Nullable<System.Int32> EthnicID { get; set; }
        [ResourceDisplayName("Tên gọi khác (Dân tộc)")]
        public string OtherEthnicName { get; set; }
        public System.Nullable<System.Int32> OtherEthnicID { get; set; }
        [ResourceDisplayName("PupilProfile_Label_Religion")]
        public string ReligionName { get; set; }
		public System.Nullable<System.Int32> ReligionID { get; set; }
          [ResourceDisplayName("PupilProfile_Label_PolicyTarget")]
        public string PolicyTargetName { get; set; }
		public System.Nullable<System.Int32> PolicyTargetID { get; set; }								
		public System.Nullable<System.Int32> FamilyTypeID { get; set; }								
		public System.Nullable<System.Int32> PriorityTypeID { get; set; }								
		public System.Nullable<System.Int32> PreviousGraduationLevelID { get; set; }
        //[ResDisplayName("PupilProfile_Label_PolicyRegime")]
        public System.Nullable<System.Int32> PolicyRegimeID { get; set; }
        [ResourceDisplayName("PupilProfile_Label_PupilCode_MN")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]					
		public System.String PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_StorageName")]
        public string StorageNumber { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Name")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
		public System.String Name { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]				
		public System.String FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]      		
		public System.Int32 Genre { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		public System.DateTime BirthDate { get; set; }

        public string BirthDate_String { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthPlace")]				
		public System.String BirthPlace { get; set; }
        [ResourceDisplayName("PupilProfile_Label_HomeTown")]				
		public System.String HomeTown { get; set; }
           [ResourceDisplayName("PupilProfile_Label_Telephone")]					
		public System.String Telephone { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Mobile")]			
		public System.String Mobile { get; set; }								
		public System.String Email { get; set; }
                  [ResourceDisplayName("PupilProfile_Label_TempResidentalAddress")]						
		public System.String TempResidentalAddress { get; set; }
        [ResourceDisplayName("PupilProfile_Label_PermanentResidentalAddress")]							
		public System.String PermanentResidentalAddress { get; set; }
        public bool CheckIsResidentIn { get; set; }					
		public System.Nullable<System.Boolean> IsResidentIn { get; set; }
         [ResourceDisplayName("PupilProfile_Label_IsDisabled")]		
        public bool CheckIsDisabled { get; set; }
        public string IsDisabledName { get; set; }	
		public System.Nullable<System.Boolean> IsDisabled { get; set; }
           [ResourceDisplayName("PupilProfile_Label_DisabledType")]   
        public string DisabledTypeName { get; set; }								
		public System.Nullable<System.Int32> DisabledTypeID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_DisabledSeverity")]        
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]									
		public System.String DisabledSeverity { get; set; }
         [ResourceDisplayName("PupilProfile_Label_BloodType")]   
        public string BloodTypeName { get; set; }		
		public System.Nullable<System.Int32> BloodType { get; set; }								
		public System.Nullable<System.Decimal> EnrolmentMark { get; set; }

        [ResourceDisplayName("PupilProfile_Label_EnrolmentDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]							
		public System.DateTime EnrolmentDate { get; set; }
         [ResourceDisplayName("PupilProfile_Label_EnrolmentDate")]
        public string EnrolmentDate_string { get; set; }	
		public System.Int32 ForeignLanguageTraining { get; set; }								
		public System.Nullable<System.Boolean> IsYoungPioneerMember { get; set; }								
		public System.Nullable<System.DateTime> YoungPioneerJoinedDate { get; set; }								
		public System.String YoungPioneerJoinedPlace { get; set; }								
		public System.Nullable<System.Boolean> IsYouthLeageMember { get; set; }								
		public System.Nullable<System.DateTime> YouthLeagueJoinedDate { get; set; }								
		public System.String YouthLeagueJoinedPlace { get; set; }								
		public System.Nullable<System.Boolean> IsCommunistPartyMember { get; set; }								
		public System.Nullable<System.DateTime> CommunistPartyJoinedDate { get; set; }								
		public System.String CommunistPartyJoinedPlace { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FatherFullName")]					
		public System.String FatherFullName { get; set; }
         [ResourceDisplayName("PupilProfile_Label_FatherBirthDate")]						
		public System.Nullable<System.DateTime> FatherBirthDate { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FatherJob")]							
		public System.String FatherJob { get; set; }	
        [ResourceDisplayName("PupilProfile_Label_FatherMobile")]					
		public System.String FatherMobile { get; set; }
        [ResourceDisplayName("PupilProfile_Label_MotherFullName")]						
		public System.String MotherFullName { get; set; }
        [ResourceDisplayName("PupilProfile_Label_MotherBirthDate")]						
		public System.Nullable<System.DateTime> MotherBirthDate { get; set; }
        [ResourceDisplayName("PupilProfile_Label_MotherJob")]							
		public System.String MotherJob { get; set; }
        [ResourceDisplayName("PupilProfile_Label_MotherMobile")]					
		public System.String MotherMobile { get; set; }
        [ResourceDisplayName("PupilProfile_Label_SponsorFullName")]						
		public System.String SponsorFullName { get; set; }
        [ResourceDisplayName("PupilProfile_Label_SponsorBirthDate")]							
		public System.Nullable<System.DateTime> SponsorBirthDate { get; set; }
        [ResourceDisplayName("PupilProfile_Label_SponsorJob")]							
		public System.String SponsorJob { get; set; }
        [ResourceDisplayName("PupilProfile_Label_SponsorMobile")]							
		public System.String SponsorMobile { get; set; }								
		public System.Int32 ProfileStatus { get; set; }																		
		public System.Boolean IsActive { get; set; }								
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }								
		public System.Nullable<System.Decimal> HeightAtBirth { get; set; }
        public System.Nullable<System.Decimal> WeightAtBirth { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FatherEmail")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String FatherEmail { get; set; }

        [ResourceDisplayName("PupilProfile_Label_MotherEmail")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String MotherEmail { get; set; }

        [ResourceDisplayName("PupilProfile_Label_SponsorEmail")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String SponsorEmail { get; set; }
        [ResourceDisplayName("PupilProfile_Label_ChildOrder")]						
		public System.Nullable<System.Int32> ChildOrder { get; set; }								
		public System.Nullable<System.Int32> NumberOfChild { get; set; }								
		public System.Nullable<System.Int32> EatingHabit { get; set; }								
		public System.Nullable<System.Int32> ReactionTrainingHabit { get; set; }								
		public System.Nullable<System.Int32> AttitudeInStrangeScene { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FavoriteTypeOfDish")]
        public System.String FavouriteTypeOfDish { get; set; }
         [ResourceDisplayName("PupilProfile_Label_HateTypeOfDish")]
        public System.String HateTypeOfDish { get; set; }
         [ResourceDisplayName("PupilProfile_Label_OtherHabit")]			
		public System.String OtherHabit { get; set; }
         [ResourceDisplayName("PupilProfile_Label_FavoriteToy")]						
		public System.String FavoriteToy { get; set; }								
		public System.Nullable<System.Int32> EvaluationConfigID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_RegisterSMS")]
        public bool IsFatherSMS { get; set; }


        [ResourceDisplayName("PupilProfile_Label_RegisterSMS")]
        public bool IsMotherSMS { get; set; }

        [ResourceDisplayName("PupilProfile_Label_RegisterSMS")]
        public bool IsSponsorSMS { get; set; }

        public bool MinorityFather { get; set; }

        public bool MinorityMother { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }
        [ResourceDisplayName("PupilProfile_Label_Class_MN_Edit")]
        public string ClassName { get; set; }
        [ResourceDisplayName("PupilProfile_Label_Status")]
        public string StatusName { get; set; }
        [ResourceDisplayName("PupilProfile_Label_EducationLevel_MN")]
        public string EducationLevelName { get; set; }
        [ResourceDisplayName("PupilProfile_Label_EducationLevel_MN")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Int32 EducationLevel { get; set; }
        [ResourceDisplayName("PupilProfile_Label_EnrolmentType")]
        public string EnrolmentTypeName { get; set; }
        public int? EnrolmentType { get; set; }
        public int? OrderInClass { get; set; }
        [ResourceDisplayName("PupilProfile_Label_ClassType")]
        public int? ClassType { get; set; }
        public bool AutoGenCode { get; set; }

        [ResourceDisplayName("Father_Birth_Date")]
        public int? iFatherBirthDate { get; set; }
        [ResourceDisplayName("Mother_Birth_Date")]
        public int? iMotherBirthDate { get; set; }
        [ResourceDisplayName("Sponsor_Birth_Date")]
        public int? iSponsorBirthDate { get; set; }

        public IEnumerable<HabitDetail> ListHabitDetail { get; set; }
        public int? rptEatingHabit { get; set; }
        public int?[] rptHabitDetail { get; set; }
        public int?[] rptTypeOfDishHate { get; set; }
        public int?[] rptTypeOfDishLike { get; set; }
        public int?[] rptTypeOfDishMain { get; set; }
        public int? rptAttitudeInStrangeScene { get; set; }
        public int? rptReactionTraining { get; set; }

        public string ResidentInName { get; set; }

         [ResourceDisplayName("PupilProfile_Label_FamilyType")]
        public string FamilyTypeName { get; set; }
        public string IsChildrenInFamily { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PriorityType")]
        public string PriorityTypeName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PolicyRegime")]
        public string PolicyRegimeName { get; set; }
        public string HiddenLink { get; set; }
        public int? ClassOrderNumber { get; set; }
        public int? VillageID { get; set; }
        
        [ResourceDisplayName("PupilProfile_Label_Village")]
        public string VillageName { get; set; }
        public bool EnableCheck { get; set; }

        public string EthnicCode { get; set; }

        public string OtherEthnicCode { get; set; }

        public int? AppliedLevel { get; set; }

        public bool isEnd { get; set; }

        public bool isFirst { get; set; }
    }
}


