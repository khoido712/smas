/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilProfileChildrenArea
{
    public class PupilProfileChildrenConstants
    {
        public const string BOOL_IS_FIRST_TIME = "BOOL_IS_FIRST_TIME";
        public const string LIST_PUPILPROFILE = "listPupilProfile";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LIST_EDUCATION_LEVEL = "LIST_EDUCATION_LEVEL";
        public const string SEMESTER_CHECKED = "SEMESTER_CHECKED";
        public const string LISTCLASS = "LISTCLASS";
        public const string LISTGENRE = "LISTGENRE";
        public const string LISTSTATUS = "LISTSTATUS";
        public const string ENABLE_IMPORT = "ENABLE_IMPORT";
        public const string LIST_IMPORTED_PUPIL = "LIST_IMPORTED_PUPIL";
        public const string MESSAGE_ERROR_IMPORT = "MessageErrorImport";
        public const string MESSAGE_STATUS_SUCCESS = "success";
        public const string LISTPOLICYREGIME = "LISTPOLICYREGIME";
        public const string LISTETHNIC = "LISTETHNIC";//
        public const string LISTOTHERETHNIC = "LISTOTHERETHNIC";//
        public const string LISTRELIGION = "LISTRELIGION";//
        public const string LISTPOLICYTARGET = "LISTPOLICYTARGET";//
        public const string LISTAREA = "LISTAREA";//all
        public const string LISTPRIORITYTYPE = "LISTPRIORITYTYPE";//all
        public const string LISTPROVINCE = "LISTPROVINCE";//all
        public const string LISTDISTRICT = "LISTDISTRICT";//dua vao provide
        public const string LISTCOMMUNE = "LISTCOMMUNE";//dua vao district
        public const string LISTDISABLEDTYPE = "LISTDISABLEDTYPE";//all

        public const string LISTBLOODTYPE = "LISTBLOODTYPE";//
        public const string LISTFAMILYTYPE = "LISTFAMILYTYPE";//all
        public const string LISTENROLMENTTYPE = "LISTENROLMENTTYPE";
        public const string LISTCLASSTYPE = "LISTCLASSTYPE";
        public const string LISTPREVIOUSGRADUATIONLEVEL = "LISTPREVIOUSGRADUATIONLEVEL";//
        public const string LISTFOREIGNLANGUAGETRAINING = "LISTFOREIGNLANGUAGETRAINING";//
        public const string LISTENROLMENTDATE = "LISTENROLMENTDATE";

        public const string LIST_CHART = "LIST_CHART";//
        public const string DETAIL_HEALTH_PERIODIC_PUPIL = "DETAIL_HEALTH_PERIODIC_PUPIL";
        public const string LIST_ACADEMICYEAR = "LIST_ACADEMICYEAR";//
        public const string LIST_HEALTH_PERIODIC = "LIST_HEALTH_PERIODIC";
        public const string LIST_CHART_CLASS = "LIST_CHART_CLASS";//
        public const string LIST_PUPIL = "LIST_PUPIL";

        //an hien control
        public const string APPLIED_LEVEL_PRIMARY = "APPLIED_LEVEL_PRIMARY";

        public const string APPLIED_LEVEL_SECONDARY = "APPLIED_LEVEL_SECONDARY";
        public const string APPLIED_LEVEL_TERTIARY = "APPLIED_LEVEL_TERTIARY";
        public const string APPLIED_LEVEL_CRECHE = "APPLIED_LEVEL_CRECHE";
        public const string APPLIED_LEVEL_KINDER_GARTEN = "APPLIED_LEVEL_KINDER_GARTEN";

        //an hien autogencode
        public const string AUTO_GEN_CODE_CHECK = "AUTO_GEN_CODE_CHECK";

        public const string AUTO_GEN_CODE_CHECK_AND_DISABLE = "AUTO_GEN_CODE_CHECK_AND_DISABLE";
        public const string AUTO_GEN_CODE_CHECK_AND_NO_DISABLE = "AUTO_GEN_CODE_CHECK_AND_NO_DISABLE";

        public const string IS_SCHOOL_MODIFIED = "IS_SCHOOL_MODIFIED";
        public const string PUPILPROFILE_MODEL_FOR_DETAIL = "PUPILPROFILE_MODEL_FOR_DETAIL";

        //
        public const string SEARCHFORM = "SEARCHFORM";

        public const string VISIBLE_ORDER = "VISIBLE_ORDER";

        public const string PERVIOUSGRADUATION_LEVEL1 = "PERVIOUSGRADUATION_LEVEL1";

        public const string ENABLE_PAGING = "ENABLE_PAGING";
        public const string PUPIL_NOT_STUDY = "PUPIL_NOT_STUDY";

        public const string LIST_HABITGROUP = "LIST_HABITGROUP";
        public const string LIST_HABITDETAIL = "LIST_HABITDETAIL";
        public const string LIST_HABITDETAILOFPUPIL = "LIST_HABITDETAILOFPUPIL";
        public const string LIST_TYPEOFDISH = "LIST_TYPEOFDISH";
        public const string LIST_TYPEOFDISHOFPUPIL = "LIST_TYPEOFDISHOFPUPIL";
        public const string Pageable = "Pageable";

        // tang truong thang
        public const string CLASS_ID = "CLASS_ID";
        public const string PUPIL_ID = "PUPIL_ID";
        public const string CLASS_NAME = "CLASS_NAME";
        public const string GENRE = "GENRE";
        public const string FULL_NAME = "FULL_NAME";
        public const string BIRTH_DAY = "BIRTH_DAY";
        public const string HISTORY_YOUR_SELF = "HISTORY_YOUR_SELF";
        public const string CLASSID_SELECT = "CLASSID_SELECT";
        public const string IS_NAVIGATE_FROM_OTHER_PAGE = "IS_NAVIGATE_FROM_OTHER_PAGE";
        public const string DICTIONARY_TABS_PERMISSION = "DICTIONARY_TABS_PERMISSION";
        public const string LIST_SCHOOL_MOVEMENT = "LIST_SCHOOL_MOVEMENT";
        public const string DIC_CLASS_BY_EDUCATION_LEVEL = "DIC_CLASS_BY_EDUCATION_LEVEL";
        public const string IS_SHOW_AVATAR = "IS_SHOW_AVATAR";

        public const string ENABLE_CREATE_DELETE = "ENABLE_CREATE_DELETE";
        public const string ENABLE_IMPORT_BUTTON_TEACHER = "ENABLE_IMPORT_BUTTON";
        public const int SCHOOL_MOVEMENT_IN_SYSTEM = 1;
        public const int SCHOOL_MOVEMENT_OUT_SYSTEM = 2;
        public const string LISTSEMESTER = "LISTSEMESTER";
        public const string LISTLEAVINGREASON = "LISTLEAVINGREASON";
        public const string CLASS_PROFILE_NAME = "CLASS_PROFILE_NAME";
        public const string FirstStartDate = "FirstStartDate";
                
    }
}