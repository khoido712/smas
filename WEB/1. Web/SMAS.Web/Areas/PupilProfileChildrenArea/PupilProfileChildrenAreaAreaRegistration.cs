﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilProfileChildrenArea
{
    public class PupilProfileChildrenAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilProfileChildrenArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilProfileChildrenArea_default",
                "PupilProfileChildrenArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
