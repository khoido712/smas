﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportApprenticeshipMarkArea
{
    public class ReportApprenticeshipMarkConstants
    {
        public const string LIST_ApprenticeshipClass = "list_ApprenticeshipClass";
        public const string LIST_SEMESTER = "listSemester";
    }
}