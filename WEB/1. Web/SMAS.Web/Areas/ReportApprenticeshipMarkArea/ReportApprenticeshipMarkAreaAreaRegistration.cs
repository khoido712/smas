﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportApprenticeshipMarkArea
{
    public class ReportApprenticeshipMarkAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportApprenticeshipMarkArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportApprenticeshipMarkArea_default",
                "ReportApprenticeshipMarkArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
