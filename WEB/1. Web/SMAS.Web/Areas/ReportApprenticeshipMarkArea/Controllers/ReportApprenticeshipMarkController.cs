﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.ReportApprenticeshipMarkArea.Models;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
namespace SMAS.Web.Areas.ReportApprenticeshipMarkArea.Controllers
{
    public class ReportApprenticeshipMarkController : Controller
    {
         private readonly IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness;
        private readonly IApprenticeshipClassBusiness ApprenticeshipClassBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;

        public ReportApprenticeshipMarkController(IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness, IApprenticeshipClassBusiness ApprenticeshipClassBusiness, ISummedUpRecordBusiness SummedUpRecordBusiness)
        {
            this.ApprenticeshipTrainingBusiness = ApprenticeshipTrainingBusiness;
            this.ApprenticeshipClassBusiness = ApprenticeshipClassBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
        }

        //
        // GET: /ReportApprenticeshipMarkArea/ReportApprenticeshipMark/

        public ActionResult Index(SearchViewModel frm)
        {
             int? Semester = new GlobalInfo().Semester;
             GetApprenticeshipClassList(Semester);
             GetSemester();
             return View();
        }

        /// <summary>
        /// Lấy về danh sách các lớp nghề
        /// </summary>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetApprenticeshipClassList(int? Semester)
        {
            GlobalInfo glo = new GlobalInfo();
            int schoolID = glo.SchoolID.Value;
            int academicYearID = glo.AcademicYearID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["Semester"] = Semester;
            if (!glo.IsAdminSchoolRole)
            {
                dic["HeadTeacherID"] = glo.EmployeeID;
            }
            ViewData[ReportApprenticeshipMarkConstants.LIST_ApprenticeshipClass] = new SelectList(ApprenticeshipClassBusiness.SearchBySchool(schoolID, dic).ToList(), "ApprenticeshipClassID", "ClassName");

            return Json(new SelectList(ApprenticeshipClassBusiness.SearchBySchool(schoolID, dic).ToList(), "ApprenticeshipClassID", "ClassName"), JsonRequestBehavior.AllowGet);
        }

        //Lấy ra danh sách học kỳ gồm HKI, HKII.Giá trị mặc định tuỳ thuộc vào thời gian hiện tại.
        private void GetSemester()
        {
            ViewData[ReportApprenticeshipMarkConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value", new GlobalInfo().Semester); 
        }
        //Xuất báo cáo
        [HttpPost]
        public FileResult ExportExcel(FormCollection col, ApprenticeshipTrainingBO reportViewModel)
        {
            GlobalInfo global = new GlobalInfo();
            int semester = SMAS.Business.Common.Utils.GetInt(Request["Semester"]);
            int ApprenticeShipClassID = SMAS.Business.Common.Utils.GetInt(Request["ApprenticeshipClassID"]);

            Stream excel = ApprenticeshipTrainingBusiness.CreateReportApprenticeshipMark(global.SchoolID.Value, global.AcademicYearID.Value, ApprenticeShipClassID, semester);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
           
            string ReportName = "HS_[SchoolLevel]_SoGoiTenVaGhiDiemNghe_[ClassName].xls";
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
            string className = ApprenticeshipClassBusiness.Find(ApprenticeShipClassID).ClassName;
            //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);
            ReportName = ReportName.Replace("[ClassName]", className);
            //ReportName = ReportName.Replace("[Semester]", semester);
            ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

            result.FileDownloadName = ReportName;

            return result;
        }
    }
}
