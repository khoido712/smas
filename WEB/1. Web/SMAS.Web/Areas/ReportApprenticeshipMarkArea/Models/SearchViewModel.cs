﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportApprenticeshipMarkArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ApprenticeshipTest_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("ViewDataKey", ReportApprenticeshipMarkConstants.LIST_ApprenticeshipClass)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public short ApprenticeshipClassID { get; set; }

        [ResourceDisplayName("ApprenticeshipClass_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        [AdditionalMetadata("ViewDataKey", ReportApprenticeshipMarkConstants.LIST_SEMESTER)]
        public int? Semester { get; set; }
    }
}