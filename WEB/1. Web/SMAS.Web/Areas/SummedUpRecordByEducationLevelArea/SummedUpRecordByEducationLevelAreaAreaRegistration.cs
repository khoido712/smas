﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SummedUpRecordByEducationLevelArea
{
    public class SummedUpRecordByEducationLevelAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SummedUpRecordByEducationLevelArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SummedUpRecordByEducationLevelArea_default",
                "SummedUpRecordByEducationLevelArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
