﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SummedUpRecordByEducationLevelArea.Models;
using SMAS.Web.Areas.SummedUpRecordByEducationLevelArea;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.Transactions;
using SMAS.Web.Controllers;
using log4net;
using System.Threading;
using SMAS.Web.Filter;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.SummedUpRecordByEducationLevelArea.Controllers
{
    public class SummedUpRecordByEducationLevelController : ThreadController
    {
        private readonly ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private static ILog iLog;

        public SummedUpRecordByEducationLevelController(ISummedUpRecordClassBusiness summedUpRecordClassBusiness, IAcademicYearBusiness academicYearBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness)
        {
            this.SummedUpRecordClassBusiness = summedUpRecordClassBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo global = new GlobalInfo();
            if (global.HasSubjectTeacherPermission(0, 0) == false)
            {
                throw new BusinessException("Common_Validate_User");
            }
            List<EducationLevel> lsEducationLevel = global.EducationLevels;
            //-	cboEducationLevel: UserInfo.EducationLevels
            if (lsEducationLevel != null)
            {
                ViewData[SummedUpRecordByEducationLevelConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[SummedUpRecordByEducationLevelConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
            }

            ViewData[SummedUpRecordByEducationLevelConstants.LISTSEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value", global.Semester);

            ViewData[SummedUpRecordByEducationLevelConstants.LISTSUMMEDUPRECORDBYEDUCATIONLEVEL] = null;
            ViewData[SummedUpRecordByEducationLevelConstants.DisableButton] = true;
            return View();
        }

        public PartialViewResult Search(int? EducationLevel, int? Semester)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            if (Semester.HasValue)
            {
                SearchInfo["Semester"] = Semester.Value;
            }
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            if (EducationLevel.HasValue)
            {
                SearchInfo["EducationLevelID"] = EducationLevel.Value;
            }
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["Type"] = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1;
            SearchInfo["IsVNEN"] = true;
            //add search info		
            IEnumerable<SummedUpRecordViewModel> lst = this._Search(SearchInfo, SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1, Semester.HasValue ? Semester.Value : 0);
            ViewData[SummedUpRecordByEducationLevelConstants.LISTSUMMEDUPRECORDBYEDUCATIONLEVEL] = lst.OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName);
            //Get view data here
            return PartialView("_ListSummedUpRecord");
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchRanking(int? EducationLevel, int? Semester)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            if (Semester.HasValue)
            {
                SearchInfo["Semester"] = Semester;
            }
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            if (EducationLevel.HasValue)
            {
                SearchInfo["EducationLevelID"] = EducationLevel;
            }
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["Type"] = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_2;
            SearchInfo["IsVNEN"] = true;
            //add search info		
            List<SummedUpRecordViewModel> lst = this._Search(SearchInfo, SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_2, Semester.HasValue ? Semester.Value : 0);
            ViewData[SummedUpRecordByEducationLevelConstants.LISTRANKINGBYEDUCATIONLEVEL] = lst.OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName);
            //Get view data here
            return PartialView("_ListRanking");
        }

        private List<SummedUpRecordViewModel> _Search(IDictionary<string, object> SearchInfo, int Type, int Semester)
        {
            GlobalInfo global = new GlobalInfo();
            AcademicYear ac = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime? endSemster = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? ac.FirstSemesterEndDate : ac.SecondSemesterEndDate;

            List<SummedUpRecordViewModel> lstSum = this.SummedUpRecordClassBusiness
                                                        .GetListClassByEducationLevel(SearchInfo)
                                                        .Select(o => new SummedUpRecordViewModel
                                                        {
                                                            ClassID = o.ClassID,
                                                            ClassName = o.ClassName,
                                                            ClassOrderNumber = o.ClassOrderNumber,
                                                            Status = o.Status,
                                                            ModifiedDate = o.ModifiedDate,
                                                            NumberOfChildren = o.NumberOfPupil,
                                                        }).ToList();
            List<int> lstClassID = lstSum.Select(u => u.ClassID).Distinct().ToList();
            Dictionary<int, bool> dicHeadteacherPermission = UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, lstClassID);

            foreach (var item in lstSum)
            {
                item.Status_String = SummedUpRecordClass_Status(item.Status, Type);

                if (item.NumberOfChildren.HasValue)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["ClassID"] = item.ClassID;
                    int TongSoHS = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, dic).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED).Count();
                    item.NumberOfChildren_String = item.NumberOfChildren.ToString() + "/" + TongSoHS.ToString();
                }

                if (item.ModifiedDate.HasValue)
                    item.ModDate = String.Format("{0:dd/MM/yyyy HH:mm:ss}", item.ModifiedDate.Value);

                item.DisableClass = !global.IsCurrentYear || !dicHeadteacherPermission[item.ClassID] || item.Status == SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING || (!global.IsAdminSchoolRole && endSemster < DateTime.Now);
                item.Check = dicHeadteacherPermission[item.ClassID] && (!item.Status.HasValue || item.Status == SystemParamsInFile.STATUS_SUR_CLASS_NOTCOMPLETE) && !item.DisableClass;
            }
            ViewData[SummedUpRecordByEducationLevelConstants.DisableButton] = !global.IsCurrentYear || lstSum.Count == 0
                                                                                || !lstSum.Any(u => dicHeadteacherPermission[u.ClassID] && !u.DisableClass)
                                                                                || (!global.IsAdminSchoolRole && endSemster < DateTime.Now);
            return lstSum;
        }


        [ValidateAntiForgeryToken]
        public JsonResult CheckStatusThread(string ThreadName)
        {
            if (this.StatusThread(ThreadName) == Web.Constants.GlobalConstants.Status_Thread_NoHas)
            {
                return Json(1);
            }
            else
            {
                //chua tong ket xong tra ve ko
                return Json(0);
            }
        }



        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult SummedUpRecord1(int[] HiddenClassID, int?[] Check, int? EducationLevelID, int? SemesterID, int? Period)
        {
            GlobalInfo global = new GlobalInfo();

            string ThreadName = Web.Constants.GlobalConstants.Thread_SummedUpSemester + global.SchoolID.ToString() + "_" + EducationLevelID.Value.ToString();

            if (this.StatusThread(ThreadName) == Web.Constants.GlobalConstants.Status_Thread_Has)
                return Json(new JsonMessage(Res.Get("SummedRedcord_Label_RankThread")));

            if (Check == null)
                throw new BusinessException(Res.Get("SummedUpRecordByEducationLevel_Label_SummedUpNoClass"));

            List<ClassProfile> ListCheck = new List<ClassProfile>();
            List<ClassProfile> ListNoCheck = new List<ClassProfile>();
            for (int i = 0; i < HiddenClassID.Count(); i++)
            {
                if (Check.Where(o => (o.Value == HiddenClassID[i])).Count() > 0)
                {
                    ClassProfile ClassProfile = new ClassProfile();
                    ClassProfile.SchoolID = global.SchoolID.Value;
                    ClassProfile.ClassProfileID = HiddenClassID[i];
                    ClassProfile.EducationLevelID = EducationLevelID.Value;
                    ClassProfile.AcademicYearID = global.AcademicYearID.Value;
                    ListCheck.Add(ClassProfile);
                }
                else
                {
                    ClassProfile ClassProfile = new ClassProfile();
                    ClassProfile.SchoolID = global.SchoolID.Value;
                    ClassProfile.ClassProfileID = HiddenClassID[i];
                    ClassProfile.EducationLevelID = EducationLevelID.Value;
                    ClassProfile.AcademicYearID = global.AcademicYearID.Value;
                    ListNoCheck.Add(ClassProfile);
                }
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID;
            dic["AcademicYearID"] = global.AcademicYearID;
            dic["Semester"] = SemesterID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["Type"] = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1;
            dic["PeriodID"] = Period;
            dic["ListNoCheck"] = ListNoCheck;
            dic["ListCheck"] = ListCheck;
            dic["UserAccountID"] = global.UserAccountID;
            dic["ThreadName"] = ThreadName;

            this.StartThreadForLongAction(ThreadName, new LongAction(SummedUp), dic);
            return Json(new JsonMessage(Res.Get("SummedRedcord_Label_RankThread"), ThreadName));
        }


        public void SummedUp(IDictionary<string, object> dic)
        {

            int UserAccountID = (int)dic["UserAccountID"];
            List<ClassProfile> ListNoCheck = dic["ListNoCheck"] as List<ClassProfile>;
            List<ClassProfile> ListCheck = dic["ListCheck"] as List<ClassProfile>;
            string ThreadName = (string)dic["ThreadName"];
            ISummedUpRecordClassBusiness SummedUpRecordClassBusinessNew = new SMAS.Business.Business.SummedUpRecordClassBusiness(iLog, new SMASEntities());
            try
            {
                SummedUpRecordClassBusinessNew.InsertOrUpdateSummedUpRecordClass(UserAccountID, ListNoCheck, ListCheck, dic);
                //SummedUpRecordClassBusinessNew.Save();
            }
            catch (Exception ex)
            {
                
                string paramList = "UserAccountID=" + UserAccountID;
                LogExtensions.ErrorExt(logger, DateTime.Now, "SummedUp", paramList, ex);
                SummedUpRecordClassBusinessNew.UpdateStatusSummedUp(UserAccountID, ListNoCheck, ListCheck, dic);
                SummedUpRecordClassBusinessNew.Save();
                this.DeleteThread(ThreadName);
            }
        }


        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult Ranking(int[] HiddenClassID, int[] Check, int? EducationLevelID, int? SemesterID)
        {
            GlobalInfo global = new GlobalInfo();
            if (Check == null)
                throw new BusinessException(Res.Get("SummedUpRecordByEducationLevel_Label_RankingNoClass"));

            List<ClassProfile> ListCheck = new List<ClassProfile>();
            List<ClassProfile> ListNoCheck = new List<ClassProfile>();

            for (int i = 0; i < HiddenClassID.Count(); i++)
            {
                if (Check.Contains(HiddenClassID[i]))
                {
                    ClassProfile ClassProfile = new ClassProfile();
                    ClassProfile.SchoolID = global.SchoolID.Value;
                    ClassProfile.ClassProfileID = HiddenClassID[i];
                    ClassProfile.EducationLevelID = (int)EducationLevelID.Value;
                    ClassProfile.AcademicYearID = global.AcademicYearID.Value;
                    ListCheck.Add(ClassProfile);
                }
                else
                {
                    ClassProfile ClassProfile = new ClassProfile();
                    ClassProfile.SchoolID = global.SchoolID.Value;
                    ClassProfile.ClassProfileID = HiddenClassID[i];
                    ClassProfile.EducationLevelID = (int)EducationLevelID.Value;
                    ClassProfile.AcademicYearID = global.AcademicYearID.Value;
                    ListNoCheck.Add(ClassProfile);
                }
            }
            string ThreadName = Web.Constants.GlobalConstants.Thread_PupilRankingSemester + global.SchoolID.ToString() + "_" + EducationLevelID.Value.ToString();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID;
            dic["AcademicYearID"] = global.AcademicYearID;
            dic["Semester"] = SemesterID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["Type"] = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_2;
            dic["AppliedLevel"] = global.AppliedLevel;
            dic["ListNoCheck"] = ListNoCheck;
            dic["ListCheck"] = ListCheck;
            dic["UserAccountID"] = global.UserAccountID;
            dic["ThreadName"] = ThreadName;

            this.StartThreadForLongAction(ThreadName, new LongAction(Ranking4Thread), dic);
            return Json(new JsonMessage(Res.Get("SummedRedcord_Label_RankThread"), ThreadName));
        }

        /// <summary>
        /// QuangLM
        /// Tien trien xep loai hoc sinh theo khoi
        /// </summary>
        /// <param name="dic"></param>
        public void Ranking4Thread(IDictionary<string, object> dic)
        {
            int UserAccountID = (int)dic["UserAccountID"];
            List<ClassProfile> ListNoCheck = dic["ListNoCheck"] as List<ClassProfile>;
            List<ClassProfile> ListCheck = dic["ListCheck"] as List<ClassProfile>;
            string ThreadName = (string)dic["ThreadName"];
            ISummedUpRecordClassBusiness SummedUpRecordClassBusinessNew = new SMAS.Business.Business.SummedUpRecordClassBusiness(iLog, new SMASEntities());
            try
            {
                SummedUpRecordClassBusinessNew.InsertOrUpdateSummedUpRecordClass(UserAccountID, ListNoCheck, ListCheck, dic);
                SummedUpRecordClassBusinessNew.Save();
            }
            catch (Exception ex)
            {
                
                string paraList = "UserAccountID=" + UserAccountID;
                LogExtensions.ErrorExt(logger, DateTime.Now, "Ranking4Thread", paraList, ex);
                SummedUpRecordClassBusinessNew.UpdateStatusSummedUp(UserAccountID, ListNoCheck, ListCheck, dic);
                SummedUpRecordClassBusinessNew.Save();
                this.DeleteThread(ThreadName);
            }
        }

        private string SummedUpRecordClass_Status(int? status, int type)
        {
            string res = "";
            if (!status.HasValue)
            {
                res = (type == SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1 ? "Common_Label_NotComplete" : "Common_Label_RankingNotComplete");
            }
            else
            {
                if (status.Value == SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE)
                {
                    res = (type == SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1 ? "Common_Label_Complete" : "Common_Label_RankingComplete");
                }
                if (status.Value == SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING)
                {
                    res = (type == SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1 ? "Common_Label_Completing" : "Common_Label_RankingCompleting");
                }
                if (status.Value == SystemParamsInFile.STATUS_SUR_CLASS_NOTCOMPLETE)
                {
                    res = (type == SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1 ? "Common_Label_NotComplete" : "Common_Label_RankingNotComplete");
                }
            }
            return Res.Get(res);
        }
    }
}
