﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SummedUpRecordByEducationLevelArea.Models
{
    public class SummedUpRecordViewModel
    {
        public System.Int32 ClassID { get; set; }
        [ResourceDisplayName("SummedUpRecordClass_Label_ClassName")]
        public System.String ClassName { get; set; }
        [ResourceDisplayName("SummedUpRecordClass_Label_Status")]
        public string Status_String { get; set; }
        public int? Status { get; set; }
        public int? ClassOrderNumber { get; set; }
        [ResourceDisplayName("SummedUpRecordClass_Label_ModifiedDate")]
        public System.DateTime? ModifiedDate { get; set; }
        [ResourceDisplayName("SummedUpRecordClass_Label_NumberOfChildren")]
        public System.String NumberOfChildren_String { get; set; }
        public int? NumberOfChildren { get; set; }
        public System.Boolean DisableClass { get; set; }
        public System.Boolean Check { get; set; }
        [ResourceDisplayName("SummedUpRecordClass_Label_ModifiedDate")]
        public string ModDate { get; set; }
    }
}