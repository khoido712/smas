﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SummedUpRecordByEducationLevelArea.Models
{
    public class SearchRankingViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]      
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SummedUpRecordByEducationLevelConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "SearchRanking()")]
        public int? EducationLevel1 { get; set; }

        [ResourceDisplayName("ApprenticeshipClass_Label_Semester")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]      
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SummedUpRecordByEducationLevelConstants.LISTSEMESTER)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "SearchRanking()")]
        public int? Semester1 { get; set; }
    }
}