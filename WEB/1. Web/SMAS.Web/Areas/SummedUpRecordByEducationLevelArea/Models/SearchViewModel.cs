﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.SummedUpRecordByEducationLevelArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]      
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SummedUpRecordByEducationLevelConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "SearchSum()")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("ApprenticeshipClass_Label_Semester")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]      
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SummedUpRecordByEducationLevelConstants.LISTSEMESTER)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "SearchSum()")]
        public int? Semester { get; set; }
    }
}