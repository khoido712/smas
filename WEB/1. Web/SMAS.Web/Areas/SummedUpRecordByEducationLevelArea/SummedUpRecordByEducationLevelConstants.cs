﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SummedUpRecordByEducationLevelArea
{
    public class SummedUpRecordByEducationLevelConstants
    {
        public const string LISTSUMMEDUPRECORDBYEDUCATIONLEVEL = "LISTSUMMEDUPRECORDBYEDUCATIONLEVEL";
        public const string LISTRANKINGBYEDUCATIONLEVEL = "LISTRANKINGBYEDUCATIONLEVEL";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTSEMESTER = "LISTSEMESTER";
        public const string SEMESTERSTRING = "SEMESTERSTRING";
        public const string DisableButton = "DisableButton";
        public const string COUNT = "count";
    }
}