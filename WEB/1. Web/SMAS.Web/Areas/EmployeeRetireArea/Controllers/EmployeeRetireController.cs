﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.EmployeeRetireArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.EmployeeRetireArea.Controllers
{
    public class EmployeeRetireController : BaseController
    {
        private readonly IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        public EmployeeRetireController(IEmployeeWorkMovementBusiness employeeworkmovementBusiness, ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeWorkMovementBusiness = employeeworkmovementBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        //
        // GET: /EmployeeRetire/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            //Get view data here
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;

            IEnumerable<EmployeeRetireViewModel> lst = this._Search(SearchInfo).Where(o=>o.MovementType==3);
            ViewData[EmployeeRetireConstants.LIST_EMPLOYEEWORKMOVEMENT] = lst;
            // Lay du lieu cho combobox
            // School Faculty
            GlobalInfo g = new GlobalInfo();
            int SchoolID = g.SchoolID.HasValue ? g.SchoolID.Value : 0;
            IQueryable<SchoolFaculty> IQSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(SchoolID, SearchInfo).OrderBy(o => o.FacultyName);
            SelectList ListSchoolFaculty =
                new SelectList((IEnumerable<SchoolFaculty>)IQSchoolFaculty.ToList(), "SchoolFacultyID", "FacultyName");
            ViewData[EmployeeRetireConstants.LIST_SCHOOLFACULTY] = ListSchoolFaculty;
            // Employee
            ViewData[EmployeeRetireConstants.LIST_EMPLOYEE] = new SelectList(new List<Employee>());
            ViewData[EmployeeRetireConstants.IS_CURRENT_YEAR] = global.IsCurrentYear;

            return View();
        }

        /// <summary>
        ///       * Thuc hien load du lieu Can bo khi co thong tin truong va to bo mon
        /// </summary>
        /// <param name="ProvinceId"></param>
        /// <param name="DistrictId"></param>
        /// <returns></returns>           

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingTeacherByFaculty(int? SchoolFacultyID)
        {
            IEnumerable<Employee> lst = new List<Employee>();
            int FacultyID = 0;
            int SchoolID = 0;
            if (SchoolFacultyID.HasValue)
            {
                FacultyID = SchoolFacultyID.Value;
            }
            GlobalInfo globalInfo = new GlobalInfo();
            if (globalInfo.SchoolID.HasValue)
            {
                SchoolID = globalInfo.SchoolID.Value;
            }
            IQueryable<Employee> ListEmployee = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(SchoolID, FacultyID).OrderBy(o=>o.Name).ThenBy(o => o.FullName);
            if (ListEmployee != null)
            {
                lst = ListEmployee.ToList();
            }
           // lst = lst.Where(o => o.AppliedLevel == globalInfo.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }

        //
        // GET: /EmployeeRetire/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            //SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            //

            IEnumerable<EmployeeRetireViewModel> lst = this._Search(SearchInfo);
            ViewData[EmployeeRetireConstants.LIST_EMPLOYEEWORKMOVEMENT] = lst;
            ViewData[EmployeeRetireConstants.IS_CURRENT_YEAR] = global.IsCurrentYear;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            EmployeeWorkMovement employeeworkmovement = new EmployeeWorkMovement();
            TryUpdateModel(employeeworkmovement);
            Utils.Utils.TrimObject(employeeworkmovement);
            employeeworkmovement.FromSchoolID = new GlobalInfo().SchoolID.Value;
            employeeworkmovement.MovementType = SystemParamsInFile.WORK_MOVEMENT_RETIREMENT;

            this.EmployeeWorkMovementBusiness.InsertRetire(employeeworkmovement);
            this.EmployeeWorkMovementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EmployeeWorkMovementID)
        {
            EmployeeWorkMovement employeeworkmovement = this.EmployeeWorkMovementBusiness.Find(EmployeeWorkMovementID);
            EmployeeRetireViewModel ervm = new EmployeeRetireViewModel();
            TryUpdateModel(employeeworkmovement);
            TryUpdateModel(ervm);
            if (!ervm.MovedDate.HasValue)
            {
                return Json(new JsonMessage(Res.Get("EmployeeRetire_Label_DateFormat"), "error"));
            }
            Utils.Utils.TrimObject(employeeworkmovement);
            employeeworkmovement.FromSchoolID = new GlobalInfo().SchoolID.Value;
            employeeworkmovement.MovementType = SystemParamsInFile.WORK_MOVEMENT_RETIREMENT;
            employeeworkmovement.MovedDate = ervm.MovedDate;

            this.EmployeeWorkMovementBusiness.UpdateRetire(employeeworkmovement, new GlobalInfo().SchoolID.Value);
            this.EmployeeWorkMovementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EmployeeWorkMovementBusiness.DeleteRetire(id, new GlobalInfo().SchoolID.Value);
            this.EmployeeWorkMovementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EmployeeRetireViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo g = new GlobalInfo();
            int SchoolID = g.SchoolID.HasValue ? g.SchoolID.Value : 0;
            IQueryable<EmployeeWorkMovement> query = this.EmployeeWorkMovementBusiness.SearchRetire(SearchInfo, SchoolID).OrderByDescending(o => o.MovedDate).ThenBy(o => o.Employee.Name).ThenBy(o=>o.Employee.FullName);
            IQueryable<EmployeeRetireViewModel> lst = query.Select(o => new EmployeeRetireViewModel
            {
                EmployeeWorkMovementID = o.EmployeeWorkMovementID,
                SchoolFacultyID = o.Employee != null ? (o.Employee.SchoolFacultyID.HasValue ? o.Employee.SchoolFacultyID.Value : 0) : 0,
                SchoolFacultyName = o.Employee != null ? (o.Employee.SchoolFaculty != null ? o.Employee.SchoolFaculty.FacultyName : string.Empty) : string.Empty,
                TeacherID = o.TeacherID,
                TeacherName = o.Employee != null ? o.Employee.FullName : string.Empty,
                TeacherCode = o.Employee != null ? o.Employee.EmployeeCode : string.Empty,
                FromSchoolID = o.FromSchoolID,
                Description = o.Description,
                ResolutionDocument = o.ResolutionDocument,
                MovementType = o.MovementType,
                MovedDate = o.MovedDate,
                IsAccepted = o.IsAccepted
            });

            return lst.ToList();
        }
    }
}





