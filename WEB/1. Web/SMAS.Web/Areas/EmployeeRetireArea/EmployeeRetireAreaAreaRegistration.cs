﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeRetireArea
{
    public class EmployeeRetireAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeRetireArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeRetireArea_default",
                "EmployeeRetireArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
