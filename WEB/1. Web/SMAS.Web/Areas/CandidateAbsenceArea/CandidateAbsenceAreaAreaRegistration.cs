﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.CandidateAbsenceArea
{
    public class CandidateAbsenceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CandidateAbsenceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CandidateAbsenceArea_default",
                "CandidateAbsenceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
