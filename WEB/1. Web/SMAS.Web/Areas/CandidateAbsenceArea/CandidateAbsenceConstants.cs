﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.CandidateAbsenceArea
{
    public class CandidateAbsenceConstants
    {
        public const string LIST_CANDIDATE_ABSENCE = "listCandidateAbsence";

        public const string LIST_EXAMINATION = "listExamination";
        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_EXAMINATION_SUBJECT = "listExaminationSubject";
        public const string LIST_CANDIDATE = "listCandidate";
        public const string LIST_ROOM = "listRoom";

        public const string ENABLE_CREATE = "enable_create";
        public const string ENABLE_DELETE = "enable_delete";

        public const string LIST_EXAMINATION_CREATE = "LIST_EXAMINATION_CREATE";
    }
}