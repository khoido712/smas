﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.CandidateAbsenceArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ExaminationID { get; set; }

        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        public int? ExaminationSubjectID { get; set; }

        [ResourceDisplayName("Candidate_Label_NamedListCode")]
        [StringLength(16, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string NamedListCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }
    }
}