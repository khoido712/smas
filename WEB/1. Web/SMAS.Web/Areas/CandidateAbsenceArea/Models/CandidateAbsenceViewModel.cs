using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using Resources;

namespace SMAS.Web.Areas.CandidateAbsenceArea.Models
{
    public class CandidateAbsenceViewModel
    {
        [ResourceDisplayName("Candidate_Label_AllTitle")]
        public int CandidateID { get; set; }

        [ResourceDisplayName("Examination_Label_Title")]
        public string ExaminationTitle { get; set; }

        [ResourceDisplayName("Candidate_Label_NamedListCode")]
        public string NameListCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime BirthDay { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public int Genre { get; set; }

        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        public string EducationLevelResolution { get; set; }

        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        public string RoomTitle { get; set; }

        [ResourceDisplayName("Candidate_Label_HasReason")]
        public bool? HasReason { get; set; }

        [ResourceDisplayName("Candidate_Label_ReasonDetail")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ReasonDetail { get; set; }
    }
}
