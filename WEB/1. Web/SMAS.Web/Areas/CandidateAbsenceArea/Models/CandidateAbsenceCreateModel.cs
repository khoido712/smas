using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.CandidateAbsenceArea.Models
{
    public class CandidateAbsenceCreateModel
    {
        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateEducationLevelID { get; set; }

        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateExaminationID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateExaminationSubjectID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        public int CreateRoomID { get; set; }

        [ResourceDisplayName("Candidate_Label_ReasonDetail")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string CreateReasonDetail { get; set; }
        
        public int[] CreateCandidates { get; set; }
    }
}
