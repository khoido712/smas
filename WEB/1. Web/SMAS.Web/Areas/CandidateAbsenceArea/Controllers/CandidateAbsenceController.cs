﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Web.Areas.CandidateAbsenceArea.Models;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System.IO;
using SMAS.Web.Filter;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.CandidateAbsenceArea.Controllers
{
    public class CandidateAbsenceController : BaseController
    {
        private readonly ICandidateAbsenceBusiness CandidateAbsenceBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExamViolationTypeBusiness ExamViolationTypeBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;

        public CandidateAbsenceController(ICandidateAbsenceBusiness candidateAbsenceBusiness,
                                            IExaminationBusiness examinationBusiness,
                                            IExamViolationTypeBusiness examViolationTypeBusiness,
                                            IExaminationSubjectBusiness examinationSubjectBusiness,
                                            ICandidateBusiness candidateBusiness,
                                            IExaminationRoomBusiness examinationRoomBusiness)
        {
            this.CandidateAbsenceBusiness = candidateAbsenceBusiness;
            this.ExaminationBusiness = examinationBusiness;
            this.ExamViolationTypeBusiness = examViolationTypeBusiness;
            this.ExaminationSubjectBusiness = examinationSubjectBusiness;
            this.CandidateBusiness = candidateBusiness;
            this.ExaminationRoomBusiness = examinationRoomBusiness;
        }

        public ActionResult Index()
        {
            ViewData[CandidateAbsenceConstants.LIST_CANDIDATE_ABSENCE] = new List<CandidateAbsenceViewModel>();
            GetViewData();
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            if (ModelState.IsValid)
            {
                Utils.Utils.TrimObject(frm);
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo.Add("EducationLevelID", frm.EducationLevelID.HasValue && frm.EducationLevelID.Value > 0 ? frm.EducationLevelID.Value : 0);
                SearchInfo.Add("ExaminationID", frm.ExaminationID);
                SearchInfo.Add("ExaminationSubjectID", frm.ExaminationSubjectID.HasValue && frm.ExaminationSubjectID.Value > 0 ? frm.ExaminationSubjectID.Value : 0);
                SearchInfo.Add("NamedListCode", !string.IsNullOrEmpty(frm.NamedListCode) ? frm.NamedListCode : "");
                SearchInfo.Add("PupilCode", !string.IsNullOrEmpty(frm.PupilCode) ? frm.PupilCode : "");
                SearchInfo.Add("FullName", !string.IsNullOrEmpty(frm.FullName) ? frm.FullName : "");
                SearchInfo.Add("RoomID", 0);

                Examination exam = ExaminationBusiness.Find(frm.ExaminationID);
                ViewData[CandidateAbsenceConstants.ENABLE_DELETE] = exam != null && (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED);
                ViewData[CandidateAbsenceConstants.LIST_CANDIDATE_ABSENCE] = this._Search(SearchInfo);
            }
            else
            {
                ViewData[CandidateAbsenceConstants.LIST_CANDIDATE_ABSENCE] = new List<CandidateAbsenceViewModel>();
                ViewData[CandidateAbsenceConstants.ENABLE_DELETE] = false;
            }
            return PartialView("_List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(CandidateAbsenceCreateModel model)
        {
            if (ModelState.IsValid && model.CreateCandidates == null)
            {
                throw new BusinessException(Res.Get("Candidate_Label_ErrorCheckMessage"));
            }
            GlobalInfo glo = new GlobalInfo();
            bool hasReason = false;
            if (!string.IsNullOrEmpty(Request.Form["CreateHasReason"]))
                hasReason = true;
            CandidateAbsenceBusiness.InsertAll(glo.SchoolID.Value, glo.AppliedLevel.Value, hasReason, model.CreateReasonDetail, model.CreateCandidates.ToList());
            CandidateAbsenceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int candidateID)
        {
            GlobalInfo glo = new GlobalInfo();

            Candidate candidate = CandidateBusiness.Find(candidateID);
            CheckPermissionForAction(candidate.ExaminationID.Value, "Examination");
            TryUpdateModel(candidate);

            if (string.IsNullOrEmpty(Request.Form["HasReason"]))
                candidate.HasReason = false;
            else
                candidate.HasReason = true;

            this.CandidateAbsenceBusiness.Update(candidateID, glo.SchoolID.Value, glo.AppliedLevel.Value, candidate.HasReason.Value, candidate.ReasonDetail);
            this.CandidateAbsenceBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadExamSubject(int? educationLevelID, int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            List<ExaminationSubjectBO> listCbo = new List<ExaminationSubjectBO>();
            if (educationLevelID.HasValue && educationLevelID.Value > 0 && examinationID.HasValue && examinationID.Value > 0)
            {
                Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
                dicExaminationSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
                dicExaminationSubject.Add("AppliedLevel", glo.AppliedLevel.Value);
                dicExaminationSubject.Add("EducationLevelID", educationLevelID);
                dicExaminationSubject.Add("ExaminationID", examinationID);
                listCbo = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject)
                    .Select(o => new ExaminationSubjectBO
                    {
                        ExaminationSubjectID = o.ExaminationSubjectID,
                        DisplayName = o.SubjectCat.DisplayName
                    }).ToList();
            }
            return Json(new SelectList(listCbo, "ExaminationSubjectID", "DisplayName"), JsonRequestBehavior.AllowGet);
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadRoom(int examSubjectID)
        {
            List<ExaminationRoom> lstRooms = ExaminationRoomBusiness.All.Where(u => u.ExaminationSubjectID == examSubjectID).ToList();
            return Json(new SelectList(lstRooms, "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GridAction(int[] CandidateAbsences, string deleteall, string export)
        {
            if (CandidateAbsences != null && CandidateAbsences.Length > 0)
            {
                if (!string.IsNullOrEmpty(deleteall)) //nguoi dung chon xoa 
                {
                    GlobalInfo glo = new GlobalInfo();
                    CandidateAbsenceBusiness.DeleteAll(glo.SchoolID.Value, glo.AppliedLevel.Value, CandidateAbsences.ToList());
                    this.CandidateAbsenceBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
                }
                else //Nguoi dung chon export
                {
                    return Json(new JsonMessage(Res.Get("Error")));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Error")));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult CheckPermision(int examinationID)
        {
            Examination exam = ExaminationBusiness.Find(examinationID);
            GlobalInfo glo = new GlobalInfo();
            bool enabled = exam != null && exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED && glo.IsCurrentYear;
            return Json(new { enabled = enabled });
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadCandidate(int roomId)
        {
            GlobalInfo glo = new GlobalInfo();


            ViewData[CandidateAbsenceConstants.LIST_CANDIDATE] = CandidateBusiness.All.Where(u => u.RoomID == roomId && !u.HasReason.HasValue)
                                                                        .Select(u => new CandidateGridViewModel
                                                                        {
                                                                            CandidateID = u.CandidateID,
                                                                            NamedListNumber = u.NamedListNumber,
                                                                            Name = u.PupilProfile.Name,
                                                                            FullName = u.PupilProfile.FullName,
                                                                            PupilCode = u.PupilCode
                                                                        }).ToList().OrderBy(o => o.NamedListNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            return PartialView("_Candidates");
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadEducationLevel(int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            if (examinationID.HasValue && examinationID.Value > 0)
            {
                dicExaminationSubject.Add("ExaminationID", examinationID.Value);
            }
            else
            {
                return Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            var lstEdu = lstExamSubject.Select(u => new { u.EducationLevelID, u.EducationLevel.Resolution })
                .OrderBy(o => o.EducationLevelID)
                .Distinct();

            return Json(new SelectList(lstEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }

        public FileResult Export(SearchViewModel frm)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo GlobalInfo = new GlobalInfo();

                Utils.Utils.TrimObject(frm);

                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = frm.EducationLevelID.HasValue && frm.EducationLevelID.Value > 0 ? frm.EducationLevelID.Value : 0;
                SearchInfo["ExaminationID"] = frm.ExaminationID;
                SearchInfo["ExaminationSubjectID"] = frm.ExaminationSubjectID.HasValue && frm.ExaminationSubjectID.Value > 0 ? frm.ExaminationSubjectID.Value : 0;
                SearchInfo["NamedListCode"] = !string.IsNullOrEmpty(frm.NamedListCode) ? frm.NamedListCode : "";
                SearchInfo["PupilCode"] = !string.IsNullOrEmpty(frm.PupilCode) ? frm.PupilCode : "";
                SearchInfo["FullName"] = !string.IsNullOrEmpty(frm.FullName) ? frm.FullName : "";
                SearchInfo["RoomID"] = 0;

                SearchInfo["SchoolID"] = GlobalInfo.SchoolID.Value;
                SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;

                string fileName = string.Empty;
                Stream stream = CandidateAbsenceBusiness.CreateCandidateAbsenceReport(SearchInfo, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();

            Dictionary<string, object> dicExamination = new Dictionary<string, object>();
            dicExamination.Add("AcademicYearID", glo.AcademicYearID.Value);
            dicExamination.Add("AppliedLevel", glo.AppliedLevel.Value);
            IQueryable<Examination> lstQExam = ExaminationBusiness.SearchBySchool(glo.SchoolID.Value, dicExamination);
            List<Examination> lstExam = lstQExam.Where(u => u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED
                                    || u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED
                                    || u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_FINISHED).OrderByDescending(u => u.ToDate).ToList();
            List<Examination> lstExamCreate = lstExam.Where(p => p.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED).ToList();
            ViewData[CandidateAbsenceConstants.LIST_EXAMINATION] = new SelectList(lstExam, "ExaminationID", "Title");
            ViewData[CandidateAbsenceConstants.LIST_EDUCATION_LEVEL] = new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            ViewData[CandidateAbsenceConstants.LIST_EXAMINATION_CREATE] = new SelectList(lstExamCreate, "ExaminationID", "Title");

            //Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            //dicExaminationSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
            //dicExaminationSubject.Add("AppliedLevel", glo.AppliedLevel.Value);

            //List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            List<ExaminationSubject> lstExamSubject = new List<ExaminationSubject>();
            ViewData[CandidateAbsenceConstants.LIST_EXAMINATION_SUBJECT] = new SelectList(lstExamSubject.Select(u => new { u.ExaminationSubjectID, u.SubjectCat.SubjectName }).ToList(), "ExaminationSubjectID", "SubjectName");

            //List<ExaminationRoom> lstRooms = ExaminationRoomBusiness.All.ToList();
            //ViewData[CandidateAbsenceConstants.LIST_ROOM] = new SelectList(lstRooms, "ExaminationRoomID", "RoomTitle");

            ViewData[CandidateAbsenceConstants.ENABLE_DELETE] = false;
        }

        private IEnumerable<CandidateAbsenceViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo glo = new GlobalInfo();
            IQueryable<Candidate> query = this.CandidateAbsenceBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo);

            IQueryable<CandidateAbsenceViewModel> lst = query.Select(o => new CandidateAbsenceViewModel
            {
                CandidateID = o.CandidateID,
                BirthDay = o.PupilProfile.BirthDate,
                ExaminationTitle = o.Examination.Title,
                EducationLevelResolution = o.EducationLevel.Resolution,
                FullName = o.PupilProfile.FullName,
                Genre = o.PupilProfile.Genre,
                HasReason = o.HasReason,
                NameListCode = o.NamedListCode,
                PupilCode = o.PupilCode,
                ReasonDetail = o.ReasonDetail,
                RoomTitle = o.ExaminationRoom.RoomTitle,
                SubjectName = o.ExaminationSubject.SubjectCat.SubjectName
            });
            return lst.ToList();
        }
    }
}
