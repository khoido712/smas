﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.ExamPupilArea.Models;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Web.Areas.ExamPupilArea;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
namespace SMAS.Web.Areas.ExamPupilArea.Controllers
{
    public class ExamPupilController : BaseController
    {

        #region properties
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private GlobalInfo global;
        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        #endregion

        #region Constructor
        public ExamPupilController(IExamPupilBusiness ExamPupilBusiness, IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness,
            IClassProfileBusiness ClassProfileBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IExamSubjectBusiness ExamSubjectBusiness)
        {
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;

            global = new GlobalInfo();
        }
        #endregion

        #region Actions
        //
        // GET: /ExamPupilArea/ExamPupil/

        public ActionResult Index()
        {
            SetViewData();

            //Search du lieu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //long? defaultExamID = listExaminations.FirstOrDefault()!=null?listExaminations.FirstOrDefault().ExaminationsID:0;
            long? defaultExamID = null;
            if (listExaminations.Count > 0)
            {
                defaultExamID = listExaminations.FirstOrDefault().ExaminationsID;
            }

            List<ExamPupilViewModel> listResult;
            if (defaultExamID == null)
            {
                listResult=new List<ExamPupilViewModel>();
                ViewData[ExamPupilConstants.LIST_EXAM_PUPIL] = listResult;
                ViewData[ExamPupilConstants.TOTAL] = 0;
                CheckCommandPermision(null, listResult);

                return View();
            }

            SearchInfo["ExaminationsID"] = defaultExamID;
            SearchInfo["SchoolID"] = global.SchoolID;

            IEnumerable<ExamPupilViewModel> iq = _Search(SearchInfo);

            int totalRecord = iq.Count();
            ViewData[ExamPupilConstants.TOTAL] = totalRecord;
            listResult = iq.Take(ExamPupilConstants.PageSize).ToList();

            ViewData[ExamPupilConstants.LIST_EXAM_PUPIL] = listResult;

            //Kiem tra button
            CheckCommandPermision(listExaminations.FirstOrDefault(), listResult);
            return View();
        }

        //
        // GET: /ExamPupil/Search
        public PartialViewResult Search(SearchViewModel form, GridCommand command)
        {
            Utils.Utils.TrimObject(form);

            List<ExamPupilViewModel> listResult;

            if (form.ExaminationsID == null)
            {
                listResult = new List<ExamPupilViewModel>();
                ViewData[ExamPupilConstants.TOTAL] = 0;
                CheckCommandPermision(null, listResult);
                return PartialView("_List", listResult);
            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["ExaminationsID"] = form.ExaminationsID;
            SearchInfo["ExamGroupID"] = form.ExamGroupID;
            SearchInfo["PupilCode"] = form.PupilCode;
            SearchInfo["PupilName"] = form.PupilName;

            //Add search info - Navigate to Search function in business
            IEnumerable<ExamPupilViewModel> iq = _Search(SearchInfo);
            int totalRecord = iq.Count();
            ViewData[ExamPupilConstants.TOTAL] = totalRecord;
            listResult = iq.Take(ExamPupilConstants.PageSize).ToList();

            //Kiem tra button
            CheckCommandPermision(ExaminationsBusiness.Find(form.ExaminationsID), listResult);
            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchExamPupilAjax(SearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);

            List<ExamPupilViewModel> listResult;
            if (form.ExaminationsID == null)
            {
                listResult = new List<ExamPupilViewModel>();
                CheckCommandPermision(null, listResult);
                return View(new GridModel<ExamPupilViewModel>
                {
                    Total = 0,
                    Data = listResult
                });
            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["ExaminationsID"] = form.ExaminationsID;
            SearchInfo["ExamGroupID"] = form.ExamGroupID;
            SearchInfo["PupilCode"] = form.PupilCode;
            SearchInfo["PupilName"] = form.PupilName;

            //Add search info - Navigate to Search function in business
            IEnumerable<ExamPupilViewModel> iq = _Search(SearchInfo);
            int totalRecord = iq.Count();
            listResult = iq.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

            //Kiem tra button
            CheckCommandPermision(ExaminationsBusiness.Find(form.ExaminationsID), listResult);
            return View(new GridModel<ExamPupilViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(string arrayID)
        {
            if (GetMenupermission("ExamPupilArea", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            string[] examPupilIDArr;
            if (arrayID != "")
            {
                arrayID=arrayID.Remove(arrayID.Length - 1);
                examPupilIDArr = arrayID.Split(',');
            }
            else
            {
                examPupilIDArr=new string[]{};
            }
            List<long> listExamPupilID = examPupilIDArr.Length > 0 ? examPupilIDArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList() :
                                            new List<long>();
            ExamPupilBusiness.DeleteList(listExamPupilID);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        //
        // GET: /ExamPupil/Create/
        public ActionResult Create(long examinationsID, long? examGroupID)
        {
            SetViewDataForCreatePopup(examinationsID);

            IDictionary<string, object> dic = new Dictionary<string, object>();
           
            //Lay nhom thi mac dinh
            long? defaultExamGroupID = null;
            if (examGroupID != null)
            {
                defaultExamGroupID = examGroupID;
            }
            else if (listExamGroup.Count > 0)
            {
                defaultExamGroupID = listExamGroup.First().ExamGroupID;
            }

            //Lay khoi mac dinh
            int? defaultEducationLevel = null;
            if (global.EducationLevels.Count > 0)
            {
                defaultEducationLevel = global.EducationLevels.First().EducationLevelID;
            }
           
            dic["EducationLevelID"] = defaultEducationLevel;

            IEnumerable<PupilViewModel> iq = _SearchForCreate(dic, examinationsID, defaultExamGroupID);
            int totalRecord = iq.Count();
            ViewData[ExamPupilConstants.CREATE_TOTAL] = totalRecord;
            List<PupilViewModel> listResult = iq.Take(ExamPupilConstants.PageSize).ToList();

            ViewData[ExamPupilConstants.CREATE_LIST_PUPIL] = listResult;
            CreateSearchViewModel model = new CreateSearchViewModel();
            model.CsExaminationsID = examinationsID;
            model.CsExamGroupID=examGroupID;
            ViewData[ExamPupilConstants.CREATE_SEARCH_INFO] = model;

            return View();
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(string arrayID, long? paraExaminationsID, long? paraExamGroupID, 
            int? paraEducationLevelID, int? paraClassID, string paraPupilCodeOrName, int isGetAll)
        {
            //Kiem tra quyen insert
            if (GetMenupermission("ExamPupilArea", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            //Kiem tra du lieu hop le
            //Neu chua chon ky thi
            if (paraExaminationsID == null)
            {
                throw new BusinessException("ExamPupil_Validate_Create_Examinations");
            }

            //Neu chua chon nhom thi
            if (paraExamGroupID == null)
            {
                throw new BusinessException("ExamPupil_Validate_Create_ExamGroup");
            }

            string[] pupilIDArr;
            if (arrayID != "")
            {
                arrayID = arrayID.Remove(arrayID.Length - 1);
                pupilIDArr = arrayID.Split(new[] {',','\"'}, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                pupilIDArr = new string[] { };
            }

            if (pupilIDArr.Length == 0 && isGetAll==0)
            {
                throw new BusinessException("ExamPupil_Validate_Create_NotChoosen");
            }

             List<long> listPupilID = pupilIDArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList();
            //Lay lai danh sach hoc sinh dang hoc

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = paraEducationLevelID;
            dic["ClassID"] = paraClassID;
            dic["PupilCodeOrName"] = paraPupilCodeOrName;
            IEnumerable<PupilViewModel> listPupil = _SearchForCreate(dic, paraExaminationsID.GetValueOrDefault(), paraExamGroupID);
 
            //Neu khong tich chon Lay tat ca, chi loc lai cac hoc sinh duoc tich chon
            if (isGetAll == 0)
            {
                listPupil = listPupil.Where(o => listPupilID.Any(u => u == o.PupilID));
            }

            //Kiem tra 1 hoc sinh khong duoc tham gia nhieu nhom thi cung mon thi
            List<int> lstPupilID=listPupil.Select(o=>o.PupilID).ToList();
            ExamGroup examGroup = ExamGroupBusiness.Find(paraExamGroupID);
            //Lay ra cac mon thi cua nhom thi
            List<int> lstSubjectID = ExamSubjectBusiness.All.Where(o => o.ExaminationsID == paraExaminationsID && o.ExamGroupID == paraExamGroupID)
                                                                    .Select(o => o.SubjectID).Distinct().ToList();

            int partition = UtilsBusiness.GetPartionId(global.SchoolID.Value);
            IQueryable<ExamPupil> query = ExamPupilBusiness.GetGroupedPupilWithSameSubject(lstPupilID, lstSubjectID, paraExaminationsID.Value,partition);
            if (query.Count() > 0)
            {
                throw new BusinessException("ExamPupil_Validate_Create_DuplicateGroupWithSameSubject");
            }


            //Tao cac entity de insert
           
            List<ExamPupil> insertList= listPupil.ToList().Select(o=>new ExamPupil
                                        {
                                            ExamPupilID  = ExamPupilBusiness.GetNextSeq<long>(),
                                            ExaminationsID=paraExaminationsID.GetValueOrDefault(),
                                            ExamGroupID=paraExamGroupID.GetValueOrDefault(),
                                            EducationLevelID=paraEducationLevelID.GetValueOrDefault(),
                                            SchoolID=global.SchoolID.GetValueOrDefault(),
                                            LastDigitSchoolID = partition,
                                            PupilID=o.PupilID,
                                            CreateTime=DateTime.Now
                                        }).ToList();

            ExamPupilBusiness.InsertList(insertList);

            //ghi log
            SetViewDataActionAudit(String.Empty, String.Empty
            , String.Empty
            , Res.Get("ExamPupil_Log_Create")
            , String.Empty
            , Res.Get("ExamPupil_Log_FunctionName")
            , SMAS.Business.Common.GlobalConstants.ACTION_ADD
            , Res.Get("ExamPupil_Log_Create"));

            return Json(new JsonMessage(Res.Get("ExamPupil_Message_CreateSuccess")));
        }
        //
        // GET: /ExamPupil/CreateSearch
        public PartialViewResult CreateSearch(CreateSearchViewModel form)
        {
            Utils.Utils.TrimObject(form);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = form.EducationLevelID;
            SearchInfo["ClassID"] = form.ClassID;
            SearchInfo["PupilCodeOrName"] = form.PupilCodeOrName;

            //Add search info - Navigate to Search function in business
            IEnumerable<PupilViewModel> iq = _SearchForCreate(SearchInfo, form.CsExaminationsID.GetValueOrDefault(), form.CsExamGroupID.GetValueOrDefault());
            int totalRecord = iq.Count();
            ViewData[ExamPupilConstants.CREATE_TOTAL] = totalRecord;

            //Kiem tra neu lop la tat ca thi phan trang
            List<PupilViewModel> listResult;
            if (!form.ClassID.HasValue || form.ClassID == 0)
            {
                listResult = iq.Take(ExamPupilConstants.PageSize).ToList();
                ViewData[ExamPupilConstants.PAGING_ENABLE] = true;
            }
            else
            {
                listResult = iq.ToList();
                ViewData[ExamPupilConstants.PAGING_ENABLE] = false;
            }

            return PartialView("_CreateList", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchPupilAjax(CreateSearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = form.EducationLevelID;
            SearchInfo["ClassID"] = form.ClassID;
            SearchInfo["PupilCodeOrName"] = form.PupilCodeOrName;

            //Add search info - Navigate to Search function in business
            IEnumerable<PupilViewModel> iq = _SearchForCreate(SearchInfo, form.CsExaminationsID.GetValueOrDefault(), form.CsExamGroupID.GetValueOrDefault());
            int totalRecord = iq.Count();
            List<PupilViewModel> listResult = iq.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

            return View(new GridModel<PupilViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        public JsonResult AjaxLoadClass(int paraEducationLevelID)
        {
            IDictionary<string, object> dicToGetClass = new Dictionary<string, object>();
            dicToGetClass["EducationLevelID"] = paraEducationLevelID;
            dicToGetClass["AcademicYearID"] = global.AcademicYearID;
            List<ClassProfile> listClass = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dicToGetClass).ToList();

            return Json(new SelectList(listClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult AjaxLoadExamGroup(long? paraExaminationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(paraExaminationsID.GetValueOrDefault())
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        #endregion

        #region Private methods
        
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
           listExaminations = ExaminationsBusiness.GetListExamination(global.AcademicYearID.GetValueOrDefault())
                                            .Where(o=>o.SchoolID==global.SchoolID)
                                            .Where(o=>o.AppliedLevel==global.AppliedLevel)
                                            .OrderByDescending(o=>o.CreateTime).ToList();

           ViewData[ExamPupilConstants.CBO_EXAM] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");

           long defaultExamID = listExaminations.FirstOrDefault() != null ? listExaminations.FirstOrDefault().ExaminationsID : 0;

            //Lấy danh sách nhóm thi

            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID)
                                                    .OrderBy(o => o.ExamGroupCode).ToList();
            ViewData[ExamPupilConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

        }

        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search của màn hình thêm mới
        /// </summary>
        private void SetViewDataForCreatePopup(long examinationsID)
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(global.AcademicYearID.GetValueOrDefault())
                                             .Where(o => o.SchoolID == global.SchoolID)
                                             .Where(o => o.AppliedLevel == global.AppliedLevel)
                                             .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamPupilConstants.CREATE_CBO_EXAM] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");

            //Lấy danh sách nhóm thi
            IDictionary<string, object> dicToGetExamGroup = new Dictionary<string, object>();

            listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                                    .OrderBy(o => o.ExamGroupCode).ToList();
            ViewData[ExamPupilConstants.CREATE_CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            //Lấy danh sách khối học
            ViewData[ExamPupilConstants.CREATE_CBO_EDUCATION_LEVEL] = new SelectList(global.EducationLevels, "EducationLevelID", "Resolution");
            long defaultEducationLevelID = global.EducationLevels.FirstOrDefault() != null ? global.EducationLevels.FirstOrDefault().EducationLevelID : 0;
            
            //Lấy danh sách lớp học
             IDictionary<string, object> dicToGetClass = new Dictionary<string, object>();
            dicToGetClass["EducationLevelID"] = defaultEducationLevelID;
            dicToGetClass["AcademicYearID"] = global.AcademicYearID;
            List<ClassProfile> listClass = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dicToGetClass).ToList();
            ViewData[ExamPupilConstants.CREATE_CBO_CLASS] = new SelectList(listClass, "ClassProfileID", "DisplayName");
        }

        /// <summary>
        /// Hàm tìm kiếm
        /// </summary>
        /// <param name="SearchInfo">Dictionary chứa các điều kiện search</param>
        /// <returns>Danh sách kết quả</returns>
        private IEnumerable<ExamPupilViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //Tim kiem 
            IEnumerable<ExamPupilBO> query = this.ExamPupilBusiness.Search(SearchInfo, global.AcademicYearID.GetValueOrDefault(), partitionID);

            IEnumerable<ExamPupilViewModel> ret = query.Select(o => new ExamPupilViewModel
                    {
                        BirthDay=o.BirthDate,
                        ClassDisplayName=o.ClassName,
                        ClassID=o.ClassID,
                        ExamGroupID=o.ExamGroupID,
                        ExamGroupCode=o.ExamGroupCode,
                        ExamGroupName=o.ExamGroupName,
                        ExamPupilID=o.ExamPupilID,
                        Genre=o.Genre,
                        PupilCode=o.PupilCode,
                        PupilName=o.PupilFullName,
                        EthnicCode=o.EthnicCode
                    }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilName.getOrderingName(o.EthnicCode)));
            return ret;
        }

        /// <summary>
        /// Ham search du lieu cho popup them moi
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IEnumerable<PupilViewModel> _SearchForCreate(IDictionary<string, object> dic, long? examinationsID, long? examGroupID)
        {

            //Lay danh sach hoc sinh dang hoc
            IQueryable<PupilOfClassBO> lstPupil = this.PupilOfClassBusiness.GetStudyingPupil(global.SchoolID.Value, 
                global.AcademicYearID.Value, global.AppliedLevel.Value, dic);

            //Lay danh sach hoc sinh da duoc xep nhom thi
            List<int> lstGroupedPupilID=new List<int>();
            if (examinationsID != null && examGroupID != null)
            {
                IDictionary<string, object> dicToGetExamPupil = new Dictionary<string, object>();
                dicToGetExamPupil["ExaminationsID"] = examinationsID;
                dicToGetExamPupil["ExamGroupID"] = examGroupID;
                dicToGetExamPupil["SchoolID"] = global.SchoolID;

                int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                lstGroupedPupilID = this.ExamPupilBusiness.Search(dicToGetExamPupil, global.AcademicYearID.Value, partitionID)
                                                    .Select(o => o.PupilID).ToList();
            }
            //Lay danh sach hoc sinh chua duoc xep nhom thi
            IEnumerable<PupilViewModel> ret = lstPupil.Where(o => !lstGroupedPupilID.Contains(o.PupilID))
                                        .Select(o => new PupilViewModel
                                        {
                                            CsBirthDay=o.Birthday,
                                            CsClassDisplayName=o.ClassName,
                                            CsClassID=o.ClassID,
                                            CsGenre=o.Genre,
                                            CsPupilCode=o.PupilCode,
                                            CsPupilName=o.PupilFullName,
                                            EthnicCode=o.EthnicCode,
                                            PupilID=o.PupilID,
                                            OrderInClass=o.OrderInClass
                                        }).ToList();

            //Kiem tra neu lop hoc la tat ca thi sap xep theo ho va ten hoc sinh, neu co lop hoc thi sap xep theo STT hoc sinh
            int classID = SMAS.Business.Common.Utils.GetInt(dic, "ClassID");
            if (classID > 0)
            {
                ret = ret.OrderBy(o => o.OrderInClass);
            }
            else
            {
                ret = ret.OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.CsPupilName.getOrderingName(o.EthnicCode)));
            }

            return ret;
        }
        /// <summary>
        /// Kiem tra disable/enable, an/hien cac button
        /// </summary>
        /// <param name="listResult"></param>
        private void CheckCommandPermision(Examinations examinations, List<ExamPupilViewModel> listResult)
        {

            SetViewDataPermission("ExamPupilArea", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin, _globalInfo.RoleID);

            bool checkCreate = false;

            //Kiem tra quyen them
            if (examinations != null)
            {
                if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE]
                    && examinations.MarkInput.GetValueOrDefault() != true
                    && examinations.MarkClosing.GetValueOrDefault() != true
                    && global.IsCurrentYear)
                {
                    checkCreate = true;
                }
            }
            ViewData[ExamPupilConstants.PER_CHECK_CREATE] = checkCreate;

            //Kiem tra quyen sua, xoa
            //Lay danh sach du lieu rang buoc
            if(examinations!=null)
            {
                List<long> lstExamPupilID = listResult.Select(o => o.ExamPupilID).ToList<long>();
                List<long> lstInUsedExamPupilID = ExamPupilBusiness.ExamPupilInUsed(lstExamPupilID,examinations.ExaminationsID,UtilsBusiness.GetPartionId(_globalInfo.SchoolID.GetValueOrDefault()));
                for (int i = 0; i < listResult.Count; i++)
                {
                    ExamPupilViewModel ex = listResult[i];
                    ex.canDelete = false;

                    if (examinations != null)
                    {
                        bool isExamPupilUsed = lstInUsedExamPupilID.Contains(ex.ExamPupilID);

                        if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE]
                         && !isExamPupilUsed
                         && examinations.MarkInput.GetValueOrDefault() != true
                         && examinations.MarkClosing.GetValueOrDefault() != true)
                        {
                            ex.canDelete = true;
                        }
                    }
                }
            }
        }
        #endregion

    }
}
