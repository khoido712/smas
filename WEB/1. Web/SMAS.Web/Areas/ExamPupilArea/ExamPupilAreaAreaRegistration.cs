﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamPupilArea
{
    public class ExamPupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamPupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamPupilArea_default",
                "ExamPupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
