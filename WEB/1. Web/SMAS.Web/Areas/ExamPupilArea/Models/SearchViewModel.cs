﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ExamPupilArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamPupil_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilConstants.CBO_EXAM)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public int? ExaminationsID { get; set; }

        [ResourceDisplayName("ExamPupil_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamPupilConstants.CBO_EXAM_GROUP)]
        public int? ExamGroupID { get; set; }

        [ResourceDisplayName("ExamPupil_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ExamPupil_Label_PupilName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilName { get; set; }
    }
}