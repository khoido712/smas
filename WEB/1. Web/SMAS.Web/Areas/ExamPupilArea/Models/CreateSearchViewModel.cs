﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ExamPupilArea.Models
{
    public class CreateSearchViewModel
    {
        [ResourceDisplayName("ExamPupil_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilConstants.CREATE_CBO_EXAM)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public long? CsExaminationsID { get; set; }

        [ResourceDisplayName("ExamPupil_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamPupilConstants.CREATE_CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public long? CsExamGroupID { get; set; }

        [ResourceDisplayName("ExamPupil_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamPupilConstants.CREATE_CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "onEducationLevelChange()")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("ExamPupil_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamPupilConstants.CREATE_CBO_CLASS)]
        [AdditionalMetadata("OnChange", "onClassChange()")]
        public int? ClassID { get; set; }

        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCodeOrName { get; set; }
    }
}