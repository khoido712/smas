﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
namespace SMAS.Web.Areas.ExamPupilArea.Models
{
    public class ExamPupilViewModel
    {
        public long ExamPupilID { get; set; }

        public int PupilID { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_PupilName")]
        public string PupilName { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_BirthDay")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDay { get; set; }

        public int? Genre { get; set; }

        public string EthnicCode { get; set; }

        public int ClassID { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_ClassDisplayName")]
        public string ClassDisplayName { get; set; }

        public long ExamGroupID { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_ExamGroupCode")]
        public string ExamGroupCode { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_ExamGroupCode")]
        public string ExamGroupName { get; set; }

        public bool canDelete { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_Genre")]
        public string DisplayGenre
        {
            get
            {
                return Genre.GetValueOrDefault() == GlobalConstants.GENRE_MALE ? GlobalConstants.GENRE_MALE_TITLE :
                                (this.Genre.GetValueOrDefault() == GlobalConstants.GENRE_FEMALE ? GlobalConstants.GENRE_FEMALE_TITLE :
                                (this.Genre.GetValueOrDefault() == 2 ? Res.Get("Common_Label_GenreUnidentified") : String.Empty));
            }
        }

    }
}