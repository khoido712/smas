﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;

using SMAS.Web.Constants;
using SMAS.Web.Utils;
namespace SMAS.Web.Areas.ExamPupilArea.Models
{
    public class PupilViewModel
    {

        public int PupilID { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_PupilCode")]
        public string CsPupilCode { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_PupilName")]
        public string CsPupilName { get; set; }

        public string EthnicCode { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_BirthDay")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CsBirthDay { get; set; }

       
        public int? CsGenre{ get;set;}

        public int CsClassID { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_ClassDisplayName")]
        public string CsClassDisplayName { get; set; }

        public int? OrderInClass { get; set; }

         [ResourceDisplayName("ExamPupil_Grid_Label_Genre")]
        public string CsDisplayGenre {
            get 
            {
            return CsGenre.GetValueOrDefault() == GlobalConstants.GENRE_MALE ? GlobalConstants.GENRE_MALE_TITLE :
                            (this.CsGenre.GetValueOrDefault() == GlobalConstants.GENRE_FEMALE ? GlobalConstants.GENRE_FEMALE_TITLE :
                            (this.CsGenre.GetValueOrDefault() == 2?Res.Get("Common_Label_GenreUnidentified"):String.Empty));
            }
        }
    }
}