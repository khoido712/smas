﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamPupilArea
{
    public class ExamPupilConstants
    {
        public const string CBO_EXAM = "cbo_exam";
        public const string CBO_EXAM_DEFAULT = "cbo_exam_default";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string CBO_EXAM_GROUP_DEFAULT = "cbo_exam_group_default";
        public const string LIST_EXAM_PUPIL = "list_exam_pupil";
        public const string PER_CHECK_CREATE = "per_check_create";
        
        //Constant for create screen
        public const string CREATE_LIST_PUPIL = "create_list_pupil";
        public const string CREATE_SEARCH_INFO = "create_search_info";
        public const string CREATE_CBO_EXAM = "create_cbo_exam";
        public const string CREATE_CBO_EXAM_DEFAULT = "create_cbo_exam_default";
        public const string CREATE_CBO_EXAM_GROUP = "create_cbo_exam_group";
        public const string CREATE_CBO_EDUCATION_LEVEL = "create_cbo_education_level";
        public const string CREATE_CBO_CLASS = "create_cbo_class";

        //Grid configuration
        public const int PageSize = 20;
        public const string CREATE_TOTAL= "create_total";
        public const string CREATE_PAGE = "create_page";
        public const string TOTAL = "total";
        public const string PAGE = "page";
        public const string PAGING_ENABLE = "paging_enable";
        
    }
}