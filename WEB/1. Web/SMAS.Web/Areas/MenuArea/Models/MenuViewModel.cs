using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;

namespace SMAS.Web.Areas.MenuArea.Models
{
    public class MenuViewModel
    {
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Menu_Label_MenuID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int MenuID { get; set; }

        [ResourceDisplayName("Menu_Label_Name")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string MenuName { get; set; }

        [ResourceDisplayName("Menu_Label_URL")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string URL { get; set; }

        [ResourceDisplayName("Menu_Label_ParentName")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MenuConstants.MENU_PARENTS)]
        public Nullable<int> ParentID { get; set; }

        [ResourceDisplayName("Menu_Label_ParentName")]
        public string ParentName { get; set; }

        [ResourceDisplayName("Menu_Label_OrderNumber")]
        public Nullable<int> OrderNumber { get; set; }

        [ResourceDisplayName("Menu_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Description { get; set; }

        [ResourceDisplayName("Menu_Checkbox_isAdmin")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public bool AdminOnly { get; set; }

        [ResourceDisplayName("Menu_Checkbox_Visible")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public bool IsVisible { get; set; }

        [ResourceDisplayName("Menu_Checkbox_isCategory")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public bool IsCategory { get; set; }

        [ResourceDisplayName("Menu_Label_ImageURL")]
        public string ImageURL { get; set; }

        [ResourceDisplayName("Menu_Label_BlockMenu")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MenuConstants.MENUGROUP)]
        public Nullable<int> BlockMenu { get; set; }

        [ResourceDisplayName("Menu_Checkbox_ApplyToAllChildren")]
        public bool ApplyToAllChildren { get; set; }
        //
        public List<MenuViewModel> MenuChildren { get; set; } //List cac item con
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Menu_Label_ProductVersionID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string ProductVersion { get; set; }//phien ban

        

    }
}
