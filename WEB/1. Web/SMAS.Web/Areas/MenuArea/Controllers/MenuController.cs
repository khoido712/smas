﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;

using SMAS.VTUtils.Utils;
using SMAS.Web.Filter;
using SMAS.Web.Constants;
using SMAS.Models.Models.CustomModels;
using Telerik.Web.Mvc;
using SMAS.Web;
using SMAS.Web.Areas.MenuArea;
using SMAS.Web.Areas.MenuArea.Models;
using AutoMapper;
using System.Transactions;
using SMAS.Web.Utils;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Utils;
using SMAS.VTUtils.HtmlHelpers;
namespace SMAS.Web.Areas.MenuArea.Controllers
{   
    public class MenuController : BaseController
    {
        private readonly IMenuBusiness MenuBusiness;
        private readonly IRoleMenuBusiness roleMenuBusiness;
        private readonly IRoleBusiness roleBusiness;
        private readonly IMenuPermissionBusiness MenuPermissionBusiness;
        GlobalInfo globalInfo;
        public MenuController(IMenuBusiness menuBusiness, IRoleMenuBusiness roleMenuPara,
            IRoleBusiness rolePara, 
            IMenuPermissionBusiness menuPermissionBusiness)
        {
            this.MenuBusiness = menuBusiness;
            this.roleMenuBusiness = roleMenuPara;
            this.roleBusiness = rolePara;
            this.MenuPermissionBusiness = menuPermissionBusiness;
            Mapper.CreateMap<Menu, MenuViewModel>();
            Mapper.CreateMap<MenuViewModel, Menu>();
            globalInfo = new GlobalInfo();
        }
        
        #region Index/Create/Edit/Delete     
        [HttpGet]
        public ViewResult Index()
        {
            string controllerName = (string)ControllerContext.RouteData.Values["Controller"];
            string actionName = (string)ControllerContext.RouteData.Values["Action"];
            SetViewData();
            List<ItemMenuModel> menuModelLevel1 = new List<ItemMenuModel>();
            menuModelLevel1 = GetListItemMenu();
            ViewData[MenuConstants.MENUITEMS] = menuModelLevel1;
            return View("Index");
        }


        public ActionResult _AjaxLoading(Telerik.Web.Mvc.UI.TreeViewItem node)
        {
            int id = int.Parse(node.Value);
            List<Menu> result = MenuBusiness.All.Where(m => m.IsActive == true && m.ParentID == id).ToList();
            List<Telerik.Web.Mvc.UI.TreeViewItemModel> ListMenu = result.Select(o => new Telerik.Web.Mvc.UI.TreeViewItemModel
            {
                Text = o.MenuName,
                Value = o.MenuID.ToString(),
                LoadOnDemand = true
            }).ToList();

            foreach (Telerik.Web.Mvc.UI.TreeViewItemModel m in ListMenu)
            {
                string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", m.Text);
                m.Text = string.IsNullOrEmpty(name) ? m.Text : name;
            }
            return new JsonResult { Data = ListMenu };
        }

        public PartialViewResult Create()
        {
            SetViewData();
            
            return PartialView("Create");
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MenuViewModel menuViewModel, FormCollection formCol, int[] roleCheck)
        {            
            Menu Menu = new Menu();
            Utils.Utils.BindTo(menuViewModel, Menu);
            Menu menu = Menu;
            string strParentID = formCol["ParentID"];
            int UserAccountID = globalInfo.UserAccountID;
            menu.CreatedUserID = UserAccountID;
            menu.ProductVersion = formCol["ProductVersion"];
            using (TransactionScope scope = new TransactionScope())
            {
                MenuBusiness.InsertMenu(menu, roleCheck);
                roleMenuBusiness.Save();
                MenuBusiness.Save();
                scope.Complete(); 
            }

            //Neu create success thi thiet lap lai TreeMenu
            SetTreeMenu();
            return PartialView("_TreeMenu");
           
        }


        public ActionResult DetailsMenu(string menuID)
        {
            SetViewData();
            int menuIDSelected = Convert.ToInt32(menuID);
            Menu menuSelected = MenuBusiness.Find(menuIDSelected);
            if (menuSelected.RoleMenus.Count > 0)
            {
                List<string> roleNames = menuSelected.RoleMenus.Select(m => m.Role.RoleName).ToList();
                ViewData[MenuConstants.ROLENAMES] = roleNames;
            }
            MenuViewModel MenuViewModel = new MenuViewModel();
            Utils.Utils.BindTo(menuSelected, MenuViewModel);
            MenuViewModel menu = MenuViewModel;
            //neu menu co menu cha thi lay ten menu cha
            if (menuSelected.Menu2 != null)
            {
                menu.ParentName = menuSelected.Menu2.MenuName;
            }
            List<ViettelCheckboxList> listProductVersion = new List<ViettelCheckboxList>();
            string productversionID = menuSelected.ProductVersion;
            List<int> lstversionID = !string.IsNullOrEmpty(productversionID) ? productversionID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList() : new List<int>();
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Rút gọn"),
                MenuConstants.PRODUCT_VERSION_ID_BASIC, lstversionID.Contains(MenuConstants.PRODUCT_VERSION_ID_BASIC), true));
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Chuẩn"),
                MenuConstants.PRODUCT_VERSION_ID_ADVANCE, lstversionID.Contains(MenuConstants.PRODUCT_VERSION_ID_ADVANCE), true));
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Đầy đủ"),
                MenuConstants.PRODUCT_VERSION_ID_FULL, lstversionID.Contains(MenuConstants.PRODUCT_VERSION_ID_FULL), true));
            ViewData[MenuConstants.LST_PRODUCT_VERSION] = listProductVersion;
            return PartialView("_DetailsMenu", menu);
        }

        [HttpGet]
        public PartialViewResult Edit(int menuId)
        {
            SetViewData();
            int menuIDSelected = Convert.ToInt32(menuId);
            Menu menuSelected = MenuBusiness.Find(menuIDSelected);
            List<SMAS.Web.Areas.MenuArea.Models.RoleCheck> rolesOfMenu = menuSelected.RoleMenus.Select(m => new SMAS.Web.Areas.MenuArea.Models.RoleCheck { Role = m.Role, IsChecked = true }).ToList();
            MenuViewModel MenuViewModel = new MenuViewModel();
            Utils.Utils.BindTo(menuSelected, MenuViewModel);
            MenuViewModel menu = MenuViewModel;
            List<SMAS.Web.Areas.MenuArea.Models.RoleCheck> allRoles = ViewData[MenuConstants.LS_ROLES] as List<SMAS.Web.Areas.MenuArea.Models.RoleCheck>;
            if (rolesOfMenu != null && rolesOfMenu.Count() > 0)
            {
                foreach (var item in rolesOfMenu)
                {
                    if (allRoles.Where(r => r.Role.RoleID == item.Role.RoleID).Count() > 0)
                    {
                        allRoles.Where(r => r.Role.RoleID == item.Role.RoleID).First().IsChecked = true;
                    }
                }
            }
            ViewData[MenuConstants.LS_ROLES] = allRoles;

            List<ViettelCheckboxList> listProductVersion = new List<ViettelCheckboxList>();
            string productversionID = menuSelected.ProductVersion;
            List<int> lstversionID = !string.IsNullOrEmpty(productversionID) ? productversionID.Split(new string[] {","},StringSplitOptions.RemoveEmptyEntries).Select(p=>Int32.Parse(p)).ToList() : new List<int>();
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Rút gọn"),
                MenuConstants.PRODUCT_VERSION_ID_BASIC, lstversionID.Contains(MenuConstants.PRODUCT_VERSION_ID_BASIC), false));
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Chuẩn"),
                MenuConstants.PRODUCT_VERSION_ID_ADVANCE, lstversionID.Contains(MenuConstants.PRODUCT_VERSION_ID_ADVANCE), false));
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Đầy đủ"),
                MenuConstants.PRODUCT_VERSION_ID_FULL, lstversionID.Contains(MenuConstants.PRODUCT_VERSION_ID_FULL), false));
            ViewData[MenuConstants.LST_PRODUCT_VERSION] = listProductVersion;
            return PartialView("_EditMenu",menu);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MenuViewModel menu, FormCollection formCol, int[] roleCheck)
        {
            //if (ModelState.IsValid)
            {
                string strParentID = formCol["ParentID"];
                int menuID = menu.MenuID;
                Menu menuEdit = MenuBusiness.Find(menuID);
                TryUpdateModel(menuEdit);

                menuEdit.MenuPath = RetrievePath(menuEdit);
                menuEdit.ProductVersion = formCol["ProductVersion"];
                using (TransactionScope scope = new TransactionScope())
                {
                    MenuBusiness.EditMenu(menuEdit,roleCheck);

                    if (menu.ApplyToAllChildren)
                    {
                        List<Menu> listMenu = MenuBusiness.GetAllSubMenu(menuID).ToList();
                        foreach (Menu mnu in listMenu)
                        {
                            MenuBusiness.UpdateMenuRole(mnu.MenuID, roleCheck);
                        }
                    }

                    roleMenuBusiness.Save();
                    MenuBusiness.Save();
                    scope.Complete();
                };
                //Neu Edit success thi cap nhat lai MenuTree
                SetTreeMenu();
                return PartialView("_TreeMenu");
            }    
            
            //return PartialView("_EditMenu", menu);
        }
        [HttpGet]
        public PartialViewResult Delete(int menuId)
        {
            TempData["MenuID"] = menuId;
            return PartialView("_DeleteMenu");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete()
        {
            int menuId = (int) TempData["MenuID"] ;
            using (TransactionScope scope = new TransactionScope())
            {
                this.MenuBusiness.DeleteMenu(menuId);
                MenuBusiness.Save();
                scope.Complete();
            }
            SetTreeMenu();
            return PartialView("_TreeMenu");
        }
        #endregion

        #region private function
        private void SetTreeMenu()
        {
            //SetViewData();
            List<ItemMenuModel> menuModelLevel1 = new List<ItemMenuModel>();
            menuModelLevel1 = GetListItemMenu();
            ViewData[MenuConstants.MENUITEMS] = menuModelLevel1;
        }

        private void SetViewData()
        {
            List<SelectListItem> menuGroup = new List<SelectListItem>();
            for (int i = 0; i <= 9; i++)
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Nhóm " + i;
                item.Value = i.ToString();
                menuGroup.Add(item);
            }
            ViewData[MenuConstants.MENUGROUP] = menuGroup;
            var listMenu = MenuBusiness.All.Where(m => m.IsActive == true).ToList().ToList().Select(u=>new {
                MenuID=u.MenuID,
                MenuName = string.IsNullOrEmpty((string)HttpContext.GetGlobalResourceObject("MenuResources", u.MenuName)) ? u.MenuName : (string)HttpContext.GetGlobalResourceObject("MenuResources", u.MenuName)
            }).OrderBy(o => o.MenuName);


            ViewData[MenuConstants.MENU_PARENTS] = new SelectList(listMenu, "MenuID", "MenuName");
           
            List<Role> lsRoles = new List<Role>();
            List<SMAS.Web.Areas.MenuArea.Models.RoleCheck> lsRoleChecks = new List<SMAS.Web.Areas.MenuArea.Models.RoleCheck>();

            lsRoles = roleBusiness.GetAll().ToList();
            foreach (var rItem in lsRoles)
            {
                SMAS.Web.Areas.MenuArea.Models.RoleCheck rc = new SMAS.Web.Areas.MenuArea.Models.RoleCheck { Role = rItem, IsChecked = false };
                lsRoleChecks.Add(rc);
            }
            

            ViewData[MenuConstants.LS_ROLES] = lsRoleChecks;

            List<ViettelCheckboxList> listProductVersion = new List<ViettelCheckboxList>();
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Rút gọn"), MenuConstants.PRODUCT_VERSION_ID_BASIC, false, false));
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Chuẩn"), MenuConstants.PRODUCT_VERSION_ID_ADVANCE, false, false));
            listProductVersion.Add(new ViettelCheckboxList(Res.Get("Đầy đủ"), MenuConstants.PRODUCT_VERSION_ID_FULL, false, false));
            ViewData[MenuConstants.LST_PRODUCT_VERSION] = listProductVersion;
        }
        
        private void SetViewData(int id)
        {
            var listMenu = MenuBusiness.All.Where(m => m.IsActive == true).ToList().ToList().Select(u => new
            {
                MenuID = u.MenuID,
                MenuName = string.IsNullOrEmpty((string)HttpContext.GetGlobalResourceObject("MenuResources", u.MenuName)) ? u.MenuName : (string)HttpContext.GetGlobalResourceObject("MenuResources", u.MenuName)
            }).OrderBy(o => o.MenuName);
            ViewData[MenuConstants.MENU_PARENTS] = new SelectList(listMenu, "MenuID", "MenuName");
        }

        /// <summary>
        /// Lay danh sach Menu theo role va build danh sach menu theo cap
        /// </summary>
        /// <returns></returns>
        private List<ItemMenuModel> GetListItemMenu()
        {
            int RoleId = globalInfo.RoleID;
            //lay menu theo Role
            //IQueryable<Menu> result = roleMenuBusiness.GetMenuOfRole(RoleId);
            //SuperAdmin co quyen thao tac tat ca menu
            IQueryable<Menu> result = MenuBusiness.All.Where(m => m.IsActive == true);
            if (result != null)
            {
                //sap xep danh sach Menu theo thu tu orderNumber
                List<Menu> lsMenu = result.OrderBy( m => m.OrderNumber).ToList();
                //build danh sach Menu
                MenuBuilder menuBuilder = new MenuBuilder(lsMenu);
                //Lay ra menu cap 1
                List<ItemMenuModel> menuModelLevel1 = menuBuilder.GetParentMenu();
                return menuModelLevel1;
            }
            else return null;            
        }

        private string RetrievePath(Menu menu) 
        {
            if (menu == null) return string.Empty;

            string path = "/";

            int? parentId = menu.ParentID;
            while (parentId.HasValue && parentId.Value > 0)
            {
                Menu parentMenu = MenuBusiness.Find(parentId);
                if (parentMenu != null)
                {
                    path = "/" + parentMenu.MenuID + path;
                    parentId = parentMenu.ParentID;
                }
            }
            return path;
        }

        #endregion
    }
}
