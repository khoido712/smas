﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MenuArea
{
    public class MenuConstants
    {
        public const string ROLENAMES = "role names of menu";

        public const string MENUITEMS = "menu items";

        public const string MENUPERMISSION = "menu permission";

        public const string MENU_PARENTS = "menu parents";

        public const string LS_ROLES = "list roles";

        public const string MENUGROUP = "menugroup";
        public const string LST_PRODUCT_VERSION = "LST_PRODUCT_VERSION";
        public const int PRODUCT_VERSION_ID_BASIC = 1;//phien ban rut gon
        public const int PRODUCT_VERSION_ID_ADVANCE = 2;//phien ban chuan
        public const int PRODUCT_VERSION_ID_FULL = 3;//phien ban day du

    }
}