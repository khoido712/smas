using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PhysicalTestArea.Models
{
    public class OverallTestViewModel
    {
        public System.Int32 MonitoringBookID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }

         
        [ResourceDisplayName("Common_Column_DateTest")]
        public System.DateTime? DateTest { get; set; }
        public string MonitoringDateStr { get; set; }
        [ResourceDisplayName("Common_Column_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("Common_Column_Class")]
        public string DisplayName { get; set; }
        [ResourceDisplayName("Common_Column_OverallInternal")]
        public string OverallInternal { get; set; }
        [ResourceDisplayName("Common_Column_OverallForeign")]
        public string OverallForeign { get; set; }
        [ResourceDisplayName("Common_Column_SkinDiseases")]
        public string SkinDiseases { get; set; }
        [ResourceDisplayName("Common_Column_IsHeartDiseases")]
        public string IsHeartDiseases { get; set; }
        [ResourceDisplayName("Common_Column_IsBirthDefect")]
        public string IsBirthDefect { get; set; }
        [ResourceDisplayName("Common_Column_Other")]
        public string Other { get; set; }
        [ResourceDisplayName("Common_Column_EvaluationHealth")]
        public string EvaluationHealth { get; set; }
        [ResourceDisplayName("Common_Column_RequrieOfDoctor")]
        public string RequrieOfDoctor { get; set; }
        public System.DateTime BirthDate { get; set; }
        public int Gende { get; set; }
    }
}
