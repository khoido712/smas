﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PhysicalTestArea.Models
{
    public class DentalTestViewModel
    {
        public int DentalTestID { get; set; }
        [ResourceDisplayName("Common_Column_DateTest")]
        public DateTime? DateTest { get; set; }
        public string MonitoringDateStr { get; set; }
        [ResourceDisplayName("Common_Column_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("Common_Column_Class")]
        public string DisplayName { get; set; }
        public int MonitoringBookID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }


        [ResourceDisplayName("Common_Column_NumDentalFilling")]
        public int? NumDentalFilling { get; set; }
        [ResourceDisplayName("Common_Column_NumPreventiveDentalFilling")]
        public int? NumPreventiveDentalFilling { get; set; }
        [ResourceDisplayName("Common_Column_NumTeethExtracted")]
        public int? NumTeethExtracted { get; set; }
        [ResourceDisplayName("Common_Column_IsScraptTeeth")]
        public string IsScraptTeeth { get; set; }
        [ResourceDisplayName("Common_Column_IsTreatmentTooth")]
        public string IsTreatmentTooth { get; set; }
        [ResourceDisplayName("Common_Column_OtherToothdiseases")]
        public string OtherToothdiseases { get; set; }
        
    }
}