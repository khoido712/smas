﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PhysicalTestArea.Models
{
    public class SpineTestViewModel
    {
        public int MonitoringBookID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SpineTestID { get; set; }
        [ResourceDisplayName("Common_Column_DateTest")]
        public DateTime? DateTest { get; set; }
        public string MonitoringDateStr { get; set; }
        [ResourceDisplayName("Common_Column_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("Common_Column_Class")]
        public string DisplayName { get; set; }
        [ResourceDisplayName("Common_Column_ExaminationStraight")]
        public string ExaminationStraight { get; set; }
        [ResourceDisplayName("Common_Column_ExaminationItalic")]
        public string ExaminationItalic { get; set; }
        [ResourceDisplayName("Common_Column_IsDeformityOfTheSpine")]
        public string IsDeformityOfTheSpine { get; set; }
        [ResourceDisplayName("Common_Column_OtherSpineDiseases")]
        public string OtherSpineDiseases { get; set; }
        
    }
}