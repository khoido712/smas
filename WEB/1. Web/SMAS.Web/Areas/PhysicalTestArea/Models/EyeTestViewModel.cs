﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PhysicalTestArea.Models
{
    public class EyeTestViewModel
    {

        public int MonitoringBookID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EyeTestID { get; set; }
         [ResourceDisplayName("Common_Column_DateTest")]
        public DateTime? DateTest { get; set; }
         public string MonitoringDateStr { get; set; }
         [ResourceDisplayName("Common_Column_FullName")]
         public string FullName { get; set; }
         [ResourceDisplayName("Common_Column_Class")]
         public string DisplayName { get; set; }

         [ResourceDisplayName("Common_Column_IsTreatment")]
         public string IsTreatment { get; set; }
         public string RIsTreatment { get; set; }
         public string LIsTreatment { get; set; }


         [ResourceDisplayName("Common_Column_NotTreatment")]
         public string NotTreatment { get; set; }
         public string RNotTreatment { get; set; }
         public string LNotTreatment { get; set; }
         [ResourceDisplayName("Common_Column_Myopic")]
         public string Myopic { get; set; }
         public string RMyopic { get; set; }
         public string LMyopic { get; set; }
         [ResourceDisplayName("Common_Column_Farsightedness")]
         public string Farsightedness { get; set; }
         public string RFarsightedness { get; set; }
         public string LFarsightedness { get; set; }
         [ResourceDisplayName("Common_Column_Astigmatism")]
         public string Astigmatism { get; set; }
         public string RAstigmatism { get; set; }
         public string LAstigmatism { get; set; }
         [ResourceDisplayName("Common_Column_OtherEyeDiseases")]
         public string OtherEyeDiseases { get; set; }
    }
}