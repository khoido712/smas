/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PhysicalTestArea.Models
{
    public class SearchViewModel
    {
        public int? EducationLevelID { get; set; }
        public int? ClassID { get; set; }
        public int? PeriodExaminationID { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }

        public int? ItemExaminationID { get; set; }
        public int? PhysicalClassificationID { get; set; }
        public int? NutritionID { get; set; }
        [ResourceDisplayName("Common_Column_HeightMin")]
         [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string HeightMin { get; set; }
        [ResourceDisplayName("Common_Column_HeightMax")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string HeightMax { get; set; }
        [ResourceDisplayName("Common_Column_WeightMin")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string WeightMin { get; set; }
        [ResourceDisplayName("Common_Column_WeightMax")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string WeightMax { get; set; }

        #region OverallTest
        public int? EvaluationHealth { get; set; }
        [ResourceDisplayName("OverallTest_Label_SkinDiseases")]
        public bool SkinDiseases { get; set; }
        [ResourceDisplayName("OverallTest_Label_IsHeartDiseases")]
        public bool IsHearDiseases { get; set; }
        [ResourceDisplayName("OverallTest_Label_IsBirthDefect")]
        public bool IsBirthDefect { get; set; }
        #endregion

        #region EyeTest
        [ResourceDisplayName("EyeTest_Label_OtherEyeDiseases")]
        public bool OtherEyeDiseases { get; set; }
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string EyesightLeft { get; set; }
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string EyesightRight { get; set; }
        public int? IsTreatment { get; set; }
        public int? DisabledRefractive { get; set; }
        #endregion

        #region DentalTest
        [ResourceDisplayName("DentalTest_Label_OtherToothDiseases")]
        public bool OtherToothDiseases { get; set; }
        [ResourceDisplayName("DentalTest_Label_IsScraptTeeth")]
        public bool IsScraptTeeth { get; set; }
        [ResourceDisplayName("DentalTest_Label_ToothDecay")]
        public bool ToothDecay { get; set; }
        [ResourceDisplayName("DentalTest_Label_IsTreatment")]
        public bool IsTreatmentTooth { get; set; }
        #endregion


        #region EarNose
        public bool IsSick { get; set; }
        public bool IsDeaf { get; set; }
        #endregion

        [HiddenInput]
        public string HfDisabledRefractive { get; set; }

    }
}