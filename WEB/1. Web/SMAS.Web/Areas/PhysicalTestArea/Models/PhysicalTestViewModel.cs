/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.PhysicalTestArea.Models
{
    public class PhysicalTestViewModel
    {
		public System.Int32 PhysicalTestID { get; set; }								
		public System.Int32 MonitoringBookID { get; set; }								
																	
		public System.Nullable<System.DateTime> CreatedDate { get; set; }								
		public System.Boolean IsActive { get; set; }								
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }								



        #region Grid
        [ResourceDisplayName("Common_Column_DateTest")]
        public System.DateTime Date { get; set; }
        [ResourceDisplayName("Common_Column_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("Common_Column_Class")]
        public string DisplayName { get; set; }
        [ResourceDisplayName("Common_Column_Height")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Height { get; set; }
        [ResourceDisplayName("Common_Column_Weight")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Weight { get; set; }
        [ResourceDisplayName("Common_Column_Breast")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Breast { get; set; }
        [ResourceDisplayName("Common_Column_PhysicalClassification")]
        public string PhysicalClassification { get; set; }
        [ResourceDisplayName("Common_Column_Nutrition")]
        public string Nutrition { get; set; }
        [ScaffoldColumn(false)]
        public string Name { get; set; }

        #endregion

        #region Tai Mui Hong
        public int PupilID { get; set; }
        public int ENTTestID { get; set; }
        public string Description {get;set;}
        public bool? IsDeaf { get; set; }
        public bool? IsSick { get; set; }
        public int? LCapacityHearing { get; set; }
        public int? RCapacityHearing { get; set; }
        public int ClassID { get; set; }
        public bool chkSick {get;set;}
        [ResourceDisplayName("Common_Column_MonitoringDate")]
        public DateTime? MonitoringDate { get; set; }
        public string MonitoringDateStr { get; set; }
        #endregion

    }
}


