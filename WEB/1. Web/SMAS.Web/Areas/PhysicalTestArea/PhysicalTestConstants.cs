﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PhysicalTestArea
{
    public class PhysicalTestConstants
    {
        public const string LIST_PHYSICALTEST = "listPhysicalTest";
        public const string CBO_EDUCATIONLEVEL = "CboEducationLevel";
        public const string CBO_CLASS = "CboClass";
        public const string CBO_PERIODEXAMINATION = "CboPeriodExamination";
        public const string CBO_ITEMEXAMINATION = "CboItemExamination";
        public const string CBO_PHYSICALCLASSIFICATION = "CboPhysicalClassification";
        public const string CBO_NUTRITION = "CboNutrition";
        public const string CBO_ISTREATMENT = "CboIsTreatment";
        public const string CBO_DISABLEDREFRACTIVE = "CboDisabledRefractive";
        public const string LIST_EYETEST = "ListEyeTest";
        public const string LIST_DENTALTEST = "ListDentalTest";
        public const string CBO_EVALUATIONHEALTH = "CboEvaluationHealth";
        public const string LIST_EARNOSE = "listEarNose";
        public const string LIST_SPINE = "listSpine";
        public const int PAGE_SIZE = 20;
        public const string TOTALRECORD = "totalrecord";
        public const string LIST_OVERTALL = "OverAll";
        public const string SEARCH_OBJ = "SearchObject";
        public const string REPORT_NAME = "HS_{0}_{1}_{2}_{3}.xls";
        public const string ALL_SCHOOL = "ToanTruong";
        public const string REPORT_SEIGNEUR = "ThongKe_TheLuc";
        public const string REPORT_EARNOSE = "ThongKe_TaiMuiHong";
        public const string REPORT_OVERALLTEST = "ThongKe_TongQuat";
        public const string REPORT_SPINE = "ThongKe_CotSong";
        public const string REPORT_DENTAL = "ThongKe_RangHamMat";
        public const string REPORT_EYE = "ThongKe_Mat";
        
        public const string PERIODEXAMINATIONNAME_I = "Dot_1";
        public const string PERIODEXAMINATIONNAME_II = "Dot_2";

        public const string CHECK_PERMISSION_VIEW_HEALTH_TEST = "CheckPermissionView";

    }
}