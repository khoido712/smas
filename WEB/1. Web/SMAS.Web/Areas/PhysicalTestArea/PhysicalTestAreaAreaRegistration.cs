﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PhysicalTestArea
{
    public class PhysicalTestAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PhysicalTestArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PhysicalTestArea_default",
                "PhysicalTestArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
