﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PhysicalTestArea.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using Telerik.Web.Mvc;
using System.Globalization;

namespace SMAS.Web.Areas.PhysicalTestArea.Controllers
{
    public class PhysicalTestController : BaseController
    {
        private readonly IPhysicalTestBusiness PhysicalTestBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IEyeTestBusiness EyeTestBusiness;
        private readonly IDentalTestBusiness DentalTestBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IENTTestBusiness ENTTestBusiness;
        private readonly IOverallTestBusiness OverallTestBusiness;
        private readonly ISpineTestBusiness SpineTestBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        public PhysicalTestController(ISpineTestBusiness SpineTestBusiness, IEyeTestBusiness EyeTestBusiness, IDentalTestBusiness DentalTestBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IEducationLevelBusiness EducationLevelBusiness, ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness,
            IOverallTestBusiness OverallTestBusiness, IClassProfileBusiness ClassProfileBusiness, IPhysicalTestBusiness PhysicalTestBusiness
            , IENTTestBusiness ENTTestBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IMonitoringBookBusiness MonitoringBookBusiness)
        {
            this.SpineTestBusiness = SpineTestBusiness;
            this.DentalTestBusiness = DentalTestBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PhysicalTestBusiness = PhysicalTestBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.EyeTestBusiness = EyeTestBusiness;
            this.OverallTestBusiness = OverallTestBusiness;
            this.ENTTestBusiness = ENTTestBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.MonitoringBookBusiness = MonitoringBookBusiness;
        }

        private static int TotalRecord = 0;
        private GlobalInfo Global = new GlobalInfo();


        //
        // GET: /PhysicalTest/
        public ActionResult Index()
        {
            if (Global.HasSubjectTeacherPermission(0, 0) == false)
            {
                throw new BusinessException("Common_Validate_User");
            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SetViewData();
            //Get view data here


            return View();
        }
        //
        // GET: /PhysicalTest/Search

        // [SkipCheckRole]
        [HttpPost]
        //[ValidateInput(true)]
        public PartialViewResult Search(SearchViewModel frm)
        {
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            Session["IsSick"] = null;
            Session["IsDeaf"] = null;
            int EducationGrade = _globalInfo.AppliedLevel.Value;
            int PageNum = 1;
            ViewData["OverallTest"] = new List<OverallTestViewModel>();
            Utils.Utils.TrimObject(frm);
            if (frm.EducationLevelID == null)
            {
                frm.EducationLevelID = 0;
            }
            if (frm.ClassID == null)
            {
                frm.ClassID = 0;
            }
            if (frm.PeriodExaminationID == null)
            {
                frm.PeriodExaminationID = 0;
            }
            //Check permission
            ViewData[PhysicalTestConstants.CHECK_PERMISSION_VIEW_HEALTH_TEST] = CheckActionPerMinViewByController("HealthTest");

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = frm.EducationLevelID;
            Dictionary["ClassID"] = frm.ClassID;
            Dictionary["FullName"] = frm.FullName;
            Dictionary["HealthPeriodID"] = frm.PeriodExaminationID;
            Dictionary["EducationGrade"] = EducationGrade;
            decimal HeightMin = 0;
            decimal HeightMax = 0;           
            int temp = 0;
            #region Thong ke

            #region The luc
            if (frm.ItemExaminationID == 1)
            {

                if (frm.PhysicalClassificationID == null)
                {
                    frm.PhysicalClassificationID = 0;
                }
                if (frm.NutritionID == null)
                {
                    frm.NutritionID = 0;
                }
                if (string.IsNullOrEmpty(frm.HeightMin))
                {
                    HeightMin = 0;
                }
                else
                {
                    decimal.TryParse(frm.HeightMin.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture,out HeightMin);
                }

                if (string.IsNullOrEmpty(frm.HeightMax))
                {
                    HeightMax = 0;
                }
                else
                {
                    decimal.TryParse(frm.HeightMax.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out HeightMax);
                }
                decimal WeightMin = 0;
                decimal WeightMax = 0;
                if (string.IsNullOrEmpty(frm.WeightMin))
                {
                    WeightMin = 0;
                }
                else
                {
                    decimal.TryParse(frm.WeightMin.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out WeightMin);
                }

                if (string.IsNullOrEmpty(frm.WeightMax))
                {
                    WeightMax = 0;
                }
                else
                {
                    decimal.TryParse(frm.WeightMax.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out WeightMax);
                }

                List<PhysicalTest> query = this.PhysicalTestBusiness.GetListPhysicalTest(Global.SchoolID.Value, Global.AcademicYearID.Value, frm.EducationLevelID.Value,
                frm.ClassID.Value, frm.FullName, frm.PeriodExaminationID.Value, frm.PhysicalClassificationID.Value, frm.NutritionID.Value, HeightMin, HeightMax,
                WeightMin, WeightMax, EducationGrade, PhysicalTestConstants.PAGE_SIZE, PageNum, ref TotalRecord, _globalInfo.Semester.Value);
                if (query.Count == 0)
                {
                    ViewData[PhysicalTestConstants.LIST_PHYSICALTEST] = new List<PhysicalTestViewModel>();
                    return PartialView("_ListPhysical");
                }
                List<PhysicalTestViewModel> lst = query.Select(o => new PhysicalTestViewModel
                {
                    PupilID = o.MonitoringBook.PupilID,
                    ClassID = o.MonitoringBook.ClassID,
                    MonitoringBookID = o.MonitoringBookID,
                    PhysicalTestID = o.PhysicalTestID,
                    Date = o.Date,
                    MonitoringDate = o.MonitoringBook.MonitoringDate,
                    MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                    FullName = o.MonitoringBook.PupilProfile.FullName,
                    Name = o.MonitoringBook.PupilProfile.Name,
                    DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                    Height = o.Height.HasValue ? o.Height.Value.ToString("0.#", CultureInfo.InvariantCulture) : "",
                    Weight = o.Weight.HasValue ? o.Weight.Value.ToString("0.#", CultureInfo.InvariantCulture) : "",
                    Breast = o.Breast.HasValue ? o.Breast.Value.ToString("0.#", CultureInfo.InvariantCulture) : "",
                    PhysicalClassification = o.PhysicalClassification.HasValue ? CommonConvert.PhysicalClassification(o.PhysicalClassification.Value) : "",
                    Nutrition = CommonConvert.Nutrition(o.Nutrition)
                }).ToList();
                ViewData[PhysicalTestConstants.LIST_PHYSICALTEST] = lst;
                ViewData[PhysicalTestConstants.TOTALRECORD] = TotalRecord;
                //Get view data here

                return PartialView("_ListPhysical");
            }
            #endregion

            #region Kham mat
            if (frm.ItemExaminationID == 2)
            {
                if (frm.IsTreatment == 1)
                {
                    Dictionary["IsTreatment"] = true;
                }
                if (frm.IsTreatment == 0)
                {
                    Dictionary["IsTreatment"] = false;
                }
                frm.OtherEyeDiseases = false;
                if (Convert.ToBoolean(Request["OtherEyeDiseases"]) == true)
                {
                    frm.OtherEyeDiseases = true;
                }
                if (!string.IsNullOrEmpty(frm.EyesightLeft) || int.TryParse(frm.EyesightLeft, out temp))
                {
                    Dictionary["Leye"] = Convert.ToInt32(frm.EyesightLeft);
                }
                if (!string.IsNullOrEmpty(frm.EyesightRight) || int.TryParse(frm.EyesightRight, out temp))
                {
                    Dictionary["Reye"] = Convert.ToInt32(frm.EyesightRight);
                }

                Dictionary["Other"] = false;

                if (frm.HfDisabledRefractive != null && frm.HfDisabledRefractive != "")
                {
                    Dictionary["HfDisabledRefractive"] = frm.HfDisabledRefractive.ToString();
                }
                else
                {
                    if (frm.DisabledRefractive != null)
                    {
                        Dictionary["HfDisabledRefractive"] = frm.DisabledRefractive.ToString();
                    }
                }
                //  IQueryable<EyeTest> lsEyeTest = EyeTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["ClassID"] = frm.ClassID;
                IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
                dic["EducationLevelID"] = frm.EducationLevelID;
                dic["HealthPeriodID"] = frm.PeriodExaminationID;
                IQueryable<MonitoringBook> LstMB = MonitoringBookBusiness.Search(dic);
                IQueryable<EyeTest> lsEyeTest = from a in EyeTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                join c in LstMB on a.MonitoringBookID equals c.MonitoringBookID
                                                join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                select a;

                List<EyeTest> lstEyeTest = new List<EyeTest>();
                List<EyeTestViewModel> lst = new List<EyeTestViewModel>();
                if (frm.EducationLevelID != 0)
                {
                    lsEyeTest = lsEyeTest.Where(p => p.MonitoringBook.EducationLevelID == frm.EducationLevelID);
                }

                ViewData[PhysicalTestConstants.TOTALRECORD] = lsEyeTest.Count();
                lstEyeTest = lsEyeTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).ThenBy(p => p.MonitoringBook.PupilProfile.FullName).Skip((PageNum - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
                foreach (EyeTest et in lstEyeTest)
                {
                    EyeTestViewModel evm = new EyeTestViewModel();
                    evm.PupilID = et.MonitoringBook.PupilID;
                    evm.ClassID = et.MonitoringBook.ClassID;
                    evm.MonitoringBookID = et.MonitoringBookID;
                    evm.EyeTestID = et.EyeTestID;
                    evm.DateTest = et.MonitoringBook.MonitoringDate;
                    evm.MonitoringDateStr = et.MonitoringBook.MonitoringDate.HasValue ? et.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "";
                    evm.FullName = et.MonitoringBook.PupilProfile.FullName;
                    evm.DisplayName = et.MonitoringBook.ClassProfile.DisplayName;
                    evm.RIsTreatment = "";
                    evm.LIsTreatment = "";
                    evm.RNotTreatment = "";
                    evm.LNotTreatment = "";
                    evm.RMyopic = "";
                    evm.LMyopic = "";
                    evm.RFarsightedness = "";
                    evm.LFarsightedness = "";
                    evm.RAstigmatism = "";
                    evm.LAstigmatism = "";

                    evm.RIsTreatment = et.RIsEye.HasValue ? et.RIsEye.ToString() + "/10" : "__/10";
                    evm.LIsTreatment = et.LIsEye.HasValue ? et.LIsEye.ToString() + "/10" : "__/10";
                    evm.RNotTreatment = et.REye.HasValue ? et.REye.ToString() + "/10" : "__/10";
                    evm.LNotTreatment = et.LEye.HasValue ? et.LEye.ToString() + "/10" : "__/10";
                   
                    if (et.IsMyopic.HasValue && et.IsMyopic.Value == true)
                    {
                        evm.RMyopic = et.RMyopic.HasValue ? et.RMyopic.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                        evm.LMyopic = et.LMyopic.HasValue ? et.LMyopic.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                    }
                    if (et.IsFarsightedness.HasValue && et.IsFarsightedness.Value == true)
                    {
                        evm.RFarsightedness = et.RFarsightedness.HasValue ? et.RFarsightedness.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                        evm.LFarsightedness = et.LFarsightedness.HasValue ? et.LFarsightedness.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                    }
                    if (et.IsFarsightedness.HasValue && et.IsFarsightedness.Value == true)
                    {
                        evm.RFarsightedness = et.RFarsightedness.HasValue ? et.RFarsightedness.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                        evm.LFarsightedness = et.LFarsightedness.HasValue ? et.LFarsightedness.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                    }
                    if (et.IsAstigmatism.HasValue && et.IsAstigmatism.Value == true)
                    {
                        evm.RAstigmatism = et.RAstigmatism.HasValue ? et.RAstigmatism.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                        evm.LAstigmatism = et.LAstigmatism.HasValue ? et.LAstigmatism.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                    }
                    evm.OtherEyeDiseases = et.Other;
                    lst.Add(evm);
                }

                ViewData[PhysicalTestConstants.LIST_EYETEST] = lst;
                return PartialView("_ListEye");
            }
            #endregion

            #region Rang ham mat
            if (frm.ItemExaminationID == 3)
            {
                bool OtherToothDiseases = false;
                bool ToothDecay = false;
                bool IsScraptTeeth = false;
                bool IsTreatmentTooth = false;
                if (Request["OtherToothDiseases"] == "")
                {
                    OtherToothDiseases = true;
                }
                if (Request["ToothDecay"] == "")
                {
                    ToothDecay = true;
                }
                if (Request["IsScraptTeeth"] == "")
                {
                    IsScraptTeeth = true;
                }
                if (Request["IsTreatmentTooth"] == "")
                {
                    IsTreatmentTooth = true;

                }
                Dictionary["Other"] = OtherToothDiseases;
                Dictionary["ToothDecay"] = ToothDecay;
                Dictionary["IsScraptTeeth"] = IsScraptTeeth;
                Dictionary["IsTreatment"] = IsTreatmentTooth;
                // IQueryable<DentalTest> lsDentalTest = DentalTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["ClassID"] = frm.ClassID;
                IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
                dic["EducationLevelID"] = frm.EducationLevelID;
                dic["HealthPeriodID"] = frm.PeriodExaminationID;
                IQueryable<MonitoringBook> LstMB = MonitoringBookBusiness.Search(dic);
                IQueryable<DentalTest> lsDentalTest = from a in DentalTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                      join c in LstMB on a.MonitoringBookID equals c.MonitoringBookID
                                                      join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                      select a;

                ViewData[PhysicalTestConstants.TOTALRECORD] = lsDentalTest.Count();
                List<DentalTest> lstDentalTest = lsDentalTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((PageNum - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();

                List<DentalTestViewModel> lst = lstDentalTest.Select(o => new DentalTestViewModel
                {
                    PupilID = o.MonitoringBook.PupilID,
                    ClassID = o.MonitoringBook.ClassID,
                    MonitoringBookID = o.MonitoringBookID,
                    DentalTestID = o.DentalTestID,
                    DateTest = o.MonitoringBook.MonitoringDate,
                    MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                    FullName = o.MonitoringBook.PupilProfile.FullName,
                    DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                    NumDentalFilling = o.NumDentalFilling,
                    NumTeethExtracted = o.NumTeethExtracted,
                    NumPreventiveDentalFilling = o.NumPreventiveDentalFilling,
                    IsScraptTeeth = Utils.CommonConvert.Tox(o.IsScraptTeeth),
                    IsTreatmentTooth = Utils.CommonConvert.Tox(o.IsTreatment),
                    OtherToothdiseases = o.Other
                }).ToList();

                ViewData[PhysicalTestConstants.LIST_DENTALTEST] = lst;

                return PartialView("_ListDental");
            }
            #endregion

            #region Cot song
            if (frm.ItemExaminationID == 5)
            {
                bool IsDeformityOfTheSpine = false;
                bool OtherSpine = false;
                if (Request["IsDeformityOfTheSpine"] == "")
                {
                    IsDeformityOfTheSpine = true;
                }
                if (Request["OtherSpine"] == "")
                {
                    OtherSpine = true;
                }
                Dictionary["IsDeformityOfTheSpine"] = IsDeformityOfTheSpine;
                Dictionary["Other"] = OtherSpine;

                //  IQueryable<SpineTest> lsSpineTest = SpineTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["ClassID"] = frm.ClassID;
                IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
                dic["EducationLevelID"] = frm.EducationLevelID;
                dic["HealthPeriodID"] = frm.PeriodExaminationID;
                IQueryable<MonitoringBook> LstMB = MonitoringBookBusiness.Search(dic); 
                IQueryable<SpineTest> lsSpineTest = from a in SpineTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                    join c in LstMB on a.MonitoringBookID equals c.MonitoringBookID
                                                    join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                    select a;

                ViewData[PhysicalTestConstants.TOTALRECORD] = lsSpineTest.Count();
                List<SpineTest> lstSpineTest = lsSpineTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((PageNum - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
                List<SpineTestViewModel> lst = lstSpineTest.Select(o => new SpineTestViewModel
                {
                    PupilID = o.MonitoringBook.PupilID,
                    ClassID = o.MonitoringBook.ClassID,
                    MonitoringBookID = o.MonitoringBookID,
                    SpineTestID = o.SpineTestID,
                    DateTest = o.MonitoringBook.MonitoringDate,
                    MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                    FullName = o.MonitoringBook.PupilProfile.FullName,
                    DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                    ExaminationItalic = o.ExaminationItalic,
                    ExaminationStraight = o.ExaminationStraight,
                    IsDeformityOfTheSpine = CommonConvert.Tox(o.IsDeformityOfTheSpine),
                    OtherSpineDiseases = o.Other
                }).ToList();

                ViewData[PhysicalTestConstants.LIST_SPINE] = lst;
                return PartialView("_ListSpine");
            }
            #endregion

            #region Tai Mui Hong
            if (frm.ItemExaminationID == 4)
            {
                if (frm.EducationLevelID == null)
                {
                    frm.EducationLevelID = 0;
                }
                if (frm.ClassID == null)
                {
                    frm.ClassID = 0;
                }
                if (frm.PeriodExaminationID == null)
                {
                    frm.PeriodExaminationID = 0;
                }

                Dictionary["IsSick"] = frm.IsSick;
                Dictionary["IsDeaf"] = frm.IsDeaf;
                Session["IsSick"] = frm.IsSick;
                Session["IsDeaf"] = frm.IsDeaf;

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["ClassID"] = frm.ClassID;
                IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
                dic["EducationLevelID"] = frm.EducationLevelID;
                dic["HealthPeriodID"] = frm.PeriodExaminationID;
                IQueryable<MonitoringBook> LstMB = MonitoringBookBusiness.Search(dic); 
                IQueryable<ENTTest> IQueEarNose = from a in ENTTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                  join c in LstMB on a.MonitoringBookID equals c.MonitoringBookID
                                                  join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                  select a;

                ViewData[PhysicalTestConstants.TOTALRECORD] = IQueEarNose.Count();
                List<ENTTest> query = IQueEarNose.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((PageNum - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
                if (query.Count == 0)
                {
                    ViewData[PhysicalTestConstants.LIST_EARNOSE] = new List<PhysicalTestViewModel>();
                }
                else
                {
                    bool chkSick = false;
                    if (frm.IsSick)
                    {
                        chkSick = true;
                    }
                    List<PhysicalTestViewModel> lst = query.Select(o => new PhysicalTestViewModel
                    {
                        ENTTestID = o.ENTTestID,
                        DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                        CreatedDate = o.CreatedDate,
                        Description = o.Description,
                        IsActive = o.IsActive,
                        IsDeaf = o.IsDeaf,
                        IsSick = o.IsSick,
                        LCapacityHearing = o.LCapacityHearing,
                        RCapacityHearing = o.RCapacityHearing,
                        ModifiedDate = o.ModifiedDate,
                        FullName = o.MonitoringBook.PupilProfile.FullName,
                        ClassID = o.MonitoringBook.ClassID,
                        MonitoringDate = o.MonitoringBook.MonitoringDate,
                        MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                        chkSick = chkSick,
                        PupilID = o.MonitoringBook.PupilID,
                        MonitoringBookID = o.MonitoringBookID
                    }).ToList();

                    ViewData[PhysicalTestConstants.LIST_EARNOSE] = lst;
                }
                return PartialView("_ListEarNose");
            }
            #endregion

            #region Tong quat
            if (frm.ItemExaminationID == 6)
            {
                Utils.Utils.TrimObject(frm);
                Dictionary["EvaluationHealth"] = frm.EvaluationHealth;
                Dictionary["SkinDiseases"] = frm.SkinDiseases == true ? 1 : 2;
                Dictionary["IsHeartDiseases"] = frm.IsHearDiseases;
                Dictionary["IsBirthDefect"] = frm.IsBirthDefect;
                Dictionary["IsDredging"] = false;
                Dictionary["IsActive"] = true;
                // IQueryable<OverallTest> query = OverallTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary);
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["ClassID"] = frm.ClassID;
                IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
                dic["EducationLevelID"] = frm.EducationLevelID;
                dic["HealthPeriodID"] = frm.PeriodExaminationID;
                IQueryable<MonitoringBook> LstMB = MonitoringBookBusiness.Search(dic); 
                IQueryable<OverallTest> query = from a in OverallTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                join c in LstMB on a.MonitoringBookID equals c.MonitoringBookID
                                                join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                select a;

                ViewData[PhysicalTestConstants.TOTALRECORD] = query.Count();
                List<OverallTest> list = query.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((PageNum - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
                List<OverallTestViewModel> lstOverallTest = list.Select(o => new OverallTestViewModel
                {
                    PupilID = o.MonitoringBook.PupilID,
                    ClassID = o.MonitoringBook.ClassID,
                    MonitoringBookID = o.MonitoringBookID,
                    DateTest = o.MonitoringBook.MonitoringDate,
                    MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                    DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                    EvaluationHealth = o.EvaluationHealth == 1 ? "Loại A" : o.EvaluationHealth == 2 ? "Loại B" : o.EvaluationHealth == 3 ? "Loại C" : "",
                    FullName = o.MonitoringBook.PupilProfile.FullName,
                    Gende = o.MonitoringBook.PupilProfile.Genre,
                    IsBirthDefect = o.IsBirthDefect == true ? "x" : null,
                    IsHeartDiseases = o.IsHeartDiseases == true ? "x" : null,
                    Other = o.Other,
                    OverallForeign = o.OverallForeign == 1 ? o.DescriptionForeign : "Bình thường",
                    OverallInternal = o.OverallInternal == 1 ? o.DescriptionOverallInternall : "Bình thường",
                    RequrieOfDoctor = o.RequireOfDoctor,
                    SkinDiseases = o.SkinDiseases == 1 ? o.DescriptionSkinDiseases : "Bình thường",
                    BirthDate = o.MonitoringBook.PupilProfile.BirthDate
                }).ToList();

                ViewData[PhysicalTestConstants.LIST_OVERTALL] = lstOverallTest;
                return PartialView("_ListOverallTest");
            }
            #endregion
            #endregion
            return PartialView("_ListPhysical");
        }

        /// <summary>
        /// Phan trang chuc nang tim kiem the luc
        /// </summary>
        /// <param name="ContactGroupID"></param>
        /// <param name="teacherID"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [GridAction(EnableCustomBinding = true)]
        public ActionResult PhysicalTestAjax(GridCommand command, SearchViewModel frm)
        {

            Utils.Utils.TrimObject(frm);
            if (frm.EducationLevelID == null)
            {
                frm.EducationLevelID = 0;
            }
            if (frm.ClassID == null)
            {
                frm.ClassID = 0;
            }
            if (frm.PeriodExaminationID == null)
            {
                frm.PeriodExaminationID = 0;
            }
            int EducationGrade = _globalInfo.AppliedLevel.Value;

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = frm.EducationLevelID;
            Dictionary["ClassID"] = frm.ClassID;
            Dictionary["FullName"] = frm.FullName;
            Dictionary["HealthPeriodID"] = frm.PeriodExaminationID;
            decimal HeightMin = 0;
            decimal HeightMax = 0;
            int temp = 0;

            //PhysicalTest           
            if (frm.PhysicalClassificationID == null)
            {
                frm.PhysicalClassificationID = 0;
            }
            if (frm.NutritionID == null)
            {
                frm.NutritionID = 0;
            }
            if (string.IsNullOrEmpty(frm.HeightMin))
            {
                HeightMin = 0;
            }
            else
            {
                decimal.TryParse(frm.HeightMin.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out HeightMin);
            }

            if (string.IsNullOrEmpty(frm.HeightMax))
            {
                HeightMax = 0;
            }
            else
            {
                decimal.TryParse(frm.HeightMax.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out HeightMax);
            }
            decimal WeightMin = 0;
            decimal WeightMax = 0;
            if (string.IsNullOrEmpty(frm.WeightMin) || !int.TryParse(frm.WeightMin, out temp))
            {
                WeightMin = 0;
            }
            else
            {
                decimal.TryParse(frm.WeightMin.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out WeightMin);
            }

            if (string.IsNullOrEmpty(frm.WeightMax) || !int.TryParse(frm.WeightMax, out temp))
            {
                WeightMax = 0;
            }
            else
            {
                decimal.TryParse(frm.WeightMax.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out WeightMax);
            }

            List<PhysicalTest> query = this.PhysicalTestBusiness.GetListPhysicalTest(Global.SchoolID.Value, Global.AcademicYearID.Value, frm.EducationLevelID.Value,
            frm.ClassID.Value, frm.FullName, frm.PeriodExaminationID.Value, frm.PhysicalClassificationID.Value, frm.NutritionID.Value, HeightMin, HeightMax,
            WeightMin, WeightMax, EducationGrade, PhysicalTestConstants.PAGE_SIZE, command.Page, ref TotalRecord, _globalInfo.Semester.Value);

            List<PhysicalTestViewModel> lst = query.Select(o => new PhysicalTestViewModel
            {
                PupilID = o.MonitoringBook.PupilID,
                ClassID = o.MonitoringBook.ClassID,
                MonitoringBookID = o.MonitoringBookID,
                PhysicalTestID = o.PhysicalTestID,
                Date = o.Date,
                MonitoringDate = o.MonitoringBook.MonitoringDate,
                MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                FullName = o.MonitoringBook.PupilProfile.FullName,
                Name = o.MonitoringBook.PupilProfile.Name,
                DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                Height = o.Height.HasValue ? o.Height.Value.ToString("0.#", CultureInfo.InvariantCulture) : "",
                Weight = o.Weight.HasValue ? o.Weight.Value.ToString("0.#", CultureInfo.InvariantCulture) : "",
                Breast = o.Breast.HasValue ? o.Breast.Value.ToString("0.#", CultureInfo.InvariantCulture) : "",
                PhysicalClassification = o.PhysicalClassification.HasValue ? CommonConvert.PhysicalClassification(o.PhysicalClassification.Value) : "",
                Nutrition = CommonConvert.Nutrition(o.Nutrition)
            }).ToList();


            return View(new GridModel<PhysicalTestViewModel>()
            {
                Data = lst,
                Total = TotalRecord
            });
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult EyeTestAjax(GridCommand command, SearchViewModel frm)
        {
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            Utils.Utils.TrimObject(frm);
            if (frm.EducationLevelID == null)
            {
                frm.EducationLevelID = 0;
            }
            if (frm.ClassID == null)
            {
                frm.ClassID = 0;
            }
            if (frm.PeriodExaminationID == null)
            {
                frm.PeriodExaminationID = 0;
            }
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = frm.EducationLevelID;
            Dictionary["ClassID"] = frm.ClassID;
            Dictionary["FullName"] = frm.FullName;
            Dictionary["HealthPeriodID"] = frm.PeriodExaminationID;
            Dictionary["EducationGrade"] = _globalInfo.AppliedLevel.Value;
            int temp = 0;
            if (frm.IsTreatment == 1)
            {
                Dictionary["IsTreatment"] = true;
            }
            if (frm.IsTreatment == 0)
            {
                Dictionary["IsTreatment"] = false;
            }
            Dictionary["Other"] = false;
            if (!string.IsNullOrEmpty(frm.EyesightLeft) || int.TryParse(frm.EyesightLeft, out temp))
            {
                Dictionary["Leye"] = Convert.ToInt32(frm.EyesightLeft);
            }
            if (!string.IsNullOrEmpty(frm.EyesightRight) || int.TryParse(frm.EyesightRight, out temp))
            {
                Dictionary["Reye"] = Convert.ToInt32(frm.EyesightRight);
            }

            if (frm.HfDisabledRefractive != null && frm.HfDisabledRefractive != "")
            {
                Dictionary["HfDisabledRefractive"] = frm.HfDisabledRefractive.ToString();
            }
            else
            {
                if (frm.DisabledRefractive != null)
                {
                    Dictionary["HfDisabledRefractive"] = frm.DisabledRefractive.ToString();
                }
            }
            // IQueryable<EyeTest> lsEyeTest = EyeTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["ClassID"] = frm.ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            IQueryable<EyeTest> lsEyeTest = from a in EyeTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                            join c in MonitoringBookBusiness.All on a.MonitoringBookID equals c.MonitoringBookID
                                            join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                            select a;

            List<EyeTest> lstEyeTest = new List<EyeTest>();
            List<EyeTestViewModel> lst = new List<EyeTestViewModel>();
            if (frm.EducationLevelID != 0)
            {
                lsEyeTest = lsEyeTest.Where(p => p.MonitoringBook.EducationLevelID == frm.EducationLevelID);
            }
            TotalRecord = lsEyeTest.Count();
            lstEyeTest = lsEyeTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((command.Page - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
            foreach (EyeTest et in lstEyeTest)
            {
                EyeTestViewModel evm = new EyeTestViewModel();
                evm.PupilID = et.MonitoringBook.PupilID;
                evm.ClassID = et.MonitoringBook.ClassID;
                evm.MonitoringBookID = et.MonitoringBookID;
                evm.EyeTestID = et.EyeTestID;
                evm.DateTest = et.MonitoringBook.MonitoringDate;
                evm.FullName = et.MonitoringBook.PupilProfile.FullName;
                evm.DisplayName = et.MonitoringBook.ClassProfile.DisplayName;
                evm.MonitoringDateStr = et.MonitoringBook.MonitoringDate.HasValue ? et.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "";
                evm.RIsTreatment = "";
                evm.LIsTreatment = "";
                evm.RNotTreatment = "";
                evm.LNotTreatment = "";
                evm.RMyopic = "";
                evm.LMyopic = "";
                evm.RFarsightedness = "";
                evm.LFarsightedness = "";
                evm.RAstigmatism = "";
                evm.LAstigmatism = "";
                if (et.IsTreatment == true)
                {
                    evm.RIsTreatment = et.REye.ToString() + "/10";
                    evm.LIsTreatment = et.LEye.ToString() + "/10";
                }
                if (et.IsTreatment == false)
                {
                    evm.RNotTreatment = et.REye.ToString() + "/10";
                    evm.LNotTreatment = et.LEye.ToString() + "/10";
                }
                if (et.IsMyopic.HasValue && et.IsMyopic.Value == true)
                {
                    evm.RMyopic = et.RMyopic.HasValue ? et.RMyopic.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                    evm.LMyopic = et.LMyopic.HasValue ? et.LMyopic.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                }
                if (et.IsFarsightedness.HasValue && et.IsFarsightedness.Value == true)
                {
                    evm.RFarsightedness = et.RFarsightedness.HasValue ? et.RFarsightedness.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                    evm.LFarsightedness = et.LFarsightedness.HasValue ? et.LFarsightedness.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                }
                if (et.IsFarsightedness.HasValue && et.IsFarsightedness.Value == true)
                {
                    evm.RFarsightedness = et.RFarsightedness.HasValue ? et.RFarsightedness.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" :"";
                    evm.LFarsightedness = et.LFarsightedness.HasValue ? et.LFarsightedness.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                }
                if (et.IsAstigmatism.HasValue && et.IsAstigmatism.Value == true)
                {
                    evm.RAstigmatism = et.RAstigmatism.HasValue ? et.RAstigmatism.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                    evm.LAstigmatism = et.LAstigmatism.HasValue ? et.LAstigmatism.Value.ToString("0.#", CultureInfo.InvariantCulture) + " D" : "";
                }
                evm.OtherEyeDiseases = et.Other;
                lst.Add(evm);
            }

            return View(new GridModel<EyeTestViewModel>()
            {
                Data = lst,
                Total = TotalRecord
            });
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult DentalAjax(GridCommand command, SearchViewModel frm)
        {
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool OtherToothDiseases = false;
            bool ToothDecay = false;
            bool IsScraptTeeth = false;
            bool IsTreatmentTooth = false;
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = frm.EducationLevelID;
            Dictionary["ClassID"] = frm.ClassID;
            Dictionary["FullName"] = frm.FullName;
            Dictionary["HealthPeriodID"] = frm.PeriodExaminationID;
            if (Request["OtherToothDiseases"] == "")
            {
                OtherToothDiseases = true;
            }
            if (Request["ToothDecay"] == "")
            {
                ToothDecay = true;
            }
            if (Request["IsScraptTeeth"] == "")
            {
                IsScraptTeeth = true;
            }
            if (Request["IsTreatmentTooth"] == "")
            {
                IsTreatmentTooth = true;
            }
            Dictionary["Other"] = OtherToothDiseases;
            Dictionary["ToothDecay"] = ToothDecay;
            Dictionary["IsScraptTeeth"] = IsScraptTeeth;
            Dictionary["IsTreatment"] = IsTreatmentTooth;
            Dictionary["EducationGrade"] = _globalInfo.AppliedLevel.Value;
            //  IQueryable<DentalTest> lsDentalTest = DentalTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = frm.ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            IQueryable<DentalTest> lsDentalTest = from a in DentalTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                  join c in MonitoringBookBusiness.All on a.MonitoringBookID equals c.MonitoringBookID
                                                  join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                  select a;

            TotalRecord = lsDentalTest.Count();
            List<DentalTest> lstDentalTest = lsDentalTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((command.Page - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
            List<DentalTestViewModel> lst = lstDentalTest.Select(o => new DentalTestViewModel
            {
                PupilID = o.MonitoringBook.PupilID,
                ClassID = o.MonitoringBook.ClassID,
                MonitoringBookID = o.MonitoringBookID,
                DentalTestID = o.DentalTestID,
                DateTest = o.MonitoringBook.MonitoringDate,
                FullName = o.MonitoringBook.PupilProfile.FullName,
                DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                NumDentalFilling = o.NumDentalFilling,
                NumTeethExtracted = o.NumTeethExtracted,
                NumPreventiveDentalFilling = o.NumPreventiveDentalFilling,
                IsScraptTeeth = Utils.CommonConvert.Tox(o.IsScraptTeeth),
                IsTreatmentTooth = Utils.CommonConvert.Tox(o.IsTreatment),
                OtherToothdiseases = o.Other
            }).ToList();
            return View(new GridModel<DentalTestViewModel>()
            {
                Data = lst,
                Total = TotalRecord
            });
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult EarNoseAjax(GridCommand command, SearchViewModel frm)
        {
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = frm.EducationLevelID;
            Dictionary["ClassID"] = frm.ClassID;
            Dictionary["FullName"] = frm.FullName;
            Dictionary["HealthPeriodID"] = frm.PeriodExaminationID;
            if (frm.EducationLevelID == null)
            {
                frm.EducationLevelID = 0;
            }
            if (frm.ClassID == null)
            {
                frm.ClassID = 0;
            }
            if (frm.PeriodExaminationID == null)
            {
                frm.PeriodExaminationID = 0;
            }

            Dictionary["IsSick"] = frm.IsSick;
            Dictionary["IsDeaf"] = frm.IsDeaf;
            Session["IsSick"] = frm.IsSick;
            Session["IsDeaf"] = frm.IsDeaf;
            Dictionary["EducationGrade"] = _globalInfo.AppliedLevel.Value;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = frm.ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            IQueryable<ENTTest> IQueEarNose = from a in ENTTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                              join c in MonitoringBookBusiness.All on a.MonitoringBookID equals c.MonitoringBookID
                                              join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                              select a;

            TotalRecord = IQueEarNose.Count();
            List<ENTTest> query = IQueEarNose.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((command.Page - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
            bool chkSick = false;
            if (frm.IsSick)
            {
                chkSick = true;
            }
            List<PhysicalTestViewModel> lst = query.Select(o => new PhysicalTestViewModel
            {
                ENTTestID = o.ENTTestID,
                DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                CreatedDate = o.CreatedDate,
                Description = o.Description,
                IsActive = o.IsActive,
                IsDeaf = o.IsDeaf,
                IsSick = o.IsSick,
                LCapacityHearing = o.LCapacityHearing,
                MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                RCapacityHearing = o.RCapacityHearing,
                ModifiedDate = o.ModifiedDate,
                FullName = o.MonitoringBook.PupilProfile.FullName,
                ClassID = o.MonitoringBook.ClassID,
                MonitoringDate = o.MonitoringBook.MonitoringDate,
                chkSick = chkSick,
                PupilID = o.MonitoringBook.PupilID,
                MonitoringBookID = o.MonitoringBookID
            }).ToList();

            return View(new GridModel<PhysicalTestViewModel>()
            {
                Data = lst,
                Total = TotalRecord
            });
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult SpineAjax(GridCommand command, SearchViewModel frm)
        {
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = frm.EducationLevelID;
            Dictionary["ClassID"] = frm.ClassID;
            Dictionary["FullName"] = frm.FullName;
            Dictionary["HealthPeriodID"] = frm.PeriodExaminationID;
            Dictionary["EducationGrade"] = _globalInfo.AppliedLevel.Value;
            bool IsDeformityOfTheSpine = false;
            bool OtherSpine = false;
            if (Request["IsDeformityOfTheSpine"] == "")
            {
                IsDeformityOfTheSpine = true;
            }
            if (Request["OtherSpine"] == "")
            {
                OtherSpine = true;
            }
            Dictionary["IsDeformityOfTheSpine"] = IsDeformityOfTheSpine;
            Dictionary["Other"] = OtherSpine;

            //IQueryable<SpineTest> lsSpineTest = SpineTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = frm.ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            IQueryable<SpineTest> lsSpineTest = from a in SpineTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                join c in MonitoringBookBusiness.All on a.MonitoringBookID equals c.MonitoringBookID
                                                join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                select a;

            TotalRecord = lsSpineTest.Count();
            List<SpineTest> lstSpineTest = lsSpineTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((command.Page - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
            List<SpineTestViewModel> lst = lstSpineTest.Select(o => new SpineTestViewModel
            {
                PupilID = o.MonitoringBook.PupilID,
                ClassID = o.MonitoringBook.ClassID,
                MonitoringBookID = o.MonitoringBookID,
                SpineTestID = o.SpineTestID,
                DateTest = o.MonitoringBook.MonitoringDate,
                FullName = o.MonitoringBook.PupilProfile.FullName,
                DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                ExaminationItalic = o.ExaminationItalic,
                ExaminationStraight = o.ExaminationStraight,
                IsDeformityOfTheSpine = CommonConvert.Tox(o.IsDeformityOfTheSpine),
                OtherSpineDiseases = o.Other
            }).ToList();

            return View(new GridModel<SpineTestViewModel>
            {
                Data = lst,
                Total = TotalRecord
            });
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult OverAllAjax(GridCommand command, SearchViewModel frm)
        {
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = frm.EducationLevelID;
            Dictionary["ClassID"] = frm.ClassID;
            Dictionary["FullName"] = frm.FullName;
            Dictionary["HealthPeriodID"] = frm.PeriodExaminationID;
            Dictionary["EducationGrade"] = _globalInfo.AppliedLevel.Value;
            Dictionary["EvaluationHealth"] = frm.EvaluationHealth;
            if (Request["SkinDiseases"] != null && Request["SkinDiseases"] != "")
            {
                Dictionary["SkinDiseases"] = Convert.ToBoolean(Request["SkinDiseases"]) == true ? 1 : 2;
            }
            if (Request["IsHearDiseases"] != null && Request["IsHearDiseases"] != "")
            {
                Dictionary["IsHeartDiseases"] = Convert.ToBoolean(Request["IsHearDiseases"]);
            }
            if (Request["IsBirthDefect"] != null && Request["IsBirthDefect"] != "")
            {
                Dictionary["IsBirthDefect"] = Convert.ToBoolean(Request["IsBirthDefect"]);
            }

            Dictionary["IsDredging"] = false;
            Dictionary["IsActive"] = true;
            //IQueryable<OverallTest> query = OverallTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = frm.ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            IQueryable<OverallTest> query = from a in OverallTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                            join c in MonitoringBookBusiness.All on a.MonitoringBookID equals c.MonitoringBookID
                                            join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                            select a;
            TotalRecord = query.Count();
            List<OverallTest> list = query.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).Skip((command.Page - 1) * PhysicalTestConstants.PAGE_SIZE).Take(PhysicalTestConstants.PAGE_SIZE).ToList();
            List<OverallTestViewModel> lstOverallTest = list.Select(o => new OverallTestViewModel
            {
                PupilID = o.MonitoringBook.PupilID,
                ClassID = o.MonitoringBook.ClassID,
                MonitoringBookID = o.MonitoringBookID,
                DateTest = o.MonitoringBook.MonitoringDate,
                DisplayName = o.MonitoringBook.ClassProfile.DisplayName,
                EvaluationHealth = o.EvaluationHealth == 1 ? "Loại A" : o.EvaluationHealth == 2 ? "Loại B" : "Loại C",
                MonitoringDateStr = o.MonitoringBook.MonitoringDate.HasValue ? o.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : "",
                FullName = o.MonitoringBook.PupilProfile.FullName,
                Gende = o.MonitoringBook.PupilProfile.Genre,
                IsBirthDefect = o.IsBirthDefect == true ? "x" : null,
                IsHeartDiseases = o.IsHeartDiseases == true ? "x" : null,
                Other = o.Other,
                OverallForeign = o.OverallForeign == 1 ? o.DescriptionForeign : "Bình thường",
                OverallInternal = o.OverallInternal == 1 ? o.DescriptionOverallInternall : "Bình thường",
                RequrieOfDoctor = o.RequireOfDoctor,
                SkinDiseases = o.SkinDiseases == 1 ? o.DescriptionSkinDiseases : "Bình thường",
                BirthDate = o.MonitoringBook.PupilProfile.BirthDate
            }).ToList();

            return View(new GridModel<OverallTestViewModel>
            {
                Data = lstOverallTest,
                Total = TotalRecord
            });
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SkipCheckRole]
        public JsonResult Create()
        {
            if (!CheckActionPermissionMinAddReturn())
            {
                return Json(new JsonMessage("Thầy/cô chưa được phân công ở chức năng này","error"));
            }
            PhysicalTest physicaltest = new PhysicalTest();
            TryUpdateModel(physicaltest);
            Utils.Utils.TrimObject(physicaltest);

            this.PhysicalTestBusiness.Insert(physicaltest);
            this.PhysicalTestBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SkipCheckRole]
        public JsonResult Edit(int PhysicalTestID)
        {
            PhysicalTest physicaltest = this.PhysicalTestBusiness.Find(PhysicalTestID);
            TryUpdateModel(physicaltest);
            Utils.Utils.TrimObject(physicaltest);
            this.PhysicalTestBusiness.Update(physicaltest);
            this.PhysicalTestBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.PhysicalTestBusiness.Delete(id);
            this.PhysicalTestBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public void SetViewData()
        {
            //Do du lieu EducationLevel
            ViewData[PhysicalTestConstants.CBO_EDUCATIONLEVEL] = Global.EducationLevels;

            // Do du lieu cboPeriodExamination

            List<SelectListItem> lstPeriodExamination = new List<SelectListItem>();
            lstPeriodExamination.Add(new SelectListItem { Text = SystemParamsInFile.PERIOD_EXAMINATION_1, Value = "1" });
            lstPeriodExamination.Add(new SelectListItem { Text = SystemParamsInFile.PERIOD_EXAMINATION_2, Value = "2" });
            ViewData[PhysicalTestConstants.CBO_PERIODEXAMINATION] = new SelectList(lstPeriodExamination, "Value", "Text");
            //Do du lieu combobox ItemExamination
            ViewData[PhysicalTestConstants.CBO_ITEMEXAMINATION] = CommonList.ItemExamination();
            //do du lieu combobox Nutrition
            ViewData[PhysicalTestConstants.CBO_NUTRITION] = CommonList.NutritionType();
            //Do du lieu Combobox PhysicalClassification
            ViewData[PhysicalTestConstants.CBO_PHYSICALCLASSIFICATION] = CommonList.PhysicalClassification();
            //Do du lieu Combobox Class
            ViewData[PhysicalTestConstants.CBO_CLASS] = new List<ClassProfile>();


            // Do du lieu cboEvaluationHealth
            List<SelectListItem> lstEvaluationHealth = new List<SelectListItem>();
            lstEvaluationHealth.Add(new SelectListItem { Text = SystemParamsInFile.EvaluationHealth_Type_A, Value = "1" });
            lstEvaluationHealth.Add(new SelectListItem { Text = SystemParamsInFile.EvaluationHealth_Type_B, Value = "2" });
            lstEvaluationHealth.Add(new SelectListItem { Text = SystemParamsInFile.EvaluationHealth_Type_C, Value = "3" });
            ViewData[PhysicalTestConstants.CBO_EVALUATIONHEALTH] = new SelectList(lstEvaluationHealth, "Value", "Text");


            #region EyeTest
            //-	cboIsTreatment: mặc định [Tất cả]
            //+ (1) Có đeo kính
            //+ (0) không đeo kính 

            List<SelectListItem> lstEyeIsTreatment = new List<SelectListItem>();
            lstEyeIsTreatment.Add(new SelectListItem { Text = SystemParamsInFile.EYE_ISTREATMENT_1, Value = "1" });
            lstEyeIsTreatment.Add(new SelectListItem { Text = SystemParamsInFile.EYE_ISTREATMENT_0, Value = "0" });
            ViewData[PhysicalTestConstants.CBO_ISTREATMENT] = new SelectList(lstEyeIsTreatment, "Value", "Text");
            //-	cboDisabledRefractive: mặc định [Tất cả]
            ViewData[PhysicalTestConstants.CBO_DISABLEDREFRACTIVE] = CommonList.DisabledRefractive();
            ViewData[PhysicalTestConstants.CHECK_PERMISSION_VIEW_HEALTH_TEST] = CheckActionPerMinViewByController("HealthTest");

            #endregion
        }

        #region LoadCombobox

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? idEducationLevel)
        {
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (idEducationLevel == null)
            {
                idEducationLevel = -1;
            }
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            if (lsClass.Count() > 0)
            {
                lstClass = lsClass.ToList();
            }

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region ExprortExcel
        [SkipCheckRole]
        public FileResult ExportExcelPhysical()
        {
            int EducationLevelID = 0;
            int ClassID = 0;
            string ClassName = Res.Get("All");
            int PeriodExaminationID = 0;
            string FullName = "";
            int ItemExaminationID = 0;
            int PhysicalClassificationID = 0;
            int NutritionID = 0;
            decimal HeightMin = 0;
            decimal HeightMax = 0;
            decimal WeightMin = 0;
            decimal WeightMax = 0;
            int EducationGrade = _globalInfo.AppliedLevel.Value;
            if (Request["EducationLevelID"] != "")
            {
                EducationLevelID = int.Parse(Request["EducationLevelID"]);

            }
            if (Request["ClassID"] != "")
            {
                ClassID = int.Parse(Request["ClassID"]);
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                ClassName = cp.DisplayName;
            }
            if (Request["PeriodExaminationID"] != "")
            {
                PeriodExaminationID = int.Parse(Request["PeriodExaminationID"]);
            }
            if (Request["FullName"] != "")
            {
                FullName = Request["FullName"];
            }
            if (Request["ItemExaminationID"] != "")
            {
                ItemExaminationID = int.Parse(Request["ItemExaminationID"]);
            }
            if (Request["NutritionID"] != "")
            {
                NutritionID = int.Parse(Request["NutritionID"]);
            }
            if (!string.IsNullOrEmpty(Request["HeightMin"]))
            {
                decimal.TryParse(Request["HeightMin"], System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out HeightMin);
            }
            if (!string.IsNullOrEmpty(Request["HeightMax"]))
            {
                decimal.TryParse(Request["HeightMax"], System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out HeightMax);
            }
            if (Request["PhysicalClassificationID"] != "")
            {
                PhysicalClassificationID = int.Parse(Request["PhysicalClassificationID"]);

            }
            if (!string.IsNullOrEmpty(Request["WeightMin"]))
            {
                decimal.TryParse(Request["WeightMin"], System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out WeightMin);
            }
            if (!string.IsNullOrEmpty(Request["WeightMax"]))
            {
                decimal.TryParse(Request["WeightMax"], System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out WeightMax);
            }

            List<PhysicalTest> lstPhysicalTest = this.PhysicalTestBusiness.GetListPhysicalTestExport(Global.SchoolID.Value, Global.AcademicYearID.Value, EducationLevelID,
            ClassID, FullName, PeriodExaminationID, PhysicalClassificationID, NutritionID, HeightMin, HeightMax, WeightMin, WeightMax, EducationGrade, _globalInfo.Semester.Value);
            AcademicYear ay = AcademicYearBusiness.Find(Global.AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(Global.SchoolID);
            SheetData SheetData = new SheetData(SystemParamsInFile.TEMPLATE_THONGKE_THELUC);
            SheetData.Data["AcademicYear"] = ay.DisplayTitle; ;
            SheetData.Data["SchoolName"] = sp.SchoolName;
            SheetData.Data["ProvinceName"] = (sp.District != null ? sp.District.DistrictName : "");
            SheetData.Data["SupervisingDeptName"] = UtilsBusiness.GetSupervisingDeptName(sp.SchoolProfileID, _globalInfo.AppliedLevel.Value);
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["ClassName"] = ClassName;
            string labelForEducationLevel;
            if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                   || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                labelForEducationLevel = Res.Get("PupilProfile_Label_EducationLevel_MN");
            }
            else
            {
                labelForEducationLevel = Res.Get("PhysicalTest_Label_EducationLevel");
            }
            SheetData.Data["LabelForEducationLevel"] = labelForEducationLevel;
            EducationLevel Edu = new EducationLevel();
            if (EducationLevelID != 0)
            {
                Edu = EducationLevelBusiness.Find(EducationLevelID);
                SheetData.Data["EducationLevelName"] = Edu.Resolution;
            }
            else
            {
                SheetData.Data["EducationLevelName"] = Res.Get("All");
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_THONGKE_THELUC);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string ReportName = this.GetReportName(EducationLevelID, ClassID, PeriodExaminationID, ClassName, Edu.Resolution, PhysicalTestConstants.REPORT_SEIGNEUR);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);

            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.CopyPasteSameSize(Template.GetRange("A1", "K11"), 1, 1);
            Sheet.FillVariableValue(SheetData.Data);

            if (lstPhysicalTest.Count > 0)
            {
                int StartRow = 12;
                int StartCol = 1;
                for (int i = 0; i < lstPhysicalTest.Count; i++)
                {
                    PhysicalTest pt = lstPhysicalTest[i];
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, "'" + (pt.MonitoringBook.MonitoringDate != null ? pt.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : ""));
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, pt.MonitoringBook.PupilProfile.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 3, "'" + pt.MonitoringBook.PupilProfile.BirthDate.ToString("dd/MM/yyyy"));
                    Sheet.SetCellValue(StartRow + i, StartCol + 4, Utils.CommonConvert.Genre(pt.MonitoringBook.PupilProfile.Genre));
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, pt.MonitoringBook.ClassProfile.DisplayName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 6, pt.Height.HasValue ? pt.Height.Value.ToString("0.#", CultureInfo.InvariantCulture) : "");
                    Sheet.SetCellValue(StartRow + i, StartCol + 7, pt.Weight.HasValue ? pt.Weight.Value.ToString("0.#", CultureInfo.InvariantCulture) : "");
                    Sheet.SetCellValue(StartRow + i, StartCol + 8, pt.Breast.HasValue ? pt.Breast.Value.ToString("0.#", CultureInfo.InvariantCulture) : "");
                    Sheet.SetCellValue(StartRow + i, StartCol + 9, pt.PhysicalClassification.HasValue ? Utils.CommonConvert.PhysicalClassification(pt.PhysicalClassification.Value) : "");
                    Sheet.SetCellValue(StartRow + i, StartCol + 10, Utils.CommonConvert.Nutrition(pt.Nutrition));
                    Sheet.CopyPaste(Template.GetRange("A" + StartRow.ToString(), "K" + StartRow.ToString()), StartRow + i, 1, true);
                }
            }
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            sheet1.Delete();
            Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = Utils.Utils.StripVNSignAndSpace(ReportName) ;
            return result;
        }
        [SkipCheckRole]
        public FileResult ExportExcelEye()
        {
            int EducationLevelID = 0;
            int ClassID = 0;
            string ClassName = Res.Get("All");
            int PeriodExaminationID = 0;
            string FullName = "";
            int ItemExaminationID = 0;
            int IsTreatment = 3;
            decimal EyesightLeft = 0;
            decimal EyesightRight = 0;
            bool OtherEyeDiseases = false;

            if (Request["EducationLevelID"] != "")
            {
                EducationLevelID = int.Parse(Request["EducationLevelID"]);

            }
            if (Request["ClassID"] != "")
            {
                ClassID = int.Parse(Request["ClassID"]);
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                ClassName = cp.DisplayName;
            }
            if (Request["PeriodExaminationID"] != "")
            {
                PeriodExaminationID = int.Parse(Request["PeriodExaminationID"]);
            }
            if (Request["FullName"] != "")
            {
                FullName = Request["FullName"];
            }
            if (Request["ItemExaminationID"] != "")
            {
                ItemExaminationID = int.Parse(Request["ItemExaminationID"]);
            }
            if (Request["IsTreatment"] != "")
            {
                IsTreatment = int.Parse(Request["IsTreatment"]);
            }
            if (Request["EyesightLeft"] != "")
            {
                EyesightLeft = decimal.Parse(Request["EyesightLeft"]);
            }
            if (Request["EyesightRight"] != "")
            {
                EyesightRight = decimal.Parse(Request["EyesightRight"]);
            }

            //if (Convert.ToBoolean(Request["OtherEyeDiseases"]) == true)
            //{
            //    OtherEyeDiseases = true;
            //}
            //else
            //{
            //    OtherEyeDiseases = false;
            //}

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = EducationLevelID;
            Dictionary["ClassID"] = ClassID;
            Dictionary["FullName"] = FullName;
            Dictionary["HealthPeriodID"] = PeriodExaminationID;
            int temp = 0;
            if (IsTreatment != 3)
            {
                if (IsTreatment == 1)
                {
                    Dictionary["IsTreatment"] = true;
                }
                if (IsTreatment == 0)
                {
                    Dictionary["IsTreatment"] = false;
                }
            }
            if (!string.IsNullOrEmpty(Request["EyesightLeft"]) || int.TryParse(Request["EyesightLeft"], out temp))
            {
                Dictionary["Leye"] = Convert.ToInt32(Request["EyesightLeft"]);
            }
            if (!string.IsNullOrEmpty(Request["EyesightRight"]) || int.TryParse(Request["EyesightRight"], out temp))
            {
                Dictionary["Reye"] = Convert.ToInt32(Request["EyesightRight"]);
            }
            if (Request["HfDisabledRefractive"] != null && Request["HfDisabledRefractive"] != "")
            {
                //List<int> listint = new List<int>();
                //listint = Request["HfDisabledRefractive"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                Dictionary["HfDisabledRefractive"] = Request["HfDisabledRefractive"];
            }
            else
            {
                if (Request["DisabledRefractive"] != "")
                {
                    Dictionary["HfDisabledRefractive"] = Request["DisabledRefractive"];
                }
            }
            Dictionary["Other"] = OtherEyeDiseases;
            Dictionary["EducationGrade"] = _globalInfo.AppliedLevel.Value;

            // IQueryable<EyeTest> lsEyeTest = EyeTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = PeriodExaminationID;
            IQueryable<EyeTest> lsEyeTest = from a in EyeTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                            join c in MonitoringBookBusiness.Search(dic) on a.MonitoringBookID equals c.MonitoringBookID
                                            join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                            select a;

            List<EyeTest> lstEyeTest = new List<EyeTest>();
            lstEyeTest = lsEyeTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).ThenBy(p => p.MonitoringBook.PupilProfile.FullName).ToList();

            AcademicYear ay = AcademicYearBusiness.Find(Global.AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(Global.SchoolID);

            SheetData SheetData = new SheetData("Template_Thongke_Mat");
            SheetData.Data["AcademicYear"] = ay.DisplayTitle;
            SheetData.Data["SchoolName"] = sp.SchoolName;
            SheetData.Data["ProvinceName"] = (sp.District != null ? sp.District.DistrictName : "");
            SheetData.Data["SupervisingDeptName"] = UtilsBusiness.GetSupervisingDeptName(sp.SchoolProfileID, _globalInfo.AppliedLevel.Value);
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["ClassName"] = ClassName;
            string labelForEducationLevel;
            if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                   || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                labelForEducationLevel = Res.Get("PupilProfile_Label_EducationLevel_MN");
            }
            else
            {
                labelForEducationLevel = Res.Get("PhysicalTest_Label_EducationLevel");
            }
            SheetData.Data["LabelForEducationLevel"] = labelForEducationLevel;
            EducationLevel Edu = new EducationLevel();
            if (EducationLevelID != 0)
            {
                Edu = EducationLevelBusiness.Find(EducationLevelID);
                SheetData.Data["EducationLevelName"] = Edu.Resolution;
            }
            SheetData.Data["EducationLevelName"] = Res.Get("All");

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_THONGKE_MAT);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string ReportName = this.GetReportName(EducationLevelID, ClassID, PeriodExaminationID, ClassName, Edu.Resolution, PhysicalTestConstants.REPORT_EYE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);

            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.CopyPasteSameSize(Template.GetRange("A1", "Q11"), 1, 1);
            Sheet.FillVariableValue(SheetData.Data);

            if (lstEyeTest.Count > 0)
            {
                int StartRow = 12;
                int StartCol = 1;
                for (int i = 0; i < lstEyeTest.Count; i++)
                {
                    EyeTest pt = lstEyeTest[i];
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, "'" + (pt.MonitoringBook.MonitoringDate != null ? pt.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : ""));
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, pt.MonitoringBook.PupilProfile.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 3, "'" + pt.MonitoringBook.PupilProfile.BirthDate.ToString("dd/MM/yyyy"));
                    Sheet.SetCellValue(StartRow + i, StartCol + 4, Utils.CommonConvert.Genre(pt.MonitoringBook.PupilProfile.Genre));
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, pt.MonitoringBook.ClassProfile.DisplayName);
                    
                    
                        Sheet.SetCellValue(StartRow + i, StartCol + 6, "'" + pt.LEye + "/10");
                        Sheet.SetCellValue(StartRow + i, StartCol + 7, "'" + pt.REye + "/10");
                   
                    Sheet.SetCellValue(StartRow + i, StartCol + 8, "'" + pt.LIsEye + "/10");
                    Sheet.SetCellValue(StartRow + i, StartCol + 9, "'" + pt.RIsEye + "/10");
                    
                    if (pt.IsMyopic == true)
                    {
                        Sheet.SetCellValue(StartRow + i, StartCol + 10, pt.LMyopic);
                        Sheet.SetCellValue(StartRow + i, StartCol + 11, pt.RMyopic);
                    }
                    if (pt.IsFarsightedness == true)
                    {
                        Sheet.SetCellValue(StartRow + i, StartCol + 12, pt.LFarsightedness);
                        Sheet.SetCellValue(StartRow + i, StartCol + 13, pt.RFarsightedness);
                    }
                    if (pt.IsAstigmatism == true)
                    {
                        Sheet.SetCellValue(StartRow + i, StartCol + 14, pt.LAstigmatism);
                        Sheet.SetCellValue(StartRow + i, StartCol + 15, pt.RAstigmatism);
                    }
                    Sheet.SetCellValue(StartRow + i, StartCol + 16, pt.Other);
                    Sheet.CopyPaste(Template.GetRange("A" + StartRow.ToString(), "Q" + StartRow.ToString()), StartRow + i, 1, true);


                }
            }
            Template.Delete();
            Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;

            return result;
        }
        [SkipCheckRole]
        public FileResult ExportExcelDental()
        {
            int EducationLevelID = 0;
            int ClassID = 0;
            string ClassName = Res.Get("All");
            int PeriodExaminationID = 0;
            string FullName = "";
            int ItemExaminationID = 0;
            bool OtherToothDiseases = false;
            bool ToothDecay = false;
            bool IsScraptTeeth = false;
            bool IsTreatmentTooth = false;
            if (Request["EducationLevelID"] != "")
            {
                EducationLevelID = int.Parse(Request["EducationLevelID"]);

            }
            if (Request["ClassID"] != "")
            {
                ClassID = int.Parse(Request["ClassID"]);
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                ClassName = cp.DisplayName;
            }
            if (Request["PeriodExaminationID"] != "")
            {
                PeriodExaminationID = int.Parse(Request["PeriodExaminationID"]);
            }
            if (Request["FullName"] != "")
            {
                FullName = Request["FullName"];
            }
            if (Request["ItemExaminationID"] != "")
            {
                ItemExaminationID = int.Parse(Request["ItemExaminationID"]);
            }
            if (Request["OtherToothDiseases"] != "")
            {
                OtherToothDiseases = bool.Parse(Request["OtherToothDiseases"]);
            }
            if (Request["ToothDecay"] != "")
            {
                ToothDecay = bool.Parse(Request["ToothDecay"]);
            }
            if (Request["IsScraptTeeth"] != "")
            {
                IsScraptTeeth = bool.Parse(Request["IsScraptTeeth"]);
            }
            if (Request["IsTreatmentTooth"] != "")
            {
                IsTreatmentTooth = bool.Parse(Request["IsTreatmentTooth"]);
            }

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = EducationLevelID;
            Dictionary["ClassID"] = ClassID;
            Dictionary["FullName"] = FullName;
            Dictionary["HealthPeriodID"] = PeriodExaminationID;
            Dictionary["Other"] = OtherToothDiseases;
            Dictionary["ToothDecay"] = ToothDecay;
            Dictionary["IsScraptTeeth"] = IsScraptTeeth;
            Dictionary["IsTreatment"] = IsTreatmentTooth;
            Dictionary["EducationGrade"] = _globalInfo.AppliedLevel.Value;

            // IQueryable<DentalTest> lsDentalTest = DentalTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = PeriodExaminationID;
            IQueryable<DentalTest> lsDentalTest = from a in DentalTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                  join c in MonitoringBookBusiness.Search(dic) on a.MonitoringBookID equals c.MonitoringBookID
                                                  join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                  select a;

            List<DentalTest> lstDentalTest = lsDentalTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).ThenBy(p => p.MonitoringBook.PupilProfile.FullName).ToList();
            AcademicYear ay = AcademicYearBusiness.Find(Global.AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(Global.SchoolID);
            SheetData SheetData = new SheetData("Template_Thongke_Ranghammat");
            SheetData.Data["AcademicYear"] = ay.DisplayTitle; ;
            SheetData.Data["SchoolName"] = sp.SchoolName;
            SheetData.Data["ProvinceName"] = (sp.District != null ? sp.District.DistrictName : "");
            SheetData.Data["SupervisingDeptName"] = UtilsBusiness.GetSupervisingDeptName(sp.SchoolProfileID, _globalInfo.AppliedLevel.Value);
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["ClassName"] = ClassName;
            string labelForEducationLevel;
            if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                   || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                labelForEducationLevel = Res.Get("PupilProfile_Label_EducationLevel_MN");
            }
            else
            {
                labelForEducationLevel = Res.Get("PhysicalTest_Label_EducationLevel");
            }
            SheetData.Data["LabelForEducationLevel"] = labelForEducationLevel;
            EducationLevel Edu = new EducationLevel();
            if (EducationLevelID != 0)
            {
                Edu = EducationLevelBusiness.Find(EducationLevelID);
                SheetData.Data["EducationLevelName"] = Edu.Resolution;
            }
            else
            {
                SheetData.Data["EducationLevelName"] = Res.Get("All");
            }
            SheetData.Data["ReportTitle"] = Res.Get("Label_Report_Title") + " " + AcademicYearBusiness.Find(_globalInfo.AcademicYearID).DisplayTitle;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_THONGKE_RANGHAMMAT);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string ReportName = this.GetReportName(EducationLevelID, ClassID, PeriodExaminationID, ClassName, Edu.Resolution, PhysicalTestConstants.REPORT_DENTAL);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            // IVTWorksheet Template = oBook.GetSheet(1);
            IVTWorksheet Sheet = oBook.GetSheet(1);
            //Sheet.CopyPasteSameSize(Template.GetRange("A1", "L10"), 1, 1);
            Sheet.FillVariableValue(SheetData.Data);

            if (lstDentalTest.Count > 0)
            {
                int StartRow = 12;
                int StartCol = 1;
                for (int i = 0; i < lstDentalTest.Count; i++)
                {
                    DentalTest pt = lstDentalTest[i];
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, "'" + (pt.MonitoringBook.MonitoringDate != null ? pt.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : ""));
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, pt.MonitoringBook.PupilProfile.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 3, "'" + pt.MonitoringBook.PupilProfile.BirthDate.ToString("dd/MM/yyyy"));
                    Sheet.SetCellValue(StartRow + i, StartCol + 4, Utils.CommonConvert.Genre(pt.MonitoringBook.PupilProfile.Genre));
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, pt.MonitoringBook.ClassProfile.DisplayName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 6, pt.NumDentalFilling);
                    Sheet.SetCellValue(StartRow + i, StartCol + 7, pt.NumPreventiveDentalFilling);
                    Sheet.SetCellValue(StartRow + i, StartCol + 8, pt.NumTeethExtracted);
                    Sheet.SetCellValue(StartRow + i, StartCol + 9, Utils.CommonConvert.Tox(pt.IsScraptTeeth));
                    Sheet.SetCellValue(StartRow + i, StartCol + 10, Utils.CommonConvert.Tox(pt.IsTreatment));
                    Sheet.SetCellValue(StartRow + i, StartCol + 11, pt.Other);
                    Sheet.CopyPaste(Sheet.GetRange("A" + StartRow.ToString(), "L" + StartRow.ToString()), StartRow + i, 1, true);
                }
            }
            Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;
        }
        [SkipCheckRole]
        public FileResult ExportExcelSpine()
        {
            int EducationLevelID = 0;
            int ClassID = 0;
            string ClassName = Res.Get("All");
            int PeriodExaminationID = 0;
            string FullName = "";
            int ItemExaminationID = 0;
            bool IsDeformityOfTheSpine = false;
            bool OtherSpine = false;
            if (Request["EducationLevelID"] != "")
            {
                EducationLevelID = int.Parse(Request["EducationLevelID"]);

            }
            if (Request["ClassID"] != "")
            {
                ClassID = int.Parse(Request["ClassID"]);
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                ClassName = cp.DisplayName;
            }
            if (Request["PeriodExaminationID"] != "")
            {
                PeriodExaminationID = int.Parse(Request["PeriodExaminationID"]);
            }
            if (Request["FullName"] != "")
            {
                FullName = Request["FullName"];
            }
            if (Request["ItemExaminationID"] != "")
            {
                ItemExaminationID = int.Parse(Request["ItemExaminationID"]);
            }
            if (Request["IsDeformityOfTheSpine"] != "")
            {
                IsDeformityOfTheSpine = bool.Parse(Request["IsDeformityOfTheSpine"]);
            }
            if (Request["OtherSpine"] != "")
            {
                OtherSpine = bool.Parse(Request["OtherSpine"]);
            }

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["EducationLevelID"] = EducationLevelID;
            Dictionary["ClassID"] = ClassID;
            Dictionary["FullName"] = FullName;
            Dictionary["HealthPeriodID"] = PeriodExaminationID;
            Dictionary["IsDeformityOfTheSpine"] = IsDeformityOfTheSpine;
            Dictionary["Other"] = OtherSpine;
            Dictionary["EducationGrade"] = _globalInfo.AppliedLevel.Value;

            //IQueryable<SpineTest> lsSpineTest = SpineTestBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);

            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["ClassID"] = ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = PeriodExaminationID;
            IQueryable<SpineTest> lsSpineTest = from a in SpineTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary)
                                                join c in MonitoringBookBusiness.Search(dic) on a.MonitoringBookID equals c.MonitoringBookID
                                                join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                select a;

            List<SpineTest> lstSpineTest = lsSpineTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).ThenBy(p => p.MonitoringBook.PupilProfile.FullName).ToList();

            AcademicYear ay = AcademicYearBusiness.Find(Global.AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(Global.SchoolID);
            SheetData SheetData = new SheetData("Template_Thongke_Cotsong");
            SheetData.Data["AcademicYear"] = ay.DisplayTitle; ;
            SheetData.Data["SchoolName"] = sp.SchoolName;
            SheetData.Data["ProvinceName"] = (sp.District != null ? sp.District.DistrictName : "");
            SheetData.Data["SupervisingDeptName"] = UtilsBusiness.GetSupervisingDeptName(sp.SchoolProfileID, _globalInfo.AppliedLevel.Value);
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["ClassName"] = ClassName;
            string labelForEducationLevel;
            if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                   || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                labelForEducationLevel = Res.Get("PupilProfile_Label_EducationLevel_MN");
            }
            else
            {
                labelForEducationLevel = Res.Get("PhysicalTest_Label_EducationLevel");
            }
            SheetData.Data["LabelForEducationLevel"] = labelForEducationLevel;
            EducationLevel Edu = new EducationLevel();
            if (EducationLevelID != 0)
            {
                Edu = EducationLevelBusiness.Find(EducationLevelID);
                SheetData.Data["EducationLevelName"] = Edu.Resolution;
            }
            else
            {
                SheetData.Data["EducationLevelName"] = Res.Get("All");
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_THONGKE_COTSONG);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string ReportName = this.GetReportName(EducationLevelID, ClassID, PeriodExaminationID, ClassName, Edu.Resolution, PhysicalTestConstants.REPORT_SPINE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Sheet = oBook.GetSheet(1);
            // IVTWorksheet Sheet = oBook.GetSheet(2);
            // Sheet.CopyPasteSameSize(Template.GetRange("A1", "J10"), 1, 1);
            Sheet.FillVariableValue(SheetData.Data);

            if (lstSpineTest.Count > 0)
            {
                int StartRow = 12;
                int StartCol = 1;
                for (int i = 0; i < lstSpineTest.Count; i++)
                {
                    SpineTest pt = lstSpineTest[i];
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, "'" + (pt.MonitoringBook.MonitoringDate != null ? pt.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : ""));
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, pt.MonitoringBook.PupilProfile.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 3, "'" + pt.MonitoringBook.PupilProfile.BirthDate.ToString("dd/MM/yyyy"));
                    Sheet.SetCellValue(StartRow + i, StartCol + 4, Utils.CommonConvert.Genre(pt.MonitoringBook.PupilProfile.Genre));
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, pt.MonitoringBook.ClassProfile.DisplayName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 6, pt.ExaminationStraight);
                    Sheet.SetCellValue(StartRow + i, StartCol + 7, pt.ExaminationItalic);
                    Sheet.SetCellValue(StartRow + i, StartCol + 8, CommonConvert.Tox(pt.IsDeformityOfTheSpine));
                    Sheet.SetCellValue(StartRow + i, StartCol + 9, pt.Other);
                    Sheet.CopyPaste(Sheet.GetRange("A" + StartRow.ToString(), "J" + StartRow.ToString()), StartRow + i, 1, true);
                }
            }
            Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;
        }
        [SkipCheckRole]
        public FileResult ExportExcelOverallTest()
        {
            GlobalInfo glo = new GlobalInfo();
            int EducationLevelID = 0;
            int ClassID = 0;
            string ClassName = Res.Get("All");
            int PeriodExaminationID = 0;
            string FullName = "";
            int ItemExaminationID = 0;
            int EvaluationHealth = 0;
            int SkinDiseases = 2;
            bool IsHearDiseases = false;
            bool IsBirthDefect = false;
            ClassProfile cp = new ClassProfile();

            if (Request["EducationLevelID"] != "")
            {
                EducationLevelID = int.Parse(Request["EducationLevelID"]);

            }
            if (Request["ClassID"] != "")
            {
                ClassID = int.Parse(Request["ClassID"]);
                cp = ClassProfileBusiness.Find(ClassID);
                ClassName = cp.DisplayName;
            }
            if (Request["PeriodExaminationID"] != "")
            {
                PeriodExaminationID = int.Parse(Request["PeriodExaminationID"]);
            }
            if (Request["FullName"] != "")
            {
                FullName = Request["FullName"];
            }
            if (Request["ItemExaminationID"] != "")
            {
                ItemExaminationID = int.Parse(Request["ItemExaminationID"]);
            }
            if (Request["EvaluationHealth"] != "")
            {
                EvaluationHealth = int.Parse(Request["EvaluationHealth"]);
            }
            if (Request["SkinDiseases"] == "true")
            {
                SkinDiseases = 1;
            }
            if (Request["IsHearDiseases"] == "true")
            {
                IsHearDiseases = bool.Parse(Request["IsHearDiseases"]);
            }
            if (Request["IsBirthDefect"] == "true")
            {
                IsBirthDefect = bool.Parse(Request["IsBirthDefect"]);

            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = glo.AcademicYearID;
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["FullName"] = FullName;
            SearchInfo["HealthPeriodID"] = PeriodExaminationID;
            SearchInfo["EvaluationHealth"] = EvaluationHealth;
            SearchInfo["SkinDiseases"] = SkinDiseases;
            SearchInfo["IsHeartDiseases"] = IsHearDiseases;
            SearchInfo["IsBirthDefect"] = IsBirthDefect;
            SearchInfo["IsActive"] = true;
            SearchInfo["EducationGrade"] = _globalInfo.AppliedLevel.Value;
            SearchInfo["IsDredging"] = false;

            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["ClassID"] = ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = PeriodExaminationID;
            IQueryable<OverallTest> OverallTestList = from a in OverallTestBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo)
                                                      join c in MonitoringBookBusiness.Search(dic) on a.MonitoringBookID equals c.MonitoringBookID
                                                      join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                      select a;

            List<OverallTest> lstOverallTest = OverallTestList.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).ThenBy(p => p.MonitoringBook.PupilProfile.FullName).ToList();
            AcademicYear ay = AcademicYearBusiness.Find(Global.AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(Global.SchoolID);
            EducationLevel Edu = new EducationLevel();
            string Resolution = "";
            if (EducationLevelID > 0)
            {
                Resolution = EducationLevelBusiness.Find(EducationLevelID).Resolution;
            }
            else
            {
                Resolution = Res.Get("All");
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_THONGKE_TONGQUAT);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string ReportName = this.GetReportName(EducationLevelID, ClassID, PeriodExaminationID, ClassName, Resolution, PhysicalTestConstants.REPORT_OVERALLTEST);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Sheet = oBook.GetSheet(1);
            // IVTWorksheet Sheet = oBook.GetSheet(2);
            //Sheet.CopyPasteSameSize(Template.GetRange("A1", "N10"), 1, 1);

            Sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(sp.SchoolProfileID, _globalInfo.AppliedLevel.Value).ToUpper());
            Sheet.SetCellValue("A3", sp.SchoolName.ToUpper());
            Sheet.SetCellValue("H4", (sp.District != null ? sp.District.DistrictName : "") + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            Sheet.SetCellValue("A8", "THỐNG KÊ KẾT QUẢ KHÁM SỨC KHỎE NĂM HỌC " + ay.DisplayTitle);
            string labelForEducationLevel;
            if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                   || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                labelForEducationLevel = Res.Get("PupilProfile_Label_EducationLevel_MN");
            }
            else
            {
                labelForEducationLevel = Res.Get("PhysicalTest_Label_EducationLevel");
            }
            Sheet.SetCellValue("A9", labelForEducationLevel + ": " + Resolution + " - Lớp: " + (string.IsNullOrEmpty(cp.DisplayName) ? Res.Get("All") : cp.DisplayName) + " - MỤC KHÁM TỔNG QUÁT");

            if (lstOverallTest.Count > 0)
            {
                int StartRow = 12;
                int StartCol = 1;
                for (int i = 0; i < lstOverallTest.Count; i++)
                {
                    OverallTest ot = lstOverallTest[i];
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, "'" + (ot.MonitoringBook.MonitoringDate != null ? ot.MonitoringBook.MonitoringDate.Value.ToString("dd/MM/yyyy") : null));
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, ot.MonitoringBook.PupilProfile.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 3, "'" + ot.MonitoringBook.PupilProfile.BirthDate.ToString("dd/MM/yyyy"));
                    Sheet.SetCellValue(StartRow + i, StartCol + 4, Utils.CommonConvert.Genre(ot.MonitoringBook.PupilProfile.Genre));
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, ot.MonitoringBook.ClassProfile.DisplayName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 6, ot.OverallInternal == 1 ? ot.DescriptionOverallInternall : "Bình thường");
                    Sheet.SetCellValue(StartRow + i, StartCol + 7, ot.OverallForeign == 1 ? ot.DescriptionForeign : "Bình thường");
                    Sheet.SetCellValue(StartRow + i, StartCol + 8, ot.SkinDiseases == 1 ? ot.DescriptionSkinDiseases : "Bình thường");
                    Sheet.SetCellValue(StartRow + i, StartCol + 9, ot.IsHeartDiseases == true ? "x" : null);
                    Sheet.SetCellValue(StartRow + i, StartCol + 10, ot.IsBirthDefect == true ? "x" : null);
                    Sheet.SetCellValue(StartRow + i, StartCol + 11, ot.Other);
                    Sheet.SetCellValue(StartRow + i, StartCol + 12, ot.EvaluationHealth == 1 ? "Loại A" : ot.EvaluationHealth == 2 ? "Loại B" : "Loại C");
                    Sheet.SetCellValue(StartRow + i, StartCol + 13, ot.RequireOfDoctor);
                    Sheet.CopyPaste(Sheet.GetRange("A" + StartRow.ToString(), "N" + StartRow.ToString()), StartRow + i, 1, true);
                }
            }
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;
        }
        [SkipCheckRole]
        public FileResult ExportEarNoseExcel()
        {
            int EducationLevelID = 0;
            int ClassID = 0;
            string ClassName = Res.Get("All");
            int PeriodExaminationID = 0;
            string FullName = "";
            bool IsDeaf = Convert.ToBoolean(Request["IsDeaf"]);
            bool IsSick = Convert.ToBoolean(Request["IsSick"]);

            string name = Request["FullName"];
            if (Request["EducationLevelID"] != "")
            {
                EducationLevelID = int.Parse(Request["EducationLevelID"]);

            }
            if (Request["ClassID"] != "")
            {
                ClassID = int.Parse(Request["ClassID"]);
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                ClassName = cp.DisplayName;
            }
            if (Request["PeriodExaminationID"] != "")
            {
                PeriodExaminationID = int.Parse(Request["PeriodExaminationID"]);
            }
            if (Request["FullName"] != "")
            {
                FullName = name;
            }

            GlobalInfo gl = new GlobalInfo();
            AcademicYear ay = AcademicYearBusiness.Find(Global.AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(Global.SchoolID);
            SupervisingDept sd = new SupervisingDept();
            string province = (sp.District != null ? sp.District.DistrictName : "");
            string SupervisingDeptName = UtilsBusiness.GetSupervisingDeptName(sp.SchoolProfileID, _globalInfo.AppliedLevel.Value);

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_THONGKE_TAIMUIHONG);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int startRow = 13;

            IVTWorksheet sheet = oBook.GetSheet(1);
            SMAS.VTUtils.Excel.Export.IVTRange range = sheet.GetRange("A13", "J13");
            string eduName = EducationLevelID > 0 ? EducationLevelBusiness.Find(EducationLevelID).Resolution : "Tất cả";
            string className = ClassID > 0 ? ClassProfileBusiness.Find(ClassID).DisplayName : "Tất cả";
            string datePlace = province + "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " Năm " + DateTime.Now.Year;
            sheet.SetCellValue(2, 1, SupervisingDeptName);
            sheet.SetCellValue(3, 1, sp.SchoolName);
            sheet.SetCellValue("G4", datePlace);
            sheet.SetCellValue(8, 1, ("Thống kê kết quả khám sức khỏe năm học " + (ay.Year + " - " + (ay.Year + 1))).ToUpper());
            string labelForEducationLevel;
            if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                   || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                labelForEducationLevel = Res.Get("PupilProfile_Label_EducationLevel_MN");
            }
            else
            {
                labelForEducationLevel = Res.Get("PhysicalTest_Label_EducationLevel");
            }
            sheet.SetCellValue(9, 1, (labelForEducationLevel +": " + eduName + " - Lớp: " + className + " - MỤC KHÁM TAI MŨI HỌNG"));
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = gl.AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ClassID"] = ClassID;
            dic["FullName"] = FullName;
            dic["HealthPeriodID"] = PeriodExaminationID;
            dic["IsSick"] = IsSick;
            dic["IsDeaf"] = IsDeaf;
            dic["EducationGrade"] = _globalInfo.AppliedLevel.Value;
            string ReportName = this.GetReportName(EducationLevelID, ClassID, PeriodExaminationID, ClassName, eduName, PhysicalTestConstants.REPORT_EARNOSE);

            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> dict = new Dictionary<string, object>();
            dict["SchoolID"] = _globalInfo.SchoolID.Value;
            dict["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dict["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dict["ClassID"] = ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dict), academicYearObj, _globalInfo.Semester.Value);
            dict["EducationLevelID"] = EducationLevelID;
            dict["HealthPeriodID"] = PeriodExaminationID;
            IQueryable<ENTTest> ENTTestList = from a in ENTTestBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                              join c in MonitoringBookBusiness.Search(dict) on a.MonitoringBookID equals c.MonitoringBookID
                                              join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                              select a;

            List<ENTTest> lst = ENTTestList.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).ThenBy(p => p.MonitoringBook.PupilProfile.FullName).ToList();
            for (int i = 0; i < lst.Count; i++)
            {
                if (startRow + i > 12)
                {
                    sheet.CopyPasteSameRowHeigh(range, startRow);
                }
                sheet.SetCellValue(startRow, 1, i + 1);
                sheet.SetCellValue(startRow, 2, "'" + (lst[i].MonitoringBook.MonitoringDate != null ? lst[i].MonitoringBook.MonitoringDate.Value.Date.ToString("dd/MM/yyyy") : ""));
                sheet.SetCellValue(startRow, 3, lst[i].MonitoringBook.PupilProfile.FullName);
                sheet.SetCellValue(startRow, 4, "'" + lst[i].MonitoringBook.PupilProfile.BirthDate.ToString("dd/MM/yyyy"));
                string gender = lst[i].MonitoringBook.PupilProfile.Genre == SystemParamsInFile.GENRE_MALE ? "Nam" : "Nữ";
                sheet.SetCellValue(startRow, 5, gender);
                sheet.SetCellValue(startRow, 6, lst[i].MonitoringBook.PupilProfile.ClassProfile.DisplayName);
                sheet.SetCellValue(startRow, 7, lst[i].LCapacityHearing.HasValue ? lst[i].LCapacityHearing + " dB" : "");
                sheet.SetCellValue(startRow, 8, lst[i].RCapacityHearing.HasValue ? lst[i].RCapacityHearing + " dB" : "");
                string deaf = lst[i].IsDeaf == true ? "x" : "";
                sheet.SetCellValue(startRow, 9, deaf);
                string sick = lst[i].IsSick.Value ? lst[i].Description : "Bình thường";
                sheet.SetCellValue(startRow, 10, sick);
                startRow++;
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;
        }

        private string GetReportName(int EducationLevelID, int ClassID, int PeriodExaminationID, string ClassName, string EducationName, string Report_typeName)
        {
            string PeriodExaminationName = PeriodExaminationID == 1 ? PhysicalTestConstants.PERIODEXAMINATIONNAME_I : PeriodExaminationID == 2 ? PhysicalTestConstants.PERIODEXAMINATIONNAME_II : "";
            string AllSchool = "";
            string ReportName = "";
            if (EducationLevelID == 0 && ClassID == 0)
            {
                AllSchool = "";
            }
            else if (EducationLevelID > 0 && ClassID == 0)
            {
                AllSchool = Utils.Utils.StripVNSign(EducationName.Replace(" ", ""));
            }
            else if (EducationLevelID > 0 && ClassID > 0)
            {
                AllSchool = ClassName;

            }

            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                ReportName = "HS" + "_TH" + (!string.IsNullOrEmpty(Report_typeName) ? ("_" + Report_typeName) : "") + (!string.IsNullOrEmpty(AllSchool) ? ("_" + AllSchool) : "") + (!string.IsNullOrEmpty(PeriodExaminationName) ? ("_" + PeriodExaminationName) : "") + ".xls";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                ReportName = "HS" + "_THCS" + (!string.IsNullOrEmpty(Report_typeName) ? ("_" + Report_typeName) : "") + (!string.IsNullOrEmpty(AllSchool) ? ("_" + AllSchool) : "") + (!string.IsNullOrEmpty(PeriodExaminationName) ? ("_" + PeriodExaminationName) : "") + ".xls";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                ReportName = "HS" + "_THPT" + (!string.IsNullOrEmpty(Report_typeName) ? ("_" + Report_typeName) : "") + (!string.IsNullOrEmpty(AllSchool) ? ("_" + AllSchool) : "") + (!string.IsNullOrEmpty(PeriodExaminationName) ? ("_" + PeriodExaminationName) : "") + ".xls";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE)
            {
                ReportName = "HS" + "_NT" + (!string.IsNullOrEmpty(Report_typeName) ? ("_" + Report_typeName) : "") + (!string.IsNullOrEmpty(AllSchool) ? ("_" + AllSchool) : "") + (!string.IsNullOrEmpty(PeriodExaminationName) ? ("_" + PeriodExaminationName) : "") + ".xls";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ReportName = "HS" + "_TH" + (!string.IsNullOrEmpty(Report_typeName) ? ("_" + Report_typeName) : "") + (!string.IsNullOrEmpty(AllSchool) ? ("_" + AllSchool) : "") + (!string.IsNullOrEmpty(PeriodExaminationName) ? ("_" + PeriodExaminationName) : "") + ".xls";
            }
            return ReportName;
        }
        #endregion

    }
}





