﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.TransferDataArea.Models;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using log4net;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.TransferDataArea.Controllers
{
    public class TransferDataController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IClassForwardingBusiness ClassForwardingBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IVMarkRecordBusiness VMarkRecordBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IVJudgeRecordBusiness VJudgeRecordBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IPupilRepeatedBusiness PupilRepeatedBusiness;
        private readonly IPupilOfSchoolBusiness PupilOfSchoolBusiness;
        private readonly IVPupilRankingBusiness VPupilRankingBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IDevelopmentOfChildrenBusiness DevelopmentOfChildrenBusiness;
        private readonly IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private readonly IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness;
        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness;
        private readonly IEvaluationCommentsBusiness EvaluationCommentsBusiness;

        private static ILog iLog;
        private const int LEN_LOP = 2;
        private const int O_LAI_LOP = 1;
        private const int MAX_EDUCATION_LEVEL_PRIMARY = 5;

        public TransferDataController(IAcademicYearBusiness AcademicYearBusiness
            , IVPupilRankingBusiness VPupilRankingBusiness
            , IVJudgeRecordBusiness VJudgeRecordBusiness
            , IVMarkRecordBusiness VMarkRecordBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IClassForwardingBusiness ClassForwardingBusiness
            , IMarkRecordBusiness MarkRecordBusiness
            , IJudgeRecordBusiness JudgeRecordBusiness
            , IPupilProfileBusiness PupilProfileBusiness
            , IPupilRankingBusiness PupilRankingBusiness
            , IPupilRepeatedBusiness PupilRepeatedBusiness
            , IPupilOfSchoolBusiness PupilOfSchoolBusiness
            , IPupilAbsenceBusiness PupilAbsenceBusiness
            , IDevelopmentOfChildrenBusiness DevelopmentOfChildrenBusiness
            , IMarkRecordHistoryBusiness MarkRecordHistoryBusiness
            , IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness
            , ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness
            , ISummedEvaluationBusiness SummedEvaluationBusiness
            , IEvaluationCommentsBusiness EvaluationCommentsBusiness)
        {
            this.VJudgeRecordBusiness = VJudgeRecordBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.VMarkRecordBusiness = VMarkRecordBusiness;
            this.ClassForwardingBusiness = ClassForwardingBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.PupilRepeatedBusiness = PupilRepeatedBusiness;
            this.PupilOfSchoolBusiness = PupilOfSchoolBusiness;
            this.VPupilRankingBusiness = VPupilRankingBusiness;
            this.PupilAbsenceBusiness = PupilAbsenceBusiness;
            this.DevelopmentOfChildrenBusiness = DevelopmentOfChildrenBusiness;
            this.MarkRecordHistoryBusiness = MarkRecordHistoryBusiness;
            this.JudgeRecordHistoryBusiness = JudgeRecordHistoryBusiness;
            this.SummedEndingEvaluationBusiness = SummedEndingEvaluationBusiness;
            this.SummedEvaluationBusiness = SummedEvaluationBusiness;
            this.EvaluationCommentsBusiness = EvaluationCommentsBusiness;
        }


        /// <summary>
        /// GET: /TransferDataArea/TransferData/
        /// </summary>
        public ActionResult Index()
        {
            AcademicYear currentAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            // Lay doi tuong nam hoc truoc
            AcademicYear previousAcademicYear = this.AcademicYearBusiness.All.FirstOrDefault(ay => ay.SchoolID == currentAcademicYear.SchoolID && ay.Year == currentAcademicYear.Year - 1);
            if (previousAcademicYear == null)
            {
                ViewBag.FatalError = "Năm học trước không tồn tại. Không thể thực hiện kết chuyển dữ liệu.";
            }
            return View();
        }

        /// <summary>
        /// GET: /TransferDataArea/TransferClassForwarding/
        /// Neu EducationLevelID == 0 thi co nghia la tim tat ca cac khoi trong cap hoc duoc chon
        /// </summary>
        public PartialViewResult TransferClassForwarding()
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            int permission = this.GetMenupermission("TransferData", globalInfo.UserAccountID, globalInfo.IsAdmin);
            ViewData[SystemParamsInFile.PERMISSION_VIEW] = permission >= 1;
            ViewData[SystemParamsInFile.PERMISSION_CREATE] = permission >= 2;
            ViewData[SystemParamsInFile.PERMISSION_UPDATE] = permission >= 3;
            ViewData[SystemParamsInFile.PERMISSION_DELETE] = permission >= 4;
            // Lay danh sach cac khoi hoc
            List<EducationLevel> lsEducationLevel = globalInfo.EducationLevels;
            // Bo giá trị cuối cấp trong danh sach cac khoi ket chuyen
            int maxEdu = lsEducationLevel.Max(o => o.EducationLevelID);
            List<EducationLevel> lstEducationLevelTransfer = lsEducationLevel.Where(o => o.EducationLevelID != maxEdu).ToList();
            // Truyen danh sach khoi hoc co lop ket chuyen sang view
            ViewData[TransferDataConstant.LS_LISTEDUCATIONLEVEL] = new SelectList(lstEducationLevelTransfer, "EducationLevelID", "Resolution", lstEducationLevelTransfer[0].EducationLevelID);
            //anhnph1_Lay du liệu cho comboboc Lop
            List<ClassForwardViewModel> lstClassForwardViewModel = getListClassForwarding(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstEducationLevelTransfer[0].EducationLevelID);
            ViewData[TransferDataConstant.LISTCLASS] = new SelectList(lstClassForwardViewModel, "OldClassID", "OldClassName");

            ViewBag.IsInSemester1 = this.IsInSemester1();
            ViewBag.IsInAcademicYear = this.IsInAcademicYear();

            bool InTimeToTransferOrUntransfer;
            if (globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN || globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                InTimeToTransferOrUntransfer = this.IsInAcademicYear();
            }
            else
            {
                InTimeToTransferOrUntransfer = this.IsInSemester1();
            }
            ViewBag.InTimeToTransferOrUntransfer = InTimeToTransferOrUntransfer;
            return PartialView("TransferClassForwarding");
        }

        public PartialViewResult SearchTab1(int EducationLevelID = 0)
        {
            List<ClassForwardViewModel> lstClassForwardViewModel = getListClassForwarding(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, EducationLevelID);
            // Danh sach cac lop trong nam hien tai
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
              {
                {"EducationLevelID",(EducationLevelID + 1)}
                ,{"AcademicYearID",_globalInfo.AcademicYearID}
              }).OrderBy(o => o.EducationLevelID).ToList();

            lstClassProfile.ForEach(p => p.OrderNumber = (p.OrderNumber.HasValue ? p.OrderNumber.Value : 0));

            List<ClassinGeneral> lsCG = lstClassProfile.OrderBy(c => c.OrderNumber).ThenBy(p=>p.DisplayName)
                                            .Select(o => new ClassinGeneral { ID = o.ClassProfileID, Name = o.DisplayName })
                                            .ToList();
            // Truyen cac du lieu den view
            ViewData[TransferDataConstant.LIST_CLASS_FORWARD] = lstClassForwardViewModel;
            ViewData[TransferDataConstant.LIST_CLASS_UP] = lsCG;
            ViewData[TransferDataConstant.LISTCLASS] = new SelectList(lstClassForwardViewModel, "OldClassID", "OldClassName");
            return PartialView("_ListClassForward");
        }

        private List<ClassForwardViewModel> getListClassForwarding(int SchoolID, int AcademicYearID, int EducationLevelID)
        {          
            bool isMamNon = _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN;
            // Cap mam non thi cho phep Ket chuyen, Huy ket chuyen neu con trong nam hoc hien tai
            // Cap 1, 2, 3 thi cho phep Ket chuyen, Huy ket chuyen neu con trong hoc ky 1
            bool inTimeToTransferAndUntransfer = isMamNon ? this.IsInAcademicYear() : this.IsInSemester1();
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);



            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

            int previousAcademicYearID = this.AcademicYearBusiness.All.Where(ay => ay.SchoolID == SchoolID && ay.Year == acaYear.Year - 1 && ay.IsActive == true).Select(p => p.AcademicYearID).FirstOrDefault();
            int CurrentAcademicYearID = _globalInfo.AcademicYearID.Value;


            // Danh sach hoc sinh thuoc dien len lop
            List<ClassForwardingBO> lstClassForwardingBO = ClassForwardingBusiness.GetTransferData(EducationLevelID, SchoolID, AcademicYearID);
            //List<int> lstOldClass = lstClassForwardingBO.Select(p => p.OldClassID).ToList();

            // Danh sach hoc sinh da duoc ket chuyen len lop
            IQueryable<PupilOfClass> lstPupilsTransferedClass = this.getListOfTransferedPupils(SchoolID, previousAcademicYearID, CurrentAcademicYearID, EducationLevelID);

            // Danh sach cac lop nam hoc truoc
            List<ClassForwardViewModel> lstClassForwardViewModel = new List<ClassForwardViewModel>();
            // Chuyen thanh cac doi tuong ViewModel
            List<PupilForwardViewModel> lstPupilForwardViewModel = new List<PupilForwardViewModel>();
            //List<PupilObjectModel> iqMRHIS = new List<PupilObjectModel>();
            //List<PupilObjectModel> iqJRHIS = new List<PupilObjectModel>();
            List<PupilObjectModel> lstMarkRecord = new List<PupilObjectModel>();
            List<PupilObjectModel> lstJudgeRecord = new List<PupilObjectModel>();
            List<PupilObjectModel> lstSumedEnding = new List<PupilObjectModel>();
            List<PupilObjectModel> lstSummedEvaluation = new List<PupilObjectModel>();
            List<PupilObjectModel> lstDevelopmentOfChildren = new List<PupilObjectModel>();

            #region lay danh sach du lieu de check enabled checkbox huy ket chuyen
            List<PupilObjectModel> lstPupilAbsence = this.getListOfPupilAbsenceOfPupils(null, AcademicYearID, partitionId, EducationLevelID);
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE ||
                _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN
                )
            {
                lstDevelopmentOfChildren = this.getListOfDevelopmentOfChildrenOfPupils(null, AcademicYearID, EducationLevelID);
            }


            //if(_globalInfo.AppliedLevel>=GlobalConstants.APPLIED_LEVEL_SECONDARY)
            //iqMRHIS = this.getListOfMarkRecordHistoryOfPupils(lstPupilID, currentAcademicYear.AcademicYearID, partitionId);
            //iqJRHIS = this.getListOfJudgeRecordHistoryOfPupils(lstPupilID, currentAcademicYear.AcademicYearID, partitionId);
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                lstSumedEnding = this.getListSummendEndingEvalutionOfPupilPrimary(null, AcademicYearID, partitionId);
                lstSummedEvaluation = this.getListSummedEvaluationOfPupilPrimary(null, AcademicYearID, partitionId);
            }
            else if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY ||
                    _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                lstSumedEnding = this.getListOfMarkRecordOfPupils(null, AcademicYearID, partitionId, EducationLevelID);
                lstSumedEnding = this.getListOfJudgeRecordOfPupils(null, AcademicYearID, partitionId, EducationLevelID);
            }
            #endregion
            foreach (var classForwardingBO in lstClassForwardingBO)
            {
                ClassForwardViewModel classForwardViewModel = new ClassForwardViewModel();
                classForwardViewModel.OldClassID = classForwardingBO.OldClassID;
                classForwardViewModel.OldClassName = classForwardingBO.OldClassName;
                classForwardViewModel.TotalPupil = classForwardingBO.TotalPupil;
                classForwardViewModel.TotalMove = classForwardingBO.TotalMove;
                classForwardViewModel.NewClassID = classForwardingBO.NewClassID;
                classForwardViewModel.TotalMoved = classForwardingBO.TotalMoved;
                // Kiem tra xem lop co duoc phep ket chuyen khong
                if (classForwardingBO.NewClassID > 0)
                {
                    classForwardViewModel.DisableTransferData = true;
                    classForwardViewModel.TitleForTransferData = "Lớp đã được kết chuyển";
                }
                else if (!inTimeToTransferAndUntransfer)
                {
                    classForwardViewModel.DisableTransferData = true;
                    classForwardViewModel.TitleForTransferData = isMamNon ? "Không được kết chuyển. Chỉ được kết chuyển trong thời gian năm học."
                                                                    : "Không được kết chuyển. Chỉ được kết chuyển trong thời gian học kỳ I.";
                }
                else
                {
                    classForwardViewModel.DisableTransferData = false;
                    classForwardViewModel.TitleForTransferData = "Chọn lớp để kết chuyển";
                }

                // Kiem tra xem lop co duoc phep huy ket chuyen khong
                if (classForwardingBO.NewClassID <= 0) // Kiem tra xem lớp đã kết chuyển chưa
                {
                    classForwardViewModel.DisableUntransferData = true;
                    classForwardViewModel.TitleForUntransferData = "Lớp chưa được kết chuyển";
                    lstClassForwardViewModel.Add(classForwardViewModel);
                    continue;
                }
                // Kiem tra co con trong thoi gian cho phep huy ket chuyen khong.
                if (!inTimeToTransferAndUntransfer)
                {
                    classForwardViewModel.DisableUntransferData = true;
                    classForwardViewModel.TitleForUntransferData = isMamNon ? "Không được hủy kết chuyển. Chỉ được hủy kết chuyển trong thời gian năm học."
                                                                    : "Không được hủy kết chuyển. Chỉ được hủy kết chuyển trong thời gian học kỳ I.";
                    lstClassForwardViewModel.Add(classForwardViewModel);
                    continue;
                }

                // Danh sach da ket chuyen trong lop dang duoc xet
                List<int> lstPupilID = lstPupilsTransferedClass.Where(p => p.ClassID == classForwardingBO.OldClassID).Select(p => p.PupilID).Distinct().ToList();
                int ClassIDNew = classForwardingBO.NewClassID;

                // Kiem tra du lieu diem danh

                int soluogbanghidiemdanhcuacachocsinhdaketchuyen = lstPupilAbsence.Count(c => c.ClassId == ClassIDNew);
                if (soluogbanghidiemdanhcuacachocsinhdaketchuyen > 0)
                {
                    classForwardViewModel.DisableUntransferData = true;
                    classForwardViewModel.TitleForUntransferData = "Lớp đã có dữ liệu điểm danh của trẻ, thầy/cô không thể hủy kết chuyển";
                    lstClassForwardViewModel.Add(classForwardViewModel);
                    continue;
                }
                // Kiem tra du lieu danh gia su phat trien
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
                {
                    int soluongbanghidanhgiasuphattriencuacachocsinhdaketchuyenlenlop = lstDevelopmentOfChildren.Count(c => c.ClassId == ClassIDNew);
                    if (soluongbanghidanhgiasuphattriencuacachocsinhdaketchuyenlenlop > 0)
                    {
                        classForwardViewModel.DisableUntransferData = true;
                        classForwardViewModel.TitleForUntransferData = "Lớp đã có dữ liệu đánh giá sự phát triển, thầy/cô không thể hủy kết chuyển";
                        lstClassForwardViewModel.Add(classForwardViewModel);
                        continue;
                    }
                }

                //neu la hoc sinh cap 1
                if (EducationLevelID <= MAX_EDUCATION_LEVEL_PRIMARY)
                {
                    int countSEE = lstSumedEnding.Count(c => c.ClassId == ClassIDNew);
                    if (countSEE > 0)
                    {
                        classForwardViewModel.DisableUntransferData = true;
                        classForwardViewModel.TitleForUntransferData = "Lớp đã có dữ liệu điểm, thầy/cô không thể hủy kết chuyển";
                        lstClassForwardViewModel.Add(classForwardViewModel);
                        continue;
                    }
                    int countSE = lstSummedEvaluation.Count(c => c.ClassId == ClassIDNew);
                    if (countSE > 0)
                    {
                        classForwardViewModel.DisableUntransferData = true;
                        classForwardViewModel.TitleForUntransferData = "Lớp đã có dữ liệu điểm, thầy/cô không thể hủy kết chuyển";
                        lstClassForwardViewModel.Add(classForwardViewModel);
                        continue;
                    }
                }

                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY ||
                    _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {

                    // Kiem tra diem mon tinh diem

                    int soluongbanghidiemmontinhdiemcuacachocsinhdaketchuyen = lstMarkRecord.Count(c => c.ClassId == ClassIDNew);
                    if (soluongbanghidiemmontinhdiemcuacachocsinhdaketchuyen > 0)
                    {
                        classForwardViewModel.DisableUntransferData = true;
                        classForwardViewModel.TitleForUntransferData = "Lớp đã có dữ liệu điểm, thầy/cô không thể hủy kết chuyển";
                        lstClassForwardViewModel.Add(classForwardViewModel);
                        continue;
                    }
                    // Kiem tra diem mon nhan xet
                    int soluongbanghidiemmonnhanxetcuacachocsinhdaketchuyen = lstJudgeRecord.Count(c => c.ClassId == ClassIDNew);
                    if (soluongbanghidiemmonnhanxetcuacachocsinhdaketchuyen > 0)
                    {
                        classForwardViewModel.DisableUntransferData = true;
                        classForwardViewModel.TitleForUntransferData = "Lớp đã có dữ liệu điểm, thầy/cô không thể hủy kết chuyển";
                        lstClassForwardViewModel.Add(classForwardViewModel);
                        continue;
                    }
                }
                // Lop duoc phep huy ket chuyen
                classForwardViewModel.DisableUntransferData = false;
                classForwardViewModel.TitleForUntransferData = "Chọn lớp để hủy kết chuyển";
                lstClassForwardViewModel.Add(classForwardViewModel);
            }
            return lstClassForwardViewModel;
        }
        #region Kiem tra du lieu
        private IQueryable<PupilOfClass> getListOfTransferedPupils(int schoolID, int previousAcademicYearID, int currentAcademicYearID, int previousEducationLevelID)
        {
            IQueryable<PupilOfClass> lstPupilsTransferedClass = from pp in this.PupilProfileBusiness.All
                                                                join oldpoc in this.PupilOfClassBusiness.All on pp.PupilProfileID equals oldpoc.PupilID
                                                                join newpoc in this.PupilOfClassBusiness.All on pp.PupilProfileID equals newpoc.PupilID
                                                                where oldpoc.AcademicYearID == previousAcademicYearID
                                                                    && oldpoc.SchoolID == schoolID
                                                                    && oldpoc.ClassProfile.EducationLevelID == previousEducationLevelID
                                                                    && oldpoc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING  // Chon hoc sinh dang hoc trong lop hoc nam truoc
                                                                    && newpoc.AcademicYearID == currentAcademicYearID
                                                                    && newpoc.SchoolID == schoolID
                                                                    && newpoc.ClassProfile.EducationLevelID == previousEducationLevelID + 1 // Dieu kien chon lop nam hoc hien tai
                                                                select oldpoc;
            return lstPupilsTransferedClass;
        }
        private List<PupilObjectModel> getListOfPupilAbsenceOfPupils(List<int> lstPupilID, int CurrentAcademicYearID, int partitionId, int educationlevelId, int ClassIDNew = 0)
        {
            var query = from pa in this.PupilAbsenceBusiness.AllNoTracking
                        where pa.AcademicYearID == CurrentAcademicYearID
                        && pa.Last2digitNumberSchool == partitionId
                        && pa.EducationLevelID == educationlevelId
                        select pa;
            if (ClassIDNew > 0)
            {
                query = query.Where(c => c.ClassID == ClassIDNew);
            }
            if (lstPupilID != null && lstPupilID.Count > 0)
            {
                query = query.Where(c => lstPupilID.Contains(c.PupilID));
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.PupilAbsenceID by q.PupilID into g
                                                      select new PupilObjectModel
                                                 {
                                                     PupilId = g.Key,
                                                     TotalRecord = g.Count(),
                                                     ClassId = 0
                                                 }).ToList();
                return lstpupilObj;
            }
            else
            {
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.PupilAbsenceID by q.ClassID into g
                                                      select new PupilObjectModel
                                                 {
                                                     ClassId = g.Key,
                                                     TotalRecord = g.Count(),
                                                     PupilId = 0
                                                 }).ToList();
                return lstpupilObj;
            }
        }
        private List<PupilObjectModel> getListOfDevelopmentOfChildrenOfPupils(List<int> lstPupilID, int CurrentAcademicYearID, int educationlevelId, int ClassIDNew = 0)
        {
            var query = from dc in this.DevelopmentOfChildrenBusiness.All
                        where dc.AcademicYearID == CurrentAcademicYearID
                        && (dc.ClassID == ClassIDNew || ClassIDNew == 0)
                        select dc;
            if (ClassIDNew > 0)
            {
                query = query.Where(c => c.ClassID == ClassIDNew);
            }
            if (lstPupilID != null && lstPupilID.Count > 0)
            {
                query = query.Where(c => lstPupilID.Contains(c.PupilID));
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.DevelopmentOfChildrenID by q.PupilID into g
                                                      select new PupilObjectModel
                                                 {
                                                     PupilId = g.Key,
                                                     TotalRecord = g.Count(),
                                                     ClassId = 0
                                                 }).ToList();
                return lstpupilObj;
            }
            else
            {
                query = query.Where(c => c.ClassProfile.EducationLevelID == educationlevelId);
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.DevelopmentOfChildrenID by q.ClassID into g
                                                      select new PupilObjectModel
                                                 {
                                                     ClassId = g.Key,
                                                     TotalRecord = g.Count(),
                                                     PupilId = 0
                                                 }).ToList();
                return lstpupilObj;
            }
        }
        private List<PupilObjectModel> getListOfMarkRecordHistoryOfPupils(List<int> lstPupilID, int CurrentAcademicYearID, int partitionId, int educationlevelId, int ClassIDNew = 0)
        {
            var query = from mr in this.MarkRecordHistoryBusiness.All
                        where mr.AcademicYearID == CurrentAcademicYearID
                        && mr.Last2digitNumberSchool == partitionId
                        select mr;
            if (ClassIDNew > 0)
            {
                query = query.Where(c => c.ClassID == ClassIDNew);
        }
            if (lstPupilID != null && lstPupilID.Count > 0)
            {
                query = query.Where(c => lstPupilID.Contains(c.PupilID));
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.MarkRecordID by q.PupilID into g
                                                      select new PupilObjectModel
                                                 {
                                                     PupilId = g.Key,
                                                     TotalRecord = g.Count(),
                                                     ClassId = 0
                                                 }).ToList();
                return lstpupilObj;
            }
            else
            {
                query = query.Where(c => c.ClassProfile.EducationLevelID == educationlevelId);
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.MarkRecordID by q.ClassID into g
                                                      select new PupilObjectModel
                                                 {
                                                     ClassId = g.Key,
                                                     TotalRecord = g.Count(),
                                                     PupilId = 0
                                                 }).ToList();
                return lstpupilObj;
            }
        }
        private List<PupilObjectModel> getListOfJudgeRecordHistoryOfPupils(List<int> lstPupilID, int CurrentAcademicYearID, int partitionId, int educationlevelId, int ClassIDNew = 0)
        {
            var query = from jrhs in this.JudgeRecordHistoryBusiness.All
                        where jrhs.AcademicYearID == CurrentAcademicYearID
                        && jrhs.Last2digitNumberSchool == partitionId
                        select jrhs;
            if (ClassIDNew > 0)
            {
                query = query.Where(c => c.ClassID == ClassIDNew);
            }
            if (lstPupilID != null && lstPupilID.Count > 0)
            {
                query = query.Where(c => lstPupilID.Contains(c.PupilID));
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.JudgeRecordID by q.PupilID into g
                                                      select new PupilObjectModel
                                                 {
                                                     PupilId = g.Key,
                                                     TotalRecord = g.Count()
                                                 }).ToList();
                return lstpupilObj;
        }
            else
            {
                query = query.Where(c => c.ClassProfile.EducationLevelID == educationlevelId);
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.JudgeRecordID by q.ClassID into g
                                                      select new PupilObjectModel
                                                 {
                                                     ClassId = g.Key,
                                                     TotalRecord = g.Count(),
                                                     PupilId = 0
                                                 }).ToList();
                return lstpupilObj;
            }


        }
        private List<PupilObjectModel> getListOfMarkRecordOfPupils(List<int> lstPupilID, int CurrentAcademicYearID, int partitionId, int educationlevelId, int ClassIDNew = 0)
        {
            var query = from mr in this.MarkRecordBusiness.All
                        where mr.AcademicYearID == CurrentAcademicYearID
                         && mr.Last2digitNumberSchool == partitionId
                        select mr;
            if (ClassIDNew > 0)
            {
                query = query.Where(c => c.ClassID == ClassIDNew);
        }
            if (lstPupilID != null && lstPupilID.Count > 0)
            {
                query = query.Where(c => lstPupilID.Contains(c.PupilID));
                List<PupilObjectModel> lstMark = (from q in query
                                             group q.MarkRecordID by q.PupilID into g
                                                  select new PupilObjectModel
                                             {
                                                 PupilId = g.Key,
                                                 TotalRecord = g.Count(),
                                                 ClassId = 0
                                             }).ToList();
                return lstMark;
            }
            else
            {
                query = query.Where(c => c.ClassProfile.EducationLevelID == educationlevelId);
                List<PupilObjectModel> lstpupilObj = (from q in query
                                                 group q.MarkRecordID by q.ClassID into g
                                                      select new PupilObjectModel
                                                 {
                                                     ClassId = g.Key,
                                                     TotalRecord = g.Count(),
                                                     PupilId = 0
                                                 }).ToList();
                return lstpupilObj;
            }

        }
        private List<PupilObjectModel> getListOfJudgeRecordOfPupils(List<int> lstPupilID, int CurrentAcademicYearID, int partitionId, int educationlevelId, int ClassIDNew = 0)
        {
            var query = from jr in this.JudgeRecordBusiness.All
                        where jr.AcademicYearID == CurrentAcademicYearID
                        && jr.Last2digitNumberSchool == partitionId
                        select jr;
            if (ClassIDNew > 0)
            {
                query = query.Where(c => c.ClassID == ClassIDNew);
            }
            if (lstPupilID != null && lstPupilID.Count > 0)
            {
                query = query.Where(c => lstPupilID.Contains(c.PupilID));
                List<PupilObjectModel> lstMark = (from q in query
                                             group q.JudgeRecordID by q.PupilID into g
                                                  select new PupilObjectModel
                                             {
                                                 PupilId = g.Key,
                                                 TotalRecord = g.Count()
                                             }).ToList();
                return lstMark;
            }
            else
            {
                query = query.Where(c => c.ClassProfile.EducationLevelID == educationlevelId);
                List<PupilObjectModel> lstMark = (from q in query
                                             group q.JudgeRecordID by q.ClassID into g
                                                  select new PupilObjectModel
                                             {
                                                 ClassId = g.Key,
                                                 TotalRecord = g.Count(),
                                                 PupilId = 0
                                             }).ToList();
                return lstMark;
        }
        }
        private List<PupilObjectModel> getListSummendEndingEvalutionOfPupilPrimary(List<int> lstPupilID, int CurrentAcademicYearID, int partitionId, int ClassIDNew = 0)
        {
            var query = from see in this.SummedEndingEvaluationBusiness.All
                        where see.AcademicYearID == CurrentAcademicYearID
                        && see.SummedEndingEvaluationID == partitionId
                        select see;
            if (ClassIDNew > 0)
            {
                query = query.Where(c => c.ClassID == ClassIDNew);
            }
            if (lstPupilID != null && lstPupilID.Count > 0)
            {
                query = query.Where(c => lstPupilID.Contains(c.PupilID));
                List<PupilObjectModel> lstMark = (from q in query
                                             group q.SummedEndingEvaluationID by q.PupilID into g
                                                  select new PupilObjectModel
                                             {
                                                 PupilId = g.Key,
                                                 TotalRecord = g.Count(),
                                                 ClassId = 0
                                             }).ToList();
                return lstMark;
            }
            else
            {
                List<PupilObjectModel> lstMark = (from q in query
                                             group q.SummedEndingEvaluationID by q.ClassID into g
                                                  select new PupilObjectModel
                                             {
                                                 ClassId = g.Key,
                                                 TotalRecord = g.Count(),
                                                 PupilId = 0
                                             }).ToList();
                return lstMark;
            }
        }
        private List<PupilObjectModel> getListSummedEvaluationOfPupilPrimary(List<int> lstPupilID, int CurrentAcademicYearID, int partitionId, int ClassIDNew = 0)
        {
            var query = from see in this.SummedEvaluationBusiness.All
                        where see.AcademicYearID == CurrentAcademicYearID
                        select see;
            if (ClassIDNew > 0)
            {
                query = query.Where(c => c.ClassID == ClassIDNew);
            }
            if (lstPupilID != null && lstPupilID.Count > 0)
            {
                query = query.Where(c => lstPupilID.Contains(c.PupilID));
                List<PupilObjectModel> lstMark = (from q in query
                                             group q.SummedEvaluationID by q.PupilID into g
                                                  select new PupilObjectModel
                                             {
                                                 PupilId = g.Key,
                                                 TotalRecord = g.Count()
                                             }).ToList();
                return lstMark;
            }
            else
            {
                List<PupilObjectModel> lstMark = (from q in query
                                             group q.SummedEvaluationID by q.ClassID into g
                                                  select new PupilObjectModel
                                             {
                                                 ClassId = g.Key,
                                                 TotalRecord = g.Count(),
                                                 PupilId = 0
                                             }).ToList();
                return lstMark;
            }
        }

        #endregion

        public PartialViewResult SearchTab2(int EducationLevelID = 0, int ClassID = 0)
        {
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            AcademicYear currentAcademicYear = AcademicYearBusiness.Find(academicYearId);
            AcademicYear previousAcademicYear = AcademicYearBusiness.SearchBySchool(schoolId, new Dictionary<string, object>() { { "Year", currentAcademicYear.Year - 1 } }).FirstOrDefault();
            if (previousAcademicYear == null) throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Lay danh sach cac lop trong nam hoc hien tai
            IDictionary<string, object> paras = new Dictionary<string, object>();
            paras.Add("EducationLevelID", EducationLevelID);
            paras.Add("AcademicYearID", academicYearId);
            IQueryable<ClassProfile> lstClassStayTemp = ClassProfileBusiness.SearchBySchool(schoolId, paras)
                .OrderBy(o => o.EducationLevelID).ThenBy(c => c.OrderNumber).ThenBy(o => o.DisplayName);
            List<ClassinGeneral> lstClassStay = lstClassStayTemp.Select(o => new ClassinGeneral { ID = o.ClassProfileID, Name = o.DisplayName }).ToList();
            ViewData[TransferDataConstant.LST_CLASS_STAY] = lstClassStay;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            bool InTimeToTransferOrUntransfer;
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                InTimeToTransferOrUntransfer = this.IsInAcademicYear();
            }
            else
            {
                InTimeToTransferOrUntransfer = this.IsInSemester1();
            }
            ViewBag.InTimeToTransferOrUntransfer = InTimeToTransferOrUntransfer;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Lay danh sach cac hoc sinh o lai trong lop duoc chon.
            var querylstPupilStayClass = PupilRepeatedBusiness.GetPupilRepeated(schoolId, academicYearId, EducationLevelID, ClassID);

   
            //var lstPupilStayClass = querylstPupilStayClass.ToList();
            // Trong danh sach tren, Lay ra danh sach cac hoc sinh da duoc ket chuyen len lop. 
            // Neu hoc sinh o lai lop da duoc ket chuyen len lop (do truong chon ket chuyen tat ca) thi khong hien thi trong danh sach ket chuyen luu ban nua
            var queryPupilsWereTransfered = (from pr in querylstPupilStayClass
                                             join poc in this.PupilOfClassBusiness.SearchBySchool(schoolId) on pr.PupilID equals poc.PupilID
                                             join cp in ClassProfileBusiness.SearchByAcademicYear(academicYearId) on poc.ClassID equals cp.ClassProfileID
                                             where poc.SchoolID == currentAcademicYear.SchoolID && poc.AcademicYearID == currentAcademicYear.AcademicYearID
                                                   && cp.EducationLevelID == EducationLevelID + 1 // Danh sach da ket chuyen len nen duoc tim tren mot khoi
                                             select pr).ToList();
            //var lstPupilWereTransfered = queryPupilsWereTransfered.ToList();
            // Danh sach cac hoc sinh se ket chuyen luu ban
            var queryPupilsRepeated = querylstPupilStayClass.Except(queryPupilsWereTransfered);
            var lstPupilsRepeated = queryPupilsRepeated
                .OrderBy(o => o.EducationLevelID).ThenBy(o => o.OldClassName)
                .ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName)
                .ToList();
            // Chuyen thanh cac doi tuong ViewModel
            List<PupilRepeatedViewModel> lstPupilRepeatedViewModel = new List<PupilRepeatedViewModel>();
            foreach (var pupilStayClass in lstPupilsRepeated)
            {
                PupilRepeatedViewModel pupilRepeatedViewModel = new PupilRepeatedViewModel();
                Utils.Utils.BindTo(pupilStayClass, pupilRepeatedViewModel);
                lstPupilRepeatedViewModel.Add(pupilRepeatedViewModel);
            }
            return PartialView("_ListClassRepeated", lstPupilRepeatedViewModel);
        }

        /// <summary>
        /// GET: /TransferDataArea/TransferDataOption/
        /// </summary>
        public PartialViewResult TransferDataOption()
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            List<EducationLevel> lsEducationLevel = globalInfo.EducationLevels;
            int selectedEducationLevel = lsEducationLevel[0].EducationLevelID;

            IDictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3.Add("SchoolID", globalInfo.SchoolID.Value);
            dic3.Add("EducationLevelID", selectedEducationLevel);
            var lsClass = this.ClassProfileBusiness
                .SearchByAcademicYear(globalInfo.AcademicYearID.Value, dic3)
                .OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName)
                .ToList();
            if (lsClass.Count() > 0)
            {
                var selectedOldClass = lsClass[0];
                ViewData[TransferDataConstant.LS_CBOLDCLASS] = new SelectList(lsClass, "ClassProfileID", "DisplayName", selectedOldClass.ClassProfileID);
                var lsClass3 = lsClass.Where(o => o.ClassProfileID != selectedOldClass.ClassProfileID)
                    .OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName);
                if (lsClass3.Count() > 0)
                {
                    var selectedNewClass = lsClass3.ElementAt(0);
                    ViewData[TransferDataConstant.LS_CBNEWCLASS] = new SelectList(lsClass3, "ClassProfileID", "DisplayName", selectedNewClass.ClassProfileID);
                }
                else
                {
                    ViewData[TransferDataConstant.LS_CBNEWCLASS] = new SelectList(new string[] { });
                }
            }
            else
            {
                ViewData[TransferDataConstant.LS_CBNEWCLASS] = new SelectList(new string[] { });
                ViewData[TransferDataConstant.LS_CBOLDCLASS] = new SelectList(new string[] { });
            }
            ViewData[TransferDataConstant.LS_LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution", selectedEducationLevel);

            int permission = this.GetMenupermission("TransferData", globalInfo.UserAccountID, globalInfo.IsAdmin);
            ViewData[SystemParamsInFile.PERMISSION_VIEW] = permission >= 1;
            ViewData[SystemParamsInFile.PERMISSION_CREATE] = permission >= 2;
            ViewData[SystemParamsInFile.PERMISSION_UPDATE] = permission >= 3;
            ViewData[SystemParamsInFile.PERMISSION_DELETE] = permission >= 4;

            return PartialView("TransferDataOption");
        }

        public PartialViewResult SearchTab3(int? oldClassID, int? newClassID)
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            int permission = this.GetMenupermission("TransferData", globalInfo.UserAccountID, globalInfo.IsAdmin);
            ViewData[SystemParamsInFile.PERMISSION_VIEW] = permission >= 1;
            ViewData[SystemParamsInFile.PERMISSION_CREATE] = permission >= 2;
            ViewData[SystemParamsInFile.PERMISSION_UPDATE] = permission >= 3;
            ViewData[SystemParamsInFile.PERMISSION_DELETE] = permission >= 4;
            ViewBag.oldClassID = oldClassID;
            ViewBag.newClassID = newClassID;
            if (globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ViewBag.IsInTimeToUpdate = this.IsInAcademicYear();
            }
            else
            {
                ViewBag.IsInTimeToUpdate = this.IsInSemester1();
            }
            return PartialView("TransferDataOptionGrid");
        }

        private bool IsInSemester1()
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            AcademicYear selectedAcademicYear = this.AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            bool trongHK1 = selectedAcademicYear.FirstSemesterStartDate.Value.Date <= DateTime.Now.Date && DateTime.Now <= selectedAcademicYear.FirstSemesterEndDate.Value.Date;
            return trongHK1;
        }

        private bool IsInAcademicYear()
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            AcademicYear selectedAcademicYear = this.AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            bool trongNamHoc = selectedAcademicYear.FirstSemesterStartDate.Value.Date <= DateTime.Now.Date && DateTime.Now <= selectedAcademicYear.SecondSemesterEndDate.Value.Date;
            return trongNamHoc;
        }

        #region ChangeGrid
        [ValidateAntiForgeryToken]
        public PartialViewResult ChangeGrid(int? id)
        {
            if (!id.HasValue)
            {
            }
            else
            {
                Session["TransferDataObjectOldClassID"] = id;
                List<int> noCombobox = new List<int> { 5, 9, 12 };
                List<TransferDataObject> lsTransferObj = new List<TransferDataObject>();

                IDictionary<string, object> dic = new Dictionary<string, object>();

                GlobalInfo globalInfo = GlobalInfo.getInstance();

                int? SchoolID = globalInfo.SchoolID;
                int? AcademicYearID = globalInfo.AcademicYearID;

                AcademicYear nowAcademicYear = AcademicYearBusiness.All.Where(o => (o.AcademicYearID == AcademicYearID.Value)).FirstOrDefault();

                int oldYear = nowAcademicYear.Year - 1;

                AcademicYear old = AcademicYearBusiness.Search(new Dictionary<string, object>() { { "SchoolID", SchoolID.Value }, { "Year", oldYear } }).FirstOrDefault();

                if (old == null)
                {
                    throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
                }

                dic["AcademicYearID"] = old.AcademicYearID;
                dic["SchoolID"] = SchoolID.Value;
                dic["EducationLevelID"] = id;

                var lsOldClass = ClassProfileBusiness.Search(dic).OrderBy(o => o.EducationLevelID).ThenBy(o => o.DisplayName).ToList();
                foreach (var classProfileSingle in lsOldClass)
                {
                    TransferDataObject newItem = new TransferDataObject();
                    newItem.ClosedClassID = classProfileSingle.ClassProfileID;
                    newItem.ClosedClassName = classProfileSingle.DisplayName;
                    newItem.SchoolID = classProfileSingle.SchoolID;
                    newItem.AcademicYearID = classProfileSingle.AcademicYearID;

                    //
                    ViewData[TransferDataConstant.LS_LISTREPEATEDCLASSNAME] = lsOldClass;

                    //Với mỗi lớp thu được gọi hàm PupilOfClassBusiness.Search() với các tham số
                    //  •	ClassID: Lấy ID của từng lớp thu được ở trên
                    //  •	AcademicYearID: ID năm học cũ (vừa lấy được ở trên)
                    //  •	SchoolID = UserInfo.Value
                    //  •	Status = 1 (Học sinh đang học)
                    //  Thu được danh sách học sinh trong lớp. Count(PupilID) trong danh
                    // sách thu được => Sĩ số lớp cho vào ClassForwarding.Column.TotalClosedPupil

                    dic["ClassID"] = classProfileSingle.ClassProfileID;
                    dic["AcademicYearID"] = classProfileSingle.AcademicYearID;
                    dic["Status"] = 1;
                    dic["SchoolID "] = globalInfo.SchoolID;
                    IQueryable<PupilOfClass> lsPupilOfClass = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic);
                    newItem.TotalClosedPupil = (lsPupilOfClass != null) ? lsPupilOfClass.Count() : 0;

                    //Cũng với lớp đó dùng hàm ClassForwardingBusiness.SearchByAcademicYear() với các tham số:
                    //•	AcademicYearID  = UserInfo.AcademicYearID (ID năm học mới)
                    //•	ClosedClassID = ID của lớp (nói trên)
                    IQueryable<ClassForwarding> lsClassFwd = ClassForwardingBusiness.SearchByAcademicYear(globalInfo.AcademicYearID.Value, new Dictionary<string, object>() { { "ClosedClassID", classProfileSingle.ClassProfileID } });

                    //Nếu tìm được kết quả là đối tượng ClassForwarding (đã kết chuyển ít nhất 1 lần) thì điền dữ liệu lên grid
                    //Các combobox cboForwardedClass và cboRepeatedClass lấy giá selected trị theo kết quả thu được và không cho chọn nữa
                    if (lsClassFwd.Count() > 0)
                    {
                        ClassForwarding first = lsClassFwd.FirstOrDefault();
                        newItem.TotalForwardedPupil = first.TotalForwardedPupil;
                        newItem.TotalRepeatedPupil = first.TotalRepeatedPupil;
                        newItem.ClassForwardingID = first.ClassForwardingID;

                        //newItem.RepeatedClassID = first.RepeatedClassID;
                    }
                    else
                    {
                        newItem.TotalForwardedPupil = 0;
                        newItem.TotalRepeatedPupil = 0;
                    }

                    //   -	Khởi tạo giá trị cho các combobox tên lớp chuyển lên dựa vào
                    //   hàm ClassProfileBusiness.SearchByAcademicYear() theo
                    //•	AcademicYearID = UserInfo.AcademicYearID
                    //•	SchoolID =  UserInfo.SchoolID
                    //•	EducationLevelID = cboEducationLevelLastYear.Value + 1. Nếu cboEducationLevelLastYear.Value
                    //    là lớp cuối cấp (5, 9, 12) thì không hiển thị combobox này nữa.
                    IDictionary<string, object> dic2 = new Dictionary<string, object>();
                    dic2.Add("SchoolID", globalInfo.SchoolID.Value);

                    //nam hoc moi,lay trong user info
                    dic2.Add("AcademicYearID", globalInfo.AcademicYearID.Value);
                    dic2.Add("EducationLevelID", id + 1);
                    IQueryable<ClassProfile> lsFdwClass = ClassProfileBusiness.Search(dic2).OrderBy(o => o.EducationLevelID).ThenBy(o => o.DisplayName);

                    //Nếu cboEducationLevelLastYear.Value là lớp cuối cấp (5, 9, 12)
                    //thì không hiển thị combobox này nữa.
                    //
                    if (!noCombobox.Contains(id.Value))
                    {
                        newItem.ListRepeatedClass = lsOldClass.Select(o => new ClassinGeneral
                        {
                            ID = o.ClassProfileID,
                            Name = o.DisplayName
                        }).ToList();
                        newItem.ListForwardedClass = lsFdwClass.Select(o => new ClassinGeneral
                        {
                            ID = o.ClassProfileID,
                            Name = o.DisplayName
                        }).ToList();
                    }
                    else
                    {
                        newItem.ListRepeatedClass = new List<ClassinGeneral>();
                        newItem.ListForwardedClass = new List<ClassinGeneral>();
                    }

                    ViewData[TransferDataConstant.LS_FORWARDEDCLASSNAME] = lsFdwClass.ToList();
                    lsTransferObj.Add(newItem);
                }

                //-	Kiểm tra quyền của người dùng. Nếu không có quyền sử dụng chức năng này thì ẩn nút kết chuyển??

                //

                //-	Nếu không phải là năm học hiện tại thì ẩn nút kết chuyển.
                //So sánh DateTime.Now với FirstSemesterStartDate và SecondSemesterEndDate của  năm học hiện tại (AcademicYear)
                if (DateTime.Compare(nowAcademicYear.FirstSemesterStartDate.Value, DateTime.Now) > 0
                    || DateTime.Compare(nowAcademicYear.SecondSemesterEndDate.Value, DateTime.Now) < 0)
                {
                    ViewData[TransferDataConstant.ENABLE_BUTTON_TRANSFER] = false;
                }
                else
                {
                    ViewData[TransferDataConstant.ENABLE_BUTTON_TRANSFER] = true;
                }

                // Thuc hien phan trang tung phan
                ViewData[TransferDataConstant.LS_CLOSEDCLASS] = lsTransferObj;
            }
            return PartialView("TransferDataFormGrid");
        }

        //[ValidateAntiForgeryToken]
        public PartialViewResult ChangeOldClassGrid(int? id)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            Session["OldClassID"] = id;
            if (!id.HasValue)
            {
                ViewData[TransferDataConstant.LS_LISTOLDCLASS] = new List<TransferDataOptionObject>(0);
            }
            else
            {
                var lstPupilInOldClass = this.GetPupilInClass(id.Value).Distinct().ToList();
                var lstPupilIDInOldClass = lstPupilInOldClass.Select(o => o.PupilProfileID).Distinct().ToList();
                AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID);
                var paras = new Dictionary<string, object>() 
                    {   
                        { "AcademicYearID", global.AcademicYearID }
                        ,{ "ClassID",id} 
                        ,{ "Year", academicYear.Year}
                    };
                var iqmr = VMarkRecordBusiness.SearchBySchool(global.SchoolID.Value, paras).Where(o => lstPupilIDInOldClass.Contains(o.PupilID));
                var iqjr = VJudgeRecordBusiness.SearchBySchool(global.SchoolID.Value, paras).Where(o => lstPupilIDInOldClass.Contains(o.PupilID));
                ViewData[TransferDataConstant.LS_LISTOLDCLASS] = lstPupilInOldClass;
            }
            return PartialView("OldClassGrid");
        }

        //[ValidateAntiForgeryToken]
        public PartialViewResult ChangeNewClassGrid(int? id)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            Session["NewClassID"] = id;
            if (!id.HasValue)
            {
                ViewData[TransferDataConstant.LS_LISTNEWCLASS] = new List<TransferDataOptionObject>(0);
            }
            else
            {
                var lstPupilOfNewClass = this.GetPupilInClass(id.Value);
                var lsPupilID = lstPupilOfNewClass.Select(o => o.PupilProfileID).Distinct().ToList();
                AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID);
                var paras = new Dictionary<string, object>() 
                    {   
                        { "AcademicYearID", global.AcademicYearID }
                        ,{ "ClassID",id} 
                        ,{ "Year", academicYear.Year}
                    };
                var iqmr = VMarkRecordBusiness.SearchBySchool(global.SchoolID.Value, paras).Where(o => lsPupilID.Contains(o.PupilID));
                var iqjr = VJudgeRecordBusiness.SearchBySchool(global.SchoolID.Value, paras).Where(o => lsPupilID.Contains(o.PupilID));

                ViewData[TransferDataConstant.LS_LISTNEWCLASS] = lstPupilOfNewClass;
            }
            return PartialView("NewClassGird");
        }

        public List<TransferDataOptionObject> GetPupilInClass(int id)
        {
            int? SchoolID = _globalInfo.SchoolID;
            int? AcademicYearID = _globalInfo.AcademicYearID;
            IQueryable<TransferDataOptionObject> lsPupilOfClass =
                from poc in this.PupilOfClassBusiness.All
                join pp in this.PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                where poc.AcademicYearID == AcademicYearID
                        && poc.SchoolID == SchoolID
                        && poc.ClassID == id
                        && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                        && pp.IsActive
                orderby poc.OrderInClass, pp.Name, pp.FullName
                select new TransferDataOptionObject()
                {
                    SchoolID = poc.SchoolID,
                    AcademicYearID = poc.AcademicYearID,
                    ClassID = poc.ClassID,
                    PupilProfileID = pp.PupilProfileID,
                    PupilCode = pp.PupilCode,
                    FullName = pp.FullName,
                    Genre = pp.Genre,
                    Birthday = pp.BirthDate,
                    DisableTransferDataOption = pp.PupilAbsences.Count(pa => pa.ClassID == poc.ClassID)
                        + pp.DevelopmentOfChildrens.Count(doc => doc.ClassID == poc.ClassID)
                        + pp.MarkRecords.Count(mr => mr.ClassID == poc.ClassID)
                        + pp.JudgeRecords.Count(jr => jr.ClassID == poc.ClassID) > 0,
                    ReasonForDisableTransferDataOption =
                        pp.PupilAbsences.Count(pa => pa.ClassID == poc.ClassID) > 0 ? "Không thể điều chỉnh vì học sinh đã có dữ liệu điểm danh"
                        : pp.DevelopmentOfChildrens.Count(doc => doc.ClassID == poc.ClassID) > 0 ? "Không thể điều chỉnh vì học sinh đã có dữ liệu đánh giá sự phát triển"
                        : pp.MarkRecords.Count(mr => mr.ClassID == poc.ClassID) > 0 ? "Không thể điều chỉnh vì học sinh đã có điêm môn tính điểm"
                        : pp.JudgeRecords.Count(mr => mr.ClassID == poc.ClassID) > 0 ? "Không thể điều chỉnh vì học sinh đã có dữ liệu môn điểm danh"
                        : "Chọn học sinh"
                };
            var result = lsPupilOfClass.ToList();
            return result;
        }

        #endregion ChangeGrid

        #region fill dropdownlist
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingOldClass(int? EducationLevelId)
        {
            return _GetClass(EducationLevelId);
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingNewClass(int? EducationLevelId, int? OldClassID)
        {
            return _GetNewClass(EducationLevelId, OldClassID);
        }

        private JsonResult _GetClass(int? ID)
        {
            if (ID.HasValue)
            {
                //•	AcademicYearID = UserInfo.AcademicYearID
                GlobalInfo globalInfo = GlobalInfo.getInstance();

                int? SchoolID = globalInfo.SchoolID;
                int? AcademicYearID = globalInfo.AcademicYearID;
                IDictionary<string, object> dic = new Dictionary<string, object>();

                //•	SchoolID =  UserInfo.SchoolID
                dic.Add("SchoolID", SchoolID.Value);
                dic.Add("EducationLevelID", ID);

                //dic.Add("AcademicYearID", globalInfo.AcademicYearID);
                IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchByAcademicYear(AcademicYearID.Value, dic)
                    .OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName);

                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        private JsonResult _GetOldClass(int? ID)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            if (ID.HasValue)
            {
                AcademicYear currentAY = AcademicYearBusiness.Find(global.AcademicYearID.Value);
                AcademicYear oldAY = AcademicYearBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() { { "Year", currentAY.Year - 1 } }).FirstOrDefault();

                if (oldAY == null)
                {
                    throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
                }

                //•	AcademicYearID = UserInfo.AcademicYearID
                GlobalInfo globalInfo = GlobalInfo.getInstance();

                int? SchoolID = globalInfo.SchoolID;
                int? AcademicYearID = oldAY.AcademicYearID;
                IDictionary<string, object> dic = new Dictionary<string, object>();

                //•	SchoolID =  UserInfo.SchoolID
                dic.Add("SchoolID", SchoolID.Value);
                dic.Add("EducationLevelID", ID);
                IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchByAcademicYear(AcademicYearID.Value, dic).OrderBy(o => o.EducationLevelID).ThenBy(o => o.DisplayName);

                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        private JsonResult _GetNewClass(int? ID, int? OldClassID)
        {
            //•	AcademicYearID = UserInfo.AcademicYearID
            GlobalInfo globalInfo = GlobalInfo.getInstance();

            int? SchoolID = globalInfo.SchoolID;
            int? AcademicYearID = globalInfo.AcademicYearID;
            IDictionary<string, object> dic = new Dictionary<string, object>();

            //•	SchoolID =  UserInfo.SchoolID
            dic.Add("SchoolID", SchoolID.Value);
            dic.Add("EducationLevelID", ID);

            //dic.Add("AcademicYearID", globalInfo.AcademicYearID);
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchByAcademicYear(AcademicYearID.Value, dic).Where(o => (o.ClassProfileID != OldClassID))
                .OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName);

            return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
        }

        #endregion fill dropdownlist

        #region button transfer click
        

        [ValidateAntiForgeryToken]
        public ActionResult TransferDataForward(string strOldPupil)
        {
            GlobalInfo global = new GlobalInfo();
            if (GetMenupermission("TransferData", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            SetViewDataPermission("TransferData", GlobalInfo.getInstance().UserAccountID, GlobalInfo.getInstance().IsAdmin);
            AcademicYear now = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            bool trongHK1 = now.FirstSemesterEndDate.HasValue && (now.FirstSemesterStartDate <= DateTime.Now) && (DateTime.Now <= (now.FirstSemesterEndDate.Value.AddDays(1)));
            ViewData[TransferDataConstant.ENABLE_BUTTON_SEMESTER] = trongHK1;
            List<int> lstOldPupilID = strOldPupil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            //checkedOldPupil = checkedOldPupil ?? new int[] { };
            IDictionary<string, object> dic = new Dictionary<string, object>();
            int? SchoolID = globalInfo.SchoolID;
            int? AcademicYearID = globalInfo.AcademicYearID;
            dic.Add("SchoolID", SchoolID.Value);
            dic.Add("AcademicYearID", AcademicYearID.Value);
            dic.Add("Status", 1);
            int? oldClassID = Session["OldClassID"] as int?;
            if (!oldClassID.HasValue) throw new SMASUserException("Lớp học cũ không hợp lê.");
            int? newClassID = Session["NewClassID"] as int?;
            if (!newClassID.HasValue) throw new SMASUserException("Lớp học mới không hợp lê.");
            dic["ClassID"] = oldClassID.Value;
            List<PupilOfClass> lstPupilOfOldClass = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, dic).Where(o => lstOldPupilID.Contains(o.PupilID)).ToList();
            dic = new Dictionary<string, object>();
            dic["ClassID"] = newClassID.Value;
            var maxOr = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, dic).Select(u => u.OrderInClass).Max();
            int MaxOrder = maxOr.HasValue ? maxOr.Value + 1 : 1;
            if (lstOldPupilID.Count() > 0)
            {
                List<PupilOfClass> lstPupilMove = new List<PupilOfClass>();
                foreach (int oldPupil in lstOldPupilID)
                {
                    PupilOfClass thisPupil = lstPupilOfOldClass.Where(o => o.PupilID == oldPupil && o.ClassID == oldClassID.Value).FirstOrDefault();
                    lstPupilMove.Add(thisPupil);
                }
                PupilOfClassBusiness.ClassForwarding(globalInfo.UserAccountID, lstPupilMove, globalInfo.SchoolID.Value, newClassID.Value, MaxOrder);
                PupilOfClassBusiness.Save();
            }

            //fill lai du lieu
            var OldGridData = this.GetPupilInClass(oldClassID.Value);
            var NewGridData = this.GetPupilInClass(newClassID.Value);
            ViewData[TransferDataConstant.LS_LISTOLDCLASS] = OldGridData;
            ViewData[TransferDataConstant.LS_LISTNEWCLASS] = NewGridData;

            //ViewData[TransferDataConstant.OPTION_GRID_RIGHT_HAS_VALUE] = OldGridData.Count() > 0;
            //ViewData[TransferDataConstant.OPTION_GRID_LEFT_HAS_VALUE] = NewGridData.Count() > 0;
            ViewData[TransferDataConstant.ENABLE_BUTTON_TRANSFER] = globalInfo.IsCurrentYear;
            ViewBag.oldClassID = oldClassID.Value;
            ViewBag.newClassID = newClassID.Value;
            if (globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ViewBag.IsInTimeToUpdate = this.IsInAcademicYear();
            }
            else
            {
                ViewBag.IsInTimeToUpdate = this.IsInSemester1();
            }
            return View("TransferDataOptionGrid");
        }
        

        [ValidateAntiForgeryToken]
        public ActionResult TransferDataBackward(string strNewPupilID)
        {
            GlobalInfo global = new GlobalInfo();
            if (GetMenupermission("TransferData", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            SetViewDataPermission("TransferData", GlobalInfo.getInstance().UserAccountID, GlobalInfo.getInstance().IsAdmin);
            AcademicYear now = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            bool trongHK1 = now.FirstSemesterEndDate.HasValue && (now.FirstSemesterStartDate <= DateTime.Now) && (DateTime.Now <= (now.FirstSemesterEndDate.Value.AddDays(1)));
            ViewData[TransferDataConstant.ENABLE_BUTTON_SEMESTER] = trongHK1;
            List<int> lstNewPupilID = strNewPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();


            int? SchoolID = globalInfo.SchoolID;
            int? AcademicYearID = globalInfo.AcademicYearID;
            dic.Add("SchoolID", SchoolID.Value);
            dic.Add("AcademicYearID", AcademicYearID.Value);
            dic.Add("Status", 1);
            // Lay ma lop hoc cu  tu session
            int? NewClassID = Session["NewClassID"] as int?;
            if (!NewClassID.HasValue) throw new SMASUserException("Lớp học cũ không hợp lê.");
            // Lay ma lop hoc moi tu session
            int? OldClassID = Session["OldClassID"] as int?;
            if (!OldClassID.HasValue) throw new SMASUserException("Lớp học cũ không hợp lê.");

            dic["ClassID"] = NewClassID.Value;
            List<PupilOfClass> lstPupilOfClassNew = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, dic).Where(o => lstNewPupilID.Contains(o.PupilID)).ToList();

            dic = new Dictionary<string, object>();
            dic["ClassID"] = OldClassID.Value;
            var maxOr = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, dic).Select(u => u.OrderInClass).Max();
            int MaxOrder = 1;
            if (maxOr.HasValue)
            {
                MaxOrder = maxOr.Value + 1;
            }
            if (lstNewPupilID.Count() > 0)
            {
                List<PupilOfClass> lstPupilMove = new List<PupilOfClass>();
                foreach (int newPupil in lstNewPupilID)
                {
                    PupilOfClass thisPupil = lstPupilOfClassNew.Where(o => o.PupilID == newPupil && o.ClassID == NewClassID.Value).FirstOrDefault();
                    lstPupilMove.Add(thisPupil);
                }
                PupilOfClassBusiness.ClassForwarding(globalInfo.UserAccountID, lstPupilMove, globalInfo.SchoolID.Value, OldClassID.Value, MaxOrder);
                PupilOfClassBusiness.Save();
            }

            //fill lai du lieu
            var OldGridData = GetPupilInClass(NewClassID.Value);
            var NewGridData = GetPupilInClass(OldClassID.Value);
            ViewData[TransferDataConstant.LS_LISTOLDCLASS] = OldGridData;
            ViewData[TransferDataConstant.LS_LISTNEWCLASS] = NewGridData;

            ViewData[TransferDataConstant.OPTION_GRID_RIGHT_HAS_VALUE] = OldGridData.Count() > 0;
            ViewData[TransferDataConstant.OPTION_GRID_LEFT_HAS_VALUE] = NewGridData.Count() > 0;

            ViewData[TransferDataConstant.ENABLE_BUTTON_TRANSFER] = globalInfo.IsCurrentYear;
            ViewBag.oldClassID = OldClassID.Value;
            ViewBag.newClassID = NewClassID.Value;
            if (globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ViewBag.IsInTimeToUpdate = this.IsInAcademicYear();
            }
            else
            {
                ViewBag.IsInTimeToUpdate = this.IsInSemester1();
            }
            return View("TransferDataOptionGrid");
        }

        [ValidateAntiForgeryToken]
        public ActionResult TransferDataForm(int[] checkedClass)
        {
            checkedClass = checkedClass ?? new int[] { };
            return View("TransferDataFormGrid");
        }

        #endregion button transfer click

        #region select

        [GridAction]
        public ActionResult SelectTransferDataObject()
        {
            //SetViewData();
            List<TransferDataObject> paging = (List<TransferDataObject>)ViewData[TransferDataConstant.LS_CLOSEDCLASS];
            return View(new GridModel(paging));
        }

        #endregion select

        #region on transfer data
        

        [ValidateAntiForgeryToken]
        public JsonResult OnTransferData(int[] checkedClass, string[] RepeatedClassName, string[] ForwardedClassName, int page = 1, string orderBy = "")
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();

            int? SchoolID = globalInfo.SchoolID;
            int? AcademicYearID = globalInfo.AcademicYearID;

            //tim old academicyear id
            AcademicYear nowAcademicYear = AcademicYearBusiness.All.Where(o => (o.AcademicYearID == AcademicYearID.Value)).FirstOrDefault();
            int oldYear = nowAcademicYear.Year - 1;
            AcademicYear oldAcademicYear = AcademicYearBusiness.Search(new Dictionary<string, object>() { { "SchoolID", SchoolID.Value }, { "Year", oldYear } }).FirstOrDefault();

            int EducationLevelID = (int)Session["TransferDataObjectOldClassID"];

            IDictionary<int, IDictionary<string, int>> dic = new Dictionary<int, IDictionary<string, int>>();

            List<int> lstUpClass = new List<int>();
            List<int> lstDownClass = new List<int>();

            //convert int[] ->List<int>
            List<int> lstClass = new List<int>();
            foreach (int item in checkedClass)
            {
                lstClass.Add(item);
            }

            for (int i = 0; i < RepeatedClassName.Length; i++)
            {
                string repeated = RepeatedClassName[i];
                string forwarded = ForwardedClassName[i];

                int temp1 = repeated.LastIndexOf("-");
                string strClosedID = repeated.Substring(0, temp1);
                string strRepeatedID = repeated.Substring(temp1 + 1);

                int temp2 = forwarded.LastIndexOf("-");
                string strForwardedID = forwarded.Substring(temp2 + 1);

                int ClosedID = int.Parse(strClosedID);
                int RepeatedID = int.Parse(strRepeatedID);
                int ForwardedID = int.Parse(strForwardedID);

                IDictionary<string, int> newDic = new Dictionary<string, int>();
                if (checkedClass.Contains(ClosedID))
                {
                    lstUpClass.Add(ForwardedID);
                    lstDownClass.Add(RepeatedID);
                }
            }

            //-	Kiểm tra quyền của người dùng. Nếu không có quyền sử dụng chức năng này return
            //-	Nếu không phải học kỳ I của năm học hiện tại thì return
            //-	Nếu trường đã nhập điểm thì return

            if (oldAcademicYear == null)
            {
                throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
            }

            ClassForwardingBusiness.TransferData(lstClass, lstUpClass, lstDownClass, oldAcademicYear.AcademicYearID, globalInfo.AcademicYearID.Value, EducationLevelID, globalInfo.SchoolID.Value, globalInfo.UserAccountID);
            return Json(new JsonMessage(Res.Get("Common_Label_TransferSuccessMessage")));
        }

        #endregion on transfer data

        #region transfer forward data

        
        [ValidateAntiForgeryToken]
        public PartialViewResult TransferClassForward(int? cbEducationLevelForward, int? cbClassForward, int[] ForwardedClassName,
                                    string[] checkAllTransferForward, int? ButtonType, int? ButtonTypeCheckPupil, int[] checkAllUnTransferForward,
                                    int[] ForwardClassNameHidden, int[] HiddenPupilID, int[] checkAllTransferPupil, int[] checkAllUnTransferPupil,
                                    bool TransferAllPupilInSelectedClass = false
                                    )
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (GetMenupermission("TransferData", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (!cbEducationLevelForward.HasValue) // Kiem tra xem chon khoi chua
            {
                throw new BusinessException("TransferData_Label_EducationLevelError");
            }
            GlobalInfo global = GlobalInfo.getInstance();

            if (ButtonType == 1 && ButtonTypeCheckPupil != 3) //ket chuyen len lop   
            {
                #region Kết chuyển lên lớp
                if (checkAllTransferForward == null) // Chua chon lop nao de ket chuyen len lop
                {
                    throw new BusinessException("TransferData_Label_NoCheckError");

                }
                if (ForwardedClassName == null) // Khon ton tai ten lop duoc chon
                {
                    throw new BusinessException("Validate_Class_TransferData");
                }
                Dictionary<int, int> dicClassFw = new Dictionary<int, int>();
                for (int i = 0; i < checkAllTransferForward.Count(); i++)
                {
                    string[] stt = checkAllTransferForward[i].Split(',');
                    int ClassID = int.Parse(stt.FirstOrDefault());
                    int index = int.Parse(stt.LastOrDefault());
                    dicClassFw[ClassID] = ForwardedClassName[index]; // Danh sach cac lop duoc chon de ket chuyen len lop, anh xa den cac lop duoc ket chuyen den
                }
                // Goi ham thuc hien ket chuyen du lieu
                ClassForwardingBusiness.TransferData(dicClassFw, TransferAllPupilInSelectedClass, global.AcademicYearID.Value, global.SchoolID.Value, cbEducationLevelForward.Value);
                // Luu lai ket qua ket chuyen vao DB
                //ClassForwardingBusiness.Save();
                #endregion
                return SearchTab1(cbEducationLevelForward.Value);
            }
            else if (ButtonType == 1 && ButtonTypeCheckPupil == 3) //kết chuyển lên lớp cho từng học sinh
            {
                #region anhnph_20150709_ Xử lý kết chuyển lên lớp cho từng học sinh
                if (ForwardClassNameHidden == null) //chua chon lop ket chuyen
                {
                    throw new BusinessException("Validate_Class_TransferData");
                }
                if (!cbClassForward.HasValue)
                {
                    cbClassForward = 0;
                }
                List<int> lsCheckLocation = new List<int>();//list hoc sinh dc check ket chuyen
                List<int> lsUnCheckLocation = new List<int>();//list hoc sinh hk dc check ket chuyen 
                for (int i = 0; i < HiddenPupilID.Count(); i++)
                {
                    if (checkAllTransferPupil.Contains(HiddenPupilID[i]))
                    {
                        lsCheckLocation.Add(i);
                    }
                    else
                    {
                        lsUnCheckLocation.Add(i);
                    }
                }

                List<PupilForwardBO> lstPupilForwardBO = new List<PupilForwardBO>();//khoi tao list hoc sinh ket chuyen
                // Lay danh sach cac hoc sinh lên lớp trong lop duoc chon.
                var iqPFBO = ClassForwardingBusiness.GetPupilForward(globalInfo.SchoolID.Value,
                                    globalInfo.AcademicYearID.Value, cbEducationLevelForward.Value, cbClassForward.Value, TransferAllPupilInSelectedClass).OrderBy(o => o.FullName).ToList();

                foreach (var location in lsCheckLocation)
                {
                    int ForwardClassID = ForwardClassNameHidden[location];
                    int PupilID = HiddenPupilID[location];
                    PupilForwardBO pfbo = iqPFBO.Where(o => o.PupilID == PupilID).FirstOrDefault();
                    if (pfbo == null)
                    {
                        throw new BusinessException("Common_Error_InternalError");
                    }
                    else
                    {
                        pfbo.ForwardClassID = ForwardClassID;
                        lstPupilForwardBO.Add(pfbo);
                    }
                }
                //luu ket chuyen
                ClassForwardingBusiness.ForwardPupil(lstPupilForwardBO, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, cbEducationLevelForward.Value);

                //n: so hoc sinh ket chuyen
                ViewData[TransferDataConstant.COUNT_PUPIL_TRANSFER] = lstPupilForwardBO.Count().ToString();
                //m: so hoc sinh dc chon
                ViewData[TransferDataConstant.COUNT_PUPIL_CHOOSE] = checkAllTransferPupil.Count().ToString();


                #endregion
                return SearchPupilTab1(cbEducationLevelForward.Value, cbClassForward ?? 0, TransferAllPupilInSelectedClass);
            }
            else
            {
                throw new BusinessException("TransferData_Label_DataError");
            }
        }

        
        [ValidateAntiForgeryToken]
        public PartialViewResult CancelTransferData(int? cbEducationLevelForward, int? cbClassForward,
             int[] ForwardClassNameHidden, int[] HiddenPupilID, int[] checkAllTransferPupil, int[] checkAllUnTransferPupil,
            int[] ForwardedClassNameHidden, int? ButtonType, int? ButtonTypeCheckPupil, string[] checkAllUnTransferForward, bool TransferAllPupilInSelectedClass = false)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (GetMenupermission("TransferData", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (!cbEducationLevelForward.HasValue)
            {
                throw new BusinessException("TransferData_Label_EducationLevelError");
            }
            GlobalInfo global = GlobalInfo.getInstance();
            if (ButtonType == 2 && ButtonTypeCheckPupil != 3)
            {
                #region Xu ly huy ket chuyen lop
                if (checkAllUnTransferForward == null || checkAllUnTransferForward.Count() == 0)// HUY KET CHUYEN CHO LOP 
                {
                    throw new BusinessException("UntransferData_Label_NoCheckError");
                }
                List<int> lstOldClassID = new List<int>(checkAllUnTransferForward.Count());
                for (int i = 0; i < checkAllUnTransferForward.Count(); i++)
                {
                    string[] stt = checkAllUnTransferForward[i].Split(',');
                    int index = int.Parse(stt.LastOrDefault());
                    int oldClassID = int.Parse(stt.FirstOrDefault());
                    int newClassID = ForwardedClassNameHidden[index];
                    if (oldClassID > 0) lstOldClassID.Add(oldClassID);
                }

                ClassForwardingBusiness.CancelTransferData(lstOldClassID, global.AcademicYearID.Value, global.SchoolID.Value, cbEducationLevelForward.Value);
                ClassForwardingBusiness.Save();
                return SearchTab1(cbEducationLevelForward.Value);
                #endregion
            }
            else if (ButtonType == 2 && ButtonTypeCheckPupil == 3)
            {
                #region Xu ly huy ket chuyen cho hoc sinh trong lop
                if (ForwardClassNameHidden == null) //chua chon lop ket chuyen
                {
                    throw new BusinessException("Validate_Class_TransferData");
                }
                if (!cbClassForward.HasValue)
                {
                    cbClassForward = 0;
                }
                if (checkAllUnTransferPupil == null)
                {
                    throw new BusinessException("Thầy/cô chưa chọn học sinh để hủy kết chuyển");
                }
                if (HiddenPupilID == null)
                {
                    throw new BusinessException("Chưa có học sinh kết chuyển");
                }
                List<int> lsCheckLocation = new List<int>();//list hoc sinh dc check huy ket chuyen
                lsCheckLocation = HiddenPupilID.Where(c => checkAllUnTransferPupil.Contains(c)).ToList();
                List<PupilForwardBO> lstPupilForwardBO = new List<PupilForwardBO>();//khoi tao list hoc sinh ket chuyen
                // Lay danh sach cac hoc sinh lên lớp trong lop duoc chon.
                //Ham nay chay qua cham, tuning lại
                lstPupilForwardBO = ClassForwardingBusiness.GetPupilForward(globalInfo.SchoolID.Value,
                                    globalInfo.AcademicYearID.Value, cbEducationLevelForward.Value, cbClassForward.Value, TransferAllPupilInSelectedClass)
                                    .Where(c => lsCheckLocation.Contains(c.PupilID))
                                    .ToList();

                //luu ket chuyen
                ClassForwardingBusiness.CancelForwardPupil(lstPupilForwardBO, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, cbEducationLevelForward.Value);

                #endregion
                return SearchPupilTab1(cbEducationLevelForward.Value, cbClassForward ?? 0,TransferAllPupilInSelectedClass);
            }
            else
            {
                throw new BusinessException("TransferData_Label_DataError");
            }
        }
        #endregion transfer forward data

        #region transfer repeated data
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClassRepeated(int? EducationLevelID)
        {
            return _GetOldClass(EducationLevelID);
        }

        
        [ValidateAntiForgeryToken]
        public PartialViewResult TransferClassRepeated(int? cbEducationLevelRepeated, int? cbClassRepeated, int[] RepeatedClassNameHidden, int[] HiddenPupilID, int[] checkAllTransfer, int? ButtonType, int[] checkAllUnTransfer)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (GetMenupermission("TransferData", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (!cbEducationLevelRepeated.HasValue)
            {
                throw new BusinessException("TransferData_Label_EducationLevelError");
            }
            if (RepeatedClassNameHidden == null)
            {
                throw new BusinessException("Validate_Class_TransferData");
            }
            GlobalInfo global = GlobalInfo.getInstance();
            if (!cbClassRepeated.HasValue)
            {
                cbClassRepeated = 0;
            }
            List<int> lsCheckLocation = new List<int>();
            List<int> lsUnCheckLocation = new List<int>();
            for (int i = 0; i < HiddenPupilID.Count(); i++)
            {
                if (checkAllTransfer.Contains(HiddenPupilID[i]))
                {
                    lsCheckLocation.Add(i);
                }
                else
                {
                    lsUnCheckLocation.Add(i);
                }
            }
            List<PupilRepeatedBO> lstPupilRepeatedBO = new List<PupilRepeatedBO>();
            var iqPRBO = PupilRepeatedBusiness.GetPupilRepeated(global.SchoolID.Value, global.AcademicYearID.Value, cbEducationLevelRepeated.Value, cbClassRepeated.Value).OrderBy(o => o.FullName).ToList();
            foreach (var location in lsCheckLocation)
            {
                int RepeatedClassID = RepeatedClassNameHidden[location];
                int PupilID = HiddenPupilID[location];
                PupilRepeatedBO prbo = iqPRBO.Where(o => o.PupilID == PupilID).FirstOrDefault();
                if (prbo == null)
                {
                    throw new BusinessException("Common_Error_InternalError");
                }
                else
                {
                    prbo.RepeatedClassID = RepeatedClassID;
                    lstPupilRepeatedBO.Add(prbo);
                }
            }
            PupilRepeatedBusiness.RepeatedPupil(lstPupilRepeatedBO, global.AcademicYearID.Value, global.SchoolID.Value, cbEducationLevelRepeated.Value);
            //PupilRepeatedBusiness.Save();
            return SearchTab2(cbEducationLevelRepeated.Value, cbClassRepeated ?? 0);
        }

        public ActionResult TransferClassRepeatedTab()
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Lay quyen nguoi ding va truyen sang view
            int permission = this.GetMenupermission("TransferData", globalInfo.UserAccountID, globalInfo.IsAdmin);
            ViewData[SystemParamsInFile.PERMISSION_VIEW] = permission >= 1;
            ViewData[SystemParamsInFile.PERMISSION_CREATE] = permission >= 2;
            ViewData[SystemParamsInFile.PERMISSION_UPDATE] = permission >= 3;
            ViewData[SystemParamsInFile.PERMISSION_DELETE] = permission >= 4;
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Lay danh sach cac khoi hoc trong nam hoc truoc va truyen sang view
            List<EducationLevel> lsEducationLevel = globalInfo.EducationLevels;
            if (lsEducationLevel == null) lsEducationLevel = new List<EducationLevel>();
            int? selectedEducationLevelID;
            if (lsEducationLevel.Count == 0) selectedEducationLevelID = null;
            else selectedEducationLevelID = lsEducationLevel[0].EducationLevelID;
            ViewData[TransferDataConstant.LST_EDUCATION_LEVEL_REPEATED] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution", selectedEducationLevelID);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Lay danh sach cac lop trong nam hoc truoc, thuoc khoi hoc duoc lua chon
            AcademicYear currentAcademicYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            // Lay doi tuong nam hoc truoc
            AcademicYear previousAcademicYear = this.AcademicYearBusiness.All
                .FirstOrDefault(ay => ay.SchoolID == currentAcademicYear.SchoolID && ay.Year == currentAcademicYear.Year - 1);
            if (previousAcademicYear == null) throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
            // Lay danh sach lop
            IEnumerable<ClassProfile> lsClass;
            if (!selectedEducationLevelID.HasValue) lsClass = new List<ClassProfile>();
            else
            {
                lsClass = this.ClassProfileBusiness.All
                    .Where(c => c.SchoolID == globalInfo.SchoolID
                            && c.AcademicYearID == previousAcademicYear.AcademicYearID
                            && (c.EducationLevelID == selectedEducationLevelID || !selectedEducationLevelID.HasValue)
                            && c.EducationLevel.Grade == globalInfo.AppliedLevel
                            && c.IsActive.Value)
                    .OrderBy(o => o.EducationLevelID).ThenBy(c => c.OrderNumber).ThenBy(o => o.DisplayName);
            }
            ViewData[TransferDataConstant.LST_CLASS_REPEATED] = new SelectList(lsClass, "ClassProfileID", "DisplayName");
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            bool InTimeToTransferOrUntransfer;
            if (globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN || globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                InTimeToTransferOrUntransfer = this.IsInAcademicYear();
            }
            else
            {
                InTimeToTransferOrUntransfer = this.IsInSemester1();
            }
            ViewBag.InTimeToTransferOrUntransfer = InTimeToTransferOrUntransfer;
            return PartialView("TransferClassRepeated");
        }

        
        [ValidateAntiForgeryToken]
        public PartialViewResult CancelTransferDataRepeated(int? cbEducationLevelRepeated, int? cbClassRepeated, int[] RepeatedClassName, int[] HiddenPupilID, int[] checkAllTransfer, int? ButtonType, int[] checkAllUnTransfer)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (GetMenupermission("TransferData", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (!cbEducationLevelRepeated.HasValue)
            {
                throw new BusinessException("TransferData_Label_EducationLevelError");
            }
            if (checkAllUnTransfer == null || checkAllUnTransfer.Count() == 0)
            {
                throw new BusinessException("TransferData_CancelRepeatedPupil_NoCheckError");
            }
            GlobalInfo global = GlobalInfo.getInstance();
            if (!cbClassRepeated.HasValue)
            {
                cbClassRepeated = 0;
            }

            List<int> lsCheckLocation = new List<int>();
            List<int> lsUnCheckLocation = new List<int>();
            for (int i = 0; i < HiddenPupilID.Count(); i++)
            {
                if (checkAllUnTransfer.Contains(HiddenPupilID[i]))
                {
                    lsCheckLocation.Add(i);
                }
                else
                {
                    lsUnCheckLocation.Add(i);
                }
            }
            var iqPRBO = PupilRepeatedBusiness.GetPupilRepeated(global.SchoolID.Value, global.AcademicYearID.Value, cbEducationLevelRepeated.Value, cbClassRepeated.Value)
                .OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            List<PupilRepeatedBO> lstPupilRepeatedBO = new List<PupilRepeatedBO>();
            foreach (var location in lsCheckLocation)
            {
                int PupilID = HiddenPupilID[location];
                PupilRepeatedBO prbo = iqPRBO.Where(o => o.PupilID == PupilID).FirstOrDefault();
                if (prbo == null)
                {
                    throw new BusinessException("Common_Error_InternalError");
                }
                lstPupilRepeatedBO.Add(prbo);
            }
                PupilRepeatedBusiness.CancelRepeatedPupil(lstPupilRepeatedBO, global.AcademicYearID.Value, global.SchoolID.Value, cbEducationLevelRepeated.Value);
            return SearchTab2(cbEducationLevelRepeated.Value, cbClassRepeated ?? 0);

        }

        #endregion transfer repeated data

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            List<ClassForwardViewModel> lstClass = getListClassForwarding(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, educationLevelID);
            return Json(new SelectList(lstClass, "OldClassID", "OldClassName"));
        }

        /// <summary>
        /// anhnph_20150708_lấy ra danh sách học sinh lên lớp của lớp được chọn
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public PartialViewResult SearchPupilTab1(int EducationLevelID = 0, int ClassID = 0, bool TransferAllPupilInSelectedClass = false)
        {
            #region Get Data
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int appliedLevel = _globalInfo.AppliedLevel.Value;
            AcademicYear currentAcademicYear = AcademicYearBusiness.Find(academicYearId);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);

            bool isMovedHistory = UtilsBusiness.IsMoveHistory(currentAcademicYear);      

            if (currentAcademicYear == null)
            {
                return new PartialViewResult();
            }


            //lấy dữ liệu các học sinh lưu ban
            List<int> lstPupilRepeated = this.PupilRepeatedBusiness.All.Where(p => p.EducationLevelID == EducationLevelID
                && p.SchoolID == schoolId && p.AcademicYearID==academicYearId).Select(p => p.PupilID).ToList();

            // Danh sach cac lop trong nam hien tai
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
              {
                {"EducationLevelID",(EducationLevelID + 1)}
                ,{"AcademicYearID",academicYearId}
              }).OrderBy(o => o.EducationLevelID).ToList();
            lstClassProfile.ForEach(p => p.OrderNumber = (p.OrderNumber.HasValue ? p.OrderNumber.Value : 0));
            List<ClassinGeneral> lstClassUp = lstClassProfile.OrderBy(c => c.OrderNumber).ThenBy(c => c.DisplayName)
                                            .Select(o => new ClassinGeneral { ID = o.ClassProfileID, Name = o.DisplayName })
                                            .ToList();
            ViewData[TransferDataConstant.LIST_CLASS_UP] = lstClassUp;

            List<int> lstClassId = new List<int>();
            if (ClassID > 0)
            {
                lstClassId.Add(ClassID);
            }
            else
            {
                lstClassId = lstClassUp.Select(c => c.ID).ToList();
            }
            #endregion

            // Cap mam non thi cho phep Ket chuyen, Huy ket chuyen neu con trong nam hoc hien tai
            // Cap 1, 2, 3 thi cho phep Ket chuyen, Huy ket chuyen neu con trong hoc ky 1
            bool InTimeToTransferOrUntransfer;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN || appliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                InTimeToTransferOrUntransfer = this.IsInAcademicYear();
            }
            else
            {
                InTimeToTransferOrUntransfer = this.IsInSemester1();
            }
            ViewBag.InTimeToTransferOrUntransfer = InTimeToTransferOrUntransfer;

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Lay danh sach cac hoc sinh lên lớp trong lop duoc chon.
            var querylstPupilUpClass = ClassForwardingBusiness.GetPupilForward(schoolId, academicYearId, EducationLevelID, ClassID, TransferAllPupilInSelectedClass);

            // Danh sach cac hoc sinh se ket chuyen len lớp
            var lstPupilsForward = querylstPupilUpClass;

            // loc ra cac hoc sinh da duoc ket chuyen luu ban 
            lstPupilsForward = (from lst in lstPupilsForward
                                where !lstPupilRepeated.Contains(lst.PupilID)
                                select lst).ToList();

            // Chuyen thanh cac doi tuong ViewModel
            List<PupilForwardViewModel> lstPupilForwardViewModel = new List<PupilForwardViewModel>();
            //List<PupilObject> iqMRHIS = new List<PupilObject>();
            //List<PupilObject> iqJRHIS = new List<PupilObject>();
            List<PupilObjectModel> iqMR = new List<PupilObjectModel>();
            List<PupilObjectModel> iqJR = new List<PupilObjectModel>();
            List<PupilObjectModel> iqSEE = new List<PupilObjectModel>();
            List<PupilObjectModel> iqSE = new List<PupilObjectModel>();
            List<PupilObjectModel> iqDOC = new List<PupilObjectModel>();
            List<int> lstPupilID = lstPupilsForward.Select(p=>p.PupilID).Distinct().ToList();
            #region lay danh sach du lieu de check enabled checkbox huy ket chuyen
            List<PupilObjectModel> iqPupilAbsence = this.getListOfPupilAbsenceOfPupils(lstPupilID, currentAcademicYear.AcademicYearID, partitionId, EducationLevelID);
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE ||
                _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN
                )
            {
                iqDOC = this.getListOfDevelopmentOfChildrenOfPupils(lstPupilID, currentAcademicYear.AcademicYearID, EducationLevelID);
            }


            //if(_globalInfo.AppliedLevel>=GlobalConstants.APPLIED_LEVEL_SECONDARY)
            //iqMRHIS = this.getListOfMarkRecordHistoryOfPupils(lstPupilID, currentAcademicYear.AcademicYearID, partitionId);
            //iqJRHIS = this.getListOfJudgeRecordHistoryOfPupils(lstPupilID, currentAcademicYear.AcademicYearID, partitionId);
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                iqSEE = this.getListSummendEndingEvalutionOfPupilPrimary(lstPupilID, currentAcademicYear.AcademicYearID, partitionId);
                iqSE = this.getListSummedEvaluationOfPupilPrimary(lstPupilID, currentAcademicYear.AcademicYearID, partitionId);
            }
            else if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY ||
                    _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                iqMR = this.getListOfMarkRecordOfPupils(lstPupilID, currentAcademicYear.AcademicYearID, partitionId, EducationLevelID);
                iqJR = this.getListOfJudgeRecordOfPupils(lstPupilID, currentAcademicYear.AcademicYearID, partitionId, EducationLevelID);
            }


            int count = 0;
            int pupilID = 0;
            int classIDNew = 0;
            PupilObjectModel objPO = null;
            #endregion
            foreach (var pupilUpClass in lstPupilsForward)
            {
                PupilForwardViewModel pupilForwardViewModel = new PupilForwardViewModel();
                Utils.Utils.BindTo(pupilUpClass, pupilForwardViewModel);
                pupilForwardViewModel.Check = true;
                pupilID = pupilUpClass.PupilID;
                classIDNew = pupilUpClass.ForwardClassID;
                // Kiem tra du lieu diem danh
                objPO = iqPupilAbsence.FirstOrDefault(p => p.PupilId == pupilID);
                count = (objPO == null) ? 0 : objPO.TotalRecord;
                if (count > 0)
                {
                    pupilForwardViewModel.Check = false;
                    lstPupilForwardViewModel.Add(pupilForwardViewModel);
                    continue;
                }

                // Kiem tra du lieu danh gia su phat trien
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
                {
                    objPO = iqDOC.FirstOrDefault(p => p.PupilId == pupilID);
                    count = (objPO == null) ? 0 : objPO.TotalRecord;
                    if (count > 0)
                    {
                        pupilForwardViewModel.Check = false;
                        lstPupilForwardViewModel.Add(pupilForwardViewModel);
                        continue;
                    }
                }
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {

                    count = iqSEE.Where(p => p.PupilId == pupilID).Count();
                    if (count > 0)
                    {
                        pupilForwardViewModel.Check = false;
                        lstPupilForwardViewModel.Add(pupilForwardViewModel);
                        continue;
                    }

                    count = iqSE.Where(p => p.PupilId == pupilID).Count();
                    if (count > 0)
                    {
                        pupilForwardViewModel.Check = false;
                        lstPupilForwardViewModel.Add(pupilForwardViewModel);
                        continue;
                     }
                }
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY ||
                    _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    objPO = iqMR.FirstOrDefault(p => p.PupilId == pupilID);
                    count = (objPO == null) ? 0 : objPO.TotalRecord;
                    if (count > 0)
                    {
                        pupilForwardViewModel.Check = false;
                        lstPupilForwardViewModel.Add(pupilForwardViewModel);
                        continue;
                    }
                    // Kiem tra diem mon nhan xet
                    objPO = iqJR.FirstOrDefault(p => p.PupilId == pupilID);
                    count = (objPO == null) ? 0 : objPO.TotalRecord;
                    if (count > 0)
                    {
                        pupilForwardViewModel.Check = false;
                        lstPupilForwardViewModel.Add(pupilForwardViewModel);
                        continue;
                    }
                }
                lstPupilForwardViewModel.Add(pupilForwardViewModel);
            }

            return PartialView("_ListClassPupilForward", lstPupilForwardViewModel);
        }
    }
}
