﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TransferDataArea
{
    public class TransferDataAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TransferDataArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TransferDataArea_default",
                "TransferDataArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}