using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.TransferDataArea.Models
{
    public class ClassForwardViewModel
    {
        public int OldClassID { get; set; }

        [ResourceDisplayName("TransferData_Label_OldClassName")]
        public string OldClassName { get; set; }

        [ResourceDisplayName("TransferData_Label_TotalPupil")]
        public int TotalPupil { get; set; }

        [ResourceDisplayName("TransferData_Label_TotalMove")]
        public int TotalMove { get; set; }

        public int NewClassID { get; set; }

        [ResourceDisplayName("TransferData_Label_TotalMoved")]
        public int? TotalMoved { get; set; }

        /// <summary>
        /// Chi ra co cho phep ket chuyen khong.
        /// </summary>
        public bool DisableTransferData
        {
            get;
            set;
        }

        /// <summary>
        /// Tieu de cho cot checkbox ket chuyen
        /// </summary>
        public string TitleForTransferData
        {
            get;
            set;
        }

        /// <summary>
        /// Chi ra co cho phep huy ket chuyen khong.
        /// </summary>
        public bool DisableUntransferData
        {
            get;
            set;
        }

        /// <summary>
        /// Tieu de cho cot checkbox huy ket chuyen
        /// </summary>
        public string TitleForUntransferData
        {
            get;
            set;
        }
    }
}
