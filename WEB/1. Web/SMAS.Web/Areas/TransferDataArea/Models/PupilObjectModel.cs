﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TransferDataArea.Models
{
    public class PupilObjectModel
    {
        public int PupilId { get; set; }
        public int TotalRecord { get; set; }
        public int ClassId { get; set; }
    }
}