using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.TransferDataArea.Models
{
    public class TransferDataObject
    {
        //public int SemeterDeclarationID
        //{
        //    get;
        //    set;
        //}

        public int ClassForwardingID
        {
            get;
            set;
        }

        [Required]
        public int ClosedClassID
        {
            get;
            set;
        }

        public int RepeatedClassID
        {
            get;
            set;
        }

        [UIHint("ForwardedClassName"), Required]
        [ResourceDisplayName("ClassForwarding_Label_ForwardedClassName")]
        public string ForwardedClassName
        {
            get;
            set;
        }

        [UIHint("RepeatedClassName"), Required]
        [ResourceDisplayName("ClassForwarding_Label_RepeatedClassName")]
        public string RepeatedClassName
        {
            get;
            set;
        }

        [Required]
        [ResourceDisplayName("ClassForwarding_Label_ClosedClassName")]
        public string ClosedClassName
        {
            get;
            set;
        }

        [ResourceDisplayName("ClassForwarding_Label_TotalClosedPupil")]
        public int TotalClosedPupil
        {
            get;
            set;
        }

        [ResourceDisplayName("ClassForwarding_Label_TotalForwardedPupil")]
        public int? TotalForwardedPupil
        {
            get;
            set;
        }

        [ResourceDisplayName("ClassForwarding_Label_TotalRepeatedPupil")]
        public int? TotalRepeatedPupil
        {
            get;
            set;
        }

        [Required]
        public List<ClassinGeneral> ListForwardedClass
        {
            get;
            set;
        }

        [Required]
        public List<ClassinGeneral> ListRepeatedClass
        {
            get;
            set;
        }

        [ResourceDisplayName("ClassForwarding_Label_Choice")]
        public bool Choice
        {
            get;
            set;
        }

        public int? SchoolID
        {
            get;
            set;
        }

        public int AcademicYearID
        {
            get;
            set;
        }

        public int OrderNumber
        {
            get;
            set;
        }

        public Dictionary<object, object> dic { get; set; }

        public int TotalMove { get; set; }

        public int TotalMoved { get; set; }
    }

    public class ClassinGeneral
    {
        public int ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
