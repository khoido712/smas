using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.TransferDataArea.Models
{
    public class PupilRepeatedViewModel
    {
        public int PupilID { get; set; }

        public int PupilRepeatedID { get; set; }

        public int OldClassID { get; set; }

        public int RepeatedClassID { get; set; }

        public int AcademicYearID { get; set; }

        public int EducationLevelID { get; set; }

        public int Year { get; set; }

        public int SchoolID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }

        public int Genre { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        [ResourceDisplayName("TransferData_Label_OldClassName1")]
        public string OldClassName { get; set; }
    }
}
