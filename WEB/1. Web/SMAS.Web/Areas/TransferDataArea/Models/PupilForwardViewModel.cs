﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TransferDataArea.Models
{
    public class PupilForwardViewModel
    {
        public int PupilID { get; set; }

        public int PupilForwardID { get; set; }

        public int OldClassID { get; set; }

        public int ForwardClassID { get; set; }

        public int AcademicYearID { get; set; }

        public int EducationLevelID { get; set; }

        public int Year { get; set; }

        public int SchoolID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }

        public int Genre { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        [ResourceDisplayName("TransferData_Label_OldClassName1")]
        public string OldClassName { get; set; }

        public bool Check { get; set; }
    }
}