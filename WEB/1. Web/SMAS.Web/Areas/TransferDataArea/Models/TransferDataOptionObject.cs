﻿using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System;

namespace SMAS.Web.Areas.TransferDataArea.Models
{
    public class TransferDataOptionObject
    {
        [Required]
        public int PupilProfileID
        {
            get;
            set;
        }

        [Required]
        public int ClassID
        {
            get;
            set;
        }

        [Required]
        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName
        {
            get;
            set;
        }

        [Required]
        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public int Genre
        {
            get;
            set;
        }

        [Required]
        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime Birthday
        {
            get;
            set;
        }


        [Required]
        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode
        {
            get;
            set;
        }

        public int? SchoolID
        {
            get;
            set;
        }

        public int? AcademicYearID
        {
            get;
            set;
        }

        public bool? Choice
        {
            get;
            set;
        }

        public int OrderNumber
        {
            get;
            set;
        }
        public bool DisableTransferDataOption
        {
            get;
            set;
        }
        public string ReasonForDisableTransferDataOption
        {
            get;
            set;
        }
    }
}