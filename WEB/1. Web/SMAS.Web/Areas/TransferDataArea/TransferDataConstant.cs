﻿namespace SMAS.Web.Areas.TransferDataArea
{
    public class TransferDataConstant
    {
        public const string LS_CLOSEDCLASS = "LISTCLOSECLASS";
        public const string LS_EDUCATIONLEVELLASTYEAR = "LISTEDUCATIONLEVELLASTYEAR";
        public const string LS_FORWARDEDCLASSNAME = "LISTFORWARDEDCLASSNAME";
        public const string LS_LISTREPEATEDCLASSNAME = "LS_LISTREPEATEDCLASSNAME";

        public const string LS_LISTEDUCATIONLEVEL = "LS_LISTEDUCATIONLEVEL";
        public const string LS_LISTOLDCLASS = "LS_LISTOLDCLASS";
        public const string LS_LISTNEWCLASS = "LS_LISTNEWCLASS";

        public const string PERMISSION = "PERMISSION";
        public const string PERMISSION_FORM = "PERMISSION_FORM";

        public const string LS_CBOLDCLASS = "LS_CBOLDCLASS";
        public const string LS_CBNEWCLASS = "LS_CBNEWCLASS";

        public const string LIST_CLASS_FORWARD = "LIST_CLASS_FORWARD";
        public const string LIST_CLASS_UP = "LIST_CLASS_UP";

        public const string ENABLE_BUTTON_TRANSFER = "ENABLE_BUTTON_TRANSFER";
        public const string ENABLE_BUTTON_UNTRANSFER = "ENABLE_BUTTON_UNTRANSFER";
        public const string ENABLE_CHECKBOX = "ENABLE_CHECKBOX";

        public const string LST_PUPIL_REPEATED = "LIST_PUPIL_REPEATED";
        public const string LST_CLASS_STAY = "LIST_CLASS_STAY";
        public const string LST_EDUCATION_LEVEL_REPEATED = "LS_EDUCATIONLEVELREPEATED";
        public const string LST_CLASS_REPEATED = "LS_CLASSREPEATED";

        public const string OPTION_GRID_RIGHT_HAS_VALUE = "OPTION_GRID_RIGHT_HAS_VALUE";
        public const string OPTION_GRID_LEFT_HAS_VALUE = "OPTION_GRID_LEFT_HAS_VALUE";
        public const string ENABLE_BUTTON_SEMESTER = "ENABLE_BUTTON_SEMESTER";
        
        public const string ENABLE_CHECKBOX_FOR_TRANSFER_OPTION = "ENABLE_CHECKBOX_FOR_TRANSFER_OPTION";

        public const string SECOND_SEMESTER_STARTDATE = "SecondSemesterStartDate";
        public const string SECOND_SEMESTER_ENDDATE = "SecondSemesterEndDate";

        public const string LISTCLASS = "ListClass";
        public const string LIST_PUPIL_FORWARD = "LIST_PUPIL_FORWARD";
        public const string LST_CLASS_UP = "LIST_CLASS_UP";

        public const string COUNT_PUPIL_CHOOSE = "COUNT_PUPIL_CHOOSE";
        public const string COUNT_PUPIL_TRANSFER = "COUNT_PUPIL_TRANSFER";

        //nang luc
        public const int CAPACTIES = 2;

        //pham chat
        public const int QUALITIES = 3;
    }
}