﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.PolicyTargetArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.PolicyTargetArea.Controllers
{
    public class PolicyTargetController : BaseController
    {        
        private readonly IPolicyTargetBusiness PolicyTargetBusiness;
		
		public PolicyTargetController (IPolicyTargetBusiness policytargetBusiness)
		{
			this.PolicyTargetBusiness = policytargetBusiness;
		}
		
		//
        // GET: /PolicyTarget/

        #region setViewData
        private void SetViewData()
        {

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            IEnumerable<PolicyTargetViewModel> lst = this._Search(SearchInfo);            
            ViewData[PolicyTargetConstants.LIST_POLICYTARGET] = lst;


        }

        #endregion

        #region Index
        public ActionResult Index()
        {

            SetViewDataPermission("PolicyTarget", _globalInfo.UserAccountID, _globalInfo.IsAdmin);

            SetViewData();
            return View();
        }

        #endregion

        #region Create

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            PolicyTarget policytarget = new PolicyTarget();
            TryUpdateModel(policytarget);
            policytarget.IsActive = true;
            Utils.Utils.TrimObject(policytarget);

            this.PolicyTargetBusiness.Insert(policytarget);
            this.PolicyTargetBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        #endregion

        #region Edit
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int PolicyTargetID)
        {
            PolicyTarget policytarget = this.PolicyTargetBusiness.Find(PolicyTargetID);
            TryUpdateModel(policytarget);
            Utils.Utils.TrimObject(policytarget);
            this.PolicyTargetBusiness.Update(policytarget);
            this.PolicyTargetBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        #region Delete
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.PolicyTargetBusiness.Delete(id);
            this.PolicyTargetBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion

        #region Search
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        /// 
       
        public PartialViewResult Search(string Resolution)
        {
            // Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = Resolution;

            IEnumerable<PolicyTargetViewModel> lst = this._Search(SearchInfo);

            ViewData[PolicyTargetConstants.LIST_POLICYTARGET] = lst;

            SetViewDataPermission("PolicyTarget", _globalInfo.UserAccountID, _globalInfo.IsAdmin);

            return PartialView("_List");
        }
        private IEnumerable<PolicyTargetViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<PolicyTarget> query = this.PolicyTargetBusiness.Search(SearchInfo);
            IQueryable<PolicyTargetViewModel> lst = query.Select(o => new PolicyTargetViewModel {               
						PolicyTargetID = o.PolicyTargetID,								
						Resolution = o.Resolution,								
						Description = o.Description					
		
            });

            return lst.OrderBy(o=>o.Resolution).ToList();
        }

        #endregion
    }
}





