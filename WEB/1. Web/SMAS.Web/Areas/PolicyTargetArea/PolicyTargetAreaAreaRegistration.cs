﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PolicyTargetArea
{
    public class PolicyTargetAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PolicyTargetArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PolicyTargetArea_default",
                "PolicyTargetArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
