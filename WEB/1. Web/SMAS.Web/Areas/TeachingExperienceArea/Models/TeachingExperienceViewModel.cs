/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.TeachingExperienceArea.Models
{
    public class TeachingExperienceViewModel
    {
        [ScaffoldColumn(false)]
        public int TeachingExperienceID { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_Faculty")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", TeachingExperienceConstants.LIST_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacherByFaculty(this)")]
        public System.Nullable<int> SchoolFacultyID { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_Faculty")]
        [ScaffoldColumn(false)]
        public string SchoolFacultyName { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_TeacherOfFaculty")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int TeacherID { get; set; }

        [ResourceDisplayName("Employee_Column_FullName")]
        [ScaffoldColumn(false)]
        public string TeacherName { get; set; }

        [ResourceDisplayName("Employee_Column_FullName")]
        [ScaffoldColumn(false)]
        public string TeacherShortName { get; set; }

        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        [ScaffoldColumn(false)]
        public string TeacherCode { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_ExperienceType")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", TeachingExperienceConstants.LIST_EXPERIENCE_TYPE)]
        public int ExperienceTypeID { get; set; }
        [ResourceDisplayName("TeachingExperience_Label_ExperienceType")]
        [ScaffoldColumn(false)]
        public string ExperienceTypeName { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]        
        [DataType(DataType.MultilineText)]
        public string Resolution { get; set; }

        [ScaffoldColumn(false)]
        public int AcademicYearID { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_Grade")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", TeachingExperienceConstants.LIST_GRADE)]
        public string Grade { get; set; }
        [ResourceDisplayName("TeachingExperience_Label_Grade")]
        [ScaffoldColumn(false)]
        public string GradeName { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_RegisteredLevel")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", TeachingExperienceConstants.LIST_REGISTERED_LEVEL)]
        public System.Nullable<int> RegisteredLevel { get; set; }
        [ResourceDisplayName("TeachingExperience_Label_RegisteredLevel")]
        [ScaffoldColumn(false)]
        public string RegisteredLevelName { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_Description")]        
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<DateTime> RegisteredDate { get; set; }       

    }
}


