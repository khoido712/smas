/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.TeachingExperienceArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("TeachingExperience_Label_RegisteredLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("OnChange", "search()")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", TeachingExperienceConstants.LIST_REGISTERED_LEVEL)]
        public System.Nullable<int> RegisteredLevel { get; set; }

        [ResourceDisplayName("TeachingExperience_Label_Grade")]
        [UIHint("Combobox")]
        [AdditionalMetadata("OnChange", "search()")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", TeachingExperienceConstants.LIST_GRADE)]
        public string Grade { get; set; }
    }
}
