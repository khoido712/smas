﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.TeachingExperienceArea.Models;

namespace SMAS.Web.Areas.TeachingExperienceArea.Controllers
{
    public class TeachingExperienceController : BaseController
    {
        private readonly ITeachingExperienceBusiness TeachingExperienceBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IExperienceTypeBusiness ExperienceTypeBusiness;

        public TeachingExperienceController(ITeachingExperienceBusiness teachingexperienceBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness,
            IEmployeeBusiness EmployeeBusiness,
            IExperienceTypeBusiness ExperienceTypeBusiness)
        {
            this.TeachingExperienceBusiness = teachingexperienceBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ExperienceTypeBusiness = ExperienceTypeBusiness;
        }

        //
        // GET: /TeachingExperience/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();
            int schoolID = globalInfo.SchoolID.HasValue ? globalInfo.SchoolID.Value : 0;
            SearchInfo["SchoolID"] = schoolID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
           // SearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            //Get view data here

            IEnumerable<TeachingExperienceViewModel> lst = this._Search(SearchInfo);
            ViewData[TeachingExperienceConstants.LIST_TEACHINGEXPERIENCE] = lst;
            // Chuan bi du lieu cho combobox
            // To bo mon
            IQueryable<SchoolFaculty> IQSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo).OrderBy(o=>o.FacultyName);
            SelectList ListSchoolFaculty =
                new SelectList((IEnumerable<SchoolFaculty>)IQSchoolFaculty.ToList(), "SchoolFacultyID", "FacultyName");
            ViewData[TeachingExperienceConstants.LIST_FACULTY] = ListSchoolFaculty;
            // Employee
            ViewData[TeachingExperienceConstants.LIST_EMPLOYEE] = new SelectList(new List<Employee>());
            // Experience type
            IQueryable<ExperienceType> IQExperienceType = ExperienceTypeBusiness.Search(SearchInfo).OrderBy(o => o.Resolution);
            ViewData[TeachingExperienceConstants.IS_ENABLE_NOTE] = IQExperienceType.Count() == 0 ? true : false;
            SelectList ListExperienceType =
                new SelectList((IEnumerable<ExperienceType>)IQExperienceType.ToList(), "ExperienceTypeID", "Resolution");
            ViewData[TeachingExperienceConstants.LIST_EXPERIENCE_TYPE] = ListExperienceType;
            // Xep loai
            List<ComboObject> ListGradeCombo = new List<ComboObject>();
            ListGradeCombo.Add(new ComboObject(SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_A,
                Res.Get("Teaching_Experience_Grade_A")));
            ListGradeCombo.Add(new ComboObject(SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_B,
                Res.Get("Teaching_Experience_Grade_B")));
            ListGradeCombo.Add(new ComboObject(SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_C,
                Res.Get("Teaching_Experience_Grade_C")));
            SelectList ListGrade =
                new SelectList(ListGradeCombo, "key", "value");
            ViewData[TeachingExperienceConstants.LIST_GRADE] = ListGrade;

            // De tai cap
            List<ComboObject> ListLevelCombo = new List<ComboObject>();
            ListLevelCombo.Add(new ComboObject(SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_MINISTRY.ToString(),
                Res.Get("Education_Hierachy_Level_Ministry")));
            ListLevelCombo.Add(new ComboObject(SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE.ToString(),
                Res.Get("Education_Hierachy_Level_Province_Office")));
            ListLevelCombo.Add(new ComboObject(SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE.ToString(),
                Res.Get("Education_Hierachy_District_Office")));
            ListLevelCombo.Add(new ComboObject(SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_SCHOOL.ToString(),
                Res.Get("Education_Hierachy_Level_School")));
            SelectList ListLevel =
                new SelectList(ListLevelCombo, "key", "value");
            ViewData[TeachingExperienceConstants.LIST_REGISTERED_LEVEL] = ListLevel;

            ViewData[TeachingExperienceConstants.IS_CURRENT_YEAR] = globalInfo.IsCurrentYear;

            return View();
        }

        /// <summary>
        ///       * Thuc hien load du lieu Can bo khi co thong tin truong va to bo mon
        /// </summary>
        /// <param name="ProvinceId"></param>
        /// <param name="DistrictId"></param>
        /// <returns></returns>           

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingTeacherByFaculty(int? SchoolFacultyID)
        {
            IEnumerable<Employee> lst = new List<Employee>();
            if (SchoolFacultyID.HasValue)
            {
                GlobalInfo globalInfo = new GlobalInfo();
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(globalInfo.SchoolID.Value, SchoolFacultyID.Value).OrderBy(o => o.Name).ThenBy(o=>o.FullName).ToList();
            }
            
            if (lst == null)
                lst = new List<Employee>();
           // lst = lst.Where(o => o.AppliedLevel == new GlobalInfo().AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }

        //
        // GET: /TeachingExperience/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["RegisteredLevel"] = frm.RegisteredLevel;
            SearchInfo["Grade"] = frm.Grade;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
           // SearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            // end

            IEnumerable<TeachingExperienceViewModel> lst = this._Search(SearchInfo);
            ViewData[TeachingExperienceConstants.LIST_TEACHINGEXPERIENCE] = lst;

            //Get view data here
            ViewData[TeachingExperienceConstants.IS_CURRENT_YEAR] = global.IsCurrentYear;
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (this.GetMenupermission("TeachingExperience", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            TeachingExperience teachingexperience = new TeachingExperience();
            TryUpdateModel(teachingexperience);
            Utils.Utils.TrimObject(teachingexperience);
            teachingexperience.AcademicYearID = globalInfo.AcademicYearID.Value;
            teachingexperience.SchoolID = globalInfo.SchoolID.Value;

            this.TeachingExperienceBusiness.Insert(teachingexperience);
            this.TeachingExperienceBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int TeachingExperienceID)
        {
            this.CheckPermissionForAction(TeachingExperienceID, "TeachingExperience");
            GlobalInfo globalInfo = new GlobalInfo();
            if (this.GetMenupermission("TeachingExperience", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            TeachingExperience teachingexperience = this.TeachingExperienceBusiness.Find(TeachingExperienceID);
            TryUpdateModel(teachingexperience);
            Utils.Utils.TrimObject(teachingexperience);
            this.TeachingExperienceBusiness.Update(teachingexperience);
            this.TeachingExperienceBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "TeachingExperience");
            GlobalInfo globalInfo = new GlobalInfo();
            if (this.GetMenupermission("TeachingExperience", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.TeachingExperienceBusiness.Delete(id);
            this.TeachingExperienceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<TeachingExperienceViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[TeachingExperienceConstants.HAS_PERMISSION] = GetMenupermission("TeachingExperience", global.UserAccountID, global.IsAdmin);
            IQueryable<TeachingExperience> query = from p in this.TeachingExperienceBusiness.Search(SearchInfo)
                                                   join q in EmployeeBusiness.All on p.TeacherID equals q.EmployeeID
                                                   where q.IsActive == true
                                                   select p;
            IQueryable<TeachingExperienceViewModel> lst = query.Select(o => new TeachingExperienceViewModel
            {
                TeachingExperienceID = o.TeachingExperienceID,
                Resolution = o.Resolution,
                TeacherID = o.TeacherID,
                TeacherName = o.Employee != null ? o.Employee.FullName : string.Empty,
                TeacherShortName  = o.Employee != null ? o.Employee.Name : string.Empty,
                TeacherCode = o.Employee != null ? o.Employee.EmployeeCode : string.Empty,
                AcademicYearID = o.AcademicYearID,
                SchoolFacultyID = o.FacultyID,
                SchoolFacultyName = o.SchoolFaculty != null ? o.SchoolFaculty.FacultyName : string.Empty,
                ExperienceTypeID = o.ExperienceTypeID,
                ExperienceTypeName = o.ExperienceType != null ? o.ExperienceType.Resolution : string.Empty,
                RegisteredLevel = o.RegisteredLevel,
                Grade = o.Grade,
                RegisteredDate = o.RegisteredDate,
                Description = o.Description
            });

            IEnumerable<TeachingExperienceViewModel> listRes = lst.ToList();
            foreach (TeachingExperienceViewModel item in listRes)
            {
                // Lay ten cap de tai
                if (SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_MINISTRY == item.RegisteredLevel)
                {
                    item.RegisteredLevelName = Res.Get("Education_Hierachy_Level_Ministry");
                }
                else if (SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE == item.RegisteredLevel)
                {
                    item.RegisteredLevelName = Res.Get("Education_Hierachy_Level_Province_Office");
                }
                else if (SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE == item.RegisteredLevel)
                {
                    item.RegisteredLevelName = Res.Get("Education_Hierachy_District_Office");
                }
                else if (SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_SCHOOL == item.RegisteredLevel)
                {
                    item.RegisteredLevelName = Res.Get("Education_Hierachy_Level_School");
                }

                // Lay thong tin xep loai
                if (SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_A == item.Grade)
                {
                    item.GradeName = Res.Get("Teaching_Experience_Grade_A");
                }
                else if (SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_B == item.Grade)
                {
                    item.GradeName = Res.Get("Teaching_Experience_Grade_B");
                }
                else if (SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_C == item.Grade)
                {
                    item.GradeName = Res.Get("Teaching_Experience_Grade_C");
                }
            }

            return listRes.OrderBy(o => o.TeacherShortName).ThenBy(o => o.TeacherName).ToList() ;
        }
    }
}





