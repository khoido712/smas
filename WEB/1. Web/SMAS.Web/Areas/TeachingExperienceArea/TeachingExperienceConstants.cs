/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.TeachingExperienceArea
{
    public class TeachingExperienceConstants
    {
        public const string LIST_TEACHINGEXPERIENCE = "listTeachingExperience";
        public const string LIST_FACULTY = "listFaculty";
        public const string LIST_EMPLOYEE = "listEmployee";
        public const string LIST_GRADE = "listGrade";
        public const string LIST_REGISTERED_LEVEL = "listRegisteredLevel";
        public const string LIST_EXPERIENCE_TYPE = "listExperienceType";
        public const string IS_ENABLE_NOTE = "IsEnableNote";

        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
    }
}