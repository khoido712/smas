﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TeachingExperienceArea
{
    public class TeachingExperienceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TeachingExperienceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TeachingExperienceArea_default",
                "TeachingExperienceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
