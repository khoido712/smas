﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSActivityPlanArea
{
    public class SCSActivityPlanConstants
    {
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTCLASS = "LISTCLASS";
        public const string LIST_BLOCK = "LIST_BLOCK";
        public const string LIST_ACTIVITYPLAN = "LIST_ACTIVITYPLAN";

        public const string LIST_ACTIVITY_TYPE = "LIST_ACTIVITY_TYPE";
        public const string LIST_ACTIVITY_TYPE_ITEM = "LIST_ACTIVITY_TYPE_ITEM";
        public const string SEARCHFORM = "SCSActivityPlanConstants_SEARCHFORM";

        public const string VISIBLE_ORDER = "VISIBLE_ORDER";

        public const string PERVIOUSGRADUATION_LEVEL1 = "PERVIOUSGRADUATION_LEVEL1";

        public const string ENABLE_PAGING = "ENABLE_PAGING";
        public const string PUPIL_NOT_STUDY = "PUPIL_NOT_STUDY";
        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";
        public const string IS_ENBLE_Replication = "enable button replication";
        public const string REPLICATION_INFORMATION = "replication information";
        public const string DateEndAcad = "DateEndAcad";

        public const string LST_MONTH = "listMonth";
        public const string FROM_TIME = "FROM_TIME";
        public const string FROM_DATE = "FROM_DATE";
        public const string ACTIVITY_PLAN_ID = "ActivityPlanID";
        public const string APPLED_CLASS = "AppledClass";
        public const string ACTIVITY_PLAN_NAME = "ActivityPlanName";
        public const string TO_MONTH = "TO_MONTH";
        public const string TO_YEAR = "TO_YEAR";
        public const string EDUCATION_LEVEL = "EDUCATION_LEVEL";
    }
}