﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SCSActivityPlanArea
{
    public class SCSActivityPlanAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SCSActivityPlanArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SCSActivityPlanArea_default",
                "SCSActivityPlanArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
