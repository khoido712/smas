﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.SCSActivityPlanArea.Models
{
    public class ActivityViewModel
    {
        [ScaffoldColumn(false)]
        public int? ActivityID { get; set; }

        public int? ActivityPlanID { get; set; }

        public byte? DayOFWeek { get; set; }

        public System.DateTime? ActivityDate { get; set; }

        public string ActivityName { get; set; }

        public int? ActivityTypeID { get; set; }

        public string ActivityTypeName { get; set; }

        public System.TimeSpan? FromTime { get; set; }

        public System.TimeSpan? ToTime { get; set; }

       





    }
}