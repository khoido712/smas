﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSActivityPlanArea.Models
{
    public class ClassProfileItem
    {
        public int ClasslID { get; set; }
        public string ClassName { get; set; }
        public bool? Checked { get; set; }
    }
}