﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SCSActivityPlanArea.Models
{
    public class ActivityPlanViewModel
    {
        [ResourceDisplayName("Common_Column_Order")]
        public int? STT { get; set; }
        [ResourceDisplayName("ActivityPlan_Label_ActivityPlanName")]
        public string ActivityNamePlan { get; set; }
        [ResourceDisplayName("ActivityPlan_Label_AppliedTime")]
        public string AppliedTime { get; set; }
        [ResourceDisplayName("ActivityPlan_Label_AppliedClass")]
        public string AppliedClass { get; set; }
        public int ActivityPlanID { get; set; }
        public bool checkDelete { get; set; }
        public bool IsActive { get; set; }

    }
}