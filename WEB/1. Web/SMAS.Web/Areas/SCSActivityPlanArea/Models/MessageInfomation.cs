﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSActivityPlanArea.Models
{
    public class MessageInfomation
    {
        public MessageInfomation(int type,string content)
        {
            this.Type = type;
            this.Content = content;
        }
        public int Type {get; set; }
        public string Content { get; set; }
    }
}