/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Models.CustomAttribute;
using System;

namespace SMAS.Web.Areas.SCSActivityPlanArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel_MN")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SCSActivityPlanConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("PupilRanking_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SCSActivityPlanConstants.LISTCLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("ActivityPlan_Label_ActivityPlanName")]
        [UIHint("Textbox")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [MaxLength(300)]
        public string ActivityPlanName { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? FromDate { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? ToDate { get; set; }
        [ScaffoldColumn(false)]
        public string ToMonthYear { get; set; }
        [ScaffoldColumn(false)]
        public string ToYear { get; set; }

        //[ResourceDisplayName("Common_Label_Time")]
        //[DataType(DataType.Date)]
        //[UIHint("DateTimePicker")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[DataConstraint(DataConstraintAttribute.LESS_EQUALS, "ToDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        //public DateTime? FromDate { get; set; }

        //[ResourceDisplayName("-")]
        //[DataType(DataType.Date)]
        //[UIHint("DateTimePicker")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //public DateTime? ToDate { get; set; }
    }
}