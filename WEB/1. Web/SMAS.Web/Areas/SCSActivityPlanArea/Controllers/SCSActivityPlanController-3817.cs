﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.SCSActivityPlanArea.Models;
using SMAS.Business.BusinessObject;
using System.Text;
using SMAS.Business.Common;
using SMAS.VTUtils.Log;
using SMAS.VTUtils.Excel.Export;
using System.IO;
//using System.IO;

namespace SMAS.Web.Areas.SCSActivityPlanArea.Controllers
{
    public class SCSActivityPlanController : BaseController
    {
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IActivityBusiness ActivityBusiness;
        //private readonly IActivityOfClassBusiness ActivityOfClassBusiness;
        private readonly IActivityPlanBusiness ActivityPlanBusiness;
        private readonly IActivityTypeBusiness ActivityTypeBusiness;
        private readonly IActivityPlanClassBusiness ActivityPlanClassBusiness;
        private readonly IActivityOfPupilBusiness ActivityOfPupilBusiness;

        private GlobalInfo gl = new GlobalInfo();
        //private const string TEMPLATE_PUPILPROFILE = "BM_KeHoachHoatDong.xls";
        //private const string TEMPLATE_PUPILPROFILE_FORMAT = "BM_DanhSachTre{0}{1}{2}.xls";
        public SCSActivityPlanController(IPupilProfileBusiness pupilprofileBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IActivityBusiness ActivityBusiness
            , IActivityPlanBusiness ActivityPlanBusiness
            , IActivityTypeBusiness ActivityTypeBusiness
            , IActivityPlanClassBusiness ActivityPlanClassBusiness,
            //IActivityOfClassBusiness ActivityOfClassBusiness,
            IActivityOfPupilBusiness ActivityOfPupilBusiness
            )
        {
            this.PupilProfileBusiness = pupilprofileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ActivityBusiness = ActivityBusiness;
            this.ActivityPlanBusiness = ActivityPlanBusiness;
            this.ActivityTypeBusiness = ActivityTypeBusiness;
            this.ActivityPlanClassBusiness = ActivityPlanClassBusiness;
            //this.ActivityOfClassBusiness = ActivityOfClassBusiness;
            this.ActivityOfPupilBusiness = ActivityOfPupilBusiness;
        }

        #region load trang index
        public ActionResult Index(bool restoreSession = false)
        {
            string SCSActivityPlan = "SCSActivityPlan";
            SetViewDataPermissionWithArea(SCSActivityPlan, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            ViewData[SCSActivityPlanConstants.TO_MONTH] = null;
            ViewData[SCSActivityPlanConstants.TO_YEAR] = null;
            if (_globalInfo.IsCurrentYear)
            {
                ViewData[SCSActivityPlanConstants.DateEndAcad] = DateTime.Now.Date;
            }
            else
            {
                DateTime dateEnd = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).SecondSemesterEndDate.Value;
                ViewData[SCSActivityPlanConstants.DateEndAcad] = dateEnd;
            }
            ViewData[SCSActivityPlanConstants.LST_MONTH] = new SelectList(new string[] { });
            SetViewData();
            LoadMonth();
            SearchViewModel searchViewModel;
            if (restoreSession) // Neu bien restoreSession bang tru thi lay lai cac thong tin tim kiem truoc do tu session
            {
                if (Session[SCSActivityPlanConstants.SEARCHFORM] != null)
                {
                    searchViewModel = Session[SCSActivityPlanConstants.SEARCHFORM] as SearchViewModel;
                    this.Search(searchViewModel);
                    ViewData[SCSActivityPlanConstants.TO_MONTH] = searchViewModel.ToMonthYear;
                    ViewData[SCSActivityPlanConstants.TO_YEAR] = searchViewModel.ToYear;
                }
                else
                {
                    searchViewModel = null;
                }
            }
            else
            {
                searchViewModel = null;
            }

            return View(searchViewModel);
        }
        #endregion

        #region Load Month
        private void LoadMonth()
        {
            if (gl.AcademicYearID.HasValue)
            {
                AcademicYear ay = AcademicYearBusiness.Find(gl.AcademicYearID.Value);
                List<ComboObject> ls = new List<ComboObject>();

                int year = DateTime.Now.Year;
                string start = "";
                string end = "";
                int startYear = ay.FirstSemesterStartDate.Value.Year;
                int endYear = ay.SecondSemesterEndDate.Value.Year;
                int startMonth = ay.FirstSemesterStartDate.Value.Month;
                int endMonth = ay.SecondSemesterEndDate.Value.Month;
                int MonthNow = DateTime.Now.Month;
                string monthDefault = MonthNow + "/" + year;
                if (MonthNow < 10)
                {
                    monthDefault = "0" + MonthNow + "/" + year;
                }
                for (int i = startMonth; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        start = Convert.ToString("0" + i + "/" + startYear);
                    }
                    else
                    {
                        start = Convert.ToString(i + "/" + startYear);
                    }
                    ls.Add(new ComboObject(Convert.ToString(i), start));
                }
                if (endYear > startYear)
                {
                    for (int j = 1; j <= endMonth; j++)
                    {
                        if (j < 10)
                        {
                            end = Convert.ToString("0" + j + "/" + endYear);
                        }
                        else
                        {
                            end = Convert.ToString(j + "/" + endYear);
                        }
                        ls.Add(new ComboObject(Convert.ToString(j + 12), end));
                    }
                }
                var check = ls.Where(o => o.value.Contains(monthDefault)).FirstOrDefault();
                int def = Convert.ToInt32(ls.LastOrDefault().key);
                if (check != null)
                {
                    def = Convert.ToInt32(check.key);
                }
                ViewData[SCSActivityPlanConstants.LST_MONTH] = new SelectList(ls, "key", "value", def);
            }
            else
            {
                ViewData[SCSActivityPlanConstants.LST_MONTH] = new SelectList(new string[] { });
            }
        }
        #endregion

        #region Tìm kiếm
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            Session[SCSActivityPlanConstants.SEARCHFORM] = frm;
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            if (frm.FromDate > frm.ToDate)
            {
                throw new BusinessException("SCSActivityPlan_Label_FromDateToDate");
            }
            //add search infor
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["SchoolID"] = global.SchoolID.Value;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["ActivityPlanName"] = frm.ActivityPlanName;
            SearchInfo["FromDate"] = frm.FromDate;
            SearchInfo["ToDate"] = frm.ToDate;
            SearchInfo["ToMonthYear"] = frm.ToMonthYear;
            SearchInfo["IsActive"] = true;
            var lst = this._Search(SearchInfo);
            List<int> lstActivityPlanID = lst.Select(p => p.ActivityPlanID).Distinct().ToList();
            List<ActivityPlanViewModel> lstView = new List<ActivityPlanViewModel>();
            List<Activity> lstActivity = ActivityBusiness.All.Where(t => lstActivityPlanID.Contains(t.ActivityPlanID)).ToList();
            ActivityPlanViewModel obj;
            int count = 0;
            for (var i = 0; i < lst.Count(); i++)
            {
                obj = new ActivityPlanViewModel();
                var x = lst[i];
                var activityPlanID = x.ActivityPlanID;
                count = lstActivity.Count(p => p.ActivityPlanID == activityPlanID);
                obj.checkDelete = count > 0 ? true : false;
                obj.ActivityPlanID = x.ActivityPlanID;
                obj.ActivityNamePlan = x.ActivityNamePlan;
                obj.AppliedTime = x.AppliedTime;
                obj.STT = x.STT;
                obj.AppliedClass = x.AppliedClass;
                obj.IsActive = x.IsActive == true;
                lstView.Add(obj);
            }
            ViewData[SCSActivityPlanConstants.LIST_ACTIVITYPLAN] = lstView;

            // Luu vao ViewData
            if (global.IsCurrentYear)
            {
                ViewData[SCSActivityPlanConstants.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[SCSActivityPlanConstants.IS_CURRENT_YEAR] = false;
            }
            return PartialView("_List");
        }
        private List<ActivityPlanViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            string SCSActivityPlan = "SCSActivityPlan";
            SetViewDataPermissionWithArea(SCSActivityPlan, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            GlobalInfo global = new GlobalInfo();
            List<ActivityPlanBO> lstActPlan;
            var monthoryear = int.Parse(SearchInfo["ToMonthYear"].ToString());
            int schoolID = global.SchoolID.Value;
            List<ActivityPlanViewModel> lstModel = new List<ActivityPlanViewModel>();
            if (monthoryear != 0)
            {
                lstActPlan =
                ActivityPlanBusiness.SearchBySchool(schoolID, SearchInfo)
                .OrderBy(o => o.FromDate.Year)
                .ThenBy(o => o.FromDate.Month)
                .ThenBy(o => o.FromDate.Day)
                .Where(o => o.FromDate.Month == monthoryear)
                .ToList();
            }
            else
            {
                lstActPlan =
                    ActivityPlanBusiness.SearchBySchool(schoolID, SearchInfo)
                    .OrderBy(o => o.FromDate.Year)
                    .ThenBy(o => o.FromDate.Month)
                    .ThenBy(o => o.FromDate.Day)
                    .ToList();
            }
            if (lstActPlan.Count > 0)
            {
                int Count = lstActPlan.Count;
                int index = 0;
                List<String> lstClassName = new List<string>();
                while (index <= lstActPlan.Count - 1)
                {
                    Count = lstActPlan.Count;
                    ActivityPlanBO activityPlan = lstActPlan[index];
                    StringBuilder content = new StringBuilder();
                    content.Append(activityPlan.ClassName).Append(",");
                    int i = index + 1;
                    while (i < Count)
                    {
                        ActivityPlanBO obj = lstActPlan[i];
                        if (obj.ActivityPlanID == activityPlan.ActivityPlanID)
                        {
                            content.Append(obj.ClassName).Append(",");
                            lstActPlan.RemoveAt(i);
                            Count = lstActPlan.Count;
                        }
                        else i++;
                    }
                    content.Remove(content.Length - 1, 1);
                    lstClassName.Add(content.ToString());
                    index++;
                }
                for (int i = 0; i < lstActPlan.Count; i++)
                {
                    ActivityPlanBO obj = lstActPlan[i];
                    lstModel.Add(new ActivityPlanViewModel()
                    {
                        STT = i + 1,
                        ActivityNamePlan = obj.ActivityPlanName,
                        ActivityPlanID = obj.ActivityPlanID,
                        IsActive = obj.IsActive,
                        AppliedTime = obj.FromDate.ToString(Constants.GlobalConstants.CUSTOM_FORMAT_DATETIME_DDMMYY) + " - " + obj.ToDate.ToString(Constants.GlobalConstants.CUSTOM_FORMAT_DATETIME_DDMMYY),
                        AppliedClass = lstClassName.ElementAt(i)
                    });
                }
            }
            return lstModel;
        }
        #endregion
        #region Popup sao chép nhiều tuần
        public PartialViewResult CoppyManyWeek(int ActivityPlanID, string ActivityNamePlan, string AppliedClass)
        {
            gl = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = gl.AcademicYearID;
            SearchInfo["AppliedLevel"] = gl.AppliedLevel;

            ViewData[SCSActivityPlanConstants.LST_MONTH] = new SelectList(new string[] { });
            LoadMonth();
            ViewData[SCSActivityPlanConstants.APPLED_CLASS] = AppliedClass;
            ViewData[SCSActivityPlanConstants.ACTIVITY_PLAN_NAME] = ActivityNamePlan;
            ViewData[SCSActivityPlanConstants.ACTIVITY_PLAN_ID] = ActivityPlanID;

            var lstPlanAcitvity = ActivityPlanBusiness.SearchBySchool(gl.SchoolID.Value, SearchInfo).Where(t => t.IsActive == true);
            ActivityPlanBO objBO = lstPlanAcitvity.FirstOrDefault(t => t.ActivityPlanID == ActivityPlanID);
            List<ActivityPlan> lst = new List<ActivityPlan>();
            List<ActivityPlan> lst2 = new List<ActivityPlan>();
            ActivityPlan obj = null;
            var lstActivity = (from a in ActivityBusiness.All
                               join b in lstPlanAcitvity on a.ActivityPlanID equals b.ActivityPlanID
                               select a).Distinct().ToList();
            for (int i = 0; i < lstActivity.Count(); i++)
            {
                obj = new ActivityPlan();
                var item = lstActivity[i];
                var activityOfpuid = ActivityOfPupilBusiness.All.Where(t => t.AcademicYearID == gl.AcademicYearID.Value & t.SchoolID == gl.SchoolID.Value & t.ActivityOfClassID == item.ActivityID).Select(t => t.ActivityOfPupilID).FirstOrDefault();
                if (activityOfpuid > 0)
                {
                    obj.FromDate = lstPlanAcitvity.Where(t => t.ActivityPlanID == item.ActivityPlanID).Select(t => t.FromDate).FirstOrDefault();
                }
                lst.Add(obj);
            }
            ActivityPlan obj2 = new ActivityPlan();
            if (objBO != null) obj2.FromDate = objBO.FromDate;
            lst.Add(obj2);
            var lstFromTime = lst.Select(t => t.FromDate).Distinct().ToList();
            var rs = string.Join(",", lstFromTime.ToArray());
            ViewData[SCSActivityPlanConstants.FROM_TIME] = rs;
            return PartialView("_AplipedManyWeek", ViewData[SCSActivityPlanConstants.LST_MONTH]);
        }
        #endregion

        #region Sao chép áp dụng cho nhiều tuần
        [HttpPost]
        public ActionResult AjaxAplipedmanyDoSave(string listday, int activityPlanID, string lstClassApplide)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
            SearchInfo["IsActive"] = true;
            List<int> lstClassId = new List<int>();
            if (!string.IsNullOrEmpty(listday) && activityPlanID != 0)
            {
                if (!string.IsNullOrEmpty(lstClassApplide))
                {
                    List<string> lstClass = new List<string>(
                        lstClassApplide.Trim().Split(new string[] { "," },
                        StringSplitOptions.RemoveEmptyEntries));

                    List<ClassProfile> lstClassProfile = ClassProfileBusiness.All.Where(c => c.AcademicYearID == _globalInfo.AcademicYearID && lstClass.Contains(c.DisplayName) && c.IsActive == true).ToList();
                    foreach (var ite in lstClassProfile)
                    {
                        lstClassId.Add(ite.ClassProfileID);
                    }
                    SearchInfo["lstClassID"] = lstClassId;
                }
                List<string> days = new List<string>(
                    listday.Split(new string[] { "," },
                    StringSplitOptions.RemoveEmptyEntries));

                GlobalInfo globalInfo = new GlobalInfo();
                var ActivityPlan_obj = ActivityPlanBusiness.All.Where(t => t.ActivityPlanID == activityPlanID);
                //list Activity where ActivityPlan
                List<ActivityBO> ListActivity = (from acc in ActivityBusiness.All.Where(t => t.ActivityPlanID == activityPlanID)
                                                 select new ActivityBO
                                                 {
                                                     ActivityDate = acc.ActivityDate,
                                                     ActivityID = acc.ActivityPlanID,
                                                     ActivityName = acc.ActivityName,
                                                     ActivityPlanID = acc.ActivityPlanID,
                                                     ActivityTypeID = acc.ActivityTypeID,
                                                     DayOfWeek = acc.DayOfWeek,
                                                     FromTime = acc.FromTime,
                                                     Section = acc.Section,
                                                     ToTime = acc.ToTime
                                                 }).ToList();

                if (ActivityPlan_obj.Count() > 0)
                {
                    foreach (var item in days)
                    {
                        var formDateday = item;
                        var year = 0;
                        int count = item.Length;
                        var monthofyear = 0;
                        var toDatedayweek = 0;
                        if (count == 10) // dd/mm/yyyy
                        {
                            year = int.Parse(item.Substring(6));
                            monthofyear = int.Parse(item.Substring(3).Substring(0, 2));
                            toDatedayweek = int.Parse(item.Substring(0, 2)) + 5;
                        }
                        var toDateday = "";
                        //tong ngay trong thang
                        int daystotal = DateTime.DaysInMonth(year, monthofyear);
                        if (toDatedayweek > daystotal)
                        {
                            toDatedayweek = toDatedayweek - daystotal;
                            monthofyear = monthofyear + 1;
                            if (monthofyear < 10)
                            {
                                if (toDatedayweek < 10)
                                {
                                    toDateday = "0" + toDatedayweek + "/" + "0" + monthofyear + "/" + year;
                                }
                                else
                                {
                                    toDateday = toDatedayweek + "/" + "0" + monthofyear + "/" + year;
                                }
                            }
                            else
                            {
                                if (toDatedayweek < 10)
                                {
                                    toDateday = "0" + toDatedayweek + "/" + monthofyear + "/" + year;
                                }
                                else
                                {
                                    toDateday = toDatedayweek + "/" + monthofyear + "/" + year;
                                }
                            }
                        }
                        else
                        {
                            if (monthofyear < 10)
                            {
                                if (toDatedayweek < 10)
                                {
                                    toDateday = "0" + toDatedayweek + "/" + "0" + monthofyear + "/" + year;
                                }
                                else
                                {
                                    toDateday = toDatedayweek + "/" + "0" + monthofyear + "/" + year;
                                }
                            }
                            else
                            {
                                if (toDatedayweek < 10)
                                {
                                    toDateday = "0" + toDatedayweek + "/" + monthofyear + "/" + year;
                                }
                                else
                                {
                                    toDateday = toDatedayweek + "/" + monthofyear + "/" + year;
                                }
                            }
                        }
                        DateTime ngayhientai = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                        //list ClassApplied where ActivityPlan
                        var lstClassApplied = ActivityPlanClassBusiness.All.Where(t => t.ActivityPlanID == activityPlanID);
                        //var rs = lstClassApplied.Count();
                        #region Thêm mới sao chép
                        if (activityPlanID > 0)
                        {
                            // them moi ActivityPlan
                            ActivityPlan obj = new ActivityPlan();
                            obj.AcademicYearID = globalInfo.AcademicYearID.Value;
                            obj.SchoolID = globalInfo.SchoolID.Value;
                            obj.ActivityPlanName = ActivityPlan_obj.Select(t => t.ActivityPlanName).FirstOrDefault();
                            obj.PlannedDate = ngayhientai.Date;
                            obj.FromDate = DateTime.Parse(formDateday).Date;
                            obj.ToDate = DateTime.Parse(toDateday).Date;
                            obj.CreatedDate = DateTime.Now;
                            obj.IsActive = true;
                            try
                            {
                                SearchInfo["FromDate"] = obj.FromDate;
                                SearchInfo["ToDate"] = obj.ToDate;
                                /*foreach (var classId in lstClassId)
                                {
                                    SearchInfo["ClassID"] = classId;
                                    var lstActivityPlan = ActivityPlanBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();
                                    foreach (var it in lstActivityPlan)
                                    {
                                        var Act = ActivityPlanBusiness.Find(it.ActivityPlanID);
                                        Act.IsActive = false;
                                        ActivityPlanBusiness.Update(Act);
                                    }
                                    ActivityPlanBusiness.Save();
                                }*/

                                ActivityPlanBusiness.Insert(obj);
                                ActivityPlanBusiness.Save();
                            }
                            catch (Exception ex)
                            {
                                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                                LogExtensions.ErrorExt(logger, DateTime.Now, "DataViewModel", ex.Message, "");
                                return Json(new { Type = "error", Message = "Có lỗi thêm mới kế hoạch" });
                            }
                            // them moi ActivityPlanClas
                            int APID = obj.ActivityPlanID;
                            List<int> ClassApplied = lstClassApplied.Select(t => t.ClassID).ToList();
                            List<ActivityPlanClass> lstPlanClassIns = new List<ActivityPlanClass>();
                            for (int i = 0, CountClass = ClassApplied.Count; i < CountClass; i++)
                            {
                                lstPlanClassIns.Add(new ActivityPlanClass()
                                {
                                    ActivityPlanID = APID,
                                    ClassID = ClassApplied[i]
                                });
                            }
                            try
                            {
                                ActivityPlanClassBusiness.Insert(lstPlanClassIns);
                                ActivityPlanClassBusiness.Save();
                            }
                            catch (Exception)
                            {
                                return Json(new { Type = "error", Message = "Có lỗi tiến hành áp dụng lớp" });
                            }
                            // get list date trong khoảng thời gian

                            
                                //List<Activity> lstActivityClient = ListActivity;
                                Activity item2;
                                List<DateTime> allDates = new List<DateTime>();
                                var startDate = DateTime.Parse(formDateday);
                                var endDate = DateTime.Parse(toDateday);

                                for (DateTime h = startDate; h <= endDate; h = h.AddDays(1))
                                {
                                    allDates.Add(h);
                                }
                                int CountNumAct = ListActivity.Count;
                                List<Activity> lstIns = new List<Activity>();
                                for (int i = 0; i < CountNumAct; i++)
                                {
                                    item2 = new Activity();
                                    var x = ListActivity[i];

                                    if (x.DayOfWeek == 2)
                                    {
                                        item2.ActivityDate = allDates[0];
                                        item2.ActivityName = x.ActivityName;
                                        item2.ActivityPlanID = APID;
                                        item2.DayOfWeek = x.DayOfWeek.Value;
                                        item2.FromTime = ngayhientai.Add(x.FromTime.TimeOfDay);
                                        item2.ToTime = ngayhientai.Add(x.ToTime.TimeOfDay);
                                        lstIns.Add(item2);
                                    }
                                    if (x.DayOfWeek == 3)
                                    {
                                        item2.ActivityDate = allDates[1];
                                        item2.ActivityName = x.ActivityName;
                                        item2.ActivityPlanID = APID;
                                        item2.DayOfWeek = x.DayOfWeek.Value;
                                        item2.FromTime = ngayhientai.Add(x.FromTime.TimeOfDay);
                                        item2.ToTime = ngayhientai.Add(x.ToTime.TimeOfDay);
                                        lstIns.Add(item2);
                                    }
                                    if (x.DayOfWeek == 4)
                                    {
                                        item2.ActivityDate = allDates[2];
                                        item2.ActivityName = x.ActivityName;
                                        item2.ActivityPlanID = APID;
                                        item2.DayOfWeek = x.DayOfWeek.Value;
                                        item2.FromTime = ngayhientai.Add(x.FromTime.TimeOfDay);
                                        item2.ToTime = ngayhientai.Add(x.ToTime.TimeOfDay);
                                        lstIns.Add(item2);
                                    }
                                    if (x.DayOfWeek == 5)
                                    {
                                        item2.ActivityDate = allDates[3];
                                        item2.ActivityName = x.ActivityName;
                                        item2.ActivityPlanID = APID;
                                        item2.DayOfWeek = x.DayOfWeek.Value;
                                        item2.FromTime = ngayhientai.Add(x.FromTime.TimeOfDay);
                                        item2.ToTime = ngayhientai.Add(x.ToTime.TimeOfDay);
                                        lstIns.Add(item2);
                                    }
                                    if (x.DayOfWeek == 6)
                                    {
                                        item2.ActivityDate = allDates[4];
                                        item2.ActivityName = x.ActivityName;
                                        item2.ActivityPlanID = APID;
                                        item2.DayOfWeek = x.DayOfWeek.Value;
                                        item2.FromTime = ngayhientai.Add(x.FromTime.TimeOfDay);
                                        item2.ToTime = ngayhientai.Add(x.ToTime.TimeOfDay);
                                        lstIns.Add(item2);
                                    }
                                    if (x.DayOfWeek == 7)
                                    {
                                        item2.ActivityDate = allDates[5];
                                        item2.ActivityName = x.ActivityName;
                                        item2.ActivityPlanID = APID;
                                        item2.DayOfWeek = x.DayOfWeek.Value;
                                        item2.FromTime = ngayhientai.Add(x.FromTime.TimeOfDay);
                                        item2.ToTime = ngayhientai.Add(x.ToTime.TimeOfDay);
                                        lstIns.Add(item2);
                                    }
                                }

                                try
                                {
                                    ActivityBusiness.InsertCoppy(lstIns, SearchInfo);
                                    ActivityBusiness.Save();
                                }
                                catch (Exception)
                                {
                                    return Json(new { Type = "error", Message = "Có lỗi tiến hành thêm hoạt động" });
                                }                         
                        }
                        #endregion
                    }
                }
            }
            else
            {
                return Json(new { Type = "error", Message = "Thầy/cô chưa chọn tuần muốn sao chép!" });
            }
            return Json(new { Type = "success", Message = "Sao chép kế hoạch thành công" });
        }
        #endregion

        #region load combobox
        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID", EducationLevelID},
                    {"AcademicYearID", global.AcademicYearID.Value}
                };
            //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
            if (!global.IsAdminSchoolRole)
            {
                dic["UserAccountID"] = global.UserAccountID;
                dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).OrderBy(c => c.OrderNumber ?? 0).ThenBy(c => c.DisplayName);
            return lsCP;
        }
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }
        public void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            List<EducationLevel> lsEducationLevel = global.EducationLevels;
            if (lsEducationLevel != null)
            {
                ViewData[SCSActivityPlanConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[SCSActivityPlanConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
            }
            ViewData[SCSActivityPlanConstants.LISTCLASS] = new SelectList(new string[] { });
        }
        #endregion

        #region Điều hướng
        public ActionResult RedirectCreatePage()
        {
            // khởi tạo các giá trị mặc định
            GlobalInfo global = new GlobalInfo();
            int schoolId = global.SchoolID.Value;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["EmployeeID"] = global.EmployeeID;
            IQueryable<ClassProfile> lstClass;
            LoadMonth();
            if (global.IsAdminSchoolRole == false && global.IsRolePrincipal == false)
            {
                lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(global.EmployeeID.Value, global.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
            }
            // Lấy về danh sách khối - lớp
            List<EducationLevel> lstLevel = lstClass.Select(o => o.EducationLevel).Distinct().OrderBy(o => o.Resolution).ToList();
            List<int> lstClassInRole = lstClass.Select(o => o.ClassProfileID).ToList();
            List<EducationLevelItem> lstLevelItem = new List<EducationLevelItem>();
            EducationLevelItem level;
            foreach (var item in lstLevel)
            {
                level = new EducationLevelItem();
                level.EducationLevelID = item.EducationLevelID;
                level.Resolution = item.Resolution;
                List<ClassProfileItem> lstClsItem = new List<ClassProfileItem>();
                List<ClassProfile> iqTemp = lstClass.Where(o => o.EducationLevelID == item.EducationLevelID).OrderBy(c => c.OrderNumber ?? 0).ThenBy(c => c.DisplayName).ToList();
                foreach (var itm in iqTemp)
                {
                    ClassProfileItem tmp = new ClassProfileItem();
                    lstClsItem.Add(new ClassProfileItem()
                    {
                        ClasslID = itm.ClassProfileID,
                        ClassName = itm.DisplayName,
                        Checked = false
                    });
                }
                level.LstClass = lstClsItem;
                lstLevelItem.Add(level);
            }
            //end

            DataViewModel data = new DataViewModel();
            data.ListEducationLevel = lstLevelItem;
            data.lstClassInRole = lstClassInRole;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;

            //  kiem tra truong hop nhan ban
            DataViewModel temp = (DataViewModel)Session[SCSActivityPlanConstants.REPLICATION_INFORMATION];
            if (temp != null)
            {
                data = temp;
                data.ActivityPlanName = string.Empty;
                data.FromDate = null;
                data.ToDate = null;
                data.ListEducationLevel = lstLevelItem;
                // kiem tra lop ap dung
                List<int> ClassApplied = temp.lstClassApplied;
                for (int i = 0; i < data.ListEducationLevel.Count; i++)
                {
                    foreach (var itm in data.ListEducationLevel[i].LstClass)
                    {
                        if (ClassApplied.Contains(itm.ClasslID))
                        {
                            itm.Checked = true;
                        }
                    }
                    // dem so item duoc check
                    int CountItemChecked = data.ListEducationLevel[i].LstClass.Where(o => o.Checked == true).Count();
                    // checked item neu toan bo lop trong khoi duoc check
                    if (CountItemChecked == data.ListEducationLevel[i].LstClass.Count)
                    {
                        data.ListEducationLevel[i].Checked = true;
                    }
                }
            }
            //end

            // danh sach ActivityType
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            List<ActivityType> lstType = ActivityTypeBusiness.Search(dic).ToList();
            ViewData[SCSActivityPlanConstants.LIST_ACTIVITY_TYPE] = lstType;
            // Model cho combo ActivityType
            List<SelectListItem> lst = new List<SelectListItem>();
            SelectListItem cbitem;
            foreach (var cls in lstType)
            {
                cbitem = new SelectListItem();
                cbitem.Text = cls.ActivityTypeName;
                //cbitem.Value = cls.ActivityTypeID.ToString();

                lst.Add(cbitem);
            }
            ViewData[SCSActivityPlanConstants.LIST_ACTIVITY_TYPE_ITEM] = lst;
            Session.Remove(SCSActivityPlanConstants.REPLICATION_INFORMATION);
            return PartialView("_CreateOrEdit", data);
        }
        #endregion

        #region Thêm mới và Cập nhật
        public PartialViewResult Create()
        {
            DataViewModel data = new DataViewModel();
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AcademicYearID"] = global.AcademicYearID;
            ClassSearchInfo["EmployeeID"] = global.EmployeeID;
            IQueryable<ClassProfile> lstClass = null;
            if (!global.IsAdminSchoolRole && !global.IsRolePrincipal)
            {
                lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(global.EmployeeID.Value, global.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, ClassSearchInfo).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
            }
            // Lấy về danh sách khôi
            List<EducationLevel> lstLevel = lstClass.Select(o => o.EducationLevel).Distinct().ToList();
            List<EducationLevelItem> lstLevelItem = new List<EducationLevelItem>();
            foreach (var item in lstLevel)
            {
                EducationLevelItem obj = new EducationLevelItem();
                obj.EducationLevelID = item.EducationLevelID;
                obj.Resolution = item.Resolution;
                List<ClassProfileItem> lstClsItem = new List<ClassProfileItem>();
                List<ClassProfile> iqTemp = lstClass.Where(o => o.EducationLevelID == item.EducationLevelID).OrderBy(c => c.OrderNumber ?? 0).ThenBy(c => c.DisplayName).ToList();
                foreach (var itm in iqTemp)
                {
                    ClassProfileItem tmp = new ClassProfileItem();
                    lstClsItem.Add(new ClassProfileItem()
                    {
                        ClasslID = itm.ClassProfileID,
                        ClassName = itm.DisplayName,
                    });
                }
                obj.LstClass = lstClsItem;
                lstLevelItem.Add(obj);
            }
            data.lstClassInRole = lstClass.Select(o => o.ClassProfileID).ToList();
            data.ListEducationLevel = lstLevelItem;
            return PartialView("_CreateOrEdit");
        }
        /// <summary>
        /// kiem tra 
        /// </summary>
        /// <param name="frm"></param>
        /// <param name="CheckName">kiem tra ten ke hoach</param>
        /// <param name="CheckDateVSNow">kiem tra thoi gian ke hoach so voi ngay hien tai</param>
        /// <param name="CheckDate">kiem tra thoi gian cua ke hoach</param>
        /// <param name="checkTime"> kiem tra thoi gian cac hoat dong</param>
        /// <returns> 
        /// 0: OK
        /// 1: ten ke hoach da co
        /// 2: thoi gian da co trong ke hoach khac
        /// 3: thoi gian cac hoat dong chong cheo
        /// </returns>
        public MessageInfomation checkValidate(DataViewModel frm, bool CheckName, bool CheckDateVSNow, bool CheckDate, bool checkTime, bool isUpdate)
        {
            string planName = frm.ActivityPlanName.Trim();
            DateTime fromDate = frm.FromDate.Value.Date;
            DateTime toDate = frm.ToDate.Value.Date;
            GlobalInfo global = new GlobalInfo();
            int schoolId = global.SchoolID.Value;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = schoolId;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["EmployeeID"] = global.EmployeeID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            //HaiVT - chi validate truong hop du lieu co trang thai isActive = true
            List<ActivityPlanBO> LstActPlan = ActivityPlanBusiness.SearchBySchool(schoolId, SearchInfo).Distinct().Where(p => p.IsActive == true).ToList();
            List<String> LstPlanName = LstActPlan.Select(o => o.ActivityPlanName).ToList();
            List<DateTime> LstPlanFromDate = LstActPlan.Select(o => o.FromDate).ToList();
            List<DateTime> LstPlanToDate = LstActPlan.Select(o => o.ToDate).ToList();
            //if (CheckName && LstPlanName.Contains(planName))
            //{
            //    return new MessageInfomation(0, "Kế hoạch đã tồn tại trong hệ thống");
            //}
            //DucPT - Tach & check ke hoach theo tung lop.     
            if (isUpdate)
            {
                List<ActivityPlanBO> LstActPlanInRangeDate = LstActPlan.Where(p => p.FromDate >= frm.FromDate && p.ToDate <= frm.ToDate && p.ActivityPlanID != frm.ActivityPlanID).ToList();
                List<string> ListClassUnavailable = new List<string>();
                bool IsAvailabaleClass = true;
                if (frm.lstClassApplied != null && frm.lstClassApplied.Count > 0)
                {
                    int SizeClass = frm.lstClassApplied.Count;
                    for (int i = 0; i < SizeClass; i++)
                    {
                        if (LstActPlanInRangeDate.Where(p => p.ClassID == frm.lstClassApplied[i]).ToList().Count > 0)
                        {
                            IsAvailabaleClass = false;
                            ListClassUnavailable.Add(ClassProfileBusiness.Find(frm.lstClassApplied[i]).DisplayName);
                        }
                    }
                    if (!IsAvailabaleClass)
                    {
                        return new MessageInfomation(0, String.Format("Lớp {0} đã có kế hoạch hoạt động trong khoảng thời gian" + frm.FromDate.Value.ToString("dd/MM/yyyy") + " - " + frm.ToDate.Value.ToString("dd/MM/yyyy"), String.Join(", ", ListClassUnavailable.ToArray())));
                    }
                }
            }
            else
            {
                List<ActivityPlanBO> LstActPlanInRangeDate = LstActPlan.Where(p => p.FromDate >= frm.FromDate && p.ToDate <= frm.ToDate).ToList();
                //List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(schoolId, SearchInfo).Distinct().ToList();
                List<string> ListClassUnavailable = new List<string>();
                bool IsAvailabaleClass = true;
                if (frm.lstClassApplied != null && frm.lstClassApplied.Count > 0)
                {
                    int SizeClass = frm.lstClassApplied.Count;
                    for (int i = 0; i < SizeClass; i++)
                    {
                        if (LstActPlanInRangeDate.Where(p => p.ClassID == frm.lstClassApplied[i]).ToList().Count > 0)
                        {
                            IsAvailabaleClass = false;
                            ListClassUnavailable.Add(ClassProfileBusiness.Find(frm.lstClassApplied[i]).DisplayName);
                        }
                    }
                    if (!IsAvailabaleClass)
                    {
                        return new MessageInfomation(0, String.Format("Lớp {0} đã có kế hoạch hoạt động trong khoảng thời gian " + frm.FromDate.Value.ToString("dd/MM/yyyy") + " - " + frm.ToDate.Value.ToString("dd/MM/yyyy"), String.Join(", ", ListClassUnavailable.ToArray())));
                    }
                }
            }
            // kiem tra thoi gian cac hoat dong chong cheo
            if (checkTime)
            {
                List<byte?> lstDayOfWeek = frm.ListActivity.Select(o => o.DayOFWeek).Distinct().ToList();
                if (lstDayOfWeek == null || lstDayOfWeek.Count == 0) return new MessageInfomation(4, "Ok");
                for (int c = 0; c < lstDayOfWeek.Count; c++)
                {
                    List<ActivityViewModel> lstActivityEachDay = frm.ListActivity.Where(o => lstDayOfWeek[c].HasValue && o.DayOFWeek == lstDayOfWeek[c].Value).ToList();
                    int CountAct = lstActivityEachDay.Count;
                    if (CountAct == 0) return new MessageInfomation(4, "Ok");
                    List<TimeActivity> LstTimeAct = new List<TimeActivity>();
                    for (int i = 0; i < CountAct; i++)
                    {
                        TimeSpan? fTime = lstActivityEachDay[i].FromTime;
                        TimeSpan? tTime = lstActivityEachDay[i].ToTime;
                        if (fTime != null && tTime != null)
                        {
                            TimeActivity obj = new TimeActivity(fTime.Value, tTime.Value);
                            LstTimeAct.Add(obj);
                        }
                    }
                    // LstTimeAct = LstTimeAct.OrderBy(o => o.FromTime.Hours).OrderBy(o => o.FromTime.Minutes).ToList(); // danh sach tang dan theo FromTime
                    LstTimeAct = LstTimeAct.OrderBy(o => o.FromTime.Hours).ThenBy(o => o.FromTime.Minutes).ToList(); // danh sach tang dan theo FromTime (anh Tho fix bug 0016133)
                    int CountTime = LstTimeAct.Count;
                    int k = 0;
                    for (k = 0; k < CountTime - 1; k++)
                    {
                        TimeActivity current = LstTimeAct[k];
                        TimeActivity next = LstTimeAct[k + 1];
                        if (next.FromTime >= current.ToTime) continue;
                        else break;
                    }
                    if (k != CountTime - 1)
                    {

                        return new MessageInfomation(0, "Thời gian các hoạt động trong ngày không được giao nhau");
                    }
                }
            }
            return new MessageInfomation(4, "Ok");
        }
        /// <summary>
        /// sua va them moi
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [ValidateInput(true)]
        public JsonResult InsertORUpdate(DataViewModel frm)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (frm == null)
            {
                return Json(new { Type = "error", Message = "Có lỗi" });
            }
            if (frm.FromDate == null || frm.ToDate == null)
            {
                return Json(new { Type = "error", Message = "vui lòng chọn tuần!" });
            }
            int? ActivityPlanID = frm.ActivityPlanID;
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            #region Thêm mới
            if (ActivityPlanID == null || !ActivityPlanID.HasValue) // truong hop them moi
            {
                // validate thong tin
                MessageInfomation result = checkValidate(frm, true, false, true, true, false);
                if (result.Type == 0)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                else if (result.Type == 1)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                else if (result.Type == 2)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                else if (result.Type == 3)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                // them moi ActivityPlan
                string planName = frm.ActivityPlanName.Trim();
                ActivityPlan obj = new ActivityPlan();
                obj.AcademicYearID = globalInfo.AcademicYearID.Value;
                obj.SchoolID = globalInfo.SchoolID.Value;
                obj.ActivityPlanName = planName;
                obj.PlannedDate = currentDate;
                obj.FromDate = frm.FromDate.Value;
                obj.ToDate = frm.ToDate.Value;
                obj.CreatedDate = DateTime.Now;
                obj.IsActive = true;
                try
                {
                    ActivityPlanBusiness.Insert(obj);
                    ActivityPlanBusiness.Save();
                }
                catch (Exception ex)
                {
                    LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                    LogExtensions.ErrorExt(logger, DateTime.Now, "DataViewModel", ex.Message, "");
                    return Json(new { Type = "error", Message = "Có lỗi thêm mới kế hoạch" });
                }
                // them moi ActivityPlanClass
                int APID = obj.ActivityPlanID;
                List<int> ClassApplied = frm.lstClassApplied;
                List<ActivityPlanClass> lstPlanClassIns = new List<ActivityPlanClass>();
                for (int i = 0, CountClass = ClassApplied.Count; i < CountClass; i++)
                {
                    if (ClassApplied[i] > 0)
                    {
                        lstPlanClassIns.Add(new ActivityPlanClass()
                        {
                            ActivityPlanID = APID,
                            ClassID = ClassApplied[i]
                        });
                    }
                }
                try
                {
                    ActivityPlanClassBusiness.Insert(lstPlanClassIns);
                    ActivityPlanClassBusiness.Save();
                }
                catch (Exception)
                {
                    return Json(new { Type = "error", Message = "Có lỗi tiến hành áp dụng lớp" });
                }

                // them moi vao Activity HOAC ActivityOfClass
                //if (globalInfo.IsAdminSchoolRole || globalInfo.IsRolePrincipal) // neu bgh - quan tri truong
                {
                    List<ActivityViewModel> lstActivityClient = frm.ListActivity;
                    ActivityViewModel item;
                    int CountNumAct = lstActivityClient.Count;
                    List<Activity> lstIns = new List<Activity>();
                    for (int i = 0; i < CountNumAct; i++)
                    {
                        item = lstActivityClient[i];
                        lstIns.Add(new Activity()
                        {
                            ActivityDate = item.ActivityDate.Value,
                            ActivityName = item.ActivityName,
                            ActivityPlanID = APID,
                            //ActivityTypeID = item.ActivityTypeID.Value,
                            DayOfWeek = item.DayOFWeek.Value,
                            FromTime = currentDate.Add(item.FromTime.Value),
                            ToTime = currentDate.Add(item.ToTime.Value)
                        });
                    }
                    try
                    {
                        ActivityBusiness.Insert(lstIns);
                        ActivityBusiness.Save();
                    }
                    catch (Exception)
                    {
                        return Json(new { Type = "error", Message = "Có lỗi tiến hành thêm hoạt động" });
                    }
                }

                #region // dong
                /*else
                {
                    List<ActivityViewModel> lstActivityClient = frm.ListActivity;
                    ActivityViewModel item;
                    int CountNumAct = lstActivityClient.Count;
                    List<ActivityOfClass> lstIns = new List<ActivityOfClass>();
                    for (int i = 0; i < CountNumAct; i++)
                    {
                        item = lstActivityClient[i];
                        lstIns.Add(new ActivityOfClass()
                        {

                            ActivityDate = item.ActivityDate.Value,
                            ActivityOfClassName = item.ActivityName,
                            ActivityPlanID = APID,
                            //ActivityTypeID = item.ActivityTypeID.Value,
                            DayOfWeek = item.DayOFWeek.Value,
                            FromTime = ngayhientai.Add(item.FromTime.Value),
                            ToTime = ngayhientai.Add(item.ToTime.Value),
                        });
                    }
                    try
                    {
                        ActivityOfClassBusiness.Insert(lstIns);
                        ActivityOfClassBusiness.Save();
                    }
                    catch (Exception)
                    {
                        return Json(new { Type = "error", Message = "Có lỗi tiến hành thêm hoạt động" });
                    }
                }*/
                #endregion

                return Json(new { Type = "success", Message = "Lưu thành công kế hoạch hoạt động", Id = APID, TypeID = 1 });
            }
            #endregion
            else // truong hop cap nhat
            #region Cap nhat
            {
                // validate thong tin
                CheckPermissionForAction(frm.ActivityPlanID, "ActivityPlan");
                MessageInfomation result = checkValidate(frm, false, false, false, true, true);
                if (result.Type == 2 || result.Type == 3 || result.Type == 0)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                ActivityPlan activityPlan = ActivityPlanBusiness.Find(ActivityPlanID.Value);
                // neu quan tri truong duoc quyen thay doi ten plan

                string planName = frm.ActivityPlanName.Trim();
                if (!String.IsNullOrEmpty(planName))
                {
                    activityPlan.ActivityPlanName = planName;
                    activityPlan.FromDate = frm.FromDate.Value;
                    activityPlan.ToDate = frm.ToDate.Value;
                    activityPlan.ModifiedDate = DateTime.Now;
                    activityPlan.IsActive = true;
                    try
                    {
                        ActivityPlanBusiness.Update(activityPlan);
                        ActivityPlanBusiness.Save();
                    }
                    catch (Exception)
                    {
                        return Json(new { Type = "error", Message = "Có lỗi cập nhật kế hoạch" });
                    }
                }

                // ap dung tren cac lop 
                List<int> ClassApplied = frm.lstClassApplied;
                List<int> ClassInRole = frm.lstClassInRole;
                List<int> ClassNotChecked = ClassInRole.Except(ClassApplied).ToList();
                List<int> lstClassIDInPlan = ActivityPlanClassBusiness.GetListClass(ActivityPlanID.Value).Select(o => o.ClassProfileID).ToList();

                List<int> lstClassINS = ClassApplied.Except(lstClassIDInPlan).ToList();
                List<int> lstClassDel = lstClassIDInPlan.Intersect(ClassNotChecked).ToList();
                List<ActivityPlanClass> lstPlanClassIns = new List<ActivityPlanClass>();
                for (int i = 0; i < lstClassINS.Count; i++)
                {
                    lstPlanClassIns.Add(new ActivityPlanClass()
                    {
                        ActivityPlanID = ActivityPlanID.Value,
                        ClassID = lstClassINS[i]
                    });
                }
                // viet ham them va xoa Plan - Class trong ActivityPlanClassBusiness
                try
                {
                    ActivityPlanClassBusiness.Insert(lstPlanClassIns);
                    ActivityPlanClassBusiness.Delete(ActivityPlanID.Value, lstClassDel);
                    ActivityPlanClassBusiness.Save();
                }
                catch (Exception)
                {
                    return Json(new { Type = "error", Message = "Có lỗi cập nhật lớp áp dụng" });
                }
                // cap nhat - them - xoa hoat dong
                List<ActivityViewModel> lstActivityClient = frm.ListActivity; // danh sach gui len
                // neu bgh - quan tri truong --> Activity
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ActivityPlanID"] = ActivityPlanID.Value;
                List<Activity> lstActivity = ActivityBusiness.Search(dic).ToList(); // danh sach hoat dong
                //List<ActivityOfClass> lstActivityOfClass = ActivityOfClassBusiness.Search(dic).ToList(); // danh sach hoat dong

                //if (lstActivityOfClass.Count == 0)
                {
                    List<int> lstActivityID = lstActivity.Select(o => o.ActivityID).ToList();
                    List<int> lstActivityIDEdit = new List<int>();
                    ActivityViewModel item;
                    int CountNumAct = lstActivityClient.Count;
                    List<Activity> lstIns = new List<Activity>();
                    for (int i = 0; i < CountNumAct; i++)
                    {
                        item = lstActivityClient[i];
                        if (!item.ActivityID.HasValue || item.ActivityID.Value <= 0) // neu chua co id thi insert
                        {
                            lstIns.Add(new Activity()
                            {
                                ActivityDate = item.ActivityDate,
                                ActivityName = item.ActivityName,
                                ActivityPlanID = ActivityPlanID.Value,
                                //ActivityTypeID = item.ActivityTypeID.Value,
                                DayOfWeek = item.DayOFWeek.Value,
                                FromTime = currentDate.Add(item.FromTime.Value),
                                ToTime = currentDate.Add(item.ToTime.Value)
                            });
                        }
                        else // neu da co id thi update
                        {
                            if (lstActivityID.Contains(item.ActivityID.Value)) // neu da co thi cap nhat
                            {
                                Activity obj = lstActivity.Where(o => o.ActivityID == item.ActivityID.Value).FirstOrDefault();
                                obj.FromTime = currentDate.Add(item.FromTime.Value);
                                obj.ActivityDate = item.ActivityDate;
                                obj.ToTime = currentDate.Add(item.ToTime.Value);
                                //obj.ActivityTypeID = item.ActivityTypeID.Value;
                                obj.ActivityName = item.ActivityName;
                                lstActivityIDEdit.Add(item.ActivityID.Value);
                                ActivityBusiness.Update(obj); // update
                            }
                        }
                    }
                    try
                    {
                        ActivityBusiness.Insert(lstIns); // insert
                        //xoa hoat dong cu                    
                        // danh sach xoa
                        List<int> lstDel = lstActivityID.Except(lstActivityIDEdit).ToList();
                        //viet ham xoa
                        ActivityBusiness.Delete(lstDel);
                        ActivityBusiness.Save();
                    }
                    catch (Exception)
                    {
                        return Json(new { Type = "error", Message = "Có lỗi cập nhật hoạt động" });
                    }
                }
                #region // Dong
                /*else
                {
                    List<int> lstActivityID = lstActivityOfClass.Select(o => o.ActivityOfClassID).ToList();
                    List<int> lstActivityIDEdit = new List<int>();
                    ActivityViewModel item;
                    int CountNumAct = lstActivityClient.Count;
                    List<ActivityOfClass> lstIns = new List<ActivityOfClass>();
                    for (int i = 0; i < CountNumAct; i++)
                    {
                        item = lstActivityClient[i];
                        if (!item.ActivityID.HasValue || item.ActivityID.Value <= 0) // neu chua co id thi insert
                        {
                            lstIns.Add(new ActivityOfClass()
                            {
                                ActivityDate = item.ActivityDate.Value,
                                ActivityOfClassName = item.ActivityName,
                                ActivityPlanID = ActivityPlanID.Value,
                                //ActivityTypeID = item.ActivityTypeID.Value,
                                DayOfWeek = item.DayOFWeek.Value,
                                FromTime = ngayhientai.Add(item.FromTime.Value),
                                ToTime = ngayhientai.Add(item.ToTime.Value)
                            });
                        }
                        else // neu da co id thi update
                        {
                            if (lstActivityID.Contains(item.ActivityID.Value)) // neu da co thi cap nhat
                            {
                                ActivityOfClass obj = lstActivityOfClass.Where(o => o.ActivityOfClassID == item.ActivityID.Value).FirstOrDefault();
                                obj.FromTime = ngayhientai.Add(item.FromTime.Value);
                                obj.ActivityDate = item.ActivityDate.Value;
                                obj.ToTime = ngayhientai.Add(item.ToTime.Value);
                                //obj.ActivityTypeID = item.ActivityTypeID.Value;
                                obj.ActivityOfClassName = item.ActivityName;
                                lstActivityIDEdit.Add(item.ActivityID.Value);
                                ActivityOfClassBusiness.Update(obj); // update
                            }
                        }
                    }
                    try
                    {
                        ActivityOfClassBusiness.Insert(lstIns); // insert
                        //xoa hoat dong cu                    
                        // danh sach xoa
                        List<int> lstDel = lstActivityID.Except(lstActivityIDEdit).ToList();
                        ActivityOfClassBusiness.Delete(lstDel);
                        ActivityOfClassBusiness.Save();
                    }
                    catch (Exception)
                    {
                        return Json(new { Type = "error", Message = "Có lỗi cập nhật hoạt động" });
                    }
                }*/
                #endregion

                return Json(new { Type = "success", Message = "Cập nhật thành công", Id = ActivityPlanID, TypeID = 2 });
            }
            #endregion
        }
        #endregion

        #region Hiển thị thông tin
        public PartialViewResult Edit(int ActivityPlanID)
        {
            ViewData[SCSActivityPlanConstants.LST_MONTH] = new SelectList(new string[] { });
            LoadMonth();
            GlobalInfo global = new GlobalInfo();
            int schoolId = global.SchoolID.Value;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["EmployeeID"] = global.EmployeeID;
            IQueryable<ClassProfile> lstClass = null;
            if (global.IsAdminSchoolRole == false && global.IsRolePrincipal == false)
            {
                lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(global.EmployeeID.Value, global.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
            }
            DateTime ngayhientai = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            // Lấy về danh sách khối - lớp
            List<int> lstClassIDInPlan = ActivityPlanClassBusiness.GetListClass(ActivityPlanID).Select(o => o.ClassProfileID).ToList();
            List<EducationLevel> lstLevel = lstClass.Select(o => o.EducationLevel).Distinct().OrderBy(o => o.Resolution).ToList();
            List<EducationLevelItem> lstLevelItem = new List<EducationLevelItem>();
            EducationLevelItem educationLevelItem;
            foreach (var item in lstLevel)
            {
                educationLevelItem = new EducationLevelItem();
                educationLevelItem.EducationLevelID = item.EducationLevelID;
                educationLevelItem.Resolution = item.Resolution;
                List<ClassProfileItem> lstClsItem = new List<ClassProfileItem>();
                List<ClassProfile> iqTemp = lstClass.Where(o => o.EducationLevelID == item.EducationLevelID).OrderBy(c => c.OrderNumber ?? 0).ThenBy(c => c.DisplayName).ToList();
                foreach (var itm in iqTemp)
                {
                    lstClsItem.Add(new ClassProfileItem()
                    {
                        ClasslID = itm.ClassProfileID,
                        ClassName = itm.DisplayName,
                        Checked = lstClassIDInPlan.Contains(itm.ClassProfileID)
                    });
                }
                // dem so item duoc check
                int CountItemChecked = lstClsItem.Where(o => o.Checked == true).Count();
                // checked item neu toan bo lop trong khoi duoc check
                if (CountItemChecked == iqTemp.Count)
                {
                    educationLevelItem.Checked = true;
                }
                educationLevelItem.LstClass = lstClsItem;
                lstLevelItem.Add(educationLevelItem);
            }
            //end
            // model for Edit
            DataViewModel data = new DataViewModel();
            data.ListEducationLevel = lstLevelItem;
            data.lstClassInRole = lstClass.Select(o => o.ClassProfileID).ToList();
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ActivityPlanID"] = ActivityPlanID;
            ActivityPlan activityPlan = this.ActivityPlanBusiness.Find(ActivityPlanID);
            data.ActivityPlanID = ActivityPlanID;
            data.ActivityPlanName = activityPlan.ActivityPlanName;
            data.FromDate = activityPlan.FromDate;
            data.ToDate = activityPlan.ToDate;
            List<ActivityViewModel> list = new List<ActivityViewModel>();
            // Lấy kế hoạch hoạt động liên quan trong ActivityOfClass
            List<ActivityViewModel> lstModel = new List<ActivityViewModel>();
            ActivityViewModel objModel = null;

            /*List<ActivityOfClass> ListActivityOfClass = this.ActivityOfClassBusiness.Search(SearchInfo).ToList();
            ActivityOfClass objAOC = null;
            for (int i = 0; i < ListActivityOfClass.Count; i++)
            {
                objAOC = ListActivityOfClass[i];
                objModel = new ActivityViewModel();
                ActivityPlanID = objAOC.ActivityPlanID;
                objModel.ActivityID = objAOC.ActivityOfClassID;
                objModel.ActivityName = objAOC.ActivityOfClassName;
                //ActivityTypeID = activityOfClass.ActivityTypeID,
                objModel.ActivityDate = objAOC.ActivityDate;
                objModel.DayOFWeek = (byte)objAOC.DayOfWeek;
                objModel.FromTime = objAOC.FromTime.TimeOfDay;
                objModel.ToTime = objAOC.ToTime.TimeOfDay;
                lstModel.Add(objModel);
            }*/

            List<Activity> ListActivity = this.ActivityBusiness.Search(SearchInfo).ToList();
            Activity objAT = null;
            for (int i = 0; i < ListActivity.Count; i++)
            {
                objAT = ListActivity[i];
                objModel = new ActivityViewModel();
                ActivityPlanID = objAT.ActivityPlanID;
                objModel.ActivityID = objAT.ActivityID;
                objModel.ActivityName = objAT.ActivityName;
                objModel.ActivityDate = objAT.ActivityDate;
                objModel.DayOFWeek = (byte)objAT.DayOfWeek;
                objModel.FromTime = objAT.FromTime.TimeOfDay;
                objModel.ToTime = objAT.ToTime.TimeOfDay;
                lstModel.Add(objModel);
            }
            data.ListActivity = lstModel;
            // danh sach ActivityType
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            List<ActivityType> lstType = ActivityTypeBusiness.Search(dic).ToList();
            ViewData[SCSActivityPlanConstants.LIST_ACTIVITY_TYPE] = lstType;
            // Model cho combo ActivityType
            List<SelectListItem> lst = new List<SelectListItem>();
            SelectListItem cbitem;
            foreach (var cls in lstType)
            {
                cbitem = new SelectListItem();
                cbitem.Text = cls.ActivityTypeName;
                //cbitem.Value = cls.ActivityTypeID.ToString();

                lst.Add(cbitem);
            }
            ViewData[SCSActivityPlanConstants.LIST_ACTIVITY_TYPE_ITEM] = lst;
            ViewData[SCSActivityPlanConstants.IS_ENBLE_Replication] = true;
            return PartialView("_CreateOrEdit", data);
        }
        #endregion

        #region Xóa
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int ActivityPlanID)
        {
            CheckPermissionForAction(ActivityPlanID, "ActivityPlan");
            GlobalInfo globalInfo = new GlobalInfo();

            var checkActivity = ActivityBusiness.All.Where(t => t.ActivityPlanID == ActivityPlanID).Select(t => t.ActivityID).FirstOrDefault();
            var activityOfpuid = ActivityOfPupilBusiness.All.Where(t => t.ActivityOfClassID == checkActivity).Select(t => t.ActivityOfPupilID).FirstOrDefault();
            //var activi
            if (activityOfpuid != 0)
            {
                return Json(new { Type = "error", Message = "Kế hoạch hoạt động đã được áp dụng." });
            }
            // delete
            ActivityPlanBusiness.Delete(globalInfo.UserAccountID, ActivityPlanID, globalInfo.SchoolID.Value);
            ActivityPlanBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region Nhân bản
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Replicate(DataViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            DataViewModel data = new DataViewModel();
            data = frm;
            // danh sach ActivityType
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            List<ActivityType> lstType = ActivityTypeBusiness.Search(dic).ToList();
            ViewData[SCSActivityPlanConstants.LIST_ACTIVITY_TYPE] = lstType;
            // Model cho combo ActivityType
            List<SelectListItem> lst = new List<SelectListItem>();
            SelectListItem cbitem;
            foreach (var cls in lstType)
            {
                cbitem = new SelectListItem();
                cbitem.Text = cls.ActivityTypeName;
                //cbitem.Value = cls.ActivityTypeID.ToString();

                lst.Add(cbitem);
            }
            ViewData[SCSActivityPlanConstants.LIST_ACTIVITY_TYPE_ITEM] = lst;
            ViewData[SCSActivityPlanConstants.IS_ENBLE_Replication] = true;

            Session[SCSActivityPlanConstants.REPLICATION_INFORMATION] = data;
            return Json(new { Type = "success", IsReplication = true });
        }
        #endregion

        #region Export
        /// <summary>
        /// Xuat danh sach excel
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public FileResult ExportExcel(SearchViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            string templatePath = string.Empty;
            string fileName = string.Empty;
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", "KeHoachHoatDong.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetCurrent = oBook.GetSheet(1);
            IVTWorksheet sheet = null;
            #region dien du lieu cho sheet
            Utils.Utils.TrimObject(frm);
            Session[SCSActivityPlanConstants.SEARCHFORM] = frm;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            if (frm.FromDate > frm.ToDate)
            {
                throw new BusinessException("SCSActivityPlan_Label_FromDateToDate");
            }
            //add search infor
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["SchoolID"] = global.SchoolID.Value;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["ActivityPlanName"] = frm.ActivityPlanName;
            SearchInfo["FromDate"] = frm.FromDate;
            SearchInfo["ToDate"] = frm.ToDate;
            SearchInfo["ToMonthYear"] = frm.ToMonthYear;
            SearchInfo["IsActive"] = true;
            SearchInfo["EmployeeID"] = global.EmployeeID;
            List<ActivityPlanViewModel> lst = this._Search(SearchInfo);
            List<int> lstActivityPlanID = lst.Select(p => p.ActivityPlanID).Distinct().ToList();
            List<ActivityBO> ListActivity = new List<ActivityBO>();
            List<ActivityBO> lstActivitytmp = new List<ActivityBO>();
            ActivityBO objActivity = null;
            List<ActivityViewModel> lstActivityViewModel = new List<ActivityViewModel>();
            ActivityViewModel objViewModel = null;

            //lay danh sach hoat dong
            IDictionary<string, object> dicSearchActivity = new Dictionary<string, object>();
            dicSearchActivity["AcademicYearID"] = global.AcademicYearID;
            dicSearchActivity["EmployeeID"] = global.EmployeeID;
            dicSearchActivity["AppliedLevel"] = global.AppliedLevel;
           
            /*List<ActivityOfClass> ListActivityOfClass = this.ActivityOfClassBusiness.Search(dicSearchActivity).Where(p => lstActivityPlanID.Contains(p.ActivityPlanID)).ToList();
            ActivityOfClass objAOC = null;
            for (int i = 0; i < ListActivityOfClass.Count; i++)
            {
                objAOC = ListActivityOfClass[i];
                objActivity = new ActivityBO();
                objActivity.ActivityPlanID = objAOC.ActivityPlanID;
                objActivity.DayOfWeek = objAOC.DayOfWeek;
                objActivity.FromTime = objAOC.FromTime;
                objActivity.ToTime = objAOC.ToTime;
                objActivity.ActivityName = objAOC.ActivityOfClassName;
                ListActivity.Add(objActivity);
            }*/

            List<Activity> lstActivity = this.ActivityBusiness.Search(SearchInfo).Where(p => lstActivityPlanID.Contains(p.ActivityPlanID)).ToList();
            Activity objAT = null;
            for (int i = 0; i < lstActivity.Count; i++)
            {
                objAT = lstActivity[i];
                objActivity = new ActivityBO();
                objActivity.ActivityPlanID = objAT.ActivityPlanID;
                objActivity.DayOfWeek = objAT.DayOfWeek;
                objActivity.FromTime = objAT.FromTime;
                objActivity.ToTime = objAT.ToTime;
                objActivity.ActivityName = objAT.ActivityName;
                ListActivity.Add(objActivity);
            }

            for (int i = 0; i < lst.Count; i++)
            {
                var ActivityPlanID = lst[i].ActivityPlanID;
                IQueryable<ClassProfile> lstClass = null;
                if (global.IsAdminSchoolRole == false && global.IsRolePrincipal == false)
                {
                    lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(global.EmployeeID.Value, global.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
                }
                else
                {
                    lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
                }
                DateTime ngayhientai = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                // Lấy về danh sách khối - lớp
                List<int> lstClassIDInPlan = ActivityPlanClassBusiness.GetListClass(ActivityPlanID).Select(o => o.ClassProfileID).ToList();
                List<EducationLevel> lstLevel = lstClass.Select(o => o.EducationLevel).Distinct().OrderBy(o => o.Resolution).ToList();
                List<EducationLevelItem> lstLevelItem = new List<EducationLevelItem>();
                EducationLevelItem educationLevelItem;
                foreach (var item in lstLevel)
                {
                    educationLevelItem = new EducationLevelItem();
                    educationLevelItem.EducationLevelID = item.EducationLevelID;
                    educationLevelItem.Resolution = item.Resolution;
                    List<ClassProfileItem> lstClsItem = new List<ClassProfileItem>();
                    List<ClassProfile> iqTemp = lstClass.Where(o => o.EducationLevelID == item.EducationLevelID).OrderBy(c => c.OrderNumber ?? 0).ThenBy(c => c.DisplayName).ToList();
                    foreach (var itm in iqTemp)
                    {
                        lstClsItem.Add(new ClassProfileItem()
                        {
                            ClasslID = itm.ClassProfileID,
                            ClassName = itm.DisplayName,
                            Checked = lstClassIDInPlan.Contains(itm.ClassProfileID)
                        });
                    }
                    // dem so item duoc check
                    int CountItemChecked = lstClsItem.Where(o => o.Checked == true).Count();
                    // checked item neu toan bo lop trong khoi duoc check
                    if (CountItemChecked == iqTemp.Count)
                    {
                        educationLevelItem.Checked = true;
                    }
                    educationLevelItem.LstClass = lstClsItem;
                    lstLevelItem.Add(educationLevelItem);
                }
                //end
                // model for Edit
                DataViewModel data = new DataViewModel();
                data.ListEducationLevel = lstLevelItem;
                data.lstClassInRole = lstClass.Select(o => o.ClassProfileID).ToList();

                SearchInfo["ActivityPlanID"] = ActivityPlanID;
                ActivityPlan activityPlan = this.ActivityPlanBusiness.Find(ActivityPlanID);
                data.ActivityPlanID = ActivityPlanID;
                data.ActivityPlanName = activityPlan.ActivityPlanName;
                data.FromDate = activityPlan.FromDate;
                data.ToDate = activityPlan.ToDate;
                List<ActivityViewModel> list = new List<ActivityViewModel>();
                var FromDATE_ToDATE = new List<string>(lst[i].AppliedTime.Trim().Split(new string[] { "-" },
                  StringSplitOptions.RemoveEmptyEntries));
                var StartDate = DateTime.Parse(FromDATE_ToDATE[0]);
                var EndDate = DateTime.Parse(FromDATE_ToDATE[1]);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = StartDate; date <= EndDate; date = date.AddDays(1))
                {
                    allDates.Add(date);
                }
                sheet = oBook.CopySheetToLast(sheetCurrent);
                sheet.Name = "KH" + i;
                sheet.SetCellValue(2, 1, "PHÒNG GD&ĐT");
                sheet.SetCellValue(3, 1, _globalInfo.SchoolName);
                sheet.SetCellValue(6, 1, lst[i].ActivityNamePlan);
                sheet.SetCellValue(7, 1, "Thời gian thực hiện: từ " + lst[i].AppliedTime);
                sheet.SetCellValue(8, 1, "Danh sách lớp áp dụng: " + lst[i].AppliedClass);
                lstActivitytmp = ListActivity.Where(t => t.ActivityPlanID == ActivityPlanID).ToList();
                lstActivityViewModel = new List<ActivityViewModel>();
                for (int n = 0; n < lstActivitytmp.Count; n++)
                {
                    objViewModel = new ActivityViewModel();
                    objActivity = lstActivitytmp[n];
                    objViewModel.DayOFWeek = (byte)objActivity.DayOfWeek;
                    objViewModel.FromTime = objActivity.FromTime.TimeOfDay;
                    objViewModel.ToTime = objActivity.ToTime.TimeOfDay;
                    objViewModel.ActivityName = objActivity.ActivityName;
                    lstActivityViewModel.Add(objViewModel);

                }
                int startRow = 11;
                int count = 0;
                for (int b = 0; b < allDates.Count; b++)
                {
                    var x = b + 2;

                    var thu = lstActivityViewModel.Where(t => t.DayOFWeek == x).ToList();
                    var day = allDates[b].ToString("dd/MM/yyyy");
                    count = count + thu.Count;
                    switch (x)
                    {
                        case 2:
                            if (thu.Count() >= 1)
                            {
                                sheet.GetRange(startRow, 1, startRow + thu.Count() - 1, 1).Merge();
                                sheet.SetCellValue(startRow, 1, b + 1);
                                sheet.GetRange(startRow, 2, startRow + thu.Count() - 1, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "Thứ " + x + " - " + day);
                                for (int e = 0; e < thu.Count(); e++)
                                {
                                    sheet.SetCellValue(startRow, 3, thu[e].FromTime.ToString());
                                    sheet.SetCellValue(startRow, 4, thu[e].ToTime.ToString());
                                    sheet.SetCellValue(startRow, 5, thu[e].ActivityName.ToString());
                                    startRow++;
                                }
                            }
                            else
                            {
                                sheet.GetRange(startRow, 2, startRow, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "");
                                sheet.SetCellValue(startRow + 1, 3, "");
                                sheet.SetCellValue(startRow + 1, 4, "");
                                sheet.SetCellValue(startRow + 1, 5, "");
                            }
                            break;
                        case 3:
                            if (thu.Count() >= 1)
                            {
                                sheet.GetRange(startRow, 1, startRow + thu.Count() - 1, 1).Merge();
                                sheet.SetCellValue(startRow, 1, b + 1);
                                sheet.GetRange(startRow, 2, startRow + thu.Count() - 1, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "Thứ " + x + " - " + day);
                                for (int e = 0; e < thu.Count(); e++)
                                {
                                    sheet.SetCellValue(startRow, 3, thu[e].FromTime.ToString());
                                    sheet.SetCellValue(startRow, 4, thu[e].ToTime.ToString());
                                    sheet.SetCellValue(startRow, 5, thu[e].ActivityName.ToString());
                                    startRow++;
                                }
                            }
                            else
                            {
                                sheet.GetRange(startRow, 2, startRow, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "");
                                sheet.SetCellValue(startRow + 1, 3, "");
                                sheet.SetCellValue(startRow + 1, 4, "");
                                sheet.SetCellValue(startRow + 1, 5, "");
                            }
                            break;
                        case 4:
                            if (thu.Count() >= 1)
                            {
                                sheet.GetRange(startRow, 1, startRow + thu.Count() - 1, 1).Merge();
                                sheet.SetCellValue(startRow, 1, b + 1);
                                sheet.GetRange(startRow, 2, startRow + thu.Count() - 1, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "Thứ " + x + " - " + day);
                                for (int e = 0; e < thu.Count(); e++)
                                {
                                    sheet.SetCellValue(startRow, 3, thu[e].FromTime.ToString());
                                    sheet.SetCellValue(startRow, 4, thu[e].ToTime.ToString());
                                    sheet.SetCellValue(startRow, 5, thu[e].ActivityName.ToString());
                                    startRow++;
                                }
                            }
                            else
                            {
                                sheet.GetRange(startRow, 2, startRow, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "");
                                sheet.SetCellValue(startRow + 1, 3, "");
                                sheet.SetCellValue(startRow + 1, 4, "");
                                sheet.SetCellValue(startRow + 1, 5, "");
                            }
                            break;
                        case 5:
                            if (thu.Count() >= 1)
                            {
                                sheet.GetRange(startRow, 1, startRow + thu.Count() - 1, 1).Merge();
                                sheet.SetCellValue(startRow, 1, b + 1);
                                sheet.GetRange(startRow, 2, startRow + thu.Count() - 1, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "Thứ " + x + " - " + day);
                                for (int e = 0; e < thu.Count(); e++)
                                {
                                    sheet.SetCellValue(startRow, 3, thu[e].FromTime.ToString());
                                    sheet.SetCellValue(startRow, 4, thu[e].ToTime.ToString());
                                    sheet.SetCellValue(startRow, 5, thu[e].ActivityName.ToString());
                                    startRow++;
                                }
                            }
                            else
                            {
                                sheet.GetRange(startRow, 2, startRow, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "");
                                sheet.SetCellValue(startRow + 1, 3, "");
                                sheet.SetCellValue(startRow + 1, 4, "");
                                sheet.SetCellValue(startRow + 1, 5, "");
                            }
                            break;
                        case 6:
                            if (thu.Count >= 1)
                            {
                                sheet.GetRange(startRow, 1, startRow + thu.Count() - 1, 1).Merge();
                                sheet.SetCellValue(startRow, 1, b + 1);
                                sheet.GetRange(startRow, 2, startRow + thu.Count() - 1, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "Thứ " + x + " - " + day);
                                for (int e = 0; e < thu.Count(); e++)
                                {
                                    sheet.SetCellValue(startRow, 3, thu[e].FromTime.ToString());
                                    sheet.SetCellValue(startRow, 4, thu[e].ToTime.ToString());
                                    sheet.SetCellValue(startRow, 5, thu[e].ActivityName.ToString());
                                    startRow++;
                                }
                            }
                            else
                            {
                                sheet.GetRange(startRow, 2, startRow, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "");
                                sheet.SetCellValue(startRow + 1, 3, "");
                                sheet.SetCellValue(startRow + 1, 4, "");
                                sheet.SetCellValue(startRow + 1, 5, "");
                            }
                            break;
                        case 7:
                            if (thu.Count() >= 1)
                            {
                                sheet.GetRange(startRow, 1, startRow + thu.Count() - 1, 1).Merge();
                                sheet.SetCellValue(startRow, 1, b + 1);
                                sheet.GetRange(startRow, 2, startRow + thu.Count() - 1, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "Thứ " + x + " - " + day);
                                for (int e = 0; e < thu.Count(); e++)
                                {
                                    sheet.SetCellValue(startRow, 3, thu[e].FromTime.ToString());
                                    sheet.SetCellValue(startRow, 4, thu[e].ToTime.ToString());
                                    sheet.SetCellValue(startRow, 5, thu[e].ActivityName.ToString());
                                    startRow++;
                                }
                            }
                            else
                            {
                                sheet.GetRange(startRow, 2, startRow, 2).Merge();
                                sheet.SetCellValue(startRow, 2, "");
                                sheet.SetCellValue(startRow + 1, 3, "");
                                sheet.SetCellValue(startRow + 1, 4, "");
                                sheet.SetCellValue(startRow + 1, 5, "");
                            }
                            break;
                    }
                }
                sheet.GetRange(11, 1, 10 + count, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(11, 1, 10 + count, 5).WrapText();
            }
            #endregion
            sheetCurrent.Delete();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            fileName = "KeHoachHoatDong_" + DateTime.Now + ".xls";
            result.FileDownloadName = fileName;
            return result;
        }
    }
    #endregion

    #region time object
    public class TimeActivity
    {
        public TimeActivity(TimeSpan from, TimeSpan to)
        {
            this.FromTime = from;
            this.ToTime = to;
        }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
    }
    #endregion
}