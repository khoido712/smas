﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.CodeConfigArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;

// TODO: HieuND, Chua xu li phan phan quyen, bo phan SkipCheckRole
namespace SMAS.Web.Areas.CodeConfigArea.Controllers
{
    public class CodeConfigController : BaseController
    {
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private readonly ISchoolCodeConfigBusiness SchoolCodeConfigBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;

        public CodeConfigController(ICodeConfigBusiness CodeConfigBusiness, ISchoolCodeConfigBusiness SchoolCodeConfigBusiness, IPupilProfileBusiness PupilProfileBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness)
        {
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.SchoolCodeConfigBusiness = SchoolCodeConfigBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo glo = new GlobalInfo();
            SetViewDataPermission("CodeConfig", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return View();
        }

        #region CodeConfig For Pupil
        [HttpGet]
        [SkipFunctionPath]
        public ActionResult _SupervisingDeptPupil()
        {
            GlobalInfo gb = new GlobalInfo();
            SetViewDataPermission("CodeConfig", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SupervisingDeptPupilViewModel model = new SupervisingDeptPupilViewModel();
            CodeConfig codeConfig = CodeConfigBusiness.SearchByProvince(gb.ProvinceID.Value, SMAS.Business.Common.GlobalConstants.CODECONFIG_CODETYPE_PUPIL);
            model.Pupil_IsChoiseSchool = true;
            model.Pupil_IsNotSchoolModify = false;
            model.Pupil_IsActive = true;
            if (codeConfig != null)
            {
                model.Pupil_IsActive = codeConfig.IsActive;
                model.Pupil_CodeConfigID = codeConfig.CodeConfigID;
                model.Pupil_IsChoiseSchool = codeConfig.IsChoiceSchool;
                model.Pupil_IsNotSchoolModify = !codeConfig.IsSchoolModify;
                model.Pupil_IsStringIdentify = codeConfig.IsStringIdentify;
                model.Pupil_MiddleCodeType = codeConfig.MiddleCodeType.Value;
                model.Pupil_ProvinceID = codeConfig.ProvinceID;
                model.Pupil_StringIdentify = codeConfig.IsStringIdentify ? codeConfig.StringIdentify : string.Empty;
                model.Pupil_NumberLength = codeConfig.NumberLength;
                model.FormatNumber = codeConfig.FormatNumber.HasValue ? codeConfig.FormatNumber.Value : 0;
            }
            GetMiddleCodeType();
            return PartialView("_SupervisingDeptPupil", model);
        }

        [HttpPost]
        [SkipFunctionPath]
        [ValidateAntiForgeryToken]
        public ActionResult _SupervisingDeptPupil(SupervisingDeptPupilViewModel model)
        {
            GlobalInfo gb = new GlobalInfo();
            if (GetMenupermission("CodeConfig", gb.UserAccountID, gb.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (model.Pupil_IsStringIdentify && string.IsNullOrEmpty(model.Pupil_StringIdentify))
                throw new BusinessException(string.Format(Res.Get("Common_Validate_Required"), Res.Get("CodeConfig_Label_StringIdentify")));

            if (ModelState.IsValid)
            {
                CodeConfig codeConfig = new CodeConfig();
                codeConfig.CodeType = SMAS.Business.Common.GlobalConstants.CODECONFIG_CODETYPE_PUPIL;
                codeConfig.IsActive = model.Pupil_IsActive;
                codeConfig.IsChoiceSchool = true;
                codeConfig.IsSchoolModify = !model.Pupil_IsNotSchoolModify;
                codeConfig.IsStringIdentify = model.Pupil_IsStringIdentify;
                codeConfig.MiddleCodeType = model.Pupil_MiddleCodeType;
                codeConfig.NumberLength = model.Pupil_NumberLength;
                codeConfig.ProvinceID = gb.ProvinceID.Value;
                codeConfig.StringIdentify = model.Pupil_IsStringIdentify ? model.Pupil_StringIdentify : string.Empty;
                codeConfig.CreatedDate = DateTime.Now;
                codeConfig.FormatNumber = model.FormatNumber;
                this.CodeConfigBusiness.Insert(codeConfig);
                this.CodeConfigBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ErrorMK")));
            }
        }

        [HttpGet]
        [SkipFunctionPath]
        public PartialViewResult _SchoolPupil()
        {
            GlobalInfo gb = new GlobalInfo();
            SetViewDataPermission("CodeConfig", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);


            SchoolPupilViewModel model = null;
            CodeConfig codeConfig = CodeConfigBusiness.SearchBySchool(gb.SchoolID.Value, SMAS.Business.Common.GlobalConstants.CODECONFIG_CODETYPE_PUPIL);
            if (codeConfig != null)
            {
                model = new SchoolPupilViewModel();
                model.Pupil_IsChoiseSchool = true;
                model.Pupil_IsActive = codeConfig.IsActive;
                model.Pupil_CodeConfigID = codeConfig.CodeConfigID;
                model.Pupil_IsChoiseSchool = codeConfig.IsChoiceSchool;
                model.Pupil_IsNotSchoolModify = !codeConfig.IsSchoolModify;
                model.Pupil_IsStringIdentify = codeConfig.IsStringIdentify;
                model.Pupil_MiddleCodeType = codeConfig.MiddleCodeType.Value;
                model.Pupil_ProvinceID = codeConfig.ProvinceID;
                model.Pupil_StringIdentify = codeConfig.IsStringIdentify ? codeConfig.StringIdentify : string.Empty;
                model.Pupil_NumberLength = codeConfig.NumberLength;
                model.FormatNumber = codeConfig.FormatNumber.HasValue ? codeConfig.FormatNumber.Value : 0;
                SchoolCodeConfig schoolCodeConfig = this.SchoolCodeConfigBusiness.SearchBySchool(codeConfig.CodeConfigID, gb.SchoolID.Value);
                model.Pupil_StartNumber = schoolCodeConfig != null ? schoolCodeConfig.StartNumber : 1;
                GetMiddleCodeType();
            }
            return PartialView("_SchoolPupil", model);
        }

        [HttpPost]
        [SkipFunctionPath]
        [ValidateAntiForgeryToken]
        public ActionResult _SchoolPupil(SchoolPupilViewModel model)
        {
            GlobalInfo gb = new GlobalInfo();
            if (GetMenupermission("CodeConfig", gb.UserAccountID, gb.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (ModelState.IsValid)
            {
                CodeConfig codeConfig = CodeConfigBusiness.SearchBySchool(gb.SchoolID.Value, SMAS.Business.Common.GlobalConstants.CODECONFIG_CODETYPE_PUPIL);

                if (codeConfig == null)
                    return Json(new JsonMessage(Res.Get("CodeConfig_Message_ProvinceNotConfigYet")));

                SchoolCodeConfig schoolCodeConfig = this.SchoolCodeConfigBusiness.SearchBySchool(codeConfig.CodeConfigID, gb.SchoolID.Value);
                if (!model.Pupil_StartNumber.HasValue)
                {
                    throw new BusinessException("CodeConfig_Message_NoStartNumber");
                }

                if (model.Pupil_StartNumber.Value <= 0)
                {
                    throw new BusinessException("CodeConfig_Message_StartNumberNotPositive");
                }
                if (model.Pupil_NumberLength < model.Pupil_StartNumber.ToString().Length)
                {
                    throw new BusinessException("CodeConfig_Message_NumberLengthLessThanStartNumberLength");
                }
                //dungnt fix bug 0168491: [Cấu hình hệ thống]_Cấu hình sinh mã tự động (acc cấp trường): Ngày cập nhật khác ngày cấu hình htại thì không thêm mới cấu hình 
                if (schoolCodeConfig != null)   //ton tai cau hinh truoc roi
                {
                    //neu cau hinh ay khong trung voi ngay hien tai thi them moi,con ko thi update
                    if (schoolCodeConfig.CreatedDate.HasValue
                        && (schoolCodeConfig.CreatedDate.Value.Day == DateTime.Now.Day
                        && schoolCodeConfig.CreatedDate.Value.Month == DateTime.Now.Month
                        && schoolCodeConfig.CreatedDate.Value.Year == DateTime.Now.Year))
                    {
                        schoolCodeConfig.StartNumber = model.Pupil_StartNumber.HasValue && model.Pupil_StartNumber.Value > 0 ? model.Pupil_StartNumber.Value : 1;
                        this.SchoolCodeConfigBusiness.Update(schoolCodeConfig);
                        this.SchoolCodeConfigBusiness.Save();
                        return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                    }
                    else
                    {
                        schoolCodeConfig = new SchoolCodeConfig();
                        schoolCodeConfig.CreatedDate = DateTime.Now;
                        schoolCodeConfig.CodeConfigID = codeConfig.CodeConfigID;
                        schoolCodeConfig.SchoolID = gb.SchoolID.Value;
                        schoolCodeConfig.StartNumber = model.Pupil_StartNumber.HasValue && model.Pupil_StartNumber.Value > 0 ? model.Pupil_StartNumber.Value : 1;
                        this.SchoolCodeConfigBusiness.Insert(schoolCodeConfig);
                        this.SchoolCodeConfigBusiness.Save();
                        return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                    }
                }
                else
                {
                    schoolCodeConfig = new SchoolCodeConfig();
                    schoolCodeConfig.CreatedDate = DateTime.Now;
                    schoolCodeConfig.CodeConfigID = codeConfig.CodeConfigID;
                    schoolCodeConfig.SchoolID = gb.SchoolID.Value;
                    schoolCodeConfig.StartNumber = model.Pupil_StartNumber.HasValue && model.Pupil_StartNumber.Value > 0 ? model.Pupil_StartNumber.Value : 1;
                    this.SchoolCodeConfigBusiness.Insert(schoolCodeConfig);
                    this.SchoolCodeConfigBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ErrorMK")));
            }
        }
        [HttpPost]
        public JsonResult ApprovePupilCode()
        {
            CodeConfigBusiness.ApproveNewPupilCode(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            return Json(new JsonMessage("Đánh mã thành công", "success"));
        }
        #endregion

        #region CodeConfig For Teacher
        [HttpGet]
        [SkipFunctionPath]
        public PartialViewResult _SupervisingDeptTeacher()
        {
            GlobalInfo gb = new GlobalInfo();
            SetViewDataPermission("CodeConfig", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SupervisingDeptTeacherViewModel model = new SupervisingDeptTeacherViewModel();
            CodeConfig codeConfig = CodeConfigBusiness.SearchByProvince(gb.ProvinceID.Value, SMAS.Business.Common.GlobalConstants.CODECONFIG_CODETYPE_TEACHER);
            model.Teacher_IsChoiseSchool = true;
            model.Teacher_IsNotSchoolModify = false;
            model.Teacher_IsActive = true;
            if (codeConfig != null)
            {
                model.Teacher_IsActive = codeConfig.IsActive;
                model.Teacher_CodeConfigID = codeConfig.CodeConfigID;
                model.Teacher_IsChoiseSchool = codeConfig.IsChoiceSchool;
                model.Teacher_IsNotSchoolModify = !codeConfig.IsSchoolModify;
                model.Teacher_IsStringIdentify = codeConfig.IsStringIdentify;
                model.Teacher_ProvinceID = codeConfig.ProvinceID;
                model.Teacher_StringIdentify = codeConfig.IsStringIdentify ? codeConfig.StringIdentify : string.Empty;
                model.Teacher_NumberLength = codeConfig.NumberLength;
            }
            GetMiddleCodeType();
            return PartialView("_SupervisingDeptTeacher", model);
        }

        [HttpPost]
        [SkipFunctionPath]
        [ValidateAntiForgeryToken]
        public ActionResult _SupervisingDeptTeacher(SupervisingDeptTeacherViewModel model)
        {
            GlobalInfo gb = new GlobalInfo();
            if (GetMenupermission("CodeConfig", gb.UserAccountID, gb.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (!gb.IsSuperVisingDeptRole)
                throw new BusinessException(Res.Get("Common_Validate_PermissionDeny"));

            if (model.Teacher_IsStringIdentify && string.IsNullOrEmpty(model.Teacher_StringIdentify))
                throw new BusinessException(string.Format(Res.Get("Common_Validate_Required"), Res.Get("CodeConfig_Label_StringIdentify")));

            if (ModelState.IsValid)
            {
                CodeConfig codeConfig = new CodeConfig();
                codeConfig.CodeType = SMAS.Business.Common.GlobalConstants.CODECONFIG_CODETYPE_TEACHER;
                codeConfig.IsActive = model.Teacher_IsActive;
                codeConfig.IsChoiceSchool = true;
                codeConfig.IsSchoolModify = !model.Teacher_IsNotSchoolModify;
                codeConfig.IsStringIdentify = model.Teacher_IsStringIdentify;
                codeConfig.NumberLength = model.Teacher_NumberLength;
                codeConfig.ProvinceID = gb.ProvinceID.Value;
                codeConfig.StringIdentify = model.Teacher_IsStringIdentify ? model.Teacher_StringIdentify : string.Empty;
                codeConfig.CreatedDate = DateTime.Now;
                this.CodeConfigBusiness.Insert(codeConfig);
                this.CodeConfigBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ErrorMK")));
            }
        }

        [HttpGet]
        [SkipFunctionPath]
        public PartialViewResult _SchoolTeacher()
        {
            SetViewDataPermission("CodeConfig", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            GlobalInfo gb = new GlobalInfo();
            SchoolTeacherViewModel model = null;
            CodeConfig codeConfig = CodeConfigBusiness.SearchBySchool(gb.SchoolID.Value, SMAS.Business.Common.GlobalConstants.CODECONFIG_CODETYPE_TEACHER);
            if (codeConfig != null)
            {
                model = new SchoolTeacherViewModel();
                model.Teacher_IsChoiseSchool = true;
                model.Teacher_IsActive = codeConfig.IsActive;
                model.Teacher_CodeConfigID = codeConfig.CodeConfigID;
                model.Teacher_IsChoiseSchool = codeConfig.IsChoiceSchool;
                model.Teacher_IsNotSchoolModify = !codeConfig.IsSchoolModify;
                model.Teacher_IsStringIdentify = codeConfig.IsStringIdentify;
                model.Teacher_ProvinceID = codeConfig.ProvinceID;
                model.Teacher_StringIdentify = codeConfig.IsStringIdentify ? codeConfig.StringIdentify : string.Empty;
                model.Teacher_NumberLength = codeConfig.NumberLength;
                SchoolCodeConfig schoolCodeConfig = this.SchoolCodeConfigBusiness.SearchBySchool(codeConfig.CodeConfigID, gb.SchoolID.Value);
                model.Teacher_StartNumber = schoolCodeConfig != null ? schoolCodeConfig.StartNumber : 1;
                GetMiddleCodeType();
            }
            return PartialView("_SchoolTeacher", model);
        }

        [HttpPost]
        [SkipFunctionPath]
        [ValidateAntiForgeryToken]
        public ActionResult _SchoolTeacher(SchoolTeacherViewModel model)
        {
            GlobalInfo gb = new GlobalInfo();
            if (GetMenupermission("CodeConfig", gb.UserAccountID, gb.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (ModelState.IsValid)
            {
                CodeConfig codeConfig = CodeConfigBusiness.SearchBySchool(gb.SchoolID.Value, SMAS.Business.Common.GlobalConstants.CODECONFIG_CODETYPE_TEACHER);

                if (codeConfig == null)
                    return Json(new JsonMessage(Res.Get("CodeConfig_Message_ProvinceNotConfigYet")));

                SchoolCodeConfig schoolCodeConfig = this.SchoolCodeConfigBusiness.SearchBySchool(codeConfig.CodeConfigID, gb.SchoolID.Value);

                if (!model.Teacher_StartNumber.HasValue)
                {
                    throw new BusinessException("CodeConfig_Message_NoStartNumber");
                }

                if (model.Teacher_StartNumber.Value <= 0)
                {
                    throw new BusinessException("CodeConfig_Message_StartNumberNotPositive");
                }

                if (model.Teacher_NumberLength < model.Teacher_StartNumber.ToString().Length)
                {
                    throw new BusinessException("CodeConfig_Message_NumberLengthLessThanStartNumberLength");
                }

                //dungnt fix bug 0168491: [Cấu hình hệ thống]_Cấu hình sinh mã tự động (acc cấp trường): Ngày cập nhật khác ngày cấu hình htại thì không thêm mới cấu hình 
                if (schoolCodeConfig != null)
                {
                    if (schoolCodeConfig.CreatedDate.HasValue
                        && (schoolCodeConfig.CreatedDate.Value.Day == DateTime.Now.Day
                        && schoolCodeConfig.CreatedDate.Value.Month == DateTime.Now.Month
                        && schoolCodeConfig.CreatedDate.Value.Year == DateTime.Now.Year))
                    {
                        schoolCodeConfig.StartNumber = model.Teacher_StartNumber.HasValue && model.Teacher_StartNumber.Value > 0 ? model.Teacher_StartNumber.Value : 1;
                        this.SchoolCodeConfigBusiness.Update(schoolCodeConfig);
                        this.SchoolCodeConfigBusiness.Save();
                        return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                    }
                    else
                    {
                        schoolCodeConfig = new SchoolCodeConfig();
                        schoolCodeConfig.CreatedDate = DateTime.Now;
                        schoolCodeConfig.CodeConfigID = codeConfig.CodeConfigID;
                        schoolCodeConfig.SchoolID = gb.SchoolID.Value;
                        schoolCodeConfig.StartNumber = model.Teacher_StartNumber.HasValue && model.Teacher_StartNumber.Value > 0 ? model.Teacher_StartNumber.Value : 1;
                        this.SchoolCodeConfigBusiness.Insert(schoolCodeConfig);
                        this.SchoolCodeConfigBusiness.Save();
                        return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                    }

                }
                else
                {
                    schoolCodeConfig = new SchoolCodeConfig();
                    schoolCodeConfig.CodeConfigID = codeConfig.CodeConfigID;
                    schoolCodeConfig.CreatedDate = DateTime.Now;
                    schoolCodeConfig.SchoolID = gb.SchoolID.Value;
                    schoolCodeConfig.StartNumber = model.Teacher_StartNumber.HasValue && model.Teacher_StartNumber.Value > 0 ? model.Teacher_StartNumber.Value : 1;
                    this.SchoolCodeConfigBusiness.Insert(schoolCodeConfig);
                    this.SchoolCodeConfigBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ErrorMK")));
            }
        }

        [HttpPost]
        public JsonResult ApproveTeacherCode()
        {
            CodeConfigBusiness.ApproveNewTeacherCode(_globalInfo.SchoolID.Value);
            return Json(new JsonMessage("Đánh mã thành công", "success"));
        }
        #endregion

        private void GetMiddleCodeType()
        {
            List<ComboObject> lstMiddleCodeType = new List<ComboObject>();
            lstMiddleCodeType.Add(new ComboObject(Res.Get("CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR"), SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR.ToString()));
            lstMiddleCodeType.Add(new ComboObject(Res.Get("CODECONFIG_MIDDLECODETYPE_START_LEVEL_1"), SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_START_LEVEL_1.ToString()));
            lstMiddleCodeType.Add(new ComboObject(Res.Get("CODECONFIG_MIDDLECODETYPE_COURSE"), SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_COURSE.ToString()));
            lstMiddleCodeType.Add(new ComboObject(Res.Get("CODECONFIG_MIDDLECODETYPE_NO_USED"), SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_NO_USED.ToString()));
            SelectList lst = new SelectList(lstMiddleCodeType, "value", "key");
            ViewData[CodeConfigConstants.LIST_MIDDLE_CODE_TYPE] = lst;
        }

        private string GetMiddleCodeTypeName(int? value)
        {
            if (!value.HasValue) return "";

            string middleCodeType = "N/A";

            switch (value)
            {
                case SMAS.Business.Common.GlobalConstants.CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR:
                    middleCodeType = Res.Get("CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR");
                    break;
                case SMAS.Business.Common.GlobalConstants.CODECONFIG_MIDDLECODETYPE_START_LEVEL_1:
                    middleCodeType = Res.Get("CODECONFIG_MIDDLECODETYPE_START_LEVEL_1");
                    break;
                case SMAS.Business.Common.GlobalConstants.CODECONFIG_MIDDLECODETYPE_COURSE:
                    middleCodeType = Res.Get("CODECONFIG_MIDDLECODETYPE_COURSE");
                    break;
                case SMAS.Business.Common.GlobalConstants.CODECONFIG_MIDDLECODETYPE_NO_USED:
                    middleCodeType = Res.Get("CODECONFIG_MIDDLECODETYPE_NO_USED");
                    break;
            }
            return middleCodeType;
        }

        private CodeConfig CopyCodeConfig(CodeConfig codeConfig)
        {
            if (codeConfig == null) return null;
            CodeConfig newCodeConfig = new CodeConfig();
            newCodeConfig.CodeType = codeConfig.CodeType;
            newCodeConfig.CreatedDate = codeConfig.CreatedDate;
            newCodeConfig.IsActive = codeConfig.IsActive;
            newCodeConfig.IsChoiceSchool = codeConfig.IsActive;
            newCodeConfig.IsSchoolModify = codeConfig.IsActive;
            newCodeConfig.IsStringIdentify = codeConfig.IsActive;
            newCodeConfig.MiddleCodeType = codeConfig.MiddleCodeType;
            newCodeConfig.ModifiedDate = codeConfig.ModifiedDate;
            newCodeConfig.NumberLength = codeConfig.NumberLength;
            newCodeConfig.ProvinceID = codeConfig.ProvinceID;
            newCodeConfig.StringIdentify = codeConfig.StringIdentify;
            return newCodeConfig;
        }

        private string ToStringWithFixedLength(int number, int length)
        {
            if (number < 0) return string.Empty;

            if (number.ToString().Length > length) return string.Empty;

            return number.ToString().PadLeft(length, '0');
        }
    }
}
