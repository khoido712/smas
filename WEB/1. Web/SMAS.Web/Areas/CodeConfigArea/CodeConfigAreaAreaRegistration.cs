﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.CodeConfigArea
{
    public class CodeConfigAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CodeConfigArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CodeConfigArea_default",
                "CodeConfigArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
