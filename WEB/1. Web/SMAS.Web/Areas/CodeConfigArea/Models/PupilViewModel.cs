using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;

namespace SMAS.Web.Areas.CodeConfigArea.Models
{
    public class PupilViewModel
    {
        [ScaffoldColumn(false)]
        public int CodeConfigID {get;set;}
        
        //CodeType=1

        [ScaffoldColumn(false)]
        public int ProvinceID {get;set;}
        [ResourceDisplayName("CodeConfig_Label_IsChoiceSchool")]
        public bool? IsChoiseSchool { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsActive")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public bool IsActive { get; set; }

        [ResourceDisplayName("CodeConfig_Label_MiddleCodeType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CodeConfigConstants.LIST_MIDDLE_CODE_TYPE)]
        public int? MiddleCodeType {get;set;}

        [ResourceDisplayName("CodeConfig_Label_IsStringIdentify")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public bool IsStringIdentify {get;set;}

        [ResourceDisplayName("CodeConfig_Label_StringIdentify")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string StringIdentify {get;set;}

        [ResourceDisplayName("CodeConfig_Label_NumberLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [Range(1, 9, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int NumberLength {get;set;}

        [ResourceDisplayName("CodeConfig_Label_StartNumber")]
        [Range(1, 99999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int? StartNumber { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsSchoolModify")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public bool IsSchoolModify {get;set;}
    }
}
