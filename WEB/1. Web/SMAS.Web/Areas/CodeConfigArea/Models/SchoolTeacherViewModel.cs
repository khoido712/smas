using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.CodeConfigArea.Models
{
    public class SchoolTeacherViewModel
    {
        [ScaffoldColumn(false)]
        public int Teacher_CodeConfigID { get; set; }

        [ScaffoldColumn(false)]
        public int Teacher_ProvinceID { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsChoiceSchool")]
        public bool? Teacher_IsChoiseSchool { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsActive")]
        public bool Teacher_IsActive { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsStringIdentify")]
        public bool Teacher_IsStringIdentify { get; set; }

        [ResourceDisplayName("CodeConfig_Label_StringIdentify")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string Teacher_StringIdentify { get; set; }

        [ResourceDisplayName("CodeConfig_Label_NumberLength")]
        public int Teacher_NumberLength { get; set; }

        [ResourceDisplayName("SchoolCodeConfig_Label_StartNumber")]
        [Range(0, 99999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int? Teacher_StartNumber { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsNotSchoolModify")]
        public bool Teacher_IsNotSchoolModify { get; set; }
    }
}
