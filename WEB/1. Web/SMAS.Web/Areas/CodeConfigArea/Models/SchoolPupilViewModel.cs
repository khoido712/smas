using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.CodeConfigArea.Models
{
    public class SchoolPupilViewModel
    {
        [ScaffoldColumn(false)]
        public int Pupil_CodeConfigID { get; set; }

        [ScaffoldColumn(false)]
        public int Pupil_ProvinceID { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsChoiceSchool")]
        public bool? Pupil_IsChoiseSchool { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsActive")]
        public bool Pupil_IsActive { get; set; }

        [ResourceDisplayName("CodeConfig_Label_MiddleCodeType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CodeConfigConstants.LIST_MIDDLE_CODE_TYPE)]
        public int Pupil_MiddleCodeType { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsStringIdentify")]
        public bool Pupil_IsStringIdentify { get; set; }

        [ResourceDisplayName("CodeConfig_Label_StringIdentify")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        public string Pupil_StringIdentify { get; set; }

        [ResourceDisplayName("CodeConfig_Label_NumberLength")]
        public int Pupil_NumberLength { get; set; }

        [ResourceDisplayName("SchoolCodeConfig_Label_StartNumber")]
        [Range(0, 99999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int? Pupil_StartNumber { get; set; }

        [ResourceDisplayName("CodeConfig_Label_IsNotSchoolModify")]
        public bool Pupil_IsNotSchoolModify { get; set; }

        public int FormatNumber { get; set; }
    }
}
