﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MonitoringBookYearArea.Models;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.MonitoringBookYearArea.Controllers
{
    public class MonitoringBookYearController : BaseController
    {
        private IClassificationLevelHealthBusiness ClassificationLevelHealthBusiness;
        private IPhysicalTestBusiness PhysicalTestBusiness;
        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private readonly IHealthPeriodBusiness HealthPeriodBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        public MonitoringBookYearController(IClassificationLevelHealthBusiness classificationLevelHealthBusiness, IPhysicalTestBusiness PhysicalTestBusiness, IPupilProfileBusiness pupilProfileBusiness, IMonitoringBookBusiness monitoringbookBusiness, IHealthPeriodBusiness healthPeriodBusiness, IClassProfileBusiness classProfileBusiness)
        {
            this.ClassificationLevelHealthBusiness = classificationLevelHealthBusiness;
            this.PhysicalTestBusiness = PhysicalTestBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.MonitoringBookBusiness = monitoringbookBusiness;
            this.HealthPeriodBusiness = healthPeriodBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
        }

        //
        // GET: /MonitoringBook/

        public ActionResult Index()
        {
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            ClassSearchInfo["EmployeeID"] = _globalInfo.EmployeeID;
            List<EducationLevel> lsEducationLevel = null;
            IQueryable<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo)
                .Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);


            lsEducationLevel = lstClass.Select(o => o.EducationLevel).Distinct().OrderBy(p=>p.EducationLevelID).ToList();
            foreach (var item in lsEducationLevel)
            {
                ViewData[item.Resolution] = lstClass.Where(c => c.EducationLevelID == item.EducationLevelID).ToList();
            }

            List<ClassProfile> listClass = lstClass.ToList();
            Session["lstClass"] = listClass;
            //-	cboEducationLevel: UserInfo.EducationLevels
            if (lsEducationLevel != null)
            {
                ViewData[MonitoringBookYearConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[MonitoringBookYearConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
            }
            //-	cboClass: Giá trị mặc định là [Tất cả]
            ViewData[MonitoringBookYearConstants.LISTCLASS] = new SelectList(new string[] { });

            //-	cboStatus: CommonList.PupilStatus
            ViewData[MonitoringBookYearConstants.LISTStatus] = new SelectList(CommonList.HealthStatus(), "key", "value");
            //cboHealthPeriod:    
            List<HealthPeriod> ListHealthPeriod = HealthPeriodBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData[MonitoringBookYearConstants.LISTHealthPeriod] = new SelectList(ListHealthPeriod, "HealthPeriodID", "Resolution");

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["HealthPeriodID"] = ListHealthPeriod.Count > 0 ? ListHealthPeriod.FirstOrDefault().HealthPeriodID : 0;
            SearchInfo["HealthStatus"] = SystemParamsInFile.HEALTH_STATUS_ALL;
            SearchInfo["IsDredging"] = false;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID.Value;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            List<MonitoringBookViewModel> lst = this._Search(SearchInfo).ToList();
            // Sap xep
            lst = lst
                .OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassName).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilShortName)).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.FullName))
                .ToList();
            if (lst != null)
            {
                ViewData["totalRecord"] = lst.Count();
                ViewData["currentPage"] = 1;
                ViewData["pageSize"] = Config.PageSize;
                lst = lst.Take(Config.PageSize).ToList();
            }
            else
            {
                ViewData["totalRecord"] = 0;
            }

            ViewData[MonitoringBookYearConstants.LIST_MONITORINGBOOK] = lst;
            ViewData[MonitoringBookYearConstants.CHECK_PERMISSION_VIEW_HEALTH_TEST] = CheckActionPerMinViewByController("MonitoringBookYear");
            return View();
        }

        public PartialViewResult Search(FormCollection frm)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            SearchInfo["HealthPeriodID"] = frm["HealthPeriodID"];
            SearchInfo["HealthStatus"] = frm["Status"];
            SearchInfo["IsDredging"] = frm["IsDreding"];
            SearchInfo["EducationLevelID"] = frm["EducationLevel"];
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfo["ClassID"] = frm["Class"];
            SearchInfo["FullName"] = frm["Fullname"];
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID.Value;
            SearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel.Value;

            List<MonitoringBookViewModel> lst = this._Search(SearchInfo).ToList();
            lst = lst.OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassName).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilShortName)).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.FullName)).ToList();
            if (lst != null)
            {
                ViewData["totalRecord"] = lst.Count();
                ViewData["currentPage"] = 1;
                ViewData["pageSize"] = Config.PageSize;
                lst = lst.Take(Config.PageSize).ToList();
            }
            else
            {
                ViewData["totalRecord"] = 0;
            }
            ViewData[MonitoringBookYearConstants.LIST_MONITORINGBOOK] = lst;
            ViewData[MonitoringBookYearConstants.CHECK_PERMISSION_VIEW_HEALTH_TEST] = CheckActionPerMinViewByController("MonitoringBookYear");
            //Get view data here
            return PartialView("_List");
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult CustomBindingSearch(FormCollection frm, GridCommand command)
        {
           // Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            SearchInfo["HealthPeriodID"] = frm["HealthPeriodID"];
            SearchInfo["HealthStatus"] = frm["Status"];
            SearchInfo["IsDredging"] = frm["IsDreding"];
            SearchInfo["EducationLevelID"] = frm["EducationLevel"];
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfo["ClassID"] = frm["Class"];
            SearchInfo["FullName"] = frm["Fullname"];
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID.Value;
            SearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel.Value;

            List<MonitoringBookViewModel> lst = this._Search(SearchInfo).ToList();
            lst = lst.OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassName).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilShortName)).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.FullName)).ToList();

            ViewData[MonitoringBookYearConstants.LIST_MONITORINGBOOK] = lst;
            int total = 0;
            if (lst != null)
            {
                total = lst.Count();
                lst = lst.Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            }
           
          
            return View(new GridModel<MonitoringBookViewModel>
            {
                Data = lst,
                Total = total
            });
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            MonitoringBook monitoringbook = new MonitoringBook();
            TryUpdateModel(monitoringbook);
            Utils.Utils.TrimObject(monitoringbook);

            this.MonitoringBookBusiness.Insert(monitoringbook);
            this.MonitoringBookBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int MonitoringBookID)
        {
            CheckPermissionForAction(MonitoringBookID, "MonitoringBook");
            MonitoringBook monitoringbook = this.MonitoringBookBusiness.Find(MonitoringBookID);
            TryUpdateModel(monitoringbook);
            Utils.Utils.TrimObject(monitoringbook);
            this.MonitoringBookBusiness.Update(monitoringbook);
            this.MonitoringBookBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            CheckPermissionForAction(id, "MonitoringBook");
            this.MonitoringBookBusiness.Delete(id);
            this.MonitoringBookBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<MonitoringBookViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EmployeeID"] = _globalInfo.EmployeeID;
            dic["Semester"] = _globalInfo.Semester;

            IQueryable<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);;          
            List<int> listClassID = new List<int>();
            foreach (ClassProfile cp in lstClass)
            {
                listClassID.Add(cp.ClassProfileID);
            }

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            IQueryable<MonitoringBookSearch> query = this.MonitoringBookBusiness.GetListPupil(SearchInfo);
            IQueryable<MonitoringBookViewModel> lst = query.Select(o => new MonitoringBookViewModel
            {
                AcademicYearID = o.AcademicYearID.Value,
                SchoolID = o.SchoolID.Value,
                ClassID = o.ClassID,
                PupilID = o.PupilID,
                PupilCode = o.PupilCode,
                FullName = o.FullName,
                BrithDate = o.BrithDate,
                Sex = (o.Sex == (int)SystemParamsInFile.GENRE_MALE) ? male : female,
                ClassName = o.ClassName,
                HealthPeriodID = o.HealthPeriodID,
                EducationLevelID = o.EducationLevelID.Value,
                IsAction = o.IsAction,
                MonitoringBookID = o.MonitoringBookID,
                DisabledLink = listClassID.Contains(o.ClassID) ? false : true,
                PupilShortName= o.PupilShortName
            });
            return lst;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                //IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                List<ClassProfile> lsCP = (List<ClassProfile>)Session["lstClass"];
                lsCP = lsCP.Where(x => x.EducationLevelID == EducationLevelID).ToList();
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }
        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            //: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary)
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };

            //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
            if (!global.IsAdminSchoolRole)
            {
                dic["UserAccountID"] = global.UserAccountID;
                dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }

            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return lsCP;
        }
    }
}
