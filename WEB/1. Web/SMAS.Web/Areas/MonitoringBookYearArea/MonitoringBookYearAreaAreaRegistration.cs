﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MonitoringBookYearArea
{
    public class MonitoringBookYearAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MonitoringBookYearArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MonitoringBookYearArea_default",
                "MonitoringBookYearArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
