﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearArea
{
    public class MonitoringBookYearConstants
    {
        public const string LIST_MONITORINGBOOK = "listMonitoringBook";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTCLASS = "LISTCLASS";
        public const string LISTHealthPeriod = "LISTHealthPeriod";
        public const string LISTStatus = "LISTStatus";
        public const string CHECK_PERMISSION_VIEW_HEALTH_TEST = "checkPremissionViewhealthTest";
    }
}