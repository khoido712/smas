﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.MonitoringBookYearArea.Models
{
    public class MonitoringBookViewModel
    {
        public int? MonitoringBookID { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int PupilID { get; set; }
        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public System.String PupilCode { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public System.String FullName { get; set; }
        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public System.String Sex { get; set; }
        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public System.Nullable<System.DateTime> BrithDate { get; set; }
        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public System.String ClassName { get; set; }
        public System.Nullable<System.DateTime> MonitoringDate { get; set; }
        public System.String Symptoms { get; set; }
        public System.String Solution { get; set; }
        public System.String UnitName { get; set; }
        public System.Nullable<System.Int32> MonitoringType { get; set; }
        public System.Nullable<System.Int32> HealthPeriodID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.Int32 EducationLevelID { get; set; }
        public bool IsAction { get; set; }
        public bool DisabledLink { get; set; }
        public string PupilShortName { get; set; }
    }
}

