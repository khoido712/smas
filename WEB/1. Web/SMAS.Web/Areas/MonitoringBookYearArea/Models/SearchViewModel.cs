﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using System.ComponentModel;
namespace SMAS.Web.Areas.MonitoringBookYearArea.Models
{
    public class SearchViewModel
    {
        [DisplayName("")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookYearConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? EducationLevel { get; set; }

        [DisplayName("")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookYearConstants.LISTCLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxGetClassID(this)")]
        public int? Class { get; set; }

        [DisplayName("")]
        [UIHint("Textbox")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Fullname { get; set; }

        [DisplayName("")]
        [UIHint("Combobox")]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookYearConstants.LISTHealthPeriod)]
        public int? HealthPeriodID { get; set; }

        [DisplayName("")]
        [UIHint("Combobox")]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("ViewDataKey", MonitoringBookYearConstants.LISTStatus)]
        public int? Status { get; set; }

        [DisplayName("")]
        [UIHint("Checkbox")]
        public Boolean IsDreding { get; set; }
    }
}