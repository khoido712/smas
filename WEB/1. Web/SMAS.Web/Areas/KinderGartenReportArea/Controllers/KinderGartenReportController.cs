﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Areas.KinderGartenReportArea.Models;
using SMAS.Web.Areas.KinderGartenReportArea;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.Web.Mvc;
using System.Configuration;
using SMAS.Web.Utils;
using System.IO;

namespace SMAS.Web.Areas.KinderGartenReportArea.Controllers
{
    public class KinderGartenReportController : ThreadController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IInputStatisticsBusiness InputStatisticsBusiness;
        public KinderGartenReportController(ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness
            , IDistrictBusiness DistrictBusiness
            , IInputStatisticsBusiness InputStatisticsBusiness)
        {
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.InputStatisticsBusiness = InputStatisticsBusiness;
        }
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        public void SetViewData()
        {
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            AcademicYear AcademicYear = null;
            int curSemester = 1;
            if (_globalInfo.IsSystemAdmin)
            {
                int year = DateTime.Now.Year;
                int firstYear = 2011;
                for (int i = year + 2; i >= firstYear; i--)
                {
                    AcademicYear = new AcademicYear
                    {
                        Year = i - 1,
                        DisplayTitle = String.Format("{0} -{1}", i - 1, i)
                    };
                    lstAcademicYear.Add(AcademicYear);
                }
                if (DateTime.Now.Month <= 8)
                {
                    year = year - 1;
                }
                curSemester = 1;
                ViewData[KinderGartenReportConstants.LS_YEAR] = new SelectList(lstAcademicYear, "Year", "DisplayTitle", year);
            }
            else
            {
                // nam hoc cho phong/so
                List<int> lstYear = AcademicYearBusiness.GetListYearForSupervisingDept_Pro(_globalInfo.SupervisingDeptID.Value).ToList();
                for (int i = 0; i < lstYear.Count(); i++)
                {
                    AcademicYear = new AcademicYear();
                    AcademicYear.Year = lstYear[i];
                    AcademicYear.DisplayTitle = lstYear[i] + "-" + (lstYear[i] + 1);
                    lstAcademicYear.Add(AcademicYear);
                }
                // Lay nam hoc hien tai
                string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
                int curYear = AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(_globalInfo.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);
                // Nam hoc
                ViewData[KinderGartenReportConstants.LS_YEAR] = new SelectList(lstAcademicYear, "Year", "DisplayTitle", curYear);
            }
            List<District> lstDictrict = new List<District>();
            if (!_globalInfo.IsSystemAdmin)
            {
                IQueryable<District> iqtDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == _globalInfo.ProvinceID && o.IsActive == true);
                if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    iqtDistrict = iqtDistrict.Where(o => o.DistrictID == _globalInfo.DistrictID);
                }
                lstDictrict = iqtDistrict.OrderBy(p => p.DistrictName).ToList();
            }
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                ViewData[KinderGartenReportConstants.LS_DISTRICT] = new SelectList(lstDictrict, "DistrictID", "DistrictName", _globalInfo.DistrictID);
            }
            else
            {
                ViewData[KinderGartenReportConstants.LS_DISTRICT] = new SelectList(lstDictrict, "DistrictID", "DistrictName");
            }
        }
        public JsonResult CreatedReport(SearchViewModel data)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["DistrictID"] = data.DistrictID;
            SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            SearchInfo["Year"] = data.AcademicYearID;
            InputStatisticsBusiness.CreateDataKinderGartenReport(SearchInfo);
            return Json(new { Type = "success" });
        }
        public FileResult ExportReport(int academicYearID, int districtID)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            SearchInfo["DistrictID"] = districtID;
            SearchInfo["Year"] = academicYearID;
            SearchInfo["SupervisingDeptName"] = _globalInfo.SuperVisingDeptName;
            Stream excel = InputStatisticsBusiness.ExportKinderGartenReport(SearchInfo);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = "BaoCaoTongHopMN.xls";
            result.FileDownloadName = fileName;
            return result;
        }
    }
}