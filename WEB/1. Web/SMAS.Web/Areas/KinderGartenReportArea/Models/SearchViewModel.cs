﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.KinderGartenReportArea.Models
{
    public class SearchViewModel
    {
        public int DistrictID { get; set; }
        public int AcademicYearID { get; set; }
    }
}