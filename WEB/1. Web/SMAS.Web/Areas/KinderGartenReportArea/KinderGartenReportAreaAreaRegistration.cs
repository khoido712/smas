﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.KinderGartenReportArea
{
    public class KinderGartenReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "KinderGartenReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "KinderGartenReportArea_default",
                "KinderGartenReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
