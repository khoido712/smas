﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.KinderGartenReportArea
{
    public class KinderGartenReportConstants
    {
        public const string LS_YEAR = "LS_YEAR";
        public const string LS_DISTRICT = "LS_DISTRICT";
    }
}