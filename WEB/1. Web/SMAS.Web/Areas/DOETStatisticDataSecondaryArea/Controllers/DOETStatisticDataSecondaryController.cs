﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using System.Configuration;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DOETStatisticDataSecondaryArea.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.DOETStatisticDataSecondaryArea.Controllers
{
    public class DOETStatisticDataSecondaryController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ICapacityStatisticBusiness CapacityStatisticBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IConductStatisticBusiness ConductStatisticBusiness;

        public DOETStatisticDataSecondaryController(IAcademicYearBusiness AcademicYearBusiness,
            IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            ICapacityStatisticBusiness CapacityStatisticBusiness,
            IProvinceBusiness ProvinceBusiness,
            IConductStatisticBusiness ConductStatisticBusiness
            )
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.CapacityStatisticBusiness = CapacityStatisticBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.ConductStatisticBusiness = ConductStatisticBusiness;

        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }
        private void GetViewData()
        {
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                ViewData[DOETStatisticDataSecondaryConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() {
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Capacity"), Value=1, cchecked=true, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Capcity_Education_Level"), Value=2, cchecked=false, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Capcity_District"), Value=3, cchecked=false, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Conduct"), Value=4, cchecked=false, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Conduct_Education_Level"), Value=5, cchecked=false, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Conduct_District"), Value=6, cchecked=false, disabled= false}
                                                };
            }
            else
            {
                ViewData[DOETStatisticDataSecondaryConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() {
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Capacity"), Value=1, cchecked=true, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Capcity_Education_Level"), Value=2, cchecked=false, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Conduct"), Value=4, cchecked=false, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Lbl_Statistic_Conduct_Education_Level"), Value=5, cchecked=false, disabled= false}
                                                };
            }

            IDictionary<string, object> DistrictsearchInfo = new Dictionary<string, object>();
            List<int> lstAca = this.AcademicYearBusiness.GetListYearForSupervisingDept_THCS(_globalInfo.SupervisingDeptID.Value); ;
            int grade = SystemParamsInFile.EDUCATION_GRADE_SECONDARY; ;

            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            for (int i = 0; i < lstAca.Count(); i++)
            {
                AcademicYear AcademicYear = new AcademicYear();
                AcademicYear.Year = (int)lstAca[i];
                AcademicYear.DisplayTitle = lstAca[i] + "-" + (lstAca[i] + 1);
                lstAcademicYear.Add(AcademicYear);
            }
            List<AcademicYear> ListAcademicYear = lstAcademicYear.ToList();

            // Lay nam hoc hien tai
            string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
            int curYear = AcademicYearOfProvinceBusiness.GetCurrentYear(_globalInfo.ProvinceID.GetValueOrDefault(), stringFirstStartDate);
            ViewData[DOETStatisticDataSecondaryConstants.LIST_AcademicYear] = new SelectList(ListAcademicYear, "Year", "DisplayTitle", curYear);
            //lay hoc ky hien tai
            int semester = 0;
            AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(_globalInfo.ProvinceID.Value, out semester);
            ViewData[DOETStatisticDataSecondaryConstants.CURRENT_SEMESTER] = semester;
            ViewData[DOETStatisticDataSecondaryConstants.LIST_Semester] = new SelectList(CommonList.SemesterAndAll(), "key", "value");

            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(o => o.Grade == grade).ToList();
            ViewData[DOETStatisticDataSecondaryConstants.LIST_EducationLevel] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");
        }

        public JsonResult GeneralReport(SearchViewModel data)
        {
            try
            {
                CapacityStatisticRequestBO dataObj = new CapacityStatisticRequestBO();
                dataObj.AppliedLevelID = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                dataObj.EducationLevelID = data.EducationLevelID.HasValue ? data.EducationLevelID.Value : 0;
                dataObj.reportType = data.ReportType;
                dataObj.SemesterID = data.Semester;
                dataObj.SupervisingDeptID = _globalInfo.SupervisingDeptID.HasValue ? _globalInfo.SupervisingDeptID.Value : 0;
                dataObj.YearID = data.Year;
                dataObj.IsSupervisingDept = _globalInfo.IsSuperVisingDeptRole;
                dataObj.ProvinceID = _globalInfo.ProvinceID.HasValue ? _globalInfo.ProvinceID.Value : 0;
                dataObj.IsEthnic = data.IsEthnic;
                dataObj.IsFemale = data.IsGenre;
                dataObj.IsEthnicFemale = data.IsGenreEthnic;
                if (!_globalInfo.IsSuperVisingDeptRole)
                {
                    dataObj.DistrictID = _globalInfo.DistrictID.HasValue ? _globalInfo.DistrictID.Value : 0;
                }
                CapacityStatisticBusiness.GeneralData(dataObj);
                return Json(new { Type = "success" });
            }
            catch (Exception ex)
            {
                string para = "AcademicYearID=" + data.AcademicYearID;
                para += "DistrictID=" + data.DistrictID;
                para += "EducationLevelID=" + data.EducationLevelID;
                LogExtensions.ErrorExt(logger, DateTime.Now, "GeneralReport", para, ex);
                return Json(new { Type = "error" });
            }
        }

        public FileResult ExportExcel(SearchViewModel data)
        {
            string templateName = this.GetTemplate(data.ReportType);
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", templateName);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Fill du lieu chung
            Province provinceObj = ProvinceBusiness.Find(_globalInfo.ProvinceID.Value);
            DateTime currentDate = DateTime.Now;

            List<CapacityStatisticsBO> listData = new List<CapacityStatisticsBO>();
            List<CapacityStatisticsBO> listDataTmp = new List<CapacityStatisticsBO>();
            List<ConductStatisticsBO> listDataConduct = new List<ConductStatisticsBO>();
            List<ConductStatisticsBO> listDataConductTmp = new List<ConductStatisticsBO>();
            List<SchoolProfileBO> listSchoolProfile = CapacityStatisticBusiness.GetListSchool(_globalInfo.IsSuperVisingDeptRole, _globalInfo.ProvinceID.Value, _globalInfo.DistrictID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY).ToList();
            //Neu la SGD
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                if (data.ReportType == SystemParamsInFile.CAPACITY_STATISTIC_ID)
                {
                    #region Thong ke hoc luc

                    listData = CapacityStatisticBusiness.GetListCapacityAllSchool(SystemParamsInFile.CAPACITY_STATISTIC_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);
                    sheetTemplate.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
                    sheetTemplate.SetCellValue("H4", provinceObj.ProvinceName + ", ngày " + currentDate.Day + " tháng " + currentDate.Month + " năm " + currentDate.Year);
                    sheetTemplate.SetCellValue("C7", "[Tất cả] - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));


                    #region Truong hop tat ca
                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                    listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillCapacityDataAllSchoolSGD(sheetData, sheetTemplate, listDataTmp, data, false, false, false, listSchoolProfile);
                    #endregion end tât cả

                    #region Truong hop chon hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillCapacityDataAllSchoolSGD(sheetData, sheetTemplate, listDataTmp, data, false, true, false, listSchoolProfile);
                    }
                    #endregion end hs nu

                    #region Truong hop chon hoc sinh nu dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillCapacityDataAllSchoolSGD(sheetData, sheetTemplate, listDataTmp, data, true, false, false, listSchoolProfile);
                    }
                    #endregion end hs dan toc

                    #region Truong hop chon hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillCapacityDataAllSchoolSGD(sheetData, sheetTemplate, listDataTmp, data, false, false, true, listSchoolProfile);
                    }
                    #endregion end hs nu dan toc

                    sheetTemplate.Delete();
                    #endregion end thong ke hoc luc
                }
                if (data.ReportType == SystemParamsInFile.CAPACITY_STATISTIC_EDUCATION_LEVEL_ID)
                {
                    #region Thong ke hoc luc theo khoi
                    listData = CapacityStatisticBusiness.GetListCapacityAllEdu(SystemParamsInFile.CAPACITY_STATISTIC_EDUCATION_LEVEL_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);//sheet mau cap 3

                    #region TH Tat ca

                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                    listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillCapacityDataByEdcuationLevelSGD_PGD(sheetData, listDataTmp, data, false, false, false);

                    #endregion

                    #region TH hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillCapacityDataByEdcuationLevelSGD_PGD(sheetData, listDataTmp, data, false, true, false);
                    }
                    #endregion

                    #region TH hoc sinh dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillCapacityDataByEdcuationLevelSGD_PGD(sheetData, listDataTmp, data, true, false, false);
                    }
                    #endregion

                    #region TH hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillCapacityDataByEdcuationLevelSGD_PGD(sheetData, listDataTmp, data, false, false, true);
                    }
                    #endregion
                    //xoa sheet mau
                    sheetTemplate.Delete();
                    oBook.GetSheet(1).Delete();

                    #endregion end thong ke hoc luc theo khoi
                }
                if (data.ReportType == SystemParamsInFile.CAPACITY_STATISTIC_DISTRICT_ID)
                {
                    #region Thong ke hoc luc theo quan huyen

                    listData = CapacityStatisticBusiness.GetListCapacityDistrict(SystemParamsInFile.CAPACITY_STATISTIC_DISTRICT_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);//sheet mau cap 3

                    #region TH Tat ca

                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, "O11");
                    listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillCapacityDataByDistrictSGD(sheetData, sheetTemplate, listDataTmp, data, false, false, false);

                    #endregion

                    #region TH hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "O11");
                        listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillCapacityDataByDistrictSGD(sheetData, sheetTemplate, listDataTmp, data, false, true, false);
                    }
                    #endregion

                    #region TH hoc sinh dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "O11");
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillCapacityDataByDistrictSGD(sheetData, sheetTemplate, listDataTmp, data, true, false, false);
                    }
                    #endregion

                    #region TH hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "O11");
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillCapacityDataByDistrictSGD(sheetData, sheetTemplate, listDataTmp, data, false, false, true);
                    }
                    #endregion

                    sheetTemplate.Delete();
                    #endregion
                }
                if (data.ReportType == SystemParamsInFile.CONDUCT_STATISTIC_ID)
                {
                    #region Thong ke hanh kiem

                    listDataConduct = ConductStatisticBusiness.GetListConductAllSchool(SystemParamsInFile.CONDUCT_STATISTIC_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);
                    sheetTemplate.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
                    sheetTemplate.SetCellValue("H4", provinceObj.ProvinceName + ", ngày " + currentDate.Day + " tháng " + currentDate.Month + " năm " + currentDate.Year);
                    sheetTemplate.SetCellValue("C7", "[Tất cả] - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));


                    #region Truong hop tat ca
                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                    listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillConductDataAllSchoolSGD(sheetData, sheetTemplate, listDataConductTmp, data, false, false, false, listSchoolProfile);
                    #endregion end tât cả

                    #region Truong hop chon hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillConductDataAllSchoolSGD(sheetData, sheetTemplate, listDataConductTmp, data, false, true, false, listSchoolProfile);
                    }
                    #endregion end hs nu

                    #region Truong hop chon hoc sinh nu dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillConductDataAllSchoolSGD(sheetData, sheetTemplate, listDataConductTmp, data, true, false, false, listSchoolProfile);
                    }
                    #endregion end hs dan toc

                    #region Truong hop chon hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillConductDataAllSchoolSGD(sheetData, sheetTemplate, listDataConductTmp, data, false, false, true, listSchoolProfile);
                    }
                    #endregion end hs nu dan toc

                    sheetTemplate.Delete();
                    #endregion
                }
                if (data.ReportType == SystemParamsInFile.CONDUCT_STATISTIC_EDUCATION_LEVEL_ID)
                {
                    #region Thong ke hanh kiem theo khoi
                    listDataConduct = ConductStatisticBusiness.GetListConductAllEdu(SystemParamsInFile.CONDUCT_STATISTIC_EDUCATION_LEVEL_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);//sheet mau cap 2

                    #region TH Tat ca

                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                    listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillConductDataByEdcuationLevelSGD_PGD(sheetData, listDataConductTmp, data, false, false, false);

                    #endregion

                    #region TH hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillConductDataByEdcuationLevelSGD_PGD(sheetData, listDataConductTmp, data, false, true, false);
                    }
                    #endregion

                    #region TH hoc sinh dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillConductDataByEdcuationLevelSGD_PGD(sheetData, listDataConductTmp, data, true, false, false);
                    }
                    #endregion

                    #region TH hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillConductDataByEdcuationLevelSGD_PGD(sheetData, listDataConductTmp, data, false, false, true);
                    }
                    #endregion
                    //xoa sheet mau
                    sheetTemplate.Delete();
                    oBook.GetSheet(1).Delete();
                    #endregion
                }
                if (data.ReportType == SystemParamsInFile.CONDUCT_STATISTIC_DISTRICT_ID)
                {
                    #region Thong ke hanh kiem theo quan huyen
                    listDataConduct = ConductStatisticBusiness.GetListConductDistrict(SystemParamsInFile.CONDUCT_STATISTIC_DISTRICT_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);//sheet mau cap 2
                    sheetTemplate.SetCellValue("A7", "CẤP TRUNG HỌC CƠ SỞ");
                    #region TH Tat ca

                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, "O11");
                    listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillConductDataByDistrictSGD(sheetData, sheetTemplate, listDataConductTmp, data, false, false, false);

                    #endregion

                    #region TH hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "O11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillConductDataByDistrictSGD(sheetData, sheetTemplate, listDataConductTmp, data, false, true, false);
                    }
                    #endregion

                    #region TH hoc sinh dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "O11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillConductDataByDistrictSGD(sheetData, sheetTemplate, listDataConductTmp, data, true, false, false);
                    }
                    #endregion

                    #region TH hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "O11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillConductDataByDistrictSGD(sheetData, sheetTemplate, listDataConductTmp, data, false, false, true);
                    }
                    #endregion

                    sheetTemplate.Delete();
                    #endregion
                }
            }
            else //Neu la phong (phong giao duc khong thong ke theo quan huyen)
            {
                if (data.ReportType == SystemParamsInFile.CAPACITY_STATISTIC_ID)
                {
                    #region Thong ke hoc luc
                    listData = CapacityStatisticBusiness.GetListCapacityAllSchool(SystemParamsInFile.CAPACITY_STATISTIC_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);
                    sheetTemplate.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
                    sheetTemplate.SetCellValue("H4", provinceObj.ProvinceName + ", ngày " + currentDate.Day + " tháng " + currentDate.Month + " năm " + currentDate.Year);
                    sheetTemplate.SetCellValue("C7", "[Tất cả] - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));

                    #region Truong hop tat ca
                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                    listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillCapacityDataAllSchoolPGD(sheetData, sheetTemplate, listDataTmp, data, false, false, false, listSchoolProfile);
                    #endregion end tât cả

                    #region Truong hop chon hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillCapacityDataAllSchoolPGD(sheetData, sheetTemplate, listDataTmp, data, false, true, false, listSchoolProfile);
                    }
                    #endregion end hs nu

                    #region Truong hop chon hoc sinh nu dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillCapacityDataAllSchoolPGD(sheetData, sheetTemplate, listDataTmp, data, true, false, false, listSchoolProfile);
                    }
                    #endregion end hs dan toc

                    #region Truong hop chon hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillCapacityDataAllSchoolPGD(sheetData, sheetTemplate, listDataTmp, data, false, false, true, listSchoolProfile);
                    }
                    #endregion end hs nu dan toc
                    sheetTemplate.Delete();

                    #endregion end thong ke hoc luc
                }
                if (data.ReportType == SystemParamsInFile.CAPACITY_STATISTIC_EDUCATION_LEVEL_ID)
                {
                    #region Thong ke hoc luc theo khoi
                    listData = CapacityStatisticBusiness.GetListCapacityAllEdu(SystemParamsInFile.CAPACITY_STATISTIC_EDUCATION_LEVEL_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);//sheet mau cap 3

                    #region TH Tat ca

                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                    listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillCapacityDataByEdcuationLevelSGD_PGD(sheetData, listDataTmp, data, false, false, false);

                    #endregion

                    #region TH hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataTmp = listData.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillCapacityDataByEdcuationLevelSGD_PGD(sheetData, listDataTmp, data, false, true, false);
                    }
                    #endregion

                    #region TH hoc sinh dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillCapacityDataByEdcuationLevelSGD_PGD(sheetData, listDataTmp, data, true, false, false);
                    }
                    #endregion

                    #region TH hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataTmp = listData.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillCapacityDataByEdcuationLevelSGD_PGD(sheetData, listDataTmp, data, false, false, true);
                    }
                    #endregion
                    //xoa sheet mau
                    sheetTemplate.Delete();
                    oBook.GetSheet(1).Delete();

                    #endregion end thong ke hoc luc theo khoi
                }
                if (data.ReportType == SystemParamsInFile.CONDUCT_STATISTIC_ID)
                {
                    #region Thong ke hanh kiem
                    listDataConduct = ConductStatisticBusiness.GetListConductAllSchool(SystemParamsInFile.CONDUCT_STATISTIC_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);
                    sheetTemplate.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
                    sheetTemplate.SetCellValue("H4", provinceObj.ProvinceName + ", ngày " + currentDate.Day + " tháng " + currentDate.Month + " năm " + currentDate.Year);
                    sheetTemplate.SetCellValue("C7", "[Tất cả] - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));


                    #region Truong hop tat ca
                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                    listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillConductDataAllSchoolPGD(sheetData, sheetTemplate, listDataConductTmp, data, false, false, false, listSchoolProfile);
                    #endregion end tât cả

                    #region Truong hop chon hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillConductDataAllSchoolPGD(sheetData, sheetTemplate, listDataConductTmp, data, false, true, false, listSchoolProfile);
                    }
                    #endregion end hs nu

                    #region Truong hop chon hoc sinh nu dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillConductDataAllSchoolPGD(sheetData, sheetTemplate, listDataConductTmp, data, true, false, false, listSchoolProfile);
                    }
                    #endregion end hs dan toc

                    #region Truong hop chon hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, "P11");
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillConductDataAllSchoolPGD(sheetData, sheetTemplate, listDataConductTmp, data, false, false, true, listSchoolProfile);
                    }
                    #endregion end hs nu dan toc

                    sheetTemplate.Delete();
                    #endregion
                }
                if (data.ReportType == SystemParamsInFile.CONDUCT_STATISTIC_EDUCATION_LEVEL_ID)
                {
                    #region Thong ke hanh kiem theo khoi
                    listDataConduct = ConductStatisticBusiness.GetListConductAllEdu(SystemParamsInFile.CONDUCT_STATISTIC_EDUCATION_LEVEL_CODE, data.AcademicYearID, data.Semester, data.EducationLevelID.Value, _globalInfo.ProvinceID.Value, _globalInfo.SupervisingDeptID.Value, SystemParamsInFile.APPLIED_LEVEL_SECONDARY, _globalInfo.IsSuperVisingDeptRole, _globalInfo.DistrictID.Value);
                    IVTWorksheet sheetTemplate = oBook.GetSheet(1);//sheet mau cap 2

                    #region TH Tat ca

                    IVTWorksheet sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                    listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == null).ToList();
                    this.FillConductDataByEdcuationLevelSGD_PGD(sheetData, listDataConductTmp, data, false, false, false);

                    #endregion

                    #region TH hoc sinh nu
                    if (data.IsGenre)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == null && p.IsFemale == true).ToList();
                        this.FillConductDataByEdcuationLevelSGD_PGD(sheetData, listDataConductTmp, data, false, true, false);
                    }
                    #endregion

                    #region TH hoc sinh dan toc
                    if (data.IsEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == null).ToList();
                        this.FillConductDataByEdcuationLevelSGD_PGD(sheetData, listDataConductTmp, data, true, false, false);
                    }
                    #endregion

                    #region TH hoc sinh nu dan toc
                    if (data.IsGenreEthnic)
                    {
                        sheetData = oBook.CopySheetToLast(sheetTemplate, null);
                        listDataConductTmp = listDataConduct.Where(p => p.IsEthnic == true && p.IsFemale == true).ToList();
                        this.FillConductDataByEdcuationLevelSGD_PGD(sheetData, listDataConductTmp, data, false, false, true);
                    }
                    #endregion
                    //xoa sheet mau
                    sheetTemplate.Delete();
                    oBook.GetSheet(1).Delete();
                    #endregion
                }
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = this.GetNameTemplate(data.EducationLevelID, data.Semester, data.ReportType);
            return result;
        }

        #region Cac ham thong ke hoc luc (Capacity)
        private void FillCapacityDataAllSchoolSGD(IVTWorksheet sheetData, IVTWorksheet sheetTemplate,
            List<CapacityStatisticsBO> listData, SearchViewModel data, bool isEthnic, bool isFemale, bool isEF, List<SchoolProfileBO> listSchoolProfile)
        {
            int startRow = 10;
            int startCol = 1;
            int STT = 1;
            sheetData.SetCellValue(startRow, startCol, "Toàn Sở");
            List<CapacityStatisticsBO> listDataOfSchoolByDistrict = new List<CapacityStatisticsBO>();
            CapacityStatisticsBO objDataBySchool = null;
            IVTRange rangeDot = sheetTemplate.GetRange("A13", "P13");
            IVTRange rangeSolid = sheetTemplate.GetRange("A16", "P16");
            IVTRange rangeSolidDistrict = sheetTemplate.GetRange("A11", "P11");
            startRow++;
            //Danh sach cac quan huyen co thong ke
            var listDictrict = (from a in listSchoolProfile
                                group a by new { a.DistrictName, a.DistrictID } into g
                                select new
                                {
                                    DistrictID = g.Key.DistrictID.HasValue ? g.Key.DistrictID.Value : 0,
                                    DistrictName = g.Key.DistrictName,
                                    TotalSchoool = g.Count()
                                }).OrderBy(p => p.DistrictID).ToList();
            //tong so toan so
            string fomulaTotal = "=SUM()";
            string fomulaTotalGioi = "=SUM()";
            string fomulaTotalKha = "=SUM()";
            string fomulaTotalTB = "=SUM()";
            string fomulaTotalYeu = "=SUM()";
            string fomulaTotalKem = "=SUM()";
            string fomulaTotalTrenTB = "=SUM()";
            //Lay danh sach truong
            List<SchoolProfileBO> listSchoolProfileTmp = new List<SchoolProfileBO>();
            int countSchool = 0;

            for (int i = 0; i < listDictrict.Count; i++)
            {
                listDataOfSchoolByDistrict = listData.Where(p => p.DistrictID == listDictrict[i].DistrictID).ToList();
                listSchoolProfileTmp = listSchoolProfile.Where(p => p.DistrictID == listDictrict[i].DistrictID).ToList();
                countSchool = listSchoolProfileTmp.Count;

                if (i > 0)
                {
                    sheetData.CopyPasteSameSize(rangeSolidDistrict, startRow, 1);
                }
                sheetData.SetCellValue(startRow, startCol, "Toàn " + listDictrict[i].DistrictName);

                //tong so toan so
                if (i == 0)
                {
                    fomulaTotal = fomulaTotal.Replace(")", "D" + startRow + ")");
                    fomulaTotalGioi = fomulaTotalGioi.Replace(")", "E" + startRow + ")");
                    fomulaTotalKha = fomulaTotalKha.Replace(")", "G" + startRow + ")");
                    fomulaTotalTB = fomulaTotalTB.Replace(")", "I" + startRow + ")");
                    fomulaTotalYeu = fomulaTotalYeu.Replace(")", "K" + startRow + ")");
                    fomulaTotalKem = fomulaTotalKem.Replace(")", "M" + startRow + ")");
                    fomulaTotalTrenTB = fomulaTotalTrenTB.Replace(")", "O" + startRow + ")");
                }
                else if (i > 0)
                {
                    fomulaTotal = fomulaTotal.Replace(")", ",D" + startRow + ")");
                    fomulaTotalGioi = fomulaTotalGioi.Replace(")", ",E" + startRow + ")");
                    fomulaTotalKha = fomulaTotalKha.Replace(")", ",G" + startRow + ")");
                    fomulaTotalTB = fomulaTotalTB.Replace(")", ",I" + startRow + ")");
                    fomulaTotalYeu = fomulaTotalYeu.Replace(")", ",K" + startRow + ")");
                    fomulaTotalKem = fomulaTotalKem.Replace(")", ",M" + startRow + ")");
                    fomulaTotalTrenTB = fomulaTotalTrenTB.Replace(")", ",O" + startRow + ")");
                }


                //tong so
                sheetData.SetFormulaValue(startRow, startCol + 3, "=SUM(D" + (startRow + 1) + ":D" + (countSchool + startRow) + ")");
                //gioi
                sheetData.SetFormulaValue(startRow, startCol + 4, "=SUM(E" + (startRow + 1) + ":E" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 5, "=IF(E" + startRow + ">0,ROUND((E" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //kha
                sheetData.SetFormulaValue(startRow, startCol + 6, "=SUM(G" + (startRow + 1) + ":G" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 7, "=IF(G" + startRow + ">0,ROUND((G" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //tb
                sheetData.SetFormulaValue(startRow, startCol + 8, "=SUM(I" + (startRow + 1) + ":I" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 9, "=IF(I" + startRow + ">0,ROUND((I" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //yeu
                sheetData.SetFormulaValue(startRow, startCol + 10, "=SUM(K" + (startRow + 1) + ":K" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 11, "=IF(K" + startRow + ">0,ROUND((K" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //kem
                sheetData.SetFormulaValue(startRow, startCol + 12, "=SUM(M" + (startRow + 1) + ":M" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 13, "=IF(M" + startRow + ">0,ROUND((M" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //tren tb
                sheetData.SetFormulaValue(startRow, startCol + 14, "=SUM(O" + (startRow + 1) + ":O" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 15, "=IF(O" + startRow + ">0,ROUND((O" + startRow + "/D" + startRow + ")*100,2),\"0\")");

                startRow++;

                for (int k = 0; k < countSchool; k++)
                {
                    //ke khung
                    if ((k + 1) % 5 == 0 || (k + 1) == countSchool)
                    {
                        sheetData.CopyPasteSameSize(rangeSolid, startRow, 1);
                    }
                    else
                    {
                        sheetData.CopyPasteSameSize(rangeDot, startRow, 1);
                    }

                    objDataBySchool = listDataOfSchoolByDistrict.Where(p => p.SchoolID == listSchoolProfileTmp[k].SchoolProfileID).FirstOrDefault();
                    sheetData.SetCellValue(startRow, startCol, STT);
                    sheetData.SetCellValue(startRow, startCol + 1, listDictrict[i].DistrictName);
                    sheetData.SetCellValue(startRow, startCol + 2, listSchoolProfileTmp[k].SchoolName);
                    sheetData.SetCellValue(startRow, startCol + 3, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.PupilTotal) : 0);
                    //gioi
                    sheetData.SetCellValue(startRow, startCol + 4, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel01) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 5, "=IF(D" + startRow + ">0,ROUND((E" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //kha
                    sheetData.SetCellValue(startRow, startCol + 6, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel02) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 7, "=IF(D" + startRow + ">0,ROUND((G" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //tb
                    sheetData.SetCellValue(startRow, startCol + 8, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel03) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 9, "=IF(D" + startRow + ">0,ROUND((I" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //yeu
                    sheetData.SetCellValue(startRow, startCol + 10, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel04) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 11, "=IF(D" + startRow + ">0,ROUND((K" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //kem
                    sheetData.SetCellValue(startRow, startCol + 12, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel05) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 13, "=IF(D" + startRow + ">0,ROUND((M" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //TB tro len
                    sheetData.SetCellValue(startRow, startCol + 14, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.OnAverage) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 15, "=IF(D" + startRow + ">0,ROUND((O" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    startRow++;
                    STT++;
                }
            }

            //Fill cong thuc tong so toan so
            sheetData.SetFormulaValue("D10", fomulaTotal);
            sheetData.SetFormulaValue("E10", fomulaTotalGioi);
            sheetData.SetFormulaValue("G10", fomulaTotalKha);
            sheetData.SetFormulaValue("I10", fomulaTotalTB);
            sheetData.SetFormulaValue("K10", fomulaTotalYeu);
            sheetData.SetFormulaValue("M10", fomulaTotalKem);
            sheetData.SetFormulaValue("O10", fomulaTotalTrenTB);

            if (isFemale)
            {
                sheetData.Name = "HS_Nu";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HỌC LỰC HỌC SINH NỮ CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh nữ - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else if (isEthnic)
            {
                sheetData.Name = "HS_DT";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HỌC LỰC HỌC SINH DÂN TỘC CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh dân tộc - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else if (isEF)
            {
                sheetData.Name = "HS_Nu_DT";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HỌC LỰC HỌC SINH NỮ DÂN TỘC CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh nữ dân tộc - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else
            {
                sheetData.Name = "Tat_ca";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HỌC LỰC HỌC SINH CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "[Tất cả] - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
        }

        private void FillCapacityDataAllSchoolPGD(IVTWorksheet sheetData, IVTWorksheet sheetTemplate,
              List<CapacityStatisticsBO> listData, SearchViewModel data, bool isEthnic, bool isFemale, bool isEF, List<SchoolProfileBO> listSchoolProfile)
        {
            int startRow = 10;
            int startCol = 1;
            int STT = 1;
            List<CapacityStatisticsBO> listDataOfSchoolByDistrict = new List<CapacityStatisticsBO>();
            List<SchoolProfileBO> listSchoolProfileTmp = new List<SchoolProfileBO>();
            CapacityStatisticsBO objDataBySchool = null;
            IVTRange rangeDot = sheetTemplate.GetRange("A13", "P13");
            IVTRange rangeSolid = sheetTemplate.GetRange("A15", "P15");

            //Danh sach cac quan huyen thong ke
            var listDictrict = (from a in listData
                                group a by new { a.DistrictName, a.DistrictID } into g
                                select new
                                {
                                    DistrictID = g.Key.DistrictID.HasValue ? g.Key.DistrictID.Value : 0,
                                    DistrictName = g.Key.DistrictName,
                                    TotalSchoool = g.Count()
                                }).OrderBy(p => p.DistrictID).ToList();
            int countschool = 0;
            for (int i = 0; i < listDictrict.Count; i++)
            {
                listDataOfSchoolByDistrict = listData.Where(p => p.DistrictID == listDictrict[i].DistrictID).ToList();
                listSchoolProfileTmp = listSchoolProfile.Where(p => p.DistrictID == listDictrict[i].DistrictID).ToList();
                countschool = listSchoolProfileTmp.Count;

                sheetData.SetCellValue(startRow, startCol, "Toàn " + listDictrict[i].DistrictName);

                //tong so
                sheetData.SetFormulaValue(startRow, startCol + 3, "=SUM(D" + (startRow + 1) + ":D" + (countschool + startRow) + ")");
                //gioi
                sheetData.SetFormulaValue(startRow, startCol + 4, "=SUM(E" + (startRow + 1) + ":E" + (countschool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 5, "=IF(E" + startRow + ">0,ROUND((E" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //kha
                sheetData.SetFormulaValue(startRow, startCol + 6, "=SUM(G" + (startRow + 1) + ":G" + (countschool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 7, "=IF(G" + startRow + ">0,ROUND((G" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //tb
                sheetData.SetFormulaValue(startRow, startCol + 8, "=SUM(I" + (startRow + 1) + ":I" + (countschool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 9, "=IF(I" + startRow + ">0,ROUND((I" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //yeu
                sheetData.SetFormulaValue(startRow, startCol + 10, "=SUM(K" + (startRow + 1) + ":K" + (countschool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 11, "=IF(K" + startRow + ">0,ROUND((K" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //kem
                sheetData.SetFormulaValue(startRow, startCol + 12, "=SUM(M" + (startRow + 1) + ":M" + (countschool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 13, "=IF(M" + startRow + ">0,ROUND((M" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //tren tb
                sheetData.SetFormulaValue(startRow, startCol + 14, "=SUM(O" + (startRow + 1) + ":O" + (countschool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 15, "=IF(O" + startRow + ">0,ROUND((O" + startRow + "/D" + startRow + ")*100,2),\"0\")");

                startRow++;

                for (int j = 0; j < countschool; j++)
                {
                    //ke khung
                    if ((j + 1) % 5 == 0 || (j + 1) == countschool)
                    {
                        sheetData.CopyPasteSameSize(rangeSolid, startRow, 1);
                    }
                    else
                    {
                        sheetData.CopyPasteSameSize(rangeDot, startRow, 1);
                    }

                    objDataBySchool = listDataOfSchoolByDistrict.Where(p => p.SchoolID == listSchoolProfileTmp[j].SchoolProfileID).FirstOrDefault();
                    sheetData.SetCellValue(startRow, startCol, STT);
                    sheetData.SetCellValue(startRow, startCol + 1, listDictrict[i].DistrictName);
                    sheetData.SetCellValue(startRow, startCol + 2, listSchoolProfileTmp[j].SchoolName);
                    sheetData.SetCellValue(startRow, startCol + 3, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.PupilTotal) : 0);
                    //gioi
                    sheetData.SetCellValue(startRow, startCol + 4, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel01) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 5, "=IF(D" + startRow + ">0,ROUND((E" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //kha
                    sheetData.SetCellValue(startRow, startCol + 6, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel02) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 7, "=IF(D" + startRow + ">0,ROUND((G" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //tb
                    sheetData.SetCellValue(startRow, startCol + 8, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel03) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 9, "=IF(D" + startRow + ">0,ROUND((I" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //yeu
                    sheetData.SetCellValue(startRow, startCol + 10, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel04) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 11, "=IF(D" + startRow + ">0,ROUND((K" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //kem
                    sheetData.SetCellValue(startRow, startCol + 12, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.CapacityLevel05) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 13, "=IF(D" + startRow + ">0,ROUND((M" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //TB tro len
                    sheetData.SetCellValue(startRow, startCol + 14, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.OnAverage) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 15, "=IF(D" + startRow + ">0,ROUND((O" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    startRow++;
                    STT++;
                }
            }

            if (isFemale)
            {
                sheetData.Name = "HS_Nu";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HỌC LỰC HỌC SINH NỮ CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh nữ - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else if (isEthnic)
            {
                sheetData.Name = "HS_DT";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HỌC LỰC HỌC SINH DÂN TỘC CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh dân tộc - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else if (isEF)
            {
                sheetData.Name = "HS_Nu_DT";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HỌC LỰC HỌC SINH NỮ DÂN TỘC CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh nữ dân tộc - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else
            {
                sheetData.Name = "Tat_ca";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HỌC LỰC HỌC SINH CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "[Tất cả] - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
        }

        private void FillCapacityDataByEdcuationLevelSGD_PGD(IVTWorksheet sheetData, List<CapacityStatisticsBO> listData,
            SearchViewModel data, bool isEthnic, bool isFemale, bool isEF)
        {
            List<CapacityStatisticsBO> listDataByEducation = new List<CapacityStatisticsBO>();
            CapacityStatisticsBO objTmp = null;
            int startRow = 11;
            int startCol = 2;
            int minEducation = 6;
            int maxEducation = 9;

            //Fill du lieu chung
            Province provinceObj = ProvinceBusiness.Find(_globalInfo.ProvinceID.Value);
            DateTime currentDate = DateTime.Now;
            sheetData.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
            sheetData.SetCellValue("I4", provinceObj.ProvinceName + ", ngày " + currentDate.Day + " tháng " + currentDate.Month + " năm " + currentDate.Year);
            sheetData.SetCellValue("A7", Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            ////So chi fill cho cap 3
            for (int i = minEducation; i <= maxEducation; i++)
            {
                listDataByEducation = listData.Where(p => p.EducationLevelID == i).ToList();
                for (int j = 0; j < listDataByEducation.Count; j++)
                {
                    objTmp = listDataByEducation[j];
                    sheetData.SetCellValue(startRow, startCol, Utils.Utils.SetValueDefault(objTmp.PupilTotal));
                    //Gioi
                    sheetData.SetCellValue(startRow, startCol + 1, Utils.Utils.SetValueDefault(objTmp.CapacityLevel01));
                    //Kha
                    sheetData.SetCellValue(startRow, startCol + 3, Utils.Utils.SetValueDefault(objTmp.CapacityLevel02));
                    //TB
                    sheetData.SetCellValue(startRow, startCol + 5, Utils.Utils.SetValueDefault(objTmp.CapacityLevel03));
                    //Yeu
                    sheetData.SetCellValue(startRow, startCol + 7, Utils.Utils.SetValueDefault(objTmp.CapacityLevel04));
                    //Kem
                    sheetData.SetCellValue(startRow, startCol + 9, Utils.Utils.SetValueDefault(objTmp.CapacityLevel05));
                    //Tren TB
                    sheetData.SetCellValue(startRow, startCol + 11, Utils.Utils.SetValueDefault(objTmp.OnAverage));

                }
                startRow++;
            }
            if (isFemale)
            {
                sheetData.Name = "HS_Nu";
            }
            else if (isEthnic)
            {
                sheetData.Name = "HS_DT";
            }
            else if (isEF)
            {
                sheetData.Name = "HS_Nu_DT";
            }
            else
            {
                sheetData.Name = "Tat_ca";
            }
        }

        private void FillCapacityDataByDistrictSGD(IVTWorksheet sheetData, IVTWorksheet sheetTemplate, List<CapacityStatisticsBO> listData,
            SearchViewModel data, bool isEthnic, bool isFemale, bool isEF)
        {
            List<CapacityStatisticsBO> listDataByEducation = new List<CapacityStatisticsBO>();
            CapacityStatisticsBO objTmp = null;
            int startRow = 12;
            int startCol = 1;

            //Fill du lieu chung
            Province provinceObj = ProvinceBusiness.Find(_globalInfo.ProvinceID.Value);
            DateTime currentDate = DateTime.Now;
            sheetData.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
            sheetData.SetCellValue("J4", provinceObj.ProvinceName + ", ngày " + currentDate.Day + " tháng " + currentDate.Month + " năm " + currentDate.Year);
            sheetData.SetCellValue("A7", "CẤP TRUNG HỌC CƠ SỞ");
            sheetData.SetCellValue("A8", Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            if (isEthnic)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HỌC LỰC HỌC SINH DÂN TỘC");
            }
            else if (isFemale)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HỌC LỰC HỌC SINH NỮ");
            }
            else if (isEF)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HỌC LỰC HỌC SINH NỮ DÂN TỘC");
            }
            else
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HỌC LỰC HỌC SINH");
            }

            IVTRange rangeDot = sheetTemplate.GetRange("A13", "P13");
            IVTRange rangeSolid = sheetTemplate.GetRange("A16", "P16");
            IVTRange rangeAllDistrict = sheetTemplate.GetRange("A36", "P36");

            for (int j = 0; j < listData.Count; j++)
            {
                objTmp = listData[j];

                //ve khung
                if ((j + 1) % 5 == 0 || (j + 1) == listData.Count)
                {
                    sheetData.CopyPasteSameSize(rangeSolid, startRow, 1);
                }
                else
                {
                    sheetData.CopyPasteSameSize(rangeDot, startRow, 1);
                }

                sheetData.SetCellValue(startRow, startCol, (j + 1));
                sheetData.SetCellValue(startRow, startCol + 1, objTmp.DistrictName);
                sheetData.SetCellValue(startRow, startCol + 2, Utils.Utils.SetValueDefault(objTmp.PupilTotal));
                //Gioi
                sheetData.SetCellValue(startRow, startCol + 3, Utils.Utils.SetValueDefault(objTmp.CapacityLevel01));
                sheetData.SetFormulaValue(startRow, startCol + 4, "=CONCATENATE(ROUND(D" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Kha
                sheetData.SetCellValue(startRow, startCol + 5, Utils.Utils.SetValueDefault(objTmp.CapacityLevel02));
                sheetData.SetFormulaValue(startRow, startCol + 6, "=CONCATENATE(ROUND(F" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //TB
                sheetData.SetCellValue(startRow, startCol + 7, Utils.Utils.SetValueDefault(objTmp.CapacityLevel03));
                sheetData.SetFormulaValue(startRow, startCol + 8, "=CONCATENATE(ROUND(H" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Yeu
                sheetData.SetCellValue(startRow, startCol + 9, Utils.Utils.SetValueDefault(objTmp.CapacityLevel04));
                sheetData.SetFormulaValue(startRow, startCol + 10, "=CONCATENATE(ROUND(J" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Kem
                sheetData.SetCellValue(startRow, startCol + 11, Utils.Utils.SetValueDefault(objTmp.CapacityLevel05));
                sheetData.SetFormulaValue(startRow, startCol + 12, "=CONCATENATE(ROUND(L" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Tren TB
                sheetData.SetCellValue(startRow, startCol + 13, Utils.Utils.SetValueDefault(objTmp.OnAverage));
                sheetData.SetFormulaValue(startRow, startCol + 14, "=CONCATENATE(ROUND(N" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                startRow++;
            }

            if (listData.Count > 0)
            {
                //VE khung va fill dong Toan tinh/TP
                sheetData.CopyPasteSameSize(rangeAllDistrict, startRow, 1);
                //fill fomular
                //Tong so hoc sinh
                sheetData.SetFormulaValue(startRow, startCol + 2, "=SUM(C12:C" + (startRow - 1) + ")");
                // Hoc luc tot
                sheetData.SetFormulaValue(startRow, startCol + 3, "=SUM(D12:D" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 4, "=CONCATENATE(ROUND(D" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Hoc luc kha
                sheetData.SetFormulaValue(startRow, startCol + 5, "=SUM(F12:F" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 6, "=CONCATENATE(ROUND(F" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Hoc luc TB
                sheetData.SetFormulaValue(startRow, startCol + 7, "=SUM(H12:H" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 8, "=CONCATENATE(ROUND(H" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Hoc luc yeu
                sheetData.SetFormulaValue(startRow, startCol + 9, "=SUM(J12:J" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 10, "=CONCATENATE(ROUND(J" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Hoc luc kem
                sheetData.SetFormulaValue(startRow, startCol + 11, "=SUM(L12:L" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 12, "=CONCATENATE(ROUND(L" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Hoc luc TB tro len
                sheetData.SetFormulaValue(startRow, startCol + 13, "=SUM(N12:N" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 14, "=CONCATENATE(ROUND(N" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
            }
            if (isFemale)
            {
                sheetData.Name = "HS_Nu";
            }
            else if (isEthnic)
            {
                sheetData.Name = "HS_DT";
            }
            else if (isEF)
            {
                sheetData.Name = "HS_Nu_DT";
            }
            else
            {
                sheetData.Name = "Tat_ca";
            }
        }

        #endregion

        #region Cac ham thong ke hanh kiem (conduct)

        private void FillConductDataAllSchoolSGD(IVTWorksheet sheetData, IVTWorksheet sheetTemplate,
            List<ConductStatisticsBO> listData, SearchViewModel data, bool isEthnic, bool isFemale, bool isEF, List<SchoolProfileBO> listSchoolProfile)
        {
            int startRow = 10;
            int startCol = 1;
            int STT = 1;
            sheetData.SetCellValue(startRow, startCol, "Toàn Sở");
            List<ConductStatisticsBO> listDataOfSchoolByDistrict = new List<ConductStatisticsBO>();
            List<SchoolProfileBO> listSchoolProfileTmp = new List<SchoolProfileBO>();
            ConductStatisticsBO objDataBySchool = null;
            IVTRange rangeDot = sheetTemplate.GetRange("A13", "P13");
            IVTRange rangeSolid = sheetTemplate.GetRange("A16", "P16");
            IVTRange rangeSolidDistrict = sheetTemplate.GetRange("A11", "P11");
            startRow++;
            //Danh sach cac quan huyen co thong ke
            var listDictrict = (from a in listSchoolProfile
                                group a by new { a.DistrictName, a.DistrictID } into g
                                select new
                                {
                                    DistrictID = g.Key.DistrictID.HasValue ? g.Key.DistrictID.Value : 0,
                                    DistrictName = g.Key.DistrictName,
                                    TotalSchoool = g.Count()
                                }).OrderBy(p => p.DistrictID).ToList();
            //tong so toan so
            string fomulaTotal = "=SUM()";
            string fomulaTotalGioi = "=SUM()";
            string fomulaTotalKha = "=SUM()";
            string fomulaTotalTB = "=SUM()";
            string fomulaTotalYeu = "=SUM()";
            string fomulaTotalTrenTB = "=SUM()";
            int countSchool = 0;
            for (int i = 0; i < listDictrict.Count; i++)
            {
                listDataOfSchoolByDistrict = listData.Where(p => p.DistrictID == listDictrict[i].DistrictID).ToList();
                listSchoolProfileTmp = listSchoolProfile.Where(p => p.DistrictID == listDictrict[i].DistrictID).ToList();
                countSchool = listSchoolProfileTmp.Count;
                if (i > 0)
                {
                    sheetData.CopyPasteSameSize(rangeSolidDistrict, startRow, 1);
                }
                sheetData.SetCellValue(startRow, startCol, "Toàn " + listDictrict[i].DistrictName);

                //tong so toan so
                if (i == 0)
                {
                    fomulaTotal = fomulaTotal.Replace(")", "D" + startRow + ")");
                    fomulaTotalGioi = fomulaTotalGioi.Replace(")", "E" + startRow + ")");
                    fomulaTotalKha = fomulaTotalKha.Replace(")", "G" + startRow + ")");
                    fomulaTotalTB = fomulaTotalTB.Replace(")", "I" + startRow + ")");
                    fomulaTotalYeu = fomulaTotalYeu.Replace(")", "K" + startRow + ")");
                    fomulaTotalTrenTB = fomulaTotalTrenTB.Replace(")", "M" + startRow + ")");
                }
                else if (i > 0)
                {
                    fomulaTotal = fomulaTotal.Replace(")", ",D" + startRow + ")");
                    fomulaTotalGioi = fomulaTotalGioi.Replace(")", ",E" + startRow + ")");
                    fomulaTotalKha = fomulaTotalKha.Replace(")", ",G" + startRow + ")");
                    fomulaTotalTB = fomulaTotalTB.Replace(")", ",I" + startRow + ")");
                    fomulaTotalYeu = fomulaTotalYeu.Replace(")", ",K" + startRow + ")");
                    fomulaTotalTrenTB = fomulaTotalTrenTB.Replace(")", ",M" + startRow + ")");
                }


                //tong so
                sheetData.SetFormulaValue(startRow, startCol + 3, "=SUM(D" + (startRow + 1) + ":D" + (countSchool + startRow) + ")");
                //gioi
                sheetData.SetFormulaValue(startRow, startCol + 4, "=SUM(E" + (startRow + 1) + ":E" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 5, "=IF(E" + startRow + ">0,ROUND((E" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //kha
                sheetData.SetFormulaValue(startRow, startCol + 6, "=SUM(G" + (startRow + 1) + ":G" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 7, "=IF(G" + startRow + ">0,ROUND((G" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //tb
                sheetData.SetFormulaValue(startRow, startCol + 8, "=SUM(I" + (startRow + 1) + ":I" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 9, "=IF(I" + startRow + ">0,ROUND((I" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //yeu
                sheetData.SetFormulaValue(startRow, startCol + 10, "=SUM(K" + (startRow + 1) + ":K" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 11, "=IF(K" + startRow + ">0,ROUND((K" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //tren tb
                sheetData.SetFormulaValue(startRow, startCol + 12, "=SUM(M" + (startRow + 1) + ":M" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 13, "=IF(M" + startRow + ">0,ROUND((M" + startRow + "/D" + startRow + ")*100,2),\"0\")");

                startRow++;

                for (int j = 0; j < countSchool; j++)
                {
                    //ke khung
                    if ((j + 1) % 5 == 0 || (j + 1) == countSchool)
                    {
                        sheetData.CopyPasteSameSize(rangeSolid, startRow, 1);
                    }
                    else
                    {
                        sheetData.CopyPasteSameSize(rangeDot, startRow, 1);
                    }

                    objDataBySchool = listDataOfSchoolByDistrict.Where(p => p.SchoolID == listSchoolProfileTmp[j].SchoolProfileID).FirstOrDefault();
                    sheetData.SetCellValue(startRow, startCol, STT);
                    sheetData.SetCellValue(startRow, startCol + 1, listDictrict[i].DistrictName);
                    sheetData.SetCellValue(startRow, startCol + 2, listSchoolProfileTmp[j].SchoolName);
                    sheetData.SetCellValue(startRow, startCol + 3, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.PupilTotal) : 0);
                    //gioi
                    sheetData.SetCellValue(startRow, startCol + 4, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.ConductLevel01) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 5, "=IF(D" + startRow + ">0,ROUND((E" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //kha
                    sheetData.SetCellValue(startRow, startCol + 6, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.ConductLevel02) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 7, "=IF(D" + startRow + ">0,ROUND((G" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //tb
                    sheetData.SetCellValue(startRow, startCol + 8, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.ConductLevel03) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 9, "=IF(D" + startRow + ">0,ROUND((I" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //yeu
                    sheetData.SetCellValue(startRow, startCol + 10, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.ConductLevel04) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 11, "=IF(D" + startRow + ">0,ROUND((K" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //TB tro len
                    sheetData.SetCellValue(startRow, startCol + 12, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.OnAverage) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 13, "=IF(D" + startRow + ">0,ROUND((M" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    startRow++;
                    STT++;
                }
            }

            //Fill cong thuc tong so toan so
            sheetData.SetFormulaValue("D10", fomulaTotal);
            sheetData.SetFormulaValue("E10", fomulaTotalGioi);
            sheetData.SetFormulaValue("G10", fomulaTotalKha);
            sheetData.SetFormulaValue("I10", fomulaTotalTB);
            sheetData.SetFormulaValue("K10", fomulaTotalYeu);
            sheetData.SetFormulaValue("M10", fomulaTotalTrenTB);

            if (isFemale)
            {
                sheetData.Name = "HS_Nu";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HẠNH KIỂM HỌC SINH NỮ CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh nữ - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else if (isEthnic)
            {
                sheetData.Name = "HS_DT";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HẠNH KIỂM HỌC SINH DÂN TỘC CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh dân tộc - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else if (isEF)
            {
                sheetData.Name = "HS_Nu_DT";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HẠNH KIỂMC HỌC SINH NỮ DÂN TỘC CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh nữ dân tộc - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else
            {
                sheetData.Name = "Tat_ca";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HẠNH KIỂM HỌC SINH CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "[Tất cả] - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
        }

        private void FillConductDataAllSchoolPGD(IVTWorksheet sheetData, IVTWorksheet sheetTemplate,
              List<ConductStatisticsBO> listData, SearchViewModel data, bool isEthnic, bool isFemale, bool isEF, List<SchoolProfileBO> listSchoolProfile)
        {
            int startRow = 10;
            int startCol = 1;
            int STT = 1;
            List<ConductStatisticsBO> listDataOfSchoolByDistrict = new List<ConductStatisticsBO>();
            List<SchoolProfileBO> listSchoolProfileTmp = new List<SchoolProfileBO>();
            ConductStatisticsBO objDataBySchool = null;
            IVTRange rangeDot = sheetTemplate.GetRange("A13", "P13");
            IVTRange rangeSolid = sheetTemplate.GetRange("A15", "P15");

            //Danh sach cac quan huyen thong ke
            var listDictrict = (from a in listData
                                group a by new { a.DistrictName, a.DistrictID } into g
                                select new
                                {
                                    DistrictID = g.Key.DistrictID.HasValue ? g.Key.DistrictID.Value : 0,
                                    DistrictName = g.Key.DistrictName,
                                    TotalSchoool = g.Count()
                                }).OrderBy(p => p.DistrictID).ToList();
            int countSchool = 0;
            for (int i = 0; i < listDictrict.Count; i++)
            {
                listDataOfSchoolByDistrict = listData.Where(p => p.DistrictID == listDictrict[i].DistrictID).ToList();
                listSchoolProfileTmp = listSchoolProfile.Where(p => p.DistrictID == listDictrict[i].DistrictID).ToList();

                countSchool = listSchoolProfileTmp.Count;

                sheetData.SetCellValue(startRow, startCol, "Toàn " + listDictrict[i].DistrictName);

                //tong so
                sheetData.SetFormulaValue(startRow, startCol + 3, "=SUM(D" + (startRow + 1) + ":D" + (countSchool + startRow) + ")");
                //gioi
                sheetData.SetFormulaValue(startRow, startCol + 4, "=SUM(E" + (startRow + 1) + ":E" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 5, "=IF(E" + startRow + ">0,ROUND((E" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //kha
                sheetData.SetFormulaValue(startRow, startCol + 6, "=SUM(G" + (startRow + 1) + ":G" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 7, "=IF(G" + startRow + ">0,ROUND((G" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //tb
                sheetData.SetFormulaValue(startRow, startCol + 8, "=SUM(I" + (startRow + 1) + ":I" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 9, "=IF(I" + startRow + ">0,ROUND((I" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //yeu
                sheetData.SetFormulaValue(startRow, startCol + 10, "=SUM(K" + (startRow + 1) + ":K" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 11, "=IF(K" + startRow + ">0,ROUND((K" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                //Tren TB
                sheetData.SetFormulaValue(startRow, startCol + 12, "=SUM(M" + (startRow + 1) + ":M" + (countSchool + startRow) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 13, "=IF(M" + startRow + ">0,ROUND((M" + startRow + "/D" + startRow + ")*100,2),\"0\")");


                startRow++;

                for (int j = 0; j < countSchool; j++)
                {
                    //ke khung
                    if ((j + 1) % 5 == 0 || (j + 1) == countSchool)
                    {
                        sheetData.CopyPasteSameSize(rangeSolid, startRow, 1);
                    }
                    else
                    {
                        sheetData.CopyPasteSameSize(rangeDot, startRow, 1);
                    }

                    objDataBySchool = listDataOfSchoolByDistrict.Where(p => p.SchoolID == listSchoolProfileTmp[j].SchoolProfileID).FirstOrDefault();
                    sheetData.SetCellValue(startRow, startCol, STT);
                    sheetData.SetCellValue(startRow, startCol + 1, listDictrict[i].DistrictName);
                    sheetData.SetCellValue(startRow, startCol + 2, listSchoolProfileTmp[j].SchoolName);
                    sheetData.SetCellValue(startRow, startCol + 3, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.PupilTotal) : 0);
                    //gioi
                    sheetData.SetCellValue(startRow, startCol + 4, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.ConductLevel01) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 5, "=IF(D" + startRow + ">0,ROUND((E" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //kha
                    sheetData.SetCellValue(startRow, startCol + 6, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.ConductLevel02) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 7, "=IF(D" + startRow + ">0,ROUND((G" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //tb
                    sheetData.SetCellValue(startRow, startCol + 8, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.ConductLevel03) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 9, "=IF(D" + startRow + ">0,ROUND((I" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //yeu
                    sheetData.SetCellValue(startRow, startCol + 10, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.ConductLevel04) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 11, "=IF(D" + startRow + ">0,ROUND((K" + startRow + "/D" + startRow + ")*100,2),\"0\")");
                    //kem
                    sheetData.SetCellValue(startRow, startCol + 12, objDataBySchool != null ? Utils.Utils.SetValueDefault(objDataBySchool.OnAverage) : 0);
                    sheetData.SetFormulaValue(startRow, startCol + 13, "=IF(D" + startRow + ">0,ROUND((M" + startRow + "/D" + startRow + ")*100,2),\"0\")");

                    startRow++;
                    STT++;
                }
            }

            if (isFemale)
            {
                sheetData.Name = "HS_Nu";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HẠNH KIỂM HỌC SINH NỮ CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh nữ - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else if (isEthnic)
            {
                sheetData.Name = "HS_DT";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HẠNH KIỂM HỌC SINH DÂN TỘC CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh dân tộc - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else if (isEF)
            {
                sheetData.Name = "HS_Nu_DT";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HẠNH KIỂM HỌC SINH NỮ DÂN TỘC CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "Học sinh nữ dân tộc - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
            else
            {
                sheetData.Name = "Tat_ca";
                sheetData.SetCellValue("C6", "THÔNG TIN VỀ HẠNH KIỂM HỌC SINH CÁC TRƯỜNG TRUNG HỌC CƠ SỞ");
                sheetData.SetCellValue("C7", "[Tất cả] - Khối " + data.EducationLevelID + " - " + Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            }
        }

        private void FillConductDataByEdcuationLevelSGD_PGD(IVTWorksheet sheetData, List<ConductStatisticsBO> listData,
           SearchViewModel data, bool isEthnic, bool isFemale, bool isEF)
        {
            List<ConductStatisticsBO> listDataByEducation = new List<ConductStatisticsBO>();
            ConductStatisticsBO objTmp = null;
            int startRow = 12;
            int startCol = 2;
            int minEducation = 6;
            int maxEducation = 9;

            //Fill du lieu chung
            Province provinceObj = ProvinceBusiness.Find(_globalInfo.ProvinceID.Value);
            DateTime currentDate = DateTime.Now;
            sheetData.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
            sheetData.SetCellValue("H4", provinceObj.ProvinceName + ", ngày " + currentDate.Day + " tháng " + currentDate.Month + " năm " + currentDate.Year);
            sheetData.SetCellValue("A8", Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            ////So chi fill cho cap 3
            for (int i = minEducation; i <= maxEducation; i++)
            {
                listDataByEducation = listData.Where(p => p.EducationLevelID == i).ToList();
                for (int j = 0; j < listDataByEducation.Count; j++)
                {
                    objTmp = listDataByEducation[j];
                    sheetData.SetCellValue(startRow, startCol, Utils.Utils.SetValueDefault(objTmp.PupilTotal));
                    //Gioi
                    sheetData.SetCellValue(startRow, startCol + 1, Utils.Utils.SetValueDefault(objTmp.ConductLevel01));
                    //Kha
                    sheetData.SetCellValue(startRow, startCol + 3, Utils.Utils.SetValueDefault(objTmp.ConductLevel02));
                    //TB
                    sheetData.SetCellValue(startRow, startCol + 5, Utils.Utils.SetValueDefault(objTmp.ConductLevel03));
                    //Yeu
                    sheetData.SetCellValue(startRow, startCol + 7, Utils.Utils.SetValueDefault(objTmp.ConductLevel04));
                    //Tren TB
                    sheetData.SetCellValue(startRow, startCol + 9, Utils.Utils.SetValueDefault(objTmp.OnAverage));
                }
                startRow++;
            }
            if (isFemale)
            {
                sheetData.Name = "HS_Nu";
            }
            else if (isEthnic)
            {
                sheetData.Name = "HS_DT";
            }
            else if (isEF)
            {
                sheetData.Name = "HS_Nu_DT";
            }
            else
            {
                sheetData.Name = "Tat_ca";
            }
        }

        private void FillConductDataByDistrictSGD(IVTWorksheet sheetData, IVTWorksheet sheetTemplate, List<ConductStatisticsBO> listData,
            SearchViewModel data, bool isEthnic, bool isFemale, bool isEF)
        {
            List<ConductStatisticsBO> listDataByEducation = new List<ConductStatisticsBO>();
            ConductStatisticsBO objTmp = null;
            int startRow = 12;
            int startCol = 1;

            //Fill du lieu chung
            Province provinceObj = ProvinceBusiness.Find(_globalInfo.ProvinceID.Value);
            DateTime currentDate = DateTime.Now;
            sheetData.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
            sheetData.SetCellValue("H4", provinceObj.ProvinceName + ", ngày " + currentDate.Day + " tháng " + currentDate.Month + " năm " + currentDate.Year);
            sheetData.SetCellValue("A8", Utils.Utils.GetNameSemester(data.Semester) + " - Năm học " + data.AcademicYearID + " - " + (data.AcademicYearID + 1));
            if (isEthnic)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HỌC LỰC HỌC SINH DÂN TỘC");
            }
            else if (isFemale)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HỌC LỰC HỌC SINH NỮ");
            }
            else if (isEF)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HỌC LỰC HỌC SINH NỮ DÂN TỘC");
            }
            else
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HỌC LỰC HỌC SINH");
            }

            IVTRange rangeDot = sheetTemplate.GetRange("A13", "P13");
            IVTRange rangeSolid = sheetTemplate.GetRange("A16", "P16");
            IVTRange rangeAllDistrict = sheetTemplate.GetRange("A36", "P36");
            ////So chi fill cho cap 3
            for (int j = 0; j < listData.Count; j++)
            {
                objTmp = listData[j];

                //ve khung
                if ((j + 1) % 5 == 0 || (j + 1) == listData.Count)
                {
                    sheetData.CopyPasteSameSize(rangeSolid, startRow, 1);
                }
                else
                {
                    sheetData.CopyPasteSameSize(rangeDot, startRow, 1);
                }

                sheetData.SetCellValue(startRow, startCol, (j + 1));
                sheetData.SetCellValue(startRow, startCol + 1, objTmp.DistrictName);
                sheetData.SetCellValue(startRow, startCol + 2, Utils.Utils.SetValueDefault(objTmp.PupilTotal));
                //Gioi
                sheetData.SetCellValue(startRow, startCol + 3, Utils.Utils.SetValueDefault(objTmp.ConductLevel01));
                sheetData.SetFormulaValue(startRow, startCol + 4, "=CONCATENATE(ROUND(D" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Kha
                sheetData.SetCellValue(startRow, startCol + 5, Utils.Utils.SetValueDefault(objTmp.ConductLevel02));
                sheetData.SetFormulaValue(startRow, startCol + 6, "=CONCATENATE(ROUND(F" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //TB
                sheetData.SetCellValue(startRow, startCol + 7, Utils.Utils.SetValueDefault(objTmp.ConductLevel03));
                sheetData.SetFormulaValue(startRow, startCol + 8, "=CONCATENATE(ROUND(H" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Yeu
                sheetData.SetCellValue(startRow, startCol + 9, Utils.Utils.SetValueDefault(objTmp.ConductLevel04));
                sheetData.SetFormulaValue(startRow, startCol + 10, "=CONCATENATE(ROUND(J" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //Tren TB
                sheetData.SetCellValue(startRow, startCol + 11, Utils.Utils.SetValueDefault(objTmp.OnAverage));
                sheetData.SetFormulaValue(startRow, startCol + 12, "=CONCATENATE(ROUND(L" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                startRow++;
            }
            if (listData.Count > 0)
            {
                //VE khung va fill dong Toan tinh/TP
                sheetData.CopyPasteSameSize(rangeAllDistrict, startRow, 1);
                //fill fomular
                //Tong so hoc sinh
                sheetData.SetFormulaValue(startRow, startCol + 2, "=SUM(C12:C" + (startRow - 1) + ")");
                // Hanh kiem tot
                sheetData.SetFormulaValue(startRow, startCol + 3, "=SUM(D12:D" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 4, "=CONCATENATE(ROUND(D" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //hanh kiem kha
                sheetData.SetFormulaValue(startRow, startCol + 5, "=SUM(F12:F" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 6, "=CONCATENATE(ROUND(F" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //hanh kiem TB
                sheetData.SetFormulaValue(startRow, startCol + 7, "=SUM(H12:H" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 8, "=CONCATENATE(ROUND(H" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //hanh kiem yeu
                sheetData.SetFormulaValue(startRow, startCol + 9, "=SUM(J12:J" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 10, "=CONCATENATE(ROUND(J" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
                //hanh kiem TB tro len
                sheetData.SetFormulaValue(startRow, startCol + 11, "=SUM(L12:L" + (startRow - 1) + ")");
                sheetData.SetFormulaValue(startRow, startCol + 12, "=CONCATENATE(ROUND(L" + startRow + "/IF(C" + startRow + "<=0,1,C" + startRow + "),4)*100,\" % \")");
            }
            if (isFemale)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HẠNH KIỂM HỌC SINH NỮ");
                sheetData.Name = "HS_Nu";
            }
            else if (isEthnic)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HẠNH KIỂM HỌC SINH DÂN TỘC");
                sheetData.Name = "HS_DT";
            }
            else if (isEF)
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HẠNH KIỂM HỌC SINH NỮ DÂN TỘC");
                sheetData.Name = "HS_Nu_DT";
            }
            else
            {
                sheetData.SetCellValue("A6", "TỔNG HỢP ĐÁNH GIÁ XẾP LOẠI HẠNH KIỂM HỌC SINH");
                sheetData.Name = "Tat_ca";
            }
        }

        #endregion

        private string GetNameTemplate(int? educationLevelId, int semester, int reportType)
        {
            string templateName = string.Empty;
            string semesterName = Utils.Utils.GetNameSemester(semester, false, true);
            if (reportType == SystemParamsInFile.CAPACITY_STATISTIC_ID)
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    templateName = "SGD_THCS_ThongKeHocLuc_Khoi" + educationLevelId + "_" + semesterName + ".xls";
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    templateName = "PGD_THCS_ThongKeHocLuc_Khoi" + educationLevelId + "_" + semesterName + ".xls";
                }
            }
            else if (reportType == SystemParamsInFile.CAPACITY_STATISTIC_EDUCATION_LEVEL_ID)
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    templateName = "SGD_THCS_ThongKeHocLucTheoKhoi_" + semesterName + ".xls";
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    templateName = "PGD_THCS_ThongKeHocLucTheoKhoi_" + semesterName + ".xls";
                }
            }
            else if (reportType == SystemParamsInFile.CAPACITY_STATISTIC_DISTRICT_ID)
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    templateName = "SGD_THCS_ThongKeHocLucTheoQuanHuyen_" + semesterName + ".xls";
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    templateName = "PGD_THCS_ThongKeHocLucTheoQuanHuyen_" + semesterName + ".xls";
                }
            }
            else if (reportType == SystemParamsInFile.CONDUCT_STATISTIC_ID)
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    templateName = "SGD_THCS_ThongKeHanhKiem_Khoi" + educationLevelId + "_" + semesterName + ".xls";
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    templateName = "PGD_THCS_ThongKeHanhKiem_Khoi" + educationLevelId + "_" + semesterName + ".xls";
                }
            }
            else if (reportType == SystemParamsInFile.CONDUCT_STATISTIC_EDUCATION_LEVEL_ID)
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    templateName = "SGD_THCS_ThongKeHanhKiemTheoKhoi_" + semesterName + ".xls";
                }
                else
                {
                    templateName = "PGD_THCS_ThongKeHanhKiemTheoKhoi_" + semesterName + ".xls";
                }
            }
            else if (reportType == SystemParamsInFile.CONDUCT_STATISTIC_DISTRICT_ID)
            {
                templateName = "SGD_THCS_ThongKeHanhKiemTheoQuanHuyen_" + semesterName + ".xls";
            }
            return templateName;
        }

        private string GetTemplate(int reportType)
        {
            string templateName = string.Empty;
            if (reportType == SystemParamsInFile.CAPACITY_STATISTIC_ID)
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    templateName = "SGD_THCS_ThongKeHocLuc_Khoi6_HKI.xls";
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    templateName = "PGD_THCS_ThongKeHocLuc_Khoi6_HKI.xls";
                }
            }
            else if (reportType == SystemParamsInFile.CAPACITY_STATISTIC_EDUCATION_LEVEL_ID)
            {
                templateName = "SGD_THCS_ThongKeHocLucTheoKhoi_HKI.xls";
            }
            else if (reportType == SystemParamsInFile.CAPACITY_STATISTIC_DISTRICT_ID)
            {
                templateName = "SGD_THCS_ThongKeHocLucTheoQuanHuyen_HKI.xls";
            }
            else if (reportType == SystemParamsInFile.CONDUCT_STATISTIC_ID)
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    templateName = "SGD_THCS_ThongKeHanhKiem_Khoi6_HKI.xls";
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    templateName = "PGD_THCS_ThongKeHanhKiem_Khoi6_HKI.xls";
                }
            }
            else if (reportType == SystemParamsInFile.CONDUCT_STATISTIC_EDUCATION_LEVEL_ID)
            {
                templateName = "SGD_THCS_ThongKeHanhKiemTheoKhoi_HKI.xls";
            }
            else if (reportType == SystemParamsInFile.CONDUCT_STATISTIC_DISTRICT_ID)
            {
                templateName = "SGD_THCS_ThongKeHanhKiemTheoQuanHuyen_HKI.xls";
            }
            return templateName;
        }


    }
}