﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETStatisticDataSecondaryArea
{
    public class DOETStatisticDataSecondaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DOETStatisticDataSecondaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DOETStatisticDataSecondaryArea_default",
                "DOETStatisticDataSecondaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
