﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SummedUpEducationLevelNewArea
{
    public class SummedUpEducationLevelNewConstants
    {
        public const string LIST_PERIOD = "LIST_PERIOD";
        public const string LIST_APPLIEDLEVEL = "LIST_APPLIEDLEVEL";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SUBJECT = "listSubject";
        public const string ShowList = "ShowList";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string CLASS_12 = "CLASS_12";
        public const string SEMESTER_1 = "SEMESTER_1";

        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";

        public const string ERROR_IMPORT = "ERROR_IMPORT";

        public const string LOCK_IMPORT = "LOCK_IMPORT";
        public const string COUNT = "count";
        public const string ENABLE_ENTRYMARK = "enable_entryMark";

        public const string COLMARKSEMESTER = "COLMARKSEMESTER";
        public const string LIST_TAB = "LIST_TAB";
    }
}