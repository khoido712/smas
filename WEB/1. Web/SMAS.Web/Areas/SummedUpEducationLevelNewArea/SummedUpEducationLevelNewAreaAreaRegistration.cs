﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SummedUpEducationLevelNewArea
{
    public class SummedUpEducationLevelNewAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SummedUpEducationLevelNewArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SummedUpEducationLevelNewArea_default",
                "SummedUpEducationLevelNewArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
