﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
namespace SMAS.Web.Areas.SummedUpEducationLevelNewArea.Controllers
{
    public class SummedUpEducationLevelNewController : Controller
    {
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;

        public SummedUpEducationLevelNewController(IConductLevelBusiness conductLevelBusiness,
                                    IPeriodDeclarationBusiness PeriodDeclarationBusiness,
                                    IClassProfileBusiness ClassProfileBusiness)
        {
            this.ConductLevelBusiness = conductLevelBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }

        private GlobalInfo Global = new GlobalInfo();

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        private void SetViewData()
        {
            List<ViettelCheckboxList> listTabs = new List<ViettelCheckboxList>();
            listTabs.Add(new ViettelCheckboxList(Res.Get("SummedUpEducationLevelNew_Label_Tab1"), 1, false, false));
            listTabs.Add(new ViettelCheckboxList(Res.Get("SummedUpEducationLevelNew_Label_Tab2"), 2, false, false));
            ViewData[SummedUpEducationLevelNewConstants.LIST_TAB] = listTabs;

            List<EducationLevel> lsEducationLevel = Global.EducationLevels;

            if (lsEducationLevel != null)
                ViewData[SummedUpEducationLevelNewConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            else
                ViewData[SummedUpEducationLevelNewConstants.LIST_EDUCATIONLEVEL] = new SelectList(new string[] { });

            IDictionary<string, object> PeriodSearchInfo = new Dictionary<string, object>();
            PeriodSearchInfo["AcademicYearID"] = Global.AcademicYearID;
            PeriodSearchInfo["Semester"] = Global.Semester;
            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(Global.SchoolID.Value, PeriodSearchInfo).ToList();

            int defaultValue = 0;
            foreach (PeriodDeclaration item in lstPeriod)
                if (item.FromDate.HasValue && item.EndDate.HasValue)
                {
                    item.Resolution = item.Resolution + " (từ " + item.FromDate.Value.ToShortDateString() + " đến " + item.EndDate.Value.ToShortDateString() + ")";
                    if (item.FromDate.Value < DateTime.Now && item.EndDate.Value > DateTime.Now) defaultValue = item.PeriodDeclarationID;
                }

            ViewData[SummedUpEducationLevelNewConstants.LIST_SEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value", Global.Semester);
            ViewData[SummedUpEducationLevelNewConstants.LIST_PERIOD] = new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", defaultValue);
            ViewData[SummedUpEducationLevelNewConstants.LIST_APPLIEDLEVEL] = Global.AppliedLevel;
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadPeriod(int semester)
        {
            IDictionary<string, object> PeriodSearchInfo = new Dictionary<string, object>();
            PeriodSearchInfo["AcademicYearID"] = Global.AcademicYearID;
            PeriodSearchInfo["Semester"] = semester;
            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(Global.SchoolID.Value, PeriodSearchInfo).ToList();
            int defaultValue = 0;
            foreach (PeriodDeclaration item in lstPeriod)
                if (item.FromDate.HasValue && item.EndDate.HasValue)
                {
                    item.Resolution = item.Resolution + " (từ " + item.FromDate.Value.ToShortDateString() + " đến " + item.EndDate.Value.ToShortDateString() + ")";
                    if (item.FromDate.Value < DateTime.Now && item.EndDate.Value > DateTime.Now) defaultValue = item.PeriodDeclarationID;
                }
            return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", defaultValue));
        }
    }
}
