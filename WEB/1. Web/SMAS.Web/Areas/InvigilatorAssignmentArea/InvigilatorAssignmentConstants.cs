/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.InvigilatorAssignmentArea
{
    public class InvigilatorAssignmentConstants
    {
        public const string LIST_INVIGILATORASSIGNMENT = "listInvigilatorAssignment";
        public const string LIST_ROOM = "LIST_ROOM";
        public const string CBO_EXAMINATION = "listEXAMINATION";
        public const string CBO_EDUCATIONLEVEL = "listEDUCATIONLEVEL";
        public const string CBO_EXAMINATIONSUBJECT = "listEXAMINATIONSUBJECT";
        public const string CBO_CHANGESUBJECT = "CboCHANGESUBJECT";
        public const string CBO_ROOM = "listROOM";
        public const string COUNT_CANDIDATE = "listCANDIDATE";
        public const string DATA_INVIGILATOR = "DataListInvigilator";
        public const string REPLACE_INVIGILATOR = "ReplaceInvigilator";
        public const string CHANGE_INVIGILATOR = "ChangeInvigilator";
        public const string LIST_CHANGEINVIGILATOR = "listChangeInvigilator";
        public const string LIST_CHANGESUBJECT = "listChangeSubject";
        public const string INDEX = "INDEX";
    }
}