﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.InvigilatorAssignmentArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.InvigilatorAssignmentArea.Controllers
{
    public class InvigilatorAssignmentController : BaseController
    {
        private readonly IInvigilatorAssignmentBusiness InvigilatorAssignmentBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        private readonly IInvigilatorBusiness InvigilatorBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;

        public InvigilatorAssignmentController(IReportDefinitionBusiness ReportDefinitionBusiness, ICandidateBusiness CandidateBusiness, IExaminationSubjectBusiness ExaminationSubjectBusiness, IInvigilatorBusiness InvigilatorBusiness, IExaminationRoomBusiness ExaminationRoomBusiness, IEducationLevelBusiness EducationLevelBusiness, IExaminationBusiness ExaminationBusiness, IInvigilatorAssignmentBusiness invigilatorassignmentBusiness)
        {
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.CandidateBusiness = CandidateBusiness;
            this.InvigilatorAssignmentBusiness = invigilatorassignmentBusiness;
            this.ExaminationBusiness = ExaminationBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ExaminationRoomBusiness = ExaminationRoomBusiness;
            this.InvigilatorBusiness = InvigilatorBusiness;
            this.ExaminationSubjectBusiness = ExaminationSubjectBusiness;
        }

        //
        // GET: /InvigilatorAssignment/

        public ActionResult Index(int? idExamination)
        {
            SetviewData();
            ViewData[InvigilatorAssignmentConstants.INDEX] = 0;
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            GlobalInfo Global = new GlobalInfo();
            //cbo Examination
            //ExaminationBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
            // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            IQueryable<Examination> lsExamination = ExaminationBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary).OrderByDescending(o => o.ToDate);
            if (lsExamination.Count() > 0)
            {
                List<Examination> ListExam = new List<Examination>();
                ListExam = lsExamination.ToList();
                ViewData[InvigilatorAssignmentConstants.CBO_EXAMINATION] = ListExam;
                ViewData[InvigilatorAssignmentConstants.INDEX] = idExamination;
                if (idExamination.HasValue)
                {
                    List<EducationLevel> lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(Global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",Global.AcademicYearID.Value},
                    {"AppliedLevel",Global.AppliedLevel.Value},
                    {"ExaminationID",idExamination.Value}
                }).Select(o => o.EducationLevel).Distinct().OrderBy(o => o.EducationLevelID).ToList();
                    ViewData[InvigilatorAssignmentConstants.CBO_EDUCATIONLEVEL] = lsEducationLevel;
                }
                else
                {
                    ViewData[InvigilatorAssignmentConstants.CBO_EDUCATIONLEVEL] = new List<EducationLevel>();
                }
            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here

            IEnumerable<InvigilatorAssignmentViewModel> lst = this._Search(SearchInfo);
            ViewData[InvigilatorAssignmentConstants.LIST_INVIGILATORASSIGNMENT] = lst;
            return View();
        }

        //
        // GET: /InvigilatorAssignment/Search


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //

            IEnumerable<InvigilatorAssignmentViewModel> lst = this._Search(SearchInfo);
            ViewData[InvigilatorAssignmentConstants.LIST_INVIGILATORASSIGNMENT] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>


        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int InvigilatorAssignmentID)
        {
            InvigilatorAssignment invigilatorassignment = this.InvigilatorAssignmentBusiness.Find(InvigilatorAssignmentID);
            //ExaminationSubject examinationsubject = this.ExaminationSubjectBusiness.Find(ExaminationSubjectID);
            CheckPermissionForAction(invigilatorassignment.ExaminationRoom.ExaminationID, "Examination");
            TryUpdateModel(invigilatorassignment);
            Utils.Utils.TrimObject(invigilatorassignment);
            this.InvigilatorAssignmentBusiness.Update(invigilatorassignment);
            this.InvigilatorAssignmentBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<InvigilatorAssignmentViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo Global = new GlobalInfo();
            IQueryable<InvigilatorAssignment> query = this.InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, SearchInfo);
            List<InvigilatorAssignmentViewModel> List = new List<InvigilatorAssignmentViewModel>();
            if (query.Count() > 0)
            {
                IQueryable<InvigilatorAssignmentViewModel> lst = query.Select(o => new InvigilatorAssignmentViewModel
                {
                    InvigilatorAssignmentID = o.InvigilatorAssignmentID,
                    InvigilatorID = o.InvigilatorID,
                    RoomID = o.RoomID,
                    AssignedDate = o.AssignedDate


                });
                List = lst.ToList();
            }

            return List;
        }

        public void SetviewData()
        {
            ViewData[InvigilatorAssignmentConstants.CBO_EXAMINATION] = new List<Examination>();
            ViewData[InvigilatorAssignmentConstants.CBO_EXAMINATIONSUBJECT] = new List<ExaminationSubject>();
            ViewData[InvigilatorAssignmentConstants.CBO_ROOM] = new List<ExaminationRoom>();
            ViewData[InvigilatorAssignmentConstants.DATA_INVIGILATOR] = new List<InvigilatorAssignmentViewModel>();
            ViewData[InvigilatorAssignmentConstants.REPLACE_INVIGILATOR] = new List<InvigilatorAssignmentViewModel>();
            ViewData[InvigilatorAssignmentConstants.CHANGE_INVIGILATOR] = new List<InvigilatorAssignmentViewModel>();
            ViewData[InvigilatorAssignmentConstants.LIST_CHANGESUBJECT] = new List<InvigilatorAssignmentViewModel>();
            ViewData[InvigilatorAssignmentConstants.CBO_CHANGESUBJECT] = new List<InvigilatorAssignmentBO>();
        }

        #region AjaxLoadEducationLevel

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEducationLevel(int? idExamination)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<EducationLevel> lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"ExaminationID",idExamination}
            }).Select(o => o.EducationLevel).Distinct();
            lsEducationLevel = lsEducationLevel.OrderBy(o => o.EducationLevelID);
            return Json(new SelectList(lsEducationLevel, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);

        }
        #endregion AjaxLoadEducationLevel

        #region Load Combobox Subject



        /// <summary>
        /// AjaxLoadSubject
        /// </summary>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? idEducationLevel, int? idExamination)
        {
            if (idExamination == null || idEducationLevel == null)
            {
                return Json(new SelectList(new List<ExaminationSubject>(), "ExaminationSubjectID", "SubjectCat.DisplayName"), JsonRequestBehavior.AllowGet); ;
            }

            GlobalInfo Global = new GlobalInfo();
            //cboExaminationSubject: ExaminationSubjectBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) để fill dữ liệu vào cboExaminationSubject
            // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            // + Dictionary[“EducationLevelID”] = cboEducationLevel.Value
            // + Dictionary[“ExaminationID”] = cboExamination.Value 
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = idExamination;

            IQueryable<ExaminationSubject> lsExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            List<ExaminationSubjectBO> lstExaminationSubject = lsExaminationSubject.Select(o => new ExaminationSubjectBO
            {
                ExaminationSubjectID = o.ExaminationSubjectID,
                DisplayName = o.SubjectCat.DisplayName,
                OrderInSubject = o.SubjectCat.OrderInSubject
            })
            .OrderBy(s => s.OrderInSubject).ThenBy(s=>s.DisplayName)
            .ToList();
            return Json(new SelectList(lstExaminationSubject, "ExaminationSubjectID", "DisplayName"), JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Load Combobox Room
        /// <summary>
        /// AjaxLoadRoom
        /// </summary>
        /// <param name="idExaminationSubject">The id examination subject.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadRoom(int? idExaminationSubject, int? idExamination)
        {
            if (idExaminationSubject == null)
            {
                return Json(new SelectList(new List<ExaminationRoom>(), "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
            }

            GlobalInfo Global = new GlobalInfo();
            //cboRoom: ExaminationRoomBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) để lấy lên danh sách các phòng (lstRoom)
            // + Dictionary[“ExaminationID”] = cboExamination.Value
            // + Dictionary[“ExaminationSubjectID”] = cboExaminationSubject.Value 

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();

            Dictionary["ExaminationID"] = idExamination;
            Dictionary["ExaminationSubjectID"] = idExaminationSubject;
            IQueryable<ExaminationRoom> lsExaminationRoom = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            List<ExaminationRoom> lstExaminationRoom = new List<ExaminationRoom>();
            if (lsExaminationRoom.Count() > 0)
            {
                lstExaminationRoom = lsExaminationRoom.ToList().OrderBy(r=>SMAS.Business.Common.Utils.SortABC(r.RoomTitle)).ToList();
            }

            return Json(new SelectList(lstExaminationRoom, "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region LoadGrid
        /// <summary>
        /// AjaxLoadGrid
        /// </summary>
        /// <param name="idRoom">The id room.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <param name="idExaminationSubject">The id examination subject.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadGrid(int? idRoom, int? idExamination, int? idExaminationSubject)
        {
            if (idExaminationSubject != null && idExaminationSubject != 0)
            {
                if (idRoom == null)
                {

                    //Grid: Với mỗi phòng thi (RoomID) trong lstRoom
                    GlobalInfo Global = new GlobalInfo();
                    //Hệ thống gọi hàm InvigilatorAssignmentBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) để lấy danh sách giám thị
                    IDictionary<string, object> Dictionary = new Dictionary<string, object>();

                    Dictionary["ExaminationID"] = idExamination;
                    Dictionary["ExaminationSubjectID"] = idExaminationSubject;
                    IQueryable<ExaminationRoom> lsExaminationRoom = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
                    // Danh sach tat ca phong thi
                    List<InvigilatorAssignmentViewModel> LsInvigilatorAssignmentExam = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary)
                        .Select(ia => new InvigilatorAssignmentViewModel
                        {
                            FullName = ia.Invigilator.Employee.FullName,
                            FacultyName = ia.Invigilator.Employee.SchoolFaculty != null ? ia.Invigilator.Employee.SchoolFaculty.FacultyName : string.Empty,
                            FacultyID = ia.Invigilator.Employee.SchoolFacultyID,
                            EmployeeCode = ia.Invigilator.Employee.EmployeeCode,
                            InvigilatorAssignmentID = ia.InvigilatorAssignmentID,
                            RoomID = ia.ExaminationRoom.ExaminationRoomID,
                            RoomTitle = ia.ExaminationRoom.RoomTitle,
                            ShortName = ia.Invigilator.Employee.Name
                        }).ToList();
                    // Lay so luong thi sinh theo phong thi
                    IQueryable<Candidate> lsCandidate = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
                    var countCandidateByRoom = lsCandidate.GroupBy(o => o.RoomID).Select(o => new { RoomID = o.Key, countCandidate = o.Count() }).ToList();
                    if (lsExaminationRoom.Count() > 0)
                    {
                        List<ExaminationRoom> lstExaminationRoom = lsExaminationRoom.OrderBy(o => o.ExaminationRoomID).ToList();
                        ViewData[InvigilatorAssignmentConstants.LIST_ROOM] = lstExaminationRoom;
                        List<InvigilatorAssignmentViewModel> ListInvi = new List<InvigilatorAssignmentViewModel>();
                        foreach (ExaminationRoom er in lstExaminationRoom)
                        {
                            List<InvigilatorAssignmentViewModel> LsInvigilatorAssignment = LsInvigilatorAssignmentExam.Where(o => o.RoomID == er.ExaminationRoomID).OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.ShortName + " " + p.FullName)).ToList();
                            int CountCandidate = countCandidateByRoom.Where(o => o.RoomID == er.ExaminationRoomID).Sum(o => o.countCandidate);
                            if (LsInvigilatorAssignment.Count > 0)
                            {
                                foreach (var ia in LsInvigilatorAssignment)
                                {
                                    ia.CountCandidate = CountCandidate;
                                    ia.strRoom = ia.RoomTitle + "(Số thí sinh :" + CountCandidate + ")";
                                    ListInvi.Add(ia);
                                }
                            }
                            else
                            {
                                InvigilatorAssignmentViewModel iarm = new InvigilatorAssignmentViewModel();
                                iarm.InvigilatorAssignmentID = 0;
                                iarm.FullName = "Chưa có giám thị";
                                iarm.RoomID = er.ExaminationRoomID;
                                iarm.RoomTitle = er.RoomTitle;
                                iarm.CountCandidate = CountCandidate;
                                iarm.strRoom = er.RoomTitle + "(Số thí sinh :" + CountCandidate + ")";
                                ListInvi.Add(iarm);
                            }
                        }
                        ViewData[InvigilatorAssignmentConstants.LIST_INVIGILATORASSIGNMENT] = ListInvi;

                    }

                }
                else
                {
                    List<InvigilatorAssignmentViewModel> ListInvi = new List<InvigilatorAssignmentViewModel>();
                    // + Dictionary[“ExaminationID”] = cboExamination.Value
                    // + Dictionary[“RoomID”] = RoomID 
                    GlobalInfo Global = new GlobalInfo();
                    IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                    Dictionary["ExaminationID"] = idExamination.Value;
                    Dictionary["RoomID"] = idRoom.Value;
                    IQueryable<InvigilatorAssignment> LsInvigilatorAssignment = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
                    // So luong hoc sinh trong phong
                    IQueryable<Candidate> lsCandidate = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
                    int countCandidate = lsCandidate.Count();

                    if (LsInvigilatorAssignment.Count() > 0)
                    {
                        List<InvigilatorAssignment> lstInvigilatorAssignment = LsInvigilatorAssignment.ToList();
                        foreach (var ia in lstInvigilatorAssignment)
                        {
                            InvigilatorAssignmentViewModel iarm = new InvigilatorAssignmentViewModel();
                            iarm.FullName = ia.Invigilator.Employee.FullName;
                            iarm.FacultyName = ia.Invigilator.Employee.SchoolFaculty != null ? ia.Invigilator.Employee.SchoolFaculty.FacultyName : string.Empty;
                            iarm.FacultyID = ia.Invigilator.Employee.SchoolFacultyID;
                            iarm.EmployeeCode = ia.Invigilator.Employee.EmployeeCode;
                            iarm.InvigilatorAssignmentID = ia.InvigilatorAssignmentID;
                            iarm.RoomID = ia.RoomID;
                            iarm.RoomTitle = ia.ExaminationRoom.RoomTitle;
                            iarm.CountCandidate = countCandidate;
                            iarm.strRoom = ia.ExaminationRoom.RoomTitle + "(Số thí sinh :" + countCandidate + ")";
                            ListInvi.Add(iarm);
                        }
                    }

                    ViewData[InvigilatorAssignmentConstants.LIST_INVIGILATORASSIGNMENT] = ListInvi;
                    Dictionary["ExaminationID"] = idExamination;
                    Dictionary["ExaminationSubjectID"] = idExaminationSubject;
                    IQueryable<ExaminationRoom> lsExaminationRoom = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
                    List<ExaminationRoom> lstExamRoom = lsExaminationRoom.Where(o => o.ExaminationRoomID == idRoom.Value).ToList();
                    ViewData[InvigilatorAssignmentConstants.LIST_ROOM] = lstExamRoom;

                }
            }
            else
            {
                ViewData[InvigilatorAssignmentConstants.LIST_INVIGILATORASSIGNMENT] = new List<InvigilatorAssignmentViewModel>();
            }

            return PartialView("_List");
        }
        #endregion

        #region Phân công
        /// <summary>
        /// PrepareAutoAssign
        /// </summary>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult PrepareAutoAssign(int? idExamination, int? EducationLevelID, int? ExaminationSubjectID, int? ExaminationRoomID)
        {
            //Hệ thống gọi hàm ExaminationRoomBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary).Count() trên màn hình tra cứu kết quả phân công để lấy lên số phòng thi và hiển thị thông tin vào lblRoom
            // + Dictionary[“ExaminationID”] = cboExamination.Value
            // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel

            //Hệ thống gọi hàm InvigilatorBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary).Count()  trên màn hình tra cứu kết quả phân công lấy lên số giám thị hiển thị vào lblInvigilator 
            //+ Dictionary[“ExaminationID”] = cboExamination.Value
            //+ Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel

            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["ExaminationID"] = idExamination;
            //Dictionary["EducationLevelID"] = EducationLevelID;
            //Dictionary["ExaminationSubjectID"] = ExaminationSubjectID; 
            IQueryable<Candidate> query = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);


            InvigilatorAssignmentViewModel iarm = new InvigilatorAssignmentViewModel();
            iarm.ExaminationID = idExamination;
            iarm.CountRoom = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary).Count();
            iarm.CountTearcher = InvigilatorBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary).Count();


            return PartialView("_AutoAssign", iarm);

        }
        /// <summary>
        /// Assign
        /// </summary>
        /// <param name="iarm">The iarm.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Assign(InvigilatorAssignmentViewModel iarm)
        {
            GlobalInfo Global = new GlobalInfo();
            //Hệ thống hiển thị xác nhận với người dùng “Khi thực hiện phân công giám thị thì tất cả các kết quả phân công giám thị trước sẽ bị xóa. 
            //    Thầy/cô có muốn tiếp tục thực hiện không?”

            //Nếu người dùng đồng ý thực hiện phân công giám thị hệ thống thực hiện gọi hàm InvigilatorAssignmentBusiness.
            //AutoAssign(UserInfo.SchoolID, UserInfo.AppliedLevel, cboExamination.Value, txtNumberOfInvigilator.Value) 
            this.InvigilatorAssignmentBusiness.AutoAssign(Global.SchoolID.Value, (int)Global.AppliedLevel, iarm.ExaminationID.Value, iarm.NumberOfInvigilator.Value);
            this.InvigilatorAssignmentBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion


        #region Thêm mới giám thị
        /// <summary>
        /// PrepareCreate
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult PrepareCreate(int? id, int? idExamSubject)
        {
            ViewData[InvigilatorAssignmentConstants.DATA_INVIGILATOR] = new List<InvigilatorAssignmentViewModel>();
            InvigilatorAssignmentViewModel iarm = new InvigilatorAssignmentViewModel();
            ExaminationRoom er = ExaminationRoomBusiness.Find(id);
            if (er != null)
            {
                iarm.RoomID = er.ExaminationRoomID;
                iarm.ExaminationID = er.ExaminationID;
                iarm.RoomTitle = er.RoomTitle;
                ExaminationSubject es = er.ExaminationSubject;
                DateTime endTime = es.StartTime.Value.AddMinutes(es.DurationInMinute.Value);
                //
                //    dlInvigilator: select from InvigilatorBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                //    iv where not exists (select 1 from InvigilatorAssignment ia where ia.InvigilatorID = iv.InvigilatorID) 
                //+ Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
                //+ Dictionary[“ExaminationID”] = ExaminationID  
                GlobalInfo Global = new GlobalInfo();

                IQueryable<InvigilatorAssignment> QueryIA = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, new Dictionary<string, object>
                {

                });
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["AppliedLevel"] = Global.AppliedLevel;
                Dictionary["ExaminationID"] = er.ExaminationID;
                List<Invigilator> lsInvigilator = InvigilatorBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary).ToList();


                List<InvigilatorAssignment> lsInvigilatorAssignment = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary).ToList();
                if (lsInvigilator.Count() > 0)
                {
                    List<int> list = new List<int>();
                    foreach (InvigilatorAssignment ia in lsInvigilatorAssignment)
                    {
                        if (ia.ExaminationRoom.ExaminationSubject.StartTime <= endTime
                            && ia.ExaminationRoom.ExaminationSubject.StartTime.Value.AddMinutes(ia.ExaminationRoom.ExaminationSubject.DurationInMinute.Value)
                            >= es.StartTime)
                        {
                            list.Add(ia.InvigilatorID);
                        }
                    }
                    lsInvigilator = lsInvigilator.FindAll(x => !list.Contains(x.InvigilatorID));



                    List<int> lstInvigilatorAssignment = lsInvigilatorAssignment.Select(a => a.Invigilator.TeacherID).Distinct().ToList();

                    List<InvigilatorAssignmentViewModel> ListIV = new List<InvigilatorAssignmentViewModel>();

                    var query = from a in lsInvigilator
                                group a by new { a.TeacherID, a.ExaminationID } into gr
                                select new
                                {
                                    TeacherID = gr.Key.TeacherID,
                                    ExaminationID = gr.Key.ExaminationID,
                                    Count = gr.Select(a => a.Examination.ExaminationRooms).Count()
                                };
                    ListIV = (from a in lsInvigilator
                              join b in query on new { a.TeacherID, a.ExaminationID } equals new { b.TeacherID, b.ExaminationID }
                              select new InvigilatorAssignmentViewModel
                              {

                                  InvigilatorID = a.InvigilatorID,
                                  TeacherID = a.TeacherID,
                                  ExaminationID = a.ExaminationID,
                                  FullName = a.Employee.FullName,
                                  EmployeeCode = a.Employee.EmployeeCode,
                                  FacultyName = a.Employee.SchoolFaculty == null ? "" : a.Employee.SchoolFaculty.FacultyName,
                                  CountRoom = b.Count,
                                  ShortName = a.Employee.Name
                              }).Distinct().ToList();
                    if (ListIV.Count > 0 && ListIV != null)
                    {
                        ListIV = ListIV.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.ShortName + " " + p.FullName)).ToList();
                    }
                    ViewData[InvigilatorAssignmentConstants.DATA_INVIGILATOR] = ListIV;
                }
            }


            return PartialView("_Create", iarm);

        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="ListInvigilator">The list invigilator.</param>
        /// <param name="iarm">The iarm.</param>
        /// <returns>
        /// ActionResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(List<int> ListInvigilator, InvigilatorAssignmentViewModel iarm)
        {
            if (ListInvigilator == null)
            {
                throw new BusinessException("Validate_NotSelectInvigilator");
            }
            GlobalInfo Global = new GlobalInfo();
            //Hệ thống gọi hàm InvigilatorAssignmentBusiness.AddInvigilatorForRoom(UserInfo.SchoolID, UserInfo.AppliedLevel, RoomID, dlInvigilator.Value)
            foreach (int item in ListInvigilator)
            {
                InvigilatorAssignmentBusiness.AddInvigilatorForRoom(Global.SchoolID.Value, (int)Global.AppliedLevel.Value, iarm.RoomID, item);
                InvigilatorAssignmentBusiness.Save();

            }
            PrepareCreate(iarm.RoomID, iarm.hidenSubjectID);
            return PartialView("_Create");
        }
        #endregion

        #region Xoá giám thị
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            InvigilatorAssignment invigilatorassignment = this.InvigilatorAssignmentBusiness.Find(id);
            CheckPermissionForAction(invigilatorassignment.ExaminationRoom.ExaminationID, "Examination");
            //Thông qua gọi hàm InvigilatorAssignmentBusiness.Delete
            //    (UserInfo.SchoolID, UserInfo.AppliedLevel, InvigilatorAssignmentID) truyền vào InvigilatorAssignmentID tương ứng được chọn trên gridview
            GlobalInfo Global = new GlobalInfo();

            this.InvigilatorAssignmentBusiness.Delete(Global.SchoolID.Value, (int)Global.AppliedLevel, id);
            this.InvigilatorAssignmentBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion


        #region Thay thế giám thị
        /// <summary>
        /// PrepareReplace
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult PrepareReplace(int? id)
        {
            InvigilatorAssignment invigilatorassignment = this.InvigilatorAssignmentBusiness.Find(id);
            CheckPermissionForAction(invigilatorassignment.ExaminationRoom.ExaminationID, "Examination");
            if (id == null)
            {
                return PartialView("_Replace");
            }
            else
            {
                ViewData[InvigilatorAssignmentConstants.REPLACE_INVIGILATOR] = new List<InvigilatorAssignmentViewModel>();
                InvigilatorAssignmentViewModel iarm = new InvigilatorAssignmentViewModel();
                //Ta có InvigilatorAssignmentID từ người dùng truyền lên server
                //Từ InvigilatorAssignmentID => ExaminationID, RoomID, EducationLevelID
                InvigilatorAssignment ia = InvigilatorAssignmentBusiness.Find(id.Value);
                if (ia != null)
                {
                    int? ExaminationID = ia.ExaminationRoom.ExaminationID;
                    iarm.RoomID = ia.RoomID;
                    iarm.InvigilatorAssignmentID = id.Value;
                    iarm.InvigilatorName = ia.Invigilator.Employee.FullName;
                    int EducationLevelID = ia.ExaminationRoom.EducationLevelID;

                    // lblChange.Value = Tên giám thị lấy từ  InvigilatorAssignmentID

                    //dlInvigilator: select from InvigilatorBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) iv where not exists 
                    //                (select 1 from InvigilatorAssignment ia where ia.InvigilatorID = iv.InvigilatorID) 
                    //+ Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
                    //+ Dictionary[“ExaminationID”] = ExaminationID 
                    GlobalInfo Global = new GlobalInfo();
                    IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                    Dictionary["AppliedLevel"] = Global.AppliedLevel;
                    Dictionary["ExaminationID"] = ExaminationID;
                    IQueryable<Invigilator> lsInvigilator = InvigilatorBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);

                    List<Invigilator> lstInvigilator = new List<Invigilator>();
                    IQueryable<InvigilatorAssignment> lsInvigilatorAssignment = InvigilatorAssignmentBusiness.All.Where(o => o.ExaminationRoom.ExaminationID == ExaminationID && o.Invigilator.Examination.AppliedLevel == (int)Global.AppliedLevel.Value);
                    if (lsInvigilator.Count() > 0)
                    {
                        List<Invigilator> ListInvigilator = lsInvigilator.ToList();
                        List<int> lstInvigilatorAssignment = lsInvigilatorAssignment.Select(a => a.Invigilator.TeacherID).Distinct().ToList();
                        lstInvigilator = (from invi in ListInvigilator
                                          where !lstInvigilatorAssignment.Contains(invi.TeacherID)
                                          select invi).ToList();
                        List<InvigilatorAssignmentViewModel> ListIV = new List<InvigilatorAssignmentViewModel>();

                        var query = from a in lstInvigilator
                                    group a by new { a.TeacherID, a.ExaminationID } into gr
                                    select new
                                    {
                                        TeacherID = gr.Key.TeacherID,
                                        ExaminationID = gr.Key.ExaminationID,
                                        Count = gr.Select(a => a.Examination.ExaminationRooms).Count()
                                    };
                        ListIV = (from a in lstInvigilator
                                  join b in query on new { a.TeacherID, a.ExaminationID } equals new { b.TeacherID, b.ExaminationID }
                                  select new InvigilatorAssignmentViewModel
                                  {

                                      InvigilatorID = a.InvigilatorID,
                                      TeacherID = a.TeacherID,
                                      ExaminationID = a.ExaminationID,
                                      FullName = a.Employee.FullName,
                                      EmployeeCode = a.Employee.EmployeeCode,
                                      FacultyName = a.Employee.SchoolFaculty == null ? "" : a.Employee.SchoolFaculty.FacultyName,
                                      CountRoom = b.Count,
                                      ShortName = a.Employee.Name
                                  }).Distinct().ToList();

                        if (ListIV != null && ListIV.Count > 0)
                        {
                            ListIV = ListIV.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.ShortName + " " + p.FullName)).ToList();
                        }

                        ViewData[InvigilatorAssignmentConstants.REPLACE_INVIGILATOR] = ListIV;
                    }

                }
                return PartialView("_Replace", iarm);
            }


        }

        /// <summary>
        /// Replace
        /// </summary>
        /// <param name="ListReplaceInvigilator">The list replace invigilator.</param>
        /// <param name="iarm">The iarm.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Replace(int? InvigilatorID, InvigilatorAssignmentViewModel iarm)
        {
            if (InvigilatorID == null)
            {
                throw new BusinessException("Validate_NotInvigilator_Replace");
            }
            //Hệ thống hoán đổi giáo viên bằng cách gọi hàm InvigilatorAssignmentBusiness.
            //    SwatchInvigilator(UserInfo.SchoolID, UserInfo.AppliedLevel, InvigilatorAssignmentID, cboSubject.Value)
            GlobalInfo Global = new GlobalInfo();
            //Hệ thống gọi hàm InvigilatorAssignmentBusiness.AddInvigilatorForRoom(UserInfo.SchoolID, UserInfo.AppliedLevel, RoomID, dlInvigilator.Value)

            InvigilatorAssignmentBusiness.ChangeInvigilator(Global.SchoolID.Value, (int)Global.AppliedLevel.Value, iarm.InvigilatorAssignmentID, InvigilatorID.Value);
            InvigilatorAssignmentBusiness.Save();

            PrepareReplace(iarm.InvigilatorAssignmentID);
            return Json(new JsonMessage(Res.Get("Common_Label_ReplaceMessage")));
        }
        #endregion

        #region Hoán đổi giám thị
        /// <summary>
        /// PrepareChange
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult PrepareChange(int? id)
        {
            InvigilatorAssignment invigilatorassignment = this.InvigilatorAssignmentBusiness.Find(id);
            CheckPermissionForAction(invigilatorassignment.ExaminationRoom.ExaminationID, "Examination");
            ViewData[InvigilatorAssignmentConstants.CHANGE_INVIGILATOR] = new List<InvigilatorAssignmentViewModel>();
            ViewData[InvigilatorAssignmentConstants.LIST_CHANGESUBJECT] = new List<InvigilatorAssignmentViewModel>();
            ViewData[InvigilatorAssignmentConstants.CBO_CHANGESUBJECT] = new List<InvigilatorAssignmentBO>();
            if (id == 0)
            {
                return PartialView("_Change");
            }
            else
            {

                InvigilatorAssignmentViewModel iarm = new InvigilatorAssignmentViewModel();
                //Ta có InvigilatorAssignmentID từ người dùng truyền lên server
                //Từ InvigilatorAssignmentID => ExaminationID, RoomID, EducationLevelID
                InvigilatorAssignment ia = InvigilatorAssignmentBusiness.Find(id.Value);
                int? ExaminationID = ia.ExaminationRoom.ExaminationID;
                iarm.RoomID = ia.RoomID;
                iarm.RoomTitle = ia.ExaminationRoom.RoomTitle;
                iarm.InvigilatorAssignmentID = id.Value;
                iarm.InvigilatorName = ia.Invigilator.Employee.FullName;
                int EducationLevelID = ia.ExaminationRoom.EducationLevelID;
                iarm.EducationLevelID = EducationLevelID;
                // lblChange.Value = Tên giám thị lấy từ  InvigilatorAssignmentID

                //dlInvigilator: select from InvigilatorBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) iv where not exists 
                //                (select 1 from InvigilatorAssignment ia where ia.InvigilatorID = iv.InvigilatorID) 
                //+ Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
                //+ Dictionary[“ExaminationID”] = ExaminationID 
                GlobalInfo Global = new GlobalInfo();
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["AppliedLevel"] = Global.AppliedLevel;
                Dictionary["ExaminationID"] = ExaminationID;
                IQueryable<Invigilator> lsInvigilator = InvigilatorBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
                IQueryable<InvigilatorAssignment> lsInvigilatorAssignment = InvigilatorAssignmentBusiness.All
                    .Where(o => o.ExaminationRoom.ExaminationID == ExaminationID &&
                    o.Invigilator.Examination.AppliedLevel == (int)Global.AppliedLevel.Value
                    && o.RoomID != ia.RoomID
                    && o.InvigilatorAssignmentID != id
                    && o.InvigilatorID != ia.InvigilatorID);
                if (lsInvigilatorAssignment.Count() > 0)
                {

                    List<Invigilator> ListIV = lsInvigilatorAssignment.Select(a => a.Invigilator).Distinct().ToList();
                    List<InvigilatorAssignmentViewModel> ListIA = new List<InvigilatorAssignmentViewModel>();
                    foreach (Invigilator iv in ListIV)
                    {
                        InvigilatorAssignmentViewModel iavm = new InvigilatorAssignmentViewModel();
                        {
                            iavm.InvigilatorID = iv.InvigilatorID;
                            iavm.TeacherID = iv.TeacherID;
                            iavm.ExaminationID = iv.ExaminationID;
                            iavm.FullName = iv.Employee.EmployeeCode + "   " + iv.Employee.FullName + "   " + (iv.Employee.SchoolFaculty == null ? "  " : iv.Employee.SchoolFaculty.FacultyName);
                            ListIA.Add(iavm);
                        }
                    }

                    ViewData[InvigilatorAssignmentConstants.CHANGE_INVIGILATOR] = ListIA;
                }


                return PartialView("_Change", iarm);
            }


        }

        /// <summary>
        /// Change
        /// </summary>
        /// <param name="LisChangeInvigilator">The lis change invigilator.</param>
        /// <param name="iarm">The iarm.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Change(int LisChangeInvigilator, InvigilatorAssignmentViewModel iarm)
        {
            //InvigilatorAssignmentBusiness.SwatchInvigilator(UserInfo.SchoolID, UserInfo.AppliedLevel, InvigilatorID, cboSubject.Value)
            GlobalInfo Global = new GlobalInfo();
            //Hệ thống gọi hàm InvigilatorAssignmentBusiness.AddInvigilatorForRoom(UserInfo.SchoolID, UserInfo.AppliedLevel, RoomID, dlInvigilator.Value)
            InvigilatorAssignmentBusiness.SwatchInvigilator(Global.SchoolID.Value, (int)Global.AppliedLevel.Value, iarm.InvigilatorAssignmentID, iarm.SubjectID);

            InvigilatorAssignmentBusiness.Save();

            PrepareReplace(iarm.InvigilatorAssignmentID);
            return Json(new JsonMessage(Res.Get("Common_Label_ReplaceMessage")));
        }

        /// <summary>
        /// AjaxLoadChangeSubject
        /// </summary>
        /// <param name="idInvigilator">The id invigilator.</param>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idRoom">The id room.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadChangeSubject(int? idInvigilator, int? idEducationLevel, int? idRoom, int? idExamination)
        {

            ViewData[InvigilatorAssignmentConstants.CBO_CHANGESUBJECT] = new List<InvigilatorAssignmentBO>();
            if (idInvigilator == 0)
            {
                return Json(new SelectList(new List<InvigilatorAssignmentBO>(), "InvigilatorAssignmentID", "SubjectNameRoom"), JsonRequestBehavior.AllowGet);
            }

            GlobalInfo Global = new GlobalInfo();
            ExaminationRoom er = ExaminationRoomBusiness.Find(idRoom.Value);
            Invigilator iv = InvigilatorBusiness.Find(idInvigilator.Value);
            IDictionary<string, object> SearchSubject = new Dictionary<string, object>();
            SearchSubject["InvigilatorID"] = idInvigilator.Value;
            SearchSubject["AppliedLevel"] = Global.AppliedLevel;
            //SearchSubject["EducationLevelID"] = idEducationLevel.Value;
            SearchSubject["ExaminationID"] = idExamination.Value;

            IQueryable<InvigilatorAssignment> lsExaminationSubject = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, SearchSubject);
            List<InvigilatorAssignment> lstExamSubject = new List<InvigilatorAssignment>();
            List<InvigilatorAssignmentBO> ListIABO = new List<InvigilatorAssignmentBO>();
            List<EducationLevel> lstEducation = EducationLevelBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", Global.AppliedLevel } }).ToList();
            if (lsExaminationSubject.Count() > 0)
            {
                lstExamSubject = lsExaminationSubject.Distinct().ToList();

                List<InvigilatorAssignmentViewModel> ListIA = new List<InvigilatorAssignmentViewModel>();

                foreach (InvigilatorAssignment ia in lstExamSubject)
                {
                    InvigilatorAssignmentViewModel iavm = new InvigilatorAssignmentViewModel();
                    iavm.SubjectName = ia.ExaminationRoom.ExaminationSubject.SubjectCat.DisplayName;
                    iavm.RoomTitle = ia.ExaminationRoom.RoomTitle;
                    iavm.ExamDate = ia.ExaminationRoom.ExaminationSubject.StartTime.Value.Date;
                    iavm.ExamTime = ia.ExaminationRoom.ExaminationSubject.StartTime.Value.TimeOfDay;
                    ListIA.Add(iavm);
                    InvigilatorAssignmentBO iabo = new InvigilatorAssignmentBO();
                    iabo.InvigilatorAssignmentID = ia.InvigilatorAssignmentID;
                    iabo.SubjectNameRoom = ia.ExaminationRoom.ExaminationSubject.SubjectCat.DisplayName + " - " + lstEducation.Where(o => o.EducationLevelID == ia.ExaminationRoom.ExaminationSubject.EducationLevelID).FirstOrDefault().Resolution;
                    ListIABO.Add(iabo);
                }
                ViewData[InvigilatorAssignmentConstants.LIST_CHANGESUBJECT] = ListIA.ToList();
                ViewData[InvigilatorAssignmentConstants.CBO_CHANGESUBJECT] = ListIABO;

            }

            return Json(new SelectList(ListIABO, "InvigilatorAssignmentID", "SubjectNameRoom"), JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// AjaxLoadListSubject
        /// </summary>
        /// <param name="idInvigilator">The id invigilator.</param>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idRoom">The id room.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>12/13/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadListSubject(int? idInvigilator, int? idEducationLevel, int? idRoom)
        {
            ViewData[InvigilatorAssignmentConstants.LIST_CHANGESUBJECT] = new List<InvigilatorAssignmentViewModel>(); ;
            if (idInvigilator == null)
            {
                return PartialView("_ListChangeInvigilator");
            }

            GlobalInfo Global = new GlobalInfo();
            ExaminationRoom er = ExaminationRoomBusiness.Find(idRoom.Value);
            Invigilator iv = InvigilatorBusiness.Find(idInvigilator.Value);
            IDictionary<string, object> SearchSubject = new Dictionary<string, object>();
            SearchSubject["InvigilatorID"] = idInvigilator;
            SearchSubject["AppliedLevel"] = Global.AppliedLevel;
            //SearchSubject["EducationLevelID"] = idEducationLevel.Value;
            SearchSubject["ExaminationID"] = iv.ExaminationID;

            IQueryable<InvigilatorAssignment> lsExaminationSubject = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, SearchSubject);
            List<InvigilatorAssignment> lstExamSubject = new List<InvigilatorAssignment>();
            List<InvigilatorAssignmentBO> ListIABO = new List<InvigilatorAssignmentBO>();

            List<EducationLevel> lstEducation = EducationLevelBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", Global.AppliedLevel } }).ToList();
            if (lsExaminationSubject.Count() > 0)
            {
                lstExamSubject = lsExaminationSubject.Distinct().ToList();

                List<InvigilatorAssignmentViewModel> ListIA = new List<InvigilatorAssignmentViewModel>();

                foreach (InvigilatorAssignment ia in lstExamSubject)
                {
                    InvigilatorAssignmentViewModel iavm = new InvigilatorAssignmentViewModel();
                    iavm.SubjectName = ia.ExaminationRoom.ExaminationSubject.SubjectCat.DisplayName + " - " + lstEducation.Where(o => o.EducationLevelID == ia.ExaminationRoom.ExaminationSubject.EducationLevelID).FirstOrDefault().Resolution;
                    iavm.RoomTitle = ia.ExaminationRoom.RoomTitle;
                    iavm.ExamDate = ia.ExaminationRoom.ExaminationSubject.StartTime.Value.Date;
                    iavm.ExamTime = ia.ExaminationRoom.ExaminationSubject.StartTime.Value.TimeOfDay;
                    ListIA.Add(iavm);

                }
                ViewData[InvigilatorAssignmentConstants.LIST_CHANGESUBJECT] = ListIA.ToList();

            }

            return PartialView("_ListChangeInvigilator");

        }

        #endregion

        #region Xuat Excel
        public FileResult ExportExcel()
        {
            GlobalInfo Global = new GlobalInfo();

            int ExaminationID = int.Parse(Request["ExaminationID"]);
            Examination e = ExaminationBusiness.Find(ExaminationID);
            int EducationLevelID = int.Parse(Request["EducationLevelID"]);
            EducationLevel Edu = EducationLevelBusiness.Find(EducationLevelID);

            int SubjectID = int.Parse(Request["SubjectID"]);
            string Subject = ExaminationSubjectBusiness.Find(SubjectID).SubjectCat.DisplayName;

            int? ExaminationRoomID = null;
            if (Request["ExaminationRoomID"] != null && Request["ExaminationRoomID"] != "")
            {
                ExaminationRoomID = int.Parse(Request["ExaminationRoomID"]);
            }
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["ExaminationID"] = ExaminationID;
            Dictionary["ExaminationSubjectID"] = SubjectID;
            IQueryable<ExaminationRoom> lsExaminationRoom = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            List<ExaminationRoom> lstExaminationRoom = new List<ExaminationRoom>();
            if (lsExaminationRoom.Count() > 0)
            {
                lstExaminationRoom = lsExaminationRoom.OrderBy(o => o.RoomTitle).ToList();
            }
            if (ExaminationRoomID == null)
            {

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationID"] = ExaminationID;


                int SoPhongThi = lstExaminationRoom.Count;


                SheetData SheetData = new SheetData("HS_THCS_DSPhanCongGiamThi_Khoi6_MonCongNghe");
                SheetData.Data["AcademicYear"] = e.AcademicYear.DisplayTitle; ;
                SheetData.Data["SchoolName"] = e.SchoolProfile.SchoolName;
                SheetData.Data["ProvinceName"] = e.SchoolProfile.Province.ProvinceName;
                SheetData.Data["SupervisingDeptName"] = e.SchoolProfile.SupervisingDept.SupervisingDeptName;
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["EducationLevelName"] = Edu.Resolution;
                SheetData.Data["SubjectName"] = Subject;
                SheetData.Data["Examination"] = e.Title;
                SheetData.Data["AssignedDate"] = ExaminationSubjectBusiness.Find(SubjectID).StartTime.Value.ToString("dd/MM/yyyy");
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THCS_DSPHANCONGGIAMTHI_KHOI6_MONCONGNGHE);
                string templatePath = ReportUtils.GetTemplatePath(reportDef);
                string ReportName = "HS_THCS_DSPhanCongGiamThi_" + Edu.Resolution + "_" + Subject;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet Template = oBook.GetSheet(1);

                IVTWorksheet Sheet = oBook.GetSheet(2);
                Sheet.CopyPasteSameSize(Template.GetRange("A1", "G10"), 1, 1);
                Sheet.FillVariableValue(SheetData.Data);

                int StartRow = 11;

                int StartCol = 1;
                int Sohocsinh = 0;
                for (int i = 0; i < lstExaminationRoom.Count; i++)
                {
                    int SohocsinhData = 0;
                    int StartRowData = StartRow + Sohocsinh;
                    dic["RoomID"] = lstExaminationRoom[i].ExaminationRoomID;
                    IQueryable<InvigilatorAssignment> LsInvigilatorAssignment = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, dic);
                    List<InvigilatorAssignment> LstInvigilatorAssignment = new List<InvigilatorAssignment>();
                    if (LsInvigilatorAssignment.Count() > 0)
                    {
                        Sheet.CopyPaste(Template.GetRange("A11", "G11"), StartRowData, StartCol, true);
                        Sheet.SetCellValue(StartRow + Sohocsinh, StartCol, i + 1);
                        Sheet.SetCellValue(StartRow + Sohocsinh, StartCol + 1, lstExaminationRoom[i].RoomTitle);


                        LstInvigilatorAssignment = LsInvigilatorAssignment.ToList();

                        for (int j = 0; j < LstInvigilatorAssignment.Count; j++)
                        {
                            InvigilatorAssignment ia = LstInvigilatorAssignment[j];


                            Sheet.SetCellValue(StartRow + j + Sohocsinh, StartCol + 2, ia.Invigilator.Employee.EmployeeCode);
                            Sheet.SetCellValue(StartRow + j + Sohocsinh, StartCol + 3, ia.Invigilator.Employee.FullName);
                            Sheet.SetCellValue(StartRow + j + Sohocsinh, StartCol + 4, ia.Invigilator.Employee.BirthDate);
                            Sheet.SetCellValue(StartRow + j + Sohocsinh, StartCol + 5, ia.Invigilator.Employee.SchoolFaculty != null ?
                                ia.Invigilator.Employee.SchoolFaculty.FacultyName : string.Empty);
                            Sheet.SetCellValue(StartRow + j + Sohocsinh, StartCol + 6, ia.Invigilator.Employee.Telephone);
                            if (j < LstInvigilatorAssignment.Count - 1 && j > 0)
                            {
                                Sheet.CopyPaste(Template.GetRange("A12", "G12"), StartRow + j + Sohocsinh, StartCol, true);
                            }

                        }
                        Sohocsinh = Sohocsinh + LstInvigilatorAssignment.Count;
                        SohocsinhData = LstInvigilatorAssignment.Count;
                        Sheet.MergeColumn(1, StartRowData, StartRowData + SohocsinhData - 1);
                        Sheet.MergeColumn(2, StartRowData, StartRowData + SohocsinhData - 1);
                        Sheet.CopyPaste(Template.GetRange("A13", "G13"), StartRowData + SohocsinhData - 1, StartCol, true);
                        StartRowData = StartRowData + Sohocsinh;

                    }


                    if (LsInvigilatorAssignment.Count() == 0)
                    {
                        Sheet.CopyPaste(Template.GetRange("A13", "G13"), StartRowData, StartCol, true);
                        Sheet.SetCellValue(StartRow + Sohocsinh, StartCol, i + 1);
                        Sheet.SetCellValue(StartRow + Sohocsinh, StartCol + 1, lstExaminationRoom[i].RoomTitle);
                        Sohocsinh = Sohocsinh + 1;
                    }
                }


                oBook.GetSheet(1).Delete();
                Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName) + ".xls";
                result.FileDownloadName = ReportName;


                return result;
            }
            else
            {
                //List<ExaminationRoom> lstExaminationRoom = lsExaminationRoom.ToList();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationID"] = ExaminationID;
                dic["RoomID"] = ExaminationRoomID;

                int SoPhongThi = 1;
                int Sohocsinh = 0;

                SheetData SheetData = new SheetData("HS_THCS_DSPhanCongGiamThi_Khoi6_MonCongNghe");
                SheetData.Data["AcademicYear"] = e.AcademicYear.DisplayTitle; ;
                SheetData.Data["SchoolName"] = e.SchoolProfile.SchoolName;
                SheetData.Data["ProvinceName"] = e.SchoolProfile.Province.ProvinceName;
                SheetData.Data["SupervisingDeptName"] = e.SchoolProfile.SupervisingDept.SupervisingDeptName;
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["EducationLevelName"] = Edu.Resolution;
                SheetData.Data["SubjectName"] = Subject;
                SheetData.Data["Examination"] = e.Title;
                SheetData.Data["AssignedDate"] = ExaminationSubjectBusiness.Find(SubjectID).StartTime;

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THCS_DSPHANCONGGIAMTHI_KHOI6_MONCONGNGHE);
                string templatePath = ReportUtils.GetTemplatePath(reportDef);
                string ReportName = "HS_THCS_DSPhanCongGiamThi_" + Edu.Resolution + "_" + ReportUtils.StripVNSign(Subject);
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet Template = oBook.GetSheet(1);

                IVTWorksheet Sheet = oBook.GetSheet(2);
                Sheet.CopyPasteSameSize(Template.GetRange("A1", "G10"), 1, 1);
                Sheet.FillVariableValue(SheetData.Data);

                int StartRow = 11;
                int StartCol = 1;

                Sheet.SetCellValue(StartRow, StartCol, 1);
                Sheet.SetCellValue(StartRow, StartCol + 1, ExaminationRoomBusiness.Find(ExaminationRoomID).RoomTitle);
                //Sheet.CopyPaste(Template.GetRange("A11", "G11"), StartRow, 1, true);
                //Sheet.CopyPaste(Template.GetRange("A13", "G13"), StartRow + Sohocsinh-1, 1, true);

                IQueryable<InvigilatorAssignment> LsInvigilatorAssignment = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, dic);
                List<InvigilatorAssignment> LstInvigilatorAssignment = new List<InvigilatorAssignment>();
                if (LsInvigilatorAssignment.Count() > 0)
                {
                    LstInvigilatorAssignment = LsInvigilatorAssignment.ToList();
                    for (int j = 0; j < LstInvigilatorAssignment.Count; j++)
                    {
                        if (j == 0 && LstInvigilatorAssignment.Count != 1)
                        {
                            Sheet.CopyPaste(Template.GetRange("A11", "G11"), StartRow + j, 1, true);
                        }
                        else if (j == LstInvigilatorAssignment.Count - 1)
                        {
                            Sheet.CopyPaste(Template.GetRange("A13", "G13"), StartRow + j, 1, true);
                        }
                        else
                        {
                            Sheet.CopyPaste(Template.GetRange("A12", "G12"), StartRow + j, 1, true);
                        }
                        InvigilatorAssignment ia = LstInvigilatorAssignment[j];
                        Sheet.SetCellValue(StartRow + j, StartCol + 2, ia.Invigilator.Employee.EmployeeCode);
                        Sheet.SetCellValue(StartRow + j, StartCol + 3, ia.Invigilator.Employee.FullName);
                        Sheet.SetCellValue(StartRow + j, StartCol + 4, ia.Invigilator.Employee.BirthDate);
                        Sheet.SetCellValue(StartRow + j, StartCol + 5, ia.Invigilator.Employee.SchoolFaculty != null ?
                            ia.Invigilator.Employee.SchoolFaculty.FacultyName : string.Empty);
                        Sheet.SetCellValue(StartRow + j, StartCol + 6, ia.Invigilator.Employee.Telephone);

                    }
                    Sheet.GetRange(StartRow, 1, StartRow + LstInvigilatorAssignment.Count - 1, 1).Merge();
                    Sheet.GetRange(StartRow, 2, StartRow + LstInvigilatorAssignment.Count - 1, 2).Merge();

                }
                if (LsInvigilatorAssignment.Count() == 0)
                {

                }
                oBook.GetSheet(1).Delete();
                Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName) + ".xls";
                result.FileDownloadName = ReportName;

                return result;

            }


        }
        #endregion


        [ValidateAntiForgeryToken]
        public JsonResult CheckButtonAdd(int? idExamination)
        {
            GlobalInfo Global = new GlobalInfo();
            if (idExamination != null)
            {
                if (Global.IsCurrentYear == true && ExaminationBusiness.Find(idExamination.Value).CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
                {
                    return Json(JsonMessage.SUCCESS);
                }
            }
            return Json(JsonMessage.ERROR);
        }



    }

}





