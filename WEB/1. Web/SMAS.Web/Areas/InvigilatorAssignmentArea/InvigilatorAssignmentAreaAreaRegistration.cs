﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.InvigilatorAssignmentArea
{
    public class InvigilatorAssignmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "InvigilatorAssignmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "InvigilatorAssignmentArea_default",
                "InvigilatorAssignmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
