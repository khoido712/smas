/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.InvigilatorAssignmentArea.Models
{
    public class InvigilatorAssignmentViewModel
    {
		public int InvigilatorAssignmentID { get; set; }								
		public int InvigilatorID { get; set; }								
		public System.Int32 RoomID { get; set; }								
		public System.Nullable<System.DateTime> AssignedDate { get; set; }
        public int? ExaminationID { get; set; }
        public int? hidenSubjectID { get; set; }
       
        public int? CountTearcher { get; set; }
        public int? CountRoom { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("InvigilatorAssignment_Label_SelectInvigilator")]
        [Range(0, 10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public int? NumberOfInvigilator { get; set; }

        public int TeacherID { get; set; }
        public string InvigilatorName { get; set; }
        public int? EducationLevelID { get; set; }

        #region Grid
        [ResourceDisplayName("InvigilatorAssignment_Label_RoomTitle")]
        public string RoomTitle { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }

        public int? FacultyID { get; set; }

        [ResourceDisplayName("Employee_Label_FacultyName")]
        public string FacultyName { get; set; }
        
        [ResourceDisplayName("InvigilatorAssignment_Label_")]
        public string strRoom { get; set; }

        public int CountCandidate { get; set; }

        public string ShortName { get; set; }

        #endregion

        #region GridChange
        [ResourceDisplayName("InvigilatorAssignment_Label_SubjectName")]
        public string SubjectName{get;set;}
        [ResourceDisplayName("InvigilatorAssignment_Label_ExamDate")]
        public DateTime? ExamDate { get; set; }
        [ResourceDisplayName("InvigilatorAssignment_Label_ExamTime")]
        public TimeSpan? ExamTime { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("InvigilatorAssignment_Label_SubjectName")]
        public int SubjectID { get; set; }
        #endregion
    }
}


