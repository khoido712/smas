/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.InvigilatorAssignmentArea.Models
{
    public class SearchViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("InvigilatorAssignment_Label_Examination")]
        public int ExaminationID { get; set; }
        [ResourceDisplayName("InvigilatorAssignment_Label_Subject")]
        public int? ExaminationSubjectID { get; set; }
        [ResourceDisplayName("InvigilatorAssignment_Label_EducationLevel")]
        public int? EducationLevelID { get; set; }
        public int? ExaminationRoomID { get; set; }
    }
}