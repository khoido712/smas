﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamPupilViolateArea.Models
{
    public class ExamPupilViolateViewModel
    {
        public long ExamPupilViolateID { get; set; }
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public long SubjectID { get; set; }
        public long? ExamRoomID { get; set; }
        public long ExamPupilID { get; set; }

        [ResourceDisplayName("ViolatedInvigilator_Label_ViolationDetail")]
        public string ContentViolated { get; set; }

        public long ExamViolatetionTypeID { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_PupilCode")]
        public string PupilCode { get; set; }

        public string Name { get; set; }

        [ResourceDisplayName("ExamPupil_Grid_Label_PupilName")]
        public string FullName { get; set; }

        public string EthnicCode { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamSubjectName")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        public string ExamRoomCode { get; set; }

        [ResourceDisplayName("ExamPupilViolate_Column_ViolateTypeName")]
        public string ExamViolatetionTypeName { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_ExamineeNumber")]
        public string SBD { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_ClassName")]
        public string ClassName { get; set; }

        public bool IsUpdate { get; set; }
    }
}