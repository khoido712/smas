﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamPupilViolateArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilViolateConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NutritionalNorm_Validate_Require")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamGroup(this)")]
        public long Examinations { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilViolateConstants.LIST_EXAM_GROUP)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamSubjectAndExamRoom(this, 0)")]
        public long? ExamGroup { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilViolateConstants.LIST_EXAM_SUBJECT)]
        [AdditionalMetadata("Placeholder", "All")]
        public long? ExamSubject { get; set; }

        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilViolateConstants.LIST_EXAM_ROOM)]
        [AdditionalMetadata("Placeholder", "All")]
        public long? ExamRoom { get; set; }

        [ResourceDisplayName("ExamPupilViolate_Label_FullName")]
        [UIHint("Textbox")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }

        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilViolateConstants.LIST_EXAMINATIONS_INSERT)]
        [AdditionalMetadata("Placeholder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NutritionalNorm_Validate_Require")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamGroupInsert(this)")]
        public long ExaminationsID { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilViolateConstants.LIST_EXAM_GROUP)]
        [AdditionalMetadata("Placeholder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NutritionalNorm_Validate_Require")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamSubjectAndExamRoomInsert(this, 1)")]
        public long ExamGroupID { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilViolateConstants.LIST_EXAM_SUBJECT)]
        [AdditionalMetadata("Placeholder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NutritionalNorm_Validate_Require")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamPupilViolate()")]
        public long ExamSubjectID { get; set; }

        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamPupilViolateConstants.LIST_EXAM_ROOM)]
        [AdditionalMetadata("Placeholder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NutritionalNorm_Validate_Require")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamPupilViolate()")]
        public long ExamRoomID { get; set; }
    }
}