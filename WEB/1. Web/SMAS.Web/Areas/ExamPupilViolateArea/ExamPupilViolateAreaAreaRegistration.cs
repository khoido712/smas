﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamPupilViolateArea
{
    public class ExamPupilViolateAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamPupilViolateArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamPupilViolateArea_default",
                "ExamPupilViolateArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
