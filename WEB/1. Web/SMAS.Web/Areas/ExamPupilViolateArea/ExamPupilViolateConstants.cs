﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamPupilViolateArea
{
    public class ExamPupilViolateConstants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";
        public const string BTN_COMMON_ENABLE = "BtnCommonEnable";

        public const string LIST_EXAM_ROOM = "ListExamRoom";
        public const string LIST_EXAM_GROUP = "ListExamGroup";
        public const string LIST_EXAMINATIONS = "ListExaminations";
        public const string LIST_EXAMINATIONS_INSERT = "ListExaminationsInsert";
        public const string LIST_EXAM_SUBJECT = "ListExamSubject";
        public const string LIST_EXAM_VIOLATION_TYPE = "ListExamViolationType";

        public const string LIST_EXAM_PUPIL_VIOLATE = "ListExamPupilViolate";
        public const string LIST_EXAM_PUPIL_VIOLATE_INSERT = "ListExamPupilViolateInsert";
        public const string LIST_EXAM_PUPIL_VIOLATE_UPDATE = "ListExamPupilViolateUpdate";

        public const string HIDDEN_EXAMINATIONS_ID = "hidden_examinations_id";
        public const string HIDDEN_EXAM_GROUP_ID = "hidden_exam_group_id";
        public const string HIDDEN_EXAM_SUBJECT_ID = "hidden_exam_subject_id";
        public const string HIDDEN_EXAM_ROOM_ID = "hidden_exam_room_id";
        public const string HIDDEN_PUPIL_NAME = "hidden_pupil_name";
    }
}