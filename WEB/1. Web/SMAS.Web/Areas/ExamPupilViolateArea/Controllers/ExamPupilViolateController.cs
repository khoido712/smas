﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using System.Collections.Generic;
using SMAS.Web.Areas.ExamPupilViolateArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Web.Areas.ExamPupilViolateArea.Controllers
{
    public class ExamPupilViolateController : BaseController
    {
        private readonly IExamPupilViolateBusiness ExamPupilViolateBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        public ExamPupilViolateController(IExamPupilViolateBusiness ExamPupilViolateBusiness,
            IExaminationsBusiness ExaminationsBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness,
            IExamRoomBusiness ExamRoomBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.ExamPupilViolateBusiness = ExamPupilViolateBusiness;
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetViewData

        public void SetViewData()
        {
            SetViewDataPermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            IDictionary<string, object> searchInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"SchoolID", _globalInfo.SchoolID.Value },
                {"IsActive", true},
                {"IsAppliedForCandidate", true }
            };

            ViewData[ExamPupilViolateConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExamPupilViolateConstants.CURRENT_SEMESTER] = _globalInfo.Semester;

            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamPupilViolateConstants.LIST_EXAMINATIONS] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");
            List<Examinations> listExamInsert = listExam.Where(o => o.MarkInput == false && o.MarkClosing == false).ToList();
            ViewData[ExamPupilViolateConstants.LIST_EXAMINATIONS_INSERT] = new SelectList(listExamInsert, "ExaminationsID", "ExaminationsName");

            if (listExam != null && listExam.Count > 0)
            {
                Examinations firstExam = listExam.FirstOrDefault();
                search["ExaminationsID"] = firstExam.ExaminationsID;
                searchInsert["ExaminationsID"] = firstExam.ExaminationsID;

                List<ExamGroup> listGroup = GetExamGroupFromExaminations(firstExam.ExaminationsID);
                if (listGroup.Count > 0)
                {
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");
                }
                else
                {
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });                  
                }

                List<ExamPupilViolateViewModel> listPupilViolate = this._Search(search).ToList();
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE] = listPupilViolate;
               // IEnumerable<ExamPupilViolateViewModel> list = this._SearchInsert(search);// khong load mat dinh list insert khi moi vao chuc nang
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = new List<ExamPupilViolateViewModel>();

                if (_globalInfo.IsCurrentYear == true && firstExam.MarkInput.HasValue && firstExam.MarkInput.Value == false && firstExam.MarkClosing.HasValue && firstExam.MarkClosing.Value == false)
                {
                    ViewData[ExamPupilViolateConstants.BTN_COMMON_ENABLE] = true;
                }
                else
                {
                    ViewData[ExamPupilViolateConstants.BTN_COMMON_ENABLE] = false;
                }

                ViewData[ExamPupilViolateConstants.HIDDEN_EXAMINATIONS_ID] = firstExam.ExaminationsID;
                
            }
            else
            {
                ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                List<ExamPupilViolateViewModel> listPupilViolate = new List<ExamPupilViolateViewModel>();
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE] = listPupilViolate;
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = listPupilViolate;
                ViewData[ExamPupilViolateConstants.BTN_COMMON_ENABLE] = false;
            }

            ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
            ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });

        }

        #endregion SetViewData

        #region LoadCombobox

        private List<ExamGroup> GetExamGroupFromExaminations(long ExaminationsID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID", ExaminationsID},
            };

            List<ExamGroup> listGroup = ExamGroupBusiness.Search(dic).OrderBy(o => o.ExamGroupCode.ToUpper()).ToList();
            return listGroup;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamGroup(long? ExaminationsID)
        {
            if (ExaminationsID.HasValue && ExaminationsID > 0)
            {
                List<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID.Value);
                return Json(new SelectList(listGroup, "ExamGroupID", "ExamGroupName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private List<ExamSubjectBO> GetExamSubjectFromExamGroup(long ExamGroupID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "ExamGroupID", ExamGroupID }
            };

            List<ExamSubjectBO> listSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();
            return listSubject;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamSubject(long? ExamGroupID)
        {
            if (ExamGroupID.HasValue && ExamGroupID > 0)
            {
                List<ExamSubjectBO> listSubject = GetExamSubjectFromExamGroup(ExamGroupID.Value);
                return Json(new SelectList(listSubject, "SubjectID", "SubjectName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private List<ExamRoom> GetExamRoomFromExamSubject(long ExamGroupID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "ExamGroupID", ExamGroupID },
            };

            List<ExamRoom> listRoom = ExamRoomBusiness.Search(dic).OrderBy(o => o.ExamRoomCode).ToList();
            return listRoom;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamRoom(long? ExamGroupID)
        {
            if (ExamGroupID.HasValue && ExamGroupID > 0)
            {
                List<ExamRoom> listRoom = GetExamRoomFromExamSubject(ExamGroupID.Value);
                return Json(new SelectList(listRoom, "ExamRoomID", "ExamRoomCode"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion LoadCombobox

        #region Search

        
        public PartialViewResult Search(SearchViewModel form)
        {
            ViewData[ExamPupilViolateConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            Examinations exam = ExaminationsBusiness.Find(form.Examinations);
            if (exam != null && _globalInfo.IsCurrentYear == true && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                ViewData[ExamPupilViolateConstants.BTN_COMMON_ENABLE] = true;
            }
            else
            {
                ViewData[ExamPupilViolateConstants.BTN_COMMON_ENABLE] = false;
            }
            
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID);
            search.Add("ExaminationsID", form.Examinations);
            search.Add("ExamGroupID", form.ExamGroup);
            search.Add("SubjectID", form.ExamSubject);
            search.Add("ExamRoomID", form.ExamRoom);
            search.Add("FullName", form.FullName);

            IEnumerable<ExamPupilViolateViewModel> list = this._Search(search);
            ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE] = list;

            ViewData[ExamPupilViolateConstants.HIDDEN_EXAMINATIONS_ID] = form.Examinations;
            ViewData[ExamPupilViolateConstants.HIDDEN_EXAM_GROUP_ID] = form.ExamGroup;
            ViewData[ExamPupilViolateConstants.HIDDEN_EXAM_SUBJECT_ID] = form.ExamSubject;
            ViewData[ExamPupilViolateConstants.HIDDEN_EXAM_ROOM_ID] = form.ExamRoom;
            ViewData[ExamPupilViolateConstants.HIDDEN_PUPIL_NAME] = form.FullName;

            return PartialView("_List");
        }

        private List<ExamPupilViolateViewModel> _Search(IDictionary<string, object> search)
        {
            SetViewDataPermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<ExamPupilViolateViewModel> listPupilViolate = ExamPupilViolateBusiness.GetListExamPupilViolate(search)
                .Select(o => new ExamPupilViolateViewModel
                {
                    ExamPupilViolateID = o.ExamPupilViolateID,
                    ExaminationsID = o.ExaminationsID,
                    ExamRoomID = o.ExamRoomID,
                    ExamPupilID = o.ExamPupilID,
                    ExamGroupID = o.ExamGroupID,
                    SubjectID = o.SubjectID,
                    ContentViolated = o.ContentViolated,
                    ExamViolatetionTypeID = o.ExamViolatetionTypeID,
                    PupilCode = o.PupilCode,
                    Name = o.Name,
                    FullName = o.FullName,
                    EthnicCode = o.EthnicCode,
                    SubjectName = o.SubjectName,
                    ExamRoomCode = o.ExamRoomCode,
                    ExamViolatetionTypeName = o.ExamViolatetionTypeName,
                    SBD = o.SBD,
                    ClassName = o.ClassName
                }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName.getOrderingName(o.EthnicCode))).ToList();

            return listPupilViolate;
        }

        
        public PartialViewResult SearchInsert(long? ExaminationsID, long? ExamGroupID, int? ExamSubjectID, long? ExamRoomID)
        {
            ViewData[ExamPupilViolateConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;

            List<ExamPupilViolateViewModel> list = null;
            if (ExaminationsID.HasValue && ExamGroupID.HasValue && ExamSubjectID.HasValue && ExamRoomID.HasValue)
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                search["SchoolID"] = _globalInfo.SchoolID.Value;
                search["ExaminationsID"] = ExaminationsID.Value;
                search["ExamGroupID"] = ExamGroupID.Value;
                search["SubjectID"] = ExamSubjectID.Value;
                search["ExamRoomID"] = ExamRoomID.Value;

                list = this._SearchInsert(search);
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = list;
            }
            else
            {
                list = new List<ExamPupilViolateViewModel>();
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = list;
            }

            return PartialView("_ListInsert");
        }

        private List<ExamPupilViolateViewModel> _SearchInsert(IDictionary<string, object> search)
        {
            IDictionary<string, object> searchInfo = new Dictionary<string, object>()
            {
                {"SchoolID", _globalInfo.SchoolID.Value},
                {"IsActive", true},
                {"IsAppliedForCandidate", true}
            };
            IEnumerable<ExamViolationType> listExamViolationType = ExamPupilViolateBusiness.SearchViolationType(searchInfo).ToList();
            ViewData[ExamPupilViolateConstants.LIST_EXAM_VIOLATION_TYPE] = listExamViolationType;

            SetViewDataPermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<ExamPupilViolateViewModel> listPupilViolate = ExamPupilViolateBusiness.GetListExamPupil(search)
                .Select(o => new ExamPupilViolateViewModel
                {
                    ExaminationsID = o.ExaminationsID,
                    ExamPupilID = o.ExamPupilID,
                    ExamGroupID = o.ExamGroupID,
                    ExamRoomID = o.ExamRoomID,
                    PupilCode = o.PupilCode,
                    Name = o.Name,
                    FullName = o.FullName,
                    EthnicCode = o.EthnicCode,
                    SBD = o.SBD

                }).OrderBy(p=>p.SBD).ToList();

            return listPupilViolate;
        }

        /*
        public PartialViewResult SearchUpdate(long ExamPupilViolateID, long? ExaminationsID, long? ExamGroupID, int? ExamSubjectID, long? ExamRoomID)
        {
            IDictionary<string, object> searchInfo = new Dictionary<string, object>()
            {
                {"SchoolID", _globalInfo.SchoolID.Value },
                {"IsActive", true},
                {"IsAppliedForCandidate", true}
            };
            IEnumerable<ExamViolationType> listExamViolationType = ExamPupilViolateBusiness.SearchViolationType(searchInfo).ToList();
            ViewData[ExamPupilViolateConstants.LIST_EXAM_VIOLATION_TYPE] = listExamViolationType;

            SetViewDataPermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin);

            if (ExaminationsID.HasValue && ExamGroupID.HasValue && ExamSubjectID.HasValue && ExamRoomID.HasValue)
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                search["SchoolID"] = _globalInfo.SchoolID.Value;
                search["ExaminationsID"] = ExaminationsID.Value;
                search["ExamGroupID"] = ExamGroupID.Value;
                search["SubjectID"] = ExamSubjectID.Value;
                search["ExamRoomID"] = ExamRoomID.Value;

                List<ExamPupilViolateViewModel> list = this._Search(search);
                ExamPupilViolateViewModel temp = null;
                for (int i = 0; i < list.Count; i++)
                {
                    temp = list[i];
                    if (temp.ExamPupilViolateID == ExamPupilViolateID)
                    {
                        list[i].IsUpdate = true;
                        break;
                    }
                }

                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_UPDATE] = list;
            }
            else
            {
                List<ExamPupilViolateViewModel> list = new List<ExamPupilViolateViewModel>();
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_UPDATE] = list;
            }

            return PartialView("_ListUpdate");
        }*/

        #endregion Search

        #region Create

        public PartialViewResult GetDataInsert(long ExaminationsID, long? ExamGroupID, long? SubjectID, long? ExamRoomID)
        {
            SetViewDataPermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);

            IDictionary<string, object> searchInfo = new Dictionary<string, object>()
            {
                {"SchoolID", _globalInfo.SchoolID.Value },
                {"IsActive", true},
                {"IsAppliedForCandidate", true}
            };
            IEnumerable<ExamViolationType> listExamViolationType = ExamPupilViolateBusiness.SearchViolationType(searchInfo).ToList();
            ViewData[ExamPupilViolateConstants.LIST_EXAM_VIOLATION_TYPE] = listExamViolationType;

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);
            search.Add("MarkClosing", 1);
            search.Add("MarkInput", 1);

            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            List<ExamGroup> listGroup = null;
            List<ExamSubjectBO> listSubject = null;
            List<ExamRoom> listRoom = null;

            if (exam != null && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
                ViewData[ExamPupilViolateConstants.LIST_EXAMINATIONS_INSERT] = new SelectList(listExam, "ExaminationsID", "ExaminationsName", ExaminationsID);

                search["ExaminationsID"] = ExaminationsID;
                listGroup = GetExamGroupFromExaminations(ExaminationsID);
                if (ExamGroupID.HasValue && ExamGroupID > 0)
                {
                    search["ExamGroupID"] = ExamGroupID.Value;
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName", ExamGroupID);

                    listSubject = GetExamSubjectFromExamGroup(ExamGroupID.Value);
                    if (ExamGroupID.HasValue && SubjectID.HasValue)
                    {
                        search["SubjectID"] = SubjectID.Value;
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName", SubjectID);
                    }
                    else if (listSubject != null && listSubject.Count > 0)
                    {
                        search["SubjectID"] = listSubject.FirstOrDefault().SubjectID;
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");
                    }
                    else
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    }

                    listRoom = GetExamRoomFromExamSubject(ExamGroupID.Value);
                    if (ExamGroupID.HasValue && ExamRoomID.HasValue)
                    {
                        search["ExamRoomID"] = ExamRoomID.Value;
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode", ExamRoomID);
                    }
                    else if (listRoom != null && listRoom.Count > 0)
                    {
                        search["ExamRoomID"] = listRoom.FirstOrDefault().ExamRoomID;
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode");
                    }
                    else
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                    }
                }
                else if (listGroup != null && listGroup.Count > 0)
                {
                    search["ExamGroupID"] = listGroup.FirstOrDefault().ExamGroupID;
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");

                    listSubject = GetExamSubjectFromExamGroup(listGroup.FirstOrDefault().ExamGroupID);
                    if (listSubject != null && listSubject.Count > 0)
                    {
                        search["SubjectID"] = listSubject.FirstOrDefault().SubjectID;
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");
                    }
                    else
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    }

                    listRoom = GetExamRoomFromExamSubject(listGroup.FirstOrDefault().ExamGroupID);
                    if (listRoom != null && listRoom.Count > 0)
                    {
                        search["ExamRoomID"] = listRoom.FirstOrDefault().ExamRoomID;
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode");
                    }
                    else
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                    }
                }
                else
                {
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                }

                if (listGroup != null && listGroup.Count > 0 && listSubject != null && listSubject.Count > 0 && listRoom != null && listRoom.Count > 0)
                {
                    List<ExamPupilViolateViewModel> list = this._SearchInsert(search);
                    if (list != null && list.Count > 0)
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = list;
                    }
                    else
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = new List<ExamPupilViolateViewModel>();
                    }
                }
                else
                {
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = new List<ExamPupilViolateViewModel>();
                }
            }
            else
            {
                List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
                
                if (listExam != null && listExam.Count > 0)
                {
                    ViewData[ExamPupilViolateConstants.LIST_EXAMINATIONS_INSERT] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");

                    Examinations firstExam = listExam.FirstOrDefault();
                    search["ExaminationsID"] = firstExam.ExaminationsID;
                    listGroup = GetExamGroupFromExaminations(firstExam.ExaminationsID);

                    if (listGroup != null && listGroup.Count > 0)
                    {
                        search["ExamGroupID"] = listGroup.FirstOrDefault().ExamGroupID;
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");

                        listSubject = GetExamSubjectFromExamGroup(listGroup.FirstOrDefault().ExamGroupID);
                        if (listSubject != null && listSubject.Count > 0)
                        {
                            search["SubjectID"] = listSubject.FirstOrDefault().SubjectID;
                            ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");
                        }
                        else
                        {
                            ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                        }

                        listRoom = GetExamRoomFromExamSubject(listGroup.FirstOrDefault().ExamGroupID);
                        if (listRoom != null && listRoom.Count > 0)
                        {
                            search["ExamRoomID"] = listRoom.FirstOrDefault().ExamRoomID;
                            ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode");
                        }
                        else
                        {
                            ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                        }
                    }
                    else
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                    }
                }
                else
                {
                    ViewData[ExamPupilViolateConstants.LIST_EXAMINATIONS] = new SelectList(new string[] { });
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                }

                if (listGroup != null && listGroup.Count > 0 && listSubject != null && listSubject.Count > 0 && listRoom != null && listRoom.Count > 0)
                {
                    List<ExamPupilViolateViewModel> list = this._SearchInsert(search);
                    if (list != null && list.Count > 0)
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = list;
                    }
                    else
                    {
                        ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = new List<ExamPupilViolateViewModel>();
                    }
                }
                else
                {
                    ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_INSERT] = new List<ExamPupilViolateViewModel>();
                }
            }
            
            return PartialView("_Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(SearchViewModel model, long[] CheckPupilViolateInsert,
            long[] HiddenExamPupilID, string[] ContentViolate, int[] ExamViolateType)
        {
            if (CheckPupilViolateInsert == null)
            {
                return Json(new JsonMessage(Res.Get("Validate_Examinee_Pupil_Violate_Error"), "error"));
            }
            if (GetMenupermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            Examinations exam = ExaminationsBusiness.Find(model.ExaminationsID);
            if (exam != null && exam.MarkInput == false && exam.MarkClosing == false)
            {
                IDictionary<string, object> insertInfo = new Dictionary<string, object>()
                {
                    { "ExaminationsID", model.ExaminationsID },
                    { "ExamGroupID", model.ExamGroupID },
                    { "SubjectID", model.ExamSubjectID },
                    { "ExamRoomID", model.ExamRoomID }
                };
                ExamPupilViolateBusiness.InsertListExamPupilViolate(insertInfo, CheckPupilViolateInsert, HiddenExamPupilID, ContentViolate, ExamViolateType);
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ExamPupilViolate_Error_Examinations"), "error"));
            }
        }

        #endregion Create

        #region Update

        public PartialViewResult GetDataUpdate(long ExaminationsID, long ExamGroupID, long SubjectID, long ExamRoomID, long ExamPupilViolateID)
        {
            SetViewDataPermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);

            IDictionary<string, object> searchInfo = new Dictionary<string, object>()
            {
                {"SchoolID", _globalInfo.SchoolID.Value },
                {"IsActive", true},
                {"IsAppliedForCandidate", true}
            };
            IEnumerable<ExamViolationType> listExamViolationType = ExamPupilViolateBusiness.SearchViolationType(searchInfo).ToList();
            ViewData[ExamPupilViolateConstants.LIST_EXAM_VIOLATION_TYPE] = listExamViolationType;

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamPupilViolateConstants.LIST_EXAMINATIONS_INSERT] = new SelectList(listExam, "ExaminationsID", "ExaminationsName", ExaminationsID);

            search["ExaminationsID"] = ExaminationsID;
            List<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID);

            search["ExamGroupID"] = ExamGroupID;
            ViewData[ExamPupilViolateConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName", ExamGroupID);

            List<ExamSubjectBO> listSubject = GetExamSubjectFromExamGroup(ExamGroupID);
            search["SubjectID"] = SubjectID;
            ViewData[ExamPupilViolateConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName", SubjectID);

            List<ExamRoom> listRoom = GetExamRoomFromExamSubject(ExamGroupID);
            search["ExamRoomID"] = ExamRoomID;
            ViewData[ExamPupilViolateConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode", ExamRoomID);

            search["ExamPupilViolateID"] = ExamPupilViolateID;
            List<ExamPupilViolateViewModel> list = this._Search(search);           

            if (list != null && list.Count > 0)
            {
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_UPDATE] = list;
            }
            else
            {
                ViewData[ExamPupilViolateConstants.LIST_EXAM_PUPIL_VIOLATE_UPDATE] = new List<ExamPupilViolateViewModel>();
            }

            return PartialView("_Edit");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Update(long? CheckPupilViolateUpdate, string ContentViolate, int ExamViolateType, long ExaminationsID)
        {
            if (GetMenupermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (ExaminationsID > 0)
            {
                Examinations examinationsObj = ExaminationsBusiness.Find(ExaminationsID);
                if (examinationsObj != null && examinationsObj.MarkClosing == false && examinationsObj.MarkInput == false && CheckPupilViolateUpdate.HasValue)
                {
                    ExamPupilViolateBusiness.UpdateExamPupilViolate(CheckPupilViolateUpdate.Value, ContentViolate, ExamViolateType);
                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("ExamPupilViolate_Error_Examinations"), "error"));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ExamBag_Validate_Required_Examinations"), "error"));
            }
        }

        #endregion Update

        #region Delete

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCheck(string stringExamPupilViolate, long examinationsID)
        {
            if (GetMenupermission("ExamPupilViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (CheckExaminations(examinationsID))
            {
                return Json(new JsonMessage(Res.Get("ExamPupilViolate_Error_Examinations")));
            }
            string[] listDelete = stringExamPupilViolate.Split(',');
            List<long> listExamPupilViolate = new List<long>();
            for (int i = 0; i < listDelete.Length - 1; i++)
            {
                listExamPupilViolate.Add(long.Parse(listDelete[i]));
            }
            ExamPupilViolateBusiness.DeleteExamPupilViolate(listExamPupilViolate);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion Delete

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["SubjectID"] = form.ExamSubjectID;
            dic["ExamRoomID"] = form.ExamRoomID;
            dic["FullName"] = form.FullName;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_THI_SINH_VI_PHAM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ExamPupilViolateBusiness.CreateExamPupilViolateReport(dic);
                processedReport = ExamPupilViolateBusiness.InsertExamPupilViolateReport(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion

        private bool CheckExaminations(long examinationsID)
        {
            if (examinationsID > 0)
            {
                Examinations examinationsObj = ExaminationsBusiness.Find(examinationsID);
                if (examinationsObj != null && examinationsObj.MarkClosing == false && examinationsObj.MarkInput == false)
                {
                    return false;
                }
                return true;
            }
            else
            {
                return true;
            }
        }
    }
}
