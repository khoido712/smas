﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HealthInsuranceManagementArea.Models
{
    public class SearchViewModel
    {
        public int EducationLevelVM { get; set; }
        public int ClassVM { get; set; }
        public string StudentNameVM { get; set; }
        public int StatusVM { get; set; }
    }
}