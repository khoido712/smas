﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HealthInsuranceManagementArea.Models
{
    public class ImportViewModel
    {             
        public int? pupilID { get; set; }
        [ResourceDisplayName("PupilAbsent_Column_PupilCode")]
        public string pupilCode { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string fullName { get; set; }
        public DateTime? birthDate { get; set; }
        public string stringBirthDate { get; set; }
        public int schoolID { get; set; }
        public int academicID { get; set; }
        
        public int gender { get; set; }
        public string stringGender { get { return gender == 1 ? "Nam" : "Nữ"; } }
        public int? classID { get; set; }
        [ResDisplayName("Tên lớp")]
        public string className { get; set; }      
        [ResDisplayName("Số thẻ")]
        public string cardNumber { get; set; }
        [ResDisplayName("Địa chỉ")]
        public string address { get; set; }
        [ResDisplayName("Nơi đăng ký")]
        public string registerAddress { get; set; }
        [ResDisplayName("Mã")]
        public string cardCode { get; set; }
        [ResDisplayName("Hạn sử dụng từ")]
        public DateTime fromDate { get; set; }
        [ResDisplayName("Hạn sử dụng từ")]
        public string stringFromDate { get; set; }
        [ResDisplayName("Hạn sử dụng đến")]
        public DateTime toDate { get; set; }
        [ResDisplayName("Hạn sử dụng đến")]
        public string stringToDate { get; set; }
        [ResDisplayName("Ngày cấp thẻ")]
        public DateTime createRange { get { return DateTime.Now; }  }
        [ResDisplayName("Ngày cấp thẻ")]
        public string stringCreateRange { get; set; }
        [ResDisplayName("Nơi cấp thẻ")]
        public string placeRange { get; set; }

        public bool Status { get; set; }
        public string ErrorLabel { get; set; }
        

    }
}