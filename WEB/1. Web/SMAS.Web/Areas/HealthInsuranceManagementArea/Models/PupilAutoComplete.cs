﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HealthInsuranceManagementArea.Models
{
    public class PupilAutoComplete
    {
        [ResDisplayName("Họ và tên")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string PupilCode { get; set; }
        public int PupilID { get; set; }
        public string DisplayName { get; set; }
        public string StringDisplayName 
        {
            get {
                if (PupilCode == "" || PupilCode == null)
                    return "";
                else
                    return DisplayName + " - " + PupilCode; 
            }
        }

        public int EmployeeID { get; set; }
    }
}