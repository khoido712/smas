﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HealthInsuranceManagementArea.Models
{
    public class InsuranceViewModel
    {
        public int managementHealthInsuranceID { get; set; }
        public int educationLevelID { get; set; }
        public int classID { get; set; }
        [ResDisplayName("Lớp")]
        public string className { get; set; }

        public int studentID { get; set; }

        [ResDisplayName("Họ và tên")]
        public string studentName { get; set; }

        [ResDisplayName("Ngày sinh")]
        public DateTime? birthDate { get; set; }
        public int typeOfGender { get; set; }

        [ResDisplayName("Giới tính")]
        public string gender { get; set; }       
        public string address { get; set; }
        public string passportCode { get; set; }             
        public string stringDuedayTo { get; set; }
        public string stringCreateDate { get; set; }
        public string createPlace { get; set; }

        [ResDisplayName("Ngày sinh")]
        public string stringBirthDate { get; set; }
       
        [ResDisplayName("Nơi ĐK KCB")]
        public string passportPlace { get; set; }

        [ResDisplayName("Số thẻ")]
        public string ticketNumber { get; set; }

        [ResourceDisplayName("Hạn sử dụng từ")]
        public DateTime duedayFrom { get; set; }       
        [ResourceDisplayName("Đến")]
        public DateTime duedayTo { get; set; }

        [ResourceDisplayName("Ngày cấp")]
        public DateTime? createDate { get; set; }

        public string stringDuedayFrom { get; set; }
        public string pupilCode { get; set; }

        [ResourceDisplayName("Học sinh")]
        public int? PupilID { get; set; }
    }
}