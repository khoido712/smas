﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.HealthInsuranceManagementArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.HealthInsuranceManagementArea.Controllers
{
    public class HealthInsuranceManagementController : BaseController
    {
        //
        // GET: /HealthInsuranceManagementArea/HealthInsuranceManagement/

        #region properties
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IManagementHealthInsuranceBusiness ManagementHealthInsuranceBusiness;

        public HealthInsuranceManagementController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IManagementHealthInsuranceBusiness ManagementHealthInsuranceBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ManagementHealthInsuranceBusiness = ManagementHealthInsuranceBusiness;
        }
        #endregion

        public ActionResult Index()
        {
            //combobox khối
            var lstEducation = _globalInfo.EducationLevels.ToList();
            ViewData["EducationLevel"] = new SelectList(lstEducation, "EducationLevelID", "Resolution");

            //combobox lớp
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

            var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                    .Where(x => x.IsActive == true)
                                                    .OrderBy(u => u.EducationLevelID)
                                                    .ThenBy(u => u.OrderNumber)
                                                    .ThenBy(u => u.DisplayName).ToList();
            ViewData["Class"] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            //conbobox tình trang
            ViewData["TypeOfInsuranceTicket"] = new SelectList(CommonList.TypeOfInsuranceTicket(), "key", "value");
            return View();
        }

        private static List<int> lstPupilStatis;
        [ValidateInput(true)]
        public PartialViewResult Search(SearchViewModel frm, GridCommand command)
        {
            Utils.Utils.TrimObject(frm);

            this.SetViewDataPermission("HealthInsuranceManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<InsuranceViewModel> listResult = new List<InsuranceViewModel>();
            InsuranceViewModel objResult = null;
            string studentNameVM = !string.IsNullOrEmpty(frm.StudentNameVM) ? frm.StudentNameVM.ToString() : "";
            int? educationLevelVM = frm.EducationLevelVM;
            int? classVM = frm.ClassVM;
            int? statusVM = frm.StatusVM;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (educationLevelVM != 0)
                dic["EducationLevelID"] = educationLevelVM;
            if (classVM != 0)
                dic["ClassID"] = classVM;
            if (studentNameVM != "")
                dic["FullName"] = studentNameVM;

            IQueryable<PupilOfClass> lstPOC = PupilOfClassBusiness.Search(dic).Where(x => x.Status == 1);
            if (studentNameVM != "")
                lstPOC = lstPOC.Where(x => x.PupilProfile.FullName == studentNameVM);

            IQueryable<ManagementHealthInsurance> lstMHI = ManagementHealthInsuranceBusiness.Search(dic);
            ManagementHealthInsurance objMHI = null;
            List<int> lstPupExistMHI = lstMHI.Select(x => x.PupilID).ToList();
            if (statusVM != 0)
            {
                if (statusVM == 1)
                {
                    lstPOC = lstPOC.Where(x => lstPupExistMHI.Contains(x.PupilID));
                }
                else if (statusVM == 2)
                {
                    lstPOC = lstPOC.Where(x => !lstPupExistMHI.Contains(x.PupilID));
                }
            }

            int totalRecord = lstPOC.Count();
            ViewData["Total"] = totalRecord;
            lstPOC = lstPOC.OrderBy(x => x.ClassProfile.EducationLevelID)
                .ThenBy(x => x.ClassProfile.OrderNumber)
                .ThenBy(x => x.ClassProfile.DisplayName)
                .ThenBy(x => x.OrderInClass)
                .ThenBy(x => x.PupilProfile.Name)
                .ThenBy(x => x.PupilProfile.FullName)
                .Take(20);
            lstPupilStatis = lstPOC.Select(x => x.PupilID).ToList();

            foreach (var item in lstPOC)
            {
                objResult = new InsuranceViewModel();
                objResult.studentName = item.PupilProfile.FullName;
                objResult.className = item.ClassProfile.DisplayName;
                objResult.stringBirthDate = item.PupilProfile.BirthDate.ToShortDateString();
                objResult.gender = item.PupilProfile.Genre == 1 ? "Nam" : "Nữ";
                objResult.PupilID = item.PupilID;
                objResult.classID = item.ClassID;

                objMHI = lstMHI.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                if (objMHI != null)
                {
                    objResult.managementHealthInsuranceID = objMHI.ManagementHealthInsuranceID;
                    objResult.ticketNumber = objMHI.CardNumber;
                    objResult.passportPlace = objMHI.RegisterAddress;
                    objResult.stringDuedayFrom = objMHI.FromDate.ToShortDateString();
                    objResult.stringDuedayTo = objMHI.ToDate.ToShortDateString();
                }
                listResult.Add(objResult);
            }

            //ViewData["Total"] = 0;
            //return PartialView("_ListToTeacher", listResult);

            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchExamPupilAjax(SearchViewModel frm, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(frm);

            List<InsuranceViewModel> listResult = new List<InsuranceViewModel>();
            InsuranceViewModel objResult = null;
            string studentNameVM = !string.IsNullOrEmpty(frm.StudentNameVM) ? frm.StudentNameVM.ToString() : "";
            int? educationLevelVM = frm.EducationLevelVM;
            int? classVM = frm.ClassVM;
            int? statusVM = frm.StatusVM;
            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (educationLevelVM != 0)
                dic["EducationLevelID"] = educationLevelVM;
            if (classVM != 0)
                dic["ClassID"] = classVM;
            if (studentNameVM != "")
                dic["FullName"] = studentNameVM;

            IQueryable<PupilOfClass> lstPOC = PupilOfClassBusiness.Search(dic).Where(x => x.Status == 1);
            if (studentNameVM != "")
                lstPOC = lstPOC.Where(x => x.PupilProfile.FullName == studentNameVM);

            IQueryable<ManagementHealthInsurance> lstMHI = ManagementHealthInsuranceBusiness.Search(dic);
            ManagementHealthInsurance objMHI = null;
            List<int> lstPupExistMHI = lstMHI.Select(x => x.PupilID).ToList();
            if (statusVM != 0)
            {
                if (statusVM == 1)
                {
                    lstPOC = lstPOC.Where(x => lstPupExistMHI.Contains(x.PupilID));
                }
                else if (statusVM == 2)
                {
                    lstPOC = lstPOC.Where(x => !lstPupExistMHI.Contains(x.PupilID));
                }
            }

            int totalRecord = lstPOC.Count();
            ViewData["Total"] = totalRecord;
            lstPOC = lstPOC.OrderBy(x => x.ClassProfile.EducationLevelID)
                .ThenBy(x => x.ClassProfile.OrderNumber)
                .ThenBy(x => x.ClassProfile.DisplayName)
                .ThenBy(x => x.OrderInClass)
                .ThenBy(x => x.PupilProfile.Name)
                .ThenBy(x => x.PupilProfile.FullName)
                .Skip((currentPage - 1) * pageSize).Take(20);
            lstPupilStatis = lstPOC.Select(x => x.PupilID).ToList();

            foreach (var item in lstPOC)
            {
                objResult = new InsuranceViewModel();
                objResult.studentName = item.PupilProfile.FullName;
                objResult.className = item.ClassProfile.DisplayName;
                objResult.stringBirthDate = item.PupilProfile.BirthDate.ToShortDateString();
                objResult.gender = item.PupilProfile.Genre == 1 ? "Nam" : "Nữ";
                objMHI = lstMHI.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                objResult.PupilID = item.PupilID;
                objResult.classID = item.ClassID;
                if (objMHI != null)
                {
                    objResult.managementHealthInsuranceID = objMHI.ManagementHealthInsuranceID;
                    objResult.ticketNumber = objMHI.CardNumber;
                    objResult.passportPlace = objMHI.RegisterAddress;
                    objResult.stringDuedayFrom = objMHI.FromDate.ToShortDateString();
                    objResult.stringDuedayTo = objMHI.ToDate.ToShortDateString();
                }
                listResult.Add(objResult);
            }

            return View(new GridModel<InsuranceViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection frm)
        {
            int pupilID = !string.IsNullOrEmpty(frm["PupilID"]) ? int.Parse(frm["PupilID"]) : 0;
            if (pupilID == 0) //“[Tên trường thông tin] không được phép rỗng
                return Json(new JsonMessage(Res.Get("Học sinh không được phép rỗng"), "error"));

            if (PupilProfileBusiness.Find(pupilID) == null)
                return Json(new JsonMessage(Res.Get("Common_Error_DataInputError"), "error"));

            int classID = !string.IsNullOrEmpty(frm["classPU"]) ? int.Parse(frm["classPU"]) : 0;
            if (ClassProfileBusiness.Find(classID) == null)
                return Json(new JsonMessage(Res.Get("Common_Error_DataInputError"), "error"));

            string ticketNumber = !string.IsNullOrEmpty(frm["ticketNumber"]) ? frm["ticketNumber"].ToString() : "";
            if (ticketNumber == "")
                return Json(new JsonMessage(Res.Get("Số thẻ không được phép rỗng"), "error"));

            string address = !string.IsNullOrEmpty(frm["address"]) ? frm["address"].ToString() : "";
            string passportPlace = !string.IsNullOrEmpty(frm["passportPlace"]) ? frm["passportPlace"].ToString() : "";
            if (passportPlace == "")
                return Json(new JsonMessage(Res.Get("Nơi ĐK KCB không được phép rỗng"), "error"));

            string passportCode = !string.IsNullOrEmpty(frm["passportCode"]) ? frm["passportCode"].ToString() : "";
            string stringFromDate = !string.IsNullOrEmpty(frm["duedayFrom"]) ? frm["duedayFrom"].ToString() : "";
            if (stringFromDate == "")
                return Json(new JsonMessage(Res.Get("Hạn sử dụng từ không được phép rỗng"), "error"));
            if (!checkDateType(stringFromDate))
                return Json(new JsonMessage(Res.Get("Hạn sử dụng từ không hợp lệ"), "error"));
            DateTime duedayFrom = DateTime.Parse(stringFromDate);

            string stringToDate = !string.IsNullOrEmpty(frm["duedayTo"]) ? frm["duedayTo"].ToString() : "";
            if (stringToDate == "")
                return Json(new JsonMessage(Res.Get("Đến không được phép rỗng"), "error"));
            if (!checkDateType(stringToDate))
                return Json(new JsonMessage(Res.Get("Đến không hợp lệ"), "error"));
            DateTime duedayTo = DateTime.Parse(stringToDate);

            AcademicYear objacademic = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            if (duedayFrom >= duedayTo)
            {
                return Json(new JsonMessage(Res.Get("Hạn sử dụng không đúng, thầy cô vui lòng kiểm tra lại"), "error"));
            }
            if (duedayFrom > objacademic.SecondSemesterEndDate || duedayTo < objacademic.FirstSemesterStartDate)
            {
                return Json(new JsonMessage(Res.Get("Hạn sử dụng không đúng, thầy cô vui lòng kiểm tra lại"), "error"));
            }

            string stringCreateDate = !string.IsNullOrEmpty(frm["createDate"]) ? frm["createDate"].ToString() : "";
            DateTime? createDate;
            if (stringCreateDate == "")
                createDate = null;
            else if (!checkDateType(stringCreateDate))
                return Json(new JsonMessage(Res.Get("Ngày cấp không hợp lệ"), "error"));
            else
                createDate = DateTime.Parse(stringCreateDate);

            string createPlace = !string.IsNullOrEmpty(frm["createPlace"]) ? frm["createPlace"].ToString() : "";

            // Check có tồn tạu trong PupilOfClass không 
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = pupilID;
            dic["ClassID"] = classID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            PupilOfClass objPOC = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(x => x.Status == 1).FirstOrDefault();

            if (objPOC != null) 
            {                
                ManagementHealthInsurance objMHI = new ManagementHealthInsurance();
                objMHI.SchoolID = _globalInfo.SchoolID.Value;
                objMHI.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objMHI.ClassID = classID;
                objMHI.PupilID = pupilID;
                objMHI.CardNumber = ticketNumber;
                objMHI.Address = address;
                objMHI.RegisterAddress = passportPlace;
                objMHI.CardCode = passportCode;
                objMHI.FromDate = duedayFrom;
                objMHI.ToDate = duedayTo;
                objMHI.DateRange = createDate;
                objMHI.PlaceRange = createPlace;
                objMHI.CreateDate = DateTime.Now;

                ManagementHealthInsuranceBusiness.Create(objMHI);
            }

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }

        [ValidateAntiForgeryToken]
        public ActionResult Update(FormCollection frm)
        {
            int managementID = SMAS.Business.Common.Utils.GetInt(frm["managementHealthInsuranceID"]);
            if (ManagementHealthInsuranceBusiness.Find(managementID) == null)
                return Json(new JsonMessage(Res.Get("Common_Error_DataInputError"), "success"));

            int pupilID = SMAS.Business.Common.Utils.GetInt(frm["PupilIDUpdate"]);
            if (pupilID == 0)
                return Json(new JsonMessage(Res.Get("Học sinh không được phép rỗng"), "error"));
            if (PupilProfileBusiness.Find(pupilID) == null)
                return Json(new JsonMessage(Res.Get("Common_Error_DataInputError"), "error"));

            int classID = SMAS.Business.Common.Utils.GetInt(frm["classPU1"]);
            if (ClassProfileBusiness.Find(classID) == null)
                return Json(new JsonMessage(Res.Get("Common_Error_DataInputError"), "error"));

            string ticketNumber = !string.IsNullOrEmpty(frm["ticketNumber1"]) ? frm["ticketNumber1"].ToString() : "";
            if (ticketNumber == "")
                return Json(new JsonMessage(Res.Get("Số thẻ không được phép rỗng"), "error"));

            string address = !string.IsNullOrEmpty(frm["address1"]) ? frm["address1"].ToString() : "";
            string passportPlace = !string.IsNullOrEmpty(frm["passportPlace1"]) ? frm["passportPlace1"].ToString() : "";
            if (passportPlace == "")
                return Json(new JsonMessage(Res.Get("Nơi ĐK KCB không được phép rỗng"), "error"));

            string passportCode = !string.IsNullOrEmpty(frm["passportCode1"]) ? frm["passportCode1"].ToString() : "";
            string stringFromDate = !string.IsNullOrEmpty(frm["duedayFrom1"]) ? frm["duedayFrom1"].ToString() : "";
            if (stringFromDate == "")
                return Json(new JsonMessage(Res.Get("Hạn sử dụng từ không được phép rỗng"), "error"));
            if (!checkDateType(stringFromDate))
                return Json(new JsonMessage(Res.Get("Hạn sử dụng từ không hợp lệ"), "error"));
            DateTime duedayFrom = DateTime.Parse(stringFromDate);

            string stringToDate = !string.IsNullOrEmpty(frm["duedayTo1"]) ? frm["duedayTo1"].ToString() : "";
            if (stringToDate == "")
                return Json(new JsonMessage(Res.Get("Đến không được phép rỗng"), "error"));
            if (!checkDateType(stringToDate))
                return Json(new JsonMessage(Res.Get("Đến không hợp lệ"), "error"));
            DateTime duedayTo = DateTime.Parse(stringToDate);

            AcademicYear objacademic = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            if (duedayFrom >= duedayTo)
            {
                return Json(new JsonMessage(Res.Get("Hạn sử dụng không đúng, thầy cô vui lòng kiểm tra lại"), "error"));
            }
            if (duedayFrom > objacademic.SecondSemesterEndDate || duedayTo < objacademic.FirstSemesterStartDate)
            {
                return Json(new JsonMessage(Res.Get("Hạn sử dụng không đúng, thầy cô vui lòng kiểm tra lại"), "error"));
            }

            string stringCreateDate = !string.IsNullOrEmpty(frm["createDate1"]) ? frm["createDate1"].ToString() : "";
            DateTime? createDate;
            if (stringCreateDate == "")
                createDate = null;
            else if (!checkDateType(stringCreateDate))
                return Json(new JsonMessage(Res.Get("Ngày cấp không hợp lệ"), "error"));
            else
                createDate = DateTime.Parse(stringCreateDate);

            string createPlace = !string.IsNullOrEmpty(frm["createPlace1"]) ? frm["createPlace1"].ToString() : "";

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = pupilID;
            dic["ClassID"] = classID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            PupilOfClass objPOC = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(x => x.Status == 1).FirstOrDefault();

            if (objPOC != null)
            {
                ManagementHealthInsurance objMHI = new ManagementHealthInsurance();
                objMHI.ManagementHealthInsuranceID = managementID;
                objMHI.SchoolID = _globalInfo.SchoolID.Value;
                objMHI.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objMHI.ClassID = classID;
                objMHI.PupilID = pupilID;
                objMHI.CardNumber = ticketNumber;
                objMHI.Address = address;
                objMHI.RegisterAddress = passportPlace;
                objMHI.CardCode = passportCode;
                objMHI.FromDate = duedayFrom;
                objMHI.ToDate = duedayTo;
                objMHI.DateRange = createDate;
                objMHI.PlaceRange = createPlace;
                objMHI.ModifiedDate = DateTime.Now;

                ManagementHealthInsuranceBusiness.UpdateDB(objMHI);
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }

        public ActionResult Delete(FormCollection frm)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<ManagementHealthInsurance> lstMHI = ManagementHealthInsuranceBusiness.Search(dic).Where(x => lstPupilStatis.Contains(x.PupilID)).ToList();
            List<ManagementHealthInsurance> lstMHIDelete = new List<ManagementHealthInsurance>();
            ManagementHealthInsurance objMHI = null;
            foreach (var item in lstPupilStatis)
            {
                if (!string.IsNullOrEmpty(frm["chk_" + item]))
                {
                    objMHI = lstMHI.Where(x => x.PupilID == item).FirstOrDefault();
                    if (objMHI != null)
                        lstMHIDelete.Add(objMHI);
                }
            }
            ManagementHealthInsuranceBusiness.DeleteAll(lstMHIDelete);
            ManagementHealthInsuranceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }

        public FileResult ExportFile(string educationVM, string classVM, string pupilNameVM, string statusVM)
        {
            String filename = "";
            Stream excel = null;

            excel = this.Export(educationVM, classVM, pupilNameVM, statusVM, out filename);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;

            return result;
        }

        public Stream Export(string educationVM, string classV, string pupilNameVM, string statusV, out string FileName)
        {
            string reportCode = SystemParamsInFile.DS_CAPTHE_BHYT;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName + ".xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            string outputNamePattern = reportDef.OutputNamePattern;
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            //Lấy sheet template
            IVTWorksheet sheet = oBook.GetSheet(1);
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            sheet.SetCellValue("A1", school.SupervisingDept.SupervisingDeptName.ToUpper());
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            AcademicYear academic = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            sheet.SetCellValue("A5", "NĂM HỌC " + academic.DisplayTitle);

            #region Lấy danh sách cần thiết

            List<InsuranceViewModel> listResult = new List<InsuranceViewModel>();
            InsuranceViewModel objResult = null;
            string studentNameVM = pupilNameVM;
            int? educationLevelVM = Int32.Parse(educationVM);
            int? classVM = Int32.Parse(classV);
            int? statusVM = Int32.Parse(statusV);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (educationLevelVM != 0)
                dic["EducationLevelID"] = educationLevelVM;
            if (classVM != 0)
                dic["ClassID"] = classVM;
            if (studentNameVM != "0")
                dic["FullName"] = studentNameVM;

            IQueryable<PupilOfClass> lstPOC = PupilOfClassBusiness.Search(dic).Where(x => x.Status == 1);
            List<ManagementHealthInsurance> lstMHI = ManagementHealthInsuranceBusiness.Search(dic).ToList();
            ManagementHealthInsurance objMHI = null;
            List<int> lstPupExistMHI = lstMHI.Select(x => x.PupilID).ToList();
            if (statusVM != 0)
            {
                if (statusVM == 1)
                {
                    lstPOC = lstPOC.Where(x => lstPupExistMHI.Contains(x.PupilID));
                }
                else if (statusVM == 2)
                {
                    lstPOC = lstPOC.Where(x => !lstPupExistMHI.Contains(x.PupilID));
                }
            }
            lstPOC = lstPOC.OrderBy(x => x.ClassProfile.EducationLevelID)
                .ThenBy(x => x.ClassProfile.OrderNumber)
                .ThenBy(x => x.ClassProfile.DisplayName)
                .ThenBy(x => x.OrderInClass)
                .ThenBy(x => x.PupilProfile.Name)
                .ThenBy(x => x.PupilProfile.FullName);
            lstPupilStatis = lstPOC.Select(x => x.PupilID).ToList();


            foreach (var item in lstPOC)
            {
                objResult = new InsuranceViewModel();
                objResult.studentName = item.PupilProfile.FullName;
                objResult.className = item.ClassProfile.DisplayName;
                objResult.stringBirthDate = item.PupilProfile.BirthDate.ToShortDateString();
                objResult.gender = item.PupilProfile.Genre == 1 ? "Nam" : "Nữ";
                objResult.PupilID = item.PupilID;
                objResult.pupilCode = item.PupilProfile.PupilCode;
                objMHI = lstMHI.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                if (objMHI != null)
                {
                    objResult.passportCode = objMHI.CardCode;
                    objResult.ticketNumber = objMHI.CardNumber;
                    objResult.address = objMHI.Address;
                    objResult.passportPlace = objMHI.RegisterAddress;
                    objResult.stringDuedayFrom = objMHI.FromDate.ToShortDateString();
                    objResult.stringDuedayTo = objMHI.ToDate.ToShortDateString();
                    objResult.stringCreateDate = objMHI.DateRange == null ? "" : objMHI.DateRange.Value.ToShortDateString();
                    objResult.createPlace = objMHI.PlaceRange;
                }
                listResult.Add(objResult);
            }

            #endregion


            int startRow = 8; int startColumn;
            int stt = 0;

            foreach (var item in listResult)
            {
                startColumn = 1;
                stt++;
                sheet.SetCellValue(startRow, startColumn, stt); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.pupilCode); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.studentName); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.className); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.stringBirthDate); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.gender); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.ticketNumber); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.address); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.passportPlace); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.passportCode); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.stringDuedayFrom); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.stringDuedayTo); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.stringCreateDate); startColumn++;
                sheet.SetCellValue(startRow, startColumn, item.createPlace);
                startRow++;
            }
            sheet.GetRange(8, 1, startRow - 1, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            //sheet.Delete();
            return oBook.ToStream();
        }

        public ActionResult LoadClass(int educationLevelVM)
        {
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            List<SelectListItem> lstClass = new List<SelectListItem>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

            if (educationLevelVM != 0)
                dicClass.Add("EducationLevelID", educationLevelVM);

            lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.EducationLevelID >= 1 && x.EducationLevelID <= 12).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();

            return Json(lstClass);
        }

        public PartialViewResult InsertPopUp(string classV, string EducationLevelVM, string PupilIDVM)
        {
            int classVM = Int32.Parse(classV);
            int pupilVM = Int32.Parse(PupilIDVM);
            ClassProfile objClass = ClassProfileBusiness.Find(classVM);
            if (objClass == null)
            {
                var lstEducation = _globalInfo.EducationLevels.ToList();
                int educationLevelID = lstEducation.First().EducationLevelID;
                int educationLevelVM = Int32.Parse(EducationLevelVM);
                if (educationLevelVM != 0)
                {
                    educationLevelID = educationLevelVM;
                }
                ViewData["EducationLevelPP"] = new SelectList(lstEducation, "EducationLevelID", "Resolution", educationLevelID);
                //combobox lớp
                IDictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

                var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                        .Where(x => x.IsActive == true && x.EducationLevelID == educationLevelID)
                                                        .OrderBy(u => u.EducationLevelID)
                                                        .ThenBy(u => u.OrderNumber)
                                                        .ThenBy(u => u.DisplayName).ToList();
                ViewData["ClassPP"] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

                return PartialView("_InsertPopUp",
                new InsuranceViewModel()
                {
                    classID = classVM,
                    educationLevelID = educationLevelID,
                    duedayFrom = DateTime.Now.Date,
                    duedayTo = DateTime.Now.Date.AddMonths(12),

                });
            }
            else
            {
                var lstEducation = _globalInfo.EducationLevels.ToList();
                ViewData["EducationLevelPP"] = new SelectList(lstEducation, "EducationLevelID", "Resolution", objClass.EducationLevelID);
                //combobox lớp
                IDictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

                var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                        .Where(x => x.IsActive == true && x.EducationLevelID == objClass.EducationLevelID)
                                                        .OrderBy(u => u.EducationLevelID)
                                                        .ThenBy(u => u.OrderNumber)
                                                        .ThenBy(u => u.DisplayName).ToList();
                ViewData["ClassPP"] = new SelectList(lstClass, "ClassProfileID", "DisplayName", classVM);
                InsuranceViewModel model = new InsuranceViewModel();
                model.classID = classVM;
                model.educationLevelID = objClass.EducationLevelID;
                model.duedayFrom = DateTime.Now.Date;
                model.duedayTo = DateTime.Now.Date.AddMonths(12);

                PupilProfile pp = PupilProfileBusiness.Find(pupilVM);
                if (pp != null)
                {
                    model.stringBirthDate = pp.BirthDate.ToShortDateString();
                    model.gender = pp.Genre == 1 ? "Nam" : "Nữ";
                    model.pupilCode = pp.PupilCode;
                    model.PupilID = pp.PupilProfileID;
                    model.studentName = pp.FullName;
                }
                return PartialView("_InsertPopUp", model);
            }
        }

        public PartialViewResult UpdatePopUp(string managementID)
        {
            int managementHealthInsuranceID = Int32.Parse(managementID);
            ManagementHealthInsurance objMHI = ManagementHealthInsuranceBusiness.Find(managementHealthInsuranceID);
            InsuranceViewModel objInsurance = new InsuranceViewModel();
            if (objMHI != null)
            {
                ClassProfile objCP = ClassProfileBusiness.Find(objMHI.ClassID);
                var lstEducation = _globalInfo.EducationLevels.ToList();
                ViewData["EducationLevelPP"] = new SelectList(lstEducation, "EducationLevelID", "Resolution", objCP.EducationLevelID);
                //combobox lớp
                IDictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

                var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                        .Where(x => x.IsActive == true && x.EducationLevelID == objCP.EducationLevelID)
                                                        .OrderBy(u => u.EducationLevelID)
                                                        .ThenBy(u => u.OrderNumber)
                                                        .ThenBy(u => u.DisplayName).ToList();
                ViewData["ClassPP"] = new SelectList(lstClass, "ClassProfileID", "DisplayName", objCP.ClassProfileID);

                PupilProfile objPP = PupilProfileBusiness.Find(objMHI.PupilID);
                objInsurance.managementHealthInsuranceID = managementHealthInsuranceID;
                objInsurance.studentName = objPP.FullName;
                objInsurance.PupilID = objMHI.PupilID;
                objInsurance.pupilCode = objPP.PupilCode;
                objInsurance.birthDate = objPP.BirthDate;
                objInsurance.stringBirthDate = objPP.BirthDate.ToShortDateString();
                objInsurance.gender = objPP.Genre == 1 ? "Nam" : "Nữ";
                objInsurance.ticketNumber = objMHI.CardNumber;
                objInsurance.address = objMHI.Address;
                objInsurance.passportPlace = objMHI.RegisterAddress;
                objInsurance.passportCode = objMHI.CardCode;
                objInsurance.duedayFrom = objMHI.FromDate;
                objInsurance.stringDuedayFrom = objMHI.FromDate.ToShortDateString();
                objInsurance.duedayTo = objMHI.ToDate;
                objInsurance.stringDuedayTo = objMHI.ToDate.ToShortDateString();
                objInsurance.createDate = objMHI.DateRange;
                objInsurance.createPlace = objMHI.PlaceRange;
            }

            return PartialView("_UpdatePopUp", objInsurance);
        }

        #region Import

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (attachments == null || attachments.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
                res.ContentType = "text/plain";
                return res;
            }
            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;

                string Error = "";
                bool status = false;
                List<ImportViewModel> list = null;

                list = getDataToFile(physicalPath, out Error, out status);

                if (Error.Trim().Length == 0 && status)
                {

                    List<ManagementHealthInsurance> lstMHI = new List<ManagementHealthInsurance>();
                    lstMHI = ListImport(list);
                    ManagementHealthInsuranceBusiness.SaveImport(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, lstMHI);
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                    res.ContentType = "text/plain";
                    return res;
                }
                else if (Error.Trim().Length > 0)
                {
                    JsonResult res = Json(new JsonMessage(Error, JsonMessage.ERROR));
                    res.ContentType = "text/plain";
                    return res;
                }
                else
                {
                    Session["ListPupil"] = list;
                    JsonResult res = Json(new { type = "ViewError", ListPupil = list });
                    res.ContentType = "text/plain";
                    return res;
                }
            }
            JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_NoFileSelected"), JsonMessage.ERROR));
            res1.ContentType = "text/plain";
            return res1;
        }

        public List<ImportViewModel> getDataToFile(string filename, out string Error, out bool Pass)
        {
            int startRow;
            int SchoolID = _globalInfo.SchoolID.Value;
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int AppliedLevel = _globalInfo.AppliedLevel.Value;
            string FilePath = filename;

            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            List<ImportViewModel> ListPupil = new List<ImportViewModel>();
            ClassProfile classProfile = new ClassProfile();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            AcademicYear objacademic = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            Error = "";
            Pass = false;
            string Title = "";

            string pupilcode = "";
            string pupilname = "";
            string titleYear = "";
            string titleClass = "";
            string titlePupil = "";

            //kiem tra cac cot trong bang csdl xem da chuan chua
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            var lstPos = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).Select(u => new { u.PupilID, u.PupilProfile.PupilCode, u.PupilProfile.FullName, u.Status, u.ClassID, u.ClassProfile.DisplayName });

            #region for
            #region Kiem tra du lieu chon dau vao
            Title = (string)sheet.GetCellValue("A4");

            if (string.IsNullOrEmpty(Title) || !Title.Contains("DANH SÁCH CẤP THẺ BẢO HIỂM Y TẾ"))
            {
                Error = "File Excel không hợp lệ";
                Pass = false;
                return (new List<ImportViewModel>());
            }

            titleYear = (string)sheet.GetCellValue("A5");
            if (!titleYear.Contains(objAcademicYear.DisplayTitle))
            {
                if (Error == "")
                    Error = "File excel không phải là file điểm danh của năm học " + objAcademicYear.DisplayTitle;
                else Error = Error + "-" + "File excel không phải là file điểm danh của năm học" + objAcademicYear.DisplayTitle;
            }
            #endregion

            //phan kiem tra du lieu chon ban dau va thong tin tren excel
            Pass = true;
            startRow = 8;

            while (sheet.GetCellValue(startRow, 1) != null)
            {
                pupilcode = sheet.GetCellValue(startRow, 2) == null ? "" : sheet.GetCellValue(startRow, 2).ToString();
                titlePupil = pupilcode;
                pupilname = (string)sheet.GetCellValue(startRow, 3);
                titleClass = (string)sheet.GetCellValue(startRow, 4);
                var poc = lstPos.Where(o => o.PupilCode == pupilcode && o.DisplayName == titleClass).FirstOrDefault();


                ImportViewModel InsuranceImport = new ImportViewModel();
                InsuranceImport.Status = true;
                InsuranceImport.pupilCode = pupilcode;
                InsuranceImport.fullName = pupilname;
                InsuranceImport.schoolID = _globalInfo.SchoolID.Value;
                InsuranceImport.academicID = _globalInfo.AcademicYearID.Value;
                InsuranceImport.pupilID = poc != null ? poc.PupilID : new Nullable<int>();
                InsuranceImport.classID = poc != null ? poc.ClassID : new Nullable<int>();
                InsuranceImport.className = titleClass;
                //InsuranceImport.stringBirthDate = sheet.GetCellValue(startRow, 5) == null ? "" : sheet.GetCellValue(startRow, 5).ToString();
                InsuranceImport.cardNumber = sheet.GetCellValue(startRow, 7) == null ? "" : sheet.GetCellValue(startRow, 7).ToString();
                InsuranceImport.address = sheet.GetCellValue(startRow, 8) == null ? "" : sheet.GetCellValue(startRow, 8).ToString();
                InsuranceImport.registerAddress = sheet.GetCellValue(startRow, 9) == null ? "" : sheet.GetCellValue(startRow, 9).ToString();
                InsuranceImport.cardCode = sheet.GetCellValue(startRow, 10) == null ? "" : sheet.GetCellValue(startRow, 10).ToString();
                InsuranceImport.stringFromDate = sheet.GetCellValue(startRow, 11) == null ? "" : sheet.GetCellValue(startRow, 11).ToString();
                InsuranceImport.stringToDate = sheet.GetCellValue(startRow, 12) == null ? "" : sheet.GetCellValue(startRow, 12).ToString();
                InsuranceImport.stringCreateRange = sheet.GetCellValue(startRow, 13) == null ? "" : sheet.GetCellValue(startRow, 13).ToString();
                InsuranceImport.placeRange = sheet.GetCellValue(startRow, 14) == null ? "" : sheet.GetCellValue(startRow, 14).ToString();
                InsuranceImport.ErrorLabel = "";
                if (poc == null)
                {
                    InsuranceImport.ErrorLabel = Res.Get("Lớp hoặc học sinh không tồn tại; ");
                    InsuranceImport.Status = false;
                    Pass = false;
                    //startRow++;
                    //continue;
                }
                if (!regexInfo(InsuranceImport.cardNumber) || InsuranceImport.cardNumber.Length > 20 || InsuranceImport.cardNumber == "")
                {
                    InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Số thẻ không hợp lệ; ");
                    InsuranceImport.Status = false;
                    Pass = false;
                }
                if (InsuranceImport.address.Length > 200)
                {
                    InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Địa chỉ không hợp lệ; ");
                    InsuranceImport.Status = false;
                    Pass = false;
                }
                if (InsuranceImport.registerAddress.Length > 200 || InsuranceImport.registerAddress == "")
                {
                    InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Nơi ĐK KCB không hợp lệ; ");
                    InsuranceImport.Status = false;
                    Pass = false;

                }
                if (InsuranceImport.cardCode != "" && (!regexInfo(InsuranceImport.cardCode) || InsuranceImport.cardCode.Length > 20))
                {
                    InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Mã không hợp lệ; ");
                    InsuranceImport.Status = false;
                    Pass = false;
                }
                if (InsuranceImport.stringFromDate == "" || !checkDateType(InsuranceImport.stringFromDate))
                {
                    InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Hạn sử dụng từ không hợp lệ; ");
                    InsuranceImport.Status = false;
                    Pass = false;
                }
                if (InsuranceImport.stringToDate == "" || !checkDateType(InsuranceImport.stringToDate))
                {
                    InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Hạn sử dụng đến không hợp lệ; ");
                    InsuranceImport.Status = false;
                    Pass = false;
                }
                if (InsuranceImport.stringCreateRange != "")
                {
                    if (!checkDateType(InsuranceImport.stringCreateRange))
                    {
                        InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Ngày cấp thẻ không hợp lệ; ");
                        InsuranceImport.Status = false;
                        Pass = false;
                    }
                }
                if (InsuranceImport.placeRange.Length > 200)
                {
                    InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Nơi cấp thẻ không hợp lệ; ");
                    InsuranceImport.Status = false;
                    Pass = false;
                }
                if (checkDateType(InsuranceImport.stringToDate) && checkDateType(InsuranceImport.stringFromDate))
                {
                    DateTime duedayFrom = DateTime.Parse(InsuranceImport.stringFromDate);
                    DateTime duedayTo = DateTime.Parse(InsuranceImport.stringToDate);
                    if (duedayFrom >= duedayTo || duedayFrom > objacademic.SecondSemesterEndDate || duedayTo < objacademic.FirstSemesterStartDate)
                    {
                        InsuranceImport.ErrorLabel = InsuranceImport.ErrorLabel + Res.Get("Hạn sử dụng không đúng; ");
                        InsuranceImport.Status = false;
                        Pass = false;
                    }

                }
                InsuranceImport.Status = InsuranceImport.Status == false ? false : true;
                ListPupil.Add(InsuranceImport);
                startRow++;
            }
            #endregion

            return ListPupil;
        }

        public PartialViewResult GetViewListToImport()
        {
            var ListPupil = (List<ImportViewModel>)Session["ListPupil"];

            ViewData["ListError"] = ListPupil;
            IDictionary<int, string> dicNameDay = new Dictionary<int, string>();

            ViewData["ERROR_IMPORT_MESSAGE"] = "File chứa dữ liệu không hợp lệ. Thầy cô vui lòng kiểm tra lại";
            return PartialView("_ChooseAction");
        }

        public List<ManagementHealthInsurance> ListImport(List<ImportViewModel> lstIVM)
        {
            List<ManagementHealthInsurance> lstMHI = new List<ManagementHealthInsurance>();
            ManagementHealthInsurance objMHI = null;
            foreach (var item in lstIVM)
            {
                if (item.classID != null && item.pupilID != null)
                {
                    objMHI = new ManagementHealthInsurance();
                    objMHI.SchoolID = _globalInfo.SchoolID.Value;
                    objMHI.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objMHI.ClassID = item.classID.Value;
                    objMHI.PupilID = item.pupilID.Value;
                    objMHI.CardNumber = item.cardNumber;
                    objMHI.Address = item.address;
                    objMHI.RegisterAddress = item.registerAddress;
                    objMHI.CardCode = item.cardCode;
                    objMHI.PlaceRange = item.placeRange;
                    if (item.stringCreateRange != "")
                    {
                        objMHI.DateRange = DateTime.Parse(item.stringCreateRange);
                    }
                    objMHI.FromDate = DateTime.Parse(item.stringFromDate);
                    objMHI.ToDate = DateTime.Parse(item.stringToDate);
                    lstMHI.Add(objMHI);
                }
            }
            return lstMHI;
        }

        #endregion

        public bool checkDateType(string a)
        {
            DateTime c;
            bool b = DateTime.TryParse(a, out c);
            return b;
        }

        public bool regexInfo(string a)
        {
            return Regex.IsMatch(a, @"^[a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐẾếỀềĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\-]+$");
        }

        #region Load combobox

        public ActionResult LoadPupil(int classPU)
        {
            List<PupilAutoComplete> lstPupil = new List<PupilAutoComplete>();

            if (classPU == 0)
            {
                ViewData["PupilAutoComplete"] = lstPupil;
                return PartialView("_PupilAutoComplete");
            }

            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            //dicClass["EducationLevelID"] = educationLevelPU;
            dicClass["ClassID"] = classPU;
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicClass["AppliedLevel"] = _globalInfo.AppliedLevel;

            lstPupil = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.Status == 1)
                        .OrderBy(x => x.ClassProfile.EducationLevelID)
                        .ThenBy(x => x.ClassProfile.OrderNumber)
                        .ThenBy(x => x.ClassProfile.DisplayName)
                        .ThenBy(x => x.OrderInClass)
                        .ThenBy(x => x.PupilProfile.Name)
                        .ThenBy(x => x.PupilProfile.FullName)
                        .Select(x => new PupilAutoComplete
                        {
                            PupilCode = x.PupilProfile.PupilCode,
                            DisplayName = x.PupilProfile.FullName,
                            PupilID = x.PupilID
                        }).ToList();

            dicClass["SchoolID"] = _globalInfo.SchoolID;
            List<ManagementHealthInsurance> lstMHI = ManagementHealthInsuranceBusiness.Search(dicClass).ToList();

            if (lstMHI.Count != 0)
            {
                List<int> lstMHIID = lstMHI.Select(x => x.PupilID).ToList();
                lstPupil = lstPupil.Where(x => !lstMHIID.Contains(x.PupilID)).ToList();
            }

            ViewData["PupilAutoComplete"] = lstPupil;
            return PartialView("_PupilAutoComplete");
        }

        public ActionResult LoadPupilUpdate(int classPU)
        {
            List<PupilAutoComplete> lstPupil = new List<PupilAutoComplete>();

            if (classPU == 0)
            {
                ViewData["PupilAutoComplete"] = lstPupil;
                return PartialView("_PupilAutoCompleteUpdate");
            }

            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            //dicClass["EducationLevelID"] = educationLevelPU;
            dicClass["ClassID"] = classPU;
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicClass["AppliedLevel"] = _globalInfo.AppliedLevel;

            lstPupil = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.Status == 1)
                        .OrderBy(x => x.ClassProfile.EducationLevelID)
                        .ThenBy(x => x.ClassProfile.OrderNumber)
                        .ThenBy(x => x.ClassProfile.DisplayName)
                        .ThenBy(x => x.OrderInClass)
                        .ThenBy(x => x.PupilProfile.Name)
                        .ThenBy(x => x.PupilProfile.FullName)
                        .Select(x => new PupilAutoComplete
                        {
                            PupilCode = x.PupilProfile.PupilCode,
                            DisplayName = x.PupilProfile.FullName,
                            PupilID = x.PupilID
                        }).ToList();

            dicClass["SchoolID"] = _globalInfo.SchoolID;
            List<ManagementHealthInsurance> lstMHI = ManagementHealthInsuranceBusiness.Search(dicClass).ToList();

            if (lstMHI.Count != 0)
            {
                List<int> lstMHIID = lstMHI.Select(x => x.PupilID).ToList();
                lstPupil = lstPupil.Where(x => !lstMHIID.Contains(x.PupilID)).ToList();
            }

            ViewData["PupilAutoCompleteUpdate"] = lstPupil;
            return PartialView("_PupilAutoCompleteUpdate");
        }

        // Genre BirthDate
        public ActionResult LoadDateAndGender(string PupilCode)
        {
            InsuranceViewModel objPupilInfo = new InsuranceViewModel();
            PupilProfileBO objPup = new PupilProfileBO();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilCode"] = PupilCode;
            dic["CurrentSchoolID"] = _globalInfo.SchoolID;
            dic["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            objPup = PupilProfileBusiness.Search(dic).Where(x => x.IsActive == true && x.PupilCode == PupilCode).FirstOrDefault();
            if (objPup != null)
            {
                objPupilInfo.address = objPup.TempResidentalAddress;
                objPupilInfo.stringBirthDate = objPup.BirthDate.ToShortDateString();
                objPupilInfo.gender = objPup.Genre == 1 ? "Nam" : "Nữ";
                objPupilInfo.PupilID = objPup.PupilProfileID;
            }
            return Json(objPupilInfo);
        }

        #endregion

    }
}
