﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HealthInsuranceManagementArea
{
    public class HealthInsuranceManagementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HealthInsuranceManagementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HealthInsuranceManagementArea_default",
                "HealthInsuranceManagementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
