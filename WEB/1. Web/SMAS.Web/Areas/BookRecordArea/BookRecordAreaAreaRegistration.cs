﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.BookRecordArea
{
    public class BookRecordAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BookRecordArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BookRecordArea_default",
                "BookRecordArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
