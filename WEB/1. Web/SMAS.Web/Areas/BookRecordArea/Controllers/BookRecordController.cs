﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.MarkRecordArea;
using SMAS.Business.BusinessObject;
using System.Threading;
using System.Globalization;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.BookRecordArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class BookRecordController : BaseController
    {
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IBookmarkedFunctionBusiness BookmarkedFunctionBusiness;
        private readonly IMasterBookBusiness MasterBookBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness; 

        public BookRecordController(IPupilProfileBusiness pupilProfileBusiness,
                                        ISummedUpRecordBusiness summedUpRecordBusiness,
                                        IAcademicYearBusiness academicYearBusiness,
                                        ISemeterDeclarationBusiness semeterDeclarationBusiness,
                                        IMarkRecordBusiness markrecordBusiness,
                                        ISubjectCatBusiness subjectCatBusiness,
                                        ITeachingAssignmentBusiness teachingAssignmentBusiness,
                                        IClassProfileBusiness classProfileBusiness,
                                        IClassSubjectBusiness classSubjectBusiness,
                                        IPeriodDeclarationBusiness PeriodDeclarationBusiness,
                                        IPupilOfClassBusiness PupilOfClassBusiness,
                                        IMarkTypeBusiness MarkTypeBusiness,
                                        IExemptedSubjectBusiness exemptedSubjectBusiness,
                                        IUserAccountBusiness UserAccountBusiness,
                                        IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness,
                                        IBookmarkedFunctionBusiness bookmarkedFunctionBusiness,
                                        IMasterBookBusiness MasterBookBusiness,
                                        IReportDefinitionBusiness ReportDefinitionBusiness,
                                        IEmployeeBusiness EmployeeBusiness)
        {
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.SemesterDeclarationBusiness = semeterDeclarationBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.MarkRecordBusiness = markrecordBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
            this.BookmarkedFunctionBusiness = bookmarkedFunctionBusiness;
            this.MasterBookBusiness = MasterBookBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }
        //[CacheFilter(Duration = 60)]
        [CompressFilter(Order = 1)]
        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            SetViewData(ClassID);
            return PartialView();
        }

        public void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            int semesterID = 0;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
                semesterID = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                {
                    semester1 = true;
                    semesterID = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                }
                else
                {
                    semester2 = true;
                    semesterID = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                }
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[BookRecordConstants.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[BookRecordConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;
            int a = 0;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["Semester"] = _globalInfo.Semester;

            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();


            List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
            if (lstPeriod != null && lstPeriod.Count() > 0)
            {
                a = 1;
                for (int k = 0; k < lstPeriod.Count; k++)
                {

                    PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                    periodDeclaration = lstPeriod[k];
                    if (periodDeclaration.FromDate < DateTime.Now && periodDeclaration.EndDate > DateTime.Now)
                    {
                        a = k;
                    }
                    if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";

                    listPeriodDeclaration.Add(periodDeclaration);
                }
            }
            ViewData[BookRecordConstants.CHECK_DEFAULT] = a;
            int period = 0;
            //Dot hien tai
            if (listPeriodDeclaration != null && listPeriodDeclaration.Count() != 0)
            {
                PeriodDeclaration objCurrentPeriod = listPeriodDeclaration.FirstOrDefault(s => s.FromDate <= DateTime.Now && DateTime.Now <= s.EndDate);
                if (objCurrentPeriod != null)
                {
                    period = objCurrentPeriod.PeriodDeclarationID;
                    ViewData[BookRecordConstants.CURRENT_PERIOD_ID] = period;
                }
                else
                {
                    period = listPeriodDeclaration.FirstOrDefault().PeriodDeclarationID;
                    ViewData[BookRecordConstants.CURRENT_PERIOD_ID] = 0;
                }

            }



            //ViewData[BookRecordConstants.LIST_PERIOD] = listPeriodDeclaration;
            ViewData[BookRecordConstants.LIST_PERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", period);

            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[BookRecordConstants.LIST_CLASS] = listClass;

                var listCP = listClass.ToList();
                ClassProfile cp = null;
                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            // Ưu tiên trước đối với giáo viên bộ môn
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                            List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                            {
                                cp = listSubjectTeacher.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                        .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listHead != null && listHead.Count > 0)
                                {
                                    cp = listHead.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            ViewData[BookRecordConstants.LIST_SUBJECT] = null;

            ViewData[BookRecordConstants.ShowList] = false;

            ViewData[BookRecordConstants.HAS_ERROR_DATA] = false;
            ViewData[BookRecordConstants.LIST_IMPORTDATA] = null;

            ViewData[BookRecordConstants.CLASS_12] = false;
            ViewData[BookRecordConstants.SEMESTER_1] = true;
            ViewData[BookRecordConstants.ERROR_BASIC_DATA] = false;
            ViewData[BookRecordConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[BookRecordConstants.ERROR_IMPORT] = true;
            ViewData[BookRecordConstants.ENABLE_ENTRYMARK] = "true";
            ViewData[BookRecordConstants.TEACHER_ID] = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0;
        }

        [HttpPost]
        //[CacheFilter(Duration = 60)]
        public PartialViewResult GetSubjectPanel()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int semester = semesterid == null ? 0 : semesterid.Value;
            ViewData[MarkRecordConstants.ClassID] = classid.Value;
            //cho phep tai khoan xem tat ca sanh sach mon hoc
            bool ViewAll = false;

            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            //Chiendd1: 06/07/2015
            //Bo sung dieu kien partition cho bang TeachingAssignmentBusiness
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            if (classid.HasValue && semesterid.HasValue)
            {
                lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value,
                                                                    semesterid.Value, classid.Value, ViewAll)
                                                                    .Where(u => (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                     ? u.SectionPerWeekFirstSemester > 0
                                                                     : u.SectionPerWeekSecondSemester > 0)
                                                                     && (!u.IsSubjectVNEN.HasValue || (u.IsSubjectVNEN.HasValue && u.IsSubjectVNEN.Value == false)))
                                  join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                  where s.IsActive == true
                                  && s.IsApprenticeshipSubject == false
                                  select new SubjectCatBO()
                                  {
                                      SubjectCatID = s.SubjectCatID,
                                      DisplayName = s.DisplayName,
                                      IsCommenting = cs.IsCommenting,
                                      OrderInSubject = s.OrderInSubject,
                                      SubjectIdInCrease = cs.SubjectIDIncrease
                                  }
                                  ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                Session["ListSubject"] = lsClassSubject;
            }
            List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();
            int UserAccountID = _globalInfo.UserAccountID;

            // Neu la admin truong thi hien thi het
            if (new GlobalInfo().IsAdminSchoolRole)
            {
                foreach (SubjectCatBO item in lsClassSubject)
                {
                    listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, false));
                }
            }
            else
            {
                int? TeacherID = UserAccountBusiness.Find(UserAccountID).EmployeeID;
                List<int> listPermitSubjectID = null; ;
                if (TeacherID.HasValue)
                {
                    // Phan cong giao vu la giao vien bo mon
                    if (ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == classid &&
                                                            o.AcademicYearID == _globalInfo.AcademicYearID &&
                                                            o.SchoolID == _globalInfo.SchoolID &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER).Count() > 0
                                                            )
                    {
                        listPermitSubjectID = lsClassSubject.Select(o => o.SubjectCatID).ToList();
                    }
                    else
                    {
                        // Khong phai lai giao vien phan cong giao vu thi kiem tra la giao vien bo mon
                        IDictionary<string, object> searchInfo = new Dictionary<string, object>();
                        searchInfo["ClassID"] = classid;
                        searchInfo["TeacherID"] = TeacherID;
                        searchInfo["Semester"] = semester;
                        searchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                        searchInfo["IsActive"] = true;
                        listPermitSubjectID = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfo).Select(o => o.SubjectID).ToList();
                        /*listPermitSubjectID = TeachingAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == classid &&
                                                            o.Semester == semester &&
                                                            o.IsActive).Select(o => o.SubjectID).ToList();*/

                    }
                }
                else
                {
                    listPermitSubjectID = new List<int>();
                }
                foreach (SubjectCatBO item in lsClassSubject)
                {
                    // todo: haivt9 review code, GetTypeSubject su dung Session["ListSubject"] cung chinh la lsClassSubject, mo 2 tab -> sai typeSubject -> fix su dung lsClassSubject
                    bool typeSubject = this.GetTypeSubject(item.SubjectCatID) == SubjectMark;
                    if (listPermitSubjectID.Contains(item.SubjectCatID))
                    {
                        listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, true, typeSubject));
                    }
                    else
                    {
                        listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, typeSubject));
                    }
                }
            }
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle))
            {
                isLockInput = objLockInput.LockTitle.Contains("HK" + semester);
            }

            ViewData[BookRecordConstants.IS_LOCK_INPUT] = isLockInput;
            if (isLockInput)
            {
                string LockUserName = string.Empty;
                int LockUserID = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? objLockInput.UserLock1ID.Value : objLockInput.UserLock2ID.Value;
                int HierachyLevelID = this.GetHierachyLevelIDByUserAccountID(LockUserID);
                LockUserName = HierachyLevelID == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE ? "Sở giáo dục" : HierachyLevelID == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE ? "Phòng giáo dục" : "";
                ViewData[BookRecordConstants.LOCK_USER_NAME] = "Sổ điểm đã bị khóa nhập liệu bởi " + LockUserName;
            }

            ViewData["ListSubjectComment"] = lsClassSubject;
            ViewData[MarkRecordConstants.LIST_SUBJECT] = listSJ;
            ViewData[MarkRecordConstants.ShowList] = false;
            return PartialView("_ListSubject");
        }

        public const int SubjectMark = 1;
        public const int SubjectJudge = 2;

        [HttpPost]
        public int GetTypeSubject(int? SubjectID)
        {
            int typeSubject = 0;
            //const 1 mon tinh diem va mon tinh diem ket hop nhan xet; 2 mon nhan xet

            if (Session["ListSubject"] != null)
            {
                List<SubjectCatBO> lsClassSubject = Session["ListSubject"] as List<SubjectCatBO>;

                var lclassSubject = lsClassSubject.Where(o => o.SubjectCatID == SubjectID.Value);

                if (lclassSubject != null && lclassSubject.Count() > 0)
                {
                    SubjectCatBO classSubject = lclassSubject.FirstOrDefault();

                    if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        return SubjectMark;
                    }
                    else
                    {
                        return SubjectJudge;
                    }
                }
            }
            return typeSubject;
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboPeriod(int? SemesterID)
        {
            if (SemesterID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                dic["Semester"] = SemesterID.Value;
                IEnumerable<PeriodDeclaration> listPeriod = PeriodDeclarationBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
                List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();

                int currentPeriodId = 0;
                if (listPeriod != null && listPeriod.Count() > 0)
                {
                    foreach (var item in listPeriod)
                    {
                        PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                        periodDeclaration = item;
                        if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        {
                            periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                            listPeriodDeclaration.Add(periodDeclaration);

                        }

                    }

                    PeriodDeclaration objCurrentPeriod = listPeriodDeclaration.FirstOrDefault(s => s.FromDate <= DateTime.Now && DateTime.Now <= s.EndDate);
                    if (objCurrentPeriod != null)
                    {
                        currentPeriodId = objCurrentPeriod.PeriodDeclarationID;
                        ViewData[BookRecordConstants.CURRENT_PERIOD_ID] = objCurrentPeriod.PeriodDeclarationID;
                    }
                    else
                    {
                        ViewData[BookRecordConstants.CURRENT_PERIOD_ID] = 0;
                    }

                }
                ViewData[MarkRecordConstants.LIST_SEMESTER] = SemesterID.Value;
                ViewData[MarkRecordConstants.ShowList] = false;
                //Utils.SessionObject("CurentPerioId", currentPeriodId);
                return Json(new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", currentPeriodId), "CurentPerioId");
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetCurrentPeriod(int? semesterId)
        {
            if (!semesterId.HasValue)
            {
                return Json(new
                {
                    Message = "Lấy thông tin đợt hiện tại",
                    Type = "success",
                    CurrentPeriodId = 0
                });
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["Semester"] = semesterId.Value;
            IEnumerable<PeriodDeclaration> iqPeriod = PeriodDeclarationBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic);
            int currentPeriodId = 0;
            PeriodDeclaration objCurrentPeriod = iqPeriod.FirstOrDefault(s => s.FromDate <= DateTime.Now && DateTime.Now <= s.EndDate);
            if (objCurrentPeriod != null)
            {
                currentPeriodId = objCurrentPeriod.PeriodDeclarationID;
            }
            else
            {
                currentPeriodId = 0;
            }
            return Json(new
            {
                Message = "Lấy thông tin đợt hiện tại",
                Type = "success",
                CurrentPeriodId = currentPeriodId
            });


        }

        public string GetListClassBySubject(int subjectId, int educationLevelId, int semester, int classID)
        {
            string strResult = string.Empty;
            StringBuilder strBuilder = new StringBuilder();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SubjectID"] = subjectId;
            int teacherId = 0;
            if (!_globalInfo.IsAdmin)
            {
                teacherId = _globalInfo.EmployeeID.Value;
            }
            List<ClassProfileTempBO> listClassProfile = ClassSubjectBusiness.GetListClassBySubjectId(
                _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, subjectId, educationLevelId,
                teacherId, _globalInfo.IsAdmin, semester, _globalInfo.AppliedLevel.Value, _globalInfo.IsAdminSchoolRole,
                _globalInfo.IsViewAll, _globalInfo.IsEmployeeManager, _globalInfo.UserAccountID, classID, true);
            if (listClassProfile != null && listClassProfile.Count > 0)
            {
                for (int i = 0; i < listClassProfile.Count; i++)
                {
                    strBuilder.Append("<span class='ClassListClassOption editor-field'><input class='ClassExport' type='checkbox' value='" + listClassProfile[i].ClassProfileID + "' name='OptionClass_" + listClassProfile[i].ClassProfileID + "' onchange='changeClasExport()'>" + listClassProfile[i].DisplayName + "</span>");
                }
            }
            if (strBuilder != null)
            {
                strResult = strBuilder.ToString();
            }
            return strResult;
        }

        public PartialViewResult _LoadGridDataImport(int? EducationLevelID, int ClassID, int SubjectID, int TypeExport)
        {
            int schoolID = _globalInfo.SchoolID.Value;
            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            ViewData[MarkRecordConstants.TYPE_REPORT] = TypeExport;
            if (TypeExport == 2)
            {
                List<ClassSubjectBO> lstClassSubject = new List<ClassSubjectBO>();
                dic.Add("SchoolID", schoolID);
                dic.Add("AcademicYearID", academicYearID);
                lstClassSubject = (from cs in ClassSubjectBusiness.SearchByClass(ClassID, dic)
                                   select new ClassSubjectBO
                                   {
                                       ClassID = cs.ClassID,
                                       SubjectID = cs.SubjectID,
                                       SubjectName = cs.SubjectCat.DisplayName,
                                       OrderInSubject = cs.SubjectCat.OrderInSubject
                                   }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
                ViewData[MarkRecordConstants.LIST_SUBJECT] = lstClassSubject;
            }
            else
            {
                List<ClassProfileBO> lstClass = new List<ClassProfileBO>();
                dic.Add("SchoolID", schoolID);
                dic.Add("AcademicYearID", academicYearID);
                dic.Add("EducationLevelID", EducationLevelID);
                dic.Add("SubjectID", SubjectID);
                lstClass = ClassProfileBusiness.GetListClassBySubject(dic);
                ViewData[MarkRecordConstants.LIST_CLASS] = lstClass;
            }
            return PartialView("_GridDataImport");
        }


        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachmentsImportPeriod, int? ClassID, int? SubjectID, int EducationLevelID, int SemesterID)
        {
            if (attachmentsImportPeriod == null || attachmentsImportPeriod.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachmentsImportPeriod.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                string physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);

                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);

                IVTWorksheet sheet = obook.GetSheet(1);
                int TypeImport = 0;
                int tmp = 0;
                string TypeImportTMP = sheet.GetCellValue("B3") != null ? sheet.GetCellValue("B3").ToString() : string.Empty;
                List<string> lstSheetName = obook.GetSheets().Select(p => p.Name).ToList();

                if (!string.IsNullOrEmpty(TypeImportTMP) && Int32.TryParse(TypeImportTMP, out tmp))
                {
                    TypeImport = tmp;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>();
                if (TypeImport == 1)
                {
                    ViewData[MarkRecordConstants.TYPE_REPORT] = 1;
                    return Json(new JsonMessage(RenderPartialViewToString("_GridDataImport", null), "ImportOnSubject"));
                }
                else if (TypeImport == 2)//import theo mon
                {
                    List<SubjectCatBO> lstSCTMP = new List<SubjectCatBO>();
                    List<SubjectCatBO> lstSC = new List<SubjectCatBO>();
                    SubjectCatBO objSCBO = null;
                    //bool ViewAll = false;//lay cac mon GV co quyen, khong lay quyen xem toan truong
                    if (ClassID.HasValue)
                    {
                        /*lstSCTMP = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SemesterID, ClassID.Value, ViewAll).Where(u => SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                    join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                    where s.IsActive == true
                                    && s.IsApprenticeshipSubject == false
                                    select new SubjectCatBO()
                                    {
                                        SubjectCatID = s.SubjectCatID,
                                        DisplayName = s.DisplayName,
                                        IsCommenting = cs.IsCommenting,
                                        OrderInSubject = s.OrderInSubject,
                                        SubjectIdInCrease = cs.SubjectIDIncrease
                                    }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();*/
                        lstSCTMP = ClassSubjectBusiness.GetListSubjectbyTeaching(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SemesterID, ClassID.Value);
                    }
                    for (int i = 0; i < lstSCTMP.Count; i++)
                    {
                        objSCBO = lstSCTMP[i];
                        if (lstSheetName.Exists(p => p.Equals(Business.Common.Utils.StripVNSignAndSpace(objSCBO.DisplayName))))
                        {
                            lstSC.Add(objSCBO);
                        }
                    }
                    ViewData[MarkRecordConstants.LIST_SUBJECT] = lstSC;
                    ViewData[MarkRecordConstants.TYPE_REPORT] = 2;
                }
                else if (TypeImport == 3)//import theo lop
                {
                    int teacherId = 0;
                    if (!_globalInfo.IsAdmin)
                    {
                        teacherId = _globalInfo.EmployeeID.Value;
                    }
                    /*List<ClassProfileTempBO> lstClassProfileTMP = ClassSubjectBusiness.GetListClassBySubjectId(
                                _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SubjectID.Value, EducationLevelID,
                                teacherId, _globalInfo.IsAdmin, SemesterID, _globalInfo.AppliedLevel.Value, _globalInfo.IsAdminSchoolRole,
                                _globalInfo.IsViewAll, false, _globalInfo.UserAccountID, ClassID.Value, true);
                     */
                    int academicYearId = _globalInfo.AcademicYearID.Value;
                    int schoolId = _globalInfo.SchoolID.Value;
                    int subjectId = SubjectID.Value;
                    bool isSchoolAdmin = _globalInfo.IsAdmin;

                    List<ClassProfileTempBO> lstClassProfileTMP = ClassSubjectBusiness.GetListClassByTeachingAndSubjectId(academicYearId, schoolId, subjectId, teacherId, isSchoolAdmin, SemesterID, true);

                    List<ClassProfileTempBO> lstCP = new List<ClassProfileTempBO>();
                    ClassProfileTempBO objCP = null;
                    for (int i = 0; i < lstClassProfileTMP.Count; i++)
                    {
                        objCP = lstClassProfileTMP[i];
                        if (lstSheetName.Exists(p => p.Equals(Business.Common.Utils.StripVNSignAndSpace(objCP.DisplayName))))
                        {
                            lstCP.Add(objCP);
                        }
                    }

                    ViewData[MarkRecordConstants.LIST_CLASS] = lstCP;
                    ViewData[MarkRecordConstants.TYPE_REPORT] = 3;
                }
                else
                {
                    return Json(new JsonMessage("File Import không đúng mẫu.", "error"));
                }
            }
            return Json(new JsonMessage(RenderPartialViewToString("_GridDataImport", null), "Grid"));
        }
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpPost]
        //[CacheFilter(Duration = 60)]
        public JsonResult GetListSubject()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["ClassID"]);
            int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["Semester"]);
            int semester = semesterid == null ? 0 : semesterid.Value;
            //cho phep tai khoan xem tat ca sanh sach mon hoc
            bool ViewAll = false;//lay cac mon GV co quyen, khong lay quyen xem toan truong
            //Bo sung dieu kien partition cho bang TeachingAssignmentBusiness
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            if (classid.HasValue && semesterid.HasValue)
            {
                lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semesterid.Value, classid.Value, ViewAll).Where(u => semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                  join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                  where s.IsActive == true
                                  && s.IsApprenticeshipSubject == false
                                  select new SubjectCatBO()
                                  {
                                      SubjectCatID = s.SubjectCatID,
                                      DisplayName = s.DisplayName,
                                      IsCommenting = cs.IsCommenting,
                                      OrderInSubject = s.OrderInSubject,
                                      SubjectIdInCrease = cs.SubjectIDIncrease
                                  }
                                  ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }

            StringBuilder strAppend = new StringBuilder();
            if (lsClassSubject != null && lsClassSubject.Count > 0)
            {
                for (int i = 0; i < lsClassSubject.Count; i++)
                {
                    strAppend.Append("<span class='ClassListSubject editor-field'>");
                    strAppend.Append("<input type='checkbox' value='" + lsClassSubject[i].SubjectCatID + "' name='OptionSubject_" + lsClassSubject[i].SubjectCatID + "' iscommenting='" + lsClassSubject[i].IsCommenting + "' class='classSubjectExport' onchange='changeSubjectExport()' subjectidincrease='" + lsClassSubject[i].SubjectIdInCrease + "'>");
                    strAppend.Append(lsClassSubject[i].DisplayName);
                    strAppend.Append("</span>");
                }
            }
            if (strAppend != null)
            {
                return Json(new { strSubject = strAppend.ToString() });
            }
            return Json(new { strSubject = "Giáo viên chưa được phân công giảng dậy." });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddMenuUsual(string url)
        {
            Menu objMenu = SMAS.Business.Common.StaticData.ListMenu.Where(c => c.URL.Contains(url)).FirstOrDefault();

            List<BookmarkedFunction> lstBm = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.GetValueOrDefault(),
                _globalInfo.AcademicYearID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID);

            BookmarkedFunction objDB = lstBm.Where(p => p.MenuID == objMenu.MenuID).FirstOrDefault();

            if (objDB == null)
            {
                BookmarkedFunction bf = new BookmarkedFunction();
                bf.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bf.AcademicYearID = _globalInfo.AcademicYearID;
                bf.LevelID = _globalInfo.AppliedLevel;
                bf.UserID = _globalInfo.UserAccountID;
                bf.MenuID = objMenu.MenuID;
                bf.Semester = _globalInfo.Semester;
                string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", objMenu.MenuName);
                bf.FunctionName = string.IsNullOrEmpty(name) ? objMenu.MenuName : name;
                bf.Order = lstBm.Count + 1;
                bf.CreatedDate = DateTime.Now;

                BookmarkedFunctionBusiness.Insert(bf);
                BookmarkedFunctionBusiness.Save();
            }
            return Json(new JsonMessage("Đã thêm vào danh sách chức năng thường dùng.", "success"));
        }

        #region Xuat so ghi diem giao vien
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(int semester, int? schoolFacultyID, int? teacherId, int? numOfRows, bool? isNumRowPupilNum, bool? idg, int fitIn)
        {
            if ((isNumRowPupilNum == null || isNumRowPupilNum == false) && (!numOfRows.HasValue || (numOfRows.HasValue && (numOfRows.Value < 20 || numOfRows > 60))))
            {
                throw new BusinessException("Số dòng dữ liệu phải nằm trong khoảng [20 - 60]");
            }
            IDictionary<string,object> tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = _globalInfo.AcademicYearID;
            tmpDic["AppliedLevel"] = _globalInfo.AppliedLevel;
            tmpDic["SchoolID"] = _globalInfo.SchoolID;
            Employee objE = EmployeeBusiness.Find(teacherId);
            tmpDic["FacultyID"] = objE != null ? objE.SchoolFacultyID : 0;
            if (semester != SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                tmpDic["Semester"] = semester;
            }
            tmpDic["TeacherID"] = teacherId;
            tmpDic["RemoveVNEN"] = true;
            IQueryable<TeachingAssignment> lstTa = (from ta in TeachingAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, tmpDic)
                                                    where ta.ClassProfile.IsActive == true
                                                    select ta);
            if (lstTa == null || (lstTa != null && lstTa.Count() == 0))
            {
                throw new BusinessException("Giáo viên chưa được phân công giảng dạy");
            }                                           

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = semester;
            dic["TeacherID"] = teacherId;
            Employee objEmp = EmployeeBusiness.Find(teacherId);
            dic["SchoolFacultyID"] = objEmp != null ? objEmp.SchoolFacultyID : 0;
            string reportCode = SystemParamsInFile.REPORT_MARKINPUTBOOK;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.MasterBookBusiness.GetReportMarkInpurBook(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = MasterBookBusiness.CreateReportMarkInpurBook(dic, numOfRows.GetValueOrDefault(), isNumRowPupilNum.GetValueOrDefault(), idg.GetValueOrDefault(), fitIn);
                processedReport = this.MasterBookBusiness.InsertReportMarkInpurBook(dic, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(int semester, int? schoolFacultyID, int? teacherId, int? numOfRows, bool? isNumRowPupilNum, bool? idg, int fitIn)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = semester;
            dic["TeacherID"] = teacherId;
            Employee objEmp = EmployeeBusiness.Find(teacherId);
            dic["SchoolFacultyID"] = objEmp != null ? objEmp.SchoolFacultyID : 0;

            Stream excel = MasterBookBusiness.CreateReportMarkInpurBook(dic, numOfRows.GetValueOrDefault(), isNumRowPupilNum.GetValueOrDefault(), idg.GetValueOrDefault(), fitIn);
            ProcessedReport processedReport = this.MasterBookBusiness.InsertReportMarkInpurBook(dic, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport, int booklet)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream file = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport pdfExporter = new PdfExport();
            var objRetVal = pdfExporter.ConvertPdf(file, processedReport.ReportName, booklet);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion
    }
}
