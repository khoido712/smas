﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.BookRecordArea
{
    public class BookRecordConstants 
    {
        public const string CHECK_DEFAULT = "CHECK_DEFAULT";
        public const string LIST_MARKRECORD = "listMarkRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_PERIOD = "listPeriod";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SUBJECT = "listSubject";
        public const string SemesterID = "SemesterID";
        public const string EducationLevelID = "EducationLevelID";
        public const string ClassID = "ClassID";
        public const string SubjectID = "SubjectID";
        public const string ShowList = "ShowList";
        public const string PeriodId = "PeriodId";
        public const string Title = "Title";
        public const string InterviewMark = "InterviewMark";
        public const string StartIndexOfInterviewMark = "StartIndexOfInterviewMark";
        public const string WritingMark = "WritingMark";
        public const string StartIndexOfWritingMark = "StartIndexOfWritingMark";
        public const string TwiceCoeffiecientMark = "TwiceCoeffiecientMark";
        public const string StartIndexOfTwiceCoeffiecientMark = "StartIndexOfTwiceCoeffiecientMark";
        public const string lblLockInfo = "lblLockInfo";
        public const string colComment = "colComment";
        public const string colSemester = "COLSEMESTER";
        public const string listlockmark = "listlockmark";
        public const string IsExemptedSubject = "IsExemptedSubject";


        public const string titleImport = "TitleImport";
        public const string listMarkRecordViewModel = "listMarkRecordViewModel";
        public const string ListTypeMarkMImport = "ListTypeMarkMImport";
        public const string ListTypeMarkPImport = "ListTypeMarkPImport";
        public const string ListTypeMarkVImport = "ListTypeMarkVImport";

        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string CLASS_12 = "CLASS_12";
        public const string SEMESTER_1 = "SEMESTER_1";

        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";

        public const string ERROR_IMPORT = "ERROR_IMPORT";

        public const string LOCK_IMPORT = "LOCK_IMPORT";
        public const string COUNT = "count";
        public const string ENABLE_ENTRYMARK = "enable_entryMark";

        public const string COLMARKSEMESTER = "COLMARKSEMESTER";

        public const string IS_CLASSIFICATION = "IS_CLASSIFICATION";

        public const string TYPE_REPORT = "TypeReport";
        public const string IS_LOCK_INPUT = "IsLockInput";
        public const string LOCK_USER_NAME = "LockUserName";

        public const string CURRENT_PERIOD_ID = "CURRENT_PERIOD_ID";
        public const string TEACHER_ID = "TeacherID";

    }
}
