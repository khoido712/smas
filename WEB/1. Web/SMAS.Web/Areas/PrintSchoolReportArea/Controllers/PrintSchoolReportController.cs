﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.PrintSchoolReportArea.Models;
using SMAS.Web.Controllers;
namespace SMAS.Web.Areas.PrintSchoolReportArea.Controllers
{
    public class PrintSchoolReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly ITranscriptsBusiness TranscriptsBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public PrintSchoolReportController(IAcademicYearBusiness AcademicYearBusiness,IClassProfileBusiness ClassProfileBusiness, IPupilOfClassBusiness PupilOfClassBusiness
            , IPupilProfileBusiness PupilProfileBusiness, ITranscriptsBusiness TranscriptsBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.TranscriptsBusiness = TranscriptsBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        //
        // GET: /PrintSchoolReportArea/PrintSchoolReport/

        public ActionResult Index()
        {
            setViewData();
            return View();
        }


        #region loadComboBox
        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            //: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary)
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };
            if (!global.IsAdminSchoolRole)
            {
                dic["UserAccountID"] = global.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }
            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).OrderBy(o=>o.DisplayName);
            return lsCP;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }
        private IQueryable<PupilOfClass> getPupilFromClass(int ClassID)
        {
           
            GlobalInfo global = new GlobalInfo();
            AcademicYear aca = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ClassID",ClassID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };
            IQueryable<PupilOfClass> lsPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, dic).Where(o=>o.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS);
            return lsPOC;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPupil(int? ClassID)
        {
            if (ClassID.HasValue)
            {
                IQueryable<PupilOfClass> lsCP = getPupilFromClass(ClassID.Value);
                List<PupilProfileBO> lsPP = lsCP.Select(o => new PupilProfileBO
                {
                    OrderInClass = o.OrderInClass,
                    PupilProfileID = o.PupilID,
                    FullName = o.PupilProfile.FullName,
                    Name = o.PupilProfile.Name
                }).Distinct().ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.FullName).ToList();
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsPP, "PupilProfileID", "FullName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }
        #endregion

        #region SetViewData
        public void setViewData()
        {
            GlobalInfo global = new GlobalInfo();
            List<EducationLevel> lsEducationLevel = global.EducationLevels;

            //-	cboEducationLevel: UserInfo.EducationLevels
            if (lsEducationLevel != null)
            {
                ViewData[PrintSchoolReportConstants.LIST_EDUCATION] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[PrintSchoolReportConstants.LIST_EDUCATION] = new SelectList(new string[] { });
            }
        }
        #endregion

        #region Download Report

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel svm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            Transcripts transcript = new Transcripts();
            transcript.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
            transcript.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            transcript.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            transcript.PupilID = svm.Pupil;
            transcript.TrainingType = GlobalInfo.TrainingType.GetValueOrDefault();
            string reportCode = "";
            if (transcript.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP1;
            }
            if (transcript.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY || transcript.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)
            {
                reportCode = SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = TranscriptsBusiness.GetTranscripts(transcript);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptsBusiness.CreateTranscripts(transcript);
                processedReport = TranscriptsBusiness.InsertTranscripts(transcript, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel svm)
        {

            GlobalInfo GlobalInfo = new GlobalInfo();
            Transcripts transcript = new Transcripts();
            transcript.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            transcript.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
            transcript.PupilID = svm.Pupil;
            transcript.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            Stream excel = TranscriptsBusiness.CreateTranscripts(transcript);
            ProcessedReport processedReport = TranscriptsBusiness.InsertTranscripts(transcript, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", global.SchoolID},
                {"AppliedLevel", global.AppliedLevel}
            };
            //Transcripts t = new Transcripts();
            //t.SchoolID = global.SchoolID.Value;
            //t.AppliedLevel = global.AppliedLevel.Value;
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP1,
                SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion

    }
}
