﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PrintSchoolReportArea
{
    public class PrintSchoolReportConstants
    {
        public const string LIST_SCHOOLREPORT = "listSchoolReport";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_EDUCATION = "LIST_EDUCATION";
        public const string LIST_PUPIL = "LIST_PUPIL";
    }
}