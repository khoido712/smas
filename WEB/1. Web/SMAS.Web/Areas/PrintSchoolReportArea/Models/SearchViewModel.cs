﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PrintSchoolReportArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PrintSchoolReport_Label_EducationLevelSearch")]
        [DataType("Combobox")]
        [AdditionalMetadata("ViewDataKey", PrintSchoolReportConstants.LIST_EDUCATION)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public string EducationLevel { get; set; }

        [ResourceDisplayName("PrintSchoolReport_Label_ClassSearch")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", PrintSchoolReportConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadPupil(this)")]
        public string Class { get; set; }

        [ResourceDisplayName("PrintSchoolReport_Label_PupilSearch")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", PrintSchoolReportConstants.LIST_PUPIL)]
        [AdditionalMetadata("Placeholder", "0")]
        public int Pupil { get; set; }
    }
}