﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PrintSchoolReportArea
{
    public class PrintSchoolReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PrintSchoolReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PrintSchoolReportArea_default",
                "PrintSchoolReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
