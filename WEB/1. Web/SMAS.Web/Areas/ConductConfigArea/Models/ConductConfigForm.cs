using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.ConductConfigArea.Models
{
    public class ConductConfigForm
    {

        public int checkSave { get; set; }
        public int CapacityLevelID { get; set; }
        public int lstConductLevel { get; set; }
        public int rdoRank { get; set; }
       // [UIHint("Resolution"), Required]
        [ResourceDisplayName("ConductLevel_Label_Resolution")]
        public string Resolution{ get; set; }
        public string ResolutionHide { get; set; }
        [ResourceDisplayName("CapacityLevel_CapacityLevel")]
        [Required]
        public string CapacityLevel { get; set; }
        [ResourceDisplayName("RankingCriteriaConfig_Label_AvgMark")]
        public bool chkAvgMark { get; set; }

        [ResourceDisplayName("RankingCriteriaConfig_Label_Capacity")]
        public bool chkCapacity { get; set; }

        [ResourceDisplayName("RankingCriteriaConfig_Label_Conduct")]
        public bool chkConduct { get; set; }

        [ResourceDisplayName("RankingCriteriaConfig_Label_SecondSemesterIsAll")]
        public bool chkSecondSemesterIsAll { get; set; }

        [ResourceDisplayName("RankingCriteriaConfig_Label_ViewAll")]
        public bool chkViewAll { get; set; }

        [ResourceDisplayName("RankingCriteriaConfig_Label_HeadTeacherCanSendSMS")]
        public bool chkHeadTeacherCanSendSMS { get; set; }
        [ResourceDisplayName("RankingCriteriaConfig_Label_SubjectTeacherCanSendSMS")]
        public bool chkSubjectTeacherCanSendSMS { get; set; }

        [ResourceDisplayName("AcademicYear_Label_ShowAvatar")]
        public bool? chkShowAvatar { get; set; }

        [ResourceDisplayName("AcademicYear_Label_ShowCalendar")]
        public bool? chkshowCalendar { get; set; }
        
        [ResourceDisplayName("ConfigurationConfig_Label_SubjectJudge")]
        public bool? chkClassification { get; set; }

        [ResourceDisplayName("AcademicYear_Label_ShowPupil")]
        public bool? chkShowPupil { get; set; }
        
    }
}
