﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.ConductConfigArea.Models
{
    public class ViolationForm
    {


        [ResourceDisplayName("ConductConfigByViolation_Label_MinValue")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public decimal? MinValue { get; set; }

        [ResourceDisplayName("ConductConfigByViolation_Label_MaxValue")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public decimal? MaxValue { get; set; }

        [ResourceDisplayName("ConductLevel_Label_Resolution")]
        public string Resolution { get; set; }

        [ResourceDisplayName("ConductLevel_Label_ConductLevelID")]
        public int ConductLevelID { get; set; }
    }
}