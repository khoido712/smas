﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.ConductConfigArea.Models
{
    public class RankingCriteriaConfigForm
    {
        [ResourceDisplayName("RankingCriteriaConfig_Label_AvgMark")]
        public bool chkAvgMark { get; set; }

        [ResourceDisplayName("RankingCriteriaConfig_Label_Capacity")]
        public bool chkCapacity { get; set; }

        [ResourceDisplayName("RankingCriteriaConfig_Label_Conduct")]
        public bool chkConduct { get; set; }
    }
}