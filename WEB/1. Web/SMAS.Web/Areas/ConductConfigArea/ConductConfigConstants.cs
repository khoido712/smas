﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ConductConfigArea
{
    public class ConductConfigConstants
    {
        public const string COUNT_CAPACITY = "COUNT_CAPACITY";
        public const string COUNT_VIOLATION = "COUNT_VIOLATION";
        public const string LS_CapacityLevel = "ListCapacityLevel";
        public const string LS_ConductConfigByCapacity = "ListCapacity";
        public const string LS_ConductConfigByViolation = "ListViolation";
        public const string LS_ConductLevel = "ListConductLevel";
        public const string NBR_ConductLevel = "NumberConductLevel";
    }
}