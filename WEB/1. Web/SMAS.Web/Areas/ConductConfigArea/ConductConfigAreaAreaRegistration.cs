﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ConductConfigArea
{
    public class ConductConfigAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ConductConfigArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ConductConfigArea_default",
                "ConductConfigArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
