﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.Web.Areas.ConductConfigArea.Models;
using SMAS.Web.Filter;
using SMAS.Business.Common;
namespace SMAS.Web.Areas.ConductConfigArea.Controllers
{
    public class ConductConfigController : BaseController
    {
        #region private member variable
        private readonly IConductConfigByCapacityBusiness ConductConfigByCapacityBusiness;
        private readonly IConductConfigByViolationBusiness ConductConfigByViolationBusiness;
        private readonly ICapacityLevelBusiness CapacityLevelBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IThreadMarkBusiness ThreadMarkBusiness; 

        private const int COUNT_NEXT = 1;
        private const int COUNT_DEFAULT = 0;
        #endregion

        #region Constructors
        public ConductConfigController(ICapacityLevelBusiness capacityLevelBusiness, IConductConfigByCapacityBusiness conductConfigByCapacityBusiness
                                       ,IConductConfigByViolationBusiness conductConfigByViolationBusiness, IAcademicYearBusiness academicYearBusiness
                                       , IConductLevelBusiness conductLevelBusiness, IThreadMarkBusiness threadMarkBusiness)
        {
            this.CapacityLevelBusiness = capacityLevelBusiness;
            this.ConductConfigByCapacityBusiness = conductConfigByCapacityBusiness;
            this.ConductConfigByViolationBusiness = conductConfigByViolationBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ConductLevelBusiness = conductLevelBusiness;
            this.ThreadMarkBusiness = threadMarkBusiness;
        }
        #endregion

        #region Index
        public ActionResult Index()
        {
            SetViewDataCapacity();

            GlobalInfo GlobalInfo = new GlobalInfo();
            ConductConfigForm frm = new ConductConfigForm();
            IDictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2["IsActive"] = true;
            dic2["SchoolID"] = GlobalInfo.SchoolID.Value;
            dic2["AcademicYearID"] = GlobalInfo.AcademicYearID.Value;
            List<AcademicYear> lsAcademicYear = AcademicYearBusiness.Search(dic2).ToList();
            AcademicYear AcademicYear = lsAcademicYear.FirstOrDefault();
            frm.rdoRank = AcademicYear.ConductEstimationType;
            if ((AcademicYear.RankingCriteria & SMAS.Business.Common.SystemParamsInFile.RANKING_CRITERIA_CAPACITY) != 0)
            {
                frm.chkCapacity = true;
            }
            if ((AcademicYear.RankingCriteria & SMAS.Business.Common.SystemParamsInFile.RANKING_CRITERIA_CONDUCT) != 0)
            {
                frm.chkConduct = true;
            }
            frm.chkSecondSemesterIsAll = AcademicYear.IsSecondSemesterToSemesterAll;
            frm.chkViewAll = AcademicYear.IsTeacherCanViewAll ;
            frm.chkHeadTeacherCanSendSMS = AcademicYear.IsHeadTeacherCanSendSMS ;
            frm.chkSubjectTeacherCanSendSMS = AcademicYear.IsSubjectTeacherCanSendSMS;
            frm.chkShowAvatar = AcademicYear.IsShowAvatar;
            frm.chkshowCalendar = AcademicYear.IsShowCalendar;
            frm.chkClassification = AcademicYear.IsClassification;
            frm.chkShowPupil = AcademicYear.IsShowPupil;

            SetViewDataPermission("ConductConfig", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);

            return View(frm);
        }
        #endregion

        #region SetviewData
        public void SetViewDataCapacity()
        {
            ViewData[ConductConfigConstants.LS_ConductConfigByCapacity] = new List<ConductConfigForm>();
            ViewData[ConductConfigConstants.LS_ConductConfigByViolation] = new List<ViolationForm>();
            ViewData[ConductConfigConstants.COUNT_CAPACITY]= 0;
            ViewData[ConductConfigConstants.COUNT_VIOLATION] = 0;
            GlobalInfo GlobalInfo = new GlobalInfo();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            dic["SchoolID"] = GlobalInfo.SchoolID;
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            List<CapacityLevel> lsCapacityLevel = CapacityLevelBusiness.All.Where(o => o.IsActive == true ).OrderBy(o => o.CapacityLevelID).ToList();
            ViewData[ConductConfigConstants.LS_CapacityLevel] = lsCapacityLevel.Take(5).ToList();
            if (GlobalInfo.AppliedLevel.Value != 1)
            {
                List<ConductLevel> lsConductLevel = ConductLevelBusiness.All.Where(o => o.IsActive == true && o.AppliedLevel == GlobalInfo.AppliedLevel.Value).ToList();
                ViewData[ConductConfigConstants.LS_ConductLevel] = lsConductLevel;

                IQueryable<ConductConfigByCapacity> lsCapacityCheck = ConductConfigByCapacityBusiness.Search(dic);
                List<ConductConfigForm> lsConductConfigForm = new List<ConductConfigForm>();
                if (lsCapacityCheck.Count() == 0)
                {
                    List<ConductConfigByCapacity> lstCapacity = new List<ConductConfigByCapacity>();

                    //gan mac dinh
                    for (int i = 0; i < 5; i++)
                    {
                        ConductConfigForm ConductConfigForm = new ConductConfigForm();
                        ConductConfigForm.lstConductLevel = (i < 1 ? lsConductLevel[0].ConductLevelID : (i > 3 ? lsConductLevel[3].ConductLevelID : lsConductLevel[i - 1].ConductLevelID));
                        ConductConfigForm.CapacityLevelID = lsCapacityLevel[i].CapacityLevelID;
                        ConductConfigForm.CapacityLevel = lsCapacityLevel[i].CapacityLevel1;
                        ConductConfigForm.Resolution = (i < 1 ? lsConductLevel[0].Resolution : (i > 3 ? lsConductLevel[3].Resolution : lsConductLevel[i - 1].Resolution));
                        ConductConfigForm.ResolutionHide = ConductConfigForm.Resolution;
                        lsConductConfigForm.Add(ConductConfigForm);
                    }
                    ViewData[ConductConfigConstants.LS_ConductConfigByCapacity] = lsConductConfigForm.ToList();
                }
                else
                {
                    lsCapacityCheck = ConductConfigByCapacityBusiness.Search(dic);

                    //Grid Capacity
                    lsCapacityCheck = lsCapacityCheck.OrderBy(o => o.ConductConfigByCapacityID);

                    IQueryable<ConductConfigForm> res = lsCapacityCheck.Select(o => new ConductConfigForm
                    {
                        CapacityLevel = o.CapacityLevel.CapacityLevel1,
                        CapacityLevelID = o.CapacityLevel.CapacityLevelID,
                        lstConductLevel = o.ConductLevelID,
                        Resolution = o.ConductLevel.Resolution,
                        ResolutionHide = o.ConductLevel.Resolution,
                    });
                    ViewData[ConductConfigConstants.LS_ConductConfigByCapacity] = res.ToList();
                    ViewData[ConductConfigConstants.COUNT_CAPACITY] = res.Count();
                }

                //Grid Violation
                List<ViolationForm> lsViolationForm = new List<ViolationForm>();
                IQueryable<ConductConfigByViolation> lsViolation = ConductConfigByViolationBusiness.Search(dic);
                if (lsViolation.Count() == 0)
                {
                    List<ConductConfigByViolation> lstViolation = new List<ConductConfigByViolation>();
                    //gan mac dinh
                    for (int i = 0; i < 4; i++)
                    {
                        ViolationForm ViolationForm = new ViolationForm();
                        ViolationForm.ConductLevelID = lsConductLevel[i].ConductLevelID;
                        ViolationForm.MinValue = COUNT_DEFAULT;
                        ViolationForm.MaxValue = COUNT_DEFAULT;
                        ViolationForm.Resolution = lsConductLevel[i].Resolution;
                        lsViolationForm.Add(ViolationForm);
                    }
                    ViewData[ConductConfigConstants.LS_ConductConfigByViolation] = lsViolationForm.ToList();
                }
                else
                {
                    lsViolation = ConductConfigByViolationBusiness.Search(dic).OrderBy(o => o.ConductConfigByViolationID);
                    IQueryable<ViolationForm> res2 = lsViolation.Select(o => new ViolationForm
                    {
                        MinValue = o.MinValue,
                        MaxValue = o.MaxValue,
                        Resolution = o.ConductLevel.Resolution,
                        ConductLevelID = o.ConductLevelID
                    });
                    //Paginate<ViolationForm> Paging2 = new Paginate<ViolationForm>(res2);
                    ViewData[ConductConfigConstants.LS_ConductConfigByViolation] = res2.ToList();
                    ViewData[ConductConfigConstants.COUNT_VIOLATION] = res2.Count();
                }
            }
        }
        #endregion

        #region Save
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(ConductConfigForm frm, List<int> lstConductLevel, List<decimal> MinValue, List<decimal> MaxValue)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ConductConfig", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            #region xếp loại hạnh kiểm theo học lực
            if (frm.rdoRank == 1)
            {
                if (GlobalInfo.AppliedLevel.Value != 1)
                {
                    //Xoa du lieu da config
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["SchoolID"] = GlobalInfo.SchoolID;
                    dic["AppliedLevel"] = GlobalInfo.AppliedLevel.Value;
                    List<ConductConfigByCapacity> lsCapacity = ConductConfigByCapacityBusiness.Search(dic).ToList();
                    //List<ConductConfigByViolation> lsViolation = ConductConfigByViolationBusiness.Search(dic).ToList();
                    ConductConfigByCapacityBusiness.Delete(lsCapacity, GlobalInfo.SchoolID.Value);
                    //ConductConfigByViolationBusiness.Delete(lsViolation, GlobalInfo.SchoolID.Value);

                    //Them moi config
                    ConductConfigByCapacity ConductConfigByCapacity = new ConductConfigByCapacity();
                    ConductConfigByCapacity.SchoolID = GlobalInfo.SchoolID.Value;

                    List<ConductConfigByCapacity> lsConductConfigByCapacity = new List<ConductConfigByCapacity>();
                    if (lstConductLevel != null)
                    {
                        for (int i = 0; i < lstConductLevel.Count; i++)
                        {
                            ConductConfigByCapacity = new ConductConfigByCapacity();
                            ConductConfigByCapacity.SchoolID = GlobalInfo.SchoolID.Value;
                            ConductConfigByCapacity.ConductLevelID = lstConductLevel[i];
                            ConductConfigByCapacity.CapacityLevelID = i + COUNT_NEXT;
                            ConductConfigByCapacity.AppliedLevel = GlobalInfo.AppliedLevel.Value;
                            lsConductConfigByCapacity.Add(ConductConfigByCapacity);
                        }
                    }
                    ConductConfigByCapacityBusiness.Insert(lsConductConfigByCapacity);
                }
                
                AcademicYear AcademicYear = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID.Value);
                AcademicYear.RankingCriteria = (int)(SMAS.Business.Common.SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK
                                                    + (frm.chkCapacity == true ? 1 : 0) * SMAS.Business.Common.SystemParamsInFile.RANKING_CRITERIA_CAPACITY
                                                    + (frm.chkConduct == true ? 1 : 0) * SMAS.Business.Common.SystemParamsInFile.RANKING_CRITERIA_CONDUCT);
                AcademicYear.ConductEstimationType = SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY;
                AcademicYear.IsSecondSemesterToSemesterAll = frm.chkSecondSemesterIsAll;
                AcademicYear.IsTeacherCanViewAll = frm.chkViewAll;
                AcademicYear.IsHeadTeacherCanSendSMS = frm.chkHeadTeacherCanSendSMS;
                AcademicYear.IsSubjectTeacherCanSendSMS = frm.chkSubjectTeacherCanSendSMS;
                AcademicYear.IsShowAvatar = frm.chkShowAvatar;
                AcademicYear.IsShowCalendar = frm.chkshowCalendar;
                AcademicYear.IsClassification = frm.chkClassification;
                AcademicYear.IsShowPupil = frm.chkShowPupil;
                AcademicYearBusiness.Update(AcademicYear);
                AcademicYearBusiness.Save();               
            }
            #endregion
            #region Xếp loại hạnh kiểm theo vi phạm
            else
            {
                if (GlobalInfo.AppliedLevel.Value != 1)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["SchoolID"] = GlobalInfo.SchoolID.Value;
                    dic["AppliedLevel"] = GlobalInfo.AppliedLevel.Value;
                   // List<ConductConfigByCapacity> lsCapacity = ConductConfigByCapacityBusiness.Search(dic).ToList();
                    List<ConductConfigByViolation> lsViolation = ConductConfigByViolationBusiness.Search(dic).ToList();
                   // ConductConfigByCapacityBusiness.Delete(lsCapacity, GlobalInfo.SchoolID.Value);
                    ConductConfigByViolationBusiness.Delete(lsViolation, GlobalInfo.SchoolID.Value);

                    ConductConfigByViolation ConductConfigByViolation = new ConductConfigByViolation();
                    ConductConfigByViolation.SchoolID = GlobalInfo.SchoolID.Value;
                    List<ConductConfigByViolation> lsConductConfigByViolation = new List<ConductConfigByViolation>();
                    if (MinValue != null && MaxValue != null)
                    {
                        for (int i = 0; i < MinValue.Count; i++)
                        {
                            ConductConfigByViolation = new ConductConfigByViolation();
                            ConductConfigByViolation.SchoolID = GlobalInfo.SchoolID.Value;
                            ConductConfigByViolation.MinValue = MinValue[i];
                            ConductConfigByViolation.MaxValue = MaxValue[i];
                            if (GlobalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY )
                            {
                                ConductConfigByViolation.ConductLevelID = (int)(i + 3);
                            }
                            if (GlobalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            {
                                ConductConfigByViolation.ConductLevelID = (int)(i + 7);
                            }
                            ConductConfigByViolation.IsActive = true;
                            ConductConfigByViolation.AppliedLevel = (int)GlobalInfo.AppliedLevel.Value;

                            lsConductConfigByViolation.Add(ConductConfigByViolation);
                        }
                    }
                    ConductConfigByViolationBusiness.Insert(lsConductConfigByViolation);
                }
                //Tiêu chí xếp hạng
                IDictionary<string, object> dic2 = new Dictionary<string, object>();
                dic2["IsActive"] = true;
                dic2["SchoolID"] = GlobalInfo.SchoolID.Value;
                dic2["AcademicYearID"] = GlobalInfo.AcademicYearID.Value;
                List<AcademicYear> lsAcademicYear = AcademicYearBusiness.Search(dic2).ToList();
                AcademicYear AcademicYear = lsAcademicYear.FirstOrDefault();
                AcademicYear.RankingCriteria = (int)(SMAS.Business.Common.SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK
                                            + (frm.chkCapacity == true ? 1 : 0) * SMAS.Business.Common.SystemParamsInFile.RANKING_CRITERIA_CAPACITY
                                            + (frm.chkConduct == true ? 1 : 0) * SMAS.Business.Common.SystemParamsInFile.RANKING_CRITERIA_CONDUCT);
                AcademicYear.ConductEstimationType = SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION;
                AcademicYear.IsSecondSemesterToSemesterAll = frm.chkSecondSemesterIsAll;
                AcademicYear.IsTeacherCanViewAll = frm.chkViewAll;
                AcademicYear.IsHeadTeacherCanSendSMS = frm.chkHeadTeacherCanSendSMS;
                AcademicYear.IsSubjectTeacherCanSendSMS = frm.chkSubjectTeacherCanSendSMS;
                AcademicYear.IsShowAvatar = frm.chkShowAvatar;
                AcademicYear.IsShowCalendar = frm.chkshowCalendar;
                AcademicYear.IsClassification = frm.chkClassification;
                AcademicYear.IsShowPupil = frm.chkShowPupil;
                AcademicYearBusiness.Update(AcademicYear);
                AcademicYearBusiness.Save();             
            }
            #endregion
            #region update bang ThreadMark tinh lai TBM tu dong
            /*IDictionary<string, object> dicThreadMark = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            ThreadMarkBusiness.UpdateThreadMark(dicThreadMark);*/
            #endregion
                return Json(new JsonMessage(Res.Get("Common_Label_Save"), JsonMessage.SUCCESS));
            }
        #endregion

        #region Loadcombobox
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _SelectResolution()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            IQueryable<ConductLevel> lsConductLevel = ConductLevelBusiness.All.Where(o => o.IsActive == true);
            ViewData[ConductConfigConstants.LS_ConductLevel] = lsConductLevel;

            return new JsonResult { Data = new SelectList(lsConductLevel.ToList(), "ConductLevelID", "Resolution") };
        }
        #endregion
    }

}
