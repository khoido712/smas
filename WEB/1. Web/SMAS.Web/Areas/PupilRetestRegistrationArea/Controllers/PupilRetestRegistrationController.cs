﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PupilRetestRegistrationArea.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.PupilRetestRegistrationArea.Controllers
{
    public class PupilRetestRegistrationController : BaseController
    {
        private readonly IPupilRetestRegistrationBusiness PupilRetestRegistrationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IVSummedUpRecordBusiness VSummedUpRecordBusiness;

        public PupilRetestRegistrationController(IPupilRetestRegistrationBusiness pupilretestregistrationBusiness, IClassProfileBusiness ClassProfileBusiness, IClassSubjectBusiness ClassSubjectBusiness, IPupilRankingBusiness PupilRankingBusiness, ISummedUpRecordBusiness SummedUpRecordBusiness, IAcademicYearBusiness AcademicYearBusiness, ISubjectCatBusiness SubjectCatBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IVSummedUpRecordBusiness VSummedUpRecordBusiness)
        {
            this.PupilRetestRegistrationBusiness = pupilretestregistrationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.VSummedUpRecordBusiness = VSummedUpRecordBusiness;
        }

        public ActionResult Index()
        {
            List<EducationLevel> lstEducationLevel = _globalInfo.EducationLevels;
            lstEducationLevel = lstEducationLevel.Where(o => o.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_NINTH && o.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_TWELFTH && o.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_FIFTH).ToList();
            ViewData[PupilRetestRegistrationConstants.LIST_EDUCATIONLEVEL] = lstEducationLevel;
            ViewData[PupilRetestRegistrationConstants.LIST_CLASSPROFILE] = new SelectList(new string[] { });
            ViewData[PupilRetestRegistrationConstants.SHOWPANEL] = false;
            return View();
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult GetData(FormCollection form)
        {
            int? EducationLevelID = SMAS.Business.Common.Utils.GetInt(form["cboEducationLevel"]);
            int? ClassID = 0;
            if (form["cboClass"] != "")
            {
                ClassID = SMAS.Business.Common.Utils.GetInt(form["cboClass"]);
            }

            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            ViewData[PupilRetestRegistrationConstants.SHOWPANEL] = true;
            ViewData[PupilRetestRegistrationConstants.educationid] = EducationLevelID;
            ViewData[PupilRetestRegistrationConstants.classid] = ClassID;

            // QuangLM comment cho thong that giua cac chua nang
            // Phan khai bao mon hoc theo cap da duoc bat o phan khai bao mon hoc cho lop roi
            //SearchInfo["AppliedLevel"] = glo.AppliedLevel.Value;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["EducationLevelID"] = EducationLevelID.Value;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["IsVNEN"] = true;

            List<ClassSubject> listClassSubject = new List<ClassSubject>();
            List<int> lstClassIDWithRole = new List<int>();
            // AnhVD 20140922 - load THÔNG TIN theo phân quyền
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsEmployeeManager || _globalInfo.IsViewAll) // Không check cấu hình xem thông tin
            {
                listClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();
                IDictionary<string, object> dicInfo = new Dictionary<string, object>();
                dicInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicInfo["EducationLevelID"] = EducationLevelID.Value;
                var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicInfo).OrderBy(u => u.DisplayName).ToList();
                lstClassIDWithRole = lstClass.Select(o => o.ClassProfileID).ToList();
            }
            else
            {
                SearchInfo.Add("UserAccountID", _globalInfo.UserAccountID);
                SearchInfo.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                lstClassIDWithRole = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).Select(o => o.ClassProfileID).ToList();
                if (lstClassIDWithRole != null && lstClassIDWithRole.Count == 0)
                {
                    lstClassIDWithRole.Add(0); // Tìm với dsach rỗng
                }
                SearchInfo.Add("LstClassID ", lstClassIDWithRole);
                listClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();
            }

            List<SubjectCat> listSubjectCat = listClassSubject.Select(u => u.SubjectCat).Distinct().OrderBy(o => o.OrderInSubject).ToList();
            List<int> listSJ = listSubjectCat.Select(o => o.SubjectCatID).ToList();
            List<int> listCoreSJ = listSubjectCat.Where(x => x.IsCoreSubject).Select(o => o.SubjectCatID).ToList();
            ViewData[PupilRetestRegistrationConstants.LIST_SUBJECT] = listSubjectCat;
            int semester = _globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                    ? SystemParamsInFile.SEMESTER_OF_YEAR_SECOND : SystemParamsInFile.SEMESTER_OF_YEAR_ALL;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID.Value;
            dic["ClassID"] = ClassID;
            dic["StudyingJudgementID"] = SMAS.Web.Constants.GlobalConstants.STUDYING_JUDGEMENT_RETEST;
            dic["Semester"] = semester;
            dic["ListClassID"] = lstClassIDWithRole;
            dic["IsVNEN"] = true;


            IEnumerable<PupilRankingBO> listPupilRanking = PupilRankingBusiness.GetPupilRetest(dic).Distinct();
            // Kiểm tra lấy thông tin chi tiết HS - Lớp mới nhất
            SearchInfo["Check"] = "on";
            IEnumerable<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).Where(p => p.Status == Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING).ToList();

            List<int> listClassID = listPupilRanking.Where(u => u.ClassID.HasValue).Select(u => u.ClassID.Value).Distinct().ToList();
            List<ClassProfile> listClassProfile = ClassProfileBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value
                && o.AcademicYearID == _globalInfo.AcademicYearID.Value && o.IsActive.Value).Where(o => listClassID.Contains(o.ClassProfileID)).ToList();
            Dictionary<int, List<PupilRetestRegistration>> dicPupilRetest = new Dictionary<int, List<PupilRetestRegistration>>();
            List<PupilRetestRegistration> listPupilRetestRegistrationAll = PupilRetestRegistrationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value } })
                .Where(o => listClassID.Contains(o.ClassID)).ToList();
            listClassID.ForEach(u =>
            {
                dicPupilRetest.Add(u, listPupilRetestRegistrationAll.Where(o => o.ClassID == u).ToList());
            });

            List<PupilRankingBO> listPupil = new List<PupilRankingBO>();
            List<int> listPupilID = new List<int>();
            List<int> listPupilClassID = new List<int>();

            foreach (PupilRankingBO pr in listPupilRanking)
            {
                // Lay them mot so thong tin phuc vu sap xep
                PupilOfClass poc = lstPoc.Where(o => o.PupilID == pr.PupilID).FirstOrDefault();
                pr.OrderInClass = poc != null ? poc.OrderInClass : null;
                ClassProfile cp = listClassProfile.Find(o => o.ClassProfileID == pr.ClassID);
                pr.ClassOrderNumber = cp.OrderNumber;
                pr.EducationLevelID = cp.EducationLevelID;
                var objPupilExist = false;
                if (listPupil.Any(x => pr.PupilID != null && pr.PupilID == x.PupilID && pr.ClassID.Value == x.ClassID))
                {
                    objPupilExist = true;
                }
                if (!objPupilExist)
                {
                    listPupil.Add(pr);
                    listPupilID.Add(pr.PupilID.Value);
                    listPupilClassID.Add(pr.ClassID.Value);
                }
            }

            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["ClassID"] = ClassID;
            dic["Semester"] = semester;

            IQueryable<VSummedUpRecord> iqSummedUpRecord = (from p in VSummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                                                        .Where(u =>
                                                                         listPupilID.Contains(u.PupilID) && !u.PeriodID.HasValue
                                                                        && (u.JudgementResult == PupilRetestRegistrationConstants.string_CD
                                                                        || u.JudgementResult == PupilRetestRegistrationConstants.string_B
                                                                        || u.SummedUpMark < 5))
                                                            join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                            where q.EducationLevelID == EducationLevelID
                                                            && (!q.IsActive.HasValue || (q.IsActive.HasValue && q.IsActive.Value))
                                                            select p);

            List<VSummedUpRecord> listSummedUpRecordPrimaryHK5 = new List<VSummedUpRecord>();
            //if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            //{
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["Year"] = acaYear.Year;
            dic["ClassID"] = ClassID;
            dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_RETEST;

            listSummedUpRecordPrimaryHK5 = (from p in VSummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                                                    .Where(u => !u.PeriodID.HasValue)
                                            join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                            where q.EducationLevelID == EducationLevelID
                                            && (!q.IsActive.HasValue || (q.IsActive.HasValue && q.IsActive.Value))
                                            select p).ToList();
            //}

            List<PupilRetestRegistrationViewModel> listPupilRetestRegistrationViewModel = new List<PupilRetestRegistrationViewModel>();
            ViewData[PupilRetestRegistrationConstants.ENABLE_BUTTON] = "false";

            if (ClassID == 0 || ClassID == null)
            {
                if (_globalInfo.IsAdminSchoolRole)
                {
                    ViewData[PupilRetestRegistrationConstants.ENABLE_BUTTON] = "true";
                }
            }
            else
            {
                if (UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID.Value))
                    ViewData[PupilRetestRegistrationConstants.ENABLE_BUTTON] = "true";
            }
            foreach (PupilRankingBO pr in listPupil)
            {
                if (pr.ClassID == null || pr.PupilID == null) continue;

                PupilRetestRegistrationViewModel pupil = new PupilRetestRegistrationViewModel();
                pupil.PupilID = pr.PupilID.Value;
                pupil.FullName = pr.FullName;
                pupil.ClassName = listClassProfile.Where(u => u.ClassProfileID == pr.ClassID).SingleOrDefault().DisplayName;
                pupil.OrderInClass = pr.OrderInClass;
                pupil.PupilName = pr.Name;
                pupil.EducationLevelID = pr.EducationLevelID;
                pupil.ClassOrderNumber = pr.ClassOrderNumber;

                //Check vao cot mon da dang ky
                pupil.Selected = new string[listSJ.Count];
                pupil.Disabled = new string[listSJ.Count];
                pupil.JudgementResult = new string[listSJ.Count];
                pupil.SummedUpMark = new Decimal?[listSJ.Count];
                pupil.CoreSubject = new bool?[listSJ.Count];
                for (int i = 0; i < listSJ.Count; i++)
                    pupil.Disabled[i] = "Disabled";

                List<VSummedUpRecord> listSurOfPupil = iqSummedUpRecord.Where(u => u.ClassID == pr.ClassID && u.PupilID == pr.PupilID).ToList();

                int index = 0;
                foreach (VSummedUpRecord sur in listSurOfPupil)
                {
                    index = 0;
                    if (sur.JudgementResult != null && sur.JudgementResult != " ")
                    {
                        if ((_globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                && sur.JudgementResult == (PupilRetestRegistrationConstants.string_B))
                                    || ((_globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                                        || _globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                                        && sur.JudgementResult == (PupilRetestRegistrationConstants.string_CD)))
                        {
                            foreach (int sj in listSJ)
                            {
                                if (sj == sur.SubjectID)
                                {
                                    pupil.Disabled[index] = "";
                                    pupil.JudgementResult[index] = PupilRetestRegistrationConstants.string_CD;
                                    break;
                                }
                                index++;
                            }
                        }
                    }
                    else if (sur.SummedUpMark < 5)
                    {
                        foreach (int sj in listSJ)
                        {
                            if (sj == sur.SubjectID)
                            {
                                pupil.Disabled[index] = "";
                                var objCore = listCoreSJ.Where(x => x == sj).Select(x => x).FirstOrDefault();
                                if (objCore > 0)
                                {
                                    pupil.CoreSubject[index] = true;
                                }

                                pupil.SummedUpMark[index] = sur.SummedUpMark;
                                //pupil.SummedUpMark[index] = sur.SummedUpMark;
                                break;
                            }
                            index++;
                        }
                    }
                }


                // set lai index
                index = 0;
                IEnumerable<VSummedUpRecord> listSurHK5OfPupil = listSummedUpRecordPrimaryHK5.Where(u => u.PupilID == pr.PupilID && u.ClassID == pr.ClassID);
                foreach (int sj in listSJ)
                {
                    // kiem tra lop hoc co mon hoc nay khong
                    if (!listClassSubject.Any(o => o.ClassID == pr.ClassID && o.SubjectID == sj))
                    {
                        pupil.Disabled[index] = "Disabled";
                        goto Finish;
                    }
                    //trang thai hoc sinh phai la dang hoc khong thi disabled
                    PupilOfClass obj = lstPoc.FirstOrDefault(u => u.ClassID == pr.ClassID && u.PupilID == pr.PupilID);
                    if (obj == null || obj.Status != Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        pupil.Disabled[index] = "Disabled";
                        goto Finish;
                    }

                    List<PupilRetestRegistration> listPupilRetestRegistration = dicPupilRetest[pr.ClassID.Value].Where(u => u.SubjectID == sj && u.PupilID == pr.PupilID).ToList();
                    if (listPupilRetestRegistration != null && listPupilRetestRegistration.Count() > 0)
                    {
                        pupil.Selected[index] = "checked";
                        PupilRetestRegistration purr = listPupilRetestRegistration.OrderByDescending(o => o.PupilRetestRegistrationID).FirstOrDefault();
                        if (purr.RegisteredDate != null && purr.ModifiedDate == null)
                        {
                            pupil.Disabled[index] = "";
                        }
                        else
                        {
                            VSummedUpRecord su = listSurHK5OfPupil.Where(u => u.SubjectID == sj).FirstOrDefault();
                            if (purr.ModifiedDate != null &&
                                (_globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY))
                            {
                                pupil.Disabled[index] = "Disabled";
                                //Các hs đã đăng ký thi lại và có kết quả thì lại
                                if (su != null && sj == su.SubjectID)
                                    //pupil.SummedUpMark[index] = su.SummedUpMark;
                                    pupil.SummedUpMark[index] = su.SummedUpMark;
                            }
                            else
                                if (_globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY && purr.ModifiedDate != null)
                            {
                                if (su != null)
                                {
                                    if (su.ReTestMark >= 5 || su.ReTestJudgement != "B")
                                    {
                                        pupil.Disabled[index] = "Disabled";
                                    }
                                    else
                                        if (su.ReTestMark < 5 || su.ReTestJudgement != "B")
                                    {
                                        if (listPupilRetestRegistration.Count(u => u.ModifiedDate.HasValue) < 3)
                                        {
                                            pupil.Disabled[index] = "";
                                            pupil.Selected[index] = "";
                                        }
                                        else
                                        {
                                            pupil.Disabled[index] = "Disabled";
                                        }
                                    }
                                }
                            }
                        }

                    }
                    Finish:
                    index++;
                }

                if (pupil.CoreSubject.Where(x => x.HasValue && x.Value == true).Count() == listCoreSJ.Count)
                {
                    pupil.IsWeakAllCoreSubject = true;
                }

                //AnhVD 20140711 - khong hien thi neu khong thi lai mon nao (fix cho truong hop thua du lieu trong pupil_ranking)
                if (!(pupil.Disabled.All(o => o == "Disabled") && pupil.Selected.All(o => o == null || o == "")))
                {
                    listPupilRetestRegistrationViewModel.Add(pupil);
                }
                // End
            }

            //Nếu cboClass  là 1 lớp bất kỳ thì sắp xếp theo PupilOfClass.OrderInClass.
            if (ClassID != 0)
            {
                listPupilRetestRegistrationViewModel = listPupilRetestRegistrationViewModel.OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilName).ThenBy(o => o.FullName).ToList();
            }
            else
            {
                listPupilRetestRegistrationViewModel = listPupilRetestRegistrationViewModel.OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber.Value : 0).ThenBy(o => o.ClassName)
                    .ThenBy(o => o.OrderInClass).ThenBy(o => o.PupilName).ThenBy(o => o.FullName).ToList();
            }
            ViewData[PupilRetestRegistrationConstants.LIST_PUPILRETESTREGISTRATION] = listPupilRetestRegistrationViewModel;
            return PartialView("_List");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult Create(FormCollection form)
        {
            int? EducationLevelID = SMAS.Business.Common.Utils.GetInt(form["EducationLevelID"]);
            int? ClassId = SMAS.Business.Common.Utils.GetInt(form["ClassID"]);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID.Value;
            dic["ClassID"] = ClassId.Value;
            int semester = 0;

            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                semester = SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            else semester = SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_ALL;

            dic["Semester"] = semester;
            dic["StudyingJudgementID"] = SystemParamsInFile.STUDYING_JUDGEMENT_RETEST;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;

            // AnhVD 20140922 - load THÔNG TIN theo phân quyền
            List<int> lstClassIDWithRole = new List<int>();
            if (!_globalInfo.IsAdminSchoolRole) // Không check cấu hình xem thông tin
            {

                dic.Add("UserAccountID", _globalInfo.UserAccountID);
                dic.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                lstClassIDWithRole = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Select(o => o.ClassProfileID).ToList();
                if (lstClassIDWithRole != null && lstClassIDWithRole.Count == 0)
                {
                    lstClassIDWithRole.Add(0); // Tìm với dsach rỗng
                }
                dic.Add("ListClassID ", lstClassIDWithRole);
            }
            // End 20140922

            IEnumerable<PupilRankingBO> listPupilRanking = PupilRankingBusiness.GetPupilRetest(dic);

            List<PupilRankingBO> listPupil = new List<PupilRankingBO>();
            List<int> lstPupilID = new List<int>();

            foreach (PupilRankingBO pr in listPupilRanking)
            {
                bool chk = false;
                if (pr.PupilID == null) { continue; }
                foreach (int pupilId in lstPupilID)
                {
                    if (pr.PupilID.Value == pupilId)
                    {
                        chk = true;
                    }
                }
                if (!chk)
                {
                    listPupil.Add(pr);
                    lstPupilID.Add(pr.PupilID.Value);
                }
            }

            dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = EducationLevelID.Value;
            dic["ClassID"] = ClassId;
            dic["ListClassID"] = lstClassIDWithRole;
            List<PupilRetestRegistration> listRetestPupils = PupilRetestRegistrationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID.Value;
            dic["ClassID"] = ClassId;
            dic["ListClassID"] = lstClassIDWithRole;
            IEnumerable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            List<PupilRetestRegistration> listPupilRetestRegistration = new List<PupilRetestRegistration>();
            foreach (PupilRankingBO pr in listPupil)
            {
                if (pr.PupilID == null || pr.ClassID == null)
                    continue;
                IEnumerable<ClassSubject> listClassSubject1 = listClassSubject.Where(o => o.ClassID == pr.ClassID);
                IEnumerable<PupilRetestRegistration> listRetestOfPupil = listRetestPupils.Where(u => u.PupilID == pr.PupilID);
                foreach (ClassSubject cs in listClassSubject1)
                {
                    String status = (String)form[pr.PupilID.ToString() + "_" + cs.SubjectID + "_Hidden"];
                    String selected_hidden = (String)form[pr.PupilID.ToString() + "_" + cs.SubjectID + "_Selected"];

                    if (status == null || selected_hidden == null || status == "Disabled")
                    {
                        continue;
                    }

                    String chk = (String)form[pr.PupilID.ToString() + "_" + cs.SubjectID];
                    PupilRetestRegistration pupilRetestRegistration = listRetestOfPupil.Where(u => u.ClassID == pr.ClassID && u.SubjectID == cs.SubjectID && u.ModifiedDate == null).SingleOrDefault();
                    if (chk == null)
                    {
                        if (selected_hidden.Equals("checked"))
                        {
                            if (pupilRetestRegistration != null)
                                PupilRetestRegistrationBusiness.DeletePupilRetestRegistration(pupilRetestRegistration.PupilRetestRegistrationID, _globalInfo.SchoolID.Value);
                        }
                    }
                    else
                        if (chk.Equals("on"))
                    {
                        if (selected_hidden.Equals("checked"))
                        {
                            continue;
                        }
                        else
                        {
                            if (pupilRetestRegistration == null)
                            {
                                PupilRetestRegistration prr = new PupilRetestRegistration();
                                prr.PupilID = pr.PupilID.Value;
                                prr.ClassID = pr.ClassID.Value;
                                prr.SchoolID = _globalInfo.SchoolID.Value;
                                prr.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                prr.SubjectID = cs.SubjectID;
                                prr.Semester = semester;
                                prr.RegisteredDate = DateTime.Now.Date;
                                listPupilRetestRegistration.Add(prr);
                            }
                        }
                    }
                }
            }

            PupilRetestRegistrationBusiness.InsertPupilRetestRegistration(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, listPupilRetestRegistration);
            return Json(new JsonMessage(Res.Get("PupilRetestRegistration_Label_Update")));
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetDataClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["EducationLevelID"] = EducationLevelID.Value;
                dic["IsVNEN"] = true;

                if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsViewAll || _globalInfo.IsEmployeeManager) // nếu là QTHT: cho phép load ra tất cả các lớp
                {
                    var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.OrderNumber).ThenBy(u => u.DisplayName).ToList();
                    return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
                }
                else // load ra lớp theo phân quyền giáo viên bộ môn hoặc giáo vụ có quyền giáo viên bộ môn hoac gvcn hoac giao vu co quyen gvcn
                {
                    dic.Add("UserAccountID", _globalInfo.UserAccountID);
                    dic.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                    var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.OrderNumber).ThenBy(u => u.DisplayName).ToList();
                    return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
                }
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }
    }
}





