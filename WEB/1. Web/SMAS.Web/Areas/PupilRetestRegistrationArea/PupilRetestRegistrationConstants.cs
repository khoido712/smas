﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilRetestRegistrationArea
{
    public class PupilRetestRegistrationConstants
    {
        public const string LIST_PUPILRETESTREGISTRATION = "listPupilRetestRegistration";
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASSPROFILE = "LIST_CLASSPROFILE";
        public const string LIST_SUBJECT = "LIST_SUBJECT";
        public const string SHOWPANEL = "SHOWPANEL";
        public const string educationid = "educationid";
        public const string classid = "class";
        public const string string_B = "B";
        public const string string_CD = "CĐ";
        public const string ENABLE_BUTTON = "ENABLE_BUTTON";

    }
}