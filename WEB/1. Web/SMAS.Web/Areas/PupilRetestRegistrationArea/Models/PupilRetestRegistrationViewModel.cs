/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.PupilRetestRegistrationArea.Models
{
    public class PupilRetestRegistrationViewModel
    {
        public int PupilRetestRegistrationID { get; set; }
		public System.Int32 PupilID { get; set; }
        public string FullName { get; set; }
        public string PupilName { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public String[] Selected { get; set; }
        public String[] Disabled { get; set; }
        public Decimal?[] SummedUpMark { get; set; }
        public Nullable<bool>[] CoreSubject { get; set; }
        public Nullable<bool> IsWeakAllCoreSubject { get; set; }
        public String[] JudgementResult { get; set; }
        public int? EducationLevelID { get; set; }
        public int? OrderInClass { get; set; }
        public int? ClassOrderNumber { get; set; }
    }
}


