﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilRetestRegistrationArea
{
    public class PupilRetestRegistrationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilRetestRegistrationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilRetestRegistrationArea_default",
                "PupilRetestRegistrationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
