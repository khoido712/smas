/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.NotEatingChildrenArea
{
    public class NotEatingChildrenConstants
    {
        public const string LIST_NOTEATINGCHILDREN = "listNotEatingChildren";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_Month = "listMonth";
        public const string LIST_CLASS = "listClass";
    }
}