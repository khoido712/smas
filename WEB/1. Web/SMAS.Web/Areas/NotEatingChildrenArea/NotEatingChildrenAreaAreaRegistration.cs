﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.NotEatingChildrenArea
{
    public class NotEatingChildrenAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "NotEatingChildrenArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "NotEatingChildrenArea_default",
                "NotEatingChildrenArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
