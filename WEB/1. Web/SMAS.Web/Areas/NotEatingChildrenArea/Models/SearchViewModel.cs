/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.NotEatingChildrenArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("NotEatingChildren_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", NotEatingChildrenConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public Nullable<int> EducationLevelID { get; set; }

        [ResourceDisplayName("NotEatingChildren_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", NotEatingChildrenConstants.LIST_CLASS)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Nullable<int> ClassID { get; set; }

        [ResourceDisplayName("NotEatingChildren_Label_PupilCode")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String PupilCode { get; set; }

        [ResourceDisplayName("NotEatingChildren_Label_FullName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String FullName { get; set; }


        [ResourceDisplayName("NotEatingChildren_Label_Month")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", NotEatingChildrenConstants.LIST_Month)]
        public String Month { get; set; }
    }
}