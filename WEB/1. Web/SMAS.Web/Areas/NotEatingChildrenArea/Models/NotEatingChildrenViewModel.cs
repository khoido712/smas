/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.NotEatingChildrenArea.Models
{
    public class NotEatingChildrenViewModel
    {

        [ResourceDisplayName("NotEatingChildren_Label_FullName")]
        public System.String FullName { get; set; }

        [ResourceDisplayName("NotEatingChildren_Label_Birthdate")]
        public Nullable<System.DateTime> Birthdate { get; set; }

        public Nullable<System.Int32> PupilID { get; set; }

        public Nullable<System.DateTime> NotEatingDate { get; set; }

        public Nullable<System.Int32> ClassID { get; set; }

        public Nullable<System.Int32> SchoolID { get; set; }

        public Nullable<System.Int32> AcademicYearID { get; set; }

        public Nullable<System.Int32> NotEatingChildrenID { get; set; }

        public Nullable<System.Int32> sumDayNotEat { get; set; }

        public Nullable<System.Int32> Month { get; set; }

        public System.String MonthYear { get; set; }

    }
}


