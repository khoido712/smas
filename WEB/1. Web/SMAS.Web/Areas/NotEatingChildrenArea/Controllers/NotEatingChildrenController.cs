﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.NotEatingChildrenArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.NotEatingChildrenArea.Controllers
{
    public class NotEatingChildrenController : BaseController
    {        
        private readonly INotEatingChildrenBusiness NotEatingChildrenBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly IMonthlyLockBusiness MonthlyLockBusiness;

        public NotEatingChildrenController(IClassProfileBusiness classprofileBusiness,
                                           IAcademicYearBusiness academicyearBusiness,
                                           IPupilOfClassBusiness pupilofclassBusiness,
                                           IMonthlyLockBusiness monthlylockBusiness,
                                           IClassAssigmentBusiness classassigmentBusiness,
                                           INotEatingChildrenBusiness noteatingchildrenBusiness)
		{
			this.NotEatingChildrenBusiness = noteatingchildrenBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.MonthlyLockBusiness = monthlylockBusiness;
            this.ClassAssigmentBusiness = classassigmentBusiness;
            this.PupilOfClassBusiness = pupilofclassBusiness;
            this.AcademicYearBusiness = academicyearBusiness;
		}
		
		//
        // GET: /NotEatingChildren/

 

        #region LoadGrid

        private void SetViewData()
        {

            List<EducationLevel> ListEducationLevel = new GlobalInfo().EducationLevels;
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }

            int EducationLevelID = ListEducationLevel.FirstOrDefault().EducationLevelID;

            ViewData[NotEatingChildrenConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution", EducationLevelID);


            List<ComboObject> lstMonth = GetMonth();
            string MonthNow = "";
            if (DateTime.Now.Month < 10)
            {
                MonthNow = "0" + DateTime.Now.Month + "/" + DateTime.Now.Year;
            }
            else
            {
                MonthNow = DateTime.Now.Month + "/" + DateTime.Now.Year;
            }
            
            ViewData[NotEatingChildrenConstants.LIST_Month] = new SelectList(lstMonth, "key", "value", MonthNow);



            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["AppliedLevel"] = new GlobalInfo().AppliedLevel.Value;
            dic["EducationLevelID"] = EducationLevelID;

            int ClassID = 0;

            if (new GlobalInfo().IsAdminSchoolRole == true)
            {
                IEnumerable<ClassProfile> lstClass = new List<ClassProfile>();
                if (EducationLevelID != 0)
                {

                    lstClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
                }
                if (lstClass == null)
                    lstClass = new List<ClassProfile>();
                ClassID = lstClass.Count() == 0? 0:lstClass.FirstOrDefault().ClassProfileID;
                ViewData[NotEatingChildrenConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName", ClassID);

            }

            else
            {
                IEnumerable<ClassAssigment> lstClass = new List<ClassAssigment>();
                lstClass = this.ClassAssigmentBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.ClassID).ToList();
                ClassID = lstClass.FirstOrDefault().ClassID;
                ViewData[NotEatingChildrenConstants.LIST_CLASS] = new SelectList(lstClass, "ClassID", "ClassProfile.DisplayName", ClassID);
            }


            // search lấy dữ liệu default

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            string Month = "";


            IDictionary<string, object> SearchInfoDefault = new Dictionary<string, object>();
            SearchInfoDefault["EducationLevelID"] = EducationLevelID;
            SearchInfoDefault["ClassID"] = ClassID;
            SearchInfoDefault["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfoDefault["Status"] = SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING;

            FromDate = Convert.ToDateTime("01/" + MonthNow);
            int month = Convert.ToInt32(MonthNow.Substring(0, 2));
                if (month < 10)
                {
                    Month = "0" + month;
                }
                else
                    Month = "" + month;
                int year = Convert.ToInt32(MonthNow.Substring(3, 4));
                int numberOfDays = DateTime.DaysInMonth(year, month);
                ToDate = Convert.ToDateTime(numberOfDays + "/" + Month + "/" + year);
                SearchInfoDefault["FromDate"] = FromDate;
                SearchInfoDefault["ToDate"] = ToDate;

                ViewData["numberOfDays"] = numberOfDays;

            // lấy danh sách listNotEatingChildren

                IEnumerable<NotEatingChildren> listNotEatingChildren = this.NotEatingChildrenBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfoDefault).ToList();

            // lấy danh sách học sinh từ pupilofclass

                List<PupilOfClass> lstPupil = this.PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfoDefault).ToList();

            // mapping giữ liệu 2 list

            List<NotEatingChildrenViewModel> listNotEatingChildrenViewModel = new List<NotEatingChildrenViewModel>();

            Dictionary<int, List<NotEatingChildrenViewModel>> Dic_listNotEatingChildren = new Dictionary<int, List<NotEatingChildrenViewModel>>();

            foreach (var itemlstPupil in lstPupil)
            {
                NotEatingChildrenViewModel objNotEating = new NotEatingChildrenViewModel();

                objNotEating.AcademicYearID = itemlstPupil.AcademicYearID;
                objNotEating.Birthdate = itemlstPupil.PupilProfile.BirthDate;
                objNotEating.ClassID = itemlstPupil.ClassID;
                objNotEating.FullName = itemlstPupil.PupilProfile.FullName;
                objNotEating.PupilID = itemlstPupil.PupilID;
                objNotEating.SchoolID = itemlstPupil.SchoolID;
                objNotEating.Month = Convert.ToInt32(Month);
                objNotEating.MonthYear = MonthNow;

                List<NotEatingChildren> lstNotEat = listNotEatingChildren.Where(o => o.PupilID == itemlstPupil.PupilID).ToList();
                int sumDayNotEat = 0;
                if (lstNotEat.Count != 0)
                {
                    List<NotEatingChildrenViewModel> listNotEat = new List<NotEatingChildrenViewModel>();
                    foreach (var itemlstNotEat in lstNotEat)
                    {

                        NotEatingChildrenViewModel NotEat = new NotEatingChildrenViewModel();
                        NotEat.NotEatingDate = itemlstNotEat.NotEatingDate;
                        NotEat.NotEatingChildrenID = itemlstNotEat.NotEatingChildrenID;
                        sumDayNotEat++;
                        listNotEat.Add(NotEat);
                    }
                    Dic_listNotEatingChildren.Add(itemlstPupil.PupilID, listNotEat);
                }

                objNotEating.sumDayNotEat = sumDayNotEat;
                listNotEatingChildrenViewModel.Add(objNotEating);

            }
            //check MonthlyLock

            bool checkMonthlyLock = this.MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, FromDate, ToDate);

            ViewData["checkMonthlyLock"] = checkMonthlyLock;
            ViewData["lstNotEating"] = Dic_listNotEatingChildren;
            ViewData[NotEatingChildrenConstants.LIST_NOTEATINGCHILDREN] = listNotEatingChildrenViewModel;



        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }



        public List<ComboObject> GetMonth()
        {
            AcademicYear academicyear = this.AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            int year = academicyear.Year;
            int nextyear = year + 1;
            int FirstSemesterStartMonth = academicyear.FirstSemesterStartDate.Value.Month;
            int FirstSemesterEndMonth = academicyear.FirstSemesterEndDate.Value.Month;
            int SecondSemesterStartMonth = academicyear.SecondSemesterStartDate.Value.Month;
            int SecondSemesterEndMonth = academicyear.SecondSemesterEndDate.Value.Month;

            List<ComboObject> listMonth = new List<ComboObject>();

            if (FirstSemesterEndMonth < FirstSemesterStartMonth)
            {
                for (int i = FirstSemesterStartMonth; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        listMonth.Add(new ComboObject("0" + i + "/" + year, "0" + i + "/" + year));
                    }
                    else
                        listMonth.Add(new ComboObject(i + "/" + year, i + "/" + year));
                }


                for (int i = 1; i <= FirstSemesterEndMonth; i++)
                {
                    if (i < 10)
                    {
                        listMonth.Add(new ComboObject("0" + i + "/" + nextyear, "0" + i + "/" + nextyear));
                    }
                    else
                        listMonth.Add(new ComboObject(i + "/" + nextyear, i + "/" + nextyear));
                }
            }
            else
            {
                for (int i = FirstSemesterStartMonth; i <= FirstSemesterEndMonth; i++)
                {
                    if (i < 10)
                    {
                        listMonth.Add(new ComboObject("0" + i + "/" + year, "0" + i + "/" + year));
                    }
                    else
                        listMonth.Add(new ComboObject(i + "/" + year, i + "/" + year));
                }
            }


            if (SecondSemesterEndMonth < SecondSemesterStartMonth)
            {
                for (int i = SecondSemesterStartMonth; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        listMonth.Add(new ComboObject("0" + i + "/" + nextyear, "0" + i + "/" + nextyear));
                    }
                    else
                        listMonth.Add(new ComboObject(i + "/" + nextyear, i + "/" + nextyear));
                }


                for (int i = 1; i <= SecondSemesterEndMonth; i++)
                {
                    if (i < 10)
                    {
                        listMonth.Add(new ComboObject("0" + i + "/" + nextyear, "0" + i + "/" + nextyear));
                    }
                    else
                        listMonth.Add(new ComboObject(i + "/" + nextyear, i + "/" + nextyear));
                }
            }
            else
            {
                for (int i = SecondSemesterStartMonth; i <= SecondSemesterEndMonth; i++)
                {
                    if (i < 10)
                    {
                        listMonth.Add(new ComboObject("0" + i + "/" + nextyear, "0" + i + "/" + nextyear));
                    }
                    else
                        listMonth.Add(new ComboObject(i + "/" + nextyear, i + "/" + nextyear));
                }
            }

           var listMonth1 = listMonth.GroupBy(o => o.key).ToList();

           listMonth = new List<ComboObject>();
           foreach (var item in listMonth1)
           {
               listMonth.Add(new ComboObject(item.Key, item.Key));
           }

            return listMonth;

        }


        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["AppliedLevel"] = new GlobalInfo().AppliedLevel.Value;
            dic["EducationLevelID"] = EducationLevelID;
            if (new GlobalInfo().IsAdminSchoolRole == true)
            {
                IEnumerable<ClassProfile> lst = new List<ClassProfile>();
                if (EducationLevelID != null)
                {

                    lst = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
                }
                if (lst == null)
                    lst = new List<ClassProfile>();
                return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));

            }

            else
            {
                IEnumerable<ClassAssigment> lstClass = new List<ClassAssigment>();
                lstClass = this.ClassAssigmentBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.ClassID).ToList();
                return Json(new SelectList(lstClass, "ClassID", "ClassProfile.DisplayName"));
            }


        }




        public PartialViewResult Search(SearchViewModel frm)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            string Month = "";

            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["Status"] = SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING;
            if (frm.Month != null)
            {
                FromDate = Convert.ToDateTime("01/" + frm.Month);
                int month = Convert.ToInt32(frm.Month.ToString().Substring(0, 2));
                if (month < 10)
                {
                    Month = "0" + month;
                }
                else
                    Month = "" + month;
                int year = Convert.ToInt32(frm.Month.ToString().Substring(3, 4));
                int numberOfDays = DateTime.DaysInMonth(year, month);
                ToDate = Convert.ToDateTime(numberOfDays + "/" + Month + "/" + year);
                SearchInfo["FromDate"] = FromDate;
                SearchInfo["ToDate"] = ToDate;
                ViewData["numberOfDays"] = numberOfDays;
            }

            // lấy danh sách listNotEatingChildren

            IEnumerable<NotEatingChildren> listNotEatingChildren = this.NotEatingChildrenBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo).ToList();

            // lấy danh sách học sinh từ pupilofclass

            List<PupilOfClass> lstPupil = this.PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo).ToList();

            // mapping giữ liệu 2 list

            List<NotEatingChildrenViewModel> listNotEatingChildrenViewModel = new List<NotEatingChildrenViewModel>();

            Dictionary<int, List<NotEatingChildrenViewModel>> Dic_listNotEatingChildren = new Dictionary<int, List<NotEatingChildrenViewModel>>();

            foreach (var itemlstPupil in lstPupil)
            {
                NotEatingChildrenViewModel objNotEating = new NotEatingChildrenViewModel();

                objNotEating.AcademicYearID = itemlstPupil.AcademicYearID;
                objNotEating.Birthdate = itemlstPupil.PupilProfile.BirthDate;
                objNotEating.ClassID = itemlstPupil.ClassID;
                objNotEating.FullName = itemlstPupil.PupilProfile.FullName;
                objNotEating.PupilID = itemlstPupil.PupilID;
                objNotEating.SchoolID = itemlstPupil.SchoolID;
                objNotEating.Month = Convert.ToInt32(Month);
                objNotEating.MonthYear = frm.Month;

                List<NotEatingChildren> lstNotEat = listNotEatingChildren.Where(o => o.PupilID == itemlstPupil.PupilID).ToList();
                int sumDayNotEat = 0;
                if (lstNotEat.Count != 0)
                {
                    List<NotEatingChildrenViewModel> listNotEat = new List<NotEatingChildrenViewModel>();
                    foreach (var itemlstNotEat in lstNotEat)
                    {

                        NotEatingChildrenViewModel NotEat = new NotEatingChildrenViewModel();
                        NotEat.NotEatingDate = itemlstNotEat.NotEatingDate;
                        NotEat.NotEatingChildrenID = itemlstNotEat.NotEatingChildrenID;
                        sumDayNotEat++;
                        listNotEat.Add(NotEat);
                    }
                    Dic_listNotEatingChildren.Add(itemlstPupil.PupilID, listNotEat);
                }

                objNotEating.sumDayNotEat = sumDayNotEat;
                listNotEatingChildrenViewModel.Add(objNotEating);

            }

            //check MonthlyLock

            bool checkMonthlyLock = this.MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, FromDate,ToDate);

            ViewData["checkMonthlyLock"] = checkMonthlyLock;
            ViewData["lstNotEating"] = Dic_listNotEatingChildren;
            ViewData["listNotEatingChildrenViewModel"] = listNotEatingChildrenViewModel;
            ViewData[NotEatingChildrenConstants.LIST_NOTEATINGCHILDREN] = listNotEatingChildrenViewModel;
            return PartialView("_List");
        }




        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<NotEatingChildrenViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<NotEatingChildren> query = this.NotEatingChildrenBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<NotEatingChildrenViewModel> lst = query.Select(o => new NotEatingChildrenViewModel
            {
                FullName = o.PupilProfile.FullName,
                Birthdate = o.PupilProfile.BirthDate,
                ClassID = o.ClassID,
                NotEatingDate = o.NotEatingDate,
                PupilID = o.PupilID,
                SchoolID = o.SchoolID,
                NotEatingChildrenID = o.NotEatingChildrenID

            });

            return lst.OrderBy(o => o.FullName).ToList();
        }


        #endregion



        #region Update

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Update( FormCollection form)
        {
            
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            string Month = "";
            int ClassID = Convert.ToInt32(form.Get("ClassID"));
            string MonthYear = form.Get("MonthYear");
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            FromDate = Convert.ToDateTime("01/" + MonthYear);
            int month = Convert.ToInt32(MonthYear.Substring(0, 2));
                int year = Convert.ToInt32(MonthYear.Substring(3, 4));
                int numberOfDays = DateTime.DaysInMonth(year, month);
                ToDate = Convert.ToDateTime(numberOfDays + "/" + MonthYear);
                SearchInfo["FromDate"] = FromDate;
                SearchInfo["ToDate"] = ToDate;

            // lấy danh sách học sinh từ pupilofclass

            List<PupilOfClass> lstPupil = this.PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo).ToList();


            List<NotEatingChildren> lstNotEatingChildren = new List<NotEatingChildren>();

            foreach (var itemlstPupil in lstPupil)
            {
               
                List<DateTime> lstNotEatingDate = GetAccept(itemlstPupil.PupilID, form, MonthYear);
                if (lstNotEatingDate.Count != 0)
                {
                    foreach (var itemDate in lstNotEatingDate)
                    {
                        NotEatingChildren noteatingchildren = new NotEatingChildren();
                        noteatingchildren.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                        noteatingchildren.ClassID = ClassID;
                        noteatingchildren.PupilID = itemlstPupil.PupilID;
                        noteatingchildren.SchoolID = itemlstPupil.SchoolID;
                        noteatingchildren.NotEatingDate = itemDate;
                        lstNotEatingChildren.Add(noteatingchildren);
                    }
                
                }
            }

            if (lstNotEatingChildren.Count == 0)
            {
                return Json(new JsonMessage(Res.Get("NotEatingChildren_Error_Insert"), "error"));
            }
            else
            {
                this.NotEatingChildrenBusiness.Insert(new GlobalInfo().SchoolID.Value, new GlobalInfo().AcademicYearID.Value, ClassID, FromDate, ToDate, lstNotEatingChildren);
                this.NotEatingChildrenBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }

           
        }


        public static List<DateTime> GetAccept(int PupilID, FormCollection frm , string Month)
        {
            List<DateTime> lstDate = new List<DateTime>();
            for (int i = 0; i < 31; i++)
            {
                string AcceptCheck = frm.Get("Accepted_" + PupilID + "_" + i);
                if (AcceptCheck != null)
                {
                    DateTime NotEatingDate = new DateTime();
                    if (i < 10)
                    {
                        string date = "0" + i + "/" + Month;
                        NotEatingDate = Convert.ToDateTime(date);
                    }
                    else {
                        string date =  i + "/" + Month;
                        NotEatingDate = Convert.ToDateTime(date);
                    }

                    lstDate.Add(NotEatingDate);
                }
            }

            return lstDate;

        }


        #endregion


    }
}





