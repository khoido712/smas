﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea
{
    public class LearningResultReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LearningResultReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LearningResultReportArea_default",
                "LearningResultReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
