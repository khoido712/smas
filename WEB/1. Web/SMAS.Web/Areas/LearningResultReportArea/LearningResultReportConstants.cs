﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LearningResultReportArea
{
    public class LearningResultReportConstants
    {
        public const string LIST_REPORT_PATTERN = "LIST_REPORT_PATTERN";
        public const string LIST_SEMESTER="list semester";
        public const string LIST_MONTH_ID = "list MonthID";
        public const string LIST_PERIOD = "list period";
        public const string LIST_EDUCATIONLEVEL = "list education level";
        public const string LIST_CLASS = "list class";
        public const string LIST_SUBJECT = "list subject";
        public const string LIST_SCHOOL_FACULTY = "list school faculty";
        public const string LIST_TEACHER = "list teacher";
        public const string LIST_PUPIL = "list pupil";
        public const string LIST_PROPERTYCLASS = "list propertyclass";
        public const string DISABLED_BUTTON = "DisabledButton";
        public const string TYPE_PRINT = "TYPE_PRINT";
        public const string PRINT_ENOUGH = "PRINT_ENOUGH";

        public const string SEMESTER = "semester";
        public const string IS_PERIOD = "is period";
        public const string PERIOD = "period";
        public const string EDUCATION_LEVEL = "education level";
        public const string CLASS_ID = "class id";
        public const string PAPER_SIZE = "paper_size";
        public const string PUPIL_CODE = "pupil_code";
        public const string PUPIL_NAME = "pupil_name";
        public const string IS_SOHCM = "IS_SOHCM";
        public const int MONTH_DGCK_HKI = 15;
        public const int MONTH_DGCK_HKII = 16;

        public const int Primary = 1;
        public const int Secondary = 2;
        public const int Tertiary = 3;
        public const int SHEET_NUMBER_PRIMARY = 4;
        public const int SHEET_NUMBER_SECONDARY = 3;
        public const int GDTX = 3;


    }
}