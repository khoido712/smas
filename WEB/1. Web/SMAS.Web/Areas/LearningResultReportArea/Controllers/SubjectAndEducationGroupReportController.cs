﻿using Ionic.Zip;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class SubjectAndEducationGroupReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        public SubjectAndEducationGroupReportController(IClassProfileBusiness classProfileBusiness, IAcademicYearBusiness academicYearBusiness,
            IProcessedReportBusiness processedReportBusiness, IReportDefinitionBusiness reportDefinitionBusiness, ISchoolSubjectBusiness schoolSubjectBusiness,
            IRatedCommentPupilBusiness ratedCommentPupilBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.RatedCommentPupilBusiness = ratedCommentPupilBusiness;
        }
        public ActionResult Index()
        {
            List<ComboObject> lstSemester = CommonList.Semester_GeneralSummed();
            int defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;
            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow < SecondStartDate)
            {
                defaultSemester = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI;
            }
            else
            {
                defaultSemester = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII;
            }

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester);

            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel}
            };
            IQueryable<SchoolSubjectBO> iqSchoolSubject = SchoolSubjectBusiness.Search(dicSearch)
                                                          .Select(p => new SchoolSubjectBO
                                                          {
                                                              SubjectID = p.SubjectID,
                                                              SubjectName = p.SubjectCat.SubjectName,
                                                              OrderInSubject = p.SubjectCat.OrderInSubject
                                                          }).Distinct().OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName);
            ViewData[LearningResultReportConstants.LIST_SUBJECT] = new SelectList(iqSchoolSubject, "SubjectID", "SubjectName");
            return View();
        }
        public JsonResult GetSubjectAndEducationGroupReport(FormCollection frm)
        {
            int typeExcelPDF = !string.IsNullOrEmpty(frm["typeExcelPDF"]) ? int.Parse(frm["typeExcelPDF"]) : 1;
            int SemesterTypeID = !string.IsNullOrEmpty(frm["SemesterTypeID"]) ? int.Parse(frm["SemesterTypeID"]) : 0;
            int SubjectID = !string.IsNullOrEmpty(frm["SubjectID"]) ? int.Parse(frm["SubjectID"]) : 0;
            int FemaleType = !string.IsNullOrEmpty(frm["FemaleType"]) ? int.Parse(frm["FemaleType"]) : 0;
            int EthnicType = !string.IsNullOrEmpty(frm["EthnicType"]) ? int.Parse(frm["EthnicType"]) : 0;
            int FemaleEthnicType = !string.IsNullOrEmpty(frm["FemaleEthnicType"]) ? int.Parse(frm["FemaleEthnicType"]) : 0;

            bool isZip = SubjectID > 0 ? false : true;

            string reportCode = "";
            if (!isZip)
            {
                reportCode = SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP_FILE_ZIP;
            }
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"SemesterTypeID",SemesterTypeID},
                {"SubjectID",SubjectID},
                {"FemaleType",FemaleType},
                {"EthnicType",EthnicType},
                {"FemaleEthnicType",FemaleEthnicType},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"CountEducationLevel",_globalInfo.EducationLevels.Count},
                {"TypeExcelPDF", typeExcelPDF}
            };

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            if (isZip)
            {
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = RatedCommentPupilBusiness.GetProcessReportRepord(dic, isZip);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    string FolderSaveFile = string.Empty;
                    RatedCommentPupilBusiness.DownloadZipFileSubjectAndEducationGroup(dic, out FolderSaveFile);
                    if (string.IsNullOrEmpty(FolderSaveFile))
                    {
                        return Json(new { Type = "error", Message = "Không có dữ liệu môn học" });
                    }
                    DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(Path.Combine(FolderSaveFile));
                        using (MemoryStream output = new MemoryStream())
                        {
                            zip.Save(output);
                            di.Delete(true);
                            processedReport = RatedCommentPupilBusiness.InsertProcessReport(dic, output, isZip);
                        }

                    }
                }
            }
            else
            {
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = RatedCommentPupilBusiness.GetProcessReportRepord(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = RatedCommentPupilBusiness.CreateSubjectAndEducationGroupReport(dic);
                    processedReport = RatedCommentPupilBusiness.InsertProcessReport(dic, excel, false);
                    excel.Close();
                }
            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        public JsonResult GetNewSubjectAndEducationGroupReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            int typeExcelPDF = !string.IsNullOrEmpty(frm["typeExcelPDF"]) ? int.Parse(frm["typeExcelPDF"]) : 1;
            int SemesterTypeID = !string.IsNullOrEmpty(frm["SemesterTypeID"]) ? int.Parse(frm["SemesterTypeID"]) : 0;
            int SubjectID = !string.IsNullOrEmpty(frm["SubjectID"]) ? int.Parse(frm["SubjectID"]) : 0;
            int FemaleType = !string.IsNullOrEmpty(frm["FemaleType"]) ? int.Parse(frm["FemaleType"]) : 0;
            int EthnicType = !string.IsNullOrEmpty(frm["EthnicType"]) ? int.Parse(frm["EthnicType"]) : 0;
            int FemaleEthnicType = !string.IsNullOrEmpty(frm["FemaleEthnicType"]) ? int.Parse(frm["FemaleEthnicType"]) : 0;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"SemesterTypeID",SemesterTypeID},
                {"SubjectID",SubjectID},
                {"FemaleType",FemaleType},
                {"EthnicType",EthnicType},
                {"FemaleEthnicType",FemaleEthnicType},
                {"CountEducationLevel",_globalInfo.EducationLevels.Count},
                {"TypeExcelPDF", typeExcelPDF}
            };
            bool isZip = SubjectID > 0 ? false : true;

            if (isZip)
            {
                string FolderSaveFile = string.Empty;
                RatedCommentPupilBusiness.DownloadZipFileSubjectAndEducationGroup(dic, out FolderSaveFile);
                if (string.IsNullOrEmpty(FolderSaveFile))
                {
                    return Json(new { Type = "error", Message = "Không có dữ liệu môn học" });
                }
                DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(Path.Combine(FolderSaveFile));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        di.Delete(true);
                        processedReport = RatedCommentPupilBusiness.InsertProcessReport(dic, output, isZip);
                    }

                }
            }
            else
            {
                Stream excel = RatedCommentPupilBusiness.CreateSubjectAndEducationGroupReport(dic);
                processedReport = RatedCommentPupilBusiness.InsertProcessReport(dic, excel);
                excel.Close();
            }

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);

            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP_FILE_ZIP.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }

            result.FileDownloadName = processedReport.ReportName;
            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            //ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            //Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            //IPdfExport objFile = new PdfExport();
            //var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            //return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP_FILE_ZIP.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                //Duong dan thu muc chua file excel
                string inFolder = objFile.CreateFolder();
                //Duong dan thu muc chua file zip
                string outFolder = objFile.CreateFolder();

                string outFile = outFolder + "\\" + processedReport.ReportName;
                try
                {
                    //1. Extract file tu stream ra thu muc
                    objFile.UnzipFromStream(excel, inFolder);
                    //2. Chuyen file .xls thanh pdf
                    objFile.ConvertFileInFolderToPDF(inFolder, 0);
                    //3 Zip thu muc PDF
                    objFile.ZipFolder(outFile, inFolder);
                    //4. Chuyen file zip sang stream
                    result = new FileStreamResult(objFile.ConvertZipFileToStream(outFile), PdfExport.CONTENT_TYPE_ZIP);
                    result.FileDownloadName = processedReport.ReportName;
                }
                finally
                {
                    objFile.DeleteFolder(inFolder);
                    objFile.DeleteFolder(outFolder);
                }
                return result;
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);

            }
        }
    }
}