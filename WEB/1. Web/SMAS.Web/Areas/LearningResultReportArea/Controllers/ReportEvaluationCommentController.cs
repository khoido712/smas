﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class ReportEvaluationCommentController:BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEvaluationCommentsBusiness EvaluationCommentsBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        public ReportEvaluationCommentController(IAcademicYearBusiness academicYearBusiness,
            IClassProfileBusiness classProfileBusiness,
            IClassSubjectBusiness classSubjectBusiness,
            ITranscriptsBusiness transcriptsBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IEvaluationCommentsBusiness evaluationCommentsBusiness,
            ITeachingAssignmentBusiness TeachingAssignmentBusiness,
            IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EvaluationCommentsBusiness = evaluationCommentsBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
        }
        public PartialViewResult LoadEvaluationCommentGVCN()
        {
            this.SetViewData();
            return PartialView("_EvaluationCommentGVCN");
        }
        public PartialViewResult LoadEvaluationCommentGVBM()
        {
            this.SetViewData();
            return PartialView("_EvaluationCommentGVBM");
        }
        private void SetViewData()
        {
            List<EducationLevel> lsEducationLevel = _globalInfo.EducationLevels;

            if (lsEducationLevel != null)
            {
                ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(new string[] { });
            }
        }
        private List<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;

            if (!_globalInfo.IsRolePrincipal && !_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager && !_globalInfo.IsViewAll)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            if (_globalInfo.IsViewAll)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
            }
            List<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.EducationLevelID == EducationLevelID).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            return lsCP;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                List<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private IQueryable<ClassSubject> getSubjectFromClass(int ClassID, int EducationLevelID)
        {
            IQueryable<ClassSubject> iqClassSubject ;
            
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ClassID"] = ClassID;

            iqClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);

            return iqClassSubject;
            
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? ClassID, int? EducationLevelID)
        {
            if (ClassID.HasValue && EducationLevelID.HasValue)
            {
                IQueryable<ClassSubject> lsCS = getSubjectFromClass(ClassID.Value, EducationLevelID.Value);
                List<SubjectCatBO> lsSC = new List<SubjectCatBO>();
                if (lsCS != null && lsCS.Count() > 0)
                {
                    lsSC = lsCS.Select(o => new SubjectCatBO
                    {
                        SubjectCatID = o.SubjectID,
                        SubjectName = o.SubjectCat.DisplayName,
                        OrderInSubject = o.SubjectCat.OrderInSubject
                    }).Distinct().OrderBy(o => o.OrderInSubject).ThenBy(o => o.SubjectName).ToList();
                }
                if (lsSC.Count != 0)
                {
                    return Json(new SelectList(lsSC, "SubjectCatID", "SubjectName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportViewModel rvm)
        {
            if (rvm.Class == 0)
            {
                throw new BusinessException(Res.Get("PrimaryTranscripts_Error_Class"));
            }
            ReportEvalutionComments reportEC = new ReportEvalutionComments();
            reportEC.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            reportEC.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            reportEC.AcademicYearID = _globalInfo.AcademicYearID.Value;
            if (rvm.Class == 0)
            {
                throw new BusinessException("Thầy/cô chưa chọn lớp học");
            }
            reportEC.ClassID = rvm.Class;
            reportEC.SubjectID = rvm.Subject;
            reportEC.EducationLevelID = rvm.EducationLevel;

            if (rvm.Subject != 0)
            {
                return GetReport_SubjectTeacher(reportEC); //xuat bao cao cho GVBM
            }
            else
            {
                return GetReport_HeadTeacher(reportEC);//xuat bao cao cho GVCN
            }
        }

        public JsonResult GetReport_SubjectTeacher(ReportEvalutionComments reportEC)
        {
            string reportCode = "";
            if (reportEC.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVBM;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {

                
                processedReport = EvaluationCommentsBusiness.GetReport_SubjectTeacher(reportEC);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = EvaluationCommentsBusiness.CreateReport_SubjectTeacher(reportEC);
                processedReport = EvaluationCommentsBusiness.InsertReport_SubjectTeacher(reportEC, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        public JsonResult GetReport_HeadTeacher(ReportEvalutionComments reportEC)
        {
            string reportCode = "";
            if (reportEC.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVCN;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = EvaluationCommentsBusiness.GetReport_HeadTeacher(reportEC);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = EvaluationCommentsBusiness.CreateReport_HeadTeacher(reportEC);
                processedReport = EvaluationCommentsBusiness.InsertReport_HeadTeacher(reportEC, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportViewModel rvm)
        {
            ReportEvalutionComments reportEC = new ReportEvalutionComments();
            reportEC.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            reportEC.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            reportEC.AcademicYearID = _globalInfo.AcademicYearID.Value;
            reportEC.ClassID = rvm.Class;
            reportEC.SubjectID = rvm.Subject;
            reportEC.EducationLevelID = rvm.EducationLevel;

            if (rvm.Subject != 0)
            {
                Stream excel = EvaluationCommentsBusiness.CreateReport_SubjectTeacher(reportEC);
                ProcessedReport processedReport = EvaluationCommentsBusiness.InsertReport_SubjectTeacher(reportEC, excel);
                excel.Close();
                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }
            else
            {
                Stream excel = EvaluationCommentsBusiness.CreateReport_HeadTeacher(reportEC);
                ProcessedReport processedReport = EvaluationCommentsBusiness.InsertReport_HeadTeacher(reportEC, excel);
                excel.Close();
                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }

        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", global.SchoolID},
                {"AppliedLevel", global.AppliedLevel},
            };

            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVBM,
                SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVCN,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", global.SchoolID},
                {"AppliedLevel", global.AppliedLevel},
            };

            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVBM,
                SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVCN,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

    }
}