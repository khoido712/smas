﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class SubjectTranscriptReportController:BaseController
    {
        IClassProfileBusiness ClassProfileBusiness;
        IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IClassSubjectBusiness ClassSubjectBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        ITranscriptsByPeriodBusiness TranscriptsByPeriodBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IUserAccountBusiness UserAccountBusiness ;
        ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness;
        public SubjectTranscriptReportController(IClassProfileBusiness classProfileBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IClassSubjectBusiness ClassSubjectBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            ITranscriptsByPeriodBusiness TranscriptsByPeriodBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IUserAccountBusiness UserAccountBusiness, ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.TranscriptsByPeriodBusiness = TranscriptsByPeriodBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.TranscriptsBySemesterBusiness = TranscriptsBySemesterBusiness;
           
        }
        public ActionResult Index()
        {
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            var lstClass = ClassProfileBusiness.getClassByRoleAppliedLevel(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value, _globalInfo.UserAccountID)
                   .Where(o => o.EducationLevelID == educationLevel.EducationLevelID);
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // load combobox Semester
            List<ComboObject> lstSemester = CommonList.SemesterAndAll();

            ComboObject defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow >= FirstStarDate && datenow <= FirstEndDate)
            {
                defaultSemester = lstSemester[0];
            }
            else if (datenow >= SecondStartDate && datenow <= SecondEndDate)
            {
                defaultSemester = lstSemester[1];
            }
            else
            {
                defaultSemester = lstSemester[2];
            }

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester.key);

            //Dot
            List<PeriodDeclaration> lstPeriodCommon = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "Semester", Convert.ToInt32(defaultSemester.key) } })
                                                           .OrderByDescending(o => o.EndDate).ToList();

            PeriodDeclaration defaultPeriod = lstPeriodCommon.Where(o => o.FromDate <= datenow && datenow <= o.EndDate).FirstOrDefault();

            if (defaultPeriod != null)
            {
                ViewData[LearningResultReportConstants.LIST_PERIOD] = new SelectList(lstPeriodCommon, "PeriodDeclarationID", "Resolution", defaultPeriod.PeriodDeclarationID);
            }
            else
            {
                ViewData[LearningResultReportConstants.LIST_PERIOD] = new SelectList(lstPeriodCommon, "PeriodDeclarationID", "Resolution");
            }


            //Mon hoc
            List<SubjectCat> lstSubject = new List<SubjectCat>();

            ViewData[LearningResultReportConstants.LIST_SUBJECT] = new SelectList(lstSubject, "SubjectCatID", "SubjectName");

            return View();
        }

        public JsonResult AjaxLoadPeriod(int semester)
        {
            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "Semester", Convert.ToInt32(semester) } })
                                                                         .OrderByDescending(o => o.EndDate).ToList();

            PeriodDeclaration defaultPeriod = lstPeriod.Where(o => o.FromDate <= datenow && datenow <= o.EndDate).FirstOrDefault();

            if (defaultPeriod != null)
            {
                return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", defaultPeriod.PeriodDeclarationID));
            }
            else
            {
                return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution"));
            }
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            var lstClass = ClassProfileBusiness.getClassByRoleAppliedLevel(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value, _globalInfo.UserAccountID)
                    .Where(o=>o.EducationLevelID==educationLevelID);

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }
        public JsonResult AjaxLoadSubject(int semester, int educationLevelID, int? classID)
        {
            List<ComboObject> lstSubject = new List<ComboObject>();
            if (classID != null)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                dic["Semester"] = semester;
                dic["EducationLevelID"] = educationLevelID;
                dic["ClassID"] = classID;
                dic["IsVNEN"] = true;

                lstSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList()
                                                .Select(u => new ComboObject(u.SubjectID.ToString(), u.SubjectCat.DisplayName))
                                                .ToList();
            }
            return Json(new SelectList(lstSubject, "key", "value"));
        }

        #region Bao cao Bang diem mon hoc theo dot

        [ValidateAntiForgeryToken]
        public JsonResult GetSubjectReportByPeriod(SearchViewModel model)
        {
            if (model.PeriodID == null)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_PeriodID");
            }

            if (model.ClassID == null)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_ClassID");
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = model.SemesterID;
            dic["EducationLevelID"] = model.EducationLevelID;
            dic["ClassID"] = model.ClassID;

            if (ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Count() == 0)
            {
                throw new BusinessException("LearningResultReport_Validate_NoClassSubject");
            }

            PupilRewardReportBO bo = new PupilRewardReportBO();
            GlobalInfo GlobalInfo = new GlobalInfo();

            string type = JsonReportMessage.NEW;

            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.SchoolID = glo.SchoolID.Value;
            toc.AcademicYearID = glo.AcademicYearID.Value;
            toc.AppliedLevel = glo.AppliedLevel.Value;
            toc.ClassID = model.ClassID.Value;
            toc.EducationLevelID = model.EducationLevelID.Value;
            toc.PeriodDeclarationID = model.PeriodID.Value;
            toc.SchoolID = glo.SchoolID.Value;
            toc.Semester = model.SemesterID;
            toc.SubjectID = model.SubjectID.GetValueOrDefault();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANGDIEMLOPCAP23THEODOT);
            ProcessedReport entity = null;

            if (reportDef.IsPreprocessed == true)
            {
                entity = TranscriptsByPeriodBusiness.GetTranscriptsByPeriod(toc);
                if (entity != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptsByPeriodBusiness.CreateTranscriptsByPeriod(toc);
                entity = TranscriptsByPeriodBusiness.InsertTranscriptsByPeriod(toc, excel);
                ProcessedReportBusiness.Save();
                excel.Close();
            }
            return Json(new JsonReportMessage(entity, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewSubjectReportByPeriod(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.SchoolID = glo.SchoolID.Value;
            toc.AcademicYearID = glo.AcademicYearID.Value;
            toc.AppliedLevel = glo.AppliedLevel.Value;
            toc.ClassID = model.ClassID.Value;
            toc.EducationLevelID = model.EducationLevelID.Value;
            toc.PeriodDeclarationID = model.PeriodID.Value;
            toc.Semester = model.SemesterID;
            toc.SubjectID = model.SubjectID.GetValueOrDefault();

            Stream excel = TranscriptsByPeriodBusiness.CreateTranscriptsByPeriod(toc);
            ProcessedReport entity = TranscriptsByPeriodBusiness.InsertTranscriptsByPeriod(toc, excel);
            excel.Close();
            return Json(new JsonReportMessage(entity, JsonReportMessage.NEW));
        }

        #endregion
        #region Bao cao Bang diem mon hoc theo hoc ky
        [ValidateAntiForgeryToken]
        public JsonResult GetSubjectReport(SearchViewModel form)
        {
            if (form.ClassID == null)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_ClassID");
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = form.SemesterID;
            dic["EducationLevelID"] = form.EducationLevelID;
            dic["ClassID"] = form.ClassID;

            if (ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Count() == 0)
            {
                throw new BusinessException("LearningResultReport_Validate_NoClassSubject");
            }

            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            GlobalInfo glo = new GlobalInfo();

            TranscriptOfClass.ClassID = form.ClassID.Value;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            TranscriptOfClass.UserAccountID = new GlobalInfo().UserAccountID;
            TranscriptOfClass.isAdmin = new GlobalInfo().IsAdminSchoolRole;
            if (glo.IsAdminSchoolRole == false)
            {
                TranscriptOfClass.TeacherID = UserAccountBusiness.Find(new GlobalInfo().UserAccountID).EmployeeID.Value;
            }
            if (form.SubjectID.HasValue)
            {
                TranscriptOfClass.SubjectID = form.SubjectID.Value;
            }
            TranscriptOfClass.Semester = form.SemesterID;
            TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
            TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            string reportCode = "";
            if (form.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_HOCKYI;
            }
            if (form.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_HOCKYII;
            }
            if (form.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_CANAM;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.TranscriptsBySemesterBusiness.GetTranscriptsBySemester(TranscriptOfClass);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.TranscriptsBySemesterBusiness.CreateTranscriptsBySemester(TranscriptOfClass);
                processedReport = this.TranscriptsBySemesterBusiness.InsertTranscriptsBySemester(TranscriptOfClass, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewSubjectReport(SearchViewModel form)
        {
            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass.ClassID = form.ClassID.Value;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            TranscriptOfClass.UserAccountID = new GlobalInfo().UserAccountID;
            TranscriptOfClass.isAdmin = new GlobalInfo().IsAdminSchoolRole;
            if (glo.IsAdminSchoolRole == false)
            {
                TranscriptOfClass.TeacherID = UserAccountBusiness.Find(new GlobalInfo().UserAccountID).EmployeeID.Value;
            }
            if (form.SubjectID.HasValue)
            {
                TranscriptOfClass.SubjectID = form.SubjectID.Value;
            }
            TranscriptOfClass.Semester = form.SemesterID;
            TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
            TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;

            Stream excel = this.TranscriptsBySemesterBusiness.CreateTranscriptsBySemester(TranscriptOfClass);
            ProcessedReport processedReport = this.TranscriptsBySemesterBusiness.InsertTranscriptsBySemester(TranscriptOfClass, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        #endregion

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}