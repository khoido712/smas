﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [SkipCheckRole]
    [ViewableBySupervisingDeptFilter]
    public class CapacityQualityReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        public CapacityQualityReportController(IClassProfileBusiness classProfileBusiness, IAcademicYearBusiness academicYearBusiness,
            IProcessedReportBusiness processedReportBusiness, IReportDefinitionBusiness reportDefinitionBusiness, ISchoolSubjectBusiness schoolSubjectBusiness,
            IRatedCommentPupilBusiness ratedCommentPupilBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.RatedCommentPupilBusiness = ratedCommentPupilBusiness;
        }
        public ActionResult Index()
        {
            List<ComboObject> lstSemester = CommonList.Semester_GeneralSummed();
            int defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;
            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow < SecondStartDate)
            {
                defaultSemester = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI;
            }
            else
            {
                defaultSemester = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII;
            }

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester);
            return View();
        }
        public JsonResult GetReport(FormCollection frm)
        {
            int SemesterTypeID = !string.IsNullOrEmpty(frm["SemesterTypeID"]) ? int.Parse(frm["SemesterTypeID"]) : 0;
            int FemaleType = !string.IsNullOrEmpty(frm["FemaleType"]) ? int.Parse(frm["FemaleType"]) : 0;
            int EthnicType = !string.IsNullOrEmpty(frm["EthnicType"]) ? int.Parse(frm["EthnicType"]) : 0;
            int FemaleEthnicType = !string.IsNullOrEmpty(frm["FemaleEthnicType"]) ? int.Parse(frm["FemaleEthnicType"]) : 0;
            int semesterID = 0;
            if (SemesterTypeID == 1 || SemesterTypeID == 3)
            {
                semesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else if (SemesterTypeID == 2 || SemesterTypeID == 4)
            {
                semesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ProvinceID",_globalInfo.ProvinceID},
                {"DistrictID",_globalInfo.DistrictID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ReportTypeID",SemesterTypeID},
                {"IsFemale",FemaleType > 0 ? true : false},
                {"IsEthnic",EthnicType > 0 ? true : false},
                {"IsFemaleEthnic",FemaleEthnicType > 0 ? true : false},
                {"SemesterID",semesterID},
                {"Year",objAy.Year},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"SupervisingDeptID",_globalInfo.SupervisingDeptID}
            };
            string reportCode = SystemParamsInFile.REPORT_CAPACITY_AND_QUALITY;
            IDictionary<string, object> DicProcessReport = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ReportTypeID",SemesterTypeID},
                {"FemaleType",FemaleType},
                {"EthnicType",EthnicType},
                {"FemaleEthnicType",FemaleEthnicType}
            };
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = RatedCommentPupilBusiness.GetProcessReportRepordCapQua(DicProcessReport);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = RatedCommentPupilBusiness.CreateCapacityAndQualityReport(dic);
                processedReport = RatedCommentPupilBusiness.InsertProcessCapQuaReport(DicProcessReport, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        public JsonResult GetNewReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            int SemesterTypeID = !string.IsNullOrEmpty(frm["SemesterTypeID"]) ? int.Parse(frm["SemesterTypeID"]) : 0;
            int FemaleType = !string.IsNullOrEmpty(frm["FemaleType"]) ? int.Parse(frm["FemaleType"]) : 0;
            int EthnicType = !string.IsNullOrEmpty(frm["EthnicType"]) ? int.Parse(frm["EthnicType"]) : 0;
            int FemaleEthnicType = !string.IsNullOrEmpty(frm["FemaleEthnicType"]) ? int.Parse(frm["FemaleEthnicType"]) : 0;
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            int semesterID = 0;
            if (SemesterTypeID == 1 || SemesterTypeID == 3)
            {
                semesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else if (SemesterTypeID == 2 || SemesterTypeID == 4)
            {
                semesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ProvinceID",_globalInfo.ProvinceID},
                {"DistrictID",_globalInfo.DistrictID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ReportTypeID",SemesterTypeID},
                {"IsFemale",FemaleType > 0 ? true : false},
                {"IsEthnic",EthnicType > 0 ? true : false},
                {"IsFemaleEthnic",FemaleEthnicType > 0 ? true : false},
                {"SemesterID",semesterID},
                {"Year",objAy.Year},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"SupervisingDeptID",_globalInfo.SupervisingDeptID}
            };
            IDictionary<string, object> DicProcessReport = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ReportTypeID",SemesterTypeID},
                {"FemaleType",FemaleType},
                {"EthnicType",EthnicType},
                {"FemaleEthnicType",FemaleEthnicType}
            };
            Stream excel = RatedCommentPupilBusiness.CreateCapacityAndQualityReport(dic);
            processedReport = RatedCommentPupilBusiness.InsertProcessCapQuaReport(DicProcessReport, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            excel = ReportUtils.Decompress(processedReport.ReportData);
            result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;
            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}