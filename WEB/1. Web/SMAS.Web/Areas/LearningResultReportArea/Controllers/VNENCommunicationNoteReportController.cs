﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using System.IO;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class VNENCommunicationNoteReportController:BaseController
    {
        #region Properties
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ICommunicationNoteReportBusiness CommunicationNoteReportBusiness;

        #endregion

        #region Constructor
        public VNENCommunicationNoteReportController(IClassProfileBusiness ClassProfileBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IAcademicYearBusiness AcademicYearBusiness, ICommunicationNoteReportBusiness CommunicationNoteReportBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.CommunicationNoteReportBusiness = CommunicationNoteReportBusiness;
        }
        #endregion

        #region Action
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public JsonResult AjaxLoadClass(int educationLevel)
        {

            List<ClassProfile> lsCP = getClassFromEducationLevel(educationLevel);
            
             return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"));
            
        }

        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(CommunicationNoteReportViewModel form)
        {
           
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["UserAccountID"] = _globalInfo.UserAccountID;
            dic["EducationLevel"] = form.EducationLevel;
            dic["Class"] = form.Class;
            dic["Semester"] = form.Semester;
            string reportCode = string.Empty;
            int TypeExport = form.TypeExport;
            dic["TypeExport"] = TypeExport;
            if (TypeExport == 1)//Hoc ky
            {
                reportCode = SystemParamsInFile.REPORT_VNEN_COMMUNICATION_NOTE;
            }
            else
            {
                dic["MonthID"] = form.MonthID.ToString();
                dic["MonthName"] = form.MonthName;
                reportCode = SystemParamsInFile.REPORT_VNEN_COMMUNICATION_NOTE_MONTH;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = CommunicationNoteReportBusiness.GetVnenCommunicationNoteReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = null;
                if (TypeExport == 1)
                {
                    excel = CommunicationNoteReportBusiness.CreateVnenCommunicationNoteReport(dic);
                }
                else
                {
                    excel = CommunicationNoteReportBusiness.CreateVNENCommunicationNoteMonthReport(dic);
                }
                processedReport = CommunicationNoteReportBusiness.InsertVnenCommunicationNoteReport(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(CommunicationNoteReportViewModel form)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["UserAccountID"] = _globalInfo.UserAccountID;
            dic["EducationLevel"] = form.EducationLevel;
            dic["Class"] = form.Class;
            dic["Semester"] = form.Semester;
            int TypeExport = form.TypeExport;
            Stream excel = null;
            if (TypeExport == 1)//Hoc ky
            {
                excel = CommunicationNoteReportBusiness.CreateVnenCommunicationNoteReport(dic);
            }
            else
            {
                dic["MonthID"] = form.MonthID;
                dic["MonthName"] = form.MonthName;
                excel = CommunicationNoteReportBusiness.CreateVNENCommunicationNoteMonthReport(dic);
            }
            dic["TypeExport"] = TypeExport;
            ProcessedReport processedReport = CommunicationNoteReportBusiness.InsertVnenCommunicationNoteReport(dic, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //danh sach hoc ky
            List<ComboObject> lstSemester = new List<ComboObject>();
            lstSemester.Add(new ComboObject { key = GlobalConstants.SEMESTER_OF_YEAR_FIRST.ToString(), value = SystemParamsInFile.SEMESTER_I });
            lstSemester.Add(new ComboObject { key = GlobalConstants.SEMESTER_OF_YEAR_SECOND.ToString(), value = SystemParamsInFile.SEMESTER_II });
            int defaultSemester = AcademicYearBusiness.GetDefaultSemester(_globalInfo.AcademicYearID.Value);
            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester);
            int selectedID = 0;
            DateTime datetimeNow = DateTime.Now;
            selectedID = Int32.Parse(datetimeNow.Year.ToString() + datetimeNow.Month.ToString());

            List<ListMonth> lstMonthID = this.GetListMonth(_globalInfo.Semester.Value);
            ViewData[LearningResultReportConstants.LIST_MONTH_ID] = new SelectList(lstMonthID, "MonthID", "MonthName",selectedID);

            //Lay danh sach khoi
            List<EducationLevel> lsEducationLevel = _globalInfo.EducationLevels;
            if (lsEducationLevel == null)
            {
                lsEducationLevel = new List<EducationLevel>();
            }

            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            int? defaultEduLevel = null;
            if (lsEducationLevel.Count > 0)
            {
                defaultEduLevel = lsEducationLevel.First().EducationLevelID;
            }

            //Lay danh sach lop
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (defaultEduLevel != null)
            {
                lstClass = getClassFromEducationLevel(defaultEduLevel.Value);
            }
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
          
        }
        private List<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.getClassByRoleAppliedLevel(_globalInfo.AcademicYearID.GetValueOrDefault()
               , _globalInfo.SchoolID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID)
               .Where(o=>o.IsVnenClass==true)
               .Where(o => o.EducationLevelID==EducationLevelID).OrderBy(o=>o.OrderNumber).ToList();

            return lstClassProfile;
        }
        private List<ListMonth> GetListMonth(int semesterID)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime FDate = new DateTime();
            DateTime EDate = new DateTime();
            FDate = objAcademicYear.FirstSemesterStartDate.Value;
            EDate = objAcademicYear.SecondSemesterEndDate.Value;

            FDate = new DateTime(FDate.Date.Year, FDate.Date.Month, 1);
            EDate = new DateTime(EDate.Date.Year, EDate.Date.Month, 1);
            List<ListMonth> lstListMonth = new List<ListMonth>();
            ListMonth objMonth = null;
            while (FDate <= EDate)
            {
                objMonth = new ListMonth();
                objMonth.MonthID = Int32.Parse(FDate.Date.Year.ToString() + FDate.Date.Month.ToString());
                objMonth.MonthName = "Tháng " + (FDate.Date.Month > 9 ? FDate.Date.Month.ToString() : "0" + FDate.Date.Month);
                lstListMonth.Add(objMonth);
                FDate = FDate.AddMonths(1);
            }
            //objMonth = new ListMonth();
            //objMonth.MonthID = (semesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? LearningResultReportConstants.MONTH_DGCK_HKI :
            //    (semesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND) ? LearningResultReportConstants.MONTH_DGCK_HKII : 0;
            //objMonth.MonthName = "Đánh giá cuối kỳ";
            //lstListMonth.Add(objMonth);
            return lstListMonth;
        }
        private class ListMonth
        {
            public int MonthID { get; set; }
            public string MonthName { get; set; }
        }
        #endregion
    }
}