﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using System.IO;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class CommunicationNoteReportController:BaseController
    {
        #region Properties
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ICommunicationNoteReportBusiness CommunicationNoteReportBusiness;

        #endregion

        #region Constructor
        public CommunicationNoteReportController(IClassProfileBusiness ClassProfileBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IAcademicYearBusiness AcademicYearBusiness, ICommunicationNoteReportBusiness CommunicationNoteReportBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.CommunicationNoteReportBusiness = CommunicationNoteReportBusiness;
        }
        #endregion

        #region Action
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {

            List<ClassProfile> lstCP = new List<ClassProfile>();
            if (educationLevelID > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dic["EducationLevelID"] = educationLevelID;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                lstCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
             return Json(new SelectList(lstCP, "ClassProfileID", "DisplayName"));
            
        }

        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(FormCollection form)
        {
            int SemesterID = !string.IsNullOrEmpty(form["SemesterID"]) ? int.Parse(form["SemesterID"]) : 0;
            int EducationLevelID = !string.IsNullOrEmpty(form["EducationLevelID"]) ? int.Parse(form["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(form["ClassID"]) ? int.Parse(form["ClassID"]) : 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["UserAccountID"] = _globalInfo.UserAccountID;
            dic["SemesterID"] = SemesterID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ClassID"] = ClassID;

            string reportCode = string.Empty;
            if (SemesterID == 1 || SemesterID == 3)//Giua HKI + Giua HKII
            {
                if (EducationLevelID < 4)
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_GHKI;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_GHKI;
                }
                
            }
            else if (SemesterID == 2 || SemesterID == 4)//Cuoi HKI + Cuoi HKII
            {
                if (EducationLevelID < 4)
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_HKII;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_HKII;
                }
                
            }
            //else if (SemesterID == 3)//Giua HKII
            //{
            //    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_HKII;
            //}
            //else//Cuoi HKII
            //{
            //    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_HKII;
            //}

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = CommunicationNoteReportBusiness.GetCommunicationNoteReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = CommunicationNoteReportBusiness.CreateCommunicationNoteReport(dic);
                processedReport = CommunicationNoteReportBusiness.InsertCommunicationNoteReport(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection form)
        {
            int SemesterID = !string.IsNullOrEmpty(form["SemesterID"]) ? int.Parse(form["SemesterID"]) : 0;
            int EducationLevelID = !string.IsNullOrEmpty(form["EducationLevelID"]) ? int.Parse(form["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(form["ClassID"]) ? int.Parse(form["ClassID"]) : 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["UserAccountID"] = _globalInfo.UserAccountID;
            dic["SemesterID"] = SemesterID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ClassID"] = ClassID;

            Stream excel = CommunicationNoteReportBusiness.CreateCommunicationNoteReport(dic);
            ProcessedReport processedReport = CommunicationNoteReportBusiness.InsertCommunicationNoteReport(dic, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadReportPdf(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //danh sach hoc ky
            List<ComboObject> lstSemester = CommonList.Semester_CommunicationNoteReport();
            int defaultSemester = 0;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow < SecondStartDate)
            {
                defaultSemester = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI;
            }
            else
            {
                defaultSemester = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII;
            }

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester);

            //Lay danh sach khoi
            List<EducationLevel> lsEducationLevel = _globalInfo.EducationLevels;
            if (lsEducationLevel == null)
            {
                lsEducationLevel = new List<EducationLevel>();
            }

            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            int? defaultEduLevel = null;
            if (lsEducationLevel.Count > 0)
            {
                defaultEduLevel = lsEducationLevel.First().EducationLevelID;
            }

            //Lay danh sach lop
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (defaultEduLevel != null)
            {
                lstClass = getClassFromEducationLevel(defaultEduLevel.Value);
            }
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
          
        }
        private List<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.getClassByRoleAppliedLevel(_globalInfo.AcademicYearID.GetValueOrDefault()
               , _globalInfo.SchoolID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID)
               .Where(o => o.EducationLevelID==EducationLevelID).OrderBy(o=>o.OrderNumber).ToList();

            return lstClassProfile;
        }
        #endregion
    }
}