﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using System.IO;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class RetestRegisterReportController:BaseController
    {
        IClassProfileBusiness ClassProfileBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IClassSubjectBusiness ClassSubjectBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IPupilRetestRegistrationBusiness PupilRetestRegistrationBusiness;

        public RetestRegisterReportController(IClassProfileBusiness classProfileBusiness, 
            IAcademicYearBusiness AcademicYearBusiness, IClassSubjectBusiness ClassSubjectBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IPupilRetestRegistrationBusiness PupilRetestRegistrationBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.PupilRetestRegistrationBusiness = PupilRetestRegistrationBusiness;
           
        }
        public ActionResult Index()
        {
            //Hoc ky
            List<ComboObject> lstSemester = CommonList.SemesterAndAll();
            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", SystemParamsInFile.SEMESTER_OF_YEAR_ALL);

            //Khoi
            List<EducationLevel> listEducationLevel = new List<EducationLevel>(_globalInfo.EducationLevels);
            listEducationLevel.RemoveAt(listEducationLevel.Count - 1);
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(listEducationLevel, "EducationLevelID", "Resolution");
           
            //Lop
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(new List<ClassProfile>(), "ClassProfileID", "DisplayName");

            //Mon hoc
           
            IDictionary<string,object> dic=new Dictionary<string,object>();
            dic["AcademicYearID"]=_globalInfo.AcademicYearID;
            dic["AppliedLevel"]=_globalInfo.AppliedLevel;
            dic["Grade"]=_globalInfo.AppliedLevel;
            dic["IsVNEN"] = true;
            var lstSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                            .Select(o => new { SubjectID = o.SubjectID, SubjectName = o.SubjectCat.SubjectName, OrderInSubject=o.SubjectCat.OrderInSubject })
                                            .Distinct().OrderBy(o=>o.OrderInSubject).ToList();

            var objSubjectCat = lstSubject.ToList().FirstOrDefault();
            if (objSubjectCat != null)
            {
                ViewData[LearningResultReportConstants.LIST_SUBJECT] = new SelectList(lstSubject, "SubjectID", "SubjectName", objSubjectCat.SubjectID);
            }
            else
            {
                ViewData[LearningResultReportConstants.LIST_SUBJECT] = new SelectList(lstSubject, "SubjectID", "SubjectName");
            }

            return View();
        }

        public JsonResult AjaxLoadClass(int? educationLevelID)
        {
            if (educationLevelID == null)
            {
                return Json(new SelectList(new List<ClassProfile>(), "ClassProfileID", "DisplayName"));
            }

            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicClass["EducationLevelID"] = educationLevelID;
            
            var lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                .OrderBy(o=>o.OrderNumber).ThenBy(o=>o.DisplayName).ToList();

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }
        public JsonResult AjaxLoadSubject(int? classID, int? educationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Grade"] = _globalInfo.AppliedLevel;
            dic["ClassID"]=classID;
            dic["EducationLevelID"]=educationLevelID;
            dic["IsVNEN"] = true;

            var lstSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                             .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                                             .Select(o => new { SubjectID = o.SubjectID, SubjectName = o.SubjectCat.SubjectName, OrderInSubject = o.SubjectCat.OrderInSubject })
                                             .Distinct().OrderBy(o => o.OrderInSubject).ToList();
            var objSubjectCat = lstSubject.ToList().FirstOrDefault();
            if (objSubjectCat != null)
            {
                return Json(new SelectList(lstSubject, "SubjectID", "SubjectName", objSubjectCat.SubjectID));
            }
            else
            {
                return Json(new SelectList(lstSubject, "SubjectID", "SubjectName"));
            }
        }

        public JsonResult GetReport(SearchViewModel form)
        {
            if (form.SubjectID == null)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_SubjectID");
            }

            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            PupilRetestRegistrationBO PupilRetestRegistrationBO = new PupilRetestRegistrationBO();
            PupilRetestRegistrationBO.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            PupilRetestRegistrationBO.EducationLevelID = form.EducationLevelID.GetValueOrDefault();
            PupilRetestRegistrationBO.ClassID = form.ClassID.GetValueOrDefault();
            PupilRetestRegistrationBO.SubjectID = form.SubjectID.Value;
            PupilRetestRegistrationBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
            PupilRetestRegistrationBO.SchoolID = _globalInfo.SchoolID.Value;
            PupilRetestRegistrationBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI; ;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.PupilRetestRegistrationBusiness.GetPupilRetest(PupilRetestRegistrationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.PupilRetestRegistrationBusiness.CreatePupilRetestRegistrationSecondary(PupilRetestRegistrationBO);
                processedReport = this.PupilRetestRegistrationBusiness.InsertPupilRetestRegistrationSecondary(PupilRetestRegistrationBO, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        public JsonResult GetNewReport(SearchViewModel form)
        {
            ProcessedReport processedReport = null;
            PupilRetestRegistrationBO PupilRetestRegistrationBO = new PupilRetestRegistrationBO();
            PupilRetestRegistrationBO.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            PupilRetestRegistrationBO.EducationLevelID = form.EducationLevelID.GetValueOrDefault();
            PupilRetestRegistrationBO.ClassID = form.ClassID.GetValueOrDefault();
            PupilRetestRegistrationBO.SubjectID = form.SubjectID.Value;
            PupilRetestRegistrationBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
            PupilRetestRegistrationBO.SchoolID = _globalInfo.SchoolID.Value;
            PupilRetestRegistrationBO.AcademicYearID = _globalInfo.AcademicYearID.Value;

            Stream excel = this.PupilRetestRegistrationBusiness.CreatePupilRetestRegistrationSecondary(PupilRetestRegistrationBO);
            processedReport = this.PupilRetestRegistrationBusiness.InsertPupilRetestRegistrationSecondary(PupilRetestRegistrationBO, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}