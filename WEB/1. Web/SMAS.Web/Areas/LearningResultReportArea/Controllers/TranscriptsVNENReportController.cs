﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using Ionic.Zip;
using System.IO.Compression;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;


namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class TranscriptsVNENReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ITranscriptsVNENReportBusiness TranscriptsVNENReportBusiness;

        public TranscriptsVNENReportController(IClassProfileBusiness classProfileBusiness, IAcademicYearBusiness academicYearBusiness
            , IProcessedReportBusiness processedReportBusiness, IPupilOfClassBusiness pupilOfClassBusiness,
            ITranscriptsVNENReportBusiness transcriptsVNENReportBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.TranscriptsVNENReportBusiness = transcriptsVNENReportBusiness;
        }

        public ActionResult Index()
        {
            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            List<ClassProfile> lstClass = new List<ClassProfile>();
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            List<PupilOfClassBO> lstPupilOfClass = new List<PupilOfClassBO>();
            ViewData[LearningResultReportConstants.LIST_PUPIL] = new SelectList(lstPupilOfClass, "PupilID", "PupilFullName");

            return View();
        }
        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["EducationLevelID"] = educationLevelID;
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            var lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.IsVnenClass == true).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            ViewData[LearningResultReportConstants.DISABLED_BUTTON] = lstClass.Count == 0;
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }
        public JsonResult AjaxLoadPupil(int ClassID)
        {
            List<PupilOfClassBO> lstPupil = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(p => p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).Distinct().OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            ViewData[LearningResultReportConstants.DISABLED_BUTTON] = lstPupil.Count == 0;
            return Json(new SelectList(lstPupil, "PupilID", "PupilFullName"));
        }
        #region Export Excel
        //[ValidateAntiForgeryToken]
        public JsonResult GetTranscriptsVNENReport(SearchViewModel form)
        {
            TranscriptsVNENReportBO objTranscriptVNENBO = new TranscriptsVNENReportBO();
            int pupilID = form.PupilID.HasValue ? form.PupilID.Value : 0;
            objTranscriptVNENBO.ClassID = form.ClassID.GetValueOrDefault();
            objTranscriptVNENBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            objTranscriptVNENBO.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            objTranscriptVNENBO.PupilID = pupilID;
            objTranscriptVNENBO.fillPaging = form.paging == 1 ? true : false;
            bool isZip = pupilID == 0 ? true : false;
            ReportDefinition reportDef = TranscriptsVNENReportBusiness.GetReportDefinition(isZip ? SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN_ZIPFILE : SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            Stream excel = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = TranscriptsVNENReportBusiness.GetProcessedReport(objTranscriptVNENBO, isZip);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                if (isZip)
                {
                    string FolderSaveFile = string.Empty;
                    TranscriptsVNENReportBusiness.DownloadZipFile(objTranscriptVNENBO, out FolderSaveFile);
                    DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(Path.Combine(FolderSaveFile));
                        using (MemoryStream output = new MemoryStream())
                        {
                            zip.Save(output);
                            di.Delete(true);
                            objTranscriptVNENBO.PupilID = 0;
                            processedReport = TranscriptsVNENReportBusiness.InsertTranscriptsVNENReport(objTranscriptVNENBO, output, isZip);
                        }
                    }
                }
                else
                {
                    excel = TranscriptsVNENReportBusiness.GetTranscriptsVNENReport(objTranscriptVNENBO);
                    processedReport = TranscriptsVNENReportBusiness.InsertTranscriptsVNENReport(objTranscriptVNENBO, excel);
                    excel.Close();
                }
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

       // [ValidateAntiForgeryToken]
        public JsonResult GetNewTranscriptsVNENReport(SearchViewModel form)
        {
            TranscriptsVNENReportBO objTranscriptVNENBO = new TranscriptsVNENReportBO();
            objTranscriptVNENBO.ClassID = form.ClassID.GetValueOrDefault();
            objTranscriptVNENBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            objTranscriptVNENBO.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            objTranscriptVNENBO.fillPaging = form.paging == 1 ? true : false;
            int pupilID = form.PupilID.HasValue ? form.PupilID.Value : 0;
            bool isZip = pupilID == 0 ? true : false;
            objTranscriptVNENBO.PupilID = pupilID;
            Stream excel = null;
            ProcessedReport processedReport = null;
            if (isZip)
            {
                string FolderSaveFile = string.Empty;
                TranscriptsVNENReportBusiness.DownloadZipFile(objTranscriptVNENBO, out FolderSaveFile);
                DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(Path.Combine(FolderSaveFile));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        di.Delete(true);
                        objTranscriptVNENBO.PupilID = 0;
                        processedReport = TranscriptsVNENReportBusiness.InsertTranscriptsVNENReport(objTranscriptVNENBO, output, isZip);
                    }
                }
            }
            else
            {
                excel = TranscriptsVNENReportBusiness.GetTranscriptsVNENReport(objTranscriptVNENBO);
                processedReport = TranscriptsVNENReportBusiness.InsertTranscriptsVNENReport(objTranscriptVNENBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN_ZIPFILE.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }

            result.FileDownloadName = processedReport.ReportName;
            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN_ZIPFILE.Equals(processedReport.ReportCode))
            {
                //excel = new MemoryStream(processedReport.ReportData);
                //result = new FileStreamResult(excel, "application/zip");
                //result.FileDownloadName = processedReport.ReportName;
                //return result;

                excel = new MemoryStream(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                //Duong dan thu muc chua file excel
                string inFolder = objFile.CreateFolder();
                //Duong dan thu muc chua file zip
                string outFolder = objFile.CreateFolder();

                string outFile = outFolder + "\\" + processedReport.ReportName;
                try
                {
                    //1. Extract file tu stream ra thu muc
                    objFile.UnzipFromStream(excel, inFolder);
                    //2. Chuyen file .xls thanh pdf
                    objFile.ConvertFileInFolderToPDF(inFolder, 0);
                    //3 Zip thu muc PDF
                    objFile.ZipFolder(outFile, inFolder);
                    //4. Chuyen file zip sang stream
                    result = new FileStreamResult(objFile.ConvertZipFileToStream(outFile), PdfExport.CONTENT_TYPE_ZIP);
                    result.FileDownloadName = processedReport.ReportName;
                }
                finally
                {
                    objFile.DeleteFolder(inFolder);
                    objFile.DeleteFolder(outFolder);
                }
                return result;

            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
            }

            
        }
        #endregion
    }
}