﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class GeneralTranscriptReportController:BaseController
    {
        IClassProfileBusiness ClassProfileBusiness;
        IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        ITranscriptsByPeriodBusiness TranscriptsByPeriodBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness;
        public GeneralTranscriptReportController(IClassProfileBusiness classProfileBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, ITranscriptsByPeriodBusiness TranscriptsByPeriodBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.TranscriptsByPeriodBusiness = TranscriptsByPeriodBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.TranscriptsBySemesterBusiness = TranscriptsBySemesterBusiness;
        }
        public ActionResult Index()
        {
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevel.EducationLevelID, null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // load combobox Semester
            List<ComboObject> lstSemester = CommonList.SemesterAndAll();

            ComboObject defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow >= FirstStarDate && datenow <= FirstEndDate)
            {
                defaultSemester = lstSemester[0];
            }
            else if (datenow >= SecondStartDate && datenow <= SecondEndDate)
            {
                defaultSemester = lstSemester[1];
            }
            else
            {
                defaultSemester = lstSemester[2];
            }

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester.key);

            List<PeriodDeclaration> lstPeriodCommon = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "Semester", Convert.ToInt32(defaultSemester.key) } })
                                                                                .OrderByDescending(o => o.EndDate).ToList();

            PeriodDeclaration defaultPeriod = lstPeriodCommon.Where(o => o.FromDate <= datenow && datenow <= o.EndDate).FirstOrDefault();

            if (defaultPeriod != null)
            {
                ViewData[LearningResultReportConstants.LIST_PERIOD] = new SelectList(lstPeriodCommon, "PeriodDeclarationID", "Resolution", defaultPeriod.PeriodDeclarationID);
            }
            else
            {
                ViewData[LearningResultReportConstants.LIST_PERIOD] = new SelectList(lstPeriodCommon, "PeriodDeclarationID", "Resolution");
            }
            

            return View();
        }

        public JsonResult AjaxLoadPeriod(int semester)
        {
            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "Semester", Convert.ToInt32(semester) } })
                                                                         .OrderByDescending(o=>o.EndDate).ToList();

            PeriodDeclaration defaultPeriod = lstPeriod.Where(o => o.FromDate <= datenow && datenow <= o.EndDate).FirstOrDefault();

            if (defaultPeriod != null)
            {
                return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", defaultPeriod.PeriodDeclarationID));
            }
            else
            {
                return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution"));
            }

        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevelID, null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        #region Bao cao Bang diem tong hop theo dot
        [ValidateAntiForgeryToken]
        public JsonResult GetGeneralTranscriptReportByPeriod(SearchViewModel model)
        {
            if (model.PeriodID == null)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_PeriodID");
            }

            if (model.EducationLevelID == null)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_EducationLevelID");
            }

            if (ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, model.EducationLevelID, null,
                                                                        _globalInfo.UserAccountID,_globalInfo.IsSuperVisingDeptRole,_globalInfo.IsSubSuperVisingDeptRole).Count() == 0)
            {
                throw new BusinessException("LearningResultReport_Validate_NotClass");
            }
            PupilRewardReportBO bo = new PupilRewardReportBO();
            GlobalInfo GlobalInfo = new GlobalInfo();

            string type = JsonReportMessage.NEW;

            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.SchoolID = glo.SchoolID.Value;
            toc.AcademicYearID = glo.AcademicYearID.Value;
            toc.AppliedLevel = glo.AppliedLevel.Value;
            if (model.ClassID.HasValue)
            {
                toc.ClassID = model.ClassID.Value;
            }
            toc.EducationLevelID = model.EducationLevelID.Value;
            toc.PeriodDeclarationID = model.PeriodID.Value;
            toc.SchoolID = glo.SchoolID.Value;
            toc.Semester = model.SemesterID;
            toc.UserAccountID = _globalInfo.UserAccountID;
            toc.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
            toc.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEODOT);
            ProcessedReport entity = null;

            if (reportDef.IsPreprocessed == true)
            {
                entity = TranscriptsByPeriodBusiness.GetSummariseTranscriptsByPeriod(toc);
                if (entity != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptsByPeriodBusiness.CreateSummariseTranscriptsByPeriod(toc);
                entity = TranscriptsByPeriodBusiness.InsertSummariseTranscriptsByPeriod(toc, excel);
                ProcessedReportBusiness.Save();
                excel.Close();
            }
            return Json(new JsonReportMessage(entity, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewGeneralTranscriptReportByPeriod(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.SchoolID = glo.SchoolID.Value;
            toc.AcademicYearID = glo.AcademicYearID.Value;
            toc.AppliedLevel = glo.AppliedLevel.Value;
            if (model.ClassID.HasValue)
            {
                toc.ClassID = model.ClassID.Value;
            }
            toc.EducationLevelID = model.EducationLevelID.Value;
            toc.PeriodDeclarationID = model.PeriodID.Value;
            toc.SchoolID = glo.SchoolID.Value;
            toc.Semester = model.SemesterID;
            toc.UserAccountID = _globalInfo.UserAccountID;
            toc.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
            toc.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
            Stream excel = TranscriptsByPeriodBusiness.CreateSummariseTranscriptsByPeriod(toc);
            ProcessedReport entity = TranscriptsByPeriodBusiness.InsertSummariseTranscriptsByPeriod(toc, excel);
            excel.Close();
            return Json(new JsonReportMessage(entity, JsonReportMessage.NEW));
        }
        #endregion
        #region Bao cao bang diem tong hop theo hoc ky
        [ValidateAntiForgeryToken]
        public JsonResult GetGeneralTranscriptReport(SearchViewModel form)
        {
            if (form.EducationLevelID == null)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_EducationLevelID");
            }

            if (ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, form.EducationLevelID, null,
                                                            _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole).Count() == 0)
            {
                throw new BusinessException("LearningResultReport_Validate_NotClass");
            }

            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();

            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            TranscriptOfClass.ClassID = form.ClassID.GetValueOrDefault();
            TranscriptOfClass.Semester = form.SemesterID;
            TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
            TranscriptOfClass.isAdmin = new GlobalInfo().IsAdminSchoolRole;
            TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            TranscriptOfClass.UserAccountID = new GlobalInfo().UserAccountID;
            string reportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEOKY; ;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.TranscriptsBySemesterBusiness.GetSummariseTranscriptsBySemester(TranscriptOfClass);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptsBySemesterBusiness.CreateSummariseTranscriptsBySemester(TranscriptOfClass);
                processedReport = this.TranscriptsBySemesterBusiness.InsertSummariseTranscriptsBySemester(TranscriptOfClass, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewGeneralTranscriptReport(SearchViewModel form)
        {
            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            TranscriptOfClass.ClassID = form.ClassID.GetValueOrDefault();
            TranscriptOfClass.Semester = form.SemesterID;
            TranscriptOfClass.isAdmin = new GlobalInfo().IsAdminSchoolRole;
            TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
            TranscriptOfClass.UserAccountID = new GlobalInfo().UserAccountID;
            Stream excel = this.TranscriptsBySemesterBusiness.CreateSummariseTranscriptsBySemester(TranscriptOfClass);
            ProcessedReport processedReport = this.TranscriptsBySemesterBusiness.InsertSummariseTranscriptsBySemester(TranscriptOfClass, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        #endregion

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}