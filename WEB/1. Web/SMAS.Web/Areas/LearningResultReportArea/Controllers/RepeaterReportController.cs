﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class RepeaterReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        public RepeaterReportController(IClassProfileBusiness classProfileBusiness, IAcademicYearBusiness academicYearBusiness
            , IProcessedReportBusiness processedReportBusiness, ISummedEndingEvaluationBusiness summedEndingEvaluationBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.SummedEndingEvaluationBusiness = summedEndingEvaluationBusiness;
        }
        public ActionResult Index()
        {
            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            List<ClassProfile> lstClass = new List<ClassProfile>();
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            return View();
        }
        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            List<ClassProfile> lstCP = new List<ClassProfile>();
            if (educationLevelID > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dic["EducationLevelID"] = educationLevelID;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                lstCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }

            return Json(new SelectList(lstCP, "ClassProfileID", "DisplayName"));
        }
        #region Export
        [ValidateAntiForgeryToken]
        public JsonResult GetRepeaterReport(FormCollection frm)
        {
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int TypeExport = !string.IsNullOrEmpty(frm["TypeExport"]) ? int.Parse(frm["TypeExport"]) : 0;
            string type = JsonReportMessage.NEW;
            string ReportCode = string.Empty;
            if (TypeExport == 1)
            {
                ReportCode = SystemParamsInFile.REPORT_REPEATER_TT22;
            }
            else
            {
                ReportCode = SystemParamsInFile.REPORT_RATEADD_TT22;
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"TypeExport",TypeExport},
                {"ReportCode",ReportCode}
            };
            ProcessedReport processedReport = SummedEndingEvaluationBusiness.GetProcessedRepeaterReport(dic);
            if (processedReport != null)
            {
                type = JsonReportMessage.OLD;
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = SummedEndingEvaluationBusiness.GetRepeaterReport(dic);
                processedReport = SummedEndingEvaluationBusiness.InsertRepeaterProcessedReport(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewRepeaterReport(FormCollection frm)
        {
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int TypeExport = !string.IsNullOrEmpty(frm["TypeExport"]) ? int.Parse(frm["TypeExport"]) : 0;
            Stream excel = null;
            ProcessedReport processedReport = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"TypeExport",TypeExport}
            };
            string ReportCode = string.Empty;
            if (TypeExport == 1)
            {
                ReportCode = SystemParamsInFile.REPORT_REPEATER_TT22;
            }
            else
            {
                ReportCode = SystemParamsInFile.REPORT_RATEADD_TT22;
            }
            dic.Add("ReportCode", ReportCode);
            excel = SummedEndingEvaluationBusiness.GetRepeaterReport(dic);
            processedReport = SummedEndingEvaluationBusiness.InsertRepeaterProcessedReport(dic, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            excel = ReportUtils.Decompress(processedReport.ReportData);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;
            return result;
        }

        public FileResult DownloadReportPdf(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion
    }
}