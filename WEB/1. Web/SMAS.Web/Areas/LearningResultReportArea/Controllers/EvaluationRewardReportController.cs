﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class EvaluationRewardReportController:BaseController
    {
        IAcademicYearBusiness AcademicYearBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        IEvaluationRewardBusiness EvaluationRewardBusiness;
        public EvaluationRewardReportController(IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IClassProfileBusiness ClassProfileBusiness, IEvaluationRewardBusiness evaluationRewardBusiness)
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EvaluationRewardBusiness = evaluationRewardBusiness;
        }
        public ActionResult Index()
        {

            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevel.EducationLevelID, null,
                                                                         _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            // Hoc ky
            List<ComboObject> lstSemester = null;
            lstSemester = CommonList.Semester();
            ComboObject defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);
            if (datenow >= FirstStarDate && datenow <= FirstEndDate)
            {
                defaultSemester = lstSemester[0];
            }
            else
            {
                defaultSemester = lstSemester[1];
            }
            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester.key);
            return View();
        }
        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevelID, null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult GetPupilRewardReport(FormCollection frm)
        {
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int SemesterID = !string.IsNullOrEmpty(frm["SemesterID"]) ? int.Parse(frm["SemesterID"]) : 0;
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            IDictionary<string,object> dic = new Dictionary<string,object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID}
            };
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT22;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = EvaluationRewardBusiness.GetEvaluationRewardRepord(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = EvaluationRewardBusiness.CreateEvaluationRewardReport(dic);
                processedReport = EvaluationRewardBusiness.InsertEvaluationRewarReport(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        public JsonResult GetNewPupilRewardReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int SemesterID = !string.IsNullOrEmpty(frm["SemesterID"]) ? int.Parse(frm["SemesterID"]) : 0;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID}
            };
            Stream excel = EvaluationRewardBusiness.CreateEvaluationRewardReport(dic);
            processedReport = EvaluationRewardBusiness.InsertEvaluationRewarReport(dic, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}