﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Web.Constants;
using SMAS.Business.Common;
using SMAS.Web.Utils;
using System.IO;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class LearningResultCertificateController:BaseController
    {
        #region Properties
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ILearningResultReportBusiness LearningResultReportBusiness;

        #endregion

        #region Constructor
        public LearningResultCertificateController(IPupilProfileBusiness PupilProfileBusiness, IClassProfileBusiness ClassProfileBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IAcademicYearBusiness AcademicYearBusiness, IPupilOfClassBusiness PupilOfClassBusiness,
            ILearningResultReportBusiness LearningResultReportBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.LearningResultReportBusiness = LearningResultReportBusiness;
        }
        #endregion

        #region Action
        public ActionResult Index()
        {
            SetViewData();
            ViewData[LearningResultReportConstants.LIST_PUPIL] = _Search(_globalInfo.EducationLevels.FirstOrDefault().EducationLevelID, null, null, null);
            ViewData[LearningResultReportConstants.EDUCATION_LEVEL] = _globalInfo.EducationLevels.FirstOrDefault().EducationLevelID;
            ViewData[LearningResultReportConstants.CLASS_ID] = null;
            ViewData[LearningResultReportConstants.PUPIL_CODE] = null;
            ViewData[LearningResultReportConstants.PUPIL_NAME] = null;
            return View();

        }

        public JsonResult AjaxLoadClass(int educationLevelId)
        {

            List<ClassProfile> lsCP = getClassFromEducationLevel(educationLevelId);
            
             return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"));
            
        }

        public ActionResult Search(LearningResultCertificateSearchViewModel form)
        {
            List<LearningResultCertificateListViewModel> lstResult = _Search(form.EducationLevelID.Value, form.ClassID, form.PupilCode, form.PupilName);
            ViewData[LearningResultReportConstants.EDUCATION_LEVEL] = form.EducationLevelID;
            ViewData[LearningResultReportConstants.CLASS_ID] = form.ClassID;
            ViewData[LearningResultReportConstants.PUPIL_CODE] = form.PupilCode;
            ViewData[LearningResultReportConstants.PUPIL_NAME] = form.PupilName;
            return PartialView("_List", lstResult);
        }
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(LearningResultCertificateSearchViewModel form, List<int> lstPupilOfClassID)
        {
            if (lstPupilOfClassID == null || (lstPupilOfClassID != null && lstPupilOfClassID.Count == 0))
            {
                throw new BusinessException("Thầy cô cần chọn học sinh để in giấy chứng nhận kết quả học tập");
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EducationLevelID"] = form.EducationLevelID;
            dic["ClassID"] = form.ClassID;
            dic["Semester"] = form.Semester;
            dic["PupilCode"] = form.PupilCode;
            dic["PupilName"] = form.PupilName;
            dic["ListPupilOfClassID"] = lstPupilOfClassID;

            string reportCode = SystemParamsInFile.REPORT_LEARNING_RESULT_CERTIFICATE;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = LearningResultReportBusiness.GetLearningResultCertificate(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = LearningResultReportBusiness.CreateLearningResultCertificate(dic);
                processedReport = LearningResultReportBusiness.InsertLearningResultCertificate(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(LearningResultCertificateSearchViewModel form, List<int> lstPupilOfClassID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EducationLevelID"] = form.EducationLevelID;
            dic["ClassID"] = form.ClassID;
            dic["Semester"] = form.Semester;
            dic["PupilCode"] = form.PupilCode;
            dic["PupilName"] = form.PupilName;
            dic["ListPupilOfClassID"] = lstPupilOfClassID;

            Stream excel = LearningResultReportBusiness.CreateLearningResultCertificate(dic);
            ProcessedReport processedReport = LearningResultReportBusiness.InsertLearningResultCertificate(dic, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF); ;
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
          
            //Lay danh sach khoi
            List<EducationLevel> lsEducationLevel = _globalInfo.EducationLevels;
            if (lsEducationLevel == null)
            {
                lsEducationLevel = new List<EducationLevel>();
            }

            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            int? defaultEduLevel = null;
            if (lsEducationLevel.Count > 0)
            {
                defaultEduLevel = lsEducationLevel.First().EducationLevelID;
            }

            //Lay danh sach lop
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (defaultEduLevel != null)
            {
                lstClass = getClassFromEducationLevel(defaultEduLevel.Value);
            }
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
          
        }

        private List<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.getClassByRoleAppliedLevel(_globalInfo.AcademicYearID.GetValueOrDefault()
               , _globalInfo.SchoolID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID)
               .Where(o => o.EducationLevelID == EducationLevelID)
               .Where(o => o.IsVnenClass == null || o.IsVnenClass == false).OrderBy(o => o.OrderNumber).ToList();

            return lstClassProfile;
        }

        private List<LearningResultCertificateListViewModel> _Search(int educationLevelId, int? classId, string pupilCode, string pupilName)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = educationLevelId;
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["PupilCode"] = pupilCode;
            dic["FullName"] = pupilName;
            List<LearningResultCertificateListViewModel> lstPoc = PupilOfClassBusiness.Search(dic)
                .Where(o => o.ClassProfile.IsVnenClass == null || o.ClassProfile.IsVnenClass == false)
                .OrderBy(o => o.ClassProfile.OrderNumber.HasValue ? o.ClassProfile.OrderNumber : 0)
                .ThenBy(o => o.ClassProfile.DisplayName)
                .ThenBy(u => u.OrderInClass)
                .ThenBy(o => o.PupilProfile.Name)
                .ThenBy(o => o.PupilProfile.FullName)
                .Select(o => new LearningResultCertificateListViewModel
                {
                    ClassName = o.ClassProfile.DisplayName,
                    Genre = o.PupilProfile.Genre == SMAS.Web.Constants.GlobalConstants.GENRE_FEMALE ? "Nữ" : "Nam",
                    PupilCode = o.PupilProfile.PupilCode,
                    PupilName = o.PupilProfile.FullName,
                    PupilOfClassID = o.PupilOfClassID,
                    BirthDay = o.PupilProfile.BirthDate
                }).ToList();

            return lstPoc;
        }
        #endregion
    }
}