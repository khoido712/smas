﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using System.IO;
using SMAS.Web.Utils;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilRetestReportController:BaseController
    {
        IAcademicYearBusiness AcademicYearBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        IPupilRetestBusiness PupilRetestBusiness;
        public PupilRetestReportController(IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IClassProfileBusiness ClassProfileBusiness, IPupilRetestBusiness PupilRetestBusiness)
        {

            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilRetestBusiness = PupilRetestBusiness;
        }
        public ActionResult Index()
        {
            //Khoi
            List<EducationLevel> listEducationLevel = new List<EducationLevel>(_globalInfo.EducationLevels);
            listEducationLevel.RemoveAt(listEducationLevel.Count - 1);
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(listEducationLevel, "EducationLevelID", "Resolution");

            //Lop

            List<ClassProfile> lstClass = new List<ClassProfile>();
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

           
            return View();
        }

        public JsonResult AjaxLoadClass(int? educationLevelID)
        {
            if (educationLevelID == null)
            {
                return Json(new SelectList(new List<ClassProfile>(), "ClassProfileID", "DisplayName"));
            }
            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicClass["EducationLevelID"] = educationLevelID;
            dicClass["IsVNEN"] = true;
            
            var lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                .OrderBy(o=>o.OrderNumber).ThenBy(o=>o.DisplayName).ToList();

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult GetReport(SearchViewModel form)
        {
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            PupilRetestBO PupilRetestBO = new PupilRetestBO();
            PupilRetestBO.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            PupilRetestBO.EducationLevelID = form.EducationLevelID.GetValueOrDefault() ;
            PupilRetestBO.ClassID = form.ClassID.GetValueOrDefault();
            PupilRetestBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
            PupilRetestBO.SchoolID = _globalInfo.SchoolID.Value;
            PupilRetestBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
            string reportCode = SystemParamsInFile.REPORT_DS_HS_THI_LAI; ;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.PupilRetestBusiness.GetPupilRetest(PupilRetestBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.PupilRetestBusiness.CreatePupilRetest(PupilRetestBO);
                processedReport = this.PupilRetestBusiness.InsertPupilRetest(PupilRetestBO, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        public JsonResult GetNewReport(SearchViewModel form)
        {
            ProcessedReport processedReport = null;
            PupilRetestBO PupilRetestBO = new PupilRetestBO();
            PupilRetestBO.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            PupilRetestBO.EducationLevelID = form.EducationLevelID.GetValueOrDefault();
            PupilRetestBO.ClassID = form.ClassID.GetValueOrDefault();
            PupilRetestBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
            PupilRetestBO.SchoolID = _globalInfo.SchoolID.Value;
            PupilRetestBO.AcademicYearID = _globalInfo.AcademicYearID.Value;

            Stream excel = this.PupilRetestBusiness.CreatePupilRetest(PupilRetestBO);
            processedReport = this.PupilRetestBusiness.InsertPupilRetest(PupilRetestBO, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}