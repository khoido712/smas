﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;
using Ionic.Zip;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    public class MarkInputBookReportController:BaseController
    {
               
        IAcademicYearBusiness AcademicYearBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        ISchoolFacultyBusiness SchoolFacultyBusiness;
        IEmployeeBusiness EmployeeBusiness;
        IMasterBookBusiness MasterBookBusiness;

        public MarkInputBookReportController(IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IClassProfileBusiness ClassProfileBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness,
            IMasterBookBusiness MasterBookBusiness)
        {

            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.MasterBookBusiness = MasterBookBusiness;
        }
        public ActionResult Index()
        {
            //To bo mon
            List<SchoolFaculty> lstSf = SchoolFacultyBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID && o.IsActive).OrderBy(o => o.FacultyName).ToList();
            ViewData[LearningResultReportConstants.LIST_SCHOOL_FACULTY] = new SelectList(lstSf, "SchoolFacultyID", "FacultyName");

            // Hoc ky
            List<ComboObject> lstSemester = null;
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                lstSemester = CommonList.SemesterPrimary();
            }
            else
            {
                lstSemester = CommonList.SemesterAndAll();
            }

            int selectedSemester = _globalInfo.Semester.HasValue && _globalInfo.Semester.Value == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? 1 : 3;
            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", selectedSemester);

            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = _globalInfo.SchoolID;
            List<Employee> listEmp = EmployeeBusiness.Search(dicToGetTeacher).ToList().OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            IEnumerable<ComboObject> lstComboEmployee = from q in listEmp
                                                        select new ComboObject
                                                        {
                                                            key = q.EmployeeID.ToString(),
                                                            value = q.FullName + " - " + q.EmployeeCode

                                                        };

            ViewData[LearningResultReportConstants.LIST_TEACHER] = lstComboEmployee;

            return View();
        }

        public PartialViewResult AjaxLoadTeacher(int? schoolFacultyID)
        {
            //Lay danh sach giao vien trong truong
            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = _globalInfo.SchoolID;
            dicToGetTeacher["FacultyID"] = schoolFacultyID;
            List<Employee> listEmp = EmployeeBusiness.Search(dicToGetTeacher).ToList().OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            IEnumerable<ComboObject> lstComboEmployee = from q in listEmp
                                                        select new ComboObject
                                                        {
                                                            key = q.EmployeeID.ToString(),
                                                            value = q.FullName + " - " + q.EmployeeCode

                                                        };

            ViewData[LearningResultReportConstants.LIST_TEACHER] = lstComboEmployee;

            return PartialView("_TeacherCombobox");
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(int semester, int? schoolFacultyID, int? teacherId, int? numOfRows, bool? isNumRowPupilNum, bool? idg, int fitIn)
        {
            if ((isNumRowPupilNum == null || isNumRowPupilNum == false) && (!numOfRows.HasValue || (numOfRows.HasValue && (numOfRows.Value < 20 || numOfRows > 60))))
            {
                throw new BusinessException("Số dòng dữ liệu phải nằm trong khoảng [20 - 60]");
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = semester;
            dic["TeacherID"] = teacherId;
            dic["SchoolFacultyID"] = schoolFacultyID;

            string reportCode = SystemParamsInFile.REPORT_MARKINPUTBOOK;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.MasterBookBusiness.GetReportMarkInpurBook(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = MasterBookBusiness.CreateReportMarkInpurBook(dic, numOfRows.GetValueOrDefault(), isNumRowPupilNum.GetValueOrDefault(), idg.GetValueOrDefault(), fitIn);
                processedReport = this.MasterBookBusiness.InsertReportMarkInpurBook(dic, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(int semester, int? schoolFacultyID, int? teacherId, int? numOfRows, bool? isNumRowPupilNum, bool? idg, int fitIn)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = semester;
            dic["TeacherID"] = teacherId;
            dic["SchoolFacultyID"] = schoolFacultyID;

            Stream excel = MasterBookBusiness.CreateReportMarkInpurBook(dic, numOfRows.GetValueOrDefault(), isNumRowPupilNum.GetValueOrDefault(), idg.GetValueOrDefault(), fitIn);
            ProcessedReport processedReport = this.MasterBookBusiness.InsertReportMarkInpurBook(dic, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport, int booklet)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream file = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport pdfExporter = new PdfExport();
            if (processedReport.ReportName.EndsWith(".xls"))
            {
                var objRetVal = pdfExporter.ConvertPdf(file, processedReport.ReportName, booklet);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
            }
            else
            {
                List<OutputFile> lstOuputFile = new List<OutputFile>();
                List<string> lstFileName = new List<string>();
                using (ZipFile zip = ZipFile.Read(file))
                {
                    foreach (ZipEntry e in zip)
                    {
                        using (MemoryStream stream = new MemoryStream())
                        {
                            e.Extract(stream);
                            var objRetVal = pdfExporter.ConvertPdf(stream, e.FileName, booklet);
                            lstOuputFile.Add(objRetVal);
                        }
                    }
                }

                using (ZipFile zip = new ZipFile())
                {
                    for (int i = 0; i < lstOuputFile.Count; i++)
                    {
                        zip.AddEntry(lstOuputFile[i].SaveFileName, lstOuputFile[i].ContentFile);
                    }

                    MemoryStream output = new MemoryStream();

                    zip.Save(output);
                    output.Seek(0, SeekOrigin.Begin);
                    return File(output, PdfExport.CONTENT_TYPE_ZIP, processedReport.ReportName);
                }
            }
        }
    }
}