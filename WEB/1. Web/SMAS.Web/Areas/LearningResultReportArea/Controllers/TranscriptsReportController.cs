﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using System.IO;
using SMAS.Web.Utils;
using SMAS.Business.BusinessObject;
using Ionic.Zip;
using SMAS.VTUtils.Utils;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class TranscriptsReportController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ITranscriptsBusiness TranscriptsBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private int ViewBooklet
        {
            get
            {
                return SessionObject.Get<int>("TranscriptsReportController");
            }
            set
            {
                SessionObject.Set<int>("TranscriptsReportController", value);
            }
        }
        public TranscriptsReportController(IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IClassProfileBusiness ClassProfileBusiness, IPupilOfClassBusiness PupilOfClassBusiness,
            ITranscriptsBusiness TranscriptsBusiness, IPupilProfileBusiness PupilProfileBusiness)
        {

            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.TranscriptsBusiness = TranscriptsBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
        }
        public ActionResult Index()
        {

            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevel.EducationLevelID, null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            //Hoc sinh
            ViewData[LearningResultReportConstants.LIST_PUPIL] = new List<ComboObject>();

            return View();
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevelID, null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        public PartialViewResult AjaxLoadPupil(int? educationLevelID, int? classID)
        {
            if (educationLevelID == null || classID == null)
            {
                ViewData[LearningResultReportConstants.LIST_PUPIL] = new List<ComboObject>();

                return PartialView("_PupilCombobox");
            }

            List<ComboObject> lstPupil = new List<ComboObject>();
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                IQueryable<PupilOfClass> lsCP = getPupilFromClass(classID.Value);
                List<PupilProfileBO> lsPP = lsCP.Select(o => new PupilProfileBO
                {
                    OrderInClass = o.OrderInClass,
                    PupilProfileID = o.PupilID,
                    FullName = o.PupilProfile.FullName,
                    Name = o.PupilProfile.Name
                }).Distinct().ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.FullName).ToList();

                lstPupil = lsPP.Select(o => new ComboObject { key = o.PupilProfileID.ToString(), value = o.FullName }).ToList();

            }
            else if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                //AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        { "ClassID", classID },
                                                        { "AcademicYearID", _globalInfo.AcademicYearID.Value }
                                                    };

                IQueryable<PupilOfClass> lsPOC = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(o => o.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS);

                List<PupilProfileBO> lsPP = lsPOC.Select(o => new PupilProfileBO
                {
                    OrderInClass = o.OrderInClass,
                    PupilProfileID = o.PupilID,
                    FullName = o.PupilProfile.FullName,
                    Name = o.PupilProfile.Name
                }).Distinct().ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.FullName).ToList();

                lstPupil = lsPP.Select(o => new ComboObject { key = o.PupilProfileID.ToString(), value = o.FullName }).ToList();
            }

            ViewData[LearningResultReportConstants.LIST_PUPIL] = lstPupil;
            return PartialView("_PupilCombobox");
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            if (!form.ClassID.HasValue)
            {
                throw new BusinessException("Chưa chọn lớp học");
            }
            //Fix loi ATTT
            ValidateForm(form);
            ViewBooklet = form.ViewBooklet;
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                #region Cap 2,3
                Transcripts transcript = new Transcripts();
                transcript.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                transcript.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                transcript.AcademicYearID = _globalInfo.AcademicYearID.Value;
                transcript.PupilID = form.PupilID.HasValue ? form.PupilID.Value : 0;
                transcript.ClassID = form.ClassID.HasValue ? form.ClassID.Value : 0;
                transcript.TrainingType = _globalInfo.TrainingType.GetValueOrDefault();
                transcript.currentYear = transcript.TrainingType !=  SystemParamsInFile.TRAINING_TYPE_GDTX ? form.currentYear : 0;
                transcript.paging = transcript.TrainingType != SystemParamsInFile.TRAINING_TYPE_GDTX ? form.paging : 0;
                string reportCode = "";
                bool isZip = transcript.PupilID == 0 ? true : false;

                if (!isZip)
                {
                    reportCode = transcript.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX ? SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX : SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23TT58;
                }
                else
                {
                    reportCode = transcript.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX ? SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX_ZIPFILE : SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_ZIPFILE;
                }
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                ProcessedReport processedReport = null;
                string type = JsonReportMessage.NEW;
                if (transcript.PupilID <= 0)
                {
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = TranscriptsBusiness.GetTranscripts(transcript, isZip);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        if (isZip)
                        {
                            string FolderSaveFile = string.Empty;
                            TranscriptsBusiness.DownloadZipFileTranscript(transcript, out FolderSaveFile);
                            if(FolderSaveFile == "") {
                                return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                            }
                            DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                            using (ZipFile zip = new ZipFile())
                            {
                                zip.AddDirectory(Path.Combine(FolderSaveFile));
                                using (MemoryStream output = new MemoryStream())
                                {
                                    zip.Save(output);
                                    di.Delete(true);
                                    transcript.PupilID = 0;
                                    processedReport = TranscriptsBusiness.InsertTranscriptsReport(transcript, output, isZip);
                                }

                            }
                        }
                    }
                }
                else
                {
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = TranscriptsBusiness.GetTranscripts(transcript);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = TranscriptsBusiness.CreateTranscripts(transcript);
                        processedReport = TranscriptsBusiness.InsertTranscripts(transcript, excel);
                        excel.Close();
                    }
                }
                return Json(new JsonReportMessage(processedReport, type));
                #endregion
            }
            else
            {
                #region Cap 1
                //PupilProfile pupil = PupilProfileBusiness.Find(form.PupilID);
                IDictionary<string, object> info = new Dictionary<string, object>();
                info["AppliedLevel"] = _globalInfo.AppliedLevel.GetValueOrDefault();
                info["SchoolID"] = _globalInfo.SchoolID.GetValueOrDefault();
                info["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                info["EducationLevelID"] = form.EducationLevelID;
                info["ClassID"] = form.ClassID;
                info["PupilProfileID"] = form.PupilID;
                info["fillPaging"] = form.paging == 1 ? true : false;
                string reportCode = string.Empty;
                bool isZip = form.PupilID.HasValue && form.PupilID.Value > 0 ? false : true;

                if (!isZip)
                {
                    reportCode = SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_ZIPFILE;
                }

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string type = JsonReportMessage.NEW;
                ProcessedReport processedReport = null;

                if (isZip)
                {
                    if (reportDef.IsPreprocessed == true)
                    {
                        info.Remove("PupilProfileID");
                        info["PupilProfileID"] = 0;
                        processedReport = TranscriptsBusiness.GetReportPrimaryTranscripts(info, isZip);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        string FolderSaveFile = string.Empty;
                        TranscriptsBusiness.DownloadZipFileTranscriptPrimary(info, out FolderSaveFile);
                        if (string.IsNullOrEmpty(FolderSaveFile))
                        {
                            return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                        }
                        DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                        using (ZipFile zip = new ZipFile())
                        {
                            zip.AddDirectory(Path.Combine(FolderSaveFile));
                            using (MemoryStream output = new MemoryStream())
                            {
                                zip.Save(output);
                                di.Delete(true);
                                info.Remove("PupilProfileID");
                                info["PupilProfileID"] = 0;
                                processedReport = TranscriptsBusiness.InsertReportPrimaryTranscripts(info, output, isZip);
                            }

                        }
                    }
                }
                else
                {
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = TranscriptsBusiness.GetReportPrimaryTranscripts(info);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = TranscriptsBusiness.CreateReportPrimaryTranscripts(info);
                        processedReport = TranscriptsBusiness.InsertReportPrimaryTranscripts(info, excel, false);
                        excel.Close();
                    }
                }

                return Json(new JsonReportMessage(processedReport, type));

                #endregion
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel form)
        {
            if (!form.ClassID.HasValue)
            {
                throw new BusinessException("Chưa chọn lớp học");
            }

            //Fix loi ATTT
            ValidateForm(form);

            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                #region Cap 2,3
                GlobalInfo GlobalInfo = new GlobalInfo();
                Transcripts transcript = new Transcripts();
                transcript.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                transcript.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                transcript.PupilID = form.PupilID.HasValue ? form.PupilID.Value : 0;
                transcript.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                transcript.ClassID = form.ClassID.HasValue ? form.ClassID.Value : 0;
                transcript.TrainingType = _globalInfo.TrainingType.GetValueOrDefault();
                transcript.currentYear = transcript.TrainingType != SystemParamsInFile.TRAINING_TYPE_GDTX ? form.currentYear : 0;
                transcript.paging = transcript.TrainingType != SystemParamsInFile.TRAINING_TYPE_GDTX ? form.paging : 0;
                //string reportCode = "";
                bool isZip = transcript.PupilID == 0 ? true : false;
                ProcessedReport processedReport = null;
                if (isZip)
                {
                    string FolderSaveFile = string.Empty;
                    TranscriptsBusiness.DownloadZipFileTranscript(transcript, out FolderSaveFile);
                    if (FolderSaveFile == "")
                    {
                        return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                    }
                    DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(Path.Combine(FolderSaveFile));
                        using (MemoryStream output = new MemoryStream())
                        {
                            zip.Save(output);
                            di.Delete(true);
                            transcript.PupilID = 0;
                            processedReport = TranscriptsBusiness.InsertTranscriptsReport(transcript, output, isZip);
                        }

                    }
                }
                else
                {
                    Stream excel = TranscriptsBusiness.CreateTranscripts(transcript);
                    processedReport = TranscriptsBusiness.InsertTranscripts(transcript, excel);
                    excel.Close();
                }

                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                #endregion
            }
            else
            {
                #region Cap 1
                PupilProfile pupil = PupilProfileBusiness.Find(form.PupilID);
                IDictionary<string, object> info = new Dictionary<string, object>();
                info["AppliedLevel"] = _globalInfo.AppliedLevel.GetValueOrDefault();
                info["SchoolID"] = _globalInfo.SchoolID.GetValueOrDefault();
                info["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                info["EducationLevelID"] = form.EducationLevelID;
                info["ClassID"] = form.ClassID;
                info["PupilProfileID"] = form.PupilID;
                info["fillPaging"] = form.paging == 1 ? true : false;
                bool isZip = form.PupilID.HasValue && form.PupilID.Value > 0 ? false : true;
                ProcessedReport processedReport = null;
                if (isZip)
                {
                    string FolderSaveFile = string.Empty;
                    TranscriptsBusiness.DownloadZipFileTranscriptPrimary(info, out FolderSaveFile);
                    if (string.IsNullOrEmpty(FolderSaveFile))
                    {
                        return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                    }
                    DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(Path.Combine(FolderSaveFile));
                        using (MemoryStream output = new MemoryStream())
                        {
                            zip.Save(output);
                            di.Delete(true);
                            info.Remove("PupilProfileID");
                            info["PupilProfileID"] = 0;
                            processedReport = TranscriptsBusiness.InsertReportPrimaryTranscripts(info, output, isZip);
                        }

                    }
                }
                else
                {
                    Stream excel = TranscriptsBusiness.CreateReportPrimaryTranscripts(info);
                    processedReport = TranscriptsBusiness.InsertReportPrimaryTranscripts(info, excel, false);
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                #endregion
            }
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);

            Stream excel = null;
            FileStreamResult result = null;
            if ((SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_ZIPFILE.Equals(processedReport.ReportCode) || SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX_ZIPFILE.Equals(processedReport.ReportCode)
                || (SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_ZIPFILE.Equals(processedReport.ReportCode))))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }
            result.FileDownloadName = processedReport.ReportName;
            return result;
        }

        public FileResult DownloadReportPdf(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if ((SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_ZIPFILE.Equals(processedReport.ReportCode) || SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX_ZIPFILE.Equals(processedReport.ReportCode)
                || (SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_ZIPFILE.Equals(processedReport.ReportCode))))
            {
                excel = new MemoryStream(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                //Duong dan thu muc chua file excel
                string inFolder = objFile.CreateFolder();
                //Duong dan thu muc chua file zip
                string outFolder = objFile.CreateFolder();

                string outFile = outFolder + "\\" + processedReport.ReportName;
                try
                {
                    //1. Extract file tu stream ra thu muc
                    objFile.UnzipFromStream(excel, inFolder);
                    //2. Chuyen file .xls thanh pdf
                    objFile.ConvertFileInFolderToPDF(inFolder, ViewBooklet);
                    //3 Zip thu muc PDF
                    objFile.ZipFolder(outFile, inFolder);
                    //4. Chuyen file zip sang stream
                    result = new FileStreamResult(objFile.ConvertZipFileToStream(outFile), PdfExport.CONTENT_TYPE_ZIP);
                    result.FileDownloadName = processedReport.ReportName;
                }
                finally
                {
                    objFile.DeleteFolder(inFolder);
                    objFile.DeleteFolder(outFolder);
                }
                return result;
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, ViewBooklet);
                //result = new FileStreamResult(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
                //result.FileDownloadName = objRetVal.SaveFileName;
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);

            }



        }


        private IQueryable<PupilOfClass> getPupilFromClass(int ClassID)
        {

            GlobalInfo global = new GlobalInfo();
            AcademicYear aca = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ClassID",ClassID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };
            IQueryable<PupilOfClass> lsPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, dic).Where(o => o.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS);
            return lsPOC;
        }

        public ActionResult ViewHtmlPattern()
        {
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                return PartialView("_HtmlPatternPrimary");
            }
            else
            {
                if (_globalInfo.TrainingType.HasValue && _globalInfo.TrainingType.Value == SystemParamsInFile.TRAINING_TYPE_GDTX)
                    return PartialView("_HtmlPattern");

                return PartialView("_HtmlPatternTT58");
            }
        }

        private void ValidateForm(SearchViewModel form)
        {
            ///Fix loi ATTT
            int academicYearId = _globalInfo.AcademicYearID.Value;
            if (form.ClassID > 0)
            {
                bool exists = ClassProfileBusiness.All.Any(c => c.ClassProfileID == form.ClassID && c.AcademicYearID == academicYearId);
                if (!exists)
                {
                    throw new BusinessException("Thông tin lớp học không hợp lệ");
                }
            }
            if (form.PupilID > 0)
            {
                bool exists = PupilOfClassBusiness.All.Any(c => c.ClassID == form.ClassID && c.AcademicYearID == academicYearId && c.PupilID == form.PupilID);
                if (!exists)
                {
                    throw new BusinessException("Thông tin học sinh không hợp lệ");
                }
            }

        }
    }
}