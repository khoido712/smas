﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Web.Utils;
using System.IO;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using System.Configuration;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class RepeatPupilReportController: BaseController
    {
                //
        // GET: /PupilRepeatersArea/PupilRepeaters/
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IProcessedCellDataBusiness ProcessedCellDataBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness;
        private readonly IReviewBookPupilBusiness ReviewBookPupilBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;

        public RepeatPupilReportController(IPupilRankingBusiness PupilRankingBusiness, IProcessedCellDataBusiness ProcessedCellDataBusiness, 
            IProcessedReportBusiness ProcessedReportBusiness, IEducationLevelBusiness EducationLevelBusiness, 
            IClassProfileBusiness ClassProfileBusiness, IAcademicYearBusiness AcademicYearBusiness,
            ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness, IReviewBookPupilBusiness ReviewBookPupilBusiness,
            ISummedUpRecordBusiness SummedUpRecordBusiness, ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness,
            ISubjectCatBusiness SubjectCatBusiness)
        {
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.ProcessedCellDataBusiness = ProcessedCellDataBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.TeacherNoteBookSemesterBusiness = TeacherNoteBookSemesterBusiness;
            this.ReviewBookPupilBusiness = ReviewBookPupilBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
        }

        public ActionResult Index()
        {
            SetViewDefault();
            return View();
        }

        private void SetViewDefault()
        {
            //Khoi
            List<EducationLevel> listEducationLevel = new List<EducationLevel>(_globalInfo.EducationLevels);
            listEducationLevel.RemoveAt(listEducationLevel.Count - 1);
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(listEducationLevel, "EducationLevelID", "Resolution");

            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(new List<ClassProfile>());
        }

        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID !=null)
            {
                List<ClassProfile> lsCP = GetClassFromEducationLevel(EducationLevelID.Value).OrderBy(o => o.DisplayName).ToList();
                return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        private IQueryable<ClassProfile> GetClassFromEducationLevel(int EducationLevel)
        {
            return ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"EducationLevelID",EducationLevel}
            });
        }

        public JsonResult ExportExcel(SearchViewModel model)
        {
            string ReportCode = GeneralReportCode(model.EducationLevelID.HasValue?model.EducationLevelID.Value:0, model.ClassName);
            PupilRanking PupilRainkingObj = new PupilRanking();
            PupilRainkingObj.SchoolID = _globalInfo.SchoolID.Value;
            PupilRainkingObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
            PupilRainkingObj.Semester = _globalInfo.Semester.Value;
            PupilRainkingObj.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
            PupilRainkingObj.ClassID = model.ClassID.HasValue ? model.ClassID.Value : 0;
            //er.Year = ProcessedCellDataBusiness.GetYear(_global.AcademicYearID.Value).Value;
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = PupilRankingBusiness.GetProcessedReport(PupilRainkingObj, ReportCode);

            if (processedReport != null)
            {
                type = JsonReportMessage.OLD;
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = CreateDataExcel(model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0, model.ClassID.HasValue ? model.ClassID.Value : 0);
                processedReport = PupilRankingBusiness.InsertProcessReport(PupilRainkingObj, excel, ReportCode);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        public JsonResult GetNewReport(SearchViewModel model)
        {
            //string CodeDefinition = GetCodeDefinition();
            string ReportCode = GeneralReportCode(model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0, model.ClassName);//lay ten file excel de lam reportcode      
            PupilRanking PupilRainkingObj = new PupilRanking();
            PupilRainkingObj.SchoolID = _globalInfo.SchoolID.Value;
            PupilRainkingObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
            PupilRainkingObj.Semester = _globalInfo.Semester.Value;
            PupilRainkingObj.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;

            ProcessedReport processedReport = PupilRankingBusiness.GetProcessedReport(PupilRainkingObj, ReportCode);
            Stream excel = CreateDataExcel(model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0, model.ClassID.HasValue ? model.ClassID.Value : 0);
            processedReport = PupilRankingBusiness.InsertProcessReport(PupilRainkingObj, excel, ReportCode);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport, SearchViewModel model)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"SchoolID", _globalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
               Session["ReportCode"].ToString()
            };

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName + ".xls";

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport, SearchViewModel model)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"SchoolID", _globalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
               Session["ReportCode"].ToString()
            };

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        //Viethd4: Tạo hàm mới xuất cả cho lớp VNEN
        public Stream CreateDataExcel(int EducationLevel, int ClassID)
        {
            string templateName = "HS_TH_DSHocSinhOLaiLop_Khoi1";
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + templateName + ".xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            IVTWorksheet sheet3 = oBook.GetSheet(3);
            IVTWorksheet sheet4 = oBook.GetSheet(4);
            List<int> listEducationLevel = _globalInfo.EducationLevels.Select(o => o.EducationLevelID).ToList();
            listEducationLevel.RemoveAt(listEducationLevel.Count - 1);
            if (EducationLevel != 0)
            {
                listEducationLevel = listEducationLevel.Where(o => o == EducationLevel).ToList();
            }

            List<ClassProfile> lstCpAll = GetClassFromEducationLevel(0).ToList();

            List<PupilRepeaterBO> ListPupilRepeaterAll = PupilRankingBusiness.GetListPupilRepeater(_globalInfo.AppliedLevel.Value, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, EducationLevel, ClassID, true);

            //cac hoc sinh danh gia chua hoan thanh VNEN
            List<PupilRepeaterBO> lstPupilRepeaterVnenAll = ReviewBookPupilBusiness.GetListRepeatPupilOfSchool(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value);
            IEnumerable<int> lstVnenRepeatPupilId = lstPupilRepeaterVnenAll.Select(o => o.PupilID);
            
            

            //Danh sach diem cua tat ca hoc sinh
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<TeacherNoteBookSemester> lstTnbsAll = TeacherNoteBookSemesterBusiness.All
                    .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.PartitionID == partition
                    && o.SchoolID == _globalInfo.SchoolID
                    && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND
                    && lstVnenRepeatPupilId.Contains(o.PupilID)
                    && (o.Rate == 2
                    || (o.PERIODIC_SCORE_END.HasValue && o.PERIODIC_SCORE_END < 5)
                    || (o.PERIODIC_SCORE_END_JUDGLE == "CĐ"))).ToList();


            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_ALL;

            AcademicYear AcademicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(AcademicYearObj);
                
            List<SummedUpRecordBO> lstSummedUpRecordAll;
            if (isMovedHistory)
            {
                lstSummedUpRecordAll = SummedUpRecordHistoryBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                    .Where(o => o.PeriodID == null)
                    .Select(o => new SummedUpRecordBO()
                    {
                        PupilID = o.PupilID,
                        SubjectID = o.SubjectID,
                        SummedUpMark = o.SummedUpMark,
                        JudgementResult = o.JudgementResult,
                        Semester = o.Semester,
                        ClassID = o.ClassID
                    }).ToList();

            }
            else
            {
                lstSummedUpRecordAll = SummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                    .Where(o => o.PeriodID == null)
                    .Select(o => new SummedUpRecordBO()
                    {
                        PupilID = o.PupilID,
                        SubjectID = o.SubjectID,
                        SummedUpMark = o.SummedUpMark,
                        JudgementResult = o.JudgementResult,
                        Semester = o.Semester,
                        ClassID = o.ClassID
                    }).ToList();
            }

            lstSummedUpRecordAll = lstSummedUpRecordAll.Where(o => lstVnenRepeatPupilId.Contains(o.PupilID) && (o.SummedUpMark.HasValue && o.SummedUpMark < 5) || o.JudgementResult == "CĐ").ToList();

            
            int Year = AcademicYearObj.Year;
            IVTWorksheet sheet;
            for (int i = 0; i < listEducationLevel.Count; i++)
            {
                int curEduLevel = listEducationLevel[i];
                List<ClassProfile> lsCP = lstCpAll.Where(o => o.EducationLevelID == curEduLevel).OrderBy(o => o.DisplayName).ToList();
                if (ClassID != 0)
                {
                    lsCP = lsCP.Where(o => o.ClassProfileID == ClassID).ToList();
                }
                if (lsCP.Where(o => o.IsVnenClass != true).Count() == lsCP.Count)
                {

                    int StartRow = 7;
                    if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                    {
                        sheet = oBook.CopySheetToLast(sheet1);
                    }
                    else
                    {
                        sheet = oBook.CopySheetToLast(sheet2);
                    }

                    
                    List<PupilRepeaterBO> listPupil = ListPupilRepeaterAll.Where(p => p.EducationLevelID == curEduLevel)
                        .OrderBy(o => o.OrderClass.HasValue ? o.OrderClass : 0).ThenBy(o => o.ClassName)
                        .ThenBy(u => u.OrderID).ThenBy(o => o.PupilShortName).ThenBy(o => o.PupilFullName).ToList();

                    if (listPupil.Count > 0 && listPupil != null)
                    {
                        for (int j = 0; j < listPupil.Count(); j++)
                        {

                            sheet.SetCellValue("A" + (StartRow + j), j + 1);
                            sheet.SetCellValue("B" + (StartRow + j), listPupil[j].PupilFullName);
                            sheet.SetCellValue("C" + (StartRow + j), listPupil[j].ClassName);
                            sheet.SetCellValue("D" + (StartRow + j), GetCapacityByID(listPupil[j].CapacityID));
                            sheet.SetCellValue("E" + (StartRow + j), GetConductByID(listPupil[j].ConductID));

                            // dinh dang file excel
                            if (((j + 1)) % 5 == 0)
                            {
                                sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                                sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                                sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                            }
                            else
                            {
                                sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                                sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                                sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            }
                            if ((j + 1) == listPupil.Count)
                            {
                                sheet.GetRange(listPupil.Count + StartRow - 1, 1, listPupil.Count + StartRow - 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                                sheet.GetRange(listPupil.Count + StartRow - 1, 1, listPupil.Count + StartRow - 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            }
                            sheet.GetRange("A" + (StartRow + j), "A" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.GetRange("B" + (StartRow + j), "B" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.GetRange("C" + (StartRow + j), "C" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.GetRange("D" + (StartRow + j), "D" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.GetRange("E" + (StartRow + j), "E" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        }
                    }

                    //mer title
                    sheet.MergeColumn(1, 2, 2);
                    sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
                    sheet.SetCellValue("A3", Res.Get("Pupil_Repeater_List_Education") + " " + curEduLevel + Res.Get("Pupil_Repeater_AcademicYear") + " " + Year + " - " + (Year + 1));
                    sheet.Name = "Khoi_" + curEduLevel;
                }
                else if (lsCP.Where(o => o.IsVnenClass == true).Count() == lsCP.Count)
                {
                    sheet = oBook.CopySheetToLast(sheet3,"Z1000");

                    //Lay danh sach mon de lam tieu de
                    IEnumerable<int> lstClassId=lsCP.Select(o=>o.ClassProfileID);
                    List<TeacherNoteBookSemester> lstTnbsEdu = lstTnbsAll.Where(o => lstClassId.Contains(o.ClassID)).ToList();
                    List<SummedUpRecordBO> lstSummedUpRecordEdu = lstSummedUpRecordAll.Where(o => lstClassId.Contains(o.ClassID)).ToList();

                    List<int> lstSubjectId = lstTnbsEdu.Select(o => o.SubjectID).ToList();
                    lstSubjectId.AddRange(lstSummedUpRecordEdu.Select(o => o.SubjectID.GetValueOrDefault()).ToList());
                    lstSubjectId = lstSubjectId.Distinct().ToList();

                    List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All
                        .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel
                        && o.IsActive == true
                        && lstSubjectId.Contains(o.SubjectCatID))
                        .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();

                    //Fill tieu de
                    
                    int subjectRow = 6;
                    int startSubectCol = 4;
                    int subjectCol = startSubectCol;

                    for (int iSc = 0; iSc < lstSubjectCat.Count; iSc++)
                    {
                        SubjectCat sc = lstSubjectCat[iSc];

                        sheet.SetCellValue(subjectRow, subjectCol, sc.DisplayName);
                        sheet.MergeRow(subjectRow, subjectCol, subjectCol + 1);
                        sheet.SetCellValue(subjectRow + 1, subjectCol, "Điểm KTCK");
                        sheet.SetCellValue(subjectRow + 1, subjectCol + 1, "Đánh giá");
                        sheet.SetColumnWidth(subjectCol, 6);
                        sheet.SetColumnWidth(subjectCol + 1, 6);
                        subjectCol = subjectCol + 2;
                       
                    }

                    if(lstSubjectCat.Count>0)
                    {
                        sheet.GetRange(subjectRow - 1, startSubectCol, subjectRow - 1, subjectCol - 1).Merge();
                        sheet.SetCellValue(subjectRow - 1, startSubectCol, "Kết quả môn học");
                    }

                    //Add cac cot con lai
                    //Nang luc
                    sheet.SetCellValue(subjectRow, subjectCol, "Năng lực");
                    sheet.MergeRow(subjectRow, subjectCol, subjectCol + 1);
                    sheet.SetCellValue(subjectRow + 1, subjectCol, "Đánh giá");
                    sheet.SetCellValue(subjectRow + 1, subjectCol + 1, "Rèn luyện bổ sung");
                    sheet.SetColumnWidth(subjectCol, 8);
                    sheet.SetColumnWidth(subjectCol + 1, 8);
                    sheet.GetRange(subjectRow - 1, subjectCol, subjectRow - 1, subjectCol + 3).Merge();
                    sheet.SetCellValue(subjectRow - 1, subjectCol, "Năng lực, phẩm chất");
                    

                    //Phẩm chất
                    sheet.SetCellValue(subjectRow, subjectCol + 2, "Phẩm chất");
                    sheet.MergeRow(subjectRow, subjectCol + 2, subjectCol + 3);
                    sheet.SetCellValue(subjectRow + 1, subjectCol + 2, "Đánh giá");
                    sheet.SetCellValue(subjectRow + 1, subjectCol + 3, "Rèn luyện bổ sung");
                    sheet.SetColumnWidth(subjectCol + 2, 8);
                    sheet.SetColumnWidth(subjectCol + 3, 8);
                    
                    sheet.SetCellValue(subjectRow - 1, subjectCol + 4, "Đánh giá bổ sung");
                    sheet.SetColumnWidth(subjectCol + 4, 9);
                    sheet.GetRange(subjectRow - 1, subjectCol + 4, subjectRow + 1, subjectCol + 4).Merge();

                    sheet.SetCellValue(subjectRow - 1, subjectCol + 5, "Ghi chú");
                    sheet.SetColumnWidth(subjectCol + 5, 19);
                    sheet.GetRange(subjectRow - 1, subjectCol + 5, subjectRow + 1, subjectCol + 5).Merge();
                    



                    //Lay danh sach hoc sinh o lai cua khoi
                    List<PupilRepeaterBO> lstRepeaterEdu = lstPupilRepeaterVnenAll.Where(o => lstClassId.Contains(o.ClassID))
                        .OrderBy(o => o.OrderClass.HasValue ? o.OrderClass : 0).ThenBy(o => o.ClassName)
                        .ThenBy(u => u.OrderID).ThenBy(o => o.PupilShortName).ThenBy(o => o.PupilFullName).ToList();

                    //Fill du lieu
                    int startRow = 8;
                    int lastRow = startRow + lstRepeaterEdu.Count - 1;
                    int curRow = startRow;
                    int startColumn = 1;
                    int curColumn = startColumn;

                    for (int iRepeat = 0; iRepeat < lstRepeaterEdu.Count; iRepeat++)
                    {
                        PupilRepeaterBO repeat = lstRepeaterEdu[iRepeat];

                        //STT
                        sheet.SetCellValue(curRow, curColumn, iRepeat + 1);
                        curColumn++;
                       
                        //Ho ten
                        sheet.SetCellValue(curRow, curColumn, repeat.PupilFullName);
                        curColumn++;

                        //Lop
                        sheet.SetCellValue(curRow, curColumn, repeat.ClassName);
                        curColumn++;


                        //Fill diem cac mon
                        for (int iSc = 0; iSc < lstSubjectCat.Count; iSc++)
                        {
                            SubjectCat sc = lstSubjectCat[iSc];
                            TeacherNoteBookSemester tnbs = lstTnbsEdu.Where(o => o.PupilID == repeat.PupilID && o.SubjectID==sc.SubjectCatID).FirstOrDefault();
                            SummedUpRecordBO sur = lstSummedUpRecordEdu.Where(o => o.PupilID == repeat.PupilID && o.SubjectID == sc.SubjectCatID).FirstOrDefault();
                            if (tnbs != null)
                            {
                                sheet.SetCellValue(curRow, curColumn, tnbs.PERIODIC_SCORE_END.HasValue ? (tnbs.PERIODIC_SCORE_END != 0 && tnbs.PERIODIC_SCORE_END != 10 ? String.Format("{0:0.0}", tnbs.PERIODIC_SCORE_END) : String.Format("{0:0}", tnbs.PERIODIC_SCORE_END)) : tnbs.PERIODIC_SCORE_END_JUDGLE);
                                sheet.SetCellValue(curRow, curColumn + 1, tnbs.Rate == 1 ? "HT" : (tnbs.Rate == 2 ? "CHT" : String.Empty));
                            }
                            else if(sur!=null)
                            {
                                sheet.SetCellValue(curRow, curColumn, sur.SummedUpMark.HasValue ? (sur.SummedUpMark != 0 && sur.SummedUpMark != 10 ? String.Format("{0:0.0}", sur.SummedUpMark) : String.Format("{0:0}", sur.SummedUpMark)) : sur.JudgementResult);
                            }
                            curColumn += 2;
                        }

                        //Nang luc
                        sheet.SetCellValue(curRow, curColumn, GetCapacityVnenByID(repeat.Capacity));
                        sheet.SetCellValue(curRow, curColumn + 1, GetCapacityVnenByID(repeat.CapacityAdd));
                        curColumn += 2;

                        //Pham chat
                        sheet.SetCellValue(curRow, curColumn, GetCapacityVnenByID(repeat.Quality));
                        sheet.SetCellValue(curRow, curColumn + 1, GetCapacityVnenByID(repeat.QualityAdd));
                        curColumn += 2;

                        //Danh gia bo sung
                        sheet.SetCellValue(curRow, curColumn, GetCapacityVnenByID(repeat.RateAdd, true));
                        curColumn++;

                        curRow++;
                        curColumn = startColumn;
                    }

                    //format
                    int lastCol = 9 + lstSubjectCat.Count * 2;
                    IVTRange headerRange = sheet.GetRange(5, 1, 7, lastCol);
                    headerRange.SetBorder(VTBorderStyle.Solid,VTBorderIndex.All);
                    headerRange.FillColor(System.Drawing.Color.LightGray);
                    headerRange.WrapText();

                    sheet.GetRange(startRow, 1, lastRow, lastCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                    //Merge header
                    sheet.GetRange(3, 1, 3, lastCol).Merge();

                    sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
                    sheet.SetCellValue("A3", Res.Get("Pupil_Repeater_List_Education") + " " + curEduLevel + Res.Get("Pupil_Repeater_AcademicYear") + " " + Year + " - " + (Year + 1));
                    sheet.Name = "Khoi_" + curEduLevel;
                }
                //Vua co lop vnen, vua co lop thuong
                else
                {
                    sheet = oBook.CopySheetToLast(sheet4, "Z1000");
                    //Lay danh sach lop Khong phai VNEN
                    IEnumerable<int> lstCpIdNotVnen = lsCP.Where(o => o.IsVnenClass == null || o.IsVnenClass == false).Select(o=>o.ClassProfileID);
                    //Danh sach cac lop VNEN
                    IEnumerable<int> lstCpIdVnen = lsCP.Where(o => o.IsVnenClass == true).Select(o => o.ClassProfileID);

                    List<PupilRepeaterBO> lstRepeaterEdu = ListPupilRepeaterAll.Where(o => lstCpIdNotVnen.Contains(o.ClassID)).ToList();
                    List<PupilRepeaterBO> lstRepeaterVnenEdu = lstPupilRepeaterVnenAll.Where(o => lstCpIdVnen.Contains(o.ClassID)).ToList();

                    List<PupilRepeaterBO> lstRepeatAllEdu = lstRepeaterEdu.Union(lstRepeaterVnenEdu)
                        .OrderBy(o => o.OrderClass.HasValue ? o.OrderClass : 0).ThenBy(o => o.ClassName)
                        .ThenBy(u => u.OrderID).ThenBy(o => o.PupilShortName).ThenBy(o => o.PupilFullName).ToList();

                    //Fill data
                    //Fill du lieu
                    int startRow = 7;
                    int curRow = startRow;
                    int startColumn = 1;
                    int curColumn = startColumn;

                    for (int iRepeat = 0; iRepeat < lstRepeatAllEdu.Count; iRepeat++)
                    {
                        PupilRepeaterBO repeat = lstRepeatAllEdu[iRepeat];

                        //STT
                        sheet.SetCellValue(curRow, curColumn, iRepeat + 1);
                        curColumn++;
                     
                        //Ho ten
                        sheet.SetCellValue(curRow, curColumn, repeat.PupilFullName);
                        curColumn++;

                        //Lop 
                        sheet.SetCellValue(curRow, curColumn, repeat.ClassName);
                        curColumn++;

                       //Hoc luc
                        sheet.SetCellValue(curRow, curColumn, GetCapacityByID(repeat.CapacityID));
                        curColumn++;

                        //Hanh kiem
                        sheet.SetCellValue(curRow, curColumn, GetConductByID(repeat.ConductID));
                        curColumn++;

                        //Danh gia cuoi nam
                        sheet.SetCellValue(curRow, curColumn, GetCapacityVnenByID(repeat.RateEndYear, true));
                        curColumn++;

                        //Danh gia bo sung
                        sheet.SetCellValue(curRow, curColumn, GetCapacityVnenByID(repeat.RateAdd, true));
                        curColumn++;

                        curRow++;
                        curColumn = startColumn;
                    }

                    //format
                    IVTRange headerRange = sheet.GetRange(5, 1, 6, 8);
                    headerRange.SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                    headerRange.FillColor(System.Drawing.Color.LightGray);

                    sheet.GetRange(startRow, 1, curRow - 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                   
                    sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
                    sheet.SetCellValue("A3", Res.Get("Pupil_Repeater_List_Education") + " " + curEduLevel + Res.Get("Pupil_Repeater_AcademicYear") + " " + Year + " - " + (Year + 1));
                    sheet.Name = "Khoi_" + curEduLevel;
                }

            }

            //Xoa cac sheet
            sheet1.Delete();
            sheet2.Delete();
            sheet3.Delete();
            sheet4.Delete();

            return oBook.ToStream();
        }
        //viethd4: Bỏ hàm cũ
        //public Stream CreateDataExcel(int EducationLevel, int ClassID)
        //{
        //    //string ReportCode = "";// report code la tenfile excel
        //    string TemplateName = GetTemplateExcel();// ten template
        //    int StartRow = 7;
        //    // ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
        //    string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "THI" + "/" + TemplateName + ".xls";
        //    IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

        //    List<PupilRepeaterBO> ListPupilRepeater = PupilRankingBusiness.GetListPupilRepeater(_globalInfo.AppliedLevel.Value, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, EducationLevel, ClassID, true);

        //    if (ListPupilRepeater.Count > 0 && ListPupilRepeater != null)
        //    {
        //        List<int> ListEducationLevelID = ListPupilRepeater.Select(p => p.EducationLevelID).Distinct().OrderBy(p => p).ToList();
        //        if (ListEducationLevelID.Count > 0 && ListEducationLevelID != null)
        //        {
        //            for (int i = 0; i < ListEducationLevelID.Count; i++)
        //            {
        //                IVTWorksheet sheet = oBook.GetSheet(i + 1);

        //                AcademicYear AcademicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
        //                int Year = AcademicYearObj.Year;
        //                int _educationlevelID = ListEducationLevelID[i];
        //                List<PupilRepeaterBO> listPupil = ListPupilRepeater.Where(p => p.EducationLevelID == _educationlevelID).OrderBy(p => p.ClassName).ThenBy(p => p.PupilShortName).ToList();
        //                if (listPupil.Count > 0 && listPupil != null)
        //                {
        //                    for (int j = 0; j < listPupil.Count(); j++)
        //                    {

        //                        sheet.SetCellValue("A" + (StartRow + j), j + 1);
        //                        sheet.SetCellValue("B" + (StartRow + j), listPupil[j].PupilFullName);
        //                        sheet.SetCellValue("C" + (StartRow + j), listPupil[j].ClassName);
        //                        sheet.SetCellValue("D" + (StartRow + j), GetCapacityByID(listPupil[j].CapacityID));
        //                        sheet.SetCellValue("E" + (StartRow + j), GetConductByID(listPupil[j].ConductID));

        //                        // dinh dang file excel
        //                        if (((j + 1)) % 5 == 0)
        //                        {
        //                            sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
        //                            sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
        //                            sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
        //                        }
        //                        else
        //                        {
        //                            sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
        //                            sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
        //                            sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
        //                        }
        //                        if ((j + 1) == listPupil.Count)
        //                        {
        //                            sheet.GetRange(listPupil.Count + StartRow - 1, 1, listPupil.Count + StartRow - 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
        //                            sheet.GetRange(listPupil.Count + StartRow - 1, 1, listPupil.Count + StartRow - 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
        //                        }
        //                        sheet.GetRange("A" + (StartRow + j), "A" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
        //                        sheet.GetRange("B" + (StartRow + j), "B" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
        //                        sheet.GetRange("C" + (StartRow + j), "C" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
        //                        sheet.GetRange("D" + (StartRow + j), "D" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
        //                        sheet.GetRange("E" + (StartRow + j), "E" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
        //                    }
        //                }

        //                //mer title
        //                sheet.MergeColumn(1, 2, 2);
        //                sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
        //                sheet.SetCellValue("A3", Res.Get("Pupil_Repeater_List_Education") + " " + _educationlevelID + Res.Get("Pupil_Repeater_AcademicYear") + " " + Year + " - " + (Year + 1));
        //                sheet.Name = "Khoi_" + _educationlevelID;
        //            }

        //            #region Delete sheet
        //            int sheetNumber = LearningResultReportConstants.SHEET_NUMBER_PRIMARY;
        //            if (_globalInfo.AppliedLevel >= SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY)
        //            {
        //                sheetNumber = LearningResultReportConstants.SHEET_NUMBER_SECONDARY;
        //            }
        //            for (int i = sheetNumber; i > ListEducationLevelID.Count; i--)
        //            {
        //                oBook.GetSheet(i).Delete();
        //            }
        //            #endregion

        //        }
        //    }
        //    else
        //    {
        //        throw new BusinessException(Res.Get("Pupil_Repeater_Empty"));
        //    }
        //    return oBook.ToStream();
        //}
        public string GetCapacityVnenByID(int? id, bool rate = false)
        {
            string str=String.Empty;
            if (rate == false)
            {
                if (id == 1)
                {
                    str = "Đ";
                }

                else if(id == 2)
                {
                    str = "CĐ";
                }
            }
            else
            {
                if (id == 1)
                {
                    str = "HT";
                }

                else if (id == 2)
                {
                    str = "CHT";
                }
            }
            return str;
        }
        private string GetCapacityByID(int CapacityID)
        {
            string Capacity = string.Empty;
            if (CapacityID == 1)
            {
                Capacity = "Giỏi";
            }
            else if (CapacityID == 2)
            {
                Capacity = "Khá";
            }
            else if (CapacityID == 3)
            {
                Capacity = "TB";
            }
            else if (CapacityID == 4)
            {
                Capacity = "Yếu";
            }
            else if (CapacityID == 5)
            {
                Capacity = "Kém";
            }
            else if (CapacityID == 6)
            {
                Capacity = "A+";
            }
            else if (CapacityID == 7)
            {
                Capacity = "A";
            }
            else if (CapacityID == 8)
            {
                Capacity = "B";
            }
            return Capacity;
        }

        private string GetConductByID(int ConductID)
        {
            string Conduct = string.Empty;
            if (ConductID == 1)
            {
                Conduct = "Đ";
            }
            else if (ConductID == 2)
            {
                Conduct = "CĐ";
            }
            else if (ConductID == 3 || ConductID == 7)
            {
                Conduct = "Tốt";
            }
            else if (ConductID == 4 || ConductID == 8)
            {
                Conduct = "Khá";
            }
            else if (ConductID == 5 || ConductID == 9)
            {
                Conduct = "TB";
            }
            else if (ConductID == 6 || ConductID == 10)
            {
                Conduct = "Yếu";
            }
            return Conduct;
        }

        private string GetTemplateExcel()
        {
            string template = string.Empty;
            if (_globalInfo.AppliedLevel == LearningResultReportConstants.Primary)// cap1
     
            {
                template = "HS_TH_DSHocSinhOLaiLop_Khoi1";
            }
            else
            {
                template = "HS_THPT_DSHocSinhOLaiLop_Khoi10";
            }

            return template;
        }

        private string GeneralReportCode(int EducationLevel, string ClassName)
        {
            //HS_[TH/THCS/THPT]_ DSHocSinhOLaiLop_[Ten khoi] 
            int Grade = _globalInfo.AppliedLevel.Value;
            string ReportCode = string.Empty;
            if (EducationLevel == 6 || EducationLevel == 7 || EducationLevel == 8)
            {
                ReportCode = "HS_THCS_DSHocSinhOLaiLop_Khoi" + EducationLevel + "_" + Utils.Utils.StripVNSign(ClassName);
            }
            else if (EducationLevel == 1 || EducationLevel == 2 || EducationLevel == 3 || EducationLevel == 4)
            {
                ReportCode = "HS_TH_DSHocSinhOLaiLop_Khoi" + EducationLevel + "_" + Utils.Utils.StripVNSign(ClassName);
            }
            else if (EducationLevel == 10 || EducationLevel == 11)
            {
                ReportCode = "HS_THPT_DSHocSinhOLaiLop_Khoi" + EducationLevel + "_" + Utils.Utils.StripVNSign(ClassName);
            }
            else if (Grade == LearningResultReportConstants.Primary)
            {
                ReportCode = "HS_TH_DSHocSinhOLaiLop";
            }
            else if (Grade == LearningResultReportConstants.Secondary)
            {
                ReportCode = "HS_THCS_DSHocSinhOLaiLop";
            }
            else if (Grade == LearningResultReportConstants.Tertiary)
            {
                ReportCode = "HS_THPT_DSHocSinhOLaiLop";
            }
            Session["ReportCode"] = ReportCode;
            return ReportCode;
        }
    }
}