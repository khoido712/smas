﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;


namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PointNoteReportController:BaseController
    {
        IClassProfileBusiness ClassProfileBusiness;
        IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        ITranscriptsByPeriodBusiness TranscriptsByPeriodBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IPointReportBusiness PointReportBusiness;
        IMarkRecordBusiness MarkRecordBusiness;
        IMarkTypeBusiness MarkTypeBusiness;
        IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        IJudgeRecordBusiness JudgeRecordBusiness;
        IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness;
        ISummedUpRecordBusiness SummedUpRecordBusiness;
        ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        IPupilOfClassBusiness PupilOfClassBusiness;
        IClassSubjectBusiness ClassSubjectBusiness;
       
        public PointNoteReportController(IClassProfileBusiness classProfileBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, ITranscriptsByPeriodBusiness TranscriptsByPeriodBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IPointReportBusiness PointReportBusiness, IMarkRecordBusiness MarkRecordBusiness,
            IMarkTypeBusiness MarkTypeBusiness, IMarkRecordHistoryBusiness MarkRecordHistoryBusiness, IJudgeRecordBusiness JudgeRecordBusiness,
            IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness, ISummedUpRecordBusiness SummedUpRecordBusiness,
            ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IClassSubjectBusiness classSubjectBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.TranscriptsByPeriodBusiness = TranscriptsByPeriodBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.PointReportBusiness = PointReportBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.MarkRecordHistoryBusiness = MarkRecordHistoryBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.JudgeRecordHistoryBusiness = JudgeRecordHistoryBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
        }
        public ActionResult Index()
        {
            // Mau bao cao
            List<ReportPatternViewModel> lstReportPatternVM = new List<ReportPatternViewModel>();
            lstReportPatternVM.Add(new ReportPatternViewModel() { ReportPatternID = 1, ReportPatternName = "Mẫu số 1" });
            lstReportPatternVM.Add(new ReportPatternViewModel() { ReportPatternID = 2, ReportPatternName = "Mẫu số 2" });
            ViewData[LearningResultReportConstants.LIST_REPORT_PATTERN] = new SelectList(lstReportPatternVM, "ReportPatternID", "ReportPatternName", 1);
            
            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            //Lop

            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevel.EducationLevelID,null,
                                                                        _globalInfo.UserAccountID,_globalInfo.IsSuperVisingDeptRole,_globalInfo.IsSubSuperVisingDeptRole);
                                                                
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // Hoc ky
            List<ComboObject> lstSemester = CommonList.Semester();

            ComboObject defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow >= FirstStarDate && datenow <= FirstEndDate)
            {
                defaultSemester = lstSemester[0];
            }
            else 
            {
                defaultSemester = lstSemester[1];
            }
           
            //Dot
            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester.key);

            List<PeriodDeclaration> lstPeriodCommon = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "Semester", Convert.ToInt32(defaultSemester.key) } })
                                                                                .OrderByDescending(o => o.EndDate).ToList();

            PeriodDeclaration defaultPeriod = lstPeriodCommon.Where(o => o.FromDate <= datenow && datenow <= o.EndDate).FirstOrDefault();

            if (defaultPeriod != null)
            {
                ViewData[LearningResultReportConstants.LIST_PERIOD] = new SelectList(lstPeriodCommon, "PeriodDeclarationID", "Resolution", defaultPeriod.PeriodDeclarationID);
            }
            else
            {
                ViewData[LearningResultReportConstants.LIST_PERIOD] = new SelectList(lstPeriodCommon, "PeriodDeclarationID", "Resolution");
            }
            

            return View();
        }

        public JsonResult AjaxLoadPeriod(int semester)
        {
            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "Semester", Convert.ToInt32(semester) } })
                                                                         .OrderByDescending(o=>o.EndDate).ToList();

            PeriodDeclaration defaultPeriod = lstPeriod.Where(o => o.FromDate <= datenow && datenow <= o.EndDate).FirstOrDefault();

            if (defaultPeriod != null)
            {
                return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", defaultPeriod.PeriodDeclarationID));
            }
            else
            {
                return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution"));
            }
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevelID,null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReportByPeriod(SearchViewModel form)
        {
            // Neu khong co dot bao loi
            if (form.PeriodID == null)
            {
                throw new BusinessException("Common_Null_PeriodID");
            }

            PointReportBO pointReportBO = new PointReportBO();
            pointReportBO.Semester = form.SemesterID;
            pointReportBO.PeriodDeclarationID = form.PeriodID.Value;
            pointReportBO.EducationLevelID = form.EducationLevelID.Value;
            pointReportBO.ClassID = form.ClassID.GetValueOrDefault();
            pointReportBO.PaperSize = form.PaperSize;
            pointReportBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            pointReportBO.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            pointReportBO.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            pointReportBO.UserAccountID = _globalInfo.UserAccountID;
            pointReportBO.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
            pointReportBO.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
            ReportDefinition reportDef = PointReportBusiness.GetReportDefinitionOfPeriodReport(pointReportBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PointReportBusiness.GetPointReportByPeriod(pointReportBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = PointReportBusiness.CreatePointReportByPeriod(pointReportBO);
                processedReport = PointReportBusiness.InsertPointReportByPeriod(pointReportBO, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportByPeriod(SearchViewModel form)
        {
            // Neu khong co dot bao loi
            if (form.PeriodID == null)
            {
                throw new BusinessException("Common_Null_PeriodID");
            }

            PointReportBO pointReportBO = new PointReportBO();

            pointReportBO.Semester = form.SemesterID;
            pointReportBO.PeriodDeclarationID = form.PeriodID.Value;
            pointReportBO.EducationLevelID = form.EducationLevelID.Value;
            pointReportBO.ClassID = form.ClassID.GetValueOrDefault();
            pointReportBO.PaperSize = form.PaperSize;
            pointReportBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            pointReportBO.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            pointReportBO.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            pointReportBO.UserAccountID = _globalInfo.UserAccountID;
            pointReportBO.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
            pointReportBO.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
            Stream excel = PointReportBusiness.CreatePointReportByPeriod(pointReportBO);
            ProcessedReport processedReport = PointReportBusiness.InsertPointReportByPeriod(pointReportBO, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetReportBySemester(SearchViewModel form)
        {
            PointReportBO pointReportBO = new PointReportBO();
            pointReportBO.ReportPatternID = form.ReportPatternID;
            pointReportBO.Semester = form.SemesterID;
            pointReportBO.EducationLevelID = form.EducationLevelID.Value;
            pointReportBO.ClassID = form.ClassID.GetValueOrDefault();
            pointReportBO.PaperSize = form.PaperSize;
            pointReportBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            pointReportBO.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            pointReportBO.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            pointReportBO.UserAccountID = _globalInfo.UserAccountID;
            ReportDefinition reportDef = PointReportBusiness.GetReportDefinitionOfSemesterReport(pointReportBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PointReportBusiness.GetPointReportBySemester(pointReportBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                List<MarkRecordBO> lstMarkRecord = new List<MarkRecordBO>() ;
                List<JudgeRecordBO> lstJudgeRecord = new List<JudgeRecordBO>();
                List<SummedUpRecordBO> lstSummedUpRecord = new List<SummedUpRecordBO>();
                AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
                if (isMovedHistory)
                {
                    if (form.ReportPatternID == 1) // theo mẫu cũ
                    {
                        //Lấy danh sách thông tin điểm môn tính điểm
                        lstMarkRecord = this.getMarkRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                            , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                        //Lấy danh sách thông tin điểm môn nhận xét
                        lstJudgeRecord = this.getJudgeRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                            , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                    }

                    //Lấy danh sách thông tin điểm tổng kết
                    lstSummedUpRecord = this.getSummedUpRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                }
                else
                {
                    if (form.ReportPatternID == 1) // theo mẫu cũ
                    {
                        //Lấy danh sách thông tin điểm môn tính điểm
                        lstMarkRecord = this.getMarkRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                            , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                        //Lấy danh sách thông tin điểm môn nhận xét
                        lstJudgeRecord = this.getJudgeRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                            , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                    }

                    //Lấy danh sách thông tin điểm tổng kết
                    lstSummedUpRecord = this.getSummedUpRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                }

                Stream excel = null;
                if (form.ReportPatternID == 1) // theo mẫu cũ
                {
                    excel = PointReportBusiness.CreatePointReportBySemester(pointReportBO, lstMarkRecord, lstJudgeRecord, lstSummedUpRecord);
                }
                else // theo mẫu mới
                {
                    excel = PointReportBusiness.CreatePointReportBySemesterFollowNewPattern(pointReportBO, lstSummedUpRecord);
                }

                processedReport = PointReportBusiness.InsertPointReportBySemester(pointReportBO, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySemester(SearchViewModel form)
        {
            PointReportBO pointReportBO = new PointReportBO();
            pointReportBO.ReportPatternID = form.ReportPatternID;
            pointReportBO.Semester = form.SemesterID;
            pointReportBO.EducationLevelID = form.EducationLevelID.Value;
            pointReportBO.ClassID = form.ClassID.GetValueOrDefault();
            pointReportBO.PaperSize = form.PaperSize;
            pointReportBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            pointReportBO.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            pointReportBO.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            pointReportBO.UserAccountID = _globalInfo.UserAccountID;
            List<MarkRecordBO> lstMarkRecord = new List<MarkRecordBO>() ;
            List<JudgeRecordBO> lstJudgeRecord = new List<JudgeRecordBO>() ;
            List<SummedUpRecordBO> lstSummedUpRecord = new List<SummedUpRecordBO>() ;
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
                

            if (isMovedHistory)
            {
                if (form.ReportPatternID == 1) // theo mẫu cũ
                {
                    //Lấy danh sách thông tin điểm môn tính điểm
                    lstMarkRecord = this.getMarkRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                    //Lấy danh sách thông tin điểm môn nhận xét
                    lstJudgeRecord = this.getJudgeRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                }
                
                //Lấy danh sách thông tin điểm tổng kết
                lstSummedUpRecord = this.getSummedUpRecordHistory(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                    , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
            }
            else
            {
                if (form.ReportPatternID == 1) // theo mẫu cũ
                {
                    //Lấy danh sách thông tin điểm môn tính điểm
                    lstMarkRecord = this.getMarkRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                    //Lấy danh sách thông tin điểm môn nhận xét
                    lstJudgeRecord = this.getJudgeRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID, pointReportBO.EducationLevelID
                        , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
                }

                //Lấy danh sách thông tin điểm tổng kết
                lstSummedUpRecord = this.getSummedUpRecord(pointReportBO.AcademicYearID, pointReportBO.SchoolID,  pointReportBO.EducationLevelID
                    , pointReportBO.ClassID, pointReportBO.Semester, acaYear.Year);
            }

            Stream excel = null;
            if (form.ReportPatternID == 1) // theo mẫu cũ
            {
                excel = PointReportBusiness.CreatePointReportBySemester(pointReportBO, lstMarkRecord, lstJudgeRecord, lstSummedUpRecord);
            }
            else // theo mẫu mới
            {
                excel = PointReportBusiness.CreatePointReportBySemesterFollowNewPattern(pointReportBO, lstSummedUpRecord);
            }

            ProcessedReport processedReport = PointReportBusiness.InsertPointReportBySemester(pointReportBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        private List<MarkRecordBO> getMarkRecord(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm môn tính điểm
            Dictionary<string, object> dicMarkRecord = new Dictionary<string, object> 
                {
                    {"AcademicYearID", AcademicYearID}, 
                    {"SchoolID", SchoolID},
                    {"EducationLevelID", EducationLevelID},
                    {"ClassID", ClassID},
                    {"Semester", Semester},
                    {"Year", Year}
                };
            //chi lay nhung mon khong phai mon tang cuong
            List<int> lstSubjectID = new List<int>();
            IDictionary<string, object> dicCS = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID",EducationLevelID}
            };
            lstSubjectID = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCS).Where(p=>p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p=>p.SubjectIDIncrease.Value).ToList();
            List<MarkRecordBO> lstMarkRecord = (from p in this.MarkRecordBusiness.SearchBySchool(SchoolID, dicMarkRecord).Where(p=>!lstSubjectID.Contains(p.SubjectID))
                                                join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                select new MarkRecordBO
                                                {
                                                    PupilID = p.PupilID,
                                                    ClassID = p.ClassID,
                                                    SubjectID = p.SubjectID,
                                                    Title = q.Title,
                                                    Mark = p.Mark,
                                                    Semester = p.Semester,
                                                    OrderNumber = p.OrderNumber
                                                })
                            .ToList();
            return lstMarkRecord;
        }
        private List<MarkRecordBO> getMarkRecordHistory(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm môn tính điểm
            Dictionary<string, object> dicMarkRecord = new Dictionary<string, object> 
                {
                    {"AcademicYearID", AcademicYearID}, 
                    {"SchoolID", SchoolID},
                    {"EducationLevelID", EducationLevelID},
                    {"ClassID", ClassID},
                    {"Semester", Semester},
                    {"Year", Year}
                };
            //chi lay nhung mon khong phai mon tang cuong
            List<int> lstSubjectID = new List<int>();
            IDictionary<string, object> dicCS = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID",EducationLevelID}
            };
            lstSubjectID = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCS).Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease.Value).ToList();
            List<MarkRecordBO> lstMarkRecord = (from p in this.MarkRecordHistoryBusiness.SearchMarkRecordHistory(dicMarkRecord).Where(p=>!lstSubjectID.Contains(p.SubjectID))
                                                join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                select new MarkRecordBO
                                                {
                                                    PupilID = p.PupilID,
                                                    ClassID = p.ClassID,
                                                    SubjectID = p.SubjectID,
                                                    Title = q.Title,
                                                    Mark = p.Mark,
                                                    Semester = p.Semester,
                                                    OrderNumber = p.OrderNumber
                                                })
                            .ToList();
            return lstMarkRecord;
        }
        private List<JudgeRecordBO> getJudgeRecord(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm môn nhận xét
            Dictionary<string, object> dicJudgeRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"SchoolID", SchoolID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID},
                {"Semester", Semester},
                {"checkWithClassMovement", "checkWithClassMovement"},
                {"Year", Year}
            };
            //chi lay nhung mon khong phai mon tang cuong
            List<int> lstSubjectID = new List<int>();
            IDictionary<string, object> dicCS = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID",EducationLevelID}
            };
            lstSubjectID = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCS).Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease.Value).ToList();
            List<JudgeRecordBO> lstJudgeRecord = (from p in JudgeRecordBusiness.SearchBySchool(SchoolID, dicJudgeRecord).Where(p=>!lstSubjectID.Contains(p.SubjectID))
                                                  join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                  select new JudgeRecordBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      ClassID = p.ClassID,
                                                      SubjectID = p.SubjectID,
                                                      Title = q.Title,
                                                      Judgement = p.Judgement,
                                                      Semester = p.Semester,
                                                      OrderNumber = p.OrderNumber
                                                  }).ToList();
            return lstJudgeRecord;
        }
        private List<JudgeRecordBO> getJudgeRecordHistory(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm môn nhận xét
            Dictionary<string, object> dicJudgeRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"SchoolID", SchoolID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID},
                {"Semester", Semester},
                {"checkWithClassMovement", "checkWithClassMovement"},
                {"Year", Year}
            };
            //chi lay nhung mon khong phai mon tang cuong
            List<int> lstSubjectID = new List<int>();
            IDictionary<string, object> dicCS = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID",EducationLevelID}
            };
            lstSubjectID = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCS).Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease.Value).ToList();
            List<JudgeRecordBO> lstJudgeRecord = (from p in JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(dicJudgeRecord).Where(p=>!lstSubjectID.Contains(p.SubjectID))
                                                  join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                  select new JudgeRecordBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      ClassID = p.ClassID,
                                                      SubjectID = p.SubjectID,
                                                      Title = q.Title,
                                                      Judgement = p.Judgement,
                                                      Semester = p.Semester,
                                                      OrderNumber = p.OrderNumber
                                                  })
                .ToList();
            return lstJudgeRecord;
        }
        private List<SummedUpRecordBO> getSummedUpRecord(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm tổng kết
            Dictionary<string, object> dicSummedUpRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"SchoolID", SchoolID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID}
            };
            //chi lay nhung mon khong phai mon tang cuong
            List<int> lstSubjectID = new List<int>();
            IDictionary<string, object> dicCS = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID",EducationLevelID}
            };
            lstSubjectID = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCS).Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease.Value).ToList();
            List<SummedUpRecordBO> lstSummedUpRecord = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, dicSummedUpRecord).Where(p=>!lstSubjectID.Contains(p.SubjectID))
                 .Select(o => new SummedUpRecordBO
                 {
                     PupilID = o.PupilID,
                     ClassID = o.ClassID,
                     SubjectID = o.SubjectID,
                     JudgementResult = o.JudgementResult,
                     SummedUpMark = o.SummedUpMark,
                     ReTestMark = o.ReTestMark,
                     ReTestJudgement = o.ReTestJudgement,
                     Semester = o.Semester
                 }).ToList();
            return lstSummedUpRecord;
        }
        private List<SummedUpRecordBO> getSummedUpRecordHistory(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID, int Semester, int Year)
        {
            //Lấy danh sách thông tin điểm tổng kết
            Dictionary<string, object> dicSummedUpRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"SchoolID", SchoolID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID}
            };
            //chi lay nhung mon khong phai mon tang cuong
            List<int> lstSubjectID = new List<int>();
            IDictionary<string, object> dicCS = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID",EducationLevelID}
            };
            lstSubjectID = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCS).Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease.Value).ToList();
            List<SummedUpRecordBO> lstSummedUpRecord = this.SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, dicSummedUpRecord).Where(p=>!lstSubjectID.Contains(p.SubjectID))
                 .Select(o => new SummedUpRecordBO
                 {
                     PupilID = o.PupilID,
                     ClassID = o.ClassID,
                     SubjectID = o.SubjectID,
                     JudgementResult = o.JudgementResult,
                     SummedUpMark = o.SummedUpMark,
                     ReTestMark = o.ReTestMark,
                     ReTestJudgement = o.ReTestJudgement,
                     Semester = o.Semester
                 }).ToList();
            return lstSummedUpRecord;
        }

        
    }
}