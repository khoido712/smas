﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Business.Common;
using Ionic.Zip;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;
namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class EvaluationBookReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEvaluationBookReportBusiness EvaluationBookReportBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        public EvaluationBookReportController(IClassProfileBusiness classProfileBusiness, IAcademicYearBusiness academicYearBusiness
            , IEvaluationBookReportBusiness evaluationBookReportBusiness, IProcessedReportBusiness processedReportBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EvaluationBookReportBusiness = evaluationBookReportBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
        }

        public ActionResult Index()
        {
            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            List<ClassProfile> lstClass = new List<ClassProfile>();
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // Hoc ky
            List<ComboObject> lstSemester = CommonList.Semester_SK();

            ComboObject defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow >= FirstStarDate && datenow <= FirstEndDate)
            {
                defaultSemester = lstSemester[0];
            }
            else
            {
                defaultSemester = lstSemester[1];
            }
            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester.key);
            return View();
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["EducationLevelID"] = educationLevelID;
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            var lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.IsVnenClass.HasValue && p.IsVnenClass.Value).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }
        #region Export
        [ValidateAntiForgeryToken]
        public JsonResult GetEvaluationBookReport(SearchViewModel form)
        {

            EvaluationBookReportBO objEvaluationBookBO = new EvaluationBookReportBO();
            objEvaluationBookBO.SemesterID = form.SemesterID;
            objEvaluationBookBO.ClassID = form.ClassID.GetValueOrDefault();
            objEvaluationBookBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            objEvaluationBookBO.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            objEvaluationBookBO.EducationLevelID = form.EducationLevelID.GetValueOrDefault();
            objEvaluationBookBO.Year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;
            objEvaluationBookBO.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            bool isZip = form.ClassID.HasValue && form.ClassID.Value > 0 ? false : true;
            ReportDefinition reportDef = EvaluationBookReportBusiness.GetReportDefinition(isZip ? SystemParamsInFile.REPORT_EVALUATION_BOOK_ZIPFILE : SystemParamsInFile.REPORT_EVALUATION_BOOK);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = EvaluationBookReportBusiness.GetProcessedReport(objEvaluationBookBO, isZip);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                if (isZip)
                {
                    string FolderSaveFile = string.Empty;
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"IsAdminSchoolRole",_globalInfo.IsAdminSchoolRole},
                        {"IsViewAll",_globalInfo.IsViewAll},
                        {"IsEmployeeManager",_globalInfo.IsEmployeeManager},
                        {"UserAccountID",_globalInfo.UserAccountID},
                        {"AppliedLevel",_globalInfo.AppliedLevel},
                    };
                    EvaluationBookReportBusiness.GetZipFileReport(objEvaluationBookBO, dic, out FolderSaveFile);
                    DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(Path.Combine(FolderSaveFile));
                        using (MemoryStream output = new MemoryStream())
                        {
                            zip.Save(output);
                            di.Delete(true);
                            objEvaluationBookBO.ClassID = 0;
                            processedReport = EvaluationBookReportBusiness.InsertEvaluationBookReport(objEvaluationBookBO, output, isZip);
                        }
                    }
                }
                else
                {
                    Stream excel = EvaluationBookReportBusiness.GetEvaluationBookReport(objEvaluationBookBO);
                    processedReport = EvaluationBookReportBusiness.InsertEvaluationBookReport(objEvaluationBookBO, excel);
                    excel.Close();
                }
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewEvaluationBookReport(SearchViewModel form)
        {
            EvaluationBookReportBO objEvaluationBookReportBO = new EvaluationBookReportBO();
            objEvaluationBookReportBO.SemesterID = form.SemesterID;
            objEvaluationBookReportBO.ClassID = form.ClassID.GetValueOrDefault();
            objEvaluationBookReportBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            objEvaluationBookReportBO.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            objEvaluationBookReportBO.EducationLevelID = form.EducationLevelID.GetValueOrDefault();
            objEvaluationBookReportBO.Year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;
            objEvaluationBookReportBO.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            bool isZip = form.ClassID.HasValue && form.ClassID.Value > 0 ? false : true;
            Stream excel = null;
            ProcessedReport processedReport = null;

            if (isZip)
            {
                string FolderSaveFile = string.Empty;
                IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"IsAdminSchoolRole",_globalInfo.IsAdminSchoolRole},
                        {"IsViewAll",_globalInfo.IsViewAll},
                        {"IsEmployeeManager",_globalInfo.IsEmployeeManager},
                        {"UserAccountID",_globalInfo.UserAccountID},
                        {"AppliedLevel",_globalInfo.AppliedLevel},
                    };
                EvaluationBookReportBusiness.GetZipFileReport(objEvaluationBookReportBO, dic, out FolderSaveFile);
                DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(Path.Combine(FolderSaveFile));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        di.Delete(true);
                        objEvaluationBookReportBO.ClassID = 0;
                        processedReport = EvaluationBookReportBusiness.InsertEvaluationBookReport(objEvaluationBookReportBO, output, isZip);
                    }
                }
            }
            else
            {
                excel = EvaluationBookReportBusiness.GetEvaluationBookReport(objEvaluationBookReportBO); ;
                processedReport = EvaluationBookReportBusiness.InsertEvaluationBookReport(objEvaluationBookReportBO, excel);
                excel.Close();
            }

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_EVALUATION_BOOK_ZIPFILE.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }
            result.FileDownloadName = processedReport.ReportName;
            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_EVALUATION_BOOK_ZIPFILE.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                //Duong dan thu muc chua file excel
                string inFolder = objFile.CreateFolder();
                //Duong dan thu muc chua file zip
                string outFolder = objFile.CreateFolder();

                string outFile = outFolder + "\\" + processedReport.ReportName;
                try
                {
                    //1. Extract file tu stream ra thu muc
                    objFile.UnzipFromStream(excel, inFolder);
                    //2. Chuyen file .xls thanh pdf
                    objFile.ConvertFileInFolderToPDF(inFolder, 0);
                    //3 Zip thu muc PDF
                    objFile.ZipFolder(outFile, inFolder);
                    //4. Chuyen file zip sang stream
                    result = new FileStreamResult(objFile.ConvertZipFileToStream(outFile), PdfExport.CONTENT_TYPE_ZIP);
                    result.FileDownloadName = processedReport.ReportName;
                }
                finally
                {
                    objFile.DeleteFolder(inFolder);
                    objFile.DeleteFolder(outFolder);
                }
                return result;
                //excel = new MemoryStream(processedReport.ReportData);
                //result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName,0);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
            }
        }

        #endregion
    }
}