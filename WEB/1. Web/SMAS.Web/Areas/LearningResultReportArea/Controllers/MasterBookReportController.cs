﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Utils;
using SMAS.VTUtils.Pdf;
using System.Text;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class MasterBookReportController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IMasterBookBusiness MasterBookBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        private int ViewBooklet
        {
            get
            {
                return SessionObject.Get<int>("MasterBookReportController");
            }
            set
            {
                SessionObject.Set<int>("MasterBookReportController", value);
            }
        }
        public MasterBookReportController(IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IClassProfileBusiness ClassProfileBusiness, IMasterBookBusiness MasterBookBusiness,
            IClassSubjectBusiness ClassSubjectBusiness, ISubjectCatBusiness SubjectCatBusiness, ISemeterDeclarationBusiness SemesterDeclarationBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness)
        {

            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.MasterBookBusiness = MasterBookBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SemesterDeclarationBusiness = SemesterDeclarationBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }
        public ActionResult Index()
        {

            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            //Lop
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevel.EducationLevelID, null,
                                                                         _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // Hoc ky
            List<ComboObject> lstSemester = CommonList.SemesterAndAll();

            ComboObject defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow >= FirstStarDate && datenow <= FirstEndDate)
            {
                defaultSemester = lstSemester[0];
            }
            else if (datenow >= SecondStartDate && datenow <= SecondEndDate)
            {
                defaultSemester = lstSemester[1];
            }
            else
            {
                defaultSemester = lstSemester[2];
            }

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester.key);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            bool isSoHCM = false;
            if (objSP != null && objSP.SupervisingDept != null && objSP.SupervisingDept.Province != null && objSP.SupervisingDept.Province.ProvinceName.ToUpper() == GlobalConstants.Province_HCM)
            {
                isSoHCM = true;
            }
            ViewData[LearningResultReportConstants.IS_SOHCM] = isSoHCM;
            return View();
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevelID, null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        public string GetSubjectBySemester(int ClassID, int SemesterID)
        {
            StringBuilder strResult = new StringBuilder();
            Dictionary<string, object> searchClass = new Dictionary<string, object>();
            searchClass["ClassID"] = ClassID;
            searchClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            searchClass["SchoolID"] = _globalInfo.SchoolID;
            IQueryable<ClassSubjectBO> iquery = (from p in ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchClass)
                                                 join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                 select new ClassSubjectBO
                                                 {
                                                     ClassID = p.ClassID,
                                                     DisplayName = q.DisplayName,
                                                     OrderInSubject = q.OrderInSubject,
                                                     SubjectID = q.SubjectCatID,
                                                     SubjectName = q.SubjectName,
                                                     IsCommenting = p.IsCommenting,
                                                     Abbreviation = q.Abbreviation,
                                                     SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester,
                                                     SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester,
                                                     AppliedType = p.AppliedType,
                                                     IsForeignLanguage = q.IsForeignLanguage
                                                 });

            if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                iquery = iquery.Where(o => o.SectionPerWeekFirstSemester > 0);
            }
            else if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                iquery = iquery.Where(o => o.SectionPerWeekSecondSemester > 0);
            }
            else
            {
                iquery = iquery.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0);
            }

            //Sap xep mon hoc theo thu tu: Môn bắt buộc (Tính điểm > nhận xét) > ngoại ngữ 2 > tự chọn > nghề PT.
            List<ClassSubjectBO> lsAllSubject = iquery.ToList();
            List<ClassSubjectBO> lstTotalSubject = new List<ClassSubjectBO>();
            //Danh sach mon hoc bat buoc
            List<ClassSubjectBO> lstMonBatBuoc = lsAllSubject.Where(s => s.AppliedType == SystemParamsInFile.APPLIED_TYPE_MANDATORY).OrderBy(s => s.OrderInSubject).ThenBy(s => s.IsCommenting).ToList();
            if (lstMonBatBuoc != null && lstMonBatBuoc.Count > 0)
            {
                lstTotalSubject.AddRange(lstMonBatBuoc);
            }


            List<int> lsAppliedTypeMonTuChon = new List<int>() { SystemParamsInFile.APPLIED_TYPE_VOLUNTARY, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PLUS_MARK, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PRIORITY };

            //Lay các môn ngoai ngu khac mon bat buoc va mon nghe
            List<ClassSubjectBO> lsMonNN2 = lsAllSubject.Where(o => o.IsForeignLanguage == true && o.AppliedType != SystemParamsInFile.APPLIED_TYPE_MANDATORY && o.AppliedType != SystemParamsInFile.APPLIED_TYPE_APPRENTICESHIP).ToList();

            //Lay danh sach mon hoc tu chon bo ra mon NN2
            List<ClassSubjectBO> lsMonTC = new List<ClassSubjectBO>();

            if (lsMonNN2 != null && lsMonNN2.Count > 0)
            {
                lstTotalSubject.AddRange(lsMonNN2);
                lsMonTC = lsAllSubject.Except(lsMonNN2).Where(p => lsAppliedTypeMonTuChon.Contains(p.AppliedType.Value)).OrderBy(s => s.OrderInSubject).ToList();
            }
            else
            {
                lsMonTC = lsAllSubject.Where(p => lsAppliedTypeMonTuChon.Contains(p.AppliedType.Value)).OrderBy(s => s.OrderInSubject).ToList();
            }

            if (lsMonTC != null && lsMonTC.Count > 0)
            {
                lstTotalSubject.AddRange(lsMonTC);
            }


            List<ClassSubjectBO> lsMonNghePT = lsAllSubject.Where(p => p.AppliedType.Value == SystemParamsInFile.APPLIED_TYPE_APPRENTICESHIP).ToList();
            if (lsMonNghePT != null && lsMonNghePT.Count > 0)
            {
                lstTotalSubject.AddRange(lsMonNghePT);
            }


            foreach (ClassSubjectBO item in lstTotalSubject)
            {
                strResult.Append("<span class='ClassListSubject editor-field'>");
                strResult.Append("<input type='checkbox' onchange='OneChecked()' class='classSubjectExport' checked='checked' value='").Append(item.SubjectID).Append("'/>").Append(item.DisplayName);
                strResult.Append("</span>");
            }
            return strResult.ToString();
        }

        public JsonResult GetReport(SearchViewModel form)
        {
            string type = JsonReportMessage.NEW;
            if (form.ClassID == null)
            {
                type = JsonMessage.ERROR;
                //throw new BusinessException("LearningResultReport_Validate_Required_ClassID");
                return Json(new JsonMessage(Res.Get("LearningResultReport_Validate_Required_ClassID"), type));
            }
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            SupervisingDept supervisingDept = sp.SupervisingDept;
            MasterBook mb = new MasterBook();
            mb.AcademicYearID = _globalInfo.AcademicYearID.Value;
            mb.ClassID = form.ClassID.Value;
            mb.EducationLevelID = form.EducationLevelID.Value;
            mb.Semester = form.SemesterID;
            mb.PupilNumber = form.PupilNumber == 0 ? 55 : form.PupilNumber;
            mb.fillPageNumber = form.paging == 1 ? true : false;
            if (_globalInfo.ProvinceID == GlobalConstants.ProvinceID_HCM && !form.isGVBM)
            {
                mb.isGVBM = true;
            }
            else
            {
                mb.isGVBM = form.isGVBM;
            }
            mb.isBorderMark = form.isBorderMark;
            mb.chkAllPupil = form.chkAllPupil;
            mb.SchoolID = _globalInfo.SchoolID.Value;
            mb.AppliedLevel = _globalInfo.AppliedLevel.Value;
            mb.chkAllSubject = form.chkAllSubject;
            mb.arrSubjectID = form.arrSubjectID;
            mb.rdo1 = form.rdo1;
            mb.rdo2 = form.rdo2;
            mb.chkPRAddress = form.chkPRAddress;
            mb.chkTRAddress = form.chkTRAddress;
            mb.chkVillageID = form.chkVillageID;
            mb.chkCommuneID = form.chkCommuneID;
            mb.chkDistrictID = form.chkDistrictID;
            mb.chkProvinceID = form.chkProvinceID;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            SemeterDeclaration SemeterDeclaration = SemesterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            if (SemeterDeclaration != null)
            {
                mb.InterviewMark = SemeterDeclaration.InterviewMark;
                mb.WritingMark = SemeterDeclaration.WritingMark;
                mb.TwiceCoeffiecientMark = SemeterDeclaration.TwiceCoeffiecientMark;
            }

            ProcessedReport processedReport = MasterBookBusiness.GetMasterBook(mb);
            if (processedReport != null)
            {
                type = JsonReportMessage.OLD;
            }

            if (type == JsonReportMessage.NEW)
            {
                string suffixName = ConfigurationManager.AppSettings[string.Format("INSOGOITENVAGHIDIEM{0}", _globalInfo.ProvinceID)];
                Stream excel = MasterBookBusiness.CreateMasterBook(mb, suffixName);
                processedReport = MasterBookBusiness.InsertMasterBook(mb, excel);
                excel.Close();
            }
            ViewBooklet = form.ViewBooklet;
            return Json(new JsonReportMessage(processedReport, type));
        }

        public JsonResult GetNewReport(SearchViewModel form)
        {
            MasterBook mb = new MasterBook();
            mb.AcademicYearID = _globalInfo.AcademicYearID.Value;
            mb.ClassID = form.ClassID.Value;
            mb.EducationLevelID = form.EducationLevelID.Value;
            mb.Semester = form.SemesterID;
            mb.SchoolID = _globalInfo.SchoolID.Value;
            mb.AppliedLevel = _globalInfo.AppliedLevel.Value;
            mb.PupilNumber = form.PupilNumber == 0 ? 55 : form.PupilNumber;
            mb.fillPageNumber = form.paging == 1 ? true : false;
            if (_globalInfo.ProvinceID == GlobalConstants.ProvinceID_HCM && !form.isGVBM)
            {
                mb.isGVBM = true;
            }
            else
            {
                mb.isGVBM = form.isGVBM;
            }

            mb.isBorderMark = form.isBorderMark;
            mb.chkAllPupil = form.chkAllPupil;
            mb.chkAllSubject = form.chkAllSubject;
            mb.arrSubjectID = form.arrSubjectID;
            mb.rdo1 = form.rdo1;
            mb.rdo2 = form.rdo2;
            mb.chkPRAddress = form.chkPRAddress;
            mb.chkTRAddress = form.chkTRAddress;
            mb.chkVillageID = form.chkVillageID;
            mb.chkCommuneID = form.chkCommuneID;
            mb.chkDistrictID = form.chkDistrictID;
            mb.chkProvinceID = form.chkProvinceID;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            SemeterDeclaration SemeterDeclaration = SemesterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            if (SemeterDeclaration != null)
            {
                mb.InterviewMark = SemeterDeclaration.InterviewMark;
                mb.WritingMark = SemeterDeclaration.WritingMark;
                mb.TwiceCoeffiecientMark = SemeterDeclaration.TwiceCoeffiecientMark;
            }
            ProcessedReport processedReport = MasterBookBusiness.GetMasterBook(mb);
            string suffixName = ConfigurationManager.AppSettings[string.Format("INSOGOITENVAGHIDIEM{0}", _globalInfo.ProvinceID)];
            Stream excel = MasterBookBusiness.CreateMasterBook(mb, suffixName);
            processedReport = MasterBookBusiness.InsertMasterBook(mb, excel);
            ViewBooklet = form.ViewBooklet;
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            FileStreamResult result = new FileStreamResult(excel, PdfExport.CONTENT_TYPE_XLS);
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public ActionResult DownloadReportPdf(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);

            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, ViewBooklet, Aspose.Cells.PaperSizeType.PaperA3);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public ActionResult ViewPattern()
        {
            return PartialView("_HtmlPattern");
        }
    }

}