﻿using Ionic.Zip;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class GeneralSummedEvaluationReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEvaluationRewardBusiness EvaluationRewardBusiness;
        public GeneralSummedEvaluationReportController(IClassProfileBusiness classProfileBusiness, IAcademicYearBusiness academicYearBusiness,
            IProcessedReportBusiness processedReportBusiness, ISummedEndingEvaluationBusiness summedEndingEvaluationBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness, IRatedCommentPupilBusiness RatedCommentPupilBusiness, IPupilOfClassBusiness PupilOfClassBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness, IEmployeeBusiness EmployeeBusiness, IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness,
            IClassSubjectBusiness ClassSubjectBusiness, IEvaluationRewardBusiness EvaluationRewardBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.SummedEndingEvaluationBusiness = summedEndingEvaluationBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.RatedCommentPupilBusiness = RatedCommentPupilBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.RatedCommentPupilHistoryBusiness = RatedCommentPupilHistoryBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EvaluationRewardBusiness = EvaluationRewardBusiness;
        }
        public ActionResult Index()
        {

            List<ComboObject> lstSemester = new List<ComboObject>();
            lstSemester.Add(new ComboObject(SystemParamsInFile.INDEX_PAGE.ToString(), Res.Get("Bìa")));
            lstSemester.Add(new ComboObject(SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI.ToString(), Res.Get("MarkRecord_Label_MidSemester1")));
            lstSemester.Add(new ComboObject(SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI.ToString(), Res.Get("MarkRecord_Label_EndSemester1")));
            lstSemester.Add(new ComboObject(SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII.ToString(), Res.Get("MarkRecord_Label_MidSemester2")));
            lstSemester.Add(new ComboObject(SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII.ToString(), "Cuối kỳ II"));

            List<ComboObject> lstTypePrint = new List<ComboObject>();
            lstTypePrint.Add(new ComboObject(1.ToString(), Res.Get("In ngang (A4)")));
            lstTypePrint.Add(new ComboObject(2.ToString(), Res.Get("In dọc (A3)")));

            List<ComboObject> lstPrintEnough = new List<ComboObject>();
            lstPrintEnough.Add(new ComboObject(1.ToString(), Res.Get("Tự động")));
            lstPrintEnough.Add(new ComboObject(2.ToString(), Res.Get("1 trang")));
            lstPrintEnough.Add(new ComboObject(3.ToString(), Res.Get("2 trang")));

            int defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow < SecondStartDate)
            {
                defaultSemester = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI;
            }
            else
            {
                defaultSemester = SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII;
            }

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester);
            ViewData[LearningResultReportConstants.TYPE_PRINT] = new SelectList(lstTypePrint, "key", "value", 1);
            ViewData[LearningResultReportConstants.PRINT_ENOUGH] = new SelectList(lstPrintEnough, "key", "value", 1);

            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            List<ClassProfile> lstClass = new List<ClassProfile>();
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            return View();
        }
        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["EducationLevelID"] = educationLevelID;
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            var lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }
        public JsonResult GetGeneralSummedEvaluation(FormCollection frm)
        {
            int TypeSemesterID = 0;
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;            
            int SemesterID = 0;
            int TypePrint = !string.IsNullOrEmpty(frm["TypePrint"]) ? int.Parse(frm["TypePrint"]) : 0;
            List<string> lstDataPrint = new List<string>();
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint1"]) ? frm["DataPrint1"] : "off");// GK1
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint3"]) ? frm["DataPrint3"] : "off");// CK1
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint2"]) ? frm["DataPrint2"] : "off");// GK2      
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint4"]) ? frm["DataPrint4"] : "off");// CN
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint5"]) ? frm["DataPrint5"] : "off");// BIA
            string IsPupilInClass = !string.IsNullOrEmpty(frm["IsPupilInClass"]) ? frm["IsPupilInClass"] : "off";
            int NumberOfRow = !string.IsNullOrEmpty(frm["NumberOfRow"]) ? int.Parse(frm["NumberOfRow"]) : 0;
            int PrintEnough = !string.IsNullOrEmpty(frm["PrintEnough"]) ? int.Parse(frm["PrintEnough"]) : 0;
            string reportCode = "";
            if (ClassID > 0)
            {
                if (TypePrint == 1)
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA;
                else
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3;
            }
            else
            {
                if (TypePrint == 1)
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_ZIP;
                else
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3_ZIP;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            string ClassName = string.Empty;
            List<int> lstClassID = new List<int>();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"SemesterID",SemesterID},
                {"TypeSemesterID",TypeSemesterID},              
                {"IsPupilInClass",IsPupilInClass},
                {"NumberOfRow",NumberOfRow},
                {"PrintEnough",PrintEnough},
                {"lstDataPrint", lstDataPrint},
                {"EducationLevelID", EducationLevelID},  
                {"TypePrint", TypePrint},
            };

            if (ClassID > 0)
            {
                #region Có ClassID
                ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
                ClassName = objCP.DisplayName;
		        lstClassID.Add(ClassID);
                dic["lstClassID"] = lstClassID;              
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = SummedEndingEvaluationBusiness.GetProcessedReportSummedEnding(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, TypeSemesterID, EducationLevelID, ClassID, TypePrint, false, lstDataPrint);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {                    
                    Stream excel = SummedEndingEvaluationBusiness.GetGeneralReport(dic);
                    processedReport = SummedEndingEvaluationBusiness.InsertGeneralSummedEvaluationReport(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, EducationLevelID, ClassID, TypeSemesterID, ClassName, excel, TypePrint, false, lstDataPrint);
                    excel.Close();
                }
                #endregion
            }
            else
            {
                #region Tất cả lớp
                ClassName = "TatCa";                
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;                  
                }
                List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                lstClassID = lstClass.Select(p=>p.ClassProfileID).Distinct().ToList();
                dic["lstClassID"] = lstClassID;  
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = SummedEndingEvaluationBusiness.GetProcessedReportSummedEnding(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, TypeSemesterID, EducationLevelID, ClassID, TypePrint, true, lstDataPrint);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {                  
                    string FolderSaveFile = string.Empty;
                    DownloadZipFileTranscript(dic, out FolderSaveFile);
                    if (FolderSaveFile == "")
                    {
                        return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                    }
                    DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(Path.Combine(FolderSaveFile));
                        using (MemoryStream output = new MemoryStream())
                        {
                            zip.Save(output);
                            di.Delete(true);
                            processedReport = SummedEndingEvaluationBusiness.InsertGeneralSummedEvaluationReportZip(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, EducationLevelID, ClassID, TypeSemesterID, ClassName, output, TypePrint, true, lstDataPrint);
                        }
                    }
                }
                #endregion
            }         
            return Json(new JsonReportMessage(processedReport, type));
        }   
        public JsonResult GetNewGeneralSummedEvaluation(FormCollection frm)
        {
            int TypeSemesterID = 0;//!string.IsNullOrEmpty(frm["SemesterID"]) ? int.Parse(frm["SemesterID"]) : 0;
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            //int SemesterID = (TypeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI 
            //                    || TypeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI) 
             //                       ? GlobalConstants.SEMESTER_OF_YEAR_FIRST : GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            int SemesterID = 0;
            int TypePrint = !string.IsNullOrEmpty(frm["TypePrint"]) ? int.Parse(frm["TypePrint"]) : 0;
            List<string> lstDataPrint = new List<string>();
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint1"]) ? frm["DataPrint1"] : "off");
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint3"]) ? frm["DataPrint3"] : "off");
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint2"]) ? frm["DataPrint2"] : "off");          
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint4"]) ? frm["DataPrint4"] : "off");
            lstDataPrint.Add(!string.IsNullOrEmpty(frm["DataPrint5"]) ? frm["DataPrint5"] : "off");
            string IsPupilInClass = !string.IsNullOrEmpty(frm["IsPupilInClass"]) ? frm["IsPupilInClass"] : "off";
            int NumberOfRow = !string.IsNullOrEmpty(frm["NumberOfRow"]) ? int.Parse(frm["NumberOfRow"]) : 0;
            int PrintEnough = !string.IsNullOrEmpty(frm["PrintEnough"]) ? int.Parse(frm["PrintEnough"]) : 0;

            string ClassName = string.Empty;
            List<int> lstClassID = new List<int>();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"SemesterID",SemesterID},
                {"TypeSemesterID",TypeSemesterID},              
                {"IsPupilInClass",IsPupilInClass},
                {"NumberOfRow",NumberOfRow},
                {"PrintEnough",PrintEnough},
                {"lstDataPrint", lstDataPrint},
                {"EducationLevelID", EducationLevelID},
                {"TypePrint", TypePrint},
            };

            string reportCode = "";
            if (ClassID > 0)
            {
                if(TypePrint == 1)
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA;
                else
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3;
            }
            else
            {
                if(TypePrint == 1)
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_ZIP;
                else
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3_ZIP;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport processedReport = null;

            if (ClassID > 0)
            {
                ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
                ClassName = objCP.DisplayName;
                lstClassID.Add(ClassID);
                dic["lstClassID"] = lstClassID;              
                Stream excel = SummedEndingEvaluationBusiness.GetGeneralReport(dic);
                processedReport = SummedEndingEvaluationBusiness.InsertGeneralSummedEvaluationReport(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, EducationLevelID, ClassID, TypeSemesterID, ClassName, excel, TypePrint, false, lstDataPrint);
                excel.Close();
            }
            else
            {
                ClassName = "TatCa";
                IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicSearch["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dicSearch["EducationLevelID"] = EducationLevelID;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dicSearch["UserAccountID"] = _globalInfo.UserAccountID;
                    dicSearch["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                lstClassID = lstClass.Select(p => p.ClassProfileID).Distinct().ToList();
                dic["lstClassID"] = lstClassID;  

                string FolderSaveFile = string.Empty;
                DownloadZipFileTranscript(dic, out FolderSaveFile);
                if (FolderSaveFile == "")
                {
                    return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                }
                DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(Path.Combine(FolderSaveFile));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        di.Delete(true);
                        processedReport = SummedEndingEvaluationBusiness.InsertGeneralSummedEvaluationReportZip(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, EducationLevelID, ClassID, TypeSemesterID, ClassName, output, TypePrint, true, lstDataPrint);
                    }
                }

            }
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public void DownloadZipFileTranscript(IDictionary<string, object> dic, out string folderSaveFile)
        {
            #region lay danh sach hoc sinh cua lop
            int SchoolID = Business.Common.Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Business.Common.Utils.GetInt(dic, "AcademicYearID");
            List<int> lstClassID = Business.Common.Utils.GetIntList(dic, "lstClassID");
            int AppliedLevel = Business.Common.Utils.GetInt(dic, "AppliedLevelID");
            int SemesterID = Business.Common.Utils.GetInt(dic, "SemesterID");//
            int TypeSemesterID = Business.Common.Utils.GetInt(dic, "TypeSemesterID");//
            List<string> lstDataPrint = Business.Common.Utils.GetStringList(dic, "lstDataPrint");
            int EducationLevelID = Business.Common.Utils.GetInt(dic, "EducationLevelID");//
            int TypePrint = Business.Common.Utils.GetInt(dic, "TypePrint");
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            string tmp = string.Empty;
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);
            string reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA;
            if (TypePrint == 2) 
            {
                reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string fileName = "HS_TH_Bangtonghop_{0}.xls";//THI_THPT_PhieuThuBaiThi_[NhomThi]_[MonThi]

            #region getData
            IDictionary<string, object> dicSearchClass = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID},
                {"EducationLevelID", EducationLevelID},
            };
            List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicSearchClass).ToList();
            #region lay danh sach hoc sinh cua cac lop
            //lay danh sach hoc sinh theo list classID
            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.Search(dic)
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           join e in EmployeeBusiness.All on cp.HeadTeacherID equals e.EmployeeID into l
                                           from l1 in l.DefaultIfEmpty()
                                           where cp.IsActive.HasValue && cp.IsActive.Value
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = poc.PupilProfile.PupilCode,
                                               PupilFullName = poc.PupilProfile.FullName,
                                               ClassID = poc.ClassID,
                                               ClassName = poc.ClassProfile.DisplayName,
                                               Status = poc.Status,
                                               OrderInClass = poc.OrderInClass,
                                               Name = poc.PupilProfile.Name,
                                               Birthday = poc.PupilProfile.BirthDate,
                                               HeadTeacherName = l1.FullName,
                                               Genre = poc.PupilProfile.Genre
                                           }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            #endregion

            List<int> lstPupilRemove = new List<int>();
            if (objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value == true)
            {
                lstPupilRemove = lstPOC.Where(x => x.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            && x.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING).Select(x => x.PupilID).ToList();
                lstPOC = lstPOC.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
            }

            #region lay danh sach diem cua hoc sinh
            //lay danh sach diem cua hoc sinh
            List<RatedCommentPupilBO> lstRatedCommentPupilBO = new List<RatedCommentPupilBO>();
            IDictionary<string, object> dicRated = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID},
                //{"SemesterID",SemesterID},//
            };
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedCommentPupilBO = (from rh in RatedCommentPupilHistoryBusiness.Search(dicRated)
                                          select new RatedCommentPupilBO
                                          {
                                              SchoolID = rh.SchoolID,
                                              AcademicYearID = rh.AcademicYearID,
                                              PupilID = rh.PupilID,
                                              ClassID = rh.ClassID,
                                              SubjectID = rh.SubjectID,
                                              SemesterID = rh.SemesterID,
                                              EvaluationID = rh.EvaluationID,
                                              PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                              PeriodicEndingMark = rh.PeriodicEndingMark,
                                              PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                              PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                              MiddleEvaluation = rh.MiddleEvaluation,
                                              EndingEvaluation = rh.EndingEvaluation,
                                              CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                              CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                              QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                              QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                              Comment = rh.Comment,
                                              CreateTime = rh.CreateTime,
                                              UpdateTime = rh.UpdateTime,
                                              FullNameComment = rh.FullNameComment,
                                              LogChangeID = rh.LogChangeID,
                                          }).ToList();
            }
            else
            {
                lstRatedCommentPupilBO = (from rh in RatedCommentPupilBusiness.Search(dic)
                                          select new RatedCommentPupilBO
                                          {
                                              SchoolID = rh.SchoolID,
                                              AcademicYearID = rh.AcademicYearID,
                                              PupilID = rh.PupilID,
                                              ClassID = rh.ClassID,
                                              SubjectID = rh.SubjectID,
                                              SemesterID = rh.SemesterID,
                                              EvaluationID = rh.EvaluationID,
                                              PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                              PeriodicEndingMark = rh.PeriodicEndingMark,
                                              PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                              PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                              MiddleEvaluation = rh.MiddleEvaluation,
                                              EndingEvaluation = rh.EndingEvaluation,
                                              CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                              CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                              QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                              QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                              Comment = rh.Comment,
                                              CreateTime = rh.CreateTime,
                                              UpdateTime = rh.UpdateTime,
                                              FullNameComment = rh.FullNameComment,
                                              LogChangeID = rh.LogChangeID
                                          }).ToList();
            }
            #endregion
            #region lay danh sach mon hoc
            //lay danh sach mon hoc 
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ListClassID"] = lstClassID;
            dicSubject["AcademicYearID"] = AcademicYearID;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = AppliedLevel;
            //dicSubject["Semester"] = SemesterID;//
            iqSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSubject);
            List<ClassSubjectBO> lstSubject = iqSubject.Select(o => new ClassSubjectBO()
            {
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                DisplayName = o.SubjectCat.DisplayName,
                SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                IsCommenting = o.IsCommenting,
                Abbreviation = o.SubjectCat.Abbreviation
            }).OrderBy(p => p.IsCommenting).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            #endregion
            #region lay danh sach khen thuong cua hoc sinh
            List<EvaluationReward> lstEvaluationReward = new List<EvaluationReward>();
            List<SummedEndingEvaluationBO> lstSEE = new List<SummedEndingEvaluationBO>();
            IDictionary<string, object> dicSearchReward = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID},
                {"Semester",GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING},
                {"EvaluationID",GlobalConstants.EVALUATION_RESULT}
            };
            lstSEE = SummedEndingEvaluationBusiness.Search(dicSearchReward).ToList();

            dicSearchReward = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID}
            };
            lstEvaluationReward = EvaluationRewardBusiness.Search(dicSearchReward).ToList();
            #endregion

            if (lstPupilRemove.Count > 0)
            {
                lstRatedCommentPupilBO = lstRatedCommentPupilBO.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                lstEvaluationReward = lstEvaluationReward.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                lstSEE = lstSEE.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
            }
            #endregion

            for (int i = 0; i < lstCP.Count; i++)
            {
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                SummedEndingEvaluationBusiness.GetGeneralReportZip(dic, lstCP[i], lstDataPrint, lstPOC, oBook, objAy, objSP, lstSubject, lstRatedCommentPupilBO, lstSEE, lstEvaluationReward);
                oBook.CreateZipFile(string.Format(fileName, ReportUtils.StripVNSign(lstCP[i].DisplayName)), folder);
            }
            folderSaveFile = folder;
            #endregion
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;

            if (SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_ZIP.Equals(processedReport.ReportCode) ||
                SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3_ZIP.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }

            result.FileDownloadName = processedReport.ReportName;


            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_ZIP.Equals(processedReport.ReportCode) ||
                SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3_ZIP.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                //Duong dan thu muc chua file excel
                string inFolder = objFile.CreateFolder();
                //Duong dan thu muc chua file zip
                string outFolder = objFile.CreateFolder();

                string outFile = outFolder + "\\" + processedReport.ReportName;
                try
                {
                    //1. Extract file tu stream ra thu muc
                    objFile.UnzipFromStream(excel, inFolder);
                    //2. Chuyen file .xls thanh pdf
                    objFile.ConvertFileInFolderToPDF(inFolder, 0);
                    //3 Zip thu muc PDF
                    objFile.ZipFolder(outFile, inFolder);
                    //4. Chuyen file zip sang stream
                    result = new FileStreamResult(objFile.ConvertZipFileToStream(outFile), PdfExport.CONTENT_TYPE_ZIP);
                    result.FileDownloadName = processedReport.ReportName;
                }
                finally
                {
                    objFile.DeleteFolder(inFolder);
                    objFile.DeleteFolder(outFolder);
                }
                return result;
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
            }



        }
    }
}