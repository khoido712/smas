﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class AverageMarkOfYearReportController:BaseController
    {
        IAcademicYearBusiness AcademicYearBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness;

        public AverageMarkOfYearReportController(IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IClassProfileBusiness ClassProfileBusiness, ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness)
        {

            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.TranscriptsBySemesterBusiness = TranscriptsBySemesterBusiness;
        }
        public ActionResult Index()
        {
            
            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            //Lop
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevel.EducationLevelID, null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // Hoc ky
            List<ComboObject> lstSemester = CommonList.SemesterAndAll();

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", SystemParamsInFile.SEMESTER_OF_YEAR_ALL);

           
            return View();
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevelID, null,
                                                                        _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult GetReport(SearchViewModel form)
        {
            if (form.EducationLevelID == null)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_EducationLevelID");
            }

            if (ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, form.EducationLevelID, null,
                                                            _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole).Count() == 0)
            {
                throw new BusinessException("LearningResultReport_Validate_NotClass");
            }

            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            if (_globalInfo.SchoolID.HasValue) TranscriptOfClass.SchoolID = _globalInfo.SchoolID.Value;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.ClassID = form.ClassID.GetValueOrDefault();
            TranscriptOfClass.AcademicYearID = _globalInfo.AcademicYearID.Value;
            TranscriptOfClass.AppliedLevel = _globalInfo.AppliedLevel.Value;
            TranscriptOfClass.isAdmin = _globalInfo.IsAdminSchoolRole;
            TranscriptOfClass.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            TranscriptOfClass.UserAccountID = _globalInfo.UserAccountID;
            TranscriptOfClass.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
            TranscriptOfClass.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
            string reportCode = SystemParamsInFile.REPORT_BANG_DIEM_BINH_QUAN_CA_NAM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.TranscriptsBySemesterBusiness.GetAverageAnnualMark(TranscriptOfClass);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.TranscriptsBySemesterBusiness.CreateAverageAnnualMark(TranscriptOfClass);
                processedReport = this.TranscriptsBySemesterBusiness.InsertAverageAnnualMark(TranscriptOfClass, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));

        }

        public JsonResult GetNewReport(SearchViewModel form)
        {
            GlobalInfo gl = new GlobalInfo();
            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            if (gl.SchoolID.HasValue) TranscriptOfClass.SchoolID = gl.SchoolID.Value;
            TranscriptOfClass.ClassID = form.ClassID.GetValueOrDefault();
            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AcademicYearID = gl.AcademicYearID.Value;
            TranscriptOfClass.AppliedLevel = gl.AppliedLevel.Value;
            TranscriptOfClass.isAdmin = gl.IsAdminSchoolRole;
            TranscriptOfClass.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            TranscriptOfClass.UserAccountID = gl.UserAccountID;
            TranscriptOfClass.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
            TranscriptOfClass.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
            Stream excel = this.TranscriptsBySemesterBusiness.CreateAverageAnnualMark(TranscriptOfClass);
            ProcessedReport processedReport = this.TranscriptsBySemesterBusiness.InsertAverageAnnualMark(TranscriptOfClass, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDF(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}