﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class SubjectMarkBookReportController : BaseController
    {
        IAcademicYearBusiness AcademicYearBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        IClassSubjectBusiness ClassSubjectBusiness;
        ISchoolFacultyBusiness SchoolFacultyBusiness;
        IEmployeeBusiness EmployeeBusiness;
        ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        IReportMarkSubjectByTeacherBusiness ReportMarkSubjectByTeacherBusiness;

        public SubjectMarkBookReportController(IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IClassProfileBusiness ClassProfileBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness, ITeachingAssignmentBusiness TeachingAssignmentBusiness,
            IReportMarkSubjectByTeacherBusiness ReportMarkSubjectByTeacherBusiness)
        {

            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.ReportMarkSubjectByTeacherBusiness = ReportMarkSubjectByTeacherBusiness;
        }
        public ActionResult Index()
        {

            // Hoc ky
            List<ComboObject> lstSemester = CommonList.Semester();
            int defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (datenow >= FirstStarDate && datenow <= FirstEndDate)
            {
                defaultSemester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
            }
            else
            {
                defaultSemester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            }
            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester);

            //Mon hoc
            //Lay cac lop theo cap dang chon
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            List<int> listClassID = ClassProfileBusiness.Search(dic).Select(o => o.ClassProfileID).ToList();
            //Lay danh sach cac mon hoc cua cac lop
            dic = new Dictionary<string, object>();
            //dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Grade"] = _globalInfo.Grade;
            dic["ListClassID"] = listClassID;
            dic["IsVNEN"] = true;
            var listSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Select(o => new { SubjectID = o.SubjectID, SubjectName = o.SubjectCat.SubjectName, OrderInSubject = o.SubjectCat.OrderInSubject })
                                                                .Distinct().OrderBy(o => o.OrderInSubject).ToList();
            ViewData[LearningResultReportConstants.LIST_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");

            //to bo mon
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            List<SchoolFaculty> listSF = SchoolFacultyBusiness.Search(dic).OrderBy(p => p.FacultyName).ToList();
            ViewData[LearningResultReportConstants.LIST_SCHOOL_FACULTY] = new SelectList(listSF, "SchoolFacultyID", "FacultyName");

            //Giao vien
            ViewData[LearningResultReportConstants.LIST_TEACHER] = new List<ComboObject>();

            return View();
        }

        public PartialViewResult AjaxLoadTeacher(int? subjectID, int semester, int? schoolFacultyID)
        {
            if (subjectID == null || schoolFacultyID == null)
            {
                ViewData[LearningResultReportConstants.LIST_TEACHER] = new List<ComboObject>();

                return PartialView("_TeacherCombobox");
            }
            //Lay danh sach giao vien trong truong
            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = _globalInfo.SchoolID;
            dicToGetTeacher["FacultyID"] = schoolFacultyID;
            List<Employee> listEmp = EmployeeBusiness.Search(dicToGetTeacher).ToList();

            //Lay danh sach giao vien duong phan cong day mon duoc chon
            IDictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
            dicToGetAssignment["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicToGetAssignment["SubjectID"] = subjectID;
            dicToGetAssignment["Semester"] = semester;

            IQueryable<TeachingAssignment> lstTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicToGetAssignment);

            IEnumerable<ComboObject> lstComboEmployee = from q in listEmp
                                                        where lstTeachingAssignment.Any(o => o.TeacherID == q.EmployeeID)
                                                        select new ComboObject
                                                        {
                                                            key = q.EmployeeID.ToString(),
                                                            value = q.FullName + " - " + q.EmployeeCode

                                                        };

            ViewData[LearningResultReportConstants.LIST_TEACHER] = lstComboEmployee;

            return PartialView("_TeacherCombobox");
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(FormCollection form)
        {
            if (String.IsNullOrEmpty(form["SubjectID"]))
            {
                throw new BusinessException("LearningResultReport_Validate_Required_SubjectID");
            }
            if (String.IsNullOrEmpty(form["SchoolFacultyID"]))
            {
                throw new BusinessException("Tổ bộ môn không được để trống");
            }
            int val;
            if (String.IsNullOrEmpty(form["TeacherID"]) || Int32.TryParse(form["TeacherID"], out val) == false)
            {
                throw new BusinessException("LearningResultReport_Validate_Required_TeacherID");
            }


            ReportMarkSubjectBO reportMarkSubject = new ReportMarkSubjectBO();

            reportMarkSubject.AppliedLevel = _globalInfo.AppliedLevel.Value;
            reportMarkSubject.Semester = int.Parse(form["SemesterID"]);
            reportMarkSubject.SchoolID = _globalInfo.SchoolID.Value;
            reportMarkSubject.AcademicYearID = _globalInfo.AcademicYearID.Value;
            reportMarkSubject.SubjectID = int.Parse(form["SubjectID"]);
            reportMarkSubject.TeacherID = int.Parse(form["TeacherID"]);
            string reportCode = SystemParamsInFile.REPORT_SODIEMBOMONTHEOGIAOVIEN;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.ReportMarkSubjectByTeacherBusiness.GetReportMarkSubjectByTeacher(reportMarkSubject);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportMarkSubjectByTeacherBusiness.CreateReportMarkSubjectByTeacher(reportMarkSubject);
                processedReport = this.ReportMarkSubjectByTeacherBusiness.InsertReportMarkSubjectByTeacher(reportMarkSubject, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection form)
        {
            ReportMarkSubjectBO reportMarkSubject = new ReportMarkSubjectBO();
            reportMarkSubject.AcademicYearID = _globalInfo.AcademicYearID.Value;
            reportMarkSubject.AppliedLevel = _globalInfo.AppliedLevel.Value;
            reportMarkSubject.Semester = int.Parse(form["SemesterID"]);

            reportMarkSubject.SchoolID = _globalInfo.SchoolID.Value;
            reportMarkSubject.SubjectID = int.Parse(form["SubjectID"]);
            reportMarkSubject.TeacherID = int.Parse(form["TeacherID"]);

            Stream excel = this.ReportMarkSubjectByTeacherBusiness.CreateReportMarkSubjectByTeacher(reportMarkSubject);
            ProcessedReport processedReport = this.ReportMarkSubjectByTeacherBusiness.InsertReportMarkSubjectByTeacher(reportMarkSubject, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}