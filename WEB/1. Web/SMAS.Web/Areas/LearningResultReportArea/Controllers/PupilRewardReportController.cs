﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LearningResultReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilRewardReportController:BaseController
    {
               
        IAcademicYearBusiness AcademicYearBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        IPupilRewardReportBusiness PupilRewardReportBusiness;

        public PupilRewardReportController(IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IClassProfileBusiness ClassProfileBusiness,IPupilRewardReportBusiness PupilRewardReportBusiness)
        {

            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilRewardReportBusiness = PupilRewardReportBusiness;
        }
        public ActionResult Index()
        {
            
            //Khoi
            ViewData[LearningResultReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();

            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevel.EducationLevelID, null,
                                                                         _globalInfo.UserAccountID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);
            ViewData[LearningResultReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // Hoc ky
            List<ComboObject> lstSemester = null;
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                lstSemester = CommonList.SemesterPrimary();
            }
            else
            {
                lstSemester = CommonList.SemesterAndAll();
            }
            

            ComboObject defaultSemester;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime? SecondStartDate = academicYear.SecondSemesterStartDate;
            DateTime? SecondEndDate = academicYear.SecondSemesterEndDate;

            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);

            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                if (datenow >= FirstStarDate && datenow <= FirstEndDate)
                {
                    defaultSemester = lstSemester[0];
                }
                else
                {
                    defaultSemester = lstSemester[1];
                }
            }
            else
            {
                if (datenow >= FirstStarDate && datenow <= FirstEndDate)
                {
                    defaultSemester = lstSemester[0];
                }
                else if (datenow >= SecondStartDate && datenow <= SecondEndDate)
                {
                    defaultSemester = lstSemester[1];
                }
                else
                {
                    defaultSemester = lstSemester[2];
                }
            }           

            ViewData[LearningResultReportConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", defaultSemester.key);

           
            return View();
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            var lstClass = ClassProfileBusiness.getClassByAccountRole(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, educationLevelID, null,
                                                                        _globalInfo.UserAccountID,_globalInfo.IsSuperVisingDeptRole,_globalInfo.IsSubSuperVisingDeptRole);

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        #region Bao cao Danh sach khen thuong
        public JsonResult GetPupilRewardReport(FormCollection frm)
        {
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                ProcessedReport processedReport = null;
                string type = JsonReportMessage.NEW;

                PupilRewardReportBO PupilRewardReportBO = new PupilRewardReportBO();
                PupilRewardReportBO.Semester =  !string.IsNullOrEmpty(frm["SemesterID"]) ? int.Parse(frm["SemesterID"]) : 0;
                if (string.IsNullOrEmpty(frm["EducationLevelID"]))
                {
                    throw new BusinessException("LearningResultReport_Validate_Required_EducationLevelID");
                }
                PupilRewardReportBO.EducationLevelID = int.Parse(frm["EducationLevelID"]);
                PupilRewardReportBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                PupilRewardReportBO.SchoolID = _globalInfo.SchoolID.Value;
                PupilRewardReportBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                PupilRewardReportBO.ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
                PupilRewardReportBO.UserAccountID = _globalInfo.UserAccountID;
                if ("on".Equals(frm["chkChecked"]))
                {
                    PupilRewardReportBO.chkChecked = true;
                }
                else
                {
                    PupilRewardReportBO.chkChecked = false;
                }

                string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT30_CAP1; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.PupilRewardReportBusiness.GetPupilRewardReportForPrimaryTT30(PupilRewardReportBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.PupilRewardReportBusiness.CreatePupilRewardPrimaryReportTT30(PupilRewardReportBO);
                    processedReport = this.PupilRewardReportBusiness.InsertPupilRewardPrimaryReportTT30(PupilRewardReportBO, excel);
                    excel.Close();

                }

                return Json(new JsonReportMessage(processedReport, type));
            }
            else
            {
                ProcessedReport processedReport = null;
                string type = JsonReportMessage.NEW;

                PupilRewardReportBO PupilRewardReportBO = new PupilRewardReportBO();
                PupilRewardReportBO.Semester = !string.IsNullOrEmpty(frm["SemesterID"]) ? int.Parse(frm["SemesterID"]) : 0;
                if (string.IsNullOrEmpty(frm["EducationLevelID"]))
                {
                    throw new BusinessException("LearningResultReport_Validate_Required_EducationLevelID");
                }
                PupilRewardReportBO.EducationLevelID = int.Parse(frm["EducationLevelID"]);
                PupilRewardReportBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                PupilRewardReportBO.SchoolID = _globalInfo.SchoolID.Value;
                PupilRewardReportBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                PupilRewardReportBO.ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
                PupilRewardReportBO.UserAccountID = _globalInfo.UserAccountID;
                PupilRewardReportBO.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
                PupilRewardReportBO.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
                if ("on".Equals(frm["chkChecked"]))
                {
                    PupilRewardReportBO.chkChecked = true;
                }
                else
                {
                    PupilRewardReportBO.chkChecked = false;
                }
                string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG; ;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.PupilRewardReportBusiness.GetPupilReward(PupilRewardReportBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.PupilRewardReportBusiness.CreatePupilReward(PupilRewardReportBO);
                    processedReport = this.PupilRewardReportBusiness.InsertPupilReward(PupilRewardReportBO, excel);
                    excel.Close();

                }

                return Json(new JsonReportMessage(processedReport, type));
            }
        }

        public JsonResult GetNewPupilRewardReport(FormCollection frm)
        {
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                ProcessedReport processedReport = null;
                PupilRewardReportBO bo = new PupilRewardReportBO();
                bo.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                bo.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bo.AppliedLevel = _globalInfo.AppliedLevel;
                bo.Semester = !string.IsNullOrEmpty(frm["SemesterID"]) ? int.Parse(frm["SemesterID"]) : 0;
                bo.EducationLevelID = int.Parse(frm["EducationLevelID"]);
                bo.ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
                bo.UserAccountID = _globalInfo.UserAccountID;
                if ("on".Equals(frm["chkChecked"]))
                {
                    bo.chkChecked = true;
                }
                else
                {
                    bo.chkChecked = false;
                }
                Stream excel = PupilRewardReportBusiness.CreatePupilRewardPrimaryReportTT30(bo);
                processedReport = PupilRewardReportBusiness.InsertPupilRewardPrimaryReportTT30(bo, excel);
                excel.Close();

                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }
            else
            {
                ProcessedReport processedReport = null;
                PupilRewardReportBO bo = new PupilRewardReportBO();
                bo.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                bo.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bo.AppliedLevel =_globalInfo.AppliedLevel;
                bo.Semester = !string.IsNullOrEmpty(frm["SemesterID"]) ? int.Parse(frm["SemesterID"]) : 0;
                bo.EducationLevelID = int.Parse(frm["EducationLevelID"]);
                bo.ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
                bo.UserAccountID = _globalInfo.UserAccountID;
                bo.IsSuperVisingDept = _globalInfo.IsSuperVisingDeptRole;
                bo.IsSubSuperVisingDept = _globalInfo.IsSubSuperVisingDeptRole;
                if ("on".Equals(frm["chkChecked"]))
                {
                    bo.chkChecked = true;
                }
                else
                {
                    bo.chkChecked = false;
                }
                Stream excel = PupilRewardReportBusiness.CreatePupilReward(bo);
                processedReport = PupilRewardReportBusiness.InsertPupilReward(bo, excel);
                excel.Close();

                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }

        }
        #endregion

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}