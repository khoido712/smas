﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.LearningResultReportArea;

namespace SMAS.Web.Areas.LearningResultReportArea.Models
{
    public class ReportViewModel
    {
        [ResourceDisplayName("ReportEvaluationComments_Label_EducationLevelSearch")]
        [DataType("Combobox")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int EducationLevel { get; set; }

        [ResourceDisplayName("ReportEvaluationComments_Label_ClassSearch")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_Required_ReportEvaluation_Class")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int Class { get; set; }

        [ResourceDisplayName("ReportEvaluationComments_Label_SubjectSearch")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_Required_ReportEvaluation_Subject")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_SUBJECT)]
        [AdditionalMetadata("Placeholder", "0")]
        public int Subject { get; set; }
    }
}