﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea.Models
{
    public class LearningResultCertificateSearchViewModel
    {
        [ResourceDisplayName("Common_Label_Education_Level")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "OnEducationLevelChange(this)")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("Common_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("Common_Label_PupilCode")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("Common_Label_PupilName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilName { get; set; }

        public int Semester { get; set; }
    }
}