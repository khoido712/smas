﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LearningResultReportArea.Models
{
    public class LearningResultCertificateListViewModel
    {
        public int PupilOfClassID { get; set; }
        [ResourceDisplayName("Common_Label_PupilCode2")]
        public string PupilCode { get; set; }
        [ResourceDisplayName("Common_Label_PupilName")]
        public string PupilName { get; set; }
        [ResourceDisplayName("Common_Label_Genre")]
        public string Genre { get; set; }
        [ResourceDisplayName("Common_Label_Class")]
        public string ClassName { get; set; }
        [ResourceDisplayName("Common_Label_BirthDay")]
        public DateTime BirthDay { get; set; }
    }
}