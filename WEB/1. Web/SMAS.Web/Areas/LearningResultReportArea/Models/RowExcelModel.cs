﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LearningResultReportArea.Models
{
    public class RowExcelModel
    {
        public int RowId { get; set; }
        public string RowName { get; set; }
    }
}