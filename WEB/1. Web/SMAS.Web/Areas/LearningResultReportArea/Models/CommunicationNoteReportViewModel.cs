﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningResultReportArea.Models
{
    public class CommunicationNoteReportViewModel
    {
        [ResourceDisplayName("Common_Label_Semester")]
        [DataType("Combobox")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_SEMESTER)]
        [AdditionalMetadata("Placeholder", "null")]
        public int? Semester { get; set; }

        [ResourceDisplayName("Common_Label_Month")]
        [DataType("Combobox")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_MONTH_ID)]
        [AdditionalMetadata("Placeholder", "null")]
        public int? MonthID { get; set; }

        [ResourceDisplayName("Common_Label_Education_Level")]
        [DataType("Combobox")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("Common_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LearningResultReportConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Class { get; set; }
        [ScaffoldColumn(false)]
        public int TypeExport { get; set; }
        [ScaffoldColumn(false)]
        public string MonthName { get; set; }
    }
}