﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LearningResultReportArea.Models
{
    public class ReportPatternViewModel
    {
        public int ReportPatternID { get; set; }
        public string ReportPatternName { get; set; }
    }
}