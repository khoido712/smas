﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LearningResultReportArea.Models
{
    public class SearchViewModel
    {
        public int SemesterID { get; set; }
        public int? PeriodID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? ClassID { get; set; }
        public string ClassName { get; set; }
        public int? SubjectID { get; set; }
        public int? PupilID { get; set; }
        public int PaperSize { get; set; }
        public string PupilCode { get; set; }
        public string PupilName { get; set; }
        public bool? chkChecked { get; set; }
        public int ViewBooklet { get; set; }
        public bool chkAllPupil { get; set; }
        public int PupilNumber { get; set; }
        public bool isGVBM { get; set; }
        public bool isBorderMark { get; set; }
        public bool chkAllSubject { get; set; }
        public string arrSubjectID { get; set; }
        public bool rdo1 { get; set; }
        public bool rdo2 { get; set; }
        public bool chkPRAddress { get; set; }
        public bool chkTRAddress { get; set; }
        public bool chkVillageID { get; set; }
        public bool chkCommuneID { get; set; }
        public bool chkDistrictID { get; set; }
        public bool chkProvinceID { get; set; }
        public int currentYear { get; set; }
        public int paging { get; set; }
        public int ReportPatternID { get; set; }
    }
}