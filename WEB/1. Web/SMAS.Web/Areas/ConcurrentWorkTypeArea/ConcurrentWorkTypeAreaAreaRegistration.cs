﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ConcurrentWorkTypeArea
{
    public class ConcurrentWorkTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ConcurrentWorkTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ConcurrentWorkTypeArea_default",
                "ConcurrentWorkTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
