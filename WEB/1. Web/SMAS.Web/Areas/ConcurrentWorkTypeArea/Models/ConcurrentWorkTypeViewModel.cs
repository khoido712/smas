/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ConcurrentWorkTypeArea.Models
{
    public class ConcurrentWorkTypeViewModel
    {
        [ScaffoldColumn(false)]
        public int ConcurrentWorkTypeID { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<int> SchoolID { get; set; }

        [ResourceDisplayName("ConcurrentWorkType_Control_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Resolution { get; set; }

        [ResourceDisplayName("ConcurrentWorkType_Control_SectionPerWeek")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumberInt")]
        public int SectionPerWeek { get; set; }

        [ResourceDisplayName("ConcurrentWorkType_Control_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}
