﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv
* @version $Revision: $
*/
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.ConcurrentWorkTypeArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.ConcurrentWorkTypeArea.Controllers
{
    public class ConcurrentWorkTypeController : BaseController
    {
        private readonly IConcurrentWorkTypeBusiness _concurrentWorkTypeBusiness;
        private readonly IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness;

        public ConcurrentWorkTypeController(IConcurrentWorkTypeBusiness concurrentWorkTypeBusiness, IConcurrentWorkAssignmentBusiness concurrentWorkAssignmentBusiness)
        {
            this._concurrentWorkTypeBusiness = concurrentWorkTypeBusiness;
            this.ConcurrentWorkAssignmentBusiness = concurrentWorkAssignmentBusiness;
        }

        public ActionResult Index()
        {
            SetViewDataPermission("ConcurrentWorkType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> searchInfo = new Dictionary<string, object>();
            searchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            searchInfo["IsActive"] = true;
            IEnumerable<ConcurrentWorkTypeViewModel> lst = _Search(searchInfo);
            ViewData[ConcurrentWorkTypeConstants.LIST_CONCURRENTWORKTYPE] = lst;
            return View();
        }

        [SkipCheckRole]
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> searchInfo = new Dictionary<string, object>();
            searchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            searchInfo["IsActive"] = true;
            searchInfo["Resolution"] = frm.Resolution;
            IEnumerable<ConcurrentWorkTypeViewModel> lst = _Search(searchInfo);
            ViewData[ConcurrentWorkTypeConstants.LIST_CONCURRENTWORKTYPE] = lst;
            return PartialView("_List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ConcurrentWorkType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ConcurrentWorkType concurrentworktype = new ConcurrentWorkType();
            concurrentworktype.SchoolID = new GlobalInfo().SchoolID;
            TryUpdateModel(concurrentworktype);
            concurrentworktype.IsActive = true;

            Utils.Utils.TrimObject(concurrentworktype);

            _concurrentWorkTypeBusiness.Insert(concurrentworktype);
            _concurrentWorkTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int concurrentworktypeid)
        {
            this.CheckPermissionForAction(concurrentworktypeid, "ConcurrentWorkType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ConcurrentWorkType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ConcurrentWorkType concurrentworktype = _concurrentWorkTypeBusiness.Find(concurrentworktypeid);
            TryUpdateModel(concurrentworktype);

            Utils.Utils.TrimObject(concurrentworktype);
            _concurrentWorkTypeBusiness.Update(concurrentworktype);
            _concurrentWorkTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "ConcurrentWorkType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ConcurrentWorkType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ConcurrentWorkTypeID",id},
                {"IsActive",true}
            };
            List<ConcurrentWorkAssignment> lstCWA = ConcurrentWorkAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            if (lstCWA.Count > 0)
            {
                throw new BusinessException("Validate_ConcurrentWorkAssignment");
            }
            _concurrentWorkTypeBusiness.Delete(id);
            _concurrentWorkTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        private IEnumerable<ConcurrentWorkTypeViewModel> _Search(IDictionary<string, object> searchInfo)
        {
            SetViewDataPermission("ConcurrentWorkType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<ConcurrentWorkType> query = _concurrentWorkTypeBusiness.Search(searchInfo);
            IQueryable<ConcurrentWorkTypeViewModel> lst = query.Select(o => new ConcurrentWorkTypeViewModel
            {
                ConcurrentWorkTypeID = o.ConcurrentWorkTypeID,
                SchoolID = o.SchoolID,
                Resolution = o.Resolution,
                SectionPerWeek = o.SectionPerWeek,
                Description = o.Description
            });
            return lst.ToList().OrderBy(o=>o.Resolution);
        }
    }
}
