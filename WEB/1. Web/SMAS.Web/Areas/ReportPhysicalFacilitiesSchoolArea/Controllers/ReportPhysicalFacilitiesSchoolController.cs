﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportPhysicalFacilitiesSchoolArea.Controllers
{
    public class ReportPhysicalFacilitiesSchoolController : BaseController
    {
        private IIndicatorBusiness IndicatorBusiness;
        private IIndicatorDataBusiness IndicatorDataBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private IProcessedReportBusiness ProcessedReportBusiness;
        public ReportPhysicalFacilitiesSchoolController(IIndicatorBusiness IndicatorBusiness, 
            IIndicatorDataBusiness IndicatorDataBusiness, 
            IAcademicYearBusiness AcademicYearBusiness,
            IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.IndicatorBusiness = IndicatorBusiness;
            this.IndicatorDataBusiness = IndicatorDataBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }

        public ActionResult Index()
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString(); ;
            SetViewDataPermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            int period = IndicatorBusiness.GetCurrentPeriodReport(_globalInfo.AcademicYearID.Value);
            List<SelectListItem> listPeriod = new List<SelectListItem>();
            listPeriod.Add(new SelectListItem { Text = Res.Get("Lbl_First_Semester"), Value = "1" });
            listPeriod.Add(new SelectListItem { Text = Res.Get("Lbl_Mid_Semester"), Value = "2" });
            listPeriod.Add(new SelectListItem { Text = Res.Get("Lbl_End_Semester"), Value = "3" });
            ViewData[ReportPhysicalFacilitiesSchoolConstant.PERIOD_ID] = period;
            ViewData[ReportPhysicalFacilitiesSchoolConstant.LIST_REIOD] = listPeriod;
            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(FormCollection fr)
        {
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            int periodId = fr["PeriodID"] != null ? Int32.Parse(fr["PeriodID"]) : 0;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            string periodname = this.GetNameByPeriodId(periodId);
            PhysicalFacilitiesBO entity = new PhysicalFacilitiesBO();
            entity.SchoolID = _globalInfo.SchoolID.Value;
            entity.Year = aca.Year;
            entity.Period = periodId;
            string fileName = string.Format("Truong_BCCSVC_{0}.xls", Utils.Utils.StripVNSignAndSpace(periodname));
            Stream excel = this.GetExport(periodId, periodname);
            processedReport = IndicatorBusiness.InsertFacilitiesReport(entity, excel, fileName);
            excel.Close();
            processedReport = IndicatorBusiness.GetReportIndicator(entity);
            if (processedReport != null)
            {
                type = JsonReportMessage.OLD;
            }
            //if (type == JsonReportMessage.NEW)
            //{
            //    string fileName = string.Format("Truong_BCCSVC_{0}.xls", Utils.Utils.StripVNSignAndSpace(periodname));
            //    Stream excel = this.GetExport(periodId, periodname);
            //    processedReport = IndicatorBusiness.InsertFacilitiesReport(entity, excel, fileName);
            //    excel.Close();
            //}
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection fr)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int periodId = fr["PeriodID"] != null ? Int32.Parse(fr["PeriodID"]) : 0;
            string periodname = this.GetNameByPeriodId(periodId);
            Stream excel = this.GetExport(periodId, periodname, _globalInfo.AppliedLevel);
            PhysicalFacilitiesBO entity = new PhysicalFacilitiesBO();
            entity.SchoolID = _globalInfo.SchoolID.Value;
            entity.Year = aca.Year;
            entity.Period = periodId;
            string fileName = string.Format("Truong_BCCSVC_{0}.xls", Utils.Utils.StripVNSignAndSpace(periodname));
            if (_globalInfo.AppliedLevel > 3) 
            {
                fileName = string.Format("Truong_BCCSVC_MN_{0}.xls", Utils.Utils.StripVNSignAndSpace(periodname));
            }
            ProcessedReport processedReport = IndicatorBusiness.InsertFacilitiesReport(entity, excel, fileName);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            string fileName = processedReport.ReportName;
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        private Stream GetExport(int periodId, string periodname, int? levelApplied = 0)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            var templateName = SystemParamsInFile.TEMPLATE_NAME_PHYCICAL_FACILITIES;
            if (levelApplied != null && levelApplied > 3)
            {
                templateName = SystemParamsInFile.TEMPLATE_NAME_PHYCICAL_FACILITIES_PRIMARY;
            }
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, templateName);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            List<PhysicalFacilitiesBO> listResult = new List<PhysicalFacilitiesBO>();
            if (periodId > 0)
            {
                listResult = IndicatorDataBusiness.GetListIndicatorData(_globalInfo.ProvinceID.Value, periodId, aca.SchoolID, aca.Year, levelApplied);
            }
            foreach (var item in listResult)
            {
                item.CellAddress = !string.IsNullOrEmpty(item.CellAddress) ? item.CellAddress.Replace("MN_",""): item.CellAddress;
            } 
            #region Fill thong tin chung
            //Tieu de bao cao
            string title = string.Format(Res.Get("Lbl_Title_Report_Physical_Facilites"), periodname.ToLower(), aca.DisplayTitle);
            sheet.SetCellValue("A4", title);
            //Ten truong
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            #endregion

            int iresult = 0;
            bool isInteger = true;
            for (int i = 0; i < listResult.Count; i++)
            {
                if (!string.IsNullOrEmpty(listResult[i].CellValue))
                {
                    isInteger = Int32.TryParse(listResult[i].CellValue, out iresult);
                    if (isInteger)
                    {
                        sheet.SetCellValue(listResult[i].CellAddress, Int32.Parse(listResult[i].CellValue));
                    }
                    else
                    {
                        sheet.SetCellValue(listResult[i].CellAddress, null);
                    }
                }
                else
                {
                    sheet.SetCellValue(listResult[i].CellAddress, null);
                }
            }
            Stream excel = oBook.ToStream();
            return excel;
        }
        private string GetNameByPeriodId(int periodId)
        {
            string periodName = string.Empty;
            if (periodId == SystemParamsInFile.FIRST_PERIOD)
            {
                periodName = Res.Get("Lbl_First_Semester");
            }
            else if (periodId == SystemParamsInFile.MID_PERIOD)
            {
                periodName = Res.Get("Lbl_Mid_Semester");
            }
            else if (periodId == SystemParamsInFile.END_PERIOD)
            {
                periodName = Res.Get("Lbl_End_Semester");
            }
            return periodName;
        }
    }
}
