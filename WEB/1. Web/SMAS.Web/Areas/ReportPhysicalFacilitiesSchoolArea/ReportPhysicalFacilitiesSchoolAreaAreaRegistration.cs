﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportPhysicalFacilitiesSchoolArea
{
    public class ReportPhysicalFacilitiesSchoolAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportPhysicalFacilitiesSchoolArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportPhysicalFacilitiesSchoolArea_default",
                "ReportPhysicalFacilitiesSchoolArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
