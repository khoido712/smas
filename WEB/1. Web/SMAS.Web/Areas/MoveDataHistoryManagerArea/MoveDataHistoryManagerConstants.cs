﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MoveDataHistoryManagerArea
{
    public class MoveDataHistoryManagerConstants
    {
        public const string LS_SchoolProfile = "ListSchoolProfile";
       
       
        public const string LS_Province = "ListProvince";
        public const string NBR_Province = "NombreProvince";
               
        public const string LS_District = "ListDistrict";
       
        public const string NBR_District = "NombreDistrict";
       
        public const string SchoolID = "SchoolProfileID";
        public const string LS_EDUCATIONGRADE = "ListEducationGrade";
        public const string ADMIN_SCHOOL = "AdminSchool";
        public const string LIST_GRADE = "LIST_GRADE";
        public const string INDEX_DISTRICT = "INDEX_DISTRICT";

        public const string SEARCH_EDUCATIONGRADE = "SEARCH_EDUCATIONGRADE";
        public const string SEARCH_PROVINCE = "SEARCH_PROVINCE";
        public const string SEARCH_DISTRICT = "SEARCH_DISTRICT";
        public const string SEARCH_SCHOOLNAME = "SEARCH_SCHOOLNAME";
        public const string SEARCH_SCHOOLCODE = "SEARCH_SCHOOLCODE";
        public const string SEARCH_ADMINISTRATOR = "SEARCH_ADMINISTRATOR";
        public const string SEARCH_YEAR = "SEARCH_YEAR";
        public const string SEARCH_ORDERBY = "_SEARCH_ORDERBY_";
        public const string SEARCH_PAGE = "_SEARCH_PAGE_";
        public const string LIST_SEMESTER = "LIST_SEMESTER";


        /// <summary>
        /// danh sach nam hoc
        /// </summary>
        public const string LS_YEAR = "LS_YEAR";
        
    }
}