﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.MoveDataHistoryManagerArea.Models
{
    public class MoveDataHistoryManagerSearch
    {
        [ResourceDisplayName("SchoolProfile_Label_EducationGrade")]
        public Nullable<int> EducationGrade { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Province")]
        public Nullable<int> Province { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_District")]
        public Nullable<int> District { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolProfileName")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolProfileCode")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SchoolCode { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Administrator")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Administrator { get; set; }
        public int YearId { get; set; }

        public int SemesterId { get; set; }
    }
}