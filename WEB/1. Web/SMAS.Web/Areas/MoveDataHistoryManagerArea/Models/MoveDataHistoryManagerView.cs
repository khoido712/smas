﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Security;
using SMAS.Models.CustomAttribute;


namespace SMAS.Web.Areas.MoveDataHistoryManagerArea.Models
{
    public class MoveDataHistoryManagerView
    {
        public int SchoolProfileID { get; set; }
        public string Administrator { get; set; }
        public int EducationGradeSchool { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string CommuneName { get; set; }
        public string txtSchoolName { get; set; }
        public string txtSchoolCode { get; set; }
        public int EducationGrade { get; set; }
              
      
        public string SupervisingDeptName { get; set; }        
        public string Resolution { get; set; }
        public string txtUserName { get; set; }
        public int AcademicYearId { get; set; }
        public int Year { get; set; }

    }
}
