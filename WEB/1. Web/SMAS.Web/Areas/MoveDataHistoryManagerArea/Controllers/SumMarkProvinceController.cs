﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Data.Objects.SqlClient;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using SMAS.Web.Areas.MoveDataHistoryManagerArea.Models;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.DAL.Repository;
using System.Threading;
using System.Globalization;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Filter;
using SMAS.Business.BusinessObject;


namespace SMAS.Web.Areas.MoveDataHistoryManagerArea.Controllers
{
    public class SumMarkProvinceController : BaseController
    {
        //
        // GET: /MoveDataHistoryManagerArea/MoveDataHistoryManager/
        #region private member variable
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAreaBusiness AreaBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;

        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly Iaspnet_UsersBusiness aspnet_UsersBusiness;

        private readonly Iaspnet_MembershipBusiness aspnet_MembershipBusiness;
        private readonly IThreadMovedDataBusiness ThreadMovedDataBusiness;
        private const int COUNT_PAGE = 1; //Biến phân trang
        private const string SSK_SEARCH_TERM_PAGE = "_PAGE_";
        private const string SSK_SEARCH_TERM_ORDERBY = "_ORDERBY_";
        #endregion

        #region Constructors
        public SumMarkProvinceController(ISchoolProfileBusiness schoolprofileBusiness, IAreaBusiness areabusiness, IProvinceBusiness provincebusiness,
            IDistrictBusiness districtbusiness, ICommuneBusiness communeBusiness, ISupervisingDeptBusiness supervisingDeptBusiness,
            ITrainingTypeBusiness trainingTypeBusiness, ISchoolTypeBusiness schoolTypeBusiness,
            IUserAccountBusiness userAccountBusiness, Iaspnet_UsersBusiness Aspnet_UsersBusiness
            , Iaspnet_MembershipBusiness aspnet_MembershipBusiness,
            IThreadMovedDataBusiness _ThreadMovedDataBusiness
            )
        {

            this.SchoolProfileBusiness = schoolprofileBusiness;
            this.AreaBusiness = areabusiness;
            this.ProvinceBusiness = provincebusiness;
            this.DistrictBusiness = districtbusiness;
            this.CommuneBusiness = communeBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.aspnet_UsersBusiness = Aspnet_UsersBusiness;
            this.aspnet_MembershipBusiness = aspnet_MembershipBusiness;
            this.ThreadMovedDataBusiness = _ThreadMovedDataBusiness;

        }
        #endregion

        public ActionResult Index()
        {

            SetViewDataSchoolProfile();
            return View();

        }

        #region Tìm kiếm trường

        [ValidateAntiForgeryToken]
        [HttpPost]
        public PartialViewResult Search(MoveDataHistoryManagerSearch SearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            SetViewDataPermission("MoveDataHistoryManager", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            GlobalInfo global = new GlobalInfo();
            Paginate<MoveDataHistoryManagerView> paging = this._Search(SearchForm, page, orderBy);
            ViewData[MoveDataHistoryManagerConstants.LS_SchoolProfile] = paging;
            ViewData[MoveDataHistoryManagerConstants.SEARCH_ADMINISTRATOR] = SearchForm.Administrator;
            ViewData[MoveDataHistoryManagerConstants.SEARCH_PROVINCE] = SearchForm.Province;
            ViewData[MoveDataHistoryManagerConstants.SEARCH_DISTRICT] = SearchForm.District;
            ViewData[MoveDataHistoryManagerConstants.SEARCH_YEAR] = SearchForm.YearId;
            ViewData[MoveDataHistoryManagerConstants.SEARCH_EDUCATIONGRADE] = SearchForm.EducationGrade;
            return PartialView("_ListSchoolProfile");
        }

        public JsonResult AjaxLoadingDistrict(int? provinceId)
        {
            IEnumerable<District> lst = new List<District>();
            if (provinceId != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ProvinceID"] = provinceId;
                dic["IsActive"] = true;
                lst = this.DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName).ToList();
            }
            if (lst == null)
                lst = new List<District>();
            return Json(new SelectList(lst, "DistrictID", "DistrictName"));
        }

        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(MoveDataHistoryManagerSearch SchoolProfileSearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            Paginate<MoveDataHistoryManagerView> paging = this._Search(SchoolProfileSearchForm, page, orderBy);
            GridModel<MoveDataHistoryManagerView> gm = new GridModel<MoveDataHistoryManagerView>();
            gm.Total = paging.total;
            gm.Data = paging.List;
            return View(gm);
        }

        #endregion

        #region "Save"
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public JsonResult SumMark(FormCollection frm)
        {
            string userName = frm["hdUserName"].ToString();
            int provinceId = Business.Common.Utils.GetInt(frm["hdProvince"], 0);
            int districtId = Business.Common.Utils.GetInt(frm["hdDistrict"], 0);
            int yearId = Business.Common.Utils.GetInt(frm["hdYearId"], 0);
            int educationGade = Business.Common.Utils.GetInt(frm["hdEducationGrade"], 0);
            int semester = Business.Common.Utils.GetInt(frm["hdSemester"], 0);
            MoveDataHistoryManagerSearch objSearch = new MoveDataHistoryManagerSearch
            {
                Administrator = userName,
                Province = provinceId,
                District = districtId,
                YearId = yearId,
                EducationGrade = educationGade,
            };


            int checkAll = 0;
            if (frm.GetValues("chkAll") != null && "on".Equals(frm["chkAll"]))
            {
                checkAll = 1;
            }

            //Neu chon tat ca
            List<ThreadMovedDataBO> lstThread = null;
            if (checkAll > 0)
            {
                lstThread = Search(objSearch).ToList();
            }
            else
            {
                List<int> lstSchoolId = new List<int>();
                foreach (var item in frm.AllKeys)
                {
                    if (!item.Contains("chkSchool_") && !frm[item].Equals("on"))
                    {
                        continue;
                    }
                    string sschoolId = item.ToString().Replace("chkSchool_", "");
                    int schoolId = Business.Common.Utils.GetInt(sschoolId, 0);
                    if (schoolId > 0)
                    {
                        lstSchoolId.Add(schoolId);
                    }
                }
                lstThread = Search(objSearch).Where(sp => lstSchoolId.Contains(sp.SchoolProfileID)).ToList();
            }
            //Thuc hien insert tung bang ghi vao bang ThreadMoveData;
            if (lstThread == null || lstThread.Count == 0)
            {
                return Json(new JsonMessage("Không tìm thấy dữ liệu để trường"), GlobalConstants.TYPE_ERROR);
            }
            ThreadMovedDataBusiness.ThreadSummedMarkbyProvince(lstThread, semester);

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), GlobalConstants.TYPE_SUCCESS));
        }
        #endregion

        #region "Delete DuplicateMark"
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteMark(FormCollection frm)
        {
            string userName = frm["hdUserName"].ToString();
            int provinceId = Business.Common.Utils.GetInt(frm["hdProvince"], 0);
            int districtId = Business.Common.Utils.GetInt(frm["hdDistrict"], 0);
            int yearId = Business.Common.Utils.GetInt(frm["hdYearId"], 0);
            int educationGade = Business.Common.Utils.GetInt(frm["hdEducationGrade"], 0);
            int semester = Business.Common.Utils.GetInt(frm["hdSemester"], 0);
            MoveDataHistoryManagerSearch objSearch = new MoveDataHistoryManagerSearch
            {
                Administrator = userName,
                Province = provinceId,
                District = districtId,
                YearId = yearId,
                EducationGrade = educationGade
            };


            int checkAll = 0;
            if (frm.GetValues("chkAll") != null && "on".Equals(frm["chkAll"]))
            {
                checkAll = 1;
            }

            //Neu chon tat ca
            List<ThreadMovedDataBO> lstThread = null;
            if (checkAll > 0)
            {
                lstThread = Search(objSearch).ToList();
            }
            else
            {
                List<int> lstSchoolId = new List<int>();
                foreach (var item in frm.AllKeys)
                {
                    if (!item.Contains("chkSchool_") && !frm[item].Equals("on"))
                    {
                        continue;
                    }
                    string sschoolId = item.ToString().Replace("chkSchool_", "");
                    int schoolId = Business.Common.Utils.GetInt(sschoolId, 0);
                    if (schoolId > 0)
                    {
                        lstSchoolId.Add(schoolId);
                    }
                }
                lstThread = Search(objSearch).Where(sp => lstSchoolId.Contains(sp.SchoolProfileID)).ToList();
            }
            //Thuc hien insert tung bang ghi vao bang ThreadMoveData;
            if (lstThread == null || lstThread.Count == 0)
            {
                return Json(new JsonMessage("Không tìm thấy dữ liệu để trường"), GlobalConstants.TYPE_ERROR);
            }
            ThreadMovedDataBusiness.ThreadDeleteMarkDuplicate(lstThread, semester);

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), GlobalConstants.TYPE_SUCCESS));
        }
        #endregion

        #region SetViewData
        private void SetViewDataSchoolProfile()
        {
            SetViewDataPermission("SumMarkProvince", _globalInfo.UserAccountID, _globalInfo.IsAdmin);

            ViewData[MoveDataHistoryManagerConstants.SEARCH_EDUCATIONGRADE] = CommonList.EducationGradeSchool();
            //Nam hoc 
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            AcademicYear AcademicYear = null;
            int year = DateTime.Now.Year;
            int firstYear = 2016;
            int yearSelected = firstYear;
            for (int i = firstYear; i <= year; i++)
            {
                AcademicYear = new AcademicYear
                {
                    Year = i,
                    DisplayTitle = String.Format("{0} - {1}", i, i + 1)
                };
                lstAcademicYear.Add(AcademicYear);
            }
            if (DateTime.Now.Month >= 9)
            {
                yearSelected++;
            }
            ViewData[MoveDataHistoryManagerConstants.LS_YEAR] = new SelectList(lstAcademicYear, "Year", "DisplayTitle", yearSelected);
            // Nếu tài khoản là admin
            List<Province> lsProvince = ProvinceBusiness.All.Where(o => o.IsActive == true).OrderBy(o => o.ProvinceName).ToList();
            ViewData[MoveDataHistoryManagerConstants.LS_Province] = lsProvince; // Sử dụng ở hàm search
            // quyen huyen
            List<District> lstDictrict = new List<District>();
            ViewData[MoveDataHistoryManagerConstants.LS_District] = new SelectList(lstDictrict, "DistrictID", "DistrictName", null);

            ViewData[MoveDataHistoryManagerConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value", 1);
        }
        #endregion

        #region paginate
        /// <summary>
        /// _Search
        /// </summary>
        /// <param name="SearchForm">The search form.</param>
        /// <param name="page">The page.</param>
        /// <param name="orderBy">The order by.</param>
        /// <returns>
        /// Paginate{SchoolProfileForm}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/11/2012</date>
        private Paginate<MoveDataHistoryManagerView> _Search(MoveDataHistoryManagerSearch SearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            // Thuc hien truy van du lieu
            IQueryable<ThreadMovedDataBO> lst = Search(SearchForm);

            lst = lst.OrderBy(c => c.ProvinceName).ThenBy(c => c.SchoolName);

            // Convert du lieu sang doi tuong MoveDataHistoryManagerView
            IQueryable<MoveDataHistoryManagerView> res = lst.Select(o => new MoveDataHistoryManagerView
            {
                SchoolProfileID = o.SchoolProfileID,
                txtSchoolName = o.SchoolName,
                txtSchoolCode = o.SchoolCode,
                Administrator = o.UserName,
                txtUserName = o.UserName,
                SupervisingDeptName = o.SupervisingDeptName,
                AcademicYearId = o.AcademicYearId,
                Year = o.Year,
                EducationGrade = o.EducationGrade,
                ProvinceName = o.ProvinceName,
                DistrictName = o.DistrictName
            });

            // Thuc hien phan trang tung phan
            Paginate<MoveDataHistoryManagerView> Paging = new Paginate<MoveDataHistoryManagerView>(res);
            Paging.page = page;
            Paging.size = 15;
            //Paging.total = res.Count();
            Paging.paginate();
            return Paging;
        }

        private IQueryable<ThreadMovedDataBO> Search(MoveDataHistoryManagerSearch SearchForm)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            Utils.Utils.TrimObject(SearchForm);
            SearchInfo["SchoolName"] = SearchForm.SchoolName;
            SearchInfo["SchoolCode"] = SearchForm.SchoolCode;
            SearchInfo["ProvinceID"] = SearchForm.Province;
            SearchInfo["DistrictID"] = SearchForm.District;
            SearchInfo["EducationGrade"] = SearchForm.EducationGrade;
            SearchInfo["Administrator"] = SearchForm.Administrator;
            SearchInfo["IsActive"] = true;
            SearchInfo["YearId"] = SearchForm.YearId;
            SearchInfo["appliedLevelId"] = GlobalConstants.APPLIED_LEVEL_SECONDARY;
            // Thuc hien truy van du lieu
            IQueryable<ThreadMovedDataBO> lst = this.ThreadMovedDataBusiness.SearchSummedSchool(SearchInfo);
            return lst;
        }
        #endregion




    }
}
