﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MoveDataHistoryManagerArea
{
    public class MoveDataHistoryManagerAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MoveDataHistoryManagerArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MoveDataHistoryManagerArea_default",
                "MoveDataHistoryManagerArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
