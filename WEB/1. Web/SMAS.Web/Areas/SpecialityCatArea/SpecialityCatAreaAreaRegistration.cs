﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SpecialityCatArea
{
    public class SpecialityCatAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SpecialityCatArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SpecialityCatArea_default",
                "SpecialityCatArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
