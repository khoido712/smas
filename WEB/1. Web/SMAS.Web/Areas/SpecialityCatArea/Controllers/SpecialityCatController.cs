﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.SpecialityCatArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.SpecialityCatArea.Controllers
{
    public class SpecialityCatController : BaseController
    {        
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
		
		public SpecialityCatController (ISpecialityCatBusiness specialitycatBusiness)
		{
			this.SpecialityCatBusiness = specialitycatBusiness;
		}
		
		//
        // GET: /SpecialityCat/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here

            IEnumerable<SpecialityCatViewModel> lst = this._Search(SearchInfo);
            ViewData[SpecialityCatConstants.LIST_SPECIALITYCAT] = lst;
            return View();
        }

		//
        // GET: /SpecialityCat/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;


			//add search info
			//

            IEnumerable<SpecialityCatViewModel> lst = this._Search(SearchInfo);
            ViewData[SpecialityCatConstants.LIST_SPECIALITYCAT] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            SpecialityCat specialitycat = new SpecialityCat();
            TryUpdateModel(specialitycat); 
            Utils.Utils.TrimObject(specialitycat);
            specialitycat.IsActive = true;

            this.SpecialityCatBusiness.Insert(specialitycat);
            this.SpecialityCatBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int SpecialityCatID)
        {
            SpecialityCat specialitycat = this.SpecialityCatBusiness.Find(SpecialityCatID);
            TryUpdateModel(specialitycat);
            Utils.Utils.TrimObject(specialitycat);
            this.SpecialityCatBusiness.Update(specialitycat);
            this.SpecialityCatBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.SpecialityCatBusiness.Delete(id);
            this.SpecialityCatBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<SpecialityCatViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<SpecialityCat> query = this.SpecialityCatBusiness.Search(SearchInfo);
            IQueryable<SpecialityCatViewModel> lst = query.Select(o => new SpecialityCatViewModel {               
						SpecialityCatID = o.SpecialityCatID,								
						Resolution = o.Resolution,								
						Description = o.Description,								
            });

            return lst.OrderBy(o=>o.Resolution).ToList();
        }        
    }
}





