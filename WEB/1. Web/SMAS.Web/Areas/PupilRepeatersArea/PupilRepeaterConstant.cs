﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilRepeatersArea
{
    public class PupilRepeaterConstant
    {
        public const int Primary = 1;
        public const int Secondary = 2;
        public const int Tertiary = 3;
        public const string LIST_EDUCATIONLEVEL = "List_EducationLevel";
        public const int SHEET_NUMBER_PRIMARY = 4;
        public const int SHEET_NUMBER_SECONDARY = 3;
    }
}