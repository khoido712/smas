﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilRepeatersArea
{
    public class PupilRepeatersAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilRepeatersArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilRepeatersArea_default",
                "PupilRepeatersArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
