﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using log4net;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.PupilRepeatersArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.PupilRepeatersArea.Controllers
{
    public class PupilRepeatersController : BaseController
    {
        //
        // GET: /PupilRepeatersArea/PupilRepeaters/
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IProcessedCellDataBusiness ProcessedCellDataBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public PupilRepeatersController(IPupilRankingBusiness PupilRankingBusiness, IProcessedCellDataBusiness ProcessedCellDataBusiness, IProcessedReportBusiness ProcessedReportBusiness, IEducationLevelBusiness EducationLevelBusiness, IClassProfileBusiness ClassProfileBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.ProcessedCellDataBusiness = ProcessedCellDataBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        public ActionResult Index()
        {
            SetViewDefault();
            return View();
        }

        private void SetViewDefault()
        {
            int EducationGrade = _globalInfo.AppliedLevel.Value;
            List<EducationLevel> ListEducationLevel = null;
            IDictionary<string, object> dic = new Dictionary<string, object> { };
            dic.Add("IsActive", true);
            dic.Add("Grade", EducationGrade);
            ListEducationLevel = EducationLevelBusiness.Search(dic).ToList();

            List<SelectListItem> list = new List<SelectListItem>();
            if (ListEducationLevel.Count > 0 && ListEducationLevel != null)
            {
                for (int i = 0; i < ListEducationLevel.Count - 1; i++)
                {
                    list.Add(new SelectListItem { Text = ListEducationLevel[i].Resolution, Value = ListEducationLevel[i].EducationLevelID.ToString() });
                }
            }
            ViewData[PupilRepeaterConstant.LIST_EDUCATIONLEVEL] = list;
        }

        public JsonResult AjaxLoadClass(int EducationLevel)
        {
            if (EducationLevel > 0)
            {
                List<ClassProfile> lsCP = GetClassFromEducationLevel(EducationLevel).OrderBy(o => o.DisplayName).ToList();
                return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        private IQueryable<ClassProfile> GetClassFromEducationLevel(int EducationLevel)
        {
            return ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"EducationLevelID",EducationLevel},
                {"IsVNEN",true}
            });
        }

        public JsonResult ExportExcel(ViewModel model)
        {
            string ReportCode = GeneralReportCode(model.EducationLevel, model.ClassName);
            PupilRanking PupilRainkingObj = new PupilRanking();
            PupilRainkingObj.SchoolID = _globalInfo.SchoolID.Value;
            PupilRainkingObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
            PupilRainkingObj.Semester = _globalInfo.Semester.Value;
            PupilRainkingObj.EducationLevelID = model.EducationLevel;
            PupilRainkingObj.ClassID = model.ClassID;
            //er.Year = ProcessedCellDataBusiness.GetYear(_global.AcademicYearID.Value).Value;
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = PupilRankingBusiness.GetProcessedReport(PupilRainkingObj, ReportCode);

            if (processedReport != null)
            {
                type = JsonReportMessage.OLD;
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = CreateDataExcel(model.EducationLevel, model.ClassID);
                processedReport = PupilRankingBusiness.InsertProcessReport(PupilRainkingObj, excel, ReportCode);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        public JsonResult GetNewReport(ViewModel model)
        {
            //string CodeDefinition = GetCodeDefinition();
            string ReportCode = GeneralReportCode(model.EducationLevel, model.ClassName);//lay ten file excel de lam reportcode      
            PupilRanking PupilRainkingObj = new PupilRanking();
            PupilRainkingObj.SchoolID = _globalInfo.SchoolID.Value;
            PupilRainkingObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
            PupilRainkingObj.Semester = _globalInfo.Semester.Value;
            PupilRainkingObj.EducationLevelID = model.EducationLevel;

            ProcessedReport processedReport = PupilRankingBusiness.GetProcessedReport(PupilRainkingObj, ReportCode);
            Stream excel = CreateDataExcel(model.EducationLevel, model.ClassID);
            processedReport = PupilRankingBusiness.InsertProcessReport(PupilRainkingObj, excel, ReportCode);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport, ViewModel model)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"SchoolID", _globalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
               Session["ReportCode"].ToString()
            };

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName + ".xls";

            return result;
        }

        public Stream CreateDataExcel(int EducationLevel, int ClassID)
        {
            //string ReportCode = "";// report code la tenfile excel
            string TemplateName = GetTemplateExcel();// ten template
            int StartRow = 7;
            // ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "THI" + "/" + TemplateName + ".xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            List<PupilRepeaterBO> ListPupilRepeater = PupilRankingBusiness.GetListPupilRepeater(_globalInfo.AppliedLevel.Value,_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, EducationLevel, ClassID,true);

            if (ListPupilRepeater.Count > 0 && ListPupilRepeater != null)
            {
                List<int> ListEducationLevelID = ListPupilRepeater.Select(p => p.EducationLevelID).Distinct().OrderBy(p => p).ToList();
                if (ListEducationLevelID.Count > 0 && ListEducationLevelID != null)
                {
                    for (int i = 0; i < ListEducationLevelID.Count; i++)
                    {
                        IVTWorksheet sheet = oBook.GetSheet(i + 1);

                        AcademicYear AcademicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                        int Year = AcademicYearObj.Year;
                        int _educationlevelID = ListEducationLevelID[i];
                        List<PupilRepeaterBO> listPupil = ListPupilRepeater.Where(p => p.EducationLevelID == _educationlevelID).OrderBy(p=>p.ClassName).ThenBy(p=>p.PupilShortName).ToList();
                        if (listPupil.Count > 0 && listPupil != null)
                        {
                            for (int j = 0; j < listPupil.Count(); j++)
                            {

                                sheet.SetCellValue("A" + (StartRow + j), j + 1);
                                sheet.SetCellValue("B" + (StartRow + j), listPupil[j].PupilFullName);
                                sheet.SetCellValue("C" + (StartRow + j), listPupil[j].ClassName);
                                sheet.SetCellValue("D" + (StartRow + j), GetCapacityByID(listPupil[j].CapacityID));
                                sheet.SetCellValue("E" + (StartRow + j), GetConductByID(listPupil[j].ConductID));

                                // dinh dang file excel
                                if (((j+ 1)) % 5 == 0)
                                {
                                    sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                                    sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                                    sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                                }
                                else
                                {
                                    sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                                    sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                                    sheet.GetRange((StartRow + j), 1, (StartRow + j), 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                                }
                                if ((j+ 1) == listPupil.Count)
                                {
                                    sheet.GetRange(listPupil.Count + StartRow - 1, 1, listPupil.Count + StartRow - 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                                    sheet.GetRange(listPupil.Count + StartRow - 1, 1, listPupil.Count + StartRow - 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                                }
                                sheet.GetRange("A" + (StartRow + j), "A" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                                sheet.GetRange("B" + (StartRow + j), "B" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                                sheet.GetRange("C" + (StartRow + j), "C" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                                sheet.GetRange("D" + (StartRow + j), "D" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                                sheet.GetRange("E" + (StartRow + j), "E" + (StartRow + j)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            }
                        }

                        //mer title
                        sheet.MergeColumn(1, 2, 2);
                        sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
                        sheet.SetCellValue("A3", Res.Get("Pupil_Repeater_List_Education")+ " " + _educationlevelID + Res.Get("Pupil_Repeater_AcademicYear") + Year + " - " + (Year + 1));
                        sheet.Name = "Khoi_" + _educationlevelID;
                    }

                    #region Delete sheet
                    int sheetNumber = PupilRepeaterConstant.SHEET_NUMBER_PRIMARY;
                    if (_globalInfo.AppliedLevel >= SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY)
                    {
                        sheetNumber = PupilRepeaterConstant.SHEET_NUMBER_SECONDARY;
                }
                    for (int i = sheetNumber; i > ListEducationLevelID.Count; i--)
                    {
                        oBook.GetSheet(i).Delete();
                    }
                    #endregion

                }
            }
            else
            {
                throw new BusinessException(Res.Get("Pupil_Repeater_Empty"));
            }
            return oBook.ToStream();
        }
        private string GetCapacityByID(int CapacityID)
        {
            string Capacity = string.Empty;
            if (CapacityID == 1)
            {
                Capacity = "Giỏi";
            }
            else if (CapacityID == 2)
            {
                Capacity = "Khá";
            }
            else if (CapacityID == 3)
            {
                Capacity = "TB";
            }
            else if (CapacityID == 4)
            {
                Capacity = "Yếu";
            }
            else if (CapacityID == 5)
            {
                Capacity = "Kém";
            }
            else if (CapacityID == 6)
            {
                Capacity = "A+";
            }
            else if (CapacityID == 7)
            {
                Capacity = "A";
            }
            else if (CapacityID == 8)
            {
                Capacity = "B";
            }
            return Capacity;
        }

        private string GetConductByID(int ConductID)
        {
            string Conduct = string.Empty;
            if (ConductID == 1)
            {
                Conduct = "Đ";
            }
            else if (ConductID == 2)
            {
                Conduct = "CĐ";
            }
            else if (ConductID == 3 || ConductID == 7)
            {
                Conduct = "Tốt";
            }
            else if (ConductID == 4 || ConductID == 8)
            {
                Conduct = "Khá";
            }
            else if (ConductID == 5 || ConductID == 9)
            {
                Conduct = "TB";
            }
            else if (ConductID == 6 || ConductID == 10)
            {
                Conduct = "Yếu";
            }
            return Conduct;
        }

        private string GetTemplateExcel()
        {
            string template = string.Empty;
            if (_globalInfo.AppliedLevel == PupilRepeaterConstant.Primary)// cap1
            {
                template = "HS_TH_DSHocSinhOLaiLop_Khoi1";
            }
            else
            {
                template = "HS_THPT_DSHocSinhOLaiLop_Khoi10";
            }

            return template;
        }

        private string GeneralReportCode(int EducationLevel, string ClassName)
        {
            //HS_[TH/THCS/THPT]_ DSHocSinhOLaiLop_[Ten khoi] 
            int Grade = _globalInfo.AppliedLevel.Value;
            string ReportCode = string.Empty;
            if (EducationLevel == 6 || EducationLevel == 7 || EducationLevel == 8)
            {
                ReportCode = "HS_THCS_DSHocSinhOLaiLop_Khoi" + EducationLevel + "_" + Utils.Utils.StripVNSign(ClassName);
            }
            else if (EducationLevel == 1 || EducationLevel == 2 || EducationLevel == 3 || EducationLevel == 4)
            {
                ReportCode = "HS_TH_DSHocSinhOLaiLop_Khoi" + EducationLevel + "_" + Utils.Utils.StripVNSign(ClassName);
            }
            else if (EducationLevel == 10 || EducationLevel == 11)
            {
                ReportCode = "HS_THPT_DSHocSinhOLaiLop_Khoi" + EducationLevel + "_" + Utils.Utils.StripVNSign(ClassName);
            }
            else if (Grade == PupilRepeaterConstant.Primary)
            {
                ReportCode = "HS_TH_DSHocSinhOLaiLop";
            }
            else if (Grade == PupilRepeaterConstant.Secondary)
            {
                ReportCode = "HS_THCS_DSHocSinhOLaiLop";
            }
            else if (Grade == PupilRepeaterConstant.Tertiary)
            {
                ReportCode = "HS_THPT_DSHocSinhOLaiLop";
            }
            Session["ReportCode"] = ReportCode;
            return ReportCode;
        }
    }
}
