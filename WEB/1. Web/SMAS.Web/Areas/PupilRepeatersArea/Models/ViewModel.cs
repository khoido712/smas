﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilRepeatersArea.Models
{
    public class ViewModel
    {
        public int EducationLevel { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
    }
}