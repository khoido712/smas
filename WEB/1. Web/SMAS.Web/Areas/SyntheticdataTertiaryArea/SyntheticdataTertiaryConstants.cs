﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SyntheticdataTertiaryArea
{
    public class SyntheticdataTertiaryConstants
    {
        public const string CBO_DISTRICT = "CboDistrict";
        public const string CUR_YEAR = "CurYear";
        public const string CBO_ACADEMICYEAR = "CboAcademicYear";
        public const string CBO_EDUCATIONLEVEL = "CboEducationLevel";
        public const string CBO_TRAININGTYPE = "CboTrainingType";
        public const string CBO_SEMESTER = "CboSemester";
        public const string CBO_SUBJECT = "CboSubject";
        public const string CBO_SUBCOMMITTEE = "CboSubCommittee";
        public const string GRID_REPORT_ID = "GridReportID";
        public const string LIST_MARKSTATISTIC = "ListMarkStatistic";
        public const string LIST_TYPESTATICS = "ListTypestatics";
        public const string LIST_CONDUCTSTATISTICSOFPROVINCETERTIARY = "lstConductStatisticsOfProvinceTertiary";
        public const string GRID_REPORT_DATA = "grid_report_data";
        public const string LIST_EducationLevel = "LIST_EducationLevel";
        public const string LIST_Semester = "LIST_Semmester";
        public const string LIST_Subject = "LIST_Subject";
        public const string LIST_AcademicYear = "LIST_AcademicYear";        
    }
}