﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SyntheticdataTertiaryArea
{
    public class SyntheticdataTertiaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SyntheticdataTertiaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SyntheticdataTertiaryArea_default",
                "SyntheticdataTertiaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
