﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SyntheticdataTertiaryArea.Models
{
    public class SearchViewModel
    {
        /*public int? DistrictID { get; set; }
        public int Year { get; set; }
        public string Resolution { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EducationLevelID { get; set; }

        public int? TrainingTypeID { get; set; }


        public int SemesterID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_SubjectCatID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SubjectID { get; set; }
        public int? SubCommitteeID { get; set; }*/
        public int ReportType { get; set; }
        public int? EducationLevelID { get; set; }
        public int AcademicYearID { get; set; }
        public int Semester { get; set; }
        public int YearID { get; set; }
        public int SubjectCatID { get; set; }
        public bool IsGenre { get; set; }
        public bool IsEthnic { get; set; }
        public bool IsGenreEthnic { get; set; }

    }
}