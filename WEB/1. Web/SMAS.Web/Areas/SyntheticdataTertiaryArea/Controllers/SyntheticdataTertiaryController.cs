﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Web.Areas.SyntheticdataTertiaryArea;
using SMAS.Web.Areas.SyntheticdataTertiaryArea.Models;
using SMAS.Business.Business;
using System.Configuration;

namespace SMAS.Web.Areas.SyntheticdataTertiaryArea.Controllers
{
    public class SyntheticdataTertiaryController : Controller
    {
        //
        // GET: /SyntheticdataTertiaryArea/SyntheticdataTertiary/
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public readonly ITrainingTypeBusiness TrainingTypeBusiness;
        public readonly ISubCommitteeBusiness SubCommitteeBusiness;
        public readonly IMarkStatisticBusiness MarkStatisticBusiness;
        public readonly IEducationLevelBusiness EducationLevelBusiness;
        public readonly IProcessedReportBusiness ProcessedReportBusiness;
        public readonly ICapacityStatisticBusiness CapacityStatisticBusiness;
        public readonly IConductStatisticBusiness ConductStatisticBusiness;
        public readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;

        public SyntheticdataTertiaryController(IProcessedReportBusiness processedReportBusiness,
            IEducationLevelBusiness educationLevelBusiness, IMarkStatisticBusiness markStatisticBusiness,
            ISubCommitteeBusiness subCommitteeBusiness, IAcademicYearBusiness academicYearBusiness,
            IDistrictBusiness districtBusiness, ITrainingTypeBusiness trainingTypeBusiness, ICapacityStatisticBusiness capacityStatisticBusiness
            , IConductStatisticBusiness conductStatisticBusiness
            , ISupervisingDeptBusiness supervisingDeptBusiness
            , IAcademicYearOfProvinceBusiness academicYearOfProvinceBusiness,
            ISubjectCatBusiness subjectCatBusiness)
        {
            this.DistrictBusiness = districtBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
            this.MarkStatisticBusiness = markStatisticBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.AcademicYearOfProvinceBusiness = academicYearOfProvinceBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
        }        
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }
        public void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[SyntheticdataTertiaryConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() { 
                                                    new ViettelCheckboxList{Label="Thống kê điểm kiểm tra định kỳ", Value=1, cchecked=true, disabled= false}, 
                                                    new ViettelCheckboxList{Label="Thống kê điểm kiểm tra học kỳ", Value=2, cchecked=false, disabled= false}, 
                                                    new ViettelCheckboxList{Label="Thống kê  học lực môn", Value=3, cchecked=false, disabled= false}
                                                };
            IDictionary<string, object> DistrictsearchInfo = new Dictionary<string, object>();
            

            List<int> lstAca = this.AcademicYearBusiness.GetListYearForSupervisingDept_THPT(global.SupervisingDeptID.Value);
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            for (int i = 0; i < lstAca.Count(); i++)
            {
                AcademicYear AcademicYear = new AcademicYear();
                AcademicYear.Year = lstAca[i];
                AcademicYear.DisplayTitle = lstAca[i] + "-" + (lstAca[i] + 1);
                lstAcademicYear.Add(AcademicYear);
            }
            List<AcademicYear> ListAcademicYear = lstAcademicYear.ToList();
            // Lay nam hoc hien tai
            string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
            int curSemester = 1;
            int curYear = AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(global.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);
            ViewData[SyntheticdataTertiaryConstants.LIST_AcademicYear] = new SelectList(ListAcademicYear, "Year", "DisplayTitle", curYear);

            ViewData[SyntheticdataTertiaryConstants.LIST_Semester] = new SelectList(CommonList.Semester(), "key", "value",curSemester);

            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(o => o.Grade == SystemParamsInFile.EDUCATION_GRADE_TERTIARY).ToList();
            ViewData[SyntheticdataTertiaryConstants.LIST_EducationLevel] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

                        
            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();
            MarkStatisticsearchInfo["Year"] = curYear;
            MarkStatisticsearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_TENTH;
            MarkStatisticsearchInfo["ProvinceID"] = global.ProvinceID;
            MarkStatisticsearchInfo["SupervisingDeptID"] = global.SupervisingDeptID;
            MarkStatisticsearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            MarkStatisticsearchInfo["IsCommenting"] = 0;
            List<SubjectCatBO> ListSubject = SupervisingDeptBusiness.SearchSubject(MarkStatisticsearchInfo);
            ViewData[SyntheticdataTertiaryConstants.LIST_Subject] = new SelectList(ListSubject, "SubjectCatID", "DisplayName");    
        }
        private string GetReportCode( int reportType)
        {
            if (reportType == SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_DINH_KY)
            {
                return SystemParamsInFile.SGD_THPT_THONGKEDIEMKIEMTRADINHKY;
            }
            else if (reportType == SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_HOC_KY)
            {
                return SystemParamsInFile.SGD_THPT_THONGKEDIEMKIEMTRAHOCKY;
            }
            else if (reportType == SystemParamsInFile.AdditionReport.HLM)
            {
                return SystemParamsInFile.REPORT_SGD_THPT_ThongKeHLM;
            }
            return "";
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? academicYearID, int? educationLevelID, int? semester, int? valueRadio)
        {
            IEnumerable<SubjectCatBO> lst = new List<SubjectCatBO>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            GlobalInfo global = GlobalInfo.getInstance();
            dic["Year"] = academicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["ProvinceID"] = global.ProvinceID;
            dic["SupervisingDeptID"] = global.SupervisingDeptID;
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;

            if (valueRadio <= SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_HOC_KY)
            {
                dic["IsCommenting"] = 0;
            }
            lst = SupervisingDeptBusiness.SearchSubject(dic);
            return Json(new SelectList(lst, "SubjectCatID", "DisplayName"));

        }
        public JsonResult LoadSemester(int? reportTypeId)
        {
            GlobalInfo global = new GlobalInfo();
            string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
            int curSemester = 1;
            AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(global.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);
            if (reportTypeId <= SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_HOC_KY)
            {
                return Json(new SelectList(CommonList.Semester(), "key", "value", curSemester));
            }
            else
            {
                return Json(new SelectList(CommonList.SemesterAndAll(), "key", "value", curSemester));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult CreatedReport(SearchViewModel data)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = global.ProvinceID;
            SearchInfo["DistrictID"] = 0;
            SearchInfo["SuperVisingDeptID"] = 0;
            SearchInfo["Year"] = data.YearID;
            SearchInfo["ReportCode"] = GetReportCode(data.ReportType);
            SearchInfo["MarkType"] = data.ReportType;
            SearchInfo["Semester"] = data.Semester;
            SearchInfo["SubjectID"] = data.SubjectCatID;
            SearchInfo["EducationLevelID"] = data.EducationLevelID;
            SearchInfo["IsFemale"] = data.IsGenre;
            SearchInfo["IsEthnic"] = data.IsEthnic;
            SearchInfo["IsFemaleAndEthnic"] = data.IsGenreEthnic;
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            SearchInfo["UnitId"] = global.SupervisingDeptID;
            MarkStatisticBusiness.CreateMarkStatistic(SearchInfo);
            return Json(new { Type = "success" });
        }

        public FileResult ExportReport(int reportType, int year, int semester, int educationLevelID, int subjectId, bool isFemale, bool isEthnic, bool isFemaleEthnic)
        {
            if (subjectId == 0)
            {
                throw new Exception("Chưa khai báo môn học");
            }
            GlobalInfo global = GlobalInfo.getInstance();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = global.ProvinceID;
            SearchInfo["DistrictID"] = 0;
            SearchInfo["SuperVisingDeptID"] = 0;   
            string reportCode = GetReportCode(reportType);
            SearchInfo["ReportCode"] = reportCode;
            SearchInfo["MarkType"] = reportType;
            SearchInfo["Semester"] = semester;
            SearchInfo["SubjectID"] = subjectId;
            SearchInfo["EducationLevelID"] = educationLevelID;
            SearchInfo["IsFemale"] = isFemale;
            SearchInfo["IsEthnic"] = isEthnic;
            SearchInfo["IsFeMaleAndEthnic"] = isFemaleEthnic;
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            SearchInfo["ReportType"] = reportType;
            SearchInfo["UnitId"] = global.SupervisingDeptID;
            SearchInfo["Year"] = year;
            Stream excel = MarkStatisticBusiness.ExportMarkStatistic(SearchInfo);
            SubjectCat objSubject = SubjectCatBusiness.Find(subjectId);
            string subjectName = (objSubject == null) ? "" : Utils.Utils.StripVNSignAndSpace(objSubject.SubjectName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = String.Format("{0}_Khoi{1}_HK{2}_{3}.xls", reportCode, educationLevelID, semester,subjectName);
            result.FileDownloadName = fileName;
            return result;
        }        
      
    }
}
