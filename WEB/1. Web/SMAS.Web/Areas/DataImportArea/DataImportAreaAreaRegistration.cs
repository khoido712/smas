﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DataImportArea
{
    public class DataImportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DataImportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DataImportArea_default",
                "DataImportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
