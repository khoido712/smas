﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DataImportArea.Controllers
{
    public class DataImportController : Controller
    {
        //
        // GET: /DataImportArea/DataImport/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /DataImportArea/DataImport/Details/5


        [ValidateAntiForgeryToken]
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /DataImportArea/DataImport/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /DataImportArea/DataImport/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /DataImportArea/DataImport/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /DataImportArea/DataImport/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /DataImportArea/DataImport/Delete/5
 

        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /DataImportArea/DataImport/Delete/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        [ValidateAntiForgeryToken]
        public ActionResult getUrl(int? menuOrder)
        {
            string url = "";
            if (menuOrder == 1)
            {
                url = "/ImportTeacherArea/ImportTeacher/";
            }
            else if (menuOrder == 2)
            {
                url = "/ImportClassArea/ImportClass/";
            }
            else if (menuOrder == 3)
            {
                url = "/ImportPupilArea/ImportPupil/";
            }
            else if (menuOrder == 4)
            {
                url = "/ImportClassSubjectAndTeacherArea/ImportClassSubjectAndTeacher/";
            }
            ViewData["url"] = url;
            return PartialView("_List");
        }
    }
}
