﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class DentalViewModel
    {
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public int Status { get; set; }
        public int MonitoringBookID { get; set; }
        public DateTime? MonitoringDate { get; set; }
        public string Symptom { get; set; }
        public string Solution { get; set; }
        public string UnitName { get; set; }

        public int DentalTestID { get; set; }       
        public int? Num_Teeth_Extracted { get; set; }
        public int? Num_Dental_Filling { get; set; }
        public int? Num_Preventive_Dental_Flling { get; set; }
        public Boolean Is_Scrapt_Teeth { get; set; }
        public Boolean Is_Treatment { get; set; }
        public string Other { get; set; }
        public string Num_Dental_Filling_One { get; set; }
        public string Num_Dental_Filling_Two { get; set; }
        public string Num_Dental_Filling_Three { get; set; }
        public string Num_Dental_Filling_Four { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? ModifineDate { get; set; }

        public int LogID { get; set; }
    }
}