﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class SpinViewModel
    {
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public int Status { get; set; }
        public int MonitoringBookID { get; set; }
        public DateTime? MonitoringDate { get; set; }
        public string Symptom { get; set; }
        public string Solution { get; set; }
        public string UnitName { get; set; }

        public int SpinTestID { get; set; }
        public Boolean Deformity { get; set; }
        public string Examinate_Straight { get; set; }
        public string Examinate_Italic { get; set; }
        public string Examinate_Other { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifineDate { get; set; }

        public int LogID { get; set; }
    }
}