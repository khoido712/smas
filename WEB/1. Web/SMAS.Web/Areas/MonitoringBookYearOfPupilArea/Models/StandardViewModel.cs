﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class StandardViewModel
    {
        public int StandardID { get; set; }
        public string StandardName { get; set; }
    }
}