﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class EyeViewModel
    {
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public int Status { get; set; }
        public int MonitoringBookID { get; set; }
        public DateTime? MonitoringDate { get; set; }
        public string MonitoringDateStr { get; set; }
        public string UnitName { get; set; }

        public int? EyeTestID { get; set; }
        public int? RightEye { get; set; }
        public int? LeftEye { get; set; }
        public string RightEyeStr { get; set; }
        public string LeftEyeStr { get; set; }

        public int? RightIsEye { get; set; }
        public int? LeftIsEye { get; set; }
        public string RightIsEyeStr { get; set; }
        public string LeftIsEyeStr { get; set; }

        public decimal? RMyopic { get; set; }
        public decimal? LMyopic { get; set; }
        public decimal? RFarsightedness { get; set; }
        public decimal? LFarsightedness { get; set; }
        public decimal? RAstigmatism { get; set; }
        public decimal? LAstigmatism { get; set; }
        public string Other { get; set; }
        public bool? IsTreatment { get; set; }
        public bool? IsActive { get; set; }  
        public int LogID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifineDate { get; set; }

        public string RMyopicStr { get; set; }
        public string LMyopicStr { get; set; }
        public string RFarsightednessStr { get; set; }
        public string LFarsightednessStr { get; set; }
        public string RAstigmatismStr { get; set; }
        public string LAstigmatismStr { get; set; }
    }
}