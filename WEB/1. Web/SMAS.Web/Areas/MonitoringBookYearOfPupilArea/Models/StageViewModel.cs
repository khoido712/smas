﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class StageViewModel
    {
        public int StageID { get; set; }
        public int StageName { get; set; }
    }
}