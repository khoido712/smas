﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using System.ComponentModel;
using System.Web.Mvc;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class DentalTestViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int PupilID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? DentalTestID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MonitoringBookID { get; set; }        

        [ResourceDisplayName("DentalTest_Label_HealthTestPeriod")]
        [Required]
        public int HealthPeriodID { get; set; }

        [ResourceDisplayName("DentalTest_Label_MonitoringBookDate")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MonitoringDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool EnableToEditMonitoringDate { get; set; }

        [ResourceDisplayName("DentalTest_Label_UnitName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string UnitName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool EnableToEditUnitName { get; set; }

        [ResourceDisplayName("DentalTest_Label_Other")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Other { get; set; }

        [ResourceDisplayName("DentalTest_Label_DescriptionDentalFilling")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DescriptionDentalFilling { get; set; }

        [ResourceDisplayName("DentalTest_Label_DescriptionPreventiveDentalFilling")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DescriptionPreventiveDentalFilling { get; set; }

        public int? NumPreventiveDentalFilling { get; set; }
        public int? NumTeethExtracted { get; set; }
        public int? NumDentalFilling { get; set; }

        [ResourceDisplayName("DentalTest_Label_DescriptionTeethExtracted")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DescriptionTeethExtracted { get; set; }

        public string NumDentalFillingOne { get; set; }

        public string NumDentalFillingTwo { get; set; }

        public string NumDentalFillingThree { get; set; }

        public string NumDentalFillingFour { get; set; }

        //public int ClassID { get; set; }

        [ResourceDisplayName("DentalTest_Label_IsScraptTeeth")]
        public bool IsScraptTeeth { get; set; }
        
        [ResourceDisplayName("DentalTest_Label_IsTreatment")]
        public bool IsTreatment { get; set; }
    }
}