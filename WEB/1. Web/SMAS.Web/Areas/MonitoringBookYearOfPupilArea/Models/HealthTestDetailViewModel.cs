﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class HealthTestDetailViewModel
    {
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public string GenreName { get; set; }
        public string ClassName { get; set; }
        public string SchoolName { get; set; }
        public string Address { get; set; }
        public string HistoryYourself { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public string ProvinceName { get; set; }
        public MonitoringBook MonitoringBook { get; set; }
        public PhysicalTest PhysicalTest { get; set; }
        public EyeTest EyeTest { get; set; }
        public ENTTest ENTTest { get; set; }
        public SpineTest SpineTest { get; set; }
        public DentalTest DentalTest { get; set; }
        public OverallTest OverallTest { get; set; }
    }

    public class ObjectHealthyTest
    {
        public List<HealthTestDetailViewModel> ListObjectHealthy { get; set; }
    }
}