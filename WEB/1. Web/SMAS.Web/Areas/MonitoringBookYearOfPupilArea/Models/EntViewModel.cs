﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class EntViewModel
    {
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public int Status { get; set; }
        public int MonitoringBookID { get; set; }
        public DateTime? MonitoringDate { get; set; }
        public string MonitoringDateStr { get; set; }
        public string Symptom { get; set; }
        public string Solution { get; set; }
        public string UnitName { get; set; }

        public int? ENTTestID { get; set; }
        public int? HealthPeriodID { get; set; }
        public bool? IsDeaf { get; set; }
        public string IsDeafStr { get; set; }
        public bool? IsSick { get; set; }
        public string IsSickStr { get; set; }
        public int? RCapacityHearing { get; set; }
        public int? LCapacityHearing { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int LogID { get; set; }
    }
}