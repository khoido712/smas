﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models
{
    public class PhysicalViewModel
    {
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public int Status { get; set; }
        public int MonitoringBookID { get; set; }       
        public DateTime? MonitoringDate { get; set; }
        //public string MonitoringDateStr { get; set; }
        public string Symptom { get; set; }
        public string Solution { get; set; }
        public string UnitName { get; set; }

        public int? PhysicalTestID { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Breast { get; set; }
        public int PhysicalClassification { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifineDate { get; set; }
        public int Nutrition { get; set; }
        public string NutritionString { get; set; }
        public decimal? BMI { get; set; }

        public int LogID { get; set; }

        public string HeightStr { get; set; }
        public string WeightStr { get; set; }
        public string BreastStr { get; set; }
        public string BMIStr { get; set; }


    }
}