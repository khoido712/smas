﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models;
using SMAS.Web.Areas.MonitoringBookYearOfPupilArea;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using SMAS.Web.Constants;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using System.Text;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Controllers
{
    public class MonitoringBookYearOfPupilController : BaseController
    {
        #region Create Constructor
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IHealthPeriodBusiness HealthPeriodBusiness;
        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private readonly IPhysicalTestBusiness PhysicalTestBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassificationCriteriaDetailBusiness ClassificationCriteriaDetailBusiness;
        private readonly ISpineTestBusiness SpineTestBusiness;
        private readonly IDentalTestBusiness DentalTestBusiness;
        private readonly IOverallTestBusiness OverallTestBusiness;
        private readonly IEyeTestBusiness EyeTestBusiness;
        private readonly IENTTestBusiness ENTTestBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;

        private GlobalInfo globalInfo = GlobalInfo.getInstance();
        public MonitoringBookYearOfPupilController(
              IClassProfileBusiness ClassProfileBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IClassAssigmentBusiness ClassAssigmentBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness
            , IEmployeeBusiness EmployeeBusiness
            , IHealthPeriodBusiness HealthPeriodBusiness
            , IMonitoringBookBusiness MonitoringBookBusiness
            , IPhysicalTestBusiness PhysicalTestBusiness
            , IPupilProfileBusiness PupilProfileBusiness
            , IClassificationCriteriaDetailBusiness ClassificationCriteriaDetailBusiness
            , ISpineTestBusiness SpineTestBusiness
            , IDentalTestBusiness DentalTestBusiness
            , IOverallTestBusiness OverallTestBusiness
            , IEyeTestBusiness EyeTestBusiness
            , IENTTestBusiness ENTTestBusiness
            , IProvinceBusiness ProvinceBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassAssigmentBusiness = ClassAssigmentBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.HealthPeriodBusiness = HealthPeriodBusiness;
            this.MonitoringBookBusiness = MonitoringBookBusiness;
            this.PhysicalTestBusiness = PhysicalTestBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ClassificationCriteriaDetailBusiness = ClassificationCriteriaDetailBusiness;
            this.SpineTestBusiness = SpineTestBusiness;
            this.DentalTestBusiness = DentalTestBusiness;
            this.OverallTestBusiness = OverallTestBusiness;
            this.EyeTestBusiness = EyeTestBusiness;
            this.ENTTestBusiness = ENTTestBusiness;
            this.ProvinceBusiness = ProvinceBusiness;

        }
        #endregion

        //
        // GET: /MonitoringBookYearOfPupilArea/MonitoringBookYearOfPupil/

        public ActionResult Index(int? ClassID)
        {
            this.SetViewData(ClassID);
            return View();
        }

        # region SetViewData
        private void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();

            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }
            ViewData[MonitoringBookYearOfPupilConstant.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    //dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                                                            .OrderBy(x => x.EducationLevelID)
                                                                            .ThenBy(x => x.DisplayName).ToList();

                ViewData[MonitoringBookYearOfPupilConstant.LIST_CLASS] = listClass;
                var listCP = listClass.ToList();

                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        // Nếu không phải là QT/HT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            //dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                            List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listHead != null && listHead.Count > 0)
                            {
                                cp = listHead.First();
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            //danh sach tieu chi
            List<StandardViewModel> lstStandard = new List<StandardViewModel>();
            if (cp != null)
            {
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && cp.ClassProfileID > 0)
                {
                    lstStandard = Common.StandardVM();
                }
            }
            else
            {
                ViewData[MonitoringBookYearOfPupilConstant.CLASS_NULL] = true;
            }

            ViewData[MonitoringBookYearOfPupilConstant.LIST_STANDARD] = lstStandard;

            List<HealthPeriod> lstHealth = HealthPeriodBusiness.All.Where(x => x.IsActive == true).ToList();
            ViewData[MonitoringBookYearOfPupilConstant.LIST_HEALTH] = lstHealth;
        }
        #endregion

        #region Get Evaluation Panel
        // lấy danh sách tiêu chí
        [HttpPost]
        public PartialViewResult GetEvaluationPanel()
        {
            int SchoolID = _globalInfo.SchoolID.Value;
            List<StandardViewModel> lstStandard = Common.StandardVM();
            ViewData[MonitoringBookYearOfPupilConstant.LIST_STANDARD] = lstStandard;
            return PartialView("_ViewStandard");
        }
        #endregion

        private List<MonitoringBook> ListMonitoringBook(IDictionary<string, object> dic)
        {
            return MonitoringBookBusiness.Search(dic).ToList();
        }

        #region AjaxLoadGrid
        public PartialViewResult AjaxLoadGrid(int ClassID, int StandardID, int HealthPeriodID)
        {
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["ClassID"] = ClassID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = HealthPeriodID;
            List<MonitoringBook> lstMoniBook = ListMonitoringBook(dic);
            List<int> lstMoniBookID = lstMoniBook.Select(x => x.MonitoringBookID).ToList();
            this.SetViewDataPermission("MonitoringBookYearOfPupil", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ToList();

            // thể lực
            if (StandardID == MonitoringBookYearOfPupilConstant.PHYSICAL_VALUE)
            {
                #region
                var physicalTest = PhysicalTestBusiness.ListPhysicalTestByMonitoringBookId(lstMoniBookID);
                List<PhysicalViewModel> lstResult = GetListDataPhysical(lstPOC, lstMoniBook, physicalTest);
                ViewData[MonitoringBookYearOfPupilConstant.LIST_PHYSICAL_VM] = lstResult;

                #region nguoi cap nhat sau cung
                List<int> lstEmployeeID = new List<int>();
                PhysicalViewModel lastUpdateVM = new PhysicalViewModel();
                Employee lastUser = null;
                string tmpStr = String.Empty;
                string strLastUpdate = String.Empty;
                List<Employee> lstEmployeeChange = new List<Employee>();
                lstEmployeeID = lstResult.Where(p => p.LogID > 0).Select(p => p.LogID).Distinct().ToList();
                if (lstEmployeeID.Count > 0)
                {
                    lstEmployeeChange = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID && lstEmployeeID.Contains(x.EmployeeID)).ToList();
                }
                lastUpdateVM = lstResult.OrderByDescending(x => x.ModifineDate).FirstOrDefault();

                if (physicalTest.Count > 0)
                {
                    string lastUserName = String.Empty;

                    if (lastUpdateVM.LogID == 0)
                    {
                        lastUserName = "Quản trị trường";
                    }
                    else
                    {
                        lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdateVM.LogID).FirstOrDefault();
                        if (lastUser != null)
                        {
                            lastUserName = lastUser.FullName;
                        }
                    }

                    if (lastUpdateVM.ModifineDate.HasValue)
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.ModifineDate.Value.Hour.ToString(),
                             lastUpdateVM.ModifineDate.Value.Minute.ToString("D2"),
                             lastUpdateVM.ModifineDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }
                    else
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.CreateDate.Hour.ToString(),
                             lastUpdateVM.CreateDate.Minute.ToString("D2"),
                             lastUpdateVM.CreateDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }

                    if (physicalTest.Count > 0)
                        strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);
                    else
                        strLastUpdate = string.Empty;
                }
                #endregion

                ViewData[MonitoringBookYearOfPupilConstant.LAST_UPDATE_STRING] = strLastUpdate;
                return PartialView("_GridPhysical");
                #endregion
            }// mắt
            else if (StandardID == MonitoringBookYearOfPupilConstant.EYE_VALUE)
            {
                #region
                var eyeTest = EyeTestBusiness.ListEyeTestByMonitoringBookId(lstMoniBookID);
                List<EyeViewModel> lstResult = GetListDataEye(lstPOC, lstMoniBook, eyeTest);
                ViewData[MonitoringBookYearOfPupilConstant.LIST_EYE_VM] = lstResult;

                #region nguoi cap nhat sau cung
                List<int> lstEmployeeID = new List<int>();
                EyeViewModel lastUpdateVM = new EyeViewModel();
                Employee lastUser = null;
                string tmpStr = String.Empty;
                string strLastUpdate = String.Empty;
                List<Employee> lstEmployeeChange = new List<Employee>();
                lstEmployeeID = lstResult.Where(p => p.LogID > 0).Select(p => p.LogID).Distinct().ToList();
                if (lstEmployeeID.Count > 0)
                {
                    lstEmployeeChange = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID && lstEmployeeID.Contains(x.EmployeeID)).ToList();
                }
                lastUpdateVM = lstResult.OrderByDescending(x => x.ModifineDate).FirstOrDefault();

                if (eyeTest.Count > 0)
                {
                    string lastUserName = String.Empty;

                    if (lastUpdateVM.LogID == 0)
                    {
                        lastUserName = "Quản trị trường";
                    }
                    else
                    {
                        lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdateVM.LogID).FirstOrDefault();
                        if (lastUser != null)
                        {
                            lastUserName = lastUser.FullName;
                        }
                    }

                    if (lastUpdateVM.ModifineDate.HasValue)
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.ModifineDate.Value.Hour.ToString(),
                             lastUpdateVM.ModifineDate.Value.Minute.ToString("D2"),
                             lastUpdateVM.ModifineDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }
                    else
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.CreateDate.Hour.ToString(),
                             lastUpdateVM.CreateDate.Minute.ToString("D2"),
                             lastUpdateVM.CreateDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }

                    strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);

                }
                #endregion

                ViewData[MonitoringBookYearOfPupilConstant.LAST_UPDATE_STRING] = strLastUpdate;
                return PartialView("_GridEye");
                #endregion
            }
            // Tai mũi họng
            else if (StandardID == MonitoringBookYearOfPupilConstant.ENT_VALUE)
            {
                #region
                var entTest = ENTTestBusiness.ListENTTestByMonitoringBookId(lstMoniBookID);
                List<EntViewModel> lstResult = GetListDataEnt(lstPOC, lstMoniBook, entTest);
                ViewData[MonitoringBookYearOfPupilConstant.LIST_ENT_VM] = lstResult;

                #region nguoi cap nhat sau cung
                List<int> lstEmployeeID = new List<int>();
                EntViewModel lastUpdateVM = new EntViewModel();
                Employee lastUser = null;
                string tmpStr = String.Empty;
                string strLastUpdate = String.Empty;
                List<Employee> lstEmployeeChange = new List<Employee>();
                lstEmployeeID = lstResult.Where(p => p.LogID > 0).Select(p => p.LogID).Distinct().ToList();
                if (lstEmployeeID.Count > 0)
                {
                    lstEmployeeChange = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID && lstEmployeeID.Contains(x.EmployeeID)).ToList();
                }
                lastUpdateVM = lstResult.OrderByDescending(x => x.ModifiedDate).FirstOrDefault();

                if (entTest.Count > 0)
                {
                    string lastUserName = String.Empty;

                    if (lastUpdateVM.LogID == 0)
                    {
                        lastUserName = "Quản trị trường";
                    }
                    else
                    {
                        lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdateVM.LogID).FirstOrDefault();
                        if (lastUser != null)
                        {
                            lastUserName = lastUser.FullName;
                        }
                    }

                    if (lastUpdateVM.ModifiedDate.HasValue)
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.ModifiedDate.Value.Hour.ToString(),
                             lastUpdateVM.ModifiedDate.Value.Minute.ToString("D2"),
                             lastUpdateVM.ModifiedDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }
                    else
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.CreatedDate.Value.Hour.ToString(),
                             lastUpdateVM.CreatedDate.Value.Minute.ToString("D2"),
                             lastUpdateVM.CreatedDate.Value,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }


                    strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);

                }
                #endregion

                ViewData[MonitoringBookYearOfPupilConstant.LAST_UPDATE_STRING] = strLastUpdate;
                return PartialView("_GridEnt");
                #endregion

            }// cột sống
            else if (StandardID == MonitoringBookYearOfPupilConstant.SPINE_VALUE)
            {
                var spinTest = SpineTestBusiness.ListSpineTestByMonitoringBookId(lstMoniBookID);
                List<SpinViewModel> lstResult = GetListDataSpin(lstPOC, lstMoniBook, spinTest);
                ViewData[MonitoringBookYearOfPupilConstant.LIST_SPIN_VM] = lstResult;

                #region nguoi cap nhat sau cung
                List<int> lstEmployeeID = new List<int>();
                SpinViewModel lastUpdateVM = new SpinViewModel();
                Employee lastUser = null;
                string tmpStr = String.Empty;
                string strLastUpdate = String.Empty;
                List<Employee> lstEmployeeChange = new List<Employee>();
                lstEmployeeID = lstResult.Where(p => p.LogID > 0).Select(p => p.LogID).Distinct().ToList();
                if (lstEmployeeID.Count > 0)
                {
                    lstEmployeeChange = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID && lstEmployeeID.Contains(x.EmployeeID)).ToList();
                }
                lastUpdateVM = lstResult.OrderByDescending(x => x.ModifineDate).FirstOrDefault();

                if (spinTest.Count > 0)
                {
                    string lastUserName = String.Empty;

                    if (lastUpdateVM.LogID == 0)
                    {
                        lastUserName = "Quản trị trường";
                    }
                    else
                    {
                        lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdateVM.LogID).FirstOrDefault();
                        if (lastUser != null)
                        {
                            lastUserName = lastUser.FullName;
                        }
                    }

                    if (lastUpdateVM.ModifineDate.HasValue)
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.ModifineDate.Value.Hour.ToString(),
                             lastUpdateVM.ModifineDate.Value.Minute.ToString("D2"),
                             lastUpdateVM.ModifineDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }
                    else
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.CreateDate.Hour.ToString(),
                             lastUpdateVM.CreateDate.Minute.ToString("D2"),
                             lastUpdateVM.CreateDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }

                    strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);

                }
                #endregion

                ViewData[MonitoringBookYearOfPupilConstant.LAST_UPDATE_STRING] = strLastUpdate;
                return PartialView("_GridSpine");
            } // Răng hàm mặt
            else if (StandardID == MonitoringBookYearOfPupilConstant.DENTAL_VALUE)
            {
                var dentalTest = DentalTestBusiness.ListDentalTestByMonitoringBookId(lstMoniBookID);
                List<DentalViewModel> lstResult = GetListDataDental(lstPOC, lstMoniBook, dentalTest);
                ViewData[MonitoringBookYearOfPupilConstant.LIST_DENTAL_VM] = lstResult;

                #region nguoi cap nhat sau cung
                List<int> lstEmployeeID = new List<int>();
                DentalViewModel lastUpdateVM = new DentalViewModel();
                Employee lastUser = null;
                string tmpStr = String.Empty;
                string strLastUpdate = String.Empty;
                List<Employee> lstEmployeeChange = new List<Employee>();
                lstEmployeeID = lstResult.Where(p => p.LogID > 0).Select(p => p.LogID).Distinct().ToList();
                if (lstEmployeeID.Count > 0)
                {
                    lstEmployeeChange = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID && lstEmployeeID.Contains(x.EmployeeID)).ToList();
                }
                lastUpdateVM = lstResult.OrderByDescending(x => x.ModifineDate).FirstOrDefault();

                if (dentalTest.Count > 0)
                {
                    string lastUserName = String.Empty;

                    if (lastUpdateVM.LogID == 0)
                    {
                        lastUserName = "Quản trị trường";
                    }
                    else
                    {
                        lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdateVM.LogID).FirstOrDefault();
                        if (lastUser != null)
                        {
                            lastUserName = lastUser.FullName;
                        }
                    }

                    if (lastUpdateVM.ModifineDate.HasValue)
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.ModifineDate.Value.Hour.ToString(),
                             lastUpdateVM.ModifineDate.Value.Minute.ToString("D2"),
                             lastUpdateVM.ModifineDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }
                    else
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.CreateDate.Hour.ToString(),
                             lastUpdateVM.CreateDate.Minute.ToString("D2"),
                             lastUpdateVM.CreateDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }

                    strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);
                }
                #endregion

                ViewData[MonitoringBookYearOfPupilConstant.LAST_UPDATE_STRING] = strLastUpdate;
                return PartialView("_GridDental");
            } // Tổng quát
            else if (StandardID == MonitoringBookYearOfPupilConstant.OVERALL_VALUE)
            {
                var overAllTest = OverallTestBusiness.ListOverallTestByMonitoringBookId(lstMoniBookID);
                List<OverAllViewModel> lstResult = GetListDataOverAll(lstPOC, lstMoniBook, overAllTest);
                ViewData[MonitoringBookYearOfPupilConstant.LIST_OVERALL_VM] = lstResult;

                #region nguoi cap nhat sau cung
                List<int> lstEmployeeID = new List<int>();
                OverAllViewModel lastUpdateVM = new OverAllViewModel();
                Employee lastUser = null;
                string tmpStr = String.Empty;
                string strLastUpdate = String.Empty;
                List<Employee> lstEmployeeChange = new List<Employee>();
                lstEmployeeID = lstResult.Where(p => p.LogID > 0).Select(p => p.LogID).Distinct().ToList();
                if (lstEmployeeID.Count > 0)
                {
                    lstEmployeeChange = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID && lstEmployeeID.Contains(x.EmployeeID)).ToList();
                }

                lastUpdateVM = lstResult.OrderByDescending(x => x.ModifiedDate).FirstOrDefault();

                if (overAllTest.Count > 0)
                {
                    string lastUserName = String.Empty;

                    if (lastUpdateVM.LogID == 0)
                    {
                        lastUserName = "Quản trị trường";
                    }
                    else
                    {
                        lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdateVM.LogID).FirstOrDefault();
                        if (lastUser != null)
                        {
                            lastUserName = lastUser.FullName;
                        }
                    }

                    if (lastUpdateVM.ModifiedDate.HasValue)
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.ModifiedDate.Value.Hour.ToString(),
                             lastUpdateVM.ModifiedDate.Value.Minute.ToString("D2"),
                             lastUpdateVM.ModifiedDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }
                    else if(lastUpdateVM.CreatedDate.HasValue)
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateVM.CreatedDate.Value.Hour.ToString(),
                             lastUpdateVM.CreatedDate.Value.Minute.ToString("D2"),
                             lastUpdateVM.CreatedDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }

                    strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);

                }
                #endregion

                ViewData[MonitoringBookYearOfPupilConstant.LAST_UPDATE_STRING] = strLastUpdate;
                return PartialView("_GridOverall");
            }

            return PartialView("_GridMonitoringBook");
        }
        #endregion

        class EvaluationResult
        {
            public decimal BMI;
            public int Nutrition;
            public string NutritionName;
        }

        #region GetListDataPhysical
        private List<PhysicalViewModel> GetListDataPhysical(List<PupilOfClassBO> lstPOC, List<MonitoringBook> lstMoniBook, List<PhysicalTest> lstPhysical)
        {
            List<PhysicalViewModel> lstResultVM = new List<PhysicalViewModel>();
            PhysicalViewModel objPhysical = null;

            MonitoringBook objDB = null;
            PupilOfClassBO objPOC = null;
            int PupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objPhysical = new PhysicalViewModel();
                PupilID = objPOC.PupilID;
                objPhysical.PupilID = PupilID;
                objPhysical.PupilName = objPOC.PupilFullName;
                objPhysical.Status = objPOC.Status;
                objDB = lstMoniBook.Where(x => x.PupilID == PupilID && x.ClassID == objPOC.ClassID).FirstOrDefault();
                if (objDB != null)
                {
                    objPhysical.MonitoringBookID = objDB.MonitoringBookID;
                    var objPhy = lstPhysical.Where(x => x.MonitoringBookID == objDB.MonitoringBookID).FirstOrDefault();
                    if (objPhy != null)
                    {
                        objPhysical.PhysicalTestID = objPhy.PhysicalTestID;
                        objPhysical.Height = objPhy.Height.HasValue ? objPhy.Height : null;
                        objPhysical.HeightStr = objPhysical.Height.HasValue ? objPhysical.Height.FormatDecimal() : string.Empty;
                        objPhysical.Weight = objPhy.Weight.HasValue ? objPhy.Weight : null;
                        objPhysical.WeightStr = objPhysical.Weight.HasValue ? objPhysical.Weight.FormatDecimal() : string.Empty;
                        objPhysical.Breast = objPhy.Breast.HasValue ? objPhy.Breast : null;
                        objPhysical.BreastStr = objPhysical.Breast.HasValue ? objPhysical.Breast.FormatDecimal() : string.Empty;
                        objPhysical.LogID = objPhy.LogID.HasValue ? objPhy.LogID.Value : 0;
                        objPhysical.ModifineDate = objPhy.ModifiedDate;
                        objPhysical.CreateDate = objPhy.CreatedDate.Value;

                        if (objPhy.PhysicalClassification.HasValue)
                            objPhysical.PhysicalClassification = objPhy.PhysicalClassification.Value;

                        /*if (objPhy.Height.HasValue && objPhy.Weight.HasValue && objDB.MonitoringDate.HasValue)
                        {
                            int month = this.caculateNumberOfMonthBetweenTowDate(objPOC.Birthday, objDB.MonitoringDate.Value);
                            var result = this.DanhGiaPhanLoaiDinhDuong(month, objPOC.Genre.Value, objPhy.Height.Value, objPhy.Weight.Value);
                            objPhysical.BMI = result.BMI;
                            objPhysical.BMIStr = objPhysical.BMI.FormatDecimal();
                            objPhysical.NutritionString = result.NutritionName;
                        }*/

                        if (objPhy.Height.HasValue && objPhy.Weight.HasValue)
                        {
                            objPhysical.BMI = Math.Round(objPhy.Weight.Value / ((objPhy.Height.Value * 0.01m) * (objPhy.Height.Value * 0.01m)), 2);
                            objPhysical.BMIStr = objPhysical.BMI.FormatDecimal();
                        }

                        objPhysical.NutritionString = objPhy.Nutrition == 1 ? "Suy dinh dưỡng" : objPhy.Nutrition == 2 ? "Béo phì" : objPhy.Nutrition == 3 ? "Bình Thường" : "";
                    }
                }
                lstResultVM.Add(objPhysical);
            }

            return lstResultVM.ToList();
        }
        #endregion

        #region GetListDataSpin
        private List<SpinViewModel> GetListDataSpin(List<PupilOfClassBO> lstPOC, List<MonitoringBook> lstMoniBook, List<SpineTest> lstSpin)
        {
            List<SpinViewModel> lstResultVM = new List<SpinViewModel>();
            SpinViewModel objSpin = null;

            MonitoringBook objDB = null;
            PupilOfClassBO objPOC = null;
            int PupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objSpin = new SpinViewModel();
                PupilID = objPOC.PupilID;
                objSpin.PupilID = PupilID;
                objSpin.PupilName = objPOC.PupilFullName;
                objSpin.Status = objPOC.Status;

                objDB = lstMoniBook.Where(x => x.PupilID == PupilID && x.ClassID == objPOC.ClassID).FirstOrDefault();
                if (objDB != null)
                {

                    objSpin.MonitoringBookID = objDB.MonitoringBookID;
                    objSpin.MonitoringDate = objDB.MonitoringDate;
                    objSpin.UnitName = objDB.UnitName;
                    objSpin.Symptom = objDB.Symptoms;
                    //objPhysical.NutritionString = objDB. == 1 ? "Suy dinh dưỡng" :"Bình thường";
                    var objPhy = lstSpin.Where(x => x.MonitoringBookID == objDB.MonitoringBookID).FirstOrDefault();
                    if (objPhy != null)
                    {
                        objSpin.Deformity = objPhy.IsDeformityOfTheSpine;
                        objSpin.Examinate_Straight = objPhy.ExaminationStraight != null ? objPhy.ExaminationStraight.ToString() : "";
                        objSpin.Examinate_Italic = objPhy.ExaminationItalic != null ? objPhy.ExaminationItalic.ToString() : "";
                        objSpin.Examinate_Other = objPhy.Other != null ? objPhy.Other.ToString() : "";
                        objSpin.LogID = objPhy.LogID.HasValue ? objPhy.LogID.Value : 0;
                        objSpin.ModifineDate = objPhy.ModifiedDate;
                        objSpin.CreateDate = objPhy.CreatedDate.Value;
                    }
                }
                lstResultVM.Add(objSpin);
            }

            return lstResultVM.ToList();
        }
        #endregion

        #region GetListDataDental
        private List<DentalViewModel> GetListDataDental(List<PupilOfClassBO> lstPOC, List<MonitoringBook> lstMoniBook, List<DentalTest> lstPhysical)
        {
            List<DentalViewModel> lstResultVM = new List<DentalViewModel>();
            DentalViewModel objPhysical = null;

            MonitoringBook objDB = null;
            PupilOfClassBO objPOC = null;
            int PupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objPhysical = new DentalViewModel();
                PupilID = objPOC.PupilID;
                objPhysical.PupilID = PupilID;
                objPhysical.PupilName = objPOC.PupilFullName;
                objPhysical.Status = objPOC.Status;

                objDB = lstMoniBook.Where(x => x.PupilID == PupilID && x.ClassID == objPOC.ClassID).FirstOrDefault();
                if (objDB != null)
                {

                    objPhysical.MonitoringBookID = objDB.MonitoringBookID;
                    objPhysical.MonitoringDate = objDB.MonitoringDate;
                    objPhysical.UnitName = objDB.UnitName;
                    objPhysical.Symptom = objDB.Symptoms;
                    //objPhysical.NutritionString = objDB. == 1 ? "Suy dinh dưỡng" :"Bình thường";
                    var objPhy = lstPhysical.Where(x => x.MonitoringBookID == objDB.MonitoringBookID).FirstOrDefault();
                    if (objPhy != null)
                    {
                        objPhysical.DentalTestID = objPhy.DentalTestID;
                        objPhysical.Num_Teeth_Extracted = objPhy.NumTeethExtracted;
                        objPhysical.Num_Dental_Filling = objPhy.NumDentalFilling;
                        objPhysical.Num_Preventive_Dental_Flling = objPhy.NumPreventiveDentalFilling;
                        objPhysical.Is_Scrapt_Teeth = objPhy.IsScraptTeeth;
                        objPhysical.Is_Treatment = objPhy.IsTreatment;
                        objPhysical.Other = objPhy.Other;
                        objPhysical.Num_Dental_Filling_One = objPhy.NumDentalFillingOne;
                        objPhysical.Num_Dental_Filling_Two = objPhy.NumDentalFillingTwo;
                        objPhysical.Num_Dental_Filling_Three = objPhy.NumDentalFillingThree;
                        objPhysical.Num_Dental_Filling_Four = objPhy.NumDentalFillingFour;
                        objPhysical.LogID = objPhy.LogID.HasValue ? objPhy.LogID.Value : 0;
                        objPhysical.ModifineDate = objPhy.ModifiedDate;
                        objPhysical.CreateDate = objPhy.CreatedDate.Value;
                    }
                }
                lstResultVM.Add(objPhysical);
            }

            return lstResultVM.ToList();
        }
        #endregion

        #region GetListDataOverAll
        private List<OverAllViewModel> GetListDataOverAll(List<PupilOfClassBO> lstPOC, List<MonitoringBook> lstMoniBook, List<OverallTest> lstOverAll)
        {
            List<OverAllViewModel> lstResultVM = new List<OverAllViewModel>();
            OverAllViewModel objOverAll = null;

            MonitoringBook objDB = null;
            PupilOfClassBO objPOC = null;
            int PupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objOverAll = new OverAllViewModel();
                PupilID = objPOC.PupilID;
                objOverAll.PupilID = PupilID;
                objOverAll.PupilName = objPOC.PupilFullName;
                objOverAll.Status = objPOC.Status;

                objDB = lstMoniBook.Where(x => x.PupilID == PupilID && x.ClassID == objPOC.ClassID).FirstOrDefault();
                if (objDB != null)
                {
                    objOverAll.MonitoringBookID = objDB.MonitoringBookID;
                    objOverAll.MonitoringDate = objDB.MonitoringDate;
                    objOverAll.UnitName = objDB.UnitName;
                    objOverAll.Symptom = objDB.Symptoms;
                    objOverAll.MonitoringDate = objDB.MonitoringDate;
                    objOverAll.MonitoringDateStr = objDB.MonitoringDate != null ? objDB.MonitoringDate.Value.ToString("dd/MM/yyyy") : "";

                    var objPhy = lstOverAll.Where(x => x.MonitoringBookID == objDB.MonitoringBookID).FirstOrDefault();
                    if (objPhy != null)
                    {
                        objOverAll.OverallTestID = objPhy.OverallTestID;
                        objOverAll.LogID = objPhy.LogID.HasValue ? objPhy.LogID.Value : 0;
                        objOverAll.Doctor = objPhy.Doctor;
                        objOverAll.EvaluationHealth = objPhy.EvaluationHealth;
                        objOverAll.RequireOfDoctor = objPhy.RequireOfDoctor;
                        objOverAll.ModifiedDate = objPhy.ModifiedDate;
                        objOverAll.CreatedDate = objPhy.CreatedDate.Value;
                        objOverAll.HistoryYourSelf = objPhy.HistoryYourSelf;
                        objOverAll.OverallInternal = objPhy.OverallInternal;
                        objOverAll.DescriptionOverallInternal = objPhy.DescriptionOverallInternall;
                        objOverAll.OverallForeign = objPhy.OverallForeign;
                        objOverAll.DescriptionOverallForeign = objPhy.DescriptionForeign;
                        objOverAll.SkinDiseases = objPhy.SkinDiseases;
                        objOverAll.DescriptionSkinDiseases = objPhy.DescriptionSkinDiseases;
                        objOverAll.Other = objPhy.Other;
                        objOverAll.IsHeartDiseases = objPhy.IsHeartDiseases;
                        objOverAll.IsBirthDefect = objPhy.IsBirthDefect;
                        objOverAll.IsDredging = objPhy.IsDredging;
                    }
                }
                lstResultVM.Add(objOverAll);
            }
            return lstResultVM.ToList();
        }
        #endregion

        #region GetListDataEye
        private List<EyeViewModel> GetListDataEye(List<PupilOfClassBO> lstPOC, List<MonitoringBook> lstMoniBook, List<EyeTest> lstEye)
        {
            List<EyeViewModel> lstResultVM = new List<EyeViewModel>();
            EyeViewModel objEye = null;

            MonitoringBook objDB = null;
            PupilOfClassBO objPOC = null;
            int PupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objEye = new EyeViewModel();
                PupilID = objPOC.PupilID;
                objEye.PupilID = PupilID;
                objEye.PupilName = objPOC.PupilFullName;
                objEye.Status = objPOC.Status;
                objEye.MonitoringDate = null;
                objDB = lstMoniBook.Where(x => x.PupilID == PupilID && x.ClassID == objPOC.ClassID).FirstOrDefault();
                if (objDB != null)
                {
                    objEye.MonitoringBookID = objDB.MonitoringBookID;
                    var objEyeDB = lstEye.Where(x => x.MonitoringBookID == objDB.MonitoringBookID).FirstOrDefault();
                    if (objEyeDB != null)
                    {
                        objEye.MonitoringDate = objDB.MonitoringDate;
                        objEye.MonitoringDateStr = objDB.MonitoringDate != null ? objDB.MonitoringDate.Value.ToString("dd/MM/yyyy") : "";

                        objEye.EyeTestID = objEyeDB.EyeTestID;

                        if (objEyeDB.REye.HasValue)
                        {
                            objEye.RightEye = objEyeDB.REye;
                            objEye.RightEyeStr = objEyeDB.REye.Value.ToString();
                        }
                        else
                        {
                            objEye.RightEyeStr = "__";
                        }

                        if (objEyeDB.LEye.HasValue)
                        {
                            objEye.LeftEye = objEyeDB.LEye;
                            objEye.LeftEyeStr = objEyeDB.LEye.Value.ToString();
                        }
                        else
                        {
                            objEye.LeftEyeStr = "__";
                        }

                        if (objEyeDB.LIsEye.HasValue)
                        {
                            objEye.LeftIsEye = objEyeDB.LIsEye;
                            objEye.LeftIsEyeStr = objEyeDB.LIsEye.Value.ToString();
                        }
                        else
                        {
                            objEye.LeftIsEyeStr = "__";
                        }

                        if (objEyeDB.RIsEye.HasValue)
                        {
                            objEye.RightIsEye = objEyeDB.RIsEye;
                            objEye.RightIsEyeStr = objEyeDB.RIsEye.Value.ToString();
                        }
                        else
                        {
                            objEye.RightIsEyeStr = "__";
                        }

                        objEye.RMyopic = objEyeDB.RMyopic;
                        objEye.RMyopicStr = objEye.RMyopic.FormatDecimal();
                        objEye.LMyopic = objEyeDB.LMyopic;
                        objEye.LMyopicStr = objEye.LMyopic.FormatDecimal();
                        objEye.RFarsightedness = objEyeDB.RFarsightedness;
                        objEye.RFarsightednessStr = objEye.RFarsightedness.FormatDecimal();
                        objEye.LFarsightedness = objEyeDB.LFarsightedness;
                        objEye.LFarsightednessStr = objEye.LFarsightedness.FormatDecimal();
                        objEye.RAstigmatism = objEyeDB.RAstigmatism;
                        objEye.RAstigmatismStr = objEye.RAstigmatism.FormatDecimal();
                        objEye.LAstigmatism = objEyeDB.LAstigmatism;
                        objEye.LAstigmatismStr = objEye.LAstigmatism.FormatDecimal();
                        objEye.Other = objEyeDB.Other;
                        objEye.IsTreatment = objEyeDB.IsTreatment;
                        objEye.CreateDate = objEyeDB.CreatedDate.Value;
                        objEye.IsActive = objEyeDB.IsActive;
                        objEye.LogID = objEyeDB.LogID.HasValue ? objEyeDB.LogID.Value : 0;
                        objEye.ModifineDate = objEyeDB.ModifiedDate;
                    }
                    else
                    {
                        objEye.RightIsEyeStr = "__";
                        objEye.LeftIsEyeStr = "__";
                        objEye.LeftEyeStr = "__";
                        objEye.RightEyeStr = "__";
                    }
                }
                lstResultVM.Add(objEye);
            }

            return lstResultVM.ToList();
        }
        #endregion

        #region GetListDataEnt
        private List<EntViewModel> GetListDataEnt(List<PupilOfClassBO> lstPOC, List<MonitoringBook> lstMoniBook, List<ENTTest> lstEnt)
        {
            List<EntViewModel> lstResultVM = new List<EntViewModel>();
            EntViewModel objEnt = null;

            MonitoringBook objDB = null;
            PupilOfClassBO objPOC = null;
            int PupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objEnt = new EntViewModel();
                PupilID = objPOC.PupilID;
                objEnt.PupilID = PupilID;
                objEnt.PupilName = objPOC.PupilFullName;
                objEnt.Status = objPOC.Status;
                objEnt.MonitoringDate = null;
                objDB = lstMoniBook.Where(x => x.PupilID == PupilID && x.ClassID == objPOC.ClassID).FirstOrDefault();
                if (objDB != null)
                {
                    objEnt.MonitoringBookID = objDB.MonitoringBookID;
                    var objEntDB = lstEnt.Where(x => x.MonitoringBookID == objDB.MonitoringBookID).FirstOrDefault();
                    if (objEntDB != null)
                    {
                        objEnt.MonitoringDate = objDB.MonitoringDate;
                        objEnt.MonitoringDateStr = objDB.MonitoringDate != null ? objDB.MonitoringDate.Value.ToString("dd/MM/yyyy") : "";

                        objEnt.ENTTestID = objEntDB.ENTTestID;
                        objEnt.IsSick = objEntDB.IsSick;
                        if (objEntDB.IsSick.Value)
                            objEnt.IsSickStr = "X";
                        objEnt.IsDeaf = objEntDB.IsDeaf;
                        if (objEntDB.IsDeaf.Value)
                            objEnt.IsDeafStr = "X";
                        objEnt.Description = objEntDB.Description;
                        objEnt.CreatedDate = objEntDB.CreatedDate;
                        objEnt.ModifiedDate = objEntDB.ModifiedDate;
                        objEnt.LCapacityHearing = objEntDB.LCapacityHearing.HasValue ? objEntDB.LCapacityHearing : null;
                        objEnt.RCapacityHearing = objEntDB.RCapacityHearing.HasValue ? objEntDB.RCapacityHearing : null;
                    }
                }
                lstResultVM.Add(objEnt);
            }

            return lstResultVM.ToList();
        }
        #endregion

        #region SavePhysical
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SavePhysical(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int healthPeriodID = !string.IsNullOrEmpty(frm["HealthPeriodIDCreate"]) ? int.Parse(frm["HealthPeriodIDCreate"]) : 0;
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;

            Dictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",ClassID},
                {"HealthPeriodID", healthPeriodID},   
            };

            this.MonitoringBookBusiness.ValidatePhysicalInsert(frm, dicInsert, LogChangeID);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        #endregion

        #region SaveSpin
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSpin(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int healthPeriodID = !string.IsNullOrEmpty(frm["HealthPeriodIDCreate"]) ? int.Parse(frm["HealthPeriodIDCreate"]) : 0;
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            Dictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",ClassID},
                {"HealthPeriodID", healthPeriodID},   
            };
            this.MonitoringBookBusiness.ValidateSpineInsert(frm, dicInsert, LogChangeID);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        #endregion

        #region SaveDental
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveDental(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int healthPeriodID = !string.IsNullOrEmpty(frm["HealthPeriodIDCreate"]) ? int.Parse(frm["HealthPeriodIDCreate"]) : 0;
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            Dictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",ClassID},
                {"HealthPeriodID", healthPeriodID},   
            };
            this.MonitoringBookBusiness.ValidateDentalInsert(frm, dicInsert, LogChangeID);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        #endregion

        #region SaveOverAll
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveOverAll(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int healthPeriodID = !string.IsNullOrEmpty(frm["HealthPeriodIDCreate"]) ? int.Parse(frm["HealthPeriodIDCreate"]) : 0;
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            Dictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",ClassID},
                {"HealthPeriodID", healthPeriodID},   
            };
            this.MonitoringBookBusiness.ValidateOverAllInsert(frm, dicInsert, LogChangeID);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        #endregion

        #region SaveEye
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveEye(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int healthPeriodID = !string.IsNullOrEmpty(frm["HealthPeriodIDCreate"]) ? int.Parse(frm["HealthPeriodIDCreate"]) : 0;
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;

            Dictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",ClassID},              
                {"HealthPeriodID", healthPeriodID},   
            };

            this.MonitoringBookBusiness.ValidateEyeInsert(frm, dicInsert, LogChangeID);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        #endregion

        #region SaveEnt
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveEnt(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int healthPeriodID = !string.IsNullOrEmpty(frm["HealthPeriodIDCreate"]) ? int.Parse(frm["HealthPeriodIDCreate"]) : 0;
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;

            Dictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",ClassID},
                {"HealthPeriodID", healthPeriodID}
            };

            this.MonitoringBookBusiness.ValidateEntInsert(frm, dicInsert, LogChangeID);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        #endregion

        #region DeletePhysical
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePhysical(int ClassID, int HealthPeriodID, string arrPupilID)
        {
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID", EducationLevelID},
                {"HealthPeriodID", HealthPeriodID},         
                {"lstPupilID",lstPupilID}                
            };
            this.MonitoringBookBusiness.DeletePhysical(dic);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }
        #endregion

        #region DeleteEye
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEye(int ClassID, int HealthPeriodID, string arrPupilID)
        {
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID", EducationLevelID},
                {"HealthPeriodID", HealthPeriodID},         
                {"lstPupilID",lstPupilID}                
            };
            this.MonitoringBookBusiness.DeleteEye(dic);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }
        #endregion

        #region DeleteEnt
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEnt(int ClassID, int HealthPeriodID, string arrPupilID)
        {
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID", EducationLevelID},
                {"HealthPeriodID", HealthPeriodID},         
                {"lstPupilID",lstPupilID}                
            };
            this.MonitoringBookBusiness.DeleteEnt(dic);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }
        #endregion

        #region DeleteSpin
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSpin(int ClassID, int HealthPeriodID, string arrPupilID)
        {
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID", EducationLevelID},
                {"HealthPeriodID", HealthPeriodID},         
                {"lstPupilID",lstPupilID}                
            };
            this.MonitoringBookBusiness.DeleteSpin(dic);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }
        #endregion

        #region DeleteDental
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteDental(int ClassID, int HealthPeriodID, string arrPupilID)
        {
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID", EducationLevelID},
                {"HealthPeriodID", HealthPeriodID},         
                {"lstPupilID",lstPupilID}                
            };
            this.MonitoringBookBusiness.DeleteDental(dic);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }
        #endregion

        #region DeleteOverAll
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteOverAll(int ClassID, int HealthPeriodID, string arrPupilID)
        {
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EducationLevelID", EducationLevelID},
                {"HealthPeriodID", HealthPeriodID},         
                {"lstPupilID",lstPupilID},     
            };
            this.MonitoringBookBusiness.DeleteOverAll(dic);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }
        #endregion

        #region GetInfo
        [HttpGet]
        public ActionResult EditDentalTest(int id)
        {
            DentalTest dentalTest = this.DentalTestBusiness.Find(id);
            DentalTestViewModel model = new DentalTestViewModel();
            if (dentalTest != null)
            {
                model = new DentalTestViewModel()
                {
                    DentalTestID = dentalTest.DentalTestID,
                    PupilID = dentalTest.MonitoringBook.PupilID,
                    MonitoringBookID = dentalTest.MonitoringBookID,
                    HealthPeriodID = dentalTest.MonitoringBook.HealthPeriodID ?? 0,
                    MonitoringDate = dentalTest.MonitoringBook.MonitoringDate,
                    EnableToEditMonitoringDate = true,
                    UnitName = dentalTest.MonitoringBook.UnitName,
                    EnableToEditUnitName = true,
                    DescriptionDentalFilling = dentalTest.DescriptionDentalFilling,
                    DescriptionPreventiveDentalFilling = dentalTest.DescriptionPreventiveDentalFilling,
                    DescriptionTeethExtracted = dentalTest.DescriptionTeethExtracted,
                    IsScraptTeeth = dentalTest.IsScraptTeeth,
                    IsTreatment = dentalTest.IsTreatment,
                    NumDentalFilling = dentalTest.NumDentalFilling,
                    NumDentalFillingFour = dentalTest.NumDentalFillingFour,
                    NumDentalFillingOne = dentalTest.NumDentalFillingOne,
                    NumDentalFillingThree = dentalTest.NumDentalFillingThree,
                    NumDentalFillingTwo = dentalTest.NumDentalFillingTwo,
                    NumPreventiveDentalFilling = dentalTest.NumPreventiveDentalFilling,
                    NumTeethExtracted = dentalTest.NumTeethExtracted,
                    Other = dentalTest.Other
                };
            }
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "update";
            return PartialView("_createDentalPosition", model);
        }

        private List<HealthPeriod> getHealthPeriods()
        {
            return this.HealthPeriodBusiness.All.Where(o => o.IsActive).OrderBy(p => p.HealthPeriodID).ToList();
        }

        [HttpGet]
        public ActionResult EditOverallTest(int id)
        {
            OverallTest dentalTest = this.OverallTestBusiness.Find(id);
            OverAllViewModel model = new OverAllViewModel();
            if (dentalTest != null)
            {
                model = new OverAllViewModel()
                {
                    OverallTestID = dentalTest.OverallTestID,
                    PupilID = dentalTest.MonitoringBook.PupilID,
                    MonitoringBookID = dentalTest.MonitoringBookID,
                    HealthPeriodID = dentalTest.MonitoringBook.HealthPeriodID ?? 0,
                    MonitoringDate = dentalTest.MonitoringBook.MonitoringDate,
                    UnitName = dentalTest.MonitoringBook.UnitName,
                    HistoryYourSelf = dentalTest.HistoryYourSelf, // Tien su ban than
                    OverallInternal = dentalTest.OverallInternal, // Noi tong quat
                    DescriptionOverallInternal = dentalTest.DescriptionOverallInternall, // Mo ta noi tong quat
                    OverallForeign = dentalTest.OverallForeign, // Ngoai tong quat
                    DescriptionOverallForeign = dentalTest.DescriptionForeign, // Mo ta ngoai tong quat
                    SkinDiseases = dentalTest.SkinDiseases, // Benh ngoai da
                    DescriptionSkinDiseases = dentalTest.DescriptionSkinDiseases, // Mo ta ngoai tong quat
                    IsHeartDiseases = dentalTest.IsHeartDiseases, // Tim bam sinh
                    IsBirthDefect = dentalTest.IsBirthDefect, // Di tat bam sinh
                    Doctor = dentalTest.Doctor,
                    EvaluationHealth = dentalTest.EvaluationHealth,
                    IsDredging = dentalTest.IsDredging,
                    Other = dentalTest.Other,
                    RequireOfDoctor = dentalTest.RequireOfDoctor,
                };
            }
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "update";
            return PartialView("_createOverAll", model);
        }
        #endregion

        #region xuat excel
        public FileResult ExportHealthy(int ClassID, int HealthPeriodID, int StandardID, int TypeExport)
        {
            string templatePath = string.Empty;
            string fileName = string.Empty;
            string fileTemplate = string.Empty;
            if (TypeExport == 1 || TypeExport == 3)
            {
                switch (StandardID)
                {
                    case MonitoringBookYearOfPupilConstant.PHYSICAL_VALUE:
                        fileTemplate = string.Format("{0}.xls", SystemParamsInFile.SoTheoDoiSKNam_TheLuc_12_Dot1);
                        break;
                    case MonitoringBookYearOfPupilConstant.EYE_VALUE:
                        fileTemplate = string.Format("{0}.xls", SystemParamsInFile.SoTheoDoiSKNam_Mat_12_Dot1);
                        break;
                    case MonitoringBookYearOfPupilConstant.ENT_VALUE:
                        fileTemplate = string.Format("{0}.xls", SystemParamsInFile.SoTheoDoiSKNam_TaiMuiHong_12_Dot1);
                        break;
                    case MonitoringBookYearOfPupilConstant.SPINE_VALUE:
                        fileTemplate = string.Format("{0}.xls", SystemParamsInFile.SoTheoDoiSKNam_CotSong_12_Dot1);
                        break;
                    case MonitoringBookYearOfPupilConstant.DENTAL_VALUE:
                        fileTemplate = string.Format("{0}.xls", SystemParamsInFile.SoTheoDoiSKNam_RangHamMat_12_Dot1);
                        break;
                    case MonitoringBookYearOfPupilConstant.OVERALL_VALUE:
                        fileTemplate = string.Format("{0}.xls", SystemParamsInFile.SoTheoDoiSKNam_TongQuat_12_Dot1);
                        break;
                    default:
                        break;
                }
            }
            else if (TypeExport == 2)
            {
                fileTemplate = string.Format("{0}.xls", SystemParamsInFile.SoTheoDoiSKNam_12_Dot1);
            }
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", fileTemplate);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            List<VTDataValidation> lstDataValidation = new List<VTDataValidation>();

            string suppervisingDeptName = "";
            if (_globalInfo.EmployeeID > 0)
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                suppervisingDeptName = objSP.SupervisingDept.SupervisingDeptName.ToUpper();
            }
            else
            {
                suppervisingDeptName = _globalInfo.SuperVisingDeptName.ToUpper();
            }

            #region TypeExport 1,2, 3
            string className = string.Empty;

            //lay danh sach hoc sinh cua 1 lop
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            if (TypeExport == 1 || TypeExport == 2)
            {
                dic["ClassID"] = ClassID;
            }

            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = HealthPeriodID;
            List<MonitoringBook> lstMoniBook = ListMonitoringBook(dic);
            List<int> lstMoniBookID = lstMoniBook.Select(x => x.MonitoringBookID).ToList();
            HealthPeriod objHealthPeriod = HealthPeriodBusiness.All.Where(x => x.IsActive == true && x.HealthPeriodID == HealthPeriodID).FirstOrDefault();
            string healthPeriodName = objHealthPeriod.Resolution;
            AcademicYear objAY = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<StandardViewModel> lstStandardVM = Common.StandardVM();
            string standardName = string.Empty;

            // TypeExport 1, 2
            if (TypeExport == 1 || TypeExport == 2) // 1 linh vuc cua lop
            {
                ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
                className = SMAS.Web.Utils.Utils.StripVNSignAndSpace(objCP.DisplayName);
                lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ToList();

                if (TypeExport == 1)
                {
                    #region
                    firstSheet = oBook.GetSheet(1);
                    firstSheet.SetCellValue("A2", suppervisingDeptName);
                    firstSheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
                    standardName = lstStandardVM.Where(x => x.StandardID == StandardID).FirstOrDefault().StandardName;

                    switch (StandardID)
                    {
                        case MonitoringBookYearOfPupilConstant.PHYSICAL_VALUE:
                            var physicalTest = PhysicalTestBusiness.ListPhysicalTestByMonitoringBookId(lstMoniBookID);
                            List<PhysicalViewModel> lstResult = GetListDataPhysical(lstPOC, lstMoniBook, physicalTest);
                            this.SetDataPhysicalToFile(firstSheet, lstPOC, lstResult, objAY.DisplayTitle, standardName, objCP.DisplayName, healthPeriodName);
                            break;
                        case MonitoringBookYearOfPupilConstant.EYE_VALUE:
                            var eyeTest = EyeTestBusiness.ListEyeTestByMonitoringBookId(lstMoniBookID);
                            List<EyeViewModel> lstResult2 = GetListDataEye(lstPOC, lstMoniBook, eyeTest);
                            this.SetDataEyeToFile(firstSheet, lstPOC, lstResult2, objAY.DisplayTitle, standardName, objCP.DisplayName, healthPeriodName);
                            break;
                        case MonitoringBookYearOfPupilConstant.ENT_VALUE:
                            var entTest = ENTTestBusiness.ListENTTestByMonitoringBookId(lstMoniBookID);
                            List<EntViewModel> lstResult3 = GetListDataEnt(lstPOC, lstMoniBook, entTest);
                            this.SetDataEntToFile(firstSheet, lstDataValidation, lstPOC, lstResult3, objAY.DisplayTitle, standardName, objCP.DisplayName, healthPeriodName, 1);
                            break;
                        case MonitoringBookYearOfPupilConstant.SPINE_VALUE:
                            var spinTest = SpineTestBusiness.ListSpineTestByMonitoringBookId(lstMoniBookID);
                            List<SpinViewModel> lstResult4 = GetListDataSpin(lstPOC, lstMoniBook, spinTest);
                            this.SetDataSpineToFile(firstSheet, lstDataValidation, lstPOC, lstResult4, objAY.DisplayTitle, standardName, objCP.DisplayName, healthPeriodName, 1);
                            break;
                        case MonitoringBookYearOfPupilConstant.DENTAL_VALUE:
                            var dentalTest = DentalTestBusiness.ListDentalTestByMonitoringBookId(lstMoniBookID);
                            List<DentalViewModel> lstResult5 = GetListDataDental(lstPOC, lstMoniBook, dentalTest);
                            this.SetDataDentalToFile(firstSheet, lstDataValidation, lstPOC, lstResult5, objAY.DisplayTitle, standardName, objCP.DisplayName, healthPeriodName, 1);
                            break;
                        case MonitoringBookYearOfPupilConstant.OVERALL_VALUE:
                            var overAllTest = OverallTestBusiness.ListOverallTestByMonitoringBookId(lstMoniBookID);
                            List<OverAllViewModel> lstResult6 = GetListDataOverAll(lstPOC, lstMoniBook, overAllTest);
                            this.SetDataOverAllToFile(firstSheet, lstDataValidation, lstPOC, lstResult6, objAY.DisplayTitle, standardName, objCP.DisplayName, healthPeriodName, 1);
                            break;
                        default:
                            break;
                    }

                    firstSheet.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(standardName, true);
                    fileName = string.Format("SoTheoDoiSKNam_{0}_{1}_{2}.xls", standardName, className, healthPeriodName);
                    if (StandardID == MonitoringBookYearOfPupilConstant.DENTAL_VALUE)
                    {
                        firstSheet.SetCellValue("AO3", TypeExport);
                        VTVector cellHiddenValue = new VTVector("AO3");
                        firstSheet.HideColumn(cellHiddenValue.Y);
                    }
                    else
                    {
                        firstSheet.SetCellValue("Y3", TypeExport);
                        VTVector cellHiddenValue = new VTVector("Y3");
                        firstSheet.HideColumn(cellHiddenValue.Y);
                    }

                    firstSheet.ProtectSheet();
                    #endregion
                }
                else
                {
                    #region
                    var physicalTest = PhysicalTestBusiness.ListPhysicalTestByMonitoringBookId(lstMoniBookID);
                    List<PhysicalViewModel> lstResultPhysical = GetListDataPhysical(lstPOC, lstMoniBook, physicalTest);

                    var eyeTest = EyeTestBusiness.ListEyeTestByMonitoringBookId(lstMoniBookID);
                    List<EyeViewModel> lstResultEye = GetListDataEye(lstPOC, lstMoniBook, eyeTest);

                    var entTest = ENTTestBusiness.ListENTTestByMonitoringBookId(lstMoniBookID);
                    List<EntViewModel> lstResultENT = GetListDataEnt(lstPOC, lstMoniBook, entTest);

                    var spinTest = SpineTestBusiness.ListSpineTestByMonitoringBookId(lstMoniBookID);
                    List<SpinViewModel> lstResultSpin = GetListDataSpin(lstPOC, lstMoniBook, spinTest);

                    var dentalTest = DentalTestBusiness.ListDentalTestByMonitoringBookId(lstMoniBookID);
                    List<DentalViewModel> lstResultDental = GetListDataDental(lstPOC, lstMoniBook, dentalTest);

                    var overAllTest = OverallTestBusiness.ListOverallTestByMonitoringBookId(lstMoniBookID);
                    List<OverAllViewModel> lstResultOverAll = GetListDataOverAll(lstPOC, lstMoniBook, overAllTest);

                    IVTWorksheet sheet = null;
                    for (int i = 0; i < lstStandardVM.Count; i++)
                    {
                        switch ((i + 1))
                        {
                            case MonitoringBookYearOfPupilConstant.PHYSICAL_VALUE:
                                sheet = oBook.GetSheet(1);
                                this.SetDataPhysicalToFile(sheet, lstPOC, lstResultPhysical, objAY.DisplayTitle, lstStandardVM[i].StandardName, objCP.DisplayName, healthPeriodName);
                                break;
                            case MonitoringBookYearOfPupilConstant.EYE_VALUE:
                                sheet = oBook.GetSheet(2);
                                this.SetDataEyeToFile(sheet, lstPOC, lstResultEye, objAY.DisplayTitle, lstStandardVM[i].StandardName, objCP.DisplayName, healthPeriodName);
                                break;
                            case MonitoringBookYearOfPupilConstant.ENT_VALUE:
                                sheet = oBook.GetSheet(3);
                                this.SetDataEntToFile(sheet, lstDataValidation, lstPOC, lstResultENT, objAY.DisplayTitle, lstStandardVM[i].StandardName, objCP.DisplayName, healthPeriodName, (i + 1));
                                break;
                            case MonitoringBookYearOfPupilConstant.SPINE_VALUE:
                                sheet = oBook.GetSheet(4);
                                this.SetDataSpineToFile(sheet, lstDataValidation, lstPOC, lstResultSpin, objAY.DisplayTitle, lstStandardVM[i].StandardName, objCP.DisplayName, healthPeriodName, (i + 1));
                                break;
                            case MonitoringBookYearOfPupilConstant.DENTAL_VALUE:
                                sheet = oBook.GetSheet(5);
                                this.SetDataDentalToFile(sheet, lstDataValidation, lstPOC, lstResultDental, objAY.DisplayTitle, lstStandardVM[i].StandardName, objCP.DisplayName, healthPeriodName, (i + 1));
                                break;
                            case MonitoringBookYearOfPupilConstant.OVERALL_VALUE:
                                sheet = oBook.GetSheet(6);
                                this.SetDataOverAllToFile(sheet, lstDataValidation, lstPOC, lstResultOverAll, objAY.DisplayTitle, lstStandardVM[i].StandardName, objCP.DisplayName, healthPeriodName, (i + 1));
                                break;
                            default:
                                break;
                        }

                        sheet.SetCellValue("A2", suppervisingDeptName);
                        sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
                        if ((i + 1) == MonitoringBookYearOfPupilConstant.DENTAL_VALUE)
                        {
                            sheet.SetCellValue("AO3", TypeExport);
                            VTVector cellHiddenValue = new VTVector("AO3");
                            sheet.HideColumn(cellHiddenValue.Y);
                        }
                        else
                        {
                            sheet.SetCellValue("Y3", TypeExport);
                            VTVector cellHiddenValue = new VTVector("Y3");
                            sheet.HideColumn(cellHiddenValue.Y);
                        }

                        sheet.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(lstStandardVM[i].StandardName, true);
                        sheet.ProtectSheet();
                    }
                    fileName = string.Format("SoTheoDoiSKNam_{0}_{1}.xls", className, healthPeriodName);
                    #endregion
                }
            }//TypeExport 3
            else
            {
                #region
                IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                dicSearch["SchoolID"] = _globalInfo.SchoolID.Value;
                dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicSearch["EducationLevelID"] = EducationLevelID;
                firstSheet = oBook.GetSheet(1);
                lstPOC = PupilOfClassBusiness.Search(dicSearch)
                                               .Select(p => new PupilOfClassBO
                                               {
                                                   PupilID = p.PupilID,
                                                   PupilCode = p.PupilProfile.PupilCode,
                                                   PupilFullName = p.PupilProfile.FullName,
                                                   ClassID = p.ClassID,
                                                   Status = p.Status,
                                                   OrderInClass = p.OrderInClass,
                                                   Name = p.PupilProfile.Name,
                                                   ClassName = p.ClassProfile.DisplayName
                                               }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name)
                                               .ThenBy(c => c.PupilFullName).ToList();
                var lstClassProfile = ClassProfileBusiness.Search(dicSearch).Where(x=>x.IsActive == true).OrderBy(x=>x.OrderNumber).ToList();
                List<int> lstClassID = lstClassProfile.Select(x => x.ClassProfileID).Distinct().ToList();          
                standardName = lstStandardVM.Where(x => x.StandardID == StandardID).FirstOrDefault().StandardName;

                List<PupilOfClassBO> _lstPOC = new List<PupilOfClassBO>();
                if (StandardID == MonitoringBookYearOfPupilConstant.PHYSICAL_VALUE)
                {
                    var physicalTest = PhysicalTestBusiness.ListPhysicalTestByMonitoringBookId(lstMoniBookID);
                    List<PhysicalViewModel> lstResult = new List<PhysicalViewModel>();

                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                        _lstPOC = lstPOC.Where(x => x.ClassID == lstClassID[i]).ToList();
                        if (_lstPOC.Count > 0)
                        {
                            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                            sheet.SetCellValue("A2", suppervisingDeptName);
                            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());                          
                            className = _lstPOC.Select(x => x.ClassName).FirstOrDefault();
                            lstResult = GetListDataPhysical(_lstPOC, lstMoniBook, physicalTest);
                            this.SetDataPhysicalToFile(sheet, _lstPOC, lstResult, objAY.DisplayTitle, standardName, className, healthPeriodName);
                            sheet.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(className, true);

                            sheet.SetCellValue("Y3", TypeExport);
                            VTVector cellHiddenValue = new VTVector("Y3");
                            sheet.HideColumn(cellHiddenValue.Y);
                            sheet.ProtectSheet();
                        }
                        
                    }
                }
                else if (StandardID == MonitoringBookYearOfPupilConstant.EYE_VALUE)
                {
                    var eyeTest = EyeTestBusiness.ListEyeTestByMonitoringBookId(lstMoniBookID);
                    List<EyeViewModel> lstResult = new List<EyeViewModel>();
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                         _lstPOC = lstPOC.Where(x => x.ClassID == lstClassID[i]).ToList();
                         if (_lstPOC.Count > 0)
                         {
                             IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                             sheet.SetCellValue("A2", suppervisingDeptName);
                             sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
                             className = _lstPOC.Select(x => x.ClassName).FirstOrDefault();
                             lstResult = GetListDataEye(_lstPOC, lstMoniBook, eyeTest);
                             this.SetDataEyeToFile(sheet, _lstPOC, lstResult, objAY.DisplayTitle, standardName, className, healthPeriodName);
                             sheet.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(className, true);

                             sheet.SetCellValue("Y3", TypeExport);
                             VTVector cellHiddenValue = new VTVector("Y3");
                             sheet.HideColumn(cellHiddenValue.Y);
                             sheet.ProtectSheet();
                         }
                    }
                }
                else if (StandardID == MonitoringBookYearOfPupilConstant.ENT_VALUE)
                {
                    var entTest = ENTTestBusiness.ListENTTestByMonitoringBookId(lstMoniBookID);
                    List<EntViewModel> lstResult = new List<EntViewModel>();
                    int index = 0;
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                         _lstPOC = lstPOC.Where(x => x.ClassID == lstClassID[i]).ToList();
                         if (_lstPOC.Count > 0)
                         {
                             index++;
                             IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                             sheet.SetCellValue("A2", suppervisingDeptName);
                             sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
                             className = _lstPOC.Select(x => x.ClassName).FirstOrDefault();
                             lstResult = GetListDataEnt(_lstPOC, lstMoniBook, entTest);
                             this.SetDataEntToFile(sheet, lstDataValidation, _lstPOC, lstResult, objAY.DisplayTitle, standardName, className, healthPeriodName, index);
                             sheet.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(className, true);

                             sheet.SetCellValue("Y3", TypeExport);
                             VTVector cellHiddenValue = new VTVector("Y3");
                             sheet.HideColumn(cellHiddenValue.Y);
                             sheet.ProtectSheet();
                         }
                    }
                }
                else if (StandardID == MonitoringBookYearOfPupilConstant.DENTAL_VALUE)
                {
                    var dentalTest = DentalTestBusiness.ListDentalTestByMonitoringBookId(lstMoniBookID);
                    List<DentalViewModel> lstResult = new List<DentalViewModel>();
                    int index = 0;
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                         _lstPOC = lstPOC.Where(x => x.ClassID == lstClassID[i]).ToList();
                         if (_lstPOC.Count > 0)
                         {
                             index++;
                             IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                             sheet.SetCellValue("A2", suppervisingDeptName);
                             sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());                           
                             className = _lstPOC.Select(x => x.ClassName).FirstOrDefault();
                             lstResult = GetListDataDental(_lstPOC, lstMoniBook, dentalTest);
                             this.SetDataDentalToFile(sheet, lstDataValidation, _lstPOC, lstResult, objAY.DisplayTitle, standardName, className, healthPeriodName, index);
                             sheet.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(className, true);

                             sheet.SetCellValue("AO3", TypeExport);
                             VTVector cellHiddenValue = new VTVector("AO3");
                             sheet.HideColumn(cellHiddenValue.Y);
                             sheet.ProtectSheet();
                         }
                    }
                }
                else if (StandardID == MonitoringBookYearOfPupilConstant.SPINE_VALUE)
                {
                    var spinTest = SpineTestBusiness.ListSpineTestByMonitoringBookId(lstMoniBookID);
                    List<SpinViewModel> lstResult = new List<SpinViewModel>();
                    int index = 0;
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                         _lstPOC = lstPOC.Where(x => x.ClassID == lstClassID[i]).ToList();
                         if (_lstPOC.Count > 0)
                         {
                             index++;
                             IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                             sheet.SetCellValue("A2", suppervisingDeptName);
                             sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
                             _lstPOC = lstPOC.Where(x => x.ClassID == lstClassID[i]).ToList();
                             className = _lstPOC.Select(x => x.ClassName).FirstOrDefault();
                             lstResult = GetListDataSpin(_lstPOC, lstMoniBook, spinTest);
                             this.SetDataSpineToFile(sheet, lstDataValidation, _lstPOC, lstResult, objAY.DisplayTitle, standardName, className, healthPeriodName, index);
                             sheet.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(className, true);

                             sheet.SetCellValue("Y3", TypeExport);
                             VTVector cellHiddenValue = new VTVector("Y3");
                             sheet.HideColumn(cellHiddenValue.Y);
                             sheet.ProtectSheet();
                         }
                    }
                }
                else if (StandardID == MonitoringBookYearOfPupilConstant.OVERALL_VALUE)
                {
                    var overAllTest = OverallTestBusiness.ListOverallTestByMonitoringBookId(lstMoniBookID);
                    List<OverAllViewModel> lstResult = new List<OverAllViewModel>();
                    int index = 0;
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                         _lstPOC = lstPOC.Where(x => x.ClassID == lstClassID[i]).ToList();
                         if (_lstPOC.Count > 0)
                         {
                             index++;
                             IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                             sheet.SetCellValue("A2", suppervisingDeptName);
                             sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
                             className = _lstPOC.Select(x => x.ClassName).FirstOrDefault();
                             lstResult = GetListDataOverAll(_lstPOC, lstMoniBook, overAllTest);
                             this.SetDataOverAllToFile(sheet, lstDataValidation, _lstPOC, lstResult, objAY.DisplayTitle, standardName, className, healthPeriodName, index);
                             sheet.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(className, true);

                             sheet.SetCellValue("Y3", TypeExport);
                             VTVector cellHiddenValue = new VTVector("Y3");
                             sheet.HideColumn(cellHiddenValue.Y);
                             sheet.ProtectSheet();
                         }
                    }
                }

                fileName = string.Format("SoTheoDoiSKNam_{0}_{1}.xls", standardName, healthPeriodName);
                firstSheet.Delete();
                #endregion
            }

            #endregion
            Stream excel = null;
            if (TypeExport == 1 || TypeExport == 3)
            {
                if (StandardID == MonitoringBookYearOfPupilConstant.ENT_VALUE
               || StandardID == MonitoringBookYearOfPupilConstant.SPINE_VALUE
               || StandardID == MonitoringBookYearOfPupilConstant.DENTAL_VALUE
               || StandardID == MonitoringBookYearOfPupilConstant.OVERALL_VALUE)
                {
                    excel = oBook.ToStreamValidationData(lstDataValidation);
                }
                else
                {
                    excel = oBook.ToStream();
                }
            }
            else
            {
                excel = oBook.ToStreamValidationData(lstDataValidation);
            }

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = Utils.Utils.StripVNSignAndSpace(fileName);
            return result;
        }


        #region SetDataPhysicalToFile
        private void SetDataPhysicalToFile(
            IVTWorksheet sheet,
            List<PupilOfClassBO> lstPOC,
            List<PhysicalViewModel> lstPhysical,
            string YearTitle,
            string standardName,
            string ClassName,
            string healthPeriodName)
        {
            //fill thong tin chung
            string strTitle = string.Empty;
            strTitle = string.Format("SỔ THEO DÕI HẰNG NĂM - {0}", standardName.ToUpper());

            sheet.SetCellValue("A5", strTitle);
            if (ClassName.ToUpper().Contains("LỚP"))
            {
                strTitle = string.Format("{0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            else
            {
                strTitle = string.Format("Lớp {0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            sheet.SetCellValue("A6", strTitle);

            int startRow = 9;
            PupilOfClassBO objPOC = null;
            PhysicalViewModel objPhysical = null;
            int index = 1;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                // thong tin hoc sinh
                sheet.SetCellValue(startRow, 1, index);
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);

                objPhysical = lstPhysical.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                if (objPhysical == null)
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 2, startRow, 9).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, 9).IsLock = true;
                    }
                    else
                    {
                        sheet.GetRange(startRow, 4, startRow, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 9).IsLock = false;
                    }
                    index++;
                    startRow++;
                    continue;
                }
                else
                {
                    sheet.SetCellValue(startRow, 4, objPhysical.HeightStr);
                    sheet.SetCellValue(startRow, 5, objPhysical.WeightStr);
                    sheet.SetCellValue(startRow, 6, objPhysical.BreastStr);
                    sheet.SetCellValue(startRow, 7, objPhysical.PhysicalClassification != 0 ? objPhysical.PhysicalClassification.ToString() : "");
                    sheet.SetCellValue(startRow, 8, objPhysical.BMIStr);
                    sheet.SetCellValue(startRow, 9, objPhysical.NutritionString);
                    sheet.GetRange(startRow, 4, startRow, 7).IsLock = false;

                    sheet.GetRange(startRow, 4, startRow, 8).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 9, startRow, 9).SetHAlign(VTHAlign.xlHAlignLeft);
                }

                if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, 9).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, 1, startRow, 9).IsLock = true;
                }

                index++;
                startRow++;
            }

            if (lstPOC.Count > 0)
            {
                sheet.GetRange(9, 1, 9 + lstPOC.Count - 1, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            sheet.SetFontName("Times New Roman", 12);
        }
        #endregion

        #region SetDataEyeToFile
        private void SetDataEyeToFile(
            IVTWorksheet sheet,
            List<PupilOfClassBO> lstPOC,
            List<EyeViewModel> lstEye,
            string YearTitle,
            string standardName,
            string ClassName,
            string healthPeriodName)
        {
            //fill thong tin chung
            string strTitle = string.Empty;
            strTitle = string.Format("SỔ THEO DÕI HẰNG NĂM - {0}", standardName.ToUpper());

            sheet.SetCellValue("A5", strTitle);
            if (ClassName.ToUpper().Contains("LỚP"))
            {
                strTitle = string.Format("{0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            else
            {
                strTitle = string.Format("Lớp {0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            sheet.SetCellValue("A6", strTitle);

            int startRow = 10;
            PupilOfClassBO objPOC = null;
            EyeViewModel objEye = null;
            int index = 1;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                // thong tin hoc sinh
                sheet.SetCellValue(startRow, 1, index);
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);

                objEye = lstEye.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                if (objEye == null)
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 2, startRow, 14).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, 14).IsLock = true;
                    }
                    else
                    {
                        sheet.GetRange(startRow, 4, startRow, 14).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 14).IsLock = false;
                    }
                    index++;
                    startRow++;
                    continue;
                }
                else
                {
                    sheet.SetCellValue(startRow, 4, objEye.LeftEye != 0 ? objEye.LeftEye.ToString() : "");
                    sheet.SetCellValue(startRow, 5, objEye.RightEye != 0 ? objEye.RightEye.ToString() : "");
                    sheet.SetCellValue(startRow, 6, objEye.LeftIsEye != 0 ? objEye.LeftIsEye.ToString() : "");
                    sheet.SetCellValue(startRow, 7, objEye.RightIsEye != 0 ? objEye.RightIsEye.ToString() : "");
                    sheet.SetCellValue(startRow, 8, objEye.LMyopicStr);
                    sheet.SetCellValue(startRow, 9, objEye.RMyopic);
                    sheet.SetCellValue(startRow, 10, objEye.LFarsightednessStr);
                    sheet.SetCellValue(startRow, 11, objEye.RFarsightednessStr);
                    sheet.SetCellValue(startRow, 12, objEye.LAstigmatismStr);
                    sheet.SetCellValue(startRow, 13, objEye.RAstigmatismStr);
                    sheet.SetCellValue(startRow, 14, objEye.Other);
                    sheet.GetRange(startRow, 4, startRow, 14).IsLock = false;

                    sheet.GetRange(startRow, 4, startRow, 13).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 14, startRow, 14).SetHAlign(VTHAlign.xlHAlignLeft);
                }

                if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, 14).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, 1, startRow, 14).IsLock = true;
                }

                index++;
                startRow++;
            }

            if (lstPOC.Count > 0)
            {
                sheet.GetRange(10, 1, 10 + lstPOC.Count - 1, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            sheet.SetFontName("Times New Roman", 12);
        }
        #endregion

        #region SetDataEntToFile
        private void SetDataEntToFile(
            IVTWorksheet sheet,
            List<VTDataValidation> lstValidation,
            List<PupilOfClassBO> lstPOC,
            List<EntViewModel> lstEnt,
            string YearTitle,
            string standardName,
            string ClassName,
            string healthPeriodName,
            int indexSheet)
        {
            //fill thong tin chung
            string strTitle = string.Empty;
            strTitle = string.Format("SỔ THEO DÕI HẰNG NĂM - {0}", standardName.ToUpper());

            sheet.SetCellValue("A5", strTitle);
            if (ClassName.ToUpper().Contains("LỚP"))
            {
                strTitle = string.Format("{0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            else
            {
                strTitle = string.Format("Lớp {0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            sheet.SetCellValue("A6", strTitle);

            int startRow = 10;
            PupilOfClassBO objPOC = null;
            EntViewModel objEnt = null;
            int index = 1;

            lstValidation.Add(new VTDataValidation
            {
                Contrains = new string[] { "X", "x" },
                FromColumn = 6,
                FromRow = 10,
                SheetIndex = indexSheet,
                ToColumn = 7,
                ToRow = 10 + lstPOC.Count - 1,
                Type = VTValidationType.LIST
            });

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                // thong tin hoc sinh
                sheet.SetCellValue(startRow, 1, index);
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);

                objEnt = lstEnt.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                if (objEnt == null)
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 2, startRow, 8).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, 8).IsLock = true;
                    }
                    else
                    {
                        sheet.GetRange(startRow, 4, startRow, 8).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 8).IsLock = false;
                    }
                    index++;
                    startRow++;
                    continue;
                }
                else
                {
                    sheet.SetCellValue(startRow, 4, objEnt.LCapacityHearing);
                    sheet.SetCellValue(startRow, 5, objEnt.RCapacityHearing);
                    sheet.SetCellValue(startRow, 6, objEnt.IsDeafStr != null ? objEnt.IsDeafStr : "");
                    sheet.SetCellValue(startRow, 7, objEnt.IsSickStr != null ? objEnt.IsSickStr : "");
                    sheet.SetCellValue(startRow, 8, objEnt.Description);
                    sheet.GetRange(startRow, 4, startRow, 18).IsLock = false;
                    sheet.GetRange(startRow, 4, startRow, 7).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 8, startRow, 8).SetHAlign(VTHAlign.xlHAlignLeft);
                }

                if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, 8).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, 1, startRow, 8).IsLock = true;
                }

                index++;
                startRow++;
            }

            if (lstPOC.Count > 0)
            {
                sheet.GetRange(10, 1, 10 + lstPOC.Count - 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            sheet.SetFontName("Times New Roman", 12);
        }
        #endregion

        #region SetDataSpineToFile
        private void SetDataSpineToFile(
            IVTWorksheet sheet,
            List<VTDataValidation> lstValidation,
            List<PupilOfClassBO> lstPOC,
            List<SpinViewModel> lstSpine,
            string YearTitle,
            string standardName,
            string ClassName,
            string healthPeriodName,
            int indexSheet)
        {
            //fill thong tin chung
            string strTitle = string.Empty;
            strTitle = string.Format("SỔ THEO DÕI HẰNG NĂM - {0}", standardName.ToUpper());

            sheet.SetCellValue("A5", strTitle);
            if (ClassName.ToUpper().Contains("LỚP"))
            {
                strTitle = string.Format("{0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            else
            {
                strTitle = string.Format("Lớp {0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            sheet.SetCellValue("A6", strTitle);

            int startRow = 9;
            PupilOfClassBO objPOC = null;
            SpinViewModel objSpine = null;
            int index = 1;

            lstValidation.Add(new VTDataValidation
            {
                Contrains = new string[] { "X", "x" },
                FromColumn = 4,
                FromRow = 9,
                SheetIndex = indexSheet,
                ToColumn = 4,
                ToRow = 9 + lstPOC.Count - 1,
                Type = VTValidationType.LIST
            });

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                // thong tin hoc sinh
                sheet.SetCellValue(startRow, 1, index);
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);

                objSpine = lstSpine.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                if (objSpine == null)
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 2, startRow, 8).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, 8).IsLock = true;
                    }
                    else
                    {
                        sheet.GetRange(startRow, 4, startRow, 8).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 8).IsLock = false;
                    }
                    index++;
                    startRow++;
                    continue;
                }
                else
                {
                    sheet.SetCellValue(startRow, 4, objSpine.Deformity == true ? "X" : "");
                    sheet.SetCellValue(startRow, 5, objSpine.Examinate_Straight);
                    sheet.SetCellValue(startRow, 6, objSpine.Examinate_Italic);
                    sheet.SetCellValue(startRow, 7, objSpine.Examinate_Other);
                    sheet.GetRange(startRow, 4, startRow, 18).IsLock = false;
                    sheet.GetRange(startRow, 4, startRow, 7).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 8, startRow, 8).SetHAlign(VTHAlign.xlHAlignLeft);
                }

                if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, 7).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, 1, startRow, 7).IsLock = true;
                }

                index++;
                startRow++;
            }

            if (lstPOC.Count > 0)
            {
                sheet.GetRange(9, 1, 9 + lstPOC.Count - 1, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            sheet.SetFontName("Times New Roman", 12);
        }
        #endregion

        #region SetDataDentalToFile
        private void SetDataDentalToFile(
            IVTWorksheet sheet,
            List<VTDataValidation> lstValidation,
            List<PupilOfClassBO> lstPOC,
            List<DentalViewModel> lstDental,
            string YearTitle,
            string standardName,
            string ClassName,
            string healthPeriodName,
            int indexSheet)
        {
            //fill thong tin chung
            string strTitle = string.Empty;
            strTitle = string.Format("SỔ THEO DÕI HẰNG NĂM - {0}", standardName.ToUpper());

            sheet.SetCellValue("A5", strTitle);
            if (ClassName.ToUpper().Contains("LỚP"))
            {
                strTitle = string.Format("{0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            else
            {
                strTitle = string.Format("Lớp {0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            sheet.SetCellValue("A6", strTitle);

            int startRow = 11;
            PupilOfClassBO objPOC = null;
            DentalViewModel objDental = null;
            int index = 1;

            lstValidation.Add(new VTDataValidation
            {
                Contrains = new string[] { "X", "x" },
                FromColumn = 4,
                FromRow = 11,
                SheetIndex = indexSheet,
                ToColumn = 35,
                ToRow = 11 + lstPOC.Count - 1,
                Type = VTValidationType.LIST
            });

            lstValidation.Add(new VTDataValidation
            {
                Contrains = new string[] { "X", "x" },
                FromColumn = 38,
                FromRow = 11,
                SheetIndex = indexSheet,
                ToColumn = 39,
                ToRow = 11 + lstPOC.Count - 1,
                Type = VTValidationType.LIST
            });

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                // thong tin hoc sinh
                sheet.SetCellValue(startRow, 1, index);
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);

                objDental = lstDental.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                if (objDental == null)
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 2, startRow, 40).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, 40).IsLock = true;
                    }
                    else
                    {
                        sheet.GetRange(startRow, 4, startRow, 40).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 40).IsLock = false;
                    }
                    index++;
                    startRow++;
                    continue;
                }
                else
                {
                    int columnOne = 4;
                    int columnThree = 28;
                    int columnTwo = 12;
                    int columnFour = 20;
                    for (int h = 8; h > 0; h--)
                    {
                        if (objDental.Num_Dental_Filling_One != null)
                        {
                            sheet.SetCellValue(startRow, columnOne, objDental.Num_Dental_Filling_One.Contains("" + h) ? "X" : "");
                            columnOne++;
                        }
                        if (objDental.Num_Dental_Filling_Four != null)
                        {
                            sheet.SetCellValue(startRow, columnFour, objDental.Num_Dental_Filling_Four.Contains("" + h) ? "X" : "");
                            columnFour++;
                        }

                    }
                    for (int h = 1; h <= 8; h++)
                    {
                        if (objDental.Num_Dental_Filling_Two != null)
                        {
                            sheet.SetCellValue(startRow, columnTwo, objDental.Num_Dental_Filling_Two.Contains("" + h) ? "X" : "");
                            columnTwo++;
                        }
                        if (objDental.Num_Dental_Filling_Three != null)
                        {
                            sheet.SetCellValue(startRow, columnThree, objDental.Num_Dental_Filling_Three.Contains("" + h) ? "X" : "");
                            columnThree++;
                        }
                    }

                    sheet.SetCellValue(startRow, 36, objDental.Num_Preventive_Dental_Flling);
                    sheet.SetCellValue(startRow, 37, objDental.Num_Teeth_Extracted);
                    sheet.SetCellValue(startRow, 38, objDental.Is_Scrapt_Teeth == true ? "X" : "");
                    sheet.SetCellValue(startRow, 39, objDental.Is_Treatment == true ? "X" : "");
                    sheet.SetCellValue(startRow, 40, objDental.Other);
                    sheet.GetRange(startRow, 4, startRow, 40).IsLock = false;
                    sheet.GetRange(startRow, 4, startRow, 39).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 40, startRow, 40).SetHAlign(VTHAlign.xlHAlignLeft);
                }

                if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, 40).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, 1, startRow, 40).IsLock = true;
                }

                index++;
                startRow++;
            }

            if (lstPOC.Count > 0)
            {
                sheet.GetRange(11, 1, 11 + lstPOC.Count - 1, 40).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            sheet.SetFontName("Times New Roman", 12);
        }
        #endregion

        #region SetDataOverAllToFile
        private void SetDataOverAllToFile(
            IVTWorksheet sheet,
            List<VTDataValidation> lstValidation,
            List<PupilOfClassBO> lstPOC,
            List<OverAllViewModel> lstOverAll,
            string YearTitle,
            string standardName,
            string ClassName,
            string healthPeriodName,
            int indexSheet)
        {
            //fill thong tin chung
            string strTitle = string.Empty;
            strTitle = string.Format("SỔ THEO DÕI HẰNG NĂM - {0}", standardName.ToUpper());

            sheet.SetCellValue("A5", strTitle);
            if (ClassName.ToUpper().Contains("LỚP"))
            {
                strTitle = string.Format("{0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            else
            {
                strTitle = string.Format("Lớp {0}, {1}, Năm học {2}", ClassName, healthPeriodName, YearTitle);
            }
            sheet.SetCellValue("A6", strTitle);

            int startRow = 9;
            PupilOfClassBO objPOC = null;
            OverAllViewModel objOverAll = null;
            int index = 1;

            lstValidation.Add(new VTDataValidation
            {
                Contrains = new string[] { "X", "x" },
                FromColumn = 12,
                FromRow = 9,
                SheetIndex = indexSheet,
                ToColumn = 13,
                ToRow = 9 + lstPOC.Count - 1,
                Type = VTValidationType.LIST
            });

            lstValidation.Add(new VTDataValidation
            {
                Contrains = new string[] { "A", "a", "B", "b", "C", "c" },
                FromColumn = 14,
                FromRow = 9,
                SheetIndex = indexSheet,
                ToColumn = 14,
                ToRow = 9 + lstPOC.Count - 1,
                Type = VTValidationType.LIST
            });

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                // thong tin hoc sinh
                sheet.SetCellValue(startRow, 1, index);
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);

                objOverAll = lstOverAll.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                if (objOverAll == null)
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 2, startRow, 15).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, 15).IsLock = true;
                    }
                    else
                    {
                        sheet.GetRange(startRow, 4, startRow, 15).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 15).IsLock = false;
                    }
                    index++;
                    startRow++;
                    continue;
                }
                else
                {
                    sheet.SetCellValue(startRow, 4, ("'" + objOverAll.MonitoringDateStr));
                    sheet.SetCellValue(startRow, 5, objOverAll.UnitName);
                    sheet.SetCellValue(startRow, 6, objOverAll.Doctor);
                    sheet.SetCellValue(startRow, 7, objOverAll.HistoryYourSelf);
                    sheet.SetCellValue(startRow, 8, objOverAll.DescriptionOverallInternal);
                    sheet.SetCellValue(startRow, 9, objOverAll.DescriptionOverallForeign);
                    sheet.SetCellValue(startRow, 10, objOverAll.DescriptionSkinDiseases);
                    sheet.SetCellValue(startRow, 11, objOverAll.Other);
                    sheet.SetCellValue(startRow, 12, objOverAll.IsHeartDiseases == true ? "X" : "");
                    sheet.SetCellValue(startRow, 13, objOverAll.IsBirthDefect == true ? "X" : "");

                    if (objOverAll.EvaluationHealth != null)
                    {
                        if (objOverAll.EvaluationHealth == 1)
                            sheet.SetCellValue(startRow, 14, "A");
                        else if (objOverAll.EvaluationHealth == 2)
                            sheet.SetCellValue(startRow, 14, "B");
                        else if (objOverAll.EvaluationHealth == 3)
                            sheet.SetCellValue(startRow, 14, "C");
                        else
                            sheet.SetCellValue(startRow, 14, "");
                    }
                    else
                        sheet.SetCellValue(startRow, 14, "");

                    sheet.SetCellValue(startRow, 15, objOverAll.RequireOfDoctor);

                    sheet.GetRange(startRow, 4, startRow, 15).IsLock = false;
                    sheet.GetRange(startRow, 4, startRow, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 5, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 15, startRow, 15).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 15, startRow, 15).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 14).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 14).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 4, startRow, 15).WrapText();
                }

                if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, 15).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, 1, startRow, 15).IsLock = true;
                }

                index++;
                startRow++;
            }

            if (lstPOC.Count > 0)
            {
                sheet.GetRange(9, 1, 9 + lstPOC.Count - 1, 15).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            sheet.SetFontName("Times New Roman", 12);
        }
        #endregion
        #endregion

        #region Import Excel
        #region //RenderPartialViewToString
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        #region // UploadFileImportSubject
        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImportSubject(IEnumerable<HttpPostedFileBase> attachments, int ClassID, int StandardID)
        {
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;
                List<int> listTypeReportExcel = new List<int>();
                List<string> listSheetNameExcel = new List<string>();
                string strValue = string.Empty;
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 5120)
                {
                    return Json(new JsonMessage(Res.Get("Common_Max_Size_FileExcel"), "error"));
                }

                List<PupilOfClassBO> lstPC = new List<PupilOfClassBO>();
                List<PupilOfClassBO> _lstPOC = new List<PupilOfClassBO>();
                List<string> lstClassName = new List<string>();
                // danh sach cac linh vuc
                int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
                List<StandardViewModel> lstStandard = Common.StandardVM();
                string sheetInfo = string.Empty;
                // duyệt kiểm tra từng sheet
                string sheetName = string.Empty;
                StringBuilder strBuilder = new StringBuilder();
                if (lstSheets != null && lstSheets.Count > 0)
                {
                    for (int i = 0; i < lstSheets.Count; i++)
                    {
                        sheet = oBook.GetSheet(i + 1);
                        strValue = (sheet.GetCellValue("Y3") != null ? sheet.GetCellValue("Y3").ToString() : string.Empty);
                        if (string.IsNullOrEmpty(strValue))
                        {
                            strValue = (sheet.GetCellValue("AO3") != null ? sheet.GetCellValue("AO3").ToString() : string.Empty);
                        }

                        if (!string.IsNullOrEmpty(strValue))
                        {
                            if (strValue.Equals("1") || strValue.Equals("2") || strValue.Equals("3"))
                            {
                                if (strValue.Equals("1"))
                                {
                                    var objStandard = lstStandard.Where(x => x.StandardID == StandardID).FirstOrDefault();
                                    if (objStandard != null)
                                    {
                                        string standardName = SMAS.Web.Utils.Utils.StripVNSignAndSpace(objStandard.StandardName, true);
                                        if (standardName.ToUpper().Equals(sheet.Name.ToUpper()))
                                        {
                                            listSheetNameExcel.Add(sheet.Name);
                                            listTypeReportExcel.Add(int.Parse(strValue));
                                        }
                                        else
                                        {
                                            return Json(new JsonMessage(Res.Get("File dữ liệu không phải của danh mục " + objStandard.StandardName), "error"));
                                        }
                                    }

                                    if (listSheetNameExcel.Count <= 0)
                                        return Json(new JsonMessage(Res.Get("Import dữ liệu thất bại"), "error"));
                                }
                                else
                                {
                                    listSheetNameExcel.Add(sheet.Name);
                                    listTypeReportExcel.Add(int.Parse(strValue));
                                }

                            }
                        }
                        else
                        {
                            return Json(new JsonMessage(Res.Get("File dữ liệu không hợp lệ"), "error"));
                        }
                    }

                    if (listTypeReportExcel.Count > 0)
                    {
                        List<int> listType = listTypeReportExcel.Distinct().ToList();
                        if (listType.Count > 1)
                        {
                            return Json(new JsonMessage(Res.Get("File dữ liệu không hợp lệ"), "error"));
                        }

                        int TypeImport = listTypeReportExcel.FirstOrDefault();
                        Session[MonitoringBookYearOfPupilConstant.PHISICAL_PATH] = physicalPath;

                        if (TypeImport == 1 || TypeImport == 2)
                        {
                            if (TypeImport == 2)
                            {
                                foreach (var item in lstStandard)
                                {
                                    sheetName = SMAS.Web.Utils.Utils.StripVNSignAndSpace(item.StandardName, true);
                                    if (listSheetNameExcel.Contains(sheetName))
                                    {
                                        strBuilder.Append("<span class='ClassListSubjectImport editor-field' style='padding:5px;'>");
                                        strBuilder.Append("<input class='classSubjectImport' checked='checked' type='checkbox' value='" + item.StandardID + "' name='OptionSubject_" + item.StandardName + "' onchange='changeSubjectImport()'>  " + item.StandardName + "");
                                        strBuilder.Append("</span>");
                                    }
                                }
                            }
                        }
                        else
                        {
                            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                            dicSearch["SchoolID"] = _globalInfo.SchoolID.Value;
                            dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                            dicSearch["EducationLevelID"] = EducationLevelID;
                            lstPC = PupilOfClassBusiness.Search(dicSearch)
                                                           .Select(p => new PupilOfClassBO
                                                           {
                                                               ClassID = p.ClassID,
                                                               OrderInClass = p.OrderInClass,
                                                               ClassName = p.ClassProfile.DisplayName
                                                           }).OrderBy(c => c.OrderInClass).ToList();
                            lstClassName = lstPC.Select(x => x.ClassName).Distinct().ToList();

                            foreach (var item in lstClassName)
                            {
                                _lstPOC = lstPC.Where(x => x.ClassName == item).ToList();
                                if (_lstPOC.Count > 0)
                                {
                                    sheetName = SMAS.Web.Utils.Utils.StripVNSignAndSpace(item, true);
                                    if (listSheetNameExcel.Contains(sheetName))
                                    {
                                        strBuilder.Append("<span class='ClassListClassOption editor-field' style='padding:5px;'>");
                                        strBuilder.Append("<input class='ClassExport' type='checkbox' checked='checked' value='" + _lstPOC.FirstOrDefault().ClassID + "' name='OptionClass_" + _lstPOC.FirstOrDefault().ClassID + "' onchange='changeClasImport()'>  " + item + "");
                                        strBuilder.Append("</span>");
                                    }

                                }
                            }
                        }

                        if (TypeImport == 2 || TypeImport == 3)
                            if (strBuilder.Length == 0)
                                return Json(new JsonMessage(Res.Get("File dữ liệu không hợp lệ"), "error"));

                        return Json(new { TypeImport = TypeImport, strOption = strBuilder != null ? strBuilder.ToString() : string.Empty });
                    }
                }
                return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }
        #endregion

        #region // ImportExcel
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int typeImport, int ClassID, int StandardID, int healthPeriodID,
            string arrStandardIDImport, string arrClassIDImport)
        {
            if (Session[MonitoringBookYearOfPupilConstant.PHISICAL_PATH] == null)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            string physicalPath = (string)Session[MonitoringBookYearOfPupilConstant.PHISICAL_PATH];
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            #region
            List<MonitoringBookBO> lstMonitoringBookBOOfPhysical = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfEye = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfENT = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfSpine = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfDental = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfOverAll = new List<MonitoringBookBO>();
            List<PhysicalTestBO> lstPhysicalTestBO = new List<PhysicalTestBO>();
            List<EyeTestBO> lstEyeTestBO = new List<EyeTestBO>();
            List<ENTTestBO> lstEntTestBO = new List<ENTTestBO>();
            List<SpineTestBO> lstSpineTestBO = new List<SpineTestBO>();
            List<DentalTestBO> lstDentalTestBO = new List<DentalTestBO>();
            List<OverallTestBO> lstOverallTestBO = new List<OverallTestBO>();
            List<int> lstPupilIDOfPhysical = new List<int>();
            List<int> lstPupilIDOfEye = new List<int>();
            List<int> lstPupilIDOfENT = new List<int>();
            List<int> lstPupilIDOfSpine = new List<int>();
            List<int> lstPupilIDOfDental = new List<int>();
            List<int> lstPupilIDofOverAll = new List<int>();
            string ErrorOfPhysical = string.Empty;
            string ErrorOfEye = string.Empty;
            string ErrorOfENT = string.Empty;
            string ErrorOfDental = string.Empty;
            string ErrorOfSpine = string.Empty;
            string ErrorOfOverAll = string.Empty;
            List<int> lstErrorOfPhysical = new List<int>();
            List<int> lstErrorOfEye = new List<int>();
            List<int> lstErrorOfENT = new List<int>();
            List<int> lstErrorOfSpine = new List<int>();
            List<int> lstErrorOfDental = new List<int>();
            List<int> lstErrorOfOverAll = new List<int>();
            #endregion

            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            List<int> lstPupilID = new List<int>();

            IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);
            string messageSuccess = string.Empty;
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            string Error = string.Empty;
            List<int> lstError = new List<int>();

            // danh sach danh mục
            List<StandardViewModel> lstStandardVM = Common.StandardVM();

            // danh sách id các danh mục
            List<int> lstStandardIdAll = arrStandardIDImport.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            List<int> lstStandardID = new List<int>();
            List<int> lstClassID = new List<int>();

            #region //typeImport 1, 2, 3
            if (typeImport == 1 || typeImport == 2 || typeImport == 3)
            {
                if (typeImport == 1 || typeImport == 2)
                {
                    lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ToList();
                }
                else
                {
                    IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                    dicSearch["SchoolID"] = _globalInfo.SchoolID.Value;
                    dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                    dicSearch["EducationLevelID"] = EducationLevelID;
                    lstPOC = PupilOfClassBusiness.Search(dicSearch)
                                                   .Select(p => new PupilOfClassBO
                                                   {
                                                       PupilID = p.PupilID,
                                                       PupilCode = p.PupilProfile.PupilCode,
                                                       PupilFullName = p.PupilProfile.FullName,
                                                       ClassID = p.ClassID,
                                                       Status = p.Status,
                                                       Birthday = p.PupilProfile.BirthDate,
                                                       Genre = p.PupilProfile.Genre,
                                                       OrderInClass = p.OrderInClass,
                                                       Name = p.PupilProfile.Name,
                                                       ClassName = p.ClassProfile.DisplayName
                                                   }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                }

                #region //typeImport 1, 2
                if (typeImport == 1)
                {
                    messageSuccess = "Nhập dữ liệu từ Excel thành công";
                    lstStandardID.Add(StandardID);
                }
                else if (typeImport == 2)
                {
                    for (int i = 0; i < lstStandardIdAll.Count; i++)
                    {
                        foreach (var item in lstStandardVM)
                        {
                            if (item.StandardID == lstStandardIdAll[i])
                            {
                                lstStandardID.Add(lstStandardIdAll[i]);
                                break;
                            }
                        }
                    }

                    if (lstStandardID.Count >= 1)
                    {
                        List<string> lstStandardName = lstStandardVM
                            .Where(p => lstStandardID.Contains(p.StandardID))
                            .OrderBy(p => p.StandardID)
                            .Select(p => p.StandardName).ToList();

                        messageSuccess = "Nhập dữ liệu từ Excel thành công cho danh mục ";
                        for (int i = 0; i < lstStandardName.Count; i++)
                        {
                            if (i < lstStandardName.Count - 1)
                            {
                                messageSuccess += lstStandardName[i] + ", ";
                            }
                            else
                            {
                                messageSuccess += lstStandardName[i];
                            }
                        }
                    }
                }
                else
                {
                    List<int> lstClassIdAll = arrClassIDImport.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                    List<int> lstClassIdOfDB = lstPOC.Select(x => x.ClassID).Distinct().ToList();
                    for (int i = 0; i < lstClassIdAll.Count; i++)
                    {
                        foreach (var item in lstClassIdOfDB)
                        {
                            if (item == lstClassIdAll[i])
                            {
                                lstClassID.Add(lstClassIdAll[i]);
                                break;
                            }
                        }
                    }

                    if (lstClassID.Count >= 1)
                    {
                        List<string> lstClassName = lstPOC.Where(x => lstClassID.Contains(x.ClassID)).Select(x => x.ClassName).Distinct().ToList();
                        messageSuccess = "Nhập dữ liệu từ Excel thành công cho lớp ";
                        lstStandardID.Add(StandardID);
                        for (int i = 0; i < lstClassName.Count; i++)
                        {
                            if (i < lstClassName.Count - 1)
                            {
                                messageSuccess += lstClassName[i] + ", ";
                            }
                            else
                            {
                                messageSuccess += lstClassName[i];
                            }
                        }
                    }
                }

                //lay danh sach hoc sinh theo lop
                EvalueExcel objValueExcel = null;

                objValueExcel = this.GetDataToFileExcel(
                    obook, lstPOC, lstStandardVM, lstStandardID, lstClassID,
                    healthPeriodID, ClassID, StandardID, EducationLevelID,
                    lstPupilID, academicYear, typeImport);

                ErrorOfPhysical = objValueExcel.ErrorOfPhysical;
                if (!string.IsNullOrEmpty(ErrorOfPhysical))
                {
                    return Json(new JsonMessage(ErrorOfPhysical, "error"));
                }

                ErrorOfEye = objValueExcel.ErrorOfEye;
                if (!string.IsNullOrEmpty(ErrorOfEye))
                {
                    return Json(new JsonMessage(ErrorOfEye, "error"));
                }

                ErrorOfENT = objValueExcel.ErrorOfENT;
                if (!string.IsNullOrEmpty(ErrorOfENT))
                {
                    return Json(new JsonMessage(ErrorOfENT, "error"));
                }

                ErrorOfDental = objValueExcel.ErrorOfDental;
                if (!string.IsNullOrEmpty(ErrorOfDental))
                {
                    return Json(new JsonMessage(ErrorOfDental, "error"));
                }

                ErrorOfSpine = objValueExcel.ErrorOfSpine;
                if (!string.IsNullOrEmpty(ErrorOfSpine))
                {
                    return Json(new JsonMessage(ErrorOfSpine, "error"));
                }

                ErrorOfOverAll = objValueExcel.ErrorOfOverAll;
                if (!string.IsNullOrEmpty(ErrorOfOverAll))
                {
                    return Json(new JsonMessage(ErrorOfOverAll, "error"));
                }

                List<ListErrorBO> LstErrorMessage = new List<ListErrorBO>();
                ListErrorBO objError = null;

                #region
                if (objValueExcel.lstMonitoringBookBOOfPhysical.Count > 0)
                {
                    lstMonitoringBookBOOfPhysical = objValueExcel.lstMonitoringBookBOOfPhysical;
                    lstPhysicalTestBO = objValueExcel.lstPhysicalTestBO;
                    lstPupilIDOfPhysical = objValueExcel.lstPupilIDOfPhysical;
                    lstErrorOfPhysical = objValueExcel.lstErrorIndexOfPhysical;
                    if (lstErrorOfPhysical.Count > 0)
                    {
                        foreach (var item in lstMonitoringBookBOOfPhysical)
                        {
                            if (item.ListErrorMessage != null)
                            {
                                foreach (var item2 in item.ListErrorMessage)
                                {
                                    var obj = LstErrorMessage.Where(x => x.PupilCode == item2.PupilCode && x.SheetName == item2.SheetName).Distinct().FirstOrDefault();
                                    if (obj != null)
                                    {
                                        obj.Description = obj.Description + "<br/> " + item2.Description;
                                        LstErrorMessage.Remove(obj);

                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = obj.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                    else
                                    {
                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = item2.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region
                if (objValueExcel.lstMonitoringBookBOOfEye.Count > 0)
                {
                    lstMonitoringBookBOOfEye = objValueExcel.lstMonitoringBookBOOfEye;
                    lstEyeTestBO = objValueExcel.lstEyeTestBO;
                    lstPupilIDOfEye = objValueExcel.lstPupilIDOfEye;
                    lstErrorOfEye = objValueExcel.lstErrorIndexOfEye;

                    if (lstErrorOfEye.Count > 0)
                    {
                        foreach (var item in lstMonitoringBookBOOfEye)
                        {
                            if (item.ListErrorMessage != null)
                            {
                                foreach (var item2 in item.ListErrorMessage)
                                {
                                    var obj = LstErrorMessage.Where(x => x.PupilCode == item2.PupilCode && x.SheetName == item2.SheetName).Distinct().FirstOrDefault();
                                    if (obj != null)
                                    {
                                        obj.Description = obj.Description + "<br/> " + item2.Description;
                                        LstErrorMessage.Remove(obj);

                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = obj.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                    else
                                    {
                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = item2.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }

                                }
                            }
                        }
                    }
                }
                #endregion

                #region
                if (objValueExcel.lstPupilIDOfENT.Count > 0)
                {
                    lstMonitoringBookBOOfENT = objValueExcel.lstMonitoringBookBOOfENT;
                    lstEntTestBO = objValueExcel.lstEntTestBO;
                    lstPupilIDOfENT = objValueExcel.lstPupilIDOfENT;
                    lstErrorOfENT = objValueExcel.lstErrorIndexOfENT;

                    if (lstErrorOfENT.Count > 0)
                    {
                        foreach (var item in lstMonitoringBookBOOfENT)
                        {
                            if (item.ListErrorMessage != null)
                            {
                                foreach (var item2 in item.ListErrorMessage)
                                {
                                    var obj = LstErrorMessage.Where(x => x.PupilCode == item2.PupilCode && x.SheetName == item2.SheetName).Distinct().FirstOrDefault();
                                    if (obj != null)
                                    {
                                        obj.Description = obj.Description + "<br/> " + item2.Description;
                                        LstErrorMessage.Remove(obj);

                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = obj.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                    else
                                    {
                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = item2.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region
                if (objValueExcel.lstMonitoringBookBOOfDental.Count > 0)
                {
                    lstMonitoringBookBOOfDental = objValueExcel.lstMonitoringBookBOOfDental;
                    lstDentalTestBO = objValueExcel.lstDentalTestBO;
                    lstPupilIDOfDental = objValueExcel.lstPupilIDOfDental;

                    lstErrorOfDental = objValueExcel.lstErrorIndexOfDental;

                    if (lstErrorOfDental.Count > 0)
                    {
                        foreach (var item in lstMonitoringBookBOOfDental)
                        {
                            if (item.ListErrorMessage != null)
                            {
                                foreach (var item2 in item.ListErrorMessage)
                                {
                                    var obj = LstErrorMessage.Where(x => x.PupilCode == item2.PupilCode && x.SheetName == item2.SheetName).Distinct().FirstOrDefault();
                                    if (obj != null)
                                    {
                                        obj.Description = obj.Description + "<br/> " + item2.Description;
                                        LstErrorMessage.Remove(obj);

                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = obj.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                    else
                                    {
                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = item2.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region
                if (objValueExcel.lstMonitoringBookBOOfSpine.Count > 0)
                {
                    lstMonitoringBookBOOfSpine = objValueExcel.lstMonitoringBookBOOfSpine;
                    lstSpineTestBO = objValueExcel.lstSpinTestBO;
                    lstPupilIDOfSpine = objValueExcel.lstPupilIDOfSpine;

                    lstErrorOfSpine = objValueExcel.lstErrorIndexOfSpine;

                    if (lstErrorOfSpine.Count > 0)
                    {
                        foreach (var item in lstMonitoringBookBOOfSpine)
                        {
                            if (item.ListErrorMessage != null)
                            {
                                foreach (var item2 in item.ListErrorMessage)
                                {
                                    var obj = LstErrorMessage.Where(x => x.PupilCode == item2.PupilCode && x.SheetName == item2.SheetName).Distinct().FirstOrDefault();
                                    if (obj != null)
                                    {
                                        obj.Description = obj.Description + "<br/> " + item2.Description;
                                        LstErrorMessage.Remove(obj);

                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = obj.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                    else
                                    {
                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = item2.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region
                if (objValueExcel.lstMonitoringBookBOOfOverAll.Count > 0)
                {
                    lstMonitoringBookBOOfOverAll = objValueExcel.lstMonitoringBookBOOfOverAll;
                    lstOverallTestBO = objValueExcel.lstOverAllTestBO;
                    lstPupilIDofOverAll = objValueExcel.lstPupilIDofOverAll;

                    lstErrorOfOverAll = objValueExcel.lstErrorIndexOfOverAll;

                    if (lstErrorOfOverAll.Count > 0)
                    {
                        foreach (var item in lstMonitoringBookBOOfOverAll)
                        {
                            if (item.ListErrorMessage != null)
                            {
                                foreach (var item2 in item.ListErrorMessage)
                                {
                                    var obj = LstErrorMessage.Where(x => x.PupilCode == item2.PupilCode && x.SheetName == item2.SheetName).Distinct().FirstOrDefault();
                                    if (obj != null)
                                    {
                                        obj.Description = obj.Description + "<br/> " + item2.Description;
                                        LstErrorMessage.Remove(obj);

                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = obj.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                    else
                                    {
                                        objError = new ListErrorBO();
                                        objError.PupilCode = item2.PupilCode;
                                        objError.PupilName = item2.PupilName;
                                        objError.Description = item2.Description;
                                        objError.SheetName = item2.SheetName;
                                        LstErrorMessage.Add(objError);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                if (LstErrorMessage.Count > 0)
                {
                    ViewData[MonitoringBookYearOfPupilConstant.LIST_ERROR_IMPORT] = LstErrorMessage;
                    return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "CreateViewError"));
                }

                Dictionary<string, object> dicInsert = new Dictionary<string, object>()
                {
                        {"SchoolID",_globalInfo.SchoolID.Value},
                        {"AcademicYearID",_globalInfo.AcademicYearID.Value},                      
                        {"EducationLevelID", EducationLevelID},
                        {"HealthPeriodID", healthPeriodID}
                 };

                if (typeImport == 1 || typeImport == 2)
                {
                    dicInsert.Add("ClassID", ClassID);
                }

                #region
                if (lstMonitoringBookBOOfPhysical.Count > 0)
                {
                    dicInsert.Add("lstPupilIDOfPhysical", lstPupilIDOfPhysical);
                    this.MonitoringBookBusiness.InsertOrUpdateValuePhysical(lstMonitoringBookBOOfPhysical, lstPhysicalTestBO, dicInsert);
                }
                #endregion

                #region
                if (lstMonitoringBookBOOfEye.Count > 0)
                {
                    dicInsert.Add("lstPupilIDOfEye", lstPupilIDOfEye);
                    this.MonitoringBookBusiness.InsertOrUpdateValueEye(lstMonitoringBookBOOfEye, lstEyeTestBO, dicInsert);
                }
                #endregion

                #region
                if (lstPupilIDOfENT.Count > 0)
                {
                    dicInsert.Add("lstPupilIDOfENT", lstPupilIDOfENT);
                    this.MonitoringBookBusiness.InsertOrUpdateValueENT(lstMonitoringBookBOOfENT, lstEntTestBO, dicInsert);
                }
                #endregion

                #region
                if (lstMonitoringBookBOOfDental.Count > 0)
                {
                    dicInsert.Add("lstPupilIDOfDental", lstPupilIDOfDental);
                    this.MonitoringBookBusiness.InsertOrUpdateValueDental(lstMonitoringBookBOOfDental, lstDentalTestBO, dicInsert);
                }
                #endregion

                #region
                if (lstMonitoringBookBOOfSpine.Count > 0)
                {
                    dicInsert.Add("lstPupilIDOfSpine", lstPupilIDOfSpine);
                    this.MonitoringBookBusiness.InsertOrUpdateValueSpine(lstMonitoringBookBOOfSpine, lstSpineTestBO, dicInsert);
                }
                #endregion

                #region
                if (lstMonitoringBookBOOfOverAll.Count > 0)
                {
                    dicInsert.Add("lstPupilIDofOverAll", lstPupilIDofOverAll);
                    this.MonitoringBookBusiness.InsertOrUpdateValueOverAll(lstMonitoringBookBOOfOverAll, lstOverallTestBO, dicInsert);
                }
                #endregion
                return Json(new JsonMessage(messageSuccess, "success"));
                #endregion
            }
            #endregion

            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }
        #endregion

        #region //GetDataToFileExcel
        private EvalueExcel GetDataToFileExcel(
          IVTWorkbook oBook,
           List<PupilOfClassBO> lstPOC,
           List<StandardViewModel> lstStandardVM,
           List<int> lstStandardID,
           List<int> lstClassID,
           int healthPeriodID,
           int ClassID,
           int StandardID,
           int EducationLevelID,
           List<int> lstPupilID,
           AcademicYear academicYear,
           int typeImport)
        {
            List<MonitoringBookBO> lstMonitoringBookBOOfPhysical = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfEye = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfENT = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfSpine = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfDental = new List<MonitoringBookBO>();
            List<MonitoringBookBO> lstMonitoringBookBOOfOverAll = new List<MonitoringBookBO>();
            List<PhysicalTestBO> lstPhysicalTestBO = new List<PhysicalTestBO>();
            List<EyeTestBO> lstEyeTestBO = new List<EyeTestBO>();
            List<ENTTestBO> lstEntTestBO = new List<ENTTestBO>();
            List<SpineTestBO> lstSpineTestBO = new List<SpineTestBO>();
            List<DentalTestBO> lstDentalTestBO = new List<DentalTestBO>();
            List<OverallTestBO> lstOverallTestBO = new List<OverallTestBO>();
            List<int> lstPupilIDOfPhysical = new List<int>();
            List<int> lstPupilIDOfEye = new List<int>();
            List<int> lstPupilIDOfENT = new List<int>();
            List<int> lstPupilIDOfSpine = new List<int>();
            List<int> lstPupilIDOfDental = new List<int>();
            List<int> lstPupilIDofOverAll = new List<int>();
            /*string ErrorOfPhysical = string.Empty;
            string ErrorOfEye = string.Empty;
            string ErrorOfENT = string.Empty;
            string ErrorOfDental = string.Empty;
            string ErrorOfSpine = string.Empty;
            string ErrorOfOverAll = string.Empty;*/
            List<int> lstErrorOfPhysical = new List<int>();
            List<int> lstErrorOfEye = new List<int>();
            List<int> lstErrorOfENT = new List<int>();
            List<int> lstErrorOfSpine = new List<int>();
            List<int> lstErrorOfDental = new List<int>();
            List<int> lstErrorOfOverAll = new List<int>();
            string Error = string.Empty;
            string sheetNameDefault = string.Empty;
            string sheetName = string.Empty;

            EvalueExcel objValueExcel = null;
            IVTWorksheet sheetByEvaluaName = null;
            StandardViewModel objStandardVM = null;

            string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
            string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
            HealthPeriod objHealth = HealthPeriodBusiness.All.Where(x => x.IsActive == true && x.HealthPeriodID == healthPeriodID).FirstOrDefault();

            if (typeImport == 1 || typeImport == 2)
            {
                // thông tin lop
                ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
                #region Validate du lieu
                for (int i = 0; i < lstStandardID.Count; i++)
                {
                    objStandardVM = lstStandardVM.Where(x => x.StandardID == lstStandardID[i]).FirstOrDefault();
                    sheetNameDefault = Utils.Utils.StripVNSignAndSpace(objStandardVM.StandardName, true);
                    sheetByEvaluaName = oBook.GetSheet(sheetNameDefault);

                    #region Kiem tra du lieu chon dau vao
                    string ClassName = classProfile.DisplayName;
                    string StandardName = objStandardVM.StandardName;
                    string AcademicYearNameFromExcel = (string)sheetByEvaluaName.GetCellValue("A6");
                    string Title = "";
                    var TitleObj = sheetByEvaluaName.GetCellValue("A5");

                    if (TitleObj != null)
                        Title = TitleObj.ToString();

                    if (Title == null || AcademicYearNameFromExcel == null)
                    {
                        Error = "File excel không đúng định dạng";
                    }

                    if (!Title.Contains(StandardName.ToUpper()))
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = "Danh mục " + StandardName + " không hợp lệ";
                        }
                    }

                    if (!AcademicYearNameFromExcel.ToUpper().Contains(ClassName.ToUpper()))
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = "File excel không phải của lớp " + ClassName;
                        }
                    }

                    if (!AcademicYearNameFromExcel.ToUpper().Contains(objHealth.Resolution.ToUpper()))
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = "File excel không phải của " + objHealth.Resolution;
                        }
                    }

                    if (!AcademicYearNameFromExcel.ToUpper().Contains(AcademicYearName.ToUpper())
                        && !AcademicYearNameFromExcel.ToUpper().Contains(_AcademicYearName.ToUpper()))
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = "Năm học không phải năm học hiện tại hoặc định dạng không hợp lệ";
                        }
                    }

                    if (!string.IsNullOrEmpty(Error))
                    {
                        if (objStandardVM.StandardID == 1)
                            return new EvalueExcel { ErrorOfPhysical = Error };
                        if (objStandardVM.StandardID == 2)
                            return new EvalueExcel { ErrorOfEye = Error };
                        if (objStandardVM.StandardID == 3)
                            return new EvalueExcel { ErrorOfENT = Error };
                        if (objStandardVM.StandardID == 4)
                            return new EvalueExcel { ErrorOfSpine = Error };
                        if (objStandardVM.StandardID == 5)
                            return new EvalueExcel { ErrorOfDental = Error };
                        if (objStandardVM.StandardID == 6)
                            return new EvalueExcel { ErrorOfOverAll = Error };
                    }
                    #endregion
                }
                #endregion

                #region lay du lieu tu file
                for (int i = 0; i < lstStandardID.Count; i++)
                {
                    objStandardVM = lstStandardVM.Where(x => x.StandardID == lstStandardID[i]).FirstOrDefault();
                    sheetNameDefault = Utils.Utils.StripVNSignAndSpace(objStandardVM.StandardName, true);
                    sheetByEvaluaName = oBook.GetSheet(sheetNameDefault);

                    switch (objStandardVM.StandardID)
                    {
                        case MonitoringBookYearOfPupilConstant.PHYSICAL_VALUE:
                            objValueExcel = this.GetDataPhysical(sheetByEvaluaName, lstMonitoringBookBOOfPhysical, lstPhysicalTestBO, lstPOC,
                            classProfile.DisplayName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfPhysical);
                            lstMonitoringBookBOOfPhysical = objValueExcel.lstMonitoringBookBOOfPhysical;
                            lstPhysicalTestBO = objValueExcel.lstPhysicalTestBO;
                            lstPupilIDOfPhysical = objValueExcel.lstPupilIDOfPhysical;
                            lstErrorOfPhysical = objValueExcel.lstErrorIndexOfPhysical;
                            break;
                        case MonitoringBookYearOfPupilConstant.EYE_VALUE:
                            objValueExcel = this.GetDataEye(sheetByEvaluaName, lstMonitoringBookBOOfEye, lstEyeTestBO, lstPOC,
                            classProfile.DisplayName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfEye);
                            lstMonitoringBookBOOfEye = objValueExcel.lstMonitoringBookBOOfEye;
                            lstEyeTestBO = objValueExcel.lstEyeTestBO;
                            lstPupilIDOfEye = objValueExcel.lstPupilIDOfEye;
                            lstErrorOfEye = objValueExcel.lstErrorIndexOfEye;
                            break;
                        case MonitoringBookYearOfPupilConstant.ENT_VALUE:
                            objValueExcel = this.GetDataEnt(sheetByEvaluaName, lstMonitoringBookBOOfENT, lstEntTestBO, lstPOC,
                            classProfile.DisplayName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfENT);
                            lstMonitoringBookBOOfENT = objValueExcel.lstMonitoringBookBOOfENT;
                            lstEntTestBO = objValueExcel.lstEntTestBO;
                            lstPupilIDOfENT = objValueExcel.lstPupilIDOfENT;
                            lstErrorOfENT = objValueExcel.lstErrorIndexOfENT;
                            break;
                        case MonitoringBookYearOfPupilConstant.SPINE_VALUE:
                            objValueExcel = this.GetDataSpin(sheetByEvaluaName, lstMonitoringBookBOOfSpine, lstSpineTestBO, lstPOC,
                            classProfile.DisplayName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfSpine);
                            lstMonitoringBookBOOfSpine = objValueExcel.lstMonitoringBookBOOfSpine;
                            lstSpineTestBO = objValueExcel.lstSpinTestBO;
                            lstPupilIDOfSpine = objValueExcel.lstPupilIDOfSpine;
                            lstErrorOfSpine = objValueExcel.lstErrorIndexOfSpine;
                            break;
                        case MonitoringBookYearOfPupilConstant.DENTAL_VALUE:
                            objValueExcel = this.GetDataDental(sheetByEvaluaName, lstMonitoringBookBOOfDental, lstDentalTestBO, lstPOC,
                           classProfile.DisplayName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfDental);
                            lstMonitoringBookBOOfDental = objValueExcel.lstMonitoringBookBOOfDental;
                            lstDentalTestBO = objValueExcel.lstDentalTestBO;
                            lstPupilIDOfDental = objValueExcel.lstPupilIDOfDental;
                            lstErrorOfDental = objValueExcel.lstErrorIndexOfDental;
                            break;
                        case MonitoringBookYearOfPupilConstant.OVERALL_VALUE:
                            objValueExcel = this.GetDataOverAll(sheetByEvaluaName, lstMonitoringBookBOOfOverAll, lstOverallTestBO, lstPOC,
                             classProfile.DisplayName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfOverAll, academicYear);
                            lstMonitoringBookBOOfOverAll = objValueExcel.lstMonitoringBookBOOfOverAll;
                            lstOverallTestBO = objValueExcel.lstOverAllTestBO;
                            lstPupilIDofOverAll = objValueExcel.lstPupilIDofOverAll;
                            lstErrorOfOverAll = objValueExcel.lstErrorIndexOfOverAll;
                            break;
                        default:
                            break;
                    }
                }
                #endregion
            }
            else
            {
                #region typeImport 3
                List<PupilOfClassBO> _lstPOC = new List<PupilOfClassBO>();
                List<string> lstClassName = lstPOC.Select(x => x.ClassName).Distinct().ToList();
                objStandardVM = lstStandardVM.Where(x => x.StandardID == StandardID).FirstOrDefault();
                string StandardName = objStandardVM.StandardName;
                string ClassName = string.Empty;
                for (int i = 0; i < lstClassName.Count; i++)
                {
                    ClassName = lstClassName[i];
                    sheetNameDefault = Utils.Utils.StripVNSignAndSpace(ClassName, true);
                    sheetByEvaluaName = oBook.GetSheet(sheetNameDefault);

                    #region Kiem tra du lieu chon dau vao
                    string AcademicYearNameFromExcel = (string)sheetByEvaluaName.GetCellValue("A6");
                    string Title = "";
                    var TitleObj = sheetByEvaluaName.GetCellValue("A5");

                    if (TitleObj != null)
                        Title = TitleObj.ToString();

                    if (Title == null || AcademicYearNameFromExcel == null)
                    {
                        Error = "File excel không đúng định dạng";
                    }

                    if (!Title.Contains(StandardName.ToUpper()))
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = "Danh mục " + StandardName + " không hợp lệ";
                        }
                    }

                    if (!AcademicYearNameFromExcel.ToUpper().Contains(ClassName.ToUpper()))
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = "File excel không phải của lớp " + ClassName;
                        }
                    }

                    if (!AcademicYearNameFromExcel.ToUpper().Contains(objHealth.Resolution.ToUpper()))
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = "File excel không phải của " + objHealth.Resolution;
                        }
                    }

                    if (!AcademicYearNameFromExcel.ToUpper().Contains(AcademicYearName.ToUpper())
                        && !AcademicYearNameFromExcel.ToUpper().Contains(_AcademicYearName.ToUpper()))
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = "Năm học không phải năm học hiện tại hoặc định dạng không hợp lệ";
                        }
                    }

                    if (!string.IsNullOrEmpty(Error))
                    {
                        if (objStandardVM.StandardID == 1)
                            return new EvalueExcel { ErrorOfPhysical = Error };
                        if (objStandardVM.StandardID == 2)
                            return new EvalueExcel { ErrorOfEye = Error };
                        if (objStandardVM.StandardID == 3)
                            return new EvalueExcel { ErrorOfENT = Error };
                        if (objStandardVM.StandardID == 4)
                            return new EvalueExcel { ErrorOfSpine = Error };
                        if (objStandardVM.StandardID == 5)
                            return new EvalueExcel { ErrorOfDental = Error };
                        if (objStandardVM.StandardID == 6)
                            return new EvalueExcel { ErrorOfOverAll = Error };
                    }
                    #endregion
                    ClassName = string.Empty;
                }

                for (int i = 0; i < lstClassID.Count; i++)
                {
                    _lstPOC = lstPOC.Where(x => x.ClassID == lstClassID[i]).ToList();
                    ClassName = _lstPOC.Count > 0 ? _lstPOC.FirstOrDefault().ClassName.ToString() : string.Empty;
                    sheetNameDefault = Utils.Utils.StripVNSignAndSpace(ClassName, true);
                    sheetByEvaluaName = oBook.GetSheet(sheetNameDefault);

                    switch (StandardID)
                    {
                        case MonitoringBookYearOfPupilConstant.PHYSICAL_VALUE:
                            objValueExcel = this.GetDataPhysical(sheetByEvaluaName, lstMonitoringBookBOOfPhysical, lstPhysicalTestBO, _lstPOC,
                             ClassName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfPhysical);
                            lstMonitoringBookBOOfPhysical = objValueExcel.lstMonitoringBookBOOfPhysical;
                            lstPhysicalTestBO = objValueExcel.lstPhysicalTestBO;
                            lstErrorOfPhysical = objValueExcel.lstErrorIndexOfPhysical;
                            foreach (var item in objValueExcel.lstPupilIDOfPhysical)
                            {
                                lstPupilIDOfPhysical.Add(item);
                            }
                            break;
                        case MonitoringBookYearOfPupilConstant.EYE_VALUE:
                            objValueExcel = this.GetDataEye(sheetByEvaluaName, lstMonitoringBookBOOfEye, lstEyeTestBO, _lstPOC,
                           ClassName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfEye);
                            lstMonitoringBookBOOfEye = objValueExcel.lstMonitoringBookBOOfEye;
                            lstEyeTestBO = objValueExcel.lstEyeTestBO;
                            lstErrorOfEye = objValueExcel.lstErrorIndexOfEye;
                            foreach (var item in objValueExcel.lstPupilIDOfEye)
                            {
                                lstPupilIDOfEye.Add(item);
                            }
                            break;
                        case MonitoringBookYearOfPupilConstant.ENT_VALUE:
                            objValueExcel = this.GetDataEnt(sheetByEvaluaName, lstMonitoringBookBOOfENT, lstEntTestBO, _lstPOC,
                             ClassName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfENT);
                            lstMonitoringBookBOOfENT = objValueExcel.lstMonitoringBookBOOfENT;
                            lstEntTestBO = objValueExcel.lstEntTestBO;
                            lstErrorOfENT = objValueExcel.lstErrorIndexOfENT;
                            foreach (var item in objValueExcel.lstPupilIDOfENT)
                            {
                                lstPupilIDOfENT.Add(item);
                            }
                            break;
                        case MonitoringBookYearOfPupilConstant.SPINE_VALUE:
                            objValueExcel = this.GetDataSpin(sheetByEvaluaName, lstMonitoringBookBOOfSpine, lstSpineTestBO, _lstPOC,
                             ClassName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfSpine);
                            lstMonitoringBookBOOfSpine = objValueExcel.lstMonitoringBookBOOfSpine;
                            lstSpineTestBO = objValueExcel.lstSpinTestBO;
                            lstErrorOfSpine = objValueExcel.lstErrorIndexOfSpine;
                            foreach (var item in objValueExcel.lstPupilIDOfSpine)
                            {
                                lstPupilIDOfSpine.Add(item);
                            }
                            break;
                        case MonitoringBookYearOfPupilConstant.DENTAL_VALUE:
                            objValueExcel = this.GetDataDental(sheetByEvaluaName, lstMonitoringBookBOOfDental, lstDentalTestBO, _lstPOC,
                            ClassName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfDental);
                            lstMonitoringBookBOOfDental = objValueExcel.lstMonitoringBookBOOfDental;
                            lstDentalTestBO = objValueExcel.lstDentalTestBO;
                            lstErrorOfDental = objValueExcel.lstErrorIndexOfDental;
                            foreach (var item in objValueExcel.lstPupilIDOfDental)
                            {
                                lstPupilIDOfDental.Add(item);
                            }
                            break;
                        case MonitoringBookYearOfPupilConstant.OVERALL_VALUE:
                            objValueExcel = this.GetDataOverAll(sheetByEvaluaName, lstMonitoringBookBOOfOverAll, lstOverallTestBO, _lstPOC,
                              ClassName, EducationLevelID, healthPeriodID, lstPupilID, lstErrorOfOverAll, academicYear);
                            lstMonitoringBookBOOfOverAll = objValueExcel.lstMonitoringBookBOOfOverAll;
                            lstOverallTestBO = objValueExcel.lstOverAllTestBO;
                            lstErrorOfOverAll = objValueExcel.lstErrorIndexOfOverAll;
                            foreach (var item in objValueExcel.lstPupilIDofOverAll)
                            {
                                lstPupilIDofOverAll.Add(item);
                            }
                            break;
                        default:
                            break;
                    }
                    ClassName = string.Empty;
                }
                #endregion
            }

            #region
            return new EvalueExcel
            {
                lstMonitoringBookBOOfPhysical = lstMonitoringBookBOOfPhysical,
                lstMonitoringBookBOOfEye = lstMonitoringBookBOOfEye,
                lstMonitoringBookBOOfOverAll = lstMonitoringBookBOOfOverAll,
                lstMonitoringBookBOOfDental = lstMonitoringBookBOOfDental,
                lstMonitoringBookBOOfENT = lstMonitoringBookBOOfENT,
                lstMonitoringBookBOOfSpine = lstMonitoringBookBOOfSpine,

                lstPhysicalTestBO = lstPhysicalTestBO,
                lstEyeTestBO = lstEyeTestBO,
                lstEntTestBO = lstEntTestBO,
                lstSpinTestBO = lstSpineTestBO,
                lstDentalTestBO = lstDentalTestBO,
                lstOverAllTestBO = lstOverallTestBO,

                lstPupilIDOfDental = lstPupilIDOfDental,
                lstPupilIDOfENT = lstPupilIDOfENT,
                lstPupilIDOfEye = lstPupilIDOfEye,
                lstPupilIDofOverAll = lstPupilIDofOverAll,
                lstPupilIDOfPhysical = lstPupilIDOfPhysical,
                lstPupilIDOfSpine = lstPupilIDOfSpine,

                lstErrorIndexOfOverAll = lstErrorOfOverAll,
                lstErrorIndexOfDental = lstErrorOfDental,
                lstErrorIndexOfENT = lstErrorOfENT,
                lstErrorIndexOfEye = lstErrorOfEye,
                lstErrorIndexOfPhysical = lstErrorOfPhysical,
                lstErrorIndexOfSpine = lstErrorOfSpine
            };
            #endregion
        }
        #endregion

        #region //GetDataPhysical
        private EvalueExcel GetDataPhysical(
            IVTWorksheet sheet,
            List<MonitoringBookBO> lstMonitoringBookBO,
            List<PhysicalTestBO> lstPhysicalTestBO,
            List<PupilOfClassBO> lstPOC,
            string ClassName,
            int EducationLevelID,
            int healthPeriodID,
            List<int> lstPupilID,
            List<int> lstErrorIndex)
        {
            int logChangeID = _globalInfo.IsAdminSchoolRole ? 0 : _globalInfo.EmployeeID.Value;
            PupilOfClassBO objPOC = null;
            string pupilCode = string.Empty;
            int pupilID = 0;
            int startRow = 9;
            int indexError = 0;
            MonitoringBookBO objMonitoringBookBO = null;
            PhysicalTestBO objPhysicalTestBO = null;
            List<int> lstNewPupilID = new List<int>();
            List<ListErrorBO> lstError = new List<ListErrorBO>();
            ListErrorBO objError = null;
            string lstEvaluaIndexNotFound = string.Empty;

            //duyệt nội dung đánh giá
            DateTime dateNow = DateTime.Now;
            while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
            {
                lstError = new List<ListErrorBO>();
                objMonitoringBookBO = new MonitoringBookBO();
                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    objError = new ListErrorBO();
                    objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh không tồn tại" : "- Mã trẻ không tồn tại";
                    objError.SheetName = sheet.Name;
                    lstError.Add(objError);
                    objMonitoringBookBO.ListErrorMessage = lstError;
                    lstMonitoringBookBO.Add(objMonitoringBookBO);
                    indexError++;

                    startRow++;
                    continue;
                }
                else
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    }
                    pupilID = objPOC.PupilID;
                    lstNewPupilID.Add(pupilID);
                    if (lstNewPupilID.Where(p => p == pupilID).Count() > 1)
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh bị trùng lập" : "- Mã trẻ bị trùng lập";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;

                    }

                }
             
                objMonitoringBookBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objMonitoringBookBO.SchoolID = _globalInfo.SchoolID.Value;
                objMonitoringBookBO.ClassID = objPOC.ClassID;
                objMonitoringBookBO.PupilID = pupilID;
                objMonitoringBookBO.BirthDate = objPOC.Birthday;
                objMonitoringBookBO.Genre = objPOC.Genre.Value;
                objMonitoringBookBO.MonitoringType = 2; // theo dõi theo đợt
                objMonitoringBookBO.CreateDate = DateTime.Now;
                objMonitoringBookBO.HealthPeriodID = healthPeriodID;
                objMonitoringBookBO.EducationLevelID = EducationLevelID;

                objPhysicalTestBO = new PhysicalTestBO();
                objPhysicalTestBO.PupilProFileID = pupilID;
                objPhysicalTestBO.ClassProFileID = objPOC.ClassID;
                objPhysicalTestBO.Date = dateNow;
                objPhysicalTestBO.ModifineDate = dateNow;

                //validate chiều cao
                if (sheet.GetCellValue(startRow, 4) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 4).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 4).ToString()) <= 0
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 4).ToString()) > 999.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Chiều cao không hợp lệ. Giá trị nhập phải là số trong khoảng (0 - 999.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objPhysicalTestBO.Height = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 4).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Chiều cao không hợp lệ. Giá trị nhập phải là số trong khoảng (0 - 999.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                //validate cân nặng
                if (sheet.GetCellValue(startRow, 5) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 5).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 5).ToString()) <= 0
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 5).ToString()) > 999.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Cân nặng không hợp lệ. Giá trị nhập phải là số trong khoảng (0 - 999.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objPhysicalTestBO.Weight = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 5).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Cân nặng không hợp lệ. Giá trị nhập phải là số trong khoảng (0 - 999.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                //validate vòng ngực
                if (sheet.GetCellValue(startRow, 6) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 6).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 6).ToString()) <= 0
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 6).ToString()) > 999.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Vòng ngực không hợp lệ. Giá trị nhập phải là số trong khoảng (0 - 999.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objPhysicalTestBO.Breast = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 6).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Vòng ngực không hợp lệ. Giá trị nhập phải là số trong khoảng (0 - 999.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }
                // validate phân loại thể lực
                if (sheet.GetCellValue(startRow, 7) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 7).ToString()))
                    {
                        if (Int32.Parse(sheet.GetCellValue(startRow, 7).ToString()) <= 0
                            || Int32.Parse(sheet.GetCellValue(startRow, 7).ToString()) > 5)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Phân loại thể lực không hợp lệ. Giá trị nhập phải là số nguyên trong khoảng [1 - 5]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objPhysicalTestBO.PhysicalClassification = Int32.Parse(sheet.GetCellValue(startRow, 7).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Phân loại thể lực không hợp lệ. Giá trị nhập phải là số nguyên trong khoảng [1 - 5]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                if (lstError.Count > 0)
                    objMonitoringBookBO.ListErrorMessage = lstError;

                lstMonitoringBookBO.Add(objMonitoringBookBO);
                lstPhysicalTestBO.Add(objPhysicalTestBO);
                startRow++;
            }

            if (indexError != 0)
                lstErrorIndex.Add(indexError);

            if (lstPupilID == null || lstPupilID.Count == 0)
                lstPupilID = lstNewPupilID;

            return new EvalueExcel
            {
                lstMonitoringBookBOOfPhysical = lstMonitoringBookBO,
                lstPhysicalTestBO = lstPhysicalTestBO,
                lstPupilIDOfPhysical = lstPupilID,
                lstErrorIndexOfPhysical = lstErrorIndex
            };
        }
        #endregion

        #region //GetDataEye
        private EvalueExcel GetDataEye(
          IVTWorksheet sheet,
           List<MonitoringBookBO> lstMonitoringBookBO,
           List<EyeTestBO> lstEyeTestBO,
           List<PupilOfClassBO> lstPOC,
           string ClassName,
           int EducationLevelID,
           int healthPeriodID,
           List<int> lstPupilID,
           List<int> lstErrorIndex)
        {
            int logChangeID = _globalInfo.IsAdminSchoolRole ? 0 : _globalInfo.EmployeeID.Value;
            PupilOfClassBO objPOC = null;
            string pupilCode = string.Empty;
            int pupilID = 0;
            int startRow = 10;
            int indexError = 0;
            MonitoringBookBO objMonitoringBookBO = null;
            EyeTestBO objEyeTestBO = null;
            List<int> lstNewPupilID = new List<int>();
            List<ListErrorBO> lstError = new List<ListErrorBO>();
            ListErrorBO objError = null;
            string lstEvaluaIndexNotFound = string.Empty;

            //duyệt nội dung đánh giá          
            while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
            {
                lstError = new List<ListErrorBO>();
                objMonitoringBookBO = new MonitoringBookBO();
                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    objError = new ListErrorBO();
                    objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh không tồn tại" : "- Mã trẻ không tồn tại";
                    objError.SheetName = sheet.Name;
                    lstError.Add(objError);
                    objMonitoringBookBO.ListErrorMessage = lstError;
                    lstMonitoringBookBO.Add(objMonitoringBookBO);
                    indexError++;

                    startRow++;
                    continue;
                }
                else
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    }
                    pupilID = objPOC.PupilID;
                    lstNewPupilID.Add(pupilID);
                    if (lstNewPupilID.Where(p => p == pupilID).Count() > 1)
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh bị trùng lập" : "- Mã trẻ bị trùng lập";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;

                    }

                }
                
                objMonitoringBookBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objMonitoringBookBO.SchoolID = _globalInfo.SchoolID.Value;
                objMonitoringBookBO.ClassID = objPOC.ClassID;
                objMonitoringBookBO.PupilID = pupilID;
                objMonitoringBookBO.MonitoringType = 2; // theo dõi theo đợt
                objMonitoringBookBO.CreateDate = DateTime.Now;
                objMonitoringBookBO.HealthPeriodID = healthPeriodID;
                objMonitoringBookBO.EducationLevelID = EducationLevelID;

                objEyeTestBO = new EyeTestBO();
                objEyeTestBO.ClassID = objPOC.ClassID;
                objEyeTestBO.PupilProFileID = pupilID;
                objEyeTestBO.Date = DateTime.Now;

                #region Validate dữ liệu
                // validate mắt trái - không kính
                if (sheet.GetCellValue(startRow, 4) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 4).ToString()))
                    {
                        if (Int32.Parse(sheet.GetCellValue(startRow, 4).ToString()) < 0
                            || Int32.Parse(sheet.GetCellValue(startRow, 4).ToString()) > 10)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt trái (không kính) không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 10]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.LEye = Int32.Parse(sheet.GetCellValue(startRow, 4).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt trái (không kính) không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 10]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt phải - không kính
                if (sheet.GetCellValue(startRow, 5) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 5).ToString()))
                    {
                        if (Int32.Parse(sheet.GetCellValue(startRow, 5).ToString()) < 0
                            || Int32.Parse(sheet.GetCellValue(startRow, 5).ToString()) > 10)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt phải (không kính) không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 10]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.REye = Int32.Parse(sheet.GetCellValue(startRow, 5).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt phải (không kính) không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 10]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt trái - có kính
                if (sheet.GetCellValue(startRow, 6) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 6).ToString()))
                    {
                        if (Int32.Parse(sheet.GetCellValue(startRow, 6).ToString()) < 0
                            || Int32.Parse(sheet.GetCellValue(startRow, 6).ToString()) > 10)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt trái (có kính) không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 10]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.LIsEye = Int32.Parse(sheet.GetCellValue(startRow, 6).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt trái (có kính) không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 10]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt phải - có kính
                if (sheet.GetCellValue(startRow, 7) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 7).ToString()))
                    {
                        if (Int32.Parse(sheet.GetCellValue(startRow, 7).ToString()) < 0
                            || Int32.Parse(sheet.GetCellValue(startRow, 7).ToString()) > 10)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt phải (có kính) không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 10]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.RIsEye = Int32.Parse(sheet.GetCellValue(startRow, 7).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt phải (có kính) không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 10]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt trái - cận thị
                if (sheet.GetCellValue(startRow, 8) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 8).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 8).ToString()) < 0.1m
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 8).ToString()) > 99.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt trái (cận thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.LMyopic = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 8).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt trái (cận thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt phải - cận thị
                if (sheet.GetCellValue(startRow, 9) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 9).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 9).ToString()) < 0.1m
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 9).ToString()) > 99.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt phải (cận thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.RMyopic = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 9).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt phải (cận thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt trái - viễn thị
                if (sheet.GetCellValue(startRow, 10) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 10).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 10).ToString()) < 0.1m
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 10).ToString()) > 99.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt trái (viễn thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.LFarsightedness = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 10).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt trái (viễn thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt phải - viễn thị
                if (sheet.GetCellValue(startRow, 11) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 11).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 11).ToString()) < 0.1m
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 11).ToString()) > 99.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt phải (viễn thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.RFarsightedness = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 11).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt phải (viễn thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt trái - loạn thị
                if (sheet.GetCellValue(startRow, 12) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 12).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 12).ToString()) < 0.1m
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 12).ToString()) > 99.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt trái (loạn thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.LAstigmatism = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 12).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt trái (loạn thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                // validate mắt phải - loạn thị
                if (sheet.GetCellValue(startRow, 13) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDecimal(sheet.GetCellValue(startRow, 13).ToString()))
                    {
                        if (SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 13).ToString()) < 0.1m
                            || SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 13).ToString()) > 99.9m)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá mắt phải (loạn thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEyeTestBO.RAstigmatism = SMAS.Business.Common.Utils.ToDecimal(sheet.GetCellValue(startRow, 13).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá mắt phải (loạn thị) không hợp lệ. Giá trị nhập phải là số trong khoảng [0.1 - 99.9]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                objEyeTestBO.IsTreatment = false;
                if (objEyeTestBO.LIsEye.HasValue || objEyeTestBO.RIsEye.HasValue)
                    objEyeTestBO.IsTreatment = true; // có kính

                objEyeTestBO.IsMyopic = false;
                if (objEyeTestBO.RMyopic.HasValue || objEyeTestBO.LMyopic.HasValue)
                    objEyeTestBO.IsMyopic = true;

                objEyeTestBO.IsFarsightedness = false;
                if (objEyeTestBO.RFarsightedness.HasValue || objEyeTestBO.LFarsightedness.HasValue)
                    objEyeTestBO.IsFarsightedness = true;

                objEyeTestBO.IsAstigmatism = false;
                if (objEyeTestBO.RAstigmatism.HasValue || objEyeTestBO.LAstigmatism.HasValue)
                    objEyeTestBO.IsAstigmatism = true;

                #endregion

                objEyeTestBO.Other = sheet.GetCellValue(startRow, 14) != null ? sheet.GetCellValue(startRow, 14).ToString() : string.Empty;
                if (!string.IsNullOrEmpty(objEyeTestBO.Other))
                {
                    if (objEyeTestBO.Other.Length > 200)
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Mô tả bệnh khác không được vượt quá 200 ký tự";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }
                objEyeTestBO.CreateDate = DateTime.Now;
                objEyeTestBO.ModifiedDate = DateTime.Now;
                objEyeTestBO.LogID = logChangeID;
                objEyeTestBO.IsActive = true;

                if (lstError.Count > 0)
                    objMonitoringBookBO.ListErrorMessage = lstError;

                lstMonitoringBookBO.Add(objMonitoringBookBO);
                lstEyeTestBO.Add(objEyeTestBO);
                startRow++;
            }

            if (indexError != 0)
                lstErrorIndex.Add(indexError);

            if (lstPupilID == null || lstPupilID.Count == 0)
                lstPupilID = lstNewPupilID;

            return new EvalueExcel
            {
                lstMonitoringBookBOOfEye = lstMonitoringBookBO,
                lstEyeTestBO = lstEyeTestBO,
                lstPupilIDOfEye = lstPupilID,
                lstErrorIndexOfEye = lstErrorIndex
            };
        }
        #endregion

        #region //GetDataEnt
        private EvalueExcel GetDataEnt(
          IVTWorksheet sheet,
          List<MonitoringBookBO> lstMonitoringBookBO,
          List<ENTTestBO> lstEntTestBO,
          List<PupilOfClassBO> lstPOC,
          string ClassName,
          int EducationLevelID,
          int healthPeriodID,
          List<int> lstPupilID,
          List<int> lstErrorIndex)
        {
            int logChangeID = _globalInfo.IsAdminSchoolRole ? 0 : _globalInfo.EmployeeID.Value;
            PupilOfClassBO objPOC = null;
            string pupilCode = string.Empty;
            int pupilID = 0;
            int startRow = 10;
            int indexError = 0;
            MonitoringBookBO objMonitoringBookBO = null;
            ENTTestBO objEntTestBO = null;
            List<int> lstNewPupilID = new List<int>();
            List<ListErrorBO> lstError = new List<ListErrorBO>();
            ListErrorBO objError = null;
            string lstEvaluaIndexNotFound = string.Empty;

            //duyệt nội dung đánh giá          
            while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
            {
                lstError = new List<ListErrorBO>();
                objMonitoringBookBO = new MonitoringBookBO();
                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    objError = new ListErrorBO();
                    objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh không tồn tại" : "- Mã trẻ không tồn tại";
                    objError.SheetName = sheet.Name;
                    lstError.Add(objError);
                    objMonitoringBookBO.ListErrorMessage = lstError;
                    lstMonitoringBookBO.Add(objMonitoringBookBO);
                    indexError++;

                    startRow++;
                    continue;
                }
                else
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    }

                    pupilID = objPOC.PupilID;
                    lstNewPupilID.Add(pupilID);
                    if (lstNewPupilID.Where(p => p == pupilID).Count() > 1)
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh bị trùng lập" : "- Mã trẻ bị trùng lập";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;

                    }
                    
                }
               
                objMonitoringBookBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objMonitoringBookBO.SchoolID = _globalInfo.SchoolID.Value;
                objMonitoringBookBO.ClassID = objPOC.ClassID;
                objMonitoringBookBO.PupilID = pupilID;
                objMonitoringBookBO.MonitoringType = 2; // theo dõi theo đợt
                objMonitoringBookBO.CreateDate = DateTime.Now;
                objMonitoringBookBO.HealthPeriodID = healthPeriodID;
                objMonitoringBookBO.EducationLevelID = EducationLevelID;

                objEntTestBO = new ENTTestBO();
                objEntTestBO.ClassID = objPOC.ClassID;
                objEntTestBO.PupilProFileID = pupilID;

                if (sheet.GetCellValue(startRow, 4) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 4).ToString()))
                    {
                        if (Int32.Parse(sheet.GetCellValue(startRow, 4).ToString()) < 0
                            || Int32.Parse(sheet.GetCellValue(startRow, 4).ToString()) > 1000)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá sức nghe tai trái không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 1000]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEntTestBO.LCapacityHearing = Int32.Parse(sheet.GetCellValue(startRow, 4).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá sức nghe tai trái không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 1000]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                if (sheet.GetCellValue(startRow, 5) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 5).ToString()))
                    {
                        if (Int32.Parse(sheet.GetCellValue(startRow, 5).ToString()) < 0
                            || Int32.Parse(sheet.GetCellValue(startRow, 5).ToString()) > 1000)
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá sức nghe tai phải không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 1000]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);
                            indexError++;
                        }
                        else
                        {
                            objEntTestBO.RCapacityHearing = Int32.Parse(sheet.GetCellValue(startRow, 5).ToString());
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá sức nghe tai phải không hợp lệ. Giá trị phải là số nguyên trong khoảng [0 - 1000]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }

                if (sheet.GetCellValue(startRow, 6) != null)
                {
                    if (sheet.GetCellValue(startRow, 6).ToString() == "X" || sheet.GetCellValue(startRow, 6).ToString() == "x")
                    {
                        objEntTestBO.IsDeaf = true;
                    }
                    else
                    {
                        objEntTestBO.IsDeaf = false;
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá điếc không hợp lệ. Nếu có nhập X hoặc x";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }
                else
                {
                    objEntTestBO.IsDeaf = false;
                }

                if (sheet.GetCellValue(startRow, 7) != null)
                {
                    if (sheet.GetCellValue(startRow, 7).ToString() == "X" || sheet.GetCellValue(startRow, 7).ToString() == "x")
                    {
                        objEntTestBO.IsSick = true;
                    }
                    else
                    {
                        objEntTestBO.IsSick = false;
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá có bệnh không hợp lệ. Nếu có nhập X hoặc x";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);
                        indexError++;
                    }
                }
                else
                {
                    objEntTestBO.IsSick = false;
                }

                objEntTestBO.Description = sheet.GetCellValue(startRow, 8) != null ? sheet.GetCellValue(startRow, 8).ToString() : string.Empty;
                if (!objEntTestBO.IsSick.Value)
                    objEntTestBO.Description = string.Empty;

                objEntTestBO.CreatedDate = DateTime.Now;
                objEntTestBO.IsActive = true;
                objEntTestBO.ModifiedDate = DateTime.Now;
                objEntTestBO.LogID = logChangeID;

                if (lstError.Count > 0)
                    objMonitoringBookBO.ListErrorMessage = lstError;

                lstMonitoringBookBO.Add(objMonitoringBookBO);
                lstEntTestBO.Add(objEntTestBO);
                startRow++;
            }

            if (indexError != 0)
                lstErrorIndex.Add(indexError);

            if (lstPupilID == null || lstPupilID.Count == 0)
                lstPupilID = lstNewPupilID;

            return new EvalueExcel
            {
                lstMonitoringBookBOOfENT = lstMonitoringBookBO,
                lstEntTestBO = lstEntTestBO,
                lstPupilIDOfENT = lstPupilID,
                lstErrorIndexOfENT = lstErrorIndex
            };
        }
        #endregion

        #region //GetDataSpin
        private EvalueExcel GetDataSpin(
          IVTWorksheet sheet,
           List<MonitoringBookBO> lstMonitoringBookBO,
           List<SpineTestBO> lstSpineTestBO,
           List<PupilOfClassBO> lstPOC,
           string ClassName,
           int EducationLevelID,
           int healthPeriodID,
           List<int> lstPupilID,
           List<int> lstErrorIndex)
        {
            int logChangeID = _globalInfo.IsAdminSchoolRole ? 0 : _globalInfo.EmployeeID.Value;
            PupilOfClassBO objPOC = null;
            string pupilCode = string.Empty;
            int pupilID = 0;
            int startRow = 9;
            int indexError = 0;
            MonitoringBookBO objMonitoringBookBO = null;
            SpineTestBO objSpineTestBO = null;
            List<int> lstNewPupilID = new List<int>();
            List<ListErrorBO> lstError = new List<ListErrorBO>();
            ListErrorBO objError = null;
            string lstEvaluaIndexNotFound = string.Empty;

            //duyệt nội dung đánh giá          
            while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
            {
                lstError = new List<ListErrorBO>();
                objMonitoringBookBO = new MonitoringBookBO();
                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    objError = new ListErrorBO();
                    objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh không tồn tại" : "- Mã trẻ không tồn tại";
                    objError.SheetName = sheet.Name;
                    lstError.Add(objError);
                    objMonitoringBookBO.ListErrorMessage = lstError;
                    lstMonitoringBookBO.Add(objMonitoringBookBO);
                    indexError++;

                    startRow++;
                    continue;
                }
                else
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    }

                    pupilID = objPOC.PupilID;
                    lstNewPupilID.Add(pupilID);
                    if (lstNewPupilID.Where(p => p == pupilID).Count() > 1)
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh bị trùng lập" : "- Mã trẻ bị trùng lập";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                       
                        indexError++;

                    }
                   
                }
              
                objMonitoringBookBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objMonitoringBookBO.SchoolID = _globalInfo.SchoolID.Value;
                objMonitoringBookBO.ClassID = objPOC.ClassID;
                objMonitoringBookBO.PupilID = pupilID;
                objMonitoringBookBO.MonitoringType = 2; // theo dõi theo đợt
                objMonitoringBookBO.CreateDate = DateTime.Now;
                objMonitoringBookBO.HealthPeriodID = healthPeriodID;
                objMonitoringBookBO.EducationLevelID = EducationLevelID;

                objSpineTestBO = new SpineTestBO();
                objSpineTestBO.ClassID = objPOC.ClassID;
                objSpineTestBO.PupilProFileID = pupilID;

                if (sheet.GetCellValue(startRow, 4) != null)
                {
                    if (sheet.GetCellValue(startRow, 4).ToString() == "X" || sheet.GetCellValue(startRow, 4).ToString() == "x")
                    {
                        objSpineTestBO.IsDeformity = true;
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá cong vẹo cột sống không hợp lệ. Nếu có nhập X hoặc x";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                       
                        indexError++;
                    }
                }
                else
                    objSpineTestBO.IsDeformity = false;

                objSpineTestBO.ExaminationStraight = sheet.GetCellValue(startRow, 5) != null ? sheet.GetCellValue(startRow, 5).ToString() : "";
                objSpineTestBO.ExaminationItalic = sheet.GetCellValue(startRow, 6) != null ? sheet.GetCellValue(startRow, 6).ToString() : "";
                objSpineTestBO.Other = sheet.GetCellValue(startRow, 7) != null ? sheet.GetCellValue(startRow, 7).ToString() : "";

                objSpineTestBO.CreateDate = DateTime.Now;
                objSpineTestBO.ModifiedDate = DateTime.Now;
                objSpineTestBO.LogID = logChangeID;
                objSpineTestBO.IsActive = true;

                if (lstError.Count > 0)
                    objMonitoringBookBO.ListErrorMessage = lstError;

                lstMonitoringBookBO.Add(objMonitoringBookBO);
                lstSpineTestBO.Add(objSpineTestBO);
                startRow++;
            }

            if (indexError != 0)
                lstErrorIndex.Add(indexError);

            if (lstPupilID == null || lstPupilID.Count == 0)
                lstPupilID = lstNewPupilID;

            return new EvalueExcel
            {
                lstMonitoringBookBOOfSpine = lstMonitoringBookBO,
                lstSpinTestBO = lstSpineTestBO,
                lstPupilIDOfSpine = lstPupilID,
                lstErrorIndexOfSpine = lstErrorIndex
            };
        }
        #endregion

        #region //GetDataDental
        private EvalueExcel GetDataDental(
           IVTWorksheet sheet,
           List<MonitoringBookBO> lstMonitoringBookBO,
           List<DentalTestBO> lstDentalTestBO,
           List<PupilOfClassBO> lstPOC,
           string ClassName,
           int EducationLevelID,
           int healthPeriodID,
           List<int> lstPupilID,
           List<int> lstErrorIndex)
        {
            int logChangeID = _globalInfo.IsAdminSchoolRole ? 0 : _globalInfo.EmployeeID.Value;
            PupilOfClassBO objPOC = null;
            string pupilCode = string.Empty;
            int pupilID = 0;
            int startRow = 11;
            int indexError = 0;
            MonitoringBookBO objMonitoringBookBO = null;
            DentalTestBO objDentalTestBO = null;
            List<int> lstNewPupilID = new List<int>();
            List<ListErrorBO> lstError = new List<ListErrorBO>();
            ListErrorBO objError = null;
            string lstEvaluaIndexNotFound = string.Empty;

            //duyệt nội dung đánh giá          
            while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
            {
                lstError = new List<ListErrorBO>();
                objMonitoringBookBO = new MonitoringBookBO();
                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    objError = new ListErrorBO();
                    objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh không tồn tại" : "- Mã trẻ không tồn tại";
                    objError.SheetName = sheet.Name;
                    lstError.Add(objError);
                    objMonitoringBookBO.ListErrorMessage = lstError;
                    lstMonitoringBookBO.Add(objMonitoringBookBO);
                    indexError++;

                    startRow++;
                    continue;
                }
                else
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    }

                    pupilID = objPOC.PupilID;
                    lstNewPupilID.Add(pupilID);
                    if (lstNewPupilID.Where(p => p == pupilID).Count() > 1)
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh bị trùng lập" : "- Mã trẻ bị trùng lập";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                       
                        indexError++;
                    }                  
                }
               
                objMonitoringBookBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objMonitoringBookBO.SchoolID = _globalInfo.SchoolID.Value;
                objMonitoringBookBO.ClassID = objPOC.ClassID;
                objMonitoringBookBO.PupilID = pupilID;
                objMonitoringBookBO.MonitoringType = 2; // theo dõi theo đợt
                objMonitoringBookBO.CreateDate = DateTime.Now;
                objMonitoringBookBO.HealthPeriodID = healthPeriodID;
                objMonitoringBookBO.EducationLevelID = EducationLevelID;

                objDentalTestBO = new DentalTestBO();
                objDentalTestBO.ClassID = objPOC.ClassID;
                objDentalTestBO.PupilProFileID = pupilID;

                if (sheet.GetCellValue(startRow, 38) != null)
                {
                    if (sheet.GetCellValue(startRow, 38).ToString() == "X" || sheet.GetCellValue(startRow, 38).ToString() == "x")
                    {
                        objDentalTestBO.IsScraptTeethCustom = true;
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá cạo vôi răng không hợp lệ. Nếu có nhập X hoặc x";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                      
                        indexError++;
                    }
                }
                else
                    objDentalTestBO.IsScraptTeethCustom = false;

                if (sheet.GetCellValue(startRow, 39) != null)
                {
                    if (sheet.GetCellValue(startRow, 39).ToString() == "X" || sheet.GetCellValue(startRow, 39).ToString() == "x")
                    {
                        objDentalTestBO.IsTreatmentCustom = true;
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá điều trị nha chu không hợp lệ. Nếu có nhập X hoặc x";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                        
                        indexError++;
                    }
                }
                else
                    objDentalTestBO.IsTreatmentCustom = false;

                objDentalTestBO.Other = sheet.GetCellValue(startRow, 40) != null ? sheet.GetCellValue(startRow, 40).ToString() : "";

                if (sheet.GetCellValue(startRow, 36) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 36).ToString()))
                    {
                        int numPre = Int32.Parse(sheet.GetCellValue(startRow, 36).ToString());
                        if (numPre < 33 && numPre >= 0)
                            objDentalTestBO.NumPreventiveDentalFilling = numPre;
                        else
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá số răng cần trám không hợp lệ. Giá trị phải là số nguyên trong khoảng [1 - 32]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);                           
                            indexError++;
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá số răng cần trám không hợp lệ. Giá trị phải là số nguyên trong khoảng [1 - 32]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                        
                        indexError++;
                    }
                }

                if (sheet.GetCellValue(startRow, 37) != null)
                {
                    if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 37).ToString()))
                    {
                        int numExtract = Int32.Parse(sheet.GetCellValue(startRow, 37).ToString());
                        if (numExtract < 33 && numExtract >= 0)
                        {
                            objDentalTestBO.NumTeethExtracted = numExtract;
                        }
                        else
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Đánh giá số răng cần nhổ không hợp lệ. Giá trị phải là số nguyên trong khoảng [1 - 32]";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);                           
                            indexError++;
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá số răng cần nhổ không hợp lệ. Giá trị phải là số nguyên trong khoảng [1 - 32]";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                        
                        indexError++;
                    }
                }



                int count = 0;
                int one = 8; string Fillone = string.Empty;
                int two = 1; string Filltwo = string.Empty;
                int four = 8; string Fillfour = string.Empty;
                int three = 1; string Fillthree = string.Empty;
                for (int k = 4; k < 36; k++)
                {
                    if (sheet.GetCellValue(startRow, k) != null)
                    {
                        count++;
                    }
                    if (k <= 11)
                    {
                        if (sheet.GetCellValue(startRow, k) != null)
                        {
                            Fillone = Fillone + "," + one; //,8,7
                        }
                        one--;
                    }
                    else if (k <= 19)
                    {
                        if (sheet.GetCellValue(startRow, k) != null)
                        {
                            Filltwo = Filltwo + "," + two; //,1,2
                        }
                        two++;
                    }
                    else if (k <= 27)
                    {
                        if (sheet.GetCellValue(startRow, k) != null)
                        {
                            Fillfour = Fillfour + "," + four; //,8,7
                        }
                        four--;
                    }
                    else if (k <= 35)
                    {
                        if (sheet.GetCellValue(startRow, k) != null)
                        {
                            Fillthree = Fillthree + "," + three; //,1,2
                        }
                        three++;
                    }
                }
                objDentalTestBO.NumDentalFillingCustom = count;
                objDentalTestBO.NumDentalFillingOne = Fillone != "" ? Fillone.Substring(1) + "," : "";
                objDentalTestBO.NumDentalFillingTwo = Filltwo != "" ? Filltwo.Substring(1) + "," : "";
                objDentalTestBO.NumDentalFillingThree = Fillthree != "" ? Fillthree.Substring(1) + "," : "";
                objDentalTestBO.NumDentalFillingFour = Fillfour != "" ? Fillfour.Substring(1) + "," : "";

                objDentalTestBO.CreateDate = DateTime.Now;
                objDentalTestBO.ModifiedDate = DateTime.Now;
                objDentalTestBO.LogID = logChangeID;
                objDentalTestBO.IsActive = true;

                if (lstError.Count > 0)
                    objMonitoringBookBO.ListErrorMessage = lstError;

                lstMonitoringBookBO.Add(objMonitoringBookBO);
                lstDentalTestBO.Add(objDentalTestBO);
                startRow++;
            }

            if (indexError != 0)
                lstErrorIndex.Add(indexError);

            if (lstPupilID == null || lstPupilID.Count == 0)
                lstPupilID = lstNewPupilID;

            return new EvalueExcel
            {
                lstMonitoringBookBOOfDental = lstMonitoringBookBO,
                lstDentalTestBO = lstDentalTestBO,
                lstPupilIDOfDental = lstPupilID,
                lstErrorIndexOfDental = lstErrorIndex
            };
        }
        #endregion

        #region //GetDataOverAll
        private EvalueExcel GetDataOverAll(
           IVTWorksheet sheet,
           List<MonitoringBookBO> lstMonitoringBookBO,
           List<OverallTestBO> lstOverallTestBO,
           List<PupilOfClassBO> lstPOC,
           string ClassName,
           int EducationLevelID,
           int healthPeriodID,
           List<int> lstPupilID,
           List<int> lstErrorIndex,
           AcademicYear ay)
        {
            int logChangeID = _globalInfo.IsAdminSchoolRole ? 0 : _globalInfo.EmployeeID.Value;
            PupilOfClassBO objPOC = null;
            string pupilCode = string.Empty;
            int pupilID = 0;
            int startRow = 9;
            int indexError = 0;
            MonitoringBookBO objMonitoringBookBO = null;
            OverallTestBO objOverallTestBO = null;
            List<int> lstNewPupilID = new List<int>();
            List<ListErrorBO> lstError = new List<ListErrorBO>();
            ListErrorBO objError = null;
            string lstEvaluaIndexNotFound = string.Empty;

            DateTime dateNow = DateTime.Now;
            //duyệt nội dung đánh giá          
            while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
            {
                lstError = new List<ListErrorBO>();
                objMonitoringBookBO = new MonitoringBookBO();
                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    objError = new ListErrorBO();
                    objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh không tồn tại" : "- Mã trẻ không tồn tại";
                    objError.SheetName = sheet.Name;
                    lstError.Add(objError);
                    objMonitoringBookBO.ListErrorMessage = lstError;
                    lstMonitoringBookBO.Add(objMonitoringBookBO);
                    indexError++;

                    startRow++;
                    continue;
                }
                else
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    } 

                    pupilID = objPOC.PupilID;
                    lstNewPupilID.Add(pupilID);
                    if (lstNewPupilID.Where(p => p == pupilID).Count() > 1)
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = _globalInfo.AppliedLevel <= 3 ? "- Mã học sinh bị trùng lập" : "- Mã trẻ bị trùng lập";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                     
                        indexError++;

                    }
                    
                }
         
                objMonitoringBookBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objMonitoringBookBO.SchoolID = _globalInfo.SchoolID.Value;
                objMonitoringBookBO.ClassID = objPOC.ClassID;
                objMonitoringBookBO.PupilID = pupilID;
                objMonitoringBookBO.MonitoringType = 2; // theo dõi theo đợt
                objMonitoringBookBO.CreateDate = DateTime.Now;
                objMonitoringBookBO.HealthPeriodID = healthPeriodID;
                objMonitoringBookBO.EducationLevelID = EducationLevelID;

                objOverallTestBO = new OverallTestBO();
                objOverallTestBO.ClassID = objPOC.ClassID;
                objOverallTestBO.PupilProFileID = pupilID;

                // validate ngày khám
                if (sheet.GetCellValue(startRow, 4) != null)
                {
                    if (SMAS.Business.Common.Utils.IsDateTime(sheet.GetCellValue(startRow, 4).ToString()))
                    {
                        string dateVal = sheet.GetCellValue(startRow, 4).ToString();
                        DateTime inputDate = DateTime.Now;
                        if (dateVal.Length > 10)
                        {
                            List<string> lstdateVal = dateVal.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                            List<string> lstdate = lstdateVal[0].Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                            inputDate = new DateTime(Int32.Parse(lstdate[2]), Int32.Parse(lstdate[0]), Int32.Parse(lstdate[1]));
                        }
                        else if (dateVal.Length <= 10)
                        {
                            inputDate = Convert.ToDateTime(dateVal);
                        }
                   
                        double dTotalDays = (dateNow - inputDate).TotalDays;
                        if (dTotalDays >= 0)
                        {                          
                            if (inputDate.Date < ay.FirstSemesterStartDate.Value.Date)
                            {
                                objError = new ListErrorBO();
                                objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                                objError.PupilCode = pupilCode;
                                objError.Description = "- Ngày khám phải lớn hơn ngày " + ay.FirstSemesterStartDate.Value.Date.ToString("dd/MM/yyyy") + " bắt đầu năm học";
                                objError.SheetName = sheet.Name;
                                lstError.Add(objError);
                                indexError++;
                            }
                            else if (inputDate.Date > ay.SecondSemesterEndDate.Value.Date)
                            {
                                objError = new ListErrorBO();
                                objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                                objError.PupilCode = pupilCode;
                                objError.Description = "- Ngày khám phải nhỏ hơn ngày " + ay.SecondSemesterEndDate.Value.Date.ToString("dd/MM/yyyy") + " kết thúc năm học";
                                objError.SheetName = sheet.Name;
                                lstError.Add(objError);                                
                                indexError++;
                            }
                            else
                                objMonitoringBookBO.MonitoringDate = inputDate;
                        }
                        else
                        {
                            objError = new ListErrorBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "- Ngày khám không được lớn hơn ngày hiện tại";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);                          
                            indexError++;
                        }
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Định dạng ngày khám không hợp lệ";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                      
                        indexError++;
                    }
                }

                // validate đơn vị khám
                objMonitoringBookBO.UnitName = sheet.GetCellValue(startRow, 5) != null ? sheet.GetCellValue(startRow, 5).ToString() : null;
                if (!string.IsNullOrEmpty(objMonitoringBookBO.UnitName))
                {
                    if (objMonitoringBookBO.UnitName.Length > 100)
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đơn vị khám không nhập quá 100 ký tự";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                        
                        indexError++;
                    }
                }

                objOverallTestBO.Doctor = sheet.GetCellValue(startRow, 6) != null ? sheet.GetCellValue(startRow, 6).ToString() : "";
                objOverallTestBO.HistoryYourSelf = sheet.GetCellValue(startRow, 7) != null ? sheet.GetCellValue(startRow, 7).ToString() : "";
                objOverallTestBO.DescriptionOverallInternal = sheet.GetCellValue(startRow, 8) != null ? sheet.GetCellValue(startRow, 8).ToString() : "";
                objOverallTestBO.OverallInternal = sheet.GetCellValue(startRow, 8) != null ? 1 : 2;
                objOverallTestBO.DescriptionOverallForeign = sheet.GetCellValue(startRow, 9) != null ? sheet.GetCellValue(startRow, 9).ToString() : "";
                objOverallTestBO.OverallForeign = sheet.GetCellValue(startRow, 9) != null ? 1 : 2;
                objOverallTestBO.DescriptionSkinDiseases = sheet.GetCellValue(startRow, 10) != null ? sheet.GetCellValue(startRow, 10).ToString() : "";
                objOverallTestBO.SkinDiseases = sheet.GetCellValue(startRow, 10) != null ? 1 : 2;
                objOverallTestBO.Other = sheet.GetCellValue(startRow, 11) != null ? sheet.GetCellValue(startRow, 11).ToString() : "";

                if (sheet.GetCellValue(startRow, 12) != null)
                {
                    if (sheet.GetCellValue(startRow, 12).ToString() == "X" || sheet.GetCellValue(startRow, 12).ToString() == "x")
                    {
                        objOverallTestBO.IsHeartDiseases = true;
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá bệnh tim không hợp lệ. Nếu có nhập X hoặc x";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                      
                        indexError++;
                    }
                }
                else
                    objOverallTestBO.IsHeartDiseases = false;

                if (sheet.GetCellValue(startRow, 13) != null)
                {
                    if (sheet.GetCellValue(startRow, 13).ToString() == "X" || sheet.GetCellValue(startRow, 13).ToString() == "x")
                    {
                        objOverallTestBO.IsBirthDefect = true;
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá dị tật bẩm sinh không hợp lệ. Nếu có nhập X hoặc x";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                      
                        indexError++;
                    }
                }
                else
                    objOverallTestBO.IsBirthDefect = false;

                if (sheet.GetCellValue(startRow, 14) != null)
                {
                    string b = sheet.GetCellValue(startRow, 14).ToString();
                    if (sheet.GetCellValue(startRow, 14).ToString() == "a" || sheet.GetCellValue(startRow, 14).ToString() == "A")
                        objOverallTestBO.EvaluationHealth = 1;
                    else if (sheet.GetCellValue(startRow, 14).ToString() == "b" || sheet.GetCellValue(startRow, 14).ToString() == "B")
                        objOverallTestBO.EvaluationHealth = 2;
                    else if (sheet.GetCellValue(startRow, 14).ToString() == "c" || sheet.GetCellValue(startRow, 14).ToString() == "C")
                        objOverallTestBO.EvaluationHealth = 3;
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString(); ;
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá sức khỏe không hợp lệ. Loại A nhập A hoặc a, Loại B nhập B hoặc b, Loại C nhập C hoặc c ";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                       
                        indexError++;
                    }
                }
                else
                    objOverallTestBO.EvaluationHealth = 0;

                if (sheet.GetCellValue(startRow, 15) != null)
                {
                    if (sheet.GetCellValue(startRow, 15).ToString().Length < 1000)
                    {
                        objOverallTestBO.RequireOfDoctor = sheet.GetCellValue(startRow, 15) != null ? sheet.GetCellValue(startRow, 15).ToString() : "";
                    }
                    else
                    {
                        objError = new ListErrorBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "- Đánh giá đề nghị không hợp lệ. Độ dài phải nằm trong khoảng [0 - 1000] ";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);                        
                        indexError++;
                    }
                }
                else
                {
                    objOverallTestBO.RequireOfDoctor = sheet.GetCellValue(startRow, 15) != null ? sheet.GetCellValue(startRow, 15).ToString() : "";
                }

                objOverallTestBO.CreateDate = DateTime.Now;
                objOverallTestBO.ModifiedDate = DateTime.Now;
                objOverallTestBO.LogID = logChangeID;
                objOverallTestBO.IsActive = true;

                if (lstError.Count > 0)
                    objMonitoringBookBO.ListErrorMessage = lstError;                  

                lstMonitoringBookBO.Add(objMonitoringBookBO);
                lstOverallTestBO.Add(objOverallTestBO);
                startRow++;
            }

            if (indexError != 0)
                lstErrorIndex.Add(indexError);

            if (lstPupilID == null || lstPupilID.Count == 0)
                lstPupilID = lstNewPupilID;

            return new EvalueExcel
            {
                lstMonitoringBookBOOfOverAll = lstMonitoringBookBO,
                lstOverAllTestBO = lstOverallTestBO,
                lstPupilIDofOverAll = lstPupilID,
                lstErrorIndexOfOverAll = lstErrorIndex
            };
        }
        #endregion

        #region EvalueExcel
        class EvalueExcel
        {
            public List<MonitoringBookBO> lstMonitoringBookBOOfPhysical;
            public List<MonitoringBookBO> lstMonitoringBookBOOfEye;
            public List<MonitoringBookBO> lstMonitoringBookBOOfENT;
            public List<MonitoringBookBO> lstMonitoringBookBOOfDental;
            public List<MonitoringBookBO> lstMonitoringBookBOOfSpine;
            public List<MonitoringBookBO> lstMonitoringBookBOOfOverAll;
            public List<PhysicalTestBO> lstPhysicalTestBO;
            public List<EyeTestBO> lstEyeTestBO;
            public List<ENTTestBO> lstEntTestBO;
            public List<SpineTestBO> lstSpinTestBO;
            public List<DentalTestBO> lstDentalTestBO;
            public List<OverallTestBO> lstOverAllTestBO;
            public List<int> lstPupilIDOfPhysical;
            public List<int> lstPupilIDOfEye;
            public List<int> lstPupilIDOfENT;
            public List<int> lstPupilIDOfSpine;
            public List<int> lstPupilIDOfDental;
            public List<int> lstPupilIDofOverAll;
            public string ErrorOfPhysical;
            public string ErrorOfEye;
            public string ErrorOfENT;
            public string ErrorOfDental;
            public string ErrorOfSpine;
            public string ErrorOfOverAll;
            public List<int> lstErrorIndexOfPhysical;
            public List<int> lstErrorIndexOfEye;
            public List<int> lstErrorIndexOfENT;
            public List<int> lstErrorIndexOfSpine;
            public List<int> lstErrorIndexOfDental;
            public List<int> lstErrorIndexOfOverAll;
        }
        #endregion
        #endregion

        #region // print
        public ActionResult PrintDetailHealthTest(string arrpupilID, int ClassID, int StandardID, int HealthPeriodID)
        {
            List<int> lstPupilID = arrpupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Int32.Parse(x)).ToList();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["lstPupilID"] = lstPupilID;
            dic["ClassID"] = ClassID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = HealthPeriodID;
            List<MonitoringBook> LstMonitoringBook = ListMonitoringBook(dic).ToList();
            MonitoringBook monitoringBook = null;
            List<PupilProfileBO> lstPupilProfile = PupilProfileBusiness.Search(dic).Where(x => lstPupilID.Contains(x.PupilProfileID)).ToList();
            PupilProfileBO objPupil = null;

            string schoolName = new GlobalInfo().SchoolName;
            if (schoolName.Contains("Trường"))
            {
                schoolName = schoolName.Replace("Trường", "");
            }

            ObjectHealthyTest objModel = new ObjectHealthyTest();
            List<HealthTestDetailViewModel> lstmodel = new List<HealthTestDetailViewModel>();
            HealthTestDetailViewModel model = null;
            foreach (var item in lstPupilID)
            {
                monitoringBook = LstMonitoringBook.Where(x => x.PupilID == item).FirstOrDefault();
                objPupil = lstPupilProfile.Where(x => x.PupilProfileID == item).FirstOrDefault();
                model = new HealthTestDetailViewModel();

                model.PupilID = item;
                model.ClassID = ClassID;
                model.SchoolName = schoolName;
                model.ProvinceName = ProvinceBusiness.Find(_globalInfo.ProvinceID.Value).ProvinceName;

                if (objPupil != null)
                {
                    model.BirthDate = objPupil.BirthDate;
                    model.FullName = objPupil.FullName;
                    model.Address = objPupil.PermanentResidentalAddress;
                    model.ClassName = objPupil.ClassName;
                    model.GenreName = CommonList.GenreAndSelect().Where(u => u.key.Equals(objPupil.Genre.ToString())).FirstOrDefault().value;
                }

                if (monitoringBook != null)
                {
                    model.MonitoringBook = monitoringBook;
                    model.DentalTest = monitoringBook.DentalTests.FirstOrDefault();
                    model.ENTTest = monitoringBook.ENTTests.FirstOrDefault();
                    model.EyeTest = monitoringBook.EyeTests.FirstOrDefault();
                    model.OverallTest = monitoringBook.OverallTests.FirstOrDefault();
                    model.PhysicalTest = monitoringBook.PhysicalTests.FirstOrDefault();
                    model.SpineTest = monitoringBook.SpineTests.FirstOrDefault();
                }
                lstmodel.Add(model);
            }

            objModel.ListObjectHealthy = lstmodel;
            return View(objModel);
        }
        #endregion
    }
}
