﻿using SMAS.Web.Areas.MonitoringBookYearOfPupilArea.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea
{
    public static class Common
    {
        public static bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            return DateTime.TryParse(txtDate, out tempDate);
        }

        public static List<StandardViewModel> StandardVM()
        {
            List<StandardViewModel> lstStandard = new List<StandardViewModel>();
            lstStandard.Add(new StandardViewModel
            {
                StandardID = MonitoringBookYearOfPupilConstant.PHYSICAL_VALUE,
                StandardName = MonitoringBookYearOfPupilConstant.PHYSICAL
            });
            lstStandard.Add(new StandardViewModel
            {
                StandardID = MonitoringBookYearOfPupilConstant.EYE_VALUE,
                StandardName = MonitoringBookYearOfPupilConstant.EYE
            });

            lstStandard.Add(new StandardViewModel
            {
                StandardID = MonitoringBookYearOfPupilConstant.ENT_VALUE,
                StandardName = MonitoringBookYearOfPupilConstant.ENT
            });
            lstStandard.Add(new StandardViewModel
            {
                StandardID = MonitoringBookYearOfPupilConstant.SPINE_VALUE,
                StandardName = MonitoringBookYearOfPupilConstant.SPINE
            });
            lstStandard.Add(new StandardViewModel
            {
                StandardID = MonitoringBookYearOfPupilConstant.DENTAL_VALUE,
                StandardName = MonitoringBookYearOfPupilConstant.DENTAL
            });
            lstStandard.Add(new StandardViewModel
            {
                StandardID = MonitoringBookYearOfPupilConstant.OVERALL_VALUE,
                StandardName = MonitoringBookYearOfPupilConstant.OVERALL
            });

            return lstStandard;
        }
    }
}