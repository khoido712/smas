﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea
{
    public class MonitoringBookYearOfPupilConstant
    {
        public const string PHYSICAL = "Thể lực";
        public const int PHYSICAL_VALUE = 1;
        public const string EYE = "Mắt";
        public const int EYE_VALUE = 2;     
        public const string ENT = "Tai mũi họng";
        public const int ENT_VALUE = 3;
        public const string SPINE = "Cột sống";
        public const int SPINE_VALUE = 4;
        public const string DENTAL = "Răng hàm mặt";
        public const int DENTAL_VALUE = 5;
        public const string OVERALL = "Tổng quát";
        public const int OVERALL_VALUE = 6;

        public const string NORMAL = "Bình thường";
        public const string UN_NORMAL = "Suy dinh dưỡng";
        public const string FAT = "Béo phì";
        //
        public const string LIST_PHYSICAL_VM = "listPhysicalViewModel";
        public const string LIST_SPIN_VM = "listSpinViewModel";
        public const string LIST_DENTAL_VM = "listDentalViewModel";
        public const string LIST_OVERALL_VM = "listOverAllViewModel";
        public const string LIST_EYE_VM = "listEyeViewModel";
        public const string LIST_ENT_VM = "listEntViewModel";

        public const string CLASS_NULL = "classnull";
        public const string LIST_CLASS = "listClass";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_STANDARD = "listStandard";
        public const string LIST_HEALTH = "listHealth";
        public const string ISHEALTH = "isHealth";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string CELL_CHECK_TYPE_FILE_IMPORT = "Y3";
        public const string PHISICAL_PATH = "PhisicalPath";
        public const string LIST_ERROR_IMPORT = "ListErrorImport";
    }
}