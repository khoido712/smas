﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MonitoringBookYearOfPupilArea
{
    public class MonitoringBookYearOfPupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MonitoringBookYearOfPupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MonitoringBookYearOfPupilArea_default",
                "MonitoringBookYearOfPupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
