/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ExamViolationTypeArea
{
    public class ExamViolationTypeConstants
    {
        public const string LIST_EXAMVIOLATIONTYPE = "listExamViolationType";
        public const bool IS_APPLIED_CANDIDATE = true;
        public const bool IS_APPLIED_INVIGILATOR = true;
        public const string VISIABLE_MARK = "Visiablemark";
    }
}