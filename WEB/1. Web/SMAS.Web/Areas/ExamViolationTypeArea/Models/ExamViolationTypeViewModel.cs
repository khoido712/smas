/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ExamViolationTypeArea.Models
{
    public class ExamViolationTypeViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 ExamViolationTypeID { get; set; }

        [ResourceDisplayName("ExamViolationType_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Resolution { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.Int32> SchoolID { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.Boolean> AppliedForCandidate { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.Boolean> AppliedForInvigilator { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 IsAppliedFor { get; set; }


        [ResourceDisplayName("ExamViolationType_Label_PenalizedMark")]
       // [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumberInt")]
       // [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Nullable<System.Int32> PenalizedMark { get; set; }

        [ScaffoldColumn(false)]
        public System.String Penalization { get; set; }

        [ResourceDisplayName("ExamViolationType_Label_Description")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        public System.Boolean IsActive { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }

    }
}


