﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamViolationTypeArea
{
    public class ExamViolationTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamViolationTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamViolationTypeArea_default",
                "ExamViolationTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
