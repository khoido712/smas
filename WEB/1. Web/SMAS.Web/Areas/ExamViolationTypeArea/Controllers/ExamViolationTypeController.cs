﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ExamViolationTypeArea.Models;

namespace SMAS.Web.Areas.ExamViolationTypeArea.Controllers
{
    public class ExamViolationTypeController : BaseController
    {
        private readonly IExamViolationTypeBusiness ExamViolationTypeBusiness;
        private readonly IExamPupilViolateBusiness ExamPupilViolateBusiness;
        private readonly IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness;

        public ExamViolationTypeController(IExamViolationTypeBusiness examviolationtypeBusiness, IExamPupilViolateBusiness ExamPupilViolateBusiness
            , IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness)
        {
            this.ExamViolationTypeBusiness = examviolationtypeBusiness;
            this.ExamPupilViolateBusiness = ExamPupilViolateBusiness;
            this.ExamSupervisoryViolateBusiness = ExamSupervisoryViolateBusiness;
        }

        //
        // GET: /ExamViolationType/

        public ActionResult Index()
        {
            SetViewDataPermission("ExamViolationType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            IEnumerable<ExamViolationTypeViewModel> lst = this._Search(SearchInfo).OrderBy(o => o.Resolution);
            ViewData[ExamViolationTypeConstants.LIST_EXAMVIOLATIONTYPE] = lst;
            return View();
        }

        //
        // GET: /ExamViolationType/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            SearchInfo["Resolution"] = frm.Resolution;
            SearchInfo["IsAppliedFor"] = frm.IsAppliedFor;
            IEnumerable<ExamViolationTypeViewModel> lst = this._Search(SearchInfo).OrderBy(o => o.Resolution);
            ViewData[ExamViolationTypeConstants.LIST_EXAMVIOLATIONTYPE] = lst;
            ViewData[ExamViolationTypeConstants.VISIABLE_MARK] = frm.IsAppliedFor;
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(FormCollection frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ExamViolationType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            int Min_PenalizedMark = 1;
            int Max_PenalizedMark = 100;
            bool IsAppliedFor = true;
            if (frm["IsAppliedForCreate"] != null)
            {
                bool.TryParse(frm["IsAppliedForCreate"], out IsAppliedFor);
            }

            ExamViolationType examviolationtype = new ExamViolationType();
            TryUpdateModel(examviolationtype);
            examviolationtype.SchoolID = new GlobalInfo().SchoolID;
            Utils.Utils.TrimObject(examviolationtype);

            if (string.IsNullOrEmpty(examviolationtype.Resolution))
            {
                return Json(new JsonMessage(Res.Get("Validate_Error_Empty_ExamViolation"), "error"));
            }

            if (IsAppliedFor)
            {
                examviolationtype.AppliedForCandidate = ExamViolationTypeConstants.IS_APPLIED_CANDIDATE;
                if (examviolationtype.PenalizedMark == null)
                {
                    return Json(new JsonMessage(Res.Get("Common_Validate_Require_Empty"), "error"));
                }
                if (examviolationtype.PenalizedMark >= Min_PenalizedMark && examviolationtype.PenalizedMark <= Max_PenalizedMark)
                {
                    this.ExamViolationTypeBusiness.Insert(examviolationtype);
                    this.ExamViolationTypeBusiness.Save();

                    //ghi log
                    SetViewDataActionAudit(String.Empty, String.Empty
                    , string.Empty
                    , Res.Get("Exam_Violation_Type_Label_Add") + ": " + examviolationtype.Resolution
                    , "PenalizedMark: " + examviolationtype.PenalizedMark + ",Resolution: " + examviolationtype.Resolution
                    , Res.Get("Exam_Violation_Type_Label")
                    , SMAS.Business.Common.GlobalConstants.ACTION_ADD
                    , Res.Get("Exam_Violation_Type_Label_Add") + ": " + examviolationtype.Resolution);
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("ExamViolationType_Validation_MinMax"), "error"));
                }
            }
            else
            {
                examviolationtype.AppliedForInvigilator = ExamViolationTypeConstants.IS_APPLIED_INVIGILATOR;
                this.ExamViolationTypeBusiness.Insert(examviolationtype);
                this.ExamViolationTypeBusiness.Save();
                //ghi log
                SetViewDataActionAudit(String.Empty, String.Empty
                , string.Empty
                , Res.Get("Exam_Violation_Type_Label_Add") + ": " + examviolationtype.Resolution
                , "PenalizedMark: " + examviolationtype.PenalizedMark + ",Resolution: " + examviolationtype.Resolution
                , Res.Get("Exam_Violation_Type_Label")
                , SMAS.Business.Common.GlobalConstants.ACTION_ADD
                , Res.Get("Exam_Violation_Type_Label_Add") + ": " + examviolationtype.Resolution);
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ExamViolationTypeID, bool? IsAppliedForEdit)
        {
            //check quyen an toan thong tin
            this.CheckPermissionForAction(ExamViolationTypeID, "ExamViolationType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ExamViolationType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            int Min_PenalizedMark = 1;
            int Max_PenalizedMark = 100;

            ExamViolationType examviolationtype = this.ExamViolationTypeBusiness.Find(ExamViolationTypeID);

            TryUpdateModel(examviolationtype);
            Utils.Utils.TrimObject(examviolationtype);

            if (string.IsNullOrEmpty(examviolationtype.Resolution))
            {
                return Json(new JsonMessage(Res.Get("Validate_Error_Empty_ExamViolation"), "error"));
            }

            if (IsAppliedForEdit.HasValue && IsAppliedForEdit.Value)
            {
                examviolationtype.AppliedForCandidate = ExamViolationTypeConstants.IS_APPLIED_CANDIDATE;
                examviolationtype.AppliedForInvigilator = false;
                if (examviolationtype.PenalizedMark == null)
                {
                    return Json(new JsonMessage(Res.Get("Common_Validate_Require_Empty"), "error"));
                }
                if (examviolationtype.PenalizedMark >= Min_PenalizedMark && examviolationtype.PenalizedMark <= Max_PenalizedMark)
                {
                    this.ExamViolationTypeBusiness.Update(examviolationtype);
                    this.ExamViolationTypeBusiness.Save();
                    //ghi log
                    SetViewDataActionAudit(String.Empty, String.Empty
                    , examviolationtype.ExamViolationTypeID.ToString()
                    , Res.Get("Exam_Violation_Type_Label_Edit") +": " + examviolationtype.Resolution
                    , "ExamViolationTypeID: " + examviolationtype.ExamViolationTypeID + " PenalizedMark: " + examviolationtype.PenalizedMark + ", Resolution: " + examviolationtype.Resolution
                    , Res.Get("Exam_Violation_Type_Label")
                    , SMAS.Business.Common.GlobalConstants.ACTION_UPDATE
                    , Res.Get("Exam_Violation_Type_Label_Edit") + ": " +examviolationtype.Resolution);
                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("ExamViolationType_Validation_MinMax"), "error"));
                }
            }
            else
            {
                examviolationtype.AppliedForInvigilator = ExamViolationTypeConstants.IS_APPLIED_INVIGILATOR;
                examviolationtype.AppliedForCandidate = false;
                this.ExamViolationTypeBusiness.Update(examviolationtype);
                this.ExamViolationTypeBusiness.Save();
                SetViewDataActionAudit(String.Empty, String.Empty
                   , examviolationtype.ExamViolationTypeID.ToString()
                   , Res.Get("Exam_Violation_Type_Label_Edit") + ": " + examviolationtype.Resolution
                   , "ExamViolationTypeID: " + examviolationtype.ExamViolationTypeID + " PenalizedMark: " + examviolationtype.PenalizedMark + ", Resolution: " + examviolationtype.Resolution
                   , Res.Get("Exam_Violation_Type_Label")
                   , SMAS.Business.Common.GlobalConstants.ACTION_UPDATE
                   , Res.Get("Exam_Violation_Type_Label_Edit") + ": " + examviolationtype.Resolution);
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "ExamViolationType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ExamViolationType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            //Kiem tra loi vi pham da duoc su dung hay chua
            ExamViolationType evt = ExamViolationTypeBusiness.Find(id);
            if (evt != null)
            {
                if (evt.AppliedForCandidate==true)
                {
                    if (ExamPupilViolateBusiness.All.Where(o => o.ExamViolationTypeID == evt.ExamViolationTypeID).Count() > 0)
                    {
                        throw new BusinessException("ExamViolationType_Validate_InUsed");
                    }
                }
                if (evt.AppliedForInvigilator == true)
                {
                    if (ExamSupervisoryViolateBusiness.All.Where(o => o.ExamViolationTypeID == evt.ExamViolationTypeID).Count() > 0)
                    {
                        throw new BusinessException("ExamViolationType_Validate_InUsed");
                    }
                }
            }

            this.ExamViolationTypeBusiness.Delete(id);
            this.ExamViolationTypeBusiness.Save();
            //ghi log
            SetViewDataActionAudit(String.Empty, String.Empty
            , id.ToString()
            , Res.Get("Exam_Violation_Type_Label_Delete") +": " + id
            , "ExamViolationTypeID: " + id
            , Res.Get("Exam_Violation_Type_Label")
            , SMAS.Business.Common.GlobalConstants.ACTION_DELETE
            , Res.Get("Exam_Violation_Type_Label_Delete") + ": ");
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ExamViolationTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("ExamViolationType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<ExamViolationType> query = this.ExamViolationTypeBusiness.Search(SearchInfo);
            IQueryable<ExamViolationTypeViewModel> lst = query.Select(o => new ExamViolationTypeViewModel
            {
                ExamViolationTypeID = o.ExamViolationTypeID,
                Resolution = o.Resolution,
                SchoolID = o.SchoolID,
                AppliedForCandidate = o.AppliedForCandidate,
                AppliedForInvigilator = o.AppliedForInvigilator,
                PenalizedMark = o.PenalizedMark,
                Penalization = o.Penalization,
                Description = o.Description,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate
            });
            return lst.OrderBy(o => o.Resolution).ToList();
        }
    }
}





