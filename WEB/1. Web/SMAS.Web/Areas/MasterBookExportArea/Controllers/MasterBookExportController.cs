﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.MasterBookExportArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.MasterBookExportArea.Controllers
{
    public class MasterBookExportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IMasterBookBusiness MasterBookBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        public MasterBookExportController(IClassProfileBusiness ClassProfileBusiness
            , IMasterBookBusiness MasterBookBusiness
            , IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.MasterBookBusiness = MasterBookBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }
        //
        // GET: /MasterBookExportArea/MasterBookExport/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetviewData
        private void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();

            List<EducationLevel> lsEL = global.EducationLevels;
            int defaultEL = lsEL.FirstOrDefault().EducationLevelID;
            ViewData[MasterBookExportConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEL, "EducationLevelID", "Resolution");
            ViewData[MasterBookExportConstants.DEFAULT_EDUCATIONLEVEL] = defaultEL;

            List<ClassProfile> lsCP = GetClassFromEducationLevel(defaultEL).ToList();
            ViewData[MasterBookExportConstants.LISTCLASS] = new SelectList(lsCP, "ClassProfileID", "DisplayName");

            List<ComboObject> lsS = CommonList.SemesterAndAll();
            int defaultS = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            if (global.Semester.HasValue)
            {
                defaultS = global.Semester.Value;
            }
            ViewData[MasterBookExportConstants.LISTSEMESTER] = new SelectList(lsS, "key", "value", defaultS);
            ViewData[MasterBookExportConstants.DEFAULT_SEMESTER] = defaultS;

        }

        private IQueryable<ClassProfile> GetClassFromEducationLevel(int EducationLevel)
        {
            GlobalInfo global = new GlobalInfo();
            return ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"EducationLevelID",EducationLevel}
            });
        }


        #endregion

        #region ajax load combobox
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            if (EducationLevelID.HasValue)
            {
                List<ClassProfile> lsCP = GetClassFromEducationLevel(EducationLevelID.Value).OrderBy(o => o.DisplayName).ToList();
                return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ExportExcel
        
        public JsonResult ExportExcel(SearchViewModel svm)
        {
            GlobalInfo global = new GlobalInfo();
            MasterBook mb = new MasterBook();
            mb.AcademicYearID = global.AcademicYearID.Value;
            mb.ClassID = svm.Class;
            mb.EducationLevelID = svm.EducationLevel.Value;
            mb.Semester = (svm.Semester.HasValue)?svm.Semester.Value:0;
            if(mb.Semester == 0)
            {
                mb.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            }
            mb.SchoolID = global.SchoolID.Value;
            mb.AppliedLevel = global.AppliedLevel.Value;

            string type = JsonReportMessage.NEW;

            ProcessedReport processedReport = MasterBookBusiness.GetMasterBook(mb);
            if (processedReport != null)
            {
                type = JsonReportMessage.OLD;
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = MasterBookBusiness.CreateMasterBook(mb);
                processedReport = MasterBookBusiness.InsertMasterBook(mb, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel svm)
        {
            GlobalInfo global = new GlobalInfo();
            MasterBook mb = new MasterBook();
            mb.AcademicYearID = global.AcademicYearID.Value;
            mb.ClassID = svm.Class;
            mb.EducationLevelID = svm.EducationLevel.Value;
            mb.Semester = (svm.Semester.HasValue) ? svm.Semester.Value : 0;
            if (mb.Semester == 0)
            {
                mb.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            }
            mb.SchoolID = global.SchoolID.Value;
            mb.AppliedLevel = global.AppliedLevel.Value;
            ProcessedReport processedReport = MasterBookBusiness.GetMasterBook(mb);

            Stream excel = MasterBookBusiness.CreateMasterBook(mb);
            processedReport = MasterBookBusiness.InsertMasterBook(mb, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_INSOGOITENVAGHIDIEM
            };

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion
    }
}
