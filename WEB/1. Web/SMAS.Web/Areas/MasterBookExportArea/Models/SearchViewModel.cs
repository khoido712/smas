﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.MasterBookExportArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("MasterBookExport_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MasterBookExportConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "Choice")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("MasterBookExport_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MasterBookExportConstants.LISTCLASS)]
        [AdditionalMetadata("Placeholder", "Choice")]
        public int Class { get; set; }

        [ResourceDisplayName("MasterBookExport_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MasterBookExportConstants.LISTSEMESTER)]
        [AdditionalMetadata("Placeholder", "null")]
        public int? Semester { get; set; }

    }
}