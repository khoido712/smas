﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MasterBookExportArea
{
    public class MasterBookExportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MasterBookExportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MasterBookExportArea_default",
                "MasterBookExportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
