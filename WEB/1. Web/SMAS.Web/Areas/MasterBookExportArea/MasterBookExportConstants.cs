﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MasterBookExportArea
{
    public class MasterBookExportConstants
    {
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTCLASS = "LISTCLASS";
        public const string LISTSEMESTER = "LISTSEMESTER";
        public const string DEFAULT_EDUCATIONLEVEL = "DEFAULT_EDUCATIONLEVEL";
        public const string DEFAULT_SEMESTER = "DEFAULT_SEMESTER";


    }
}