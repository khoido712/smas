﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ImportClassSubjectAndTeacherArea
{
    public class ImportClassSubjectAndTeacherConstants
    {
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";
        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";

        public const string ERROR_IMPORT_DATA_IN_GRID = "ERROR_IMPORT_DATA_IN_GRID";

        public const string EXCEL_FILE_NAME = "Import_ClassSubject_And_Teacher.xls";
        public const string CUSTOM_EXCEL_FILE_WITHDATA = "Import_ClassSubject_And_Teacher_{0}_DataTemplate.xls";
        public const string CUSTOM_EXCEL_FILE_EMPTY = "Import_ClassSubject_And_Teacher_{0}_EmptyTemplate.xls";

        public const string EXCEL_FILE_NAME_NEW = "BM_PCGD.xls";
        public const string TEMPLATE_TH = "GV_TH_Import_PCGD.xls";
        public const string TEMPLATE_THCS = "GV_THCS_Import_PCGD.xls";
        public const string TEMPLATE_THPT = "GV_THPT_Import_PCGD.xls";

        public const string IS_PERMISSION = "IS_PERMISSION";

        public const int SEMESTER_I = 1;
        public const int SEMESTER_II = 2;

        public const int BEGIN_ROW_PCGD = 8;
        public const int BEGIN_ROW_SUBJECT = 3;

        public const string MA_GIAO_VIEN = "Mã giáo viên";
        public const string HO_TEN_GIAO_VIEN = "Họ và tên giáo viên";
        public const string TO_BO_MON = "Tổ bộ môn";
        public const string PHAN_CONG_KYI = "Phân công giảng dạy HKI";
        public const string PHAN_CONG_KYII = "Phân công giảng dạy HK II";
    }
}