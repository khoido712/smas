﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using System.IO;
using SMAS.Web.Areas.ImportClassSubjectAndTeacherArea.Models;
using SMAS.Models.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.BusinessObject;
using SMAS.Web.Controllers;
using System.Text.RegularExpressions;
using System.Text;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.ImportClassSubjectAndTeacherArea.Controllers
{
    public class ImportClassSubjectAndTeacherController : BaseController
    {
        //
        // GET: /ImportClassSubjectAndTeacherArea/ImportClassSubjectAndTeacher/
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        public ImportClassSubjectAndTeacherController(IClassSubjectBusiness ClassSubjectBusiness
            , ITeachingAssignmentBusiness TeachingAssignmentBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , ISchoolSubjectBusiness SchoolSubjectBusiness
            , IEmployeeBusiness EmployeeBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness
            , IJudgeRecordBusiness JudgeRecordBusiness
            , IMarkRecordBusiness MarkRecordBusiness
            , ISummedUpRecordBusiness SummedUpRecordBusiness
            , ISchoolFacultyBusiness SchoolFacultyBusiness)
        {
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
        }

        public struct ErrSubjectClass
        {
            public string ErrSubjectName;
            public string ErrClass;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region import
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion

        #region setViewData
        private void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[ImportClassSubjectAndTeacherConstants.LIST_IMPORTDATA] = new List<ImportClassSubjectAndTeacherViewModel>();
            ViewData[ImportClassSubjectAndTeacherConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[ImportClassSubjectAndTeacherConstants.ERROR_IMPORT_DATA_IN_GRID] = "";
            ViewData[ImportClassSubjectAndTeacherConstants.IS_PERMISSION] = global.IsCurrentYear;

        }
        #endregion

        #region fill excel to download
        public FileResult DownloadTemplateReportWithData()
        {
            Stream excel = CreateExcel(false);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            GlobalInfo global = new GlobalInfo();
            SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID.Value);
            result.FileDownloadName = string.Format(ImportClassSubjectAndTeacherConstants.CUSTOM_EXCEL_FILE_WITHDATA, ReportUtils.RemoveSpecialCharacters(sp.SchoolName));
            return result;

        }

        public FileResult DownloadEmptyTemplateReport()
        {
            Stream excel = CreateExcel(true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            result.FileDownloadName = string.Format(ImportClassSubjectAndTeacherConstants.CUSTOM_EXCEL_FILE_EMPTY, ReportUtils.RemoveSpecialCharacters(sp.SchoolName));

            return result;
        }

        public FileResult DownloadDataErr()
        {
            Stream excel = CreateExcel(true, true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            result.FileDownloadName = string.Format(ImportClassSubjectAndTeacherConstants.CUSTOM_EXCEL_FILE_WITHDATA, ReportUtils.RemoveSpecialCharacters(sp.SchoolName));
            return result;
        }

        public Stream CreateExcel(bool empty, bool isFileErr = false)
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/HT" + "/" + ImportClassSubjectAndTeacherConstants.EXCEL_FILE_NAME;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTemplateDropdownList = oBook.GetSheet(2);
            IVTWorksheet sheetTemplateEmpty = oBook.GetSheet(3);
            IVTWorksheet sheetTemplateData = oBook.GetSheet(4);

            IVTRange templateRange;
            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            //fill du lieu cho dropdown list
            var lsEdu = _globalInfo.EducationLevels.Select(o => o.EducationLevelID).ToList();
            IQueryable<ClassProfile> iqCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                new Dictionary<string, object>() {
                 {"SchoolID",_globalInfo.SchoolID}
                 ,{"AcademicYearID",_globalInfo.AcademicYearID}
                 })
                .Where(o => lsEdu.Contains(o.EducationLevelID)).OrderBy(o => o.EducationLevelID).ThenBy(o => o.DisplayName);

            List<object> lsCP = new List<object>();
            foreach (var item in iqCP)
            {
                lsCP.Add(item);
            }
            KingDic.Add("ClassProfile", lsCP);

            IQueryable<SchoolSubject> iqSS = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
             {
             {"AcademicYearID",_globalInfo.AcademicYearID}
             }).Where(o => lsEdu.Contains(o.EducationLevelID.Value)).OrderBy(o => o.SubjectCat.DisplayName);
            List<object> lsSS = new List<object>();
            var iqSSN = iqSS.Select(o => o.SubjectCat.DisplayName).Distinct().ToList();
            foreach (var item in iqSSN)
            {
                lsSS.Add(new { SubjectName = item });
            }
            KingDic.Add("SchoolSubject", lsSS);

            IQueryable<Employee> iE = EmployeeBusiness.SearchTeacher(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true }, { "CurrentEmployeeStatus", GlobalConstants.EMPLOYMENT_STATUS_WORKING }, { "CurrentSchoolID", _globalInfo.SchoolID } }).OrderBy(o => o.Name).ThenBy(o => o.FullName);
            List<object> lE = new List<object>();
            foreach (var item in iE)
            {
                lE.Add(item);
            }
            KingDic.Add("Employee", lE);

            List<object> listAppliedType = new List<object>();
            listAppliedType.Add(new ComboObject(Res.Get("ClassSubjectImport_Label_AppliedTypeRequiredMark"), "0"));
            listAppliedType.Add(new ComboObject(Res.Get("ClassSubjectImport_Label_AppliedTypeOption"), "1"));
            listAppliedType.Add(new ComboObject(Res.Get("ClassSubjectImport_Label_AppliedTypeOptionAndPlus"), "2"));
            KingDic.Add("AppliedType", listAppliedType);

            List<object> listIsCommenting = new List<object>();
            listIsCommenting.Add(new ComboObject(Res.Get("ClassSubjectImport_Label_CommitingMarked"), "0"));
            listIsCommenting.Add(new ComboObject(Res.Get("ClassSubjectImport_Label_CommitingCommented"), "1"));
            KingDic.Add("IsCommenting", listIsCommenting);

            List<object> ListSemester = new List<object>();
            ListSemester.Add(new ComboObject(Res.Get("Common_Label_FirstSemester"), SystemParamsInFile.SEMESTER_OF_YEAR_FIRST.ToString()));
            ListSemester.Add(new ComboObject(Res.Get("Common_Label_SecondSemester"), SystemParamsInFile.SEMESTER_OF_YEAR_SECOND.ToString()));
            ListSemester.Add(new ComboObject(Res.Get("Common_Label_AllYear"), SystemParamsInFile.SEMESTER_OF_YEAR_ALL.ToString()));
            KingDic.Add("SemesterList", ListSemester);
            List<object> QueenDic = new List<object>();
            var lsCS = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
             {
                 {"AcademicYearID",_globalInfo.AcademicYearID.Value}
                 ,{"AppliedLevel",_globalInfo.AppliedLevel.Value}
             })
            .Select(o => new
            {
                o.ClassProfile.EducationLevelID,
                o.ClassProfile.OrderNumber,
                ClassName = o.ClassProfile.DisplayName,
                o.IsCommenting,
                o.AppliedType,
                o.FirstSemesterCoefficient,
                SubjectName = o.SubjectCat.DisplayName,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                o.SecondSemesterCoefficient,
                o.SectionPerWeekFirstSemester,
                o.SectionPerWeekSecondSemester
            })
            .OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInSubject).ThenBy(o => o.SubjectName).ToList();
            if (empty)
            {
                templateRange = sheetTemplateEmpty.GetRange("A1", "L80");

            }
            else
            {
                templateRange = sheetTemplateData.GetRange("A1", "L1000");
                //fill du lieu
                for (int i = 0; i < lsCS.Count(); i++)
                {
                    var thisClassSubject = lsCS[i];
                    Dictionary<string, object> princeDic = new Dictionary<string, object>();
                    princeDic["STT"] = i + 1;
                    princeDic["Class"] = thisClassSubject.ClassName;
                    princeDic["Subject"] = thisClassSubject.SubjectName;
                    princeDic["TeacherCode"] = "";
                    princeDic["TeacherName"] = "";
                    princeDic["Iscommenting"] = thisClassSubject.IsCommenting.HasValue ? ChangeToIsCommenting(thisClassSubject.IsCommenting.Value) : "";
                    princeDic["AppliedType"] = thisClassSubject.AppliedType.HasValue ? ChangeToAppliedType(thisClassSubject.AppliedType.Value) : ""; ;
                    princeDic["FirstSemesterCoefficient"] = thisClassSubject.FirstSemesterCoefficient.HasValue ? thisClassSubject.FirstSemesterCoefficient.Value.ToString() : "";
                    princeDic["SecondSemesterCoefficient"] = thisClassSubject.SecondSemesterCoefficient.HasValue ? thisClassSubject.SecondSemesterCoefficient.Value.ToString() : "";
                    princeDic["SectionPerWeekFirstSemester"] = thisClassSubject.SectionPerWeekFirstSemester.HasValue ? thisClassSubject.SectionPerWeekFirstSemester.Value.ToString("0.#") : "";
                    princeDic["SectionPerWeekSecondSemester"] = thisClassSubject.SectionPerWeekSecondSemester.HasValue ? thisClassSubject.SectionPerWeekSecondSemester.Value.ToString("0.#") : "";
                    princeDic["Semester"] = "";
                    QueenDic.Add(princeDic);
                }

            }
            //
            KingDic.Add("Rows", QueenDic);
            SupervisingDept sd = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID.Value);
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string year = "Năm học {0} - {1}";
            string date = (sd.Province != null ? sd.Province.ProvinceName : "") + ", ngày {0} tháng {1} năm {2}";
            string dateTitle = string.Format(date, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string yearTitle = string.Format(year, ay.Year, (ay.Year + 1));

            KingDic.Add("SupervisingDeptName", sd.SupervisingDeptName);
            KingDic.Add("SchoolName", sp.SchoolName);
            KingDic.Add("TitleYear", yearTitle);
            KingDic.Add("DateTime", dateTitle);

            IVTRange templateRangeForDrop = sheetTemplateDropdownList.GetRange("A1", "G65");

            templateRangeForDrop.FillVariableValue(KingDic);
            templateRange.FillVariableValue(KingDic);
            sheet.CopyPasteSameRowHeigh(templateRange, 1, VTVector.dic['A']);

            if (isFileErr)//Neu la download file du lieu loi
            {
                List<ImportClassSubjectAndTeacherViewModel> lsDataErr = new List<ImportClassSubjectAndTeacherViewModel>();
                lsDataErr = (List<ImportClassSubjectAndTeacherViewModel>)Session["ImportData"];
                lsDataErr = lsDataErr.Where(p => p.isLegal == false).ToList();
                ImportClassSubjectAndTeacherViewModel objDataErr = null;
                int startRowErr = 10;
                for (int i = 0; i < lsDataErr.Count; i++)
                {
                    objDataErr = lsDataErr[i];
                    //STT
                    sheet.SetCellValue(startRowErr, 1, i + 1);
                    //Lop
                    sheet.SetCellValue(startRowErr, 2, objDataErr.ClassName);
                    //Ten mon hoc
                    sheet.SetCellValue(startRowErr, 3, objDataErr.SubjectName);
                    //Ma GV
                    sheet.SetCellValue(startRowErr, 4, objDataErr.TeacherCode);
                    //Ten GV
                    sheet.SetCellValue(startRowErr, 5, objDataErr.TeacherName);
                    //Loai mon
                    sheet.SetCellValue(startRowErr, 6, objDataErr.AppliedTypeName);
                    //Kieu mon
                    sheet.SetCellValue(startRowErr, 7, objDataErr.IsCommentingName);
                    //He so HKI
                    sheet.SetCellValue(startRowErr, 8, objDataErr.FirstSemesterCoefficient);
                    //He so HKII
                    sheet.SetCellValue(startRowErr, 9, objDataErr.SecondSemesterCoefficient);
                    //So tiet/tuan HKI
                    sheet.SetCellValue(startRowErr, 10, objDataErr.SectionPerWeekFirstSemester);
                    //So tiet/tuan HKII
                    sheet.SetCellValue(startRowErr, 11, objDataErr.SectionPerWeekSecondSemester);
                    //Hoc ky hoc
                    sheet.SetCellValue(startRowErr, 12, objDataErr.SemesterName);
                    //Mo ta
                    sheet.SetCellValue(startRowErr, 13, objDataErr.ErrorDescription);
                    startRowErr++;
                }
            }
            else
            {
                sheet.SetColumnWidth('M', 0);
            }

            sheetTemplateEmpty.Delete();
            sheetTemplateData.Delete();
            return oBook.ToStream();
        }

        private string ChangeToIsCommenting(int IsCommenting)
        {
            switch (IsCommenting)
            {
                case 0:
                    return Res.Get("ClassSubjectImport_Label_CommitingMarked");
                case 1:
                    return Res.Get("ClassSubjectImport_Label_CommitingCommented");
                case 2:
                    return Res.Get("ClassSubject_Label_CommitingCommentedMarked");
                default:
                    return "";
            }
        }

        private string ChangeToAppliedType(int AppliedType)
        {
            switch (AppliedType)
            {
                case 0:
                    return Res.Get("ClassSubjectImport_Label_AppliedTypeRequiredMark");
                case 1:
                    return Res.Get("ClassSubjectImport_Label_AppliedTypeOption");
                case 2:
                    return Res.Get("ClassSubjectImport_Label_AppliedTypeOptionAndPlus");
                case 3:
                    return Res.Get("ClassSubjectImport_Label_AppliedTypeJob");
                default:
                    return "";
            }
        }
        #endregion


        #region 20150622_anhnph1_nang cap giai doan 10


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportLegalData()
        {
            List<ImportClassSubjectAndTeacherViewModel> lsTemp = (List<ImportClassSubjectAndTeacherViewModel>)Session["ImportData"];
            lsTemp = lsTemp.Where(o => o.isLegal).ToList();
            return SaveLegalDataImport(lsTemp);
        }

        public FileResult DownloadFileTemplate()
        {
            Stream excel = this.SetValueTemplate();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            int applicationLevel = _globalInfo.AppliedLevel.Value;
            string fileName = "BM_PCGD.xls";
            if (applicationLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                fileName = ImportClassSubjectAndTeacherConstants.TEMPLATE_TH;
            }
            else if (applicationLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                fileName = ImportClassSubjectAndTeacherConstants.TEMPLATE_THCS;
            }
            else if (applicationLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                fileName = ImportClassSubjectAndTeacherConstants.TEMPLATE_THPT;
            }
            string ReportName = fileName;
            result.FileDownloadName = ReportName;
            return result;
        }

        private Stream SetValueTemplate()
        {
            string templatePath = string.Empty;
            int applicationLevel = _globalInfo.AppliedLevel.Value;
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            if (applicationLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Import/" + ImportClassSubjectAndTeacherConstants.TEMPLATE_TH;
            }
            else if (applicationLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Import/" + ImportClassSubjectAndTeacherConstants.TEMPLATE_THCS;
            }
            else if (applicationLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Import/" + ImportClassSubjectAndTeacherConstants.TEMPLATE_THPT;
            }

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //lay ra danh sach ca sheet
            IVTWorksheet tempSheet = oBook.GetSheet(1);
            IVTWorksheet subjectSheet = oBook.GetSheet(2);

            //lay du lieu
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"AcademicYearID", academicYearId},
                {"SchoolID", schoolId},
                {"AppliedLevel", applicationLevel},
                {"IsActive", true}
            };

            //ds phan cong giang day cua can bo
            List<TeachingAssignmentBO> lstTAbo = TeachingAssignmentBusiness.GetTeachingAssignmentbyAcademicYear(schoolId, academicYearId, applicationLevel);
            //danh sach can bộ
            List<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(schoolId, new Dictionary<string, object>() {
                            { "IsActive", true },
                            { "CurrentEmployeeStatus", GlobalConstants.EMPLOYMENT_STATUS_WORKING },
                            {"AcademicYearID",academicYearId},
                            {"EmploymentStatus",GlobalConstants.EMPLOYMENT_STATUS_WORKING}
                            //{ "CurrentSchoolID", schoolId },
                            }).ToList().OrderBy((c => c.SchoolFacultyID))
                                        .OrderBy(c => Utils.Utils.SortABC(c.Name))
                                        .ThenBy(c => Utils.Utils.SortABC(c.FullName))
                                        .ToList();

            //khoi tao 
            Employee objEmployee;
            List<TeachingAssignmentBO> lstTempSemesterI = new List<TeachingAssignmentBO>();
            List<TeachingAssignmentBO> lstTempSemesterII = new List<TeachingAssignmentBO>();
            string teachingAssignSemester1;
            string teachingAssignSemester2;
            int countInfo = lstEmployee.Count;
            int startRowPCGD = ImportClassSubjectAndTeacherConstants.BEGIN_ROW_PCGD;
            for (int i = 0; i < lstEmployee.Count; i++)
            {
                objEmployee = lstEmployee[i];
                lstTempSemesterI = lstTAbo.Where(t => t.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                                                  && t.TeacherID == objEmployee.EmployeeID).ToList();
                lstTempSemesterII = lstTAbo.Where(t => t.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND
                                                  && t.TeacherID == objEmployee.EmployeeID).ToList();

                tempSheet.SetCellValue(startRowPCGD, 1, i + 1);
                tempSheet.GetRange(startRowPCGD, 1, startRowPCGD, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                tempSheet.SetCellValue(startRowPCGD, 2, objEmployee.EmployeeCode); //ma nhan vien
                tempSheet.GetRange(startRowPCGD, 2, startRowPCGD, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                tempSheet.SetCellValue(startRowPCGD, 3, objEmployee.FullName); //ho ten
                tempSheet.GetRange(startRowPCGD, 3, startRowPCGD, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                tempSheet.SetCellValue(startRowPCGD, 4, objEmployee.SchoolFaculty.FacultyName); //to bo mon 
                tempSheet.GetRange(startRowPCGD, 4, startRowPCGD, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                teachingAssignSemester1 = GetAssignSemesterByTeacher(lstTempSemesterI);
                teachingAssignSemester2 = GetAssignSemesterByTeacher(lstTempSemesterII);
                tempSheet.SetCellValue(startRowPCGD, 5, teachingAssignSemester1); //PCGD hoc ky I
                tempSheet.GetRange(startRowPCGD, 5, startRowPCGD, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                tempSheet.SetCellValue(startRowPCGD, 6, teachingAssignSemester2); //PCGD hoc ky II
                tempSheet.GetRange(startRowPCGD, 6, startRowPCGD, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                startRowPCGD++;
            }

            //ds mon hoc khai bao cho truong o cap dang thuc hien
            List<SchoolSubject> lstSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
             {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel", _globalInfo.AppliedLevel}
             }).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            List<int> lstSubjectID = lstSchoolSubject.Select(p => p.SubjectID).Distinct().ToList();
            int startRowSubject = ImportClassSubjectAndTeacherConstants.BEGIN_ROW_SUBJECT;
            int countSubject = lstSubjectID.Count;
            SchoolSubject objSS = null;
            for (int i = 0; i < countSubject; i++)
            {
                objSS = lstSchoolSubject.Where(c => c.SubjectID == lstSubjectID[i]).FirstOrDefault();
                subjectSheet.SetCellValue(startRowSubject, 1, i + 1); //STT
                subjectSheet.SetCellValue(startRowSubject, 2, objSS.SubjectCat.SubjectName); //mon hoc
                subjectSheet.SetCellValue(startRowSubject, 3, objSS.SubjectCat.Abbreviation); // ky hieu
                startRowSubject++;
            }
            IVTRange rangeSubject = subjectSheet.GetRange(2, 1, 2 + countSubject, 3);
            rangeSubject.SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            #endregion fill sheet mon hoc

            return oBook.ToStream();
        }
        private string GetAssignSemesterByTeacher(List<TeachingAssignmentBO> lstTempSemester)
        {
            StringBuilder assignSemeter = new StringBuilder();
            StringBuilder retVal = new StringBuilder();
            int countSe = lstTempSemester.Count;

            if (lstTempSemester == null || lstTempSemester.Count == 0)
            {
                return "";
            }
            //tach thanh 2 nhom, 1 nhom mon hoc va mot nhom lop hoc. Thuc hien vong for de ghep chuoi de duoc ket qua theo cau truc
            // Toan (7/1, 7/2) + VL (7/1, 7/3)
            List<SubjectModel> lstSubjectId = lstTempSemester.Select(c => new SubjectModel
            {
                SubjectID = c.SubjectID.Value,
                SubjcetAbbreviation = c.SubjcetAbbreviation,
                SubjectName = c.SubjectName,
                OrderInSubject = c.OrderInSubject
            }).Distinct().ToList();
            lstSubjectId = lstSubjectId.OrderBy(c => c.OrderInSubject).ThenBy(c => c.SubjectName).ToList();
            List<int> listSubjectID = lstSubjectId.Select(p => p.SubjectID).Distinct().ToList();

            List<string> lstClassName;
            string subjcetAbbreviation;
            SubjectModel objSubjectModel;
            for (int i = 0; i < listSubjectID.Count; i++)
            {
                objSubjectModel = lstSubjectId.Where(p => p.SubjectID == listSubjectID[i]).FirstOrDefault();
                lstClassName = lstTempSemester.Where(c => c.SubjectID == objSubjectModel.SubjectID)
                                                        .OrderBy(c => c.EducationLevelID)
                                                        .ThenBy(c => c.ClassOrderNumber)
                                                        .Select(c => c.ClassName).ToList();
                if (lstClassName == null || lstClassName.Count == 0)
                {
                    continue;
                }
                subjcetAbbreviation = objSubjectModel.SubjcetAbbreviation;
                if (assignSemeter.Equals(null) || String.IsNullOrEmpty(assignSemeter.ToString()))
                {
                    assignSemeter.Append(subjcetAbbreviation).Append("(");
                }
                else
                {
                    assignSemeter.Append("+").Append(subjcetAbbreviation).Append("(");
                }

                for (int j = 0; j < lstClassName.Count; j++)
                {
                    if (j == lstClassName.Count - 1)
                    {
                        assignSemeter.Append(lstClassName[j]);
                    }
                    else
                    {
                        assignSemeter.Append(lstClassName[j]).Append(",");
                    }
                }
                if (!assignSemeter.Equals(null) || !String.IsNullOrEmpty(assignSemeter.ToString()))
                {
                    assignSemeter.Append(")");
                }
            }
            return assignSemeter.ToString();
        }
        public bool checkExcelTemplate(IVTWorkbook oBook)
        {
            try
            {
                IVTWorksheet sheet = oBook.GetSheet(1);
                int indexheader = ImportClassSubjectAndTeacherConstants.BEGIN_ROW_PCGD - 1;
                string TeacherCode = sheet.GetCellValue(indexheader, VTVector.dic['B']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['B']).ToString();
                string FullName = sheet.GetCellValue(indexheader, VTVector.dic['C']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['C']).ToString();
                string FaultyName = sheet.GetCellValue(indexheader, VTVector.dic['D']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['D']).ToString();
                string AssginHKI = sheet.GetCellValue(indexheader, VTVector.dic['E']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['E']).ToString();
                string AssginHKII = sheet.GetCellValue(indexheader, VTVector.dic['F']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['F']).ToString();

                bool isTeacherCode = !TeacherCode.ToUpper().Contains(ImportClassSubjectAndTeacherConstants.MA_GIAO_VIEN.ToUpper());
                bool isFullName = !FullName.ToUpper().Contains(ImportClassSubjectAndTeacherConstants.HO_TEN_GIAO_VIEN.ToUpper());
                bool isFaultyName = !FaultyName.ToUpper().Contains(ImportClassSubjectAndTeacherConstants.TO_BO_MON.ToUpper());
                bool isAssginHKI = !AssginHKI.ToUpper().Contains(ImportClassSubjectAndTeacherConstants.PHAN_CONG_KYI.ToUpper());
                bool isAssginHKII = !AssginHKII.ToUpper().Contains(ImportClassSubjectAndTeacherConstants.PHAN_CONG_KYII.ToUpper());

                if (isTeacherCode || isFullName || isFaultyName || isAssginHKI || isAssginHKII)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "checkExcelTemplate","null", ex);

                return false;
            }
        }


        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments)
        {
            ViewData[ImportClassSubjectAndTeacherConstants.IS_PERMISSION] = _globalInfo.IsCurrentYear;

            if (attachments == null || attachments.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
                res.ContentType = "text/plain";
                return res;
            }
            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res1.ContentType = "text/plain";
                    return res1;
                }

                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);

                //check file excel co dung mau template ->khong dung return lai index va loi
                bool GoodTemplate = checkExcelTemplate(book); //TODO:chua viet checkExcelTemplate
                if (!GoodTemplate)
                {
                    ViewData[ImportClassSubjectAndTeacherConstants.ERROR_IMPORT_MESSAGE] = "File import không đúng mẫu.";
                    JsonResult res2 = Json(new JsonMessage(RenderPartialViewToString("_ErrorMessage", null), "errortemplate"));
                    res2.ContentType = "text/plain";
                    return res2;
                }
                else
                {
                    //neu dung mau template,bat dau kiem tra du lieu,neu co loi ->ban ra popup va hien grid
                    List<ImportClassSubjectAndTeacherViewModel> lsData = getDataFromImportFile(book);//TODO:chua viet getDataFromImportFile
                    Session["ImportData"] = lsData;
                    if (lsData.Where(o => !o.isLegal).Count() > 0)
                    {
                        ViewData[ImportClassSubjectAndTeacherConstants.ERROR_IMPORT_DATA_IN_GRID] = "Trong file excel có dữ liệu không hợp lệ.";
                        ViewData[ImportClassSubjectAndTeacherConstants.LIST_IMPORTDATA] = lsData;
                        ViewData[ImportClassSubjectAndTeacherConstants.ERROR_IMPORT_MESSAGE] = "File import không đúng mẫu.Thầy/cô hãy sử dụng mẫu import theo link bên dưới";
                        JsonResult res3 = Json(new JsonMessage(RenderPartialViewToString("_ListData", null), "grid"));
                        res3.ContentType = "text/plain";
                        return res3;
                    }
                    else  //neu dung template,ko loi->import luon va return message success
                    {
                        return SaveLegalDataImport(lsData); //TODO:chua viet SaveLegalDataImport
                    }
                }

            }

            JsonResult res4 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res4.ContentType = "text/plain";
            return res4;
            //return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveLegalDataImport(List<ImportClassSubjectAndTeacherViewModel> LegalData)
        {
            if (LegalData == null || LegalData.Count == 0)
            {
                LegalData = (List<ImportClassSubjectAndTeacherViewModel>)Session["ImportData"];
            }
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            ViewData[ImportClassSubjectAndTeacherConstants.IS_PERMISSION] = _globalInfo.IsCurrentYear;
            // Lay tat ca thong tin mon hoc cua lop trong truong
            List<ClassSubject> lsCS = ClassSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
                {
                    {"AcademicYearID",academicYearId}
                }).ToList();

            IDictionary<string, object> searchInfoTA = new Dictionary<string, object>();
            searchInfoTA["AcademicYearID"] = academicYearId;
            searchInfoTA["IsActive"] = true;
            List<TeachingAssignment> listTA = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfoTA).ToList();
            //List<TeachingAssignment> listTA = TeachingAssignmentBusiness.All.Where(o => o.SchoolID == global.SchoolID.Value && o.AcademicYearID == global.AcademicYearID.Value && o.IsActive == true && o.Last2digitNumberSchool == partitionId).ToList();
            List<TeachingAssignment> lsTA = new List<TeachingAssignment>();
            // Danh sach phan cong giang day cua lop se thuc hien cap nhat vao CSDL
            List<TeachingAssignment> lsTAInsertOrUpdate = new List<TeachingAssignment>();
            // Dictionary luu danh sach lop duoc phan cong
            Dictionary<int, List<TeachingAssignment>> dicTa = new Dictionary<int, List<TeachingAssignment>>();
            List<TeachingAssignment> lstDelete = new List<TeachingAssignment>();
            SubjectByClassBO objSBC = null;
            foreach (var item in LegalData)
            {
                #region xu ly du lieu cho HKI
                if (!String.IsNullOrEmpty(item.AssginSemesterI) && item.lstSubjectByClassHKI != null)
                {
                    //duyet danh sach cac mon hoc phan cong cho lop cua giao vien trong file excel
                    for (int i = 0; i < item.lstSubjectByClassHKI.Count; i++)
                    {
                        objSBC = item.lstSubjectByClassHKI[i];
                        //lay ra danh sach cac lop trong db cua giao vien
                        dicTa[objSBC.SubjectID] = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                            && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                            && p.TeacherID == item.TeacherID).ToList();

                        //TH1: giao vien chua duoc phan cong giang day
                        if (dicTa[objSBC.SubjectID] == null || dicTa[objSBC.SubjectID].Count <= 0)
                        {
                            for (int j = 0; j < objSBC.listClassID.Count; j++)
                            {
                                TeachingAssignment ta = new TeachingAssignment();
                                ta.AcademicYearID = academicYearId;
                                ta.AssignedDate = DateTime.Now;
                                ta.ClassID = objSBC.listClassID[j];
                                ta.FacultyID = item.FacultyID;
                                ta.IsActive = true;
                                ta.SchoolID = schoolId;
                                ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                                ta.SubjectID = objSBC.SubjectID;
                                ta.TeacherID = item.TeacherID;
                                ta.Last2digitNumberSchool = partitionId;
                                lsTAInsertOrUpdate.Add(ta);
                            }
                        }
                        else //TH2: giao vien duoc phan cong giang day cho mon hoc do
                        {
                            //duyet danh sach lop
                            for (int j = 0; j < objSBC.listClassID.Count; j++)
                            {
                                //kiem tra da ton tai phan cong trong db chua. Neu chua thuc hien insert, ton tai thi bo qua 
                                TeachingAssignment check_ta = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                    && p.ClassID == objSBC.listClassID[j]
                                    && p.TeacherID == item.TeacherID
                                    && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                if (check_ta != null)
                                {
                                    continue;
                                }
                                else
                                {
                                    TeachingAssignment ta = new TeachingAssignment();
                                    ta.AcademicYearID = academicYearId;
                                    ta.AssignedDate = DateTime.Now;
                                    ta.ClassID = objSBC.listClassID[j];
                                    ta.FacultyID = item.FacultyID;
                                    ta.IsActive = true;
                                    ta.SchoolID = schoolId;
                                    ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                                    ta.SubjectID = objSBC.SubjectID;
                                    ta.TeacherID = item.TeacherID;
                                    ta.Last2digitNumberSchool = partitionId;
                                    lsTAInsertOrUpdate.Add(ta);
                                }
                            }
                        }
                    }
                }
                else//TH: giáo viên đã được phân công giang dạy nhưng import từ file excel => bỏ phân công giảng dạy 
                {
                    if (objSBC != null)
                    {
                        //duyet danh sach lop
                        for (int j = 0; j < objSBC.listClassID.Count; j++)
                        {
                            //kiem tra da ton tai phan cong trong db chua. Neu chua thuc hien insert, ton tai thi bo qua 
                            TeachingAssignment ta = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                && p.ClassID == objSBC.listClassID[j]
                                && p.TeacherID == item.TeacherID
                                && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            if (ta != null)
                            {
                                TeachingAssignmentBusiness.Delete(ta.TeachingAssignmentID);
                            }
                        }
                    }
                }
                #endregion xu ly du lieu cho HKI

                #region xu ly du lieu cho HKII
                if (!String.IsNullOrEmpty(item.AssignSemesterII) && item.lstSubjectByClassHKII != null)
                {
                    //duyet danh sach cac mon hoc phan cong cho lop cua giao vien trong file excel
                    for (int i = 0; i < item.lstSubjectByClassHKII.Count; i++)
                    {
                        objSBC = item.lstSubjectByClassHKII[i];
                        //lay ra danh sach cac lop trong db cua giao vien
                        dicTa[objSBC.SubjectID] = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                            && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            && p.TeacherID == item.TeacherID).ToList();

                        //TH1: giao vien chua duoc phan cong giang day
                        if (dicTa[objSBC.SubjectID] == null || dicTa[objSBC.SubjectID].Count <= 0)
                        {
                            for (int j = 0; j < objSBC.listClassID.Count; j++)
                            {
                                TeachingAssignment ta = new TeachingAssignment();
                                ta.AcademicYearID = academicYearId;
                                ta.AssignedDate = DateTime.Now;
                                ta.ClassID = objSBC.listClassID[j];
                                ta.FacultyID = item.FacultyID;
                                ta.IsActive = true;
                                ta.SchoolID = schoolId;
                                ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                                ta.SubjectID = objSBC.SubjectID;
                                ta.TeacherID = item.TeacherID;
                                ta.Last2digitNumberSchool = partitionId;
                                lsTAInsertOrUpdate.Add(ta);
                            }
                        }
                        else //TH2: giao vien duoc phan cong giang day cho mon hoc do
                        {
                            //duyet danh sach lop
                            for (int j = 0; j < objSBC.listClassID.Count; j++)
                            {
                                //kiem tra da ton tai phan cong trong db chua. Neu chua thuc hien insert, ton tai thi bo qua 
                                TeachingAssignment check_ta = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                    && p.ClassID == objSBC.listClassID[j]
                                    && p.TeacherID == item.TeacherID
                                    && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                if (check_ta != null)
                                {
                                    //Xét trường hợp AssignHKI null. Kiềm tra tồn tại phân công giảng dạy đang xét ở HKI thì xóa đi                                  
                                    if (item.AssginSemesterI == string.Empty)
                                    {
                                        TeachingAssignment tahkI = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                            && p.ClassID == objSBC.listClassID[j]
                                            && p.TeacherID == item.TeacherID
                                            && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                        if (tahkI != null)
                                        {
                                            lstDelete.Add(tahkI);
                                        }
                                    }
                                    continue;
                                }
                                else
                                {
                                    TeachingAssignment ta = new TeachingAssignment();
                                    ta.AcademicYearID = academicYearId;
                                    ta.AssignedDate = DateTime.Now;
                                    ta.ClassID = objSBC.listClassID[j];
                                    ta.FacultyID = item.FacultyID;
                                    ta.IsActive = true;
                                    ta.SchoolID = schoolId;
                                    ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                                    ta.SubjectID = objSBC.SubjectID;
                                    ta.TeacherID = item.TeacherID;
                                    ta.Last2digitNumberSchool = partitionId;
                                    lsTAInsertOrUpdate.Add(ta);
                                }
                            }
                        }
                    }


                }
                else//TH: giáo viên đã được phân công giang dạy nhưng import từ file excel => bỏ phân công giảng dạy 
                {

                    if (objSBC != null)
                    {
                        //duyet danh sach lop
                        for (int j = 0; j < objSBC.listClassID.Count; j++)
                        {
                            //kiem tra da ton tai phan cong trong db chua. Neu chua thuc hien insert, ton tai thi bo qua 
                            TeachingAssignment ta = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                && p.ClassID == objSBC.listClassID[j]
                                && p.TeacherID == item.TeacherID
                                && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                            if (ta != null)
                            {
                                lstDelete.Add(ta);
                            }
                        }
                    }
                }
                #endregion xu ly du lieu cho HKII
            }
            if (lstDelete.Count > 0 && lstDelete != null)
            {
                TeachingAssignmentBusiness.DeleteAll(lstDelete);
            }

            // Luu thong tin phan cong giang day
            TeachingAssignmentBusiness.InsertOrUpdateListTeachingAssignment(schoolId, academicYearId, lsTAInsertOrUpdate);

            TeachingAssignmentBusiness.Save();
            List<ImportClassSubjectAndTeacherViewModel> lsTemp = (List<ImportClassSubjectAndTeacherViewModel>)Session["ImportData"];
            int total = ((lsTemp == null) ? 0 : lsTemp.Count);
            int retVal = (LegalData == null) ? 0 : LegalData.Where(c => String.IsNullOrEmpty(c.ErrorDescription)).Count();
            string retMsg = String.Format("{0} {1}/{2} giáo viên", Res.Get("Import phân công giảng dạy thành công cho "), retVal, total);
            return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS));
            //JsonResult res4 = Json(new JsonMessage(Res.Get("ImportClassSubjectAndTeacher_Label_ImportSuccess")));
            //return res4;
        }

        public List<ImportClassSubjectAndTeacherViewModel> getDataFromImportFile(IVTWorkbook oBook)
        {
            #region Khai bao + lay du lieu truy van
            List<ImportClassSubjectAndTeacherViewModel> lsICSATVM = new List<ImportClassSubjectAndTeacherViewModel>();
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int appliedLevel = _globalInfo.AppliedLevel.Value;
            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            int count = 0;
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
                {
                    {"AppliedLevel",appliedLevel},
                    {"AcademicYearID",academicYearId}
                }).ToList();
            ClassProfile cp = new ClassProfile();
            List<SubjectCat> lstSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>()
                {
                    {"AppliedLevel",appliedLevel},
                    {"IsActive",true}
                }).ToList();
            SubjectCat sc = new SubjectCat();

            List<SchoolFaculty> lstSchoolFaculty = SchoolFacultyBusiness.All.Where(o => o.SchoolID == schoolId && o.IsActive == true).ToList();
            SchoolFaculty sf = new SchoolFaculty();

            List<SchoolSubjectBO> lstSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
                {
                    {"AppliedLevel",appliedLevel},
                    {"AcademicYearID",academicYearId},
                }).Select(o => new SchoolSubjectBO
                {
                    SubjectID = o.SubjectID,
                    SubjectName = o.SubjectCat.DisplayName,
                    EducationLevelID = o.EducationLevelID,
                    Abbreviation = o.SubjectCat.Abbreviation
                }).ToList();

            List<ClassSubjectBO> lstClassSubjectHKI = ClassSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
            {
                {"AppliedLevel",appliedLevel},
                {"AcademicYearID",academicYearId},
                {"Semester",SystemParamsInFile.SEMESTER_OF_YEAR_FIRST},
            }).Select(o => new ClassSubjectBO
            {
                ClassSubjectID = o.ClassSubjectID,
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
            }).ToList();

            List<ClassSubjectBO> lstClassSubjectHKII = ClassSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
            {
                {"AppliedLevel",appliedLevel},
                {"AcademicYearID",academicYearId},
                {"Semester",SystemParamsInFile.SEMESTER_OF_YEAR_SECOND},
            }).Select(o => new ClassSubjectBO
            {
                ClassSubjectID = o.ClassSubjectID,
                ClassID = o.ClassID,
                SubjectID = o.SubjectID
            }).ToList();

            List<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(schoolId, new Dictionary<string, object>()
            {
                //{"CurrentEmployeeStatus", SystemParamsInFile.EMPLOYMENT_STATUS_WORKING}
            }).ToList();

            List<Employee> listEmployeeCheck = new List<Employee>();
            List<string> ErrClassI = null;
            List<string> ErrSubjectI = null;
            List<string> ErrClassII = null;
            List<string> ErrSubjectII = null;

            List<ErrSubjectClass> lstErrSubjectByClassI = null;
            List<ErrSubjectClass> lstErrSubjectByClassII = null;
            List<SubjectByClassBO> lstTemp_HKI = null;
            List<SubjectByClassBO> lstTemp_HKII = null;
            #endregion Khai bao + lay du lieu truy van

            while (true)
            {
                bool isLegalBot = true;
                string TeacherCode, TeacherName, FacultyName, AssignSemesterI, AssignSemesterII;

                int index = count + ImportClassSubjectAndTeacherConstants.BEGIN_ROW_PCGD;
                TeacherCode = sheet.GetCellValue(index, VTVector.dic['B']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['B']).ToString();
                TeacherCode = String.IsNullOrEmpty(TeacherCode) ? "" : TeacherCode.Trim();
                TeacherName = sheet.GetCellValue(index, VTVector.dic['C']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['C']).ToString();
                TeacherName = String.IsNullOrEmpty(TeacherName) ? "" : TeacherName.Trim();
                FacultyName = sheet.GetCellValue(index, VTVector.dic['D']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['D']).ToString();
                FacultyName = String.IsNullOrEmpty(FacultyName) ? "" : FacultyName.Trim();

                AssignSemesterI = sheet.GetCellValue(index, VTVector.dic['E']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['E']).ToString();
                AssignSemesterI = String.IsNullOrEmpty(AssignSemesterI) ? "" : AssignSemesterI.Trim();

                AssignSemesterII = sheet.GetCellValue(index, VTVector.dic['F']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['F']).ToString();
                AssignSemesterII = String.IsNullOrEmpty(AssignSemesterII) ? "" : AssignSemesterII.Trim();

                if (String.IsNullOrEmpty(TeacherCode)
                    && String.IsNullOrEmpty(TeacherName)
                    && String.IsNullOrEmpty(FacultyName)
                    && String.IsNullOrEmpty(AssignSemesterI)
                    && String.IsNullOrEmpty(AssignSemesterII)
                    )
                {
                    break;
                }

                ImportClassSubjectAndTeacherViewModel ICSATVM = new ImportClassSubjectAndTeacherViewModel();

                //----------------bat dau validate-----------------------------
                //to bo mon
                sf = lstSchoolFaculty.Where(p => p.FacultyName.ToUpper().Contains(FacultyName.ToUpper())).FirstOrDefault();
                if (sf != null)
                {
                    ICSATVM.FacultyID = sf.SchoolFacultyID;
                }
                ICSATVM.FacultyName = FacultyName;

                //giao vien
                if (String.IsNullOrEmpty(TeacherCode))//&& (TeacherName == null || TeacherName.Trim().Length == 0))
                {
                    // Neu khong co ma va ten giao vien thi bao loi
                    isLegalBot = false;
                    ICSATVM.TeacherCode = TeacherCode;
                    ICSATVM.TeacherName = TeacherName;
                    ICSATVM.ErrorDescription += " - Chưa nhập mã giáo viên <br/>";
                }
                else
                {
                    Employee e = null;
                    // Neu co ma thi tim theo ma, khong co thi tim theo ten
                    if (!String.IsNullOrEmpty(TeacherCode))
                    {
                        listEmployeeCheck = lstEmployee.Where(o => o.EmployeeCode.ToLower() == TeacherCode.Trim().ToLower()).ToList();
                        // Neu trung ma ma co nhap ten thi tim theo ten
                        // Neu duy nhat thi co the thuc hien tiep
                        // Con trong ca ma va ca ten thi bo tay, bao loi de truong sua thong tin giao vien
                        if (listEmployeeCheck.Count > 1)
                        {
                            e = listEmployeeCheck.Where(o => o.FullName.ToLower() == TeacherName.Trim().ToLower()).FirstOrDefault();
                        }
                        else
                        {
                            e = listEmployeeCheck.FirstOrDefault();
                        }
                    }
                    else
                    {
                        e = lstEmployee.Where(o => o.FullName.ToLower() == TeacherName.Trim().ToLower()).FirstOrDefault();
                    }

                    if (e != null)
                    {
                        if (e.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
                        {
                            ICSATVM.TeacherID = e.EmployeeID;
                            ICSATVM.TeacherCode = TeacherCode;
                            ICSATVM.TeacherName = TeacherName;
                        }
                        else
                        {
                            ICSATVM.TeacherCode = TeacherCode;
                            ICSATVM.TeacherName = TeacherName;
                            isLegalBot = false;
                            if (TeacherCode != null && TeacherCode.Trim().Length > 0)
                            {
                                ICSATVM.ErrorDescription += " - Giáo viên đang ở trạng thái khác đang làm việc <br/>";
                            }
                        }

                    }
                    else
                    {
                        ICSATVM.TeacherCode = TeacherCode;
                        ICSATVM.TeacherName = TeacherName;
                        isLegalBot = false;
                        if (TeacherCode != null && TeacherCode.Trim().Length > 0)
                        {
                            ICSATVM.ErrorDescription += " - Mã giáo viên chưa được khai báo <br/>";
                        }
                    }
                }

                //Phan cong giang day
                if (String.IsNullOrEmpty(AssignSemesterI) && String.IsNullOrEmpty(AssignSemesterII))
                {
                    // Neu khong phan cong giang day HKI va HKII
                    isLegalBot = false;
                    ICSATVM.ErrorDescription += " - Giáo viên chưa được phân công giảng dạy <br/>";
                }
                else
                {
                    ////Kiem tra đúng quy tắc quy định trong file excel
                    if (CheckRule(AssignSemesterI) == false || CheckRule(AssignSemesterII) == false)
                    {
                        isLegalBot = false;
                        ICSATVM.AssginSemesterI = AssignSemesterI;
                        ICSATVM.AssignSemesterII = AssignSemesterII;
                        ICSATVM.ErrorDescription += "- Dữ liệu phân công giảng dạy không hợp lệ <br/>";
                    }
                    else
                    {
                        //kiem tra lop hoc chua duoc khai bao trong csdl theo cau truc "Lop hoc y1, y2,..,yn chua khai bao" (o 2 ky)
                        //kiem tra mon hoc chua duoc khai bao trong csdl theo cau truc "Mon hoc x1, x2,...xn chua duoc khai bao" (o 2 ky)
                        ErrClassI = new List<string>();
                        ErrClassII = new List<string>();
                        ErrSubjectI = new List<string>();
                        ErrSubjectII = new List<string>();
                        string error = string.Empty;
                        CheckErrExsistClassAndSubject(AssignSemesterI, ErrClassI, ErrSubjectI, lstClassProfile, lstSchoolSubject);
                        CheckErrExsistClassAndSubject(AssignSemesterII, ErrClassII, ErrSubjectII, lstClassProfile, lstSchoolSubject);
                        if ((ErrClassI != null && ErrClassI.Count > 0) || (ErrClassII != null && ErrClassII.Count > 0))
                        {
                            isLegalBot = false;
                            ICSATVM.AssginSemesterI = AssignSemesterI;
                            ICSATVM.AssignSemesterII = AssignSemesterII;
                            error = TransString(ErrClassI, ErrClassII);
                            ICSATVM.ErrorDescription += " - Lớp học " + error + " chưa khai báo <br/>";
                        }
                        if ((ErrSubjectI != null && ErrSubjectI.Count > 0) || (ErrSubjectII != null && ErrSubjectII.Count > 0))
                        {
                            isLegalBot = false;
                            ICSATVM.AssginSemesterI = AssignSemesterI;
                            ICSATVM.AssignSemesterII = AssignSemesterII;
                            error = TransString(ErrSubjectI, ErrSubjectII);
                            ICSATVM.ErrorDescription += " - Môn học " + error + " chưa được khai báo <br/>";
                        }
                        else if (ErrSubjectI.Count == 0 && ErrSubjectII.Count == 0 && ErrClassI.Count == 0 && ErrClassII.Count == 0)
                        {
                            //Môn học chưa được khai báo cho lớp ở học kỳ I. Cau truc "Mon hoc X chua khai bao cho lop Y1, Y2,.., Yn o hoc ky I"
                            lstTemp_HKI = new List<SubjectByClassBO>();
                            lstTemp_HKII = new List<SubjectByClassBO>();
                            lstErrSubjectByClassI = new List<ErrSubjectClass>();
                            lstErrSubjectByClassII = new List<ErrSubjectClass>();
                            CheckErrSubjectByClass(AssignSemesterI, lstErrSubjectByClassI, lstTemp_HKI, lstClassSubjectHKI, lstClassProfile, lstSubjectCat);
                            if (lstErrSubjectByClassI != null && lstErrSubjectByClassI.Count > 0)
                            {
                                isLegalBot = false;
                                ICSATVM.AssginSemesterI = AssignSemesterI;
                                ICSATVM.AssignSemesterII = AssignSemesterII;
                                for (int i = 0; i < lstErrSubjectByClassI.Count; i++)
                                {
                                    ICSATVM.ErrorDescription += " - Môn học " + lstErrSubjectByClassI[i].ErrSubjectName + " chưa được khai báo cho lớp "
                                                            + lstErrSubjectByClassI[i].ErrClass + " ở học kỳ I <br/>";
                                }
                            }

                            //Môn học chưa được khai báo cho lớp ở học kỳ II. Cau truc "Mon hoc X chua khai bao cho lop Y1, Y2,.., Yn o hoc ky II"
                            CheckErrSubjectByClass(AssignSemesterII, lstErrSubjectByClassII, lstTemp_HKII, lstClassSubjectHKII, lstClassProfile, lstSubjectCat);
                            if (lstErrSubjectByClassII != null && lstErrSubjectByClassII.Count > 0)
                            {
                                isLegalBot = false;
                                ICSATVM.AssginSemesterI = AssignSemesterI;
                                ICSATVM.AssignSemesterII = AssignSemesterII;
                                for (int i = 0; i < lstErrSubjectByClassII.Count; i++)
                                {
                                    ICSATVM.ErrorDescription += " - Môn học " + lstErrSubjectByClassII[i].ErrSubjectName + " chưa được khai báo cho lớp "
                                                            + lstErrSubjectByClassII[i].ErrClass + " ở học kỳ II <br/>";
                                }
                            }
                            else
                            {
                                ICSATVM.AssginSemesterI = AssignSemesterI;
                                ICSATVM.AssignSemesterII = AssignSemesterII;
                                ICSATVM.lstSubjectByClassHKI = lstTemp_HKI;
                                ICSATVM.lstSubjectByClassHKII = lstTemp_HKII;
                            }
                        }
                    } //cho not kiem tra quy tac
                }

                //hoc ky
                if (AssignSemesterI != null && AssignSemesterII != null)
                {
                    ICSATVM.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                }
                else if (AssignSemesterI != null)
                {
                    ICSATVM.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                }
                else
                {
                    ICSATVM.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                }
                ICSATVM.isLegal = isLegalBot;
                lsICSATVM.Add(ICSATVM);
                //-------------------------------------------------------------
                count++;
            }
            return lsICSATVM;
        }

        private string TransString(List<string> lstErr1, List<string> lstErr2)
        {
            string error = string.Empty;
            List<string> lstTemp = new List<string>();
            if (lstErr1.Count == 0)
            {
                lstTemp = lstErr2;
            }
            else if (lstErr2.Count == 0)
            {
                lstTemp = lstErr1;
            }
            else
            {
                for (int i = 0; i < lstErr2.Count; i++)
                {
                    lstErr1.Add(lstErr2[i]);
                }
                lstTemp = lstErr1;
            }
            lstTemp = lstTemp.Select(s => s.Trim()).Distinct().ToList();
            for (int j = 0; j < lstTemp.Count; j++)
            {
                error += lstTemp[j] + ", ";
            }
            error = error != string.Empty ? error.Remove(error.Length - 2) : string.Empty;
            return error;
        }


        /// <summary>
        /// Kiem tra mon hoc co duoc khai bao cho lop trong hoc ki I or hoc ky II
        /// </summary>
        /// <param name="AssignSemester"></param>
        /// <param name="lstErrSubjectByClass"></param>
        /// <param name="lstClassSubjectHK"></param>
        /// <param name="lstClassProfile"></param>
        /// <param name="lstSubjectCat"></param>
        private void CheckErrSubjectByClass(string AssignSemester, List<ErrSubjectClass> lstErrSubjectByClass, List<SubjectByClassBO> lstTemp_HK, List<ClassSubjectBO> lstClassSubjectHK,
                                List<ClassProfile> lstClassProfile, List<SubjectCat> lstSubjectCat)
        {
            //VL(6A1, 6A2, 6A3) + TC2(7A1, 7A2)
            //cat chuoi "+" lay ra tung mon hoc va lop rieng biet
            //Sua loi exception khi xu ly chuoi.
            if (String.IsNullOrEmpty(AssignSemester) || string.IsNullOrWhiteSpace(AssignSemester))
            {
                return;
            }
            List<string> lstAS = AssignSemester.Split('+').Select(s => s.Trim()).ToList(); //VL(6A1, 6A2, 6A3)
            if (lstAS == null || lstAS.Count == 0)
            {
                return;
            }
            lstAS = lstAS.Distinct().ToList();
            List<string> lstobjAS = null; //chua mang gia tri mon hoc va cac lop sau khi cat chuoi theo "("
            string subject = string.Empty;
            List<string> lstclass = null;
            ClassProfile cp = new ClassProfile();
            SubjectCat sc = new SubjectCat();
            ClassSubjectBO scBO = new ClassSubjectBO();
            ErrSubjectClass errSC; //struct chua cac lop chua dc khia bao mon hoc 
            SubjectByClassBO objSubjectByClassBO = null; // lop dc khia bao mon hoc
            string temp;
            for (int i = 0; i < lstAS.Count; i++)
            {
                errSC = new ErrSubjectClass();
                objSubjectByClassBO = new SubjectByClassBO();
                objSubjectByClassBO.listClassID = new List<int>();

                lstobjAS = lstAS[i].Split('(').ToList();
                if (lstobjAS == null)
                {
                    continue;
                }
                subject = lstobjAS[0].Trim();
                temp = lstobjAS[1].Replace(")", " ").Trim();
                if (String.IsNullOrEmpty(temp))
                {
                    continue;
                }
                lstclass = temp.Split(',').Select(s => s.Trim()).ToList();
                lstclass = lstclass.Distinct().ToList();
                if (lstclass == null)
                {
                    continue;
                }

                //lay SubjectID
                sc = lstSubjectCat.Where(p => p.Abbreviation.ToUpper().Equals(subject.ToUpper())).FirstOrDefault();
                errSC.ErrSubjectName = subject;
                errSC.ErrClass = string.Empty;
                string className;
                for (int j = 0; j < lstclass.Count; j++)
                {
                    //lay ClassID
                    className = lstclass[j].Trim();
                    cp = lstClassProfile.Where(p => p.DisplayName != null && p.DisplayName.ToUpper().Equals(className.ToUpper())).FirstOrDefault();
                    if (cp == null)
                    {
                        errSC.ErrClass += lstclass[j] + ", ";
                        continue;
                    }
                    //tim kiem mon hoc co duoc khai bao cho lop
                    scBO = lstClassSubjectHK.Where(p => p.ClassID == cp.ClassProfileID && p.SubjectID == sc.SubjectCatID).FirstOrDefault();
                    if (scBO == null)
                    {
                        errSC.ErrClass += lstclass[j] + ", ";
                    }
                    else
                    {
                        objSubjectByClassBO.listClassID.Add(cp.ClassProfileID);
                    }
                }
                objSubjectByClassBO.SubjectID = sc.SubjectCatID;
                lstTemp_HK.Add(objSubjectByClassBO);
                errSC.ErrClass = errSC.ErrClass.Length != 0 ? errSC.ErrClass.Remove(errSC.ErrClass.Length - 2).Trim() : string.Empty;
                if (errSC.ErrClass != string.Empty)
                {
                    lstErrSubjectByClass.Add(errSC);
                }
            }
        }

        /// <summary>
        /// Kiem tra mon hoc va lop co ton tai trong csdl
        /// </summary>
        /// <param name="AssignSemester"></param>
        /// <param name="ErrClass"></param>
        /// <param name="ErrSubject"></param>
        /// <param name="lstClassProfile"></param>
        /// <param name="lstSchoolSubject"></param>
        private void CheckErrExsistClassAndSubject(string AssignSemester, List<string> ErrClass, List<string> ErrSubject,
                                        List<ClassProfile> lstClassProfile, List<SchoolSubjectBO> lstSchoolSubject)
        {
            //VL(6A1, 6A2, 6A3) + TC2(7A1, 7A2)
            //cat chuoi "+" lay ra tung mon hoc va lop rieng biet
            if (String.IsNullOrEmpty(AssignSemester) || string.IsNullOrWhiteSpace(AssignSemester))
            {
                return;
            }
            List<string> lstAS = AssignSemester.Split('+').ToList(); //VL(6A1, 6A2, 6A3)
            List<string> lstobjAS = null; //chua mang gia tri mon hoc va cac lop sau khi cat chuoi theo "("
            string subject = string.Empty;
            List<string> lstclass = null;
            int countSubject = 0;
            int countClass = 0;

            for (int i = 0; i < lstAS.Count; i++)
            {
                lstobjAS = lstAS[i].Split('(').ToList();
                subject = lstobjAS[0].Trim();
                lstclass = lstobjAS[1].Replace(")", " ").Trim().Split(',').Select(s => s.Trim()).ToList();
                lstclass = lstclass.Distinct().ToList();
                //kiem tra mon hoc
                countSubject = lstSchoolSubject.Where(p => p.Abbreviation.ToUpper().Equals(subject.ToUpper())).Count();
                if (countSubject == 0)
                {
                    ErrSubject.Add(subject);
                }

                //kiem tra lop hoc
                for (int j = 0; j < lstclass.Count; j++)
                {
                    countClass = lstClassProfile.Where(p => p.DisplayName != null && p.DisplayName.ToUpper().Equals(lstclass[j].Trim().ToUpper())).Count();
                    if (countClass == 0)
                    {
                        ErrClass.Add(lstclass[j]);
                    }
                }
            }
        }

        /// <summary>
        /// Kiểm tra khai báo phân công giảng day trong file excel co dung quy tac quy dinh
        /// </summary>
        /// <param name="AssignSemester"></param>
        /// <returns></returns>
        private bool CheckRule(string AssignSemester)
        {
            AssignSemester = Utils.Utils.StripVNSign(AssignSemester);
            bool check = true;
            Regex rgx = new Regex(@"[a-zA-Z0-9\s]+\(+[a-zA-Z0-9\s,./_-]+\)");
            List<string> lstAS = AssignSemester.Split('+').Select(s => s.Trim()).ToList();
            int countRegex = 0;
            if (AssignSemester != string.Empty)
            {
                if (lstAS.Count > 1)
                {
                    if (!AssignSemester.Contains("+"))
                        check = false;
                }
                else
                {
                    countRegex = rgx.Matches(lstAS[0]).Count;
                    if (countRegex > 1)
                        check = false;
                }
                foreach (string AS in lstAS)
                {
                    if (!rgx.IsMatch(AS))
                        check = false;
                }
            }
            return check;
        }
        //#endregion 20150622_anhnph1_nang cap giai doan 10
    }
}

