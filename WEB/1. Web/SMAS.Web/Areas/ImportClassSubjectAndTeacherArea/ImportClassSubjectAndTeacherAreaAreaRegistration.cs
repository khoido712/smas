﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ImportClassSubjectAndTeacherArea
{
    public class ImportClassSubjectAndTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ImportClassSubjectAndTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ImportClassSubjectAndTeacherArea_default",
                "ImportClassSubjectAndTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
