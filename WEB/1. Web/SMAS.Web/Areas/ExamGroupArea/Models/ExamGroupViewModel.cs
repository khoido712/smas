﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using SMAS.Web.Areas.ExamGroupArea;
using Resources;
namespace SMAS.Web.Areas.ExamGroupArea.Models
{
    public class ExamGroupViewModel
    {
        [ScaffoldColumn(false)]
        public long ExamGroupID { get; set; }

        public long ExaminationsID { get; set; }

        [ResourceDisplayName("ExamGroup_Label_ExamGroupCode")]        
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamGroup_Validate_Required_ExamGroupCode")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamGroup_Validate_Regex_ExamGroupCode")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]

        public string ExamGroupCode { get; set; }

        [ResourceDisplayName("ExamGroup_Label_ExamGroupName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamGroup_Validate_Required_ExamGroupName")]
        [StringLength(64, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ExamGroupName { get; set; }

        [ResourceDisplayName("ExamGroup_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamGroupConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public long? tempExaminationsID { get; set; }

        public bool checkEdit { get; set; }

        public bool checkDelete { get; set; }
    }
}