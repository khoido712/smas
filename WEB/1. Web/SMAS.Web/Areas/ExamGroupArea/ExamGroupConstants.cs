﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamGroupArea
{
    public class ExamGroupConstants
    {
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string LIST_EXAM_GROUP = "list_exam_group";
        public const string PER_CHECK_CREATE = "per_check_create";
    }
}