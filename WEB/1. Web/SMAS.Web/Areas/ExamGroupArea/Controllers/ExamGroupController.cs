﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Web.Areas.ExamGroupArea.Models;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.ExamGroupArea.Controllers
{
    public class ExamGroupController : BaseController
    {
        #region Properties
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private GlobalInfo global;
        private List<Examinations> listExaminations;
        #endregion

        #region Constructor
        public ExamGroupController(IExamGroupBusiness examGroupBusiness, IExaminationsBusiness examinationsBusiness)
        {
            this.ExamGroupBusiness = examGroupBusiness;
            this.ExaminationsBusiness = examinationsBusiness;
       
            global = new GlobalInfo();
            listExaminations = new List<Examinations>();
        }
        #endregion

        #region Actions
        //
        // GET: /ExamPupilArea/ExamPupil/

        public ActionResult Index()
        {
            
            SetViewData();

            //Search du lieu
            long defaulExamID = listExaminations.FirstOrDefault() != null?listExaminations.FirstOrDefault().ExaminationsID:0;
            List<ExamGroupViewModel> listResult = _Search(defaulExamID).ToList();
            ViewData[ExamGroupConstants.LIST_EXAM_GROUP] = listResult;
            
            //Kiem tra button
            CheckCommandPermision(listExaminations.FirstOrDefault(), listResult);

            return View();
        }

        //
        // GET: /ExamGroup/Search
        
        public PartialViewResult Search(long? examinationsID)
        {
            List<ExamGroupViewModel> listResult = _Search(examinationsID.GetValueOrDefault()).ToList();
            //Kiem tra button
            CheckCommandPermision(ExaminationsBusiness.Find(examinationsID), listResult);
            return PartialView("_List", listResult);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            if (GetMenupermission("ExamGroup", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ExamGroup examGroup = new ExamGroup();
            TryUpdateModel(examGroup);
            Utils.Utils.TrimObject(examGroup);
            this.ExamGroupBusiness.Insert(examGroup);
            this.ExamGroupBusiness.Save();

            //ghi log
            SetViewDataActionAudit(String.Empty, String.Empty
            , String.Empty
            , Res.Get("ExamGroup_Log_Create")
            , String.Empty
            , Res.Get("ExamGroup_Log_FunctionName")
            , SMAS.Business.Common.GlobalConstants.ACTION_ADD
            , Res.Get("ExamGroup_Log_Create"));

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ExamGroupID)
        {
            if (GetMenupermission("ExamGroup", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            ExamGroup oriExamGroup = this.ExamGroupBusiness.Find(ExamGroupID);
            if (!CanBeEdited(oriExamGroup))
            {
                throw new BusinessException("ExamGroup_Validate_NotEdit");
            }

            TryUpdateModel(oriExamGroup);
            Utils.Utils.TrimObject(oriExamGroup);

            this.ExamGroupBusiness.Update(oriExamGroup);
            this.ExamGroupBusiness.Save();

            //ghi log
            SetViewDataActionAudit(String.Empty, String.Empty
            , oriExamGroup.ExamGroupID.ToString()
            , Res.Get("ExamGroup_Log_Edit")
            , "ExamGroupID: "+oriExamGroup.ExamGroupID
            , Res.Get("ExamGroup_Log_FunctionName")
            , SMAS.Business.Common.GlobalConstants.ACTION_UPDATE
            , Res.Get("ExamGroup_Log_Edit"));

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(long id)
        {
            if (GetMenupermission("ExamGroup", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ExamGroup oriExamGroup = this.ExamGroupBusiness.Find(id);
            if (!CanBeDeleted(oriExamGroup))
            {
                throw new BusinessException("ExamGroup_Validate_NotDelete");
            }
            this.ExamGroupBusiness.Delete(id);
            this.ExamGroupBusiness.Save();
            // QuangNN2 - Kiểm tra khi xóa nhóm thi, nếu kỳ thi của nhóm thi không còn dữ liệu thì reset kế thừa (nếu có)
            ExaminationsBusiness.ResetInheritance(oriExamGroup.ExaminationsID);

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #region Private methods
        
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(global.AcademicYearID.GetValueOrDefault())
                                            .Where(o=>o.SchoolID==global.SchoolID)
                                            .Where(o=>o.AppliedLevel==global.AppliedLevel)
                                            .OrderByDescending(o=>o.CreateTime).ToList();

            ViewData[ExamGroupConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");


        }


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ExamGroupViewModel> _Search(long examinationID)
        {
            List<ExamGroupViewModel> ret = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationID)
                        .OrderBy(o => o.ExamGroupCode)
                        .Select(o => new ExamGroupViewModel
                        {
                            ExamGroupID=o.ExamGroupID,
                            ExamGroupCode=o.ExamGroupCode,
                            ExamGroupName=o.ExamGroupName
                        }).ToList();
            return ret;
        }

        /// <summary>
        /// Kiem tra disable/enable, an/hien cac button
        /// </summary>
        /// <param name="listResult"></param>
        private void CheckCommandPermision(Examinations examinations, List<ExamGroupViewModel> listResult)
        {
            SetViewDataPermission("ExamGroup", _globalInfo.UserAccountID, _globalInfo.IsAdmin,_globalInfo.RoleID);
            //Kiem tra quyen them, sua, xoa
            bool checkCreate = false;

            //Kiem tra quyen them
            if (examinations != null)
            {
                if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE]
                    && examinations.MarkInput.GetValueOrDefault() != true
                    && examinations.MarkClosing.GetValueOrDefault() != true
                    && global.IsCurrentYear)
                {
                    checkCreate = true;
                }
            }
            ViewData[ExamGroupConstants.PER_CHECK_CREATE] = checkCreate;

            //Kiem tra quyen sua, xoa
            //Lay danh sach du lieu rang buoc
            List<long> lstExamGroupID=listResult.Select(o=>o.ExamGroupID).ToList<long>();
            List<long> lstInUsedExamGroupID = ExamGroupBusiness.ExamGroupInUsed(lstExamGroupID);
            for (int i = 0; i < listResult.Count; i++)
            {
                ExamGroupViewModel ex = listResult[i];
                ex.checkEdit = false;
                ex.checkDelete = false;

                if (examinations != null)
                {
                    bool isExamGroupUsed = lstInUsedExamGroupID.Contains(ex.ExamGroupID);
                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_UPDATE]
                        && examinations.MarkInput.GetValueOrDefault() != true
                        && examinations.MarkClosing.GetValueOrDefault() != true
                        && global.IsCurrentYear)
                    {
                        ex.checkEdit = true;
                    }

                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE]
                     && !isExamGroupUsed
                     && examinations.MarkInput.GetValueOrDefault() != true
                     && examinations.MarkClosing.GetValueOrDefault() != true)
                    {
                        ex.checkDelete = true;
                    }
                }
            }
        }

        private bool CanBeEdited(ExamGroup examGroup)
        {
            if (examGroup == null)
            {
                return false;
            }
            // Nhóm thi chưa có dữ liệu ràng buộc
            if (ExamGroupBusiness.isInUsed(examGroup.ExamGroupID))
            {
                return false;
            }
            // Kỳ thi chưa tích chọn vào điểm thi và chốt điểm thi
            Examinations exam = ExaminationsBusiness.Find(examGroup.ExaminationsID);
            if (exam != null)
            {
                if (exam.MarkInput.GetValueOrDefault() == true || exam.MarkClosing.GetValueOrDefault() == true)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            //Năm học đang thực hiện là năm học hiện tại
            if (!global.IsCurrentYear)
            {
                return false;
            }

            return true;
        }

        private bool CanBeDeleted(ExamGroup examGroup)
        {
            if (examGroup == null)
            {
                return false;
            }
            // Nhóm thi chưa có dữ liệu ràng buộc
            if (ExamGroupBusiness.isInUsed(examGroup.ExamGroupID))
            {
                return false;
            }
            // Kỳ thi chưa tích chọn vào điểm thi và chốt điểm thi
            Examinations exam = ExaminationsBusiness.Find(examGroup.ExaminationsID);
            if (exam != null)
            {
                if (exam.MarkInput.GetValueOrDefault() == true || exam.MarkClosing.GetValueOrDefault() == true)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }
        #endregion

    }
}
