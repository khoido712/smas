﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamGroupArea
{
    public class ExamGroupAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamGroupArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamGroupArea_default",
                "ExamGroupArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
