﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TranscriptsBySemesterArea
{
    public class TranscriptsBySemesterAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TranscriptsBySemesterArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TranscriptsBySemesterArea_default",
                "TranscriptsBySemesterArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
