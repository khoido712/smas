﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.TranscriptsBySemesterArea.Models
{
    public class TranscriptsBySemesterViewModel 
    {
        [ResourceDisplayName("TranscriptsBySemester_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", TranscriptsBySemesterConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public Nullable<int> EducationLevelID { get; set; }


        [ResourceDisplayName("TranscriptsBySemester_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "CHOICE")]
        [AdditionalMetadata("ViewDataKey", TranscriptsBySemesterConstants.LIST_CLASS)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int ClassID { get; set; }


        [ResourceDisplayName("TranscriptsBySemester_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", TranscriptsBySemesterConstants.LIST_SUBJECT)]
        public Nullable<int> SubjectID { get; set; }

        [ResourceDisplayName("TranscriptsBySemester_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", TranscriptsBySemesterConstants.LIST_SEMESTER)]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject2(this)")]
        public Nullable<int> Semester { get; set; }

    }
}
