﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.TranscriptsBySemesterArea.Models
{
    public class ReviewTranscriptViewModel 
    {
        [ResourceDisplayName("TranscriptsBySemester_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", TranscriptsBySemesterConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClassSumer(this)")]
        public Nullable<int> EducationLevelID { get; set; }


        [ResourceDisplayName("TranscriptsBySemester_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TranscriptsBySemesterConstants.LIST_CLASS)]
        public int? ClassID { get; set; }


        [ResourceDisplayName("TranscriptsBySemester_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", TranscriptsBySemesterConstants.LIST_SEMESTER)]
        public Nullable<int> Semester { get; set; }

    }
}
