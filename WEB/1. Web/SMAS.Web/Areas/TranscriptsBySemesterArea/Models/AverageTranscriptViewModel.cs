﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.TranscriptsBySemesterArea.Models
{
    public class AverageTranscriptViewModel
    {
        [ResourceDisplayName("TranscriptsBySemester_Label_EducationLevel")]
        public int? EducationLevelID2 { get; set; }
        [ResourceDisplayName("TranscriptsBySemester_Label_Class")]
        public int ClassID2 { get; set; }

    }
}
