﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.TranscriptsBySemesterArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.Business.Business;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.TranscriptsBySemesterArea.Controllers
{
    public class AverageTranscriptController : BaseController
    {
        private readonly ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        public AverageTranscriptController(IClassProfileBusiness classprofileBusiness,
            IReportDefinitionBusiness reportdefinitionBusiness,
            IProcessedReportBusiness processedreportBusiness,
                                               ITranscriptsBySemesterBusiness transcriptsbysemesterBusiness
                                              )
        {
            this.TranscriptsBySemesterBusiness = transcriptsbysemesterBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
            this.ProcessedReportBusiness = processedreportBusiness;
        }


        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        private void SetViewData()
        {

            // load combobox EducationLevel
            List<EducationLevel> ListEducationLevel = new GlobalInfo().EducationLevels;
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }

            int EducationLevelID = ListEducationLevel.FirstOrDefault().EducationLevelID;

            ViewData[TranscriptsBySemesterConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution", EducationLevelID);

            // load combobox Class

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;

            int ClassID = 0;

            if (new GlobalInfo().IsAdminSchoolRole == false)
            {

                dic["UserAccountID"] = new GlobalInfo().UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }


            IEnumerable<ClassProfile> lstClass = new List<ClassProfile>();
            if (EducationLevelID != 0)
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
                if (lstClass != null) ClassID = lstClass.FirstOrDefault().ClassProfileID;
            }
            if (lstClass == null) lstClass = new List<ClassProfile>();
           
            ViewData[TranscriptsBySemesterConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            // load combobox Semester
            ViewData[TranscriptsBySemesterConstants.LIST_SEMESTER] = CommonList.SemesterAndAll();

        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass2(int? EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;

            if (new GlobalInfo().IsAdminSchoolRole == false)
            {
                dic["UserAccountID"] = new GlobalInfo().UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }

            IEnumerable<ClassProfile> lstClass = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            }
            if (lstClass == null) lstClass = new List<ClassProfile>();

            ViewData[TranscriptsBySemesterConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetReportAverageAnnualMark(AverageTranscriptViewModel form)
        {
            GlobalInfo gl = new GlobalInfo();
            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            if (gl.SchoolID.HasValue) TranscriptOfClass.SchoolID = gl.SchoolID.Value;

            TranscriptOfClass.ClassID = form.ClassID2;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID2.Value;
            TranscriptOfClass.AcademicYearID = gl.AcademicYearID.Value;
            TranscriptOfClass.AppliedLevel = gl.AppliedLevel.Value;
            TranscriptOfClass.isAdmin = gl.IsAdminSchoolRole;
            TranscriptOfClass.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            TranscriptOfClass.UserAccountID = gl.UserAccountID;
            string reportCode = SystemParamsInFile.REPORT_BANG_DIEM_BINH_QUAN_CA_NAM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.TranscriptsBySemesterBusiness.GetAverageAnnualMark(TranscriptOfClass);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.TranscriptsBySemesterBusiness.CreateAverageAnnualMark(TranscriptOfClass);
                processedReport = this.TranscriptsBySemesterBusiness.InsertAverageAnnualMark(TranscriptOfClass, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportAverageAnnualMark(AverageTranscriptViewModel form)
        {
            GlobalInfo gl = new GlobalInfo();
            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            if (gl.SchoolID.HasValue) TranscriptOfClass.SchoolID = gl.SchoolID.Value;
            TranscriptOfClass.ClassID = form.ClassID2;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID2.Value;
            TranscriptOfClass.AcademicYearID = gl.AcademicYearID.Value;
            TranscriptOfClass.AppliedLevel = gl.AppliedLevel.Value;
            TranscriptOfClass.isAdmin = gl.IsAdminSchoolRole;
            TranscriptOfClass.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            TranscriptOfClass.UserAccountID =gl.UserAccountID;
            Stream excel = this.TranscriptsBySemesterBusiness.CreateAverageAnnualMark(TranscriptOfClass);
            ProcessedReport processedReport = this.TranscriptsBySemesterBusiness.InsertAverageAnnualMark(TranscriptOfClass, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}
