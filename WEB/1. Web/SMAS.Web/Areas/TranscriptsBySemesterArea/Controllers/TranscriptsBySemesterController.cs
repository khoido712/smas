﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Web.Areas.TranscriptsBySemesterArea;
using SMAS.Web.Areas.TranscriptsBySemesterArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.TranscriptsBySemesterArea.Controllers
{


    public class TranscriptsBySemesterController : BaseController
    {
        public int GVCN = 1;

        private readonly ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;


        public TranscriptsBySemesterController(IClassProfileBusiness classprofileBusiness,
                                               IProcessedReportBusiness processedreportBusiness,
                                               ITranscriptsBySemesterBusiness transcriptsbysemesterBusiness,
                                               IReportDefinitionBusiness reportdefinitionBusiness,
            IAcademicYearBusiness academicYearBusiness,
                                               IClassSubjectBusiness classsubjectBusiness, IClassSupervisorAssignmentBusiness classSupervisorAssignmentBusiness, IUserAccountBusiness userAccountBusiness, ITeachingAssignmentBusiness TeachingAssignmentBusiness)
        {
            this.TranscriptsBySemesterBusiness = transcriptsbysemesterBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.ClassSubjectBusiness = classsubjectBusiness;
            this.ProcessedReportBusiness = processedreportBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
            this.ClassSupervisorAssignmentBusiness = classSupervisorAssignmentBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
        }


        private void SetViewData()
        {

            // load combobox EducationLevel
            List<EducationLevel> ListEducationLevel = new GlobalInfo().EducationLevels;
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }

            int EducationLevelID = ListEducationLevel.FirstOrDefault().EducationLevelID;

            ViewData[TranscriptsBySemesterConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution", EducationLevelID);

            // load combobox Class

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;

            if (new GlobalInfo().IsAdminSchoolRole == false)
            {

                dic["UserAccountID"] = new GlobalInfo().UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }


            IEnumerable<ClassProfile> lstClass = new List<ClassProfile>();
            if (EducationLevelID > 0)
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            }
            if (lstClass == null)
                lstClass = new List<ClassProfile>();

            ViewData[TranscriptsBySemesterConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // load combobox Semester
            List<ComboObject> lstSemester = CommonList.SemesterAndAll();
            ViewData[TranscriptsBySemesterConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", lstSemester.FirstOrDefault().key);

            // combobox Subject
            ViewData[TranscriptsBySemesterConstants.LIST_SUBJECT] = new SelectList(new string[] { });

        }




        public ActionResult Index()
        {
            SetViewData();
            GlobalInfo global = new GlobalInfo();
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            //DateTime? SecondEndDate = academicYear.SecondSemesterEndDate.Value;
            //DateTime? SecondStartDate = academicYear.SecondSemesterStartDate.Value;
            string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);
            ViewData["DateHK"] = null;
            if (datenow >= FirstStarDate && datenow <= FirstEndDate)
            {
                ViewData["DateHK"] = "1";
            }
            else
            {
                ViewData["DateHK"] = "2";
            }
            return View();
        }


        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID, int? semesterID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;
            if (semesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || semesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                dic["Semester"] = semesterID.Value;
            }


            if (new GlobalInfo().IsAdminSchoolRole == false)
            {

                dic["UserAccountID"] = new GlobalInfo().UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }


            IEnumerable<ClassProfile> lstClass = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            }
            if (lstClass == null)
                lstClass = new List<ClassProfile>();

            ViewData[TranscriptsBySemesterConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        [SkipCheckRole]

        //[ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? ClassID, int? semesterID)
        {
            List<ClassSubject> lstSubject = new List<ClassSubject>();
            GlobalInfo global = new GlobalInfo();
            var userAccountID = global.UserAccountID;
            int academicYearID = global.AcademicYearID.Value;
            int schoolID = global.SchoolID.Value;
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["SchoolID"] = schoolID;
            if (ClassID.HasValue)
            {
                if (UtilsBusiness.HasHeadTeacherPermission(userAccountID, ClassID.Value))
                {
                    lstSubject = this.ClassSubjectBusiness.SearchByClass(ClassID.Value, dicClassSubject).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
                }
                //Nếu có quyền giáo viên bộ môn
                else if (UtilsBusiness.HasSubjectTeacherPermission_BM(userAccountID, ClassID.Value, semesterID.Value))
                {
                    //giáo viên được phân quyền giáo viên bộ môn trong chức năng phân công giáo vụ
                    int? teacherID = UserAccountBusiness.Find(userAccountID).EmployeeID;
                    if (teacherID != 0)
                    {
                        IQueryable<ClassSupervisorAssignment> lstCsa = ClassSupervisorAssignmentBusiness.All.Where(
                                                              o => o.TeacherID == teacherID &&
                                                                  o.ClassID == ClassID &&
                                                                  o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);
                        if (lstCsa.Count() > 0)
                        {
                            lstSubject = this.ClassSubjectBusiness.SearchByClass(ClassID.Value, dicClassSubject).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
                        }
                        //Giáo viên được phân công giảng dạy
                        else
                        {
                            List<ClassSubjectTemp> lstSubjectCat = TeachingAssignmentBusiness.GetListSubjectBySubjectTeacher_BM(teacherID.Value, academicYearID, schoolID, semesterID.Value, ClassID.Value).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                            return Json(new SelectList(lstSubjectCat, "SubjectID", "DisplayName", ClassID));
                        }

                    }

                }
                return Json(new SelectList(lstSubject, "SubjectID", "SubjectCat.DisplayName", ClassID));
            }
            else
            {
                return Json(new SelectList(lstSubject, "SubjectID", "SubjectCat.DisplayName", ClassID));
            }

        }

        [SkipCheckRole]

        //[ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject2(int? ClassID, int? semesterID)
        {
            List<ClassSubject> lstSubject = new List<ClassSubject>();
            GlobalInfo global = new GlobalInfo();
            var userAccountID = global.UserAccountID;
            int academicYearID = global.AcademicYearID.Value;
            int schoolID = global.SchoolID.Value;
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["SchoolID"] = schoolID;
            if (ClassID.HasValue)
            {
                if (UtilsBusiness.HasHeadTeacherPermission(userAccountID, ClassID.Value))
                {
                    lstSubject = this.ClassSubjectBusiness.SearchByClass(ClassID.Value,dicClassSubject).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
                }
                //Nếu có quyền giáo viên bộ môn
                else if (UtilsBusiness.HasSubjectTeacherPermission_BM(userAccountID, ClassID.Value, semesterID.Value))
                {
                    //giáo viên được phân quyền giáo viên bộ môn trong chức năng phân công giáo vụ
                    int? teacherID = UserAccountBusiness.Find(userAccountID).EmployeeID;
                    if (teacherID != 0)
                    {
                        IQueryable<ClassSupervisorAssignment> lstCsa = ClassSupervisorAssignmentBusiness.All.Where(
                                                              o => o.TeacherID == teacherID &&
                                                                  o.ClassID == ClassID &&
                                                                  o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);
                        if (lstCsa.Count() > 0)
                        {
                            lstSubject = this.ClassSubjectBusiness.SearchByClass(ClassID.Value,dicClassSubject).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
                        }
                        //Giáo viên được phân công giảng dạy
                        else
                        {
                            List<ClassSubjectTemp> lstSubjectCat = TeachingAssignmentBusiness.GetListSubjectBySubjectTeacher_BM(teacherID.Value, academicYearID, schoolID, semesterID.Value, ClassID.Value).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                            return Json(new SelectList(lstSubjectCat, "SubjectID", "DisplayName", ClassID));
                        }

                    }

                }
                return Json(new SelectList(lstSubject, "SubjectID", "SubjectCat.DisplayName", ClassID));
            }
            else
            {
                return Json(new SelectList(lstSubject, "SubjectID", "SubjectCat.DisplayName", ClassID));
            }

        }




        //[ValidateAntiForgeryToken]
        public JsonResult GetReportBySemester(TranscriptsBySemesterViewModel form)
        {

            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            GlobalInfo glo = new GlobalInfo();

            TranscriptOfClass.ClassID = form.ClassID;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            TranscriptOfClass.UserAccountID = new GlobalInfo().UserAccountID;
            TranscriptOfClass.isAdmin = new GlobalInfo().IsAdminSchoolRole;
            if (glo.IsAdminSchoolRole == false)
            {
                TranscriptOfClass.TeacherID = UserAccountBusiness.Find(new GlobalInfo().UserAccountID).EmployeeID.Value;
            }
            if (form.SubjectID.HasValue)
            {
                TranscriptOfClass.SubjectID = form.SubjectID.Value;
            }
            TranscriptOfClass.Semester = form.Semester.Value;
            TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
            TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            string reportCode = "";
            if (form.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_HOCKYI;
            }
            if (form.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_HOCKYII;
            }
            if (form.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_CANAM;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.TranscriptsBySemesterBusiness.GetTranscriptsBySemester(TranscriptOfClass);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.TranscriptsBySemesterBusiness.CreateTranscriptsBySemester(TranscriptOfClass);
                processedReport = this.TranscriptsBySemesterBusiness.InsertTranscriptsBySemester(TranscriptOfClass, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySemester(TranscriptsBySemesterViewModel form)
        {
            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            GlobalInfo glo = new GlobalInfo();
            TranscriptOfClass.ClassID = form.ClassID;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            TranscriptOfClass.UserAccountID = new GlobalInfo().UserAccountID;
            TranscriptOfClass.isAdmin = new GlobalInfo().IsAdminSchoolRole;
            if (glo.IsAdminSchoolRole == false)
            {
                TranscriptOfClass.TeacherID = UserAccountBusiness.Find(new GlobalInfo().UserAccountID).EmployeeID.Value;
            }
            if (form.SubjectID.HasValue)
            {
                TranscriptOfClass.SubjectID = form.SubjectID.Value;
            }
            TranscriptOfClass.Semester = form.Semester.Value;
            TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
            TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;

            Stream excel = this.TranscriptsBySemesterBusiness.CreateTranscriptsBySemester(TranscriptOfClass);
            ProcessedReport processedReport = this.TranscriptsBySemesterBusiness.InsertTranscriptsBySemester(TranscriptOfClass, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

    }
}
