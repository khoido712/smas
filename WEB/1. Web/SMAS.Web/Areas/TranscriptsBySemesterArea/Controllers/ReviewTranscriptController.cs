﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Web.Areas.TranscriptsBySemesterArea;
using SMAS.Web.Areas.TranscriptsBySemesterArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Business;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.TranscriptsBySemesterArea.Controllers
{
    public class ReviewTranscriptController : BaseController
    {
        public int GVCN = 1;
        //
        // GET: /TranscriptsBySemesterArea/ReviewTranscript/
        private readonly ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;

        public ReviewTranscriptController(IClassProfileBusiness classprofileBusiness,
                                               IProcessedReportBusiness processedreportBusiness,
                                               ITranscriptsBySemesterBusiness transcriptsbysemesterBusiness,
                                               IReportDefinitionBusiness reportdefinitionBusiness,
                                               IAcademicYearBusiness academicYearBusiness,
                                               IClassSubjectBusiness classsubjectBusiness)
        {
            this.TranscriptsBySemesterBusiness = transcriptsbysemesterBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.ClassSubjectBusiness = classsubjectBusiness;
            this.ProcessedReportBusiness = processedreportBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
        }


        private void SetViewData()
        {

            // load combobox EducationLevel
            List<EducationLevel> ListEducationLevel = new GlobalInfo().EducationLevels;
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }

            int EducationLevelID = ListEducationLevel.FirstOrDefault().EducationLevelID;

            ViewData[TranscriptsBySemesterConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution", EducationLevelID);

            // load combobox Class

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;

            int ClassID = 0;

            if (new GlobalInfo().IsAdminSchoolRole == false)
            {

                dic["UserAccountID"] = new GlobalInfo().UserAccountID;
                dic["Type"] = GVCN;
            }


            IEnumerable<ClassProfile> lstClass = new List<ClassProfile>();
            if (EducationLevelID>0)
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
                ClassID = lstClass.FirstOrDefault().ClassProfileID;
            }
            if (lstClass == null)
                lstClass = new List<ClassProfile>();

            ViewData[TranscriptsBySemesterConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName", Res.Get("All"));

            // load combobox Semester
            List<ComboObject> lstSemester = CommonList.SemesterAndAll();
            ViewData[TranscriptsBySemesterConstants.LIST_SEMESTER] = new SelectList(lstSemester, "key", "value", lstSemester.FirstOrDefault().key);



        }

        [SkipCheckRole]

        //[ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;

            //int ClassID = 0;

            if (new GlobalInfo().IsAdminSchoolRole == false)
            {

                dic["UserAccountID"] = new GlobalInfo().UserAccountID;
                dic["Type"] = GVCN;
            }


            IEnumerable<ClassProfile> lstClass = new List<ClassProfile>();
            if (EducationLevelID != null)
            {

                lstClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
                if (lstClass == null || lstClass.Count() == 0)
                    lstClass = new List<ClassProfile>();
                //else
                   // ClassID = lstClass.FirstOrDefault().ClassProfileID;
            }

            ViewData[TranscriptsBySemesterConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
            //return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName", ClassID));
        }

        [SkipCheckRole]

        //[ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int ClassID, int? semesterID)
        {
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["SchoolID"] = _globalInfo.SchoolID;
            List<ClassSubject> lstSubject = this.ClassSubjectBusiness.SearchByClass(ClassID, dicClassSubject).ToList();
            if (semesterID.HasValue && semesterID == 1)
            {
                lstSubject = lstSubject.Where(o => o.SectionPerWeekFirstSemester > 0).ToList();
            }
            if (semesterID.HasValue && semesterID == 2)
            {
                lstSubject = lstSubject.Where(o => o.SectionPerWeekSecondSemester > 0).ToList();
            }
            if (semesterID.HasValue && semesterID == 3)
            {
                lstSubject = lstSubject.Where(o => o.SectionPerWeekSecondSemester > 0 && o.SectionPerWeekFirstSemester > 0).ToList();
            }
            return Json(new SelectList(lstSubject, "SubjectID", "SubjectCat.DisplayName", ClassID));
        }

        public ActionResult Index()
        {
            SetViewData();

            return View();
        }

        //[ValidateAntiForgeryToken]
        public JsonResult GetReportBySemester(TranscriptsBySemesterViewModel form)
        {

            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();

            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            TranscriptOfClass.ClassID = form.ClassID;
            TranscriptOfClass.Semester = form.Semester.Value;
            TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
            TranscriptOfClass.isAdmin = new GlobalInfo().IsAdminSchoolRole;
            TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            TranscriptOfClass.UserAccountID = new GlobalInfo().UserAccountID;
            string reportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEOKY; ;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.TranscriptsBySemesterBusiness.GetSummariseTranscriptsBySemester(TranscriptOfClass);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptsBySemesterBusiness.CreateSummariseTranscriptsBySemester(TranscriptOfClass);
                processedReport = this.TranscriptsBySemesterBusiness.InsertSummariseTranscriptsBySemester(TranscriptOfClass, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        //[ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySemester(TranscriptsBySemesterViewModel form)
        {
            TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
            TranscriptOfClass.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            TranscriptOfClass.EducationLevelID = form.EducationLevelID.Value;
            TranscriptOfClass.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            TranscriptOfClass.ClassID = form.ClassID;
            TranscriptOfClass.Semester = form.Semester.Value;
            TranscriptOfClass.isAdmin = new GlobalInfo().IsAdminSchoolRole;
            TranscriptOfClass.SchoolID = new GlobalInfo().SchoolID.Value;
            TranscriptOfClass.UserAccountID = new GlobalInfo().UserAccountID;
            Stream excel = this.TranscriptsBySemesterBusiness.CreateSummariseTranscriptsBySemester(TranscriptOfClass);
            ProcessedReport processedReport = this.TranscriptsBySemesterBusiness.InsertSummariseTranscriptsBySemester(TranscriptOfClass, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}
