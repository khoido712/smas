/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.EmployeeWorkMovementArea
{
    public class EmployeeWorkMovementConstants
    {
        public const string LIST_EMPLOYEEWORKMOVEMENT = "listEmployeeWorkMovement";
        public const string LIST_SCHOOLFACULTY = "listSchoolFaculty";
        public const string LIST_EMPLOYEE = "listEmployee";
        public const string LIST_MOVETYPE = "listMoveType";
        public const string LIST_SCHOOL = "listSchool";
        public const string LIST_PROVINCE = "listProvince";
        public const string LIST_DISTRICT = "listDISTRICT";
        public const int MOVE_TYPE_SYSSCHOOL = 1;
        public const int MOVE_TYPE_OTHERDEPT = 2;

        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";

    }
}
