/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using System.Web.Mvc;
namespace SMAS.Web.Areas.EmployeeWorkMovementArea.Models
{
    public class EmployeeWorkMovementViewModel
    {
        [ScaffoldColumn(false)]
		public Int32 EmployeeWorkMovementID { get; set; }
 
        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Int32 SchoolFacultyID { get; set; }

        [ResourceDisplayName("WorkMovement_Label_SchoolFaculty")]
        [ScaffoldColumn(false)]
        public string SchoolFacultyName { get; set; }

        [ResourceDisplayName("WorkMovement_Label_EmployeeName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ScaffoldColumn(false)]
        public Int32 TeacherID { get; set; }

        [ResourceDisplayName("WorkMovement_Label_EmployeeName")]
        [ScaffoldColumn(false)]
        public string TeacherName { get; set; }

        [ResourceDisplayName("WorkMovement_Label_EmployeeName")]
        [ScaffoldColumn(false)]
        public string TeacherShortName { get; set; }

        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        [ScaffoldColumn(false)]
        public string TeacherCode { get; set; }

        [ResourceDisplayName("WorkMovement_Label_MoveType")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeWorkMovementConstants.LIST_MOVETYPE)]
        [AdditionalMetadata("OnChange", "ChangeDiplay(this)")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int MovementType { get; set; }

        [ResourceDisplayName("WorkMovement_Label_MoveType")]
        [ScaffoldColumn(false)]
        public string MoveName { get; set; }

        [ResourceDisplayName("WorkMovement_Label_Province")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeWorkMovementConstants.LIST_PROVINCE)]
        [AdditionalMetadata("OnChange", "AjaxLoadDistrictByProvince(this)")]
        public Nullable<Int32> ToProvinceID { get; set; }

        [ResourceDisplayName("WorkMovement_Label_Province")]
        [ScaffoldColumn(false)]
        public string ToProvinceName { get; set; }

        [ResourceDisplayName("WorkMovement_Label_District")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeWorkMovementConstants.LIST_DISTRICT)]
        [AdditionalMetadata("OnChange", "AjaxLoadSchoolByDistrict(this)")]
        public Nullable<Int32> ToDistrictID { get; set; }
        [ResourceDisplayName("WorkMovement_Label_District")]
        [ScaffoldColumn(false)]
        public string ToDistrictName { get; set; }

        [ResourceDisplayName("WorkMovement_Label_School")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeWorkMovementConstants.LIST_SCHOOL)]
        public Nullable<Int32> ToSchoolID { get; set; }
        [ResourceDisplayName("WorkMovement_Label_School")]
        [ScaffoldColumn(false)]
        public string ToSchoolName { get; set; }

        [ResourceDisplayName("WorkMovement_Label_MoveTo")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]        
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]        
        public string MoveTo { get; set; }	

        [ScaffoldColumn(false)]
		public Int32 FromSchoolID { get; set; }								

        [ResourceDisplayName("WorkMovement_Label_ResolutionDocument")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]        
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]        
        public string ResolutionDocument { get; set; }
								
		[ScaffoldColumn(false)]						
		public Nullable<Int32> ToSupervisingDeptID { get; set; }								
										
		[ResourceDisplayName("WorkMovement_Label_MovedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]				
		public Nullable<DateTime> MovedDate { get; set; }

        [ResourceDisplayName("WorkMovement_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }	
		
		[ScaffoldColumn(false)]			
		public bool? IsAccepted { get; set; }								
	       
    }
}


