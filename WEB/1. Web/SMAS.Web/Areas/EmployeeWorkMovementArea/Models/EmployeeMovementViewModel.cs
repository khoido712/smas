﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.EmployeeWorkMovementArea.Models
{
    public class EmployeeMovementViewModel
    {
        public int MovementType { get; set; }

        public int? EmployeeWorkMovementID { get; set; }

        public int TeacherID { get; set; }

        public int? ToProvinceID { get; set; }

        public int? ToDistrictID { get; set; }

        public int? ToCommuneID { get; set; }
         
        [Required(ErrorMessage="Trường không được để trống")]
        public int ToSchoolID { get; set; }

        [ResourceDisplayName("WorkMovement_Label_ResolutionDocument")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]        
        public string ResolutionDocument { get; set; }

        [ResourceDisplayName("WorkMovement_Label_MoveTo")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]   
        public string MoveTo { get; set; }

        [ResourceDisplayName("WorkMovement_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [ResourceDisplayName("WorkMovement_Label_MovedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]	
        public DateTime MovedDate { get; set; }

        [ResourceDisplayName("WorkMovement_Label_MovedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime MovedDateOther { get; set; }

        [ResourceDisplayName("WorkMovement_Label_ResolutionDocument")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ResolutionDocumentOther { get; set; }

        [ResourceDisplayName("WorkMovement_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string DescriptionOther { get; set; }
    }
}