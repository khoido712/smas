﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.EmployeeWorkMovementArea.Models;

namespace SMAS.Web.Areas.EmployeeWorkMovementArea.Controllers
{
    public class EmployeeWorkMovementController : BaseController
    {
        private readonly IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ICommuneBusiness CommuneBusiness;

        public EmployeeWorkMovementController(IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness
                    , ISchoolFacultyBusiness SchoolFacultyBusiness
                    , IEmployeeBusiness EmployeeBusiness
                    , IProvinceBusiness ProvinceBusiness
                    , IDistrictBusiness DistrictBusiness
                    , ISchoolProfileBusiness SchoolProfileBusiness
                    , ICommuneBusiness CommuneBusiness)
        {
            this.EmployeeWorkMovementBusiness = EmployeeWorkMovementBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.CommuneBusiness = CommuneBusiness;
        }

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();
            //Get view data here

            int AcademicYearID = globalInfo.AcademicYearID.HasValue ? globalInfo.AcademicYearID.Value : 0;
            int SchoolID = globalInfo.SchoolID.HasValue ? globalInfo.SchoolID.Value : 0;
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["SchoolID"] = SchoolID;
           // SearchInfo["AppliedLevel"] = globalInfo.AppliedLevel;
            
            IEnumerable<EmployeeWorkMovementViewModel> lst = this._Search(SearchInfo);
            ViewData[EmployeeWorkMovementConstants.LIST_EMPLOYEEWORKMOVEMENT] = lst;
            // Lay du lieu cho combobox
            // School Faculty
            IQueryable<SchoolFaculty> IQSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo).OrderBy(o => o.FacultyName);
            SelectList ListSchoolFaculty =
                new SelectList((IEnumerable<SchoolFaculty>)IQSchoolFaculty.ToList(), "SchoolFacultyID", "FacultyName");
            ViewData[EmployeeWorkMovementConstants.LIST_SCHOOLFACULTY] = ListSchoolFaculty;
            // Employee
            ViewData[EmployeeWorkMovementConstants.LIST_EMPLOYEE] = new SelectList(new List<Employee>());
            // Move Type
            List<ComboObject> ListComboMoveType = new List<ComboObject>();
            ListComboMoveType.Add(new ComboObject(EmployeeWorkMovementConstants.MOVE_TYPE_SYSSCHOOL.ToString(),
                Res.Get("WorkMovement_Label_SysSchool")));
            ListComboMoveType.Add(new ComboObject(EmployeeWorkMovementConstants.MOVE_TYPE_OTHERDEPT.ToString(),
                Res.Get("WorkMovement_Label_OtherDept")));
            SelectList ListMoveType =
                new SelectList(ListComboMoveType, "key", "value");
            ViewData[EmployeeWorkMovementConstants.LIST_MOVETYPE] = ListMoveType;
            // Province
            IQueryable<Province> IQProvince = ProvinceBusiness.Search(SearchInfo).OrderBy(o => o.ProvinceName);
            SelectList ListProvince =
                new SelectList((IEnumerable<Province>)IQProvince.ToList(), "ProvinceID", "ProvinceName");
            ViewData[EmployeeWorkMovementConstants.LIST_PROVINCE] = ListProvince;
            // District
            ViewData[EmployeeWorkMovementConstants.LIST_DISTRICT] = new SelectList(new List<District>(), "DistrictID", "DistrictName");
            // School
            ViewData[EmployeeWorkMovementConstants.LIST_DISTRICT] = new SelectList(new List<SchoolProfile>(), "SchoolProfileID", "SchoolName");
            ViewData[EmployeeWorkMovementConstants.IS_CURRENT_YEAR] = globalInfo.IsCurrentYear;
            return View();
        }

        /**
         * Thuc hien load du lieu Quan/Huyen khi chon Tinh/Thanh Pho
         **/

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingDistrictByProvince(int? ProvinceId)
        {
            IEnumerable<District> lst = new List<District>();
            if (ProvinceId.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ProvinceID"] = ProvinceId;
                dic["IsActive"] = true;
                lst = this.DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName).ToList();
            }
            if (lst == null)
                lst = new List<District>();
            return Json(new SelectList(lst, "DistrictID", "DistrictName"));
        }

        /**
         * Thuc hien load du lieu Phuong/xa theo quan huyen
         **/

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingCommuneByDistrict(int? ProvinceId, int? DistrictId)
        {
            IEnumerable<Commune> lst = new List<Commune>();
            if (DistrictId.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["DistrictID"] = DistrictId.Value;
                if (ProvinceId.HasValue)
                {
                    dic["ProvinceID"] = ProvinceId.Value;
                }
                lst = CommuneBusiness.Search(dic).OrderBy(u=> u.CommuneName).ToList();
            }
            if (lst == null)
                lst = new List<Commune>();
            return Json(new SelectList(lst, "CommuneID", "CommuneName"));
        }

        /**
         * Thuc hien load du lieu Truong khi co thong tin Quan/Huyen va Tinh/Thanh Pho
         **/

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingSchoolByDistrict(int? ProvinceId, int? DistrictId)
        {
            IEnumerable<SchoolProfile> lst = new List<SchoolProfile>();
            if (DistrictId.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["DistrictID"] = DistrictId.Value;
                if (ProvinceId.HasValue)
                {
                    dic["ProvinceID"] = ProvinceId.Value;
                }
                lst = this.SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName).ToList();
            }
            return Json(new SelectList(lst, "SchoolProfileID", "SchoolName"));
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingSchoolByCommune(int? DistrictId, int? CommuneId)
        {
            IEnumerable<SchoolProfile> lst = new List<SchoolProfile>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (DistrictId.HasValue) dic["DistrictID"] = DistrictId.Value;
            if (CommuneId.HasValue) dic["CommuneID"] = CommuneId.Value;
            lst = this.SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName).ToList();
            return Json(new SelectList(lst, "SchoolProfileID", "SchoolName"));
        }
        
        /// <summary>
        ///       * Thuc hien load du lieu Can bo khi co thong tin truong va to bo mon
        /// </summary>
        /// <param name="ProvinceId"></param>
        /// <param name="DistrictId"></param>
        /// <returns></returns>           

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingTeacherByFaculty(int? SchoolFacultyID)
        {
            IEnumerable<Employee> lst = new List<Employee>();
            int FacultyID = 0;
            int SchoolID = 0;
            if (SchoolFacultyID.HasValue)
            {
                FacultyID = SchoolFacultyID.Value;
            }
            GlobalInfo globalInfo = new GlobalInfo();
            if (globalInfo.SchoolID.HasValue)
            {
                SchoolID = globalInfo.SchoolID.Value;
            }
            IQueryable<Employee> ListEmployee = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(SchoolID, FacultyID).OrderBy(o=>o.Name).ThenBy(o => o.FullName);
            if (ListEmployee != null){
                lst = ListEmployee.ToList();
            }
           // lst = lst.Where(o => o.AppliedLevel == globalInfo.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }

        //
        // GET: /EmployeeWorkMovement/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = globalInfo.AcademicYearID;
            SearchInfo["SchoolID"] = globalInfo.SchoolID;
            //

            IEnumerable<EmployeeWorkMovementViewModel> lst = this._Search(SearchInfo);
            ViewData[EmployeeWorkMovementConstants.LIST_EMPLOYEEWORKMOVEMENT] = lst;
            ViewData[EmployeeWorkMovementConstants.IS_CURRENT_YEAR] = globalInfo.IsCurrentYear;
            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            EmployeeWorkMovement employeeworkmovement = new EmployeeWorkMovement();
            TryUpdateModel(employeeworkmovement);
            Utils.Utils.TrimObject(employeeworkmovement);
            employeeworkmovement.FromSchoolID = new GlobalInfo().SchoolID.Value;
            if (employeeworkmovement.MovementType == SystemParamsInFile.WORK_MOVEMENT_WITHIN_SYSTEM)
            {
                employeeworkmovement.MoveTo = null;
                this.EmployeeWorkMovementBusiness.InsertMovementToSchoolWithinSystem(employeeworkmovement);
            }
            else if (employeeworkmovement.MovementType == SystemParamsInFile.WORK_MOVEMENT_OTHER_ORGANIZATION)
            {
                employeeworkmovement.ToProvinceID = null;
                employeeworkmovement.ToDistrictID = null;
                employeeworkmovement.ToSchoolID = null;
                this.EmployeeWorkMovementBusiness.InsertMovementToOtherOrganization(employeeworkmovement);
            }            
            this.EmployeeWorkMovementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EmployeeWorkMovementID)
        {
            EmployeeWorkMovement employeeworkmovement = this.EmployeeWorkMovementBusiness.Find(EmployeeWorkMovementID);
            TryUpdateModel(employeeworkmovement);
            Utils.Utils.TrimObject(employeeworkmovement);
            if (employeeworkmovement.MovementType == SystemParamsInFile.WORK_MOVEMENT_WITHIN_SYSTEM)
            {
                employeeworkmovement.MoveTo = null;
                this.EmployeeWorkMovementBusiness.UpdateMovementToSchoolWithinSystem(employeeworkmovement);
            }
            else if (employeeworkmovement.MovementType == SystemParamsInFile.WORK_MOVEMENT_OTHER_ORGANIZATION)
            {
                employeeworkmovement.ToProvinceID = null;
                employeeworkmovement.ToDistrictID = null;
                employeeworkmovement.ToSchoolID = null;
                this.EmployeeWorkMovementBusiness.UpdateMovementToOtherOrganization(employeeworkmovement);
            }
            this.EmployeeWorkMovementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult CreateMovement()
        {
            EmployeeMovementViewModel employeeMovementViewModel = new EmployeeMovementViewModel();
            TryUpdateModel(employeeMovementViewModel);
            EmployeeWorkMovement employeeworkmovement = new EmployeeWorkMovement();
            employeeworkmovement.FromSchoolID = _globalInfo.SchoolID.Value;

            if (employeeMovementViewModel.MovementType == SystemParamsInFile.WORK_MOVEMENT_WITHIN_SYSTEM)
            {
                employeeworkmovement.MoveTo = null;
                employeeworkmovement.MovementType = employeeMovementViewModel.MovementType;
                employeeworkmovement.TeacherID = employeeMovementViewModel.TeacherID;
                employeeworkmovement.EmployeeWorkMovementID = employeeMovementViewModel.EmployeeWorkMovementID != null ? employeeMovementViewModel.EmployeeWorkMovementID.Value : 0;
                employeeworkmovement.ToProvinceID = employeeMovementViewModel.ToProvinceID;
                employeeworkmovement.ToDistrictID = employeeMovementViewModel.ToDistrictID;
                employeeworkmovement.ToCommuneID = employeeMovementViewModel.ToCommuneID;
                employeeworkmovement.ToSchoolID = employeeMovementViewModel.ToSchoolID;
                employeeworkmovement.MovedDate = employeeMovementViewModel.MovedDate;
                employeeworkmovement.ResolutionDocument = employeeMovementViewModel.ResolutionDocument;
                employeeworkmovement.Description = employeeMovementViewModel.Description;
                this.EmployeeWorkMovementBusiness.InsertMovementToSchoolWithinSystem(employeeworkmovement);
            }
            else if (employeeMovementViewModel.MovementType == SystemParamsInFile.WORK_MOVEMENT_OTHER_ORGANIZATION)
            {
                employeeworkmovement.ToProvinceID = null;
                employeeworkmovement.ToDistrictID = null;
                employeeworkmovement.ToSchoolID = null;
                employeeworkmovement.MovementType = employeeMovementViewModel.MovementType;
                employeeworkmovement.TeacherID = employeeMovementViewModel.TeacherID;
                employeeworkmovement.EmployeeWorkMovementID = employeeMovementViewModel.EmployeeWorkMovementID != null ? employeeMovementViewModel.EmployeeWorkMovementID.Value : 0; ;
                employeeworkmovement.MoveTo = employeeMovementViewModel.MoveTo;
                employeeworkmovement.MovedDate = employeeMovementViewModel.MovedDateOther;
                employeeworkmovement.ResolutionDocument = employeeMovementViewModel.ResolutionDocumentOther;
                employeeworkmovement.Description = employeeMovementViewModel.DescriptionOther;
                this.EmployeeWorkMovementBusiness.InsertMovementToOtherOrganization(employeeworkmovement);
            }
            this.EmployeeWorkMovementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult EditMovement(int EmployeeWorkMovementID)
        {
            EmployeeMovementViewModel employeeMovementViewModel = new EmployeeMovementViewModel();
            TryUpdateModel(employeeMovementViewModel);
            EmployeeWorkMovement employeeworkmovement = this.EmployeeWorkMovementBusiness.Find(EmployeeWorkMovementID);
            if (employeeMovementViewModel.MovementType == SystemParamsInFile.WORK_MOVEMENT_WITHIN_SYSTEM)
            {
                employeeworkmovement.MoveTo = null;
                employeeworkmovement.MovementType = employeeMovementViewModel.MovementType;
                employeeworkmovement.TeacherID = employeeMovementViewModel.TeacherID;
                employeeworkmovement.EmployeeWorkMovementID = employeeMovementViewModel.EmployeeWorkMovementID != null ? employeeMovementViewModel.EmployeeWorkMovementID.Value : 0;
                employeeworkmovement.ToProvinceID = employeeMovementViewModel.ToProvinceID;
                employeeworkmovement.ToDistrictID = employeeMovementViewModel.ToDistrictID;
                employeeworkmovement.ToCommuneID = employeeMovementViewModel.ToCommuneID;
                employeeworkmovement.ToSchoolID = employeeMovementViewModel.ToSchoolID;
                employeeworkmovement.MovedDate = employeeMovementViewModel.MovedDate;
                employeeworkmovement.ResolutionDocument = employeeMovementViewModel.ResolutionDocument;
                employeeworkmovement.Description = employeeMovementViewModel.Description;
                this.EmployeeWorkMovementBusiness.UpdateMovementToSchoolWithinSystem(employeeworkmovement);
            }
            else if (employeeMovementViewModel.MovementType == SystemParamsInFile.WORK_MOVEMENT_OTHER_ORGANIZATION)
            {
                employeeworkmovement.ToProvinceID = null;
                employeeworkmovement.ToDistrictID = null;
                employeeworkmovement.ToSchoolID = null;
                employeeworkmovement.MovementType = employeeMovementViewModel.MovementType;
                employeeworkmovement.TeacherID = employeeMovementViewModel.TeacherID;
                employeeworkmovement.EmployeeWorkMovementID = employeeMovementViewModel.EmployeeWorkMovementID != null ? employeeMovementViewModel.EmployeeWorkMovementID.Value : 0;
                employeeworkmovement.MoveTo = employeeMovementViewModel.MoveTo;
                employeeworkmovement.MovedDate = employeeMovementViewModel.MovedDateOther;
                employeeworkmovement.ResolutionDocument = employeeMovementViewModel.ResolutionDocumentOther;
                employeeworkmovement.Description = employeeMovementViewModel.DescriptionOther;
                this.EmployeeWorkMovementBusiness.UpdateMovementToOtherOrganization(employeeworkmovement);
            }
            this.EmployeeWorkMovementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EmployeeWorkMovementBusiness.DeleteMovement(new GlobalInfo().SchoolID.Value, id);
            this.EmployeeWorkMovementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EmployeeWorkMovementViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo g = new GlobalInfo();
            int SchoolID = g.SchoolID.HasValue ? g.SchoolID.Value : 0;
            IQueryable<EmployeeWorkMovement> query = this.EmployeeWorkMovementBusiness.SearchMovement(SearchInfo, SchoolID);
            IQueryable<EmployeeWorkMovementViewModel> lst = query.Select(o => new EmployeeWorkMovementViewModel
            {
                EmployeeWorkMovementID = o.EmployeeWorkMovementID,
                TeacherID = o.TeacherID,
                TeacherName = o.Employee != null ? o.Employee.FullName : string.Empty,
                TeacherCode = o.Employee != null ? o.Employee.EmployeeCode : string.Empty,
                SchoolFacultyID = o.Employee != null ? (o.Employee.SchoolFacultyID.HasValue ? o.Employee.SchoolFacultyID.Value : 0) : 0,
                SchoolFacultyName = o.Employee != null ? (o.Employee.SchoolFaculty != null ? o.Employee.SchoolFaculty.FacultyName : string.Empty) : string.Empty,
                FromSchoolID = o.FromSchoolID,
                Description = o.Description,
                ResolutionDocument = o.ResolutionDocument,
                MovementType = o.MovementType,
                ToSchoolID = o.ToSchoolID,
                TeacherShortName = o.Employee != null ? o.Employee.Name : string.Empty,
                ToSchoolName = o.SchoolProfile1 != null ? o.SchoolProfile1.SchoolName : string.Empty,
                ToSupervisingDeptID = o.ToSupervisingDeptID,
                ToProvinceID = o.ToProvinceID,
                ToProvinceName = o.Province != null ? o.Province.ProvinceName : string.Empty,
                ToDistrictID = o.ToDistrictID,
                ToDistrictName = o.District != null ? o.District.DistrictName : string.Empty,
                MoveTo = (SystemParamsInFile.WORK_MOVEMENT_WITHIN_SYSTEM == o.MovementType) ? (o.SchoolProfile1 != null ? o.SchoolProfile1.SchoolName : string.Empty) :
                (SystemParamsInFile.WORK_MOVEMENT_OTHER_ORGANIZATION == o.MovementType ? o.MoveTo : string.Empty),
                MovedDate = o.MovedDate,
                IsAccepted = o.IsAccepted
            }).OrderByDescending(o => o.MovedDate).ThenBy(o=>o.TeacherShortName).ThenBy(o=>o.TeacherName);

            return lst.ToList();
        }
    }
}





