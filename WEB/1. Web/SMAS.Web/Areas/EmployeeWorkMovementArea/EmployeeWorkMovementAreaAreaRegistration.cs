﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeWorkMovementArea
{
    public class EmployeeWorkMovementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeWorkMovementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeWorkMovementArea_default",
                "EmployeeWorkMovementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
