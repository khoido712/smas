﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LeavingReasonArea
{
    public class LeavingReasonAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LeavingReasonArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LeavingReasonArea_default",
                "LeavingReasonArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
