﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.LeavingReasonArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.LeavingReasonArea.Controllers
{
    public class LeavingReasonController : BaseController
    {        
        private readonly ILeavingReasonBusiness LeavingReasonBusiness;
		
		public LeavingReasonController (ILeavingReasonBusiness leavingreasonBusiness)
		{
			this.LeavingReasonBusiness = leavingreasonBusiness;
		}
		
		//
        // GET: /LeavingReason/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here

            IEnumerable<LeavingReasonViewModel> lst = this._Search(SearchInfo);
            ViewData[LeavingReasonConstants.LIST_LEAVINGREASON] = lst;
            return View();
        }

		//
        // GET: /LeavingReason/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
			//add search info
			//

            IEnumerable<LeavingReasonViewModel> lst = this._Search(SearchInfo);
            ViewData[LeavingReasonConstants.LIST_LEAVINGREASON] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            LeavingReason leavingreason = new LeavingReason();
            TryUpdateModel(leavingreason); 
            Utils.Utils.TrimObject(leavingreason);
            leavingreason.IsActive = true;

            this.LeavingReasonBusiness.Insert(leavingreason);
            this.LeavingReasonBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int LeavingReasonID)
        {
            LeavingReason leavingreason = this.LeavingReasonBusiness.Find(LeavingReasonID);
            TryUpdateModel(leavingreason);
            Utils.Utils.TrimObject(leavingreason);
            this.LeavingReasonBusiness.Update(leavingreason);
            this.LeavingReasonBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.LeavingReasonBusiness.Delete(id);
            this.LeavingReasonBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<LeavingReasonViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<LeavingReason> query = this.LeavingReasonBusiness.Search(SearchInfo);
            IQueryable<LeavingReasonViewModel> lst = query.Select(o => new LeavingReasonViewModel {               
						LeavingReasonID = o.LeavingReasonID,								
						Resolution = o.Resolution,								
						Description = o.Description,								
													
					
				
            });

            return lst.OrderBy(o=>o.Resolution).ToList();
        }        
    }
}





