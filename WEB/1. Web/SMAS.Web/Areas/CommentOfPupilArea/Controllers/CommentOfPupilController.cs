﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.CommentOfPupilArea;
using SMAS.Web.Areas.CommentOfPupilArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text.RegularExpressions;

namespace SMAS.Web.Areas.CommentOfPupilArea.Controllers
{
    public class CommentOfPupilController : BaseController
    {
        private readonly ICommentOfPupilBusiness CommentOfPupilBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IBookmarkedFunctionBusiness BookmarkedFunctionBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private const string SPACE = "\r\n";
        public CommentOfPupilController(ICommentOfPupilBusiness commentOfPupilBusiness,IAcademicYearBusiness academicYearBusiness,
         IClassProfileBusiness classProfileBusiness, IClassSubjectBusiness classSubjectBusiness, ISubjectCatBusiness subjectCatBusiness,
         IClassSupervisorAssignmentBusiness classSupervisorAssignmentBusiness, IUserAccountBusiness userAccountBusiness,
         ITeachingAssignmentBusiness teachingAssignmentBusiness, IBookmarkedFunctionBusiness bookmarkedFunctionBusiness,
         IPupilOfClassBusiness pupilOfClassBusiness, ILockRatedCommentPupilBusiness lockRatedCommentPupilBusiness,
         ISchoolProfileBusiness schoolProfileBusiness, IEmployeeBusiness employeeBusiness)
        {
            this.CommentOfPupilBusiness = commentOfPupilBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassSupervisorAssignmentBusiness = classSupervisorAssignmentBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.BookmarkedFunctionBusiness = bookmarkedFunctionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.EmployeeBusiness = employeeBusiness;
        }
        public ActionResult Index(int? ClassID)
        {
            this.SetViewData(ClassID);
            return View();
        }
        [HttpPost]
        public PartialViewResult GetSubjectPanel()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int OldSubjectID = SMAS.Business.Common.Utils.GetInt(Request["oldSubjectID"]);
            int semester = semesterid == null ? 0 : semesterid.Value;
            ViewData[CommentOfPupilConstants.ClassID] = classid.Value;
            bool ViewAll = false;

            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            if (classid.HasValue && semesterid.HasValue)
            {
                lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semesterid.Value, classid.Value, ViewAll).Where(u => semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                  join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                  where s.IsActive == true
                                  && s.IsApprenticeshipSubject == false
                                  select new SubjectCatBO()
                                  {
                                      SubjectCatID = s.SubjectCatID,
                                      DisplayName = s.DisplayName,
                                      IsCommenting = cs.IsCommenting,
                                      OrderInSubject = s.OrderInSubject
                                  }
                                  ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            ViewData["ListSubjectComment"] = lsClassSubject;
            ViewData[CommentOfPupilConstants.ShowList] = false;
            ViewData[CommentOfPupilConstants.OLD_SUBJECT_ID] = OldSubjectID;
            //ViewData[CommentOfPupilConstants.GVBM_TEACHER] = UtilsBusiness.HasSubjectTeacherPermission_BM(_globalInfo.UserAccountID, classid.Value, semester);
            ViewData[CommentOfPupilConstants.GVCN_TEACHER] = UtilsBusiness.HasChargeTeacherPemission(_globalInfo.UserAccountID, classid.Value);
            return PartialView("_ViewSubject");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadGrid(int ClassID,int SemesterID,int SubjectID)
        {
            ViewData[CommentOfPupilConstants.SEMESTER_ID] = SemesterID;
            bool isGVBM = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, ClassID,SubjectID);
            ViewData[CommentOfPupilConstants.GVCN_TEACHER] = UtilsBusiness.HasChargeTeacherPemission(_globalInfo.UserAccountID, ClassID);
            ViewData[CommentOfPupilConstants.GVBM_TEACHER] = isGVBM;
            SetVisbileButton(SemesterID, ClassID, SubjectID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID},
            };
            List<MonthComment> lstMonthComment = new List<MonthComment>();
            List<DayOfMonthComment> lstDayOfMonth = new List<DayOfMonthComment>();
            int MonthSelectID = 0;
            int DaySelectID = 0;
            this.SetMonthAndDayOfMonth(dic, SemesterID, ref lstMonthComment, ref lstDayOfMonth, ref MonthSelectID, ref DaySelectID);
            ViewData[CommentOfPupilConstants.LIST_MONTH] = lstMonthComment;
            ViewData[CommentOfPupilConstants.LIST_DAY_OF_MONTH] = lstDayOfMonth;
            ViewData[CommentOfPupilConstants.MONTH_SELECT_ID] = MonthSelectID;
            ViewData[CommentOfPupilConstants.DAY_SELECT_ID] = DaySelectID;
            ViewData[CommentOfPupilConstants.SubjectID] = SubjectID;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                ViewData[CommentOfPupilConstants.FROM_DATE] = objAca.FirstSemesterStartDate;
                ViewData[CommentOfPupilConstants.TO_DATE] = objAca.FirstSemesterEndDate;
            }
            else
            {
                ViewData[CommentOfPupilConstants.FROM_DATE] = objAca.SecondSemesterStartDate;
                ViewData[CommentOfPupilConstants.TO_DATE] = objAca.SecondSemesterEndDate;
            }
            return PartialView("_GridCommentOfPupil");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadGridView(int ClassID,int SemesterID,int SubjectID,int MonthID,int DayOfMonthID,string MonthName)
        {
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            SetVisbileButton(SemesterID, ClassID, SubjectID);
            ViewData[CommentOfPupilConstants.SEMESTER_ID] = SemesterID;
            List<CommentOfPupilViewModel> lstResult = new List<CommentOfPupilViewModel>();
            lstResult = this.GetDataComment(lstPOC, ClassID, SemesterID, SubjectID, MonthID, DayOfMonthID, MonthName);
            ViewData[CommentOfPupilConstants.LIST_RESULT] = lstResult;
            ViewData[CommentOfPupilConstants.DAY_OF_MONTH] = DayOfMonthID;
            ViewData[CommentOfPupilConstants.SubjectID] = SubjectID;
            ViewData[CommentOfPupilConstants.ClassID] = ClassID;
            ViewData[CommentOfPupilConstants.SEMESTER_ID] = SemesterID;
            ViewData[CommentOfPupilConstants.SUBJECT_NAME] = SubjectID > 0 ? SubjectCatBusiness.Find(SubjectID).DisplayName : "Tất cả các môn";
            DateTime DateTimeComment = new DateTime();
            if (DayOfMonthID > 0)
            {
                DateTimeComment = Convert.ToDateTime(DayOfMonthID.ToString() + "/" + MonthName);
                ViewData[CommentOfPupilConstants.FROM_DATE] = DateTimeComment.ToString("yyyy/MM/dd");
                ViewData[CommentOfPupilConstants.TO_DATE] = DateTimeComment.ToString("yyyy/MM/dd");
            }
            else
            {
                DateTimeComment = Convert.ToDateTime("01" + "/" + MonthName);
                int[] arrTime = MonthName.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToArray();
                int lastDayOfMonth = DateTime.DaysInMonth(arrTime[1], arrTime[0]);
                ViewData[CommentOfPupilConstants.FROM_DATE] = DateTimeComment.ToString("yyyy/MM/dd");
                ViewData[CommentOfPupilConstants.TO_DATE] = Convert.ToDateTime(lastDayOfMonth.ToString() + "/" + MonthName).ToString("yyyy/MM/dd");

            }
            return PartialView("_ViewGridComment");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadAddComment(int ClassID, string MonthName,int PupilID,int SemesterID)
        {
            int[] arrTime = MonthName.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToArray();
            List<DayOfMonthComment> lstDayOfMonth = new List<DayOfMonthComment>();
            DayOfMonthComment objDayOfMonth = null;
            int DayInMonth = DateTime.DaysInMonth(arrTime[1], arrTime[0]);
            int selectDayID = 0;
            DateTime DateTimeNow = DateTime.Now;
            if (DateTimeNow.Month > arrTime[0])
            {
                selectDayID = 1;
            }
            else if (DateTimeNow.Month == arrTime[0])
            {
                selectDayID = DateTimeNow.Day;
            }
            else
            {
                selectDayID = DayInMonth;
            }
            for (int i = 1; i <= DayInMonth; i++)
            {
                objDayOfMonth = new DayOfMonthComment();
                objDayOfMonth.DayID = i;
                objDayOfMonth.DayName = i.ToString();
                lstDayOfMonth.Add(objDayOfMonth);
            }
            ViewData[CommentOfPupilConstants.LIST_DAY_OF_MONTH] = new SelectList(lstDayOfMonth, "DayID", "DayName",selectDayID);
            //lay danh sach hoc sinh cua lop
            List<PupilCommentViewModel> lstPupil = (from poc in PupilOfClassBusiness.GetPupilInClass(ClassID).Where(p=>p.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                                                    select new PupilCommentViewModel
                                                    {
                                                        PupilID = poc.PupilID,
                                                        FullName = poc.PupilFullName,
                                                        OrderInClass = poc.OrderInClass,
                                                        Name = poc.Name
                                                    }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.FullName).ToList();
            ViewData[CommentOfPupilConstants.LIST_PUPIL] = lstPupil;
            ViewData[CommentOfPupilConstants.PUPIL_ID] = PupilID;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                ViewData[CommentOfPupilConstants.FROM_DATE] = objAca.FirstSemesterStartDate;
                ViewData[CommentOfPupilConstants.TO_DATE] = objAca.FirstSemesterEndDate;
            }
            else
            {
                ViewData[CommentOfPupilConstants.FROM_DATE] = objAca.SecondSemesterStartDate;
                ViewData[CommentOfPupilConstants.TO_DATE] = objAca.SecondSemesterEndDate;
            }
            return PartialView("_AddCommentPupil");
        }
        public JsonResult AjaxLoadCommentByPupil(int ClassID,int SubjectID,int SemesterID,int MonthID,int DayOfMonth,string MonthName,int PupilID)
        {
            DateTime DateTimeComment = Convert.ToDateTime(DayOfMonth.ToString() + "/" + MonthName);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID},
                {"DateTimeComment",DateTimeComment},
                {"PupilID",PupilID}
            };
            CommentOfPupil objCommentOfPupil = CommentOfPupilBusiness.Search(dic).FirstOrDefault();
            string CommentOfPupil = objCommentOfPupil != null ? objCommentOfPupil.Comment : "";
            return Json(new JsonMessage(CommentOfPupil, "success"));
        }
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public JsonResult SaveComentsPupil(FormCollection frm)
        {
            int ClassID = frm["ClassID"] != null ? int.Parse(frm["ClassID"]) : 0;
            int SubjectID = frm["SubjectID"] != null ? int.Parse(frm["SubjectID"]) : 0;
            int SemesterID = frm["Semester"] != null ? int.Parse(frm["semester"]) : 0;
            int MonthID = frm["MonthID"] != null ? int.Parse(frm["MonthID"]) : 0;
            string MonthName = frm["hdfMonthName"];
            int DayID = frm["DayID"] != null ? int.Parse(frm["DayID"]) : 0;
            DateTime DateTimeComment = Convert.ToDateTime(DayID.ToString() + "/" + MonthName);
            List<CommentOfPupilBO> lstCommentOfPupilBO = new List<CommentOfPupilBO>();
            CommentOfPupilBO objCommentOfPupilBO = null;
            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVBM = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, ClassID, SubjectID);
            if (!(isAdminShcool || isGVBM))
            {
                return Json(new JsonMessage(Res.Get("Comment_Of_Pupil_SaveError"), "error"));
            }
            //lay danh sach hoc sinh
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            PupilOfClassBO objPOC = null;
            int PupilID = 0;
            int LogChangID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                objCommentOfPupilBO = new CommentOfPupilBO();
                PupilID = objPOC.PupilID;
                if ("on".Equals(frm["chk_" + PupilID]))
                {
                    objCommentOfPupilBO.SchoolID = _globalInfo.SchoolID.Value;
                    objCommentOfPupilBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objCommentOfPupilBO.PupilID = PupilID;
                    objCommentOfPupilBO.ClassID = objPOC.ClassID;
                    objCommentOfPupilBO.SemesterID = SemesterID;
                    objCommentOfPupilBO.SubjectID = SubjectID;
                    objCommentOfPupilBO.MonthID = MonthID;
                    objCommentOfPupilBO.DateTimeComment = DateTimeComment;
                    objCommentOfPupilBO.Comment = frm["txtCommentID_" + PupilID];
                    objCommentOfPupilBO.LogChangeID = LogChangID;
                    lstCommentOfPupilBO.Add(objCommentOfPupilBO);
                }
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID},
                {"MonthID",MonthID},
                {"DateTimeComment",DateTimeComment}
            };
            CommentOfPupilBusiness.InsertOrUpdateComment(lstCommentOfPupilBO, dic);
            return Json(new JsonMessage(Res.Get("success_Save"), "success"));
        }
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public JsonResult AddCommentPupil(FormCollection frm)
        {
            int ClassID = frm["ClassID"] != null ? int.Parse(frm["ClassID"]) : 0;
            int SubjectID = frm["SubjectID"] != null ? int.Parse(frm["SubjectID"]) : 0;
            int SemesterID = frm["Semester"] != null ? int.Parse(frm["semester"]) : 0;
            int MonthID = frm["hdfMonthID"] != null ? int.Parse(frm["hdfMonthID"]) : 0;
            string MonthName = frm["hdfMonthNameAdd"];
            int PupilID = frm["PupilID"] != null ? int.Parse(frm["PupilID"]) : 0;
            int DayID = frm["DayAddID"] != null ? int.Parse(frm["DayAddID"]) : 0;
            DateTime DateTimeComment = Convert.ToDateTime(DayID.ToString() + "/" + MonthName);
            List<CommentOfPupilBO> lstCommentOfPupilBO = new List<CommentOfPupilBO>();
            CommentOfPupilBO objCommentOfPupilBO = new CommentOfPupilBO();
            objCommentOfPupilBO.SchoolID = _globalInfo.SchoolID.Value;
            objCommentOfPupilBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
            objCommentOfPupilBO.PupilID = PupilID;
            objCommentOfPupilBO.ClassID = ClassID;
            objCommentOfPupilBO.SemesterID = SemesterID;
            objCommentOfPupilBO.SubjectID = SubjectID;
            objCommentOfPupilBO.MonthID = MonthID;
            objCommentOfPupilBO.DateTimeComment = DateTimeComment;
            objCommentOfPupilBO.Comment = frm["CommentAddID"];
            lstCommentOfPupilBO.Add(objCommentOfPupilBO);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID},
                {"MonthID",MonthID},
                {"DateTimeComment",DateTimeComment}
            };
            CommentOfPupilBusiness.InsertOrUpdateComment(lstCommentOfPupilBO, dic);
            return Json(new JsonMessage(Res.Get("success_Save"), "success"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCommentOfPupil(string ArrPupilDelete,int ClassID,int SemesterID,int SubjectID,int MonthID,int DayOfMonthID,string MonthName)
        {
            List<int> lstPupilID = ArrPupilDelete.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID},
                {"MonthID",MonthID},
                {"lstPupilID",lstPupilID}
            };
            DateTime DateTimeComment = new DateTime();
            if (DayOfMonthID > 0)
            {
                DateTimeComment = Convert.ToDateTime((DayOfMonthID + "/" + MonthName));
                dic.Add("DateTimeComment", DateTimeComment);
            }
            CommentOfPupilBusiness.DeleteComment(dic);
            return Json(new JsonMessage("Xóa thành công"));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddMenuUsual(string url)
        {
            Menu objMenu = SMAS.Business.Common.StaticData.ListMenu.Where(c => c.URL.Contains(url)).FirstOrDefault();

            List<BookmarkedFunction> lstBm = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.GetValueOrDefault(),
                _globalInfo.AcademicYearID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID);

            BookmarkedFunction objDB = lstBm.Where(p => p.MenuID == objMenu.MenuID).FirstOrDefault();

            if (objDB == null)
            {
                BookmarkedFunction bf = new BookmarkedFunction();
                bf.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bf.AcademicYearID = _globalInfo.AcademicYearID;
                bf.LevelID = _globalInfo.AppliedLevel;
                bf.UserID = _globalInfo.UserAccountID;
                bf.MenuID = objMenu.MenuID;
                bf.Semester = _globalInfo.Semester;
                string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", objMenu.MenuName);
                bf.FunctionName = string.IsNullOrEmpty(name) ? objMenu.MenuName : name;
                bf.Order = lstBm.Count + 1;
                bf.CreatedDate = DateTime.Now;

                BookmarkedFunctionBusiness.Insert(bf);
                BookmarkedFunctionBusiness.Save();
            }
            return Json(new JsonMessage("Đã thêm vào danh sách chức năng thường dùng.", "success"));
        }
        public JsonResult AjaxLoadDayOfMonth(string MonthName)
        {
            int[] arrTime = MonthName.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToArray();
            List<DayOfMonthComment> lstDayOfMonth = new List<DayOfMonthComment>();
            DayOfMonthComment objDayOfMonth = null;
            int DayInMonth = DateTime.DaysInMonth(arrTime[1], arrTime[0]);
            int selectDayID = 0;
            DateTime DateTimeNow = DateTime.Now;
            if (arrTime[0] == DateTimeNow.Month && arrTime[1] == DateTimeNow.Year)
            {
                selectDayID = DateTimeNow.Day;
            }
            for (int i = 1; i <= DayInMonth; i++)
            {
                objDayOfMonth = new DayOfMonthComment();
                objDayOfMonth.DayID = i;
                objDayOfMonth.DayName = i.ToString();
                lstDayOfMonth.Add(objDayOfMonth);
            }
            return Json(new SelectList(lstDayOfMonth, "DayID", "DayName",selectDayID));
        }
        #region Import Export
        public FileResult ExportComment(int ClassID,int SemesterID,int SubjectID,int MonthID,int DayOfMonthID,string MonthName)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", CommentOfPupilConstants.TEMPLATE_FILE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            PupilOfClassBO objPOC = null;
            List<CommentOfPupilViewModel> lstResult = new List<CommentOfPupilViewModel>();
            CommentOfPupilViewModel objViewModel = null;
            lstResult = this.GetDataComment(lstPOC, ClassID, SemesterID, SubjectID, MonthID, DayOfMonthID, MonthName);

            //fill thong tin chung
            string suppervisingDeptName = "";
            if (_globalInfo.EmployeeID > 0)
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                suppervisingDeptName = objSP.SupervisingDept.SupervisingDeptName.ToUpper();
            }
            else
            {
                suppervisingDeptName = UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value).ToUpper();
            }
            sheet.SetCellValue("A2", suppervisingDeptName);
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            string Title = string.Format("SỔ NHẬN XÉT - {0}", objCP.DisplayName).ToUpper();
            sheet.SetCellValue("A5", Title);
            sheet.SetCellValue("E9", SubjectID);//bien de check khi import
            sheet.SetCellValue("F9", "COP");//bien de check file khi import
            string TimeName = string.Empty;
            string fromDate = "";
            string toDate = "";
            int[] arrTime = MonthName.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToArray();
            int lastDayOfMonth = DateTime.DaysInMonth(arrTime[1], arrTime[0]);
            toDate = lastDayOfMonth.ToString() + "/" + MonthName;
            if (DayOfMonthID == 0)
            {
                fromDate = "01" + "/" + MonthName;
                TimeName = string.Format("Từ ngày {0} đến {1}", fromDate, toDate);
            }
            else
            {
                fromDate = (DayOfMonthID < 10 ? "0" + DayOfMonthID.ToString() : DayOfMonthID.ToString()) + "/" + MonthName;
                TimeName = string.Format("Ngày {0}", fromDate);
            }
            
            sheet.SetCellValue("A6", TimeName);

            int startRow = 10;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objViewModel = lstResult.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                sheet.SetCellValue(startRow, 1, (i + 1));//STT
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);
                sheet.SetCellValue(startRow, 4, objViewModel != null ? objViewModel.Comments : "");
                sheet.GetRange(startRow, 4, startRow, 4).IsLock = false;
                if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, 4).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, 1, startRow, 4).IsLock = true;
                }
                startRow++;
            }
            if (lstPOC.Count > 0)
            {
                sheet.GetRange(10, 1, 10 + lstPOC.Count - 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            sheet.Name = SubjectID > 0 ? SMAS.Business.Common.Utils.StripVNSignAndSpace(objSC.DisplayName) : "TatCa";
            sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = string.Empty;
            if (SubjectID > 0)
            {
                fileName = String.Format("SoNhanXet_{0}_{1}.xls", SMAS.Business.Common.Utils.StripVNSignAndSpace(objCP.DisplayName), SMAS.Business.Common.Utils.StripVNSignAndSpace(objSC.DisplayName));   
            }
            else
            {
                fileName = String.Format("SoNhanXet_{0}.xls", SMAS.Business.Common.Utils.StripVNSignAndSpace(objCP.DisplayName));   
            }
            result.FileDownloadName = fileName;
            return result;
        }
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcelComment(IEnumerable<HttpPostedFileBase> attachments,int ClassID,int SubjectID,int SemesterID,string DateTimeImport)
        {
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 1024)
                {
                    return Json(new JsonMessage(Res.Get("CommentOfPupil_File_Max_Size"), "error"));
                }
                string FileCode = sheet.GetCellValue("F9") != null ? sheet.GetCellValue("F9").ToString() : "";
                if (!"COP".Equals(FileCode))
                {
                    return Json(new JsonMessage(Res.Get("FileError_Label_Import"), "error"));
                }

                SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
                int SubjectIDFile = sheet.GetCellValue("E9") != null ? int.Parse(sheet.GetCellValue("E9").ToString()) : 0;
                if (SubjectIDFile != SubjectID)
                {
                    return Json(new JsonMessage(Res.Get("CommentOfPupil_Validate_Subject") + objSC.DisplayName, "error"));
                }
                List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                PupilOfClassBO objPOC = null;
                List<CommentOfPupilBO> lstCommentOfPupilBO = new List<CommentOfPupilBO>();
                CommentOfPupilBO objCommentOfPupilBO = null;
                int startRow = 10;
                string pupilCode = string.Empty;
                int pupilID = 0;
                DateTime DateTimeComment = Convert.ToDateTime(DateTimeImport);
                string strMonth = DateTimeComment.Month.ToString() + DateTimeComment.Year.ToString();
                int MonthID = int.Parse(strMonth);
                string strComment = string.Empty;
                string TitleDateComment = sheet.GetCellValue("A6").ToString();
                int LogChangID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
                if (!TitleDateComment.Contains(DateTimeImport))
                {
                    return Json(new JsonMessage(Res.Get("CommentOfPupil_Validate_DateComment") + DateTimeImport, "error"));
                }
                while (sheet.GetCellValue(startRow,2) != null && sheet.GetCellValue(startRow,3) != null)
                {
                    objCommentOfPupilBO = new CommentOfPupilBO();
                    pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                    objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                    if (objPOC != null)
                    {
                        pupilID = objPOC.PupilID;
                        if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                        {
                            startRow++;
                            continue;
                        }
                    }
                    else
                    {
                        return Json(new JsonMessage(Res.Get("FileError_Label_Import"), "error"));
                    }
                    strComment = sheet.GetCellValue(startRow,4) != null ? sheet.GetCellValue(startRow,4).ToString() : "";
                    if (strComment.Length > 500)
                    {
                        return Json(new JsonMessage(Res.Get("Comment_Of_Pupil_CommentErr"), "error"));
                    }
                    if (strComment.Contains("<") || strComment.Contains(">"))
                    {
                        return Json(new JsonMessage(Res.Get("Common_Validate_XSS"), "error"));
                    }   
                    objCommentOfPupilBO.SchoolID = _globalInfo.SchoolID.Value;
                    objCommentOfPupilBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objCommentOfPupilBO.PupilID = pupilID;
                    objCommentOfPupilBO.ClassID = objPOC.ClassID;
                    objCommentOfPupilBO.SemesterID = SemesterID;
                    objCommentOfPupilBO.SubjectID = SubjectID;
                    objCommentOfPupilBO.MonthID = MonthID;
                    objCommentOfPupilBO.DateTimeComment = DateTimeComment;
                    objCommentOfPupilBO.Comment = strComment;
                    objCommentOfPupilBO.LogChangeID = LogChangID;
                    lstCommentOfPupilBO.Add(objCommentOfPupilBO);
                    startRow++;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SubjectID",SubjectID},
                    {"SemesterID",SemesterID},
                    {"MonthID",MonthID},
                    {"DateTimeComment",DateTimeComment}
                };
                CommentOfPupilBusiness.InsertOrUpdateComment(lstCommentOfPupilBO, dic);
            }
            return Json(new JsonMessage(Res.Get("Comment_Of_Pupil_ImportSuccess"), "success"));
        }
        #endregion
        private List<CommentOfPupilViewModel> GetDataComment(List<PupilOfClassBO> lstPOC, int ClassID, int SemesterID, int SubjectID, int MonthID, int DayOfMonthID, string MonthName)
        {
            List<CommentOfPupilViewModel> lstResult = new List<CommentOfPupilViewModel>();
            PupilOfClassBO objPOC = null;
            DateTime DateTimeComment = new DateTime();
            //lay danh sanh nhan xet
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID},
                {"MonthID",MonthID},
            };
            if (DayOfMonthID > 0)
            {
                DateTimeComment = Convert.ToDateTime((DayOfMonthID + "/" + MonthName));
                dic.Add("DateTimeComment", DateTimeComment);
            }
            List<CommentOfPupilBO> lstCommentOfPupil = new List<CommentOfPupilBO>();
            List<CommentOfPupilBO> lsttmp = null;
            CommentOfPupilBO objCommentOfPupil = null;
            lstCommentOfPupil = (from cop in CommentOfPupilBusiness.Search(dic)
                                 join sc in SubjectCatBusiness.All on cop.SubjectID equals sc.SubjectCatID
                                 select new CommentOfPupilBO
                                 {
                                     PupilID = cop.PupilID,
                                     ClassID = cop.ClassID,
                                     SubjectID = cop.SubjectID,
                                     SubjectName = sc.DisplayName,
                                     DateTimeComment = cop.DateTimeComment,
                                     MonthID = cop.MonthID,
                                     Comment = cop.Comment,
                                     OrderInSubject = sc.OrderInSubject,
                                     CreateTime = cop.CreateTime,
                                     UpdateTime = cop.UpdateTime,
                                     LogChangeID = cop.LogChangeID
                                 }).ToList();
            CommentOfPupilViewModel objResult = null;
            string CommentOfPupils = string.Empty;
            DateTime MaxDate = new DateTime();
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                CommentOfPupils = string.Empty;
                objResult = new CommentOfPupilViewModel();
                if (DayOfMonthID > 0)
                {
                    if (SubjectID == 0)
                    {
                        lsttmp = lstCommentOfPupil.Where(p => p.PupilID == objPOC.PupilID).OrderBy(p=>p.OrderInSubject).ToList();
                        if (lsttmp.Count > 0)
                        {
                            MaxDate = lsttmp.Max(p => p.UpdateTime.HasValue ? p.UpdateTime.Value : p.CreateTime);    
                        }
                        for (int j = 0; j < lsttmp.Count; j++)
                        {
                            objCommentOfPupil = lsttmp[j];
                            if (!string.IsNullOrEmpty(objCommentOfPupil.Comment))
                            {
                                CommentOfPupils += "[" + objCommentOfPupil.SubjectName + "]: " + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                            }
                            objResult.CreateTime = MaxDate;
                            objResult.UpdateTime = MaxDate;
                        }
                    }
                    else
                    {
                        objCommentOfPupil = lstCommentOfPupil.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                        if (objCommentOfPupil != null)
                        {
                            objResult.CreateTime = objCommentOfPupil.CreateTime;
                            objResult.UpdateTime = objCommentOfPupil.UpdateTime;
                            CommentOfPupils = objCommentOfPupil.Comment;
                            objResult.LogChangeID = objCommentOfPupil.LogChangeID;
                        }
                    }
                    
                }
                else
                {
                    lsttmp = lstCommentOfPupil.Where(p => p.PupilID == objPOC.PupilID).OrderBy(p=>p.DateTimeComment).ThenBy(p=>p.OrderInSubject).ToList();
                    if (lsttmp.Count > 0)
                    {
                        MaxDate = lsttmp.Max(p => p.UpdateTime.HasValue ? p.UpdateTime.Value : p.CreateTime);
                    }
                    for (int j = 0; j < lsttmp.Count; j++)
                    {
                        objCommentOfPupil = lsttmp[j];
                        if (!string.IsNullOrEmpty(objCommentOfPupil.Comment))
                        {
                            if (SubjectID == 0)
                            {
                                CommentOfPupils += "[" + objCommentOfPupil.DateTimeComment.ToString("dd/MM/yyyy") + "][" + objCommentOfPupil.SubjectName + "]: " + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                            }
                            else
                            {
                                CommentOfPupils += "[" + objCommentOfPupil.DateTimeComment.ToString("dd/MM/yyyy") + "]: " + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                objResult.LogChangeID = objCommentOfPupil.LogChangeID;
                            }
                        }
                        objResult.CreateTime = MaxDate;
                        objResult.UpdateTime = MaxDate;
                    }
                }
                objResult.PupilID = objPOC.PupilID;
                objResult.ClassID = objPOC.ClassID;
                objResult.FullName = objPOC.PupilFullName;
                objResult.Status = objPOC.Status;
                objResult.Comments = CommentOfPupils;
                lstResult.Add(objResult);
            }


            List<int> lstEmployeeID = lstCommentOfPupil.Where(p => p.LogChangeID > 0).Select(p => p.LogChangeID).Distinct().ToList();
            var lastUpdate = lstCommentOfPupil.Where(p=>!string.IsNullOrEmpty(p.Comment)).OrderByDescending(o => o.UpdateTime).FirstOrDefault();
            List<Employee> lstEmployeeChange = new List<Employee>();
            if (lstEmployeeID.Count > 0)
            {
                lstEmployeeChange = (from e in EmployeeBusiness.All
                                     where e.SchoolID == _globalInfo.SchoolID
                                     && lstEmployeeID.Contains(e.EmployeeID)
                                     select e).ToList();
            }
            Employee lastUser = null;
            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            List<int> lstLastUpdatePupilID = new List<int>();
            if (lastUpdate != null)
            {
                string lastUserName = String.Empty;
                if (lastUpdate.LogChangeID == 0)
                {
                    lastUserName = Res.Get("Message_Label_GroupUserSchoolAdmin");
                }
                else
                {
                    lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdate.LogChangeID).FirstOrDefault();
                    if (lastUser != null)
                    {
                        lastUserName = lastUser.FullName;
                    }
                }
                if (lastUpdate.UpdateTime.HasValue)
                {
                    tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                        lastUpdate.UpdateTime.Value.Hour.ToString(),
                        lastUpdate.UpdateTime.Value.Minute.ToString("D2"),
                        lastUpdate.UpdateTime,
                        (!String.IsNullOrEmpty(lastUserName) && SubjectID > 0)?"bởi ":String.Empty,
                        (!String.IsNullOrEmpty(lastUserName) && SubjectID > 0)?lastUserName:String.Empty});
                }
                else
                {
                    tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                        lastUpdate.CreateTime.Hour.ToString(),
                        lastUpdate.CreateTime.Minute.ToString("D2"),
                        lastUpdate.CreateTime,
                        (!String.IsNullOrEmpty(lastUserName) && SubjectID > 0)?"bởi ":String.Empty,
                        (!String.IsNullOrEmpty(lastUserName) && SubjectID > 0)?lastUserName:String.Empty});
                }



                strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);

                string lastUpdateTime = lastUpdate.UpdateTime != null ? lastUpdate.UpdateTime.Value.ToString("dd/MM/yyyy hh:mm") : lastUpdate.CreateTime.ToString("dd/MM/yyyy hh:mm");
                lstLastUpdatePupilID = lstResult.Where(p => (p.UpdateTime.HasValue ? (p.UpdateTime.Value.ToString("dd/MM/yyyy hh:mm") == lastUpdateTime && !string.IsNullOrEmpty(p.Comments)) 
                                                                                    : (p.CreateTime.ToString("dd/MM/yyyy hh:mm") == lastUpdateTime && !string.IsNullOrEmpty(p.Comments))))
                                        .Select(p => p.PupilID).Distinct().ToList();
            }
            ViewData[CommentOfPupilConstants.LAST_UPDATE] = lstLastUpdatePupilID;
            ViewData[CommentOfPupilConstants.LAST_UPDATE_STRING] = strLastUpdate;
            return lstResult;
        }
        private void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                    semester1 = true;
                else semester2 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[CommentOfPupilConstants.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[CommentOfPupilConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[CommentOfPupilConstants.LIST_CLASS] = listClass;
                var listCP = listClass.ToList();
                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            // Ưu tiên trước đối với giáo viên bộ môn
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                            List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                            {
                                cp = listSubjectTeacher.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                        .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listHead != null && listHead.Count > 0)
                                {
                                    cp = listHead.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            //lay list mon hoc
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (cp != null)
            {
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && cp.ClassProfileID > 0)
                {
                    lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, _globalInfo.Semester.Value, cp.ClassProfileID, _globalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                        //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .ToList();
                }
            }
            else
            {
                ViewData[CommentOfPupilConstants.CLASS_NULL] = true;
            }

            ViewData[CommentOfPupilConstants.SubjectID] = lsClassSubject.Select(c => c.SubjectID).FirstOrDefault();
        }
        public PartialViewResult AjaxViewImport(int SemesterID)
        {
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                ViewData[CommentOfPupilConstants.FROM_DATE] = objAca.FirstSemesterStartDate;
                ViewData[CommentOfPupilConstants.TO_DATE] = objAca.FirstSemesterEndDate;
            }
            else
            {
                ViewData[CommentOfPupilConstants.FROM_DATE] = objAca.SecondSemesterStartDate;
                ViewData[CommentOfPupilConstants.TO_DATE] = objAca.SecondSemesterEndDate;
            }
            return PartialView("_ViewImportExcel");
        }
        private void SetVisbileButton(int semester, int classID, int subjectID)
        {
            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVBM = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, classID, subjectID);
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (!(isAdminShcool || isGVBM))
            {
                ViewData[CommentOfPupilConstants.IS_VISIBLED_BUTTON] = false;
            }
            else if (isAdminShcool)
            {
                ViewData[CommentOfPupilConstants.IS_VISIBLED_BUTTON] = true;
            }
            else if (isGVBM)
            {
                if (semester == 1)
                {
                    if (dateTimeNow < objAca.FirstSemesterStartDate.Value || dateTimeNow > objAca.FirstSemesterEndDate)
                    {
                        ViewData[CommentOfPupilConstants.IS_VISIBLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[CommentOfPupilConstants.IS_VISIBLED_BUTTON] = true;
                    }
                }
                else if (semester == 2)
                {
                    if (dateTimeNow < objAca.SecondSemesterStartDate.Value || dateTimeNow > objAca.SecondSemesterEndDate.Value)
                    {
                        ViewData[CommentOfPupilConstants.IS_VISIBLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[CommentOfPupilConstants.IS_VISIBLED_BUTTON] = true;
                    }
                }
            }
        }
        private void SetMonthAndDayOfMonth(IDictionary<string,object> dic,int SemesterID,ref List<MonthComment> lstMonthComment,ref List<DayOfMonthComment> lstDayOfMonth,ref int MonthSelectID,ref int DaySelectID)
        {
            #region Tinh ra thang
            IQueryable<CommentOfPupil> iqCommentOfPupil = CommentOfPupilBusiness.Search(dic).Where(p=>!string.IsNullOrEmpty(p.Comment));
            MonthComment objMonthComment = null;
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime DateTimeNow = DateTime.Now;
            List<DateTime> lstDateTime = new List<DateTime>();
            DateTime MinDate = new DateTime();
            DateTime MaxDate = new DateTime();
            DateTime Datetmp = new DateTime();
            string MonthID = string.Empty;
            if (DateTimeNow < objAy.FirstSemesterStartDate)
            {
                if (iqCommentOfPupil.Count() > 0)
                {
                    MinDate = iqCommentOfPupil.Min(p => p.DateTimeComment);
                    MaxDate = iqCommentOfPupil.Max(p => p.DateTimeComment);
                    if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (MinDate > objAy.FirstSemesterStartDate)
                        {
                            MinDate = objAy.FirstSemesterStartDate.Value;
                        }
                        if (MaxDate < objAy.FirstSemesterStartDate)
                        {
                            MaxDate = objAy.FirstSemesterStartDate.Value;
                        }
                    }
                    else
                    {
                        if (MinDate > objAy.SecondSemesterStartDate)
                        {
                            MinDate = objAy.SecondSemesterStartDate.Value;
                        }
                        if (MaxDate < objAy.SecondSemesterStartDate)
                        {
                            MaxDate = objAy.SecondSemesterStartDate.Value;
                        }
                    }
                    
                }
                else
                {
                    MinDate = MaxDate = objAy.FirstSemesterStartDate.Value;
                }
                while (MinDate.StartOfMonth() < MaxDate.EndOfMonth())
                {
                    objMonthComment = new MonthComment();
                    MonthID = ((MinDate.Month < 10 ? ("0" + MinDate.Month.ToString()) : MinDate.Month.ToString()) + MinDate.Year.ToString());
                    objMonthComment.DateTime = MinDate;
                    objMonthComment.MonthID = int.Parse(MonthID);
                    objMonthComment.MonthName = (MinDate.Month < 10 ? ("0" + MinDate.Month.ToString()) : MinDate.Month.ToString()) + "/" + MinDate.Year;
                    if (lstMonthComment.Where(p => p.MonthID == objMonthComment.MonthID).Count() == 0)
                    {
                        lstMonthComment.Add(objMonthComment);
                    }
                    if (MinDate.Month == DateTimeNow.Month)
                    {
                        MonthSelectID = objMonthComment.MonthID;
                    }
                    MinDate = MinDate.AddMonths(1);
                }
            }
            else if ((objAy.FirstSemesterStartDate <= DateTimeNow && DateTimeNow <= objAy.FirstSemesterEndDate)
                    || objAy.SecondSemesterStartDate <= DateTimeNow && DateTimeNow <= objAy.SecondSemesterEndDate)
            {
                MaxDate = DateTime.Now;
                if (iqCommentOfPupil.Count() > 0)
                {
                    MinDate = iqCommentOfPupil.Min(p => p.DateTimeComment);
                    if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (MinDate > objAy.FirstSemesterStartDate)
                        {
                            MinDate = objAy.FirstSemesterStartDate.Value;
                        }
                    }
                    else
                    {
                        if (MinDate > objAy.SecondSemesterStartDate)
                        {
                            MinDate = objAy.SecondSemesterStartDate.Value;
                        }
                    }
                    
                }
                else
                {
                    MinDate = objAy.FirstSemesterStartDate.Value;
                    if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        MinDate = objAy.FirstSemesterStartDate.Value;
                    }
                    else
                    {
                        MinDate = objAy.SecondSemesterStartDate.Value;
                    }
                }

                while (MinDate.StartOfMonth() < MaxDate.EndOfMonth())
                {
                    objMonthComment = new MonthComment();
                    MonthID = ((MinDate.Month < 10 ? ("0" + MinDate.Month.ToString()) : MinDate.Month.ToString()) + MinDate.Year.ToString());
                    objMonthComment.DateTime = MinDate;
                    objMonthComment.MonthID = int.Parse(MonthID);
                    objMonthComment.MonthName = (MinDate.Month < 10 ? ("0" + MinDate.Month.ToString()) : MinDate.Month.ToString()) + "/" + MinDate.Year;
                    if (lstMonthComment.Where(p => p.MonthID == objMonthComment.MonthID).Count() == 0)
                    {
                        lstMonthComment.Add(objMonthComment);
                    }
                    if (MinDate.Month == DateTimeNow.Month)
                    {
                        MonthSelectID = objMonthComment.MonthID;
                    }
                    MinDate = MinDate.AddMonths(1);
                }
            }
            else if (DateTimeNow > objAy.SecondSemesterEndDate)
            {
                if (iqCommentOfPupil.Count() > 0)
                {
                    MinDate = iqCommentOfPupil.Min(p => p.DateTimeComment);
                    MaxDate = iqCommentOfPupil.Max(p => p.DateTimeComment);
                    if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (MinDate > objAy.FirstSemesterStartDate)
                        {
                            MinDate = objAy.FirstSemesterStartDate.Value;
                        }
                        if (MaxDate < objAy.FirstSemesterEndDate)
                        {
                            MaxDate = objAy.FirstSemesterEndDate.Value;
                        }
                    }
                    else
                    {
                        if (MinDate > objAy.SecondSemesterStartDate)
                        {
                            MinDate = objAy.SecondSemesterStartDate.Value;
                        }
                        if (MaxDate < objAy.SecondSemesterEndDate)
                        {
                            MaxDate = objAy.SecondSemesterEndDate.Value;
                        }
                    }
                }
                else
                {
                    if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        MinDate = objAy.FirstSemesterStartDate.Value;
                        MaxDate = objAy.FirstSemesterEndDate.Value;
                    }
                    else
                    {
                        MinDate = objAy.SecondSemesterStartDate.Value;
                        MaxDate = objAy.SecondSemesterEndDate.Value;
                    }
                }

                while (MinDate.StartOfMonth() < MaxDate.EndOfMonth())
                {
                    objMonthComment = new MonthComment();
                    MonthID = ((MinDate.Month < 10 ? ("0" + MinDate.Month.ToString()) : MinDate.Month.ToString()) + MinDate.Year.ToString());
                    objMonthComment.DateTime = MinDate;
                    objMonthComment.MonthID = int.Parse(MonthID);
                    objMonthComment.MonthName = (MinDate.Month < 10 ? ("0" + MinDate.Month.ToString()) : MinDate.Month.ToString()) + "/" + MinDate.Year;
                    if (lstMonthComment.Where(p => p.MonthID == objMonthComment.MonthID).Count() == 0)
                    {
                        lstMonthComment.Add(objMonthComment);
                    }
                    if (MinDate.Month == DateTimeNow.Month)
                    {
                        MonthSelectID = objMonthComment.MonthID;
                    }
                    MinDate = MinDate.AddMonths(1);
                }
            }
            #endregion
            #region Ngay cua thang
            DayOfMonthComment objDayOfMonth = null;
            Datetmp = lstMonthComment.Count > 0 ? lstMonthComment.Max(p => p.DateTime) : DateTime.Now;
            int DayInMonth = DateTime.DaysInMonth(Datetmp.Year, Datetmp.Month);
            int MonthNow = int.Parse((DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString()));
            if (MonthNow == MonthSelectID)
            {
                DaySelectID = DateTime.Now.Day;
            }
            for (int i = 1; i <= DayInMonth; i++)
            {
                objDayOfMonth = new DayOfMonthComment();
                objDayOfMonth.DayID = i;
                objDayOfMonth.DayName = i.ToString();
                lstDayOfMonth.Add(objDayOfMonth);
            }
            #endregion
        }
        public class MonthComment
        {
            public DateTime DateTime { get; set; }
            public int MonthID { get; set; }
            public string MonthName { get; set; }
        }
        public class DayOfMonthComment
        {
            public int DayID { get; set; }
            public string DayName { get; set; }
        }
    }
}