﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.CommentOfPupilArea
{
    public class CommentOfPupilConstants
    {
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string CLASS_NULL = "classnull";
        public const string ClassID = "ClassID";
        public const string SubjectID = "SubjectID";
        public const string SUBJECT_NAME = "SubjectName";
        public const string DAY_OF_MONTH = "DayOfMonth";
        public const string MONTH_SELECT_ID = "MonthSelectID";
        public const string DAY_SELECT_ID = "DaySelectID";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_SEMESTER = "listSemester";
        public const string ShowList = "ShowList";
        public const string OLD_SUBJECT_ID = "OldSubjectID";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string IS_VISIBLED_BUTTON = "IsVisibledButton";
        public const string HEAD_TEACHE = "HeadTeacher";
        public const string GVBM_TEACHER = "GVBMTeacher";
        public const string GVCN_TEACHER = "GVCNTeacher";
        public const string SEMESTER_ID = "SemesterID";
        public const string LAST_UPDATE = "LAST_UPDATE";
        public const string LIST_RESULT = "ListResult";
        public const string LIST_MONTH = "ListMonth";
        public const string LIST_DAY_OF_MONTH = "ListDayOfMonth";
        public const string LIST_PUPIL = "ListPupil";
        public const string TEMPLATE_FILE = "SoGhiNhanXet.xls";
        public const string FROM_DATE = "FromDate";
        public const string TO_DATE = "ToDate";
        public const string PUPIL_ID = "PupilID";
    }
}