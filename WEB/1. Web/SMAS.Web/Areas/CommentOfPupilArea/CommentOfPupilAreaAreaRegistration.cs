﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.CommentOfPupilArea
{
    public class CommentOfPupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CommentOfPupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CommentOfPupilArea_default",
                "CommentOfPupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
