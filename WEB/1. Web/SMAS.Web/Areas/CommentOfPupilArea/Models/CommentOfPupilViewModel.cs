﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.CommentOfPupilArea.Models
{
    public class CommentOfPupilViewModel
    {
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public int Status { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public string Comments { get; set; }
        public int LogChangeID { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}