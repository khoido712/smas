﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.CommentOfPupilArea.Models
{
    public class PupilCommentViewModel
    {
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }
    }
}