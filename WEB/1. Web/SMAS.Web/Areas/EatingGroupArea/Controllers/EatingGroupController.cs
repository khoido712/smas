﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.EatingGroupArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc.UI;

namespace SMAS.Web.Areas.EatingGroupArea.Controllers
{
    public class EatingGroupController : BaseController
    {
        private readonly IEatingGroupBusiness EatingGroupBusiness;
        private readonly IEatingGroupClassBusiness EatingGroupClassBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IDailyMenuBusiness DailyMenuBusiness;
        private readonly INutritionalNormBusiness NutritionalNormBusiness;
        private int ID_OF_GRADE = -1;
        private int ID_OF_EDUCATIONLEVEL = -2;
        private int ID_LOAD_CREATE = 1;
        private int ID_LOAD_EDIT = 2;
        public EatingGroupController(IEducationLevelBusiness EducationLevelBusiness, IEatingGroupBusiness eatinggroupBusiness, IClassProfileBusiness ClassProfileBusiness, IEatingGroupClassBusiness EatingGroupClassBusiness, IDailyMenuBusiness DailyMenuBusiness, INutritionalNormBusiness NutritionalNormBusiness)
        {
            this.EatingGroupBusiness = eatinggroupBusiness;
            this.EatingGroupClassBusiness = EatingGroupClassBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DailyMenuBusiness = DailyMenuBusiness;
            this.NutritionalNormBusiness = NutritionalNormBusiness;
        }

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            IEnumerable<EatingGroupViewModel> lst = this._Search(SearchInfo);
            ViewData[EatingGroupConstants.LIST_EATINGGROUP] = lst.ToList();
            ViewData[SMAS.Web.Areas.EatingGroupArea.EatingGroupConstants.LT_TREE] = new List<EatingGroupTreeViewModel>();
            ViewData[SMAS.Web.Areas.EatingGroupArea.EatingGroupConstants.LIST_CLASS] = new List<EatingGroupTreeViewModel>();
            return View();
        }
        //Load cap hoc.Chi gom cap mau giao va cap nha tre. Mam non bao gom ca 2
        //Cap mau giao: AppliedLevel = 4,  Nha tre: 5

        //editOrCreateId: Neu la load create thi = ID_LOAD_CREATE; Neu la load edit thi = ID_LOAD_EDIT
        private void LoadGrade(int editOrCreateId, int? eatingGroupID)
        {
            List<EatingGroupTreeViewModel> listGrade = new List<EatingGroupTreeViewModel>();
            GlobalInfo gi = new GlobalInfo();
            if (gi.AppliedLevel.HasValue)
            {
                int accountID = gi.AppliedLevel.Value;
                //+Test vs cap 1. Khi co cap Mau Giao thi doi thanh SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN
                if (accountID == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
                {
                    int count = 0;
                    EatingGroupTreeViewModel grade = new EatingGroupTreeViewModel();
                    grade.ClassName = Res.Get("EatingGroup_Label_KINDERGARTEN");
                    grade.ClassID = ID_OF_GRADE;
                    grade.Enabled = true;
                    grade.ListChildren = LoadGroup(editOrCreateId, eatingGroupID);
                    //Dem tong so lop thuoc cap hoc
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;
                    List<EducationLevel> lsEdu = EducationLevelBusiness.GetByGrade(accountID).ToList();
                    int countClassOfGrade = 0;
                    EducationLevel itemEdu = null;
                    for (int i = 0, tempEdu = lsEdu.Count; i < tempEdu; i++)
                    {
                        itemEdu = lsEdu[i];
                        //}
                        //foreach (EducationLevel item in lsEdu)
                        //{
                        int countClassOfEdu = ClassProfileBusiness.SearchBySchool(gi.SchoolID.Value, dic).Where(o => o.EducationLevelID == itemEdu.EducationLevelID).Count();
                        countClassOfGrade += countClassOfEdu;
                    }
                    var item = new EatingGroupTreeViewModel();
                    for (int i = 0, tempChildren = grade.ListChildren.Count; i < tempChildren; i++)
                    {
                        item = grade.ListChildren[i];
                        //}
                        //foreach (var item in grade.ListChildren)
                        //{
                        if (!item.Enabled) count++;
                    }
                    //Neu tat ca cac lop thuoc cap hoc deu da co nhom an thi disable checkbox cap hoc
                    if (count == countClassOfGrade) grade.Enabled = false;
                    listGrade.Add(grade);
                }
                //+Test voi cap 2. Khi co cap Nha Tre thi doi thanh SystemParamsInFile.APPLIED_LEVEL_CRECHE
                if (accountID == SystemParamsInFile.APPLIED_LEVEL_CRECHE)
                {
                    int count2 = 0;
                    EatingGroupTreeViewModel grade = new EatingGroupTreeViewModel();
                    grade.ClassName = Res.Get("EatingGroup_Label_Creche");
                    grade.ClassID = ID_OF_GRADE;
                    grade.Enabled = true;
                    grade.ListChildren = LoadGroup(editOrCreateId, eatingGroupID);
                    //Dem tong so lop thuoc cap hoc
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    List<EducationLevel> lsEdu = EducationLevelBusiness.GetByGrade(accountID).ToList();
                    int countClassOfGrade = 0;
                    EducationLevel itemEdu = null;
                    for (int i = 0, tempEdu = lsEdu.Count; i < tempEdu; i++)
                    {
                        itemEdu = lsEdu[i];
                        //}
                        //foreach (EducationLevel item in lsEdu)
                        //{
                        int countClassOfEdu = ClassProfileBusiness.SearchBySchool(gi.SchoolID.Value, dic).Where(o => o.EducationLevelID == itemEdu.EducationLevelID).Count();
                        countClassOfGrade += countClassOfEdu;
                    }
                    var item = new EatingGroupTreeViewModel();
                    for (int i = 0, tempChildren = grade.ListChildren.Count; i < tempChildren; i++)
                    {
                        item = grade.ListChildren[i];
                        //}
                        //foreach (var item in grade.ListChildren)
                        //{
                        if (!item.Enabled) count2++;
                    }
                    //Neu tat ca cac lop thuoc cap hoc deu da co nhom an thi disable checkbox cap hoc
                    if (count2 == countClassOfGrade) grade.Enabled = false;
                    listGrade.Add(grade);
                }
                ViewData[SMAS.Web.Areas.EatingGroupArea.EatingGroupConstants.LT_TREE] = listGrade;
            }
            else
            {
                throw new BusinessException(Res.Get("Common_Validate_NotDataPermintion"));
            }
        }
        //Load nhom hoc thuoc cap
        private List<EatingGroupTreeViewModel> LoadGroup(int editOrCreateId, int? eatingGroupID)
        {
            int count = 0;
            List<EatingGroupTreeViewModel> ls = new List<EatingGroupTreeViewModel>();
            List<EatingGroupTreeViewModel> lsAllClass = new List<EatingGroupTreeViewModel>();
            GlobalInfo global = new GlobalInfo();
            List<EducationLevel> lsEducationLevel = global.EducationLevels;
            EducationLevel item = null;
            EatingGroupTreeViewModel egt = null;
            for (int i = 0, tempEducationLevel = lsEducationLevel.Count; i < tempEducationLevel; i++)
            {
                egt = new EatingGroupTreeViewModel();
                item = lsEducationLevel[i];
                //}
                //foreach (EducationLevel item in lsEducationLevel)
                //{
                egt.ClassID = ID_OF_EDUCATIONLEVEL;
                egt.ClassName = item.Resolution;
                egt.Enabled = true;
                egt.ListChildren = LoadClass(item.EducationLevelID, editOrCreateId, eatingGroupID);
                //Dem so lop thuoc EducationLevel
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = item.EducationLevelID;
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;
                int countClassOfEducationLevel = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).Count();
                count = 0;
                EatingGroupTreeViewModel eg = null;
                for (int j = 0, tempChildren = egt.ListChildren.Count; j < tempChildren; j++)
                {
                    eg = egt.ListChildren[j];
                    //}
                    //foreach (EatingGroupTreeViewModel eg in egt.ListChildren)
                    //{
                    //Dem so lop da co nhom an(Enabled = false) thuoc EducationLevel
                    if (!eg.Enabled) count++;
                    lsAllClass.Add(eg);
                }
                //Neu tat ca cac lop thuoc khoi deu da co nhom an thi disabled checkbox khoi hoc
                if (count == countClassOfEducationLevel) egt.Enabled = false;
                ls.Add(egt);

            }
            ViewData[EatingGroupConstants.LIST_CLASS] = lsAllClass;
            return ls;
        }
        //Load cac lop thuoc nhom
        private List<EatingGroupTreeViewModel> LoadClass(int groupID, int editOrCreateId, int? eatingGroupID)
        {
            GlobalInfo global = new GlobalInfo();
            List<ClassProfile> listAllClass = new List<ClassProfile>();
            List<int> listClassDisabled = new List<int>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = groupID;
            dic["AcademicYearID"] = global.AcademicYearID;
            listAllClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            List<EatingGroupTreeViewModel> listClassEating = new List<EatingGroupTreeViewModel>();
            EatingGroupTreeViewModel egt = new EatingGroupTreeViewModel();

            //+ Hiển thị các lớp thuộc các nhóm: Thực hiện gọi hàm EatingGroupBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary). 
            //Kết quả thu được lstEatingGroup=> Chỉ hiển thị Checkbox cho các lớp thuộc nhóm nếu không tồn tại trong lstEatingGroup. 
            //Nếu tất cả  lớp học thuộc nhóm đã đăng ký thì checkbox nhóm bị disable.
            if (new GlobalInfo().SchoolID.HasValue)
            {
                //if (editOrCreateId == ID_LOAD_CREATE)
                //{
                //listClassEating = GetEatingGroup(null, listAllClass, editOrCreateId);
                listClassEating = GetEatingGroup(eatingGroupID, listAllClass, editOrCreateId);

                //}
                //else
                //{
                //    if (eatingGroupID.HasValue)
                //    {
                //        listClassEating = GetEatingGroup(eatingGroupID, listAllClass, editOrCreateId);
                //    }
                //}
            }
            return listClassEating;
        }

        private List<EatingGroupTreeViewModel> GetEatingGroup(int? EatingGroupID, List<ClassProfile> listClass, int editOrCreateId)
        {
            GlobalInfo global = new GlobalInfo();
            List<ClassProfile> listAllClass = listClass;
            List<int> listClassDisabled = new List<int>();
            List<EatingGroupBO> listEGByEGID = new List<EatingGroupBO>();
            List<EatingGroupBO> listEGAll = new List<EatingGroupBO>();
            List<EatingGroupTreeViewModel> listClassEating = new List<EatingGroupTreeViewModel>();
            EatingGroupTreeViewModel egt = new EatingGroupTreeViewModel();
            IDictionary<string, object> SearchInfoByEatingGroupID = new Dictionary<string, object>();
            IDictionary<string, object> SearchInfoAll = new Dictionary<string, object>();
            SearchInfoByEatingGroupID["IsActive"] = true;
            SearchInfoByEatingGroupID["AcademicYearID"] = global.AcademicYearID;
            SearchInfoAll["IsActive"] = true;
            SearchInfoAll["AcademicYearID"] = global.AcademicYearID;
            IQueryable<EatingGroupClass> listEatingGroupClass = EatingGroupClassBusiness.All;
            //Load khi sua
            if (EatingGroupID.HasValue)
            {
                SearchInfoByEatingGroupID["EatingGroupID"] = EatingGroupID.Value;
                listEGByEGID = EatingGroupBusiness.SearchBySchool(global.SchoolID.Value, SearchInfoByEatingGroupID).ToList();
            }
           listEGAll = EatingGroupBusiness.SearchBySchool(global.SchoolID.Value, SearchInfoAll).ToList();
            EatingGroupBO item = null;
            for (int i = 0, templist = listEGAll.Count; i < templist; i++)
            {
                item = listEGAll[i];
                //}
                //foreach (EatingGroupBO item in list)
                //{
                if (item.ClassID.HasValue)
                {
                    EatingGroupBO  checkClassInEG = new EatingGroupBO();
                    if (listEGByEGID.Count() != 0)
                    {
                        checkClassInEG = listEGByEGID.Where(x => x.ClassID == item.ClassID.Value).FirstOrDefault();
                    }
                    if (checkClassInEG!=null && checkClassInEG.EatingGroupID != null)
                    {
                        continue;
                    }
                    else
                    {
                        if (listClassDisabled.Contains(item.ClassID.Value))
                        {
                            continue;
                        }
                        listClassDisabled.Add(item.ClassID.Value);
                    }
                }
                //if (listClassDisabled.Count != 0)
                //{
                //    foreach (int id in listClassDisabled)
                //    {
                //        if (item.ClassID != id)
                //        {
                //            listClassDisabled.Add(item.ClassID.Value);
                //            break;
                //        }
                //    }
                //}
                //else
                //{
                //    listClassDisabled.Add(item.ClassID.Value);
                //}

            }
            bool isCreate = true;
            if (editOrCreateId == ID_LOAD_CREATE)
            {
                isCreate = true;

            }
            else
            {
                isCreate = false;
            }
            ClassProfile itemAllClass = null;
            int? classDisabledID = null;
            for (int i = 0, tempAllClass = listAllClass.Count; i < tempAllClass; i++)
            {
                itemAllClass = listAllClass[i];
                //}
                //foreach (ClassProfile item in listAllClass)
                //{
                egt = new EatingGroupTreeViewModel();
                egt.ClassID = itemAllClass.ClassProfileID;
                egt.ClassName = itemAllClass.DisplayName;
                egt.ListChildren = new List<EatingGroupTreeViewModel>();
                egt.Enabled = isCreate;
                egt.isCreate = isCreate;
                for (int j = 0, tempClassDisabled = listClassDisabled.Count; j < tempClassDisabled; j++)
                {
                    classDisabledID = listClassDisabled[j];
                    //}
                    //foreach (int classDisabledID in listClassDisabled)
                    //{
                    if (itemAllClass.ClassProfileID == classDisabledID)
                    {
                        egt.Enabled = !isCreate;
                        break;
                    }
                }
                // Lấy ra các lớp được check khi da dang ki (ischeck = true else false)
                int classProfileID = itemAllClass.ClassProfileID;
                int classID = 0;
                var temp = listEatingGroupClass.Where(p => p.ClassID == classProfileID && p.EatingGroupID == EatingGroupID).FirstOrDefault();
                if (temp != null)
                {
                    classID = temp.ClassID;
                    if (classID != 0)
                    {
                        egt.isCheck = true;
                    }
                    else
                    {
                        egt.isCheck = false;
                    }
                }
                else
                {
                    egt.isCheck = false;

                }
                // kiem tra an doi voi cac lop da co dang ki nhung khac nhom an
                var temp1 = listEatingGroupClass.Where(p => p.ClassID == classProfileID && p.EatingGroupID != EatingGroupID).FirstOrDefault();
                if (temp1 != null)
                {
                    classID = temp1.ClassID;
                    if (classID != 0)
                    {
                        egt.isEnabled = false;
                    }
                }
                else
                {
                    egt.isEnabled = true;

                }
                listClassEating.Add(egt);
            }

            return listClassEating;
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            IEnumerable<EatingGroupViewModel> lst = this._Search(SearchInfo);
            ViewData[EatingGroupConstants.LIST_EATINGGROUP] = lst.ToList();
            return PartialView("_List");
        }

        public PartialViewResult _Create()
        {
            LoadGrade(ID_LOAD_CREATE, null);
            return PartialView("_Create");
        }
        //id: eatinggroupid
        public PartialViewResult _Edit(int? id)
        {
            EatingGroup eg = EatingGroupBusiness.Find(id.Value);
            EatingGroupViewModel egv = new EatingGroupViewModel();
            egv.AcademicYearID = eg.AcademicYearID;
            egv.SchoolID = eg.SchoolID;
            egv.EatingGroupID = eg.EatingGroupID;
            egv.EatingGroupName = eg.EatingGroupName;
            egv.MonthFrom = eg.MonthFrom;
            egv.MonthTo = eg.MonthTo;
            egv.IsActive = eg.IsActive;
            if (id.HasValue)
            {
                LoadGrade(ID_LOAD_CREATE, id);
            }
            return PartialView("_Edit", egv);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(List<TreeViewItem> trvDept2_checkedNodes)
        {
            if (trvDept2_checkedNodes == null)
            {
                throw new BusinessException(Res.Get("EatingGroup_Validate_Class"));
            }
            GlobalInfo gl = new GlobalInfo();
            EatingGroup eatinggroup = new EatingGroup();
            TryUpdateModel(eatinggroup);
            Utils.Utils.TrimObject(eatinggroup);
            if (gl.SchoolID.HasValue)
            {
                eatinggroup.SchoolID = gl.SchoolID.Value;
                eatinggroup.AcademicYearID = gl.AcademicYearID.Value;
            }
            List<int> lstClass = new List<int>();
            if (trvDept2_checkedNodes != null)
            {
                TreeViewItem node = null;
                for (int i = 0, tempcheckedNode = trvDept2_checkedNodes.Count; i < tempcheckedNode; i++)
                {
                    node = trvDept2_checkedNodes[i];
                    //}
                    //foreach (TreeViewItem node in trvDept2_checkedNodes)
                    //{
                    if (Convert.ToInt32(node.Value) > 0)
                    {
                        lstClass.Add(Int32.Parse(node.Value));
                    }
                }
            }

            this.EatingGroupBusiness.Insert(eatinggroup, lstClass);
            this.EatingGroupBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EatingGroupID, List<TreeViewItem> trvDept_checkedNodes)
        {
            if (trvDept_checkedNodes == null)
            {
                throw new BusinessException(Res.Get("EatingGroup_Validate_EditClass"));
            }
            EatingGroup eatinggroup = this.EatingGroupBusiness.Find(EatingGroupID);
            TryUpdateModel(eatinggroup);
            Utils.Utils.TrimObject(eatinggroup);

            List<int> lstClass = new List<int>();
            if (trvDept_checkedNodes != null)
            {
                TreeViewItem node = null;
                for (int i = 0, tempcheckedNode = trvDept_checkedNodes.Count; i < tempcheckedNode; i++)
                {
                    node = trvDept_checkedNodes[i];
                    //}
                    //foreach (TreeViewItem node in trvDept_checkedNodes)
                    //{
                    if (Convert.ToInt32(node.Value) > 0)
                    {
                        lstClass.Add(Int32.Parse(node.Value));
                    }
                }
            }
            this.EatingGroupBusiness.Update(eatinggroup, lstClass);
            this.EatingGroupBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EatingGroupBusiness.Delete(id, new GlobalInfo().SchoolID.Value);
            this.EatingGroupBusiness.Save();
            Index();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        private IEnumerable<EatingGroupViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            var listDailyMenu = DailyMenuBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID && o.IsActive == true);
            var listNutritionalNorm = NutritionalNormBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID && o.IsActive == true);
            List<EatingGroupViewModel> listClassEating = new List<EatingGroupViewModel>();
            List<EatingGroupBO> query = this.EatingGroupBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo).ToList();
            List<EatingGroupViewModel> lst = query.Select(o => new EatingGroupViewModel
            {
                EatingGroupID = o.EatingGroupID.Value,
                SchoolID = o.SchoolID.Value,
                MonthFrom = o.MonthFrom,
                MonthTo = o.MonthTo,
                AcademicYearID = o.AcademicYearID.Value,
                //CreatedDate = o.CreatedDate,								
                IsActive = o.IsActive,
                //ModifiedDate = o.ModifiedDate,								
                EatingGroupName = o.EatingGroupName,
                ClassID = o.ClassID,
                ClassName = o.ClassName               
            }).ToList();
            if (lst == null)
            {
                listClassEating = new List<EatingGroupViewModel>();
            }
            else
            {
                List<int> listEatingGroupID = new List<int>();
                string lstClassNameOfEating = "";
                EatingGroupViewModel item = null;
                int? eatingGroupID = null;
                for (int i = 0, templst = lst.Count; i < templst; i++)
                {
                    item = lst[i];
                    //}
                    //foreach (EatingGroupViewModel item in lst)
                    //{
                    if (listEatingGroupID.Contains(item.EatingGroupID))
                    {
                        continue;
                    }
                    listEatingGroupID.Add(item.EatingGroupID);
                }
                
                for (int i = 0, tempEatingGroupID = listEatingGroupID.Count; i < tempEatingGroupID; i++)
                {
                    eatingGroupID = listEatingGroupID[i];
                    //}
                    //foreach (int eatingGroupID in listEatingGroupID)
                    //{
                    List<EatingGroupViewModel> q = lst.Where(o => o.EatingGroupID == eatingGroupID).ToList();
                    EatingGroupViewModel eg = q.First();
                    for (int j = 0; j < q.Count - 1; j++)
                    {
                        lstClassNameOfEating += q[j].ClassName + " ; ";
                    }
                    lstClassNameOfEating += q[q.Count - 1].ClassName;
                    eg.lstClassName = lstClassNameOfEating;
                    listClassEating.Add(eg);
                    lstClassNameOfEating = "";
                    if (listDailyMenu.Where(p => p.EatingGroupID == eatingGroupID).Count() > 0 || listNutritionalNorm.Where(p => p.EatingGroupID == eatingGroupID).Count() > 0)
                    {
                        listClassEating[i].DeleteAble = false;
                    }
                    else
                    {
                        listClassEating[i].DeleteAble = true;
                    }

                }
            }
            
            return listClassEating;
            
        }
    }
}




