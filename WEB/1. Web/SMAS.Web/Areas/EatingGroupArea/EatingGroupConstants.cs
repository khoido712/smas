/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.EatingGroupArea
{
    public class EatingGroupConstants
    {
        public const string LIST_EATINGGROUP = "listEatingGroup";
        public const string LT_TREE = "listTree";
        public const string LIST_CLASS = "listClassEnable";
        public const string LT_TREE_EDIT = "listTreeForEdit";
    }
}