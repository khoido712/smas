/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using Telerik.Web.Mvc.UI;
namespace SMAS.Web.Areas.EatingGroupArea.Models
{
    public class EatingGroupViewModel
    {
        [ScaffoldColumn(false)]
		public int EatingGroupID { get; set; }
        [ScaffoldColumn(false)]
		public int SchoolID { get; set; }
        [ResourceDisplayName("EatingGroup_Label_MonthFrom")]					
		public int? MonthFrom { get; set; }

        [ResourceDisplayName("EatingGroup_Label_MonthTo")]	           
		public int? MonthTo { get; set; }
        [ScaffoldColumn(false)]
		public int AcademicYearID { get; set; }
        [ScaffoldColumn(false)]
		public System.DateTime CreatedDate { get; set; }
        [ScaffoldColumn(false)]
		public bool? IsActive { get; set; }
        [ScaffoldColumn(false)]				
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        [ResourceDisplayName("EatingGroup_Label_EatingGroupName")]			
		public string EatingGroupName { get; set; }
         [ResourceDisplayName("EatingGroup_Label_ListClassName")]	
        public string lstClassName { get; set; }
        [ScaffoldColumn(false)]
        public int? ClassID { get; set; }
        [ScaffoldColumn(false)]
        public string ClassName { get; set; }
        [ScaffoldColumn(false)]
        public List<int> listClassID { get; set; }
        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }
    }
}


