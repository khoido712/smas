﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.EatingGroupArea.Models
{
    public class EatingGroupTreeViewModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public List<EatingGroupTreeViewModel> ListChildren { get; set; }
        public bool Enabled { get; set; }
        //Bien xa c dinh hanh vi nguoi dung la create hay edit
        public bool isCreate { get; set; }
        public bool isCheck { get; set; }// kiem tra co check hay khong
        public bool isEnabled { get; set; }// kiem tra co cho hien hay khong doi voi cac lop da dang ki hoac chua dang ki doi voi 1 mon
    }
}