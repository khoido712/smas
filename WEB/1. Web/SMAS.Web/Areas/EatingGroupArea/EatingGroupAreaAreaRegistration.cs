﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EatingGroupArea
{
    public class EatingGroupAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EatingGroupArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EatingGroupArea_default",
                "EatingGroupArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
