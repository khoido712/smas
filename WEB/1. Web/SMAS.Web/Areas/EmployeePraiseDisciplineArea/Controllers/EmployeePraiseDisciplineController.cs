﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;



using SMAS.Business.Business;
using SMAS.Business.IBusiness;

using SMAS.Web.Controllers;

using SMAS.Models.Models;
using SMAS.Web.Constants;
using System.Transactions;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.Web.Areas.EmployeePraiseDisciplineArea.Models;
using SMAS.Business.Common;


namespace SMAS.Web.Areas.EmployeePraiseDisciplineArea.Controllers
{   
    public class EmployeePraiseDisciplineController : BaseController
    {
        
        private readonly IEmployeePraiseDisciplineBusiness EmployeePraiseDisciplineBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ITeacherOfFacultyBusiness TeacherOfFacultyBusiness;
        public EmployeePraiseDisciplineController(ITeacherOfFacultyBusiness teacherOfFacultyBusiness,
            IEmployeePraiseDisciplineBusiness employeepraisedisciplineBusiness, 
            ISchoolFacultyBusiness schoolFacultyBusiness,
            IEmployeeBusiness employeeBusiness)
		{
            this.TeacherOfFacultyBusiness = teacherOfFacultyBusiness;
			this.EmployeePraiseDisciplineBusiness = employeepraisedisciplineBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.EmployeeBusiness = employeeBusiness;
		}




        public ViewResult Index(int? EmployeeID)
        {
            CheckPermissionForAction(EmployeeID, "Employee");
            Employee emp =  EmployeeBusiness.Find(EmployeeID);
            ViewData[EmployeePraiseDisciplineConstants.EMP] = emp;
            if (EmployeeID.HasValue && EmployeeID != 0)
            {
                ViewData[EmployeePraiseDisciplineConstants.DISABLED_COMBO] = "true";
            }
            else {
                ViewData[EmployeePraiseDisciplineConstants.DISABLED_COMBO] = "false";
            }
            
            SetViewData(EmployeeID);
            GlobalInfo global = new GlobalInfo();
            ViewData[EmployeePraiseDisciplineConstants.HAS_PERMISSION] = GetMenupermission("EmployeePraiseDiscipline", global.UserAccountID, global.IsAdmin);
            return View("Index");
        }
        #region Search
        private IEnumerable<EmployeePraiseViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[EmployeePraiseDisciplineConstants.HAS_PERMISSION] = GetMenupermission("EmployeePraiseDiscipline", global.UserAccountID, global.IsAdmin);
            IQueryable<EmployeePraiseDiscipline> query = from p in this.EmployeePraiseDisciplineBusiness.Search(SearchInfo)
                                                         join q in EmployeeBusiness.All on p.EmployeeID equals q.EmployeeID
                                                         where q.IsActive == true
                                                         select p;
            List<EmployeePraiseViewModel> lst = query.Select(o => new EmployeePraiseViewModel
            {
                EmployeePraiseDisciplineID = o.EmployeePraiseDisciplineID,
                FullName = o.Employee.FullName,
                Name = o.Employee.Name,
                FacultyName = o.SchoolFaculty.FacultyName,
                PraiseDetailContent = o.DetailContent,
                DisDetailContent =o.DetailContent,
                PraiseResolution = o.Resolution,
                DisResolution = o.Resolution,
                PraiseStartDate = o.StartDate,
                DisStartDate = o.StartDate,
                Description = o.Description,
                SchoolID = o.SchoolID,
                PraiseForm = o.Form,
                DisForm = o.Form,
            }).ToList();
            if (lst.Count > 0 && lst != null)
            {
                lst = lst.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
            }
            return lst;
        }
        

        [ValidateAntiForgeryToken]
        public ActionResult SearchPraise(int? FacultyID, int? EmployeeID)
        {
           // Utils.Utils.TrimObject(frm);
            if (FacultyID == null)
            {
                FacultyID = 0;
            }
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["SchoolFacultyID"] = FacultyID;
            SearchInfo["SchoolID"] = GlobalInfo.SchoolID;
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            SearchInfo["IsDiscipline"] = false;
            if(EmployeeID.HasValue && EmployeeID!=0){
                SearchInfo["EmployeeID"] = EmployeeID;
            }
            IEnumerable<EmployeePraiseViewModel> lstPraise = this._Search(SearchInfo);
            ViewData[EmployeePraiseDisciplineConstants.LS_EMPLOYEE_PRAISE] = lstPraise;

            //IDictionary<string, object> SearchInfoDis = new Dictionary<string, object>();
            //SearchInfoDis["IsActive"] = true;
            //SearchInfoDis["SchoolID"] = GlobalInfo.SchoolID;
            //SearchInfoDis["SchoolFacultyID"] = FacultyID;
            //SearchInfoDis["AcademicYearID"] = GlobalInfo.AcademicYearID;
            //SearchInfoDis["IsDiscipline"] = true;

            //IEnumerable<EmployeePraiseViewModel> lstDis = this._Search(SearchInfoDis);
            //ViewData[EmployeePraiseDisciplineConstants.LS_EMPLOYEE_DISCIPLINE] = lstDis;
            //SetViewData();

            return PartialView("_GridEmployeePraise", lstPraise);
        }
        

        [ValidateAntiForgeryToken]
        public ActionResult SearchDis(int? FacultyID, int? EmployeeID)
        {
            // Utils.Utils.TrimObject(frm);
            GlobalInfo GlobalInfo = new GlobalInfo();
            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //SearchInfo["IsActive"] = true;
            //SearchInfo["SchoolID"] = GlobalInfo.SchoolID;
            //SearchInfo["SchoolFacultyID"] = FacultyID;
            //SearchInfo["IsDiscipline"] = false;
            //SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;

            //IEnumerable<EmployeePraiseViewModel> lstPraise = this._Search(SearchInfo);
            //ViewData[EmployeePraiseDisciplineConstants.LS_EMPLOYEE_PRAISE] = lstPraise;

            IDictionary<string, object> SearchInfoDis = new Dictionary<string, object>();
            SearchInfoDis["IsActive"] = true;
            SearchInfoDis["SchoolID"] = GlobalInfo.SchoolID;
            SearchInfoDis["SchoolFacultyID"] = FacultyID;
            SearchInfoDis["IsDiscipline"] = true;
            SearchInfoDis["AcademicYearID"] = GlobalInfo.AcademicYearID;
            if (EmployeeID.HasValue && EmployeeID != 0)
            {
                SearchInfoDis["EmployeeID"] = EmployeeID;
            }
            IEnumerable<EmployeePraiseViewModel> lstDis = this._Search(SearchInfoDis);
            ViewData[EmployeePraiseDisciplineConstants.LS_EMPLOYEE_DISCIPLINE] = lstDis;
            //SetViewData();

            return PartialView("_GridEmployeeDiscipline", lstDis);
        }

        /// <summary>
        /// Load dữ liệu cho commbobox Giáo viên khi chọn Tổ bộ môn
        /// </summary>
        /// <param name="FacultyID"></param>
        /// <returns></returns>
        [SkipCheckRole]
        public JsonResult AjaxLoadTeacherSearch(int? FacultyID, int? TeacherIDHid)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<Employee> lst = new List<Employee>();
            if (FacultyID != null)
            {

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolFacultyID"] = FacultyID;
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, FacultyID.Value).OrderBy(o => o.FullName).ToList();
                if (TeacherIDHid != 0 && TeacherIDHid != null)
                {
                    lst = lst.Where(o => o.EmployeeID != TeacherIDHid);
                }
            }
            if (lst == null)
                lst = new List<Employee>();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }
        #endregion

        #region Create
        [HttpPost]
		[ValidateAntiForgeryToken]
        
        public ActionResult CreatePraise(EmployeePraiseViewModel epvm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("EmployeePraiseDiscipline", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeePraiseDiscipline ep = new EmployeePraiseDiscipline();         
            ep.SchoolID = GlobalInfo.SchoolID.Value;
            ep.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            ep.EmployeeID = epvm.TeacherOfFaculty;
            ep.SchoolFacultyID = epvm.FacultyPraiseID;
            ep.Resolution = epvm.PraiseResolution;
            ep.DetailContent = epvm.PraiseDetailContent;
            ep.Form = epvm.PraiseForm;
            ep.StartDate = epvm.PraiseStartDateCreate;
            ep.Description = epvm.Description;
             
            ep.IsDiscipline = false;
                
            
                using (TransactionScope scope = new TransactionScope())
                {
                    
                    this.EmployeePraiseDisciplineBusiness.InsertPraise(ep);
                    this.EmployeePraiseDisciplineBusiness.Save();
                    
                    scope.Complete();
                }
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreateDis(EmployeePraiseViewModel epvm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("EmployeePraiseDiscipline", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeePraiseDiscipline ep = new EmployeePraiseDiscipline();
            ep.SchoolID = GlobalInfo.SchoolID.Value;

            ep.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            ep.EmployeeID = epvm.TeacherOfFacultyDis;
            ep.SchoolFacultyID = epvm.FacultyDisID;
            ep.Resolution = epvm.DisResolution;
            ep.DetailContent = epvm.DisDetailContent;
            ep.Form = epvm.DisForm;
            ep.StartDate = epvm.DisStartDate;
            ep.Description = epvm.Description;
             
            ep.IsDiscipline = true;


            using (TransactionScope scope = new TransactionScope())
            {
                ep.IsDiscipline = true;
                this.EmployeePraiseDisciplineBusiness.InsertDiscipline(ep);
                this.EmployeePraiseDisciplineBusiness.Save();

                scope.Complete();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));

        }
        #endregion

        #region EditDis

        public PartialViewResult EditDis(int id)
        {
            CheckPermissionForAction(id, "EmployeePraiseDiscipline");
            EmployeePraiseDiscipline ep = this.EmployeePraiseDisciplineBusiness.Find(id);
            EmployeePraiseViewModel emprai = new EmployeePraiseViewModel();

            emprai.EmployeePraiseDisciplineID = ep.EmployeePraiseDisciplineID;
            if (ep.SchoolFacultyID != null)
            {
                emprai.FacultyDisID = ep.SchoolFacultyID.Value;
            }
            emprai.FacultyName = ep.SchoolFaculty.FacultyName;
            emprai.FullName = ep.Employee.FullName;
            emprai.DisDetailContent = ep.DetailContent;
            emprai.DisForm = ep.Form;
            emprai.DisResolution = ep.Resolution;
            emprai.DisStartDateEdit = ep.StartDate;
            emprai.Description = ep.Description;




            return PartialView("EditDis", emprai);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult EditEmployeeDis(EmployeePraiseViewModel epvm)
        {
            CheckPermissionForAction(epvm.EmployeePraiseDisciplineID, "EmployeePraiseDiscipline");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("EmployeePraiseDiscipline", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeePraiseDiscipline ep = this.EmployeePraiseDisciplineBusiness.Find(epvm.EmployeePraiseDisciplineID);
            ep.Resolution = epvm.DisResolution;
            ep.DetailContent = epvm.DisDetailContent;
            ep.Form = epvm.DisForm;
            ep.StartDate = epvm.DisStartDateEdit;
            ep.Description = epvm.Description;


            TryUpdateModel(ep);
            Utils.Utils.TrimObject(ep);
            using (TransactionScope scope = new TransactionScope())
            {
                ep.IsDiscipline = true;
                EmployeePraiseDisciplineBusiness.UpdateDiscipline(ep);
                EmployeePraiseDisciplineBusiness.Save();
                scope.Complete();
            }


            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        #endregion

        #region EditPraise
        
        public PartialViewResult EditPraise(int id)
        {
            CheckPermissionForAction(id, "EmployeePraiseDiscipline");
            EmployeePraiseDiscipline ep = this.EmployeePraiseDisciplineBusiness.Find(id);
            EmployeePraiseViewModel emprai = new EmployeePraiseViewModel();

            emprai.EmployeePraiseDisciplineID = ep.EmployeePraiseDisciplineID;
            if (ep.SchoolFacultyID != null)
            {
                emprai.FacultyPraiseID = ep.SchoolFacultyID.Value;
            }
            emprai.EmployeePraiseDisciplineID = id;
            emprai.FacultyName = ep.SchoolFaculty.FacultyName;
            emprai.FullName = ep.Employee.FullName;
            emprai.PraiseDetailContent = ep.DetailContent;
            emprai.PraiseForm = ep.Form;
            emprai.PraiseResolution = ep.Resolution;
            emprai.PraiseStartDateEdit = ep.StartDate;
            emprai.Description = ep.Description;
            

           

            return PartialView("EditPraise",emprai);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult EditEmployeePraise(EmployeePraiseViewModel epvm)
        {
            CheckPermissionForAction(epvm.EmployeePraiseDisciplineID, "EmployeePraiseDiscipline");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("EmployeePraiseDiscipline", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeePraiseDiscipline ep = this.EmployeePraiseDisciplineBusiness.Find(epvm.EmployeePraiseDisciplineID);        
            ep.Resolution = epvm.PraiseResolution;
            ep.DetailContent = epvm.PraiseDetailContent;
            ep.Form = epvm.PraiseForm;
            ep.StartDate = epvm.PraiseStartDateEdit;
            ep.Description = epvm.Description;
           

            TryUpdateModel(ep);
            Utils.Utils.TrimObject(ep);
            using (TransactionScope scope = new TransactionScope())
            {
                ep.IsDiscipline = false;
                EmployeePraiseDisciplineBusiness.UpdatePraise(ep);
                EmployeePraiseDisciplineBusiness.Save();
                scope.Complete();
            }


            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        #endregion

        #region DeletePraise
        
        [ValidateAntiForgeryToken]
        public ActionResult DeletePraise(int id)
        {
            CheckPermissionForAction(id, "EmployeePraiseDiscipline");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("EmployeePraiseDiscipline", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            GlobalInfo global = new GlobalInfo();
            using (TransactionScope scope = new TransactionScope())
            {
                EmployeePraiseDisciplineBusiness.DeletePraise(id, global.SchoolID.Value,global.AcademicYearID.Value);
                EmployeePraiseDisciplineBusiness.Save();
                scope.Complete();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        
        [ValidateAntiForgeryToken]
        public ActionResult DeleteDis(int id)
        {
            CheckPermissionForAction(id, "EmployeePraiseDiscipline");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("EmployeePraiseDiscipline", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            GlobalInfo global = new GlobalInfo();
            using (TransactionScope scope = new TransactionScope())
            {
                EmployeePraiseDisciplineBusiness.DeleteDiscipline(id, global.SchoolID.Value, global.AcademicYearID.Value);
                EmployeePraiseDisciplineBusiness.Save();
                scope.Complete();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        //
        // POST: /EmployeePraiseDiscipline/Delete/5
       
        #endregion

        #region setViewData
        private void SetViewData(int? EmployeeID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            this._Search(dic);
            GlobalInfo GlobalInfo= new GlobalInfo();
            
            // Danh sách giáo viên
            List<Employee> ListTeacher = new List<Employee>();
            ViewData[EmployeePraiseDisciplineConstants.LIST_TEACHER] = new SelectList(ListTeacher, "EmployeeID", "FullName");

            //cboSchoolFaculty:  SchoolFacultyBusiness.SearchBySchool(UserInfo.SchoolID) 
            IDictionary<string, object> FacultySearchInfo = new Dictionary<string, object>();
            FacultySearchInfo["IsActive"] = true;
            //FacultySearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            List<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, FacultySearchInfo).OrderBy(o=>o.FacultyName).ToList();
            ViewData[EmployeePraiseDisciplineConstants.LS_FACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");

            //cbo
            List<TeacherOfFaculty> lsTeacher = new List<TeacherOfFaculty>();
            ViewData[EmployeePraiseDisciplineConstants.LS_TEACHEROFFACULTYDIS] = new SelectList(lsTeacher, "EmployeeID", "Name");
            //Load grid theo EmployeePraiseDisciplineBusiness.SearchPraise(UserInfo.SchoolID, Dictionary<string, object>) + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            
            IDictionary<string, object> PraiseSearchInfo = new Dictionary<string, object>();
            PraiseSearchInfo["IsActive"] = true;
            PraiseSearchInfo["IsDiscipline"] = false;
            PraiseSearchInfo["SchoolID"] = GlobalInfo.SchoolID;
            PraiseSearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            // Thêm điều kiện tìm kiếm khen thưởng kỷ luật của từng cán bộ - 10/08 - DungVA
            if(EmployeeID.HasValue && EmployeeID!=0){
                PraiseSearchInfo["EmployeeID"] = EmployeeID.Value;
            }
            // End of Thêm điều kiện tìm kiếm khen thưởng kỷ luật của từng cán bộ - 10/08 - DungVA
            IEnumerable<EmployeePraiseViewModel> lstPraise = this._Search(PraiseSearchInfo);
            ViewData[EmployeePraiseDisciplineConstants.LS_EMPLOYEE_PRAISE] = lstPraise;
            //EmployeeDiscipline
            IDictionary<string, object> DisSearchInfo = new Dictionary<string, object>();
            DisSearchInfo["IsActive"] = true;
            DisSearchInfo["IsDiscipline"] = true;
            DisSearchInfo["SchoolID"] = GlobalInfo.SchoolID;
            DisSearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            // Thêm điều kiện tìm kiếm khen thưởng kỷ luật của từng cán bộ - 10/08 - DungVA
            if (EmployeeID.HasValue && EmployeeID != 0)
            {
                DisSearchInfo["EmployeeID"] = EmployeeID.Value;
            }
            // End of Thêm điều kiện tìm kiếm khen thưởng kỷ luật của từng cán bộ - 10/08 - DungVA
            IEnumerable<EmployeePraiseViewModel> lstDis = this._Search(DisSearchInfo);
            ViewData[EmployeePraiseDisciplineConstants.LS_EMPLOYEE_DISCIPLINE] = lstDis;

        }

        #endregion

        #region Load Combobox


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadTeacherOfFaculty(int? SchoolFacultyId)
        {
            IEnumerable<Employee> lst = new List<Employee>();
            if (SchoolFacultyId.ToString() != "")
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolFacultyID"] = SchoolFacultyId;
                dic["IsActive"] = true;
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, SchoolFacultyId.Value).OrderBy(o => o.Name).ThenBy(o=>o.FullName);
            }
            if (lst == null)
                lst = new List<Employee>();
            //lst = lst.Where(o => o.AppliedLevel == new GlobalInfo().AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }

        #endregion

    }
}
