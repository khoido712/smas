﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeePraiseDisciplineArea
{
    public class EmployeePraiseDisciplineAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeePraiseDisciplineArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeePraiseDisciplineArea_default",
                "EmployeePraiseDisciplineArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
