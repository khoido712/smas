﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EmployeePraiseDisciplineArea
{
    public class EmployeePraiseDisciplineConstants
    {
        public const string LS_FACULTY = "ListFalculty";
        public const string LS_EMPLOYEE_PRAISE = "ListEmployeePraise";
        public const string LS_EMPLOYEE_DISCIPLINE = "ListEmployeeDiscipline";
        public const string NBR_FACULTY = "NombreFaculty";
        public const string MSG = "message";
        public const string LS_TEACHEROFFACULTY = "ListTeacherOfFaculty";
        public const string LS_TEACHEROFFACULTYDIS = "ListTeacherOfFacultyDis";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
        public const string EMP = "EMP";
        public const string LIST_TEACHER = "ListTeacher";
        public const string DISABLED_COMBO = "DISABLED_COMBO";
    }
}