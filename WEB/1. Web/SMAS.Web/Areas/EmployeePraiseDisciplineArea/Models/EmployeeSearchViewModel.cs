﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.EmployeePraiseDisciplineArea.Models
{
    public class EmployeeSearchViewModel
    {
        [ResourceDisplayName("EmployeePraiseDiscipline_Label_Faculty")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", EmployeePraiseDisciplineConstants.LS_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacherSearch(this)")]
        public Nullable<int> FacultyID { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Teacher")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", EmployeePraiseDisciplineConstants.LIST_TEACHER)]
        public Nullable<int> TeacherID { get; set; }
    }
}
