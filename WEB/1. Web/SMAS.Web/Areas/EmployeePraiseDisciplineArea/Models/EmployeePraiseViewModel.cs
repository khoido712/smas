﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;


namespace SMAS.Web.Areas.EmployeePraiseDisciplineArea.Models
{
    public class EmployeePraiseViewModel
    {
        public int EmployeePraiseDisciplineID { get; set; }

        [ResourceDisplayName("EmployeePraiseDiscipline_Label_TeacherOfFaculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int TeacherOfFacultyDis { get; set; }

        [ResourceDisplayName("EmployeePraiseDiscipline_Label_TeacherOfFaculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int TeacherOfFaculty { get; set; }

        [ResourceDisplayName("EmployeePraiseDiscipline_Label_Faculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? FacultyPraiseID { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? FacultyDisID { get; set; }

        [ScaffoldColumn(false)]
        public int SchoolID { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FullName { get; set; }


        [ResourceDisplayName("Employee_Label_FullName")]
        public string Name { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FacultyName { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_DetailContent")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PraiseDetailContent { get; set; }

        [ResourceDisplayName("EmployeeDiscipline_Label_DetailContent")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DisDetailContent { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_Form")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PraiseForm { get; set; }

        [ResourceDisplayName("EmployeeDiscipline_Label_Form")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DisForm { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_Resoltuion")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PraiseResolution { get; set; }

        [ResourceDisplayName("EmployeeDiscipline_Label_Resoltuion")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DisResolution { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_StartDate")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? PraiseStartDateCreate { get; set; }
        [ResourceDisplayName("EmployeePraise_Label_StartDate")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? PraiseStartDateEdit { get; set; }
        [ResourceDisplayName("EmployeePraise_Label_StartDate")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? PraiseStartDate { get; set; }

        [ResourceDisplayName("EmployeeDiscipline_Label_StartDate")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? DisStartDate { get; set; }
        [ResourceDisplayName("EmployeeDiscipline_Label_StartDate")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? DisStartDateEdit { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}
