﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.EmployeePraiseDisciplineArea.Models
{
    public class EmployeeViewModel
    {
        [ScaffoldColumn(false)]
        public int EmployeePraiseDisciplineID { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FullName { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FacultyName    { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_DetailContent")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DetailContent { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_Form")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Form { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_Resoltuion")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Resoltuion { get; set; }

        [ResourceDisplayName("EmployeePraise_Label_StartDate")]
        public DateTime? StartDate { get; set; }


        [ResourceDisplayName("EmployeePraiseDiscipline_Label_Description")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Description { get; set; }
    }
}
