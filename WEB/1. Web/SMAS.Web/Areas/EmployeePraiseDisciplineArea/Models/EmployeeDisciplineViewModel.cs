﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;


namespace SMAS.Web.Areas.EmployeePraiseDisciplineArea.Models
{
    public class EmployeeDisciplineViewModel
    {
        [ScaffoldColumn(false)]
        public int EmployeePraiseDisciplineID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Employee_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FullName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("EmployeePraise_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FacultyName { get; set; }

        [ResourceDisplayName("EmployeePraiseDiscipline_label_FacultyName")]
        [UIHint("Combobox")]
        [AdditionalMetadata("FacultyID", EmployeePraiseDisciplineConstants.LS_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacherOfFaculty(this)")]
        public int FacultyID { get; set; }

        [ResourceDisplayName("EmployeePraiseDiscipline_label_TeacherOfFaculty")]
        [UIHint("Combobox")]
        [AdditionalMetadata("TeacherOfFacultyID", EmployeePraiseDisciplineConstants.LS_TEACHEROFFACULTY)]
        [AdditionalMetadata("OnChange", "search()")]
        public int TeacherOfFacultyID { get; set; }

        

        
        [ResourceDisplayName("EmployeeDiscipline_Label_DetailContent")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DisDetailContent { get; set; }



       
        [ResourceDisplayName("EmployeeDiscipline_Label_Form")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DisForm { get; set; }

        


       
        [ResourceDisplayName("EmployeeDiscipline_Label_Resoltuion")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DisResolution { get; set; }

        


       
        [UIHint("DateTimePicker")]
        [ResourceDisplayName("EmployeeDiscipline_Label_StartDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? DisStartDate { get; set; }


        [ResourceDisplayName("EmployeePraise_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}
