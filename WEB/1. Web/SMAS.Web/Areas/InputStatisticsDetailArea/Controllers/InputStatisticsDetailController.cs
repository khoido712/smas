﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.InputStatisticsDetailArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Web.Areas.InputStatisticsDetailArea.Controllers
{
    public partial class InputStatisticsDetailController : BaseController
    {
        private readonly IInputStatisticsBusiness InputStatisticsDetailBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private GlobalInfo globalInfo = new GlobalInfo();

        public InputStatisticsDetailController(IInputStatisticsBusiness InputStatisticsDetailBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness, IDistrictBusiness DistrictBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.InputStatisticsDetailBusiness = InputStatisticsDetailBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }

        //
        // GET: /InputStatisticsDetail/

        public ActionResult Index()
        {

            SupervisingDept sd = SupervisingDeptBusiness.GetSupervisingDeptOfUser(globalInfo.UserAccountID);
            // Lay quan huyen
            List<District> listDistrict = new List<District>();
            int districSelected = 0;
            if (globalInfo.IsSuperVisingDeptRole)
            {
                listDistrict = DistrictBusiness.Search(new Dictionary<string, object>()
                {
                    {"ProvinceID", globalInfo.ProvinceID.Value}
                }).ToList();
            }
            else if (globalInfo.IsSubSuperVisingDeptRole)
            {
                listDistrict = DistrictBusiness.Search(new Dictionary<string, object>()
                {
                    {"DistrictID", globalInfo.DistrictID.Value}
                }).ToList();
                districSelected = globalInfo.DistrictID.Value;
            }
            SelectList selectDistrictList = new SelectList(listDistrict, "DistrictID", "DistrictName", districSelected);
            ViewData[InputStatisticsDetailConstants.LIST_DISTRICT] = selectDistrictList;
            // Nam hoc
            List<int> listYear = AcademicYearBusiness.GetListYearForSupervisingDept(sd.SupervisingDeptID);
            List<ComboObject> listYearObj = new List<ComboObject>();
            foreach (int year in listYear)
            {
                ComboObject comboObj = new ComboObject(year.ToString(), year + " - " + (year + 1));
                listYearObj.Add(comboObj);
            }
            SelectList selectYear = new SelectList(listYearObj, "key", "value");
            ViewData[InputStatisticsDetailConstants.LIST_ACADEMICYEAR] = selectYear;
            // Cap hoc
            List<ComboObject> listGrade = new List<ComboObject>();
            if (sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                || sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                listGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("SubjectCat_AppliedLevel_Level1")));
                listGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("SubjectCat_AppliedLevel_Level2")));
                listGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("SubjectCat_AppliedLevel_Level3")));

            }
            else if (sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE
              || sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                listGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("SubjectCat_AppliedLevel_Level1")));
                listGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("SubjectCat_AppliedLevel_Level2")));
            }
            SelectList selectGrade = new SelectList(listGrade, "key", "value");
            ViewData[InputStatisticsDetailConstants.LIST_GRADE] = selectGrade;
            return View();
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="frm">The FRM.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>12/5/2012</date>

        public PartialViewResult Search(FormCollection frm)
        {
            //Utils.Utils.TrimObject(frm);
            int DistrictID = 0;
            if (frm["DistrictID"] != "")
            {
                DistrictID = Convert.ToInt32(frm["DistrictID"]);
            }

            int Year = Convert.ToInt32(frm["Year"]);
            DateTime ReportDate = Convert.ToDateTime(frm["ReportDate"]);
            int Grade = Convert.ToInt32(frm["Grade"]);
            int cbovalue = Convert.ToInt32(frm["cboInputStatistics"]);
            List<InputStatistics> lst = null;
            if (cbovalue == 1) //thong ke theo chi tiet
            {
                lst = this._Search(globalInfo.UserAccountID, globalInfo.SupervisingDeptID.Value,
                DistrictID, Year, ReportDate, Grade);
                ViewData[InputStatisticsDetailConstants.LIST_INPUT_STATISTICS] = lst;
            }
            else if (cbovalue == 2) // thong ke theo quan huyen
            {
                GroupSearchViewModel groupSearchViewModel = new GroupSearchViewModel();
                groupSearchViewModel.DistrictID = DistrictID;
                groupSearchViewModel.GroupReportDate = ReportDate;
                groupSearchViewModel.Year = Year;
                ViewData[InputStatisticsDetailConstants.LISTSEARCHBYDISTICT] = this.SearchGroup(groupSearchViewModel);
                return PartialView("ListGroupSearch");
            }
            else
            { // tình hình nhập liệu tại các trường
                SchoolSearchViewModel schoolSearchViewModel = new SchoolSearchViewModel();
                schoolSearchViewModel.DistrictID = DistrictID;
                schoolSearchViewModel.SchoolReportDate = ReportDate;
                schoolSearchViewModel.Year = Year;
                ViewData[InputStatisticsDetailConstants.LISTSEARCHBYSCHOOL] = this.SearchSchool(schoolSearchViewModel);
                return PartialView("_SearchSchool");
            }
            return PartialView("_List");
        }


        /// <summary>
        /// DownloadReport
        /// </summary>
        /// <param name="idProcessedReport">The id processed report.</param>
        /// <returns>
        /// FileResult
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>12/5/2012</date>
        public FileResult DownloadReport()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            int districtID = Request["DistrictID"] != null && Request["DistrictID"] != string.Empty
                ? int.Parse(Request["DistrictID"]) : 0;
            int grade = int.Parse(Request["Grade"]);
            int year = int.Parse(Request["Year"]);
            DateTime reportDate = DateTime.Parse(Request["ReportDate"]);
            FileStreamResult result = null;
            int cboInputStatistics = int.Parse(Request["cboInputStatistics"]);
            if (cboInputStatistics == 1) // thong ke chi tiet
            {
                ProcessedReport processedReport = InputStatisticsDetailBusiness.ExportDetailStatistics(globalInfo.UserAccountID,
                    globalInfo.SupervisingDeptID.Value, districtID, year, reportDate, grade);
                Stream excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = processedReport.ReportName;
            }
            else if (cboInputStatistics == 2)
            { //tinh hinh nhap lieu theo quan huyen
                GroupSearchViewModel groupSearchViewModel = new GroupSearchViewModel();
                groupSearchViewModel.DistrictID = districtID;
                groupSearchViewModel.GroupReportDate = reportDate;
                groupSearchViewModel.Year = year;
                return this.GroupReport(groupSearchViewModel);
            }
            else
            { // tinh hinh nhap lieu theo cac truong
                SchoolSearchViewModel schoolSearchViewModel = new SchoolSearchViewModel();
                schoolSearchViewModel.DistrictID = districtID;
                schoolSearchViewModel.SchoolReportDate = reportDate;
                schoolSearchViewModel.Year = year;
                return this.SchoolReport(schoolSearchViewModel);
            }
            return result;
        }


        /// <summary>
        /// _Search
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// List{InputStatistics}
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>12/5/2012</date>
        private List<InputStatistics> _Search(int UserAccountID, int SupervisingDeptID, int DistrictID,
                    int Year, DateTime? ReportDate, int AppliedLevel)
        {
            return InputStatisticsDetailBusiness.DetailStatistics(UserAccountID, SupervisingDeptID, DistrictID,
                    Year, ReportDate, AppliedLevel);
        }
    }
}





