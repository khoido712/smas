﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.InputStatisticsDetailArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;

namespace SMAS.Web.Areas.InputStatisticsDetailArea.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>AuNH</author>
    /// <date>12/6/2012</date>
    public partial class InputStatisticsDetailController
    {
        public ContentResult SearchGroup(GroupSearchViewModel frm)
        {
            int districtId = frm.DistrictID.HasValue ? frm.DistrictID.Value : 0;
            string htmlReport = this.InputStatisticsDetailBusiness.GroupByDistrictStatistics(
                globalInfo.UserAccountID,
                globalInfo.SupervisingDeptID.Value,
                districtId,
                frm.Year,
                frm.GroupReportDate.Value
            );

            return Content(htmlReport);
        }

        public FileResult GroupReport(GroupSearchViewModel frm)
        {
            int districtId = frm.DistrictID.HasValue ? frm.DistrictID.Value : 0;
            ProcessedReport processedReport = InputStatisticsDetailBusiness.ExportGroupByDistrictStatistics(
                globalInfo.UserAccountID,
                globalInfo.SupervisingDeptID.Value,
                districtId,
                frm.Year,
                frm.GroupReportDate.Value
            );

            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public ContentResult SearchSchool(SchoolSearchViewModel frm)
        {
            int districtId = frm.DistrictID.HasValue ? frm.DistrictID.Value : 0;
            string htmlReport = this.InputStatisticsDetailBusiness.SchoolStatistics(
                globalInfo.UserAccountID,
                globalInfo.SupervisingDeptID.Value,
                districtId,
                frm.Year,
                frm.SchoolReportDate.Value
            );

            return Content(htmlReport);
        }

        public FileResult SchoolReport(SchoolSearchViewModel frm)
        {
            int districtId = frm.DistrictID.HasValue ? frm.DistrictID.Value : 0;
            ProcessedReport processedReport = InputStatisticsDetailBusiness.ExportSchoolStatistics(
                globalInfo.UserAccountID,
                globalInfo.SupervisingDeptID.Value,
                districtId,
                frm.Year,
                frm.SchoolReportDate.Value
            );

            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}