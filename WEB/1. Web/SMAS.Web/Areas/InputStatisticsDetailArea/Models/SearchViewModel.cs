/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.InputStatisticsDetailArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("InputStatistics_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", InputStatisticsDetailConstants.LIST_DISTRICT)]
        public int? DistrictID { get; set; }

        [ResourceDisplayName("InputStatistics_Label_AppliedLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", InputStatisticsDetailConstants.LIST_GRADE)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int Grade { get; set; }

        [ResourceDisplayName("InputStatistics_Label_Year")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", InputStatisticsDetailConstants.LIST_ACADEMICYEAR)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int Year { get; set; }

        [ResourceDisplayName("InputStatistics_Label_ReportDate")]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("Default", "Now")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public DateTime? ReportDate { get; set; }
    }
}