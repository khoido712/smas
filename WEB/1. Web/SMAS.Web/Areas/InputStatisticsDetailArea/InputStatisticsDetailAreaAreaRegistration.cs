﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.InputStatisticsDetailArea
{
    public class InputStatisticsDetailAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "InputStatisticsDetailArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "InputStatisticsDetailArea_default",
                "InputStatisticsDetailArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
