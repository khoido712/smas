/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.InputStatisticsDetailArea
{
    public class InputStatisticsDetailConstants
    {
        public const string LIST_INPUT_STATISTICS = "listInputStatistics";
        public const string LIST_DISTRICT = "listDistrict";
        public const string LIST_ACADEMICYEAR = "listAcademicYear";
        public const string LIST_GRADE = "listGrade";
        public const string LISTSEARCHBYDISTICT = "ListSearchByDistrict";
        public const string LISTSEARCHBYSCHOOL = "ListSearchBySChool";
    }
}