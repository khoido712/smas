﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.WeeklyMenuAdditionalArea
{
    public class WeeklyMenuAdditionalAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "WeeklyMenuAdditionalArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "WeeklyMenuAdditionalArea_default",
                "WeeklyMenuAdditionalArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
