﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.WeeklyMenuAdditionalArea.Models;
using SMAS.Web.Areas.WeeklyMenuAdditionalArea;
using System.Transactions;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.WeeklyMenuAdditionalArea.Controllers
{
    public class WeeklyMenuAdditionalController : BaseController
    {
        private readonly IWeeklyMenuBusiness WeeklyMenuBusiness;
        private readonly IEatingGroupBusiness EatingGroupBusiness;
        private readonly IEatingGroupClassBusiness EatingGroupClassBusiness;
        private readonly IMealCatBusiness MealCatBusiness;
        private readonly IDailyMenuBusiness DailyMenuBusiness;
        private readonly IDailyMenuDetailBusiness DailyMenuDetailBusiness;
        private readonly IWeeklyMenuDetailBusiness WeeklyMenuDetailBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAddMenuForChildrenBusiness AddMenuForChildrenBusiness;
        private readonly IEvaluationConfigBusiness EvaluationConfigBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        private string Thu = Res.Get("Common_Day").ToUpper();
        public WeeklyMenuAdditionalController(IWeeklyMenuBusiness weeklymenuBusiness, IEatingGroupBusiness eatingGroupBusiness, IEatingGroupClassBusiness eatingGroupClassBusiness, IMealCatBusiness mealCatBusiness,
            IDailyMenuBusiness dailyMenuBusiness, IDailyMenuDetailBusiness dailyMenuDetailBusiness, IWeeklyMenuDetailBusiness weeklyMenuDetailBusiness,
            IClassProfileBusiness classProfileBusiness, IAddMenuForChildrenBusiness addMenuForChildrenBusiness, IEvaluationConfigBusiness evaluationConfigBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness, IProcessedReportBusiness processedReportBusiness, ISchoolProfileBusiness schoolProfileBusiness, IAcademicYearBusiness academicYearBusiness)
        {
            this.WeeklyMenuBusiness = weeklymenuBusiness;
            this.EatingGroupBusiness = eatingGroupBusiness;
            this.EatingGroupClassBusiness = eatingGroupClassBusiness;
            this.MealCatBusiness = mealCatBusiness;
            this.DailyMenuBusiness = dailyMenuBusiness;
            this.DailyMenuDetailBusiness = dailyMenuDetailBusiness;
            this.WeeklyMenuDetailBusiness = weeklyMenuDetailBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.AddMenuForChildrenBusiness = addMenuForChildrenBusiness;
            this.EvaluationConfigBusiness = evaluationConfigBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }

        //
        // GET: /WeeklyMenuAdditional/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            int? AcademicYearID = _globalInfo.AcademicYearID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            IQueryable<EatingGroup> lsEatingGroup = EatingGroupBusiness.SearchEatingGroup(SearchInfo);
            ViewData[WeeklyMenuAdditionalConstants.List_EatingGroup] = new SelectList(lsEatingGroup.ToList(), "EatingGroupID", "EatingGroupName");
            //Get view data here
            IEnumerable<WeeklyMenuAdditionalViewModel> lst = this._Search(SearchInfo);
            ViewData[WeeklyMenuAdditionalConstants.LIST_WEEKLYMENU] = lst;
            DateTime DateFirstOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateFirstOfMonth = DateFirstOfMonth.AddDays((-DateFirstOfMonth.Day) + 1);
            ViewData[WeeklyMenuAdditionalConstants.DateFirstOfMonth] = DateFirstOfMonth.ToString();
            bool NowInCurrentYear;
            if (AcademicYearID.HasValue)
                NowInCurrentYear = this.AcademicYearBusiness.CheckTimeInYear(AcademicYearID.Value, DateTime.Now);
            else
                NowInCurrentYear = false;
            ViewData[WeeklyMenuAdditionalConstants.NowInCurrentYear] = NowInCurrentYear;
            return View();
        }

        //
        // GET: /WeeklyMenuAdditional/Search


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchInsertChildren(SearchInsertChildrenViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            WeeklyMenu WeeklyMenu = WeeklyMenuBusiness.Find(frm.WeeklyMenuID);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EvaluationConfigID"] = frm.EvaluationConfigID;
            SearchInfo["ClassID"] = frm.Class;
            SearchInfo["EatingGroupID"] = WeeklyMenu.EatingGroupID;
            SearchInfo["Fullname"] = frm.Fullname;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            //add search info		
            IEnumerable<PupilProfileViewModel> lst = this._SearchChildrenInsert(SearchInfo);
            ViewData[WeeklyMenuAdditionalConstants.List_PupilInsert] = lst;

            //Get view data here
            return PartialView("_ListPupilInsert");
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchChildren(SearchChildrenViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["WeeklyMenuID"] = frm.WeeklyMenuID;
            SearchInfo["ClassID"] = frm.Class;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            //add search info		
            IEnumerable<PupilProfileViewModel> lst = this._SearchChildren(SearchInfo);
            ViewData[WeeklyMenuAdditionalConstants.List_Pupil] = lst;

            //Get view data here
            return PartialView("_ListPupil");
        }
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["WeeklyMenuCode"] = frm.WeeklyMenuCode;
            SearchInfo["EatingGroupID"] = frm.EatingGroupID;
            SearchInfo["FromDate"] = frm.StartDate;
            SearchInfo["ToDate"] = frm.EndDate;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            //add search info		
            IEnumerable<WeeklyMenuAdditionalViewModel> lst = this._Search(SearchInfo);
            ViewData[WeeklyMenuAdditionalConstants.LIST_WEEKLYMENU] = lst;
            //Get view data here
            int? AcademicYearID = _globalInfo.AcademicYearID;
            bool NowInCurrentYear;
            if (AcademicYearID.HasValue)
                NowInCurrentYear = this.AcademicYearBusiness.CheckTimeInYear(AcademicYearID.Value, DateTime.Now);
            else
                NowInCurrentYear = false;
            ViewData[WeeklyMenuAdditionalConstants.NowInCurrentYear] = NowInCurrentYear;
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        // 
        public JsonResult Create(WeeklyMenuAdditionalViewModel frm, int?[] DailyMenuID, int[] DayOfWeek)
        {
            WeeklyMenu weeklymenu = new WeeklyMenu();
            TryUpdateModel(weeklymenu);
            weeklymenu.SchoolID = _globalInfo.SchoolID.Value;
            weeklymenu.AcademicYearID = _globalInfo.AcademicYearID.Value;
            weeklymenu.IsActive = true;
            Utils.Utils.TrimObject(weeklymenu);

            List<WeeklyMenuDetail> Lstweeklymenudetail = new List<WeeklyMenuDetail>();
            if (DailyMenuID != null)
            {
                for (int i = 0; i < DayOfWeek.Count(); i++)
                {
                    if (DailyMenuID[i].HasValue)
                    {
                        WeeklyMenuDetail weeklymenudetail = new WeeklyMenuDetail();
                        weeklymenudetail.DayOfWeek = (int)DayOfWeek[i];
                        weeklymenudetail.DailyMenuID = DailyMenuID[i].Value;
                        Lstweeklymenudetail.Add(weeklymenudetail);
                    }
                }
            }
            using (TransactionScope scope = new TransactionScope())
            {
                this.WeeklyMenuBusiness.InsertAdditionalMenu(weeklymenu, Lstweeklymenudetail);
                this.WeeklyMenuBusiness.Save();
                this.WeeklyMenuDetailBusiness.Save();
                scope.Complete();
                return Json("20");
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult CreateChildren(int WeeklyMenuID, int?[] Check)
        {
            List<int> ListPupilID = new List<int>();
            if (Check != null)
            {
                for (int i = 0; i < Check.Count(); i++)
                {
                    ListPupilID.Add(Check[i].Value);
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ErrorCheckRow"), "error"));
            }

            using (TransactionScope scope = new TransactionScope())
            {
                this.AddMenuForChildrenBusiness.InsertAll(ListPupilID, new GlobalInfo().SchoolID.Value, WeeklyMenuID);
                this.AddMenuForChildrenBusiness.Save();
                scope.Complete();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int WeeklyMenuID, WeeklyMenuAdditionalViewModel frm, int?[] DailyMenuID, int[] DayOfWeek)
        {
            CheckPermissionForAction(WeeklyMenuID, "WeeklyMenu");
            WeeklyMenu weeklymenu = WeeklyMenuBusiness.Find(WeeklyMenuID);
            TryUpdateModel(weeklymenu);
            Utils.Utils.TrimObject(weeklymenu);

            List<WeeklyMenuDetail> Lstweeklymenudetail = new List<WeeklyMenuDetail>();
            if (DailyMenuID != null)
            {
                for (int i = 0; i < DayOfWeek.Count(); i++)
                {
                    if (DailyMenuID[i].HasValue)
                    {
                        WeeklyMenuDetail weeklymenudetail = new WeeklyMenuDetail();
                        weeklymenudetail.DayOfWeek = (int)DayOfWeek[i];
                        weeklymenudetail.DailyMenuID = DailyMenuID[i].Value;
                        Lstweeklymenudetail.Add(weeklymenudetail);
                    }
                }
            }

            using (TransactionScope scope = new TransactionScope())
            {
                this.WeeklyMenuBusiness.UpdateAdditionalMenu(weeklymenu, Lstweeklymenudetail);
                this.WeeklyMenuBusiness.Save();
                this.WeeklyMenuDetailBusiness.Save();
                scope.Complete();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }


        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            CheckPermissionForAction(id, "WeeklyMenu");
            this.WeeklyMenuBusiness.DeleteAdditionalMenu(id, new GlobalInfo().SchoolID.Value);
            this.WeeklyMenuBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult DeleteChildren(int?[] Check)
        {
            List<int> LstAddMenuForChildren = new List<int>();
            if (Check != null)
            {
                for (int i = 0; i < Check.Count(); i++)
                {
                    LstAddMenuForChildren.Add(Check[i].Value);
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ErrorCheckRow"), "error"));
            }
            this.AddMenuForChildrenBusiness.DeleteAll(LstAddMenuForChildren, new GlobalInfo().SchoolID.Value);
            this.AddMenuForChildrenBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<WeeklyMenuAdditionalViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<WeeklyMenu> query = this.WeeklyMenuBusiness.SearchAdditionalMenu(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<WeeklyMenuAdditionalViewModel> lst = query.Select(o => new WeeklyMenuAdditionalViewModel
            {
                WeeklyMenuID = o.WeeklyMenuID,
                WeeklyMenuCode = o.WeeklyMenuCode,
                MenuType = o.MenuType,
                Note = o.Note,
                StartDate = o.StartDate,
                EatingGroupID = o.EatingGroupID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate,
                EatingGroupName = o.EatingGroup.EatingGroupName


            });
            List<WeeklyMenuAdditionalViewModel> ListWeekleMenu = lst.ToList();
            foreach (var item in ListWeekleMenu)
            {
                item.DateApply = item.StartDate.ToString().Substring(0, 10) + "-" + (item.StartDate.AddDays(5)).ToString().Substring(0, 10);
            }
            return ListWeekleMenu.ToList();
        }
        private IEnumerable<PupilProfileViewModel> _SearchChildren(IDictionary<string, object> SearchInfo)
        {
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            IQueryable<AddMenuForChildren> query = this.AddMenuForChildrenBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<PupilProfileViewModel> lst = query.Select(o => new PupilProfileViewModel
            {
                AddMenuForChildrenID = o.AddMenuForChildrenID,
                WeeklyMenuID = o.WeeklyMenuID,
                PupilID = o.PupilID,
                PupilCode = o.PupilProfile.PupilCode,
                FullName = o.PupilProfile.FullName,
                BirthDate = o.PupilProfile.BirthDate,
                GenreName = (o.PupilProfile.Genre == (int)SystemParamsInFile.GENRE_MALE) ? male : female,
                ClassName = o.ClassProfile.DisplayName,
                EvaluationConfigName = o.PupilProfile.EvaluationConfig.EvaluationConfigName

            }).OrderBy(p => p.FullName);
            return lst.ToList();
        }
        private IEnumerable<PupilProfileViewModel> _SearchChildrenInsert(IDictionary<string, object> SearchInfo)
        {
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            IEnumerable<PupilProfileViewModel> lstInserted = this._SearchChildren(SearchInfo); // list hs đã insert
            IEnumerable<int> lstInt = lstInserted.Select(p => p.PupilID);
            IQueryable<PupilOfClass> query = this.PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<PupilProfileViewModel> lst = query.Select(o => new PupilProfileViewModel
            {
                PupilID = o.PupilID,
                PupilCode = o.PupilProfile.PupilCode,
                FullName = o.PupilProfile.FullName,
                BirthDate = o.PupilProfile.BirthDate,
                GenreName = (o.PupilProfile.Genre == (int)SystemParamsInFile.GENRE_MALE) ? male : female,
                ClassName = o.ClassProfile.DisplayName,
                EvaluationConfigName = o.PupilProfile.EvaluationConfig.EvaluationConfigName

            }).Where(p => !lstInt.Contains(p.PupilID)).OrderBy(p => p.FullName);
            return lst.ToList();
        }

        [ValidateAntiForgeryToken]
        public ActionResult ChooseChildren(int WeeklyMenuID)
        {
            WeeklyMenu WeeklyMenu = WeeklyMenuBusiness.Find(WeeklyMenuID);
            ViewData[WeeklyMenuAdditionalConstants.EatingGroup] = Utils.Utils.htmlEndCode(WeeklyMenu.EatingGroup.EatingGroupName);
            int EatingGroupID = WeeklyMenu.EatingGroup.EatingGroupID;
            //Trả về trang thông báo lỗi “Bạn không có quyền truy cập vào dữ liệu này”
            GlobalInfo global = new GlobalInfo();
            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, 0))
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            ClassSearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            IQueryable<ClassProfile> lsClassProfile = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, ClassSearchInfo).OrderBy(p => p.DisplayName);
            IQueryable<ClassProfile> ListClassProfile = (from listClass in lsClassProfile
                                                         join eatinggroupdetail in EatingGroupClassBusiness.All on listClass.ClassProfileID equals eatinggroupdetail.ClassID
                                                         join eatinggroup in EatingGroupBusiness.All on eatinggroupdetail.EatingGroupID equals eatinggroup.EatingGroupID
                                                         where eatinggroup.EatingGroupID == EatingGroupID
                                                         select listClass).OrderBy(p => p.DisplayName); 
            if (ListClassProfile != null)
            {
                ViewData[WeeklyMenuAdditionalConstants.LIST_CLASS] = new SelectList(ListClassProfile.ToList(), "ClassProfileID", "DisplayName");
            }

            //-	cboSex: CommonList.GenreAndAll
            ViewData[WeeklyMenuAdditionalConstants.LIST_GENRE] = new SelectList(CommonList.GenreAndAll(), "key", "value");

            //Get view data here           
            ViewData[WeeklyMenuAdditionalConstants.List_PupilInsert] = null;
            return PartialView("ChooseChildren");
        }
        public ActionResult CreatePage()
        {

            //Kiểm tra UserInfo. HasHeadTeacherPermission (0) = false:
            //Trả về trang thông báo lỗi “Bạn không có quyền truy cập vào dữ liệu này”
            GlobalInfo global = new GlobalInfo();
            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, 0))
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            IQueryable<EatingGroup> lsEatingGroup = EatingGroupBusiness.SearchEatingGroup(SearchInfo);

            ViewData[WeeklyMenuAdditionalConstants.List_EatingGroup] = new SelectList(lsEatingGroup.ToList(), "EatingGroupID", "EatingGroupName");

            IDictionary<string, object> MealCatSearchInfo = new Dictionary<string, object>();
            IQueryable<MealCat> lsMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, MealCatSearchInfo);
            ViewData[WeeklyMenuAdditionalConstants.List_MealCat] = lsMealCat.ToList();

            List<WeeklyMenuDetailViewModel> lstWeeklyMenuDetail = new List<WeeklyMenuDetailViewModel>();

            WeeklyMenuDetailViewModel WeeklyMenuDetailViewModel1 = new WeeklyMenuDetailViewModel();
            WeeklyMenuDetailViewModel1.DayOfWeek = SystemParamsInFile.DAY_OF_WEEK_MONDAY;
            WeeklyMenuDetailViewModel1.DayName = Thu + " " + SystemParamsInFile.DAY_OF_WEEK_MONDAY.ToString();
            lstWeeklyMenuDetail.Add(WeeklyMenuDetailViewModel1);

            WeeklyMenuDetailViewModel WeeklyMenuDetailViewModel2 = new WeeklyMenuDetailViewModel();
            WeeklyMenuDetailViewModel2.DayOfWeek = SystemParamsInFile.DAY_OF_WEEK_TUESDAY;
            WeeklyMenuDetailViewModel2.DayName = Thu + " " + SystemParamsInFile.DAY_OF_WEEK_TUESDAY.ToString();
            lstWeeklyMenuDetail.Add(WeeklyMenuDetailViewModel2);

            WeeklyMenuDetailViewModel WeeklyMenuDetailViewModel3 = new WeeklyMenuDetailViewModel();
            WeeklyMenuDetailViewModel3.DayOfWeek = SystemParamsInFile.DAY_OF_WEEK_WEDNESDAY;
            WeeklyMenuDetailViewModel3.DayName = Thu + " " + SystemParamsInFile.DAY_OF_WEEK_WEDNESDAY.ToString();
            lstWeeklyMenuDetail.Add(WeeklyMenuDetailViewModel3);

            WeeklyMenuDetailViewModel WeeklyMenuDetailViewModel4 = new WeeklyMenuDetailViewModel();
            WeeklyMenuDetailViewModel4.DayOfWeek = SystemParamsInFile.DAY_OF_WEEK_THURSDAY;
            WeeklyMenuDetailViewModel4.DayName = Thu + " " + SystemParamsInFile.DAY_OF_WEEK_THURSDAY.ToString();
            lstWeeklyMenuDetail.Add(WeeklyMenuDetailViewModel4);

            WeeklyMenuDetailViewModel WeeklyMenuDetailViewModel5 = new WeeklyMenuDetailViewModel();
            WeeklyMenuDetailViewModel5.DayOfWeek = SystemParamsInFile.DAY_OF_WEEK_FRIDAY;
            WeeklyMenuDetailViewModel5.DayName = Thu + " " + SystemParamsInFile.DAY_OF_WEEK_FRIDAY.ToString();
            lstWeeklyMenuDetail.Add(WeeklyMenuDetailViewModel5);

            WeeklyMenuDetailViewModel WeeklyMenuDetailViewModel6 = new WeeklyMenuDetailViewModel();
            WeeklyMenuDetailViewModel6.DayOfWeek = SystemParamsInFile.DAY_OF_WEEK_SATURDAY;
            WeeklyMenuDetailViewModel6.DayName = Thu + " " + SystemParamsInFile.DAY_OF_WEEK_SATURDAY.ToString();
            lstWeeklyMenuDetail.Add(WeeklyMenuDetailViewModel6);

            ViewData[WeeklyMenuAdditionalConstants.LIST_WEEKLYMENUDETAIL] = lstWeeklyMenuDetail;

            List<ComboObject> listDalyMenu = new List<ComboObject>();
            listDalyMenu.Add(new ComboObject(Res.Get("CHOICE"), "0"));
            ViewData[WeeklyMenuAdditionalConstants.LIST_DalyMenu] = listDalyMenu;


            DateTime DateFirstOfWeek = DateTime.Now.StartOfWeekGreaterNow();
            ViewData[WeeklyMenuAdditionalConstants.DateFirstOfWeek] = DateFirstOfWeek.ToString();
            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        public ActionResult EditPage(int WeeklyMenuID)
        {
            GlobalInfo global = new GlobalInfo();
            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, 0))
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            IQueryable<EatingGroup> lsEatingGroup = EatingGroupBusiness.SearchEatingGroup(SearchInfo);

            ViewData[WeeklyMenuAdditionalConstants.List_EatingGroup] = new SelectList(lsEatingGroup.ToList(), "EatingGroupID", "EatingGroupName");

            IDictionary<string, object> MealCatSearchInfo = new Dictionary<string, object>();
            IQueryable<MealCat> lsMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, MealCatSearchInfo);
            ViewData[WeeklyMenuAdditionalConstants.List_MealCat] = lsMealCat.ToList();

            IDictionary<string, object> WeeklyMenuDetailSearchInfo = new Dictionary<string, object>();
            WeeklyMenuDetailSearchInfo["WeeklyMenuID"] = WeeklyMenuID;
            IQueryable<WeeklyMenuDetail> lsWeeklyMenuDetail = WeeklyMenuDetailBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, WeeklyMenuDetailSearchInfo);
            List<WeeklyMenuDetailViewModel> LstWeeklyMenuDetailViewModel = new List<WeeklyMenuDetailViewModel>();

            foreach (var item in lsWeeklyMenuDetail)
            {
                WeeklyMenuDetailViewModel WeeklyMenuDetailViewModel = new WeeklyMenuDetailViewModel();
                WeeklyMenuDetailViewModel.DailyMenuID = item.DailyMenuID;
                WeeklyMenuDetailViewModel.WeeklyMenuDetailID = item.WeeklyMenuDetailID;
                WeeklyMenuDetailViewModel.DayOfWeek = item.DayOfWeek.Value;
                WeeklyMenuDetailViewModel.WeeklyMenuID = item.WeeklyMenuID;
                WeeklyMenuDetailViewModel.DayName = Thu + " " + item.DayOfWeek.Value.ToString();
                LstWeeklyMenuDetailViewModel.Add(WeeklyMenuDetailViewModel);
            }
            ViewData[WeeklyMenuAdditionalConstants.LIST_WEEKLYMENUDETAIL] = LstWeeklyMenuDetailViewModel;

            WeeklyMenu WeeklyMenu = WeeklyMenuBusiness.Find(WeeklyMenuID);

            IDictionary<string, object> DailyMenuSearchInfo = new Dictionary<string, object>();
            DailyMenuSearchInfo["EatingGroupID"] = WeeklyMenu.EatingGroupID;
            DailyMenuSearchInfo["MenuType"] = SystemParamsInFile.MENU_TYPE_ADDITIONAL;
            DailyMenuSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            List<DailyMenu> lstDalyMenu = DailyMenuBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, DailyMenuSearchInfo).ToList();

            List<ComboObject> listDalyMenu = new List<ComboObject>();
            listDalyMenu.Add(new ComboObject(Res.Get("CHOICE"), "0"));
            foreach (var item in lstDalyMenu)
            {
                listDalyMenu.Add(new ComboObject(item.DailyMenuCode, item.DailyMenuID.ToString()));
            }
            ViewData[WeeklyMenuAdditionalConstants.LIST_DalyMenu] = listDalyMenu;

            IDictionary<string, object> DailyMenuDetailSearchInfo = new Dictionary<string, object>();
            DailyMenuDetailSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            DailyMenuDetailSearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            List<DailyMenuDetail> lstDailyMenuDetail = DailyMenuDetailBusiness.Search(DailyMenuDetailSearchInfo).ToList();
            ViewData[WeeklyMenuAdditionalConstants.List_DailyMenuDetailALL] = lstDailyMenuDetail;

            DateTime DateFirstOfWeek = WeeklyMenu.StartDate;
            ViewData[WeeklyMenuAdditionalConstants.DateFirstOfWeek] = DateFirstOfWeek.ToString();

            WeeklyMenuAdditionalViewModel model = new WeeklyMenuAdditionalViewModel();
            Utils.Utils.BindTo(WeeklyMenu, model);

            return View("_Edit", model);
        }

        [ValidateAntiForgeryToken]
        public ActionResult DetailPage(int WeeklyMenuID)
        {

            IDictionary<string, object> MealCatSearchInfo = new Dictionary<string, object>();
            IQueryable<MealCat> lsMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, MealCatSearchInfo);
            ViewData[WeeklyMenuAdditionalConstants.List_MealCat] = lsMealCat.ToList();

            IDictionary<string, object> WeeklyMenuDetailSearchInfo = new Dictionary<string, object>();
            WeeklyMenuDetailSearchInfo["WeeklyMenuID"] = WeeklyMenuID;
            IQueryable<WeeklyMenuDetail> lsWeeklyMenuDetail = WeeklyMenuDetailBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, WeeklyMenuDetailSearchInfo);
            List<WeeklyMenuDetailViewModel> LstWeeklyMenuDetailViewModel = new List<WeeklyMenuDetailViewModel>();
            foreach (var item in lsWeeklyMenuDetail)
            {
                WeeklyMenuDetailViewModel WeeklyMenuDetailViewModel = new WeeklyMenuDetailViewModel();
                WeeklyMenuDetailViewModel.DailyMenuID = item.DailyMenuID;
                WeeklyMenuDetailViewModel.WeeklyMenuDetailID = item.WeeklyMenuDetailID;
                WeeklyMenuDetailViewModel.DayOfWeek = item.DayOfWeek.Value;
                WeeklyMenuDetailViewModel.WeeklyMenuID = item.WeeklyMenuID;
                WeeklyMenuDetailViewModel.DayName = Thu + " " + item.DayOfWeek.Value.ToString();
                WeeklyMenuDetailViewModel.DailyMenuCode = item.DailyMenu.DailyMenuCode;
                LstWeeklyMenuDetailViewModel.Add(WeeklyMenuDetailViewModel);
            }
            ViewData[WeeklyMenuAdditionalConstants.LIST_WEEKLYMENUDETAIL] = LstWeeklyMenuDetailViewModel;

            WeeklyMenu WeeklyMenu = WeeklyMenuBusiness.Find(WeeklyMenuID);

            IDictionary<string, object> DailyMenuDetailSearchInfo = new Dictionary<string, object>();
            DailyMenuDetailSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            DailyMenuDetailSearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            List<DailyMenuDetail> lstDailyMenuDetail = DailyMenuDetailBusiness.Search(DailyMenuDetailSearchInfo).ToList();
            ViewData[WeeklyMenuAdditionalConstants.List_DailyMenuDetailALL] = lstDailyMenuDetail;

            WeeklyMenuAdditionalViewModel model = new WeeklyMenuAdditionalViewModel();
            Utils.Utils.BindTo(WeeklyMenu, model);
            model.StartDate_string = model.StartDate.Date.ToString().Substring(0, 10);
            model.EndDate = model.StartDate.AddDays(5).Date.ToString().Substring(0, 10);
            model.EatingGroupName = WeeklyMenu.EatingGroup.EatingGroupName;
            return View("_Detail", model);
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadDalyMenu(int? eatingGroupID)
        {
            List<DailyMenu> lst = new List<DailyMenu>();
            if (eatingGroupID.ToString() != "")
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["MenuType"] = SystemParamsInFile.MENU_TYPE_ADDITIONAL;
                dic["EatingGroupID"] = eatingGroupID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;//new GlobalInfo().AcademicYearID;
                lst = this.DailyMenuBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), dic).ToList();
            }
            if (lst == null)
                lst = new List<DailyMenu>();
            return Json(new SelectList(lst, "DailyMenuID", "DailyMenuCode"));
        }


        [ValidateAntiForgeryToken]
        public ActionResult CreatePageChildren(int WeeklyMenuID)
        {
            WeeklyMenu WeeklyMenu = WeeklyMenuBusiness.Find(WeeklyMenuID);
            ViewData[WeeklyMenuAdditionalConstants.EatingGroup] = Utils.Utils.htmlEndCode(WeeklyMenu.EatingGroup.EatingGroupName);
            int EatingGroup = WeeklyMenu.EatingGroup.EatingGroupID;
            //Trả về trang thông báo lỗi “Bạn không có quyền truy cập vào dữ liệu này”
            GlobalInfo global = new GlobalInfo();
            if (!UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, 0))
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }

            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;//new GlobalInfo().AcademicYearID;
            ClassSearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;//new GlobalInfo().AppliedLevel;                        
            IQueryable<ClassProfile> lsClassProfile = ClassProfileBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), ClassSearchInfo);
            IQueryable<ClassProfile> ListClassProfile = (from listClass in lsClassProfile
                                                         join eatinggroupdetail in EatingGroupClassBusiness.All on listClass.ClassProfileID equals eatinggroupdetail.ClassID
                                                         join eatinggroup in EatingGroupBusiness.All on eatinggroupdetail.EatingGroupID equals eatinggroup.EatingGroupID
                                                         where eatinggroup.EatingGroupID == EatingGroup
                                                         select listClass).OrderBy(p => p.DisplayName);
            if (ListClassProfile != null)
            {
                ViewData[WeeklyMenuAdditionalConstants.LIST_CLASS] = new SelectList(ListClassProfile.ToList(), "ClassProfileID", "DisplayName");
            }
            IDictionary<string, object> EvaluationConfigSearchInfo = new Dictionary<string, object>();
            //EvaluationConfigSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            IQueryable<EvaluationConfig> lsEvaluationConfig = EvaluationConfigBusiness.Search(EvaluationConfigSearchInfo);
            if (lsEvaluationConfig != null)
            {
                ViewData[WeeklyMenuAdditionalConstants.List_EvaluationConfig] = new SelectList(lsEvaluationConfig.ToList(), "EvaluationConfigID", "EvaluationConfigName");
            }
            //Get view data here           
            ViewData[WeeklyMenuAdditionalConstants.List_Pupil] = new SelectList("", "", "");
            return PartialView("_CreateChildren");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadMeal(int? dailyMenuID)
        {
            List<DailyViewModel> lstDailyBO = new List<DailyViewModel>();
            if (dailyMenuID.ToString() != "" && dailyMenuID.ToString() != "0")
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["DailyMenuID"] = dailyMenuID;
                var lst = this.DailyMenuDetailBusiness.Search(dic);

                bool getFromDishCat = this.DailyMenuBusiness.Find(dailyMenuID.Value).GetFromDishCat;
                if (getFromDishCat == true)
                {
                    foreach (var item in lst)
                    {
                        DailyViewModel DailyBO = new DailyViewModel();
                        DailyBO.DailyMenuID = item.DailyMenuID;
                        DailyBO.DishName = item.DishCat.DishName;
                        DailyBO.MealID = item.MealID;
                        lstDailyBO.Add(DailyBO);
                    }
                }
                else
                {
                    foreach (var item in lst)
                    {
                        DailyViewModel DailyBO = new DailyViewModel();
                        DailyBO.DailyMenuID = item.DailyMenuID;
                        DailyBO.DishName = item.DishName;
                        DailyBO.MealID = item.MealID;
                        lstDailyBO.Add(DailyBO);
                    }
                }

            }
            return Json(lstDailyBO);


        }

        public FileResult ExportExcel(int WeeklyMenuID)
        {
            IDictionary<string, object> MealCatSearchInfo = new Dictionary<string, object>();
            IQueryable<MealCat> lsMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, MealCatSearchInfo);

            IDictionary<string, object> WeeklyMenuDetailSearchInfo = new Dictionary<string, object>();
            WeeklyMenuDetailSearchInfo["WeeklyMenuID"] = WeeklyMenuID;
            IQueryable<WeeklyMenuDetail> lsWeeklyMenuDetail = WeeklyMenuDetailBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, WeeklyMenuDetailSearchInfo);

            WeeklyMenu WeeklyMenu = WeeklyMenuBusiness.Find(WeeklyMenuID);

            IDictionary<string, object> DailyMenuDetailSearchInfo = new Dictionary<string, object>();
            DailyMenuDetailSearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            DailyMenuDetailSearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            List<DailyMenuDetail> lstDailyMenuDetail = DailyMenuDetailBusiness.Search(DailyMenuDetailSearchInfo).ToList();

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "MN" + "/" + "ThucDonTuan.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            SchoolProfile school = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID);

            //Fill dữ liệu chung
            string schoolName = school.SchoolName.ToUpper();

            sheet.SetCellValue("A2", school.SupervisingDept.SupervisingDeptName);
            sheet.SetCellValue("A3", schoolName);
            sheet.SetCellValue("D4", school.Province.ProvinceName + ", " + "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            sheet.SetCellValue("A6", "THỰC ĐƠN TUẦN BỔ SUNG " + WeeklyMenu.StartDate.ToString().Substring(0, 10) + " - " + WeeklyMenu.StartDate.AddDays(5).ToString().Substring(0, 10));
            sheet.SetCellValue("A7", "THỰC ĐƠN: " + WeeklyMenu.WeeklyMenuCode.ToUpper() + " - " + "NHÓM ĂN: " + WeeklyMenu.EatingGroup.EatingGroupName.ToUpper());

            IVTRange range = sheet.GetRange("C9", "C9");
            IVTRange range1 = sheet.GetRange("C10", "C10");
            int d = 0;
            foreach (var item in lsMealCat)
            {
                d = d + 1;
                //Copy row style
                sheet.CopyPasteSameSize(range, 9, 2 + d);
                sheet.CopyPasteSameSize(range1, 10, 2 + d);
                sheet.SetCellValue(9, 2 + d, item.MealName);
            }
            int firstRow = 10;
            range = sheet.GetRange(10, 1, 10, 2 + d);
            int headerStartCol = 0;
            int index = 0;
            foreach (var item in lsWeeklyMenuDetail)
            {
                int currentRow = firstRow + index;
                index++;
                //Cot diem dau tien     
                headerStartCol = 3;
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, currentRow);
                //Fill dữ liệu
                sheet.SetCellValue("B" + currentRow, "THỨ " + item.DayOfWeek);
                foreach (MealCat MealCat in lsMealCat)
                {
                    string DishName = "";
                    IEnumerable<DailyMenuDetail> LstDailyMenuDetailOfMeal = lstDailyMenuDetail.Where(o => o.DailyMenuID == item.DailyMenuID && o.MealID == MealCat.MealCatID);
                    if (LstDailyMenuDetailOfMeal != null && LstDailyMenuDetailOfMeal.Count() > 0)
                    {
                        foreach (DailyMenuDetail DailyMenuDetail in LstDailyMenuDetailOfMeal)
                        {
                            if (DailyMenuDetail.DailyMenu.GetFromDishCat == true)
                            {
                                if (DishName == "")
                                {
                                    DishName = DailyMenuDetail.DishCat.DishName;
                                }
                                else
                                {
                                    DishName = DishName + "\n" + DailyMenuDetail.DishCat.DishName;
                                }

                            }
                            else
                            {
                                if (DishName == "")
                                {
                                    DishName = DailyMenuDetail.DishName;
                                }
                                else
                                {
                                    DishName = DishName + "\n" + DailyMenuDetail.DishName;
                                }
                            }
                        }
                    }
                    sheet.SetCellValue(currentRow, headerStartCol, DishName);
                    headerStartCol = headerStartCol + 1;
                }
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            string ReportName = "ThucDonTuanBoSung_" + WeeklyMenu.WeeklyMenuCode + ".xls";
            result.FileDownloadName = ReportName;
            return result;
        }
    }
}





