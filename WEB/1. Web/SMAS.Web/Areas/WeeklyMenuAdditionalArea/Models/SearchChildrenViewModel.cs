using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using Resources;

namespace SMAS.Web.Areas.WeeklyMenuAdditionalArea.Models
{
    public class SearchChildrenViewModel
    {
        [ScaffoldColumn(false)]
        public int? WeeklyMenuID { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_EatingGroup")]
        [UIHint("Textbox")]
        public string EatingGroupName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", WeeklyMenuAdditionalConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]       
        public int? Class { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", WeeklyMenuAdditionalConstants.LIST_GENRE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Genre { get; set; }

        [ResourceDisplayName("PupilProfileChildren_Label_PupilCode")]
        [UIHint("Textbox")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [UIHint("Textbox")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Fullname { get; set; }

    }
}
