/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.WeeklyMenuAdditionalArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("WeeklyMenu_Label_WeeklyMenuCode")]
        [UIHint("Textbox")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string WeeklyMenuCode { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_EatingGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", WeeklyMenuAdditionalConstants.List_EatingGroup)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? EatingGroupID { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_StartDate")]
        [UIHint("DateTimePicker")]
        public DateTime? StartDate { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_EndDate")]
        [UIHint("DateTimePicker")]
        public DateTime? EndDate { get; set; }	
    }
}