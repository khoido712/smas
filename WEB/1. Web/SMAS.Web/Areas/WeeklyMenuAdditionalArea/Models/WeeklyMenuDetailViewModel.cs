using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.WeeklyMenuAdditionalArea.Models
{
    public class WeeklyMenuDetailViewModel
    {
        public System.Int32 WeeklyMenuDetailID { get; set; }
        public System.Int32 DayOfWeek { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_DailyMenuCode")]
        public Nullable<System.Int32> DailyMenuID { get; set; }
        public System.Int32 WeeklyMenuID { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_DayName")]
        public System.String DayName { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_DailyMenuCodeDay")]
        public string DailyMenuCode { get; set; }
    }
}
