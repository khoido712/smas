﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using Resources;

namespace SMAS.Web.Areas.WeeklyMenuAdditionalArea.Models
{
    public class PupilProfileViewModel
    {
        public System.Int32 AddMenuForChildrenID { get; set; }
        public System.Int32 WeeklyMenuID { get; set; }
        public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("PupilProfileChildren_Label_PupilCode")]    
        public System.String PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Name")]     
        public System.String Name { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FullName")]      
        public System.String FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]       
        public System.DateTime BirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_EvaluationConfigName")]
        public string EvaluationConfigName { get; set; }        
    }
}