﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.WeeklyMenuAdditionalArea.Models
{
    public class SearchInsertChildrenViewModel
    {

        [ScaffoldColumn(false)]
        public int? WeeklyMenuID { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_EatingGroup")]
        [UIHint("Textbox")]
        public string EatingGroupName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", WeeklyMenuAdditionalConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxGetClassID(this)")]
        public int? Class { get; set; }

        [ResourceDisplayName("PupilProfile_Label_EvaluationConfig")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", WeeklyMenuAdditionalConstants.List_EvaluationConfig)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? EvaluationConfigID { get; set; }       

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [UIHint("Textbox")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Fullname { get; set; }
    }
}