﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilAbsenceArea
{
    public class PupilAbsenceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilAbsenceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilAbsenceArea_default",
                "PupilAbsenceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
