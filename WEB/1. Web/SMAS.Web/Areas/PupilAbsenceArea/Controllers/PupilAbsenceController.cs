﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  NAMTA
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.PupilAbsenceArea.Models;
using SMAS.VTUtils;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using System.Globalization;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.PupilAbsenceArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilAbsenceController : BaseController
    {
        GlobalInfo glo = new GlobalInfo();

        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilFaultBusiness PupilFaultBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ILockTrainingBusiness LockTrainingBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;

        public const int absentOn = 1;
        public const int absentOff = 2;
        public const int absentWeekend = 3;
        public const string absent_P = "P";
        public const string absent_K = "K";

        public PupilAbsenceController(IPupilAbsenceBusiness pupilabsenceBusiness, IClassProfileBusiness classprofileBusiness, IPupilOfClassBusiness pupilOfClassBusiness, IPupilFaultBusiness pupilFaultBusiness, IAcademicYearBusiness academicYearBusiness,
            ILockTrainingBusiness lockTrainingBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, ISchoolProfileBusiness SchoolProfileBusiness, ITeachingAssignmentBusiness TeachingAssignmentBusiness, IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness)
        {
            this.PupilAbsenceBusiness = pupilabsenceBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.PupilFaultBusiness = pupilFaultBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.LockTrainingBusiness = lockTrainingBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
        }

        public int CheckIsShowPupil()
        {
            AcademicYear academic = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            if (academic != null && academic.IsShowPupil == true)
                return 1;
            else
                return 0;
        }

        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.GetValueOrDefault());

            if (_globalInfo.AppliedLevel <= 3)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                GlobalInfo global = GlobalInfo.getInstance();

                //Hiển thị danh sách lớp được hiển thị 
                Dictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", global.AcademicYearID},
                {"AppliedLevel", global.AppliedLevel}
                };

                if (!academicYear.IsTeacherCanViewAll && !global.IsAdminSchoolRole && !global.IsEmployeeManager)
                {
                    dic["UserAccountID"] = global.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING;
                }
                List<ClassProfile> listCP = ClassProfileBusiness.SearchBySchool(
                    global.SchoolID.GetValueOrDefault(), dic)
                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[PupilAbsenceConstants.LIST_CLASS] = listCP;

                ClassProfile cp = null;
                if (listCP != null && listCP.Count > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        if (!global.IsAdminSchoolRole || !global.IsEmployeeManager)
                        {
                            dic["UserAccountID"] = global.UserAccountID;
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                            List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(
                                global.SchoolID.GetValueOrDefault(), dic)
                                .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listHead != null && listHead.Count > 0)
                            {
                                cp = listHead.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_OVERSEEING;
                                List<ClassProfile> listOverSee = ClassProfileBusiness.SearchBySchool(
                                    global.SchoolID.GetValueOrDefault(), dic)
                                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listOverSee != null && listOverSee.Count > 0)
                                {
                                    cp = listOverSee.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }
            else
            {   // Cap mam non
                IDictionary<string, object> dic = new Dictionary<string, object>();
                ClassProfile cp = null;
                dic = new Dictionary<string, object>();

                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_TEACHING;
                }

                List<ClassProfile> listClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                            .OrderBy(x => x.EducationLevelID)
                                                                            .ThenBy(x => x.DisplayName).ToList();

                ViewData[PupilAbsenceConstants.LIST_CLASS] = listClass;
                var listCP = listClass.ToList();

                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            //Combo section
            List<ComboObject> listSection = new List<ComboObject>();
            ViewData[PupilAbsenceConstants.LIST_SECTION] = listSection;

            //Combo Month
            List<ComboObject> listMonth = new List<ComboObject>();
            if (academicYear.FirstSemesterStartDate != null && academicYear.SecondSemesterEndDate != null)
            {
                DateTime begin = academicYear.FirstSemesterStartDate.Value.StartOfMonth();
                while (begin <= academicYear.SecondSemesterEndDate.Value.EndOfMonth()
                    && begin <= DateTime.Now.EndOfMonth())
                {
                    listMonth.Add(new ComboObject(begin.Month.ToString(), begin.ToString()));
                    begin = begin.AddMonths(1);
                }
            }
            ViewData[PupilAbsenceConstants.LIST_MONTH] = listMonth;
            ViewData[PupilAbsenceConstants.ACADEMIC_YEAR] = academicYear;

            return PartialView();
        }

        /// <summary>
        /// Cap nhat thong tin diem danh hoc sinh
        /// </summary>
        /// <param name="model"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_ADD)]
        public JsonResult Create(PupilAbsenceViewModel model, FormCollection col)
        {
            //lay thong tin tu form
            string _class = col["class"];
            string _section = col["section"];
            string _date = col["date"];
            DateTime? date = DateTime.Parse(_date);
            int? classId = SMAS.Business.Common.Utils.GetInt(_class);
            int? sectionId = SMAS.Business.Common.Utils.GetInt(_section);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = _class;
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            //Chiendd1: 02/07/2015, Bo sung truong partitionId
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).Where(o => o.PupilProfile.IsActive == true);
            if (CheckIsShowPupil() == 1)
            {
                listPupilOfClass = listPupilOfClass.Where(x => x.Status == 1 || x.Status == 2);
            }

            List<PupilAbsence> listPupilAbsence = new List<PupilAbsence>();
            //Danh sach hoc sinh tu lop hoc
            foreach (var Pupil in listPupilOfClass)
            {
                string absent = col[Pupil.PupilID.ToString()];
                string disabled = col[Pupil.PupilID.ToString() + "_Disabled"].ToString();

                if (disabled.Equals("Disabled"))
                {
                    continue;
                }

                int? ab = 0;
                if (absent == null)
                {

                    continue;
                }
                else
                {
                    ab = SMAS.Business.Common.Utils.GetInt(absent[0]);
                }
                if (ab == 0)
                {
                    continue;
                }
                else if (ab == absentOn || ab == absentOff)
                {
                    //Neu trang thai diem danh la 1 hoac la 2 thi tien hanh tao doi tuong diem danh
                    PupilAbsence PupilAbsence = new PupilAbsence();
                    PupilAbsence.PupilID = Pupil.PupilID;
                    PupilAbsence.ClassID = classId.Value;
                    PupilAbsence.SchoolID = new GlobalInfo().SchoolID.Value;
                    PupilAbsence.AbsentDate = date.Value;
                    PupilAbsence.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                    PupilAbsence.EducationLevelID = Pupil.ClassProfile.EducationLevelID;
                    PupilAbsence.Section = (int)sectionId.Value;
                    PupilAbsence.PupilCode = PupilAbsence.PupilCode;
                    if (ab == 1) PupilAbsence.IsAccepted = false;
                    else if (ab == 2) PupilAbsence.IsAccepted = true;
                    PupilAbsence.Last2digitNumberSchool = partitionId;
                    listPupilAbsence.Add(PupilAbsence);
                }
                else
                {
                    continue;
                }
            }
            //Them moi doi tuong
            PupilAbsenceBusiness.InsertOrUpdatePupilAbsence(_globalInfo.UserAccountID, _globalInfo.IsAdmin, 0, classId.Value, sectionId.Value, date.Value, date.Value, listPupilAbsence);
            //Cap nhat doi tuong
            PupilAbsenceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //

            IEnumerable<PupilAbsenceViewModel> lst = this._Search(SearchInfo);
            ViewData[PupilAbsenceConstants.LIST_PUPILABSENCE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.PupilAbsenceBusiness.Delete(id);
            this.PupilAbsenceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        private IEnumerable<PupilAbsenceViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<PupilAbsence> query = this.PupilAbsenceBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<PupilAbsenceViewModel> lst = query.Select(o => new PupilAbsenceViewModel
            {



            });

            return null;
        }

        [HttpPost]
        public FileResult ExportFile(FormCollection form)
        {
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int SchoolID = new GlobalInfo().SchoolID.Value;
            int ClassID = SMAS.Business.Common.Utils.GetInt(form["ClassID"]);
            int section = SMAS.Business.Common.Utils.GetInt(form["Section"]);
            int ButtonTypeExport = SMAS.Business.Common.Utils.GetInt(form["HiddenButtonTypeExport"]);
            int typeReport = SMAS.Business.Common.Utils.GetInt(form["typeReportExcel"]);

            String filename = "";
            Stream excel = null;
            if (ButtonTypeExport == 0) //điểm danh học sinh theo tháng
            {
                DateTime date = new DateTime();
                bool tmp = DateTime.TryParse(form["Month"].ToString(), out date);
                int employeeID = 0;
                if (_globalInfo.EmployeeID != null)
                    employeeID = _globalInfo.EmployeeID.Value;

                IDictionary<string, object> dicCP = new Dictionary<string, object>();
                dicCP["AcademicYearID"] = AcademicYearID;
                dicCP["AppliedLevel"] = _globalInfo.AppliedLevel;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dicCP["UserAccountID"] = _globalInfo.UserAccountID;
                    dicCP["Type"] = SystemParamsInFile.TEACHER_ROLE_TEACHING;
                }

                excel = this.PupilAbsenceBusiness.ExportReport(dicCP, SchoolID, AcademicYearID, section, ClassID,
                    date, _globalInfo.IsAdmin, out filename, typeReport, employeeID, _globalInfo.AppliedLevel.Value);
            }
            else //điểm danh học sinh theo tuần
            {
                DateTime dateWeek = new DateTime();
                bool tmp = DateTime.TryParse(form["AbsenceDate"].ToString(), out dateWeek);
                excel = this.ExportReportWeek(SchoolID, AcademicYearID, section, ClassID,
                    dateWeek, _globalInfo.IsAdmin, out filename, typeReport);
            }

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;

            return result;
        }

        public Stream ExportReportWeek(int SchoolID, int AcademicYearID, int Section, int ClassID, DateTime dateWeek, bool IsAdmin, out string FileName, int typeReport)
        {
            string reportCode = SystemParamsInFile.HS_BANGDIEMDANH_TUAN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            ClassProfile objClass = ClassProfileBusiness.Find(ClassID);
            string outputNamePattern = reportDef.OutputNamePattern;
            string typeName = "";
            if (typeReport == 1)
                typeName = objClass.DisplayName;
            else if (typeReport == 2)
                typeName = objClass.EducationLevel.Resolution;
            else if (typeReport == 3)
                typeName = "ToanTruong";

            outputNamePattern = outputNamePattern.Replace("Lop_", "_").Replace("[Class]", typeName);
            if (typeReport == 4)
                outputNamePattern = outputNamePattern.Replace("_", "");
            FileName =Utils.Utils.StripVNSignAndSpace(outputNamePattern) + "." + reportDef.OutputFormat;

            //Lấy sheet template
            IVTWorksheet firstSheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet1 = oBook.CopySheetToLast(firstSheet1);

            List<DateTime> lstDayByWeek = GetDayOfWeek(dateWeek);
            int educationLevelID = objClass.EducationLevelID;

            //Lấy danh sách học sinh và nghỉ
            DateTime dteStart = lstDayByWeek.Count > 0 ? lstDayByWeek[0] : DateTime.Now;
            DateTime dteEnd = lstDayByWeek.Count > 0 ? lstDayByWeek[lstDayByWeek.Count - 1] : DateTime.Now;
            IDictionary<string, object> dic = new Dictionary<string, object>();

            List<int> lstSectionType = new List<int>();
            PupilAbsenceBusiness.GetTypeSection(lstSectionType, Section);

            dic["AcademicYearID"] = AcademicYearID;
            dic["IsActive"] = 1;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            List<PupilOfClassBO> iqueryPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).Where(x => lstSectionType.Contains(x.ClassProfile.Section.Value))
                .Select(o => new PupilOfClassBO
                {
                    ClassID = o.ClassID,
                    PupilID = o.PupilID,
                    PupilCode = o.PupilProfile.PupilCode,
                    Name = o.PupilProfile.Name,
                    PupilFullName = o.PupilProfile.FullName,
                    OrderInClass = o.OrderInClass,
                    ClassOrder = o.ClassProfile.OrderNumber,
                    Status = o.Status,
                    EducationLevelID = o.ClassProfile.EducationLevelID,
                    ClassName = o.ClassProfile.DisplayName,
                    SectionID = o.ClassProfile.Section,
                }).ToList();
            if (CheckIsShowPupil() == 1)
            {
                iqueryPOC = iqueryPOC.Where(x => x.Status == 1 || x.Status == 2).ToList();
            }

            // danh sach ID Class
            List<int> lstClassID = new List<int>();
            if (typeReport == 1)
            {
                lstClassID = iqueryPOC.Where(x => x.ClassID == ClassID).ToList()
                                            .OrderBy(o => o.EducationLevelID)
                                            .ThenBy(o => o.ClassOrder)
                                            .ThenBy(o => o.ClassName)
                                            .Select(x => x.ClassID).Distinct().ToList();
            }
            else if (typeReport == 2)
            {
                lstClassID = iqueryPOC.Where(x => x.EducationLevelID == educationLevelID).ToList()
                                                    .OrderBy(o => o.EducationLevelID)
                                                    .ThenBy(o => o.ClassOrder)
                                                    .ThenBy(o => o.ClassName)
                                                    .Select(x => x.ClassID).Distinct().ToList();
            }
            else if (typeReport == 3)
            {
                lstClassID = iqueryPOC.ToList()
                            .OrderBy(o => o.EducationLevelID)
                            .ThenBy(o => o.ClassOrder)
                            .ThenBy(o => o.ClassName)
                            .Select(x => x.ClassID).Distinct().ToList();
            }
            else if (typeReport == 4)
            {
                List<int> lstCSA = new List<int>();
                if (_globalInfo.AppliedLevel <= 3)
                {
                    //Tìm các lớp giáo viên quyền chủ nhiệm, giám thị
                    IDictionary<string, object> dicCSA = new Dictionary<string, object>();
                    dicCSA["TeacherID"] = _globalInfo.EmployeeID;
                    dicCSA["AcademicYearID"] = _globalInfo.AcademicYearID;
                    lstCSA = ClassSupervisorAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCSA)
                        .Where(x => (x.PermissionLevel == 1 || x.PermissionLevel == 3) && x.TeacherID == _globalInfo.EmployeeID.Value).Select(x => x.ClassProfile.ClassProfileID).Distinct().ToList();

                    IDictionary<string, object> dicCP = new Dictionary<string, object>();
                    dicCP["AppliedLevel"] = _globalInfo.AppliedLevel;
                    dicCP["AcademicYearID"] = _globalInfo.AcademicYearID;
                    dicCP["HeadTeacherID"] = _globalInfo.EmployeeID;
                    List<int> lstCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCP).Where(x => x.IsActive == true && x.HeadTeacherID == _globalInfo.EmployeeID).Select(x => x.ClassProfileID).Distinct().ToList();
                    
                    lstCSA.AddRange(lstCP);
                }
                else
                {
                    IDictionary<string, object> dicCP = new Dictionary<string, object>();
                    dicCP["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                    dicCP["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                    if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                    {
                        dicCP["UserAccountID"] = _globalInfo.UserAccountID;
                        dicCP["Type"] = SystemParamsInFile.TEACHER_ROLE_TEACHING;
                    }
                    lstCSA = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dicCP).Select(x=>x.ClassProfileID).ToList();
                }

                if (lstCSA.Count() > 0)
                {
                    iqueryPOC = iqueryPOC.Where(x => lstCSA.Contains(x.ClassID)).ToList();
                    lstClassID = iqueryPOC.ToList()
                            .OrderBy(o => o.EducationLevelID)
                            .ThenBy(o => o.ClassOrder)
                            .ThenBy(o => o.ClassName)
                            .Select(x => x.ClassID).Distinct().ToList();
                }
            }

            dic = new Dictionary<string, object>();
            //dic["ClassID"] = ClassID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["Section"] = Section;
            dic["FromAbsentDate"] = dteStart;
            dic["ToAbsentDate"] = dteEnd;
            dic["lstClassID"] = lstClassID;

            if (typeReport == 1)
                dic["ClassID"] = ClassID;

            if (typeReport == 2)
                dic["EducationLevelID"] = educationLevelID;
            List<PupilAbsence> listPupilAbsenceAll = PupilAbsenceBusiness.SearchBySchool(SchoolID, dic).ToList();
            List<PupilOfClassBO> listPupilOfClassAll = iqueryPOC.Where(p => lstClassID.Contains(p.ClassID)).ToList()
                                                        .OrderBy(o => o.OrderInClass)
                                                        .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name))
                                                        .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilFullName)).ToList();

            // Lay thong tin thoi gian bi khoa diem danh
            List<LockTraining> lstlockTraining = new List<LockTraining>();
            LockTraining lockTraining = null;
            if (!IsAdmin)
            {
                IDictionary<string, object> dicLockTraining = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"IsAbsence", true}
                };
                lstlockTraining = LockTrainingBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicLockTraining).Where(p => lstClassID.Contains(p.ClassID)).ToList();
            }

            // Các danh sách và biến 
            List<VTDataValidation> lstValidate = new List<VTDataValidation>();
            List<PupilAbsence> listPupilAbsence = new List<PupilAbsence>();
            int startRow, indexPupil, indexRow;
            int index = 0;
            string className = "";
            SchoolProfile objSchoolProfile = SchoolProfileBusiness.Find(SchoolID);
            string supervisingName = UtilsBusiness.GetSupervisingDeptName(SchoolID, _globalInfo.AppliedLevel.Value);
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            DateTime startYear = academicYear.FirstSemesterStartDate.Value;
            DateTime endYear = academicYear.SecondSemesterEndDate.Value;
            // fill dữ liệu
            for (int i = 0; i < lstClassID.Count(); i++)
            {
                index++;
                IVTWorksheet firstSheet = oBook.CopySheetToLast(firstSheet1);
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                IVTRange yellowRow = firstSheet.GetRow(10);
                IVTRange niceRow = sheet.GetRow(9);

                var listPupilOfClass = listPupilOfClassAll.Where(x => x.ClassID == lstClassID[i]);
                listPupilAbsence = listPupilAbsenceAll.Where(x => x.ClassID == lstClassID[i]).ToList();
                #region for
                //Change header
                className = listPupilOfClass.First().ClassName.ToUpper().Replace("LỚP", "").Trim();
                String title = "BẢNG ĐIỂM DANH TỪ NGÀY ";
                title += lstDayByWeek[0].ToShortDateString();
                title += " ĐẾN NGÀY " + lstDayByWeek[lstDayByWeek.Count - 1].ToShortDateString();
                title += " - ";
                title += " LỚP ";
                title += className;
                title += " - ";
                if (Section == 1)
                    title += " BUỔI SÁNG ";
                else if (Section == 3)
                    title += " BUỔI TỐI ";
                else
                    title += " BUỔI CHIỀU ";

                sheet.SetCellValue("A3", objSchoolProfile.SchoolName.ToUpper());
                sheet.SetCellValue("A2", supervisingName.ToUpper());
                sheet.SetCellValue("A6", title);

                string datename = string.Empty;
                //load cac ngay trong tuan
                foreach (var dt in lstDayByWeek)
                {
                    datename += dt.Day.ToString().Length > 1 ? dt.Day.ToString() : "0" + dt.Day.ToString();
                    datename += "/";
                    datename += dt.Month.ToString().Length > 1 ? dt.Month.ToString() : "0" + dt.Month.ToString();

                    switch (dt.DayOfWeek)
                    {
                        case DayOfWeek.Monday:
                            sheet.SetCellValue("E8", datename);
                            datename = string.Empty;
                            continue;
                        case DayOfWeek.Tuesday:
                            sheet.SetCellValue("F8", datename);
                            datename = string.Empty;
                            continue;
                        case DayOfWeek.Wednesday:
                            sheet.SetCellValue("G8", datename);
                            datename = string.Empty;
                            continue;
                        case DayOfWeek.Thursday:
                            sheet.SetCellValue("H8", datename);
                            datename = string.Empty;
                            continue;
                        case DayOfWeek.Friday:
                            sheet.SetCellValue("I8", datename);
                            datename = string.Empty;
                            continue;
                        case DayOfWeek.Saturday:
                            sheet.SetCellValue("J8", datename);
                            datename = string.Empty;
                            continue;
                        case DayOfWeek.Sunday:
                            sheet.SetCellValue("K8", datename);
                            datename = string.Empty;
                            continue;
                    }
                }

                //dem so hoc sinh
                startRow = 9;
                int cntPupil = listPupilOfClass.Count();
                indexPupil = 0;
                indexRow = 10;

                lockTraining = !IsAdmin ? lstlockTraining.Where(p => p.ClassID == lstClassID[i]).FirstOrDefault() : null;
                bool isLock = (lockTraining != null);
                foreach (var PupilOfClass in listPupilOfClass)
                {
                    startRow++;
                    if (PupilOfClass.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        sheet.CopyAndInsertARow(niceRow, startRow);
                        sheet.GetRange(startRow, 5, startRow, 14).IsLock = false;
                    }
                    else
                    {
                        sheet.CopyAndInsertARow(yellowRow, startRow);
                        sheet.GetRange(startRow, 5, startRow, 14).IsLock = true;
                    }

                    indexPupil++;

                    //cot stt
                    sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                    sheet.SetCellValue(startRow, 2, PupilOfClass.PupilFullName);
                    sheet.SetCellValue(startRow, 3, PupilOfClass.PupilCode);
                    sheet.SetCellValue(startRow, 4, PupilOfClass.PupilID);
                    //Lấy theo năm học:
                    // cac cot cua cac ngay trong thang
                    int colstartDte = 5;
                    foreach (var dte in lstDayByWeek)
                    {
                        //lay thong tin diem danh theo ngay va ma hoc sinh
                        if (dte < startYear || dte > endYear || dte > DateTime.Now)
                        {
                            sheet.GetRange(startRow, colstartDte, startRow, colstartDte).IsLock = true;
                        }
                        PupilAbsence PupilAbsenceItem = listPupilAbsence.Where(o => o.PupilID == PupilOfClass.PupilID && o.AbsentDate == dte).FirstOrDefault();

                        if (PupilAbsenceItem != null && PupilAbsenceItem.IsAccepted != null)
                        {
                            if (PupilAbsenceItem.IsAccepted.Value)
                                sheet.SetCellValue(startRow, colstartDte, "P");
                            else
                                sheet.SetCellValue(startRow, colstartDte, "K");
                        }
                        // Neu bi khoa diem danh thi lock cell chi ap dung voi tk giao vien
                        if (!IsAdmin && isLock && (!lockTraining.FromDate.HasValue || lockTraining.FromDate.Value <= dte) && lockTraining.ToDate >= dte)
                        {
                            sheet.GetRange(startRow, colstartDte, startRow, colstartDte).IsLock = true;
                        }
                        //string fomular count K
                        string f_countK = "=COUNTIF(E" + indexRow + ":K" + indexRow + ",'K')";
                        string f_countP = "=COUNTIF(E" + indexRow + ":K" + indexRow + ",'P')";

                        sheet.SetFormulaValue(startRow, 12, f_countP.Replace(",", ";").Replace("\'", "\""));
                        sheet.SetFormulaValue(startRow, 13, f_countK.Replace(",", ";").Replace("\'", "\""));
                        colstartDte++;
                    }
                    indexRow++;
                }

                sheet.SetCellValue("E4", "K");
                sheet.SetCellValue("E5", "P");
                sheet.SetCellValue("T7", lstClassID[i]);
                sheet.HideColumn(20);
                sheet.SetColumnWidth(4, 0);
                sheet.ProtectSheet("123456a@");
                sheet.DeleteRow(9);
                sheet.DeleteRow(9 + listPupilOfClass.Count());
                sheet.Name = Utils.Utils.StripVNSignAndSpace(className);
                firstSheet.Delete();
                lstValidate.Add(new VTDataValidation
                {
                    Contrains = new string[] { "P", "K" },
                    FromColumn = 5,
                    FromRow = 9,
                    SheetIndex = index,
                    ToColumn = 11,
                    ToRow = startRow - 1,
                    Type = VTValidationType.LIST
                });
                #endregion
            }
            sheet1.Delete();
            firstSheet1.Delete();

            return oBook.ToStreamValidationData(lstValidate);
        }

        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)]
        [HttpPost]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int? ClassID, int? Section, DateTime? AbsentDate,
                                                                            DateTime? DateWeek, int? ButtonType)
        {
            if (attachments == null || attachments.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
                res.ContentType = "text/plain";
                return res;
            }
            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + glo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;

                string Error = "";
                bool status = false;
                List<PupilAbsenceImport> list = null;
                List<DateTime> lstOutDate = new List<DateTime>();
                if (ButtonType == 0) //điểm danh tháng
                {
                    list = getDataToFileImport(physicalPath, ClassID, Section, AbsentDate, out Error, out status);
                }
                else
                {
                    list = getDataToFileImportByWeek(physicalPath, ClassID, Section, DateWeek, out Error, out status, out lstOutDate);
                }

                if (Error.Trim().Length == 0 && status)
                {
                    DateTime? startMonth = Business.Common.Utils.StartOfMonth(AbsentDate.Value);
                    //DateTime? endMonth = Business.Common.Utils.EndOfMonth(startMonth.Value);
                    List<PupilAbsence> listPupil = list.SelectMany(u => u.ListPupilAbsence).Where(u => u != null).ToList();
                    PupilAbsenceBusiness.SaveAMonthManyClass(glo.UserAccountID, glo.IsAdmin, glo.SchoolID.GetValueOrDefault(), glo.AcademicYearID.GetValueOrDefault(), glo.AppliedLevel.GetValueOrDefault(), Section.Value, startMonth.Value, listPupil, lstOutDate);
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                    res.ContentType = "text/plain";
                    return res;
                }
                else if (Error.Trim().Length > 0)
                {
                    JsonResult res = Json(new JsonMessage(Error, JsonMessage.ERROR));
                    res.ContentType = "text/plain";
                    return res;
                }
                else
                {
                    Session["ListPupil"] = list;
                    Session["AbsentDate"] = AbsentDate.Value;
                    Session["DateWeek"] = DateWeek.Value;
                    Session["ButtonType"] = ButtonType.Value;
                    JsonResult res = Json(new { type = "ViewError", ClassID = ClassID, Section = Section });
                    res.ContentType = "text/plain";
                    return res;
                }
            }
            JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_NoFileSelected"), JsonMessage.ERROR));
            res1.ContentType = "text/plain";
            return res1;
        }

        /// <summary>
        /// anhnhp_06/08/2015_kiểm tra dữ liệu import điểm danh tuần
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="ClassID"></param>
        /// <param name="Section"></param>
        /// <param name="DateWeek"></param>
        /// <param name="Error"></param>
        /// <param name="Pass"></param>
        /// <returns></returns>
        /// 
        public List<PupilAbsenceImport> getDataToFileImportByWeek(string filename, int? ClassID1, int? Section, DateTime? DateWeek, out string Error, out bool Pass, out List<DateTime> lstOutDate)
        {
            int startRow;
            int SchoolID = glo.SchoolID.Value;
            int AcademicYearID = glo.AcademicYearID.Value;
            int AppliedLevel = glo.AppliedLevel.Value;
            string FilePath = filename;

            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            //IVTWorksheet sheet = oBook.GetSheet(1);
            List<IVTWorksheet> lstWorkSheet = oBook.GetSheets();
            int sheetClassID;
            int strColDay;
            List<PupilAbsenceImport> ListPupil = new List<PupilAbsenceImport>();
            ClassProfile classProfile = new ClassProfile();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            DateTime startDate = objAcademicYear.FirstSemesterStartDate.Value;
            DateTime endDate = objAcademicYear.SecondSemesterEndDate.Value;
            int partitionid = UtilsBusiness.GetPartionId(SchoolID);
            List<DateTime> lstDayOfWeek = this.GetDayOfWeek(DateWeek.Value);
            lstOutDate = lstDayOfWeek;
            Error = "";
            Pass = false;
            string ClassName = "";
            string SectionString = "";
            string Title = "";
            string time = "";
            string pupilcode = "";
            string pupilname = "";
            string absent = "";
            string sheetName = "";

            //kiem tra cac cot trong bang csdl xem da chuan chua
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            //dic["Check"] = "check";
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            var lstPosAll = PupilOfClassBusiness.SearchBySchool(SchoolID, dic)
                                                .Select(u => new PupilOfClassBO
                                                {
                                                    PupilID = u.PupilID,
                                                    PupilCode = u.PupilProfile.PupilCode,
                                                    Name = u.PupilProfile.FullName,
                                                    Status = u.Status,
                                                    ClassID = u.ClassID,
                                                    EducationLevelID = u.ClassProfile.EducationLevelID
                                                });

            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = AcademicYearID;
            dicClass["AppliedLevel"] = AppliedLevel;
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(SchoolID, dicClass).ToList();


            foreach (var workSheetComponent in lstWorkSheet)
            {
                IVTWorksheet sheet = workSheetComponent;
                sheetName = workSheetComponent.Name;
                if (sheet.GetCellValue("T7") == null)
                {
                    Error = "File Excel không hợp lệ";
                    Pass = false;
                    return (new List<PupilAbsenceImport>());
                }
                sheetClassID = Int32.Parse(sheet.GetCellValue("T7").ToString());
                classProfile = lstClassProfile.Find(x => x.ClassProfileID == sheetClassID);
                startRow = 9;
                #region for

                #region Kiem tra du lieu chon dau vao
                Title = (string)sheet.GetCellValue("A6");
                if (Section == SystemParamsInFile.SECTION_MORNING)
                    SectionString = Res.Get("Common_Label_Morning");
                else if (Section == SystemParamsInFile.SECTION_AFTERNOON)
                    SectionString = Res.Get("PupilAbsence_Label_Afternoon");
                else if (Section == SystemParamsInFile.SECTION_EVENING)
                    SectionString = Res.Get("PupilAbsence_Label_Night");

                if (string.IsNullOrEmpty(Title) || !Title.Contains("BẢNG ĐIỂM DANH") || classProfile == null)
                {
                    Error = "File Excel không hợp lệ";
                    Pass = false;
                    return (new List<PupilAbsenceImport>());
                }

                time = "từ ngày " + lstDayOfWeek[0].ToShortDateString() + " đến ngày " + lstDayOfWeek[lstDayOfWeek.Count - 1].ToShortDateString();
                if (Title.Contains(time.ToUpper()) == false)
                {
                    Error = "File excel không phải là file điểm danh tuần  " + time;
                }

                if (Title.Contains(SectionString.ToUpper()) == false)
                {
                    Error = "File excel không phải là file điểm danh tuần của buổi " + SectionString;
                }

                ClassName = classProfile.DisplayName;
                if (Title.Contains(ClassName.ToUpper()) == false)
                {
                    if (Error == "")
                        Error = "File excel không phải là file điểm danh của lớp " + ClassName;
                    else Error = Error + "-" + "File excel không phải là file điểm danh của lớp " + ClassName;
                }
                #endregion

                //phan kiem tra du lieu chon ban dau va thong tin tren excel
                Pass = true;

                //kiem tra cac cot trong bang csdl xem da chuan chua               
                var lstPos = lstPosAll.Where(x => x.ClassID == sheetClassID).ToList();
                Pass = true;
                while (sheet.GetCellValue(startRow, 1) != null)
                {
                    pupilcode = (string)sheet.GetCellValue(startRow, 3);
                    pupilname = (string)sheet.GetCellValue(startRow, 2);

                    PupilAbsenceImport PupilAbsenceImport = new PupilAbsenceImport();
                    PupilAbsenceImport.sheetName = sheetName;
                    PupilAbsenceImport.Status = true;
                    PupilAbsenceImport.AbsenceWeekDicString = new Dictionary<string, string>();
                    PupilAbsenceImport.PupilCode = pupilcode;
                    PupilAbsenceImport.FullName = pupilname;
                    PupilAbsenceImport.ListPupilAbsence = new List<PupilAbsence>();

                    var poc = lstPos.Where(o => o.PupilCode == pupilcode).FirstOrDefault();
                    PupilAbsenceImport.PupilID = poc != null ? poc.PupilID : new Nullable<int>();

                    if (poc == null)
                    {
                        PupilAbsenceImport.ErrorLabel = Res.Get("MarkRecord_Label_PupilIDError");
                        PupilAbsenceImport.Status = false;
                        ListPupil.Add(PupilAbsenceImport);
                        Pass = false;
                        startRow++;
                        continue;
                    }

                    //if (!PupilOfClassBusiness.CheckValidateName(pupilname, sheetClassID))
                    if (lstPos.Where(o => o.ClassID == sheetClassID && o.Name == pupilname).Count() <= 0)
                    {
                        PupilAbsenceImport.ErrorLabel += Res.Get("MarkRecord_Label_NameError");
                        PupilAbsenceImport.Status = false;
                        ListPupil.Add(PupilAbsenceImport);
                        Pass = false;
                        startRow++;
                        continue;
                    }

                    strColDay = 4;
                    foreach (var dt in lstDayOfWeek)
                    {
                        strColDay++;
                        PupilAbsence Pupil = new PupilAbsence();
                        Pupil.PupilID = poc.PupilID;
                        Pupil.AbsentDate = dt;
                        Pupil.ClassID = sheetClassID;
                        Pupil.SchoolID = glo.SchoolID.Value;
                        Pupil.AcademicYearID = glo.AcademicYearID.Value;
                        Pupil.EducationLevelID = classProfile.EducationLevelID;
                        Pupil.Section = (int)Section.Value;
                        Pupil.PupilCode = pupilcode;
                        Pupil.Last2digitNumberSchool = partitionid;

                        #region Kiem tra hop le

                        if (sheet.GetCellValue(startRow, strColDay) == null || sheet.GetCellValue(startRow, strColDay).ToString().Trim() == string.Empty)
                        {
                            continue;
                        }

                        absent = sheet.GetCellValue(startRow, strColDay).ToString().ToUpper();
                        PupilAbsenceImport.AbsenceWeekDicString[dt.Day.ToString()] = absent;
                        if (!string.IsNullOrEmpty(absent))
                        {
                            if (absent != "P" && absent != "K")
                            {
                                PupilAbsenceImport.ErrorLabel = Res.Get("Trạng thái điểm danh phải là P/K,");
                                PupilAbsenceImport.Status = false;
                                PupilAbsenceImport.Value = absent;
                                Pass = false;
                                continue;
                            }
                        }
                        else
                            continue;

                        if (Pupil.AbsentDate > DateTime.Now)
                        {
                            PupilAbsenceImport.ErrorLabel += Res.Get("Tồn tại ngày điểm danh trước ngày hiện tại,");
                            PupilAbsenceImport.Status = false;
                            Pass = false;
                            continue;
                        }

                        if (Pupil.AbsentDate < startDate && Pupil.AbsentDate > endDate)
                        {
                            PupilAbsenceImport.ErrorLabel += Res.Get("Tồn tại ngày điểm danh không có trong học kỳ ");
                            PupilAbsenceImport.Status = false;
                            Pass = false;
                            continue;
                        }
                        #endregion

                        Pupil.IsAccepted = absent == "P";
                        PupilAbsenceImport.ListPupilAbsence.Add(Pupil);
                        PupilAbsenceImport.Status = PupilAbsenceImport.Status == false ? false : true;

                    }
                    PupilAbsenceImport.CntP = PupilAbsenceImport.AbsenceWeekDicString.Count(u => u.Value == "P");
                    PupilAbsenceImport.CntK = PupilAbsenceImport.AbsenceWeekDicString.Count(u => u.Value == "K");
                    PupilAbsenceImport.CntTS = PupilAbsenceImport.CntP + PupilAbsenceImport.CntK;
                    ListPupil.Add(PupilAbsenceImport);
                    startRow++;
                }
                #endregion
            }

            if (ListPupil.Where(x => x.Status == false).FirstOrDefault() != null)
                Pass = false;

            //PupilAbsenceImport.Status = PupilAbsenceImport.Status == false ? false : true;

            return ListPupil;
        }

        public PartialViewResult GetViewListToImport(int? ClassID, int? Section)
        {
            DateTime AbsentDate = (DateTime)Session["AbsentDate"];
            var ListPupil = (List<PupilAbsenceImport>)Session["ListPupil"];
            DateTime StartDate = Business.Common.Utils.StartOfMonth(AbsentDate);
            DateTime EndDate = Business.Common.Utils.EndOfMonth(StartDate);
            DateTime DateWeek = (DateTime)Session["DateWeek"];
            int ButtonType = (int)Session["ButtonType"];

            ViewData[PupilAbsenceConstants.LIST_PUPILABSENCE] = ListPupil;
            ViewData[PupilAbsenceConstants.DateEndMonth] = EndDate.Day;

            ViewData[PupilAbsenceConstants.BUTTON_TYPE] = ButtonType;
            List<DateTime> lstDayOfWeek = GetDayOfWeek(DateWeek);
            IDictionary<int, string> dicNameDay = new Dictionary<int, string>();
            foreach (var dt in lstDayOfWeek)
            {
                dicNameDay[dt.Day] = GetNameDay(dt);
            }
            ViewData[PupilAbsenceConstants.LIST_DAY_OF_WEEk] = lstDayOfWeek;
            ViewData[PupilAbsenceConstants.LIST_DAY_NAME] = dicNameDay;

            ViewData[PupilAbsenceConstants.ERROR_IMPORT_MESSAGE] = "Tồn tại dữ liệu không hợp lệ";
            return PartialView("_ChooseAction");
        }

        public List<PupilAbsenceImport> getDataToFileImport(string filename, int? ClassID1, int? Section, DateTime? AbsentDate, out string Error, out bool Pass)
        {
            int startColMark, endColMark, startRow;
            int SchoolID = glo.SchoolID.Value;
            int AcademicYearID = glo.AcademicYearID.Value;
            int AppliedLevel = glo.AppliedLevel.Value;
            string FilePath = filename;
            string ClassName = "";
            string Title = "";
            string sheetName = "";

            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            List<IVTWorksheet> lstWorkSheet = oBook.GetSheets();
            List<PupilAbsenceImport> ListPupil = new List<PupilAbsenceImport>();
            ClassProfile classProfile = new ClassProfile();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            DateTime startDate = objAcademicYear.FirstSemesterStartDate.Value;
            DateTime endDate = objAcademicYear.SecondSemesterEndDate.Value;
            int partitionid = UtilsBusiness.GetPartionId(SchoolID);
            int sheetClassID;
            Error = "";
            Pass = false;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            //dic["Check"] = "check";
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            var lstPosAll = PupilOfClassBusiness.SearchBySchool(SchoolID, dic)
                                                .Select(u => new PupilOfClassBO
                                                {
                                                    PupilID = u.PupilID,
                                                    PupilCode = u.PupilProfile.PupilCode,
                                                    Name = u.PupilProfile.FullName,
                                                    Status = u.Status,
                                                    ClassID = u.ClassID,
                                                    PupilProfile = u.PupilProfile
                                                });

            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = AcademicYearID;
            dicClass["AppliedLevel"] = AppliedLevel;
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(SchoolID, dicClass).ToList();

            foreach (var workSheetComponent in lstWorkSheet)
            {
                sheetName = workSheetComponent.Name;
                startColMark = 5;
                endColMark = 35;
                startRow = 9;
                IVTWorksheet sheet = workSheetComponent;
                if (sheet.GetCellValue("AN7") == null)
                {
                    Error = "File Excel không hợp lệ";
                    Pass = false;
                    return (new List<PupilAbsenceImport>());
                }
                sheetClassID = Int32.Parse(sheet.GetCellValue("AN7").ToString());
                classProfile = lstClassProfile.Find(x => x.ClassProfileID == sheetClassID);
                Error = "";

                #region Kiem tra du lieu chon dau vao

                Title = (string)sheet.GetCellValue("A6");
                string SectionString = "";
                if (Section == SystemParamsInFile.SECTION_MORNING)
                    SectionString = Res.Get("Common_Label_Morning");
                else if (Section == SystemParamsInFile.SECTION_AFTERNOON)
                    SectionString = Res.Get("PupilAbsence_Label_Afternoon");
                else if (Section == SystemParamsInFile.SECTION_EVENING)
                    SectionString = Res.Get("PupilAbsence_Label_Night");

                if (string.IsNullOrEmpty(Title) || !Title.Contains("BẢNG ĐIỂM DANH") || classProfile == null)
                {
                    Error = "File Excel không hợp lệ";
                    Pass = false;
                    return (new List<PupilAbsenceImport>());
                }

                string time = "Tháng " + AbsentDate.Value.Month.ToString() + "- Năm " + AbsentDate.Value.Year.ToString();
                if (Title.Contains(SectionString.ToUpper()) == false)
                {
                    Error = "File excel không phải là file điểm danh của buổi " + SectionString;
                }
                string[] words = Title.Split(' ');

                ClassName = classProfile.DisplayName.ToUpper().Replace("LỚP", "").Trim();
                if (Title.Contains(ClassName.ToUpper()) == false)
                {
                    if (Error == "")
                        Error = "File excel không phải là file điểm danh của lớp " + ClassName;
                    else Error = Error + "-" + "File excel không phải là file điểm danh của lớp " + ClassName;
                }
                if (Title.Contains(time.ToUpper()) == false)
                {
                    if (Error == "")
                        Error = "File excel không phải là file điểm danh của tháng " + AbsentDate.Value.Month.ToString();
                    else Error = Error + "-" + "File excel không phải là file điểm danh của tháng " + AbsentDate.Value.Month.ToString();
                }
                #endregion

                //phan kiem tra du lieu chon ban dau va thong tin tren excel
                Pass = true;

                //kiem tra cac cot trong bang csdl  xem da chuan chua      
                var lstPos = lstPosAll.Where(x => x.ClassID == sheetClassID).ToList();
                while (sheet.GetCellValue(startRow, 3) != null)
                {
                    string pupilcode = (string)sheet.GetCellValue(startRow, 3);
                    string pupilname = (string)sheet.GetCellValue(startRow, 2);
                    DateTime? startMonth = Business.Common.Utils.StartOfMonth(AbsentDate.Value);
                    DateTime? endMonth = Business.Common.Utils.EndOfMonth(startMonth.Value);

                    PupilAbsenceImport PupilAbsenceImport = new PupilAbsenceImport();
                    PupilAbsenceImport.sheetName = sheetName;
                    PupilAbsenceImport.Status = true;
                    PupilAbsenceImport.AbsenceString = new string[endMonth.Value.Day];
                    PupilAbsenceImport.PupilCode = pupilcode;
                    PupilAbsenceImport.FullName = pupilname;
                    PupilAbsenceImport.ListPupilAbsence = new List<PupilAbsence>();

                    var poc = lstPos.Where(o => o.PupilCode == pupilcode).FirstOrDefault();
                    PupilAbsenceImport.PupilID = poc != null ? poc.PupilID : new Nullable<int>();

                    if (poc == null)
                    {
                        PupilAbsenceImport.ErrorLabel = Res.Get("MarkRecord_Label_PupilIDError");
                        PupilAbsenceImport.sheetName = sheetName;
                        PupilAbsenceImport.Status = false;
                        ListPupil.Add(PupilAbsenceImport);
                        Pass = false;
                        startRow++;
                        continue;
                    }

                    //if (!PupilOfClassBusiness.CheckValidateName(pupilname, sheetClassID))
                    if (lstPos.Where(o => o.ClassID == sheetClassID && o.Name == pupilname).Count() <= 0)
                    {
                        PupilAbsenceImport.ErrorLabel += Res.Get("MarkRecord_Label_NameError");
                        PupilAbsenceImport.sheetName = sheetName;
                        PupilAbsenceImport.Status = false;
                        ListPupil.Add(PupilAbsenceImport);
                        Pass = false;
                        startRow++;
                        continue;
                    }

                    int date = 0; // ngay bat dau cua thang

                    for (int i = startColMark; i <= endColMark && date < endMonth.Value.Day; i++)
                    {
                        date++;
                        PupilAbsence Pupil = new PupilAbsence();
                        Pupil.PupilID = poc.PupilID;
                        Pupil.AbsentDate = new DateTime(AbsentDate.Value.Year, AbsentDate.Value.Month, date);
                        Pupil.ClassID = sheetClassID;
                        Pupil.SchoolID = glo.SchoolID.Value;
                        Pupil.AcademicYearID = glo.AcademicYearID.Value;
                        Pupil.EducationLevelID = classProfile.EducationLevelID;
                        Pupil.Section = (int)Section.Value;
                        Pupil.PupilCode = pupilcode;
                        Pupil.Last2digitNumberSchool = partitionid;

                        #region Kiem tra hop le

                        if (sheet.GetCellValue(startRow, i) == null) continue;
                        string m = sheet.GetCellValue(startRow, i).ToString();
                        string absent = sheet.GetCellValue(startRow, i).ToString().ToUpper();
                        PupilAbsenceImport.AbsenceString[date - 1] = absent;
                        if (!string.IsNullOrEmpty(absent))
                        {
                            if (absent != "P" && absent != "K")
                            {
                                PupilAbsenceImport.ErrorLabel = Res.Get("Trạng thái điểm danh phải là P/K,");
                                PupilAbsenceImport.Status = false;
                                PupilAbsenceImport.Value = absent;
                                Pass = false;
                                continue;
                            }
                        }
                        else
                            continue;

                        if (Pupil.AbsentDate > DateTime.Now)
                        {
                            PupilAbsenceImport.ErrorLabel += Res.Get("Tồn tại ngày điểm danh trước ngày hiện tại,");
                            PupilAbsenceImport.Status = false;
                            Pass = false;
                            continue;
                        }

                        if (Pupil.AbsentDate < startDate && Pupil.AbsentDate > endDate)
                        {
                            PupilAbsenceImport.ErrorLabel += Res.Get("Tồn tại ngày điểm danh không có trong học kỳ ");
                            PupilAbsenceImport.Status = false;
                            Pass = false;
                            continue;
                        }
                        #endregion

                        Pupil.IsAccepted = absent == "P";
                        PupilAbsenceImport.ListPupilAbsence.Add(Pupil);
                        PupilAbsenceImport.Status = PupilAbsenceImport.Status == false ? false : true;
                    }
                    PupilAbsenceImport.CntP = PupilAbsenceImport.AbsenceString.Count(u => u == "P");
                    PupilAbsenceImport.CntK = PupilAbsenceImport.AbsenceString.Count(u => u == "K");
                    PupilAbsenceImport.CntTS = PupilAbsenceImport.CntP + PupilAbsenceImport.CntK;
                    ListPupil.Add(PupilAbsenceImport);
                    startRow++;
                }
            }
            Pass = ListPupil.Where(x => x.Status == false).FirstOrDefault() != null ? false : true;
            return ListPupil;
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportDataToDB(int ClassID, int Section)
        {
            GlobalInfo gi = new GlobalInfo();
            DateTime AbsentDate = (DateTime)Session["AbsentDate"];
            var list = (List<PupilAbsenceImport>)Session["ListPupil"];
            DateTime DateWeek = (DateTime)Session["DateWeek"];
            int ButtonType = (int)Session["ButtonType"];

            DateTime? startMonth = Business.Common.Utils.StartOfMonth(AbsentDate);
            DateTime? endMonth = Business.Common.Utils.EndOfMonth(startMonth.Value);
            List<PupilAbsence> listPupil = list.SelectMany(u => u.ListPupilAbsence).Where(u => u != null).ToList();
            if (ButtonType == 0)
            {
                PupilAbsenceBusiness.SaveAMonthManyClass(gi.UserAccountID, _globalInfo.IsAdmin, gi.SchoolID.GetValueOrDefault(), gi.AcademicYearID.GetValueOrDefault(), gi.AppliedLevel.GetValueOrDefault(), Section, AbsentDate, listPupil);
                //PupilAbsenceBusiness.SaveAMonth(gi.UserAccountID, _globalInfo.IsAdmin, gi.SchoolID.GetValueOrDefault(), ClassID, Section, AbsentDate,listPupil);
            }
            else
            {
                PupilAbsenceBusiness.SaveAMonthManyClass(gi.UserAccountID, _globalInfo.IsAdmin, gi.SchoolID.GetValueOrDefault(), gi.AcademicYearID.GetValueOrDefault(), gi.AppliedLevel.GetValueOrDefault(), Section, DateWeek, listPupil);
                //PupilAbsenceBusiness.SaveAMonth(gi.UserAccountID, _globalInfo.IsAdmin, gi.SchoolID.GetValueOrDefault(), ClassID, Section, DateWeek, listPupil);
            }

            PupilAbsenceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// List danh danh sach hoc sinh diem danh
        /// </summary>
        /// phuongh1
        /// <returns></returns>
        ///
        public ActionResult GetListPupilAbsence()
        {
            int? ClassID = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime fHKI = objAcademicYear.FirstSemesterStartDate.Value;
            DateTime eHKI = objAcademicYear.FirstSemesterEndDate.Value;
            DateTime fHKII = objAcademicYear.SecondSemesterStartDate.Value;
            DateTime eHKII = objAcademicYear.SecondSemesterEndDate.Value;
            // DungVA - 12/04/2013
            // Check quyền giáo viên chủ nhiệm, giáo viên giám thị và admin trường
            // nếu không phải các quyền trên thì disable đi các button cho phép nhập điểm, import
            int userAccountID = _globalInfo.UserAccountID;
            bool hasHeadTeacherPermission = UtilsBusiness.HasHeadTeacherPermission(userAccountID, ClassID.Value);
            bool hasOverSeePermission = UtilsBusiness.HasOverseeingTeacherPermission(userAccountID, ClassID.Value);
            //bool enableButton = false;
            bool disabsence = false;
            if (hasHeadTeacherPermission || hasOverSeePermission)
            {
                ViewData[PupilAbsenceConstants.CHECK_PERMISSION] = true;
                disabsence = true;
            }
            else
            {
                ViewData[PupilAbsenceConstants.CHECK_PERMISSION] = false;
            }
            // End of DungVA

            DateTime? date = DateTime.Parse(Request["date"]);

            int? Section = SMAS.Business.Common.Utils.GetInt(Request["section"]);
            if (ClassID != 0 && date != null && Section != 0
                && date.Value.StartOfMonth().Date <= DateTime.Now.Date)
            {
                AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                DateTime startDate = academicYear.FirstSemesterStartDate.Value;
                DateTime endDate = academicYear.SecondSemesterEndDate.Value;

                DateTime selectedMonth = date.Value;
                //Tính ngày đầu và ngày cuối
                DateTime dateMin = selectedMonth.StartOfMonth().Date;
                DateTime dateMax = selectedMonth.EndOfMonth().Date;

                //Nếu tháng được chọn là tháng bắt đầu năm học
                if (selectedMonth.Month == startDate.Month && selectedMonth.Year == startDate.Year)
                {
                    dateMin = startDate.Date;
                }

                //Nếu tháng được chọn là tháng kết thúc năm học
                if (selectedMonth.Month == endDate.Month && selectedMonth.Year == endDate.Year)
                {
                    dateMax = endDate.Date;
                }

                //Nếu tháng được chọn là tháng hiện tại
                if (selectedMonth.Month == DateTime.Now.Month
                    && selectedMonth.Year == DateTime.Now.Year
                    && dateMax.Date > DateTime.Now.Date)
                {
                    dateMax = DateTime.Now.Date;
                }

                ViewData["min"] = dateMin.Day;
                ViewData["max"] = dateMax.Day;

                // Lay thong tin thoi gian bi khoa diem danh
                LockTraining lockTraining = LockTrainingBusiness.SearchByClass(ClassID.Value, new Dictionary<string, object>()
                {
                    {"IsAbsence", true}
                }).FirstOrDefault();
                if (lockTraining != null)
                {
                    DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : startDate;
                    DateTime toLockDate = lockTraining.ToDate.AddDays(1);
                    ViewData["lockTitle"] = string.Format(Res.Get("LockTraining_Label_AbsenceTitle"), fromLockDate.ToString("dd/MM/yyyy"), lockTraining.ToDate.ToString("dd/MM/yyyy"));
                    if (!_globalInfo.IsAdmin)
                    {
                        if (fromLockDate > dateMax || toLockDate <= dateMin)
                        {
                            ViewData["isLock"] = false;
                        }
                        else
                        {
                            ViewData["isLock"] = true;
                            // Luu thong tin thoi gian khoa, chi lay thong tin ve ngay
                            // Neu khoa keo dai qua nhieu thang thi chi lay thoi gian bi khoa trong thang dang xet de xu ly
                            ViewData["fromDayLock"] = fromLockDate.Day;
                            if (fromLockDate <= dateMin)
                            {
                                ViewData["fromDayLock"] = dateMin.Day;
                            }
                            // Den ngay
                            ViewData["toDayLock"] = toLockDate.Day - 1;
                            if (toLockDate > dateMax)
                            {
                                ViewData["toDayLock"] = dateMax.Day;
                            }
                        }
                    }
                }
                else
                {
                    ViewData["isLock"] = false;
                }

                //Lấy danh sách các ngày thứ 7, chủ nhật
                List<int> listSunday = new List<int>();
                List<int> listSaturday = new List<int>();
                DateTime dt = dateMin;
                while (dt < dateMax.AddDays(1))
                {
                    if (dt.DayOfWeek == DayOfWeek.Saturday)
                    {
                        listSaturday.Add(dt.Day);
                    }
                    else if (dt.DayOfWeek == DayOfWeek.Sunday)
                    {
                        listSunday.Add(dt.Day);
                    }
                    dt = dt.AddDays(1);
                }
                ViewData["listSaturday"] = listSaturday;
                ViewData["listSunday"] = listSunday;

                ViewData["ClassID"] = ClassID;
                ViewData["Section"] = Section;


                //Lấy thông tin điểm danh của lớp được 
                List<PupilAbsence> listPupilAbsence = PupilAbsenceBusiness.SearchBySchool(
                    _globalInfo.SchoolID.Value, new Dictionary<string, object>
                    {
                        {"ClassID", ClassID},
                        {"Section", Section}
                    }).ToList();

                //lay danh sach cac hoc sinh trong lop
                var listPupilOfClass = PupilOfClassBusiness.SearchBySchool(
                    _globalInfo.SchoolID.Value, new Dictionary<string, object>
                    {
                        {"ClassID", ClassID},
                        {"Check", "Check"}                        
                    }).Select(o => new
                    {
                        o.ClassID,
                        o.PupilID,
                        o.PupilProfile.FullName,
                        o.PupilProfile.Name,
                        o.OrderInClass,
                        o.Status
                    }).ToList()
                    .OrderBy(o => o.OrderInClass)
                    .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name))
                    .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName)).ToList();

                if (CheckIsShowPupil() == 1)
                {
                    listPupilOfClass = listPupilOfClass.Where(x => x.Status == 1 || x.Status == 2).ToList();
                }


                List<PupilAbsenceForEditViewModel> listModel = new List<PupilAbsenceForEditViewModel>();
                foreach (var pupil in listPupilOfClass)
                {
                    PupilAbsenceForEditViewModel model = new PupilAbsenceForEditViewModel();
                    model.PupilID = pupil.PupilID;
                    model.PupilName = pupil.FullName;


                    //if (pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pupil.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    if (pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING || !disabsence)
                    {
                        model.IsDisabled = true;
                    }
                    else
                    {
                        model.IsDisabled = false;
                    }

                    model.DicValue = new Dictionary<int, string>();
                    dt = dateMin;
                    int totalK = 0;
                    int totalP = 0;
                    int total = 0;
                    while (dt < dateMax.AddDays(1))
                    {
                        PupilAbsence pa = listPupilAbsence.Find(x => x.PupilID == pupil.PupilID
                            && x.ClassID == pupil.ClassID
                            && x.AbsentDate.Date == dt.Date);
                        if (dt < fHKI || (dt > eHKI && dt < fHKII) || dt > eHKII)
                        {
                            model.DicValue[dt.Day] = "disabled";
                        }
                        else if (pa == null)
                        {
                            model.DicValue[dt.Day] = "";
                        }
                        else
                        {

                            if (pa.IsAccepted.HasValue && pa.IsAccepted.Value)
                            {
                                model.DicValue[dt.Day] = "P";
                                totalP++;
                                total++;
                            }
                            else if (pa.IsAccepted.HasValue && pa.IsAccepted.Value == false)
                            {
                                model.DicValue[dt.Day] = "K";
                                totalK++;
                                total++;
                            }
                            else
                            {
                                model.DicValue[dt.Day] = String.Empty;
                            }
                        }
                        dt = dt.AddDays(1);
                    }
                    model.TotalK = totalK;
                    model.TotalP = totalP;
                    model.Total = total;
                    model.pupilStatus = pupil.Status;
                    listModel.Add(model);
                }
                ViewData[PupilAbsenceConstants.LIST_PUPILABSENCE] = listModel;

                ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
                ViewData["ClassName"] = string.Format(Res.Get("PupilAbsence_Label_ClassName"), classProfile.DisplayName.ToUpper().Replace("LỚP", ""));
                ViewData["ClassID"] = classProfile.ClassProfileID;
                ViewData["Section"] = Section;
                ViewData["Month"] = selectedMonth;
            }
            return PartialView("_ListEditAll");
        }

        public ActionResult GetListPupilAbsenceByWeek()
        {
            int? ClassID = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            // Check quyền giáo viên chủ nhiệm, giáo viên giám thị và admin trường
            // nếu không phải các quyền trên thì disable đi các button cho phép nhập điểm, import
            GlobalInfo globalInfo = new GlobalInfo();
            int userAccountID = globalInfo.UserAccountID;
            bool hasHeadTeacherPermission = UtilsBusiness.HasHeadTeacherPermission(userAccountID, ClassID.Value);
            bool hasOverSeePermission = UtilsBusiness.HasOverseeingTeacherPermission(userAccountID, ClassID.Value);
            bool disabsence = false;
            if (hasHeadTeacherPermission || hasOverSeePermission)
            {
                ViewData[PupilAbsenceConstants.CHECK_PERMISSION] = true;
                disabsence = true;
            }
            else
            {
                ViewData[PupilAbsenceConstants.CHECK_PERMISSION] = false;
            }

            int? Section = SMAS.Business.Common.Utils.GetInt(Request["section"]);
            DateTime? date = DateTime.Parse(Request["dateChoice"]);

            #region Note
            if (ClassID != 0 && date != null && Section != 0
                && date.Value.StartOfMonth().Date <= DateTime.Now.Date)
            {
                AcademicYear academicYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
                DateTime startDate = academicYear.FirstSemesterStartDate.Value;
                DateTime endDate = academicYear.SecondSemesterEndDate.Value;

                DateTime selectedMonth = date.Value;
                //Tính ngày đầu và ngày cuối
                DateTime dateMin = selectedMonth.StartOfMonth().Date;
                DateTime dateMax = selectedMonth.EndOfMonth().Date;

                //Nếu tháng được chọn là tháng bắt đầu năm học
                if (selectedMonth.Month == startDate.Month && selectedMonth.Year == startDate.Year)
                {
                    dateMin = startDate.Date;
                }

                //Nếu tháng được chọn là tháng kết thúc năm học
                if (selectedMonth.Month == endDate.Month && selectedMonth.Year == endDate.Year)
                {
                    dateMax = endDate.Date;
                }

                //Nếu tháng được chọn là tháng hiện tại
                if (selectedMonth.Month == DateTime.Now.Month
                    && selectedMonth.Year == DateTime.Now.Year
                    && dateMax.Date > DateTime.Now.Date)
                {
                    dateMax = DateTime.Now.Date;
                }
                ViewData["min"] = dateMin.Day;
                ViewData["max"] = dateMax.Day;
                ViewData[PupilAbsenceConstants.ACADEMIC_YEAR] = academicYear;
                // Lay thong tin thoi gian bi khoa diem danh
                LockTraining lockTraining = LockTrainingBusiness.SearchByClass(ClassID.Value, new Dictionary<string, object>()
                {
                    {"IsAbsence", true}
                }).FirstOrDefault();

                if (lockTraining != null)
                {
                    DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : startDate;
                    DateTime toLockDate = lockTraining.ToDate.AddDays(1);
                    ViewData["lockTitle"] = string.Format(Res.Get("LockTraining_Label_AbsenceTitle"), fromLockDate.ToString("dd/MM/yyyy"), lockTraining.ToDate.ToString("dd/MM/yyyy"));
                    if (!_globalInfo.IsAdmin)
                    {
                        if (fromLockDate > dateMax || toLockDate <= dateMin)
                        {
                            ViewData["isLock"] = false;
                        }
                        else
                        {
                            ViewData["isLock"] = true;
                            // Luu thong tin thoi gian khoa, chi lay thong tin ve ngay
                            // Neu khoa keo dai qua nhieu thang thi chi lay thoi gian bi khoa trong thang dang xet de xu ly
                            ViewData["fromDayLock"] = fromLockDate.Day;
                            if (fromLockDate <= dateMin)
                            {
                                ViewData["fromDayLock"] = dateMin.Day;
                            }
                            // Den ngay
                            ViewData["toDayLock"] = toLockDate.Day - 1;
                            if (toLockDate > dateMax)
                            {
                                ViewData["toDayLock"] = dateMax.Day;
                            }
                        }
                    }
                }
                else
                {
                    ViewData["isLock"] = false;
                }
            }
            #endregion

            //Lấy thông tin điểm danh của lớp được 
            List<PupilAbsence> listPupilAbsence = PupilAbsenceBusiness.SearchBySchool(
                globalInfo.SchoolID.Value, new Dictionary<string, object>
                    {
                        {"ClassID", ClassID},
                        {"Section", Section}
                    }).ToList();


            //lay danh sach cac hoc sinh trong lop
            var listPupilOfClass = PupilOfClassBusiness.SearchBySchool(
                globalInfo.SchoolID.Value, new Dictionary<string, object>
                    {
                        {"ClassID", ClassID},
                        {"Check", "Check"}                        
                    }).Select(o => new
                    {
                        o.ClassID,
                        o.PupilID,
                        o.PupilProfile.FullName,
                        o.PupilProfile.Name,
                        o.OrderInClass,
                        o.Status
                    }).ToList()
                .OrderBy(o => o.OrderInClass)
                .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name))
                .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName)).ToList();

            if (CheckIsShowPupil() == 1)
            {
                listPupilOfClass = listPupilOfClass.Where(x => x.Status == 1 || x.Status == 2).ToList();
            }

            //lấy danh sách các ngày trong tuần của ngày được chọn
            DateTime selectDate = date.Value;
            List<DateTime> lstDayOfWeek = GetDayOfWeek(selectDate);
            IDictionary<int, string> dicNameDay = new Dictionary<int, string>();
            foreach (var dt in lstDayOfWeek)
            {
                dicNameDay[dt.Day] = GetNameDay(dt);
            }
            ViewData[PupilAbsenceConstants.LIST_DAY_OF_WEEk] = lstDayOfWeek;
            ViewData[PupilAbsenceConstants.LIST_DAY_NAME] = dicNameDay;

            List<PupilAbsenceForEditViewModel> listModel = new List<PupilAbsenceForEditViewModel>();
            foreach (var pupil in listPupilOfClass)
            {
                PupilAbsenceForEditViewModel model = new PupilAbsenceForEditViewModel();
                model.PupilID = pupil.PupilID;
                model.PupilName = pupil.FullName;

                if (pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING || !disabsence)
                {
                    model.IsDisabled = true;
                }
                else
                {
                    model.IsDisabled = false;
                }

                model.DicValue = new Dictionary<int, string>();
                int totalK = 0;
                int totalP = 0;
                int total = 0;
                //duyet wa cac ngay trong tuan
                foreach (var dt in lstDayOfWeek)
                {
                    PupilAbsence pa = listPupilAbsence.Find(x => x.PupilID == pupil.PupilID
                            && x.ClassID == pupil.ClassID
                            && x.AbsentDate.Date == dt.Date);
                    if (pa == null)
                    {
                        model.DicValue[dt.Day] = "";
                    }
                    else
                    {

                        if (pa.IsAccepted.HasValue && pa.IsAccepted.Value)
                        {
                            model.DicValue[dt.Day] = "P";
                            totalP++;
                            total++;
                        }
                        else if (pa.IsAccepted.HasValue && !pa.IsAccepted.Value)
                        {
                            model.DicValue[dt.Day] = "K";
                            totalK++;
                            total++;
                        }
                        else
                        {
                            model.DicValue[dt.Day] = "";
                        }
                    }
                }
                model.TotalK = totalK;
                model.TotalP = totalP;
                model.Total = total;
                model.pupilStatus = pupil.Status;
                listModel.Add(model);
            }
            ViewData[PupilAbsenceConstants.LIST_PUPILABSENCE] = listModel;

            ClassProfile classProfile = ClassID != 0 ? ClassProfileBusiness.Find(ClassID) : null;
            if (classProfile != null)
            {
                ViewData["ClassName"] = string.Format(Res.Get("PupilAbsence_Label_ClassName"), classProfile.DisplayName.ToUpper().Replace("LỚP", "").Trim());
                ViewData["ClassID"] = classProfile.ClassProfileID;
            }
            ViewData["Section"] = Section;
            ViewData["DateWeek"] = selectDate;

            return PartialView("_ListAbsenceWeek");
        }

        private string GetNameDay(DateTime dt)
        {
            string name = string.Empty;
            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    name = "Thứ 2, ";
                    name += dt.Day.ToString().Length > 1 ? dt.Day.ToString() : "0" + dt.Day.ToString();
                    name += "/";
                    name += dt.Month.ToString().Length > 1 ? dt.Month.ToString() : "0" + dt.Month.ToString();
                    return name;
                case DayOfWeek.Tuesday:
                    name = "Thứ 3, ";
                    name += dt.Day.ToString().Length > 1 ? dt.Day.ToString() : "0" + dt.Day.ToString();
                    name += "/";
                    name += dt.Month.ToString().Length > 1 ? dt.Month.ToString() : "0" + dt.Month.ToString();
                    return name;
                case DayOfWeek.Wednesday:
                    name = "Thứ 4, ";
                    name += dt.Day.ToString().Length > 1 ? dt.Day.ToString() : "0" + dt.Day.ToString();
                    name += "/";
                    name += dt.Month.ToString().Length > 1 ? dt.Month.ToString() : "0" + dt.Month.ToString();
                    return name;
                case DayOfWeek.Thursday:
                    name = "Thứ 5, ";
                    name += dt.Day.ToString().Length > 1 ? dt.Day.ToString() : "0" + dt.Day.ToString();
                    name += "/";
                    name += dt.Month.ToString().Length > 1 ? dt.Month.ToString() : "0" + dt.Month.ToString();
                    return name;
                case DayOfWeek.Friday:
                    name = "Thứ 6, ";
                    name += dt.Day.ToString().Length > 1 ? dt.Day.ToString() : "0" + dt.Day.ToString();
                    name += "/";
                    name += dt.Month.ToString().Length > 1 ? dt.Month.ToString() : "0" + dt.Month.ToString();
                    return name;
                case DayOfWeek.Saturday:
                    name = "Thứ 7, ";
                    name += dt.Day.ToString().Length > 1 ? dt.Day.ToString() : "0" + dt.Day.ToString();
                    name += "/";
                    name += dt.Month.ToString().Length > 1 ? dt.Month.ToString() : "0" + dt.Month.ToString();
                    return name;
                case DayOfWeek.Sunday:
                    name = "CN, ";
                    name += dt.Day.ToString().Length > 1 ? dt.Day.ToString() : "0" + dt.Day.ToString();
                    name += "/";
                    name += dt.Month.ToString().Length > 1 ? dt.Month.ToString() : "0" + dt.Month.ToString();
                    return name;
            }
            return name;
        }

        /// <summary>
        /// Lấy ra danh sách các ngày trong tuần
        /// </summary>
        /// <param name="selectDate"></param>
        /// <returns></returns>
        private List<DateTime> GetDayOfWeek(DateTime selectDate)
        {
            List<DateTime> lstDay = new List<DateTime>();

            switch (selectDate.DayOfWeek)
            {
                case DayOfWeek.Monday: //thu 2
                    lstDay.Add(selectDate);
                    lstDay.Add(selectDate.AddDays(1));
                    lstDay.Add(selectDate.AddDays(2));
                    lstDay.Add(selectDate.AddDays(3));
                    lstDay.Add(selectDate.AddDays(4));
                    lstDay.Add(selectDate.AddDays(5));
                    lstDay.Add(selectDate.AddDays(6));
                    return lstDay;

                case DayOfWeek.Tuesday: //thu 3
                    lstDay.Add(selectDate.AddDays(-1));
                    lstDay.Add(selectDate);
                    lstDay.Add(selectDate.AddDays(1));
                    lstDay.Add(selectDate.AddDays(2));
                    lstDay.Add(selectDate.AddDays(3));
                    lstDay.Add(selectDate.AddDays(4));
                    lstDay.Add(selectDate.AddDays(5));
                    return lstDay;

                case DayOfWeek.Wednesday: //thu 4
                    lstDay.Add(selectDate.AddDays(-2));
                    lstDay.Add(selectDate.AddDays(-1));
                    lstDay.Add(selectDate);
                    lstDay.Add(selectDate.AddDays(1));
                    lstDay.Add(selectDate.AddDays(2));
                    lstDay.Add(selectDate.AddDays(3));
                    lstDay.Add(selectDate.AddDays(4));
                    return lstDay;

                case DayOfWeek.Thursday: //thu 5
                    lstDay.Add(selectDate.AddDays(-3));
                    lstDay.Add(selectDate.AddDays(-2));
                    lstDay.Add(selectDate.AddDays(-1));
                    lstDay.Add(selectDate);
                    lstDay.Add(selectDate.AddDays(1));
                    lstDay.Add(selectDate.AddDays(2));
                    lstDay.Add(selectDate.AddDays(3));
                    return lstDay;

                case DayOfWeek.Friday: //thu 6
                    lstDay.Add(selectDate.AddDays(-4));
                    lstDay.Add(selectDate.AddDays(-3));
                    lstDay.Add(selectDate.AddDays(-2));
                    lstDay.Add(selectDate.AddDays(-1));
                    lstDay.Add(selectDate);
                    lstDay.Add(selectDate.AddDays(1));
                    lstDay.Add(selectDate.AddDays(2));
                    return lstDay;

                case DayOfWeek.Saturday: //thu 7
                    lstDay.Add(selectDate.AddDays(-5));
                    lstDay.Add(selectDate.AddDays(-4));
                    lstDay.Add(selectDate.AddDays(-3));
                    lstDay.Add(selectDate.AddDays(-2));
                    lstDay.Add(selectDate.AddDays(-1));
                    lstDay.Add(selectDate);
                    lstDay.Add(selectDate.AddDays(1));
                    return lstDay;

                case DayOfWeek.Sunday: //chu nhat
                    lstDay.Add(selectDate.AddDays(-6));
                    lstDay.Add(selectDate.AddDays(-5));
                    lstDay.Add(selectDate.AddDays(-4));
                    lstDay.Add(selectDate.AddDays(-3));
                    lstDay.Add(selectDate.AddDays(-2));
                    lstDay.Add(selectDate.AddDays(-1));
                    lstDay.Add(selectDate);
                    return lstDay;

            }
            return lstDay;
        }

        //phuongh1
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSection(int ClassID)
        {
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            List<ComboObject> list = new List<ComboObject>();
            if (classProfile.Section != null)
            {
                if (classProfile.Section == SystemParamsInFile.SECTION_MORNING || classProfile.Section == SystemParamsInFile.SECTION_MORNING_AFTERNOON
                    || classProfile.Section == SystemParamsInFile.SECTION_MORNING_EVENING || classProfile.Section == SystemParamsInFile.SECTION_ALL)
                {
                    list.Add(new ComboObject(Res.Get("PupilAbsence_Label_Morning"), "1"));
                }
                if (classProfile.Section == SystemParamsInFile.SECTION_AFTERNOON || classProfile.Section == SystemParamsInFile.SECTION_MORNING_AFTERNOON
                    || classProfile.Section == SystemParamsInFile.SECTION_AFTERNOON_EVENING || classProfile.Section == SystemParamsInFile.SECTION_ALL)
                {
                    list.Add(new ComboObject(Res.Get("PupilAbsence_Label_Evening"), "2"));
                }
                if (classProfile.Section == SystemParamsInFile.SECTION_EVENING || classProfile.Section == SystemParamsInFile.SECTION_MORNING_EVENING
                    || classProfile.Section == SystemParamsInFile.SECTION_AFTERNOON_EVENING || classProfile.Section == SystemParamsInFile.SECTION_ALL)
                {
                    list.Add(new ComboObject(Res.Get("PupilAbsence_Label_Night"), "3"));
                }
            }
            return Json(new SelectList(list, "value", "key"));

        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        [ValidateAntiForgeryToken]
        public JsonResult Save(int ClassID, int Section, DateTime Month, string Data, bool CheckAllDay)
        {
            List<PupilAbsence> listData = new List<PupilAbsence>();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime fHKI = objAcademicYear.FirstSemesterStartDate.Value;
            DateTime eHKI = objAcademicYear.FirstSemesterEndDate.Value;
            DateTime fHKII = objAcademicYear.SecondSemesterStartDate.Value;
            DateTime eHKII = objAcademicYear.SecondSemesterEndDate.Value;
            // Lay thong tin thoi gian bi khoa diem danh
            LockTraining lockTraining = !_globalInfo.IsAdmin ? LockTrainingBusiness.SearchByClass(ClassID, new Dictionary<string, object>()
                {
                    {"IsAbsence", true}
                }).FirstOrDefault() : null;
            bool isLock = (lockTraining != null);
            string[] arrData = Data.Split(',');
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            foreach (string data in arrData)
            {
                if (!string.IsNullOrWhiteSpace(data))
                {
                    if (!data.Contains("undefined"))// Fix bug 0168878 Hungnd8 - 13/-5/2013
                    {
                        string[] arrEle = data.Split('_');
                        DateTime AbsentDate = new DateTime(Month.Year, Month.Month, Int32.Parse(arrEle[0]));
                        // Neu thuoc thoi gian khoa thi bo qua
                        if (isLock && (!lockTraining.FromDate.HasValue || lockTraining.FromDate.Value <= AbsentDate)
                            && lockTraining.ToDate >= AbsentDate || AbsentDate < fHKI || (AbsentDate > eHKI && AbsentDate < fHKII) || AbsentDate > eHKII)
                        {
                            continue;
                        }
                        PupilAbsence pa = new PupilAbsence();
                        pa.PupilID = Int32.Parse(arrEle[1]);
                        pa.AbsentDate = AbsentDate;
                        pa.IsAccepted = arrEle[2] == "K" ? false : true;
                        pa.Last2digitNumberSchool = partitionId;
                        listData.Add(pa);
                    }
                }
            }
            PupilAbsenceBusiness.SaveAMonth(_globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.SchoolID.GetValueOrDefault(),
                ClassID, Section, Month, listData, CheckAllDay, _globalInfo.AppliedLevel.Value);
            PupilFaultBusiness.Save();
            return Json(new JsonMessage(Res.Get("PupilAbsence_Label_Success")));
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveByWeek(int ClassID, int Section, DateTime DateWeek, string Data, bool CheckAllDay)
        {
            List<PupilAbsence> listData = new List<PupilAbsence>();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime fHKI = objAcademicYear.FirstSemesterStartDate.Value;
            DateTime eHKI = objAcademicYear.FirstSemesterEndDate.Value;
            DateTime fHKII = objAcademicYear.SecondSemesterStartDate.Value;
            DateTime eHKII = objAcademicYear.SecondSemesterEndDate.Value;
            // Lay thong tin thoi gian bi khoa diem danh
            LockTraining lockTraining = !_globalInfo.IsAdmin ? LockTrainingBusiness.SearchByClass(ClassID, new Dictionary<string, object>()
                {
                    {"IsAbsence", true}
                }).FirstOrDefault() : null;
            bool isLock = (lockTraining != null);
            string[] arrData = Data.Split(',');
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            foreach (string data in arrData)
            {
                if (!string.IsNullOrWhiteSpace(data))
                {
                    if (!data.Contains("undefined"))// Fix bug 0168878 Hungnd8 - 13/-5/2013
                    {
                        string[] arrEle = data.Split('_');
                        if (String.IsNullOrEmpty(arrEle[2]))
                        {
                            continue;
                        }
                        DateTime AbsentDate = DateTime.ParseExact(arrEle[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        // Neu thuoc thoi gian khoa thi bo qua
                        if (isLock && (!lockTraining.FromDate.HasValue || lockTraining.FromDate.Value <= AbsentDate)
                            && lockTraining.ToDate >= AbsentDate || AbsentDate < fHKI || (AbsentDate > eHKI && AbsentDate < fHKII) || AbsentDate > eHKII)
                        {
                            continue;
                        }
                        PupilAbsence pa = new PupilAbsence();
                        pa.PupilID = Int32.Parse(arrEle[1]);
                        pa.AbsentDate = AbsentDate;
                        if ("K".Equals(arrEle[2]))
                        {
                            pa.IsAccepted = false;
                        }
                        else if ("P".Equals(arrEle[2]))
                        {
                            pa.IsAccepted = true;
                        }
                        pa.Last2digitNumberSchool = partitionId;
                        listData.Add(pa);
                    }
                }
            }
            List<DateTime> lstDayByWeek = GetDayOfWeek(DateWeek);
            PupilAbsenceBusiness.SaveAWeek(_globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.SchoolID.GetValueOrDefault(),
                ClassID, Section, DateWeek, listData, lstDayByWeek.FirstOrDefault(), lstDayByWeek[lstDayByWeek.Count - 1], CheckAllDay, _globalInfo.AppliedLevel.Value);
            PupilFaultBusiness.Save();
            return Json(new JsonMessage(Res.Get("PupilAbsence_Label_Success")));
        }

        public PartialViewResult LoadPopUpReportOption(int classID, string month, string section, string date, string type)
        {
            bool isadmin = false;
            if (_globalInfo.IsAdmin == true)
                isadmin = true;

            ClassProfile objclass = ClassProfileBusiness.Find(classID);
            ReportOptionViewModel model = new ReportOptionViewModel();
            model.classID = classID;
            model.className = objclass.DisplayName.Trim().Replace("Lớp", "").Replace("LỚP", "").Replace("lớp", "").Trim();
            model.educationLevelID = objclass.EducationLevelID;
            model.educationName = objclass.EducationLevel.Resolution;
            model.month = month;
            model.isAdmin = isadmin;
            model.monthyear = "";
            model.sectionName = section == "1" ? "Buổi sáng" : section == "2" ? "Buổi chiều" : "Buổi tối";
            model.type = type;
            if (checkDateType(month))
            {
                DateTime datetimeTemp = DateTime.Parse(month);
                model.monthyear = datetimeTemp.Month + "/" + datetimeTemp.Year;
            }
            if (checkDateType(date) && type == "2")
            {
                List<DateTime> lstDayByWeek = GetDayOfWeek(DateTime.Parse(date));
                model.stringdate = lstDayByWeek[0].ToShortDateString() + " đến " + lstDayByWeek[lstDayByWeek.Count - 1].ToShortDateString();
            }
            return PartialView("_ReportOption", model);
        }

        public bool checkDateType(string a)
        {
            DateTime c;
            bool b = DateTime.TryParse(a, out c);
            return b;
        }

        public bool checkInt(string a)
        {
            int c;
            bool b = Int32.TryParse(a, out c);
            return b;
        }

    }
}





