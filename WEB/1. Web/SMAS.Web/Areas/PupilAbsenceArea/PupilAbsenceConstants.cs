/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilAbsenceArea
{
    public class PupilAbsenceConstants
    {
        public const string LIST_PUPILABSENCE = "listPupilAbsence";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SECTION = "listSection";
        public const string DateNow = "DateNow";

        public const string DateStartMonth = "DateStartMonth";
        public const string DateEndMonth = "DateEndMonth";
        public const string DateAbsent_K = "K";
        public const string DateAbsent_P = "P";
        public const string Permision = "Permision";

        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";
        public const string CHECK_PERMISSION = "CHECK_PERMISSION";

        public const string LIST_MONTH = "listMonth";
        public const string LIST_DAY_OF_WEEk = "LIST_DAY_OF_WEEk";
        public const string LIST_DAY_NAME = "LIST_DAY_NAME";

        public const string BUTTON_TYPE = "BUTTON_TYPE";
        public const string ACADEMIC_YEAR = "AcademicYear";

    }
}