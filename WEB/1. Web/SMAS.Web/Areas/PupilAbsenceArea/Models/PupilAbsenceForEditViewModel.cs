﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.PupilAbsenceArea.Models
{
    public class PupilAbsenceForEditViewModel
    {
        public int PupilID { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string PupilName { get; set; }
        public int TotalK { get; set; }
        public int TotalP { get; set; }
        public int Total { get; set; }
        public Dictionary<int, string> DicValue {get; set;}
        public Dictionary<int, string> DicNameDay { get; set; }
        public bool? IsDisabled { get; set; }
        public int pupilStatus { get; set; }
    }
}