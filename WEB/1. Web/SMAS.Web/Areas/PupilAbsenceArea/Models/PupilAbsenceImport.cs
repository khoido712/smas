﻿/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.Models;
namespace SMAS.Web.Areas.PupilAbsenceArea.Models
{
    public class PupilAbsenceImport
    {
        public List<PupilAbsence> ListPupilAbsence { get; set; }
        [ResourceDisplayName("Sheet")]
        public string sheetName { get; set; }
        public string Value { get; set; }
        public bool Status { get; set; }
        public string ErrorLabel { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("PupilAbsent_Column_PupilCode")]
        public string PupilCode { get; set; }
        public int? PupilID { get; set; }
        public string[] AbsenceString { get; set; }
        public IDictionary<string, string> AbsenceWeekDicString { get; set; }
        public int? CntK { get; set; }
        public int? CntP { get; set; }
        public int? CntTS { get; set; }
    }
}






