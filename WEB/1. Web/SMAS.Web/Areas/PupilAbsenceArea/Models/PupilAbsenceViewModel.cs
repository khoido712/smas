/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.PupilAbsenceArea.Models
{
    public class PupilAbsenceViewModel
    {
        //public System.Int32 AcademicYearID { get; set; }								
        //public System.Int32 PupilID { get; set; }								
        //public System.Int32 SchoolID { get; set; }								
        //public System.Int32 EducationLevelID { get; set; }								
        //public System.Int32 ClassID { get; set; }								
        //public System.Int32 Section { get; set; }								
        //public System.DateTime AbsentDate { get; set; }								
        //public System.Nullable<System.Boolean> IsAccepted { get; set; }								
        //public System.String PupilCode { get; set; }		
						
        public int PupilID { get; set; }
        [ResourceDisplayName("PupilAbsent_Column_PupilCode")]
        public string PupilCode { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string PupilName { get; set; }
        public int[] DateAbsent { get; set; }
        public string Disabled { get; set; }

        public bool? Status { get; set; }
        public string Errror { get; set; }
        public bool CheckPermission { get; set; }
    }
}


