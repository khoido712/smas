﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilAbsenceArea.Models
{
    public class PupilAbsentModel
    {
        int PupilID { get; set; }
        [ResourceDisplayName("PupilAbsent_Column_PupilCode")]
        string PupilCode { get; set; }
        [ResourceDisplayName("PupilProfile_Label_FullName")]
        string PupilName { get; set; }
        int PupilAbsent { get; set; }
        SelectListItem DateAbsent { get; set; }

    }
}