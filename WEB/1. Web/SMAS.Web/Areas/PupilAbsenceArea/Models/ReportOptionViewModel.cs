﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilAbsenceArea.Models
{
    public class ReportOptionViewModel
    {
        public int classID { get; set; }
        public string className { get; set; }
        public int educationLevelID { get; set; }
        public string educationName { get; set; }
        public string month { get; set; }
        public string monthyear { get; set; }
        public bool isAdmin { get; set; }
        public string sectionName { get; set; }
        public string stringFrom { get; set; }
        public string stringTo { get; set; }
        public string type { get; set; }
        public string stringdate { get; set; }
        public ReportOptionViewModel()
        {
        }
    }
}