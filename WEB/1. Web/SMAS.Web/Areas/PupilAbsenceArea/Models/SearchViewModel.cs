/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PupilAbsenceArea.Models
{
    public class SearchViewModel
    {
        [ScaffoldColumn(false)]
        public int? EducationLevelId { get; set; }
        [ScaffoldColumn(false)]
        public int? ClassId { get; set; }
        [ScaffoldColumn(false)]
        public int? SectionId { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilAbsence_Label_Date")]
        public DateTime? AbsentDate { get; set; }
            
    }
}