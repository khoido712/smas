﻿namespace SMAS.Web.Areas.TransferMarkToBookMarkArea
{
    public class TransferMarkToBookMarkConstants
    {
        public const string LISTEXAMINATION = "LISTEXAMINATION";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTEXAMINATIONSUBJECT = "LISTEXAMINATIONSUBJECT";
        public const string DEFAULT_EXAMINATION = "DEFAULT_EXAMINATION";
        public const string DEFAULT_EDUCATIONLEVEL = "DEFAULT_EDUCATIONLEVEL";

        public const string LISTTRANSFERMARKTOBOOKMARKVIEWMODEL = "LISTTRANSFERMARKTOBOOKMARKVIEWMODEL";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_MARKTYPE = "LIST_MARKTYPE";
        public const string LIST_MARKCOLUMN = "LIST_MARKCOLUMN";
        public const string DEFAULT_SEMESTER = "DEFAULT_SEMESTER";
    }
}