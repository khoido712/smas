﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.TransferMarkToBookMarkArea.Models;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using System;

namespace SMAS.Web.Areas.TransferMarkToBookMarkArea.Controllers
{
    public class TransferMarkToBookMarkController : Controller
    {
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public TransferMarkToBookMarkController(IExaminationBusiness ExaminationBusiness
            , IExaminationSubjectBusiness ExaminationSubjectBusiness
            , ICandidateBusiness CandidateBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , ISemeterDeclarationBusiness SemeterDeclarationBusiness
            , IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ExaminationBusiness = ExaminationBusiness;
            this.ExaminationSubjectBusiness = ExaminationSubjectBusiness;
            this.CandidateBusiness = CandidateBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SemeterDeclarationBusiness = SemeterDeclarationBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region set view data

        private void SetViewData()
        {
            //fill du lieu combobox
            GlobalInfo global = new GlobalInfo();
            List<Examination> lsE = ExaminationBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value}
                ,{"Status", new List<int> {SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED, SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED}}
            }).OrderByDescending(o => o.ToDate).ToList();

            int defaultExamination = lsE.FirstOrDefault()!=null?lsE.FirstOrDefault().ExaminationID:0;
            ViewData[TransferMarkToBookMarkConstants.LISTEXAMINATION] = new SelectList(lsE, "ExaminationID", "Title");
            ViewData[TransferMarkToBookMarkConstants.DEFAULT_EXAMINATION] = defaultExamination;

            //List<EducationLevel> lsEL = global.EducationLevels;
            //int defaultEL = lsEL.FirstOrDefault().EducationLevelID;

            List<EducationLevel> lsEL = new List<EducationLevel>();
            int defaultEL =0;
            ViewData[TransferMarkToBookMarkConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEL, "EducationLevelID", "Resolution");
            ViewData[TransferMarkToBookMarkConstants.DEFAULT_EDUCATIONLEVEL] = defaultEL;

            //List<ExaminationSubjectObject> lsESO = GetExaminationSubjectObject(defaultEL, defaultExamination).ToList();
            List<ExaminationSubjectObject> lsESO = new List<ExaminationSubjectObject>();
            ViewData[TransferMarkToBookMarkConstants.LISTEXAMINATIONSUBJECT] = new SelectList(lsESO, "ExaminationSubjectID", "SubjectName");

            List<ComboObject> lsSemester = CommonList.Semester();
            ViewData[TransferMarkToBookMarkConstants.LIST_SEMESTER] = new SelectList(lsSemester, "key", "value");
            int? semester = global.Semester;
            if (!semester.HasValue || semester == 0)
            {
                ViewData[TransferMarkToBookMarkConstants.DEFAULT_SEMESTER] = 2;
            }
            else
            {
                ViewData[TransferMarkToBookMarkConstants.DEFAULT_SEMESTER] = semester.Value;
            }

            ViewData[TransferMarkToBookMarkConstants.LIST_MARKTYPE] = new SelectList(CommonList.MarkType(), "key", "value");
            ViewData[TransferMarkToBookMarkConstants.LIST_MARKCOLUMN] = new SelectList(new string[] { });
        }

        #endregion set view data

        #region ho tro

        private IQueryable<ExaminationSubjectObject> GetExaminationSubjectObject(int EducationLevelID, int ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<ExaminationSubject> lsES = GetExaminationSubject(ExaminationID, EducationLevelID);
            IQueryable<ExaminationSubjectObject> lsESO = lsES.Select(o => new ExaminationSubjectObject
            {
                DurationInMinute = o.DurationInMinute,
                EducationLevelID = o.EducationLevelID,
                ExaminationID = o.ExaminationID,
                ExaminationSubjectID = o.ExaminationSubjectID,
                HasDetachableHead = o.HasDetachableHead,
                IsLocked = o.IsLocked,
                OrderNumberPrefix = o.OrderNumberPrefix,
                StartTime = o.StartTime,
                SubjectID = o.SubjectID,
                SubjectName = o.SubjectCat.SubjectName
            });
            return lsESO;
        }

        public IQueryable<ExaminationSubject> GetExaminationSubject(int? ExaminationID, int? EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            return ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"EducationLevelID",EducationLevelID},
                {"ExaminationID",ExaminationID}
            });
        }

        public List<EducationLevel> GetEducationLevelBaseOnExamination(int? ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            if (ExaminationID.HasValue)
            {
                var lsEduID = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"ExaminationID",ExaminationID}
            }).Select(o => o.EducationLevelID).Distinct().ToList();
                var lsEdu = global.EducationLevels.Where(o => lsEduID.Contains(o.EducationLevelID)).ToList();
                return lsEdu;
            }else
            {
                return new List<EducationLevel>();
            }
        }
        #endregion ho tro

        #region ajax load combobox

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationSubject(int EducationLevelID, int ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<ExaminationSubjectObject> lsESO = GetExaminationSubjectObject(EducationLevelID, ExaminationID);
            return Json(new SelectList(lsESO, "ExaminationSubjectID", "SubjectName"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadMarkColumn(string MarkType, int Semester)
        {
            if (MarkType == null)
            {
                return Json(new SelectList(new string[] { }), JsonRequestBehavior.AllowGet);
            }
            if (MarkType.ToUpper().CompareTo("HK") == 0)
            {
                List<ComboObject> lsCO = new List<ComboObject>();
                ComboObject co = new ComboObject("0", Res.Get("TransferMarkToBookMark_Label_KTHK"));
                lsCO.Add(co);
                return Json(new SelectList(lsCO, "key", "value"), JsonRequestBehavior.AllowGet);
            }
            else if (MarkType.ToUpper().CompareTo("V") == 0)
            {
                GlobalInfo global = new GlobalInfo();
                IQueryable<SemeterDeclaration> lsSD = SemeterDeclarationBusiness.Search(new Dictionary<string, object>()
                {
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"Semester",Semester},
                    {"SchoolID",global.SchoolID.Value}
                }).Where(o=>DateTime.Compare(DateTime.Now,o.AppliedDate)>=0).OrderByDescending(o=>o.AppliedDate);
                if (lsSD.Count() == 0)
                {
                    throw new BusinessException("TransferMarkToBookMark_Label_NoMarkConfigError");
                }
                else
                {
                    SemeterDeclaration sd = lsSD.FirstOrDefault();
                    int TwiceCoeffiecientMark = sd.TwiceCoeffiecientMark;
                    List<ComboObject> lsCO = new List<ComboObject>();
                    for (int i = 1; i <= TwiceCoeffiecientMark; i++)
                    {
                        ComboObject co = new ComboObject(i.ToString(), "V" + i.ToString());
                        lsCO.Add(co);
                    }
                    return Json(new SelectList(lsCO, "key", "value"), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new SelectList(new string[] { }), JsonRequestBehavior.AllowGet);
        }


         [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEducationLevelBaseOnExamination(int? ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            var lsEdu = GetEducationLevelBaseOnExamination(ExaminationID);
            return Json(new SelectList(lsEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }

         public string CheckExaminationStatus(int? ExaminationID)
         {
             Examination exam = ExaminationBusiness.Find(ExaminationID);
             if (exam != null && exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
             {
                 return "enable";
             }
             else
                 return "disable";
         }

        
        #endregion ajax load combobox

        #region search

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["ExaminationID"] = frm.ExaminationID;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["ExaminationSubjectID"] = frm.ExaminationSubjectID;
            Examination exam = ExaminationBusiness.Find(frm.ExaminationID);
            if (exam.CurrentStage != SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
            {
                throw new BusinessException("Validate_Exam_Mark_Completed");
            }
            //
            IEnumerable<TransferMarkToBookMarkViewModel> lst = this._Search(SearchInfo);
            ViewData[TransferMarkToBookMarkConstants.LISTTRANSFERMARKTOBOOKMARKVIEWMODEL] = lst;

            //
            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<TransferMarkToBookMarkViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<CandidateMark> query = CandidateBusiness.SearchMark(global.SchoolID.Value, (int)global.AppliedLevel.Value, SearchInfo).Where(o => o.RealMark != "");

            IEnumerable<TransferMarkToBookMarkViewModel> lst = query.Select(o => new TransferMarkToBookMarkViewModel
                                                                                {
                                                                                    CandidateID = o.CandidateID,
                                                                                    ClassID = o.ClassID,
                                                                                    PupilCode = o.PupilCode,
                                                                                    FullName = o.PupilName,
                                                                                    RoomID = o.RoomID,
                                                                                    NamedListCode = o.NamedListCode,
                                                                                    NamedListNumber = o.NamedListNumber,
                                                                                    ExamMark = o.ExamMark,
                                                                                    RealMark = o.RealMark,
                                                                                    Description = o.Description,
                                                                                    PupilID = o.PupilID,
                                                                                    RoomTitle = o.RoomName
                                                                                }).OrderBy(o => o.NamedListNumber).ToList();

            Dictionary<int, string> dicClassName = new Dictionary<int, string>();
            List<int> lstClassID = lst.Where(u => u.ClassID.HasValue).Select(u => u.ClassID.Value).Distinct().ToList();
            lstClassID.ForEach(u => dicClassName[u] = ClassProfileBusiness.Find(u).DisplayName);

            foreach (var item in lst)
                if (item.ClassID.HasValue)
                    item.ClassName = dicClassName[item.ClassID.Value];

            return lst;
        }

        #endregion search

        #region TransferMark

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult TransferMark(int? EducationLevel, int? ExaminationID, int? ExaminationSubjectID
            , int[] checkedTransferMarkToBookMark, int? cboSemester, string cboMarkType, int? cboMarkColumn)
        {
            if (!ExaminationSubjectID.HasValue)
            {
                List<object> Params = new List<object>();
                Params.Add("Candidate_Label_Subject");
                throw new BusinessException("TransferMarkToBookMark_Label_TransferMarkError", Params);
            }
            if (!cboSemester.HasValue)
            {
                List<object> Params = new List<object>();
                Params.Add("TransferMarkToBookMark_Label_Semester");
                throw new BusinessException("TransferMarkToBookMark_Label_TransferMarkError", Params);
            }

            if (cboMarkType == null || cboMarkType.Trim().Length == 0)
            {
                List<object> Params = new List<object>();
                Params.Add("TransferMarkToBookMark_Label_MarkType");
                throw new BusinessException("TransferMarkToBookMark_Label_TransferMarkError", Params);
            }
            if (!cboMarkColumn.HasValue)
            {
                List<object> Params = new List<object>();
                Params.Add("TransferMarkToBookMark_Label_MarkColumn");
                throw new BusinessException("TransferMarkToBookMark_Label_TransferMarkError", Params);
            }
            GlobalInfo global = GlobalInfo.getInstance();
            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);  

            if (isMovedHistory)
            {
                CandidateBusiness.TransferMarkHistory(global.SchoolID.Value, (int)global.AppliedLevel.Value, ExaminationSubjectID.Value
                , checkedTransferMarkToBookMark.ToList(), cboSemester.Value, cboMarkType, cboMarkColumn.Value);
            }
            else
            {
            CandidateBusiness.TransferMark(global.SchoolID.Value, (int)global.AppliedLevel.Value, ExaminationSubjectID.Value
                , checkedTransferMarkToBookMark.ToList(), cboSemester.Value, cboMarkType, cboMarkColumn.Value);
            }
            CandidateBusiness.Save();
            return Json(new JsonMessage(Res.Get("TransferMarkToBookMark_Label_TransferMarkSuccess")));
        }

        #endregion TransferMark
    }
}
