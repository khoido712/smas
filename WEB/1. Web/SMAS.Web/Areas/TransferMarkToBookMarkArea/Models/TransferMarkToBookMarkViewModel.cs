using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.TransferMarkToBookMarkArea.Models
{
    public class TransferMarkToBookMarkViewModel
    {
        public System.Int32 CandidateID { get; set; }

        public System.Nullable<System.Int32> ExaminationID { get; set; }

        public System.Nullable<System.Int32> SubjectID { get; set; }

        public System.Nullable<System.Int32> RoomID { get; set; }

        public System.Nullable<System.Int32> ClassID { get; set; }

        public System.Int32 PupilID { get; set; }

        public System.Nullable<System.Int32> OrderNumber { get; set; }

        public System.Nullable<System.Int32> EducationLevelID { get; set; }

        public System.String Judgement { get; set; }

        public System.Nullable<System.Int32> NamedListNumber { get; set; }

        public System.Nullable<System.Boolean> HasReason { get; set; }

        public System.String ReasonDetail { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthPlace")]
        public System.String BirthPlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        [ResourceDisplayName("Candidate_Label_NameListCode")]
        public string NamedListCode { get; set; }

        [ResourceDisplayName("SearchCandidateMark_Label_RoomTitle")]
        public string RoomTitle { get; set; }

        [ResourceDisplayName("SearchCandidateMark_Label_ExamMark")]
        public string ExamMark { get; set; }

        [ResourceDisplayName("SearchCandidateMark_Label_RealMark")]
        public string RealMark { get; set; }

        [ResourceDisplayName("SearchCandidateMark_Label_Description")]
        public string Description { get; set; }
    }
}
