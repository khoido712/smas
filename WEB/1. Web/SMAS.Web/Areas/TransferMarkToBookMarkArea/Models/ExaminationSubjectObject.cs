using System;

namespace SMAS.Web.Areas.TransferMarkToBookMarkArea.Models
{
    public class ExaminationSubjectObject
    {
        public int ExaminationSubjectID { get; set; }

        public int ExaminationID { get; set; }

        public int SubjectID { get; set; }

        public int EducationLevelID { get; set; }

        public Nullable<System.DateTime> StartTime { get; set; }

        public Nullable<int> DurationInMinute { get; set; }

        public string OrderNumberPrefix { get; set; }

        public bool IsLocked { get; set; }

        public bool HasDetachableHead { get; set; }

        public string SubjectName { get; set; }
    }
}
