﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.TransferMarkToBookMarkArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Examination_Label_TitleChoice")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TransferMarkToBookMarkConstants.LISTEXAMINATION)]
        [AdditionalMetadata("OnChange", "onExaminationChange(this)")]
        public System.Int32 ExaminationID { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_EducationLevelChoice")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TransferMarkToBookMarkConstants.LISTEDUCATIONLEVEL)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange(this)")]
        public int EducationLevel { get; set; }

        [ResourceDisplayName("Candidate_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("ViewDataKey", TransferMarkToBookMarkConstants.LISTEXAMINATIONSUBJECT)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [AdditionalMetadata("OnChange", "onExaminationSubjectChange(this)")]
        public int? ExaminationSubjectID { get; set; }
    }
}