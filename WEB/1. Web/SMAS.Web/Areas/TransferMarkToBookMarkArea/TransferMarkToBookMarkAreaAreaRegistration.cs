﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TransferMarkToBookMarkArea
{
    public class TransferMarkToBookMarkAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TransferMarkToBookMarkArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TransferMarkToBookMarkArea_default",
                "TransferMarkToBookMarkArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}