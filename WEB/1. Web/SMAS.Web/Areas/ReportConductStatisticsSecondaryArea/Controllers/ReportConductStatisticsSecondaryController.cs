﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.ReportConductStatisticsSecondaryArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.Web.Controllers;


namespace SMAS.Web.Areas.ReportConductStatisticsSecondaryArea.Controllers
{
    public class ReportConductStatisticsSecondaryController : BaseController
    {
        IDistrictBusiness DistrictBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        ITrainingTypeBusiness TrainingTypeBusiness;
        ISubCommitteeBusiness SubCommitteeBusiness;
        ICapacityStatisticBusiness CapacityStatisticBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IConductStatisticBusiness ConductStatisticBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        public ReportConductStatisticsSecondaryController(ICapacityStatisticBusiness CapacityStatisticBusiness, IDistrictBusiness districtBusiness,
                                                            IAcademicYearBusiness academicYearBusiness,
                                                            ITrainingTypeBusiness trainingTypeBusiness,
                                                            ISubCommitteeBusiness subCommitteeBusiness,
           IProcessedReportBusiness processedReportBusiness,
            IConductStatisticBusiness conductStatisticBusiness,
            IEducationLevelBusiness educationLevelBusiness)
        {
            this.DistrictBusiness = districtBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.CapacityStatisticBusiness = CapacityStatisticBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        private void GetViewData()
        {
            ViewData[ReportConductStatisticsSecondaryConstants.DISABLE_COMBOBOX] = false;
            GlobalInfo glo = new GlobalInfo();
           // -	cboDistrict: Nếu là account cấp Sở: thì lấy ra danh sách các Quận/Huyện thuộc Sở dựa vào hàm DistrictBusiness.Search() với ProvinceID của Sở lấy từ session. 
            //Nếu là account cấp Phòng: thì chỉ lấy ra Quận/Huyện tương ứng với DistrictID của Phòng. Disable combobox và không cho chọn lại          
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.EDUCATION_GRADE_SECONDARY).ToList();
           
            List<int> listAcademicYear = AcademicYearBusiness.GetListYearForSupervisingDept(glo.SupervisingDeptID.Value);
            ViewData[ReportConductStatisticsSecondaryConstants.LIST_ACADEMIC_YEAR] = listAcademicYear.Select(u => new SelectListItem { Text = u + " - " + (u + 1), Value = u.ToString(), Selected = false }).ToList();

            ViewData[ReportConductStatisticsSecondaryConstants.LIST_SEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value");
            ViewData[ReportConductStatisticsSecondaryConstants.LIST_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            List<TrainingType> listTrainingType = TrainingTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            ViewData[ReportConductStatisticsSecondaryConstants.LIST_TRAINING_TYPE] = new SelectList(listTrainingType, "TrainingTypeID", "Resolution");
            ViewData[ReportConductStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC_BY_SCHOOL] = new List<ConductStatisticsOfProvince23BO>();
            ViewData[ReportConductStatisticsSecondaryConstants.visibleList] = "0";

            if (glo.IsSuperVisingDeptRole)
            {
                ViewData[ReportConductStatisticsSecondaryConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() { 
                                                    new ViettelCheckboxList{ Label="Thống kê theo trường", Value=1, cchecked=true, disabled= false}, 
                                                    new ViettelCheckboxList{Label="Thống kê theo khối", Value=2, cchecked=false, disabled= false}, 
                                                    new ViettelCheckboxList{Label="Thống kê theo Quận/Huyện", Value=3, cchecked=false, disabled= false}
                                                };
                List<District> listDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "ProvinceID", glo.ProvinceID.Value }, { "IsActive", true } }).ToList();
                ViewData[ReportConductStatisticsSecondaryConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName");
            }
            //Neu la account phong thi disable cbb district
            else if (glo.IsSubSuperVisingDeptRole)
            {
                District District = DistrictBusiness.Find(glo.DistrictID);
                List<District> listDistrict = new List<SMAS.Models.Models.District>();
                listDistrict.Add(District);
                ViewData[ReportConductStatisticsSecondaryConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() { 
                                                    new ViettelCheckboxList{ Label="Thống kê theo trường", Value=1, cchecked=true, disabled= false}, 
                                                    new ViettelCheckboxList{Label="Thống kê theo khối", Value=2, cchecked=false, disabled= false}, 
                                                    new ViettelCheckboxList{Label="Thống kê theo Quận/Huyện", Value=3, cchecked=false, disabled= true}
                                                };
                ViewData[ReportConductStatisticsSecondaryConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName",glo.DistrictID);
                ViewData[ReportConductStatisticsSecondaryConstants.DISABLE_COMBOBOX] = true;
                ViewData[ReportConductStatisticsSecondaryConstants.DISTRICT_NAME] = District.DistrictName;
            }
        }

        public ActionResult Search(SearchViewModel model, FormCollection col)
        {
            GlobalInfo glo = new GlobalInfo();
            int reportType = int.Parse(col["ReportType"]);
            #region Thống kê theo khối
            if (reportType == 2)
            {
                //I. Nếu là account cấp Sở
                if (glo.IsSuperVisingDeptRole)
                {
                    CapacityConductReport conductSecondaryrep = new CapacityConductReport();
                    conductSecondaryrep.Year = model.AcademicYearID;
                    
                    conductSecondaryrep.EducationLevelID = 0;
                    conductSecondaryrep.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    conductSecondaryrep.TrainingTypeID = model.TrainingTypeID.Value;
                    conductSecondaryrep.SubcommitteeID = 0;
                    conductSecondaryrep.ProvinceID = glo.ProvinceID.Value;
                    if(model.DistrictID == null) model.DistrictID = 0;
                    conductSecondaryrep.DistrictID = model.DistrictID.Value;
                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(conductSecondaryrep);
                    string ReportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport( ReportCode,InputParameterHashKey);
                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
                //Sua lai ham IsSubSuperVisingDeptRole
                if (glo.IsSuperVisingDeptRole == false)
                {
                    CapacityConductReport conductSecondaryrep = new CapacityConductReport();
                    conductSecondaryrep.Year = model.AcademicYearID;

                    conductSecondaryrep.EducationLevelID = 0;
                    conductSecondaryrep.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    conductSecondaryrep.TrainingTypeID = model.TrainingTypeID.Value;
                    conductSecondaryrep.SubcommitteeID = 0;
                    conductSecondaryrep.ProvinceID = glo.ProvinceID.Value;
                  
                    conductSecondaryrep.DistrictID = glo.DistrictID.Value;
                   
                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(conductSecondaryrep);
                    string ReportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID });
                    }
                }
            }
            #endregion

            #region Thống kê theo quận huyện
            if (reportType == 3)
            {
                //I. Nếu là account cấp Sở

                if (glo.IsSuperVisingDeptRole)
                {
                    CapacityConductReport conductSecondaryrep = new CapacityConductReport();

                    conductSecondaryrep.Year = model.AcademicYearID;

                    if (model.EducationLevelID == null) model.EducationLevelID = 0;

                    conductSecondaryrep.EducationLevelID = model.EducationLevelID.Value;

                    conductSecondaryrep.Semester = model.Semester;

                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;

                    conductSecondaryrep.TrainingTypeID = model.TrainingTypeID.Value;

                    conductSecondaryrep.SubcommitteeID = 0;

                    conductSecondaryrep.ProvinceID = glo.ProvinceID.Value;

                    conductSecondaryrep.DistrictID = 0;

                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(conductSecondaryrep);

                    string ReportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_QUAN_HUYEN_CAP2;

                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);

                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0  });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;

                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }

                }

            } 
            #endregion

            #region Thống kê theo trường
            if (reportType == 1)
            {
                ViewData[ReportConductStatisticsSecondaryConstants.visibleList] = "1";
                //Neu la account cap So
                if (glo.IsSuperVisingDeptRole)
                {
                    CapacityConductReport CapacityConductReport = new CapacityConductReport();
                    CapacityConductReport.Year = model.AcademicYearID;
                    if (model.EducationLevelID == null) model.EducationLevelID = 0;
                    CapacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                    CapacityConductReport.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    CapacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                    CapacityConductReport.SubcommitteeID = 0;
                    CapacityConductReport.ProvinceID = glo.ProvinceID.Value;
                    if (model.DistrictID == null) model.DistrictID = 0;
                    CapacityConductReport.DistrictID = model.DistrictID.Value;
                    string inputHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(CapacityConductReport);
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_SO;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(reportCode, inputHashKey);
                    object o = entity;
                    if (o == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID ,  ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy")  });
                    }
                }
                //Neu la nguoi dung cap phong UserInfo.IsSubSuperVisingDeptRole() == true
                else
                {
                    CapacityConductReport CapacityConductReport = new CapacityConductReport();
                    CapacityConductReport.Year = model.AcademicYearID;
                    if (model.EducationLevelID == null) model.EducationLevelID = 0;
                    CapacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                    CapacityConductReport.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    CapacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                    CapacityConductReport.SubcommitteeID = 0;
                    CapacityConductReport.ProvinceID = glo.ProvinceID.Value;
                    CapacityConductReport.DistrictID = glo.DistrictID.Value;
                    string inputHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(CapacityConductReport);
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_PHONG;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(reportCode, inputHashKey);
                    object o = entity;
                    if (o == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID ,  ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy")  });
                    }
                }
            }
            #endregion

            return null;
        }


        public FileResult DownloadReport(int ProcessedReportID)
        {

            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SupervisingDeptID", GlobalInfo.SupervisingDeptID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2, 
                SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_QUAN_HUYEN_CAP2,
                SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_SO,
                SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_PHONG
               
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(ProcessedReportID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            if (model.ReportType == 3)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.AcademicYearID;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = model.EducationLevelID;
                dic["TrainingTypeID"] = model.TrainingTypeID;
                dic["ReportCode"] = "ThongKeHanhKiemCap2";
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                dic["DistrictID"] = 0;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID.Value;

                int FileID = 0;
                CapacityConductReport capacityConductReport = new CapacityConductReport();

                capacityConductReport.Year = model.AcademicYearID;
                if (model.EducationLevelID == null) model.EducationLevelID = 0;
                capacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                capacityConductReport.Semester = model.Semester;
                if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                capacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                capacityConductReport.SubcommitteeID = 0;
                capacityConductReport.ProvinceID = glo.ProvinceID.Value;
                capacityConductReport.DistrictID = 0;


                string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
             
                List<ConductStatisticsOfProvinceGroupByDistrict23BO> lstConductStatisticsOfProvinceByDistrictSecondary = ConductStatisticBusiness.CreateSGDConductStatisticsByDistrictSecondary(dic, input, out FileID);
                ViewData[ReportConductStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC] = lstConductStatisticsOfProvinceByDistrictSecondary;
                ViewData[ReportConductStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                return PartialView("_ListConducts");
                //ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_DATA] = CapacityStatisticBusiness.SearchByProvince23(dic);
            }

            if (model.ReportType == 2)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.AcademicYearID;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = 0;
                dic["TrainingTypeID"] = model.TrainingTypeID;
                dic["ReportCode"] = "ThongKeHanhKiemCap2";
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
               
               
                dic["SupervisingDeptID"] = glo.SupervisingDeptID;
                CapacityConductReport capacityConductReport = new CapacityConductReport();

                capacityConductReport.Year = model.AcademicYearID;
                if (model.EducationLevelID == null) model.EducationLevelID = 0;
                capacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                capacityConductReport.Semester = model.Semester;
                if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                capacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                capacityConductReport.SubcommitteeID = 0;
                capacityConductReport.ProvinceID = glo.ProvinceID.Value;
               
                int FileID = 0;
                if (glo.IsSuperVisingDeptRole)
                {
                    if (model.DistrictID == null) model.DistrictID = 0;
                    capacityConductReport.DistrictID = model.DistrictID.Value;
                    dic["DistrictID"] = model.DistrictID;
                    string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<ConductStatisticsOfProvinceGroupByLevel23BO> lstConductStatisticsOfProvinceByLevelSecondary = ConductStatisticBusiness.CreateSGDConductStatisticsByLevelSecondary(dic, input, out FileID);
                    ViewData[ReportConductStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC_BY_LEVEL] = lstConductStatisticsOfProvinceByLevelSecondary;
                    ViewData[ReportConductStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                }
                else
                {
                    capacityConductReport.DistrictID = glo.DistrictID.Value;
                    dic["DistrictID"] = glo.DistrictID;
                    string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<ConductStatisticsOfProvinceGroupByLevel23BO> lstConductStatisticsOfProvinceByLevelSecondary = ConductStatisticBusiness.CreatePGDConductStatisticsByLevelSecondary(dic, input, out FileID);
                    ViewData[ReportConductStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC_BY_LEVEL] = lstConductStatisticsOfProvinceByLevelSecondary;
                    ViewData[ReportConductStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                }
                return PartialView("_ListConductsByLevel");
                
            }
            if (model.ReportType == 1)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.AcademicYearID;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = model.EducationLevelID;
                dic["TrainingTypeID"] = model.TrainingTypeID;
                dic["ReportCode"] = "ThongKeHanhKiemCap2";
                dic["SupervisingDeptID"] = glo.SupervisingDeptID;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                
                CapacityConductReport capacityConductReport = new CapacityConductReport();
                capacityConductReport.Year = model.AcademicYearID;
                if (model.EducationLevelID == null) model.EducationLevelID = 0;
                capacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                capacityConductReport.Semester = model.Semester;
                if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                capacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                capacityConductReport.SubcommitteeID = 0;
                capacityConductReport.ProvinceID = glo.ProvinceID.Value;
                
                int FileID = 0;

                if (glo.IsSuperVisingDeptRole)
                {                
                    if (model.DistrictID == null) model.DistrictID = 0;
                    capacityConductReport.DistrictID = model.DistrictID.Value;
                    dic["DistrictID"] = model.DistrictID;
                    string inputHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<ConductStatisticsOfProvince23BO> lsConductStatistics = ConductStatisticBusiness.CreateSGDConductStatisticsSecondary(dic, inputHashKey, out FileID);
                    ViewData[ReportConductStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC_BY_SCHOOL] = lsConductStatistics;
                    ViewData[ReportConductStatisticsSecondaryConstants.FILE_ID] = FileID;
                }
                else
                {
                    capacityConductReport.DistrictID = glo.DistrictID.Value;
                    dic["DistrictID"] = glo.DistrictID;
                    string inputHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<ConductStatisticsOfProvince23BO> lsConductStatistics = ConductStatisticBusiness.CreatePGDConductStatisticsSecondary(dic, inputHashKey, out FileID);
                    ViewData[ReportConductStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC_BY_SCHOOL] = lsConductStatistics;
                    ViewData[ReportConductStatisticsSecondaryConstants.FILE_ID] = FileID;
                
                }
                return PartialView("_ListConductsBySchool");
            }
            return PartialView("");
        }
    }
}
