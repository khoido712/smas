﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportConductStatisticsSecondaryArea
{
    public class ReportConductStatisticsSecondaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportConductStatisticsSecondaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportConductStatisticsSecondaryArea_default",
                "ReportConductStatisticsSecondaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
