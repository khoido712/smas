﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportConductStatisticsSecondaryArea
{
    public class ReportConductStatisticsSecondaryConstants
    {
        public const string LIST_TYPESTATICS = "list_typestatics";
        public const string LIST_DISTRICT = "list_district";
        public const string LIST_ACADEMIC_YEAR = "list_academic_year";
        public const string LIST_SEMESTER = "list_semester";
        public const string LIST_EDUCATION_LEVEL = "list_education_level";
        public const string LIST_TRAINING_TYPE = "list_training_type";
        public const string LIST_CONDUCTSTATISTIC = "list_conduct_statistic";

        public const string LIST_CONDUCTSTATISTIC_BY_SCHOOL = "list_Conduct_By_School";
        public const string visibleList = "0";
        //public const string LIST_CONDUCTSTATISTIC_BY_SCHOOL_PHONG = "list_Conduct_By_School_Phong";
      
        public const string FILE_ID = "FileID";      

        public const string LIST_CONDUCTSTATISTIC_BY_LEVEL = "list_conduct_statistic_by_level";
        public const string GRID_REPORT_ID = "grid_report_id";
        public const string DISABLE_COMBOBOX = "cbb";
        public const string DISTRICT_NAME =" districtname";
    }
}