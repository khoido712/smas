﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Areas.ContractTypeArea.Models;

namespace SMAS.Web.Areas.ContractTypeArea.Controllers
{
    // TODO : HieuND, Chua xu li phan phan quyen, bo phan SkipCheckRole
    public class ContractTypeController : BaseController
    {
        private readonly IContractTypeBusiness ContractTypeBusiness;

        public ContractTypeController(IContractTypeBusiness contracttypeBusiness)
        {
            this.ContractTypeBusiness = contracttypeBusiness;
        }

        //
        // GET: /ContractType/
        public ActionResult Index()
        {
            SetViewDataPermission("ContractType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here
            IEnumerable<ContractTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[ContractTypeConstants.LIST_CONTRACTTYPE] = lst;
            GetContractStaff();
            return View();
        }

        //
        // GET: /ContractType/Search
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
            SearchInfo["Type"] = frm.Type;
            IEnumerable<ContractTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[ContractTypeConstants.LIST_CONTRACTTYPE] = lst;
            GetContractStaff();
            //Get view data here
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            ContractType contracttype = new ContractType();
            TryUpdateModel(contracttype);
            Utils.Utils.TrimObject(contracttype);

            this.ContractTypeBusiness.Insert(contracttype);
            this.ContractTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ContractTypeID)
        {
            ContractType contracttype = this.ContractTypeBusiness.Find(ContractTypeID);
            TryUpdateModel(contracttype);
            Utils.Utils.TrimObject(contracttype);
            this.ContractTypeBusiness.Update(contracttype);
            this.ContractTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.ContractTypeBusiness.Delete(id);
            this.ContractTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ContractTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ContractType> query = this.ContractTypeBusiness.Search(SearchInfo);
            IEnumerable<ContractTypeViewModel> lst = query.Select(o => new ContractTypeViewModel
            {
                ContractTypeID = o.ContractTypeID,
                Resolution = o.Resolution,
                Type = o.Type,
                ContractStaffType = string.Empty,
                Description = o.Description
            }).ToList();
            foreach (var item in lst)
                item.ContractStaffType = GetContractStaffName(item.Type);
            return lst.OrderBy(o=>o.Resolution);
        }

        private void GetContractStaff()
        {
            List<ComboObject> lstContractStaff = new List<ComboObject>();
            lstContractStaff.Add(new ComboObject(Res.Get("CONTRACT_TYPE_STAFF"), SMAS.Business.Common.GlobalConstants.CONTRACT_TYPE_STAFF.ToString()));
            lstContractStaff.Add(new ComboObject(Res.Get("CONTRACT_TYPE_NOSTAFF"), SMAS.Business.Common.GlobalConstants.CONTRACT_TYPE_NOSTAFF.ToString()));
            lstContractStaff.Add(new ComboObject(Res.Get("CONTRACT_TYPE_BUDGET"), SMAS.Business.Common.GlobalConstants.CONTRACT_TYPE_BUDGET.ToString()));
            SelectList lst = new SelectList(lstContractStaff, "value", "key");
            ViewData[ContractTypeConstants.LIST_STAFF_TYPE] = lst;
        }

        private string GetContractStaffName(int? value)
        {
            if (!value.HasValue) return "";

            string contractStaffName = "N/A";

            switch (value)
            {
                case SMAS.Business.Common.GlobalConstants.CONTRACT_TYPE_STAFF:
                    contractStaffName = Res.Get("CONTRACT_TYPE_STAFF");
                    break;
                case SMAS.Business.Common.GlobalConstants.CONTRACT_TYPE_NOSTAFF:
                    contractStaffName = Res.Get("CONTRACT_TYPE_NOSTAFF");
                    break;
                case SMAS.Business.Common.GlobalConstants.CONTRACT_TYPE_BUDGET:
                    contractStaffName = Res.Get("CONTRACT_TYPE_BUDGET");
                    break;
            }
            return contractStaffName;
        }
    }
}





