/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ContractTypeArea.Models
{
    public class ContractTypeViewModel
    {
        [ScaffoldColumn(false)]
		public int ContractTypeID { get; set; }

        [ResourceDisplayName("ContractType_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]	
		public string Resolution { get; set; }

        [ResourceDisplayName("ContractType_Label_Type")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ContractTypeConstants.LIST_STAFF_TYPE)]
		public Nullable<int> Type { get; set; }

        [ResourceDisplayName("ContractType_Label_Type")]
        [ScaffoldColumn(false)]
        public string ContractStaffType { get; set; }


        //[ScaffoldColumn(false)]
        [ResourceDisplayName("ContractType_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }			
					
    }
}


