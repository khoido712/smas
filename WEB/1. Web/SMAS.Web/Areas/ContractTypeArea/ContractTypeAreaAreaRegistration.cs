﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ContractTypeArea
{
    public class ContractTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ContractTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ContractTypeArea_default",
                "ContractTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
