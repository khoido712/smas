﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkStatisticsSecondaryDKArea
{
    public class MarkStatisticsSecondaryDKAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkStatisticsSecondaryDKArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkStatisticsSecondaryDKArea_default",
                "MarkStatisticsSecondaryDKArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
