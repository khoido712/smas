﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel;

namespace SMAS.Web.Areas.MarkStatisticsSecondaryDKArea.Models
{
    public class MarkStatisticsViewModel
    {
        [ResourceDisplayName("MarkStatistics_Label_SchoolName")]
        public string SchoolName { get; set; }
        public int SchoolID { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_SentDate")]
        public DateTime? SentDate { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_TotalSchool")]
        public int TotalSchool { get; set; }

        [DisplayName("0,0")]
        public int M00 { get; set; }
        [DisplayName("0,3")]
        public int M03 { get; set; }
        [DisplayName("0,5")]
        public int M05 { get; set; }
        [DisplayName("0,8")]
        public int M08 { get; set; }
        [DisplayName("1,0")]
        public int M10 { get; set; }
        [DisplayName("1,3")]
        public int M13 { get; set; }
        [DisplayName("1,5")]
        public int M15 { get; set; }
        [DisplayName("1,8")]
        public int M18 { get; set; }
        [DisplayName("2,0")]
        public int M20 { get; set; }
        [DisplayName("2,3")]
        public int M23 { get; set; }
        [DisplayName("2,5")]
        public int M25 { get; set; }
        [DisplayName("2,8")]
        public int M28 { get; set; }
        [DisplayName("3,0")]
        public int M30 { get; set; }
        [DisplayName("3,3")]
        public int M33 { get; set; }
        [DisplayName("3,5")]
        public int M35 { get; set; }
        [DisplayName("3,8")]
        public int M38 { get; set; }
        [DisplayName("4,0")]
        public int M40 { get; set; }
        [DisplayName("4,3")]
        public int M43 { get; set; }
        [DisplayName("4,5")]
        public int M45 { get; set; }
        [DisplayName("4,8")]
        public int M48 { get; set; }
        [DisplayName("5,0")]
        public int M50 { get; set; }
        [DisplayName("5,3")]
        public int M53 { get; set; }
        [DisplayName("5,5")]
        public int M55 { get; set; }
        [DisplayName("5,8")]
        public int M58 { get; set; }
        [DisplayName("6,0")]
        public int M60 { get; set; }
        [DisplayName("6,3")]
        public int M63 { get; set; }
        [DisplayName("6,5")]
        public int M65 { get; set; }
        [DisplayName("6,8")]
        public int M68 { get; set; }
        [DisplayName("7,0")]
        public int M70 { get; set; }
        [DisplayName("7,3")]
        public int M73 { get; set; }
        [DisplayName("7,5")]
        public int M75 { get; set; }
        [DisplayName("7,8")]
        public int M78 { get; set; }
        [DisplayName("8,0")]
        public int M80 { get; set; }
        [DisplayName("8,3")]
        public int M83 { get; set; }
        [DisplayName("8,5")]
        public int M85 { get; set; }
        [DisplayName("8,8")]
        public int M88 { get; set; }
        [DisplayName("9,0")]
        public int M90 { get; set; }
        [DisplayName("9,3")]
        public int M93 { get; set; }
        [DisplayName("9,5")]
        public int M95 { get; set; }
        [DisplayName("9,8")]
        public int M98 { get; set; }
        [DisplayName("10")]
        public int M100 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_TotalTest")]
        public int TotalTest { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_TotalBelowAverage")]
        public int TotalBelowAverage { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_PercentBelowAverage")]
        public double PercentBelowAverage { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_TotalAboveAverage")]
        public int TotalAboveAverage { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_PercentAboveAverage")]
        public double PercentAboveAverage { get; set; }
        
    }
}