﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMarkInputSituationArea
{
    public class ReportMarkInputSituationConstants
    {

        public const string CBO_EDUCATIONLEVEL = "CboEducationLevel";
        public const string CBO_CLASS = "CboClass";
        public const string CBO_SEMESTER = "CboSemester";
    }
}