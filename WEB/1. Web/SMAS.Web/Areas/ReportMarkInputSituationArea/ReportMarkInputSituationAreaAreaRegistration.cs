﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportMarkInputSituationArea
{
    public class ReportMarkInputSituationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportMarkInputSituationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportMarkInputSituationArea_default",
                "ReportMarkInputSituationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
