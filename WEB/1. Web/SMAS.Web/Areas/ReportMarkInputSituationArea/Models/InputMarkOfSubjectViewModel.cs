﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ReportMarkInputSituationArea.Models
{
    public class InputMarkOfSubjectViewModel
    {
        public string SubjectName { get; set; }
        public List<int?> ListInterview { get; set; }
        public List<int?> ListWriting { get; set; }
        public List<int?> ListTwiceCoeffiecient { get; set; }
        public List<int?> ListFinal { get; set; }

        public static InputMarkOfSubjectViewModel Parse(InputMarkOfSubjectBO bo)
        {
            InputMarkOfSubjectViewModel model = new InputMarkOfSubjectViewModel();
            model.ListInterview = bo.ListInterview;
            model.ListWriting = bo.ListWriting;
            model.ListTwiceCoeffiecient = bo.ListTwiceCoeffiecient;
            model.ListFinal = bo.ListFinal;
            model.SubjectName = bo.SubjectName;
            return model;
        }
    }

    public class ListInputMarkViewModel
    {
        public List<string> ListClassName { get; set; }
        public List<InputMarkOfSubjectViewModel> ListSubject { get; set; }

        public static ListInputMarkViewModel Parse(ReportMarkInputSituationBO bo)
        {
            ListInputMarkViewModel model = new ListInputMarkViewModel();
            model.ListClassName = bo.ListClassName;
            model.ListSubject = bo.ListSubject.Select(x => InputMarkOfSubjectViewModel.Parse(x)).ToList();
            return model;
        }
    }

    //public class ReportMissingMarkSituationViewModel
    //{
    //    public List<string> ListSubjectName { get; set; }
    //    public List<MissingMarkOfPupilViewModel> ListPupil { get; set; }

    //    public static ReportMissingMarkSituationViewModel Parse(ReportMissingMarkSituationBO bo)
    //    {
    //        ReportMissingMarkSituationViewModel model = new ReportMissingMarkSituationViewModel();
    //        model.ListSubjectName = bo.ListSubjectName;
    //        model.ListPupil = bo.ListPupil.Select(x => MissingMarkOfPupilViewModel.Parse(x)).ToList();
    //        return model;
    //    }
    //}

    //public class MissingMarkOfPupilViewModel
    //{
    //    public string PupilName { get; set; }
    //    public List<int?> ListMissingMark { get; set; }

    //    public static MissingMarkOfPupilViewModel Parse(MissingMarkOfPupilBO bo)
    //    {
    //        MissingMarkOfPupilViewModel model = new MissingMarkOfPupilViewModel();
    //        model.PupilName = bo.PupilName;
    //        model.ListMissingMark = bo.ListMissingMark;
    //        return model;
    //    }
    //}
}