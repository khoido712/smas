﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMarkInputSituationArea.Models
{
    public class SearchViewModel
    {
        public int rdoReportMarkInputSituation { get; set; }
        public int rdoReportPupilNotEnoughMark { get; set; }
        public int EducationLevelID { get; set; }
        public int ClassID { get; set; }
        public int Semester { get; set; }
    }
}