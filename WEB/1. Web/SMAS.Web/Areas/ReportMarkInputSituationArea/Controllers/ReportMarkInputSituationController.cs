﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ReportMarkInputSituationArea.Models;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.HtmlHelpers.Control;

namespace SMAS.Web.Areas.ReportMarkInputSituationArea.Controllers
{
    [SkipCheckRole]
    public class ReportMarkInputSituationController : BaseController
    {
        //
        // GET: /ReportMarkInputSituationArea/ReportMarkInputSituation/
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        public ReportMarkInputSituationController(ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IClassProfileBusiness ClassProfileBusiness, IEducationLevelBusiness EducationLevelBusiness, IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness)
        {
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportMarkInputSituationBusiness = ReportMarkInputSituationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
        }
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }
        private GlobalInfo Global = new GlobalInfo();
        public void SetViewData()
        {
            //Do du lieu EducationLevel
            ViewData[ReportMarkInputSituationConstants.CBO_EDUCATIONLEVEL] = Global.EducationLevels;
            int EducationLEvelID = (int)Global.EducationLevels.FirstOrDefault().EducationLevelID;
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["EducationLevelID"] = EducationLEvelID;
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            ViewData[ReportMarkInputSituationConstants.CBO_CLASS] = new List<ClassProfile>();
            if (lsClass.Count() > 0)
            {
                ViewData[ReportMarkInputSituationConstants.CBO_CLASS] = lsClass.ToList();

            }
            //Do du lieu Semester
            ViewData[ReportMarkInputSituationConstants.CBO_SEMESTER] = CommonList.Semester();
        }
        #region LoadCombobox
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? idEducationLevel)
        {
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (idEducationLevel == null)
            {
                idEducationLevel = 0;
            }
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            if (lsClass.Count() > 0)
            {
                lstClass = lsClass.ToList();
            }

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region ExportExcel
        public FileResult ExportExcel()
        {
            int ClassID = 0;
            int EducationLevelID = int.Parse(Request["EducationLevelID"]);
            if (Request["ClassID"] != null && Request["ClassID"] != "")
            {
                ClassID = int.Parse(Request["ClassID"]);
            }
            int SemesterID = int.Parse(Request["SemesterID"]);
            int ReportMarkInput = int.Parse(Request["ReportMarkInput"]);
            EducationLevel Edu = EducationLevelBusiness.Find(EducationLevelID);
            AcademicYear Aca = AcademicYearBusiness.Find(Global.AcademicYearID);
            if (ReportMarkInput == 1)
            {
                Stream excel = ReportMarkInputSituationBusiness.CreateReportMarkInputSituation(
                    Global.SchoolID.Value, Global.AcademicYearID.Value, SemesterID, EducationLevelID);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THPT_TKTINHHINHNHAPDIEM_KHOI10_HKI);
                string SchoolLevel = ReportUtils.ConvertAppliedLevelForReportName(Edu.Grade); ;
                string semester = ReportUtils.ConvertSemesterForReportName(SemesterID);
                string EducationLevel = ReportUtils.StripVNSign(Edu.Resolution);
                string ReportName = "HS_" + SchoolLevel + "_TKTinhHinhNhapDiem_" + EducationLevel + "_" + semester + ".xls";
                result.FileDownloadName = ReportName;
                return result;
            }
            if (ReportMarkInput == 2)
            {
                Stream excel = ReportMarkInputSituationBusiness.CreateReportPupilNotEnoughMark(
                    Global.SchoolID.Value, Global.AcademicYearID.Value, SemesterID, EducationLevelID, ClassID);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THCS_DSHOCSINHTHIEUDIEM_KHOI6_HKI);
                string SchoolLevel = ReportUtils.ConvertAppliedLevelForReportName(Edu.Grade); ;
                string semester = ReportUtils.ConvertSemesterForReportName(SemesterID);
                string ReportName = "";
                string EducationLevel = ReportUtils.StripVNSign(Edu.Resolution);

                if (ClassID == 0)
                {
                    ReportName = string.Format("HS_{0}_DSHocSinhThieuDiem_{1}_{2}.xls", SchoolLevel, EducationLevel, semester);
                }
                else
                {
                    ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                    string DisplayName = ReportUtils.StripVNSign(cp.DisplayName);
                    ReportName = string.Format("HS_{0}_DSHocSinhThieuDiem_{1}_{2}.xls", SchoolLevel, DisplayName, semester);
                }
                result.FileDownloadName = ReportName;
                return result;
            }
            return null;
        }
        #endregion

        //phuongh1 - 23/4/2013

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchInputMark(int Semester, int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            ListInputMarkViewModel model = ListInputMarkViewModel.Parse(
                ReportMarkInputSituationBusiness.GetInputMark(global.SchoolID.GetValueOrDefault(),
                global.AcademicYearID.GetValueOrDefault(), Semester, EducationLevelID));
            ViewData["ListInputMark"] = model;
            return PartialView("_ListInputMark");
        }

        //phuongh1 - 23/4/2013

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchMissingMark(int Semester, int EducationLevelID, int? ClassID)
        {
            GlobalInfo global = new GlobalInfo();
            ReportMissingMarkSituationBO bo = ReportMarkInputSituationBusiness.GetMissingMark(global.SchoolID.GetValueOrDefault(),
                global.AcademicYearID.GetValueOrDefault(), Semester, EducationLevelID, ClassID.GetValueOrDefault());
            if (bo.ListPupil == null || bo.ListPupil.Count == 0)
            {
                ViewData["ListMissingMark"] = Res.Get("common_Validate_NoData");
            }
            else if (bo.ListSubjectName == null || bo.ListSubjectName.Count == 0)
            {
                ViewData["ListMissingMark"] = Res.Get("common_Validate_NoData");
            }
            else
            {
                //Tạo table
                Table table = new Table();
                int index = 1;

                //Tạo Header
                table.Thead.SetAttributes(new[] { "class", "t-grid-header" });
                List<string> listStaticHeader = new List<string> {
                    Res.Get("Common_Column_Order"),
                    Res.Get("ReportMarkInputSituation_Column_Class"),
                    Res.Get("ReportMarkInputSituation_Column_Name")
                };
                table.Thead.AddTR(Tr.ParseTH(listStaticHeader, new[] { "class", "t-header", "rowspan", "2" })
                    .AddRangeTH(bo.ListSubjectName, new[] { "class", "t-header", "colspan", "3" }));

                Tr secondRow = new Tr();
                foreach (string subject in bo.ListSubjectName)
                {
                    secondRow.AddTH(Th.Parse(Res.Get("ReportMarkInputSituation_Row_Writing"), new[] { "class", "t-header" }));
                    secondRow.AddTH(Th.Parse(Res.Get("ReportMarkInputSituation_Row_TwiceCoeffiecient"), new[] { "class", "t-header" }));
                    secondRow.AddTH(Th.Parse(Res.Get("ReportMarkInputSituation_Row_Final"), new[] { "class", "t-header" }));
                }
                table.Thead.AddTR(secondRow);

                foreach (MissingMarkOfPupilBO pupil in bo.ListPupil)
                {
                    if (pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        table.Tbody.AddTR(new Tr()
                            .AddTD(index, new[] { "class", "t-cell-number" })
                            .AddTD(pupil.ClassName)
                            .AddTD(pupil.PupilName)
                            .AddRangeTD(pupil.ListMissingMark, new[] { "class", "t-cell-number" }));
                    }
                    else
                    {
                        table.Tbody.AddTR(new Tr()
                            .AddTD(index, new[] { "class", "t-cell-number" })
                            .AddTD(pupil.ClassName, new[] { "style", " color:red;" })
                            .AddTD(pupil.PupilName, new[] { "style", " color:red;" })
                            .AddRangeTD(pupil.ListMissingMark, new Dictionary<string, string> { { "class", "t-cell-number" }, { "style", "color:red;" } }));
                    }
                    index++;
                }
                ViewData["ListMissingMark"] = string.Format("<div class=\"t-widget t-grid\">{0}</div>", table.ToHTML());
            }
            return PartialView("_ListMissingMark");
        }
    }
}
