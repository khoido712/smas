﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ViolatedInvigilatorArea.Models;
using SMAS.Web.Filter;

using SMAS.Models.Models;
using System.IO;

namespace SMAS.Web.Areas.ViolatedInvigilatorArea.Controllers
{
    public class ViolatedInvigilatorController : BaseController
    {
        private readonly IViolatedInvigilatorBusiness ViolatedInvigilatorBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly IInvigilatorBusiness InvigilatorBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        private readonly IInvigilatorAssignmentBusiness InvigilatorAssignmentBusiness;

        public ViolatedInvigilatorController(IViolatedInvigilatorBusiness violatedInvigilatorBusiness,
                                            IExaminationBusiness examinationBusiness,
                                            ISchoolFacultyBusiness schoolFacultyBusiness,
                                            IExaminationSubjectBusiness examinationSubjectBusiness,
                                            IInvigilatorBusiness invigilatorBusiness,
                                            IExaminationRoomBusiness examinationRoomBusiness,
                                            IInvigilatorAssignmentBusiness invigilatorAssignmentBusiness)
        {
            this.ViolatedInvigilatorBusiness = violatedInvigilatorBusiness;
            this.ExaminationBusiness = examinationBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.ExaminationSubjectBusiness = examinationSubjectBusiness;
            this.InvigilatorBusiness = invigilatorBusiness;
            this.ExaminationRoomBusiness = examinationRoomBusiness;
            this.InvigilatorAssignmentBusiness = invigilatorAssignmentBusiness;
        }

        public ActionResult Index()
        {
            ViewData[ViolatedInvigilatorConstants.LIST_VIOLATED_INVIGILATOR] = new List<ViolatedInvigilatorViewModel>();
            GetViewData();
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                Utils.Utils.TrimObject(frm);

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

                SearchInfo["EducationLevelID"] = frm.EducationLevelID.HasValue && frm.EducationLevelID.Value > 0 ? frm.EducationLevelID.Value : 0;
                SearchInfo["ExaminationID"] = frm.ExaminationID;
                SearchInfo["ExaminationSubjectID"] = frm.ExaminationSubjectID.HasValue && frm.ExaminationSubjectID.Value > 0 ? frm.ExaminationSubjectID.Value : 0;
                SearchInfo["SchoolFacultyID"] = frm.SchoolFacultyID.HasValue && frm.SchoolFacultyID.Value > 0 ? frm.SchoolFacultyID.Value : 0;
                SearchInfo["TeacherName"] = !string.IsNullOrEmpty(frm.TeacherName) ? frm.TeacherName : "";
                SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;

                Examination exam = ExaminationBusiness.Find(frm.ExaminationID);

                ViewData[ViolatedInvigilatorConstants.LIST_VIOLATED_INVIGILATOR] = this._Search(SearchInfo);
                ViewData[ViolatedInvigilatorConstants.ENABLE_DELETE] = exam != null && (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED);
            }
            else
            {
                ViewData[ViolatedInvigilatorConstants.LIST_VIOLATED_INVIGILATOR] = new List<ViolatedInvigilatorViewModel>();
                ViewData[ViolatedInvigilatorConstants.ENABLE_DELETE] = false;
            }
            return PartialView("_List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(ViolatedInvigilatorCreateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.CreateInvigilatorAssignmentIDs != null && model.CreateInvigilatorAssignmentIDs.Length > 0)
                {
                    GlobalInfo glo = new GlobalInfo();
                    ViolatedInvigilatorBusiness.InsertAll(glo.SchoolID.Value, glo.AppliedLevel.Value, model.ViolationDetail, model.DisciplinedForm, model.CreateInvigilatorAssignmentIDs.ToList());
                    ViolatedInvigilatorBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Common_Validate_NoRecordSelected"), JsonMessage.ERROR));
                }
            }
            else
            {
                throw new BusinessException(string.Join(",", ModelState.Where(u => u.Value.Errors.Count > 0).SelectMany(u => u.Value.Errors).Select(u => u.ErrorMessage)));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int ViolatedInvigilatorID)
        {
            ViolatedInvigilator violatedinvigilator = this.ViolatedInvigilatorBusiness.Find(ViolatedInvigilatorID);
            CheckPermissionForAction(violatedinvigilator.ExaminationID.Value, "Examination");
            TryUpdateModel(violatedinvigilator);
            Utils.Utils.TrimObject(violatedinvigilator);
            this.ViolatedInvigilatorBusiness.Update(violatedinvigilator);
            this.ViolatedInvigilatorBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.ViolatedInvigilatorBusiness.Delete(id);
            this.ViolatedInvigilatorBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadExamSubject(int? educationLevelID, int? examinationID)
        {

            GlobalInfo glo = new GlobalInfo();
            List<ExaminationSubject> lstExamSubject = new List<ExaminationSubject>();
            if (educationLevelID.HasValue && educationLevelID.Value > 0 && examinationID.HasValue && examinationID.Value > 0)
            {
                Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
                dicExaminationSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
                dicExaminationSubject.Add("AppliedLevel", glo.AppliedLevel.Value);
                dicExaminationSubject.Add("EducationLevelID", educationLevelID);
                dicExaminationSubject.Add("ExaminationID", examinationID);
                lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            }
            return Json(lstExamSubject.Select(u => new ComboObject(u.ExaminationSubjectID.ToString(), u.SubjectCat.SubjectName)).ToList());
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadRoom(int examSubjectID)
        {
            List<ExaminationRoom> lstRooms = ExaminationRoomBusiness.All.Where(u => u.ExaminationSubjectID == examSubjectID).ToList();
            return Json(lstRooms.Select(u => new ComboObject(u.ExaminationRoomID.ToString(), u.RoomTitle)).ToList());
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadEducationLevel(int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            if (examinationID.HasValue && examinationID.Value > 0)
                dicExaminationSubject.Add("ExaminationID", examinationID);
            else
            {
                Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);  
            }
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            var lstEdu = lstExamSubject.Select(u => new { u.EducationLevelID, u.EducationLevel.Resolution }).Distinct()
                .OrderBy(o => o.EducationLevelID).ToList();
            return Json(new SelectList(lstEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);            
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadInvigilator(int roomId)
        {
            GlobalInfo glo = new GlobalInfo();
            ViewData[ViolatedInvigilatorConstants.LIST_INVIGILATOR] = InvigilatorAssignmentBusiness.All.Where(u => u.RoomID == roomId)
                                                                        .Select(u => new InvigilatorGridViewModel
                                                                        {
                                                                            InvivilatorAssignmentID = u.InvigilatorAssignmentID,
                                                                            InvigilatorID = u.InvigilatorID,
                                                                            BirthDay = u.Invigilator.Employee.BirthDate.Value,
                                                                            Genre = u.Invigilator.Employee.Genre,
                                                                            TeacherName = u.Invigilator.Employee.FullName,
                                                                            Name = u.Invigilator.Employee.Name
                                                                        }).ToList().OrderBy(o => o.Name).ThenBy(o => o.TeacherName).ToList();
            return PartialView("_InvigilatorAsignments");
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAll(int[] ViolatedInvigilators)
        {
            if (ViolatedInvigilators != null && ViolatedInvigilators.Length > 0)
            {
                GlobalInfo glo = new GlobalInfo();
                ViolatedInvigilatorBusiness.DeleteAll(glo.SchoolID.Value, glo.AppliedLevel.Value, ViolatedInvigilators.ToList());
                this.ViolatedInvigilatorBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_NoRecordSelected"), JsonMessage.ERROR));
            }
        }

        public FileResult Export(SearchViewModel frm)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo GlobalInfo = new GlobalInfo();

                Utils.Utils.TrimObject(frm);

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

                SearchInfo["EducationLevelID"] = frm.EducationLevelID.HasValue && frm.EducationLevelID.Value > 0 ? frm.EducationLevelID.Value : 0;
                SearchInfo["ExaminationID"] = frm.ExaminationID;
                SearchInfo["ExaminationSubjectID"] = frm.ExaminationSubjectID.HasValue && frm.ExaminationSubjectID.Value > 0 ? frm.ExaminationSubjectID.Value : 0;
                SearchInfo["SchoolFacultyID"] = frm.SchoolFacultyID.HasValue && frm.SchoolFacultyID.Value > 0 ? frm.SchoolFacultyID.Value : 0;
                SearchInfo["TeacherName"] = !string.IsNullOrEmpty(frm.TeacherName) ? frm.TeacherName : "";

                SearchInfo["SchoolID"] = GlobalInfo.SchoolID.Value;
                SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;

                string fileName = string.Empty;
                Stream stream = ViolatedInvigilatorBusiness.CreateViolatedInvigilatorReport(SearchInfo, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }

        private IEnumerable<ViolatedInvigilatorViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo glo = new GlobalInfo();
            IQueryable<ViolatedInvigilator> query = this.ViolatedInvigilatorBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo);
            IQueryable<ViolatedInvigilatorViewModel> lst = query.Select(o => new ViolatedInvigilatorViewModel
            {
                BirthDay = o.Employee.BirthDate.Value,
                EducationLevelResolution = o.EducationLevel.Resolution,
                ExaminationSubjectID = o.EducationLevelID.Value,
                ExaminationTitle = o.Examination.Title,
                FullName = o.Employee.FullName,
                Name = o.Employee.Name,
                Genre = o.Employee.Genre,
                RoomTitle = o.ExaminationRoom.RoomTitle,
                SubjectName = o.ExaminationSubject.SubjectCat.SubjectName,
                ViolationDetail = o.ViolationDetail,
                DisciplinedForm = o.DisciplinedForm,
                ViolatedInvigilatorID = o.ViolatedInvigilatorID
            });
            return lst.ToList().OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();

            Dictionary<string, object> dicExamination = new Dictionary<string, object>();
            dicExamination.Add("AcademicYearID", glo.AcademicYearID.Value);
            dicExamination.Add("AppliedLevel", glo.AppliedLevel.Value);
            IQueryable<Examination> lstQExam = ExaminationBusiness.SearchBySchool(glo.SchoolID.Value, dicExamination);
            List<Examination> lstExam = lstQExam.Where(u => u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED
                                    || u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED
                                    || u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_FINISHED).OrderByDescending(u => u.ToDate).ToList();
            List<Examination> lstExamCreate = lstExam.Where(u => u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED).OrderByDescending(u => u.ToDate).ToList();
            ViewData[ViolatedInvigilatorConstants.LIST_EXAMINATION] = new SelectList(lstExam, "ExaminationID", "Title");
            ViewData[ViolatedInvigilatorConstants.LIST_EXAMINATION_CREATE] = new SelectList(lstExamCreate, "ExaminationID", "Title");

            ViewData[ViolatedInvigilatorConstants.LIST_EDUCATION_LEVEL] = new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");

            //Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            //dicExaminationSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
            //dicExaminationSubject.Add("AppliedLevel", glo.AppliedLevel.Value);

            //List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            List<ExaminationSubject> lstExamSubject = new List<ExaminationSubject>();
            ViewData[ViolatedInvigilatorConstants.LIST_EXAMINATION_SUBJECT] = new SelectList(lstExamSubject.Select(u => new { u.ExaminationSubjectID, u.SubjectCat.SubjectName }).ToList(), "ExaminationSubjectID", "SubjectName");

            List<SchoolFaculty> lstSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object>()).ToList();
            ViewData[ViolatedInvigilatorConstants.LIST_SCHOOL_FACULTY] = new SelectList(lstSchoolFaculty, "SchoolFacultyID", "FacultyName");

            //List<ExaminationRoom> lstRooms = ExaminationRoomBusiness.All.ToList();
            //ViewData[ViolatedInvigilatorConstants.LIST_ROOM] = new SelectList(lstRooms, "ExaminationRoomID", "RoomTitle");

            Examination exam = lstExam.FirstOrDefault();
            ViewData[ViolatedInvigilatorConstants.ENABLE_CREATE] = exam != null && glo.IsCurrentYear && (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED);
            ViewData[ViolatedInvigilatorConstants.ENABLE_DELETE] = false;
        }
    }
}
