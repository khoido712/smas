﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ViolatedInvigilatorArea
{
    public class ViolatedInvigilatorAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ViolatedInvigilatorArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ViolatedInvigilatorArea_default",
                "ViolatedInvigilatorArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
