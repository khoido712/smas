using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.ViolatedInvigilatorArea.Models
{
    public class ViolatedInvigilatorCreateModel
    {
        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateEducationLevelID { get; set; }

        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? CreateExaminationID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateExaminationSubjectID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateRoomID { get; set; }

        [ResourceDisplayName("ViolatedInvigilator_Label_ViolationDetail")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ViolationDetail { get; set; }

        [ResourceDisplayName("ViolatedInvigilator_Label_DisciplinedForm")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DisciplinedForm { get; set; }

        [ResourceDisplayName("SchoolFaculty_Label_SchoolFacultyID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateSchoolFacultyID { get; set; }

        public int[] CreateInvigilatorAssignmentIDs { get; set; }
    }
}
