/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ViolatedInvigilatorArea.Models
{
    public class ViolatedInvigilatorViewModel
    {
        [ScaffoldColumn(false)]
        public int ViolatedInvigilatorID { get; set; }

        [ResourceDisplayName("Examination_Label_Title")]
        public string ExaminationTitle { get; set; }

        [ResourceDisplayName("ViolatedInvigilator_Label_ViolationDetail")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ViolationDetail { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        public string EducationLevelResolution { get; set; }

        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        public string RoomTitle { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Candidate_Label_ExaminationSubject")]
        public int ExaminationSubjectID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        public string FullName { get; set; }

        [ScaffoldColumn(false)]
        public string Name { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime? BirthDay { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public bool Genre { get; set; }

        [ResourceDisplayName("ViolatedInvigilator_Label_DisciplinedForm")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DisciplinedForm { get; set; }
    }
}


