﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ViolatedInvigilatorArea.Models
{
    public class InvigilatorGridViewModel
    {
        public int InvivilatorAssignmentID { get; set; }
        public int InvigilatorID { get; set; }
        public string TeacherName { get; set; }
        public string Name { get; set; }
        public DateTime? BirthDay { get; set; }
        public bool Genre { get; set; }
    }
}