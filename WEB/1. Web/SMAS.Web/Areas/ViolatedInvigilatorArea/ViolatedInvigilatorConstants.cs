/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ViolatedInvigilatorArea
{
    public class ViolatedInvigilatorConstants
    {
        public const string LIST_EXAMINATION = "listExamination";
        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_EXAMINATION_SUBJECT = "listExaminationSubject";
        public const string LIST_SCHOOL_FACULTY = "listSchoolFaculty";
        public const string LIST_VIOLATED_INVIGILATOR = "listViolatedInvigilator";
        public const string LIST_ROOM = "listRoom";
        public const string LIST_INVIGILATOR = "listInvigilator";

        public const string ENABLE_CREATE = "enable_create";
        public const string ENABLE_DELETE = "enable_delete";

        public const string LIST_EXAMINATION_CREATE = "listExaminationcreate";
    }
}