﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportFirstHighSchoolArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.Collections;

namespace SMAS.Web.Areas.ReportFirstHighSchoolArea.Controllers
{
    public class ReportFirstHighSchoolController : Controller
    {

        //
        // GET: /ReportFirstHighSchoolArea/ReportFirstHighSchool/
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IStatisticOfTertiaryBusiness StatisticOfTertiaryBusiness;
        private readonly IStatisticSubcommitteeOfTertiaryBusiness StatisticSubcommitteeOfTertiaryBusiness;
        private readonly IStatisticSubjectOfTertiaryBusiness StatisticSubjectOfTertiaryBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        public ReportFirstHighSchoolController(IStatisticOfTertiaryBusiness StatisticOfTertiaryBusiness
            , IStatisticSubjectOfTertiaryBusiness StatisticSubjectOfTertiaryBusiness
            , IStatisticSubcommitteeOfTertiaryBusiness StatisticSubcommitteeOfTertiaryBusiness
            , ISchoolSubjectBusiness SchoolSubjectBusiness, ISubjectCatBusiness SubjectCatBusiness
            , ISubCommitteeBusiness SubCommitteeBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IEmployeeBusiness EmployeeBusiness)
        {
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.StatisticOfTertiaryBusiness = StatisticOfTertiaryBusiness;
            this.StatisticSubjectOfTertiaryBusiness = StatisticSubjectOfTertiaryBusiness;
            this.StatisticSubcommitteeOfTertiaryBusiness = StatisticSubcommitteeOfTertiaryBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        private GlobalInfo Global = new GlobalInfo();

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }
        public void SetViewData()
        {
            ViewData[ReportFirstHighSchoolConstants.ERROR_MSG] = "";
            ViewData[ReportFirstHighSchoolConstants.LIST_SUBJECT_TITLE] = new List<SubjectCat>();
            //Grid Teacher
            List<FirstHighSchoolViewModel> ListTeacher = new List<FirstHighSchoolViewModel>();

            //Lấy ra danh sách môn học
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Global.AcademicYearID;
            IQueryable<SubjectCat> lsSubjectCat = SchoolSubjectBusiness.SearchBySchool(Global.SchoolID.Value, dic).Select(o => o.SubjectCat).Distinct();
            SubjectCat SubjectCat = new SubjectCat();
            SubjectCat.SubjectCatID = 0;
            SubjectCat.SubjectName = "Môn khác dạy GDCD";
            List<SubjectCat> lstsc = new List<SubjectCat>();
            if (lsSubjectCat.Count() > 0)
            {
                lstsc = lsSubjectCat.Where(o => o.IsActive == true).ToList();
            }
            //Gán thêm môn GDCD
            lstsc.Add(SubjectCat);
            //Gan vao ViewData
            ViewData[ReportFirstHighSchoolConstants.LIST_SUBJECT_TITLE] = lstsc;

            //Lấy dữ liệu để hiển thị số giáo viên của từng môn
            IQueryable<StatisticSubjectOfTertiary> lsStatisticSubjectOfTertiary = StatisticSubjectOfTertiaryBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            List<StatisticSubjectOfTertiary> lstStatisticSubjectOfTertiary = new List<StatisticSubjectOfTertiary>();
            if (lsStatisticSubjectOfTertiary.Count() > 0)
            {
                lstStatisticSubjectOfTertiary = lsStatisticSubjectOfTertiary.ToList();
            }
            IQueryable<StatisticOfTertiary> lsStatisticOfTertiary = StatisticOfTertiaryBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            if (lsStatisticOfTertiary.Count() > 0)
            {
                List<StatisticOfTertiary> lstsot = lsStatisticOfTertiary.ToList();
                FirstHighSchoolViewModel frm = new FirstHighSchoolViewModel();
                StatisticOfTertiary Sta = lstsot.FirstOrDefault();
                frm.NumOfStaff = Sta.NumOfStaff;
                frm.NumOfStandard = Sta.NumOfStandard;
                frm.NumOfUnderStandard = Sta.NumOfUnderStandard;
                frm.NumOfOverStandard = Sta.NumOfOverStandard;
                frm.SpecialType = "";
                frm.TotalTeacher = 0;
                frm.TotalNumOfTeacherPerSubject = new Dictionary<string, object>();
                foreach (SubjectCat sc in lstsc)
                {
                    int? SubCatID = sc.SubjectCatID;
                    if (sc.SubjectCatID == 0)
                    {
                        SubCatID = null;
                        frm.SpecialType = "1";
                    }
                    IEnumerable<StatisticSubjectOfTertiary> lsssot = lstStatisticSubjectOfTertiary.Where(o => o.SubjectID == SubCatID);
                    if (lsssot.Count() > 0)
                    {
                        StatisticSubjectOfTertiary ssoterti = lsssot.FirstOrDefault();
                        if (ssoterti.TotalTeacher != null)
                        { frm.TotalTeacher += ssoterti.TotalTeacher.Value; }
                        frm.TotalNumOfTeacherPerSubject[sc.SubjectCatID.ToString() + "_" + frm.SpecialType] = ssoterti.TotalTeacher;
                    }
                    else
                    {
                        frm.TotalNumOfTeacherPerSubject[sc.SubjectCatID.ToString() + "_" + frm.SpecialType] = null;
                    }

                }
                StatisticOfTertiary sot = lsStatisticOfTertiary.FirstOrDefault();
                frm.PupilFromSecondary = sot.PupilFromSecondary;
                frm.PupilOfOther = sot.PupilOfOther;
                frm.ComputerRoom = sot.ComputerRoom;
                frm.NumberOfComputer = sot.NumberOfComputer;
                frm.NumberOfComputerInternet = sot.NumberOfComputerInternetConnected;
                frm.HasWebsite = sot.HasWebsite.Value;
                frm.HasDatabase = sot.HasDatabase.Value;
                frm.LearningRoom = sot.LearningRoom;
                ListTeacher.Add(frm);
            }
            else
            {
                FirstHighSchoolViewModel frm = new FirstHighSchoolViewModel();
                ListTeacher.Add(frm);
            }
            ViewData[ReportFirstHighSchoolConstants.LIST_TEACHER] = ListTeacher;
            IQueryable<SubCommittee> lsSubCommittee = SubCommitteeBusiness.All;

            ViewData[ReportFirstHighSchoolConstants.COUNT_SUB] = 0;
            ViewData[ReportFirstHighSchoolConstants.LIST_SUB] = new List<SubCommittee>();
            List<SubCommittee> lstSubCommittee = new List<SubCommittee>();

            if (lsSubCommittee.Count() > 0)
            {
                lstSubCommittee = lsSubCommittee.ToList();
                SubCommittee SubCom = new SubCommittee();
                SubCom.SubCommitteeID = -1;
                SubCom.Resolution = Res.Get("FirstHighSchool_Label_HigherEducation");
                lstSubCommittee.Add(SubCom);
                SubCommittee SubCom1 = new SubCommittee();
                SubCom1.SubCommitteeID = 0;
                SubCom1.Resolution = Res.Get("FirstHighSchool_Label_NotHigherEducation");
                lstSubCommittee.Add(SubCom1);
                ViewData[ReportFirstHighSchoolConstants.COUNT_SUB] = lstSubCommittee.Count;
                ViewData[ReportFirstHighSchoolConstants.LIST_SUB] = lstSubCommittee;
            }
            List<FirstHighSchoolViewModel> ListPupil = new List<FirstHighSchoolViewModel>();
            List<EducationLevel> ListEducationLevel = Global.EducationLevels;
            IQueryable<StatisticSubcommitteeOfTertiary> lsStatisticSubCommitteeOfTertiary = StatisticSubcommitteeOfTertiaryBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            List<StatisticSubcommitteeOfTertiary> lstStatisticSubcommitteeOfTertiary = new List<StatisticSubcommitteeOfTertiary>();

            if (lsStatisticSubCommitteeOfTertiary.Count() > 0)
            {
                lstStatisticSubcommitteeOfTertiary = lsStatisticSubCommitteeOfTertiary.ToList();
            }
            foreach (EducationLevel el in ListEducationLevel)
            {
                FirstHighSchoolViewModel frm = new FirstHighSchoolViewModel();
                frm.EducationLevel = el.Resolution;
                frm.SpecialType = "";
                frm.NumberClass = new Dictionary<string, object>();
                frm.NumberPupil = new Dictionary<string, object>();
                frm.SumClass = 0;
                frm.SumPupil = 0;
                foreach (SubCommittee sc in lstSubCommittee)
                {
                    int? SubID = sc.SubCommitteeID;
                    if (sc.SubCommitteeID == -1)
                    {
                        frm.SpecialType = "1";
                        SubID = null;
                    }
                    if (sc.SubCommitteeID == 0)
                    {
                        frm.SpecialType = "2";
                        SubID = null;
                    }
                    IEnumerable<StatisticSubcommitteeOfTertiary> lsssot = lstStatisticSubcommitteeOfTertiary.Where(o => o.EducationLevelID == el.EducationLevelID && o.SubCommitteeID == SubID);
                    if (lsssot.Count() > 0)
                    {
                        StatisticSubcommitteeOfTertiary ssoter = lsssot.FirstOrDefault();
                        if (ssoter.TotalClass != null)
                        {
                            frm.SumClass += ssoter.TotalClass.Value;

                        }
                        if (ssoter.TotalPupil != null)
                        {
                            frm.SumPupil += ssoter.TotalPupil.Value;
                        }
                        frm.NumberClass[el.EducationLevelID + sc.Resolution + "Class" + "_" + frm.SpecialType] = ssoter.TotalClass;
                        frm.NumberPupil[el.EducationLevelID + sc.Resolution + "Pupil" + "_" + frm.SpecialType] = ssoter.TotalPupil;
                    }
                    else
                    {
                        frm.NumberClass[el.EducationLevelID + sc.Resolution + "Class" + "_" + frm.SpecialType] = null;
                        frm.NumberPupil[el.EducationLevelID + sc.Resolution + "Pupil" + "_" + frm.SpecialType] = null;
                    }
                }
                frm.EducationLevelID = el.EducationLevelID;
                ListPupil.Add(frm);
            }
            ViewData[ReportFirstHighSchoolConstants.LIST_PUPIL] = ListPupil;

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendInfo(FirstHighSchoolViewModel frm, FormCollection fc)
        {
            //Lấy dữ liệu trên form tạo thành đối tượng: StatisticOfTertiary sot.
            StatisticOfTertiary sot = new StatisticOfTertiary();
            sot.PupilFromSecondary = frm.PupilFromSecondary;
            sot.PupilOfOther = frm.PupilOfOther;
            sot.ComputerRoom = frm.ComputerRoom;
            sot.NumberOfComputer = frm.NumberOfComputer;
            sot.NumberOfComputerInternetConnected = frm.NumberOfComputerInternet;
            sot.HasWebsite = frm.HasWebsite;
            sot.HasDatabase = frm.HasDatabase;
            sot.LearningRoom = frm.LearningRoom;
            sot.NumOfStaff = frm.NumOfStaff;
            sot.NumOfStandard = frm.NumOfStandard;
            sot.NumOfOverStandard = frm.NumOfOverStandard;
            sot.NumOfUnderStandard = frm.NumOfUnderStandard;
            sot.SentDate = DateTime.Now;
            //Giá trị lấy từ grdEnvirollment, grdMaterialBase và grdTeacher (số GV trong biên chế, trên chuẩn, đạt chuẩn, dưới chuẩn.
            //List<StatisticSubcommitteeOfTertiary> lstStatisticSubCommittee. Dữ liệu lấy từ grdPupil
            List<StatisticSubcommitteeOfTertiary> lstStatisticSubcommitteeOfTertiary = new List<StatisticSubcommitteeOfTertiary>();
            List<EducationLevel> ListEducationLevel = Global.EducationLevels;
            IQueryable<SubCommittee> lsSubCommittee = SubCommitteeBusiness.All;
            if (lsSubCommittee.Count() > 0)
            {
                List<SubCommittee> lstSubCommittee = lsSubCommittee.ToList();
                SubCommittee SubCom = new SubCommittee();
                SubCom.SubCommitteeID = -1;
                SubCom.Resolution = Res.Get("FirstHighSchool_Label_HigherEducation");
                lstSubCommittee.Add(SubCom);
                SubCommittee SubCom1 = new SubCommittee();
                SubCom1.SubCommitteeID = 0;
                SubCom1.Resolution = Res.Get("FirstHighSchool_Label_NotHigherEducation");
                lstSubCommittee.Add(SubCom);
                foreach (EducationLevel el in ListEducationLevel)
                {
                    foreach (SubCommittee sc in lstSubCommittee)
                    {
                        StatisticSubcommitteeOfTertiary ssot = new StatisticSubcommitteeOfTertiary();
                        ssot.EducationLevelID = el.EducationLevelID;

                        ssot.SubCommitteeID = sc.SubCommitteeID;
                        if (sc.SubCommitteeID == -1)
                        {
                            ssot.SubCommitteeID = null;
                        }
                        if (sc.SubCommitteeID == 0)
                        {
                            ssot.SubCommitteeID = null;
                        }
                        string temp = "";
                        if (sc.SubCommitteeID == -1)
                        {
                            ssot.SpecicalType = 1;
                            temp = "1";
                        }
                        if (sc.SubCommitteeID == 0)
                        {
                            ssot.SpecicalType = 2;
                            temp = "2";
                        }
                        if (fc[el.EducationLevelID + sc.Resolution + "Class" + "_" + temp] != "" && fc[el.EducationLevelID + sc.Resolution + "Class" + "_" + temp] != null)
                        {
                            ssot.TotalClass = int.Parse(fc[el.EducationLevelID + sc.Resolution + "Class" + "_" + temp]);
                        }
                        if (fc[el.EducationLevelID + sc.Resolution + "pupil" + "_" + temp] != "" && fc[el.EducationLevelID + sc.Resolution + "pupil" + "_" + temp] != null)
                        {
                            ssot.TotalPupil = int.Parse(fc[el.EducationLevelID + sc.Resolution + "pupil" + "_" + temp]);
                        }
                        ssot.SentDate = DateTime.Now;
                        lstStatisticSubcommitteeOfTertiary.Add(ssot);
                    }
                }
            }
            //List<StatisticSubjectOfTertiary> lstStatisticSubject. Dữ liệu lấy từ grdTeacher.

            List<StatisticSubjectOfTertiary> lstStatisticSubject = new List<StatisticSubjectOfTertiary>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Global.AcademicYearID;
            IQueryable<SubjectCat> lsSubjectCat = SchoolSubjectBusiness.SearchBySchool(Global.SchoolID.Value, dic).Select(o => o.SubjectCat).Distinct();
            SubjectCat SubjectCat = new SubjectCat();
            SubjectCat.SubjectCatID = 0;
            SubjectCat.SubjectName = "Môn khác dạy GDCD";
            List<SubjectCat> lstsc = new List<SubjectCat>();
            if (lsSubjectCat.Count() > 0)
            {

                lstsc = lsSubjectCat.Where(o => o.IsActive == true).ToList();

            }
            lstsc.Add(SubjectCat);
            foreach (SubjectCat sc in lstsc)
            {

                StatisticSubjectOfTertiary ssot = new StatisticSubjectOfTertiary();
                string temp = "";
                if (sc.SubjectCatID == 0)
                {
                    ssot.SpecicalType = 1;
                    temp = "1";
                }
                string SubjectID = sc.SubjectCatID.ToString();
                if (fc[SubjectID + "_" + temp] != "" && fc[SubjectID + "_" + temp] != null)
                {
                    ssot.TotalTeacher = int.Parse(fc[SubjectID + "_" + temp]);
                }
                ssot.SubjectID = sc.SubjectCatID;
                if (sc.SubjectCatID == 0)
                {
                    ssot.SubjectID = null;
                }
                ssot.SentDate = DateTime.Now;
                lstStatisticSubject.Add(ssot);

            }
            StatisticOfTertiaryBusiness.InsertOrUpdate(Global.SchoolID.Value, Global.AcademicYearID.Value, sot, lstStatisticSubcommitteeOfTertiary, lstStatisticSubject);
            StatisticOfTertiaryBusiness.Save();

            //Gọi hàm StatisticOfTertiaryBusiness.InsertOrUpdate(UserInfo.SchoolID, UserInfo.AcademicYearID, sot, lstStatisticSubCommittee, lstStatisticSubject)
            return Json(new JsonMessage(Res.Get("Common_Label_SendInfo")));
        }

        public FileResult ExportExcel()
        {

            AcademicYear Aca = AcademicYearBusiness.Find(Global.AcademicYearID);
            string ReportName = "";
            Stream excel = StatisticOfTertiaryBusiness.CreateSchoolReport(Global.AcademicYearID.Value, out ReportName);//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;

        }


        #region Tổng hợp số liệu
        public ActionResult Synthesis()
        {
            List<FirstHighSchoolViewModel> ListPupil = new List<FirstHighSchoolViewModel>();
            
            //Danh sách khối: UserInfo.EducationLevels
            List<EducationLevel> ListEducationLevel = Global.EducationLevels;
            //Danh sách ban học: SubCommitteeBusiness.Search()
            IQueryable<SubCommittee> lsSubCommittee = SubCommitteeBusiness.All;

            ViewData[ReportFirstHighSchoolConstants.COUNT_SUB] = 0;
            ViewData[ReportFirstHighSchoolConstants.LIST_SUB] = new List<SubCommittee>();
            List<SubCommittee> lstSubCommittee = new List<SubCommittee>();
            
            
            if (lsSubCommittee.Count() > 0)
            {
                lstSubCommittee = lsSubCommittee.ToList();

            }
            //Danh sách lớp: ClassProfileBusiness.SearchBySchool(UserInfo.SchoolID, dic)
            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = Global.AcademicYearID;
            IQueryable<ClassProfile> lsClassProfile = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dicClass);
            List<ClassProfile> lstClassProfile = new List<ClassProfile>();
            foreach (EducationLevel el in ListEducationLevel)
            {
                FirstHighSchoolViewModel frm = new FirstHighSchoolViewModel();
                frm.EducationLevelID = el.EducationLevelID;
                frm.EducationLevel = el.Resolution;
                frm.SpecialType = "";
                frm.NumberClass = new Dictionary<string, object>();
                frm.NumberPupil = new Dictionary<string, object>();
                
                frm.SumClass = 0;
                frm.SumPupil = 0;
                foreach (SubCommittee sub in lstSubCommittee)
                {
                   
                    int Pupil = 0;
                    if (lsClassProfile.Count() > 0)
                    {
                        lstClassProfile = lsClassProfile.Where(o => o.EducationLevelID == el.EducationLevelID&&o.SubCommitteeID==sub.SubCommitteeID).ToList();
                        frm.NumberClass[el.EducationLevelID + sub.Resolution + "Class" + "_" + frm.SpecialType] = lstClassProfile.Count;
                        
                        if (lstClassProfile.Count > 0)
                        {
                            foreach (ClassProfile cp in lstClassProfile)
                            {
                                Dictionary<string, object> dicPupil = new Dictionary<string, object>();
                                dicPupil["AcademicYearID"] = Global.AcademicYearID;
                                dicPupil["ClassID"] = cp.ClassProfileID;
                                dicPupil["check"] = "Check";
                                dicPupil["Status"] = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                                IQueryable<PupilOfClass> lsPupil = PupilOfClassBusiness.SearchBySchool(Global.SchoolID.Value, dicPupil);
                                Pupil += lsPupil.Count();
                            }
                        }
                        frm.NumberPupil[el.EducationLevelID + sub.Resolution + "Pupil" + "_" + frm.SpecialType] = Pupil;
                    }
                }
                ListPupil.Add(frm);
            }

            List<FirstHighSchoolViewModel> ListTeacher = new List<FirstHighSchoolViewModel>();
            Dictionary<string, object> dicTeacher = new Dictionary<string, object>();
            dicTeacher["AcademicYearID"] = Global.AcademicYearID;
            dicTeacher["EmployeeStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            IQueryable<Employee> lsEmployee = EmployeeBusiness.SearchTeacher(Global.SchoolID.Value, dicTeacher);
            FirstHighSchoolViewModel frm1 = new FirstHighSchoolViewModel();
            frm1.NumOfStaff = lsEmployee.Where(o=>o.ContractType.Type==SystemParamsInFile.CONTRACT_TYPE_STAFF).Count();
            frm1.NumOfOverStandard = lsEmployee.Where(o => o.GraduationLevel.FullfilmentStatus == SystemParamsInFile.FULLFILLMENT_STATUS_ABOVE_STANDARD).Count();
            frm1.NumOfStandard = lsEmployee.Where(o => o.GraduationLevel.FullfilmentStatus == SystemParamsInFile.FULLFILLMENT_STATUS_STANDARD).Count();
            frm1.NumOfUnderStandard = lsEmployee.Where(o => o.GraduationLevel.FullfilmentStatus == SystemParamsInFile.FULLFILLMENT_STATUS_UNDER_STANDARD).Count();

            IQueryable<SubjectCat> lsSubject = SchoolSubjectBusiness.SearchBySchool(Global.SchoolID.Value, dicClass).Select(o => o.SubjectCat).Distinct();
            //Danh sách môn học: SchoolSubjectBusiness.SearchBySchool(UserInfo.SchoolID, dic).Select(o => o.SubjectCat).Distinct()
            //Số giáo viên giảng dạy từng môn iqEmp.Where(o => o.SubjectCat.SubjectID = SubjectID).Count
            List<SubjectCat> lstsc = new List<SubjectCat>();
            if (lsSubject.Count() > 0)
            {
                frm1.TotalNumOfTeacherPerSubject = new Dictionary<string, object>();
                lstsc = lsSubject.ToList();
                foreach (SubjectCat sc in lstsc)
                {
                    frm1.TotalNumOfTeacherPerSubject[sc.SubjectCatID.ToString() + "_" + frm1.SpecialType] = lsEmployee.Where(o => o.SubjectCat.SubjectCatID == sc.SubjectCatID).Count();
                    
                }
            }
            ListTeacher.Add(frm1);
            ViewData[ReportFirstHighSchoolConstants.LIST_SUBJECT_TITLE] = lstsc;
            ViewData[ReportFirstHighSchoolConstants.LIST_TEACHER] = ListTeacher;
            ViewData[ReportFirstHighSchoolConstants.LIST_PUPIL] = ListPupil;
            ViewData[ReportFirstHighSchoolConstants.COUNT_SUB] = lstSubCommittee.Count;
            ViewData[ReportFirstHighSchoolConstants.LIST_SUB] = lstSubCommittee;
            return View("_Total");
        }

        #endregion

        #region import excel

        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments)
        {
            GlobalInfo global = new GlobalInfo();
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                string error = "";

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);

                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet MainSheet = oBook.GetSheet(1);
                string SupervisingDept = (string)MainSheet.GetCellValue(2, VTVector.dic['A']);
                string School = (string)MainSheet.GetCellValue(3, VTVector.dic['A']);
                string Year = (string)MainSheet.GetCellValue(5, VTVector.dic['A']);

                SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID.Value);
                SupervisingDept sd = SupervisingDeptBusiness.Find(global.SupervisingDeptID.Value);
                AcademicYear academic = AcademicYearBusiness.Find(global.AcademicYearID.Value);
                string YearTitle = academic.Year + "-" + (academic.Year + 1).ToString();
                bool isYearError = !Year.Replace(" ", "").Contains(YearTitle.Trim());

                var lsEdu = EducationLevelBusiness.Search(new Dictionary<string, object>() { { "Grade", 3 } }).ToList();

                if (!SupervisingDept.Contains(sd.SupervisingDeptName))
                {
                    error += "-Không phải file excel của sở " + sd.SupervisingDeptName + "<br>";
                }
                if (!School.Contains(sp.SchoolName))
                {
                    error += "Không phải file excel của trường " + sp.SchoolName + "<br>";
                }
                if (isYearError)
                {
                    error += "-File excel không phải là file điểm của năm " + academic.Year + "<br>";
                }

                //bat dau lay du lieu grid dau tien
                double xPupilFromSecondary = (double)MainSheet.GetCellValue(10, VTVector.dic['A']);
                double xPupilOfOther = (double)MainSheet.GetCellValue(10, VTVector.dic['B']);
                double xTotalPupil = (double)MainSheet.GetCellValue(10, VTVector.dic['C']);

                var xComputerRoom = (double)MainSheet.GetCellValue(15, VTVector.dic['A']);
                var xNumberOfComputer = (double)MainSheet.GetCellValue(15, VTVector.dic['B']);
                var xNumberOfComputerInternet = (double)MainSheet.GetCellValue(15, VTVector.dic['C']);
                var xHasWebsite = (string)MainSheet.GetCellValue(15, VTVector.dic['D']);
                var xHasDatabase = (string)MainSheet.GetCellValue(15, VTVector.dic['E']);
                var xLearningRoom = (double)MainSheet.GetCellValue(15, VTVector.dic['F']);

                //du lieu grid thu 3
                Dictionary<string, object> dicCCGV = new Dictionary<string, object>();
                Dictionary<string, object> dicTD = new Dictionary<string, object>();
                double NumOfStandard = 0;
                double NumOfUnderStandard = 0;
                double NumOfOverStandard = 0;
                double NumOfStaff = 0;
                //du lieu grid thu 2
                List<double> lsSumClass = new List<double>();
                List<double> lsSumPupil = new List<double>();
                Dictionary<string, object> dicBanClass = new Dictionary<string, object>();
                Dictionary<string, object> dicBanPupil = new Dictionary<string, object>();

                //tim vi tri cua co cau giao vien va trinh do ,grid thu 3
                int lsCCGV = 0;
                int lsTD = 0;
                List<string> lsSubject = new List<string>();
                int start = VTVector.dic['D'];
                for (int i = start;i<1000 ; i++)
                {
                    string isCCGVorTD = (string)MainSheet.GetCellValue(29, i);
                    string isSubject = (string)MainSheet.GetCellValue(30, i);
                    if (isCCGVorTD == null || isCCGVorTD.Trim().Length == 0)
                    {
                        if (isSubject == null || isSubject.Trim().Length == 0)
                        {
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    if (isCCGVorTD.Trim().ToLower().ToLower().Contains("cơ cấu giáo viên theo môn học"))
                    {
                        lsCCGV = i;
                    }
                    else if (isCCGVorTD.Trim().ToLower().ToLower().Contains("trình độ"))
                    {
                        lsTD = i;
                    }
                }


                //lay ra dc cac gia tri dua vao dictionary
                if (lsCCGV != 0 && lsTD != 0)
                {
                    for (int i = lsCCGV; i < lsTD; i++)
                    {
                        string xSubject = (string)MainSheet.GetCellValue(30, i);
                        double xTotalTeacherOfThisSubject = (double)MainSheet.GetCellValue(31, i);
                        string specialType = "";
                        SubjectCat sc = SubjectCatBusiness.Search(new Dictionary<string, object>() { { "SubjectName", xSubject } }).FirstOrDefault();
                        if (sc == null)
                        {
                            if (xSubject.Trim().ToLower().Contains("môn khác dạy gdcd"))
                            {
                                lsSubject.Add(xSubject);
                                specialType = "1";
                                dicCCGV["0" + "_" + specialType] = xTotalTeacherOfThisSubject;
                            }
                            else
                            {
                                error += "-Không tồn tại môn " + xSubject + "<br>";
                            }
                        }
                        else
                        {
                            lsSubject.Add(sc.SubjectName);
                            dicCCGV[sc.SubjectCatID.ToString() + "_" + specialType] = xTotalTeacherOfThisSubject;
                        }
                    }

                    double datChuan = (double)MainSheet.GetCellValue(31, lsTD);
                    double trenChuan = (double)MainSheet.GetCellValue(31, lsTD + 1);
                    double duoiChuan = (double)MainSheet.GetCellValue(31, lsTD + 2);
                    double TrongBienChe = (double)MainSheet.GetCellValue(31, VTVector.dic['B']);
                    NumOfStandard = datChuan;
                    NumOfOverStandard = trenChuan;
                    NumOfUnderStandard = duoiChuan;
                    NumOfStaff = TrongBienChe;
                }


                //grid thu 2
                for (int i = 22; i < 25; i++)
                {
                    double sumclass = (double)MainSheet.GetCellValue(i, VTVector.dic['B']);
                    double sumpupil = (double)MainSheet.GetCellValue(i, VTVector.dic['C']);
                    lsSumClass.Add(sumclass);
                    lsSumPupil.Add(sumpupil);
                }


                //lay du lieu grid thu 2
                //d18
                int d = VTVector.dic['D'];
                int nowColumn = d;
                for (int i = 0;i<1000; i++)
                {
                    nowColumn = i * 2 + d;
                    string specialType = "";
                    string ban = (string)MainSheet.GetCellValue(18, nowColumn);
                    if (!ban.Trim().ToUpper().Contains("BAN CƠ BẢN"))
                    {
                        double soLop10 = MainSheet.GetCellValue(22, i * 2 + d) != null ? (double)MainSheet.GetCellValue(22, i * 2 + d) : 0;
                        double soHS10 = MainSheet.GetCellValue(22, i * 2 + d + 1) != null ? (double)MainSheet.GetCellValue(22, i * 2 + d + 1) : 0;
                        double soLop11 = MainSheet.GetCellValue(23, i * 2 + d) != null ? (double)MainSheet.GetCellValue(23, i * 2 + d) : 0;
                        double soHS11 = MainSheet.GetCellValue(23, i * 2 + d + 1) != null ? (double)MainSheet.GetCellValue(23, i * 2 + d + 1) : 0;
                        double soLop12 = MainSheet.GetCellValue(24, i * 2 + d) != null ? (double)MainSheet.GetCellValue(24, i * 2 + d) : 0;
                        double soHS12 = MainSheet.GetCellValue(24, i * 2 + d + 1) != null ? (double)MainSheet.GetCellValue(24, i * 2 + d + 1) : 0;

                        SubCommittee sct = SubCommitteeBusiness.Search(new Dictionary<string, object>() { { "Resolution", ban.Trim() } }).FirstOrDefault();
                        if (sct == null)
                        {
                            error += "-Không tồn tại ban " + ban + "<br>";
                        }

                        dicBanClass[lsEdu[0].EducationLevelID + ban + "Class" + "_" + specialType] = soLop10;
                        dicBanPupil[lsEdu[0].EducationLevelID + ban + "Pupil" + "_" + specialType] = soHS10;

                        dicBanClass[lsEdu[1].EducationLevelID + ban + "Class" + "_" + specialType] = soLop11;
                        dicBanPupil[lsEdu[1].EducationLevelID + ban + "Pupil" + "_" + specialType] = soHS11;

                        dicBanClass[lsEdu[2].EducationLevelID + ban + "Class" + "_" + specialType] = soLop12;
                        dicBanPupil[lsEdu[2].EducationLevelID + ban + "Pupil" + "_" + specialType] = soHS12;

                    }
                    else
                    {
                        //bat dau duyet ban co ban
                        int startDuyetBanColumn = nowColumn + 2;
                        while (true)
                        {
                            string banCoban = (string)MainSheet.GetCellValue(20, startDuyetBanColumn);
                            if (banCoban == null || banCoban.Trim().Length == 0)
                            {
                                break;//vong while
                            }

                            if (banCoban.Contains("Môn NC"))
                            {
                                specialType = "1";
                            }
                            else if (banCoban.Contains("Không học NC"))
                            {
                                specialType = "2";
                            }
                            else
                            {
                                SubCommittee sct = SubCommitteeBusiness.Search(new Dictionary<string, object>() { { "Resolution", banCoban.Trim() } }).FirstOrDefault();
                                if (sct == null)
                                {
                                    error += "-Không tồn tại ban " + ban + "<br>";
                                }
                            }

                            double soLop10 = MainSheet.GetCellValue(22, startDuyetBanColumn) != null ? (double)MainSheet.GetCellValue(22, startDuyetBanColumn) : 0;
                            double soHS10 = MainSheet.GetCellValue(22, startDuyetBanColumn + 1) != null ? (double)MainSheet.GetCellValue(22, startDuyetBanColumn + 1) : 0;
                            double soLop11 = MainSheet.GetCellValue(23, startDuyetBanColumn) != null ? (double)MainSheet.GetCellValue(23, startDuyetBanColumn) : 0;
                            double soHS11 = MainSheet.GetCellValue(23, startDuyetBanColumn + 1) != null ? (double)MainSheet.GetCellValue(23, startDuyetBanColumn + 1) : 0;
                            double soLop12 = MainSheet.GetCellValue(24, startDuyetBanColumn) != null ? (double)MainSheet.GetCellValue(24, startDuyetBanColumn) : 0;
                            double soHS12 = MainSheet.GetCellValue(24, startDuyetBanColumn + 1) != null ? (double)MainSheet.GetCellValue(24, startDuyetBanColumn + 1) : 0;


                            dicBanClass[lsEdu[0].EducationLevelID + banCoban + "Class" + "_" + specialType] = soLop10;
                            dicBanPupil[lsEdu[0].EducationLevelID + banCoban + "Pupil" + "_" + specialType] = soHS10;

                            dicBanClass[lsEdu[1].EducationLevelID + banCoban + "Class" + "_" + specialType] = soLop11;
                            dicBanPupil[lsEdu[1].EducationLevelID + banCoban + "Pupil" + "_" + specialType] = soHS11;

                            dicBanClass[lsEdu[2].EducationLevelID + banCoban + "Class" + "_" + specialType] = soLop12;
                            dicBanPupil[lsEdu[2].EducationLevelID + banCoban + "Pupil" + "_" + specialType] = soHS12;

                            startDuyetBanColumn += 2;
                        }

                        break;//vong for
                    }
                }

                //bat dau dua vao viewData
                List<FirstHighSchoolViewModel> ListPupil = new List<FirstHighSchoolViewModel>();
                List<FirstHighSchoolViewModel> ListTeacher = new List<FirstHighSchoolViewModel>();
                FirstHighSchoolViewModel frm = new FirstHighSchoolViewModel();

                //cho grid 4
                frm.NumOfStaff = (int)NumOfStaff;
                frm.NumOfStandard = (int)NumOfStandard;
                frm.NumOfUnderStandard = (int)NumOfUnderStandard;
                frm.NumOfOverStandard = (int)NumOfOverStandard;
                frm.SpecialType = "";
                frm.TotalTeacher = 0;
                frm.TotalNumOfTeacherPerSubject = dicCCGV;

                //grid 1,2
                frm.PupilFromSecondary = (int)xPupilFromSecondary;
                frm.PupilOfOther = (int)xPupilOfOther;
                frm.ComputerRoom = (int)xComputerRoom;
                frm.NumberOfComputer = (int)xNumberOfComputer;
                frm.NumberOfComputerInternet = (int)xNumberOfComputerInternet;
                frm.HasWebsite = xHasWebsite.ToLower() == "có" ? true : false;
                frm.HasDatabase = xHasDatabase.ToLower() == "có" ? true : false; ;
                frm.LearningRoom = (int)xLearningRoom;

                ListTeacher.Add(frm);
                ViewData[ReportFirstHighSchoolConstants.LIST_TEACHER] = ListTeacher;

                //grid 3
                for (int i = 0; i < lsEdu.Count(); i++)
                {
                    EducationLevel el = lsEdu[i];
                    FirstHighSchoolViewModel frm2 = new FirstHighSchoolViewModel();
                    frm2.EducationLevel = el.Resolution;
                    frm2.SpecialType = "";
                    frm2.NumberClass = new Dictionary<string, object>();
                    frm2.NumberPupil = new Dictionary<string, object>();
                    frm2.SumClass = 0;
                    frm2.SumPupil = 0;
                    frm2.SumClass = (int)lsSumClass[i];
                    frm2.SumPupil = (int)lsSumPupil[i];

                    frm2.NumberClass = dicBanClass;
                    frm2.NumberPupil = dicBanPupil;
                    frm2.EducationLevelID = el.EducationLevelID;
                    ListPupil.Add(frm2);
                }
                ViewData[ReportFirstHighSchoolConstants.LIST_PUPIL] = ListPupil;
                //list vo van
                IQueryable<SubCommittee> lsSubCommittee = SubCommitteeBusiness.All;

                ViewData[ReportFirstHighSchoolConstants.COUNT_SUB] = 0;
                ViewData[ReportFirstHighSchoolConstants.LIST_SUB] = new List<SubCommittee>();
                List<SubCommittee> lstSubCommittee = new List<SubCommittee>();

                if (lsSubCommittee.Count() > 0)
                {
                    lstSubCommittee = lsSubCommittee.ToList();
                    SubCommittee SubCom = new SubCommittee();
                    SubCom.SubCommitteeID = -1;
                    SubCom.Resolution = Res.Get("FirstHighSchool_Label_HigherEducation");
                    lstSubCommittee.Add(SubCom);
                    SubCommittee SubCom1 = new SubCommittee();
                    SubCom1.SubCommitteeID = 0;
                    SubCom1.Resolution = Res.Get("FirstHighSchool_Label_NotHigherEducation");
                    lstSubCommittee.Add(SubCom1);
                    ViewData[ReportFirstHighSchoolConstants.COUNT_SUB] = lstSubCommittee.Count;
                    ViewData[ReportFirstHighSchoolConstants.LIST_SUB] = lstSubCommittee;
                }

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = Global.AcademicYearID;
                IQueryable<SubjectCat> lsSubjectCat = SchoolSubjectBusiness.SearchBySchool(Global.SchoolID.Value, dic).Select(o => o.SubjectCat).Distinct();
                SubjectCat SubjectCat = new SubjectCat();
                SubjectCat.SubjectCatID = 0;
                SubjectCat.SubjectName = "Môn khác dạy GDCD";
                List<SubjectCat> lstsc = new List<SubjectCat>();
                if (lsSubjectCat.Count() > 0)
                {

                    lstsc = lsSubjectCat.Where(o => o.IsActive == true).ToList();

                }
                lstsc.Add(SubjectCat);
                ViewData[ReportFirstHighSchoolConstants.LIST_SUBJECT_TITLE] = lstsc;

                //-------------------------------------------------------------

                if (error.Trim().Length == 0)
                {
                    return Json(new JsonMessage(RenderPartialViewToString("_Total", null)));
                }
                else
                {
                    ViewData[ReportFirstHighSchoolConstants.ERROR_MSG] = error;
                    return Json(new JsonMessage(RenderPartialViewToString("_ErrorMessage", null), "errortemplate"));
                }
            }
            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion




    }

}
