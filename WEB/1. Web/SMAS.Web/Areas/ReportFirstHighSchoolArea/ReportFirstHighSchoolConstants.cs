﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportFirstHighSchoolArea
{
    public class ReportFirstHighSchoolConstants
    {
        public const string LIST_PUPIL = "ListPupil";
        public const string COUNT_SUB = "CountSubmittee";
        public const string LIST_SUB = "ListSubmittee";
        public const string LIST_SUBJECT_TITLE = "LIST_SUBJECT_TITLE";
        public const string LIST_TEACHER = "LIST_TEACHER";

        public const string ERROR_MSG = "ERROR_MSG";
        
    }
}