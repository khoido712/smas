﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.ReportFirstHighSchoolArea.Models
{
    public class HighSchoolTeacherViewModel
    {
        [ResourceDisplayName( "StatisticOfTertiary_Label_NumOfStaff")]
        public Nullable<int> NumOfStaff { get; set; }

        [ResourceDisplayName( "StatisticOfTertiary_Label_NumOfStandard")]
        public Nullable<int> NumOfStandard { get; set; }

        [ResourceDisplayName( "StatisticOfTertiary_Label_NumOfOverStandard")]
        public Nullable<int> NumOfOverStandard { get; set; }

        [ResourceDisplayName( "StatisticOfTertiary_Label_NumOfUnderStandard")]
        public Nullable<int> NumOfUnderStandard { get; set; }

        [ResourceDisplayName("StatisticOfTertiary_Label_TotalNumOfTeacherPerSubject")]
        public List<ComboObject> TotalNumOfTeacherPerSubject { get; set; }

        [ResourceDisplayName("StatisticSubjectOfTertiary_Label_TotalTeacher")]
        public Nullable<int> TotalTeacher { get; set; }	
    }
}