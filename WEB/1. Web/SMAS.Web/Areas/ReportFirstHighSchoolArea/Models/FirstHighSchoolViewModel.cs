﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.ReportFirstHighSchoolArea.Models
{
    public class FirstHighSchoolViewModel
    {
        #region Enrollment
        public int? PupilFromSecondary { get; set; }
        public int? PupilOfOther { get; set; }
        public int? TotalPupil { get; set; }
        #endregion

        #region MateriaBase
        public int? ComputerRoom { get; set; }
        public int? NumberOfComputer { get; set; }
        public int? NumberOfComputerInternet { get; set; }
        public bool HasWebsite { get; set; }
        public bool HasDatabase { get; set; }
        public int? LearningRoom { get; set; }
        #endregion

        #region Pupil
        public int EducationLevelID { get; set; }
        [ResourceDisplayName("FirstHighSchool_Label_EducationLevels")]
        public string EducationLevel { get; set; }
        [ResourceDisplayName("FirstHighSchool_Label_SumClass")]
        public int SumClass { get; set; }
        [ResourceDisplayName("FirstHighSchool_Label_SumPupil")]
        public int SumPupil { get; set; }
        public Dictionary<string, object> NumberPupil { get; set; }
        public Dictionary<string, object> NumberClass { get; set; }
        public string SpecialType { get; set; }

        #endregion

        [ResourceDisplayName("StatisticOfTertiary_Label_NumOfStaff")]
        public Nullable<int> NumOfStaff { get; set; }

        [ResourceDisplayName("StatisticOfTertiary_Label_NumOfStandard")]
        public Nullable<int> NumOfStandard { get; set; }

        [ResourceDisplayName("StatisticOfTertiary_Label_NumOfOverStandard")]
        public Nullable<int> NumOfOverStandard { get; set; }

        [ResourceDisplayName("StatisticOfTertiary_Label_NumOfUnderStandard")]
        public Nullable<int> NumOfUnderStandard { get; set; }

        [ResourceDisplayName("StatisticOfTertiary_Label_TotalNumOfTeacherPerSubject")]
        public Dictionary<string, object> TotalNumOfTeacherPerSubject { get; set; }

        [ResourceDisplayName("StatisticSubjectOfTertiary_Label_TotalTeacher")]
        public int TotalTeacher { get; set; }	
    }

}