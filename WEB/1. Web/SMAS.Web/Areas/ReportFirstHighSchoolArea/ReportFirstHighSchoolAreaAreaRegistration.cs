﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportFirstHighSchoolArea
{
    public class ReportFirstHighSchoolAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportFirstHighSchoolArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportFirstHighSchoolArea_default",
                "ReportFirstHighSchoolArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
