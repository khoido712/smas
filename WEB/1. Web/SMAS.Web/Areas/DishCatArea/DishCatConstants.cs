/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.DishCatArea
{
    public class DishCatConstants
    {
        public const string LIST_DISHCAT = "listDishCat";
        public const string LIST_TYPEOFDISH = "listTypeOfDish";
        public const string DISHCATID = "DishCatID";
        public const string DISHCATNAME = "DishCatName";
        public const string NUMBEROFSERVING = "NumberOfServing";
        public const string LIST_TYPEOFFOOD = "ListTypeOfFood";
        public const string LIST_FOODCAT = "LIST_FOODCAT";
        public const string LIST_FOODMODEL = "LIST_FOODMODEL";
        public const string LIST_MINERALCAT = "LIST_MINERALCAT";
        public const string DISHCATOBJECT = "DISHCATOBJECT";
        public const string NAMEDISH = "NAMEDISH";
    }
}