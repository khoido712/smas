﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DishCatArea.Models
{
    public class FoodModel
    {
        [ScaffoldColumn(false)]
        public int FoodID {get;set;}

        [ResourceDisplayName("DishCat_Label_TypeOfFood")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        //[AdditionalMetadata("PlaceHolder", "")]
        [AdditionalMetadata("ViewDataKey", DishCatConstants.LIST_TYPEOFFOOD)]
        public int? TypeOfFoodID { get; set; }

        [ResourceDisplayName("DishCat_Label_Food")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        //[AdditionalMetadata("PlaceHolder", "")]
        [AdditionalMetadata("ViewDataKey", DishCatConstants.LIST_FOODCAT)]
        public int FoodCatID { get; set; }

        public string FoodName { get; set; }
        public string SFood { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string WeightAll { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1,2})?$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public decimal WeightOnly { get; set; }

    }
}