﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.DishCatArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("DishCat_Label_DishName")]
        public string DishName { get; set; }

        [ResourceDisplayName("DishCat_Label_TypeOfDish")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", DishCatConstants.LIST_TYPEOFDISH)]
        public int? TypeOfDish { get; set; }


    }
}