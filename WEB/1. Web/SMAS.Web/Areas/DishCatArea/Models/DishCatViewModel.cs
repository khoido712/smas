/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.DishCatArea.Models
{
    public class DishCatViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 DishCatID { get; set; }
        [ResourceDisplayName("DishCat_Label_DishName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
		public System.String DishName { get; set; }

        [ResourceDisplayName("DishCat_Label_TypeOfDish")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        //[AdditionalMetadata("PlaceHolder", "")]
        [AdditionalMetadata("ViewDataKey", DishCatConstants.LIST_TYPEOFDISH)]
        public System.Int32 TypeOfDishID { get; set; }

        [ResourceDisplayName("DishCat_Label_Number")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [Range(1, 9999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public System.Int32 NumberOfServing { get; set; }

        [ResourceDisplayName("DishCat_Label_Prepare")]
        [StringLength(5000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
		public System.String Prepare { get; set; }

        [ResourceDisplayName("DishCat_Label_Receipt")]
        [StringLength(5000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
		public System.String Receipt { get; set; }

        [ResourceDisplayName("DishCat_Label_Note")]
        [DataType(DataType.MultilineText)]		
		public System.String Note { get; set; }

        [ScaffoldColumn(false)]						
		public System.Nullable<System.Int32> SchoolID { get; set; }

        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }				
	       
    }
}


