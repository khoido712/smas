﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DishCatArea
{
    public class DishCatAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DishCatArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DishCatArea_default",
                "DishCatArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
