﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DishCatArea.Models;

using SMAS.Models.Models;
using Telerik.Web.Mvc;

using System;
using Telerik.Web.Mvc.UI;
using System.Threading;
using System.Globalization;

namespace SMAS.Web.Areas.DishCatArea.Controllers
{
    public class DishCatController : BaseController
    {        
        private readonly IDishCatBusiness DishCatBusiness;
        private readonly ITypeOfDishBusiness TypeOfDishBusiness;
        private readonly ITypeOfFoodBusiness TypeOfFoodBusiness;
        private readonly IFoodCatBusiness FoodCatBusiness;
        private readonly IMinenalCatBusiness MineralCatBusiness;
        private readonly IFoodMineralBusiness FoodMineralBusiness;
        private readonly IDishDetailBusiness DishDetailBusiness;
        private readonly IDailyMenuDetailBusiness DailyMenuDetailBusiness;

        public DishCatController(IDishCatBusiness dishcatBusiness, ITypeOfDishBusiness TypeOfDishBusiness, ITypeOfFoodBusiness TypeOfFoodBusiness, IFoodCatBusiness FoodCatBusiness, IMinenalCatBusiness MineralCatBusiness,IFoodMineralBusiness FoodMineralBusiness,IDishDetailBusiness DishDetailBusiness,
            IDailyMenuDetailBusiness DailyMenuDetailBusiness)
		{
			this.DishCatBusiness = dishcatBusiness;
            this.TypeOfDishBusiness = TypeOfDishBusiness;
            this.TypeOfFoodBusiness = TypeOfFoodBusiness;
            this.FoodCatBusiness = FoodCatBusiness;
            this.MineralCatBusiness = MineralCatBusiness;
            this.FoodMineralBusiness = FoodMineralBusiness;
            this.DishDetailBusiness = DishDetailBusiness;
            this.DailyMenuDetailBusiness = DailyMenuDetailBusiness;
		}
		
		//
        // GET: /DishCat/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here

           

            SearchInfo = new Dictionary<string, object>();
            IEnumerable<TypeOfDish> ListTypeOfDish = TypeOfDishBusiness.Search(SearchInfo);
            ListTypeOfDish = ListTypeOfDish.OrderBy(o=>o.TypeOfDishName);
            ViewData[DishCatConstants.LIST_TYPEOFDISH] = new SelectList(ListTypeOfDish, "TypeOfDishID", "TypeOfDishName");


            SearchInfo = new Dictionary<string, object>();

            IEnumerable<DishCatViewModel> lst = this._Search(SearchInfo);
            lst = lst.OrderBy(o=>o.DishName);
            ViewData[DishCatConstants.LIST_DISHCAT] = lst;

            return View();
        }

		//
        // GET: /DishCat/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
            SearchInfo["DishName"] = frm.DishName;
            SearchInfo["TypeOfDishID"] = frm.TypeOfDish;

            IEnumerable<DishCatViewModel> lst = this._Search(SearchInfo);
            ViewData[DishCatConstants.LIST_DISHCAT] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        
        public ActionResult Create()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            SearchInfo = new Dictionary<string, object>();
            IEnumerable<TypeOfDish> ListTypeOfDish= TypeOfDishBusiness.Search(SearchInfo);
            ListTypeOfDish = ListTypeOfDish.OrderBy(o=>o.TypeOfDishName);
            ViewData[DishCatConstants.LIST_TYPEOFDISH] = new SelectList(ListTypeOfDish, "TypeOfDishID", "TypeOfDishName");
            ViewData[DishCatConstants.DISHCATID] = 0;


            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        
        public ActionResult Edit(int idDishCat)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            DishCat DishCat = DishCatBusiness.Find(idDishCat);
            if (DishCat == null)
                return View();
            DishCatViewModel DishCatViewModel = new DishCatViewModel();
            DishCatViewModel.DishCatID = idDishCat;
            DishCatViewModel.DishName = DishCat.DishName;
            DishCatViewModel.Note = DishCat.Note;
            DishCatViewModel.NumberOfServing = DishCat.NumberOfServing;
            DishCatViewModel.Prepare = DishCat.Prepare;
            DishCatViewModel.Receipt = DishCat.Receipt;
            DishCatViewModel.TypeOfDishID = DishCat.TypeOfDishID;
            DishCatViewModel.SchoolID = DishCat.SchoolID;

            //Get view data here
            SearchInfo = new Dictionary<string, object>();
            IEnumerable<TypeOfDish> ListTypeOfDish = TypeOfDishBusiness.Search(SearchInfo);
            ListTypeOfDish = ListTypeOfDish.OrderBy(o => o.TypeOfDishName);
            ViewData[DishCatConstants.LIST_TYPEOFDISH] = new SelectList(ListTypeOfDish, "TypeOfDishID", "TypeOfDishName");
            ViewData[DishCatConstants.DISHCATID] = idDishCat;


            return View(DishCatViewModel);
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult ChangeCboFoodCat(int? id)
        {
            if (id.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                FoodCat Food = FoodCatBusiness.Find(id);
                String str = "-";
                String proteindv="0";
                String proteintv = "0";
                String beodv = "0";
                String beotv = "0";
                String duong = "0";
                String calo = "0";
                IDictionary<string,object> SearchInfo = new Dictionary<string, object>();
                IEnumerable<MinenalCat> ListMinenalCat = MineralCatBusiness.Search(SearchInfo);
                if (Food != null)
                {
                    if (Food.GroupType == GlobalConstants.FOOD_GROUP_TYPE_ANIMAL)
                    {
                        proteindv = Food.Protein.ToString();
                        beodv = Food.Fat.ToString();

                    }
                    else if (Food.GroupType == GlobalConstants.FOOD_GROUP_TYPE_PLANT)
                    {
                        beotv = Food.Fat.ToString();
                        proteintv = Food.Protein.ToString();
                    }

                    //chat khoang va yeu to vi luong
                    foreach (MinenalCat MinenalCat in ListMinenalCat)
                    {
                        SearchInfo = new Dictionary<string, object>();
                        SearchInfo["FoodID"] = id;
                        SearchInfo["MineralCatID"] = MinenalCat.MinenalCatID;
                        IEnumerable < FoodMineral > ListFoodMineral = FoodMineralBusiness.Search(SearchInfo);
                        if (ListFoodMineral != null && ListFoodMineral.Count() > 0)
                        {
                            FoodMineral FoodMineral = ListFoodMineral.FirstOrDefault();
                            str = str + "Mine"+MinenalCat.MinenalCatID.ToString() + ":" + FoodMineral.Weight.ToString() + "-";
                        }
                    }
                    duong = Food.Sugar.ToString();
                    calo = Food.Calo.ToString();
                    str = str + "ProteinDV:" + proteindv.ToString() + "-" + "ProteinTV:" + proteintv.ToString() + "-" + "BeoDV:" + beodv.ToString() + "-" + "BeoTV:" + beotv.ToString() + "-" + "Duong:" + duong.ToString() + "-" + "Calo:" + calo.ToString() + "-"; 
                }

                return Json(str.ToString());
            }
            else
            {
                return Json("");
            }
        }

        public String ChangeFoodCat(int? id)
        {
            String s = "";
            if (id.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                FoodCat Food = FoodCatBusiness.Find(id);
                String str = "-";
                String proteindv = "0";
                String proteintv = "0";
                String beodv = "0";
                String beotv = "0";
                String duong = "0";
                String calo = "0";
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                IEnumerable<MinenalCat> ListMinenalCat = MineralCatBusiness.Search(SearchInfo);
                if (Food != null)
                {
                    if (Food.GroupType == GlobalConstants.FOOD_GROUP_TYPE_ANIMAL)
                    {
                        proteindv = Food.Protein.ToString();
                        beodv = Food.Fat.ToString();

                    }
                    else if (Food.GroupType == GlobalConstants.FOOD_GROUP_TYPE_PLANT)
                    {
                        beotv = Food.Fat.ToString();
                        proteintv = Food.Protein.ToString();
                    }

                    //chat khoang va yeu to vi luong
                    foreach (MinenalCat MinenalCat in ListMinenalCat)
                    {
                        SearchInfo = new Dictionary<string, object>();
                        SearchInfo["FoodID"] = id;
                        SearchInfo["MineralCatID"] = MinenalCat.MinenalCatID;
                        IEnumerable<FoodMineral> ListFoodMineral = FoodMineralBusiness.Search(SearchInfo);
                        if (ListFoodMineral != null && ListFoodMineral.Count() > 0)
                        {
                            FoodMineral FoodMineral = ListFoodMineral.FirstOrDefault();
                            str = str + "Mine" + MinenalCat.MinenalCatID.ToString() + ":" + FoodMineral.Weight.ToString() + "-";
                        }
                    }
                    duong = Food.Sugar.ToString();
                    calo = Food.Calo.ToString();
                    str = str + "ProteinDV:" + proteindv.ToString() + "-" + "ProteinTV:" + proteintv.ToString() + "-" + "BeoDV:" + beodv.ToString() + "-" + "BeoTV:" + beotv.ToString() + "-" + "Duong:" + duong.ToString() + "-" + "Calo:" + calo.ToString() + "-";
                }

                return (str);
            }
            return s;
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public ActionResult GetCbFootCat(int? id)
        {
            if (id.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["TypeOfFoodID"] = id.Value;

                IQueryable<FoodCat> listFoodCat = FoodCatBusiness.Search(dic);
                if (listFoodCat != null)
                {
                    listFoodCat = listFoodCat.OrderBy(x => x.FoodName.ToLower());
                }
                return Json(new SelectList(listFoodCat, "FoodID", "FoodName"));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Create POST
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(DishCatViewModel model)
        {
            if (model.NumberOfServing <= 0)
            {
                return Json(new JsonMessage(Res.Get("DishCat_Label_NumberZero")));
            }

            DishCat DishCat = new DishCat();
            DishCat.DishName = model.DishName;
            DishCat.IsActive = true;
            DishCat.NumberOfServing = model.NumberOfServing;
            DishCat.Prepare = model.Prepare;
            DishCat.Receipt = model.Receipt;
            DishCat.Note = model.Note;
            DishCat.SchoolID = new GlobalInfo().SchoolID.Value;
            DishCat.TypeOfDishID = model.TypeOfDishID;

            int CheckErro = CheckValidate(model.DishName, DishCat.DishCatID);
            if (CheckErro > 0)
            {
                DishCatBusiness.Insert(DishCat);
                DishCatBusiness.Save();
                if (CheckErro == 1)
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                }
            }
            else
            {
                    return Json(new JsonMessage(Res.Get("DishCat_Label_DishName") + " " + Res.Get("ClassProfile_Control_DisplayNameDuplicated"), "error"));
            }
            //int ID = DishCat.DishCatID;
            //return Json(new JsonMessage(ID.ToString()));
        }
        /// <summary>
        /// Kiem tra ton tai ten
        /// </summary>
        /// <param name="DishCatName"></param>
        /// <param name="DishCatID"></param>
        /// <returns>bool</returns>
        public int CheckValidate(string DishCatName, int DishCatID)
        {
            IEnumerable<DishCat> listDishCat = DishCatBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), new Dictionary<string, object>() { { "IsActive", true } });
            var FirstDisCatName = listDishCat.Where(a => a.DishName == DishCatName).FirstOrDefault(); 
            if (DishCatID == 0) //them moi
            {
                if (FirstDisCatName != null)
                {
                    return 0;
                }
                else
                {
                    return 1; //Them moi thanh cong
                }
            }
            else
            {
                var FirstDisCatID = listDishCat.Where(a => a.DishCatID == DishCatID).FirstOrDefault();
                if (FirstDisCatID.DishName == DishCatName) //Khong sua ten
                {
                    return 2; //cap nhat thanh cong
                }
                else //Neu sua ten thi kiem tra xem ten da ton tai chua
                {
                    if (FirstDisCatName != null)
                    {
                        return 0; //Cap nhat that bai
                    }
                    else
                    {
                        return 2;
                    }
                }
            }
        }
        /// <summary>
        /// InsertFood
        /// </summary>
        /// <param name="form">The form.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>11/16/2012</Date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult InsertFood(FormCollection form)
        {
            int? cntRow =SMAS.Business.Common.Utils.GetInt(form["cntRow"]);
            List<DishDetail> listDishDetail = new List<DishDetail>();
            if (cntRow == null || cntRow.Value <=0)
            {
                return Json(new JsonMessage(Res.Get("DishCat_Label_NoFoodAdd"), "Error"));
            }
            else if (cntRow > 0)
            {
                
                IQueryable<DishDetail> listDishCatDetail = DishDetailBusiness.SearchBySchool(new Dictionary<string, object>() { { "IsActive", true } });
                for (int i = 1; i <= cntRow; i++)
                {
                    int? DishCatID = SMAS.Business.Common.Utils.GetInt(form["DishCatID"]);
                    int? FoodCatID = SMAS.Business.Common.Utils.GetInt(form["FoodCatID" + i.ToString()]);
                    int DishID = Convert.ToInt32(DishCatID);
                    int FoodID = Convert.ToInt32(FoodCatID);
                    var fistItem = listDishCatDetail.Where(a => a.DishID == DishID).Where(b=>b.FoodID ==FoodID).FirstOrDefault();
                    if (fistItem != null)
                    {
                        return Json(new JsonMessage("Bạn đã chọn thực phẩm nhiều lần.", "Error"));
                    }

                    if (form["FoodCatID" + i.ToString()] == null || form["FoodCatID" + i.ToString()] == "")
                        continue;
                    
                    if (FoodCatID == null)
                        continue;
                    else if (FoodCatID.Value == 0)
                        continue;
                    
                    decimal WeightPerRation = 0;
                    if (!Decimal.TryParse(form["WeightPeration" + i.ToString()], out WeightPerRation) || WeightPerRation==0)
                    {
                        return Json(new JsonMessage(Res.Get("DishCat_Invalid_Weight"), "Error"));
                    }

                   
                    if (FoodCatID == null ||  DishCatID == null)
                        continue;
                    else
                    {
                        if (FoodCatID == 0 || WeightPerRation == 0 || DishCatID == 0)
                            continue;

                        DishDetail DishDetail = new DishDetail();
                        DishDetail.DishID = DishCatID.Value;
                        DishDetail.FoodID = FoodCatID.Value;
                        DishDetail.WeightPerRation = WeightPerRation;
                        listDishDetail.Add(DishDetail);
                    }
                }
            }
            if (listDishDetail.Count > 0)
            {
                DishDetailBusiness.Insert(listDishDetail);
                DishDetailBusiness.Save();
            }

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            
        }

        /// <summary>
        /// CreateFood
        /// </summary>
        /// <returns></returns>
        

        [ValidateAntiForgeryToken]
        public ActionResult CreateFood(int id)
        {
            DishCat DishCat = DishCatBusiness.Find(id);

            ViewData[DishCatConstants.DISHCATNAME] = DishCat.DishName;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            ViewData[DishCatConstants.NUMBEROFSERVING] = DishCat.NumberOfServing;
            //Get view data here
            SearchInfo = new Dictionary<string, object>();
            IEnumerable<TypeOfFood> ListTypeOfFood = TypeOfFoodBusiness.Search(SearchInfo);
            if (ListTypeOfFood != null)
            {
                ListTypeOfFood = ListTypeOfFood.OrderBy(x => x.TypeOfFoodName.ToLower());
            }
            ViewData[DishCatConstants.LIST_TYPEOFFOOD] = new SelectList(ListTypeOfFood, "TypeOfFoodID", "TypeOfFoodName");

            ViewData[DishCatConstants.LIST_FOODCAT] = new SelectList(new SelectList(new string[] { }));
            SearchInfo = new Dictionary<string, object>();
            IEnumerable<MinenalCat> ListMinenalCat = MineralCatBusiness.Search(SearchInfo);
            ViewData[DishCatConstants.LIST_MINERALCAT] = ListMinenalCat;

            ViewData[DishCatConstants.DISHCATID] = id;

            List<FoodModel> listFoodModel = new List<FoodModel>();
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["DishID"] = id;
            IEnumerable<DishDetail> ListDishDetail = DishDetailBusiness.Search(SearchInfo);
            foreach (DishDetail DishDetail in ListDishDetail)
            {
                FoodModel FoodModel = new FoodModel();
                FoodModel.FoodCatID = DishDetail.FoodID;
                FoodModel.WeightOnly = DishDetail.WeightPerRation;
                listFoodModel.Add(FoodModel);
                FoodModel.SFood = ChangeFoodCat(DishDetail.FoodID);
            }

            ViewData[DishCatConstants.LIST_FOODMODEL] = listFoodModel;
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDishCat"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public ActionResult View(int idDishCat)
        {
            DishCat DishCat = DishCatBusiness.Find(idDishCat);
            ViewData[DishCatConstants.DISHCATNAME] = DishCat.DishName;
            ViewData[DishCatConstants.DISHCATOBJECT] = DishCat;
            ViewData[DishCatConstants.NUMBEROFSERVING] = DishCat.NumberOfServing;
            //Get view data here
            IDictionary<string, object> SearchInfo;
            SearchInfo = new Dictionary<string, object>();
            IEnumerable<FoodCat> ListFoodCat = FoodCatBusiness.Search(SearchInfo);
            ViewData[DishCatConstants.LIST_FOODCAT] = new SelectList(ListFoodCat, "FoodID", "FoodName");
            SearchInfo = new Dictionary<string, object>();
            IEnumerable<MinenalCat> ListMinenalCat = MineralCatBusiness.Search(SearchInfo);
            ViewData[DishCatConstants.LIST_MINERALCAT] = ListMinenalCat;

            ViewData[DishCatConstants.DISHCATID] = idDishCat;

            List<FoodModel> listFoodModel = new List<FoodModel>();
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["DishID"] = idDishCat;
            IEnumerable<DishDetail> ListDishDetail = DishDetailBusiness.Search(SearchInfo);
            foreach (DishDetail DishDetail in ListDishDetail)
            {
                FoodModel FoodModel = new FoodModel();
                FoodModel.FoodCatID = DishDetail.FoodID;
                FoodModel.FoodName = FoodCatBusiness.Find(DishDetail.FoodID).FoodName;
                FoodModel.WeightOnly = DishDetail.WeightPerRation;
                FoodModel.WeightAll = ((DishDetail.WeightPerRation * DishCat.NumberOfServing) / 1000).ToString("0.00#");
                listFoodModel.Add(FoodModel);
                FoodModel.SFood = ChangeFoodCat(DishDetail.FoodID);
            }
            ViewData[DishCatConstants.LIST_FOODMODEL] = listFoodModel.OrderBy(p=>p.FoodName);
            return View();
        }

		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(DishCatViewModel model)
        {
            CheckPermissionForAction(model.DishCatID, "DishCat");
            
            if (model.NumberOfServing <= 0)
            {
                return Json(new JsonMessage(Res.Get("DishCat_Label_NumberZero")));
            }

            DishCat DishCat = DishCatBusiness.Find(model.DishCatID);
            DishCat.DishName = model.DishName;
            DishCat.IsActive = true;
            DishCat.NumberOfServing = model.NumberOfServing;
            DishCat.Prepare = model.Prepare;
            DishCat.Receipt = model.Receipt;
            DishCat.Note = model.Note;
            DishCat.SchoolID = new GlobalInfo().SchoolID.Value;
            DishCat.TypeOfDishID = model.TypeOfDishID;

            DishCatBusiness.Update(DishCat);
            DishCatBusiness.Save();

            int ID = DishCat.DishCatID;
            return Json(new JsonMessage(ID.ToString()));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            CheckPermissionForAction(id, "DishCat");
            this.DishCatBusiness.Delete(id, new GlobalInfo().SchoolID.Value);
            this.DishCatBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DishCatViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<DishCat> query = this.DishCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<DishCatViewModel> lst = query.Select(o => new DishCatViewModel {               
						DishCatID = o.DishCatID,								
						DishName = o.DishName,								
						NumberOfServing = o.NumberOfServing,								
						Prepare = o.Prepare,								
						Receipt = o.Receipt,								
						Note = o.Note,								
						TypeOfDishID = o.TypeOfDishID,								
						SchoolID = o.SchoolID,								
					
				
            });
            IQueryable<DailyMenuDetail> listDailyMenuDetail = DailyMenuDetailBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), new Dictionary<string, object>() { { "IsActive", true } });
            List<DishCatViewModel> list = new List<DishCatViewModel>();
            foreach (DishCatViewModel item in lst)
            {
                var itemObjec = listDailyMenuDetail.Where(a => a.DishID == item.DishCatID).FirstOrDefault();
                if (itemObjec != null)
                {
                    item.DeleteAble = false;
                }
                else
                {
                    item.DeleteAble = true;
                }
                list.Add(item);
            }

            return list.ToList();
        }        
    }
}





