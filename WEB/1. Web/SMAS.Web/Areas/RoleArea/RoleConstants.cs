﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RoleArea
{
    public class RoleConstants
    {
        public const string LS_ROLES_PARENT_SEARCH ="list of role parents to search";

        public const string ROLE_NAME = "role name";

        public const string ROLE_PARENT_ID = "role parent id";
    }
}