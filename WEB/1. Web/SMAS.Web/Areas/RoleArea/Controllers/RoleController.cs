﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.VTUtils.Utils;
using SMAS.Web.Areas.RoleArea.Models;
using AutoMapper;
using Telerik.Web.Mvc;
using System.Transactions;
namespace SMAS.Web.Areas.RoleArea.Controllers
{   
    public class RoleController : BaseController
    {        
        private readonly IRoleBusiness RoleBusiness;
		
		public RoleController (IRoleBusiness roleBusiness)
		{
			this.RoleBusiness = roleBusiness;
            Mapper.CreateMap<RoleViewModel, Role>();
            Mapper.CreateMap<Role, RoleViewModel>();
		}

        public ActionResult Index()
        {
            SetViewDataNew();
            ViewData[RoleConstants.ROLE_NAME] = "";
            ViewData[RoleConstants.ROLE_PARENT_ID] = 0;
            return View();
        }

        //
        [GridAction]

        [ValidateAntiForgeryToken]
        public ActionResult Select(string RoleName, int RoleParentID)
        {
            SetViewDataNew();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(RoleName))
            {
                dic.Add("RoleName", RoleName);
            }
            if (RoleParentID != 0)
            {
                dic.Add("RoleParentID", RoleParentID);
            }
            List<Role> roles = RoleBusiness.Search(dic).ToList();
            List<RoleViewModel> rolesViewModel = MapToListViewModel(roles);
            return View(new GridModel(rolesViewModel));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult Insert()
        {
            //Create a new instance of the RoleViewModel class.
            RoleViewModel roleViewModel = new RoleViewModel();
            //Perform model binding (fill the group properties and validate it).
            if (TryUpdateModel(roleViewModel))
            {
                //The model is valid - insert the product.
                Role roleModel = MapBackToModel(roleViewModel);
                using (TransactionScope scope = new TransactionScope())
                {
                    RoleBusiness.Insert(roleModel);
                    RoleBusiness.Save();
                    scope.Complete();
                }
            }
            //Rebind the grid
            return View(new GridModel(MapToListViewModel(RoleBusiness.GetAll().ToList())));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult Edit(int id)
        {
            //Save Ajax Editing
            Role product = RoleBusiness.Find(id);
            TryUpdateModel(product);
            using (TransactionScope scope = new TransactionScope())
            {
                RoleBusiness.Update(product);
                RoleBusiness.Save();
                scope.Complete();
            }
            return View(new GridModel(MapToListViewModel(RoleBusiness.GetAll().ToList())));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]

        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                RoleBusiness.Delete(id);
                RoleBusiness.Save();
                scope.Complete();
            }
            //Rebind the grid
            return View(new GridModel(MapToListViewModel(RoleBusiness.GetAll().ToList())));
        }

        private void SetViewDataNew()
        {
            IQueryable<RoleViewModel> listAllRoles = RoleBusiness.GetAll().Select(r => new RoleViewModel { RoleID = r.RoleID, RoleName = r.RoleName }).OrderBy(r => r.RoleID);
            ViewData[RoleConstants.LS_ROLES_PARENT_SEARCH] = listAllRoles;
        }
        //
        // GET: /Role/Details/5
        /*
        public ViewResult Details(int id)
        {            
            Role role = this.RoleBusiness.Find(id);
			return View(role);
        }

        #region Create
        //
        // GET: /Role/Create
        private void SetViewData()
        {

            List<Role> listRole = RoleBusiness.All.Where(m => m.IsActive == true).ToList();
            ViewData[GlobalConstants.ROLE_PARENTS] = new SelectList(listRole, "RoleID", "RoleName");
        }

        private void SetViewDataNew()
        {
            IQueryable<RoleViewModel> listAllRoles = RoleBusiness.GetAll().Select(r => new RoleViewModel { RoleID = r.RoleID, RoleName = r.RoleName }).OrderBy(r => r.RoleID);
            ViewData[RoleConstants.LS_ROLES_PARENT_SEARCH] = listAllRoles;                        
        }
            

        public ActionResult Create()
        {
            //chi co user co Role la Admin cap cao nhat moi co the tao Role moi
            UserAccount superAdmin = HttpContext.Session[GlobalConstants.USERACCOUNT] as UserAccount;
            Role selectedRole = HttpContext.Session[GlobalConstants.SELECTED_ROLE] as Role;
            //Kiem tra co quyen tao Role ko?
            if (superAdmin == null || selectedRole == null || selectedRole.RoleID != GlobalConstants.SUPER_ROLE_ID)
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            SetViewData();
            return View();
        } 

        //
        // POST: /Role/Create

        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Create(Role role,FormCollection formCol)
        {
            //SetViewData();
            string roleParentID = formCol[GlobalConstants.ROLE_PARENT_ID];
            //set gia tri cho ParentID va RolePath
            RoleBusiness.SetValuesToCreate(role, roleParentID);
            if (role.RolePath != null) ModelState.Remove("RolePath");
            ModelState.Remove("IsActive");
            if (ModelState.IsValid)
            {				
				this.RoleBusiness.Insert(role);
                //return RedirectToAction("Details", new { id = role.RoleID});  
                return View(role);
            }
            return View(role); 
        }       
        
        #endregion Create
        //
        // GET: /Role/Edit/5
 
        public ActionResult Edit(int id)
        {
            int i;
            SetViewData();
            Role role = this.RoleBusiness.Find(id);
            this.RoleBusiness.All.ToList();
            TempData["Role Edit"] = role;

            return View(role);
        }

        //
        // POST: /Role/Edit/5

        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Edit(Role role)
        {            
            Role originRole = TempData["Role Edit"] as Role;
            role.CreatedDate = originRole.CreatedDate;
            //ModelState.SetModelValue("RolePath", new ValueProviderResult(originRole.RolePath, originRole.RolePath,null));
            role.RolePath = originRole.RolePath; //Can xem lai cho nay neu thay doi Role Parent
            if(role.RolePath != null) ModelState.Remove("RolePath");
            //TryUpdateModel(role);
            if (ModelState.IsValid)
            {
				this.RoleBusiness.Update(role);
                //BusinessActionLog.LogAction(HttpContext.Request.UserHostAddress, HttpContext.Request.UserAgent, "EditRole", 1, role.RoleID, "sua Role", role.RoleID, role.RoleID);
                return RedirectToAction("Index");
            }
            return View(role);

        }

        //
        // GET: /Role/Delete/5
 

        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            Role role = this.RoleBusiness.Find(id);
            return View(role);
        }

        //
        // POST: /Role/Delete/5

        [HttpPost, ActionName("Delete")]		

        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this.RoleBusiness.Delete(id);
            return RedirectToAction("Index");
        }
         */

        #region Automapper
        private RoleViewModel MapToViewModel(Role role)
        {
            RoleViewModel roleViewModel = new RoleViewModel();
            Utils.Utils.BindTo(role, roleViewModel);
            return roleViewModel;
        }

        private Role MapBackToModel(RoleViewModel role)
        {
            Role Role = new Role();
            Utils.Utils.BindTo(role, Role);
            return Role;
        }

        private List<RoleViewModel> MapToListViewModel(List<Role> roles)
        {
            List<RoleViewModel> rolesViewModel = new List<RoleViewModel>();
            //Mapper.CreateMap<Role, RoleViewModel>();
            foreach (var role in roles)
            {
                RoleViewModel roleViewItem = MapToViewModel(role);
                rolesViewModel.Add(roleViewItem);
            }
            return rolesViewModel;
        }

        private List<Role> MapBackToListModel(List<RoleViewModel> roles)
        {
            List<Role> g = new List<Role>();
            //Mapper.CreateMap<RoleViewModel, Role>();
            foreach (var role in roles)
            {
                Role roleModel = MapBackToModel(role);
                g.Add(roleModel);
            }
            return g;
        }
        #endregion
    }
}
