﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.RoleArea.Models
{
    public class RoleViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("RoleID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int RoleID { get;  set; }

        [DisplayName("Role Name")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string RoleName { get; set; }

        /*
        [DisplayName("Role Path")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string RolePath { get; set; }
        */

        [UIHint("GridForeignKey")]
        [DisplayName("Parent")]
        public Nullable<int> ParentID { get; set; }

        [DisplayName("Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }	
  
    }
}