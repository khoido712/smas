﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.UpdateRewardArea
{
    public class UpdateRewardAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "UpdateRewardArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "UpdateRewardArea_default",
                "UpdateRewardArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
