﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.UpdateRewardArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Filter;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.UpdateRewardArea.Controllers
{
    [SkipCheckRole]
    public class UpdateRewardController:BaseController
    {
        #region Properties
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness;
        private readonly ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness;
        private readonly IReviewBookPupilBusiness ReviewBookPupilBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness;
        private readonly IUpdateRewardBusiness UpdateRewardBusiness;
        private readonly IRewardFinalBusiness RewardFinalBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness;
        private int partitionId;
        #endregion

        #region Contructor
        public UpdateRewardController(IClassProfileBusiness ClassProfileBusiness, IAcademicYearBusiness AcademicYearBusiness, 
            IPupilOfClassBusiness PupilOfClassBusiness, ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness, IReviewBookPupilBusiness ReviewBookPupilBusiness,
            ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IExemptedSubjectBusiness ExemptedSubjectBusiness, IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness, IUpdateRewardBusiness UpdateRewardBusiness,
            IRewardFinalBusiness RewardFinalBusiness, ISubjectCatBusiness SubjectCatBusiness, ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.TeacherNoteBookSemesterBusiness = TeacherNoteBookSemesterBusiness;
            this.TeacherNoteBookMonthBusiness = TeacherNoteBookMonthBusiness;
            this.ReviewBookPupilBusiness = ReviewBookPupilBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.RegisterSubjectSpecializeBusiness = RegisterSubjectSpecializeBusiness;
            this.UpdateRewardBusiness = UpdateRewardBusiness;
            this.RewardFinalBusiness = RewardFinalBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.LockRatedCommentPupilBusiness = LockRatedCommentPupilBusiness;
            this.partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
        }
        #endregion

        #region Action methods
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index(int? classID)
        {
            SetViewData(classID);
            ViewData[UpdateRewardConstants.LIST_REWARD_FINAL] = RewardFinalBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID).ToList();

            return View("Index");
        }

        [HttpPost]
        public PartialViewResult Search(int classId, int semester)
        {
            List<ListViewModel> lstResult = _Search(classId, semester);
            ViewData[UpdateRewardConstants.LIST_RESULT] = lstResult;
            ViewData[UpdateRewardConstants.DEFAULT_SEMESTER] = semester;
            ViewData[UpdateRewardConstants.CLASS_ID] = classId;
            ViewData[UpdateRewardConstants.PERMISSION_BUTTON] = CheckButtonPermission(classId, semester);
            ViewData[UpdateRewardConstants.SHOW_CHECK_BOX] = CheckShowCheckboxList(classId);
            ViewData[UpdateRewardConstants.LIST_REWARD_FINAL] = RewardFinalBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID).ToList();
            string LockTitle = string.Empty;
            IDictionary<string, object> dicLock = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",classId},
                    {"lstEvaluationID",new List<int>(){5,6,7,8}}
                };
            LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);
            ViewData[UpdateRewardConstants.LOCK_TITLE] = LockTitle;

            return PartialView("_List");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public ActionResult Save(List<ListViewModel> lstViewModel, int classId, int semester)
        {
            if (!CheckButtonPermission(classId, semester))
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            string LockTitle = string.Empty;
            IDictionary<string, object> dicLock = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",classId},
                    {"lstEvaluationID",new List<int>(){5,6,7,8}}
                };
            LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);
            if (LockTitle.Contains("KTHK" + semester))//Neu khoa thi return
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }
            //Lay ra cac hoc sinh duoc check chon
            List<ListViewModel> lstModel = lstViewModel.Where(o => o.Status == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();

            if (lstModel.Count == 0)
            {
                throw new BusinessException("PupilEvaluationBook_Msg_NoPupil");
            }

            //Lay ra cac khen thuong cu
            List<UpdateReward> lstUpdateReward = UpdateRewardBusiness.All
                .Where(o => o.PartitionID == partitionId
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.SchoolID == _globalInfo.SchoolID
                && o.ClassID == classId
                && o.SemesterID == semester).ToList();

            for (int i = 0; i < lstModel.Count; i++)
            {
                ListViewModel model = lstModel[i];
                string rewardIds = String.Empty;
                if (model.LstRewardId == null)
                {
                    model.LstRewardId = new List<int>();
                }

                string strRewardIds = String.Empty;
                for (int j = 0; j < model.LstRewardId.Count; j++)
                {
                    strRewardIds = strRewardIds + model.LstRewardId[j].ToString();
                    if (j < model.LstRewardId.Count - 1)
                    {
                        strRewardIds = strRewardIds + ",";
                    }
                }

                UpdateReward ur = lstUpdateReward.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                //Neu chua co thi insert
                if (ur == null)
                {
                    if (String.IsNullOrEmpty(strRewardIds))
                    {
                        continue;
                    }
                    else
                    {
                        UpdateReward obj = new UpdateReward();
                        obj.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                        obj.ClassID = classId;
                        obj.CreateTime = DateTime.Now;
                        obj.PupilID = model.PupilID;
                        obj.Rewards = strRewardIds;
                        obj.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        obj.SemesterID = semester;
                        obj.UpdateRewardID = UpdateRewardBusiness.GetNextSeq<int>("UPDATE_REWARD_SEQ");
                        obj.PartitionID = partitionId;

                        UpdateRewardBusiness.Insert(obj);

                    }
                }
                //Neu co thi update
                else
                {
                    if (!String.IsNullOrEmpty(strRewardIds))
                    {
                        ur.Rewards = strRewardIds;
                        ur.UpdateTime = DateTime.Now;

                        UpdateRewardBusiness.Update(ur);
                    }
                    else
                    {
                        UpdateRewardBusiness.Delete(ur.UpdateRewardID);
                    }
                }
            }

            UpdateRewardBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        public ActionResult Delete(int classId, int semester)
        {
            if (!CheckButtonPermission(classId, semester))
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //Lay ra cac hoc sinh co nhap danh gia bo sung

            List<UpdateReward> lstUpdateReward = UpdateRewardBusiness.All.Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.PartitionID == partitionId
                && o.SchoolID == _globalInfo.SchoolID
                && o.SemesterID == semester).ToList();

            for (int i = 0; i < lstUpdateReward.Count; i++)
            {
                UpdateRewardBusiness.Delete(lstUpdateReward[i].UpdateRewardID);
            }

            UpdateRewardBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion

        #region Report
        [ValidateAntiForgeryToken]

        public JsonResult GetReport(int classId, int semester)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ClassID"] = classId;
            dic["Semester"] = semester;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_UPDATE_REWARD;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            Stream excel = UpdateRewardBusiness.CreateUpdateRewardReport(dic);
            processedReport = UpdateRewardBusiness.InsertUpdateRewardReport(dic, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, type));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion

        #region Private methods
        private void SetViewData(int? classID)
        {
            
            //Lay danh sach khoi
            ViewData[UpdateRewardConstants.LIST_EDUCATION_LEVEL] = _globalInfo.EducationLevels;
            //Lay danh sach lop
            List<ClassProfile> lstClass = GetListClas();
            ViewData[UpdateRewardConstants.LIST_CLASS] = lstClass;
            //Lay lop mac dinh
            ClassProfile selectedCp = lstClass.Where(o => o.ClassProfileID == classID).FirstOrDefault();
            if (selectedCp == null)
            {
                selectedCp = lstClass.FirstOrDefault();
            }


            ViewData[UpdateRewardConstants.SELECTED_CLASS] = selectedCp;
            
            //Lay danh sach hoc ky
            List<ViettelCheckboxList> lstSemester = new List<ViettelCheckboxList>();
            lstSemester.Add(new ViettelCheckboxList() { Label = Res.Get("Common_Label_Semester1"), Value = 1 });
            lstSemester.Add(new ViettelCheckboxList() { Label = Res.Get("Common_Label_Semester2"), Value = 2 });
            ViewData[UpdateRewardConstants.LIST_SEMESTER] = lstSemester;
            //Lay hoc ky mac dinh
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            int defaultSemester;
            //nam hoc hien tai
            if (nowDate > ay.SecondSemesterEndDate.GetValueOrDefault().Date)
            {
                defaultSemester = 2;
            }
            else if (DateTime.Compare(ay.SecondSemesterStartDate.GetValueOrDefault().Date, nowDate) <= 0 && DateTime.Compare(nowDate, ay.SecondSemesterEndDate.GetValueOrDefault().Date) <= 0)
            {
                defaultSemester = 2;
            }
            else if (DateTime.Compare(ay.FirstSemesterStartDate.GetValueOrDefault().Date, nowDate) <= 0 && DateTime.Compare(nowDate, ay.FirstSemesterEndDate.GetValueOrDefault().Date) <= 0)
            {
                defaultSemester = 1;
            }
            else
            {
                defaultSemester = 1;
            }

            ViewData[UpdateRewardConstants.DEFAULT_SEMESTER] = defaultSemester;

        }

        private List<ListViewModel> _Search(int classId, int semester)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            //Lay danh sach hoc sinh trong lop
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = ay.IsShowPupil.HasValue && ay.IsShowPupil.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = cp.EducationLevelID;
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            IQueryable<PupilOfClass> lstPoc = PupilOfClassBusiness.Search(dic).AddPupilStatus(isNotShowPupil).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);

            lstResult = lstPoc.Select(o => new ListViewModel
            {
                PupilID = o.PupilID,
                PupilName = o.PupilProfile.FullName,
                Status = o.Status,
                EndDate = o.EndDate
            }).ToList();

            //Lay thong tin khen thuong
            List<UpdateReward> lstUpdateReward = UpdateRewardBusiness.All
                .Where(o => o.PartitionID == partitionId
                    && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.SchoolID == _globalInfo.SchoolID
                && o.ClassID == classId
                && o.SemesterID == semester).ToList();

            //Lay danh muc khen thuong
            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID).ToList();

            //Lay thong tin danh gia nang luc pham chat
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.SemesterID == semester
                && o.PartitionID == partitionId).ToList();

            //Lay thong tin nhan xet cac mon 
            int monthId = semester == 1 ? 15 : 16;
            List<TeacherNoteBookMonthBO> lstTeacherNoteBookMonth = (from tnbm in TeacherNoteBookMonthBusiness.All
                                                                    join sc in SubjectCatBusiness.All on tnbm.SubjectID equals sc.SubjectCatID
                                                                    where tnbm.SchoolID == _globalInfo.SchoolID
                                                                    && tnbm.AcademicYearID == _globalInfo.AcademicYearID
                                                                    && tnbm.ClassID == classId
                                                                    && tnbm.MonthID == monthId
                                                                    select new TeacherNoteBookMonthBO
                                                                    {
                                                                        PupilID = tnbm.PupilID,
                                                                        SubjectID = tnbm.SubjectID,
                                                                        SubjectName = sc.DisplayName,
                                                                        CommentCQ = tnbm.CommentCQ,
                                                                        OrderInSubject = sc.OrderInSubject,
                                                                        CommentSubject = tnbm.CommentSubject
                                                                    }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.SubjectName).ToList();

            DateTime endsemester = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? ay.FirstSemesterEndDate.Value : ay.SecondSemesterEndDate.Value;

            for (int i = 0; i < lstResult.Count; i++)
            {
                ListViewModel model = lstResult[i];

                bool isShow = model.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || model.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                isShow = isShow || ((model.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                    || model.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || model.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    && model.EndDate.HasValue && model.EndDate.Value > endsemester);

                if (!isShow)
                {
                    model.IsShowData = false;
                    continue;
                }

                model.IsShowData = true;

                //Load nhan xet GVBM
                List<TeacherNoteBookMonthBO> lstTnbOfPupil = lstTeacherNoteBookMonth.Where(o => o.PupilID == model.PupilID).ToList();
                string comment = String.Empty;
                for (int j = 0; j < lstTnbOfPupil.Count; j++)
                {
                    TeacherNoteBookMonthBO tnbm = lstTnbOfPupil[j];
                    if (!string.IsNullOrEmpty(tnbm.CommentSubject))
                    {
                        comment += tnbm.SubjectName + ": " + tnbm.CommentSubject;
                        if (j < lstTnbOfPupil.Count - 1)
                        {
                            comment += "\n";
                        }
                    }
                }
                model.CommentGVBM = comment;

                //Load nhan xet GVCN
                ReviewBookPupil review = lstReviewBook.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                if (review != null)
                {
                    model.CommentGVCN = review.CQComment;
                }

                //load tu so tay giao vien
                UpdateReward ur= lstUpdateReward.Where(o=>o.PupilID==model.PupilID).FirstOrDefault();
                model.LstRewardId = new List<int>();
                if (ur != null)
                {
                    model.RewardIds = ur.Rewards;
                    if (model.RewardIds != null)
                    {
                        model.LstRewardId = model.RewardIds.Split(',').Select(tag => tag.Trim()).
                          Where(tag => !string.IsNullOrEmpty(tag)).Select(o => Int32.Parse(o)).ToList();
                        List<RewardFinal> lstRf = lstRewardFinal.Where(o => model.LstRewardId.Contains(o.RewardFinalID)).ToList();

                        string strReward = String.Empty;
                        for (int j = 0; j < lstRf.Count; j++)
                        {
                            RewardFinal rf = lstRf[j];
                            strReward = strReward + rf.RewardMode;

                            if (j < lstRf.Count - 1)
                            {
                                strReward = strReward + "; ";
                            }
                            

                        }

                        model.StrRewards = strReward;
                    }

                }
                if (String.IsNullOrEmpty(model.StrRewards) && model.Status==GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    model.StrRewards = "[Lựa chọn]";
                }
                
            }

            return lstResult;
        }

        private List<ClassProfile> GetListClas()
        {
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.getClassByRoleAppliedLevel(_globalInfo.AcademicYearID.GetValueOrDefault()
                , _globalInfo.SchoolID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID).Where(o=>o.IsVnenClass == true).ToList();

            return lstClassProfile;
        }

        private bool CheckButtonPermission(int classId, int semester)
        {
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classId);
            DateTime semesterStartDate;
            DateTime semesterEndDate;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                semesterStartDate = aca.FirstSemesterStartDate.GetValueOrDefault().Date;
                semesterEndDate = aca.FirstSemesterEndDate.GetValueOrDefault().Date;
            }
            else
            {
                semesterStartDate = aca.SecondSemesterStartDate.GetValueOrDefault().Date;
                semesterEndDate = aca.SecondSemesterEndDate.GetValueOrDefault().Date;
            }
            DateTime dateNow = DateTime.Now.Date;
            bool isInSemester = DateTime.Compare(dateNow, semesterStartDate) >= 0 && DateTime.Compare(dateNow, semesterEndDate) <= 0;

            return ((isGVCN && isInSemester) || _globalInfo.IsAdmin) && _globalInfo.IsCurrentYear;
        }

        private bool CheckShowCheckboxList(int classId)
        {
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classId);
            
            return isGVCN || _globalInfo.IsAdmin;
        }
        #endregion

    }
}