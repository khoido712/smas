﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.UpdateRewardArea
{
    public class UpdateRewardConstants
    {
        public const string LIST_CLASS = "list_class";
        public const string LIST_EDUCATION_LEVEL = "list_education_level";
        public const string LIST_SEMESTER = "list_semester";
        public const string DEFAULT_SEMESTER = "default_semester";
        public const string CLASS_ID = "class_id";
        public const string SELECTED_CLASS = "selected_class";
        public const string LIST_RESULT = "LIST_RESULT";
        public const string LIST_REWARD_FINAL = "LIST_REWARD_FINAL";
        public const string PERMISSION_BUTTON = "PERMISSION_BUTTON";
        public const string SHOW_CHECK_BOX = "SHOW_CHECK_BOX";
        public const string LOCK_TITLE = "LockTitle";
    }
}