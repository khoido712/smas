﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.UpdateRewardArea.Models
{
    public class ListViewModel
    {
        public int PupilID { get; set; }
        [ResourceDisplayName("PupilEvaluationBook_Pupil_Name")]
        public string PupilName { get; set; }
        public int Status { get; set; }
        public DateTime? EndDate { get; set; }
        public string CommentGVCN { get; set; }
        public string CommentGVBM { get; set; }
        public string RewardIds { get; set; }
        public List<int> LstRewardId { get; set; }
        public string StrRewards { get; set; }
        public bool IsShowData { get; set; }
    }
}