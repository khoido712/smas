﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ConcurrentWorkReplacementArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.ConcurrentWorkReplacementArea.Controllers
{
    public class ConcurrentWorkReplacementController : BaseController
    {        
        private readonly IConcurrentWorkReplacementBusiness ConcurrentWorkReplacementBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IConcurrentWorkTypeBusiness ConcurrentWorkTypeBusiness;
        private readonly IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness;
		
		public ConcurrentWorkReplacementController (IConcurrentWorkReplacementBusiness concurrentworkreplacementBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness,
            IConcurrentWorkTypeBusiness ConcurrentWorkTypeBusiness,
            IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness,
            IEmployeeBusiness EmployeeBusiness)
		{
			this.ConcurrentWorkReplacementBusiness = concurrentworkreplacementBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ConcurrentWorkTypeBusiness = ConcurrentWorkTypeBusiness;
            this.ConcurrentWorkAssignmentBusiness = ConcurrentWorkAssignmentBusiness;
		}


        private void SetViewData()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            // Danh sách Loại công việc kiêm nhiệm
            List<ConcurrentWorkType> ListWorkType = new List<ConcurrentWorkType>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = GlobalInfo.SchoolID;
           // dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            ListWorkType = ConcurrentWorkTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            ViewData[ConcurrentWorkReplacementConstants.LIST_WORKTYPE] = new SelectList(ListWorkType, "ConcurrentWorkTypeID", "Resolution");

            // Danh sách tổ bộ môn
            List<SchoolFaculty> ListFaculty = new List<SchoolFaculty>();
            if (GlobalInfo.SchoolID != null)
            {
                ListFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, new Dictionary<string, object>()).OrderBy(o => o.FacultyName).ToList();
            }
            ViewData[ConcurrentWorkReplacementConstants.LIST_FACULTY] = new SelectList(ListFaculty, "SchoolFacultyID", "FacultyName");

            // Danh sách giáo viên kiêm nhiệm
            List<Employee> ListTeacher = new List<Employee>();
            ViewData[ConcurrentWorkReplacementConstants.LIST_TEACHERASSIGNED] = new SelectList(ListTeacher, "EmployeeID", "FullName");

            // Danh sách giáo viên thay thế
            List<Employee> ListReplacedTeacher = new List<Employee>();
            ViewData[ConcurrentWorkReplacementConstants.LIST_REPLACEDTEACHER] = new SelectList(ListReplacedTeacher, "EmployeeID", "FullName");
        }

		//
        // GET: /ConcurrentWorkReplacement/

        public ActionResult Index()
        {
            SetViewData();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;

            IEnumerable<ConcurrentWorkReplacementViewModel> lst = this._Search(SearchInfo);
            ViewData[ConcurrentWorkReplacementConstants.LIST_CONCURRENTWORKREPLACEMENT] = lst;
            return View();
        }

		//
        // GET: /ConcurrentWorkReplacement/Search

        
        public PartialViewResult Search()
        {
            SetViewData();

            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
			//

            IEnumerable<ConcurrentWorkReplacementViewModel> lst = this._Search(SearchInfo);
            ViewData[ConcurrentWorkReplacementConstants.LIST_CONCURRENTWORKREPLACEMENT] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(ConcurrentWorkReplacementViewModel frm)
        {
            ConcurrentWorkReplacement concurrentworkreplacement = new ConcurrentWorkReplacement();
            TryUpdateModel(concurrentworkreplacement); 
            Utils.Utils.TrimObject(concurrentworkreplacement);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ConcurrentWorkTypeID"] = frm.ConcurrentWorkTypeID;
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            dic["TeacherID"] = frm.TeacherID;
            ConcurrentWorkAssignment concurrentWorkAssignment = this.ConcurrentWorkAssignmentBusiness.Search(dic).FirstOrDefault();
            concurrentworkreplacement.ConcurrentWorkAssignmentID = concurrentWorkAssignment.ConcurrentWorkAssignmentID;
            concurrentworkreplacement.IsActive = true;
            concurrentworkreplacement.AcademicYearID = new GlobalInfo().AcademicYearID;
            concurrentworkreplacement.SchoolID = new GlobalInfo().SchoolID;

            this.ConcurrentWorkReplacementBusiness.Insert(concurrentworkreplacement);
            this.ConcurrentWorkReplacementBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(ConcurrentWorkReplacementViewModel frm)
        {
            ConcurrentWorkReplacement concurrentworkreplacement = this.ConcurrentWorkReplacementBusiness.Find(frm.ConcurrentWorkReplacementID);
            TryUpdateModel(concurrentworkreplacement);
            Utils.Utils.TrimObject(concurrentworkreplacement);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ConcurrentWorkTypeID"] = frm.ConcurrentWorkTypeID;
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            dic["TeacherID"] = frm.TeacherID;
            ConcurrentWorkAssignment concurrentWorkAssignment = this.ConcurrentWorkAssignmentBusiness.Search(dic).FirstOrDefault();
            concurrentworkreplacement.ConcurrentWorkAssignmentID = concurrentWorkAssignment.ConcurrentWorkAssignmentID;

            this.ConcurrentWorkReplacementBusiness.Update(concurrentworkreplacement);
            this.ConcurrentWorkReplacementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.ConcurrentWorkReplacementBusiness.Delete(new GlobalInfo().SchoolID.Value, id);
            this.ConcurrentWorkReplacementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ConcurrentWorkReplacementViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ConcurrentWorkReplacement> query = this.ConcurrentWorkReplacementBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<ConcurrentWorkReplacementViewModel> lst = query.Select(o => new ConcurrentWorkReplacementViewModel {               
						ConcurrentWorkReplacementID = o.ConcurrentWorkReplacementID,								
						TeacherID = o.TeacherID,								
                        TeacherName = o.Employee.FullName,
                        TeacherShortName = o.Employee.FullName,
						SchoolFacultyID = o.SchoolFacultyID,								
						ConcurrentWorkTypeID = o.ConcurrentWorkAssignment.ConcurrentWorkTypeID,
                        ConcurrentWorkTypeName = o.ConcurrentWorkAssignment.ConcurrentWorkType.Resolution,
						ReplacedTeacherID = o.ReplacedTeacherID,								
                        ReplacedTeacherName = o.Employee1.FullName,
						FromDate = o.FromDate,								
						ToDate = o.ToDate,	
            });

            return lst.OrderBy(o=>o.TeacherShortName).ThenBy(o=>o.TeacherName).ToList();
        }


        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadReplacedTeacher(int? SchoolFacultyID)
        {
            IEnumerable<Employee> lst = new List<Employee>();
            GlobalInfo global = new GlobalInfo();
            if (SchoolFacultyID != null)
            {
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, SchoolFacultyID.Value).OrderBy(o => o.Name).ThenBy(o=>o.FullName).ToList();
            }
            if (lst == null)
                lst = new List<Employee>();

            //lst = lst.Where(o => o.AppliedLevel == global.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadAsignedTeacher(int? ConcurrentWorkTypeID)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<Employee> lst = new List<Employee>();
            if (ConcurrentWorkTypeID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ConcurrentWorkTypeID"] = ConcurrentWorkTypeID;
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;
                lst = this.ConcurrentWorkAssignmentBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).Select(o => o.Employee).OrderBy(o => o.FullName);
            }
            if (lst == null)
                lst = new List<Employee>();
            //lst = lst.Where(o => o.AppliedLevel == global.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }
    }
}





