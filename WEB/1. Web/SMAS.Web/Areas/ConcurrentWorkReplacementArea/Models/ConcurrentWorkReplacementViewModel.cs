/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Models.CustomAttribute;
namespace SMAS.Web.Areas.ConcurrentWorkReplacementArea.Models
{
    public class ConcurrentWorkReplacementViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 ConcurrentWorkReplacementID { get; set; }

        [ResourceDisplayName("ConcurrentWorkReplacement_Label_ConcurrentWorkAssignment")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConcurrentWorkReplacementConstants.LIST_WORKTYPE)]
        [AdditionalMetadata("OnChange", "AjaxLoadAsignedTeacher(this)")]
        public System.Int32 ConcurrentWorkTypeID { get; set; }


        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkReplacement_Label_ConcurrentWorkAssignment")]
        public string ConcurrentWorkTypeName { get; set; }


        [ResourceDisplayName("ConcurrentWorkReplacement_Label_TeacherAssigned")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConcurrentWorkReplacementConstants.LIST_TEACHERASSIGNED)]
        [AdditionalMetadata("OnChange", "AjaxLoadReplaTeacher(this)")]
		public System.Int32 TeacherID { get; set; }


        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkReplacement_Label_TeacherAssigned")]
        public string TeacherName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkReplacement_Label_TeacherAssigned")]
        public string TeacherShortName { get; set; }


        [ResourceDisplayName("ConcurrentWorkReplacement_Label_Faculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConcurrentWorkReplacementConstants.LIST_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadReplacedTeacher(this)")]
        public System.Nullable<System.Int32> SchoolFacultyID { get; set; }




        [ResourceDisplayName("ConcurrentWorkReplacement_Label_ReplacedTeacher")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConcurrentWorkReplacementConstants.LIST_REPLACEDTEACHER)]
		public System.Int32 ReplacedTeacherID { get; set; }


        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkReplacement_Label_ReplacedTeacher")]
        public string ReplacedTeacherName { get; set; }

        


        [ResourceDisplayName("ConcurrentWorkReplacement_Label_FromDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "ToDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
		public System.Nullable<System.DateTime> FromDate { get; set; }



        [ResourceDisplayName("ConcurrentWorkReplacement_Label_ToDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
		public System.Nullable<System.DateTime> ToDate { get; set; }

        [ResourceDisplayName("ConcurrentWorkReplacement_Label_Description")]
        [UIHint("MultilineText")]
        public string Description { get; set; }
    }
}


