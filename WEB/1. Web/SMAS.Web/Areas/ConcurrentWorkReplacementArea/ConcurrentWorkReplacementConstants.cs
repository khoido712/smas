/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ConcurrentWorkReplacementArea
{
    public class ConcurrentWorkReplacementConstants
    {
        public const string LIST_CONCURRENTWORKREPLACEMENT = "listConcurrentWorkReplacement";
        public const string LIST_WORKTYPE = "LIST_WORKTYPE";
        public const string LIST_TEACHERASSIGNED = "LIST_TEACHERASSIGNED";
        public const string LIST_FACULTY = "LIST_FACULTY";
        public const string LIST_REPLACEDTEACHER = "LIST_REPLACEDTEACHER";
    }
}