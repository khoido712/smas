﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ConcurrentWorkReplacementArea
{
    public class ConcurrentWorkReplacementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ConcurrentWorkReplacementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ConcurrentWorkReplacementArea_default",
                "ConcurrentWorkReplacementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
