﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Controllers;
using SMAS.DAL.Repository;
using SMAS.Web.Areas.PupilAwayArea.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilAwayArea;

namespace SMAS.Web.Areas.ReportMenuChildrenArea.Controllers
{
    public class ChildrenAbsentController : BaseController
    {
        //
        // GET: /ReportMenuChildrenArea/ChildrenAbsent/
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        public ChildrenAbsentController(IAcademicYearBusiness academicYearID,
                      IProcessedReportBusiness processedReportBusiness, IClassProfileBusiness classProfileBusiness,
                      ISchoolProfileBusiness schoolProfileBusiness, IEducationLevelBusiness educationLevelBusiness,
                      IPupilAbsenceBusiness pupilAbsenceBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
                      IPupilOfClassBusiness pupilOfClassBusiness)
        {
            this.AcademicYearBusiness = academicYearID;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();
            var lstEducation = Global.EducationLevels;
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            ViewData[ChidrenAbsentContans.ACADEMIC_YEAR] = objAcademicYear;
            ViewData["listEducation"] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());

            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", glo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            var lstClass = this.ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();
            return Json(lstClass);
        }

        public Stream ExportExcel(int? educationLevelID, int? classID, string fromDate, string toDate)
        {
            //Tên khối
            string educationLevelName = string.Empty;
            if (educationLevelID.HasValue && educationLevelID > 0)
            {
                EducationLevel objEducationLevel = EducationLevelBusiness.Find(educationLevelID);
                educationLevelName = objEducationLevel.Resolution;
            }
            //Tên lớp
            string className = string.Empty;
            if (classID.HasValue && classID > 0)
            {
                ClassProfile classProfileObj = ClassProfileBusiness.Find(classID);
                className = classProfileObj.DisplayName;
            }
            //Lấy Cấp học và Từ ngày, Đến ngày
            int? appliedLevel = _globalInfo.AppliedLevel;
            DateTime FromDate = Convert.ToDateTime(fromDate);
            DateTime ToDate = Convert.ToDateTime(toDate);

            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.Tre_BaoCaoTreNghiHoc + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            string fileName = "";
            if (educationLevelID == 0 && classID == 0)
            {
                fileName = string.Format("Tre_BaoCaoTreNghiHoc_{0}.xls", "Tatca");
            }
            else if (educationLevelID != 0 && classID == 0)
            {
                fileName = string.Format("Tre_BaoCaoTreNghiHoc_{0}.xls", "NhomLop_" + Utils.Utils.StripVNSign(educationLevelName));
            }
            else
            {
                fileName = string.Format("Tre_BaoCaoTreNghiHoc_{0}.xls", Utils.Utils.StripVNSign(className));
            }

            //Fill dữ liệu
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //Dữ liệu chung
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            Province province = school.Province;
            firstSheet.SetCellValue("A" + 2, _globalInfo.SchoolName);
            firstSheet.SetCellValue("A" + 6, "Từ ngày " + fromDate + " đến ngày " + toDate);
            if (classID.HasValue && classID > 0)
            {
                firstSheet.SetCellValue("A" + 5, "Lớp " + className);
            }
            else if ((!classID.HasValue || classID < 1) && (educationLevelID.HasValue && educationLevelID > 0))
            {
                firstSheet.SetCellValue("A" + 5, educationLevelName);
            }
            else
            {
                firstSheet.SetCellValue("A" + 5, "Tất cả");
            }

            //Fill danh sách
            int startRow = 10;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ClassID",classID},
                {"FromAbsentDate",FromDate},
                {"ToAbsentDate",ToDate},
                {"AppliedLevel",appliedLevel},
            };
            if (educationLevelID > 0)
            {
                dic.Add("EducationLevelID", educationLevelID);
            }


            //List HS nghi hoc
            IQueryable<PupilAbsence> lstPupilAbsence = PupilAbsenceBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
            List<PupilAbsenceBO> lstPupilAbsenceBO = (from p in lstPupilAbsence
                                                      join cp in ClassProfileBusiness.All on p.ClassID equals cp.ClassProfileID
                                                      join poc in PupilOfClassBusiness.All on new { p.ClassID, p.PupilID } equals new { poc.ClassID, poc.PupilID }
                                                      where ((((poc.AssignedDate <= p.AbsentDate && p.AbsentDate <= poc.EndDate)
                                                               && (poc.Status == 3 || poc.Status == 4 || poc.Status == 5))
                                                               || ((poc.AssignedDate <= p.AbsentDate) && (poc.Status == 1 || poc.Status == 2)))
                                                               && poc.AcademicYearID == _globalInfo.AcademicYearID)
                                                               && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                      select new PupilAbsenceBO
                                                      {
                                                          ClassID = p.ClassID,
                                                          Name = p.PupilProfile.Name,
                                                          ClassName = p.ClassProfile.DisplayName,
                                                          PupilID = p.PupilID,
                                                          FullName = p.PupilProfile.FullName,
                                                          AbsentDate = p.AbsentDate,
                                                          Section = p.Section,
                                                          IsAccepted = p.IsAccepted,
                                                          Status = poc.Status,
                                                          OrderNumber = cp.OrderNumber,
                                                          OrderInClass = poc.OrderInClass,
                                                          EducationLevelID = cp.EducationLevelID
                                                      }).ToList();
            //Sắp xếp
            lstPupilAbsenceBO = lstPupilAbsenceBO.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0)
                                    .ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass.HasValue ? o.OrderInClass : 0).ThenBy(o => o.Name).ThenBy(o => o.FullName)
                                    .ThenByDescending(o => o.AbsentDate).ToList();

            int count = 0;
            List<PupilAbsenceBO> listFillExcel = new List<PupilAbsenceBO>();
            List<PupilAbsenceBO> pupilAbsenceBOList = null;
            PupilAbsenceBO pupilAbsenceBOObj = null;
            PupilAbsenceBO pupilAbsenceBOObjTmp = null;
            List<PupilByDateViewModel> pupilByDateList = (from l in lstPupilAbsenceBO
                                                          group l by new { l.PupilID, l.AbsentDate, l.ClassName, l.FullName } into grp
                                                          select new PupilByDateViewModel
                                                          {
                                                              PupilID = grp.Key.PupilID,
                                                              AbsenceDate = grp.Key.AbsentDate,
                                                              ClassName = grp.Key.ClassName,
                                                              FullName = grp.Key.FullName,
                                                          }).Distinct().ToList();

            PupilByDateViewModel pupilByDateObj = null;
            for (int i = 0; i < pupilByDateList.Count; i++)
            {
                pupilByDateObj = pupilByDateList[i];
                pupilAbsenceBOObj = new PupilAbsenceBO();
                pupilAbsenceBOObj.ClassName = pupilByDateObj.ClassName;
                pupilAbsenceBOObj.FullName = pupilByDateObj.FullName;
                pupilAbsenceBOObj.AbsentDate = pupilByDateObj.AbsenceDate;

                //list so buoi cua hoc sinh trong 1 ngay nghi
                pupilAbsenceBOList = lstPupilAbsenceBO.Where(p => p.PupilID == pupilByDateObj.PupilID && p.AbsentDate == pupilByDateObj.AbsenceDate).ToList();
                if (pupilAbsenceBOList != null && pupilAbsenceBOList.Count > 0)
                {
                    for (int j = 0; j < pupilAbsenceBOList.Count; j++)
                    {
                        pupilAbsenceBOObjTmp = pupilAbsenceBOList[j];
                        if (pupilAbsenceBOList[j].Section == 1)
                        {
                            pupilAbsenceBOObj.Section_M = pupilAbsenceBOObjTmp.IsAccepted.HasValue ? (pupilAbsenceBOObjTmp.IsAccepted == false ? "K" : "P") : "";
                        }
                        if (pupilAbsenceBOList[j].Section == 2)
                        {
                            pupilAbsenceBOObj.Section_A = pupilAbsenceBOObjTmp.IsAccepted.HasValue ? (pupilAbsenceBOObjTmp.IsAccepted == false ? "K" : "P") : "";
                        }
                        if (pupilAbsenceBOList[j].Section == 3)
                        {
                            pupilAbsenceBOObj.Section_N = pupilAbsenceBOObjTmp.IsAccepted.HasValue ? (pupilAbsenceBOObjTmp.IsAccepted == false ? "K" : "P") : "";
                        }
                    }
                }

                listFillExcel.Add(pupilAbsenceBOObj);
            }

            PupilAbsenceBO pupilAbsenceBOExcel = null;
            if (listFillExcel != null && listFillExcel.Count > 0)
            {
                for (int k = 0; k < listFillExcel.Count; k++)
                {
                    pupilAbsenceBOExcel = listFillExcel[k];
                    firstSheet.SetCellValue(startRow, 1, k + 1);
                    firstSheet.SetCellValue(startRow, 2, pupilAbsenceBOExcel.ClassName);
                    firstSheet.SetCellValue(startRow, 3, pupilAbsenceBOExcel.FullName);
                    firstSheet.SetCellValue(startRow, 4, pupilAbsenceBOExcel.AbsentDate == null ? "" : pupilAbsenceBOExcel.AbsentDate.Value.ToShortDateString());
                    firstSheet.SetCellValue(startRow, 5, pupilAbsenceBOExcel.Section_M);
                    firstSheet.SetCellValue(startRow, 6, pupilAbsenceBOExcel.Section_A);
                    firstSheet.SetCellValue(startRow, 7, pupilAbsenceBOExcel.Section_N);
                    firstSheet.SetRowHeight(startRow, 20);
                    startRow++;
                }
            }

            // Vẽ khung 
            startRow = 10;
            firstSheet.GetRange(startRow, 1, startRow - 1 + listFillExcel.Count(), 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            //Fill dl Tổng cộng
            //startRow = 10;
            count = count + 1;
            firstSheet.GetRange(listFillExcel.Count() + startRow, 1, listFillExcel.Count() + 1 + startRow, 2).Merge(true);
            firstSheet.SetCellValue((listFillExcel.Count() + startRow + 1), 1, "Tổng cộng: ");
            firstSheet.GetRange(listFillExcel.Count() + startRow + 1, 3, listFillExcel.Count() + 2 + startRow, 3).SetFontStyle(false, System.Drawing.Color.Black, true, 12, false, false);
            firstSheet.SetCellValue((listFillExcel.Count() + startRow + 1), 3, "Nghỉ có phép: ");
            firstSheet.SetCellValue((listFillExcel.Count() + startRow + 2), 3, "Nghỉ không phép: ");
            firstSheet.SetCellValue((listFillExcel.Count() + 3 + startRow), 3, "Tổng: ");
            firstSheet.SetFormulaValue((listFillExcel.Count() + startRow + 1), 4, "=COUNTIF(E" + (startRow) + ":G" + ((listFillExcel.Count() + startRow - 1)) + "," + "\"P\"" + ")");
            firstSheet.SetFormulaValue((listFillExcel.Count() + 2 + startRow), 4, "=COUNTIF(E" + (startRow) + ":G" + ((listFillExcel.Count() + startRow - 1)) + "," + "\"K\"" + ")");
            firstSheet.SetFormulaValue((listFillExcel.Count() + 3 + startRow), 4, "=SUM(D" + ((listFillExcel.Count() + startRow + 1)) + ",D" + (listFillExcel.Count() + startRow + 2) + ")");
            //Xuất file
            Stream excel = oBook.ToStream();
            return excel;
        }

        #region Load file cũ đã lưu
        [ValidateAntiForgeryToken]
        public JsonResult GetReportPupilAway(FormCollection frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            string fromDate = frm["FromDate"];
            string toDate = frm["ToDate"];
            ClassProfile objClassProfile = ClassProfileBusiness.Find(classID);
            PupilAbsenceBO objPupilAbsenceBO = new PupilAbsenceBO();
            objPupilAbsenceBO.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            objPupilAbsenceBO.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            objPupilAbsenceBO.AppliedLevelID = _globalInfo.AppliedLevel.Value;
            objPupilAbsenceBO.EducationName = educationLevelID > 0 ? "Khoi" + educationLevelID : "TatCa";
            objPupilAbsenceBO.ClassName = objClassProfile != null ? objClassProfile.DisplayName : "";
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.Tre_BaoCaoTreNghiHoc);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilAbsenceBusiness.GetReportChildrenAway(objPupilAbsenceBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ExportExcel(educationLevelID, classID, fromDate, toDate);
                processedReport = PupilAbsenceBusiness.InsertChildrenAwayReport(objPupilAbsenceBO, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }
        #endregion

        #region Load file mới
        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportPupilAway(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            ClassProfile objClassProFile = ClassProfileBusiness.Find(classID);
            string fromDate = frm["FromDate"];
            string toDate = frm["ToDate"];
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            PupilAbsenceBO objPupilAbsenceBO = new PupilAbsenceBO();
            objPupilAbsenceBO.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            objPupilAbsenceBO.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            objPupilAbsenceBO.AppliedLevelID = _globalInfo.AppliedLevel.Value;
            objPupilAbsenceBO.EducationLevelID = educationLevelID;
            objPupilAbsenceBO.EducationName = educationLevelID > 0 ? "NhomLop " + educationLevelID : "TatCa";
            objPupilAbsenceBO.ClassName = objClassProFile != null ? objClassProFile.DisplayName : "";
            Stream excel = ExportExcel(educationLevelID, classID, fromDate, toDate);
            processedReport = PupilAbsenceBusiness.InsertChildrenAwayReport(objPupilAbsenceBO, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        #endregion

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.Tre_BaoCaoTreNghiHoc,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.Tre_BaoCaoTreNghiHoc,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

    }
}
