﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilProfileReportArea;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SMAS.Web.Areas.ReportMenuChildrenArea.Controllers
{
    public class ChildrenPolicyReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPolicyTargetBusiness PolicyTargetBusiness;
        private readonly IPriorityTypeBusiness PriorityTypeBusiness;
        public ChildrenPolicyReportController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IPolicyTargetBusiness policyTargetBusiness,
            IPriorityTypeBusiness priorityTypeBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.PolicyTargetBusiness = policyTargetBusiness;
            this.PriorityTypeBusiness = priorityTypeBusiness;
        }

        // GET: /ReportMenuChildrenArea/ChildrenPolicyReport/
        public ActionResult Index()
        {
            List<EducationLevel> lsEducationLevel = new List<EducationLevel>();
            lsEducationLevel = _globalInfo.EducationLevels.ToList();
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);
            if (checkCombineClass)
            {
                lsEducationLevel.Add(new EducationLevel { Resolution = SystemParamsInFile.Combine_Class_Name, EducationLevelID = SystemParamsInFile.Combine_Class_ID });
            }
            ViewData[TheStatisticsPupilCountContans.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");

            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            if (eduId != SystemParamsInFile.Combine_Class_ID)
                dicClass.Add("EducationLevelID", eduId);

            List<SelectListItem> lstClass = new List<SelectListItem>();
            if (eduId == SystemParamsInFile.Combine_Class_ID)
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.IsCombinedClass == true).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.CombinedClassCode.ToUpper(), Text = u.CombinedClassCode, Selected = false })
                                                    .Distinct()
                                                    .ToList();
                lstClass = lstClass
                            .GroupBy(p => new { p.Value })
                            .Select(g => g.First())
                            .ToList();
            }

            else
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.IsCombinedClass != true).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();
            return Json(lstClass);
        }

        #region Export Excel
        [ValidateAntiForgeryToken]
        public JsonResult GetChildrenPolicyReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string classID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : "0";
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.MN_TRECHINHSACH);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
            };
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dic.Add("ClassID", classID);

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBusiness.GetProcessReportOfChilden(dic, SystemParamsInFile.MN_TRECHINHSACH);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExcelCreatePolicyTarget(educationLevelID, classID);
                processedReport = PupilOfClassBusiness.InsertProcessReportOfChildren(dic, excel, SystemParamsInFile.MN_TRECHINHSACH);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string classID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : "0";

            ProcessedReport processedReport = null;
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                
            };
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dicInsert.Add("ClassID", classID);

            Stream excel = this.ExcelCreatePolicyTarget(educationLevelID, classID);
            processedReport = PupilOfClassBusiness.InsertProcessReportOfChildren(dicInsert, excel, SystemParamsInFile.MN_TRECHINHSACH);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.MN_TRECHINHSACH,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.MN_TRECHINHSACH,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream ExcelCreatePolicyTarget(int? educationLevelID, string classID)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.TRE_MN_TKTreChinhSach + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            //Lấy sheet hoc sinh dien chinh sach
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "F8");
            sheet.Name = "TKTreChinhSach";

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);

            sheet.SetCellValue("A3", school.SchoolName.ToUpper());
            sheet.GetRange(3, 1, 3, 1).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, true);
            sheet.SetCellValue("A2", school.SupervisingDept.SupervisingDeptName.ToUpper());

            VTVector cellClassName = new VTVector("A9");
            VTVector cellOrderNumber = new VTVector("A10");
            VTVector cellFullName = new VTVector("B10");
            VTVector cellBirthDate = new VTVector("C10");
            VTVector cellPriorityType = new VTVector("D10");
            VTVector cellPolicyTarget = new VTVector("E10");
            VTVector cellNote = new VTVector("F10");

            IVTRange topRow = firstSheet.GetRange("A9", "F9");
            IVTRange bottomRow = firstSheet.GetRange("A10", "F10");

            //lay danh sach lop hoc theo khoi
            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", academicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                   .OrderBy(p => p.EducationLevelID)
                                                   .ThenBy(u => u.OrderNumber)
                                                   .ThenBy(u => u.DisplayName).ToList();
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                //combobox không chọn tất cả
                if (educationLevelID != 0)
                {
                    int classIDMain = Int32.Parse(classID.ToString());
                    lstClassProfile = lstClassProfile
                                        .Where(x => x.IsCombinedClass != true &&
                                                    x.EducationLevelID == educationLevelID).ToList();
                    if (classIDMain != 0)
                    {
                        lstClassProfile = lstClassProfile.Where(x => x.ClassProfileID == classIDMain).ToList();
                    }
                }
            }
            else
            {
                lstClassProfile = lstClassProfile
                                    .Where(x => x.IsCombinedClass == true).ToList();

                if (classID != "0")
                {
                    lstClassProfile = lstClassProfile.Where(x => x.CombinedClassCode.ToUpper() == classID).ToList();
                }
            }

            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();

            //danh sach hoc sinh trong lop
            List<PupilOfClassBO> lstPupilOfClassBO = ListPupilOfClass(lstClassID);
            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
            ClassProfile classProfile = new ClassProfile();

            //lay lstClassProfile disticnt theo CombinedClassCode
            if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
                lstClassProfile = lstClassProfile
                .GroupBy(p => new { p.CombinedClassCode })
                .Select(g => g.First())
                .ToList();

            List<string> lstCombineClassCode = lstClassProfile.Select(p => p.CombinedClassCode).Distinct().ToList();
            List<ClassProfile> lstClassCombine = new List<ClassProfile>();
            ClassProfile objCP = null;
            int countRow = 0;

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                #region lop khong ghep
                for (int i = 0; i < lstClassProfile.Count; i++)
                {
                    objCP = lstClassProfile[i];
                    if (objCP.IsCombinedClass == true)
                    {
                        lstClassCombine.Add(objCP);
                        continue;
                    }
                    bool isPupilOfClass = false;
                    // danh sach hoc sinh trong lop
                    lstPOCtmp = lstPupilOfClassBO.Where(p => p.ClassID == objCP.ClassProfileID).ToList();
                    int myInt = objCP.ClassProfileID;
                    classProfile = ClassProfileBusiness.Find(myInt);
                    string className = "Lớp: ";
                    if (classProfile != null)
                    {
                        className += classProfile.DisplayName;
                    }

                    int currentClassRow = 9 + countRow;

                    if (lstPOCtmp.Count > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentClassRow);
                        isPupilOfClass = true;
                        sheet.SetCellValue(currentClassRow, 1, className);
                    }
                    else
                    {
                        currentClassRow = 8 + countRow;
                        sheet.GetRange(currentClassRow, 1, currentClassRow, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    }

                    if (lstPOCtmp != null && lstPOCtmp.Count > 0)
                    {
                        int rowRecordOfClass = 1;
                        for (int j = 0; j < lstPOCtmp.Count; j++)
                        {
                            int currentRow = 10 + countRow;
                            sheet.CopyPaste(bottomRow, currentRow, 1, true);

                            VTVector vtIndex = new VTVector(countRow, 0);

                            sheet.SetCellValue(cellOrderNumber + vtIndex, rowRecordOfClass);
                            sheet.SetCellValue(cellFullName + vtIndex, lstPOCtmp[j].PupilFullName);

                            sheet.SetCellValue(cellBirthDate + vtIndex, String.Format("{0:dd/MM/yyyy}", lstPOCtmp[j].Birthday));
                            sheet.SetCellValue(cellPriorityType + vtIndex, lstPOCtmp[j].PriorityTypeName);
                            sheet.SetCellValue(cellPolicyTarget + vtIndex, lstPOCtmp[j].PolicyTargetName);
                            sheet.GetRange(currentRow, 4, currentRow, 5).WrapText();
                            
                            countRow++;
                            rowRecordOfClass++;
                        }
                    }

                    if (isPupilOfClass)
                        countRow++;
                }

                if (lstClassProfile.Count == 0)
                {

                    sheet.GetRange(8, 1, 8, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
                #endregion
            }
            else
                FillExcelCombineClass(sheet, lstClassProfile, lstPupilOfClassBO, countRow, topRow, bottomRow);

            if (lstClassCombine.Count != 0)
                FillExcelCombineClass(sheet, lstClassCombine, lstPupilOfClassBO, countRow, topRow, bottomRow);
            sheet.SetFontName("Times New Roman", 0);
            sheet.FitAllColumnsOnOnePage = true;
            firstSheet.Delete();
            return oBook.ToStream();
        }

        public void FillExcelCombineClass(IVTWorksheet sheet,
                                            List<ClassProfile> lstClassCombine,
                                            List<PupilOfClassBO> lstPupilOfClassBO,
                                            int countRow,
                                            IVTRange topRow,
                                            IVTRange bottomRow)
        {
            #region lop ghep
            ClassProfile objCP = null;
            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
            VTVector cellClassName = new VTVector("A9");
            VTVector cellOrderNumber = new VTVector("A10");
            VTVector cellFullName = new VTVector("B10");
            VTVector cellBirthDate = new VTVector("C10");
            VTVector cellPriorityType = new VTVector("D10");
            VTVector cellPolicyTarget = new VTVector("E10");
            VTVector cellNote = new VTVector("F10");

            lstClassCombine = lstClassCombine
                .GroupBy(p => new { p.CombinedClassCode })
                .Select(g => g.First())
                .ToList();

            for (int i = 0; i < lstClassCombine.Count; i++)
            {
                objCP = lstClassCombine[i];
                bool isPupilOfClass = false;
                // danh sach hoc sinh trong lop
                lstPOCtmp = lstPupilOfClassBO.Where(p => p.Combine_Class_Code == objCP.CombinedClassCode).ToList();
                int myInt = objCP.ClassProfileID;
                //classProfile = ClassProfileBusiness.Find(myInt);
                string className = string.Format("Lớp: {0}", objCP.CombinedClassCode);

                int currentClassRow = 9 + countRow;

                if (lstPOCtmp.Count > 0)
                {
                    sheet.CopyPasteSameRowHeigh(topRow, currentClassRow);
                    isPupilOfClass = true;
                    sheet.SetCellValue(currentClassRow, 1, className);
                }
                else
                {
                    currentClassRow = 8 + countRow;
                    sheet.GetRange(currentClassRow, 1, currentClassRow, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }

                if (lstPOCtmp != null && lstPOCtmp.Count > 0)
                {
                    int rowRecordOfClass = 1;
                    for (int j = 0; j < lstPOCtmp.Count; j++)
                    {
                        int currentRow = 10 + countRow;
                        sheet.CopyPaste(bottomRow, currentRow, 1, true);

                        VTVector vtIndex = new VTVector(countRow, 0);

                        sheet.SetCellValue(cellOrderNumber + vtIndex, rowRecordOfClass);
                        sheet.SetCellValue(cellFullName + vtIndex, lstPOCtmp[j].PupilFullName);
                        sheet.SetCellValue(cellBirthDate + vtIndex, String.Format("{0:dd/MM/yyyy}", lstPOCtmp[j].Birthday));
                        sheet.SetCellValue(cellPriorityType + vtIndex, lstPOCtmp[j].PriorityTypeName);
                        sheet.SetCellValue(cellPolicyTarget + vtIndex, lstPOCtmp[j].PolicyTargetName);
                        sheet.GetRange(currentRow, 4, currentRow, 5).WrapText();
                        countRow++;
                        rowRecordOfClass++;
                    }
                }

                if (isPupilOfClass)
                    countRow++;
            }

            if (lstClassCombine.Count == 0)
            {
                sheet.GetRange(8, 1, 8, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            #endregion
        }

        private List<PupilOfClassBO> ListPupilOfClass(List<int> lstClassID)
        {
            List<PupilOfClassBO> lstPupilOfClassBO = new List<PupilOfClassBO>();
            lstPupilOfClassBO = (from poc in PupilOfClassBusiness.All
                                 join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                 where poc.AcademicYearID == _globalInfo.AcademicYearID.Value
                                 && poc.SchoolID == _globalInfo.SchoolID
                                 && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                 && lstClassID.Contains(poc.ClassID)
                                 && pf.IsActive == true
                                 && (pf.PolicyTargetID != 0 || pf.PriorityTypeID != 0)
                                 select new PupilOfClassBO
            {
                PupilID = poc.PupilID,
                ClassID = poc.ClassID,
                PupilFullName = poc.PupilProfile.FullName,
                PupilCode = poc.PupilProfile.PupilCode,
                Birthday = poc.PupilProfile.BirthDate,
                Genre = poc.PupilProfile.Genre,
                EthnicName = poc.PupilProfile.Ethnic.EthnicName,
                OrderInClass = poc.OrderInClass,
                Name = pf.Name,
                PolicyTargetName = pf.PolicyTarget.Resolution,
                PriorityTypeName = pf.PriorityType.Resolution,
                Combine_Class_Code = poc.ClassProfile.CombinedClassCode
            }).OrderBy(p => p.OrderInClass.HasValue ? p.OrderInClass : 0)
                                 .ThenBy(p => p.PupilFullName).ThenBy(p => p.Name).ToList();

            return lstPupilOfClassBO;
        }
        #endregion

    }
}
