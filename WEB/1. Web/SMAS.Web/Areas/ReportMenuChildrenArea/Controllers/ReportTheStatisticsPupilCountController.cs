﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilProfileReportArea;
using SMAS.Web.Areas.ReportMenuChildrenArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportMenuChildrenArea.Controllers
{
    public class ReportTheStatisticsPupilCountController : BaseController
    {

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;

        public ReportTheStatisticsPupilCountController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IEducationLevelBusiness educationLevelBusiness
            )
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
        }

        //
        // GET: /ReportMenuChildrenArea/ReportTheStatisticsPupilCount/

        public ActionResult Index()
        {
            List<EducationLevel> lsEducationLevel = new List<EducationLevel>();
            lsEducationLevel = _globalInfo.EducationLevels.ToList();
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);
            if (checkCombineClass)
            {
                lsEducationLevel.Add(new EducationLevel { Resolution = SystemParamsInFile.Combine_Class_Name, EducationLevelID = SystemParamsInFile.Combine_Class_ID });
            }
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            return View();
        }

        #region Export Excel
        [ValidateAntiForgeryToken]
        public JsonResult GetNumberOfClassReport_03(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int reportType = int.Parse(frm["reportType"]);
            if (reportType == 1)
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.MN_THONGKESISO);
                IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        {"AcademicYearID",_globalInfo.AcademicYearID},
                                                        {"SchoolID",_globalInfo.SchoolID},
                                                        {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                        {"EducationLevelID",educationLevelID}
                                                    };
                if (reportDef.IsPreprocessed == true)
                {
                    // viet them ham
                    processedReport = PupilOfClassBusiness.GetProcessReportNumberOfChildren(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.ExportExcel(educationLevelID);
                    processedReport = PupilOfClassBusiness.InsertProcessReportNumberOfChildren(dic, excel);
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, type));
            }
            else
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THONG_KE_SO_LIEU_TRE);
                IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        {"AcademicYearID",_globalInfo.AcademicYearID},
                                                        {"SchoolID",_globalInfo.SchoolID},
                                                        {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                        {"EducationLevelID",educationLevelID}
                                                    };
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = PupilOfClassBusiness.GetChildrenByAgeReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = PupilOfClassBusiness.CreateChildrenByAgeReport(dic);
                    processedReport = PupilOfClassBusiness.InsertChildrenByAgeReport(dic, excel, _globalInfo.AppliedLevel.Value);
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, type));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int reportType = int.Parse(frm["reportType"]);
            ProcessedReport processedReport = null;

            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID}
            };
            if (reportType == 1)
            {
                Stream excel = this.ExportExcel(educationLevelID);
                processedReport = PupilOfClassBusiness.InsertProcessReportNumberOfChildren(dicInsert, excel);
                excel.Close();
            }
            else
            {
                Stream excel = PupilOfClassBusiness.CreateChildrenByAgeReport(dicInsert);
                processedReport = PupilOfClassBusiness.InsertChildrenByAgeReport(dicInsert, excel, _globalInfo.AppliedLevel.Value);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.MN_THONGKESISO,
                SystemParamsInFile.THONG_KE_SO_LIEU_TRE
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.MN_THONGKESISO,
                SystemParamsInFile.THONG_KE_SO_LIEU_TRE
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }

            var a = processedReport.ReportData;

            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream ExportExcel(int? educationLevelID)
        {
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.MN_THONGKESISO + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //fill thong tin chung
            sheet.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string Title = "THỐNG KÊ SĨ SỐ TRẺ NĂM HỌC " + objAca.DisplayTitle;
            sheet.SetCellValue("A5", Title);

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dicClass.Add("EducationLevelID", educationLevelID);

            // Lấy danh sách lớp 
            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                .OrderBy(p => p.EducationLevelID)
                .ThenBy(u => u.OrderNumber)
                .ThenBy(u => u.DisplayName).ToList();

            //Lấy List chỉ chứa lớp ghép
            if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                lstClassProfile = lstClassProfile.Where(x => x.IsCombinedClass == true).ToList();
            }
            else if (educationLevelID != 0)
                lstClassProfile = lstClassProfile.Where(x => x.IsCombinedClass == false).ToList();

            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();
            List<int> lstEducationLevelID = lstClassProfile.Select(p => p.EducationLevelID).Distinct().ToList();

            // Kiểm tra trường có lớp ghép hay không
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);

            // Nếu có trường ghép thêm vào list lstEducationLevelID
            if ((educationLevelID == 0 || educationLevelID == SystemParamsInFile.Combine_Class_ID) && checkCombineClass)
                lstEducationLevelID.Add(SystemParamsInFile.Combine_Class_ID);

            // Lấy educationLevelID khi chọn education
            if (educationLevelID > 0)
            {
                lstEducationLevelID = lstEducationLevelID.Select((v, i) => new { v, i })
                        .Where(x => x.v == educationLevelID)
                        .Select(x => x.v).ToList<int>();
            }

            //Láy danh sách học sinh theo cấp
            List<Information> lstPOC = (from poc in PupilOfClassBusiness.All
                                        join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                        join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                        where poc.AcademicYearID == _globalInfo.AcademicYearID
                                        && poc.SchoolID == _globalInfo.SchoolID
                                        && lstClassID.Contains(poc.ClassID)
                                        && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                        && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                        group poc by new
                                        {
                                            poc.ClassID,
                                            pf.EthnicID,
                                            pf.Genre,
                                            pf.ClassType,
                                            cp.EducationLevelID,
                                            cp.CombinedClassCode,
                                            pf.IsResidentIn
                                        } into g
                                        select new Information
                                        {
                                            ClassID = g.Key.ClassID,
                                            EthnicID = g.Key.EthnicID,
                                            Genre = g.Key.Genre,
                                            ClassType = g.Key.ClassType,
                                            EducationLevelID = g.Key.EducationLevelID,
                                            CombinedClassCode = g.Key.CombinedClassCode,
                                            IsResidentIn = g.Key.IsResidentIn,
                                            CountPupil = g.Count()
                                        }).ToList();

            int EduID = 0;
            int firstRow = 9;
            int startRow = 9;
            List<ClassProfile> lstCPtmp = new List<ClassProfile>();

            ClassProfile objtmp = null;
            string formular = string.Empty;
            IDictionary<int, int> dicTotal = new Dictionary<int, int>();
            for (int i = 0; i < lstEducationLevelID.Count; i++)
            {
                sheet.GetRange(startRow, 1, startRow, 10).SetHAlign(VTHAlign.xlHAlignCenter);
                EduID = lstEducationLevelID[i];
                lstCPtmp = lstClassProfile.ToList();
                if (lstEducationLevelID[i] != SystemParamsInFile.Combine_Class_ID)
                    lstCPtmp = lstClassProfile.Where(x => x.EducationLevelID == lstEducationLevelID[i] && x.IsCombinedClass == false).ToList();

                if (lstEducationLevelID[i] == SystemParamsInFile.Combine_Class_ID)
                    lstCPtmp = lstClassProfile.Where(x => x.IsCombinedClass == true).ToList();

                EducationLevel Resolution = EducationLevelBusiness.Find(EduID);
                sheet.SetCellValue(startRow, 1, lstEducationLevelID[i] == SystemParamsInFile.Combine_Class_ID ? SystemParamsInFile.Combine_Class_Name : Resolution == null ? "" : Resolution.Resolution );
                
                //Tong so
                formular = "=SUM(B" + (startRow + 1) + ":B" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 2, formular);
                //Nu
                formular = "=SUM(C" + (startRow + 1) + ":C" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 3, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(C" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 4, formular);
                //DT
                formular = "=SUM(E" + (startRow + 1) + ":E" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 5, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(E" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 6, formular);
                //Nu DT
                formular = "=SUM(G" + (startRow + 1) + ":G" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 7, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(G" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 8, formular);
                //Ban tru
                formular = "=SUM(I" + (startRow + 1) + ":I" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 9, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(I" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 10, formular);

                sheet.GetRange(startRow, 1, startRow, 10).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                dicTotal[i] = startRow;
                startRow++;

                if (lstCPtmp.Count != 0)
                {
                    for (int j = 0; j < lstCPtmp.Count; j++)
                    {
                        objtmp = lstCPtmp[j];
                        sheet.GetRange(startRow, 1, startRow, 10).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.SetCellValue(startRow, 1, objtmp.DisplayName);
                        var lstPOCtmp = lstPOC.Where(p => p.ClassID == objtmp.ClassProfileID);
                        //tong so
                        sheet.SetCellValue(startRow, 2, lstPOCtmp.Count() > 0 ? lstPOCtmp.Select(p => p.CountPupil).Sum() : 0);
                        //Nu
                        sheet.SetCellValue(startRow, 3, lstPOCtmp.Where(p => p.Genre == 0).Count() > 0 ? lstPOCtmp.Where(p => p.Genre == 0).Select(p => p.CountPupil).Sum() : 0);
                        formular = "=IF(B" + startRow + "=0,0,ROUND(C" + startRow + "*100/B" + startRow + ",2))";
                        sheet.SetFormulaValue(startRow, 4, formular);
                        //Dan toc
                        sheet.SetCellValue(startRow, 5, lstPOCtmp.Where(p => p.EthnicID != null && p.EthnicID > SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).Count() > 0 ? lstPOCtmp.Where(p => p.EthnicID != null && p.EthnicID > SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).Select(p => p.CountPupil).Sum() : 0);
                        formular = "=IF(B" + startRow + "=0,0,ROUND(E" + startRow + "*100/B" + startRow + ",2))";
                        sheet.SetFormulaValue(startRow, 6, formular);
                        //Nu dan toc
                        sheet.SetCellValue(startRow, 7, lstPOCtmp.Where(p => p.Genre == 0 && p.EthnicID != null && p.EthnicID > SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).Count() > 0 ? lstPOCtmp.Where(p => p.Genre == 0 && p.EthnicID != null && p.EthnicID > SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).Select(p => p.CountPupil).Sum() : 0);
                        formular = "=IF(B" + startRow + "=0,0,ROUND(G" + startRow + "*100/B" + startRow + ",2))";
                        sheet.SetFormulaValue(startRow, 8, formular);
                        //Ban tru ClassType
                        sheet.SetCellValue(startRow, 9, lstPOCtmp.Where(p => p.IsResidentIn == true).Count() > 0 ? lstPOCtmp.Where(p => p.IsResidentIn == true).Select(p => p.CountPupil).Sum() : 0);
                        formular = "=IF(B" + startRow + "=0,0,ROUND(I" + startRow + "*100/B" + startRow + ",2))";
                        sheet.SetFormulaValue(startRow, 10, formular);
                        startRow++;
                    }
                }
                }
                if (lstEducationLevelID.Count > 0)
                {
                    //fill tong cong
                    int startRowTotal = startRow;
                    sheet.SetCellValue(startRowTotal, 1, "Tổng cộng");
                    string formularTT = "=SUM(B";
                    string formularNu = "=SUM(C";
                    string formularDT = "=SUM(E";
                    string formularNDT = "=SUM(G";
                    string formularNT = "=SUM(I";
                    for (int i = 0; i < dicTotal.Count; i++)
                    {
                        formularTT += dicTotal[i] + ",B";
                        formularNu += dicTotal[i] + ",C";
                        formularDT += dicTotal[i] + ",E";
                        formularNDT += dicTotal[i] + ",G";
                        formularNT += dicTotal[i] + ",I";
                    }
                    formularTT = formularTT.Substring(0, formularTT.Length - 2) + ")";
                    formularNu = formularNu.Substring(0, formularNu.Length - 2) + ")";
                    formularDT = formularDT.Substring(0, formularDT.Length - 2) + ")";
                    formularNDT = formularNDT.Substring(0, formularNDT.Length - 2) + ")";
                    formularNT = formularNT.Substring(0, formularNT.Length - 2) + ")";
                    //Tong
                    sheet.SetCellValue(startRowTotal, 2, formularTT);
                    //Nu
                    sheet.SetCellValue(startRowTotal, 3, formularNu);
                    formular = "=IF(B" + startRowTotal + "=0,0,ROUND(C" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                    sheet.SetCellValue(startRowTotal, 4, formular);
                    //DT
                    sheet.SetCellValue(startRowTotal, 5, formularDT);
                    formular = "=IF(B" + startRowTotal + "=0,0,ROUND(E" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                    sheet.SetCellValue(startRowTotal, 6, formular);
                    //Nu DT
                    sheet.SetCellValue(startRow, 7, formularNDT);
                    formular = "=IF(B" + startRowTotal + "=0,0,ROUND(G" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                    sheet.SetCellValue(startRowTotal, 8, formular);
                    //Ban tru
                    sheet.SetCellValue(startRowTotal, 9, formularNT);
                    formular = "=IF(B" + startRowTotal + "=0,0,ROUND(I" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                    sheet.SetCellValue(startRowTotal, 10, formular);
                    sheet.GetRange(startRow, 1, startRow, 10).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRowTotal, 1, startRowTotal, 10).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                }
                //ke khung
                sheet.GetRange(9, 1, startRow, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                //fill chữ ký
                string strDate = objSP.Province.ProvinceName + ", ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Date.Month + " năm " + DateTime.Now.Date.Year;
                int startRowDate = firstRow + lstClassProfile.Count + lstEducationLevelID.Count + 3;
                sheet.GetRange(startRowDate, 6, startRowDate, 10).Merge();
                sheet.SetCellValue(startRowDate, 6, strDate);
                sheet.GetRange(startRowDate + 1, 6, startRowDate + 1, 10).Merge();
                sheet.GetRange(startRowDate, 6, startRowDate, 10).SetFontStyle(false, System.Drawing.Color.Black, true, 11, false, false);
                sheet.SetCellValue(startRowDate + 1, 6, "HIỆU TRƯỞNG");
                sheet.GetRange(startRowDate + 1, 6, startRowDate + 1, 10).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                sheet.GetRange(startRowDate + 5, 6, startRowDate + 5, 10).Merge();
                sheet.GetRange(startRowDate + 5, 6, startRowDate + 5, 10).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                sheet.SetCellValue(startRowDate + 5, 6, SchoolProfileBusiness.Find(_globalInfo.SchoolID).HeadMasterName);

                return oBook.ToStream();
            }

        #endregion

        }
    }

