﻿using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportMenuChildrenArea.Controllers
{
    public class StatisticalChildrenAbsentController : BaseController
    {
        //
        // GET: /ReportMenuChildrenArea/StatisticalChildrenAbsent/
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ICalendarBusiness CalendarBusiness;

        public StatisticalChildrenAbsentController(IAcademicYearBusiness academicYearID,
                      IProcessedReportBusiness processedReportBusiness, IClassProfileBusiness classProfileBusiness,
                      ISchoolProfileBusiness schoolProfileBusiness, IEducationLevelBusiness educationLevelBusiness,
                      IPupilAbsenceBusiness pupilAbsenceBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
                      IPupilOfClassBusiness pupilOfClassBusiness, ICalendarBusiness CalendarBusiness)
        {
            this.AcademicYearBusiness = academicYearID;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.CalendarBusiness = CalendarBusiness;
        }

        public ActionResult Index()
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            ViewData["AcademicYear"] = objAcademicYear;
            var chooseStatiscal = new List<object>() { new { type = "Ngày", value = 1 }, new { type = "Buổi", value = 2 } };
            ViewData["ChooseStatiscal"] = new SelectList(chooseStatiscal, "value", "type");
            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReportPupil(FormCollection frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            string fromDate = frm["FromDate"];
            string toDate = frm["ToDate"];
            int typeStatiscal = int.Parse(frm["TypeStatiscalID"]);
            PupilAbsenceBO objPupilAbsenceBO = new PupilAbsenceBO();
            objPupilAbsenceBO.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            objPupilAbsenceBO.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            objPupilAbsenceBO.AppliedLevelID = _globalInfo.AppliedLevel.Value;
            objPupilAbsenceBO.FromDate = DateTime.Parse(fromDate);
            objPupilAbsenceBO.ToDate = DateTime.Parse(toDate);
            if (typeStatiscal == 1)
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.Tre_TKTreNghiHocTheoNgay);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = PupilAbsenceBusiness.GetReportChildrenAbsenceByDay(objPupilAbsenceBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ExportExcel(fromDate, toDate, typeStatiscal);
                    processedReport = PupilAbsenceBusiness.InsertChildrenAbsenceReportByDay(objPupilAbsenceBO, excel);
                    excel.Close();

                }
            }
            else
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.Tre_TKTreNghiHoc);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = PupilAbsenceBusiness.GetReportChildrenAbsence(objPupilAbsenceBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ExportExcel(fromDate, toDate, typeStatiscal);
                    processedReport = PupilAbsenceBusiness.InsertChildrenAbsenceReport(objPupilAbsenceBO, excel);
                    excel.Close();

                }
            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportPupil(FormCollection frm)
        {
            //int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            //int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            //ClassProfile objClassProFile = ClassProfileBusiness.Find(classID);
            string fromDate = frm["FromDate"];
            string toDate = frm["ToDate"];
            int typeStatiscal = int.Parse(frm["TypeStatiscalID"]);
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            PupilAbsenceBO objPupilAbsenceBO = new PupilAbsenceBO();
            objPupilAbsenceBO.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            objPupilAbsenceBO.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            objPupilAbsenceBO.AppliedLevelID = _globalInfo.AppliedLevel.Value;
            objPupilAbsenceBO.FromDate = DateTime.Parse(fromDate);
            objPupilAbsenceBO.ToDate = DateTime.Parse(toDate);
            //objPupilAbsenceBO.EducationLevelID = educationLevelID;
            //objPupilAbsenceBO.EducationName = educationLevelID > 0 ? "Khoi" + educationLevelID : "TatCa";
            //objPupilAbsenceBO.ClassName = objClassProFile != null ? objClassProFile.DisplayName : "";
            if (typeStatiscal == 1)
            {
                Stream excel = ExportExcel(fromDate, toDate, typeStatiscal);
                processedReport = PupilAbsenceBusiness.InsertChildrenAbsenceReportByDay(objPupilAbsenceBO, excel);
                excel.Close();

            }
            else
            {
                Stream excel = ExportExcel(fromDate, toDate, typeStatiscal);
                processedReport = PupilAbsenceBusiness.InsertChildrenAbsenceReport(objPupilAbsenceBO, excel);
                excel.Close();
            }


            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReportBySession(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> {
                SystemParamsInFile.Tre_TKTreNghiHoc,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReportBySession(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.Tre_TKTreNghiHoc,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public FileResult DownloadReportByDay(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> {
                SystemParamsInFile.Tre_TKTreNghiHocTheoNgay,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReportByDay(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.Tre_TKTreNghiHocTheoNgay,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream ExportExcel(string fromDate, string toDate, int typeStatiscal)
        {
            //Tên khối
            string educationLevelName = string.Empty;
            //Tên lớp
            string className = string.Empty;
            //Lấy Cấp học và Từ ngày, Đến ngày
            int? appliedLevel = _globalInfo.AppliedLevel;

            DateTime FromDate = Convert.ToDateTime(fromDate);
            DateTime ToDate = Convert.ToDateTime(toDate);
            Stream excel = null;
            #region // Theo ngày
            if (typeStatiscal == 1)
            {
                //Đường dẫn template & Tên file xuất ra
                string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.Tre_TKTreNghiHocTheoNgay + ".xls");
                IVTWorkbook oBook = VTExport.OpenWorkbook(template);
                string fileName = "";
                fileName = string.Format("Tre_TKHSNghiHocTheoNgay.xls");

                //Fill dữ liệu
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                //Dữ liệu chung
                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                Province province = school.Province;
                firstSheet.SetCellValue("A" + 2, UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, _globalInfo.AppliedLevel.Value).ToUpper());
                firstSheet.SetCellValue("A" + 3, _globalInfo.SchoolName);
                firstSheet.SetCellValue("A" + 5, "THỐNG KÊ TRẺ NGHỈ HỌC");
                firstSheet.SetCellValue("A" + 6, "Từ ngày " + fromDate + " đến ngày " + toDate);

                //Fill danh sách
                IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                //{"ClassID",classID},
                {"FromAbsentDate",FromDate},
                {"ToAbsentDate",ToDate},
                {"AppliedLevel",appliedLevel},
            };

                //List HS nghi hoc
                IQueryable<PupilAbsence> lstPupilAbsence = PupilAbsenceBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
                List<PupilAbsenceBO> lstPupilAbsenceBO = (from p in lstPupilAbsence
                                                          join cp in ClassProfileBusiness.All on p.ClassID equals cp.ClassProfileID
                                                          join poc in PupilOfClassBusiness.All on new { p.ClassID, p.PupilID } equals new { poc.ClassID, poc.PupilID }
                                                          where ((((poc.AssignedDate <= p.AbsentDate && p.AbsentDate <= poc.EndDate)
                                                                   && (poc.Status == 3 || poc.Status == 4 || poc.Status == 5))
                                                                   || ((poc.AssignedDate <= p.AbsentDate) && (poc.Status == 1 || poc.Status == 2)))
                                                                   && poc.AcademicYearID == _globalInfo.AcademicYearID)
                                                                   && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                          select new PupilAbsenceBO
                                                          {
                                                              ClassID = p.ClassID,
                                                              Name = p.PupilProfile.Name,
                                                              ClassName = p.ClassProfile.DisplayName,
                                                              PupilID = p.PupilID,
                                                              FullName = p.PupilProfile.FullName,
                                                              AbsentDate = p.AbsentDate,
                                                              Section = p.Section,
                                                              IsAccepted = p.IsAccepted,
                                                              Status = poc.Status,
                                                              OrderNumber = cp.OrderNumber,
                                                              OrderInClass = poc.OrderInClass,
                                                              EducationLevelID = cp.EducationLevelID
                                                          }).ToList();
                //Sắp xếp
                lstPupilAbsenceBO = lstPupilAbsenceBO.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0)
                                        .ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass.HasValue ? o.OrderInClass : 0).ThenBy(o => o.Name).ThenBy(o => o.FullName)
                                        .ThenByDescending(o => o.AbsentDate).ToList();
                //Lấy số buổi tối đa của các lớp
                Dictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass["SchoolID"] = _globalInfo.SchoolID.Value;
                dicClass["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicClass["AppliedLevel"] = _globalInfo.AppliedLevel;
                List<ClassProfile> lstClass = ClassProfileBusiness.Search(dicClass).OrderBy(a => a.DisplayName).ToList();
                var lstSection = new List<CalendarBusiness.ClassSection>();
                foreach (ClassProfile cl in lstClass)
                {
                    var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                    CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                    var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                    lstSection = lstSection.Union(listSectionOther).ToList();

                    if (lstSection.Count >= 3)
                        break;
                }
                //danh sách Education
                List<EducationLevel> lstEducationLevel = _globalInfo.EducationLevels;
                List<PupilAbsenceBO> lstPupilAbsenceTemp = new List<PupilAbsenceBO>();
                List<ClassProfile> lstClassTemp = new List<ClassProfile>();
                ClassProfile objClassProfile = new ClassProfile();
                int startRow = 9;
                int accept, notAccept = 0;
                string totalAll = "";
                for (int i = 0; i < lstEducationLevel.Count(); i++)
                {
                    educationLevelName = lstEducationLevel[i].Resolution;
                    //firstSheet.SetCellValue(stratRow, stratColumn, educationLevelName);
                    int stt = 0;
                    lstClassTemp = lstClass.Where(x => x.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    for (int j = 0; j < lstClassTemp.Count; j++)
                    {
                        stt++;
                        int startColumn = 1;
                        objClassProfile = lstClassTemp[j];
                        lstPupilAbsenceTemp = lstPupilAbsenceBO.Where(x => x.ClassID == objClassProfile.ClassProfileID).OrderBy(x => x.Section).ToList();
                        lstPupilAbsenceTemp = lstPupilAbsenceTemp.GroupBy(x => new {x.PupilID, x.AbsentDate }).Select(g => g.First()).ToList();
                        //var lstTemp = new List<PupilAbsenceBO>();
                        //for (int z = 0; z < lstPupilAbsenceTemp.Count(); )
                        //{
                        //    int checkAdd = 0;
                        //    for (int k = z + 1; k < lstPupilAbsenceTemp.Count(); k++)
                        //    {
                        //        if (lstPupilAbsenceTemp[z].AbsentDate == lstPupilAbsenceTemp[k].AbsentDate && lstPupilAbsenceTemp[z].PupilID == lstPupilAbsenceTemp[k].PupilID)
                        //        {
                        //            if (lstPupilAbsenceTemp[z].Section == 1)
                        //            {
                        //                lstTemp.Add(lstPupilAbsenceTemp[z]);
                        //                checkAdd++;
                        //                z = z + 2;
                        //                break;
                        //            }
                        //            else 
                        //            {
                        //                lstTemp.Add(lstPupilAbsenceTemp[k]);
                        //                checkAdd++;
                        //                z = z + 2;
                        //                break;
                        //            }
                        //        }
                        //    }
                            
                        //    if(checkAdd == 0)
                        //    {
                        //        lstTemp.Add(lstPupilAbsenceTemp[z]);
                        //        z++;
                        //    }
                        //}
                        //lstPupilAbsenceTemp = lstTemp;
                        firstSheet.SetCellValue(startRow, startColumn, stt);
                        startColumn++;
                        firstSheet.SetCellValue(startRow, startColumn, objClassProfile.DisplayName);
                        startColumn++;
                        
                        accept = lstPupilAbsenceTemp.Where(x => (x.Section == 1 || x.Section == 2 || x.Section == 3) && x.IsAccepted == true).Count();
                        firstSheet.SetCellValue(startRow, startColumn, accept);
                        startColumn++;
                        notAccept = lstPupilAbsenceTemp.Where(x => (x.Section == 1 || x.Section == 2 || x.Section == 3) && x.IsAccepted == false).Count();
                        firstSheet.SetCellValue(startRow, startColumn, notAccept);
                        startColumn++;
                        firstSheet.SetCellValue(startRow, startColumn, "=C" + startRow + "+D" + startRow);
                        startColumn++;
                        firstSheet.SetRowHeight(startRow, 20);
                        startRow++;
                    }                  
                    totalAll = totalAll + startRow + ",";
                    firstSheet.SetCellValue("A" + startRow, educationLevelName);
                    firstSheet.GetRange("A" + startRow, "B" + startRow).Merge();
                    firstSheet.GetRange("A" + startRow, "E" + startRow).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    firstSheet.SetCellValue("C" + startRow, "=SUM(C" + (startRow - lstClassTemp.Count()) + ":" + "C" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("D" + startRow, "=SUM(D" + (startRow - lstClassTemp.Count()) + ":" + "D" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("E" + startRow, "=SUM(E" + (startRow - lstClassTemp.Count()) + ":" + "E" + (startRow - 1) + ")");
                    firstSheet.SetRowHeight(startRow, 20);
                    startRow++;
                }

                string[] lstTotalRow = totalAll.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                firstSheet.SetCellValue("A" + startRow, "Toàn trường");
                firstSheet.GetRange("A" + startRow, "B" + startRow).Merge();
                firstSheet.SetCellValue("C" + startRow, "=" + getTotalData("C", lstTotalRow));
                firstSheet.SetCellValue("D" + startRow, "=" + getTotalData("D", lstTotalRow));
                firstSheet.SetCellValue("E" + startRow, "=" + getTotalData("E", lstTotalRow));
                firstSheet.GetRange("A" + startRow, "E" + startRow).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                firstSheet.GetRange(9, 1, startRow, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                firstSheet.SetRowHeight(startRow, 20);
                startRow = startRow + 2;
                firstSheet.SetCellValue("A" + startRow, "Lưu ý: Trẻ được tính là nghỉ học trong ngày khi nghỉ 1 hoặc nhiều buổi trong cùng ngày. Nếu trẻ nghỉ học nhiều buổi trong ngày; có phép/không phép sẽ dựa vào buổi học đầu tiên.");
                firstSheet.GetRange("A" + startRow, "E" + startRow).Merge();
                firstSheet.GetRange("A" + startRow, "E" + startRow).WrapText();
                firstSheet.GetRange(startRow, 1, startRow + 1, 1).Merge();
                firstSheet.SetRowHeight(startRow, 30);
                firstSheet.SetRowHeight(startRow + 1, 30);


                if (lstSection.Where(x => x.Section == 1).Count() == 0)
                {
                    firstSheet.HideColumn(3);
                    firstSheet.HideColumn(4);
                    firstSheet.HideColumn(5);
                }

                if (lstSection.Where(x => x.Section == 2).Count() == 0)
                {
                    firstSheet.HideColumn(6);
                    firstSheet.HideColumn(7);
                    firstSheet.HideColumn(8);
                }

                if (lstSection.Where(x => x.Section == 3).Count() == 0)
                {
                    firstSheet.HideColumn(9);
                    firstSheet.HideColumn(10);
                    firstSheet.HideColumn(11);
                }
                excel = oBook.ToStream();
            }
            #endregion

            #region // Theo buổi
            else
            {
                //Đường dẫn template & Tên file xuất ra
                string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.Tre_TKTreNghiHoc + ".xls");
                IVTWorkbook oBook = VTExport.OpenWorkbook(template);
                string fileName = "";
                fileName = string.Format("Tre_TKHSNghiHoc.xls");

                //Fill dữ liệu
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                //Dữ liệu chung
                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                Province province = school.Province;
                firstSheet.SetCellValue("A" + 2, UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, _globalInfo.AppliedLevel.Value).ToUpper());
                firstSheet.SetCellValue("A" + 3, _globalInfo.SchoolName);
                firstSheet.SetCellValue("A" + 5, "THỐNG KÊ TRẺ NGHỈ HỌC");
                firstSheet.SetCellValue("A" + 6, "Từ ngày " + fromDate + " đến ngày " + toDate);

                //Fill danh sách
                IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                //{"ClassID",classID},
                {"FromAbsentDate",FromDate},
                {"ToAbsentDate",ToDate},
                {"AppliedLevel",appliedLevel},
            };

                //List HS nghi hoc
                IQueryable<PupilAbsence> lstPupilAbsence = PupilAbsenceBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
                List<PupilAbsenceBO> lstPupilAbsenceBO = (from p in lstPupilAbsence
                                                          join cp in ClassProfileBusiness.All on p.ClassID equals cp.ClassProfileID
                                                          join poc in PupilOfClassBusiness.All on new { p.ClassID, p.PupilID } equals new { poc.ClassID, poc.PupilID }
                                                          where ((((poc.AssignedDate <= p.AbsentDate && p.AbsentDate <= poc.EndDate)
                                                                   && (poc.Status == 3 || poc.Status == 4 || poc.Status == 5))
                                                                   || ((poc.AssignedDate <= p.AbsentDate) && (poc.Status == 1 || poc.Status == 2)))
                                                                   && poc.AcademicYearID == _globalInfo.AcademicYearID)
                                                                   && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                          select new PupilAbsenceBO
                                                          {
                                                              ClassID = p.ClassID,
                                                              Name = p.PupilProfile.Name,
                                                              ClassName = p.ClassProfile.DisplayName,
                                                              PupilID = p.PupilID,
                                                              FullName = p.PupilProfile.FullName,
                                                              AbsentDate = p.AbsentDate,
                                                              Section = p.Section,
                                                              IsAccepted = p.IsAccepted,
                                                              Status = poc.Status,
                                                              OrderNumber = cp.OrderNumber,
                                                              OrderInClass = poc.OrderInClass,
                                                              EducationLevelID = cp.EducationLevelID
                                                          }).ToList();
                //Sắp xếp
                lstPupilAbsenceBO = lstPupilAbsenceBO.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0)
                                        .ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass.HasValue ? o.OrderInClass : 0).ThenBy(o => o.Name).ThenBy(o => o.FullName)
                                        .ThenByDescending(o => o.AbsentDate).ToList();
                //Lấy số buổi tối đa của các lớp
                Dictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass["SchoolID"] = _globalInfo.SchoolID.Value;
                dicClass["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicClass["AppliedLevel"] = _globalInfo.AppliedLevel;
                List<ClassProfile> lstClass = ClassProfileBusiness.Search(dicClass).OrderBy(a => a.DisplayName).ToList();
                var lstSection = new List<CalendarBusiness.ClassSection>();
                foreach (ClassProfile cl in lstClass)
                {
                    var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                    CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                    var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                    lstSection = lstSection.Union(listSectionOther).ToList();

                    if (lstSection.Count >= 3)
                        break;
                }
                //danh sách Education
                List<EducationLevel> lstEducationLevel = _globalInfo.EducationLevels;
                List<PupilAbsenceBO> lstPupilAbsenceTemp = new List<PupilAbsenceBO>();
                List<ClassProfile> lstClassTemp = new List<ClassProfile>();
                ClassProfile objClassProfile = new ClassProfile();
                int startRow = 10;
                int accept, notAccept = 0;
                string totalAll = "";
                for (int i = 0; i < lstEducationLevel.Count(); i++)
                {
                    educationLevelName = lstEducationLevel[i].Resolution;
                    //firstSheet.SetCellValue(stratRow, stratColumn, educationLevelName);
                    int stt = 0;
                    lstClassTemp = lstClass.Where(x => x.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    for (int j = 0; j < lstClassTemp.Count; j++)
                    {
                        stt++;
                        int startColumn = 1;
                        objClassProfile = lstClassTemp[j];
                        lstPupilAbsenceTemp = lstPupilAbsenceBO.Where(x => x.ClassID == objClassProfile.ClassProfileID).ToList();
                        firstSheet.SetCellValue(startRow, startColumn, stt);
                        startColumn++;
                        firstSheet.SetCellValue(startRow, startColumn, objClassProfile.DisplayName);
                        startColumn++;

                        int? maxSectionTemp = objClassProfile.Section;
                        var lstSectionTemp = new List<CalendarBusiness.ClassSection>();
                        CalendarBusiness.GetSection((maxSectionTemp.HasValue) ? maxSectionTemp.Value : 0, ref lstSectionTemp);

                        // sáng
                        if (lstSectionTemp.Where(x => x.Section == 1).Count() == 0 && lstSection.Where(x => x.Section == 1).Count() > 0)
                        {
                            firstSheet.GetRange(startRow, startColumn, startRow, startColumn + 2).Merge();
                            //firstSheet.GetRange(startRow, startColumn, startRow, startColumn + 2).FillColor(System.Drawing.Color.LightGray);
                            startColumn = startColumn + 3;
                        }
                        else
                        {
                            accept = lstPupilAbsenceTemp.Where(x => x.Section == 1 && x.IsAccepted == true).Count();
                            firstSheet.SetCellValue(startRow, startColumn, accept);
                            startColumn++;
                            notAccept = lstPupilAbsenceTemp.Where(x => x.Section == 1 && x.IsAccepted == false).Count();
                            firstSheet.SetCellValue(startRow, startColumn, notAccept);
                            startColumn++;
                            firstSheet.SetCellValue(startRow, startColumn, "=C" + startRow + "+D" + startRow);
                            startColumn++;
                        }

                        //chiều
                        if (lstSectionTemp.Where(x => x.Section == 2).Count() == 0 && lstSection.Where(x => x.Section == 2).Count() > 0)
                        {
                            firstSheet.GetRange(startRow, startColumn, startRow, startColumn + 2).Merge();
                            //firstSheet.GetRange(startRow, startColumn, startRow, startColumn + 2).FillColor(System.Drawing.Color.LightGray);
                            startColumn = startColumn + 3;
                        }
                        else
                        {
                            accept = lstPupilAbsenceTemp.Where(x => x.Section == 2 && x.IsAccepted == true).Count();
                            firstSheet.SetCellValue(startRow, startColumn, accept);
                            startColumn++;
                            notAccept = lstPupilAbsenceTemp.Where(x => x.Section == 2 && x.IsAccepted == false).Count();
                            firstSheet.SetCellValue(startRow, startColumn, notAccept);
                            startColumn++;
                            firstSheet.SetCellValue(startRow, startColumn, "=F" + startRow + "+G" + startRow);
                            startColumn++;
                        }

                        //tối
                        if (lstSectionTemp.Where(x => x.Section == 3).Count() == 0 && lstSection.Where(x => x.Section == 3).Count() > 0)
                        {
                            firstSheet.GetRange(startRow, startColumn, startRow, startColumn + 2).Merge();
                            //firstSheet.GetRange(startRow, startColumn, startRow, startColumn + 2).FillColor(System.Drawing.Color.LightGray);
                            startColumn = startColumn + 3;
                        }
                        else
                        {
                            accept = lstPupilAbsenceTemp.Where(x => x.Section == 3 && x.IsAccepted == true).Count();
                            firstSheet.SetCellValue(startRow, startColumn, accept);
                            startColumn++;
                            notAccept = lstPupilAbsenceTemp.Where(x => x.Section == 3 && x.IsAccepted == false).Count();
                            firstSheet.SetCellValue(startRow, startColumn, notAccept);
                            startColumn++;
                            firstSheet.SetCellValue(startRow, startColumn, "=I" + startRow + "+J" + startRow);
                            startColumn++;
                        }
                        firstSheet.SetRowHeight(startRow, 20);
                        startRow++;
                    }
                    totalAll = totalAll + startRow + ",";
                    firstSheet.SetCellValue("A" + startRow, educationLevelName);
                    firstSheet.GetRange("A" + startRow, "B" + startRow).Merge();
                    firstSheet.GetRange("A" + startRow, "K" + startRow).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    firstSheet.SetCellValue("C" + startRow, "=SUM(C" + (startRow - lstClassTemp.Count()) + ":" + "C" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("D" + startRow, "=SUM(D" + (startRow - lstClassTemp.Count()) + ":" + "D" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("E" + startRow, "=SUM(E" + (startRow - lstClassTemp.Count()) + ":" + "E" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("F" + startRow, "=SUM(F" + (startRow - lstClassTemp.Count()) + ":" + "F" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("G" + startRow, "=SUM(G" + (startRow - lstClassTemp.Count()) + ":" + "G" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("H" + startRow, "=SUM(H" + (startRow - lstClassTemp.Count()) + ":" + "H" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("I" + startRow, "=SUM(I" + (startRow - lstClassTemp.Count()) + ":" + "I" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("J" + startRow, "=SUM(J" + (startRow - lstClassTemp.Count()) + ":" + "J" + (startRow - 1) + ")");
                    firstSheet.SetCellValue("K" + startRow, "=SUM(K" + (startRow - lstClassTemp.Count()) + ":" + "K" + (startRow - 1) + ")");
                    firstSheet.SetRowHeight(startRow, 20);
                    startRow++;
                }

                string[] lstTotalRow = totalAll.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                firstSheet.SetCellValue("A" + startRow, "Toàn trường");
                firstSheet.GetRange("A" + startRow, "B" + startRow).Merge();
                firstSheet.SetCellValue("C" + startRow, "=" + getTotalData("C", lstTotalRow));
                firstSheet.SetCellValue("D" + startRow, "=" + getTotalData("D", lstTotalRow));
                firstSheet.SetCellValue("E" + startRow, "=" + getTotalData("E", lstTotalRow));
                firstSheet.SetCellValue("F" + startRow, "=" + getTotalData("F", lstTotalRow));
                firstSheet.SetCellValue("G" + startRow, "=" + getTotalData("G", lstTotalRow));
                firstSheet.SetCellValue("H" + startRow, "=" + getTotalData("H", lstTotalRow));
                firstSheet.SetCellValue("I" + startRow, "=" + getTotalData("I", lstTotalRow));
                firstSheet.SetCellValue("J" + startRow, "=" + getTotalData("J", lstTotalRow));
                firstSheet.SetCellValue("K" + startRow, "=" + getTotalData("K", lstTotalRow));
                firstSheet.GetRange("A" + startRow, "K" + startRow).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                firstSheet.GetRange(10, 1, startRow, 11).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                firstSheet.SetRowHeight(startRow, 20);
                if (lstSection.Where(x => x.Section == 1).Count() == 0)
                {
                    firstSheet.HideColumn(3);
                    firstSheet.HideColumn(4);
                    firstSheet.HideColumn(5);
                }

                if (lstSection.Where(x => x.Section == 2).Count() == 0)
                {
                    firstSheet.HideColumn(6);
                    firstSheet.HideColumn(7);
                    firstSheet.HideColumn(8);
                }

                if (lstSection.Where(x => x.Section == 3).Count() == 0)
                {
                    firstSheet.HideColumn(9);
                    firstSheet.HideColumn(10);
                    firstSheet.HideColumn(11);
                }
                excel = oBook.ToStream();
            }
            #endregion
            return excel;
        }

        public string getTotalData(string a, string[] b)
        {
            string result = "";
            foreach (var i in b)
            {
                result = result + a + i + "+";
            }
            if (result != "")
                result = result.Substring(0, result.Length - 1);
            return result;
        }

    }
}
