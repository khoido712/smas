﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMenuChildrenArea.Models
{
    public class ReportTheStatisticsPupilCountModel
    {

    }
    public class Information
    {
        public int ClassID { get; set; }
        public int? EthnicID { get; set; }
        public int Genre { get; set; }
        public int? ClassType { get; set; }
        public int EducationLevelID { get; set; }
        public bool? IsResidentIn { get; set; }
        public int CountPupil { get; set; }
        public string CombinedClassCode { get; set; }
    }
}
