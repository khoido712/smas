﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportMenuChildrenArea
{
    public class ReportMenuChildrenAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportMenuChildrenArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportMenuChildrenArea_default",
                "ReportMenuChildrenArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
