﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.ReportCapacityStatisticsTertiaryArea.Models;
using SMAS.Web.Controllers;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Web.Areas.ReportConductStatisticsArea;

namespace SMAS.Web.Areas.ReportCapacityStatisticsTertiaryArea.Controllers
{
    public class ReportCapacityStatisticsTertiaryController : BaseController
    {
        IDistrictBusiness DistrictBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        ITrainingTypeBusiness TrainingTypeBusiness;
        ISubCommitteeBusiness SubCommitteeBusiness;
        ICapacityStatisticBusiness CapacityStatisticBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IConductStatisticBusiness ConductStatisticBusiness;
        IEducationLevelBusiness EducationLevelBussiness;
        public ReportCapacityStatisticsTertiaryController(  IDistrictBusiness districtBusiness,
                                                            IAcademicYearBusiness academicYearBusiness,
                                                            ITrainingTypeBusiness trainingTypeBusiness,
                                                            ISubCommitteeBusiness subCommitteeBusiness,
                                                            ICapacityStatisticBusiness capacityStatisticBusiness,
                                                            IProcessedReportBusiness processedReportBusiness,
                                                            IConductStatisticBusiness conductStatisticBusiness,
                                                            IEducationLevelBusiness educationLevelBusiness)
        {
            this.DistrictBusiness = districtBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
            this.EducationLevelBussiness = educationLevelBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        public ActionResult Search(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            #region Thống kê HL theo ban
            if (model.ReportType == 2)
            {
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                string ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUCTHEOBAN;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                }
            }
            #endregion
            #region Thống kê HL theo quận huyện
            if (model.ReportType == 3)
            {
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                string ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUCTHEOQUANHUYEN;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                }
            }
            #endregion
            #region Thống kê HK theo ban
            if (model.ReportType == 4)
            {
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = 0;
                ccr.DistrictID = glo.DistrictID.HasValue ? glo.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                string ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOBAN;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                }
            }
            #endregion
            #region Thống kê HK theo quận/ huyện
            if(model.ReportType == 5)
            {
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;
                ccr.SupervisingDeptID = glo.SupervisingDeptID.Value;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                string ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                }
            }
            #endregion

            return Json(new { });
        }

        public FileResult DownloadReport(int ProcessedReportID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                    {"SupervisingDeptID", GlobalInfo.SupervisingDeptID}
                };
            List<string> listRC = new List<string> { 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEDIEMKIEMTRADINHKY, 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEDIEMKIEMTRAHOCKY, 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEM, 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOBAN, 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN, 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEHLM, 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUC, 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUCTHEOBAN, 
                    SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUCTHEOQUANHUYEN
                };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(ProcessedReportID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            #region TK HL theo ban
            if (model.ReportType == 2)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.Year;
                dic["Semester"] = model.Semester;
                dic["ReportCode"] = SystemParamsInFile.THONGKEHOCLUCTHEOBANCAP3;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                //dic["DistrictID"] = model.DistrictID;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID;
                dic["IsSubSuperVisingDeptRole"] = glo.IsSubSuperVisingDeptRole;
                dic["IsSuperVisingDeptRole"] = glo.IsSuperVisingDeptRole;
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                int FileID = 0;
                IEnumerable<CapacityStatisticsBO> listCapacityStatisticsOfProvinceGroupBySubCommittee23 = CapacityStatisticBusiness.CreateSGDCapacityStatisticsBySubCommitteeTertiary(dic, input, out FileID);
                ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE] = listCapacityStatisticsOfProvinceGroupBySubCommittee23;
                if (listCapacityStatisticsOfProvinceGroupBySubCommittee23 != null && listCapacityStatisticsOfProvinceGroupBySubCommittee23.Count() > 0)
                {
                    var listEducationLevel = from lcs in listCapacityStatisticsOfProvinceGroupBySubCommittee23.Select(u => u.EducationLevelID).Distinct()
                                             select new CommitteModel
                                             {
                                                 EducationLevelID = lcs.Value,
                                                 EducationLevelName = listCapacityStatisticsOfProvinceGroupBySubCommittee23.Where(o => o.EducationLevelID == lcs.Value).FirstOrDefault().EducationLevelName
                                             };
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE_EDUCATIONLEVEL] = listEducationLevel;
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_ID] = FileID;
                }
                else
                {
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE_EDUCATIONLEVEL] = new List<CommitteModel>();
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_ID] = FileID;
                }

                return PartialView("_GridCapacitySubCommittee");
            }
            #endregion
            #region TK HL theo quận/huyện
            if (model.ReportType == 3)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.Year;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                dic["TrainingTypeID"] = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;
                dic["ReportCode"] = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUCTHEOQUANHUYEN;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                dic["DistrictID"] = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                dic["SubCommitteeID"] = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : 0;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID.Value;
                dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                dic["IsSubSuperVisingDeptRole"] = glo.IsSubSuperVisingDeptRole;
                dic["IsSuperVisingDeptRole"] = glo.IsSuperVisingDeptRole;
                CapacityConductReport ccr = new CapacityConductReport();

                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                string hashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                int FileID = 0;
                IEnumerable<CapacityStatisticsBO> listCapacityStatistic = CapacityStatisticBusiness.CreateSGDCapacityStatisticsByDistrictTertiary(dic, hashKey, out FileID);
                ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_DATA] = listCapacityStatistic;
                ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_ID] = FileID;
                return PartialView("_GridCapacityByProvince");
            }
            #endregion
            #region TK HK theo ban
            if (model.ReportType == 4)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.Year;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = 0;
                dic["TrainingTypeID"] = 0;
                dic["ReportCode"] = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOBAN;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                dic["DistrictID"] = glo.DistrictID;
                dic["SubCommitteeID"] = 0;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID.Value;
                dic["IsSubSuperVisingDeptRole"] = glo.IsSubSuperVisingDeptRole;
                dic["IsSuperVisingDeptRole"] = glo.IsSuperVisingDeptRole;
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                int FileID = 0;
                IEnumerable<ConductStatisticsOfProvinceGroupBySubCommittee23> listCapacityStatisticsOfProvinceGroupBySubCommittee23 = ConductStatisticBusiness.CreateSGDConductStatisticsBySubCommitteeTertiary(dic, input, out FileID);
                ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE] = listCapacityStatisticsOfProvinceGroupBySubCommittee23;
                if (listCapacityStatisticsOfProvinceGroupBySubCommittee23 != null && listCapacityStatisticsOfProvinceGroupBySubCommittee23.Count() > 0)
                {
                    var listEducationLevel = from lcs in listCapacityStatisticsOfProvinceGroupBySubCommittee23.Select(u => u.EducationLevel).Distinct()
                                             select new CommitteModel
                                             {
                                                 EducationLevelID = lcs.Value,
                                                 EducationLevelName = listCapacityStatisticsOfProvinceGroupBySubCommittee23.Where(o => o.EducationLevel == lcs.Value).FirstOrDefault().EducationLevelName

                                             };
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE_EDUCATIONLEVEL] = listEducationLevel;
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_ID] = FileID;
                }
                else
                {
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE_EDUCATIONLEVEL] = new List<CommitteModel>();
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_ID] = FileID;
                }

                return PartialView("_GridConductSubCommittee");
            }
            #endregion
            #region TK HK theo quận/huyện
            if (model.ReportType == 5)
            {
                CapacityConductReport capConductRpt = new CapacityConductReport();
                capConductRpt.Year = model.Year.HasValue ? model.Year.Value : 0;
                capConductRpt.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                capConductRpt.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                capConductRpt.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                capConductRpt.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                capConductRpt.SubcommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : 0;
                capConductRpt.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = capConductRpt.Year;
                dic["Semester"] = capConductRpt.Semester;
                dic["EducationLevelID"] = 0;
                dic["TrainingTypeID"] = 0;
                dic["ReportCode"] = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = capConductRpt.ProvinceID;
                dic["DistrictID"] = capConductRpt.DistrictID;
                dic["SubCommitteeID"] = 0;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID.Value;
                dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                dic["IsSubSuperVisingDeptRole"] = glo.IsSubSuperVisingDeptRole;
                dic["IsSuperVisingDeptRole"] = glo.IsSuperVisingDeptRole;
                int FileID = 0;
                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capConductRpt);
                var lstConductStatisticsOfProvinceTertiary = ConductStatisticBusiness.CreateSGDConductStatisticsByDistrictTertiary(dic, out FileID, InputParameterHashKey);
                ViewData[ReportConductStatisticsConstants.LIST_CONDUCTSTATISTICSOFPROVINCETERTIARY] = lstConductStatisticsOfProvinceTertiary;
                ViewData[ReportConductStatisticsConstants.GRID_REPORT_ID] = FileID;
                return PartialView("_GridConductByDistric");
            }
            #endregion
            return PartialView("_GridEmpty");
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            
            ViewData[ReportCapacityStatisticsTertiaryConstants.LIST_REPORT_TYPE] = new List<ViettelCheckboxList>() { 
                                                    new ViettelCheckboxList{ Label=Res.Get("ReportCapacityStatisticsTertiary_Label_ReportTypeCommittee"), Value=2, cchecked=true, disabled= false},
                                                    new ViettelCheckboxList{Label=Res.Get("ConductStatistics_Label_ReportBySubCommittee"), Value=4, cchecked=false, disabled= false}, 
                                                    new ViettelCheckboxList{ Label=Res.Get("ReportCapacityStatisticsTertiary_Label_ReportTypeDistrict"), Value=3, cchecked=false, disabled= false},                                                    
                                                    new ViettelCheckboxList{Label=Res.Get("ConductStatistics_Label_ReportByDistrict"), Value=5, cchecked=false, disabled= false}
                                                };

            IEnumerable<District> listDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "ProvinceID", glo.ProvinceID.Value }, { "IsActive", true } });
            ViewData[ReportCapacityStatisticsTertiaryConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName");

            List<int> listAcademicYear = AcademicYearBusiness.GetListYearForSupervisingDept_THPT(glo.SupervisingDeptID.Value);
            ViewData[ReportCapacityStatisticsTertiaryConstants.LIST_ACADEMIC_YEAR] = listAcademicYear.Select(u => new SelectListItem { Text = u + " - " + (u + 1), Value = u.ToString(), Selected = false }).ToList();

            ViewData[ReportCapacityStatisticsTertiaryConstants.LIST_SEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value");
            List<EducationLevel> lstEducationLevel = EducationLevelBussiness.All.Where(o => o.Grade == SystemParamsInFile.EDUCATION_GRADE_TERTIARY).ToList();
            ViewData[ReportCapacityStatisticsTertiaryConstants.LIST_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            List<TrainingType> listTrainingType = TrainingTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            ViewData[ReportCapacityStatisticsTertiaryConstants.LIST_TRAINING_TYPE] = new SelectList(listTrainingType, "TrainingTypeID", "Resolution");

            List<SubCommittee> listSubComm = SubCommitteeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            ViewData[ReportCapacityStatisticsTertiaryConstants.LIST_SUB_COMMITTEE] = new SelectList(listSubComm, "SubCommitteeID", "Resolution");
        }
    }
}
