﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportCapacityStatisticsTertiaryArea
{
    public class ReportCapacityStatisticsTertiaryConstants
    {
        public const string LIST_DISTRICT = "list_district";
        public const string LIST_ACADEMIC_YEAR = "list_academic_year";
        public const string LIST_SEMESTER = "list_semester";
        public const string LIST_EDUCATION_LEVEL = "list_education_level";
        public const string LIST_TRAINING_TYPE = "list_training_type";
        public const string LIST_SUB_COMMITTEE = "list_sub_committee";
        public const string LIST_REPORT_TYPE = "list_report_type";
        public const string LIST_CONDUCTSTATISTICSOFPROVINCETERTIARY = "lstConductStatisticsOfProvinceTertiary";

        public const string GRID_REPORT_DATA = "grid_report_data";
        public const string GRID_REPORT_COMITTEE = "grid_report_comittee";
        public const string GRID_REPORT_COMITTEE_EDUCATIONLEVEL = "grid_report_comittee_educationlevel";
        public const string GRID_REPORT_ID = "grid_report_id";
        public const string GRID_REPORT_DATE = "grid_report_date";
    }
}