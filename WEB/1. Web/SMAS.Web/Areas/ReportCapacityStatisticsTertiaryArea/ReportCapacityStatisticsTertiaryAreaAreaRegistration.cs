﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportCapacityStatisticsTertiaryArea
{
    public class ReportCapacityStatisticsTertiaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportCapacityStatisticsTertiaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportCapacityStatisticsTertiaryArea_default",
                "ReportCapacityStatisticsTertiaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
