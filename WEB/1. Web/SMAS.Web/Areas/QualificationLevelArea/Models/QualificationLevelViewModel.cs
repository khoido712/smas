/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.QualificationLevelArea.Models
{
    public class QualificationLevelViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 QualificationLevelID { get; set; }

        [ResourceDisplayName("QualificationLevel_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]				
		public System.String Resolution { get; set; }

        [ResourceDisplayName("QualificationLevel_Label_Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]				
		public System.String Description { get; set; }								
								
	       
    }
}


