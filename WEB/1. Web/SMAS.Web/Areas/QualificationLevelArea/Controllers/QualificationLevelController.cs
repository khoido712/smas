﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.QualificationLevelArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.QualificationLevelArea.Controllers
{
    public class QualificationLevelController : BaseController
    {        
        private readonly IQualificationLevelBusiness QualificationLevelBusiness;
		
		public QualificationLevelController (IQualificationLevelBusiness qualificationlevelBusiness)
		{
			this.QualificationLevelBusiness = qualificationlevelBusiness;
		}
		
		//
        // GET: /QualificationLevel/

        public ActionResult Index()
        {
            SetViewDataPermission("QualificationLevel", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here

            IEnumerable<QualificationLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[QualificationLevelConstants.LIST_QUALIFICATIONLEVEL] = lst;
            return View();
        }

		//
        // GET: /QualificationLevel/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
			//add search info
			//

            IEnumerable<QualificationLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[QualificationLevelConstants.LIST_QUALIFICATIONLEVEL] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            QualificationLevel qualificationlevel = new QualificationLevel();
            TryUpdateModel(qualificationlevel); 
            Utils.Utils.TrimObject(qualificationlevel);
            qualificationlevel.IsActive = true;
            this.QualificationLevelBusiness.Insert(qualificationlevel);
            this.QualificationLevelBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int QualificationLevelID)
        {
            QualificationLevel qualificationlevel = this.QualificationLevelBusiness.Find(QualificationLevelID);
            TryUpdateModel(qualificationlevel);
            Utils.Utils.TrimObject(qualificationlevel);
            this.QualificationLevelBusiness.Update(qualificationlevel);
            this.QualificationLevelBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.QualificationLevelBusiness.Delete(id);
            this.QualificationLevelBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<QualificationLevelViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<QualificationLevel> query = this.QualificationLevelBusiness.Search(SearchInfo);
            IQueryable<QualificationLevelViewModel> lst = query.Select(o => new QualificationLevelViewModel {               
						QualificationLevelID = o.QualificationLevelID,								
						Resolution = o.Resolution,
						Description = o.Description,				
				
            });

            return lst.ToList();
        }        
    }
}





