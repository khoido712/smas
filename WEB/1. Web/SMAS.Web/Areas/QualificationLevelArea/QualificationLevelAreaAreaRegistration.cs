﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.QualificationLevelArea
{
    public class QualificationLevelAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "QualificationLevelArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "QualificationLevelArea_default",
                "QualificationLevelArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
