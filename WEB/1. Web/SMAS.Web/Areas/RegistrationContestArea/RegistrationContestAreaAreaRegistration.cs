﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.RegistrationContestArea
{
    public class RegistrationContestAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RegistrationContestArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RegistrationContestArea_default",
                "RegistrationContestArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
