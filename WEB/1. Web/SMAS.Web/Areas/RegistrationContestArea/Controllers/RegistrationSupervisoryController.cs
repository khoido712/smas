﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.RegistrationContestArea;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using SMAS.Web.Areas.DOETRegistrationContestArea.Models;
using SMAS.Web.Areas.DOETRegistrationContestArea;
using SMAS.Web.Areas.RegistrationContestArea.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
namespace SMAS.Web.Areas.RegistrationContestArea.Controllers
{
    public class RegistrationSupervisoryController : BaseController
    {
        #region Properties
        private readonly IDOETExamGroupBusiness DOETExamGroupBusiness;
        private readonly IDOETExaminationsBusiness DOETExaminationsBusiness;
        private readonly IDOETExamPupilBusiness DOETExamPupilBusiness;
        private readonly IDOETExamSubjectBusiness DOETExamSubjectBusiness;
        private readonly IDOETSubjectBusiness DOETSubjectBusiness;
        private readonly IDOETExamSupervisoryBusiness DOETExamSupervisoryBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private List<DOETExaminations> listExaminations;
        int totalRecord = 0;
        #endregion

        #region Constructor
        public RegistrationSupervisoryController(IDOETExamGroupBusiness examGroupBusiness, IDOETExaminationsBusiness examinationsBusiness,
            IDOETExamPupilBusiness DOETExamPupilBusiness, IDOETExamSubjectBusiness DOETExamSubjectBusiness, IDOETSubjectBusiness DOETSubjectBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IClassProfileBusiness ClassProfileBusiness, IPupilOfClassBusiness PupilOfClassBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness,
            IDOETExamSupervisoryBusiness _DOETExamSupervisoryBusiness,
            IEmployeeBusiness EmployeeBusiness)
        {
            this.DOETExamGroupBusiness = examGroupBusiness;
            this.DOETExaminationsBusiness = examinationsBusiness;
            this.DOETExamPupilBusiness = DOETExamPupilBusiness;
            this.DOETExamSubjectBusiness = DOETExamSubjectBusiness;
            this.DOETSubjectBusiness = DOETSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.DOETExamSupervisoryBusiness = _DOETExamSupervisoryBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            listExaminations = new List<DOETExaminations>();
        }
        #endregion

        public ActionResult Index()
        {
            int provineId = _globalInfo.ProvinceID ?? 0;
            ViewData[RegistrationSupervisoryConstant.SYNC_HCM_VISIBLE] = provineId == GlobalConstants.ProvinceID_HCM ? true : false;
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            // int unitId = _globalInfo.SupervisingDeptID.HasValue ? _globalInfo.SupervisingDeptID.Value : 0;

            //Neu ky thi o 
            if (provineId == GlobalConstants.ProvinceID_HCM)
            {
                SchoolProfile school = SchoolProfileBusiness.Find(aca.SchoolID);
                string schoolCode = string.IsNullOrEmpty(school.SyncCode) ? school.SchoolCode : school.SyncCode;
                int appliedLevel = UtilsBusiness.GetAppliedHCM(school.EducationGrade);
                var listKyThi = DoetHcmApi.GetKiThiTheoTruong(aca.Year, schoolCode, appliedLevel);

                listExaminations = new List<DOETExaminations>();

                if (listKyThi != null && listKyThi.Count > 0)
                {
                    foreach (KyThi item in listKyThi)
                    {
                        DOETExaminations oETExaminations = new DOETExaminations
                        {
                            AppliedType = 0,
                            CreateTime = item.HanChotDangKy,
                            DeadLine = item.HanChotDangKy,
                            ExaminationsID = item.KiThiID,
                            ExaminationsName = item.TenKiThi,
                        };

                        listExaminations.Add(oETExaminations);
                    }
                    listExaminations = listExaminations.ToList();
                }


                ViewData[RegistrationSupervisoryConstant.CBO_EXAMINATIONS] = listExaminations;
            }
            else
            {


                List<int> lisUnitId = this.GetUnitIDOfSchool();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                //dic.Add("UnitID", unitId);
                dic.Add("LisUnitID", lisUnitId);
                dic.Add("Year", aca.Year);
                dic.Add("Grade", _globalInfo.AppliedLevel);
                //Lấy danh sách kỳ thi
                listExaminations = DOETExaminationsBusiness.Search(dic).OrderByDescending(s => s.DeadLine).ToList();
                ViewData[RegistrationSupervisoryConstant.CBO_EXAMINATIONS] = listExaminations;
            }
            //Nhom thi
            int examIdFirst = listExaminations.FirstOrDefault() != null ? listExaminations.FirstOrDefault().ExaminationsID : 0;

            List<DOETSupervisoryType> listEmployeeType = GetEmployeeType();

            int firstEmployeeTypeId = listEmployeeType.FirstOrDefault().DOETSupervisoryTypeId;
            ViewData[RegistrationSupervisoryConstant.LIST_EXAM_EMPLOYEE_TYPE] = listEmployeeType;
            ViewData[RegistrationSupervisoryConstant.CBO_EMP_TYPE] = listEmployeeType;

            List<DOETExamSupervisoryBO> listResult = this.GetDataEmployeeRegised(examIdFirst, firstEmployeeTypeId);

            //viethd4: Lay loai ky thi
            int? appliedType = (listExaminations.Count > 0) ? (int?)listExaminations.FirstOrDefault().AppliedType : null;

            ViewData[RegistrationSupervisoryConstant.LIST_EXAM_EMPLOYEE] = listResult;
            ViewData[RegistrationSupervisoryConstant.TOTAL] = totalRecord;
            ViewData[RegistrationSupervisoryConstant.EXAMINATIONS_ID] = examIdFirst;
            ViewData[RegistrationSupervisoryConstant.EXAM_EMPLOYEE_TYPE_ID] = firstEmployeeTypeId;
            if (aca.SchoolProfile.ProvinceID != GlobalConstants.ProvinceID_HCM)
            {
                ViewData[RegistrationSupervisoryConstant.CHECK_EXAMINATIONS] = this.CheckDateExaminations(examIdFirst);
            }
            else
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                string SchoolCode = !string.IsNullOrEmpty(objSP.SyncCode) ? objSP.SyncCode : objSP.SchoolCode;
                int appliedLevelID = UtilsBusiness.GetAppliedHCM(objSP.EducationGrade);
                List<KyThi> lstExmaninations = DoetHcmApi.GetKiThiTheoTruong(aca.Year, SchoolCode, appliedLevelID);
                KyThi objExaminations = null;
                List<MonThi> lstExamSubject = new List<MonThi>();
                if (lstExmaninations.Count > 0)
                {
                    objExaminations = lstExmaninations.Where(p => p.KiThiID == examIdFirst).FirstOrDefault();
                }

                DateTime dateTimeNow = DateTime.Now;

                bool islock = objExaminations != null ? (objExaminations.DaKhoa == 1 ? true : false) : false;
                bool isEndDateRegis = objExaminations != null ? (objExaminations.HanChotDangKy.Date < dateTimeNow.Date ? true : false) : false;

                ViewData[RegistrationContestConstant.CHECK_EXAMINATIONS] = (islock || isEndDateRegis) ? true : false;
            }
            ViewData[RegistrationSupervisoryConstant.APPLIED_TYPE] = appliedType;
            bool isProvinceHcm = (_globalInfo.ProvinceID == GlobalConstants.ProvinceID_HCM) ? true : false;
            ViewData[RegistrationSupervisoryConstant.IS_PROVINCE_HCM] = isProvinceHcm;

            return View();
        }


        //
        public PartialViewResult Search(FormCollection fr)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int examinationId = fr["examinations"] != null ? Int32.Parse(fr["examinations"]) : 0;
            int examEmployeeTypeId = fr["employeeType"] != null ? Int32.Parse(fr["employeeType"]) : 0;

            List<DOETExamSupervisoryBO> listResult = new List<DOETExamSupervisoryBO>();

            if (examinationId > 0 && examEmployeeTypeId > 0)
            {
                listResult = this.GetDataEmployeeRegised(examinationId, examEmployeeTypeId);
            }

            //Viethd4: lay loai ky thi
            DOETExaminations exam = DOETExaminationsBusiness.Find(examinationId);
            int? appliedType = exam != null ? (int?)exam.AppliedType : null;

            ViewData[RegistrationSupervisoryConstant.LIST_EXAM_EMPLOYEE] = listResult;
            ViewData[RegistrationSupervisoryConstant.TOTAL] = totalRecord;
            ViewData[RegistrationSupervisoryConstant.EXAMINATIONS_ID] = examinationId;
            ViewData[RegistrationSupervisoryConstant.EXAM_EMPLOYEE_TYPE_ID] = examEmployeeTypeId;

            if (aca.SchoolProfile.ProvinceID != GlobalConstants.ProvinceID_HCM)
            {
                ViewData[RegistrationSupervisoryConstant.CHECK_EXAMINATIONS] = this.CheckDateExaminations(examinationId);
            }
            else
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                string SchoolCode = !string.IsNullOrEmpty(objSP.SyncCode) ? objSP.SyncCode : objSP.SchoolCode;
                int appliedLevelID = UtilsBusiness.GetAppliedHCM(objSP.EducationGrade);
                List<KyThi> lstExmaninations = DoetHcmApi.GetKiThiTheoTruong(aca.Year, SchoolCode, appliedLevelID);
                KyThi objExaminations = null;
                List<MonThi> lstExamSubject = new List<MonThi>();
                if (lstExmaninations.Count > 0)
                {
                    objExaminations = lstExmaninations.Where(p => p.KiThiID == examinationId).FirstOrDefault();
                }

                DateTime dateTimeNow = DateTime.Now;

                bool islock = objExaminations != null ? (objExaminations.DaKhoa == 1 ? true : false) : false;
                bool isEndDateRegis = objExaminations != null ? (objExaminations.HanChotDangKy.Date <= dateTimeNow.Date ? true : false) : false;

                ViewData[RegistrationContestConstant.CHECK_EXAMINATIONS] = (islock || isEndDateRegis) ? true : false;
            }

            ViewData[RegistrationSupervisoryConstant.APPLIED_TYPE] = appliedType;
            bool isProvinceHcm = (_globalInfo.ProvinceID == GlobalConstants.ProvinceID_HCM) ? true : false;
            ViewData[RegistrationSupervisoryConstant.IS_PROVINCE_HCM] = isProvinceHcm;

            return PartialView("_List");
        }

        [HttpPost]
        //        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchExamPupilAjax(FormCollection fr, GridCommand command)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<DOETExamSupervisoryBO> listResult = new List<DOETExamSupervisoryBO>();
            int examinationId = fr["examinations"] != null ? Int32.Parse(fr["examinations"]) : 0;
            int employeeTypeId = fr["employeeTypeId"] != null ? Int32.Parse(fr["employeeTypeId"]) : 0;
            if (examinationId > 0)
            {
                listResult = this.GetDataEmployeeRegised(examinationId, employeeTypeId);
            }

            return View(new GridModel<DOETExamSupervisoryBO>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        private List<DOETExamSupervisoryBO> GetDataEmployeeRegised(int examinationId, int employeeTypeId)
        {
            AcademicYear acaObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            SchoolProfile school = SchoolProfileBusiness.Find(acaObj.SchoolID);
            string schoolCode = string.IsNullOrEmpty(school.SyncCode) ? school.SchoolCode : school.SyncCode;

            //Neu la HCM thi lay danh sach tren so
            if (school.ProvinceID == GlobalConstants.ProvinceID_HCM)
            {

                List<DOETExamSupervisoryBO> lstVal = new List<DOETExamSupervisoryBO>();//DOETExamSupervisoryBusiness.GetExamEmpoyee(examinationId, acaObj.SchoolID, acaObj.AcademicYearID, employeeTypeId);


                List<DOETSupervisoryType> lstEmptype = GetEmployeeType();

                var lstCanBo = DoetHcmApi.GetCanBo(examinationId, schoolCode, employeeTypeId);
                if (lstCanBo == null || lstCanBo.Count == 0)
                {
                    return new List<DOETExamSupervisoryBO>();
                }
                foreach (CanBo item in lstCanBo)
                {
                    DOETSupervisoryType dOETSupervisoryType = lstEmptype.FirstOrDefault(s => s.DOETSupervisoryTypeId == employeeTypeId);
                    DOETExamSupervisoryBO dOETExamSupervisoryBO = new DOETExamSupervisoryBO
                    {
                        AcademicYearID = acaObj.AcademicYearID,
                        BirthDay = string.IsNullOrEmpty(item.NgaySinh) ? DateTime.Now : Convert.ToDateTime(item.NgaySinh),
                        CreateTime = DateTime.Now,
                        EmployeeCode = item.MaCanBo,
                        EthnicName = "",
                        EthnicId = 0,
                        ExamEmployeeStatus = "",
                        FullName = item.TenNguoiDung,
                        //Genre=item.GioiTinh,
                        GenreName = item.GioiTinh,
                        SchoolID = school.SchoolProfileID,
                        SupervisoryName = dOETSupervisoryType == null ? "" : dOETSupervisoryType.DOETSupervisoryName,
                        SupervisoryType = employeeTypeId,
                        TeacherID = item.NguoiDungID,
                        TrainingLevelID = 0,
                        TrainingLevelName = "",
                        ExamSupervisoryID = item.NguoiDungID,

                    };
                    lstVal.Add(dOETExamSupervisoryBO);
                }
                return lstVal;


            }
            else
            {

                List<DOETExamSupervisoryBO> lstVal = DOETExamSupervisoryBusiness.GetExamEmpoyee(examinationId, acaObj.SchoolID, acaObj.AcademicYearID, employeeTypeId);
                if (lstVal == null || lstVal.Count == 0)
                {
                    return new List<DOETExamSupervisoryBO>();
                }

                List<DOETSupervisoryType> lstEmptype = GetEmployeeType();
                for (int i = 0; i < lstVal.Count; i++)
                {
                    lstVal[i].GenreName = (lstVal[i].Genre) ? "Nam" : "Nữ";
                    lstVal[i].ExamEmployeeStatus = (lstVal[i].SyncTime.HasValue) ? "Đã gửi" : "";
                    lstVal[i].ExamEmployeeStatus = (lstVal[i].SyncTime.HasValue) ? "Đã gửi" : "";
                    lstVal[i].BirthDay = (lstVal[i].BirthDay.HasValue) ? lstVal[i].BirthDay.Value : DateTime.Now;
                    DOETSupervisoryType oETSupervisoryType = lstEmptype.FirstOrDefault(s => s.DOETSupervisoryTypeId == lstVal[i].SupervisoryType);
                    lstVal[i].SupervisoryName = (oETSupervisoryType == null) ? "" : oETSupervisoryType.DOETSupervisoryName;
                }
                return lstVal;
            }




        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Delete(string arrayID, int? examinationsId, int? employeeType)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //Neu chua chon ky thi
            if (examinationsId == null)
            {
                throw new BusinessException("ExamPupil_Validate_Create_Examinations");
            }

            //Neu chua chon loai giam thi
            if (employeeType == null)
            {
                throw new BusinessException("Thầy/cô chưa chọn loại cán bộ");
            }
            if (aca.SchoolProfile.ProvinceID != GlobalConstants.ProvinceID_HCM)
            {
                string[] examEmpIDArr;
                if (arrayID != "")
                {
                    arrayID = arrayID.Remove(arrayID.Length - 1);
                    examEmpIDArr = arrayID.Split(',');
                }
                else
                {
                    examEmpIDArr = new string[] { };
                }


                List<int> listExamTeacherId = examEmpIDArr.Length > 0 ? examEmpIDArr.ToList().Distinct().Select(o => Int32.Parse(o)).ToList() :
                                                new List<int>();
                int examId = examinationsId ?? 0;
                int empType = employeeType ?? 0;
                int academicYearId = _globalInfo.AcademicYearID.GetValueOrDefault();
                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);

                string schoolCode = string.IsNullOrEmpty(school.SyncCode) ? school.SchoolCode : school.SyncCode;


                List<DOETExamSupervisory> lstExamSupervisory = DOETExamSupervisoryBusiness.All.Where(s => s.ExaminationsID == examinationsId && s.SupervisoryType == empType && s.AcademicYearID == academicYearId && listExamTeacherId.Contains(s.ExamSupervisoryID)).ToList();

                if (lstExamSupervisory != null && lstExamSupervisory.Count > 0)
                {
                    DOETExamSupervisoryBusiness.DeleteAll(lstExamSupervisory);
                    DOETExamSupervisoryBusiness.Save();
                }


                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
            }
            else
            {
                string[] examEmpIDArr;
                if (arrayID != "")
                {
                    arrayID = arrayID.Remove(arrayID.Length - 1);
                    examEmpIDArr = arrayID.Split(',');
                }
                else
                {
                    examEmpIDArr = new string[] { };
                }

                string param = String.Join(",", examEmpIDArr);

                string ret = DoetHcmApi.XoaCanbo2(examEmpIDArr.ToList());
                if (!ret.Contains("ResponseExceptionMessage"))
                {
                    return Json(new JsonMessage(Res.Get("Xóa giám thị thành công")));
                }
                else
                {
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    ResponseExceptionMessage ex = (ResponseExceptionMessage)json_serializer.DeserializeObject(ret);
                    throw new BusinessException(ex.Message);
                }
            }
        }

        //
        // GET: /ExamPupil/Create/
        public ActionResult PrepareCreate(int? examinationsID, int? employeeType)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (!examinationsID.HasValue)
            {
                return PartialView("_DialogCreate");
            }

            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            if (aca.SchoolProfile.ProvinceID != GlobalConstants.ProvinceID_HCM)
            {
                //Danh sach ky thi
                
                //int unitId = _globalInfo.SupervisingDeptID.HasValue ? _globalInfo.SupervisingDeptID.Value : 0;
                Dictionary<string, object> dic = new Dictionary<string, object>();
                List<int> lisUnitId = this.GetUnitIDOfSchool();
                dic.Add("LisUnitID", lisUnitId);
                //dic.Add("UnitID", unitId);
                dic.Add("Year", aca.Year);
                //Lấy danh sách kỳ thi
                listExaminations = DOETExaminationsBusiness.Search(dic).OrderByDescending(s => s.DeadLine).ToList();
            }
            else
            {
                 SchoolProfile school = SchoolProfileBusiness.Find(aca.SchoolID);
                string schoolCode = string.IsNullOrEmpty(school.SyncCode) ? school.SchoolCode : school.SyncCode;
                int appliedLevel = UtilsBusiness.GetAppliedHCM(school.EducationGrade);
                var listKyThi = DoetHcmApi.GetKiThiTheoTruong(aca.Year, schoolCode, appliedLevel);

                listExaminations = new List<DOETExaminations>();

                if (listKyThi != null && listKyThi.Count > 0)
                {
                    foreach (KyThi item in listKyThi)
                    {
                        DOETExaminations oETExaminations = new DOETExaminations
                        {
                            AppliedType = 0,
                            CreateTime = item.HanChotDangKy,
                            DeadLine = item.HanChotDangKy,
                            ExaminationsID = item.KiThiID,
                            ExaminationsName = item.TenKiThi,
                        };

                        listExaminations.Add(oETExaminations);
                    }
                    listExaminations = listExaminations.ToList();
                }
            }

            ViewData[RegistrationSupervisoryConstant.CBO_EXAMINATIONS] = listExaminations;
            List<DOETSupervisoryType> listEmployeeType = GetEmployeeType();

            int firstEmployeeTypeId = listEmployeeType.FirstOrDefault().DOETSupervisoryTypeId;


            ViewData[RegistrationSupervisoryConstant.CBO_EMP_TYPE] = listEmployeeType;



            //Lay nhom thi mac dinh
            int defaultEmployeeType = 0;
            if (employeeType != null)
            {
                defaultEmployeeType = employeeType.Value;
            }
            else
            {
                defaultEmployeeType = firstEmployeeTypeId;
            }

            //Lay khoi mac dinh
            List<DOETExamSupervisoryBO> listResult = _SearchForCreate(examinationsID, defaultEmployeeType);

            int totalRecord = listResult.Count();
            ViewData[RegistrationSupervisoryConstant.TOTAL] = totalRecord;

            ViewData[RegistrationSupervisoryConstant.CREATE_LIST_EMPPLOYEE] = listResult;
            ViewData[RegistrationSupervisoryConstant.EXAMINATIONS_ID] = examinationsID;
            ViewData[RegistrationSupervisoryConstant.EXAM_EMPLOYEE_TYPE_ID] = defaultEmployeeType;
            return PartialView("_DialogCreate");
        }

        [HttpPost]
        public ActionResult SearchPupilCreateAjax(FormCollection fr)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<DOETExamSupervisoryBO> listResult = new List<DOETExamSupervisoryBO>();
            int examinationId = fr["CsExaminationsID"] != null && !string.IsNullOrEmpty(fr["CsExaminationsID"]) ? Int32.Parse(fr["CsExaminationsID"]) : 0;
            int employeeType = fr["cCsEmployeeType"] != null ? Int32.Parse(fr["cCsEmployeeType"]) : 0;

            listResult = _SearchForCreate(examinationId, employeeType);
            ViewData[RegistrationSupervisoryConstant.CREATE_LIST_EMPPLOYEE] = listResult;
            return PartialView("_CreateList");



        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(string arrayID, int? examinationsId, int? employeeType, int isGetAll)
        {

            //Kiem tra du lieu hop le
            //Neu chua chon ky thi
            if (examinationsId == null)
            {
                throw new BusinessException("ExamPupil_Validate_Create_Examinations");
            }

            //Neu chua chon loai giam thi
            if (employeeType == null)
            {
                throw new BusinessException("Thầy/cô chưa chọn loại cán bộ");
            }

            string[] empIDArr;
            if (arrayID != "")
            {
                arrayID = arrayID.Remove(arrayID.Length - 1);
                empIDArr = arrayID.Split(new[] { ',', '\"' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                empIDArr = new string[] { };
            }

            if (empIDArr.Length == 0 && isGetAll == 0)
            {
                throw new BusinessException("Thầy/cô chưa chọn loại cán bộ");
            }

            List<DOETExamSupervisoryBO> lstEmp = _SearchForCreate(examinationsId, employeeType ?? 0);

            List<int> listEmpID = new List<int>();
            if (isGetAll == 0) //Khong check chon
            {
                listEmpID = empIDArr.ToList().Select(o => int.Parse(o)).Distinct().ToList();
                lstEmp = lstEmp.Where(o => listEmpID.Any(u => u == o.TeacherID)).ToList();
            }
            //Neu khong tich chon Lay tat ca, chi loc lai cac hoc sinh duoc tich chon
            if (isGetAll == 1)
            {
                listEmpID = lstEmp.Select(o => o.TeacherID).ToList();
            }

            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            if (aca.SchoolProfile.ProvinceID == GlobalConstants.ProvinceID_HCM)
            {
                List<CanBo> lstExamSupervisoryInsert = new List<CanBo>();
                string strEthnic = DoetHcmApi.GetDanToc();
                var rootEthnic = Newtonsoft.Json.JsonConvert.DeserializeObject<RootEthnic>(strEthnic);
                List<EthnicResponse> lstEthnic = rootEthnic.Table.ToList();
                foreach (DOETExamSupervisoryBO doetSupervisory in lstEmp)
                {
                    CanBo oETExamSupervisory = new CanBo
                    {
                        MaCanBo = "",
                        NguoiDungID = doetSupervisory.TeacherID,
                        TenNguoiDung = doetSupervisory.FullName,
                        NgaySinh = doetSupervisory.BirthDay.HasValue ? doetSupervisory.BirthDay.Value.ToString("dd/MM/yyyy") : string.Empty,
                        GioiTinh = doetSupervisory.Genre ? GlobalConstants.GENRE_MALE_TITLE : GlobalConstants.GENRE_FEMALE_TITLE,
                        DanTocID = GetEthnicHCM(doetSupervisory.EthnicId.GetValueOrDefault(), lstEthnic),
                        SDT = doetSupervisory.Mobile,
                        DSChuyenMon = "",
                        DSKhoi = "",
                        TrinhDoDaoTaoID = 0,
                    };

                    lstExamSupervisoryInsert.Add(oETExamSupervisory);

                }

                if (lstExamSupervisoryInsert.Count > 0)
                {
                    string json = JsonConvert.SerializeObject(lstExamSupervisoryInsert);
                    string schoolCode = string.IsNullOrEmpty(aca.SchoolProfile.SyncCode) ? aca.SchoolProfile.SchoolCode : aca.SchoolProfile.SyncCode;
                    int appliedLevel = UtilsBusiness.GetAppliedHCM(aca.SchoolProfile.EducationGrade);

                    string ret = DoetHcmApi.NopCanBo2(examinationsId.Value, schoolCode, employeeType.Value, json);

                    if (!ret.Contains("ResponseExceptionMessage"))
                    {
                        return Json(new JsonMessage(Res.Get("Tạo giám thị thành công")));
                    }
                    else
                    {
                        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                        ResponseExceptionMessage ex = (ResponseExceptionMessage)json_serializer.DeserializeObject(ret);
                        throw new BusinessException(ex.Message);
                    }
                }
            }
            else
            {
                //Tao cac entity de insert
                int supervisingDeptId = this.GetUnitID();
                List<DOETExamSupervisory> lstExamSupervisoryInsert = new List<DOETExamSupervisory>();
                foreach (DOETExamSupervisoryBO doetSupervisory in lstEmp)
                {
                    DOETExamSupervisory oETExamSupervisory = new DOETExamSupervisory
                    {
                        AcademicYearID = aca.AcademicYearID,
                        ExamGroupID = 0,
                        ExaminationsID = examinationsId ?? 0,
                        MSourcedb = "",
                        SchoolID = aca.SchoolID,
                        SupervisoryType = employeeType ?? 0,
                        SyncSourceId = "",
                        SyncTime = null,
                        TeacherID = doetSupervisory.TeacherID,
                        UpdateTime = null,
                        CreateTime = DateTime.Now,
                    };
                    lstExamSupervisoryInsert.Add(oETExamSupervisory);
                }

                if (lstExamSupervisoryInsert.Count > 0)
                {
                    foreach (DOETExamSupervisory item in lstExamSupervisoryInsert)
                    {
                        DOETExamSupervisoryBusiness.Insert(item);
                    }
                    DOETExamSupervisoryBusiness.Save();


                }
            }

            return Json(new JsonMessage(Res.Get("Tạo giám thị thành công")));
        }


        public bool CheckDateExaminations(int examinationsId)
        {
            DOETExaminations examObj = DOETExaminationsBusiness.All.Where(p => p.ExaminationsID == examinationsId).FirstOrDefault();
            if (examObj != null)
            {
                DateTime dateDeadline = examObj.DeadLine;
                DateTime dateNow = DateTime.Now;
                //Neu thoi gian dang ky nho thoi gian hien tai thi het han dang ky
                if (dateDeadline.Date < dateNow.Date)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        #region Private methods

        private List<DOETExamGroupModel> _Search(long examinationID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ExaminationsID", examinationID);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            List<DOETExamGroupModel> ret = DOETExamGroupBusiness.Search(dic).ToList().
                Select(o => new DOETExamGroupModel
                {
                    ExamGroupID = o.ExamGroupID,
                    ExamGroupName = o.ExamGroupName
                }).ToList();
            return ret;
        }


        /// <summary>
        /// Ham search du lieu cho popup them moi
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private List<DOETExamSupervisoryBO> _SearchForCreate(int? examinationsID, int employeeType)
        {
            if (!examinationsID.HasValue)
            {
                return new List<DOETExamSupervisoryBO>();
            }
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int schoolId = _globalInfo.SchoolID.Value;
            List<int> lstId;

            if (aca.SchoolProfile.ProvinceID != GlobalConstants.ProvinceID_HCM )
            {
                List<DOETExamSupervisory> lstEmpRegis = DOETExamSupervisoryBusiness.All.Where(s => s.ExaminationsID == examinationsID.Value && s.SupervisoryType == employeeType && s.AcademicYearID == academicYearId && s.SchoolID == schoolId).ToList();
                lstId = lstEmpRegis.Select(o => o.TeacherID).ToList();
            }
            else
            {
                List<DOETExamSupervisoryBO> lstEmpRegis = GetDataEmployeeRegised(examinationsID.Value, employeeType);
                lstId = lstEmpRegis.Select(o => o.TeacherID).ToList();
            }

            var lstVal = DOETExamSupervisoryBusiness.GetEmployeeNotRegis(examinationsID.Value, employeeType, schoolId, academicYearId, lstId);
            if (lstVal != null && lstVal.Count > 0)
            {
                for (int i = 0; i < lstVal.Count; i++)
                {
                    lstVal[i].GenreName = (lstVal[i].Genre) ? "Nam" : "Nữ";
                    lstVal[i].ExamEmployeeStatus = (lstVal[i].SyncTime.HasValue) ? "Đã gửi" : "";
                    //lstVal[i].ExamEmployeeStatus = (lstVal[i].SyncTime.HasValue) ? "Đã gửi" : "";
                    lstVal[i].BirthDay = (lstVal[i].BirthDay.HasValue) ? lstVal[i].BirthDay.Value : DateTime.Now;
                }
            }
            return lstVal;
        }

        private int GetUnitID()
        {
            int superID = 0;
            if (_globalInfo.IsAdminSchoolRole)
            {
                superID = _globalInfo.SupervisingDeptID.Value;
            }
            else
            {
                //Neu truong thuoc so ma cap thuc hien dang la quan ly cua phong thi lay thong tin phong/GD
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY ||
                    _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    SchoolProfile objSchool = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                    superID = SchoolProfileBusiness.Find(_globalInfo.SchoolID).SupervisingDeptID.Value;
                    SupervisingDept dep2 = SupervisingDeptBusiness.All.FirstOrDefault(s =>
                        s.IsActive && s.ParentID == superID && s.DistrictID == objSchool.DistrictID);
                }
                else
                {
                    SchoolProfile objSchool = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                    superID = objSchool.SupervisingDeptID.Value;
                }


            }
            return superID;
        }

        private List<int> GetUnitIDOfSchool()
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            int appliedLevel = _globalInfo.AppliedLevel ?? 0;
            List<int> listSupervisingDeptId = new List<int>();
            int unitId = GetUnitID();
            SupervisingDept dept = SupervisingDeptBusiness.Find(unitId);
            //Neu truong truoc thuoc phong
            if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                //Phong va cac phong ban truc thuoc phong
                listSupervisingDeptId.Add(unitId);
                // listSupervisingDeptId.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == unitId
                //                                            && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT).Select(o => o.SupervisingDeptID).ToList());

                //So va cac phong ban truc thuoc so
                listSupervisingDeptId.Add(dept.ParentID.HasValue ? dept.ParentID.Value : 0);
                //listSupervisingDeptId.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == dept.ParentID
                //                                        && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o => o.SupervisingDeptID).ToList());
            }
            //Don vi quan ly cua truong la so
            else if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                if (appliedLevel != GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    SchoolProfile objSchool = SchoolProfileBusiness.Find(schoolId);
                    int provinceId = objSchool.ProvinceID ?? 0;
                    int districtId = objSchool.DistrictID ?? 0;
                    //Lay unitid phong giao duc
                    SupervisingDept objSupervisingDept =
                        SupervisingDeptBusiness.All.FirstOrDefault(s =>
                            s.ProvinceID == provinceId && s.DistrictID == districtId
                            && s.IsActive
                            && s.HierachyLevel == SMAS.Business.Common.SystemParamsInFile
                                .EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE);

                    listSupervisingDeptId.Add(objSupervisingDept.SupervisingDeptID);
                }
                else
                {
                    listSupervisingDeptId.Add(unitId);
                }
                //Tin cua so va cac phong ban truc thuoc so

                //listSupervisingDeptId.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDeptID
                //                                        && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o =>o.SupervisingDeptID).ToList());
            }
            return listSupervisingDeptId;
        }

        private int GetEthnicHCM(int ethnicID, List<EthnicResponse> lstEthnic)
        {
            EthnicResponse ethnicBO = lstEthnic.FirstOrDefault(s => s.DanTocID == ethnicID);
            MapSource<int> mapSourceEthnic = new MapSource<int>();
            if (ethnicBO == null)
            {
                mapSourceEthnic.SourceId = 1;
            }
            else
            {
                mapSourceEthnic.SourceId = ethnicBO.DanTocID;
            }

            return mapSourceEthnic.SourceId;
        }

        private List<DOETSupervisoryType> GetEmployeeType()
        {
            List<DOETSupervisoryType> list = new List<DOETSupervisoryType>();
            list.Add(new DOETSupervisoryType { DOETSupervisoryTypeId = 1, DOETSupervisoryName = "Giám thị" });
            list.Add(new DOETSupervisoryType { DOETSupervisoryTypeId = 2, DOETSupervisoryName = "Giám khảo" });
            list.Add(new DOETSupervisoryType { DOETSupervisoryTypeId = 3, DOETSupervisoryName = "Cán bộ phục vụ hội đồng" });
            return list;

        }
        #endregion


    }
}
