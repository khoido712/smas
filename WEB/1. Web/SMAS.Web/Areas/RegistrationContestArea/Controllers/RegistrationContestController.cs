﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.RegistrationContestArea;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using SMAS.Web.Areas.DOETRegistrationContestArea.Models;
using SMAS.Web.Areas.DOETRegistrationContestArea;
using SMAS.Web.Areas.RegistrationContestArea.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;
namespace SMAS.Web.Areas.RegistrationContestArea.Controllers
{
    public class RegistrationContestController : BaseController
    {
        #region Properties
        private readonly IDOETExamGroupBusiness DOETExamGroupBusiness;
        private readonly IDOETExaminationsBusiness DOETExaminationsBusiness;
        private readonly IDOETExamPupilBusiness DOETExamPupilBusiness;
        private readonly IDOETExamSubjectBusiness DOETExamSubjectBusiness;
        private readonly IDOETSubjectBusiness DOETSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private List<DOETExaminations> listExaminations;
        int totalRecord = 0;
        #endregion

        #region Constructor
        public RegistrationContestController(IDOETExamGroupBusiness examGroupBusiness, IDOETExaminationsBusiness examinationsBusiness,
            IDOETExamPupilBusiness DOETExamPupilBusiness, IDOETExamSubjectBusiness DOETExamSubjectBusiness, IDOETSubjectBusiness DOETSubjectBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IClassProfileBusiness ClassProfileBusiness, IPupilOfClassBusiness PupilOfClassBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.DOETExamGroupBusiness = examGroupBusiness;
            this.DOETExaminationsBusiness = examinationsBusiness;
            this.DOETExamPupilBusiness = DOETExamPupilBusiness;
            this.DOETExamSubjectBusiness = DOETExamSubjectBusiness;
            this.DOETSubjectBusiness = DOETSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            listExaminations = new List<DOETExaminations>();
        }
        #endregion

        public ActionResult Index()
        {
            int provineId = _globalInfo.ProvinceID ?? 0;
            bool isHCM = provineId == GlobalConstants.ProvinceID_HCM ? true : false;
            ViewData[RegistrationContestConstant.SYNC_HCM_VISIBLE] = isHCM;
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            if (isHCM)//xu ly cho HCM
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                string SchoolCode = !string.IsNullOrEmpty(objSP.SyncCode) ? objSP.SyncCode : objSP.SchoolCode;
                int appliedLevelID = UtilsBusiness.GetAppliedHCM(objSP.EducationGrade);
                List<KyThi> lstExmaninations = DoetHcmApi.GetKiThiTheoTruong(aca.Year, SchoolCode, appliedLevelID);
                KyThi objExaminations = null;
                List<MonThi> lstExamSubject = new List<MonThi>();
                if (lstExmaninations.Count > 0)
                {
                    objExaminations = lstExmaninations.FirstOrDefault();
                }
                if (objExaminations != null)
                {
                    lstExamSubject = this.GetlistExamSubject(objExaminations.KiThiID);
                    ViewData[RegistrationContestConstant.IS_LOCK] = objExaminations.TrangThai;
                }
                ViewData[RegistrationContestConstant.CBO_EXAMINATIONS] = lstExmaninations;
                ViewData[RegistrationContestConstant.LIST_EXAM_SUBJECT] = lstExamSubject;


                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID ?? 0);
                string schoolCode = string.IsNullOrEmpty(school.SyncCode) ? school.SchoolCode : school.SyncCode;
                string urlPrint = DoetHcmApi.GenerateURL(schoolCode);
                ViewData[RegistrationContestConstant.PRINT_CARD_HCM] = urlPrint;
            }
            else
            {
                List<int> lisUnitId = this.GetUnitIDOfSchool();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                //dic.Add("UnitID", unitId);
                dic.Add("LisUnitID", lisUnitId);
                dic.Add("Year", aca.Year);
                dic.Add("Grade", _globalInfo.AppliedLevel);
                //Lấy danh sách kỳ thi
                listExaminations = DOETExaminationsBusiness.Search(dic).OrderByDescending(s => s.DeadLine).ToList();
                ViewData[RegistrationContestConstant.CBO_EXAMINATIONS] = listExaminations;

                //Nhom thi
                int examIdFirst = listExaminations.FirstOrDefault() != null ? listExaminations.FirstOrDefault().ExaminationsID : 0;
                List<DOETExamGroupModel> listExamGroup = new List<DOETExamGroupModel>();
                if (examIdFirst > 0)
                {
                    listExamGroup = _Search(examIdFirst).ToList();
                }
                ViewData[RegistrationContestConstant.LIST_EXAM_GROUP] = listExamGroup;

                //Load mon thi cua nhom dau tien
                int idGroupFirst = listExamGroup.Select(p => p.ExamGroupID).FirstOrDefault();
                //Cac mon thi
                ViewData[RegistrationContestConstant.ARR_SUBJECT_NAME] = this.GetArrSubjectNameByGroupId(examIdFirst, idGroupFirst);
                List<DOETSubjectModel> listSubject = new List<DOETSubjectModel>();
                if (idGroupFirst > 0)
                {
                    listSubject = _SearchSubject(examIdFirst, idGroupFirst);
                }
                ViewData[RegistrationContestConstant.LIST_EXAM_SUBJECT] = listSubject;
                int subjectId = 0;
                if (listSubject != null && listSubject.Count > 0)
                {
                    subjectId = listSubject.Select(p => p.SubjectID).FirstOrDefault();
                }
                List<DOETExamPupilBO> listResult = this.GetDataPupilRegised(examIdFirst, idGroupFirst, subjectId, 1, ref totalRecord).ToList();

                //viethd4: Lay loai ky thi
                int? appliedType = (listExaminations.Count > 0) ? (int?)listExaminations.FirstOrDefault().AppliedType : null;

                ViewData[RegistrationContestConstant.LIST_EXAM_PUPIL] = listResult;
                ViewData[RegistrationContestConstant.TOTAL] = totalRecord;
                ViewData[RegistrationContestConstant.EXAMINATIONS_ID] = examIdFirst;
                ViewData[RegistrationContestConstant.EXAM_GROUP_ID] = idGroupFirst;
                ViewData[RegistrationContestConstant.EXAM_SUBJECT_ID] = subjectId;
                ViewData[RegistrationContestConstant.CHECK_EXAMINATIONS] = this.CheckDateExaminations(examIdFirst);
                ViewData[RegistrationContestConstant.APPLIED_TYPE] = appliedType;
                bool isProvinceHcm = (_globalInfo.ProvinceID == GlobalConstants.ProvinceID_HCM) ? true : false;
                ViewData[RegistrationContestConstant.IS_PROVINCE_HCM] = isProvinceHcm;
                string urlPrint = "";
                if (isProvinceHcm)
                {
                    SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID ?? 0);
                    string schoolCode = string.IsNullOrEmpty(school.SyncCode) ? school.SchoolCode : school.SyncCode;
                    urlPrint = DoetHcmApi.GenerateURL(schoolCode);
                }
                ViewData[RegistrationContestConstant.PRINT_CARD_HCM] = urlPrint;
            }
            return View();
        }

        //
        // GET: /ExamPupil/Search
        public PartialViewResult Search(FormCollection fr)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int examinationId = fr["examinations"] != null ? Int32.Parse(fr["examinations"]) : 0;
            int examGroupId = fr["examgroup"] != null ? Int32.Parse(fr["examgroup"]) : 0;
            int examSubjectId = fr["examsubject"] != null ? Int32.Parse(fr["examsubject"]) : 0;

            List<DOETExamPupilBO> listResult = new List<DOETExamPupilBO>();



            if (examinationId > 0 && examGroupId > 0)
            {
                listResult = this.GetDataPupilRegised(examinationId, examGroupId, examSubjectId, 1, ref totalRecord);
            }

            //Viethd4: lay loai ky thi
            DOETExaminations exam = DOETExaminationsBusiness.Find(examinationId);
            int? appliedType = exam != null ? (int?)exam.AppliedType : null;

            ViewData[RegistrationContestConstant.LIST_EXAM_PUPIL] = listResult;
            ViewData[RegistrationContestConstant.TOTAL] = totalRecord;
            ViewData[RegistrationContestConstant.EXAMINATIONS_ID] = examinationId;
            ViewData[RegistrationContestConstant.EXAM_GROUP_ID] = examGroupId;
            ViewData[RegistrationContestConstant.EXAM_SUBJECT_ID] = examSubjectId;
            ViewData[RegistrationContestConstant.CHECK_EXAMINATIONS] = this.CheckDateExaminations(examinationId);
            ViewData[RegistrationContestConstant.APPLIED_TYPE] = appliedType;
            bool isProvinceHcm = (_globalInfo.ProvinceID == GlobalConstants.ProvinceID_HCM) ? true : false;
            ViewData[RegistrationContestConstant.IS_PROVINCE_HCM] = isProvinceHcm;
            string urlPrint = "";
            if (isProvinceHcm)
            {
                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID ?? 0);
                string schoolCode = string.IsNullOrEmpty(school.SyncCode) ? school.SchoolCode : school.SyncCode;
                urlPrint = DoetHcmApi.GenerateURL(schoolCode);
                int appliedLevel = _globalInfo.AppliedLevel.Value;
            }
            ViewData[RegistrationContestConstant.PRINT_CARD_HCM] = urlPrint;
            return PartialView("_List");
        }

        public PartialViewResult SearchHCM(FormCollection frm)
        {
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID ?? 0);
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int examinationId = frm["examinations"] != null ? Int32.Parse(frm["examinations"]) : 0;
            int examSubjectId = frm["examsubject"] != null ? Int32.Parse(frm["examsubject"]) : 0;
            List<ExamPupilHCMViewModel> lstViewModel = new List<ExamPupilHCMViewModel>();
            ExamPupilHCMViewModel objViewModel = null;
            List<DanhSachThiSinh> lstPupilExam = DoetHcmApi.GetThiSinh(examinationId, school.SyncCode, examSubjectId);
            DanhSachThiSinh objPupilExam = null;

            for (int i = 0; i < lstPupilExam.Count; i++)
            {
                objPupilExam = lstPupilExam[i];
                objViewModel = new ExamPupilHCMViewModel();
                objViewModel.PupilID = objPupilExam.HocSinhLopID;
                objViewModel.PupilCode = objPupilExam.MaThiSinh;
                objViewModel.Status = objPupilExam.DaDuyet ? "Đã duyệt" : "Chưa duyệt";
                objViewModel.FullName = objPupilExam.Ho + " " + objPupilExam.Ten;
                objViewModel.Birthday = objPupilExam.NgaySinh;
                objViewModel.Genre = objPupilExam.Phai ? "Nữ" : "Nam";
                objViewModel.ClassName = objPupilExam.TenLop;
                objViewModel.ExamineeNumber = objPupilExam.SBD;
                objViewModel.Mark = objPupilExam.Diem;
                objViewModel.IsApproved = objPupilExam.DaDuyet;
                lstViewModel.Add(objViewModel);
            }

            ViewData[RegistrationContestConstant.LIST_EXAM_PUPIL] = lstViewModel;
            ViewData[RegistrationContestConstant.EXAMINATIONS_ID] = examinationId;
            ViewData[RegistrationContestConstant.EXAM_SUBJECT_ID] = examSubjectId;

            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string SchoolCode = !string.IsNullOrEmpty(objSP.SyncCode) ? objSP.SyncCode : objSP.SchoolCode;
            int appliedLevelID = UtilsBusiness.GetAppliedHCM(objSP.EducationGrade);
            List<KyThi> lstExmaninations = DoetHcmApi.GetKiThiTheoTruong(aca.Year, SchoolCode, appliedLevelID);
            KyThi objExaminations = null;
            List<MonThi> lstExamSubject = new List<MonThi>();
            if (lstExmaninations.Count > 0)
            {
                objExaminations = lstExmaninations.Where(p => p.KiThiID == examinationId).FirstOrDefault();
            }

            DateTime dateTimeNow = DateTime.Now;

            bool islock = objExaminations != null ? (objExaminations.DaKhoa == 1 ? true : false) : false;
            bool isEndDateRegis = objExaminations != null ? (objExaminations.HanChotDangKy.Date < dateTimeNow.Date ? true : false) : false;

            ViewData[RegistrationContestConstant.CHECK_EXAMINATIONS] = (islock || isEndDateRegis) ? true : false;
            bool isProvinceHcm = (_globalInfo.ProvinceID == GlobalConstants.ProvinceID_HCM) ? true : false;
            ViewData[RegistrationContestConstant.IS_PROVINCE_HCM] = isProvinceHcm;
            string schoolCode = string.IsNullOrEmpty(school.SyncCode) ? school.SchoolCode : school.SyncCode;
            ViewData[RegistrationContestConstant.PRINT_CARD_HCM] = DoetHcmApi.GenerateURL(schoolCode);
            return PartialView("_ListHCM");
        }
        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchExamPupilAjax(FormCollection fr, GridCommand command)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<DOETExamPupilBO> listResult = new List<DOETExamPupilBO>();
            int examinationId = fr["examinations"] != null ? Int32.Parse(fr["examinations"]) : 0;
            int examGroupId = fr["examgroup"] != null ? Int32.Parse(fr["examgroup"]) : 0;
            int examSubjectId = fr["examsubject"] != null ? Int32.Parse(fr["examsubject"]) : 0;
            if (examinationId > 0 && examGroupId > 0)
            {
                listResult = this.GetDataPupilRegised(examinationId, examGroupId, examSubjectId, command.Page, ref totalRecord);
            }
            //Kiem tra button
            //CheckCommandPermision(ExaminationsBusiness.Find(form.ExaminationsID), listResult);

            return View(new GridModel<DOETExamPupilBO>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        private List<DOETExamPupilBO> GetDataPupilRegised(int examinationId, int examGroupId, int examSubjectId, int pageNum, ref int totalRecord)
        {
            AcademicYear acaObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("YearID", acaObj.Year);
            dic.Add("ExaminationsID", examinationId);
            dic.Add("ExamGroupID", examGroupId);
            dic.Add("SubjectID", examSubjectId);
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("Grade", _globalInfo.AppliedLevel.Value);
            dic.Add("AcademicYearID", acaObj.AcademicYearID);
            IQueryable<DOETExamPupilBO> IQueryResult = DOETExamPupilBusiness.GetListDOETExamSubject(dic);
            totalRecord = IQueryResult.Count();
            List<DOETExamPupilBO> listResult = IQueryResult.Skip((pageNum - 1) * RegistrationContestConstant.PAGE_SIZE).Take(RegistrationContestConstant.PAGE_SIZE).ToList();
            if (listResult != null && listResult.Count > 0)
            {
                if (examSubjectId == 0)
                {
                    List<int> listPupilId = listResult.Select(p => p.PupilID).Distinct().ToList();
                    List<DOETExamPupilBO> listResultDistinct = new List<DOETExamPupilBO>();
                    DOETExamPupilBO pupilIObjResultTmp = null;
                    DOETExamPupilBO pupilIObjTmp = null;
                    int pupilId = 0;
                    for (int i = 0; i < listPupilId.Count; i++)
                    {
                        pupilId = listPupilId[i];
                        pupilIObjResultTmp = listResult.Where(p => p.PupilID == pupilId).FirstOrDefault();
                        pupilIObjTmp = new DOETExamPupilBO();
                        pupilIObjTmp.ExamPupilID = pupilIObjResultTmp.ExamPupilID;
                        pupilIObjTmp.ExaminationsID = pupilIObjResultTmp.ExaminationsID;
                        pupilIObjTmp.PupilID = pupilIObjResultTmp.PupilID;
                        pupilIObjTmp.PupilCode = pupilIObjResultTmp.PupilCode;
                        pupilIObjTmp.FullName = pupilIObjResultTmp.FullName;
                        pupilIObjTmp.Genre = pupilIObjResultTmp.Genre;
                        pupilIObjTmp.BirthDay = pupilIObjResultTmp.BirthDay;
                        pupilIObjTmp.ClassID = pupilIObjResultTmp.ClassID;
                        pupilIObjTmp.ClassName = pupilIObjResultTmp.ClassName;
                        pupilIObjTmp.SchoolID = pupilIObjResultTmp.SchoolID;
                        pupilIObjTmp.SchoolName = pupilIObjResultTmp.SchoolName;
                        pupilIObjTmp.ExamineeNumber = string.Empty;
                        pupilIObjTmp.ExamRoomName = string.Empty;
                        pupilIObjTmp.Location = string.Empty;
                        pupilIObjTmp.Year = pupilIObjResultTmp.Year;
                        pupilIObjTmp.ExamGroupID = pupilIObjResultTmp.ExamGroupID;
                        pupilIObjTmp.SchoolCode = pupilIObjResultTmp.SchoolCode;
                        pupilIObjTmp.EducationLevelID = pupilIObjResultTmp.EducationLevelID;
                        pupilIObjTmp.SchedulesExam = string.Empty;
                        pupilIObjTmp.SubjectID = pupilIObjResultTmp.SubjectID;
                        pupilIObjTmp.ExamCouncil = pupilIObjResultTmp.ExamCouncil;
                        pupilIObjTmp.BirthPlace = pupilIObjResultTmp.BirthPlace;
                        pupilIObjTmp.Mark = null;
                        pupilIObjResultTmp.ExamPupilStatus = pupilIObjResultTmp.SyncTime.HasValue ? "Đã gửi" : "";
                        listResultDistinct.Add(pupilIObjTmp);
                    }
                    return listResultDistinct;
                }
                else
                {
                    List<int> listExamPupilId = listResult.Select(p => p.ExamPupilID).Distinct().ToList();
                    List<int> listDOETSubjectId = listResult.Where(p => p.SubjectID.HasValue && p.SubjectID.Value > 0).Select(p => p.SubjectID.Value).Distinct().ToList();
                    List<DOETExamMarkBO> listMark = DOETExamPupilBusiness.GetListMark(listExamPupilId, listDOETSubjectId);
                    int subjectId = 0;
                    int examPupilId = 0;
                    for (int i = 0; i < listResult.Count; i++)
                    {
                        if (listResult[i].Genre == 1)
                        {
                            listResult[i].GenreName = "Nam";
                        }
                        else if (listResult[i].Genre == 0)
                        {
                            listResult[i].GenreName = "Nữ";
                        }
                        listResult[i].ExamPupilStatus = listResult[i].SyncTime.HasValue ? "Đã gửi" : "";
                        subjectId = listResult[i].SubjectID.HasValue ? listResult[i].SubjectID.Value : 0;
                        examPupilId = listResult[i].ExamPupilID;
                        if (listMark != null && listMark.Count > 0 && listMark.Exists(p => p.ExamPupilID == examPupilId && p.DoetSubjectID == subjectId))
                        {
                            decimal Mark = listMark.Where(p => p.ExamPupilID == examPupilId && p.DoetSubjectID == subjectId).Select(p => p.Mark).FirstOrDefault();
                            listResult[i].Mark = Mark;
                        }
                        else
                        {
                            listResult[i].Mark = null;
                        }
                    }
                }
            }

            return listResult;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Delete(string arrayID)
        {
            //if (GetMenupermission("ExamPupilArea", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            //{
            //    throw new BusinessException("Validate_Permission_Teacher");
            //}

            string[] examPupilIDArr;
            if (arrayID != "")
            {
                arrayID = arrayID.Remove(arrayID.Length - 1);
                examPupilIDArr = arrayID.Split(',');
            }
            else
            {
                examPupilIDArr = new string[] { };
            }
            List<int> listExamPupilID = examPupilIDArr.Length > 0 ? examPupilIDArr.ToList().Distinct().Select(o => Int32.Parse(o)).ToList() :
                                            new List<int>();

            List<DOETExamPupil> lstExamPupil = DOETExamPupilBusiness.All.Where(s => listExamPupilID.Contains(s.ExamPupilID)).ToList();

            DOETExamPupilBusiness.DeleteList(listExamPupilID);
            int provinceId = _globalInfo.ProvinceID.Value;
            //if (provinceId == GlobalConstants.ProvinceID_HCM)
            //{
            //    AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID ?? 0);
            //    SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            //    DOETExaminationsBusiness.DelteExamPupilHCM(academicYear, school, lstExamPupil);

            //    DoetHcmApi.XoaThiSinh("903775");
            //}
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult DeleteHCM(string arrayID)
        {
            if (string.IsNullOrEmpty(arrayID))
            {
                return Json(new JsonMessage("Không có thí sinh", "success"));
            }

            string sub = arrayID.Substring(0, arrayID.Length - 1);

            List<string> lstVal = sub.Split(',').ToList();
            if (lstVal == null || lstVal.Count == 0)
            {
                return Json(new JsonMessage("Không có thí sinh", "success"));
            }

            var retVal = DoetHcmApi.XoaThiSinh(lstVal);
            if (retVal.Contains("ExceptionMessage") || retVal.Contains("Message"))
            {
                ResponseExceptionMessage response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseExceptionMessage>(retVal);
                if (response != null)
                {
                    SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                    logger.Info(string.Format("Truong {0}, Id={1} tenTruong={2}, noi dung loi={3}"
                        , objSP.SyncCode, objSP.SchoolProfileID, objSP.SchoolName, response.ExceptionMessage));
                }
            }

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }

        //
        // GET: /ExamPupil/Create/
        public ActionResult PrepareCreate(int? examinationsID, int? examGroupID)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (!examinationsID.HasValue)
            {
                return PartialView("_DialogCreate");
            }
            //Danh sach ky thi
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //int unitId = _globalInfo.SupervisingDeptID.HasValue ? _globalInfo.SupervisingDeptID.Value : 0;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<int> lisUnitId = this.GetUnitIDOfSchool();
            dic.Add("LisUnitID", lisUnitId);
            //dic.Add("UnitID", unitId);
            dic.Add("Year", aca.Year);
            //Lấy danh sách kỳ thi
            listExaminations = DOETExaminationsBusiness.Search(dic).OrderByDescending(s => s.DeadLine).ToList();
            ViewData[RegistrationContestConstant.CBO_EXAMINATIONS] = listExaminations;

            //Lấy danh sách nhóm thi
            dic = new Dictionary<string, object>();
            dic.Add("ExaminationsID", examinationsID);
            // List<DOETExamGroup> listExamGroup = DOETExamGroupBusiness.Search(dic).ToList();
            List<DOETExamGroupSubject> listExamGroup = GetListGroupSubject(examinationsID.Value);
            ViewData[RegistrationContestConstant.CREATE_CBO_EXAM_GROUP] = listExamGroup;

            //Lấy danh sách khối học
            ViewData[RegistrationContestConstant.CREATE_CBO_EDUCATION_LEVEL] = _globalInfo.EducationLevels;
            int defaultEducationLevelID = _globalInfo.EducationLevels.FirstOrDefault() != null ? _globalInfo.EducationLevels.FirstOrDefault().EducationLevelID : 0;

            //Lấy danh sách lớp học
            IDictionary<string, object> dicToGetClass = new Dictionary<string, object>();
            dicToGetClass["EducationLevelID"] = defaultEducationLevelID;
            dicToGetClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicToGetClass).ToList();
            ViewData[RegistrationContestConstant.CREATE_CBO_CLASS] = listClass;

            //Lay nhom thi mac dinh
            int? defaultExamGroupID = null;
            if (examGroupID != null)
            {
                defaultExamGroupID = examGroupID;
            }
            else if (listExamGroup.Count > 0)
            {
                defaultExamGroupID = listExamGroup.First().GroupID;
            }

            //Lay khoi mac dinh
            dic["EducationLevelID"] = defaultEducationLevelID;
            IQueryable<PupilViewModel> iq = _SearchForCreate(dic, examinationsID, defaultExamGroupID);

            int totalRecord = iq.Count();
            ViewData[RegistrationContestConstant.TOTAL] = totalRecord;
            List<PupilViewModel> listResult = iq.Take(RegistrationContestConstant.PAGE_SIZE).ToList();
            for (int i = 0; i < listResult.Count; i++)
            {
                if (listResult[i].CsGenre == 1)
                {
                    listResult[i].CsGenreName = "Nam";
                }
                else if (listResult[i].CsGenre == 0)
                {
                    listResult[i].CsGenreName = "Nữ";
                }
            }
            ViewData[RegistrationContestConstant.CREATE_LIST_PUPIL] = listResult;
            ViewData[RegistrationContestConstant.EXAMINATIONS_ID] = examinationsID;
            ViewData[RegistrationContestConstant.EXAM_GROUP_ID] = defaultExamGroupID;
            return PartialView("_DialogCreate");
        }

        public ActionResult PrepareCreateHCM(int examinationsID)
        {
            ViewData[RegistrationContestConstant.CREATE_CBO_EDUCATION_LEVEL] = _globalInfo.EducationLevels;
            int defaultEducationLevelID = _globalInfo.EducationLevels.FirstOrDefault() != null ? _globalInfo.EducationLevels.FirstOrDefault().EducationLevelID : 0;
            IDictionary<string, object> dicToGetClass = new Dictionary<string, object>();
            dicToGetClass["EducationLevelID"] = defaultEducationLevelID;
            dicToGetClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicToGetClass).ToList();
            ViewData[RegistrationContestConstant.CREATE_CBO_CLASS] = listClass;
            List<MonThi> lstExamSubject = this.GetlistExamSubject(examinationsID);
            ViewData[RegistrationContestConstant.LIST_EXAM_SUBJECT] = lstExamSubject;
            ViewData[RegistrationContestConstant.EXAMINATIONS_ID] = examinationsID;
            return PartialView("_DialogCreateHCM");
        }

        public ActionResult SearchPupilHCM(FormCollection frm)
        {
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? Int32.Parse(frm["EducationLevelID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? Int32.Parse(frm["ClassID"]) : 0;
            int ExaminationID = !string.IsNullOrEmpty(frm["hdfExaminationsID"]) ? Int32.Parse(frm["hdfExaminationsID"]) : 0;
            int ExamSubjectID = !string.IsNullOrEmpty(frm["examsubject"]) ? Int32.Parse(frm["examsubject"]) : 0;
            string PupilCodeOrName = frm["PupilCodeOrName"];
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            List<PupilViewModel> lstPupilViewModel = new List<PupilViewModel>();
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID}
            };
            IQueryable<PupilOfClass> iquery = PupilOfClassBusiness.Search(dicSearch);
            if (!string.IsNullOrEmpty(PupilCodeOrName))
            {
                iquery = iquery.Where(p => (p.PupilProfile.PupilCode.Contains(PupilCodeOrName) || p.PupilProfile.FullName.Contains(PupilCodeOrName)));
            }

            //lay danh sach thi sinh da dang ky
            List<DanhSachThiSinh> lstRegisExam = DoetHcmApi.GetThiSinh(ExaminationID, objSP.SyncCode, ExamSubjectID);
            List<int> lstRegisID = lstRegisExam.Select(p => p.HocSinhLopID).Distinct().ToList();

            List<PupilOfClass> lstPOC = iquery.Where(p => !lstRegisID.Contains(p.PupilID)).ToList();
            lstPupilViewModel = (from poc in lstPOC
                                 select new PupilViewModel
                                 {
                                     PupilID = poc.PupilID,
                                     CsPupilCode = poc.PupilProfile.PupilCode,
                                     CsPupilName = poc.PupilProfile.FullName,
                                     CsClassID = poc.ClassID,
                                     OrderInClass = poc.OrderInClass,
                                     CsEducationLevelId = poc.ClassProfile.EducationLevelID,
                                     ClassOrder = poc.ClassProfile.OrderNumber,
                                     CsClassDisplayName = poc.ClassProfile.DisplayName,
                                     CsBirthDay = poc.PupilProfile.BirthDate,
                                     CsGenreName = poc.PupilProfile.Genre == 1 ? "Nam" : "Nữ"
                                 }).OrderBy(p => p.CsEducationLevelId)
                                 .ThenBy(p => p.ClassOrder)
                                 .ThenBy(p => p.CsClassDisplayName)
                                 .ThenBy(p => p.OrderInClass)
                                 .ThenBy(p => p.CsPupilName).ToList();
            ViewData[RegistrationContestConstant.CREATE_LIST_PUPIL] = lstPupilViewModel;
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            return PartialView("_CreateListHCM");
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchPupilCreateAjax(FormCollection fr, GridCommand command)
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermission(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<PupilViewModel> listResult = new List<PupilViewModel>();
            int examinationId = fr["CsExaminationsID"] != null && !string.IsNullOrEmpty(fr["CsExaminationsID"]) ? Int32.Parse(fr["CsExaminationsID"]) : 0;
            int examGroupId = fr["CsExamGroupID"] != null ? Int32.Parse(fr["CsExamGroupID"]) : 0;
            int educationLevelID = fr["EducationLevelID"] != null ? Int32.Parse(fr["EducationLevelID"]) : 0;
            int classId = fr["ClassID"] != null && !string.IsNullOrEmpty(fr["ClassID"]) ? Int32.Parse(fr["ClassID"]) : 0;
            string pupilCodeOrName = fr["PupilCodeOrName"];
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("EducationLevelID", educationLevelID);
            dic.Add("ClassID", classId);
            dic.Add("PupilCodeOrName", pupilCodeOrName);

            int Page = command.Page;
            if (command.Page == 0)
            {
                Page = 1;
            }

            if (examinationId > 0 && examGroupId > 0)
            {
                IQueryable<PupilViewModel> IQuery = this._SearchForCreate(dic, examinationId, examGroupId);
                totalRecord = IQuery.Count();
                listResult = IQuery.Skip((Page - 1) * RegistrationContestConstant.PAGE_SIZE).Take(RegistrationContestConstant.PAGE_SIZE).ToList();
            }
            //Kiem tra button
            //CheckCommandPermision(ExaminationsBusiness.Find(form.ExaminationsID), listResult);
            if (command.Page == 0)
            {
                ViewData[RegistrationContestConstant.TOTAL] = totalRecord;
                for (int i = 0; i < listResult.Count; i++)
                {
                    if (listResult[i].CsGenre == 1)
                    {
                        listResult[i].CsGenreName = "Nam";
                    }
                    else if (listResult[i].CsGenre == 0)
                    {
                        listResult[i].CsGenreName = "Nữ";
                    }
                }
                ViewData[RegistrationContestConstant.CREATE_LIST_PUPIL] = listResult;
                return PartialView("_CreateList");
            }
            return View(new GridModel<PupilViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(string arrayID, int? examinationsID, int? examGroupID,
            int? educationLevelID, int? classID, string pupilCodeOrName, int isGetAll)
        {

            //Kiem tra quyen insert
            //if (GetMenupermission("ExamPupilArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            //{
            //    throw new BusinessException("Validate_Permission_Teacher");
            //}

            //Kiem tra du lieu hop le
            //Neu chua chon ky thi
            if (examinationsID == null)
            {
                throw new BusinessException("ExamPupil_Validate_Create_Examinations");
            }

            //Neu chua chon nhom thi
            if (examGroupID == null)
            {
                throw new BusinessException("ExamPupil_Validate_Create_ExamGroup");
            }

            string[] pupilIDArr;
            if (arrayID != "")
            {
                arrayID = arrayID.Remove(arrayID.Length - 1);
                pupilIDArr = arrayID.Split(new[] { ',', '\"' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                pupilIDArr = new string[] { };
            }

            if (pupilIDArr.Length == 0 && isGetAll == 0)
            {
                throw new BusinessException("ExamPupil_Validate_Create_NotChoosen");
            }

            //Lay lai danh sach hoc sinh dang hoc
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = educationLevelID;
            IEnumerable<PupilViewModel> listPupil = _SearchForCreate(dic, examinationsID.GetValueOrDefault(), examGroupID);

            List<int> listPupilID = new List<int>();
            if (isGetAll == 0) //Khong check chon
            {
                listPupilID = pupilIDArr.ToList().Select(o => int.Parse(o)).Distinct().ToList();
                listPupil = listPupil.Where(o => listPupilID.Any(u => u == o.PupilID));
            }
            //Neu khong tich chon Lay tat ca, chi loc lai cac hoc sinh duoc tich chon
            if (isGetAll == 1)
            {
                listPupilID = listPupil.Select(o => o.PupilID).ToList();
            }

            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //Kiem tra 1 hoc sinh khong duoc tham gia nhieu nhom thi cung mon thi                       
            //Lay ra cac mon thi cua nhom thi
            List<int> lstSubjectID = DOETExamSubjectBusiness.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID)
                                                                    .Select(o => o.DOETSubjectID).Distinct().ToList();

            IQueryable<DOETExamPupil> query = DOETExamPupilBusiness.GetGroupedPupilWithSameSubject(listPupilID, lstSubjectID, examinationsID.Value);
            if (query.Count() > 0)
            {
                throw new BusinessException("ExamPupil_Validate_Create_DuplicateGroupWithSameSubject");
            }

            //Tao cac entity de insert
            int supervisingDeptId = this.GetUnitID();
            List<DOETExamPupil> insertList = listPupil.ToList().Select(o => new DOETExamPupil
            {
                ExaminationsID = examinationsID.GetValueOrDefault(),
                ExamGroupID = examGroupID.GetValueOrDefault(),
                EducationLevelID = educationLevelID.GetValueOrDefault(),
                SchoolID = _globalInfo.SchoolID.Value,
                UnitID = supervisingDeptId,
                Year = aca.Year,
                PupilID = o.PupilID,
                CreateTime = DateTime.Now
            }).ToList();

            DOETExamPupilBusiness.InsertList(insertList);

            /*SchoolProfile school = SchoolProfileBusiness.Find(aca.SchoolID);
            if (school.ProvinceID == GlobalConstants.ProvinceID_HCM)
            {
                DOETExaminationsBusiness.SyncPupilSMAStoHCM(aca, school, _globalInfo.AppliedLevel.Value, examinationsID ?? 0, examGroupID ?? 0);
            }*/

            //ghi log
            SetViewDataActionAudit(String.Empty, String.Empty
            , String.Empty
            , Res.Get("DOETExamPupil_Log_Create")
            , String.Empty
            , Res.Get("DOETExamPupil_Log_FunctionName")
            , SMAS.Business.Common.GlobalConstants.ACTION_ADD
            , Res.Get("DOETExamPupil_Log_Create"));

            return Json(new JsonMessage(Res.Get("ExamPupil_Message_CreateSuccess")));
        }

        [HttpPost]
        public JsonResult CreateHCM(string arrayID, int examinationsID, int examSubjectID,
            int? educationLevelID, int? classID)
        {
            List<int> lstPupilID = new List<int>();
            if (string.IsNullOrEmpty(arrayID))
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn học sinh để đăng ký.", "error"));
            }
            lstPupilID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"EducationLevelID",educationLevelID},
                {"ClassID",classID},
                {"lstPupilID",lstPupilID}
            };
            List<PupilOfClass> lstPOC = PupilOfClassBusiness.Search(dic).Where(s => s.Status == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();
            List<ThiSinhSMAS> lstSyncThiSinh = new List<ThiSinhSMAS>();

            string strEthnic = DoetHcmApi.GetDanToc();
            var rootEthnic = Newtonsoft.Json.JsonConvert.DeserializeObject<RootEthnic>(strEthnic);
            List<EthnicResponse> lstEthnic = rootEthnic.Table.ToList();

            foreach (var item in lstPOC)
            {
                EthnicResponse ethnicBO = lstEthnic.FirstOrDefault(s => s.DanTocID == item.PupilProfile.EthnicID);
                MapSource<int> mapSourceEthnic = new MapSource<int>();
                if (ethnicBO == null)
                {
                    mapSourceEthnic.SourceId = 1;
                }
                else
                {
                    mapSourceEthnic.SourceId = ethnicBO.DanTocID;
                }
                ThiSinhSMAS objInsert = new ThiSinhSMAS
                {
                    MaThiSinh = "0",
                    HocSinhLopID = item.PupilID,
                    NgaySinh = item.PupilProfile.BirthDate.ToString("dd/MM/yyyy"),
                    TenDuAn = "Dự án 1",
                    Khoi = item.ClassProfile.EducationLevelID,
                    NoiSinh = string.IsNullOrEmpty(item.PupilProfile.BirthPlace) ? "HCM" : item.PupilProfile.BirthPlace,
                    Phai = item.PupilProfile.Genre == 1 ? false : true,
                    TenLop = item.ClassProfile.DisplayName,
                    DanTocID = (mapSourceEthnic == null) ? 1 : mapSourceEthnic.SourceId,
                    Ho = (item.PupilProfile.FullName.Replace(item.PupilProfile.Name, "")).Trim(),
                    Ten = item.PupilProfile.Name
                };
                lstSyncThiSinh.Add(objInsert);
            }
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string schoolCode = string.IsNullOrEmpty(objSP.SyncCode) ? objSP.SchoolCode : objSP.SyncCode;

            string jsonExamPupil = Newtonsoft.Json.JsonConvert.SerializeObject(lstSyncThiSinh);
            var retVal = DoetHcmApi.NopThiSinh2(examinationsID, objSP.SyncCode, examSubjectID, jsonExamPupil);
            if (retVal.Contains("ExceptionMessage") || retVal.Contains("Message"))
            {
                ResponseExceptionMessage response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseExceptionMessage>(retVal);
                if (response != null)
                {
                    logger.Info(string.Format("Truong {0}, Id={1} tenTruong={2}, noi dung loi={3}"
                        , objSP.SyncCode, objSP.SchoolProfileID, objSP.SchoolName, response.ExceptionMessage));
                }
                return Json(new JsonMessage("Đăng ký không thành công", "error"));

            }

            /*var lstResult = UtilsBusiness.SplitListToMultipleList(lstSyncThiSinh, 5).ToList();
            ThiSinhDTO thiSinhDTO = new ThiSinhDTO
            {
                kiThiID=examinationsID,
                monThiID=examinationsID,
                SchoolID=schoolCode,

            };
            int countSuccess = 0;
            for (int i = 0; i < lstResult.Count; i++)
            {
                var objResult = lstResult[i];
                
                var retVal = DoetHcmApi.NopThiSinh(examinationsID, objSP.SyncCode, examSubjectID, jsonExamPupil);
                if (retVal.Contains("ExceptionMessage") || retVal.Contains("Message"))
                {
                    ResponseExceptionMessage response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseExceptionMessage>(retVal);
                    if (response != null)
                    {
                        logger.Info(string.Format("Truong {0}, Id={1} tenTruong={2}, noi dung loi={3}"
                            , objSP.SyncCode, objSP.SchoolProfileID, objSP.SchoolName, response.ExceptionMessage));
                    }
                }
                countSuccess += objResult.Count;
            }*/

            return Json(new JsonMessage("Đăng ký thành công " + lstSyncThiSinh.Count() + " thí sinh.", "success"));
        }

        [HttpPost]
        public JsonResult ReSyncExamPupilHCM(int? examinationsId, int? examGroupId, int? examSubjectId)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            SchoolProfile school = SchoolProfileBusiness.Find(aca.SchoolID);
            if (school.ProvinceID == GlobalConstants.ProvinceID_HCM)
            {
                DOETExaminationsBusiness.SyncPupilSMAStoHCM(aca, school, _globalInfo.AppliedLevel.Value, examinationsId ?? 0, examGroupId ?? 0);
            }
            return Json(new JsonMessage(Res.Get("ExamPupil_Message_CreateSuccess"), JsonMessage.SUCCESS));
        }

        public List<DOETExamGroupSubject> GetListGroupSubject(int examinationsId)
        {
            List<DOETExamGroupSubject> listResult = new List<DOETExamGroupSubject>();
            DOETExamGroupSubject GroupObj = null;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ExaminationsID", examinationsId);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel);
            List<DOETExamGroup> listExamGroup = DOETExamGroupBusiness.Search(dic).ToList();

            //Danh sach ID cua nhom
            List<int> listExamGroupId = listExamGroup.Select(p => p.ExamGroupID).ToList();

            dic = new Dictionary<string, object>();
            dic.Add("ExaminationsID", examinationsId);
            dic.Add("ListGroupID", listExamGroupId);
            List<DOETExamSubjectBO> listExamSubject = DOETExamSubjectBusiness.GetListExamSubject(dic).ToList();// Danh sach cac mon hoc cua nhom
            List<string> listSubjectNameTmp = null;
            if (listExamGroup != null && listExamGroup.Count > 0)
            {
                string groupName = string.Empty;
                for (int i = 0; i < listExamGroup.Count; i++)
                {
                    GroupObj = new DOETExamGroupSubject();
                    GroupObj.GroupID = listExamGroup[i].ExamGroupID;
                    GroupObj.GroupName = listExamGroup[i].ExamGroupName;

                    listSubjectNameTmp = listExamSubject.Where(p => p.ExamGroupID == listExamGroup[i].ExamGroupID).Select(p => p.DOETSubjectName).ToList();
                    groupName = string.Join(",", listSubjectNameTmp.ToArray());

                    GroupObj.ArrSubjectName = groupName;
                    listResult.Add(GroupObj);
                }
            }
            return listResult;
        }

        public string GetArrSubjectNameByGroupId(int examinationsId, int examGroupId)
        {
            string strSubject = string.Empty;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ExaminationsID", examinationsId);
            dic.Add("ExamGroupID", examGroupId);
            List<DOETExamSubjectBO> listExamSubject = DOETExamSubjectBusiness.GetListExamSubject(dic).ToList();// Danh sach cac mon hoc cua nhom
            if (listExamSubject != null && listExamSubject.Count > 0)
            {
                strSubject = string.Join(", ", listExamSubject.Select(p => p.DOETSubjectName).ToArray());
            }
            return strSubject;
        }

        public JsonResult GetArrSubject(int examinationsId, int examGroupId)
        {
            string str = GetArrSubjectNameByGroupId(examinationsId, examGroupId);
            return Json(new JsonMessage(str, "success"));
        }

        public JsonResult AjaxLoadClass(int paraEducationLevelID)
        {
            IDictionary<string, object> dicToGetClass = new Dictionary<string, object>();
            dicToGetClass["EducationLevelID"] = paraEducationLevelID;
            dicToGetClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<ClassProfile> listClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicToGetClass).ToList();

            return Json(new SelectList(listClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult AjaxLoadExamGroup(int? examinationsId)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ExaminationsID", examinationsId);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            ViewData[RegistrationContestConstant.CHECK_EXAMINATIONS] = this.CheckDateExaminations(examinationsId.Value);
            List<DOETExamGroup> listExamGroup = DOETExamGroupBusiness.Search(dic).ToList();
            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamSubject(int examinationsId, int examGroupId)
        {
            List<DOETSubjectModel> listSubject = _SearchSubject(examinationsId, examGroupId);
            StringBuilder strAppend = new StringBuilder();
            if (listSubject != null && listSubject.Count > 0)
            {
                for (int i = 0; i < listSubject.Count; i++)
                {
                    strAppend.Append("<option value='" + listSubject[i].SubjectID + "'>");
                    strAppend.Append(listSubject[i].SubjectName);
                    strAppend.Append("</option>");
                }
                return Json(new JsonMessage(strAppend.ToString(), "success"));
            }
            return Json(new JsonMessage(string.Empty, "success"));
        }

        public JsonResult AjaxLoadExamSubjectHCM(int examinationsId)
        {
            List<MonThi> listSubject = this.GetlistExamSubject(examinationsId);
            return Json(new SelectList(listSubject, "MonThiID", "TenMonThi"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewExaminations(int examinationsId)
        {
            DOETExaminations exmaintionsObj = DOETExaminationsBusiness.Find(examinationsId);
            if (exmaintionsObj == null)
            {

                exmaintionsObj = new DOETExaminations();
            }
            ViewData[RegistrationContestConstant.EXAM_OBJ] = exmaintionsObj;
            return PartialView("_ViewExamination");
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SyncExam()
        {
            int supervisingDeptId = _globalInfo.SupervisingDeptID.Value;
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(supervisingDeptId);

            if (supervisingDept.MSourcedb == "1")
            {
                return Json(new JsonMessage("Kỳ thi đang được cập nhật", JsonMessage.SUCCESS));
            }

            //Lay thong tin ky thi cua so HCM va SMAS
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            SchoolProfile school = SchoolProfileBusiness.Find(academicYear.SchoolID);
            int appliedLevel = _globalInfo.AppliedLevel.Value;
            DOETExaminationsBusiness.SyncExamHCM(academicYear, school, appliedLevel);

            return Json(new JsonMessage("Cập nhật kỳ thi thành công", JsonMessage.SUCCESS));

        }

        public FileResult ExportExcel(int examinationsID, int examGroupId, int examSubjectId)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", "PS_DSThisinh_Toan.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);

            //lay danh sach xuat excel
            AcademicYear acaObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("YearID", acaObj.Year);
            dic.Add("ExaminationsID", examinationsID);
            dic.Add("ExamGroupID", examGroupId);
            dic.Add("SubjectID", examSubjectId);
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("Grade", _globalInfo.AppliedLevel.Value);
            dic.Add("AcademicYearID", acaObj.AcademicYearID);
            SchoolProfile schoolObj = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            DOETSubject subjOBj = DOETSubjectBusiness.Find(examSubjectId);
            DOETExaminations examObj = DOETExaminationsBusiness.Find(examinationsID);
            List<DOETExamPupilBO> listResult = DOETExamPupilBusiness.GetListDOETExamSubject(dic).ToList();
            DOETExamPupilBO exapupilObj = null;

            List<int> listExamPupilId = listResult.Select(p => p.ExamPupilID).Distinct().ToList();
            List<int> listDOETSubjectId = listResult.Where(p => p.SubjectID.HasValue && p.SubjectID.Value > 0).Select(p => p.SubjectID.Value).Distinct().ToList();
            List<DOETExamMarkBO> listMark = DOETExamPupilBusiness.GetListMark(listExamPupilId, listDOETSubjectId);
            int subjectId = 0;
            int examPupilId = 0;
            for (int i = 0; i < listResult.Count; i++)
            {
                subjectId = listResult[i].SubjectID.HasValue ? listResult[i].SubjectID.Value : 0;
                examPupilId = listResult[i].ExamPupilID;
                if (listMark != null && listMark.Count > 0 && listMark.Exists(p => p.ExamPupilID == examPupilId && p.DoetSubjectID == subjectId))
                {
                    decimal Mark = listMark.Where(p => p.ExamPupilID == examPupilId && p.DoetSubjectID == subjectId).Select(p => p.Mark).FirstOrDefault();
                    listResult[i].Mark = Mark;
                }
                else
                {
                    listResult[i].Mark = null;
                }
            }

            sheet.SetCellValue("A3", examObj.ExaminationsName.ToUpper() + " - Năm học " + acaObj.DisplayTitle);
            sheet.SetCellValue("A4", "Môn " + subjOBj.DOETSubjectName);

            int startRow = 7;
            int startCol = 1;
            for (int i = 0; i < listResult.Count; i++)
            {
                startCol = 1;
                exapupilObj = listResult[i];
                sheet.SetCellValue(startRow, startCol++, i + 1);
                sheet.SetCellValue(startRow, startCol++, exapupilObj.PupilCode);
                sheet.SetCellValue(startRow, startCol++, exapupilObj.FullName);
                sheet.SetCellValue(startRow, startCol++, exapupilObj.Genre == 1 ? "Nam" : "Nữ");
                sheet.SetCellValue(startRow, startCol++, exapupilObj.BirthDay.ToString("dd/MM/yyyy"));
                sheet.SetCellValue(startRow, startCol++, exapupilObj.ClassName);
                sheet.SetCellValue(startRow, startCol++, schoolObj.SchoolCode);
                sheet.SetCellValue(startRow, startCol++, schoolObj.SchoolName);
                sheet.SetCellValue(startRow, startCol++, subjOBj.DOETSubjectName);
                sheet.SetCellValue(startRow, startCol++, exapupilObj.ExamineeNumber);
                sheet.SetCellValue(startRow, startCol++, exapupilObj.ExamRoomName);
                sheet.SetCellValue(startRow, startCol++, exapupilObj.Location);
                sheet.SetCellValue(startRow, startCol++, exapupilObj.Mark.HasValue ? exapupilObj.Mark.Value == 10 ? "10" : exapupilObj.Mark.Value == 0 ? "0" : exapupilObj.Mark.Value.ToString("0.0") : string.Empty);
                startRow++;
            }
            sheet.GetRange(7, 1, startRow - 1, startCol - 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            string sheetName = Utils.Utils.StripVNSign(subjOBj.DOETSubjectName);
            sheet.Name = sheetName;
            string fileName = string.Format("PS_DSThisinh_{0}.xls", sheetName);
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        public FileResult ExportExcelHCM(int examinationsID, int examSubjectId, string examinationsName, string subjectName)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", "PS_DSThisinh_Toan.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            sheet.SetCellValue("A3", examinationsName.ToUpper() + " - Năm học " + objAy.DisplayTitle);
            sheet.SetCellValue("A4", "Môn " + subjectName);

            int startRow = 7;
            int startCol = 1;
            List<DanhSachThiSinh> lstPupilExam = DoetHcmApi.GetThiSinh(examinationsID, !string.IsNullOrEmpty(objSP.SyncCode) ? objSP.SyncCode : objSP.SchoolCode, examSubjectId);
            DanhSachThiSinh objPupilExam = null;
            for (int i = 0; i < lstPupilExam.Count; i++)
            {
                objPupilExam = lstPupilExam[i];
                startCol = 1;
                sheet.SetCellValue(startRow, startCol++, i + 1);
                sheet.SetCellValue(startRow, startCol++, objPupilExam.MaThiSinh);
                sheet.SetCellValue(startRow, startCol++, objPupilExam.Ho + " " + objPupilExam.Ten);
                sheet.SetCellValue(startRow, startCol++, objPupilExam.Phai ? "Nữ" : "Nam");
                sheet.SetCellValue(startRow, startCol++, objPupilExam.NgaySinh);
                sheet.SetCellValue(startRow, startCol++, objPupilExam.TenLop);
                sheet.SetCellValue(startRow, startCol++, objSP.SyncCode);
                sheet.SetCellValue(startRow, startCol++, objSP.SchoolName);
                sheet.SetCellValue(startRow, startCol++, subjectName);
                sheet.SetCellValue(startRow, startCol++, objPupilExam.SBD);
                sheet.SetCellValue(startRow, startCol++, "");//phong thi
                sheet.SetCellValue(startRow, startCol++, "");//dia diem thi
                sheet.SetCellValue(startRow, startCol++, objPupilExam.Diem.HasValue ? objPupilExam.Diem.Value == 10 ? "10" : objPupilExam.Diem.Value == 0 ? "0" : objPupilExam.Diem.Value.ToString("0.0") : string.Empty);
                startRow++;
            }

            sheet.GetRange(7, 1, startRow - 1, startCol - 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            string sheetName = "DSThisinh";
            sheet.Name = sheetName;
            string fileName = string.Format("PS_DSThisinh_{0}.xls", sheetName);
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        public FileResult ExportTestCard(int examinationsID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", "Theduthi.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet SecondSheet = oBook.GetSheet(2);//Thong tin chung
            IVTWorksheet FirstSheet = oBook.GetSheet(1);//In the           
            IVTWorksheet NewSheet = null;
            AcademicYear acaObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("YearID", acaObj.Year);
            dic.Add("ExaminationsID", examinationsID);
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("Grade", _globalInfo.AppliedLevel.Value);
            dic.Add("AcademicYearID", acaObj.AcademicYearID);
            DOETExaminations examObj = DOETExaminationsBusiness.Find(examinationsID);
            #region Thong tin chung
            SecondSheet.SetCellValue("B2", _globalInfo.SuperVisingDeptName);
            SecondSheet.SetCellValue("B3", _globalInfo.SchoolName);
            SecondSheet.SetCellValue("B4", examObj.ExaminationsName);
            SecondSheet.SetCellValue("B5", examObj.Note);
            SecondSheet.SetCellValue("B6", "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            SecondSheet.SetCellValue("B8", "Năm học " + acaObj.DisplayTitle);
            #endregion
            //Danh sach nhieu nhom thi va nhieu mon thi 
            List<DOETExamPupilBO> listResult = DOETExamPupilBusiness.GetListDOETExamSubject(dic).ToList();
            List<int> listSubjectID = listResult.Where(p => p.SubjectID.HasValue && p.SubjectID.Value > 0).Select(p => p.SubjectID.Value).Distinct().ToList();//ID mon thi
            List<DOETSubject> listSubject = DOETSubjectBusiness.All.Where(p => listSubjectID.Contains(p.DOETSubjectID)).ToList();//Danh sach mon thi
            DOETSubject subjectObj = null;
            List<DOETExamPupilBO> listPupilBySubject = new List<DOETExamPupilBO>();
            DOETExamPupilBO pupilObjTmp = null;
            string subjectName = string.Empty;
            int subjectId = 0;
            int startRow = 26;
            int firstCol = 1;
            if (listSubjectID != null && listSubjectID.Count > 0)
            {
                for (int i = 0; i < listSubjectID.Count; i++)
                {

                    // Tao sheet moi
                    NewSheet = oBook.CopySheetToLast(FirstSheet, null);
                    subjectId = listSubjectID[i];
                    //Lay thogn tin hoc sinh du thi theo mon
                    listPupilBySubject = listResult.Where(p => p.SubjectID == subjectId).ToList();
                    if (listPupilBySubject != null && listPupilBySubject.Count > 0)
                    {
                        listPupilBySubject = listPupilBySubject.OrderBy(p => p.ExamineeNumber).ThenBy(p => Utils.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                    }
                    subjectObj = listSubject.Where(p => p.DOETSubjectID == subjectId).FirstOrDefault();
                    subjectName = subjectObj.DOETSubjectName;
                    //reset startRow 
                    startRow = 25 * (listPupilBySubject.Count - 1);
                    //Ten sheet
                    NewSheet.Name = (Utils.Utils.StripVNSign(subjectName).Length > 30) ? Utils.Utils.StripVNSign(subjectObj.DOETSubjectCode)
                                                                                     : Utils.Utils.StripVNSign(subjectName);
                    if (listPupilBySubject != null && listPupilBySubject.Count > 0)
                    {
                        for (int j = listPupilBySubject.Count - 1; j >= 0; j--)
                        {
                            pupilObjTmp = listPupilBySubject[j];
                            //Fill thong tin
                            NewSheet.SetCellValue("A2", "=Thongtinchung!B$2");
                            NewSheet.SetFormulaValue("A3", "=Thongtinchung!B$3");
                            NewSheet.SetFormulaValue("D6", "=Thongtinchung!B$4");
                            NewSheet.SetFormulaValue("D7", "=Thongtinchung!B$8");
                            NewSheet.SetFormulaValue("G20", "=Thongtinchung!B$7");
                            NewSheet.SetFormulaValue("D17", "=Thongtinchung!B$5");
                            NewSheet.SetFormulaValue("G19", "=Thongtinchung!B$6");
                            NewSheet.SetCellValue("D9", "SỐ BÁO DANH: " + pupilObjTmp.ExamineeNumber);
                            NewSheet.SetCellValue("G9", "PHÒNG THI: " + pupilObjTmp.ExamRoomName);
                            NewSheet.SetCellValue("D11", "Họ và tên thí sinh: " + pupilObjTmp.FullName);
                            NewSheet.SetCellValue("D12", "Ngày sinh: " + pupilObjTmp.BirthDay.ToString("dd/MM/yyyy"));
                            NewSheet.SetCellValue("G12", "Nơi sinh: " + pupilObjTmp.BirthPlace);
                            NewSheet.SetCellValue("D13", "Đang học lớp: " + pupilObjTmp.ClassName);
                            NewSheet.SetCellValue("G13", _globalInfo.SchoolName);
                            NewSheet.SetCellValue("D14", "Dự thi môn: " + subjectName);
                            NewSheet.SetCellValue("D15", "Hội đồng thi: " + pupilObjTmp.ExamCouncil);

                            //Copy thong tin sheet 1 do vao sheet cuoi
                            IVTRange range = NewSheet.GetRange("A2", "J25");
                            if (j > 0)
                            {
                                NewSheet.CopyPasteSameSize(range, startRow + 1, firstCol);
                            }
                            startRow -= 25;
                        }

                        //delete row patter
                        //for (int k = 1; k < 25; k++)
                        //{
                        //    NewSheet.DeleteRow(1);
                        //}
                    }
                }
            }
            FirstSheet.Delete();
            string fileName = "The_du_thi.xls";
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        public bool CheckDateExaminations(int examinationsId)
        {
            DOETExaminations examObj = DOETExaminationsBusiness.All.Where(p => p.ExaminationsID == examinationsId).FirstOrDefault();
            if (examObj != null)
            {
                DateTime dateDeadline = examObj.DeadLine;
                DateTime dateNow = DateTime.Now;
                //Neu thoi gian dang ky nho thoi gian hien tai thi het han dang ky
                if (dateDeadline.Date < dateNow.Date)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        #region Private methods

        private List<DOETExamGroupModel> _Search(long examinationID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ExaminationsID", examinationID);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            List<DOETExamGroupModel> ret = DOETExamGroupBusiness.Search(dic).ToList().
                Select(o => new DOETExamGroupModel
                {
                    ExamGroupID = o.ExamGroupID,
                    ExamGroupName = o.ExamGroupName
                }).ToList();
            return ret;
        }

        private List<DOETSubjectModel> _SearchSubject(int examinationsId, int examGroupId)
        {
            List<DOETSubjectModel> listExamSubject = (from es in DOETExamSubjectBusiness.All
                                                      join s in DOETSubjectBusiness.All on es.DOETSubjectID equals s.DOETSubjectID
                                                      where es.ExaminationsID == examinationsId
                                                      && es.ExamGroupID == examGroupId
                                                      select new DOETSubjectModel
                                                      {
                                                          SubjectID = es.DOETSubjectID,
                                                          SubjectName = s.DOETSubjectName
                                                      }).ToList();

            return listExamSubject;
        }


        /// <summary>
        /// Ham search du lieu cho popup them moi
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<PupilViewModel> _SearchForCreate(IDictionary<string, object> dic, int? examinationsID, int? examGroupID)
        {
            //Lay danh sach hoc sinh dang hoc
            IQueryable<PupilOfClassBO> lstPupil = PupilOfClassBusiness.GetStudyingPupil(_globalInfo.SchoolID.Value,
                _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, dic);

            //Lay danh sach hoc sinh da duoc xep nhom thi
            List<int> listPupilRegised = new List<int>();
            if (examinationsID != null && examGroupID != null)
            {
                IDictionary<string, object> dicToGetExamPupil = new Dictionary<string, object>();
                dicToGetExamPupil["ExaminationsID"] = examinationsID;
                dicToGetExamPupil["GroupID"] = examGroupID;
                listPupilRegised = DOETExamPupilBusiness.Search(dicToGetExamPupil).Select(p => p.PupilID).Distinct().ToList();
            }

            //Lay danh sach hoc sinh chua duoc xep nhom thi
            IQueryable<PupilViewModel> ret = lstPupil.Where(o => !listPupilRegised.Contains(o.PupilID))
                                        .Select(o => new PupilViewModel
                                        {
                                            CsBirthDay = o.Birthday,
                                            CsClassDisplayName = o.ClassName,
                                            CsClassID = o.ClassID,
                                            CsGenre = o.Genre,
                                            CsPupilCode = o.PupilCode,
                                            CsPupilName = o.PupilFullName,
                                            EthnicCode = o.EthnicCode,
                                            PupilID = o.PupilID,
                                            OrderInClass = o.OrderInClass,
                                            CsShortName = o.Name,
                                            CsEducationLevelId = o.EducationLevelID,
                                            ClassOrder = o.ClassOrder
                                        });

            return ret.OrderBy(p => p.CsEducationLevelId).ThenBy(p => p.ClassOrder).ThenBy(p => p.OrderInClass).ThenBy(p => p.CsShortName);
        }

        private int GetUnitID()
        {
            int superID = 0;
            if (_globalInfo.IsAdminSchoolRole)
            {
                superID = _globalInfo.SupervisingDeptID.Value;
            }
            else
            {
                //Neu truong thuoc so ma cap thuc hien dang la quan ly cua phong thi lay thong tin phong/GD
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY ||
                    _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    SchoolProfile objSchool = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                    superID = SchoolProfileBusiness.Find(_globalInfo.SchoolID).SupervisingDeptID.Value;
                    SupervisingDept dep2 = SupervisingDeptBusiness.All.FirstOrDefault(s =>
                        s.IsActive && s.ParentID == superID && s.DistrictID == objSchool.DistrictID);
                }
                else
                {
                    SchoolProfile objSchool = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                    superID = objSchool.SupervisingDeptID.Value;
                }


            }
            return superID;
        }

        private List<int> GetUnitIDOfSchool()
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            int appliedLevel = _globalInfo.AppliedLevel ?? 0;
            List<int> listSupervisingDeptId = new List<int>();
            int unitId = GetUnitID();
            SupervisingDept dept = SupervisingDeptBusiness.Find(unitId);
            //Neu truong truoc thuoc phong
            if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                //Phong va cac phong ban truc thuoc phong
                listSupervisingDeptId.Add(unitId);
                // listSupervisingDeptId.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == unitId
                //                                            && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT).Select(o => o.SupervisingDeptID).ToList());

                //So va cac phong ban truc thuoc so
                listSupervisingDeptId.Add(dept.ParentID.HasValue ? dept.ParentID.Value : 0);
                //listSupervisingDeptId.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == dept.ParentID
                //                                        && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o => o.SupervisingDeptID).ToList());
            }
            //Don vi quan ly cua truong la so
            else if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                if (appliedLevel != GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    SchoolProfile objSchool = SchoolProfileBusiness.Find(schoolId);
                    int provinceId = objSchool.ProvinceID ?? 0;
                    int districtId = objSchool.DistrictID ?? 0;
                    //Lay unitid phong giao duc
                    SupervisingDept objSupervisingDept =
                        SupervisingDeptBusiness.All.FirstOrDefault(s =>
                            s.ProvinceID == provinceId && s.DistrictID == districtId
                            && s.IsActive
                            && s.HierachyLevel == SMAS.Business.Common.SystemParamsInFile
                                .EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE);

                    listSupervisingDeptId.Add(objSupervisingDept.SupervisingDeptID);

                    if (objSchool.SchoolProfileID == 4)
                    {
                        listSupervisingDeptId.Add(20);
                    }
                }
                else
                {
                    listSupervisingDeptId.Add(unitId);
                }
                //Tin cua so va cac phong ban truc thuoc so

                //listSupervisingDeptId.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDeptID
                //                                        && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o =>o.SupervisingDeptID).ToList());
            }
            ///Neu truong co diem truong phu

            return listSupervisingDeptId;
        }

        private List<MonThi> GetlistExamSubject(int ExaminationsID)
        {
            List<MonThi> lstExamSubject = DoetHcmApi.GetMonThi(ExaminationsID);
            return lstExamSubject;
        }
        #endregion
    }
}
