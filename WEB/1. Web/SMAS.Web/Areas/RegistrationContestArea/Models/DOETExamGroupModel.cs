﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationContestArea.Models
{
    public class DOETExamGroupModel
    {
        public int ExamGroupID { set; get; }
        public string ExamGroupName { set; get; }
    }
}