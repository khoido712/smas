﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationContestArea.Models
{
    public class EmployeeViewBO
    {
        public int EmployeeId { get; set; }

        public string EmployeeCode { get; set; }
        public string FullName { get; set; }

        public int? EthnicId { get; set; }

        public string EthnicName { get; set; }

        public DateTime? BirthDay { get; set; }

        /// <summary>
        /// Trinh do dao tao
        /// </summary>
        public int? TrainingLevelID { get; set; }

        public string TrainingLevelName { get; set; }

        public string ExamEmployeeStatus { get; set; }

        public string GenreName { get; set; }
        public bool Genre { get; set; }
    }
}