﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationContestArea.Models
{
    public class ExamPupilHCMViewModel
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string Status { get; set; }
        public string FullName { get; set; }
        public string Genre { get; set; }
        public string Birthday { get; set; }
        public string ClassName { get; set; }
        public string ExamineeNumber { get; set; }
        public string ExamroomName { get; set; }
        public int ExamroomID { get; set; }
        public string SchedulesExam { get; set; }//lich thi
        public string Location { get; set; }//dia diem thi
        public decimal? Mark { get; set; }

        public bool IsApproved { get; set; }
    }
}