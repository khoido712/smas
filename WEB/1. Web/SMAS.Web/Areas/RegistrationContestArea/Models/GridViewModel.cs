﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationContestArea.Models
{
    public class GridViewModel
    {
        public string PupilCode { get; set; }
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public string Brithday { get; set; }
        public string ClassName { get; set; }
    }
}