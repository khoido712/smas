﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
namespace SMAS.Web.Areas.DOETRegistrationContestArea.Models
{
    public class PupilViewModel
    {

        public int PupilID { get; set; }
        public string CsPupilCode { get; set; }
        public string CsPupilName { get; set; }
        public string EthnicCode { get; set; }
        public DateTime CsBirthDay { get; set; }
        public int? CsGenre{ get;set;}
        public string CsGenreName { get; set; }
        public int CsClassID { get; set; }
        public string CsClassDisplayName { get; set; }
        public int? OrderInClass { get; set; }
        public string CsShortName { get; set; }
        public int CsEducationLevelId { get; set; }
        public int? ClassOrder { get; set; }
    }
}