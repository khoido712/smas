﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationContestArea.Models
{
    public class DOETSupervisoryType
    {
        public int DOETSupervisoryTypeId { get; set; }
        public string DOETSupervisoryName { get; set; }
    }
}