﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationContestArea.Models
{
    public class DOETSubjectModel
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
    }
}