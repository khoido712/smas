﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETRegistrationContestArea
{
    public class RegistrationContestConstant
    {
        public const string CBO_EXAMINATIONS = "CBO_EXAMINATIONS";
        public const string LIST_EXAM_GROUP = "LIST_EXAM_GROUP";
        public const string LIST_EXAM_SUBJECT = "LIST_EXAM_SUBEJCT";
        public const int PAGE_SIZE = 20;
        public const string LIST_EXAM_PUPIL = "LIST_EXAM_PUPIL";
        public const string TOTAL = "TOTAL";
        public const string PAGE = "PAGE";
        public const string CREATE_CBO_EXAM_GROUP = "CREATE_CBO_EXAM_GROUP";
        public const string CREATE_CBO_EDUCATION_LEVEL = "CREATE_CBO_EDUCATION_LEVEL";
        public const string CREATE_CBO_CLASS = "CREATE_CBO_CLASS";
        public const string CREATE_LIST_PUPIL = "CREATE_LIST_PUPIL";
        public const string ARR_SUBJECT_NAME = "ARR_SUBJECT_NAME";
        public const string EXAMINATIONS_ID = "EXAMINATIONS_ID";
        public const string EXAM_GROUP_ID = "EXAM_GROUP_ID";
        public const string EXAM_SUBJECT_ID = "EXAM_SUBJECT_ID";
        public const string CHECK_EXAMINATIONS = "CHECK_EXAMINATIONS";
        public const string EXAM_OBJ = "EXAM_OBJ";
        public const string APPLIED_TYPE = "APPLIED_TYPE";
        public const string SYNC_HCM_VISIBLE = "SYNC_HCM_ENABLE_";
        public const string IS_PROVINCE_HCM = "IS_PROVINCE_HCM";
        public const string PRINT_CARD_HCM = "PRINT_CARD_HCM";
        public const string IS_LOCK = "IsLock";
    }
}