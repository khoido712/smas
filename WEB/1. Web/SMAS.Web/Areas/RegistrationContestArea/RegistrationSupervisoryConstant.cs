﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationContestArea
{
    public class RegistrationSupervisoryConstant
    {
        

        public const string CBO_EXAMINATIONS = "CBO_EXAMINATIONS";
        public const string CBO_EMP_TYPE = "CBO_EMP_TYPE";
        public const string LIST_EXAM_EMPLOYEE_TYPE = "LIST_EXAM_EMPLOYEE_TYPE";
        public const string LIST_EXAM_SUBJECT = "LIST_EXAM_SUBEJCT";
        public const int PAGE_SIZE = 20;
        public const string LIST_EXAM_EMPLOYEE = "LIST_EXAM_EMPLOYEE";
        public const string TOTAL = "TOTAL";
        public const string PAGE = "PAGE";        
        public const string CREATE_LIST_EMPPLOYEE = "CREATE_LIST_EMPPLOYEE";
        public const string ARR_SUBJECT_NAME = "ARR_SUBJECT_NAME";
        public const string EXAMINATIONS_ID = "EXAMINATIONS_ID";
        public const string EXAM_EMPLOYEE_TYPE_ID = "EXAM_EMPLOYEE_TYPE_ID";
        public const string CHECK_EXAMINATIONS = "CHECK_EXAMINATIONS";
        public const string EXAM_OBJ = "EXAM_OBJ";
        public const string APPLIED_TYPE = "APPLIED_TYPE";
        public const string SYNC_HCM_VISIBLE = "SYNC_HCM_ENABLE_";
        public const string IS_PROVINCE_HCM = "IS_PROVINCE_HCM";
        //public const string PRINT_CARD_HCM = "PRINT_CARD_HCM";
    }
}