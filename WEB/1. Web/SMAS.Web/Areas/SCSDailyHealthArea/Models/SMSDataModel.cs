﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSDailyHealthArea.Models
{
    public class SMSDataModel
    {

        public List<string> Symptoms { get; set; }
        public List<string> Solutions { get; set; }
        public List<string> Contents { get; set; }
        public List<int> PupilIDs { get; set; }
        public List<int> MonitoringBookIDs { get; set; }
        public int TypeID { get; set; }
        public int ClassID { get; set; }
        public DateTime SelectedDate { get; set; }
        public SMSDataModel()
        {
            SelectedDate = DateTime.Now;
            Symptoms = new List<string>();
            Solutions = new List<string>();
            Contents = new List<string>();
            PupilIDs = new List<int>();
            MonitoringBookIDs = new List<int>();
            TypeID = 0;
            ClassID = 0;
        }
    }
}