﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SCSDailyHealthArea.Models
{
    public class SCSDailyHealthViewModel
    {
        public int STT { get; set; }
        public int PupilFileID { get; set; }
        public int ClassID { get; set; }
        public int MonitoringBookID { get; set; }
        [ResourceDisplayName("PupilLeavingOff_Label_Pupil")]
        public string PupilName { get; set; }
        public int Status { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_ContentToParent")]
        public string Content { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_CreateSymptoms")]
        public string Symptom { get; set; }
        [ResourceDisplayName("MonitoringBook_Label_CreateSolution")]
        public string Solution { get; set; }

        
        // Icon báo đã gửi 
        public bool IsSent { get; set; }
        public string IconTitle { get; set; }

        [ResourceDisplayName("SCSSMSTraining_Label_Length")]
        public string SMSLength { get; set; }

        public string SMSTemplate { get; set; }
        // check box
        public bool? isCheck { get; set; }

        public byte[] ImageData { get; set; }

        public string byteArrayToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn != null)
            {
                return "data:image/png;base64," + @System.Convert.ToBase64String(byteArrayIn);
            }
            else
            {
                return "";
            }

        }
    }
}