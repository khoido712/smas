﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SCSDailyHealthArea.Models;
using SMAS.Business.BusinessObject;
using System.Text;
using SMAS.Business.Common;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.SCSDailyHealthArea.Controllers
{
    public class SCSDailyHealthController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilFaultBusiness PupilFaultBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IHistorySMSBusiness HistorySMSBusiness;
        private readonly IMTBusiness MTBusiness;
        private readonly ISMSTypeDetailBusiness SMSTypeDetailBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBussiness;

        private readonly ISMSTypeBusiness SMSTypeBusiness;

        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private readonly IPhysicalTestBusiness PhysicalTestBusiness; // thong tin the luc
        private readonly IEyeTestBusiness EyeTestBusiness; // thong tin mằt
        private readonly IENTTestBusiness ENTTestBusiness;    // thong tin tai - mui - hong
        private readonly ISpineTestBusiness SpineTestBusiness;    // thong tin cot song
        private readonly IDentalTestBusiness DentalTestBusiness;    // thong tin rang - ham - mat
        private readonly IOverallTestBusiness OverallTestBusiness; // thong tin tong quat
        public SCSDailyHealthController(
            IAcademicYearBusiness academicYearBusiness,
            IClassProfileBusiness classprofilebusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IPupilFaultBusiness pupilFaultBusiness,
            IPupilAbsenceBusiness pupilAbsenceBusiness,
            IPupilOfClassBusiness pupilOfClassBussiness,
            IHistorySMSBusiness historySMSBusiness,
            IMTBusiness mtBusiness,
            ISMSTypeBusiness smsTypeBusiness,
            IMonitoringBookBusiness monitoringBookBusiness,
            IActivityOfPupilBusiness activityOfChildrenBusiness,
            IPhysicalTestBusiness physicalTestBusiness,
            IEyeTestBusiness eyeTestBusiness,
            ISpineTestBusiness spineTestBusiness,
            IDentalTestBusiness dentalTestBusiness,
            IOverallTestBusiness overallTestBusiness,
            ISMSTypeDetailBusiness sMSTypeDetailBusiness)
        {
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classprofilebusiness;
            this.PupilFaultBusiness = pupilFaultBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.HistorySMSBusiness = historySMSBusiness;
            this.MTBusiness = mtBusiness;
            this.PupilOfClassBussiness = pupilOfClassBussiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.SMSTypeBusiness = smsTypeBusiness;
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.PhysicalTestBusiness = physicalTestBusiness;
            this.EyeTestBusiness = eyeTestBusiness;
            this.SpineTestBusiness = spineTestBusiness;
            this.DentalTestBusiness = dentalTestBusiness;
            this.OverallTestBusiness = overallTestBusiness;
            this.SMSTypeDetailBusiness = sMSTypeDetailBusiness;
        }
        //
        // GET: /SCSDailyHealthArea/SCSDailyHealth/

        public ActionResult Index(int? ClassID)
        {
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            ClassSearchInfo["EmployeeID"] = _globalInfo.EmployeeID;
            IQueryable<ClassProfile> lstClass;
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsRolePrincipal)
            {
                lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);
            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);
            }
            // lay danh sach khoi
            List<EducationLevel> lstEducationLevel = lstClass.Select(o => o.EducationLevel).Distinct().ToList();
            if (ClassID.HasValue)
            {
                ClassProfile classProfile = ClassProfileBusiness.Find(ClassID.Value);
                List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
                SelectListItem item = null;
                EducationLevel level = null;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    item = new SelectListItem();
                    level = lstEducationLevel[i];
                    item.Value = level.EducationLevelID.ToString();
                    item.Text = level.Resolution;
                    if (level.EducationLevelID == classProfile.EducationLevelID)
                    {
                        item.Selected = true;
                    }
                    lstSelectListItem.Add(item);
                }
                ViewData[SCSDailyHealthConstants.LIST_BLOCK] = lstSelectListItem;
                // 
                List<ClassProfile> lstClassInEducation = lstClass.Where(o => o.EducationLevelID == classProfile.EducationLevelID).ToList();
                ClassProfile classItem;
                lstSelectListItem = new List<SelectListItem>(); // init new list
                for (int i = 0; i < lstClassInEducation.Count; i++)
                {
                    item = new SelectListItem();
                    classItem = lstClassInEducation[i];
                    item.Value = classItem.ClassProfileID.ToString();
                    item.Text = classItem.DisplayName;
                    if (classItem.ClassProfileID == ClassID.Value)
                    {
                        item.Selected = true;
                    }
                    lstSelectListItem.Add(item);
                }
                ViewData[SCSDailyHealthConstants.LIST_CLASS] = lstSelectListItem;
                ViewList(ClassID, DateTime.Now);
            }
            else
            {
                // select first level
                ViewData[SCSDailyHealthConstants.LIST_BLOCK] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");
                int FirstEducationLevelId = lstEducationLevel.FirstOrDefault().EducationLevelID;
                ViewData[SCSDailyHealthConstants.LIST_CLASS] = new SelectList(lstClass.Where(o => o.EducationLevelID == FirstEducationLevelId), "ClassProfileID", "DisplayName");
            }
            return View();
        }



        [ValidateAntiForgeryToken]
        public PartialViewResult ViewList(int? ClassID, DateTime? selectedDate)
        {
            //Danh sach hoc sinh tra ve
            List<SCSDailyHealthViewModel> lstPupilRet = new List<SCSDailyHealthViewModel>();
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                int schoolID = _globalInfo.SchoolID.HasValue ? _globalInfo.SchoolID.Value : 0;
                ClassProfile obj = ClassProfileBusiness.Find(ClassID);
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = _globalInfo.EducationLevel;
                SearchInfo["CurrentClassID"] = ClassID;
                SearchInfo["CurrentSchoolID"] = _globalInfo.SchoolID;
                SearchInfo["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;

                // lay ve danh sach hoc sinh theo cap, lop, truong, nam
                List<PupilProfileBO> lstPupil = PupilProfileBusiness.SearchBySchool(schoolID, SearchInfo).ToList();
                Session[SCSDailyHealthConstants.LIST_PUPIL] = lstPupil;

                // load len thong tin suc khoe
                SearchInfo["SchoolID"] = _globalInfo.SchoolID;
                SearchInfo["ClassID"] = ClassID;
                SearchInfo["EducationLevelID"] = _globalInfo.EducationLevel;
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                SearchInfo["MonitoringType"] = Constants.GlobalConstants.MONITORINGBOOK_TYPE_DATE;
                SearchInfo["MonitoringDate"] = selectedDate;
                // end

                SCSDailyHealthViewModel model = null;
                int STT = 1;
                string schoolName = _globalInfo.SchoolName;
                string SMSFormat = String.Format("{0}:TB suc khoe ngay {1}/{2} cua em ", schoolName, DateTime.Now.Day, DateTime.Now.Month + 1);

                SMSType smsType = new SMSType();
                SearchInfo["TypeID"] = smsType != null ? smsType.TypeID : 0;
                MonitoringBook item;
                //Search Monitoring Book by ClassID
                SearchInfo["classID"] = ClassID;
                SearchInfo["CreateDate"] = selectedDate;
                List<MonitoringBook> lsthealthBook = MonitoringBookBusiness.SearchBySchool(schoolID, SearchInfo).ToList();
                int pupilID = 0;
                foreach (var pupil in lstPupil)
                {
                    // khoi tao thong tin 
                    model = new SCSDailyHealthViewModel();
                    model.STT = STT++;
                    item = null;
                    pupilID = pupil.PupilProfileID;
                    model.PupilFileID = pupilID;
                    model.PupilName = pupil.FullName;
                    model.Status = pupil.ProfileStatus;
                    model.ImageData = pupil.Image;
                    model.isCheck = false;
                    model.ClassID = ClassID.Value;
                    model.SMSTemplate = SMSFormat;

                    //end
                    item = lsthealthBook.Where(a => a.PupilID == pupil.PupilProfileID).FirstOrDefault();
                    if (item != null)
                    {
                        model.Symptom = item.Symptoms;
                        model.Solution = item.Solution;
                        model.MonitoringBookID = item.MonitoringBookID;
                        if (item.IsSMS.HasValue && item.IsSMS.Value)
                        {

                            // tien hanh lay tin nhan da gui gan nhat
                            SearchInfo["PupilID"] = pupilID;
                            HistorySMS smsRecent = HistorySMSBusiness.Search(SearchInfo).OrderByDescending(a => a.CreateDate).FirstOrDefault();
                            if (smsRecent == null)
                            {
                                model.Content = string.Empty;
                                model.IconTitle = string.Empty;
                                model.SMSLength = "";
                            }
                            else
                            {
                                model.IsSent = true;
                                model.Content = smsRecent.Content;
                                model.IconTitle =string.Format(Res.Get("Communication_SMSToParentPupil_SentInformation"), smsRecent.CreateDate.Value.ToString(Constants.GlobalConstants.CUSTOM_FORMAT_DATETIME_ES));
                                model.SMSLength = String.Format("{0}/{1}", smsRecent.Content.Length, smsRecent.ContentCount);
                            }
                        }
                    }
                    lstPupilRet.Add(model);
                }
                ViewData[SCSDailyHealthConstants.SMS_TEMPLATE] = SMSFormat;
            }
            ViewData[SCSDailyHealthConstants.LIST_DAILY_HEALTH] = lstPupilRet;
            Session[SCSDailyHealthConstants.LIST_DAILY_HEALTH] = lstPupilRet;
            ViewData[SCSDailyHealthConstants.CLASS_ID] = ClassID;
            ClassProfile cls = ClassProfileBusiness.Find(ClassID);
            if (cls != null)
            {
                ViewData[SCSDailyHealthConstants.CLASS_NAME] = cls.DisplayName;
            }

            ViewData["SelectedDateTime"] = selectedDate;
            //Get view data here
            return PartialView("_ListPupil");
        }

        /// <summary>
        /// Luu suc khoe ngay
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [SMAS.Web.Filter.ActionAudit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveData(SMSDataModel data)
        {
            try
            {
                int numSent = 0;
                List<int> PupilIDs = data.PupilIDs;
                List<string> Contents = data.Contents;
                List<string> Symptoms = data.Symptoms;
                List<string> Solutions = data.Solutions;

                /* Common infomation*/
                //Kiem tra thoi gian vi pham phai thuoc thoi gian nam hoc
                if (!AcademicYearBusiness.CheckTimeInYear(_globalInfo.AcademicYearID.Value, data.SelectedDate))
                {
                    return Json(new JsonMessage(Res.Get("CalendarSchedule_Update_Time_Error"), "ERROR"));
                }
                //KIem tra phan quyen cua giao vien
                ClassProfile cls = ClassProfileBusiness.Find(data.ClassID);
                if (cls == null)
                {
                    return Json(new JsonMessage(Res.Get("CalendarSchedule_No_Data_Or_Permission"), "ERROR"));
                }
                //end
                int Count = PupilIDs.Count;
                //SMSType SMSType = SMSTypeBusiness.FindByTypeCode(GlobalConstants.HISTORYSMS_TO_HEALTH_TYPECODE);
                List<PupilProfileBO> lstPupil = (List<PupilProfileBO>)Session[SCSDailyHealthConstants.LIST_PUPIL];
                //SMSType smsType = SMSTypeBusiness.FindByTypeCode(Constants.GlobalConstants.HISTORYSMS_TO_HEALTH_TYPECODE);

                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = _globalInfo.EducationLevel;
                SearchInfo["CurrentClassID"] = data.ClassID;
                SearchInfo["CurrentSchoolID"] = _globalInfo.SchoolID;
                SearchInfo["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;
                // load len thong tin suc khoe
                SearchInfo["SchoolID"] = _globalInfo.SchoolID;
                SearchInfo["ClassID"] = data.ClassID;
                SearchInfo["EducationLevelID"] = _globalInfo.EducationLevel;
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                SearchInfo["MonitoringType"] = Constants.GlobalConstants.MONITORINGBOOK_TYPE_DATE;
                SearchInfo["MonitoringDate"] = data.SelectedDate;
                SearchInfo["CreateDate"] = data.SelectedDate;
                //SearchInfo["TypeID"] = smsType.TypeID;
                //Search Monitoring Book by ClassID
                List<MonitoringBook> lstMonitoring = MonitoringBookBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();

                for (int i = 0; i < Count; i++)
                {
                    int pupilID = PupilIDs[i];
                    string symptom = Symptoms[i].Trim();
                    string solution = Solutions[i].Trim();
                    int monitor = data.MonitoringBookIDs[i];
                    PupilProfileBO ppPF = lstPupil.Where(o => o.PupilProfileID == pupilID).FirstOrDefault();
                    MonitoringBook tmp = lstMonitoring.Where(a => a.PupilID == pupilID).FirstOrDefault();
                    if (tmp != null)
                    {
                        tmp = MonitoringBookBusiness.Find(tmp.MonitoringBookID);
                        tmp.Symptoms = symptom;
                        tmp.Solution = solution;
                        tmp.IsSMS = false;
                        MonitoringBookBusiness.Update(tmp);
                        numSent++;
                    }
                    else
                    {
                        // Luu vao MonitoringBook
                        MonitoringBook entity = new MonitoringBook();
                        entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        entity.SchoolID = _globalInfo.SchoolID.Value;
                        entity.ClassID = data.ClassID;
                        entity.PupilID = pupilID;
                        entity.MonitoringDate = data.SelectedDate;
                        entity.Symptoms = symptom;
                        entity.Solution = solution;
                        entity.IsSMS = false;
                        if (monitor == -1) entity.IsSMS = true;
                        entity.EducationLevelID = cls.EducationLevelID;
                        entity.MonitoringType = Constants.GlobalConstants.MONITORINGBOOK_TYPE_DATE;
                        entity.CreateDate = DateTime.Now;
                        MonitoringBookBusiness.Insert(entity);
                        numSent++;
                    }
                }
                if (numSent > 0)
                {
                    //Save all changed.
                    MonitoringBookBusiness.Save();
                }
            }
            catch
            {
                //do nothings
            }
            return Json(new JsonMessage(Res.Get("DailyHealth_Message_SaveSuccess")));
        }

        /// <summary>
        /// Gui SMS toi PHHS
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        /// 
        [SMAS.Web.Filter.ActionAudit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendSMS(SMSDataModel data)
        {
            
            return Json(new { Type = "success", Message = String.Format(Res.Get("Communication_SMSToParent_Result"), 0), IDs = "", Time = DateTime.Now.ToString(Constants.GlobalConstants.CUSTOM_FORMAT_DATETIME_ES) });
        }
        /// <summary>
        /// Load danh sach lop tung khoi
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo(); ;
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AcademicYearID"] = global.AcademicYearID;
            ClassSearchInfo["EmployeeID"] = global.EmployeeID;
            IQueryable<ClassProfile> lstClass;
            if (EducationLevelID.HasValue)
            {
                if (!global.IsAdminSchoolRole && !global.IsRolePrincipal)
                {
                    lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(global.EmployeeID.Value, global.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == global.AppliedLevel && p.EducationLevelID == EducationLevelID);
                }
                else
                {
                    lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, ClassSearchInfo).Where(p => p.EducationLevel.Grade == global.AppliedLevel && p.EducationLevelID == EducationLevelID);
                }
                return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
            }
            else
            {
                return Json(new SelectList(new String[] { }, "", ""));
            }

        }

    }
}
