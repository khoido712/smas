﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SCSDailyHealthArea
{
    public class SCSDailyHealthAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SCSDailyHealthArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SCSDailyHealthArea_default",
                "SCSDailyHealthArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
