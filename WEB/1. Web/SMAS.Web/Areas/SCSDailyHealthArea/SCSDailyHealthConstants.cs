﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSDailyHealthArea
{
    public class SCSDailyHealthConstants
    {
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_BLOCK = "LIST_BLOCK";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_PUPIL = "LIST_PUPILS";
        public const string LIST_DAILY_HEALTH = "LIST_DAILY_HEALTH";
        public const string CLASS_NAME = "CLASS_NAME";
        public const string CLASS_ID = "CLASS_ID";
        public const string SMS_TEMPLATE = "SMS_TEMPLATE";

        public const string LIST_SMS_TYPE = "LIST_SMS_TYPE";
    }
}