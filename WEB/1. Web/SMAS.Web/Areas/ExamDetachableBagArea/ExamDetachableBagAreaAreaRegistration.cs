﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamDetachableBagArea
{
    public class ExamDetachableBagAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamDetachableBagArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamDetachableBagArea_default",
                "ExamDetachableBagArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
