﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamDetachableBagArea
{
    public class ExamDetachableBagConstants
    {
        //View data
        public const string CBO_EXAMINATIONS= "cbo_examinations";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string CBO_SUBJECT = "cbo_subject";
        public const string CBO_EXAM_CANDENCE_BAG="cbo_exam_bag";
        public const string LIST_RESULT = "list_result";

        //Loai danh phach
        //Danh phach theo tui bai thi
        public const int AUTO_TYPE_BY_EXAM_BAG = 1;
        //Danh phach ngau nhien
        public const int AUTO_TYPE_RANDOM = 2;
        //Import danh phach thu cong
        public const int AUTO_TYPE_IMPORT = 3;

        //Quy mo danh phach
        public const int AUTO_SCALE_CURRENT_SUBJECT = 1;
        public const int AUTO_SCALE_CURRENT_EXAM_GROUP = 2;
        public const int AUTO_SCALE_ALL_SUBJECT_ALL_EXAM_GROUP = 3;

        public const string PER_CHECK_BUTTON = "per_check_button";
        public const string PER_CHECK_DELETE = "per_check_delete";
        public const string PER_CHECK_EXPORT = "per_check_export";

        //Phan trang
        public const string PAGING_ENABLE = "paging_enable";
        public const string TOTAL = "total";
        public const string PAGE = "page";
        public const int PageSize = 20;
    }
}