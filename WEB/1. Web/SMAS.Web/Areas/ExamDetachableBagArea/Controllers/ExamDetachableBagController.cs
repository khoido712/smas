﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Web.Areas.ExamDetachableBagArea.Models;
using SMAS.Web.Areas.ExamDetachableBagArea;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.ExamDetachableBagArea
{
    [SkipCheckRole]
    public class ExamDetachableBagController : BaseController
    {
        #region properties
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamBagBusiness ExamBagBusiness;
        private readonly IExamCandenceBagBusiness ExamCandenceBagBusiness;
        private readonly IExamDetachableBagBusiness ExamDetachableBagBusiness;
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamSubjectBO> listExamSubject;
        private List<ExamCandenceBag> listExamCandenceBag;
        #endregion

        #region Constructor
        public ExamDetachableBagController(IExamGroupBusiness examGroupBusiness, IExaminationsBusiness examinationsBusiness,
            IExamSubjectBusiness examSubjectBusiness, IExamBagBusiness examBagBusiness, 
            IExamDetachableBagBusiness examDetachableBagBusiness, IExamPupilBusiness examPupilBusiness,
            IExamCandenceBagBusiness examCandenceBagBusiness, IExamRoomBusiness examRoomBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.ExamGroupBusiness = examGroupBusiness;
            this.ExaminationsBusiness = examinationsBusiness;
            this.ExamSubjectBusiness = examSubjectBusiness;
            this.ExamBagBusiness = examBagBusiness;
            this.ExamDetachableBagBusiness = examDetachableBagBusiness;
            this.ExamPupilBusiness = examPupilBusiness;
            this.ExamCandenceBagBusiness = examCandenceBagBusiness;
            this.ExamRoomBusiness=examRoomBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;

            listExaminations = new List<Examinations>();
            listExamGroup = new List<ExamGroup>();
            listExamSubject = new List<ExamSubjectBO>();
            listExamCandenceBag = new List<ExamCandenceBag>();
        }
        #endregion

        #region Actions
        //
        // GET: /ExamPupilArea/ExamPupil/

        public ActionResult Index()
        {

            SetViewData();

            ////Search du lieu
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;

            long? defaultSubjectID = null;
            if (listExamSubject.Count > 0) defaultSubjectID = listExamSubject.First().SubjectID;

            long? defaultExamCandenceBagId = null;

            if (defaultExamID == null || defaultExamGroupID == null || defaultSubjectID == null)
            {
                ViewData[ExamDetachableBagConstants.LIST_RESULT] = new List<ListViewModel>();
                ViewData[ExamDetachableBagConstants.TOTAL] = 0;
                CheckCommandPermision(null);
                return View();
            }

            //List<ListViewModel> listResult = _Search(defaultExamID.Value, defaultExamGroupID.Value, defaultSubjectID.Value, defaultExamCandenceBagId).ToList().OrderBy(o=>o.ExamineeNumber).ToList();
            //ViewData[ExamDetachableBagConstants.LIST_RESULT] = listResult;
            IEnumerable<ListViewModel> iq = _Search(defaultExamID.Value, defaultExamGroupID.Value, defaultSubjectID.Value, defaultExamCandenceBagId);
            int totalRecord = iq.Count();
            ViewData[ExamDetachableBagConstants.TOTAL] = totalRecord;
            List<ListViewModel> listResult = iq.Take(ExamDetachableBagConstants.PageSize).ToList();
            ViewData[ExamDetachableBagConstants.LIST_RESULT] = listResult;

            Examinations defaultExaminations=ExaminationsBusiness.Find(defaultExamID);
            CheckCommandPermision(defaultExaminations);

            return View();
        }


        // GET: /ExamDetachableBag/Search
        public PartialViewResult Search(SearchViewModel form, GridCommand command)
        {
            Utils.Utils.TrimObject(form);

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;
            long? examCandenceBagID = form.ExamCandenceBagID;

            if (examinationsID == null || examGroupID == null || subjectID == null)
            {
                CheckCommandPermision(null);
                return PartialView("_List", new List<ListViewModel>());
            }

            IEnumerable<ListViewModel> iq = _Search(examinationsID.Value, examGroupID.Value, subjectID.Value, examCandenceBagID);
            int totalRecord = iq.Count();
            ViewData[ExamDetachableBagConstants.TOTAL] = totalRecord;

            List<ListViewModel> listResult;
            if (!form.ExamCandenceBagID.HasValue || form.ExamCandenceBagID == 0)
            {
                listResult = iq.Take(ExamDetachableBagConstants.PageSize).ToList();
                ViewData[ExamDetachableBagConstants.PAGING_ENABLE] = true;
            }
            else
            {
                listResult = iq.ToList();
                ViewData[ExamDetachableBagConstants.PAGING_ENABLE] = false;
            }

            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            CheckCommandPermision(exam);
            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(SearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;
            long? examCandenceBagID = form.ExamCandenceBagID;

            if (examinationsID == null || examGroupID == null || subjectID == null)
            {
                return View(new GridModel<ListViewModel>
                {
                    Total = 0,
                    Data = new List<ListViewModel>()
                });
            }

            //Add search info - Navigate to Search function in business
            IEnumerable<ListViewModel> iq = _Search(examinationsID.Value, examGroupID.Value, subjectID.Value, examCandenceBagID);
            int totalRecord = iq.Count();
            List<ListViewModel> listResult = iq.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

            //Kiem tra button
            return View(new GridModel<ListViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        /// <summary>
        /// Auto Detachable
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult AutoDetachable(AutoDetachableViewModel form)
        {
            if (ModelState.IsValid)
            {
                if (GetMenupermission("ExamDetachableBag", _globalInfo.UserAccountID, _globalInfo.IsAdmin,_globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }

                Utils.Utils.TrimObject(form);

                int autoType = form.type;
                bool includeExamBagCheck = form.includeExamBagCodeCheck;
                bool lengthCheck = form.lengthCheck;
                int autoScale = form.scale;
                long examinationsID=form.examinationsID;
                long examGroupID=form.examGroupID;
                int subjectID=form.subjectID;
                int length = 0;
                if (!String.IsNullOrEmpty(form.lengthNum))
                {
                    length = Convert.ToInt32(form.lengthNum);
                }
                int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);

                //Tao danh sach cac tui phach chuan bi insert
                List<ExamCandenceBag> listExamCandenceBagToInsert = new List<ExamCandenceBag>();
                //Tao danh sach cac phach chuan bi insert
                List<ExamDetachableBag> listExamDetachableBagToInsert = new List<ExamDetachableBag>();

                //Danh sach cac tui phach cu delete
                List<ExamCandenceBag> listExamCandenceBagToDelete = new List<ExamCandenceBag>();
                
                //Danh sach cac phach cu delete
                List<ExamDetachableBag> listExamDetachableBagToDelete = new List<ExamDetachableBag>();

                //Lay danh sach thi sinh cua ky thi
                List<ExamPupil> listExamPupilOfExaminations = ExamPupilBusiness.GetExamPupilOfExaminations(examinationsID, _globalInfo.SchoolID.Value,
                                                                                                _globalInfo.AcademicYearID.Value, partition).ToList();

                //Lay danh sach tui bai thi cua ky thi
                List<ExamBagBO> listExamBagOfExaminations = ExamBagBusiness.GetExamBagOfExaminations(examinationsID).ToList();

                //Danh sach tui phach cua ky thi
                List<ExamCandenceBag> listExamCandenceBagOfExaminations = this.ExamCandenceBagBusiness.All.Where(o => o.ExaminationsID == examinationsID).ToList();

                //Danh sach phach cua ky thi
                List<ExamDetachableBag> listExamDetachableBagOfExaminations = this.ExamDetachableBagBusiness.All.Where(o => o.LastDigitSchoolID == partition)
                                                                                           .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID.Value)
                                                                                           .Where(o => o.ExaminationsID == examinationsID).ToList();

                //Danh phach theo tui bai thi
                if (autoType == ExamDetachableBagConstants.AUTO_TYPE_BY_EXAM_BAG)
                {
                    //Danh phach cho mon thi hien tai
                    if (autoScale == ExamDetachableBagConstants.AUTO_SCALE_CURRENT_SUBJECT)
                    {
                        this.AutoDetachableByExamBagHandle(form, listExamCandenceBagToInsert, listExamDetachableBagToInsert, listExamPupilOfExaminations, listExamBagOfExaminations,
                                                            listExamCandenceBagOfExaminations, listExamDetachableBagOfExaminations, listExamCandenceBagToDelete,listExamDetachableBagToDelete);
                    }
                    //Danh phach cho nhom thi hien tai
                    else if(autoScale==ExamDetachableBagConstants.AUTO_SCALE_CURRENT_EXAM_GROUP)
                    {
                       //Lay danh sach mon thi cua nhom thi
                        List<ExamSubject> listExamSubject = ExamSubjectBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                                    .Where(o => o.ExamGroupID == examGroupID)
                                                                                    .ToList();
                        for (int i = 0; i < listExamSubject.Count; i++)
                        {
                            form.subjectID = listExamSubject[i].SubjectID;
                            this.AutoDetachableByExamBagHandle(form, listExamCandenceBagToInsert, listExamDetachableBagToInsert, listExamPupilOfExaminations, listExamBagOfExaminations,
                                                                listExamCandenceBagOfExaminations, listExamDetachableBagOfExaminations, listExamCandenceBagToDelete,listExamDetachableBagToDelete);
                        }
                    }

                    //Danh phach cho tat ca nhom thi
                    else if (autoScale == ExamDetachableBagConstants.AUTO_SCALE_ALL_SUBJECT_ALL_EXAM_GROUP)
                    {
                        //Lay danh sach nhom thi 
                        List<ExamGroup> listExamGroup = ExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                            .ToList();

                        //Lay danh sach mon thi cua ky thi
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        dic["ExaminationsID"] = examinationsID;
                        List<ExamSubjectBO> listExamSubjectOfExaminations = ExamSubjectBusiness.GetListExamSubject(dic).ToList();

                        for (int i = 0; i < listExamGroup.Count; i++)
                        {
                            ExamGroup group = listExamGroup[i];
                            //Lay danh sach mon thi cua nhom thi

                            List<ExamSubjectBO> listExamSubject = listExamSubjectOfExaminations.Where(o=>o.ExamGroupID==group.ExamGroupID).OrderBy(o => o.OrderInSubject).ToList();
                            for (int j = 0; j < listExamSubject.Count; j++)
                            {
                                form.subjectID = listExamSubject[j].SubjectID;
                                form.examGroupID = group.ExamGroupID;
                                this.AutoDetachableByExamBagHandle(form, listExamCandenceBagToInsert, listExamDetachableBagToInsert, listExamPupilOfExaminations, listExamBagOfExaminations,
                                                                       listExamCandenceBagOfExaminations, listExamDetachableBagOfExaminations, listExamCandenceBagToDelete,listExamDetachableBagToDelete);
                            }
                        }
                    }
                }
                //Danh phach ngau nhien
                else if (autoType == ExamDetachableBagConstants.AUTO_TYPE_RANDOM)
                {
                    //Danh phach cho mon thi hien tai
                    if (autoScale == ExamDetachableBagConstants.AUTO_SCALE_CURRENT_SUBJECT)
                    {
                        this.AutoDetachableRandomHandle(form, listExamCandenceBagToInsert, listExamDetachableBagToInsert, listExamPupilOfExaminations, listExamBagOfExaminations,
                                                        listExamCandenceBagOfExaminations, listExamDetachableBagOfExaminations, listExamCandenceBagToDelete,listExamDetachableBagToDelete);
                    }
                    //Danh phach cho nhom thi hien tai
                    else if (autoScale == ExamDetachableBagConstants.AUTO_SCALE_CURRENT_EXAM_GROUP)
                    {
                        //Lay danh sach mon thi cua nhom thi
                        List<ExamSubject> listExamSubject = ExamSubjectBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                                    .Where(o => o.ExamGroupID == examGroupID)
                                                                                    .ToList();
                        for (int i = 0; i < listExamSubject.Count; i++)
                        {
                            form.subjectID = listExamSubject[i].SubjectID;
                            this.AutoDetachableRandomHandle(form, listExamCandenceBagToInsert, listExamDetachableBagToInsert, listExamPupilOfExaminations, listExamBagOfExaminations,
                                                        listExamCandenceBagOfExaminations, listExamDetachableBagOfExaminations, listExamCandenceBagToDelete, listExamDetachableBagToDelete);
                        }
                    }

                    //Danh phach cho tat ca nhom thi
                    else if (autoScale == ExamDetachableBagConstants.AUTO_SCALE_ALL_SUBJECT_ALL_EXAM_GROUP)
                    {
                        //Lay danh sach nhom thi 
                        List<ExamGroup> listExamGroup = ExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID).ToList();
                        //Lay danh sach mon thi cua ky thi
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        dic["ExaminationsID"] = examinationsID;
                        List<ExamSubjectBO> listExamSubjectOfExaminations = ExamSubjectBusiness.GetListExamSubject(dic).ToList();

                        for (int i = 0; i < listExamGroup.Count; i++)
                        {
                            ExamGroup group = listExamGroup[i];
                            //Lay danh sach mon thi cua nhom thi
                            List<ExamSubjectBO> listExamSubject = listExamSubjectOfExaminations.Where(o=>o.ExamGroupID==group.ExamGroupID).OrderBy(o => o.OrderInSubject).ToList();
                            for (int j = 0; j < listExamSubject.Count; j++)
                            {
                                form.subjectID = listExamSubject[j].SubjectID;
                                form.examGroupID = group.ExamGroupID;
                                this.AutoDetachableRandomHandle(form, listExamCandenceBagToInsert, listExamDetachableBagToInsert, listExamPupilOfExaminations, listExamBagOfExaminations,
                                                       listExamCandenceBagOfExaminations, listExamDetachableBagOfExaminations, listExamCandenceBagToDelete, listExamDetachableBagToDelete);
                            }
                        }
                    }
                }
                
                ExamDetachableBagBusiness.InsertAllExamDetachableBag(listExamCandenceBagToDelete, listExamCandenceBagToInsert, listExamDetachableBagToDelete, listExamDetachableBagToInsert);
                // Ghi log
                SetViewDataActionAudit(String.Empty, String.Empty
                    , subjectID.ToString()
                    , Res.Get("ExamDetachableBag_Label_AutoScaleLegend")
                    , "Examinations: " + examinationsID + ", ExamGroup: " + examGroupID + ", SubjectID: " + subjectID
                    , Res.Get("ExamDetachableBag_Label_AutoScaleLegend")
                    , SMAS.Business.Common.GlobalConstants.ACTION_ADD
                    , string.Format(Res.Get("ExamDetachableBag_WriteLog_Description"), subjectID, examGroupID, examinationsID));

                return Json(new JsonMessage(Res.Get("ExamDetachableBag_Message_Success")));
            }
            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(long examinationsID, long examGroupID, int subjectID, int scale)
        {
            if (GetMenupermission("ExamDetachableBag", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            //Danh sach cac tui phach cu delete
            List<ExamCandenceBag> listExamCandenceBagToDelete = new List<ExamCandenceBag>();

            //Danh sach cac phach cu delete
            List<ExamDetachableBag> listExamDetachableBagToDelete = new List<ExamDetachableBag>();

            //Danh sach tui phach cua ky thi
            List<ExamCandenceBag> listExamCandenceBagOfExaminations = this.ExamCandenceBagBusiness.All.Where(o => o.ExaminationsID == examinationsID).ToList();

            //Danh sach phach cua ky thi
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<ExamDetachableBag> listExamDetachableBagOfExaminations = this.ExamDetachableBagBusiness.All.Where(o => o.LastDigitSchoolID == partition)
                                                                                       .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID.Value)
                                                                                       .Where(o => o.ExaminationsID == examinationsID).ToList();

            //Xoa phach cho mon thi hien tai
            if (scale == ExamDetachableBagConstants.AUTO_SCALE_CURRENT_SUBJECT)
            {
                this.DeleteDetachbleBagHandle(examGroupID, subjectID,listExamCandenceBagOfExaminations,listExamDetachableBagOfExaminations,listExamCandenceBagToDelete,
                                                listExamDetachableBagToDelete);
            }
            //Danh phach cho nhom thi hien tai
            else if (scale == ExamDetachableBagConstants.AUTO_SCALE_CURRENT_EXAM_GROUP)
            {
                //Lay danh sach mon thi cua nhom thi
                List<ExamSubject> listExamSubject = ExamSubjectBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                            .Where(o => o.ExamGroupID == examGroupID)
                                                                            .ToList();
                for (int i = 0; i < listExamSubject.Count; i++)
                {
                    this.DeleteDetachbleBagHandle(examGroupID, listExamSubject[i].SubjectID, listExamCandenceBagOfExaminations, listExamDetachableBagOfExaminations, listExamCandenceBagToDelete,
                                                listExamDetachableBagToDelete);
                }
            }

            //Danh phach cho tat ca nhom thi
            else if (scale == ExamDetachableBagConstants.AUTO_SCALE_ALL_SUBJECT_ALL_EXAM_GROUP)
            {
                //Lay danh sach nhom thi 
                List<ExamGroup> listExamGroup = ExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                    .ToList();

                //Lay danh sach mon thi cua ky thi
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = examinationsID;
                List<ExamSubjectBO> listExamSubjectOfExam = ExamSubjectBusiness.GetListExamSubject(dic).ToList();

                for (int i = 0; i < listExamGroup.Count; i++)
                {
                    ExamGroup group = listExamGroup[i];
                    //Lay danh sach mon thi cua nhom thi
                    List<ExamSubjectBO> listExamSubject = listExamSubjectOfExam.Where(o => o.ExamGroupID == group.ExamGroupID).OrderBy(o => o.OrderInSubject).ToList();
                    for (int j = 0; j < listExamSubject.Count; j++)
                    {
                        this.DeleteDetachbleBagHandle(group.ExamGroupID, listExamSubject[j].SubjectID, listExamCandenceBagOfExaminations, listExamDetachableBagOfExaminations, listExamCandenceBagToDelete,
                                                listExamDetachableBagToDelete); 
                    }
                }
            }

            //this.ExamCandenceBagBusiness.DeleteList(listExamCandenceBagToDelete);
            //this.ExamDetachableBagBusiness.DeleteList(listExamDetachableBagToDelete);
            this.ExamDetachableBagBusiness.DeleteAllExamDetachableBag(listExamCandenceBagToDelete, listExamDetachableBagToDelete);

            // Ghi log
            //ghi log
            SetViewDataActionAudit(String.Empty, String.Empty
            , String.Empty
            , Res.Get("ExamDetachableBag_Log_Delete")
            , String.Empty
            , Res.Get("ExamDetachableBag_Log_FunctionName")
            , SMAS.Business.Common.GlobalConstants.ACTION_DELETE
            , Res.Get("ExamDetachableBag_Log_Delete"));


            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamSubject(long examinationsID, long? examGroupID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            List<ExamSubjectBO> list = ExamSubjectBusiness.GetListExamSubject(dic).ToList();

            return Json(new SelectList(list, "SubjectID", "SubjectName"));
        }

         public JsonResult AjaxLoadExamCandenceBag(long examinationsID, long? examGroupID, int? subjectID)
        {
            List<ExamCandenceBag> list = ExamCandenceBagBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                 .Where(o => o.ExamGroupID == examGroupID)
                                                                 .Where(o => o.SubjectID == subjectID)
                                                                 .OrderBy(o => o.ExamCandenceBagCode)
                                                                 .ToList();

            var tempList = new List<Object>();
            tempList.Add(new { key = "", value = "Chưa xếp túi" });
            tempList.AddRange(list.Select(o => new { key = o.ExamCandenceBagID, value = o.ExamCandenceBagCode }).ToList());

            return Json(new SelectList(tempList, "key", "value"));
        } 
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["SubjectID"] = form.SubjectID;

        string type = JsonReportMessage.NEW;
        ProcessedReport processedReport = null;

        Stream excel = ExamDetachableBagBusiness.CreateExamDetachableBagReport(dic);
        processedReport = ExamDetachableBagBusiness.InsertExamDetachableBagReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
        excel.Close();
             
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel form)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["SubjectID"] = form.SubjectID;

            Stream excel = ExamDetachableBagBusiness.CreateExamDetachableBagReport(dic);
            ProcessedReport processedReport = ExamDetachableBagBusiness.InsertExamDetachableBagReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult ExportTemplateDemo(int ExaminationID, int SubjectID, int ExamGroupID, int TypeExportID, int CheckedExamCandenceBag)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = ExaminationID;
            dic["ExamGroupID"] = ExamGroupID;
            dic["SubjectID"] = SubjectID;
            dic["TypeExportID"] = TypeExportID;
            bool isChecked = CheckedExamCandenceBag == 1 ? true : false;
            dic["isExamCandenceBag"] = isChecked;
            Stream excel = ExamDetachableBagBusiness.CreateTemplateDemo(dic);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "THI_DanhPhach_ThiHocKy1.xls";
            return result;
        }
        [ValidateAntiForgeryToken]
        public JsonResult ImportExamDetachableBag(IEnumerable<HttpPostedFileBase> attachments, long ExaminationID, long ExamGroupID, int SubjectID, int TypeImportID)
        {
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FileImportCollection"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<ExamPupilViewModel> lstImport = new List<ExamPupilViewModel>();
                lstImport = this.GetDataToFile(oBook, ExaminationID, ExamGroupID, SubjectID, TypeImportID);
                if (lstImport == null)
                {
                    return Json(new JsonMessage("File import không đúng mẫu.", "error"));     
                }

                if (lstImport.Where(p=>p.isError).Count() > 0)
                {
                    Session["lstImportErr"] = lstImport;
                    return Json(new JsonMessage("Có lỗi trong quá trình Import", "GridViewErr"));
                }
                else//khong co loi thuc hien import
                {
                    this.InsertExamDetachableBag(lstImport, ExaminationID, ExamGroupID, SubjectID, TypeImportID);
                }
            }
            return Json(new JsonMessage("Import thành công", "success"));
        }
        public FileResult AjaxDownloadFileErr()
        {
            string physicalPath = (string)Session["FileImportCollection"];
            IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
            List<IVTWorksheet> lstSheet = oBook.GetSheets();
            List<ExamPupilViewModel> lstEvaluationGridModel = (List<ExamPupilViewModel>)Session["lstImportErr"];
            ExamPupilViewModel objEP = null;
            IVTWorksheet sheet = null;
            int startRow = 10;
            string sheetname = string.Empty;
            string strExamineeNumber = string.Empty;
            for (int i = 0; i < lstSheet.Count; i++)
            {
                sheet = lstSheet[i];
                sheetname = sheet.Name;
                //fill cot mo ta loi
                startRow = 10;
                while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null || sheet.GetCellValue(startRow, 4) != null)
                {
                    strExamineeNumber = sheet.GetCellValue(startRow, 2).ToString();
                    objEP = lstEvaluationGridModel.Where(p => p.ExamineeNumber.Equals(strExamineeNumber)).FirstOrDefault();
                    if (objEP != null)
                    {
                        sheet.SetCellValue(startRow, 5, objEP.ErrorMessage);
                        sheet.GetRange(startRow, 5, startRow, 5).WrapText();
                    }
                    startRow++;
                }
                sheet.UnHideColumn(5);
            }
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "THI_DanhPhach_ThiHocKy1.xls";
            return result;
        }
        [ValidateAntiForgeryToken]
        public JsonResult AjaxImportDataSuccess(int ExaminationID, int ExamGroupID, int SubjectID, int TypeImportID)
        {
            List<ExamPupilViewModel> lstEvaluationGridModel = (List<ExamPupilViewModel>)Session["lstImportErr"];
            lstEvaluationGridModel = lstEvaluationGridModel.Where(p => !p.isError).ToList();
            this.InsertExamDetachableBag(lstEvaluationGridModel, ExaminationID, ExamGroupID, SubjectID, TypeImportID);
            return Json(new JsonMessage("Import thành công", "success"));
        }
        private List<ExamPupilViewModel> GetDataToFile(IVTWorkbook obook, long ExaminationID, long ExamGroupID, int SubjectID, int TypeImportID)
        {
            List<ExamPupilViewModel> lstExamPupilVM = new List<ExamPupilViewModel>();
            ExamPupilViewModel objEPVM = null;
            List<IVTWorksheet> lstSheets = obook.GetSheets();
            IVTWorksheet sheet = null;
            string strVal = string.Empty;
            bool isErr = false;
            string ErrMessage = string.Empty;
            List<DetachBagCode> lstDetachBagCode = new List<DetachBagCode>();
            DetachBagCode objDetachBagCode = null;
            string DetachBag = string.Empty;
            string CandenceBag = string.Empty;
            List<ExamPupil> lstExamPupil = ExamPupilBusiness.GetExamPupilOfExaminations(ExaminationID, _globalInfo.SchoolID.Value,
                                                                                                _globalInfo.AcademicYearID.Value, (_globalInfo.SchoolID.Value % 100)).ToList();
            ExamPupil objExamPupil = null;
            for (int i = 0; i < lstSheets.Count; i++)
			{
			    sheet = lstSheets[i];
                int startRow = 10;
                while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null || sheet.GetCellValue(startRow, 4) != null)
                {
                    objEPVM = new ExamPupilViewModel();
                    objDetachBagCode = new DetachBagCode();
                    isErr = false;
                    ErrMessage = string.Empty;
                    strVal = sheet.GetCellValue("F2") != null ? sheet.GetCellValue("F2").ToString() : "";
                    if (string.IsNullOrEmpty(strVal))
                    {
                        return null;
                    }
                    else
                    {
                        objEPVM.ExaminationID = ExaminationID;
                    }
                    objEPVM.ExamGroupID = !string.IsNullOrEmpty(strVal) ? long.Parse(strVal) : 0;
                    strVal = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : "";
                    objExamPupil = lstExamPupil.Where(p => p.ExamGroupID == objEPVM.ExamGroupID && p.ExamineeNumber == strVal).FirstOrDefault();
                    if (objExamPupil == null)
                    {
                        isErr = true;
                        ErrMessage += "Số báo danh không tồn tại.";
                    }
                    else
                    {
                        objEPVM.ExamPupilID = objExamPupil.ExamPupilID;
                    }
                    objEPVM.ExamineeNumber = strVal;
                    strVal = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString() : "";
                    DetachBag = strVal;
                    if (string.IsNullOrEmpty(strVal))
                    {
                        isErr = true;
                        ErrMessage += "Túi phách không được để trống.";
                    }
                    else
                    {
                        if (DetachBag.Length > 10)
                        {
                            isErr = true;
                            ErrMessage += "Túi phách không hợp lệ.";
                        }
                    }
                    objEPVM.ExamCandenceBagCode = strVal;
                    strVal = sheet.GetCellValue(startRow, 4) != null ? sheet.GetCellValue(startRow, 4).ToString() : "";
                    CandenceBag = strVal;
                    if (string.IsNullOrEmpty(strVal))
                    {
                        isErr = true;
                        ErrMessage += "Số phách không được để trống.";
                    }
                    else
                    {
                        if (CandenceBag.Length > 10)
                        {
                            isErr = true;
                            ErrMessage += "Số phách không hợp lệ.";
                        }
                    }
                    objDetachBagCode.Key = DetachBag;
                    objDetachBagCode.Value = CandenceBag;
                    if (!string.IsNullOrEmpty(CandenceBag))
                    {
                        lstDetachBagCode.Add(objDetachBagCode);
                        if (lstDetachBagCode.Where(p => p.Key == DetachBag && p.Value == CandenceBag).Count() > 1)
                        {
                            isErr = true;
                            ErrMessage += "Túi thi đã tồn tại số phách này.";
                        }
                    }
                    objEPVM.ExamDetachableBagCode = strVal;
                    objEPVM.isError = isErr;
                    objEPVM.ErrorMessage = ErrMessage;
                    lstExamPupilVM.Add(objEPVM);
                    startRow++;
                }
			}
            
            return lstExamPupilVM;
        }
        private void InsertExamDetachableBag(List<ExamPupilViewModel> lstExamPupilVM, long ExaminationID, long ExamGroupID, int SubjectID, int TypeImportID)
        {
            List<ExamCandenceBag> lstExamCandenceBagDelete = new List<ExamCandenceBag>();
            List<ExamCandenceBag> lstExamCandenceBagInsert = new List<ExamCandenceBag>();
            List<ExamDetachableBag> lstExamExamDetachableBagDelete = new List<ExamDetachableBag>();
            List<ExamDetachableBag> lstExamDetachableBagInsert = new List<ExamDetachableBag>();
            int PartitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            ExamPupilViewModel objEPVM = null;
            List<ExamPupilViewModel> lstExamPupilExcel = new List<ExamPupilViewModel>();
            //Danh sach tui phach cua ky thi
            List<ExamCandenceBag> listExamCandenceBagOfExaminations = this.ExamCandenceBagBusiness.All.Where(o => o.ExaminationsID == ExaminationID).ToList();

            //Danh sach phach cua ky thi
            List<ExamDetachableBag> listExamDetachableBagOfExaminations = this.ExamDetachableBagBusiness.All.Where(o => o.LastDigitSchoolID == PartitionID)
                                                                                       .Where(o => o.AcademicYearID == _globalInfo.AcademicYearID.Value)
                                                                                       .Where(o => o.ExaminationsID == ExaminationID).ToList();
            #region Danh phach cho mon thi hien tai
            if (TypeImportID == 1)
            {
                //Tao danh sach delete
                //Lay cac tui phach cu va so phach cu trong nhom thi, mon thi
                List<ExamCandenceBag> listExamCandenceBag = listExamCandenceBagOfExaminations.Where(o => o.ExamGroupID == ExamGroupID && o.SubjectID == SubjectID).ToList();
                lstExamCandenceBagDelete.AddRange(listExamCandenceBag);
                List<ExamDetachableBag> listExamDetachableBag = listExamDetachableBagOfExaminations.Where(o => o.ExamGroupID == ExamGroupID && o.SubjectID == SubjectID).ToList();
                lstExamExamDetachableBagDelete.AddRange(listExamDetachableBag);
                //lay danh sach tui phach
                List<string> lstCandenceBagCode = lstExamPupilVM.Where(p=>p.ExamGroupID == ExamGroupID).Select(p => p.ExamCandenceBagCode).Distinct().ToList();
                string ExamCandenceBagCode = string.Empty;
                for (int i = 0; i < lstCandenceBagCode.Count; i++)
                {
                    ExamCandenceBagCode = lstCandenceBagCode[i];
                    lstExamPupilExcel = lstExamPupilVM.Where(o => o.ExamGroupID == ExamGroupID && o.ExamCandenceBagCode == ExamCandenceBagCode)
                                                                                    .OrderBy(o => o.ExamineeNumber)
                                                                                    .ToList();
                    ExamCandenceBag examCandenceBag = new ExamCandenceBag();
                    examCandenceBag.ExaminationsID = ExaminationID;
                    examCandenceBag.ExamGroupID = ExamGroupID;
                    examCandenceBag.ExamCandenceBagID = ExamCandenceBagBusiness.GetNextSeq<long>();
                    examCandenceBag.ExamCandenceBagCode = ExamCandenceBagCode;
                    examCandenceBag.SubjectID = SubjectID;
                    examCandenceBag.CreateTime = DateTime.Now;
                    examCandenceBag.UpdateTime = null;
                    lstExamCandenceBagInsert.Add(examCandenceBag);
                }

                ExamDetachableBagBusiness.InsertDeleteExamCandenceBag(lstExamCandenceBagDelete, lstExamCandenceBagInsert);
                //lay ID tui thi vua insert de them vao phach
                List<ExamCandenceBag> lstExamCandenceBag = new List<ExamCandenceBag>();
                lstExamCandenceBag = ExamCandenceBagBusiness.All.Where(p => p.ExaminationsID == ExaminationID && p.ExamGroupID == ExamGroupID && p.SubjectID == SubjectID
                                                                       && lstCandenceBagCode.Contains(p.ExamCandenceBagCode)).ToList();
                ExamCandenceBag objExamCandenceBag = null;
                for (int i = 0; i < lstCandenceBagCode.Count; i++)
                {
                    ExamCandenceBagCode = lstCandenceBagCode[i];
                    lstExamPupilExcel = lstExamPupilVM.Where(o => o.ExamGroupID == ExamGroupID && o.ExamCandenceBagCode == ExamCandenceBagCode)
                                                                                    .OrderBy(o => o.ExamineeNumber)
                                                                                    .ToList();
                    objExamCandenceBag = lstExamCandenceBag.Where(p => p.ExamCandenceBagCode == ExamCandenceBagCode).FirstOrDefault();
                    for (int j = 0; j < lstExamPupilExcel.Count; j++)
                    {
                        objEPVM = lstExamPupilExcel[j];
                        //Danh phach cho bai thi
                        ExamDetachableBag edb = new ExamDetachableBag();
                        edb.CreateTime = DateTime.Now;
                        edb.ExamCandenceBagID = objExamCandenceBag.ExamCandenceBagID;
                        edb.ExamGroupID = ExamGroupID;
                        edb.ExaminationsID = ExaminationID;
                        edb.ExamPupilID = objEPVM.ExamPupilID;
                        edb.LastDigitSchoolID = PartitionID;
                        edb.SubjectID = SubjectID;
                        edb.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        edb.UpdateTime = null;
                        edb.ExamDetachableBagCode = objEPVM.ExamDetachableBagCode;
                        lstExamDetachableBagInsert.Add(edb);
                    }
                }
            }
            #endregion
            #region Danh phach cho nhom thi hien tai
            else if (TypeImportID == 2)
            {
                //Lay cac tui phach cu va so phach cu trong nhom thi, mon thi
                List<ExamCandenceBag> listExamCandenceBag = listExamCandenceBagOfExaminations.Where(o => o.ExamGroupID == ExamGroupID).ToList();
                lstExamCandenceBagDelete.AddRange(listExamCandenceBag);
                //lay danh sach tui phach
                List<string> lstCandenceBagCode = lstExamPupilVM.Select(p => p.ExamCandenceBagCode).Distinct().ToList();
                string ExamBagCode = string.Empty;
                List<ExamSubject> listExamSubject = ExamSubjectBusiness.All.Where(o => o.ExaminationsID == ExaminationID)
                                                                                    .Where(o => o.ExamGroupID == ExamGroupID)
                                                                                    .ToList();
                ExamSubject objES = null;
                for (int i = 0; i < listExamSubject.Count; i++)
                {
                    objES = listExamSubject[i];

                    for (int j = 0; j < lstCandenceBagCode.Count; j++)
                    {
                        ExamBagCode = lstCandenceBagCode[j];
                        ExamCandenceBag examCandenceBag = new ExamCandenceBag();
                        examCandenceBag.ExaminationsID = ExaminationID;
                        examCandenceBag.ExamGroupID = ExamGroupID;
                        examCandenceBag.ExamCandenceBagCode = ExamBagCode;
                        examCandenceBag.SubjectID = objES.SubjectID;
                        examCandenceBag.CreateTime = DateTime.Now;
                        examCandenceBag.UpdateTime = null;
                        lstExamCandenceBagInsert.Add(examCandenceBag);
                    }
                }

                ExamDetachableBagBusiness.InsertDeleteExamCandenceBag(lstExamCandenceBagDelete, lstExamCandenceBagInsert);
                //lay ID tui thi vua insert de them vao phach
                List<ExamCandenceBag> lstExamCandenceBag = new List<ExamCandenceBag>();
                lstExamCandenceBag = ExamCandenceBagBusiness.All.Where(p => p.ExaminationsID == ExaminationID && p.ExamGroupID == ExamGroupID
                                                                       && lstCandenceBagCode.Contains(p.ExamCandenceBagCode)).ToList();
                ExamCandenceBag objExamCandenceBag = null;

                for (int i = 0; i < listExamSubject.Count; i++)
                {
                    objES = listExamSubject[i];

                    for (int j = 0; j < lstCandenceBagCode.Count; j++)
                    {
                        ExamBagCode = lstCandenceBagCode[j];
                        lstExamPupilExcel = lstExamPupilVM.Where(o => o.ExamGroupID == ExamGroupID && o.ExamCandenceBagCode == ExamBagCode).ToList();
                        objExamCandenceBag = lstExamCandenceBag.Where(p => p.SubjectID == objES.SubjectID && p.ExamCandenceBagCode == ExamBagCode).FirstOrDefault();
                        for (int k = 0; k < lstExamPupilExcel.Count; k++)
                        {
                            objEPVM = lstExamPupilExcel[k];
                            //Danh phach cho bai thi
                            ExamDetachableBag edb = new ExamDetachableBag();
                            edb.CreateTime = DateTime.Now;
                            edb.ExamCandenceBagID = objExamCandenceBag.ExamCandenceBagID;
                            edb.ExamGroupID = ExamGroupID;
                            edb.ExaminationsID = ExaminationID;
                            edb.ExamPupilID = objEPVM.ExamPupilID;
                            edb.LastDigitSchoolID = PartitionID;
                            edb.SubjectID = objES.SubjectID;
                            edb.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            edb.UpdateTime = null;
                            edb.ExamDetachableBagCode = objEPVM.ExamDetachableBagCode;
                            lstExamDetachableBagInsert.Add(edb);
                        }
                    }
                }
                List<ExamDetachableBag> listExamDetachableBag = listExamDetachableBagOfExaminations.Where(o => o.ExamGroupID == ExamGroupID).ToList();
                lstExamExamDetachableBagDelete.AddRange(listExamDetachableBag);
            }
            #endregion
            #region Danh phach cho toan bo nhom thi va mon thi
            else
            {
                string ExamBagCode = string.Empty;
                lstExamCandenceBagDelete.AddRange(listExamCandenceBagOfExaminations);
                lstExamExamDetachableBagDelete.AddRange(listExamDetachableBagOfExaminations);
                List<ExamGroup> listExamGroup = ExamGroupBusiness.All.Where(o => o.ExaminationsID == ExaminationID).ToList();
                List<long> lstExamGroupID = listExamGroup.Select(p => p.ExamGroupID).Distinct().ToList();
                List<ExamSubject> listExamSubject = ExamSubjectBusiness.All.Where(o => o.ExaminationsID == ExaminationID && lstExamGroupID.Contains(o.ExamGroupID)).ToList();
                ExamSubject objES = null;
                List<string> lstExamBagCodeAll = lstExamPupilVM.Select(p=>p.ExamCandenceBagCode).Distinct().ToList();

                //Lay danh sach nhom thi 
                for (int i = 0; i < listExamGroup.Count; i++)
                {
                    ExamGroup group = listExamGroup[i];
                    //Lay danh sach mon thi cua nhom thi
                    List<ExamSubject> lstExamSubjecttmp = listExamSubject.Where(o => o.ExamGroupID == group.ExamGroupID).ToList();
                    List<string> lstCandenceBagCode = lstExamPupilVM.Where(p=>p.ExamGroupID == group.ExamGroupID).Select(p => p.ExamCandenceBagCode).Distinct().ToList();
                    
                    for (int j = 0; j < lstExamSubjecttmp.Count; j++)
                    {
                        objES = lstExamSubjecttmp[j];
                        for (int k = 0; k < lstCandenceBagCode.Count; k++)
                        {
                            ExamBagCode = lstCandenceBagCode[k];
                            ExamCandenceBag examCandenceBag = new ExamCandenceBag();
                            examCandenceBag.ExaminationsID = ExaminationID;
                            examCandenceBag.ExamGroupID = group.ExamGroupID;
                            examCandenceBag.ExamCandenceBagCode = ExamBagCode;
                            examCandenceBag.SubjectID = objES.SubjectID;
                            examCandenceBag.CreateTime = DateTime.Now;
                            examCandenceBag.UpdateTime = null;
                            lstExamCandenceBagInsert.Add(examCandenceBag);
                        }
                    }
                }

                ExamDetachableBagBusiness.InsertDeleteExamCandenceBag(lstExamCandenceBagDelete, lstExamCandenceBagInsert);

                //lay ID tui thi vua insert de them vao phach
                List<ExamCandenceBag> lstExamCandenceBag = new List<ExamCandenceBag>();
                lstExamCandenceBag = ExamCandenceBagBusiness.All.Where(p => p.ExaminationsID == ExaminationID && lstExamGroupID.Contains(p.ExamGroupID)
                                                                       && lstExamBagCodeAll.Contains(p.ExamCandenceBagCode)).ToList();
                ExamCandenceBag objExamCandenceBag = null;

                for (int i = 0; i < listExamGroup.Count; i++)
                {
                    ExamGroup group = listExamGroup[i];
                    //Lay danh sach mon thi cua nhom thi
                    List<ExamSubject> lstExamSubjecttmp = listExamSubject.Where(o => o.ExamGroupID == group.ExamGroupID).ToList();
                    List<string> lstCandenceBagCode = lstExamPupilVM.Where(p => p.ExamGroupID == group.ExamGroupID).Select(p => p.ExamCandenceBagCode).Distinct().ToList();

                    for (int j = 0; j < lstExamSubjecttmp.Count; j++)
                    {
                        objES = lstExamSubjecttmp[j];
                        for (int k = 0; k < lstCandenceBagCode.Count; k++)
                        {
                            ExamBagCode = lstCandenceBagCode[k];
                            lstExamPupilExcel = lstExamPupilVM.Where(o => o.ExamGroupID == ExamGroupID && o.ExamCandenceBagCode == ExamBagCode)
                                                                                    .OrderBy(o => o.ExamineeNumber)
                                                                                    .ToList();
                            objExamCandenceBag = lstExamCandenceBag.Where(p => p.ExamGroupID == group.ExamGroupID && p.SubjectID == objES.SubjectID && p.ExamCandenceBagCode == ExamBagCode).FirstOrDefault();
                            for (int l = 0; l < lstExamPupilExcel.Count; l++)
                            {
                                objEPVM = lstExamPupilExcel[l];
                                //Danh phach cho bai thi
                                ExamDetachableBag edb = new ExamDetachableBag();
                                edb.CreateTime = DateTime.Now;
                                edb.ExamCandenceBagID = objExamCandenceBag.ExamCandenceBagID;
                                edb.ExamGroupID = group.ExamGroupID;
                                edb.ExaminationsID = ExaminationID;
                                edb.ExamPupilID = objEPVM.ExamPupilID;
                                edb.LastDigitSchoolID = PartitionID;
                                edb.SubjectID = objES.SubjectID;
                                edb.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                edb.UpdateTime = null;
                                edb.ExamDetachableBagCode = objEPVM.ExamDetachableBagCode;
                                lstExamDetachableBagInsert.Add(edb);
                            }
                        }
                    }
                }
            }
            #endregion
            ExamDetachableBagBusiness.InsertDeleteExamDetachableBag(lstExamExamDetachableBagDelete,lstExamDetachableBagInsert);
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamDetachableBagConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamDetachableBagConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;

            //Lay danh sach mon thi
            if (defaultExamGroupID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = defaultExamID;
                dic["ExamGroupID"] = defaultExamGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).ToList();
            }
            else
            {
                listExamSubject = new List<ExamSubjectBO>();
            }
            ViewData[ExamDetachableBagConstants.CBO_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");
            long? defaultSubjectID = null;
            if (listExamSubject.Count > 0) defaultSubjectID = listExamSubject.First().SubjectID;

            //Lay danh sach tui phach
            if (defaultSubjectID != null)
            {
                listExamCandenceBag = ExamCandenceBagBusiness.All.Where(o => o.ExaminationsID == defaultExamID)
                                                                 .Where(o => o.ExamGroupID == defaultExamGroupID)
                                                                 .Where(o => o.SubjectID == defaultSubjectID)
                                                                 .OrderBy(o=>o.ExamCandenceBagCode)
                                                                 .ToList();
            }
            else
            {
                listExamCandenceBag = new List<ExamCandenceBag>();
            }

            var tempList = new List<Object>();
            tempList.Add(new { key = "", value = "Chưa xếp túi" });
            tempList.AddRange(listExamCandenceBag.Select(o => new { key = o.ExamCandenceBagID, value = o.ExamCandenceBagCode }).ToList());
            ViewData[ExamDetachableBagConstants.CBO_EXAM_CANDENCE_BAG] = new SelectList(tempList, "key", "value");

        }


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ListViewModel> _Search(long examinationsID, long examGroupID, long subjectID, long? examCandenceBagID)
        {
            //Lay danh sach thi sinh
            //List <ExamPupil> listExamPupil= this.ExamPupilBusiness.All.Where(o => o.ExaminationsID == examinationsID)
            //                                            .Where(o => o.ExamGroupID == examGroupID)
            //                                            .ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //Tim kiem 
            List<ExamPupilBO> listExamPupil = this.ExamPupilBusiness.Search(dic, _globalInfo.AcademicYearID.GetValueOrDefault(), partitionID).ToList();


            //Lay danh sach danh phach
            List<ExamDetachableBag> listDetachable = this.ExamDetachableBagBusiness.All.Where(o => o.ExaminationsID == examinationsID
                                                                                            && o.ExamGroupID == examGroupID
                                                                                            && o.SubjectID == subjectID)
                                                                                            .ToList();
            IEnumerable<ListViewModel> ret=(from ep in listExamPupil
                                            join ed in listDetachable
                                                   on ep.ExamPupilID equals ed.ExamPupilID into des
                                               from x in des.DefaultIfEmpty()
                                               select new ListViewModel
                                               {
                                                   ExamineeNumber=ep.ExamineeNumber,
                                                   ExamDetachableBagCode=x!=null?x.ExamDetachableBagCode:String.Empty,
                                                   ExamDetachableBagID=x!=null?x.ExamDetachableBagID:0,
                                                   ExamCandenceBagID=x!=null?x.ExamCandenceBagID:0
                                               });

            if (examCandenceBagID == null)
            {
                ret = ret.Where(o => o.ExamDetachableBagID == 0);
            }
            else
            {
                ret = ret.Where(o => o.ExamCandenceBagID == examCandenceBagID);
            }

            ret = ret.OrderBy(o => o.ExamineeNumber);
            return ret;
        }

        private void AutoDetachableByExamBagHandle(AutoDetachableViewModel form,List<ExamCandenceBag> listExamCandenceBagToInsert,
                                                   List<ExamDetachableBag> listExamDetachableBagToInsert,List<ExamPupil> listExamPupilOfExaminations,
                                                   List<ExamBagBO> listExamBagOfExaminations, List<ExamCandenceBag> listExamCandenceBagOfExaminations,
                                                   List<ExamDetachableBag> listExamDetachableBagOfExaminations, List<ExamCandenceBag> listExamCandenceBagToDelete,
                                                   List<ExamDetachableBag> listExamDetachableBagToDelete)
        {
            bool includeExamBagCheck = form.includeExamBagCodeCheck;
            bool lengthCheck = form.lengthCheck;
            long examinationsID = form.examinationsID;
            long examGroupID = form.examGroupID;
            int subjectID = form.subjectID;
            int length = 0;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            if (!String.IsNullOrEmpty(form.lengthNum))
            {
                length = Convert.ToInt32(form.lengthNum);
            }

            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //Lay cac tui thi trong nhom thi, mon thi
            List<ExamBagBO> listExamBag = listExamBagOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                 .Where(o => o.SubjectID == subjectID)
                                                                 .OrderBy(o => o.ExamRoomCode).ToList();

            if (listExamBag.Count == 0)
            {
                throw new BusinessException("Thầy/cô chưa đánh mã túi thi");
            }
            for (int i = 0; i < listExamBag.Count; i++)
            {
                ExamBagBO examBag = listExamBag[i];
                //Tao tui phach
                ExamCandenceBag examCandenceBag = new ExamCandenceBag();
                examCandenceBag.ExaminationsID = examinationsID;
                examCandenceBag.ExamGroupID = examGroupID;
                examCandenceBag.ExamCandenceBagID = ExamCandenceBagBusiness.GetNextSeq<long>();
                examCandenceBag.ExamCandenceBagCode = examBag.ExamBagCode;
                examCandenceBag.SubjectID = subjectID;
                examCandenceBag.CreateTime = DateTime.Now;
                examCandenceBag.UpdateTime = null;
                listExamCandenceBagToInsert.Add(examCandenceBag);

                //Lay danh sach thi sinh trong tui bai thi
                long examRoomID = examBag.ExamRoomID;

                List<ExamPupil> listExamPupilOfExamBag = listExamPupilOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                    .Where(o => o.ExamRoomID == examRoomID)
                                                                                    .OrderBy(o => o.ExamineeNumber)
                                                                                    .ToList();
                Random rnd = new Random();
                Random rnd1 = new Random();
                //Danh sach so thu tu
                List<int> listSequenceNum = new List<int>();
                for (int j = 1; j <= listExamPupilOfExamBag.Count; j++)
                {
                    listSequenceNum.Add(j);
                }

               // int numLength = lengthCheck && length!=0 ? length : listExamPupilOfExamBag.Count.ToString().Length;
                //So bai thi trong tui bai thi
                int examPupilCount=listExamPupilOfExamBag.Count;
                //Chieu dai danh so
                int numLength;
                if (lengthCheck)
                {
                    if (length < examPupilCount.ToString().Length)
                    {
                        throw new BusinessException("ExamDetachableBag_Validate_Numlength");
                    }
                    numLength = length;
                }
                else
                {
                    numLength = examPupilCount.ToString().Length;
                }
                for (int j = 0; j < listExamPupilOfExamBag.Count;j++ )
                {
                    //Lay ra bai thi ngau nhien

                    ExamPupil ep = listExamPupilOfExamBag[j];

                    //Danh phach cho bai thi
                    ExamDetachableBag edb = new ExamDetachableBag();
                    edb.CreateTime = DateTime.Now;
                    edb.ExamCandenceBagID = examCandenceBag.ExamCandenceBagID;
                    edb.ExamDetachableBagID = ExamDetachableBagBusiness.GetNextSeq<long>();
                    edb.ExamGroupID = examGroupID;
                    edb.ExaminationsID = examinationsID;
                    edb.ExamPupilID = ep.ExamPupilID;
                    edb.LastDigitSchoolID = partition;
                    edb.SubjectID = subjectID;
                    edb.AcademicYearID = academicYearId;
                    edb.UpdateTime = null;
                    //Tao so phach
                    //Lay ngau nhien so thu tu trong danh sach so thu tu
                    int rdSequenceIndex = rnd1.Next(listSequenceNum.Count);
                    int sequenceNum = listSequenceNum[rdSequenceIndex];
                    listSequenceNum.Remove(sequenceNum);

                    String examDetachableBagCode;
                    //Khong tich chon tieu chi nao
                    if (includeExamBagCheck)
                    {
                        examDetachableBagCode = examBag.ExamBagCode + sequenceNum.ToString().PadLeft(numLength, '0');
                    }
                    else
                    {
                        examDetachableBagCode = sequenceNum.ToString().PadLeft(numLength, '0');
                    }

                    edb.ExamDetachableBagCode = examDetachableBagCode;

                    listExamDetachableBagToInsert.Add(edb);
                }
            }

            //Lay cac tui phach cu va so phach cu trong nhom thi, mon thi
            List<ExamCandenceBag> listExamCandenceBag = listExamCandenceBagOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                      .Where(o => o.SubjectID == subjectID).ToList();
            listExamCandenceBagToDelete.AddRange(listExamCandenceBag);

            List<ExamDetachableBag> listExamDetachableBag = listExamDetachableBagOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                            .Where(o => o.SubjectID == subjectID).ToList();
            listExamDetachableBagToDelete.AddRange(listExamDetachableBag);
        }

        private void AutoDetachableRandomHandle(AutoDetachableViewModel form, List<ExamCandenceBag> listExamCandenceBagToInsert,
                                                   List<ExamDetachableBag> listExamDetachableBagToInsert, List<ExamPupil> listExamPupilOfExaminations,
                                                    List<ExamBagBO> listExamBagOfExaminations,List<ExamCandenceBag> listExamCandenceBagOfExaminations,
                                                   List<ExamDetachableBag> listExamDetachableBagOfExaminations, List<ExamCandenceBag> listExamCandenceBagToDelete, List<ExamDetachableBag> listExamDetachableBagToDelete)
        {
            long examinationsID = form.examinationsID;
            long examGroupID = form.examGroupID;
            int subjectID = form.subjectID;
            int candenceBagNum = Convert.ToInt32(form.candenceBagNum);
            string prefix = form.prefix;
            int startNum = Convert.ToInt32(form.startNum);
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);

            //Lay cac tui thi trong nhom thi, mon thi
            List<ExamBagBO> listExamBag = listExamBagOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                 .Where(o => o.SubjectID == subjectID)
                                                                 .OrderBy(o => o.ExamRoomCode).ToList();
            //Tao tui phach
            List<ExamCandenceBag> templistExamCandenceBagToInsert = new List<ExamCandenceBag>();
            int codeLength = candenceBagNum.ToString().Length;
            for (int i = 1; i <= candenceBagNum; i++)
            {
                ExamCandenceBag examCandenceBag = new ExamCandenceBag();
                examCandenceBag.CreateTime = DateTime.Now;
                examCandenceBag.ExamCandenceBagCode = "T" + i.ToString().PadLeft(codeLength,'0');
                examCandenceBag.ExamCandenceBagID = ExamCandenceBagBusiness.GetNextSeq<long>();
                examCandenceBag.ExamGroupID = examGroupID;
                examCandenceBag.ExaminationsID = examinationsID;
                examCandenceBag.SubjectID = subjectID;
                examCandenceBag.UpdateTime = null;
                templistExamCandenceBagToInsert.Add(examCandenceBag);
            }

            listExamCandenceBagToInsert.AddRange(templistExamCandenceBagToInsert);

            //Lay danh sach phong thi cua cac tui thi
            List<long> listExamRoomID = listExamBag.Select(o => o.ExamRoomID).ToList();

            //Lay danh sach tat ca thi sinh trong cac tui thi
            List<ExamPupil> listExamPupilOfExamBags = listExamPupilOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                    .Where(o => listExamRoomID.Contains(o.ExamRoomID.GetValueOrDefault()))
                                                                                    .OrderBy(o => o.ExamineeNumber)
                                                                                    .ToList();
            int examPupilNum = listExamPupilOfExamBags.Count;
            //So bai thi tren 1 tui phach
            int examPupilNumPerBag = examPupilNum % candenceBagNum % candenceBagNum == 0 ? examPupilNum / candenceBagNum : examPupilNum / candenceBagNum + 1;

            //Lay chieu dai danh so
            int numLength=(examPupilNum+startNum-1).ToString().Length;

            Random rnd = new Random();

            //Danh sach so thu tu
            List<int> listSequenceNum = new List<int>();
            for (int i = startNum; i <= examPupilNum + startNum - 1; i++)
            {
                listSequenceNum.Add(i);
            }

            List<ExamDetachableBag> tempListExamDetachableBag = new List<ExamDetachableBag>();
            for (int i = 0; i < listExamPupilOfExamBags.Count; i++)
            {
                ExamPupil ep = listExamPupilOfExamBags[i];

                //Danh phach cho bai thi
                ExamDetachableBag edb = new ExamDetachableBag();
                edb.CreateTime = DateTime.Now;
                //edb.ExamCandenceBagID = ecb.ExamCandenceBagID;
                edb.ExamDetachableBagID = ExamDetachableBagBusiness.GetNextSeq<long>();
                edb.ExamGroupID = examGroupID;
                edb.ExaminationsID = examinationsID;
                edb.ExamPupilID = ep.ExamPupilID;
                edb.LastDigitSchoolID = partition;
                edb.SubjectID = subjectID;
                edb.AcademicYearID = _globalInfo.AcademicYearID.Value;
                edb.UpdateTime = null;

                //Tao so phach
                //Lay ngau nhien so thu tu trong danh sach so thu tu
                int rdSequenceIndex = rnd.Next(listSequenceNum.Count);
                int sequenceNum = listSequenceNum[rdSequenceIndex];
                listSequenceNum.Remove(sequenceNum);

                string examDetachableBagCode = prefix + sequenceNum.ToString().PadLeft(numLength, '0');

                edb.ExamDetachableBagCode = examDetachableBagCode;

                tempListExamDetachableBag.Add(edb);
            }

            //Chia deu cac so phach vao cac tui phach
            DividedEqually(tempListExamDetachableBag, templistExamCandenceBagToInsert);
            listExamDetachableBagToInsert.AddRange(tempListExamDetachableBag);

            //Lay cac tui phach cu va so phach cu trong nhom thi, mon thi
            List<ExamCandenceBag> listExamCandenceBag =listExamCandenceBagOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                      .Where(o => o.SubjectID == subjectID).ToList();
            listExamCandenceBagToDelete.AddRange(listExamCandenceBag);

            List<ExamDetachableBag> listExamDetachableBag =listExamDetachableBagOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                            .Where(o => o.SubjectID == subjectID).ToList();
            listExamDetachableBagToDelete.AddRange(listExamDetachableBag);
        }

        private void DeleteDetachbleBagHandle(long examGroupID, int subjectID, List<ExamCandenceBag> listExamCandenceBagOfExaminations, List<ExamDetachableBag> listExamDetachableBagOfExaminations, List<ExamCandenceBag> listExamCandenceBagToDelete, List<ExamDetachableBag> listExamDetachableBagToDelete)
        {
            //Lay cac tui phach cu va so phachtrong nhom thi, mon thi
            List<ExamCandenceBag> listExamCandenceBag = listExamCandenceBagOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                      .Where(o => o.SubjectID == subjectID).ToList();
            listExamCandenceBagToDelete.AddRange(listExamCandenceBag);

            List<ExamDetachableBag> listExamDetachableBag = listExamDetachableBagOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                            .Where(o => o.SubjectID == subjectID).ToList();
            listExamDetachableBagToDelete.AddRange(listExamDetachableBag);
        }
        private void DividedEqually(List<ExamDetachableBag> listExamDetachable, List<ExamCandenceBag> listExamCandenceBag)
        {
            int index = 0;
            while (index < listExamDetachable.Count)
            {
                for (int i = 0; i < listExamCandenceBag.Count; i++)
                {
                    if (index == listExamDetachable.Count)
                    {
                        break;
                    }
                    ExamCandenceBag ecb = listExamCandenceBag[i];
                    ExamDetachableBag edb = listExamDetachable[index];
                    edb.ExamCandenceBagID = ecb.ExamCandenceBagID;
                    index++;
                }
            }
        }

        /// <summary>
        /// Kiem tra disable/enable, an/hien cac button
        /// </summary>
        /// <param name="listResult"></param>
        private void CheckCommandPermision(Examinations examinations)
        {
            SetViewDataPermission("ExamDetachableBag", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);

            bool checkButton = false;
            bool checkDelete = false;
            bool checkExport = false;
            if (examinations != null)
            {
                if (examinations.MarkInput != true && examinations.MarkClosing != true&& _globalInfo.IsCurrentYear)
                {
                    if((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE])
                        checkButton = true;

                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE])
                        checkDelete = true;
                }
                checkExport = true;
            }

            ViewData[ExamDetachableBagConstants.PER_CHECK_BUTTON] = checkButton;
            ViewData[ExamDetachableBagConstants.PER_CHECK_DELETE] = checkDelete;
            ViewData[ExamDetachableBagConstants.PER_CHECK_EXPORT] = checkExport;
            //Button xoa

        }

        private class DetachBagCode
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }
        #endregion

    }
}
