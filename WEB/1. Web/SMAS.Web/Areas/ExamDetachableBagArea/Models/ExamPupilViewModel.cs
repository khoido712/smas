﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamDetachableBagArea.Models
{
    public class ExamPupilViewModel
    {
        public long ExamPupilID { get; set; }
        public long ExaminationID { get; set; }
        public long ExamGroupID { get; set; }
        public string ExamineeNumber { get; set; }
        public string ExamCandenceBagCode { get; set; }
        public string ExamDetachableBagCode { get; set; }
        public int SubjectID { get; set; }
        public bool isError { get; set; }
        public string ErrorMessage { get; set; }
    }
}