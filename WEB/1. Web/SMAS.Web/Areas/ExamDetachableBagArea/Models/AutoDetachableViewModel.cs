﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamDetachableBagArea.Models
{
    public class AutoDetachableViewModel
    {
        //Tieu chi
        public int type { get; set; }
        //Danh phach theo tui bai thi
        public bool includeExamBagCodeCheck { get; set; }
        public bool lengthCheck { get; set; }

        [Range(1, 9, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamDetachableBag_Validate_PosInteger_Length")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [StringLength(1, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("ExamDetachableBag_Label_Length")]
        public string lengthNum { get; set; }

        //Danh phach ngau nhien
        [Range(1, 99, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamDetachableBag_Validate_PosInteger_DetachableBagNum")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("ExamDetachableBag_Label_DetachableBagNum")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamDetachableBag_Validate_Required_CandenceBagNum")]
        public string candenceBagNum { get; set; }

         [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamDetachableBag_Validate_Regex_Prefix")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("ExamDetachableBag_Label_Prefix")]
        public string prefix { get; set; }

        [Range(1, 999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamDetachableBag_Validate_PosInteger_StartNum")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [StringLength(3, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("ExamDetachableBag_Label_StartNum")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamDetachableBag_Validate_Required_StartNum")]
        public string startNum { get; set; }

        //Quy mo danh phach
        public int scale { get; set; }

        public long examinationsID { get; set; }
        public long examGroupID { get; set; }
        public int subjectID { get; set; }
    }
}