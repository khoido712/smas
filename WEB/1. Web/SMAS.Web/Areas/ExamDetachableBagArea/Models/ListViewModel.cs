﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamDetachableBagArea.Models
{
    public class ListViewModel
    {
        [ResourceDisplayName("ExamDetachableBag_Label_ExamineeNumber")]
        public string ExamineeNumber { get; set; }
        [ResourceDisplayName("ExamDetachableBag_Label_ExamDetachableBagCode")]
        public string ExamDetachableBagCode { get; set; }
        public long ExamDetachableBagID { get; set; }
        public long ExamCandenceBagID { get; set; }
    }
}