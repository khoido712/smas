﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ExamDetachableBagArea;

namespace SMAS.Web.Areas.ExamDetachableBagArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamDetachableBag_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamDetachableBagConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public long? ExaminationsID { get; set; }

        [ResourceDisplayName("ExamDetachableBag_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamDetachableBagConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public long? ExamGroupID { get; set; }

        [ResourceDisplayName("ExamDetachableBag_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamDetachableBagConstants.CBO_SUBJECT)]
        [AdditionalMetadata("OnChange", "onExamSubjectChange()")]
        public long? SubjectID { get; set; }

        [ResourceDisplayName("ExamDetachableBag_Label_ExamCandenceBagID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamDetachableBagConstants.CBO_EXAM_CANDENCE_BAG)]
        [AdditionalMetadata("OnChange", "onExamCandenceBagChange()")]
        public long? ExamCandenceBagID { get; set; }
    }
}