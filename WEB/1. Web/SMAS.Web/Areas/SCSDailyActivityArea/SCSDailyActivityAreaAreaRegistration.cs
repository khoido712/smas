﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SCSDailyActivityArea
{
    public class SCSDailyActivityAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SCSDailyActivityArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SCSDailyActivityArea_default",
                "SCSDailyActivityArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
