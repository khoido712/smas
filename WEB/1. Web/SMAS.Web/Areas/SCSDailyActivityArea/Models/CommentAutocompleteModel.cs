﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSDailyActivityArea.Models
{
    public class CommentAutocompleteModel
    {
        public string CommentCode { get; set; }
        public string Comment { get; set; }
    }
}