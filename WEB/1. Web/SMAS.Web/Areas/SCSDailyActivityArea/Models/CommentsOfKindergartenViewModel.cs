﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSDailyActivityArea.Models
{
    public class CommentsOfKindergartenViewModel
    {
        public int CommentsOfKindergartenID { get; set; }
        public string Comment { get; set; }
        public string CommentCode { get; set; }
        public bool? isChecked { get; set; }
        public bool isEnable { get; set; }
    }
}