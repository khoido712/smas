﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSDailyActivityArea.Models
{
    public class SCSDailyModelExcelView
    {
        public string LblGetDay { get; set; }
        public string LblGetDate { get; set; }
        public string Lblweek { get; set; }
        public string Month { get; set; }
        public string LblweekFromDate { get; set; }
        public string LblweekToDate { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int GroupClassId { get; set; }
        public string GroupName { get; set; }

    }
}