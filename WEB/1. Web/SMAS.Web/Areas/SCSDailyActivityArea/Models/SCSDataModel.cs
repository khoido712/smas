﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSDailyActivityArea.Models
{
    public class SCSDataModel
    {
        public List<int> ActivityOfClassIDs { get; set; }
        public List<string> Comments { get; set; }
        public int PupilID { get; set; }
        public int TypeID { get; set; }
        public int ClassID { get; set; }

        public SCSDataModel()
        {
            Comments = new List<string>();
            ActivityOfClassIDs = new List<int>();
            TypeID = 0;
            ClassID = 0;
        }
    }
}