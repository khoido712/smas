﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.ComponentModel;
namespace SMAS.Web.Areas.SCSDailyActivityArea.Models
{
    public class SCSDailyActivityViewModel
    {
       
        public int PupilId { get; set; }
        public int ActivityOfClassId { get; set; }
        public string TimeInterval { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public int ActivityID { get; set; }
        [ResourceDisplayName("CodeConfig_Label_IsActive")]
        public string ActContent { get; set; }
        public int ActivityOfPupilID { get; set; }
        [ResourceDisplayName("Activity_Label_CommentOfTeacher")]
        public string TeacherComment { get; set; }
    }
    public class SCSDailyPupilOfClassViewModel
    {
        public int STT { get; set; }
        public int pupilId { get; set; }
        public string fullname { get; set; }
        public int ClassID { get; set; }
        public int activityID { get; set; }
        public string name { get; set; }
        public int status { get; set; }
        public int lstActivity { get; set; }
        public int? Order { get; set; }
        public List<SCSDailyActivityViewModel> lstDailyActivity { get; set; }
    }
}