﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Business.IBusinessExtensions;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.SCSDailyActivityArea.Models;
using SMAS.Business.BusinessObject;
using System.Web.Helpers;
using SMAS.Web.Constants;
using SMAS.Business.Common;
using SMAS.Web.Areas.SCSDailyActivityArea;
using SMAS.Web.Filter;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;
using System.Data.Objects;

namespace SMAS.Web.Areas.SCSDailyActivityArea.Controllers
{
    public class SCSDailyActivityController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBussiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IActivityOfPupilBusiness ActivityOfPupilBusiness;
        private readonly IActivityBusiness ActivityBusiness;
        private readonly IActivityPlanBusiness ActivityPlanBusiness;
        private readonly IActivityPlanClassBusiness ActivityPlanClassBusiness;
        private readonly IActivityTypeBusiness ActivityTypeBusiness;
        private readonly IActivityOfClassBusiness ActivityOfClassBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ICommentsOfKindergartenBusiness CommentsOfKindergartenBusiness;
        private GlobalInfo gl = new GlobalInfo();
        public SCSDailyActivityController(
            IActivityPlanClassBusiness activityPlanClassBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IClassProfileBusiness classprofilebusiness,
            IClassAssigmentBusiness classAssigmentBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBussiness,
            IActivityOfPupilBusiness activityOfPupilBusiness,
            IActivityBusiness activityBusiness,
            IActivityPlanBusiness activityPlanBusiness,
            IActivityTypeBusiness activityTypeBusiness,
            IClassSubjectBusiness classSubjectBusiness,
           IActivityOfClassBusiness ActivityOfClassBusiness,
            ISubjectCatBusiness subjectCatBusiness,
            ICommentsOfKindergartenBusiness commentsOfKindergartenBusiness
            )
        {
            this.SubjectCatBusiness = subjectCatBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classprofilebusiness;
            this.ClassAssigmentBusiness = classAssigmentBusiness;
            this.PupilOfClassBussiness = pupilOfClassBussiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.ActivityOfPupilBusiness = activityOfPupilBusiness;
            this.ActivityBusiness = activityBusiness;
            this.ActivityPlanBusiness = activityPlanBusiness;
            this.ActivityTypeBusiness = activityTypeBusiness;
            this.ActivityOfClassBusiness = ActivityOfClassBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.CommentsOfKindergartenBusiness = commentsOfKindergartenBusiness;
            this.ActivityPlanClassBusiness = activityPlanClassBusiness;
        }

        #region Khởi tạo trang index
        public ActionResult Index(int? ClassID)
        {
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            LoadCommentFast();
            ViewData[SCSDailyActivityConstants.A] = null;
            List<ClassProfile> lstClassInRole = new List<ClassProfile>();
            ViewData[SCSDailyActivityConstants.LST_MONTH] = new SelectList(new string[] { });
            LoadMonth();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsViewAll)
            {
                lstClassInRole = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo)
                    .Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel).ToList();
            }
            else
            {
                ClassSearchInfo["TeacherID"] = _globalInfo.EmployeeID;
                // Danh sách lớp GV có Phân công phụ trách
                //lstClassInRole = ClassAssigmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo).Select(o => o.ClassProfile)
                //            .Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel).ToList();

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                }
                lstClassInRole = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                            .OrderBy(x => x.EducationLevelID)
                                                                            .ThenBy(x => x.DisplayName).ToList();
                if (lstClassInRole.Count == 0)
                {
                    ViewData[SCSDailyActivityConstants.A] = "1";
                }
            }
            ClassProfile selectedClass = this.ClassProfileBusiness.Find(ClassID);
            ViewData[SCSDailyActivityConstants.LIST_EDUCATIONLEVEL] = lstClassInRole.Select(o => o.EducationLevel).Distinct().OrderBy(o => o.EducationLevelID)
                                                                        .ToList()
                                                                        .Select(o => new SelectListItem
                                                                        {
                                                                            Value = o.EducationLevelID.ToString(),
                                                                            Text = o.Resolution,
                                                                            Selected = selectedClass != null && o.EducationLevelID == selectedClass.EducationLevelID
                                                                        }).ToList();
            ViewData[SCSDailyActivityConstants.LIST_CLASS] = new SelectList(lstClassInRole.Distinct().ToList(), "ClassProfileID", "DisplayName");
            if (selectedClass != null)
            {
                ViewData[SCSDailyActivityConstants.CLASS_ID] = selectedClass.ClassProfileID;
                ViewData[SCSDailyActivityConstants.CLASS_NAME] = selectedClass.DisplayName;
            }
            // Set default first class
            this.SetViewData(ClassID);
            GetPupil(1, ClassID, true); // load hoc sinh dau tien, lop dau tien
            return View();
        }
        #endregion

        #region set dữ liệu
        private void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                    semester1 = true;
                else semester2 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[SCSDailyActivityConstants.LIST_SEMESTER] = listSemester;
            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();
            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }
            ViewData[SCSDailyActivityConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                }

                List<ClassProfile> listClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();

                ViewData[SCSDailyActivityConstants.LIST_CLASS] = listClass;
                var listCP = listClass.ToList();
                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            //lay list mon hoc
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (cp != null)
            {
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && cp.ClassProfileID > 0)
                {
                    lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, _globalInfo.Semester.Value, cp.ClassProfileID, _globalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                    //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .ToList();
                }
            }
            else
            {
                ViewData[SCSDailyActivityConstants.CLASS_NULL] = true;
            }

            ViewData[SCSDailyActivityConstants.SubjectID] = lsClassSubject.Select(c => c.SubjectID).FirstOrDefault();
        }
        #endregion

        #region LoadMonth
        private void LoadMonth()
        {
            if (gl.AcademicYearID.HasValue)
            {
                AcademicYear ay = AcademicYearBusiness.Find(gl.AcademicYearID.Value);
                List<ComboObject> ls = new List<ComboObject>();

                int year = DateTime.Now.Year;
                string start = "";
                string end = "";
                int startYear = ay.FirstSemesterStartDate.Value.Year;
                int endYear = ay.SecondSemesterEndDate.Value.Year;
                int startMonth = ay.FirstSemesterStartDate.Value.Month;
                int endMonth = ay.SecondSemesterEndDate.Value.Month;
                int MonthNow = DateTime.Now.Month;
                string monthDefault = MonthNow + "/" + year;
                if (MonthNow < 10)
                {
                    monthDefault = "0" + MonthNow + "/" + year;
                }
                for (int i = startMonth; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        start = Convert.ToString("0" + i + "/" + startYear);
                    }
                    else
                    {
                        start = Convert.ToString(i + "/" + startYear);
                    }
                    ls.Add(new ComboObject(Convert.ToString(i), start));
                }
                if (endYear > startYear)
                {
                    for (int j = 1; j <= endMonth; j++)
                    {
                        if (j < 10)
                        {
                            end = Convert.ToString("0" + j + "/" + endYear);
                        }
                        else
                        {
                            end = Convert.ToString(j + "/" + endYear);
                        }
                        ls.Add(new ComboObject(Convert.ToString(j + 12), end));
                    }
                }
                var check = ls.Where(o => o.value.Contains(monthDefault)).FirstOrDefault();
                int def = Convert.ToInt32(ls.LastOrDefault().key);
                if (check != null)
                {
                    def = Convert.ToInt32(check.key);
                }
                ViewData[SCSDailyActivityConstants.LST_MONTH] = new SelectList(ls, "key", "value", def);
            }
            else
            {
                ViewData[SCSDailyActivityConstants.LST_MONTH] = new SelectList(new string[] { });
            }
        }
        #endregion

        #region danh sách học sinh theo lớp
        public List<SCSPupilViewModel> GetListPupil(int? ClassID)
        {
            //Danh sach hoc sinh tra ve
            List<SCSPupilViewModel> lstPupilRet = new List<SCSPupilViewModel>();
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
                SearchInfo["ClassID"] = ClassID;
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                SearchInfo["Status"] = new List<int>() { { SystemParamsInFile.PUPIL_STATUS_STUDYING } };
                // lay ve danh sach hoc sinh theo cap, lop, truong, nam
                IQueryable<PupilProfile> lstPupil = from poc in PupilOfClassBussiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo)
                                                    join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                    select pp;
                if (lstPupil.Count() > 0)
                {
                    List<PupilProfile> lstPPBO = lstPupil.ToList();
                    int Count = lstPPBO.Count;
                    int index = 1;
                    PupilProfile ppBO;
                    for (int i = 0; i < Count; i++)
                    {
                        ppBO = lstPPBO[i];
                        lstPupilRet.Add(new SCSPupilViewModel()
                        {
                            STT = index++,
                            PupilID = ppBO.PupilProfileID,
                            ClassID = ClassID.Value,
                            PupilName = ppBO.FullName,
                            Status = ppBO.ProfileStatus,
                            ImageData = ppBO.Image
                        });
                    }
                }
                Session[SCSDailyActivityConstants.LIST_PUPIL] = lstPupilRet;
            }
            return lstPupilRet;
        }
        #endregion

        #region load lấy dữ liệu lớp
        public JsonResult AjaxLoadClass(int EducationLevel)
        {
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            ClassSearchInfo["EmployeeID"] = _globalInfo.EmployeeID;
            List<ClassProfile> lstClass;
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsViewAll)
            {
                lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo)
                    .Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel && p.EducationLevelID == EducationLevel)
                    .ToList()
                    .OrderBy(p => p.OrderNumber).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.DisplayName))
                    .ToList();
            }
            else
            {
                ClassSearchInfo["TeacherID"] = _globalInfo.EmployeeID;
                // Danh sách lớp GV có Phân công phụ trách
                lstClass = ClassAssigmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo).Select(o => o.ClassProfile)
                    .Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel && p.EducationLevelID == EducationLevel)
                    .ToList()
                    .OrderBy(p => p.OrderNumber).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.DisplayName))
                    .ToList();
            }
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }
        #endregion

        #region Chi tiết học sinh theo lớp
        public PartialViewResult GetPupil(int STT, int? ClassID, bool? IsFirst)
        {
            List<SCSPupilViewModel> lstPupil = new List<SCSPupilViewModel>();
            if (IsFirst.HasValue && IsFirst.Value) lstPupil = GetListPupil(ClassID);
            else lstPupil = (List<SCSPupilViewModel>)Session[SCSDailyActivityConstants.LIST_PUPIL];

            int SumOfPupil = lstPupil.Count;
            if (ClassID.HasValue)
                ViewData[SCSDailyActivityConstants.CLASS_NAME] = ClassProfileBusiness.Find(ClassID.Value).DisplayName;

            ViewData[SCSDailyActivityConstants.CLASS_ID] = ClassID;
            ViewData[SCSDailyActivityConstants.LIST_PUPIL] = lstPupil;
            ViewData[SCSDailyActivityConstants.TOTAL_PUPIL] = SumOfPupil;
            if (STT > 0 && STT <= SumOfPupil) ViewData[SCSDailyActivityConstants.PUPIL] = lstPupil[STT - 1];
            else ViewData[SCSDailyActivityConstants.PUPIL] = new SCSPupilViewModel();
            return PartialView("_SlidePupil");
        }
        #endregion

        #region Lấy thông tin đánh giá hoạt động theo lớp và ngày
        [HttpPost]
        public PartialViewResult GetListActivity(int? ClassID, string ActDate)
        {

            if (ActDate == null)
            {
                Json(new { Type = "error", Message = "Lỗi sử lý dữ liệu ngày tháng" });
            }
            String[] date = ActDate.Split('/');
            int year = Convert.ToInt16(date[2]);
            int month = Convert.ToInt16(date[1]);
            int day = Convert.ToInt16(date[0]);
            int schoolID = _globalInfo.SchoolID.Value;
            ClassProfile obj = ClassProfileBusiness.Find(ClassID);
            DateTime ActivityDate = new DateTime(year, month, day);
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = schoolID;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["ActivityDate"] = ActivityDate;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
            SearchInfo["EducationLevelID"] = obj != null ? obj.EducationLevelID : 0;
            // check giáo viên
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID.Value);
            ViewData[SCSDailyActivityConstants.GVCN_TEACHER] = isGVCN;
            // kế hoạch hoạt động
            List<ActivityPlanBO> lstActivityPlan = ActivityPlanBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).Where(s => s.IsActive).Distinct().ToList();
            List<int> lstActivityPlanID = lstActivityPlan.Select(p => p.ActivityPlanID).Distinct().ToList();
            // lay thong tin pupil- class
            List<PupilOfClass> lstPOC = PupilOfClassBussiness.Search(SearchInfo).ToList();
            List<SCSDailyPupilOfClassViewModel> lstResult = new List<SCSDailyPupilOfClassViewModel>();
            SCSDailyPupilOfClassViewModel objResult = null;
            // danh sách hoạt động
            List<ActivityBO> lstAct = new List<ActivityBO>();
            ActivityBO objAct = null;

            List<Activity> lstActivity = ActivityBusiness.All.Where(p => lstActivityPlanID.Contains(p.ActivityPlanID) && EntityFunctions.TruncateTime(p.ActivityDate.Value) == ActivityDate.Date).ToList();
            Activity objActivity = null;
            for (int i = 0; i < lstActivity.Count; i++)
            {
                objActivity = lstActivity[i];
                objAct = new ActivityBO();
                objAct.ActivityName = objActivity.ActivityName;
                objAct.ActivityID = objActivity.ActivityID;
                objAct.ActivityPlanID = objActivity.ActivityPlanID;
                objAct.FromTime = objActivity.FromTime;
                objAct.ToTime = objActivity.ToTime;
                lstAct.Add(objAct);
            }

            //Chiendd1: 30.03.208: Su dung activityOfClass khi chua migrate du lieu sang bang lstActivity
            /*List<ActivityOfClass> lstActivityOfClass = ActivityOfClassBusiness.Search(SearchInfo).Where(p => p.ActivityPlan.IsActive && lstActivityPlanID.Contains(p.ActivityPlanID)).ToList();
            ActivityOfClass objAOC = null;
            for (int i = 0; i < lstActivityOfClass.Count; i++)
            {
                objAOC = lstActivityOfClass[i];
                objAct = new ActivityBO();
                objAct.ActivityName = objAOC.ActivityOfClassName;
                objAct.ActivityID = objAOC.ActivityOfClassID;
                objAct.ActivityPlanID = objAOC.ActivityPlanID;
                objAct.FromTime = objAOC.FromTime;
                objAct.ToTime = objAOC.ToTime;
                lstAct.Add(objAct);
            }*/


            //lstAct = lstAct.OrderBy(p => p.FromTime).Distinct().ToList();
            // Lấy toàn bộ hoạt động lớp
            //var lstActivityPlanClass = ActivityPlanClassBusiness.Search(SearchInfo).ToList();

            var lstActivityPlanClass = ActivityPlanClassBusiness.All.Where(ac => lstActivityPlanID.Contains(ac.ActivityPlanID) && ac.ClassID == ClassID).ToList();

            // lấy Bảng Hoạt động lớp Join Hoạt động 
            var lstActivityPlanClass_Activity = (from a in lstAct
                                                 join b in lstActivityPlanClass
                                                 on a.ActivityPlanID equals b.ActivityPlanID
                                                 select new ActivityBO
                                                 {
                                                     ActivityID = a.ActivityID,
                                                     ActivityName = a.ActivityName,
                                                     ActivityPlanID = a.ActivityPlanID,
                                                     FromTime = a.FromTime,
                                                     ToTime = a.ToTime,
                                                 }).OrderBy(s=>s.FromTime).ToList();
            List<SCSDailyActivityViewModel> lstView = new List<SCSDailyActivityViewModel>();
            SCSDailyActivityViewModel objView = null;
            // học sinh thuộc hoạt động
            List<int> lstActivityId = lstActivityPlanClass_Activity.Select(s => s.ActivityID).Distinct().ToList();
            SearchInfo["lstActivityID"] = lstActivityId;
            List<ActivityOfPupil> lstActivityOfPupil = ActivityOfPupilBusiness.SearchBySchool(schoolID, SearchInfo).ToList();
            if (ClassID != null)
            {
                for (int i = 0; i < lstPOC.Count; i++)
                {
                    objResult = new SCSDailyPupilOfClassViewModel();
                    objResult.pupilId = lstPOC[i].PupilID;
                    objResult.fullname = lstPOC[i].PupilProfile.FullName;
                    objResult.name = lstPOC[i].PupilProfile.Name;
                    objResult.status = lstPOC[i].Status;
                    objResult.STT = i + 1;
                    objResult.Order = lstPOC[i].OrderInClass;
                    objResult.lstActivity = lstActivityPlanClass_Activity.Count();
                    for (int j = 0; j < lstActivityPlanClass_Activity.Count; j++)
                    {
                        var listActOfPupil = lstActivityOfPupil.Where(t => t.ActivityOfClassID == lstActivityPlanClass_Activity[j].ActivityID).ToList();
                        var comment = listActOfPupil.Where(t => t.PupilID == lstPOC[i].PupilID).Select(t => t.CommentOfTeacher).FirstOrDefault();
                        var AcivityOfPupilID = listActOfPupil.Where(t => t.PupilID == lstPOC[i].PupilID).Select(t => t.ActivityOfPupilID).FirstOrDefault();
                        objResult.activityID = lstActivityPlanClass_Activity[j].ActivityID;
                        objView = new SCSDailyActivityViewModel();

                        objView.PupilId = lstPOC[i].PupilID;
                        objView.ActivityID = lstActivityPlanClass_Activity[j].ActivityID;
                        objView.TeacherComment = comment != null ? comment : "";
                        objView.ActivityOfPupilID = AcivityOfPupilID;
                        lstView.Add(objView);
                        objResult.lstDailyActivity = lstView;
                    }
                    lstResult.Add(objResult);
                }
                ViewData[SCSDailyActivityConstants.LIST_DAILY_ACTIVITY] = lstResult;
                ViewData[SCSDailyActivityConstants.LST_PUPIT_PRO] = lstActivityPlanClass_Activity.Count > 0 ? lstActivityPlanClass_Activity : null;// list lstAct
            }
            return PartialView("_ListActivity");
        }
        #endregion

        #region Cập nhật hoạt dộng với học sinh
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult DoWorkCommentOfActivityPlanPupil(string list_PupilId_ActivityId_Comment, int classId, int type, string ActDate)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
            SearchInfo["ClassID"] = classId;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            //thông tin Date
            String[] date = ActDate.Split('/');
            int years = Convert.ToInt16(date[2]);
            int months = Convert.ToInt16(date[1]);
            int days = Convert.ToInt16(date[0]);
            DateTime ActivityDate = new DateTime(years, months, days);
            SearchInfo["ActivityDate"] = ActivityDate;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
            // cut string
            var lst_PupilId_ActivityId_Comment = new List<string>(list_PupilId_ActivityId_Comment.Split(new string[] { "[-!#$%^&*()]" }, StringSplitOptions.RemoveEmptyEntries));
            List<string> listPupilId = lst_PupilId_ActivityId_Comment.Select(o => o.Split(new string[] { "[*~]" }, StringSplitOptions.RemoveEmptyEntries)[0]).ToList();
            List<string> listActivityId = lst_PupilId_ActivityId_Comment.Select(o => o.Split(new string[] { "[*~]" }, StringSplitOptions.RemoveEmptyEntries)[1]).ToList();
            List<string> listActivityOfPupilID = lst_PupilId_ActivityId_Comment.Select(o => o.Split(new string[] { "[*~]" }, StringSplitOptions.RemoveEmptyEntries)[2]).ToList();
            List<string> listComment = new List<string>();
            foreach (var item in lst_PupilId_ActivityId_Comment)
            {
                var commnet = "";
                var lst = new List<string>(item.Split(new string[] { "[*~]" }, StringSplitOptions.RemoveEmptyEntries));
                if (lst.Count < 4)
                {
                    commnet = "";
                }
                else
                {
                    commnet = lst[3];
                }
                listComment.Add(commnet);
            }

            //lay danh sach hoat dong cua lop
            List<int> lstActivityID = listActivityId.Select(p => Int32.Parse(p)).Distinct().ToList();
            List<int> lstActivityPlanID = ActivityPlanClassBusiness.All.Where(p => p.ClassID == classId).Select(p => p.ActivityPlanID).Distinct().ToList();
            List<Activity> lstActivityResult = ActivityBusiness.All.Where(p => lstActivityPlanID.Contains(p.ActivityPlanID) && lstActivityID.Contains(p.ActivityID)).ToList();
            //List<ActivityOfClass> lstActivityOfClassResult = ActivityOfClassBusiness.All.Where(p => lstActivityPlanID.Contains(p.ActivityPlanID) && lstActivityID.Contains(p.ActivityOfClassID)).ToList();

            if (lstActivityResult.Count == 0)
            {
                return Json(new { Type = "error", Message = Res.Get("SCSDailyActivity_Label_Activity") });
            }

            IDictionary<string, object> dicPOC = new Dictionary<string, object>();
            dicPOC["SchoolID"] = _globalInfo.SchoolID.Value;
            dicPOC["ClassID"] = classId;
            dicPOC["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicPOC["lstPupilID"] = listPupilId.Select(p => Int32.Parse(p)).ToList();
            List<PupilOfClass> lstPOC = PupilOfClassBussiness.Search(dicPOC).ToList();
            PupilOfClass objPOC = null;

            List<ActivityOfPupil> lstObj = new List<ActivityOfPupil>();
            ActivityOfPupil obj = null;
            int pupilID = 0;
            for (var k = 0; k < listPupilId.Count; k++)
            {
                pupilID = int.Parse(listPupilId[k]);
                objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    obj = new ActivityOfPupil();
                    obj.ActivityOfPupilID = int.Parse(listActivityOfPupilID[k]);
                    obj.PupilID = objPOC.PupilID;
                    obj.ClassID = classId;
                    obj.SchoolID = _globalInfo.SchoolID.Value;
                    obj.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    obj.CommentOfTeacher = listComment[k];
                    obj.ActivityOfClassID = int.Parse(listActivityId[k]);
                    lstObj.Add(obj);
                }
            }
            SearchInfo["lstPupilID"] = listPupilId.Select(p => Int32.Parse(p)).ToList();
            var mess = "";
            //var lsobj = lstObj.Where(t => t.CommentOfTeacher != "").ToList();
            SearchInfo.Add("lstActivityID", lstActivityID);
            if (type == 1)// Lưu
            {
                ActivityOfPupilBusiness.Insert(lstObj, SearchInfo);
                mess = "Cập nhật thành công";
            }
            else if (type == 2)//Xóa
            {
                var lsobj = lstObj.Where(t => t.CommentOfTeacher != "").ToList();
                ActivityOfPupilBusiness.DeleteActivityOfPupil(lsobj, SearchInfo);
                mess = "Xóa thành công";
            }
            return Json(new { Type = "success", Message = mess });
        }
        #endregion

        #region Nhận xét nhanh
        [HttpPost]
        public ActionResult AjaxLoadDialog()
        {
            return PartialView("_GridFastComment");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadSystemComment()
        {
            //lay du lieu comment
            IDictionary<string, object> dic = new Dictionary<string, object>();
            IQueryable<CommentsOfKindergarten> iquery = CommentsOfKindergartenBusiness.SearchCommentsOfKindergarten(dic);
            List<CommentsOfKindergartenViewModel> lstResult = (from cc in iquery
                                                               select new CommentsOfKindergartenViewModel
                                                               {
                                                                   CommentsOfKindergartenID = cc.CommentsOfKindergartenID,
                                                                   Comment = cc.Comment,
                                                                   CommentCode = cc.CommentCode
                                                               }).OrderBy(p => p.Comment).ToList();
            IDictionary<string, object> dicAccount = new Dictionary<string, object>()
            {
                {"AccountID",_globalInfo.UserAccountID}
            };
            List<string> lstCommentAccount = CommentsOfKindergartenBusiness.SearchCommentsOfKindergarten(dicAccount).Select(p => p.Comment).Distinct().ToList();
            lstResult.ForEach(p =>
            {
                p.isEnable = !lstCommentAccount.Contains(p.Comment);
            });
            ViewData[SCSDailyActivityConstants.LIST_COLLECTION_COMMENT] = lstResult;
            return PartialView("_GridSystemComment");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadEmployeeComment()
        {
            //lay du lieu comment
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AccountID",_globalInfo.UserAccountID}
            };
            List<CommentsOfKindergartenViewModel> lstResult = (from cc in CommentsOfKindergartenBusiness.SearchCommentsOfKindergarten(dic)
                                                               select new CommentsOfKindergartenViewModel
                                                               {
                                                                   CommentsOfKindergartenID = cc.CommentsOfKindergartenID,
                                                                   Comment = cc.Comment,
                                                                   CommentCode = cc.CommentCode
                                                               }).OrderBy(p => p.CommentCode).ToList();
            ViewData[SCSDailyActivityConstants.LIST_COLLECTION_COMMENT] = lstResult;
            return PartialView("_GridEmployeeComment");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadMoveRigth(List<CommentsOfKindergartenViewModel> lstCollectionComment, string ArrSystemID)
        {
            List<int> lstCollectionCommentID = new List<int>();
            List<int> lstAccountID = (lstCollectionComment != null && lstCollectionComment.Count > 0) ? lstCollectionComment.Select(p => p.CommentsOfKindergartenID).ToList() : new List<int>();
            List<int> lstSystemID = ArrSystemID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            lstCollectionCommentID = lstAccountID.Union(lstSystemID).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"lstCommentsOfKindergartenID",lstCollectionCommentID}
            };
            IQueryable<CommentsOfKindergartenViewModel> lsttmp = (from cc in CommentsOfKindergartenBusiness.SearchCommentsOfKindergarten(dic)
                                                                  select new CommentsOfKindergartenViewModel
                                                                  {
                                                                      CommentsOfKindergartenID = cc.CommentsOfKindergartenID,
                                                                      Comment = cc.Comment,
                                                                      CommentCode = cc.CommentCode
                                                                  });
            List<CommentsOfKindergartenViewModel> lstCollectionSystem = lsttmp.Where(p => lstSystemID.Contains(p.CommentsOfKindergartenID) && !lstAccountID.Contains(p.CommentsOfKindergartenID)).ToList();
            // string maxCode = (lstCollectionComment != null && lstCollectionComment.Count > 0) ? lstCollectionComment.Where(p => p.CommentCode.StartsWith(objSC.Abbreviation)).Max(p => p.CommentCode) : "";
            //int maxNumber = !string.IsNullOrEmpty(maxCode) ? Convert.ToInt32(maxCode.Substring(objSC.Abbreviation.Length)) : 0;
            for (int i = 0; i < lstCollectionSystem.Count; i++)
            {
                var objSystem = lstCollectionSystem[i];
                //maxNumber++;
                //objSystem.CommentCode = objSC.Abbreviation + maxNumber.ToString();
            }
            List<CommentsOfKindergartenViewModel> lstResult = (lstCollectionComment != null && lstCollectionComment.Count > 0) ? lstCollectionComment.Union(lstCollectionSystem).ToList() : lstCollectionSystem;
            ViewData[SCSDailyActivityConstants.LIST_COLLECTION_COMMENT] = lstResult;
            return PartialView("_GridEmployeeComment");
        }
        [ValidateAntiForgeryToken]
        public JsonResult SaveCollectionComment(List<CommentsOfKindergartenViewModel> lstCollectionComment)
        {
            lstCollectionComment = lstCollectionComment.Where(p => p.isChecked.HasValue && p.isChecked.Value).ToList();
            CommentsOfKindergartenViewModel objCC = null;
            List<CommentsOfKindergarten> lstResult = new List<CommentsOfKindergarten>();
            CommentsOfKindergarten objResult = null;
            for (int i = 0; i < lstCollectionComment.Count; i++)
            {
                objCC = lstCollectionComment[i];
                objResult = new CommentsOfKindergarten();
                objResult.CommentsOfKindergartenID = objCC.CommentsOfKindergartenID;
                objResult.SchoolID = _globalInfo.SchoolID.Value;
                objResult.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objResult.AccountID = _globalInfo.UserAccountID;
                objResult.Comment = objCC.Comment;
                objResult.CommentCode = objCC.CommentCode;
                lstResult.Add(objResult);
            }
            List<int> lstCollectionID = lstResult.Select(p => p.CommentsOfKindergartenID).Distinct().ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"lstCommentsOfKindergartenID",lstCollectionID},
                {"AccountID",_globalInfo.UserAccountID},
            };
            CommentsOfKindergartenBusiness.InsertOrUpdateCommentsOfKindergarten(lstResult, dic);
            return Json(new JsonMessage("Cập nhật thành công", "success"));
        }
        [HttpPost]
        public JsonResult DeleteCollectionComment(string ArrID)
        {
            List<int> lstCommentsOfKindergartenID = ArrID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {

                {"AccountID",_globalInfo.UserAccountID},
                {"lstCommentsOfKindergartenID",lstCommentsOfKindergartenID}
            };
            CommentsOfKindergarten obj = null;
            foreach (var item in lstCommentsOfKindergartenID)
            {
                obj = new CommentsOfKindergarten();
                obj = CommentsOfKindergartenBusiness.All.Where(t => t.CommentsOfKindergartenID == item).FirstOrDefault();
                obj.AccountID = null;
                CommentsOfKindergartenBusiness.Update(obj);
            }
            CommentsOfKindergartenBusiness.Save();
            return Json(new JsonMessage("Xóa thành công", "success"));
        }
        // Import/Output Excel Nhận xét nhanh
        public FileResult ExportCommentsOfKindergartenComment()
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", "MN_NganHangNhanXet.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetCurrent = oBook.GetSheet(1);
            IVTWorksheet sheet = null;
            //fill thong tin chung
            sheet = oBook.CopySheetToLast(sheetCurrent);
            sheet.Name = "Ngan_hang_nhan_xet";
            sheet.SetCellValue(2, 1, _globalInfo.SchoolName);// tên trường

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AccountID"] = _globalInfo.UserAccountID;
            List<CommentsOfKindergartenBO> lstCC = (from cc in CommentsOfKindergartenBusiness.SearchCommentsOfKindergarten(dic)
                                                    select new CommentsOfKindergartenBO
                                                    {
                                                        CommnetsOfKindergatenId = cc.CommentsOfKindergartenID,
                                                        Commnet = cc.Comment,
                                                        CommnetCode = cc.CommentCode
                                                    }).OrderBy(p => p.CommnetCode).ToList();
            CommentsOfKindergartenBO objCC = null;
            int startRow = 7;
            for (int i = 0; i < lstCC.Count; i++)
            {
                objCC = new CommentsOfKindergartenBO();
                objCC = lstCC[i];
                sheet.SetCellValue(startRow, 1, (i + 1));
                sheet.SetCellValue(startRow, 2, objCC.CommnetCode);
                sheet.SetCellValue(startRow, 3, objCC.Commnet);
                sheet.SetCellValue(startRow, 4, objCC.CommnetsOfKindergatenId);//dung de import
                startRow++;
            }
            sheet.GetRange(7, 3, startRow - 1, 3).IsLock = false;
            sheet.GetRange(7, 1, startRow - 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.HideColumn(4);
            sheetCurrent.Delete();
            sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "MN_NganHangNhanXet_" + DateTime.Now + ".xls";
            return result;
        }
        [ValidateAntiForgeryToken]
        public JsonResult ImportCommentsOfKindergarten(IEnumerable<HttpPostedFileBase> attachmentsCC)
        {
            if (attachmentsCC == null || attachmentsCC.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachmentsCC.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                List<CommentsOfKindergarten> lstCommentsOfKindergarten = new List<CommentsOfKindergarten>();
                CommentsOfKindergarten objCommentsOfKindergarten = null;
                List<int> lstCommentsOfKindergartenID = new List<int>();
                int startRow = 7;
                if (sheet.GetCellValue(startRow - 1, 1) != null && sheet.GetCellValue(startRow - 1, 2) != null && sheet.GetCellValue(startRow - 1, 3) != null)
                {
                    if (!sheet.GetCellValue(startRow - 1, 1).Equals("STT"))
                    {
                        return Json(new JsonMessage("File import không đúng mẫu", "error"));
                    }
                }
                else
                {
                    return Json(new JsonMessage("File import không đúng mẫu", "error"));
                }

                while (sheet.GetCellValue(startRow, 1) != null && sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
                {
                    objCommentsOfKindergarten = new CommentsOfKindergarten();
                    objCommentsOfKindergarten.SchoolID = _globalInfo.SchoolID.Value;
                    objCommentsOfKindergarten.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objCommentsOfKindergarten.AccountID = _globalInfo.UserAccountID;
                    string strval = string.Empty;
                    strval = sheet.GetCellValue(startRow, 4) != null ? sheet.GetCellValue(startRow, 4).ToString() : "";
                    objCommentsOfKindergarten.CommentsOfKindergartenID = strval != null ? int.Parse(strval) : 0;
                    lstCommentsOfKindergartenID.Add(objCommentsOfKindergarten.CommentsOfKindergartenID);
                    strval = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : "";
                    objCommentsOfKindergarten.CommentCode = strval;
                    strval = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString() : "";
                    objCommentsOfKindergarten.Comment = strval;
                    lstCommentsOfKindergarten.Add(objCommentsOfKindergarten);
                    startRow++;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"AccountID",_globalInfo.UserAccountID},
                    {"lstCommentsOfKindergartenID",lstCommentsOfKindergartenID}
                };
                CommentsOfKindergartenBusiness.InsertOrUpdateCommentsOfKindergarten(lstCommentsOfKindergarten, dic);
            }
            LoadCommentFast();
            return Json(new JsonMessage("Import thành công", "success"));
        }
        public void LoadCommentFast()
        {
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AccountID"] = _globalInfo.UserAccountID;
            List<CommentAutocompleteModel> lstCommentAutocomplete = (from cc in CommentsOfKindergartenBusiness.SearchCommentsOfKindergarten(ClassSearchInfo)
                                                                     select new CommentAutocompleteModel
                                                                     {
                                                                         CommentCode = cc.CommentCode,
                                                                         Comment = cc.Comment
                                                                     }).OrderBy(p => p.Comment).ToList();
            ViewData[SCSDailyActivityConstants.LIST_AUTOCOMPLETE] = lstCommentAutocomplete;
        }
        #endregion

        #region Import/ Output file excel
        public ActionResult DialogImportExcel(int? ClassID, string ActDate)
        {
            ViewData[SCSDailyActivityConstants.ACT_CLASSID] = ClassID;
            ViewData[SCSDailyActivityConstants.ACT_DATE] = ActDate;
            TempData["ActDate"] = ActDate;
            TempData["ClassID"] = ClassID;
            return PartialView("_DialogImportExcel");
        }
        //get info output
        public ActionResult DialogOutExcel(string lblGetDate, string lblweek, string lblweekFromDate, string lblweekToDate, int classid)
        {
            var Dateday = new List<string>(lblGetDate.Trim().Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries));
            var ClassNameId = from a in ClassProfileBusiness.All.Where(t => t.ClassProfileID == classid)
                              select new
                              {
                                  a.EducationLevel.Resolution,
                                  a.EducationLevelID,
                                  a.DisplayName
                              };
            var Month = new List<string>(Dateday[1].Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries)).ToList();
            var month = Month[1].ToString();
            if (month != "")
            {
                switch (month)
                {
                    case "01":
                        month = "Tháng 1";
                        break;
                    case "02":
                        month = "Tháng 2";
                        break;
                    case "03":
                        month = "Tháng 3";
                        break;
                    case "04":
                        month = "Tháng 4";
                        break;
                    case "05":
                        month = "Tháng 5";
                        break;
                    case "06":
                        month = "Tháng 6";
                        break;
                    case "07":
                        month = "Tháng 7";
                        break;
                    case "08":
                        month = "Tháng 8";
                        break;
                    case "09":
                        month = "Tháng 9";
                        break;
                    case "10":
                        month = "Tháng 10";
                        break;
                    case "11":
                        month = "Tháng 11";
                        break;
                    case "12":
                        month = "Tháng 12";
                        break;
                }
            }
            var model = new SCSDailyModelExcelView
            {
                ClassId = classid,
                ClassName = ClassNameId.Select(t => t.DisplayName).FirstOrDefault(),
                LblGetDay = Dateday[0],
                LblGetDate = Dateday[1],
                Lblweek = lblweek,
                Month = month,
                LblweekFromDate = lblweekFromDate,
                LblweekToDate = lblweekToDate,
                GroupClassId = ClassNameId.Select(t => t.EducationLevelID).FirstOrDefault(),
                GroupName = ClassNameId.Select(t => t.Resolution).FirstOrDefault()
            };
            return PartialView("_DialogOutExcel", model);
        }
        //post info output
        public FileResult PostOutExcel(string fromDate, string toDate, string classId, string TitleTime, string TitleClass, string dayOr)
        {
            string templatePath = string.Empty;
            string fileName = string.Empty;
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", "BangDanhGiaHoatDong.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetCurrent = oBook.GetSheet(1);
            IVTWorksheet sheetCurrent2 = oBook.GetSheet(2);
            IVTWorksheet sheet = null;
            IVTWorksheet sheet2 = null;
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
            //
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (fromDate != "" & toDate != "")
            {
                String[] fromdate = fromDate.Split('/');
                String[] todate = toDate.Split('/');
                DateTime from_date = new DateTime(Convert.ToInt16(fromdate[2]), Convert.ToInt16(fromdate[1]), Convert.ToInt16(fromdate[0]));
                DateTime to_date = new DateTime(Convert.ToInt16(todate[2]), Convert.ToInt16(todate[1]), Convert.ToInt16(todate[0]));
                SearchInfo["FromDate"] = from_date;
                SearchInfo["FromDate"] = to_date;

            }
            if (dayOr != "")
            {
                String[] date = dayOr.Split('/');
                DateTime ActivityDate = new DateTime(Convert.ToInt16(date[2]), Convert.ToInt16(date[1]), Convert.ToInt16(date[0]));
                SearchInfo["ActivityDate"] = ActivityDate;
            }
            if (classId != "")
            {
                var classID = int.Parse(classId);
                SearchInfo["ClassID"] = classID;
                ClassProfile obj = ClassProfileBusiness.Find(classID);
                SearchInfo["EducationLevelID"] = obj != null ? obj.EducationLevelID : 0;
                var lst = ClassProfileBusiness.Search(SearchInfo).ToList();
                // list class 
                lstClass = lst.Where(t => t.ClassProfileID == classID).ToList();
            }
            //if (groupClassId != "")
            //{
            //    var GroupClassID = int.Parse(groupClassId);
            //    SearchInfo["EducationLevelID"] = GroupClassID;
            //    // list class 
            //    lstClass = ClassProfileBusiness.Search(SearchInfo).ToList();
            //}
            List<ActivityPlanBO> lstActivityPlan = ActivityPlanBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).Where(c=>c.IsActive).Distinct().ToList();
            if (lstActivityPlan != null && lstActivityPlan.Count > 0)
            {
                SearchInfo["LstActivityPlanID"] = lstActivityPlan.Select(o => o.ActivityPlanID).ToList();
            }
            // list học sinh
            List<PupilOfClass> lstPOC = PupilOfClassBussiness.Search(SearchInfo).ToList();
            // lấy toàn bộ hoạt động


            //List<Activity> lstActivity = ActivityBusiness.All.Where(p => lstActivityPlanID.Contains(p.ActivityPlanID) && EntityFunctions.TruncateTime(p.ActivityDate.Value) == ActivityDate.Date).ToList();

            List<ActivityBO> lstAct = (from a in ActivityBusiness.Search(SearchInfo).Where(p => p.ActivityPlan.IsActive).Distinct()
                                            .OrderBy(o => o.FromTime)
                                       select new ActivityBO
                                       {
                                           ActivityName = a.ActivityName,
                                           ActivityPlanID = a.ActivityPlanID,
                                           ActivityID = a.ActivityID,
                                           FromTime = a.FromTime,
                                           ToTime = a.ToTime,
                                       }).ToList();
            // Lấy toàn bộ hoạt động lớp
            var lstActivityPlanClass = ActivityPlanClassBusiness.Search(SearchInfo).ToList();
            // lấy Bảng Hoạt động lớp Join Hoạt động 
            var lstActivityPlanClass_Activity = (from a in lstAct
                                                 join b in lstActivityPlanClass
                                                 on a.ActivityPlanID equals b.ActivityPlanID
                                                 select new
                                                 {
                                                     a.ActivityID,
                                                     a.ActivityName,
                                                     a.ActivityPlanID,
                                                     a.FromTime,
                                                     a.ToTime,
                                                     b.ClassID
                                                 }).ToList();
            // học sinh thuộc hoạt động
            // học sinh thuộc hoạt động
            List<int> lstActivityId = lstActivityPlanClass_Activity.Select(s => s.ActivityID).Distinct().ToList();
            SearchInfo["lstActivityID"] = lstActivityId;
            List<ActivityOfPupil> lstActivityOfPupil = ActivityOfPupilBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();
            for (int i = 0; i < lstClass.Count; i++)
            {
                int classID = lstClass[i].ClassProfileID;
                // học sinh thuộc lớp
                var lisPupilOfClass = lstPOC.Where(t => t.ClassID == classID).OrderBy(t => t.OrderInClass).ToList();
                // kế hoạch hoạt động thuộc lớp
                var lstActivityPlanOfClass = lstActivityPlan.Where(t => t.ClassID == classID).ToList();
                if (lstActivityPlanOfClass != null && lstActivityPlanOfClass.Count > 0)
                {
                    SearchInfo["LstActivityPlanID"] = lstActivityPlanOfClass.Select(o => o.ActivityPlanID).ToList();
                }
                // hoạt động của lớp
                var lstActivityPlanClass_Activity_Of_Class = lstActivityPlanClass_Activity.Where(t => t.ClassID == classID).ToList();
                sheet = oBook.CopySheetToLast(sheetCurrent);
                sheet2 = oBook.CopySheetToLast(sheetCurrent2);
                int startRow = 4;
                int startcol = 1;
                sheet2.Name = "Thông tin hoạt động";
                sheet2.SetCellValue(2, 1, TitleTime);
                // hoat động seet2
                for (int k = 0; k < lstActivityPlanClass_Activity_Of_Class.Count; k++)
                {
                    sheet2.SetCellValue(startRow, 1, (k + 1));
                    sheet2.SetCellValue(startRow, 2, "Hoạt động " + (k + 1));
                    sheet2.SetCellValue(startRow, 3, lstActivityPlanClass_Activity_Of_Class[k].FromTime);
                    sheet2.SetCellValue(startRow, 4, lstActivityPlanClass_Activity_Of_Class[k].ToTime);
                    sheet2.SetCellValue(startRow, 5, lstActivityPlanClass_Activity_Of_Class[k].ActivityName);
                    startRow++;
                }
                sheet2.GetRange(4, 1, startRow - 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                startRow = 7;
                startcol = 5;
                //goi ham do du lieu vao file
                sheet.SetCellValue(2, 1, _globalInfo.SchoolName);
                sheet.SetCellValue(4, 1, "Thời gian: " + TitleTime);
                sheet.SetCellValue(3, 1, "BẢNG ĐÁNH GIÁ HOẠT ĐỘNG LỚP " + TitleClass);
                sheet.SetCellValue(1500, 1, "DGHD");// xét TypeExport
                for (int x = 0; x < lstActivityPlanClass_Activity_Of_Class.Count; x++)
                {
                    var ls = lstActivityPlanClass_Activity_Of_Class[x];
                    sheet.SetCellValue(6, startcol, "Hoạt động " + (x + 1) + "\n" + String.Format("{0:HH\\:mm} - {1:HH\\:mm}", ls.FromTime, ls.ToTime));
                    sheet.SetCellValue(1500, startcol, ls.ActivityID);
                    sheet.GetRange(6, 5, 6, startcol).FillColor(150, 150, 150);
                    sheet.GetRange(6, 5, 6, startcol + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    startcol++;
                }
                startRow = 7;
                for (int p = 0; p < lisPupilOfClass.Count; p++)
                {
                    sheet.Name = lisPupilOfClass[p].ClassProfile.DisplayName;
                    sheet.SetCellValue(startRow, 1, (p + 1));
                    sheet.SetCellValue(startRow, 2, lisPupilOfClass[p].PupilProfile.PupilCode);
                    sheet.SetCellValue(startRow, 4, lisPupilOfClass[p].PupilProfile.BirthDate);
                    startcol = 5;
                    for (int l = 0; l < lstActivityPlanClass_Activity_Of_Class.Count; l++)
                    {
                        var objActivityOfPupil = lstActivityOfPupil.Where(t => t.PupilID == lisPupilOfClass[p].PupilID && t.ActivityOfClassID == lstActivityPlanClass_Activity_Of_Class[l].ActivityID).FirstOrDefault();
                        sheet.SetCellValue(startRow, startcol, objActivityOfPupil != null ? objActivityOfPupil.CommentOfTeacher : "");
                        startcol++;
                    }
                    if (lisPupilOfClass[p].Status == SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.SetCellValue(startRow, 3, lisPupilOfClass[p].PupilProfile.FullName);
                        sheet.GetRange(startRow, 5, startRow, 4 + lstActivityPlanClass_Activity_Of_Class.Count()).IsLock = false;
                    }
                    else
                    {
                        sheet.SetCellValue(startRow, 3, lisPupilOfClass[p].PupilProfile.FullName);
                        sheet.GetRange(startRow, 5, startRow, 5 + lstActivityPlanClass_Activity_Of_Class.Count()).IsLock = true;
                        sheet.GetRange(startRow, 1, startRow, 5 + lstActivityPlanClass_Activity_Of_Class.Count()).SetFontStyle(false, System.Drawing.Color.Red, true, 11, true, true);
                    }
                    startRow++;
                }
                sheet.GetRange(7, 1, startRow - 1, 4 + lstActivityPlanClass_Activity_Of_Class.Count).WrapText();
                sheet.GetRange(7, 1, startRow - 1, 4 + lstActivityPlanClass_Activity_Of_Class.Count).SetBorder(VTBorderStyle.Dashed, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            sheetCurrent2.Delete();
            sheetCurrent.Delete();
            sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            var time = new List<string>(TitleTime.Trim().Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries));
            String[] datetime = time[1].Split('/');
            int years = Convert.ToInt16(datetime[2]);
            int months = Convert.ToInt16(datetime[1]);
            int days = Convert.ToInt16(datetime[0]);
            fileName = "BangDanhGiaHoatDong_" + Utils.Utils.StripVNSignAndSpace(TitleClass) + "_" + days + "-" + months + "-" + years + ".xls";
            result.FileDownloadName = fileName;
            return result;
        }
        // get file and check to import file excel
        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImportCap(IEnumerable<HttpPostedFileBase> attachmentsCap, int SemesterID, int EducationLevelID)
        {
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (attachmentsCap == null || attachmentsCap.Count() <= 0)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachmentsCap.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePathCap"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;
                IVTWorksheet sheet2 = null;
                List<int> listTypeReportExcel = new List<int>();
                string strValue = string.Empty;
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 5120)
                {
                    return Json(new JsonMessage(Res.Get("Common_Max_Size_FileExcel"), "error"));
                }
                if (lstSheets != null && lstSheets.Count > 1)
                {
                    sheet = oBook.GetSheet(1);
                    sheet2 = oBook.GetSheet(2);
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("File excel không hợp lệ"), "error"));
                }
                var checkFILE = sheet.GetCellValue(1500, 1);
                if (checkFILE == null)
                {
                    return Json(new JsonMessage(Res.Get("File excel không hợp lệ"), "error"));
                }
                else
                {
                    if (checkFILE.ToString().Trim() != "DGHD")
                    {
                        return Json(new JsonMessage(Res.Get("File excel không hợp lệ"), "error"));
                    }
                }
                var checkDate = sheet.GetCellValue(4, 1);
                var DATE_DAY = new List<string>(checkDate.ToString().Trim().Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries));

                if (DATE_DAY[1] != null)
                {
                    var dateDay = DATE_DAY[1];
                    var dateDay2 = TempData["ActDate"].ToString();
                    if (dateDay.Trim() != dateDay2.Trim())
                    {
                        return Json(new JsonMessage(Res.Get("Ngày nhận xét không đúng"), "error"));
                    }
                    SearchInfo["ActivityDate"] = DateTime.Parse(dateDay);
                }
                int ClasID = int.Parse(TempData["ClassID"].ToString());
                // kế hoạch hoạt động
                List<ActivityPlanBO> lstActivityPlan = ActivityPlanBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).Where(s => s.IsActive).Distinct().ToList();
                List<int> lstActivityPlanID = lstActivityPlan.Select(p => p.ActivityPlanID).Distinct().ToList();

                List<Activity> lstActivity = ActivityBusiness.Search(SearchInfo).Where(s => lstActivityPlanID.Contains(s.ActivityPlanID)).ToList();
                //ActivityBusiness.All.Where(p =>lstActivityPlanID.Contains(p.ActivityPlanID)).ToList();
                //var lstActivityPlanClass = ActivityPlanClassBusiness.Search(SearchInfo).ToList();
                //ActivityPlanClassBusiness.All.Where(ac => lstActivityPlanID.Contains(ac.ActivityPlanID) && ac.ClassID == ClasID).ToList();

                // lấy Bảng Hoạt động lớp Join Hoạt động 
                var lstActivityPlanClass_Activity = (from a in lstActivity
                                                     //join b in lstActivityPlanClass
                                                     //on a.ActivityPlanID equals b.ActivityPlanID
                                                     select new ActivityBO
                                                     {
                                                         ActivityID = a.ActivityID,
                                                         ActivityName = a.ActivityName,
                                                         ActivityPlanID = a.ActivityPlanID,
                                                         FromTime = a.FromTime,
                                                         ToTime = a.ToTime,
                                                     }).ToList();
                List<SCSDailyActivityViewModel> lstView = new List<SCSDailyActivityViewModel>();
                // học sinh thuộc hoạt động
                List<int> lstActivityId = lstActivityPlanClass_Activity.Select(s => s.ActivityID).Distinct().ToList();
                SearchInfo["lstActivityID"] = lstActivityId;

                // lay list ActivityOfPupil 
                var lstActivityOfPupil = ActivityOfPupilBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();
                // lấy danh sách học sinh - lớp của bảng Pupil_Of_Class 
                var lstPuppilOfClass = PupilOfClassBussiness.Search(SearchInfo).ToList();
                ActivityOfPupil objPupil = null;
                PupilOfClass objPOC = null;
                List<PupilOfClass> lstPOC = new List<PupilOfClass>();
                List<ActivityOfPupil> lstObjPupil = new List<ActivityOfPupil>();
                List<string> listComment = new List<string>();
                string pupilCode = string.Empty;
                string ActivityName = string.Empty;
                int ActivityID = 0;
                string errorMessage = string.Empty;
                List<int> lstPupilID = new List<int>();
                Session[SCSDailyActivityConstants.PHISICAL_PATH_CAP] = physicalPath;
                List<ClassProfile> listClass = new List<ClassProfile>();
                IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicSearch["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsViewAll)
                {
                    dicSearch["UserAccountID"] = _globalInfo.UserAccountID;
                    dicSearch["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                    listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                }
                else
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                    dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                    if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                    {
                        dic["UserAccountID"] = _globalInfo.UserAccountID;
                        dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                    }
                    listClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                                .OrderBy(x => x.EducationLevelID)
                                                                                .ThenBy(x => x.DisplayName).ToList();
                }
                

                var checkName = listClass.Where(t => t.ClassProfileID == ClasID).Select(t => t.DisplayName).FirstOrDefault();
                if (Utils.Utils.StripVNSignAndSpace(checkName) != Utils.Utils.StripVNSignAndSpace(sheet.Name))
                {
                    return Json(new JsonMessage(Res.Get("File excel không phải là file của lớp " + checkName + ""), "error"));
                }
                List<ClassProfile> lstClasstmp = new List<ClassProfile>();
                dicSearch = new Dictionary<string, object>()
                            {
                                {"SchoolID",_globalInfo.SchoolID},
                                {"AcademicYearID",_globalInfo.AcademicYearID},
                            };
                if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
                {
                    lstClasstmp = listClass.Where(p => p.EducationLevelID == EducationLevelID && Utils.Utils.StripVNSignAndSpace(p.DisplayName) == Utils.Utils.StripVNSignAndSpace(sheet.Name)).ToList();
                    dicSearch.Add("EducationLevelID", EducationLevelID);
                }
                else
                {
                    lstClasstmp = listClass.Where(t => Utils.Utils.StripVNSignAndSpace(t.DisplayName) == Utils.Utils.StripVNSignAndSpace(sheet.Name)).ToList();
                }
                if (lstClasstmp != null && lstClasstmp.Count > 0)
                {
                    string className = string.Empty;
                    for (int i = 0; i < lstClasstmp.Count; i++)
                    {
                        className = lstClasstmp[i].DisplayName;
                        var ClassID = lstClasstmp[i].ClassProfileID;

                        SearchInfo["ClassID"] = ClassID;
                        var listPupilOfClass = lstPuppilOfClass.Where(t => t.ClassID == ClassID).ToList();
                        var lstAcOfPupil = lstActivityOfPupil.Where(t => t.ClassID == ClassID).ToList();
                        int pupilID = 0;
                        for (int startRow = 7; startRow < listPupilOfClass.Count + 7; startRow++)
                        {
                            if (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null && sheet.GetCellValue(startRow, 4) != null)
                            {
                                errorMessage = string.Empty;
                                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                                objPOC = listPupilOfClass.Where(p => p.PupilProfile.PupilCode == pupilCode).FirstOrDefault();
                                if (objPOC == null)
                                {
                                    errorMessage = Res.Get("RatedCommentPupiil_Label_Not_Pupil_Status_Studying");
                                }
                                else
                                {
                                    pupilID = objPOC.PupilID;
                                    lstPupilID.Add(pupilID);
                                    if (lstPupilID.Where(p => p == pupilID).Count() > 1)
                                    {
                                        errorMessage = Res.Get("Rated_PupilCode_Duplicate");
                                    }
                                }
                                int startCol = 5;
                                int rowSheet2 = 3;
                                // danh sách PupilID
                                while (sheet.GetCellValue(6, startCol) != null)
                                {
                                    errorMessage = string.Empty;
                                    ActivityID = int.Parse(sheet.GetCellValue(1500, startCol).ToString());
                                    int ActivityOfPupidID = lstAcOfPupil.Where(t => t.ActivityOfClassID == ActivityID && t.PupilID == pupilID).Select(t => t.ActivityOfPupilID).FirstOrDefault();
                                    var COMMENT = sheet.GetCellValue(startRow, startCol) != null ? sheet.GetCellValue(startRow, startCol).ToString() : "";
                                    if (COMMENT.Length > 500)
                                    {
                                        return Json(new JsonMessage(Res.Get("Nội dung đánh giá không được > 500 ký tự"), "error"));
                                    }
                                    objPupil = new ActivityOfPupil();
                                    objPupil.ActivityOfPupilID = ActivityOfPupidID;
                                    objPupil.PupilID = pupilID;
                                    objPupil.ClassID = ClassID;
                                    objPupil.SchoolID = _globalInfo.SchoolID.Value;
                                    objPupil.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                    objPupil.CommentOfTeacher = COMMENT;
                                    objPupil.ActivityOfClassID = ActivityID;
                                    lstObjPupil.Add(objPupil);
                                    rowSheet2++;
                                    startCol++;
                                }
                            }
                        }
                    }
                    SearchInfo.Add("lstPupilID", lstPupilID);
                    ActivityOfPupilBusiness.Insert(lstObjPupil, SearchInfo);
                }
            }
            return Json(new { Type = "success", Message = "Cập nhật từ Excel thành công" });
        }
        #endregion
    }
}
