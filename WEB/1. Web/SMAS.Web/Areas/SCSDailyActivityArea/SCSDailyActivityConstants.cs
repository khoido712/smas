﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SCSDailyActivityArea
{
    public class SCSDailyActivityConstants
    {
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_BLOCK = "LIST_BLOCK";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_PUPIL = "LIST_PUPIL";
        public const string LIST_DAILY_ACTIVITY = "LIST_DAILY_ACTIVITY";
        public const string CLASS_NAME = "CLASS_NAME";
        public const string CLASS_ID = "CLASS_ID";
        public const string TOTAL_PUPIL = "TOTAL_PUPIL";
        public const string CLASS_NULL = "classnull";
        public const string PUPIL = "PUPIL";
        public const string LIST_SMS_TYPE = "LIST_SMS_TYPE";
        public const string LST_MONTH = "listMonth";
        public const string SubjectID = "SubjectID";
        public const string LST_PUPIT_PRO = "LST_PUPIT_PRO";

        public const string ACT_DATE = "ACT_DATE";
        public const string ACT_CLASSID = "ACT_CLASSID";
        public const string LIST_SUBJECT = "LIST_SUBJECT";
        public const string LIST_SUBJECT_ACCOUNT = "LIST_SUBJECT_ACCOUNT";
        public const string LIST_COLLECTION_COMMENT = "LIST_COLLECTION_COMMENT";
        public const string SUBJECT_CODE = "SUBJECT_CODE";
        public const string CELL_CHECK_TYPE_FILE_IMPORT = "Y3";
        public const string PHISICAL_PATH_CAP = "PHISICAL_PATH_CAP";

        public const string GVCN_TEACHER = "GVCN_TEACHER";
        public const string LIST_AUTOCOMPLETE = "LIST_AUTOCOMPLETELIST_AUTOCOMPLETE";
        //TEMPLATE_FILE_COLLECTION_COMMENTS
        public const string TEMPLATE_FILE_COMMENTS_OF_KINDERGARTEN = "TEMPLATE_FILE_COMMENTS_OF_KINDERGARTEN";

        public const string A = "A";
       
    }
}