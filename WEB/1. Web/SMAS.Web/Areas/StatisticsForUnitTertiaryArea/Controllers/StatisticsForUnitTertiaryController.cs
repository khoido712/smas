﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.StatisticsForUnitTertiaryArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System.IO;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.StatisticsForUnitTertiaryArea.Controllers
{
    public class StatisticsForUnitTertiaryController : BaseController
    {

        IStatisticsForUnitBusiness StatisticsForUnitBusiness;
        IStatisticsConfigBusiness StatisticsConfigBusiness;
        IMarkStatisticBusiness MarkStatisticBusiness;
        ICapacityStatisticBusiness CapacityStatisticBusiness;
        IConductStatisticBusiness ConductStatisticBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        ISubjectCatBusiness SubjectCatBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        ISubCommitteeBusiness SubCommitteeBusiness;

        public StatisticsForUnitTertiaryController(IStatisticsForUnitBusiness statisticsForUnitBusiness,
                                                    IStatisticsConfigBusiness statisticsConfigBusiness,
                                                    IMarkStatisticBusiness markStatisticBusiness,
                                                    ICapacityStatisticBusiness capacityStatisticBusiness,
                                                    IConductStatisticBusiness conductStatisticBusiness,
                                                    IEducationLevelBusiness educationLevelBusiness,
                                                    ISubjectCatBusiness subjectCatBusiness,
                                                    IAcademicYearBusiness academicYearBusiness,
                                                    ISubCommitteeBusiness subCommitteeBusiness)
        {
            this.StatisticsForUnitBusiness = statisticsForUnitBusiness;
            this.StatisticsConfigBusiness = statisticsConfigBusiness;
            this.MarkStatisticBusiness = markStatisticBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();

            List<SubCommittee> listSubCommittee = SubCommitteeBusiness.Search(new Dictionary<string, object>())
                .OrderBy(o => o.Resolution)
                .ToList();

            ViewData[StatisticsForUnitTertiaryConstants.LIST_EDUCATION_LEVEL] = new SelectList(glo.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[StatisticsForUnitTertiaryConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
            ViewData[StatisticsForUnitTertiaryConstants.LIST_SEMESTER_ALL] = new SelectList(CommonList.SemesterAndAll(), "key", "value");
            ViewData[StatisticsForUnitTertiaryConstants.LIST_SUBCOMMITTEE] = new SelectList(listSubCommittee, "SubCommitteeID", "Resolution");
            ViewData[StatisticsForUnitTertiaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitTertiaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            ViewData[StatisticsForUnitTertiaryConstants.LIST_JUDGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            ViewData[StatisticsForUnitTertiaryConstants.LIST_MARK] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            ViewData[StatisticsForUnitTertiaryConstants.LIST_CONDUCT_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);
            ViewData[StatisticsForUnitTertiaryConstants.LIST_CAPACITY_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);
        }

        #region CAPACITY

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchCapacity(StatisticsForUnitTertiaryViewModel model)
        {
            ViewData[StatisticsForUnitTertiaryConstants.LIST_CAPACITY_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
            sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;

            List<CapacityStatisticsBO> listData = StatisticsForUnitBusiness.SearchCapacityForTertiary(sfu);
            listData.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubCommittee = SubCommitteeBusiness.Find(ms.SubCommitteeID);
            });

            ViewData[StatisticsForUnitTertiaryConstants.GRID_STATISTICS_CAPACITY] = listData;
            ViewData[StatisticsForUnitTertiaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listData.Count);

            return PartialView("_CapacityGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendCapacity(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;

                List<CapacityStatisticsBO> listData = StatisticsForUnitBusiness.SearchCapacityForTertiary(sfu);
                CapacityStatisticBusiness.SendAll(listData);
                CapacityStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportCapacity(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateCapacityForTertiaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region CAPACITY SUBJECT

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchCapacitySubject(StatisticsForUnitTertiaryViewModel model)
        {
            ViewData[StatisticsForUnitTertiaryConstants.LIST_JUDGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            ViewData[StatisticsForUnitTertiaryConstants.LIST_MARK] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<CapacityStatisticsBO> listCapacitySubjectStatistic = StatisticsForUnitBusiness.SearchSubjectCapacityForTertiary(sfu);

            listCapacitySubjectStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });
            // Sap xep theo mon hoc
            listCapacitySubjectStatistic = listCapacitySubjectStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            ViewData[StatisticsForUnitTertiaryConstants.GRID_STATISTICS_CAPACITY_SUBJECT] = listCapacitySubjectStatistic;
            ViewData[StatisticsForUnitTertiaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listCapacitySubjectStatistic.Count);

            return PartialView("_CapacitySubjectGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendCapacitySubject(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<CapacityStatisticsBO> data = StatisticsForUnitBusiness.SearchSubjectCapacityForTertiary(sfu);
                CapacityStatisticBusiness.SendAll(data);
                CapacityStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportCapacitySubject(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateSubjectCapacityForTertiaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region CONDUCT

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchConduct(StatisticsForUnitTertiaryViewModel model)
        {
            ViewData[StatisticsForUnitTertiaryConstants.LIST_CONDUCT_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
            sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;

            List<ConductStatisticsBO> listData = StatisticsForUnitBusiness.SearchConductForTertiary(sfu);
            listData.ForEach(ms => { 
                    ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                    ms.SubCommittee = SubCommitteeBusiness.Find(ms.SubCommitteeID);
            });

            ViewData[StatisticsForUnitTertiaryConstants.GRID_STATISTICS_CONDUCT] = listData;
            ViewData[StatisticsForUnitTertiaryConstants.COUNT_DATA] = ViewData[StatisticsForUnitTertiaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listData.Count);

            return PartialView("_ConductGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendConduct(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;

                List<ConductStatisticsBO> listData = StatisticsForUnitBusiness.SearchConductForTertiary(sfu);
                ConductStatisticBusiness.SendAll(listData);
                ConductStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportConduct(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateConductForTertiaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region PERIOD

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchPeriod(StatisticsForUnitTertiaryViewModel model)
        {
            ViewData[StatisticsForUnitTertiaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitTertiaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<MarkStatisticBO> listMarkStatistic = StatisticsForUnitBusiness.SearchPeriodicMarkForTertiary(sfu);

            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[StatisticsForUnitTertiaryConstants.GRID_STATISTICS_PERIOD] = listMarkStatistic;
            ViewData[StatisticsForUnitTertiaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_PeriodGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendPeriod(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<MarkStatisticBO> data = StatisticsForUnitBusiness.SearchPeriodicMarkForTertiary(sfu);
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportPeriod(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreatePeriodicMarkForTertiaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region SEMESTER

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSemester(StatisticsForUnitTertiaryViewModel model)
        {
            ViewData[StatisticsForUnitTertiaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitTertiaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<MarkStatisticBO> listMarkStatistic = StatisticsForUnitBusiness.SearchSemesterMarkForTertiary(sfu);

            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[StatisticsForUnitTertiaryConstants.GRID_STATISTICS_SEMESTER] = listMarkStatistic;
            ViewData[StatisticsForUnitTertiaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_SemesterGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendSemester(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<MarkStatisticBO> data = StatisticsForUnitBusiness.SearchSemesterMarkForTertiary(sfu);
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportSemester(StatisticsForUnitTertiaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateSemesterMarkForTertiaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
