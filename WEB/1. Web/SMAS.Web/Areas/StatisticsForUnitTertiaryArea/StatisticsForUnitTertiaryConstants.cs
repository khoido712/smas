﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticsForUnitTertiaryArea
{
    public class StatisticsForUnitTertiaryConstants
    {
        public const string LIST_EDUCATION_LEVEL = "ListEducationLevel";
        public const string LIST_SEMESTER = "ListSemester";
        public const string LIST_SEMESTER_ALL = "ListSemesterAll";
        public const string LIST_SUBCOMMITTEE = "ListSubCommittee";

        public const string LIST_BELOW_AVERAGE = "ListBelowAverage";
        public const string LIST_ABOVE_AVERAGE = "ListAboveAverage";

        public const string LIST_JUDGE = "ListJudge";
        public const string LIST_MARK = "ListMark";

        public const string LIST_CONDUCT_LEVEL = "ListConductLevel";
        public const string LIST_CAPACITY_LEVEL = "ListCapacityLevel";

        public const string GRID_STATISTICS_PERIOD = "GridStatisticsPeriod";
        public const string GRID_STATISTICS_SEMESTER = "GridStatisticsSemester";
        public const string GRID_STATISTICS_CAPACITY_SUBJECT = "GridStatisticsCapacitySubject";
        public const string GRID_STATISTICS_CONDUCT = "GridStatisticsConduct";
        public const string GRID_STATISTICS_CAPACITY = "GridStatisticsCapacity";
        public const string COUNT_DATA = "CountData";
    }
}