﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticsForUnitTertiaryArea
{
    public class StatisticsForUnitTertiaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticsForUnitTertiaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticsForUnitTertiaryArea_default",
                "StatisticsForUnitTertiaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
