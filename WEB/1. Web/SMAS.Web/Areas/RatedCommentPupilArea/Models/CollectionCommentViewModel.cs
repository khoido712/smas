﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RatedCommentPupilArea.Models
{
    public class CollectionCommentViewModel
    {
        public long CollectionCommentsID { get; set; }
        public int SubjectID { get; set; }
        public string Comment { get; set; }
        public string CommentCode { get; set; }
        public bool? isChecked { get; set; }
        public bool isEnable { get; set; }
    }
}