﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RatedCommentPupilArea.Models
{
    public class RatedCommentViewModel
    {
        public string Error { get; set; }
        public List<EvaluationGridModel> lstEvalutionComment { get; set; }

    }
}