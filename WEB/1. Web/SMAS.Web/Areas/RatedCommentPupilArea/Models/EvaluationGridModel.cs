﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RatedCommentPupilArea.Models
{
    public class EvaluationGridModel
    {
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public string ClassName { get; set; }
        public int IsCommenting { get; set; }
        public int SemesterID { get; set; }
        public int TypeID { get; set; }
        public int Status { get; set; }
        public int? PeriodicMiddleMark { get; set; }
        public int? MiddleEvaluation { get; set; }
        public string MiddleEvaluationName { get; set; }
        public int? PeriodicEndingMark { get; set; }
        public int? EndingEvaluation { get; set; }
        public string EndingEvaluationName { get; set; }
        public string PeriodicMiddleJudgement { get; set; }
        public string PeriodicEndingJudgement { get; set; }
        public int? CapacityMiddleEvaluation { get; set; }
        public int? CapacityEndingEvaluation { get; set; }
        public int? QualityMiddleEvaluation { get; set; }
        public int? QualityEndingEvaluation { get; set; }
        public string CapacityMiddleEvaluationName { get; set; }
        public string CapacityEndingEvaluationName { get; set; }
        public string QualityMiddleEvaluationName { get; set; }
        public string QualityEndingEvaluationName { get; set; }
        public List<EvaluationCriteriaModel> lstEvalutionCriteria { get; set; }  
        public string Comment { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int EvaluationID { get; set; }
        public string FullNameComment { get; set; }
        public int LogChangeID { get; set; }
        public string ErrMessage { get; set; }
        public bool isErr { get; set; }
    }
    public class EvaluationCriteriaModel
    {
        public int EvaluationCriteriaID { get; set; }
        public string CriteriaName { get; set; }
        public int? GK { get; set; }
        public string GKName { get; set; }
        public int? CK { get; set; }
        public string CKName { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string Comment { get; set; }
        public int MyProperty { get; set; }
        public int LogChangeID { get; set; }
        public int EvaluationID { get; set; }
        public int TypeID { get; set; }
    }
}