﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RatedCommentPupilArea
{
    public class RatedCommentPupilConstants
    {
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string CLASS_NULL = "classnull";
        public const string ClassID = "ClassID";
        public const string SubjectID = "SubjectID";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_SUBJECT_ACCOUNT = "listSubjectAccount";
        public const string ShowList = "ShowList";
        public const string TYPE_SELECT_ID = "TypeSelectID";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string IS_VISIBLED_BUTTON = "IsVisibledButton";
        public const string IS_TOANTV = "IsToanTV";
        public const string HEAD_TEACHE = "HeadTeacher";
        public const string GVBM_TEACHER = "GVBMTeacher";
        public const string GVCN_TEACHER = "GVCNTeacher";
        public const string SEMESTER_ID = "SemesterID";
        public const string ISCOMMENTING = "IsCommenting";
        public const string EVALUATION_ID = "EvaluationID";
        public const string OLD_SUBJECT_ID = "OldSubjectID";
        public const string LIST_PUPIL_EVALUATION = "ListPupilProfileEvaluation";
        public const string LIST_EVALUATION_INDEX = "lstEvaluationIndex";
        public const string TEMPLATE_FILE_TYPE12 = "SoDanhGiaHS_1A.xls";
        public const string TEMPLATE_FILE_COLLECTION_COMMENTS = "GV_NganHangNhanXet.xls";
        public const string TEMPLATE_FILE_TYPE3 = "SoDanhGiaHS_MonToan.xls";
        public const string TEMPLATE_FILE_NLPC = "SoDanhGiaHS_NLPC.xls";
        public const string LAST_UPDATE_PERIOD_MARK = "LAST_UPDATE_PERIOD_MARK";
        public const string LOCK_MARK_TITLE = "LockMarkTitle";
        public const int FILE_UPLOAD_MAX_SIZE = 5120;
        public const string PHISICAL_PATH = "PhisicalPath";
        public const string PHISICAL_PATH_CAP = "PhisicalPathCap";
        public const string CELL_CHECK_TYPE_FILE_IMPORT = "Y3";
        public const string EVALUATION_CRITERIA = "EvaluationCriteria";
        public const string LIST_COLLECTION_COMMENT = "lstCollectionComment";
        public const string SUBJECT_CODE = "SubjectCode";
        public const string LIST_AUTOCOMPLETE = "lstAutocomplete";
        public const string IS_LOCK_INPUT = "IsLockInput";
        public const string LOCK_USER_NAME = "LockUserName";
    }
}