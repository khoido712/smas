﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using System.Web.Mvc;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Areas.RatedCommentPupilArea;
using SMAS.Web.Areas.RatedCommentPupilArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Text;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.RatedCommentPupilArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class RatedCommentPupilController : BaseController
    {
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IBookmarkedFunctionBusiness BookmarkedFunctionBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        private readonly ICollectionCommentsBusiness CollectionCommentsBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        public RatedCommentPupilController(IRatedCommentPupilBusiness ratedCommentPupilBusiness, IAcademicYearBusiness academicYearBusiness,
         IClassProfileBusiness classProfileBusiness, IClassSubjectBusiness classSubjectBusiness, ISubjectCatBusiness subjectCatBusiness,
         IClassSupervisorAssignmentBusiness classSupervisorAssignmentBusiness, IUserAccountBusiness userAccountBusiness,
         ITeachingAssignmentBusiness teachingAssignmentBusiness, IBookmarkedFunctionBusiness bookmarkedFunctionBusiness,
         IPupilOfClassBusiness pupilOfClassBusiness, ILockRatedCommentPupilBusiness lockRatedCommentPupilBusiness,
         IRatedCommentPupilHistoryBusiness ratedCommentPupilHistoryBusiness, ISchoolProfileBusiness schoolProfileBusiness, IEmployeeBusiness employeeBusiness,
         IEvaluationCriteriaBusiness evaluationCriteriaBusiness, ICollectionCommentsBusiness collectionCommentsBusiness,
          IRestoreDataBusiness RestoreDataBusiness,
          IRestoreDataDetailBusiness RestoreDataDetailBusiness
            )
        {
            this.RatedCommentPupilBusiness = ratedCommentPupilBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassSupervisorAssignmentBusiness = classSupervisorAssignmentBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.BookmarkedFunctionBusiness = bookmarkedFunctionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.LockRatedCommentPupilBusiness = lockRatedCommentPupilBusiness;
            this.RatedCommentPupilHistoryBusiness = ratedCommentPupilHistoryBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.EvaluationCriteriaBusiness = evaluationCriteriaBusiness;
            this.CollectionCommentsBusiness = collectionCommentsBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
            this.RestoreDataDetailBusiness = RestoreDataDetailBusiness;
        }
        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }
        public ActionResult _index(int? ClassID)
        {
            this.SetViewData(ClassID);
            return PartialView();
        }
        [HttpPost]
        public PartialViewResult GetSubjectPanel()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int TypeID = SMAS.Business.Common.Utils.GetInt(Request["TypeSelectID"]);
            int OldSubjectID = SMAS.Business.Common.Utils.GetInt(Request["oldSubjectID"]);
            int semester = semesterid == null ? 0 : semesterid.Value;
            ViewData[RatedCommentPupilConstants.ClassID] = classid.Value;
            if (TypeID == 1)
            {
                bool ViewAll = false;

                if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
                {
                    ViewAll = true;
                }
                int schoolId = _globalInfo.SchoolID.Value;
                List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
                if (classid.HasValue && semesterid.HasValue)
                {
                    lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semesterid.Value, classid.Value, ViewAll).Where(u => semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                      join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                      where s.IsActive == true
                                      && s.IsApprenticeshipSubject == false
                                      select new SubjectCatBO()
                                      {
                                          SubjectCatID = s.SubjectCatID,
                                          DisplayName = s.DisplayName,
                                          IsCommenting = cs.IsCommenting,
                                          OrderInSubject = s.OrderInSubject
                                      }
                                      ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                    Session["ListSubject"] = lsClassSubject;
                }
                ViewData["ListSubjectComment"] = lsClassSubject;
                ViewData[RatedCommentPupilConstants.ShowList] = false;
                ViewData[RatedCommentPupilConstants.OLD_SUBJECT_ID] = OldSubjectID;
            }
            ViewData[RatedCommentPupilConstants.TYPE_SELECT_ID] = TypeID;
            return PartialView("_ViewSubject");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddMenuUsual(string url)
        {
            Menu objMenu = SMAS.Business.Common.StaticData.ListMenu.Where(c => c.URL.Contains(url)).FirstOrDefault();

            List<BookmarkedFunction> lstBm = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.GetValueOrDefault(),
                _globalInfo.AcademicYearID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID);

            BookmarkedFunction objDB = lstBm.Where(p => p.MenuID == objMenu.MenuID).FirstOrDefault();

            if (objDB == null)
            {
                BookmarkedFunction bf = new BookmarkedFunction();
                bf.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bf.AcademicYearID = _globalInfo.AcademicYearID;
                bf.LevelID = _globalInfo.AppliedLevel;
                bf.UserID = _globalInfo.UserAccountID;
                bf.MenuID = objMenu.MenuID;
                bf.Semester = _globalInfo.Semester;
                string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", objMenu.MenuName);
                bf.FunctionName = string.IsNullOrEmpty(name) ? objMenu.MenuName : name;
                bf.Order = lstBm.Count + 1;
                bf.CreatedDate = DateTime.Now;

                BookmarkedFunctionBusiness.Insert(bf);
                BookmarkedFunctionBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("Add_Menu_To_List"), "success"));
        }

        public PartialViewResult AjaxLoadGrid(int ClassID, int SemesterID, int SubjectID, int TypeID)
        {
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            //lay danh sach hoc sinh cua lop
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
            if (isNotShowPupil)
            {
                iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            lstPOC = iquery.ToList();
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            ViewData[RatedCommentPupilConstants.SEMESTER_ID] = SemesterID;
            //lay danh sach khoa diem
            IDictionary<string, object> dicLockMark = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"EvaluationID",TypeID}
            };
            if (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
            {
                dicLockMark.Add("SubjectID", SubjectID);
            }
            string strLockMarkTitle = string.Empty;
            IQueryable<LockRatedCommentPupil> iqLockRated = LockRatedCommentPupilBusiness.Search(dicLockMark);
            LockRatedCommentPupil objLockRated = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID},
                {"TypeID",TypeID}
            };
            SetVisbileButton(SemesterID, ClassID, SubjectID, TypeID);
            //lay danh sach commnet
            IDictionary<string, object> dicAccountComment = new Dictionary<string, object>()
            {
                {"AccountID",_globalInfo.UserAccountID}
            };
            if (TypeID == 2)
            {
                dicAccountComment.Add("SubjectID", -1);
            }
            else if (TypeID == 3)
            {
                dicAccountComment.Add("SubjectID", -2);
            }
            else
            {
                dicAccountComment.Add("SubjectID", SubjectID);
            }
            List<CommentAutocompleteModel> lstCommentAutocomplete = (from cc in CollectionCommentsBusiness.SearchCollectionComment(dicAccountComment)
                                                                     select new CommentAutocompleteModel
                                                                     {
                                                                         CommentCode = cc.CommentCode,
                                                                         Comment = cc.Comment
                                                                     }).OrderBy(p => p.Comment).ToList();
            ViewData[RatedCommentPupilConstants.LIST_AUTOCOMPLETE] = lstCommentAutocomplete;
            //check xem so co khoa nhap lieu hay khong
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle))
            {
                isLockInput = objLockInput.LockTitle.Contains("HK" + SemesterID);
            }
            ViewData[RatedCommentPupilConstants.IS_LOCK_INPUT] = isLockInput;
            if (isLockInput)
            {
                string LockUserName = string.Empty;
                int LockUserID = SemesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST ? objLockInput.UserLock1ID.Value : objLockInput.UserLock2ID.Value;
                int HierachyLevelID = this.GetHierachyLevelIDByUserAccountID(LockUserID);
                LockUserName = HierachyLevelID == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE ? "Sở giáo dục" : HierachyLevelID == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE ? "Phòng giáo dục" : "";
                ViewData[RatedCommentPupilConstants.LOCK_USER_NAME] = "Sổ đánh giá học sinh đã bị khóa nhập liệu bởi " + LockUserName;
            }
            #region Load Mon hoc & HDGD
            if (TypeID == 1)
            {
                bool isGVBM = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, ClassID, SubjectID, SemesterID);
                ViewData[RatedCommentPupilConstants.GVBM_TEACHER] = isGVBM;
                dic.Add("SubjectID", SubjectID);
                this.GetListViewData(lstPOC, dic);
                SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
                bool isEnableGK = ((objCP.EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID4 || objCP.EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID5)
                                  && ("Tt".Equals(objSC.Abbreviation) || "TV".Equals(objSC.Abbreviation)));
                ViewData[RatedCommentPupilConstants.IS_TOANTV] = isEnableGK;
                int IsCommenting = CheckSubjectCommenting(SubjectID, ClassID);
                ViewData[RatedCommentPupilConstants.ISCOMMENTING] = IsCommenting;
                objLockRated = iqLockRated.FirstOrDefault();
                strLockMarkTitle = objLockRated != null ? objLockRated.LockTitle : string.Empty;
                ViewData[RatedCommentPupilConstants.LOCK_MARK_TITLE] = strLockMarkTitle;
                return PartialView("_GridSubjectAndEducationGroup");
            }
            #endregion
            #region Load Nang luc/Pham chat
            else
            {
                bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
                ViewData[RatedCommentPupilConstants.GVCN_TEACHER] = isGVCN;
                this.GetListViewData(lstPOC, dic);
                List<LockRatedCommentPupil> lstLockRatedtmp = iqLockRated.ToList();
                for (int i = 0; i < lstLockRatedtmp.Count; i++)
                {
                    objLockRated = lstLockRatedtmp[i];
                    strLockMarkTitle += objLockRated != null ? objLockRated.LockTitle + "," : string.Empty;
                }
                ViewData[RatedCommentPupilConstants.LOCK_MARK_TITLE] = strLockMarkTitle;
                List<EvaluationCriteiaBO> lstEGBO = new List<EvaluationCriteiaBO>();
                if (TypeID == 2)
                {
                    lstEGBO = EvaluationCriteriaBusiness.All.Where(p => p.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                                                            .Select(p => new EvaluationCriteiaBO
                                                            {
                                                                EvaluationCriteriaID = p.EvaluationCriteriaID,
                                                                CriteriaName = p.CriteriaName
                                                            }).OrderBy(p => p.EvaluationCriteriaID).ToList();
                    ViewData[RatedCommentPupilConstants.EVALUATION_CRITERIA] = lstEGBO;
                    return PartialView("_GridCapacities");
                }
                else
                {
                    lstEGBO = EvaluationCriteriaBusiness.All.Where(p => p.TypeID == GlobalConstants.EVALUATION_QUALITY)
                                                            .Select(p => new EvaluationCriteiaBO
                                                            {
                                                                EvaluationCriteriaID = p.EvaluationCriteriaID,
                                                                CriteriaName = p.CriteriaName
                                                            }).OrderBy(p => p.EvaluationCriteriaID).ToList();
                    ViewData[RatedCommentPupilConstants.EVALUATION_CRITERIA] = lstEGBO;
                    return PartialView("_GridQualities");
                }

            }
            #endregion
        }

        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveComentsPupil(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int SubjectID = !string.IsNullOrEmpty(frm["SubjectID"]) ? int.Parse(frm["SubjectID"]) : 0;
            int SemesterID = !string.IsNullOrEmpty(frm["Semester"]) ? int.Parse(frm["Semester"]) : 0;
            int TypeID = !string.IsNullOrEmpty(frm["hdfTypeID"]) ? int.Parse(frm["hdfTypeID"]) : 0;
            SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
            //lay danh sach hoc sinh trong lop
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).Distinct().ToList();
            PupilOfClassBO objPOC = null;
            List<RatedCommentPupilBO> lstRatedCommentPupilBO = new List<RatedCommentPupilBO>();
            RatedCommentPupilBO objRatedBO = null;
            string UserNameComment = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeName : Res.Get("Message_Label_GroupUserSchoolAdmin");
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            int PupilID = 0;
            int isCommenting = this.CheckSubjectCommenting(SubjectID, ClassID);
            string Comment = string.Empty;
            if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
            {
                for (int i = 0; i < lstPOC.Count; i++)
                {
                    objPOC = lstPOC[i];
                    PupilID = objPOC.PupilID;
                    if (!"on".Equals(frm["chk_" + PupilID]) || objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        continue;
                    }
                    objRatedBO = new RatedCommentPupilBO();
                    objRatedBO.SchoolID = _globalInfo.SchoolID.Value;
                    objRatedBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objRatedBO.PupilID = PupilID;
                    objRatedBO.ClassID = ClassID;
                    objRatedBO.SemesterID = SemesterID;
                    objRatedBO.SubjectID = SubjectID;
                    objRatedBO.EvaluationID = TypeID;
                    objRatedBO.FullNameComment = UserNameComment;
                    objRatedBO.LogChangeID = LogChangeID;
                    objRatedBO.FullName = objPOC.PupilFullName;
                    objRatedBO.PupilCode = objPOC.PupilCode;
                    objRatedBO.ClassName = objPOC.ClassName;
                    objRatedBO.SubjectName = objSC != null ? objSC.SubjectName : "";
                    objRatedBO.isCommenting = isCommenting;
                    if (!string.IsNullOrEmpty(frm["KTGKMark_" + PupilID]))
                    {
                        objRatedBO.PeriodicMiddleMark = int.Parse(frm["KTGKMark_" + PupilID]);
                    }
                    if (!string.IsNullOrEmpty(frm["DGGK_" + PupilID]))
                    {
                        objRatedBO.MiddleEvaluation = frm["DGGK_" + PupilID].Equals(GlobalConstants.EVALUATION_GOOD_COMPLETE) ? 1 : frm["DGGK_" + PupilID].Equals(GlobalConstants.EVALUATION_COMPLETE) ? 2 : 3;
                        objRatedBO.MiddleEvaluationName = frm["DGGK_" + PupilID];
                    }
                    if (!string.IsNullOrEmpty(frm["KTCKMark_" + PupilID]))
                    {
                        objRatedBO.PeriodicEndingMark = int.Parse(frm["KTCKMark_" + PupilID]);
                    }
                    if (!string.IsNullOrEmpty(frm["DGCK_" + PupilID]))
                    {
                        objRatedBO.EndingEvaluation = frm["DGCK_" + PupilID].Equals(GlobalConstants.EVALUATION_GOOD_COMPLETE) ? 1 : frm["DGCK_" + PupilID].Equals(GlobalConstants.EVALUATION_COMPLETE) ? 2 : 3;
                        objRatedBO.EndingEvaluationName = frm["DGCK_" + PupilID];
                    }
                    if (!string.IsNullOrEmpty(frm["txtSubjectCommentID_" + PupilID]))
                    {
                        Comment = frm["txtSubjectCommentID_" + PupilID];
                    }
                    else
                    {
                        Comment = string.Empty;
                    }
                    objRatedBO.Comment = Comment;
                    lstRatedCommentPupilBO.Add(objRatedBO);
                }
            }
            else
            {
                List<EvaluationCriteiaBO> lstEGBO = new List<EvaluationCriteiaBO>();
                EvaluationCriteiaBO objEGBO = null;
                if (TypeID == GlobalConstants.EVALUATION_CAPACITY)
                {
                    lstEGBO = EvaluationCriteriaBusiness.All.Where(p => p.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                                                            .Select(p => new EvaluationCriteiaBO
                                                            {
                                                                EvaluationCriteriaID = p.EvaluationCriteriaID,
                                                                CriteriaName = p.CriteriaName
                                                            }).OrderBy(p => p.EvaluationCriteriaID).ToList();
                }
                else
                {
                    lstEGBO = EvaluationCriteriaBusiness.All.Where(p => p.TypeID == GlobalConstants.EVALUATION_QUALITY)
                                                            .Select(p => new EvaluationCriteiaBO
                                                            {
                                                                EvaluationCriteriaID = p.EvaluationCriteriaID,
                                                                CriteriaName = p.CriteriaName
                                                            }).OrderBy(p => p.EvaluationCriteriaID).ToList();
                }

                for (int i = 0; i < lstEGBO.Count; i++)
                {
                    objEGBO = lstEGBO[i];
                    for (int j = 0; j < lstPOC.Count; j++)
                    {
                        objPOC = lstPOC[j];
                        PupilID = objPOC.PupilID;
                        if (!"on".Equals(frm["chk_" + PupilID]) || objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                        {
                            continue;
                        }
                        objRatedBO = new RatedCommentPupilBO();
                        objRatedBO.SchoolID = _globalInfo.SchoolID.Value;
                        objRatedBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objRatedBO.PupilID = PupilID;
                        objRatedBO.ClassID = ClassID;
                        objRatedBO.SemesterID = SemesterID;
                        objRatedBO.SubjectID = objEGBO.EvaluationCriteriaID;
                        objRatedBO.EvaluationID = TypeID;
                        objRatedBO.LogChangeID = LogChangeID;
                        objRatedBO.FullName = objPOC.PupilFullName;
                        objRatedBO.PupilCode = objPOC.PupilCode;
                        objRatedBO.ClassName = objPOC.ClassName;
                        if (TypeID == GlobalConstants.EVALUATION_CAPACITY)
                        {
                            if (!string.IsNullOrEmpty(frm["CapacityGK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID]))
                            {
                                objRatedBO.CapacityMiddleEvaluation = frm["CapacityGK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID] == GlobalConstants.EVALUATION_GOOD_COMPLETE ? 1
                                    : frm["CapacityGK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID] == GlobalConstants.EVALUATION_CAPQUA_COMPLETE ? 2 : 3;
                                objRatedBO.CapacityMiddleEvaluationName = frm["CapacityGK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID];
                            }

                            if (!string.IsNullOrEmpty(frm["CapacityCK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID]))
                            {
                                objRatedBO.CapacityEndingEvaluation = frm["CapacityCK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID] == GlobalConstants.EVALUATION_GOOD_COMPLETE ? 1
                                    : frm["CapacityCK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID] == GlobalConstants.EVALUATION_CAPQUA_COMPLETE ? 2 : 3;
                                objRatedBO.CapacityEndingEvaluationName = frm["CapacityCK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID];
                            }
                            if (!string.IsNullOrEmpty(frm["txtCommentCapID_" + PupilID]))
                            {
                                Comment = frm["txtCommentCapID_" + PupilID];
                            }
                            else
                            {
                                Comment = string.Empty;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(frm["QualityGK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID]))
                            {
                                objRatedBO.QualityMiddleEvaluation = frm["QualityGK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID] == GlobalConstants.EVALUATION_GOOD_COMPLETE ? 1
                                    : frm["QualityGK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID] == GlobalConstants.EVALUATION_CAPQUA_COMPLETE ? 2 : 3;
                                objRatedBO.QualityMiddleEvaluationName = frm["QualityGK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID];
                            }

                            if (!string.IsNullOrEmpty(frm["QualityCK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID]))
                            {
                                objRatedBO.QualityEndingEvaluation = frm["QualityCK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID] == GlobalConstants.EVALUATION_GOOD_COMPLETE ? 1
                                    : frm["QualityCK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID] == GlobalConstants.EVALUATION_CAPQUA_COMPLETE ? 2 : 3;
                                objRatedBO.QualityEndingEvaluationName = frm["QualityCK_" + objEGBO.EvaluationCriteriaID + "_" + PupilID];
                            }

                            if (!string.IsNullOrEmpty(frm["txtCommentQuaID_" + PupilID]))
                            {
                                Comment = frm["txtCommentQuaID_" + PupilID];
                            }
                            else
                            {
                                Comment = string.Empty;
                            }
                        }


                        objRatedBO.Comment = Comment;
                        lstRatedCommentPupilBO.Add(objRatedBO);
                    }
                }
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID},
                {"SubjectID",SubjectID},
                {"IsImport",false},
                {"TypeID",TypeID}
            };
            List<ActionAuditDataBO> lstActionAudit = new List<ActionAuditDataBO>();
            ActionAuditDataBO objActionAudit = null;
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                RatedCommentPupilHistoryBusiness.InsertOrUpdate(lstRatedCommentPupilBO, dic, ref lstActionAudit);
            }
            else
            {
                RatedCommentPupilBusiness.InsertOrUpdate(lstRatedCommentPupilBO, dic, ref lstActionAudit);
            }

            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            for (int i = 0; i < lstActionAudit.Count; i++)
            {
                objActionAudit = lstActionAudit[i];
                objectIDStr.Append(objActionAudit.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                descriptionStr.Append(objActionAudit.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                paramsStr.Append(objActionAudit.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                oldObjectStr.Append(objActionAudit.OldData).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                newObjectStr.Append(objActionAudit.NewData).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userActionsStr.Append(objActionAudit.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userFuntionsStr.Append(objActionAudit.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userDescriptionsStr.Append(objActionAudit.UserDescription).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
            }
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
            };
            SetViewDataActionAudit(dicLog);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }

        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCommentPupil(int ClassID, int SemesterID, int? SubjectID, int TypeID, string arrPupilID, int? isCommenting)
        {
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID},
                {"SubjectID",SubjectID},
                {"TypeID",TypeID},
                {"lstPupilID",lstPupilID},
                {"isCommenting",isCommenting},
                {"LogChangeID",LogChangeID}
            };
            List<ActionAuditDataBO> lstActionAudit = new List<ActionAuditDataBO>();
            ActionAuditDataBO objActionAudit = null;

            //Luu thong tin de phuc vu viec phuc hoi du lieu
            //Luu thong tin de phuc hoi du lieu

            ClassProfile objClass = ClassProfileBusiness.Find(ClassID);

            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value,
                SCHOOL_ID = _globalInfo.SchoolID.Value,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_MARK,
                //SHORT_DESCRIPTION = string.Format("Nhận xét đánh giá học sinh lớp: {0}, học kỳ:{1}, môn: {2}, năm học: {3}, ", objClass.DisplayName, SemesterID, objJubject.DisplayName, objClass.AcademicYear.Year + "-" + (objClass.AcademicYear.Year + 1)),
            };

            //Danh gia mon hoc
            if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
            {
                SubjectCat objJubject = SubjectCatBusiness.Find(SubjectID);
                objRes.SHORT_DESCRIPTION = string.Format(" Nhận xét đánh giá học sinh lớp: {0}, học kỳ:{1}, môn: {2}, năm học: {3}, ", objClass.DisplayName, SemesterID, objJubject.DisplayName, objClass.AcademicYear.Year + "-" + (objClass.AcademicYear.Year + 1));
            }
            else
            {
                objRes.SHORT_DESCRIPTION = string.Format(" Nhận xét đánh giá Năng lực – Phẩm chất lớp: {0}, học kỳ:{1}, năm học: {2}, ", objClass.DisplayName, SemesterID, objClass.AcademicYear.Year + "-" + (objClass.AcademicYear.Year + 1));
            }
            dic.Add("RESTORE_DATA", objRes);
            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();


            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                RatedCommentPupilHistoryBusiness.DeleteCommentPupil(dic, ref lstActionAudit);
            }
            else
            {
                RatedCommentPupilBusiness.DeleteCommentPupil(dic, ref lstActionAudit, ref lstRestoreDetail);
            }


            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            for (int i = 0; i < lstActionAudit.Count; i++)
            {
                objActionAudit = lstActionAudit[i];
                objectIDStr.Append(objActionAudit.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                descriptionStr.Append(objActionAudit.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                paramsStr.Append(objActionAudit.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                oldObjectStr.Append(objActionAudit.OldData).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userActionsStr.Append(objActionAudit.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userFuntionsStr.Append(objActionAudit.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userDescriptionsStr.Append(objActionAudit.UserDescription).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                objRes.SHORT_DESCRIPTION += objActionAudit.RestoreMsgDescription;
            }
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
            };
            SetViewDataActionAudit(dicLog);

            if (lstRestoreDetail != null && lstRestoreDetail.Count > 0)
            {
                if (objRes.SHORT_DESCRIPTION.Length > 1000)
                {
                    objRes.SHORT_DESCRIPTION = objRes.SHORT_DESCRIPTION.Substring(0, 1000) + "...";
                }
                RestoreDataBusiness.Insert(objRes);
                RestoreDataBusiness.Save();
                RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
            }


            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }

        #region nhan xet nhanh
        public PartialViewResult AjaxLoadDialog(int ClassID, int TypeID, int SubjectID, int SemesterID)
        {
            //Load mon hoc cua toan truong
            List<SubjectCat> lstSubject = new List<SubjectCat>();
            if (ClassID > 0)
            {
                IDictionary<string, object> dicSubject = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID}
                };
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue)
                {
                    lstSubject = ClassSubjectBusiness.Search(dicSubject)
                    .Where(o => o.SubjectCat.IsActive == true)
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .Select(o => o.SubjectCat)
                    .ToList();
                }
            }

            SubjectCat objSC = new SubjectCat();
            objSC.SubjectCatID = -1;
            objSC.DisplayName = "Năng lực";
            lstSubject.Add(objSC);
            objSC = new SubjectCat();
            objSC.SubjectCatID = -2;
            objSC.DisplayName = "Phẩm chất";
            lstSubject.Add(objSC);

            int selected = 0;
            if (TypeID == 1)
            {
                selected = SubjectID;
            }
            else if (TypeID == 2)
            {
                selected = -1;
            }
            else
            {
                selected = -2;
            }
            ViewData[RatedCommentPupilConstants.LIST_SUBJECT] = new SelectList(lstSubject, "SubjectCatID", "DisplayName", selected);
            //Load mon hoc cua account
            bool ViewAll = false;
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsSubjectAccount = new List<SubjectCatBO>();
            if (ClassID > 0)
            {
                lsSubjectAccount = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SemesterID, ClassID, ViewAll).Where(u => SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                    join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                    where s.IsActive == true
                                    && s.IsApprenticeshipSubject == false
                                    select new SubjectCatBO()
                                    {
                                        SubjectCatID = s.SubjectCatID,
                                        DisplayName = s.DisplayName,
                                        IsCommenting = cs.IsCommenting,
                                        OrderInSubject = s.OrderInSubject
                                    }
                                  ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            SubjectCatBO objSCBO = new SubjectCatBO();
            objSCBO.SubjectCatID = -1;
            objSCBO.DisplayName = "Năng lực";
            lsSubjectAccount.Add(objSCBO);

            objSCBO = new SubjectCatBO();
            objSCBO.SubjectCatID = -2;
            objSCBO.DisplayName = "Phẩm chất";
            lsSubjectAccount.Add(objSCBO);
            ViewData[RatedCommentPupilConstants.LIST_SUBJECT_ACCOUNT] = new SelectList(lsSubjectAccount, "SubjectCatID", "DisplayName");
            return PartialView("_GridFastComment");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadSystemComment(int SubjectSystemID, int SubjectAccountID, int TypeCollection)
        {
            //lay du lieu comment
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SubjectID",SubjectSystemID}
            };

            IQueryable<CollectionComments> iquery = CollectionCommentsBusiness.SearchCollectionComment(dic);
            if (TypeCollection == 1)
            {
                iquery = iquery.Where(p => p.AccountID == 0);
            }
            else
            {
                iquery = iquery.Where(p => p.AccountID == _globalInfo.UserAccountID);
            }
            List<CollectionCommentViewModel> lstResult = (from cc in iquery
                                                          select new CollectionCommentViewModel
                                                          {
                                                              CollectionCommentsID = cc.CollectionCommentsID,
                                                              SubjectID = cc.SubjectID,
                                                              Comment = cc.Comment,
                                                              CommentCode = cc.CommentCode
                                                          }).OrderBy(p => p.Comment).ToList();

            IDictionary<string, object> dicAccount = new Dictionary<string, object>()
            {
                {"SubjectID",SubjectAccountID},
                {"AccountID",_globalInfo.UserAccountID}
            };
            List<string> lstCommentAccount = CollectionCommentsBusiness.SearchCollectionComment(dicAccount).Select(p => p.Comment).Distinct().ToList();
            lstResult.ForEach(p =>
            {
                p.isEnable = !lstCommentAccount.Contains(p.Comment);
            });
            ViewData[RatedCommentPupilConstants.LIST_COLLECTION_COMMENT] = lstResult;
            return PartialView("_GridSystemComment");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadEmployeeComment(int SubjectID)
        {
            //lay du lieu comment
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SubjectID",SubjectID},
                {"AccountID",_globalInfo.UserAccountID}
            };
            List<CollectionCommentViewModel> lstResult = (from cc in CollectionCommentsBusiness.SearchCollectionComment(dic)
                                                          select new CollectionCommentViewModel
                                                          {
                                                              CollectionCommentsID = cc.CollectionCommentsID,
                                                              SubjectID = cc.SubjectID,
                                                              Comment = cc.Comment,
                                                              CommentCode = cc.CommentCode
                                                          }).OrderBy(p => p.CommentCode).ToList();
            ViewData[RatedCommentPupilConstants.LIST_COLLECTION_COMMENT] = lstResult;
            SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
            ViewData[RatedCommentPupilConstants.SUBJECT_CODE] = objSC.Abbreviation;
            return PartialView("_GridEmployeeComment");
        }
        [HttpPost]
        public PartialViewResult AjaxLoadMoveRigth(List<CollectionCommentViewModel> lstCollectionComment, string ArrSystemID, int SubjectID)
        {
            SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
            List<long> lstCollectionCommentID = new List<long>();
            List<long> lstAccountID = (lstCollectionComment != null && lstCollectionComment.Count > 0) ? lstCollectionComment.Select(p => p.CollectionCommentsID).ToList() : new List<long>();
            List<long> lstSystemID = ArrSystemID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => long.Parse(p)).ToList();
            lstCollectionCommentID = lstAccountID.Union(lstSystemID).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"lstCollectionCommentID",lstCollectionCommentID}
            };
            IQueryable<CollectionCommentViewModel> lsttmp = (from cc in CollectionCommentsBusiness.SearchCollectionComment(dic)
                                                             select new CollectionCommentViewModel
                                                             {
                                                                 CollectionCommentsID = cc.CollectionCommentsID,
                                                                 SubjectID = cc.SubjectID,
                                                                 Comment = cc.Comment,
                                                                 CommentCode = cc.CommentCode
                                                             });
            List<CollectionCommentViewModel> lstCollectionSystem = lsttmp.Where(p => lstSystemID.Contains(p.CollectionCommentsID) && !lstAccountID.Contains(p.CollectionCommentsID)).ToList();

            string maxCode = (lstCollectionComment != null && lstCollectionComment.Count > 0) ? lstCollectionComment.Where(p => p.CommentCode.StartsWith(objSC.Abbreviation)).Max(p => p.CommentCode) : "";
            int maxNumber = !string.IsNullOrEmpty(maxCode) ? Convert.ToInt32(maxCode.Substring(objSC.Abbreviation.Length)) : 0;
            for (int i = 0; i < lstCollectionSystem.Count; i++)
            {
                var objSystem = lstCollectionSystem[i];
                maxNumber++;
                objSystem.CommentCode = objSC.Abbreviation + maxNumber.ToString();
            }
            List<CollectionCommentViewModel> lstResult = (lstCollectionComment != null && lstCollectionComment.Count > 0) ? lstCollectionComment.Union(lstCollectionSystem).ToList() : lstCollectionSystem;

            ViewData[RatedCommentPupilConstants.LIST_COLLECTION_COMMENT] = lstResult;
            return PartialView("_GridEmployeeComment");
        }
        [ValidateAntiForgeryToken]
        public JsonResult SaveCollectionComment(List<CollectionCommentViewModel> lstCollectionComment, int hdfSubjectID)
        {
            lstCollectionComment = lstCollectionComment.Where(p => p.isChecked.HasValue && p.isChecked.Value).ToList();
            CollectionCommentViewModel objCC = null;
            List<CollectionComments> lstResult = new List<CollectionComments>();
            CollectionComments objResult = null;
            for (int i = 0; i < lstCollectionComment.Count; i++)
            {
                objCC = lstCollectionComment[i];
                objResult = new CollectionComments();
                objResult.CollectionCommentsID = objCC.CollectionCommentsID;
                objResult.SchoolID = _globalInfo.SchoolID.Value;
                objResult.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objResult.SubjectID = hdfSubjectID;
                objResult.AccountID = _globalInfo.UserAccountID;
                objResult.Comment = objCC.Comment;
                objResult.CommentCode = objCC.CommentCode;
                lstResult.Add(objResult);
            }
            List<long> lstCollectionID = lstResult.Select(p => p.CollectionCommentsID).Distinct().ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"lstCollectionCommentID",lstCollectionID},
                {"AccountID",_globalInfo.UserAccountID},
                {"SubjectID",hdfSubjectID}
            };
            CollectionCommentsBusiness.InsertOrUpdateCollectionComment(lstResult, dic);
            return Json(new JsonMessage("Cập nhật thành công", "success"));
        }
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCollectionComment(string ArrID, int SubjectID)
        {
            List<long> lstCollectionCommentID = ArrID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => long.Parse(p)).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SubjectID",SubjectID},
                {"AccountID",_globalInfo.UserAccountID},
                {"lstCollectionCommentID",lstCollectionCommentID}
            };
            List<CollectionComments> lstDelete = CollectionCommentsBusiness.SearchCollectionComment(dic).ToList();
            CollectionCommentsBusiness.DeleteAll(lstDelete);
            CollectionCommentsBusiness.Save();
            return Json(new JsonMessage("Xóa thành công", "success"));
        }
        public FileResult ExportCollectionComment(int SubjectID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "GV", RatedCommentPupilConstants.TEMPLATE_FILE_COLLECTION_COMMENTS);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //fill thong tin chung
            SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            sheet.SetCellValue("A5", "MÔN: " + objSC.SubjectName.ToUpper());
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SubjectID",SubjectID},
                {"AccountID",_globalInfo.UserAccountID}
            };
            List<CollectionCommentsBO> lstCC = (from cc in CollectionCommentsBusiness.SearchCollectionComment(dic)
                                                select new CollectionCommentsBO
                                                {
                                                    CollectionCommentsID = cc.CollectionCommentsID,
                                                    Comment = cc.Comment,
                                                    CommentCode = cc.CommentCode
                                                }).OrderBy(p => p.CommentCode).ToList();
            CollectionCommentsBO objCC = null;
            int startRow = 8;
            for (int i = 0; i < lstCC.Count; i++)
            {
                objCC = lstCC[i];
                sheet.SetCellValue(startRow, 1, (i + 1));
                sheet.SetCellValue(startRow, 2, objCC.CommentCode);
                sheet.SetCellValue(startRow, 3, objCC.Comment);
                sheet.SetCellValue(startRow, 4, objCC.CollectionCommentsID);//dung de import
                startRow++;
            }
            sheet.GetRange(8, 1, startRow - 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.HideColumn(4);
            sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "GV_NganHangNhanXet.xls";
            return result;
        }
        [ValidateAntiForgeryToken]
        public JsonResult ImportCollectionComment(IEnumerable<HttpPostedFileBase> attachmentsCC, int SubjectID)
        {
            if (attachmentsCC == null || attachmentsCC.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachmentsCC.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                List<CollectionComments> lstCollectionComment = new List<CollectionComments>();
                CollectionComments objCollectionComment = null;
                List<long> lstCollectionCommentID = new List<long>();
                int startRow = 8;
                if (sheet.GetCellValue(startRow - 1, 1) != null && sheet.GetCellValue(startRow - 1, 2) != null && sheet.GetCellValue(startRow - 1, 3) != null)
                {
                    if (!sheet.GetCellValue(startRow - 1, 1).Equals("STT") || !sheet.GetCellValue(startRow - 1, 2).Equals("Mã") || !sheet.GetCellValue(startRow - 1, 3).Equals("Nội dung nhận xét"))
                    {
                        return Json(new JsonMessage("File import không đúng mẫu", "error"));
                    }
                }
                else
                {
                    return Json(new JsonMessage("File import không đúng mẫu", "error"));
                }

                while (sheet.GetCellValue(startRow, 1) != null && sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
                {
                    objCollectionComment = new CollectionComments();
                    objCollectionComment.SchoolID = _globalInfo.SchoolID.Value;
                    objCollectionComment.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objCollectionComment.SubjectID = SubjectID;
                    objCollectionComment.AccountID = _globalInfo.UserAccountID;
                    string strval = string.Empty;
                    strval = sheet.GetCellValue(startRow, 4) != null ? sheet.GetCellValue(startRow, 4).ToString() : "";
                    objCollectionComment.CollectionCommentsID = !string.IsNullOrEmpty(strval) ? long.Parse(strval) : 0;
                    lstCollectionCommentID.Add(objCollectionComment.CollectionCommentsID);
                    strval = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : "";
                    objCollectionComment.CommentCode = strval;
                    strval = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString() : "";
                    objCollectionComment.Comment = strval;
                    lstCollectionComment.Add(objCollectionComment);
                    startRow++;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"AccountID",_globalInfo.UserAccountID},
                    {"SubjectID",SubjectID},
                    {"lstCollectionCommentID",lstCollectionCommentID}
                };
                CollectionCommentsBusiness.InsertOrUpdateCollectionComment(lstCollectionComment, dic);
            }
            return Json(new JsonMessage("Import thành công", "success"));
        }
        public JsonResult AjaxLoadSubjectAccount(int ClassID, int SubjectSystemID, int SemesterID, int TypeSelectID)
        {
            //Load mon hoc cua account
            bool ViewAll = false;
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsSubjectAccount = new List<SubjectCatBO>();
            if (ClassID > 0)
            {
                lsSubjectAccount = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SemesterID, ClassID, ViewAll)
                                        .Where(u => SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0 && ((TypeSelectID == 2 && u.SubjectID != SubjectSystemID) || TypeSelectID == 1))
                                    join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                    where s.IsActive == true
                                    && s.IsApprenticeshipSubject == false
                                    select new SubjectCatBO()
                                    {
                                        SubjectCatID = s.SubjectCatID,
                                        DisplayName = s.DisplayName,
                                        IsCommenting = cs.IsCommenting,
                                        OrderInSubject = s.OrderInSubject
                                    }
                                  ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            SubjectCatBO objSCBO = new SubjectCatBO();
            if (TypeSelectID == 1)
            {
                objSCBO.SubjectCatID = -1;
                objSCBO.DisplayName = "Năng lực";
                lsSubjectAccount.Add(objSCBO);
                objSCBO = new SubjectCatBO();
                objSCBO.SubjectCatID = -2;
                objSCBO.DisplayName = "Phẩm chất";
                lsSubjectAccount.Add(objSCBO);
            }
            else
            {
                if (SubjectSystemID == -1)
                {
                    objSCBO.SubjectCatID = -2;
                    objSCBO.DisplayName = "Phẩm chất";
                    lsSubjectAccount.Add(objSCBO);
                }
                else if (SubjectSystemID == -2)
                {
                    objSCBO.SubjectCatID = -1;
                    objSCBO.DisplayName = "Năng lực";
                    lsSubjectAccount.Add(objSCBO);
                }
                else
                {
                    objSCBO.SubjectCatID = -1;
                    objSCBO.DisplayName = "Năng lực";
                    lsSubjectAccount.Add(objSCBO);
                    objSCBO = new SubjectCatBO();
                    objSCBO.SubjectCatID = -2;
                    objSCBO.DisplayName = "Phẩm chất";
                    lsSubjectAccount.Add(objSCBO);
                }
            }
            return Json(new SelectList(lsSubjectAccount, "SubjectCatID", "DisplayName"));
        }
        #endregion

        #region Export Import Excel
        public FileResult ExportSubjectAndEducationGroup(int ClassID, int SubjectID, int SemesterID, int TypeExport, int EducationLevelID, int isComemnting)
        {
            string templatePath = string.Empty;
            string fileName = string.Empty;
            if (TypeExport == 1 || TypeExport == 2)
            {
                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", RatedCommentPupilConstants.TEMPLATE_FILE_TYPE12);
            }
            else
            {
                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", RatedCommentPupilConstants.TEMPLATE_FILE_TYPE3);
            }
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetCurrent = oBook.GetSheet(1);
            string suppervisingDeptName = "";
            if (_globalInfo.EmployeeID > 0)
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                suppervisingDeptName = objSP.SupervisingDept.SupervisingDeptName.ToUpper();
            }
            else
            {
                suppervisingDeptName = UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value).ToUpper();
            }
            sheetCurrent.SetCellValue("A2", suppervisingDeptName);
            sheetCurrent.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            List<VTDataValidation> lstDataValidation = new List<VTDataValidation>();
            if (TypeExport == 1 || TypeExport == 2)
            {
                ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
                fileName = string.Format("Sodanhgiahocsinh_{0}.xls", SMAS.Web.Utils.Utils.StripVNSignAndSpace(objCP.DisplayName));
                //lay danh sach hoc sinh cua 1 lop
                List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
                IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
                if (isNotShowPupil)
                {
                    iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
                }
                lstPOC = iquery.ToList();
                //lay danh sach danh gia cua hoc sinh
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"SemesterID",SemesterID},
                    {"ClassID",ClassID},
                    {"TypeID",GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP},

                };
                List<int> lstSubjectID = new List<int>();
                List<EvaluationGridModel> lstRatedCommentPupil = new List<EvaluationGridModel>();
                if (TypeExport == 1)
                {
                    lstSubjectID.Add(SubjectID);
                    dic.Add("lstSubjectID", lstSubjectID);
                    lstRatedCommentPupil = this.GetListRatedCommentBO(lstPOC, dic, objAy);
                    SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
                    this.SetDataToFile(sheetCurrent, lstPOC, lstRatedCommentPupil, GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP, objAy.DisplayTitle, objSC.DisplayName, objCP.DisplayName, SemesterID, 1, isComemnting, ref lstDataValidation);
                    sheetCurrent.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(objSC.DisplayName, true);
                    if (isComemnting == 0)
                    {
                        sheetCurrent.HideColumn(9);
                    }
                    else
                    {
                        sheetCurrent.HideColumn(7);
                    }
                    sheetCurrent.SetCellValue("Y3", TypeExport);
                    sheetCurrent.SetCellValue("Y4", isComemnting);
                    VTVector cellHiddenValue = new VTVector("Y3");
                    sheetCurrent.HideColumn(cellHiddenValue.Y);
                    sheetCurrent.ProtectSheet();
                }
                else
                {
                    //lay danh sach tat ca mon hoc cua lop
                    List<SubjectCatBO> lstCS = this.GetListSubject(ClassID, SemesterID);
                    SubjectCatBO objCS = null;
                    lstSubjectID = lstCS.Select(p => p.SubjectCatID).Distinct().ToList();
                    dic.Add("lstSubjectID", lstSubjectID);
                    lstRatedCommentPupil = this.GetListRatedCommentBO(lstPOC, dic, objAy);
                    List<EvaluationGridModel> lstRatedtmp = new List<EvaluationGridModel>();
                    for (int i = 0; i < lstCS.Count; i++)
                    {
                        objCS = lstCS[i];
                        IVTWorksheet sheettmp = oBook.CopySheetToLast(sheetCurrent);
                        lstRatedtmp = lstRatedCommentPupil.Where(p => p.SubjectID == objCS.SubjectCatID).ToList();
                        this.SetDataToFile(sheettmp, lstPOC, lstRatedtmp, GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP, objAy.DisplayTitle, objCS.DisplayName, objCP.DisplayName, SemesterID, i + 1, objCS.IsCommenting.Value, ref lstDataValidation);
                        sheettmp.Name = Business.Common.Utils.StripVNSignAndSpace(objCS.DisplayName);
                        if (objCS.IsCommenting.Value == 0)
                        {
                            sheettmp.HideColumn(9);
                        }
                        else
                        {
                            sheettmp.HideColumn(7);
                        }
                        sheettmp.SetFontName("Times New Roman", 12);
                        sheettmp.SetCellValue("Y3", TypeExport);
                        sheettmp.SetCellValue("Y4", objCS.IsCommenting.Value);
                        VTVector cellHiddenValue = new VTVector("Y3");
                        sheettmp.HideColumn(cellHiddenValue.Y);
                        sheettmp.ProtectSheet();
                    }
                    sheetCurrent.Delete();
                }
            }
            else
            {
                SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
                fileName = string.Format("Sodanhgiahocsinh_{0}.xls", SMAS.Web.Utils.Utils.StripVNSignAndSpace(objSC.DisplayName, true));
                int teacherId = 0;
                if (!_globalInfo.IsAdmin)
                {
                    teacherId = _globalInfo.EmployeeID.Value;
                }
                List<ClassProfileTempBO> listClassProfile = ClassSubjectBusiness.GetListClassBySubjectId(
                    _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SubjectID, EducationLevelID,
                    teacherId, _globalInfo.IsAdmin, SemesterID, _globalInfo.AppliedLevel.Value, _globalInfo.IsAdminSchoolRole,
                    _globalInfo.IsViewAll, _globalInfo.IsEmployeeManager, _globalInfo.UserAccountID, ClassID, true);
                List<ClassProfileTempBO> lstClasstmp = new List<ClassProfileTempBO>();

                IDictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                };
                if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
                {
                    lstClasstmp = listClassProfile.Where(p => p.EducationLevelID == EducationLevelID).ToList();
                    dicSearch.Add("EducationLevelID", EducationLevelID);
                }
                else
                {
                    lstClasstmp = listClassProfile.ToList();
                }
                List<int> lstClassID = lstClasstmp.Select(p => p.ClassProfileID).Distinct().ToList();
                dicSearch.Add("lstClassID", lstClassID);
                List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
                IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.Search(dicSearch)
                                              .Select(p => new PupilOfClassBO
                                              {
                                                  PupilID = p.PupilID,
                                                  PupilCode = p.PupilProfile.PupilCode,
                                                  PupilFullName = p.PupilProfile.FullName,
                                                  ClassID = p.ClassID,
                                                  Status = p.Status,
                                                  OrderInClass = p.OrderInClass,
                                                  Name = p.PupilProfile.Name
                                              }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
                if (isNotShowPupil)
                {
                    iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
                }
                lstPOC = iquery.ToList();
                //lay danh sach danh gia cua hoc sinh
                List<int> lstSubjectID = new List<int>();
                lstSubjectID.Add(SubjectID);
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"SemesterID",SemesterID},
                    {"lstSubjectID",lstSubjectID},
                    {"lstClassID",lstClassID},
                    {"TypeID",GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP},
                };
                List<EvaluationGridModel> lstRatedComment = this.GetListRatedCommentBO(lstPOC, dic, objAy);
                List<EvaluationGridModel> lstRatedtmp = null;
                ClassProfileTempBO objCP = null;
                List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();

                for (int i = 0; i < lstClasstmp.Count; i++)
                {
                    objCP = lstClasstmp[i];
                    lstRatedtmp = lstRatedComment.Where(p => p.ClassID == objCP.ClassProfileID).ToList();
                    lstPOCtmp = lstPOC.Where(p => p.ClassID == objCP.ClassProfileID).ToList();
                    IVTWorksheet sheettmp = oBook.CopySheetToLast(sheetCurrent);
                    this.SetDataToFile(sheettmp, lstPOCtmp, lstRatedtmp, GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP, objAy.DisplayTitle, objSC.DisplayName, objCP.DisplayName, SemesterID, i + 1, isComemnting, ref lstDataValidation);
                    sheettmp.Name = Business.Common.Utils.StripVNSignAndSpace(objCP.DisplayName);
                    if (isComemnting == 0)
                    {
                        sheettmp.HideColumn(9);
                    }
                    else
                    {
                        sheettmp.HideColumn(7);
                    }
                    sheettmp.SetFontName("Times New Roman", 12);
                    sheettmp.SetCellValue("Y3", TypeExport);
                    sheettmp.SetCellValue("Y4", isComemnting);
                    VTVector cellHiddenValue = new VTVector("Y3");
                    sheettmp.HideColumn(cellHiddenValue.Y);
                    sheettmp.ProtectSheet();
                }
                sheetCurrent.Delete();
            }
            Stream excel = oBook.ToStreamValidationData(lstDataValidation);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }
        public FileResult ExportCapacityAndQuality(int ClassID, int SemesterID, int TypeExport, int EducationLevelID)
        {
            string templatePath = string.Empty;
            string fileName = string.Empty;
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", RatedCommentPupilConstants.TEMPLATE_FILE_NLPC);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            List<VTDataValidation> lstDataValidation = new List<VTDataValidation>();
            IVTWorksheet sheetCurrent = oBook.GetSheet(1);
            string suppervisingDeptName = "";
            if (_globalInfo.EmployeeID > 0)
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                suppervisingDeptName = objSP.SupervisingDept.SupervisingDeptName.ToUpper();
            }
            else
            {
                suppervisingDeptName = UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value).ToUpper();
            }
            sheetCurrent.SetCellValue("A2", suppervisingDeptName);
            sheetCurrent.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"SemesterID",SemesterID},
                {"TypeID",GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY}
            };
            List<EvaluationGridModel> lstRatedCommentPupil = new List<EvaluationGridModel>();
            if (TypeExport == 1)//NLPC cua lop
            {
                dic.Add("ClassID", ClassID);
                ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
                //lay danh sach hoc sinh cua 1 lop
                List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
                IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
                if (isNotShowPupil)
                {
                    iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
                }
                lstPOC = iquery.ToList();
                lstRatedCommentPupil = this.GetListRatedCommentBO(lstPOC, dic, objAy);
                if (lstPOC.Count > 0)
                {
                    sheetCurrent.GetRange(11, 4, lstPOC.Count + 10, 19).IsLock = false;
                }
                this.SetDataToFile(sheetCurrent, lstPOC, lstRatedCommentPupil, GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY, objAy.DisplayTitle, "", objCP.DisplayName, SemesterID, 1, 0, ref lstDataValidation);
                sheetCurrent.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(objCP.DisplayName, true);
                sheetCurrent.HideColumn(20);
                sheetCurrent.SetCellValue("Y3", TypeExport);
                VTVector cellHiddenValue = new VTVector("Y3");
                sheetCurrent.HideColumn(cellHiddenValue.Y);
                sheetCurrent.ProtectSheet();
            }
            else//NLPC cua cac lop
            {
                List<ClassProfile> listClass = new List<ClassProfile>();
                IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicSearch["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsEmployeeManager)
                {
                    listClass = ClassProfileBusiness.GetListClassByHeadTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value).ToList();
                }
                else
                {
                    listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                }

                List<ClassProfile> lstClasstmp = new List<ClassProfile>();
                List<int> lstClassID = new List<int>();
                dicSearch = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };
                if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
                {
                    lstClasstmp = listClass.Where(p => p.EducationLevelID == EducationLevelID).ToList();
                    dicSearch.Add("EducationLevelID", EducationLevelID);
                }
                else
                {
                    lstClasstmp = listClass.ToList();
                }
                lstClassID = lstClasstmp.Select(p => p.ClassProfileID).Distinct().ToList();
                dicSearch.Add("lstClassID", lstClassID);

                List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
                IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.Search(dicSearch)
                                              .Select(p => new PupilOfClassBO
                                              {
                                                  PupilID = p.PupilID,
                                                  PupilCode = p.PupilProfile.PupilCode,
                                                  PupilFullName = p.PupilProfile.FullName,
                                                  ClassID = p.ClassID,
                                                  Status = p.Status,
                                                  OrderInClass = p.OrderInClass,
                                                  Name = p.PupilProfile.Name
                                              }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
                if (isNotShowPupil)
                {
                    iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
                }
                lstPOC = iquery.ToList();
                //lay danh sach danh gia cua hoc sinh
                dic.Add("lstClassID", lstClassID);
                lstRatedCommentPupil = this.GetListRatedCommentBO(lstPOC, dic, objAy);
                List<EvaluationGridModel> lstRatedtmp = null;
                ClassProfile objCP = null;
                List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
                for (int i = 0; i < lstClasstmp.Count; i++)
                {
                    objCP = lstClasstmp[i];
                    lstRatedtmp = lstRatedCommentPupil.Where(p => p.ClassID == objCP.ClassProfileID).ToList();
                    lstPOCtmp = lstPOC.Where(p => p.ClassID == objCP.ClassProfileID).ToList();
                    IVTWorksheet sheettmp = oBook.CopySheetToLast(sheetCurrent);
                    if (lstPOCtmp.Count > 0)
                    {
                        sheettmp.GetRange(11, 4, lstPOCtmp.Count + 10, 19).IsLock = false;
                    }
                    this.SetDataToFile(sheettmp, lstPOCtmp, lstRatedtmp, GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY, objAy.DisplayTitle, "", objCP.DisplayName, SemesterID, i + 1, 0, ref lstDataValidation);
                    sheettmp.Name = SMAS.Web.Utils.Utils.StripVNSignAndSpace(objCP.DisplayName, true);
                    sheettmp.SetFontName("Times New Roman", 12);
                    sheettmp.HideColumn(20);
                    sheettmp.SetCellValue("Y3", TypeExport);
                    VTVector cellHiddenValue = new VTVector("Y3");
                    sheettmp.HideColumn(cellHiddenValue.Y);
                    sheettmp.ProtectSheet();
                }
                sheetCurrent.Delete();

            }
            Stream excel = oBook.ToStreamValidationData(lstDataValidation);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            fileName = "Sodanhgiahocsinh_NLPC.xls";
            result.FileDownloadName = fileName;
            return result;

        }
        public JsonResult CheckGVBM(int ClassID, int SemesterID, int TypeExport, int EducationLevelID)
        {
            List<ClassProfile> listClass = new List<ClassProfile>();
            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSearch["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsEmployeeManager)
            {
                listClass = ClassProfileBusiness.GetListClassByHeadTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value).ToList();
            }
            else
            {
                listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
            if (listClass.Count > 0)
            {
                return Json(new JsonMessage("", "success"));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Teaching_Not_Class"), "error"));
            }
        }
        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImportSubject(IEnumerable<HttpPostedFileBase> attachments, int ClassID, int SubjectID, int SemesterID, int EducationLevelID)
        {
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;
                List<int> listTypeReportExcel = new List<int>();
                string strValue = string.Empty;
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 5120)
                {
                    return Json(new JsonMessage(Res.Get("Common_Max_Size_FileExcel"), "error"));
                }

                //danh sach ten cac mon hoc trong excel
                List<string> listSheetNameExcel = new List<string>();

                if (lstSheets != null && lstSheets.Count > 0)
                {
                    for (int i = 0; i < lstSheets.Count; i++)
                    {
                        sheet = oBook.GetSheet(i + 1);
                        strValue = (sheet.GetCellValue(RatedCommentPupilConstants.CELL_CHECK_TYPE_FILE_IMPORT) != null ? sheet.GetCellValue(RatedCommentPupilConstants.CELL_CHECK_TYPE_FILE_IMPORT).ToString() : string.Empty);

                        if (!String.IsNullOrEmpty(strValue))
                        {
                            if (strValue.Equals("1") || strValue.Equals("2") || strValue.Equals("3")) //chon option thu 2 hoac 3
                            {
                                listSheetNameExcel.Add(sheet.Name);
                                listTypeReportExcel.Add(int.Parse(strValue));
                            }
                        }
                        else
                        {
                            return Json(new JsonMessage(Res.Get("RatedCommentPupil_File_Error"), "error"));
                        }
                    }
                }
                if (listTypeReportExcel != null && listTypeReportExcel.Count > 0)
                {
                    List<int> listType = listTypeReportExcel.Distinct().ToList();
                    if (listType.Count > 1)
                    {
                        return Json(new JsonMessage(Res.Get("RatedCommentPupil_File_Error"), "error"));
                    }
                    else
                    {
                        int valueImport = listTypeReportExcel.FirstOrDefault();
                        Session[RatedCommentPupilConstants.PHISICAL_PATH] = physicalPath;
                        StringBuilder strBuilder = new StringBuilder();
                        if (valueImport == 2) //chon option nhieu mon 1 lop
                        {
                            //Lay danh sach mon co quyen doi voi giao vien
                            List<SubjectCatBO> listSubjectPremission = this.GetListSubject(ClassID, SemesterID);
                            if (listSubjectPremission != null && listSubjectPremission.Count > 0)
                            {
                                string subjectName = string.Empty;
                                for (int i = 0; i < listSubjectPremission.Count; i++)
                                {
                                    subjectName = listSubjectPremission[i].DisplayName;
                                    if (listSheetNameExcel.Exists(p => p.Equals(Business.Common.Utils.StripVNSignAndSpace(subjectName))))
                                    {
                                        strBuilder.Append("<span class='ClassListSubjectImport editor-field' style='padding:5px;'>");
                                        if (listSubjectPremission[i].SubjectCatID == SubjectID)
                                        {
                                            strBuilder.Append("<input class='classSubjectImport' checked='checked' type='checkbox' value='" + listSubjectPremission[i].SubjectCatID + "' name='OptionSubject_" + listSubjectPremission[i].SubjectCatID + "' iscommenting='" + listSubjectPremission[i].IsCommenting + "' onchange='changeSubjectImport()' subjectidincrease='" + listSubjectPremission[i].SubjectIdInCrease + "'>  " + listSubjectPremission[i].DisplayName + "");
                                        }
                                        else
                                        {
                                            strBuilder.Append("<input class='classSubjectImport' type='checkbox' value='" + listSubjectPremission[i].SubjectCatID + "' name='OptionSubject_" + listSubjectPremission[i].SubjectCatID + "' iscommenting='" + listSubjectPremission[i].IsCommenting + "' onchange='changeSubjectImport()' subjectidincrease='" + listSubjectPremission[i].SubjectIdInCrease + "'>  " + listSubjectPremission[i].DisplayName + "");
                                        }
                                        strBuilder.Append("</span>");
                                    }
                                }
                            }
                        }
                        else if (valueImport == 3) //chon option thu 3
                        {
                            //Lay danh sach cac lop hien tai ma nguoi co quuyen thao tac
                            string strResult = string.Empty;
                            Dictionary<string, object> dic = new Dictionary<string, object>();
                            dic["SubjectID"] = SubjectID;
                            int teacherId = 0;
                            if (!_globalInfo.IsAdmin)
                            {
                                teacherId = _globalInfo.EmployeeID.Value;
                            }

                            List<ClassProfileTempBO> listClassProfile = ClassSubjectBusiness.GetListClassBySubjectId(
                                _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SubjectID, EducationLevelID,
                                teacherId, _globalInfo.IsAdmin, SemesterID, _globalInfo.AppliedLevel.Value, _globalInfo.IsAdminSchoolRole,
                                _globalInfo.IsViewAll, _globalInfo.IsEmployeeManager, _globalInfo.UserAccountID, ClassID, true);

                            if (listClassProfile != null && listClassProfile.Count > 0)
                            {
                                string className = string.Empty;
                                for (int i = 0; i < listClassProfile.Count; i++)
                                {
                                    className = listClassProfile[i].DisplayName;
                                    if (listSheetNameExcel.Exists(p => p.Equals(Business.Common.Utils.StripVNSignAndSpace(className))))
                                    {
                                        if (listClassProfile[i].ClassProfileID == ClassID)
                                        {
                                            strBuilder.Append("<span class='ClassListClassOption editor-field' style='padding:5px;'><input class='ClassExport' type='checkbox' checked='checked' value='" + listClassProfile[i].ClassProfileID + "' name='OptionClass_" + listClassProfile[i].ClassProfileID + "' onchange='changeClasImport()'>  " + listClassProfile[i].DisplayName + "</span>");
                                        }
                                        else
                                        {
                                            strBuilder.Append("<span class='ClassListClassOption editor-field' style='padding:5px;'><input class='ClassExport' type='checkbox' value='" + listClassProfile[i].ClassProfileID + "' name='OptionClass_" + listClassProfile[i].ClassProfileID + "' onchange='changeClasImport()'>  " + listClassProfile[i].DisplayName + "</span>");
                                        }

                                    }
                                }
                            }
                        }

                        return Json(new { TypeImport = valueImport, strOption = strBuilder != null ? strBuilder.ToString() : string.Empty });
                    }
                }
                return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }
        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImportCap(IEnumerable<HttpPostedFileBase> attachmentsCap, int ClassID, int SemesterID, int EducationLevelID)
        {
            if (attachmentsCap == null || attachmentsCap.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachmentsCap.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePathCap"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;
                List<int> listTypeReportExcel = new List<int>();
                string strValue = string.Empty;
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 5120)
                {
                    return Json(new JsonMessage(Res.Get("Common_Max_Size_FileExcel"), "error"));
                }

                //danh sach ten cac lop hoc trong excel
                List<string> listSheetNameExcel = new List<string>();

                if (lstSheets != null && lstSheets.Count > 0)
                {
                    for (int i = 0; i < lstSheets.Count; i++)
                    {
                        sheet = oBook.GetSheet(i + 1);
                        strValue = (sheet.GetCellValue(RatedCommentPupilConstants.CELL_CHECK_TYPE_FILE_IMPORT) != null ? sheet.GetCellValue(RatedCommentPupilConstants.CELL_CHECK_TYPE_FILE_IMPORT).ToString() : string.Empty);

                        if (!String.IsNullOrEmpty(strValue))
                        {
                            if (strValue.Equals("1") || strValue.Equals("2")) //chon option thu 2
                            {
                                listSheetNameExcel.Add(sheet.Name);
                                listTypeReportExcel.Add(int.Parse(strValue));
                            }
                        }
                        else
                        {
                            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
                        }
                    }
                }
                if (listTypeReportExcel != null && listTypeReportExcel.Count > 0)
                {
                    List<int> listType = listTypeReportExcel.Distinct().ToList();
                    if (listType.Count > 1)
                    {
                        return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
                    }
                    else
                    {
                        int valueImport = listTypeReportExcel.FirstOrDefault();
                        Session[RatedCommentPupilConstants.PHISICAL_PATH_CAP] = physicalPath;
                        StringBuilder strBuilder = new StringBuilder();
                        if (valueImport == 2) //chon option nhieu mon 1 lop
                        {
                            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                            dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                            dicSearch["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                            {
                                dicSearch["UserAccountID"] = _globalInfo.UserAccountID;
                                dicSearch["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                            }
                            IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            List<ClassProfile> lstClasstmp = new List<ClassProfile>();

                            dicSearch = new Dictionary<string, object>()
                            {
                                {"SchoolID",_globalInfo.SchoolID},
                                {"AcademicYearID",_globalInfo.AcademicYearID},
                            };
                            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
                            {
                                lstClasstmp = listClass.Where(p => p.EducationLevelID == EducationLevelID).ToList();
                                dicSearch.Add("EducationLevelID", EducationLevelID);
                            }
                            else
                            {
                                lstClasstmp = listClass.ToList();
                            }

                            if (lstClasstmp != null && lstClasstmp.Count > 0)
                            {
                                string className = string.Empty;
                                for (int i = 0; i < lstClasstmp.Count; i++)
                                {
                                    className = lstClasstmp[i].DisplayName;
                                    if (listSheetNameExcel.Exists(p => p.Equals(Business.Common.Utils.StripVNSignAndSpace(className))))
                                    {
                                        if (lstClasstmp[i].ClassProfileID == ClassID)
                                        {
                                            strBuilder.Append("<span class='ClassListClassOption editor-field' style='padding:5px;'><input class='ClassExport' type='checkbox' checked='checked' value='" + lstClasstmp[i].ClassProfileID + "' name='OptionClass_" + lstClasstmp[i].ClassProfileID + "' onchange='changeClasImport()'>  " + lstClasstmp[i].DisplayName + "</span>");
                                        }
                                        else
                                        {
                                            strBuilder.Append("<span class='ClassListClassOption editor-field' style='padding:5px;'><input class='ClassExport' type='checkbox' value='" + lstClasstmp[i].ClassProfileID + "' name='OptionClass_" + lstClasstmp[i].ClassProfileID + "' onchange='changeClasImport()'>  " + lstClasstmp[i].DisplayName + "</span>");
                                        }

                                    }
                                }
                            }
                        }
                        return Json(new { TypeImport = valueImport, strOption = strBuilder != null ? strBuilder.ToString() : string.Empty });
                    }
                }
                return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int semesterid, int ClassID, int SubjectID,
            int typeImport, string arrSubjectid, string arrClassid, int EducationLevelId, int isCommenting)
        {
            if (Session[RatedCommentPupilConstants.PHISICAL_PATH] == null) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            string physicalPath = (string)Session[RatedCommentPupilConstants.PHISICAL_PATH];
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            RatedCommentViewModel objRatedComemntImport = new RatedCommentViewModel();
            List<EvaluationGridModel> lstResult = new List<EvaluationGridModel>();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);
            string messageSuccess = "";
            if (typeImport == 1 || typeImport == 2)
            {
                #region Import theo 1 lop
                if (typeImport == 1)
                {
                    messageSuccess = Res.Get("Common_Label_ImportSuccessMessage");
                }
                else
                {
                    List<int> lstSubjectAll = arrSubjectid.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();

                    List<int> lstSubjectID = new List<int>();

                    for (int i = 0; i < lstSubjectAll.Count; i++)
                    {
                        if (UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, ClassID, lstSubjectAll[i], semesterid))
                        {
                            lstSubjectID.Add(lstSubjectAll[i]);
                        }
                    }
                    if (lstSubjectID.Count == 1)
                    {
                        messageSuccess = Res.Get("Common_Label_ImportSuccessMessage");
                    }
                    else
                    {
                        List<string> lstSubjectName = SubjectCatBusiness.All.Where(p => lstSubjectID.Contains(p.SubjectCatID)).OrderBy(p => p.OrderInSubject).Select(p => p.DisplayName).ToList();
                        messageSuccess = "Nhập dữ liệu từ excel thành công cho các môn ";
                        for (int i = 0; i < lstSubjectName.Count; i++)
                        {
                            if (i < lstSubjectName.Count - 1)
                            {
                                messageSuccess += lstSubjectName[i] + ", ";
                            }
                            else
                            {
                                messageSuccess += lstSubjectName[i];
                            }
                        }
                    }
                }
                //lay danh sach hoc sinh theo lop
                lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                objRatedComemntImport = this.GetDataToFileExcel(obook, lstPOC, typeImport, ClassID, SubjectID, semesterid, EducationLevelId, arrClassid, arrSubjectid, isCommenting, GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP);
                #endregion
            }
            else if (typeImport == 3)
            {
                #region Import mot mon nhieu lop
                List<int> lstClassID = arrClassid.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                IDictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"lstClassID",lstClassID}
                };
                lstPOC = PupilOfClassBusiness.Search(dicSearch)
                                              .Select(p => new PupilOfClassBO
                                              {
                                                  PupilID = p.PupilID,
                                                  PupilCode = p.PupilProfile.PupilCode,
                                                  PupilFullName = p.PupilProfile.FullName,
                                                  ClassID = p.ClassID,
                                                  ClassName = p.ClassProfile.DisplayName,
                                                  Status = p.Status,
                                                  OrderInClass = p.OrderInClass,
                                                  Name = p.PupilProfile.Name,
                                                  ClassOrder = p.ClassProfile.OrderNumber,
                                                  EducationLevelID = p.ClassProfile.EducationLevelID
                                              }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                objRatedComemntImport = this.GetDataToFileExcel(obook, lstPOC, typeImport, ClassID, SubjectID, semesterid, EducationLevelId, arrClassid, arrSubjectid, isCommenting, GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP);

                List<string> lstClassName = lstPOC.OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassOrder).Select(p => p.ClassName).Distinct().ToList();
                if (lstClassName.Count > 1)
                {
                    messageSuccess = "Nhập dữ liệu từ excel thành công cho các lớp ";
                    for (int i = 0; i < lstClassName.Count; i++)
                    {
                        if (i < lstClassName.Count - 1)
                        {
                            messageSuccess += lstClassName[i] + ", ";
                        }
                        else
                        {
                            messageSuccess += lstClassName[i];
                        }

                    }
                }
                else
                {
                    messageSuccess = Res.Get("Common_Label_ImportSuccessMessage");
                }

                #endregion
            }
            if (!string.IsNullOrEmpty(objRatedComemntImport.Error))
            {
                return Json(new JsonMessage(objRatedComemntImport.Error, "error"));
            }
            else
            {
                lstResult = objRatedComemntImport.lstEvalutionComment;
                if (lstResult.Count > 0)
                {
                    if (lstResult.Where(p => p.isErr).Count() > 0)
                    {
                        List<EvaluationGridModel> listError = lstResult.Where(o => o.isErr).ToList();
                        ViewData[RatedCommentPupilConstants.EVALUATION_ID] = GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP;
                        Session["lstResultErr"] = listError;
                        return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "grid"));
                    }
                    else
                    {
                        IDictionary<string, object> dic = new Dictionary<string, object>()
                        {
                            {"SchoolID",_globalInfo.SchoolID},
                            {"AcademicYearID",_globalInfo.AcademicYearID},
                            {"ClassID",ClassID},
                            {"SemesterID",semesterid},
                            {"SubjectID",SubjectID},
                            {"IsImport",true},
                            {"TypeID",GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP}
                        };
                        List<ActionAuditDataBO> lstActionAudit = new List<ActionAuditDataBO>();
                        ActionAuditDataBO objActionAudit = null;
                        AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                        List<RatedCommentPupilBO> lstRatedCommentPupilBO = new List<RatedCommentPupilBO>();
                        RatedCommentPupilBO objRatedCommentPupilBO = null;
                        EvaluationGridModel objEvaluationGridModel = null;
                        for (int i = 0; i < lstResult.Count; i++)//do du lieu vao doi tuong RatedCommentPupilBO
                        {
                            objEvaluationGridModel = lstResult[i];
                            objRatedCommentPupilBO = new RatedCommentPupilBO();
                            objRatedCommentPupilBO.SchoolID = _globalInfo.SchoolID.Value;
                            objRatedCommentPupilBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objRatedCommentPupilBO.PupilID = objEvaluationGridModel.PupilID;
                            objRatedCommentPupilBO.ClassID = objEvaluationGridModel.ClassID;
                            objRatedCommentPupilBO.SemesterID = objEvaluationGridModel.SemesterID;
                            objRatedCommentPupilBO.SubjectID = objEvaluationGridModel.SubjectID;
                            objRatedCommentPupilBO.EvaluationID = objEvaluationGridModel.TypeID;
                            objRatedCommentPupilBO.FullNameComment = "";
                            objRatedCommentPupilBO.LogChangeID = objEvaluationGridModel.LogChangeID;
                            objRatedCommentPupilBO.FullName = objEvaluationGridModel.FullName;
                            objRatedCommentPupilBO.PupilCode = objEvaluationGridModel.PupilCode;
                            objRatedCommentPupilBO.ClassName = objEvaluationGridModel.ClassName;
                            objRatedCommentPupilBO.SubjectName = objEvaluationGridModel.SubjectName;
                            objRatedCommentPupilBO.isCommenting = objEvaluationGridModel.IsCommenting;
                            objRatedCommentPupilBO.PeriodicMiddleMark = objEvaluationGridModel.PeriodicMiddleMark;
                            objRatedCommentPupilBO.PeriodicMiddleJudgement = objEvaluationGridModel.PeriodicMiddleJudgement;
                            objRatedCommentPupilBO.PeriodicEndingMark = objEvaluationGridModel.PeriodicEndingMark;
                            objRatedCommentPupilBO.PeriodicEndingJudgement = objEvaluationGridModel.PeriodicEndingJudgement;
                            objRatedCommentPupilBO.MiddleEvaluation = objEvaluationGridModel.MiddleEvaluation;
                            objRatedCommentPupilBO.MiddleEvaluationName = objEvaluationGridModel.MiddleEvaluationName;
                            objRatedCommentPupilBO.EndingEvaluation = objEvaluationGridModel.EndingEvaluation;
                            objRatedCommentPupilBO.EndingEvaluationName = objEvaluationGridModel.EndingEvaluationName;
                            objRatedCommentPupilBO.CapacityMiddleEvaluation = objEvaluationGridModel.CapacityMiddleEvaluation;
                            objRatedCommentPupilBO.CapacityMiddleEvaluationName = objRatedCommentPupilBO.CapacityMiddleEvaluationName;
                            objRatedCommentPupilBO.CapacityEndingEvaluation = objEvaluationGridModel.CapacityEndingEvaluation;
                            objRatedCommentPupilBO.CapacityEndingEvaluationName = objEvaluationGridModel.CapacityEndingEvaluationName;
                            objRatedCommentPupilBO.QualityMiddleEvaluation = objEvaluationGridModel.QualityMiddleEvaluation;
                            objRatedCommentPupilBO.QualityMiddleEvaluationName = objEvaluationGridModel.QualityMiddleEvaluationName;
                            objRatedCommentPupilBO.QualityEndingEvaluation = objEvaluationGridModel.QualityEndingEvaluation;
                            objRatedCommentPupilBO.QualityEndingEvaluationName = objEvaluationGridModel.QualityEndingEvaluationName;
                            objRatedCommentPupilBO.Comment = objEvaluationGridModel.Comment;
                            lstRatedCommentPupilBO.Add(objRatedCommentPupilBO);
                        }
                        if (UtilsBusiness.IsMoveHistory(objAy))
                        {
                            RatedCommentPupilHistoryBusiness.InsertOrUpdate(lstRatedCommentPupilBO, dic, ref lstActionAudit);
                        }
                        else
                        {
                            RatedCommentPupilBusiness.InsertOrUpdate(lstRatedCommentPupilBO, dic, ref lstActionAudit);
                        }
                        StringBuilder objectIDStr = new StringBuilder();
                        StringBuilder descriptionStr = new StringBuilder();
                        StringBuilder paramsStr = new StringBuilder();
                        StringBuilder oldObjectStr = new StringBuilder();
                        StringBuilder newObjectStr = new StringBuilder();
                        StringBuilder userFuntionsStr = new StringBuilder();
                        StringBuilder userActionsStr = new StringBuilder();
                        StringBuilder userDescriptionsStr = new StringBuilder();
                        for (int i = 0; i < lstActionAudit.Count; i++)
                        {
                            objActionAudit = lstActionAudit[i];
                            objectIDStr.Append(objActionAudit.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            descriptionStr.Append(objActionAudit.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            paramsStr.Append(objActionAudit.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            oldObjectStr.Append(objActionAudit.OldData).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            newObjectStr.Append(objActionAudit.NewData).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            userActionsStr.Append(objActionAudit.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            userFuntionsStr.Append(objActionAudit.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            userDescriptionsStr.Append(objActionAudit.UserDescription).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        }
                        IDictionary<string, object> dicLog = new Dictionary<string, object>()
                        {
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                        };
                        SetViewDataActionAudit(dicLog);
                        return Json(new JsonMessage(messageSuccess, "success"));
                    }
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
                }
            }

        }
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcelCap(int SemesterID, int ClassID, int TypeImport, string arrClassID, int EducationLevelID)
        {

            if (Session[RatedCommentPupilConstants.PHISICAL_PATH_CAP] == null) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            string physicalPath = (string)Session[RatedCommentPupilConstants.PHISICAL_PATH_CAP];
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            RatedCommentViewModel objRatedComemntImport = new RatedCommentViewModel();
            List<EvaluationGridModel> lstResult = new List<EvaluationGridModel>();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);
            string messageSuccess = "";

            if (TypeImport == 1)
            {
                #region Import danh gia cua mot lop
                lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                objRatedComemntImport = this.GetDataToFileExcel(obook, lstPOC, TypeImport, ClassID, 0, SemesterID, EducationLevelID, arrClassID, "", 0, GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY);
                messageSuccess = Res.Get("Common_Label_ImportSuccessMessage");
                #endregion
            }
            else
            {
                #region Import danh gia cua nhieu lop
                List<int> lstClassID = arrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                IDictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"lstClassID",lstClassID}
                };
                lstPOC = PupilOfClassBusiness.Search(dicSearch)
                                              .Select(p => new PupilOfClassBO
                                              {
                                                  PupilID = p.PupilID,
                                                  PupilCode = p.PupilProfile.PupilCode,
                                                  PupilFullName = p.PupilProfile.FullName,
                                                  ClassID = p.ClassID,
                                                  Status = p.Status,
                                                  OrderInClass = p.OrderInClass,
                                                  Name = p.PupilProfile.Name,
                                                  ClassName = p.ClassProfile.DisplayName,
                                                  ClassOrder = p.ClassProfile.OrderNumber,
                                                  EducationLevelID = p.ClassProfile.EducationLevelID
                                              }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                objRatedComemntImport = this.GetDataToFileExcel(obook, lstPOC, TypeImport, ClassID, 0, SemesterID, EducationLevelID, arrClassID, "", 0, GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY);
                if (lstClassID.Count > 1)
                {
                    messageSuccess = "Nhập dữ liệu từ excel thành công cho các lớp ";
                    List<string> lstClassName = lstPOC.OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassOrder).Select(p => p.ClassName).Distinct().ToList();
                    for (int i = 0; i < lstClassName.Count; i++)
                    {
                        if (i < lstClassName.Count - 1)
                        {
                            messageSuccess += lstClassName[i] + ", ";
                        }
                        else
                        {
                            messageSuccess += lstClassName[i];
                        }

                    }
                }
                else
                {
                    messageSuccess = Res.Get("Common_Label_ImportSuccessMessage");
                }
                #endregion
            }

            if (!string.IsNullOrEmpty(objRatedComemntImport.Error))
            {
                return Json(new JsonMessage(objRatedComemntImport.Error, "error"));
            }
            else
            {
                lstResult = objRatedComemntImport.lstEvalutionComment;
                if (lstResult.Count > 0)
                {
                    if (lstResult.Where(p => p.isErr).Count() > 0)
                    {
                        List<EvaluationGridModel> listError = lstResult.Where(o => o.isErr).ToList();
                        ViewData[RatedCommentPupilConstants.EVALUATION_ID] = GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY;
                        Session["lstResultErr"] = listError;
                        return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "grid"));
                    }
                    else
                    {
                        IDictionary<string, object> dic = new Dictionary<string, object>()
                        {
                            {"SchoolID",_globalInfo.SchoolID},
                            {"AcademicYearID",_globalInfo.AcademicYearID},
                            {"ClassID",ClassID},
                            {"SemesterID",SemesterID},
                            {"IsImport",true},
                            {"TypeID",GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY}
                        };
                        List<ActionAuditDataBO> lstActionAudit = new List<ActionAuditDataBO>();
                        ActionAuditDataBO objActionAudit = null;
                        AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                        List<RatedCommentPupilBO> lstRatedCommentPupilBO = new List<RatedCommentPupilBO>();
                        RatedCommentPupilBO objRatedCommentPupilBO = null;
                        EvaluationGridModel objEvaluationGridModel = null;
                        List<EvaluationCriteriaModel> lstECVM = null;
                        EvaluationCriteriaModel objECVM = null;
                        for (int i = 0; i < lstResult.Count; i++)//do du lieu vao doi tuong RatedCommentPupilBO
                        {
                            objEvaluationGridModel = lstResult[i];
                            if (objEvaluationGridModel != null)
                            {
                                lstECVM = objEvaluationGridModel.lstEvalutionCriteria;
                                for (int j = 0; j < lstECVM.Count; j++)
                                {
                                    objECVM = lstECVM[j];
                                    objRatedCommentPupilBO = new RatedCommentPupilBO();
                                    objRatedCommentPupilBO.SchoolID = _globalInfo.SchoolID.Value;
                                    objRatedCommentPupilBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                    objRatedCommentPupilBO.PupilID = objEvaluationGridModel.PupilID;
                                    objRatedCommentPupilBO.ClassID = objEvaluationGridModel.ClassID;
                                    objRatedCommentPupilBO.SemesterID = objEvaluationGridModel.SemesterID;
                                    objRatedCommentPupilBO.SubjectID = objECVM.EvaluationCriteriaID;
                                    objRatedCommentPupilBO.EvaluationID = objECVM.TypeID;
                                    objRatedCommentPupilBO.LogChangeID = objEvaluationGridModel.LogChangeID;
                                    objRatedCommentPupilBO.FullName = objEvaluationGridModel.FullName;
                                    objRatedCommentPupilBO.PupilCode = objEvaluationGridModel.PupilCode;
                                    objRatedCommentPupilBO.ClassName = objEvaluationGridModel.ClassName;
                                    if (objECVM.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                                    {
                                        objRatedCommentPupilBO.CapacityMiddleEvaluation = objECVM.GK;
                                        objRatedCommentPupilBO.CapacityMiddleEvaluationName = objECVM.GKName;
                                        objRatedCommentPupilBO.CapacityEndingEvaluation = objECVM.CK;
                                        objRatedCommentPupilBO.CapacityEndingEvaluationName = objECVM.CKName;
                                    }
                                    else
                                    {
                                        objRatedCommentPupilBO.QualityMiddleEvaluation = objECVM.GK;
                                        objRatedCommentPupilBO.QualityMiddleEvaluationName = objECVM.GKName;
                                        objRatedCommentPupilBO.QualityEndingEvaluation = objECVM.CK;
                                        objRatedCommentPupilBO.QualityEndingEvaluationName = objECVM.CKName;
                                    }
                                    objRatedCommentPupilBO.Comment = objECVM.Comment;
                                    lstRatedCommentPupilBO.Add(objRatedCommentPupilBO);
                                }
                            }
                        }
                        if (UtilsBusiness.IsMoveHistory(objAy))
                        {
                            RatedCommentPupilHistoryBusiness.InsertOrUpdate(lstRatedCommentPupilBO, dic, ref lstActionAudit);
                        }
                        else
                        {
                            RatedCommentPupilBusiness.InsertOrUpdate(lstRatedCommentPupilBO, dic, ref lstActionAudit);
                        }
                        StringBuilder objectIDStr = new StringBuilder();
                        StringBuilder descriptionStr = new StringBuilder();
                        StringBuilder paramsStr = new StringBuilder();
                        StringBuilder oldObjectStr = new StringBuilder();
                        StringBuilder newObjectStr = new StringBuilder();
                        StringBuilder userFuntionsStr = new StringBuilder();
                        StringBuilder userActionsStr = new StringBuilder();
                        StringBuilder userDescriptionsStr = new StringBuilder();
                        for (int i = 0; i < lstActionAudit.Count; i++)
                        {
                            objActionAudit = lstActionAudit[i];
                            objectIDStr.Append(objActionAudit.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            descriptionStr.Append(objActionAudit.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            paramsStr.Append(objActionAudit.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            oldObjectStr.Append(objActionAudit.OldData).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            newObjectStr.Append(objActionAudit.NewData).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            userActionsStr.Append(objActionAudit.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            userFuntionsStr.Append(objActionAudit.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            userDescriptionsStr.Append(objActionAudit.UserDescription).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        }
                        IDictionary<string, object> dicLog = new Dictionary<string, object>()
                        {
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                            {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                        };
                        SetViewDataActionAudit(dicLog);
                        return Json(new JsonMessage(messageSuccess, "success"));
                    }
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
                }
            }
        }
        public FileResult DownLoadFileErr(int EvaluationID)
        {
            string physicalPath = EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP
                ? (string)Session[RatedCommentPupilConstants.PHISICAL_PATH] : (string)Session[RatedCommentPupilConstants.PHISICAL_PATH_CAP];
            IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
            List<IVTWorksheet> lstSheet = oBook.GetSheets();
            IVTWorksheet sheet = null;
            List<EvaluationGridModel> lstEvaluationGridModel = (List<EvaluationGridModel>)Session["lstResultErr"];
            EvaluationGridModel objEvaluationComment = null;
            int startRow = 0;
            string pupilCode = string.Empty;
            string sheetName = string.Empty;
            for (int i = 0; i < lstSheet.Count; i++)
            {
                startRow = 11;
                sheet = lstSheet[i];
                sheetName = sheet.Name;
                while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
                {
                    pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                    string strValue = (sheet.GetCellValue(RatedCommentPupilConstants.CELL_CHECK_TYPE_FILE_IMPORT) != null ? sheet.GetCellValue(RatedCommentPupilConstants.CELL_CHECK_TYPE_FILE_IMPORT).ToString() : string.Empty);
                    if (EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
                    {
                        if ("1".Equals(strValue))
                        {
                            objEvaluationComment = lstEvaluationGridModel.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                        }
                        else if ("2".Equals(strValue))
                        {
                            objEvaluationComment = lstEvaluationGridModel.Where(p => p.PupilCode == pupilCode
                            && SMAS.Web.Utils.Utils.StripVNSignAndSpace(p.SubjectName).Equals(sheetName)).FirstOrDefault();
                        }
                        else
                        {
                            objEvaluationComment = lstEvaluationGridModel.Where(p => p.PupilCode == pupilCode
                            && SMAS.Web.Utils.Utils.StripVNSignAndSpace(p.ClassName).Equals(sheetName)).FirstOrDefault();
                        }
                    }
                    else
                    {
                        if ("1".Equals(strValue))
                        {
                            objEvaluationComment = lstEvaluationGridModel.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                        }
                        else if ("2".Equals(strValue))
                        {
                            objEvaluationComment = lstEvaluationGridModel.Where(p => p.PupilCode == pupilCode
                            && SMAS.Web.Utils.Utils.StripVNSignAndSpace(p.ClassName).Equals(sheetName)).FirstOrDefault();
                        }
                    }

                    if (objEvaluationComment == null)
                    {
                        startRow++;
                        continue;
                    }
                    else//fill them cot mo ta loi
                    {
                        if (EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
                        {
                            int isCommenting = int.Parse(sheet.GetCellValue("Y4").ToString());
                            if (isCommenting == 0)
                            {
                                sheet.UnHideColumn(9);
                                sheet.GetRange(startRow, 9, startRow, 9).WrapText();
                                sheet.GetRange(startRow, 9, startRow, 9).SetFontStyle(false, System.Drawing.Color.Red, false, 12, false, false);
                                sheet.SetCellValue(startRow, 9, objEvaluationComment.ErrMessage);
                            }
                            else
                            {
                                sheet.UnHideColumn(7);
                                sheet.GetRange(startRow, 7, startRow, 7).WrapText();
                                sheet.GetRange(startRow, 7, startRow, 7).SetFontStyle(false, System.Drawing.Color.Red, false, 12, false, false);
                                sheet.SetCellValue(startRow, 7, objEvaluationComment.ErrMessage);
                            }
                        }
                        else
                        {
                            sheet.UnHideColumn(20);
                            sheet.GetRange(startRow, 20, startRow, 20).WrapText();
                            sheet.GetRange(startRow, 20, startRow, 20).SetFontStyle(false, System.Drawing.Color.Red, false, 12, false, false);
                            sheet.SetCellValue(startRow, 20, objEvaluationComment.ErrMessage);
                        }
                        startRow++;
                    }
                }
            }
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "Sodanhgiahocsinh.xls";
            return result;
        }
        #endregion
        #region Private Action
        private RatedCommentViewModel GetDataToFileExcel(IVTWorkbook oBook, List<PupilOfClassBO> lstPOC, int TypeImport, int ClassID, int SubjectID, int SemesterID, int EducationLevelID, string arrClassID, string arrSubjectID, int isCommenting, int EvaluationID)
        {
            RatedCommentViewModel objResult = new RatedCommentViewModel();
            List<EvaluationGridModel> lstEvaluationComment = new List<EvaluationGridModel>();
            List<EvaluationGridModel> lsttmp = new List<EvaluationGridModel>();
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string sheetNameDefault = string.Empty;
            string sheetName = string.Empty;
            #region Mon hoc va HDGD
            if (EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
            {
                if (TypeImport == 1 || TypeImport == 2)
                {
                    ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
                    if (TypeImport == 1)
                    {
                        SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
                        sheetNameDefault = Utils.Utils.StripVNSignAndSpace(objSC.DisplayName);
                        IVTWorksheet sheet = oBook.GetSheet(1);
                        #region Kiem tra du lieu chon dau vao
                        string Error = string.Empty;
                        string ClassName = classProfile.DisplayName;
                        string SubjectName = objSC.DisplayName;
                        string SemesterName = SemesterID == 1 ? "Học kỳ 1" : "Học kỳ 2";
                        string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                        string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                        string AcademicYearNameFromExcel = (string)sheet.GetCellValue("A6");
                        string Title = "";
                        var TitleObj = sheet.GetCellValue("A5");

                        if (TitleObj != null)
                            Title = TitleObj.ToString();

                        if (Title == null || AcademicYearNameFromExcel == null)
                        {
                            Error = Res.Get("Common_Label_ExcelExtensionError");
                        }

                        if (Title.Contains(SubjectName.ToUpper()) == false)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_SubjectError") + " " + SubjectName;
                            }
                        }

                        if (Title.Contains(ClassName.ToUpper()) == false)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_ClassError") + " " + ClassName;
                            }
                        }
                        string[] tmp = AcademicYearNameFromExcel.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        bool isCurrentSemester = false;
                        for (int k = tmp.Length - 1; k >= 0; k--)
                        {
                            if (tmp[k] == SemesterName)
                            {
                                isCurrentSemester = true;
                                break;
                            }
                        }

                        if (!isCurrentSemester)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_SemesterError") + " " + SemesterName;
                            }
                        }

                        if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                            }
                        }

                        if (!string.IsNullOrEmpty(Error))
                        {
                            objResult.Error = Error;
                            objResult.lstEvalutionComment = new List<EvaluationGridModel>();
                            return objResult;
                        }
                        #endregion
                        #region lay du lieu tu file
                        lstEvaluationComment = this.GetData(sheet, lstPOC, null, ClassID, classProfile.DisplayName, SubjectID, sheetNameDefault, GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP, SemesterID, isCommenting, SubjectName, objSC.Abbreviation, EducationLevelID);
                        #endregion
                    }
                    else
                    {
                        List<int> lstSubjectAll = arrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();

                        List<int> lstSubjectID = new List<int>();

                        for (int i = 0; i < lstSubjectAll.Count; i++)
                        {
                            if (UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, ClassID, lstSubjectAll[i], SemesterID))
                            {
                                lstSubjectID.Add(lstSubjectAll[i]);
                            }
                        }

                        List<SubjectCatBO> lstCS = this.GetListSubject(ClassID, SemesterID).Where(p => lstSubjectID.Contains(p.SubjectCatID)).ToList(); ;
                        SubjectCatBO objCS = null;
                        IVTWorksheet sheetBySubjectName = null;
                        #region Validate
                        string SemesterName = SemesterID == 1 ? "Học kỳ 1" : "Học kỳ 2";
                        for (int i = 0; i < lstSubjectID.Count; i++)
                        {
                            objCS = lstCS.Where(p => p.SubjectCatID == lstSubjectID[i]).FirstOrDefault();
                            sheetNameDefault = Utils.Utils.StripVNSignAndSpace(objCS.DisplayName, true);
                            sheetBySubjectName = oBook.GetSheet(sheetNameDefault);
                            #region Kiem tra du lieu chon dau vao
                            string Error = string.Empty;
                            string ClassName = classProfile.DisplayName;
                            string SubjectName = objCS.DisplayName;
                            string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                            string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                            string AcademicYearNameFromExcel = (string)sheetBySubjectName.GetCellValue("A6");
                            string Title = "";
                            var TitleObj = sheetBySubjectName.GetCellValue("A5");

                            if (TitleObj != null)
                                Title = TitleObj.ToString();

                            if (Title == null || AcademicYearNameFromExcel == null)
                            {
                                Error = Res.Get("Common_Label_ExcelExtensionError");
                            }

                            if (Title.Contains(SubjectName.ToUpper()) == false)
                            {
                                if (string.IsNullOrEmpty(Error))
                                {
                                    Error = Res.Get("RatedCommentPupil_Label_SubjectError") + " " + SubjectName;
                                }
                            }

                            if (Title.Contains(ClassName.ToUpper()) == false)
                            {
                                if (string.IsNullOrEmpty(Error))
                                {
                                    Error = Res.Get("RatedCommentPupil_Label_ClassError") + " " + ClassName;
                                }
                            }
                            string[] tmp = AcademicYearNameFromExcel.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            bool isCurrentSemester = false;
                            for (int k = tmp.Length - 1; k >= 0; k--)
                            {
                                if (tmp[k] == SemesterName)
                                {
                                    isCurrentSemester = true;
                                    break;
                                }
                            }

                            if (!isCurrentSemester)
                            {
                                if (string.IsNullOrEmpty(Error))
                                {
                                    Error = Res.Get("RatedCommentPupil_Label_SemesterError") + " " + SemesterName;
                                }
                            }

                            if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                            {
                                if (string.IsNullOrEmpty(Error))
                                {
                                    Error = Res.Get("RatedCommentPupil_Label_YearError") + " " + AcademicYearName;
                                }
                            }

                            if (!string.IsNullOrEmpty(Error))
                            {
                                objResult.Error = Error;
                                objResult.lstEvalutionComment = new List<EvaluationGridModel>();
                                return objResult;
                            }
                            #endregion
                        }
                        #endregion
                        #region lay du lieu tu file
                        for (int i = 0; i < lstSubjectID.Count; i++)
                        {
                            objCS = lstCS.Where(p => p.SubjectCatID == lstSubjectID[i]).FirstOrDefault();
                            sheetNameDefault = Utils.Utils.StripVNSignAndSpace(objCS.DisplayName, true);
                            sheetBySubjectName = oBook.GetSheet(sheetNameDefault);
                            lsttmp = this.GetData(sheetBySubjectName, lstPOC, null, ClassID, classProfile.DisplayName, objCS.SubjectCatID, sheetNameDefault, GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP, SemesterID, objCS.IsCommenting.Value, objCS.DisplayName, objCS.Abbreviation, EducationLevelID);
                            for (int j = 0; j < lsttmp.Count; j++)
                            {
                                lstEvaluationComment.Add(lsttmp[j]);
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    List<int> lstClassID = arrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                    IDictionary<string, object> dicSearchClass = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID},
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"lstClassID",lstClassID}
                    };
                    List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicSearchClass).ToList();
                    SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
                    ClassProfile objCP = null;
                    IVTWorksheet sheetByClassName = null;
                    #region Validate
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                        objCP = lstCP.Where(p => p.ClassProfileID == lstClassID[i]).FirstOrDefault();
                        sheetNameDefault = Business.Common.Utils.StripVNSignAndSpace(objCP.DisplayName);// Utils.Utils.StripVNSignAndSpace(objCP.DisplayName);
                        sheetByClassName = oBook.GetSheet(sheetNameDefault);
                        #region Kiem tra du lieu chon dau vao
                        string Error = string.Empty;
                        string ClassName = objCP.DisplayName;
                        string SemesterName = SemesterID == 1 ? "Học kỳ 1" : "Học kỳ 2";
                        string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                        string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                        string AcademicYearNameFromExcel = (string)sheetByClassName.GetCellValue("A6");
                        string Title = "";
                        var TitleObj = sheetByClassName.GetCellValue("A5");

                        if (TitleObj != null)
                            Title = TitleObj.ToString();

                        if (Title == null || AcademicYearNameFromExcel == null)
                        {
                            Error = Res.Get("Common_Label_ExcelExtensionError");
                        }

                        if (Title.Contains(objSC.SubjectName.ToUpper()) == false)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_SubjectError") + " " + objSC.SubjectName;
                            }
                        }

                        if (Title.Contains(ClassName.ToUpper()) == false)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_ClassError") + " " + ClassName;
                            }
                        }
                        string[] tmp = AcademicYearNameFromExcel.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        bool isCurrentSemester = false;
                        for (int k = tmp.Length - 1; k >= 0; k--)
                        {
                            if (tmp[k] == SemesterName)
                            {
                                isCurrentSemester = true;
                                break;
                            }
                        }

                        if (!isCurrentSemester)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_SemesterError") + " " + SemesterName;
                            }
                        }

                        if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_YearError") + " " + AcademicYearName;
                            }
                        }

                        if (!string.IsNullOrEmpty(Error))
                        {
                            objResult.Error = Error;
                            objResult.lstEvalutionComment = new List<EvaluationGridModel>();
                            return objResult;
                        }
                        #endregion
                    }
                    #endregion
                    #region lay du lieu tu file
                    List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
                    int classIDtmp = 0;
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                        classIDtmp = lstClassID[i];
                        objCP = lstCP.Where(p => p.ClassProfileID == classIDtmp).FirstOrDefault();
                        sheetNameDefault = Business.Common.Utils.StripVNSignAndSpace(objCP.DisplayName); //Utils.Utils.StripVNSignAndSpace(objCP.DisplayName, true);
                        sheetByClassName = oBook.GetSheet(sheetNameDefault);
                        if (sheetByClassName == null)
                        {
                            continue;
                        }
                        lstPOCtmp = lstPOC.Where(p => p.ClassID == classIDtmp).ToList();
                        lsttmp = this.GetData(sheetByClassName, lstPOCtmp, null, classIDtmp, objCP.DisplayName, SubjectID, sheetNameDefault, GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP, SemesterID, isCommenting, objSC.SubjectName, objSC.Abbreviation, EducationLevelID);
                        for (int j = 0; j < lsttmp.Count; j++)
                        {
                            lstEvaluationComment.Add(lsttmp[j]);
                        }
                    }
                    #endregion
                }
            }
            #endregion
            #region Nang luc pham chat
            else
            {
                List<EvaluationCriteiaBO> lstEGBO = EvaluationCriteriaBusiness.All.Select(p => new EvaluationCriteiaBO
                {
                    EvaluationCriteriaID = p.EvaluationCriteriaID,
                    TypeID = p.TypeID,
                    CriteriaName = p.CriteriaName
                }).OrderBy(p => p.EvaluationCriteriaID).ToList();
                if (TypeImport == 1)
                {
                    ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
                    sheetNameDefault = Utils.Utils.StripVNSignAndSpace(classProfile.DisplayName, true);
                    IVTWorksheet sheet = oBook.GetSheet(1);
                    #region Kiem tra du lieu chon dau vao
                    string Error = string.Empty;
                    string ClassName = classProfile.DisplayName;
                    string SemesterName = SemesterID == 1 ? "Học kỳ 1" : "Học kỳ 2";
                    string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                    string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                    string AcademicYearNameFromExcel = (string)sheet.GetCellValue("A6");
                    string Title = "";
                    var TitleObj = sheet.GetCellValue("A5");

                    if (TitleObj != null)
                        Title = TitleObj.ToString();

                    if (Title == null || AcademicYearNameFromExcel == null)
                    {
                        Error = Res.Get("Common_Label_ExcelExtensionError");
                    }

                    if (Title.Contains(ClassName.ToUpper()) == false)
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = Res.Get("RatedCommentPupil_Label_ClassError") + " " + ClassName;
                        }
                    }
                    string[] tmp = AcademicYearNameFromExcel.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    bool isCurrentSemester = false;
                    for (int k = tmp.Length - 1; k >= 0; k--)
                    {
                        if (tmp[k] == SemesterName)
                        {
                            isCurrentSemester = true;
                            break;
                        }
                    }

                    if (!isCurrentSemester)
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = Res.Get("RatedCommentPupil_Label_SemesterError") + " " + SemesterName;
                        }
                    }

                    if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                    {
                        if (string.IsNullOrEmpty(Error))
                        {
                            Error = Res.Get("RatedCommentPupil_Label_YearError") + " " + AcademicYearName;
                        }
                    }

                    if (!string.IsNullOrEmpty(Error))
                    {
                        objResult.Error = Error;
                        objResult.lstEvalutionComment = new List<EvaluationGridModel>();
                        return objResult;
                    }
                    #endregion
                    #region lay du lieu tu file
                    lstEvaluationComment = this.GetData(sheet, lstPOC, lstEGBO, ClassID, classProfile.DisplayName, SubjectID, sheetNameDefault, GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY, SemesterID, 0, "", "", EducationLevelID);
                    #endregion
                }
                else
                {
                    List<int> lstClassID = arrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                    IDictionary<string, object> dicSearchClass = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID},
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"lstClassID",lstClassID}
                    };
                    List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicSearchClass).ToList();
                    ClassProfile objCP = null;
                    IVTWorksheet sheetByClassName = null;
                    #region Validate
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                        objCP = lstCP.Where(p => p.ClassProfileID == lstClassID[i]).FirstOrDefault();
                        sheetNameDefault = Utils.Utils.StripVNSignAndSpace(objCP.DisplayName, true);
                        sheetByClassName = oBook.GetSheet(sheetNameDefault);
                        #region Kiem tra du lieu chon dau vao
                        string Error = string.Empty;
                        string ClassName = objCP.DisplayName;
                        string SemesterName = SemesterID == 1 ? "Học kỳ 1" : "Học kỳ 2";
                        string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                        string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                        string AcademicYearNameFromExcel = (string)sheetByClassName.GetCellValue("A6");
                        string Title = "";
                        var TitleObj = sheetByClassName.GetCellValue("A5");

                        if (TitleObj != null)
                            Title = TitleObj.ToString();

                        if (Title == null || AcademicYearNameFromExcel == null)
                        {
                            Error = Res.Get("Common_Label_ExcelExtensionError");
                        }

                        if (Title.Contains(ClassName.ToUpper()) == false)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_ClassError") + " " + ClassName;
                            }
                        }
                        string[] tmp = AcademicYearNameFromExcel.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        bool isCurrentSemester = false;
                        for (int k = tmp.Length - 1; k >= 0; k--)
                        {
                            if (tmp[k] == SemesterName)
                            {
                                isCurrentSemester = true;
                                break;
                            }
                        }

                        if (!isCurrentSemester)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_SemesterError") + " " + SemesterName;
                            }
                        }

                        if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                        {
                            if (string.IsNullOrEmpty(Error))
                            {
                                Error = Res.Get("RatedCommentPupil_Label_YearError") + " " + AcademicYearName;
                            }
                        }

                        if (!string.IsNullOrEmpty(Error))
                        {
                            objResult.Error = Error;
                            objResult.lstEvalutionComment = new List<EvaluationGridModel>();
                            return objResult;
                        }
                        #endregion
                    }
                    #endregion
                    #region lay du lieu tu file
                    List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
                    int classIDtmp = 0;
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                        classIDtmp = lstClassID[i];
                        objCP = lstCP.Where(p => p.ClassProfileID == classIDtmp).FirstOrDefault();
                        sheetNameDefault = Utils.Utils.StripVNSignAndSpace(objCP.DisplayName, true);
                        sheetByClassName = oBook.GetSheet(sheetNameDefault);
                        lstPOCtmp = lstPOC.Where(p => p.ClassID == classIDtmp).ToList();
                        lsttmp = this.GetData(sheetByClassName, lstPOCtmp, lstEGBO, classIDtmp, objCP.DisplayName, SubjectID, sheetNameDefault, GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY, SemesterID, 0, "", "", EducationLevelID);
                        for (int j = 0; j < lsttmp.Count; j++)
                        {
                            lstEvaluationComment.Add(lsttmp[j]);
                        }
                    }
                    #endregion
                }
            }
            #endregion
            objResult.lstEvalutionComment = lstEvaluationComment;
            return objResult;
        }
        private List<EvaluationGridModel> GetData(IVTWorksheet sheet, List<PupilOfClassBO> lstPOC, List<EvaluationCriteiaBO> lstECBO, int ClassID, string ClassName, int SubjectID, string sheetName, int EvaluationID, int SemesterID, int IsCommenting, string SubjectName, string Abbreviation, int EducationLevelID)
        {
            List<EvaluationGridModel> lstEvaluationComment = new List<EvaluationGridModel>();
            int logChangeID = _globalInfo.IsAdminSchoolRole ? 0 : _globalInfo.EmployeeID.Value;
            EvaluationGridModel objComment = null;
            PupilOfClassBO objPOC = null;
            string pupilCode = string.Empty;
            bool isErr = false;
            string errorMessage = string.Empty;
            List<int> lstPupilID = new List<int>();
            int pupilID = 0;
            int startRow = 11;
            while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
            {
                isErr = false;
                errorMessage = string.Empty;
                objComment = new EvaluationGridModel();
                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    errorMessage = Res.Get("RatedCommentPupiil_Label_Not_Pupil_Status_Studying");
                    isErr = true;
                }
                else
                {
                    pupilID = objPOC.PupilID;
                    lstPupilID.Add(pupilID);
                    if (lstPupilID.Where(p => p == pupilID).Count() > 1)
                    {
                        errorMessage = Res.Get("Rated_PupilCode_Duplicate");
                        isErr = true;
                    }
                    objComment.FullName = objPOC.PupilFullName;
                    if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    }
                }
                objComment.PupilID = pupilID;
                objComment.PupilCode = pupilCode;

                #region Mon hoc va HDGD
                if (EvaluationID == 1)
                {
                    int Mark = 0;
                    string MarkCell = string.Empty;
                    if (sheet.GetCellValue(startRow, 4) != null)//Danh gia GK
                    {
                        MarkCell = sheet.GetCellValue(startRow, 4).ToString();
                        if (!GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_NOT_COMPLETE.Equals(MarkCell))
                        {
                            if (string.IsNullOrEmpty(errorMessage))
                            {
                                errorMessage = Res.Get("RatedCommentPupil_Label_MarkRateError");
                            }
                            isErr = true;
                        }
                        objComment.MiddleEvaluation = GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) ? 1 : GlobalConstants.EVALUATION_COMPLETE.Equals(MarkCell) ? 2 : 3;
                        objComment.MiddleEvaluationName = MarkCell;
                    }

                    if (IsCommenting == 0)
                    {
                        MarkCell = string.Empty;
                        Mark = 0;
                        if (sheet.GetCellValue(startRow, 5) != null && ((Abbreviation.Equals("Tt") || Abbreviation.Equals("TV")) && (EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID4 || EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID5)))//diem KTGK
                        {
                            MarkCell = sheet.GetCellValue(startRow, 5).ToString();
                            if (int.TryParse(MarkCell, out Mark))
                            {
                                if (Mark < 1 || Mark > 10)
                                {
                                    if (string.IsNullOrEmpty(errorMessage))
                                    {
                                        errorMessage = Res.Get("RatedCommentPupil_Label_MarkRateError");
                                    }
                                    isErr = true;
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(errorMessage))
                                {
                                    errorMessage = Res.Get("RatedCommentPupil_Label_MarkRateError");
                                }
                                isErr = true;
                            }
                            objComment.PeriodicMiddleMark = Mark;
                        }

                        MarkCell = string.Empty;
                        if (sheet.GetCellValue(startRow, 6) != null)//Danh gia CK
                        {
                            MarkCell = sheet.GetCellValue(startRow, 6).ToString();
                            if (!GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_NOT_COMPLETE.Equals(MarkCell))
                            {
                                if (string.IsNullOrEmpty(errorMessage))
                                {
                                    errorMessage = Res.Get("RatedCommentPupil_Label_MarkRateError");
                                }
                                isErr = true;
                            }
                            objComment.EndingEvaluation = GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) ? 1 : GlobalConstants.EVALUATION_COMPLETE.Equals(MarkCell) ? 2 : 3;
                            objComment.EndingEvaluationName = MarkCell;
                        }

                        MarkCell = string.Empty;
                        if (sheet.GetCellValue(startRow, 7) != null)//diem KTCK
                        {
                            MarkCell = sheet.GetCellValue(startRow, 7).ToString();
                            if (IsCommenting == 0)//mon tinh diem
                            {
                                if (int.TryParse(MarkCell, out Mark))
                                {
                                    if (Mark < 1 || Mark > 10)
                                    {
                                        if (string.IsNullOrEmpty(errorMessage))
                                        {
                                            errorMessage = Res.Get("RatedCommentPupil_Label_MarkRateError");
                                        }
                                        isErr = true;
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(errorMessage))
                                    {
                                        errorMessage = Res.Get("RatedCommentPupil_Label_MarkRateError");
                                    }
                                    isErr = true;
                                }
                                objComment.PeriodicEndingMark = Mark;
                            }
                        }

                        MarkCell = string.Empty;
                        if (sheet.GetCellValue(startRow, 8) != null)
                        {
                            MarkCell = sheet.GetCellValue(startRow, 8).ToString();
                            if (MarkCell.Length > 1500)
                            {
                                if (string.IsNullOrEmpty(errorMessage))
                                {
                                    errorMessage = Res.Get("Rated_Comment_Maxlength");
                                }
                                isErr = true;
                            }
                            else if (MarkCell.Contains("<") || MarkCell.Contains(">"))
                            {
                                errorMessage = Res.Get("Common_Validate_XSS");
                                isErr = true;
                            }
                            objComment.Comment = MarkCell;
                        }
                    }
                    else
                    {
                        MarkCell = string.Empty;
                        if (sheet.GetCellValue(startRow, 5) != null)//Danh gia CK
                        {
                            MarkCell = sheet.GetCellValue(startRow, 5).ToString();
                            if (!GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_NOT_COMPLETE.Equals(MarkCell))
                            {
                                if (string.IsNullOrEmpty(errorMessage))
                                {
                                    errorMessage = Res.Get("RatedCommentPupil_Label_MarkRateError");
                                }
                                isErr = true;
                            }
                            objComment.EndingEvaluation = GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) ? 1 : GlobalConstants.EVALUATION_COMPLETE.Equals(MarkCell) ? 2 : 3;
                            objComment.EndingEvaluationName = MarkCell;
                        }

                        MarkCell = string.Empty;
                        if (sheet.GetCellValue(startRow, 6) != null)
                        {
                            MarkCell = sheet.GetCellValue(startRow, 6).ToString();
                            if (MarkCell.Length > 1500)
                            {
                                if (string.IsNullOrEmpty(errorMessage))
                                {
                                    errorMessage = Res.Get("Rated_Comment_Maxlength");
                                }
                                isErr = true;
                            }
                            else if (MarkCell.Contains("<") || MarkCell.Contains(">"))
                            {
                                errorMessage = Res.Get("Common_Validate_XSS");
                                isErr = true;
                            }
                            objComment.Comment = MarkCell;
                        }
                    }

                    objComment.EvaluationID = 1;
                    objComment.TypeID = 1;
                    objComment.isErr = isErr;
                    objComment.ErrMessage = errorMessage;
                    objComment.ClassID = ClassID;
                    objComment.ClassName = ClassName;
                    objComment.LogChangeID = logChangeID;
                    objComment.SubjectID = SubjectID;
                    objComment.SemesterID = SemesterID;
                    objComment.IsCommenting = IsCommenting;
                    objComment.SubjectName = Utils.Utils.StripVNSignAndSpace(SubjectName, true);
                }
                #endregion
                #region Nang luc pham chat
                else
                {

                    List<EvaluationCriteriaModel> lstECVM = new List<EvaluationCriteriaModel>();
                    EvaluationCriteriaModel objECVM = null;
                    EvaluationCriteiaBO objECBO = null;
                    string MarkCell = string.Empty;
                    int startCol = 4;
                    for (int i = 0; i < lstECBO.Count; i++)
                    {
                        objECBO = lstECBO[i];
                        objECVM = new EvaluationCriteriaModel();
                        objECVM.EvaluationCriteriaID = objECBO.EvaluationCriteriaID;
                        objECVM.TypeID = objECBO.TypeID;
                        MarkCell = sheet.GetCellValue(startRow, startCol) != null ? sheet.GetCellValue(startRow, startCol).ToString() : "";
                        if (!string.IsNullOrEmpty(MarkCell))
                        {
                            if (!GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_CAPQUA_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_NOT_COMPLETE.Equals(MarkCell))
                            {
                                if (string.IsNullOrEmpty(errorMessage))
                                {
                                    errorMessage = Res.Get("RatedCommentPupil_Label_CapAndQuaError");
                                }
                                isErr = true;
                            }
                            objECVM.GK = GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) ? 1 : GlobalConstants.EVALUATION_CAPQUA_COMPLETE.Equals(MarkCell) ? 2 : 3;
                            objECVM.GKName = MarkCell;
                        }

                        MarkCell = string.Empty;
                        MarkCell = sheet.GetCellValue(startRow, startCol + lstECBO.Count) != null ? sheet.GetCellValue(startRow, startCol + lstECBO.Count).ToString() : "";
                        if (!string.IsNullOrEmpty(MarkCell))
                        {
                            if (!string.IsNullOrEmpty(MarkCell) && !GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_CAPQUA_COMPLETE.Equals(MarkCell) && !GlobalConstants.EVALUATION_NOT_COMPLETE.Equals(MarkCell))
                            {
                                if (string.IsNullOrEmpty(errorMessage))
                                {
                                    errorMessage = Res.Get("RatedCommentPupil_Label_CapAndQuaError");
                                }
                                isErr = true;
                            }
                            objECVM.CK = GlobalConstants.EVALUATION_GOOD_COMPLETE.Equals(MarkCell) ? 1 : GlobalConstants.EVALUATION_CAPQUA_COMPLETE.Equals(MarkCell) ? 2 : 3;
                            objECVM.CKName = MarkCell;
                        }

                        MarkCell = string.Empty;
                        if (objECBO.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                        {
                            if (sheet.GetCellValue(startRow, 18) != null)
                            {
                                MarkCell = sheet.GetCellValue(startRow, 18).ToString();
                            }
                        }
                        else
                        {
                            if (sheet.GetCellValue(startRow, 19) != null)
                            {
                                MarkCell = sheet.GetCellValue(startRow, 19).ToString();
                            }
                        }

                        if (MarkCell.Length > 1500)
                        {
                            if (string.IsNullOrEmpty(errorMessage))
                            {
                                errorMessage = Res.Get("Rated_Comment_Maxlength");
                            }
                            isErr = true;
                        }
                        else if (MarkCell.Contains("<") || MarkCell.Contains(">"))
                        {
                            errorMessage = Res.Get("Common_Validate_XSS");
                        }
                        objECVM.Comment = MarkCell;
                        lstECVM.Add(objECVM);
                        startCol++;
                    }
                    objComment.EvaluationID = 2;
                    objComment.TypeID = 2;
                    objComment.isErr = isErr;
                    objComment.ErrMessage = errorMessage;
                    objComment.ClassID = ClassID;
                    objComment.ClassName = ClassName;
                    objComment.LogChangeID = logChangeID;
                    objComment.SemesterID = SemesterID;
                    objComment.lstEvalutionCriteria = lstECVM;
                }
                #endregion
                lstEvaluationComment.Add(objComment);
                startRow++;
            }
            return lstEvaluationComment;
        }
        private void SetDataToFile(IVTWorksheet sheet, List<PupilOfClassBO> lstPOC, List<EvaluationGridModel> lstRatedComment, int EvaluationID, string YearTitle, string SubjectName, string ClassName, int SemesterID, int sheetNumber, int isComemnting, ref List<VTDataValidation> lstValidation)
        {
            //fill thong tin chung
            string strTitle = string.Empty;
            VTDataValidation objValidation = new VTDataValidation();
            EvaluationGridModel objRated = null;
            List<EvaluationCriteriaModel> lstECVM = new List<EvaluationCriteriaModel>();
            EvaluationCriteriaModel objECVM = null;
            string[] cd = new string[] { GlobalConstants.EVALUATION_GOOD_COMPLETE, EvaluationID == GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : GlobalConstants.EVALUATION_COMPLETE, GlobalConstants.EVALUATION_NOT_COMPLETE };
            if (EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
            {
                strTitle = "BẢNG KẾT QUẢ ĐÁNH GIÁ MÔN {0} - LỚP {1}";
                sheet.SetCellValue("A5", string.Format(strTitle, SubjectName.ToUpper(), ClassName.ToUpper()));
                objValidation = new VTDataValidation
                {
                    Contrains = cd,
                    FromColumn = 4,
                    FromRow = 11,
                    SheetIndex = sheetNumber,
                    ToColumn = 4,
                    ToRow = 11 + lstPOC.Count - 1,
                    Type = VTValidationType.LIST
                };
                lstValidation.Add(objValidation);
                if (isComemnting == 0)
                {
                    objValidation = new VTDataValidation
                    {
                        FromColumn = 5,
                        FromRow = 11,
                        ToColumn = 5,
                        SheetIndex = sheetNumber,
                        ToRow = 11 + lstPOC.Count - 1,
                        FromValue = "1",
                        ToValue = "10",
                        Type = VTValidationType.INTEGER
                    };
                    lstValidation.Add(objValidation);

                    objValidation = new VTDataValidation
                    {
                        Contrains = cd,
                        FromColumn = 6,
                        FromRow = 11,
                        SheetIndex = sheetNumber,
                        ToColumn = 6,
                        ToRow = 11 + lstPOC.Count - 1,
                        Type = VTValidationType.LIST
                    };
                    lstValidation.Add(objValidation);

                    objValidation = new VTDataValidation
                    {
                        FromColumn = 7,
                        FromRow = 11,
                        ToColumn = 7,
                        SheetIndex = sheetNumber,
                        ToRow = 11 + lstPOC.Count - 1,
                        FromValue = "1",
                        ToValue = "10",
                        Type = VTValidationType.INTEGER
                    };
                    lstValidation.Add(objValidation);
                    sheet.SetComment(10, 5, Res.Get("Rated_Comment_Validate_Number"));
                    sheet.SetComment(10, 7, Res.Get("Rated_Comment_Validate_Number"));
                }
                else
                {
                    objValidation = new VTDataValidation
                    {
                        Contrains = cd,
                        FromColumn = 5,
                        FromRow = 11,
                        SheetIndex = sheetNumber,
                        ToColumn = 5,
                        ToRow = 11 + lstPOC.Count - 1,
                        Type = VTValidationType.LIST
                    };
                    lstValidation.Add(objValidation);
                }
            }
            else
            {
                strTitle = "BẢNG KẾT QUẢ ĐÁNH GIÁ NĂNG LỰC, PHẨM CHẤT - {0}";
                sheet.SetCellValue("A5", string.Format(strTitle, ClassName.ToUpper()));
                objValidation = new VTDataValidation
                {
                    Contrains = cd,
                    FromColumn = 4,
                    FromRow = 11,
                    SheetIndex = sheetNumber,
                    ToColumn = 17,
                    ToRow = 11 + lstPOC.Count - 1,
                    Type = VTValidationType.LIST
                };
                lstValidation.Add(objValidation);
            }
            strTitle = "Học kỳ " + SemesterID + ", Năm học " + YearTitle;
            sheet.SetCellValue("A6", strTitle);
            string strTitleMiddleSemester = "Giữa học kỳ " + (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "I" : "II");
            string strTitleEndingSemester = "Cuối học kỳ " + (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "I" : "II");
            if (EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
            {
                if (isComemnting == 0)
                {
                    sheet.SetCellValue("D9", strTitleMiddleSemester);
                    sheet.SetCellValue("F9", strTitleEndingSemester);
                }
                else
                {
                    sheet.DeleteColumn(5);
                    sheet.DeleteColumn(5);
                    sheet.GetRange(9, 4, 10, 4).Merge();
                    sheet.SetCellValue("D9", strTitleMiddleSemester);
                    sheet.SetColumnWidth(4, 23);
                    sheet.GetRange(9, 5, 10, 5).Merge();
                    sheet.SetCellValue("E9", strTitleEndingSemester);
                    sheet.SetColumnWidth(5, 23);

                    sheet.GetRange(5, 1, 5, 7).Merge();
                    sheet.GetRange(6, 1, 6, 7).Merge();
                    sheet.GetRange(8, 1, 8, 7).Merge();
                    sheet.GetRange(8, 1, 8, 7).SetHAlign(VTHAlign.xlHAlignRight);
                }

            }
            else
            {
                sheet.SetCellValue("D8", strTitleMiddleSemester);
                sheet.SetCellValue("K8", strTitleEndingSemester);
            }

            int startRow = 11;
            PupilOfClassBO objPOC = null;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objRated = lstRatedComment.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                sheet.SetCellValue(startRow, 1, i + 1);
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);
                if (objRated != null)
                {
                    if (EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
                    {
                        if (isComemnting == 0)
                        {
                            sheet.SetCellValue(startRow, 4, objRated.MiddleEvaluationName);
                            sheet.GetRange(startRow, 4, startRow, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheet.SetCellValue(startRow, 5, objRated.PeriodicMiddleMark.ToString());
                            sheet.GetRange(startRow, 5, startRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheet.SetCellValue(startRow, 6, objRated.EndingEvaluationName);
                            sheet.GetRange(startRow, 6, startRow, 6).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheet.SetCellValue(startRow, 7, objRated.PeriodicEndingMark.ToString());
                            sheet.GetRange(startRow, 7, startRow, 7).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheet.SetCellValue(startRow, 8, objRated.Comment);
                            sheet.GetRange(startRow, 8, startRow, 8).WrapText();
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, 4, objRated.MiddleEvaluationName);
                            sheet.GetRange(startRow, 4, startRow, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheet.SetCellValue(startRow, 5, objRated.EndingEvaluationName);
                            sheet.GetRange(startRow, 5, startRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheet.SetCellValue(startRow, 6, objRated.Comment);
                            sheet.GetRange(startRow, 6, startRow, 6).WrapText();
                        }
                    }
                    else
                    {
                        lstECVM = objRated.lstEvalutionCriteria.ToList();
                        int startCol = 4;
                        int startColQua = 7;
                        bool istrueCap = true;
                        bool istrueQua = true;
                        for (int j = 1; j < 8; j++)
                        {
                            objECVM = lstECVM.Where(p => p.EvaluationCriteriaID == j).FirstOrDefault();
                            if (objECVM != null)
                            {
                                if (objECVM.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                                {
                                    sheet.SetCellValue(startRow, startCol, objECVM.GKName);
                                    sheet.GetRange(startRow, startCol, startRow, startCol).SetHAlign(VTHAlign.xlHAlignCenter);

                                    sheet.SetCellValue(startRow, startCol + 7, objECVM.CKName);
                                    sheet.GetRange(startRow, startCol + 7, startRow, startCol + 7).SetHAlign(VTHAlign.xlHAlignCenter);
                                    startCol++;
                                    istrueCap = false;
                                }
                                else
                                {
                                    sheet.SetCellValue(startRow, startColQua, objECVM.GKName);
                                    sheet.GetRange(startRow, startColQua, startRow, startColQua).SetHAlign(VTHAlign.xlHAlignCenter);

                                    sheet.SetCellValue(startRow, startColQua + 7, objECVM.CKName);
                                    sheet.GetRange(startRow, startColQua + 7, startRow, startColQua + 7).SetHAlign(VTHAlign.xlHAlignCenter);
                                    startColQua++;
                                    istrueQua = false;
                                }
                            }
                            else
                            {
                                istrueCap = true;
                                istrueQua = true;
                                if (j < 4)
                                {
                                    if (istrueCap)
                                    {
                                        startCol++;
                                    }
                                }
                                else
                                {
                                    if (istrueQua)
                                    {
                                        startColQua++;
                                    }
                                }
                            }
                        }
                        objECVM = lstECVM.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY).FirstOrDefault();
                        sheet.SetCellValue(startRow, 18, objECVM != null ? objECVM.Comment : "");
                        sheet.GetRange(startRow, 18, startRow, 18).WrapText();
                        objECVM = lstECVM.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_QUALITY).FirstOrDefault();
                        sheet.SetCellValue(startRow, 19, objECVM != null ? objECVM.Comment : "");
                        sheet.GetRange(startRow, 19, startRow, 19).WrapText();
                    }
                }
                else
                {
                    if (EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                    {
                        sheet.GetRange(startRow, 4, startRow, 7).SetHAlign(VTHAlign.xlHAlignCenter);
                    }
                    else
                    {
                        sheet.GetRange(startRow, 4, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    }
                }
                if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    if (EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                    {
                        sheet.GetRange(startRow, 1, startRow, 8).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        if (isComemnting == 0)
                        {
                            sheet.GetRange(startRow, 1, startRow, 8).IsLock = true;
                        }
                        else
                        {
                            sheet.GetRange(startRow, 1, startRow, 6).IsLock = true;
                        }

                    }
                    else
                    {
                        sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, 19).IsLock = true;
                    }

                }
                else
                {
                    if (EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                    {
                        if (isComemnting == 0)
                        {
                            sheet.GetRange(startRow, 1, startRow, 8).IsLock = false;
                        }
                        else
                        {
                            sheet.GetRange(startRow, 1, startRow, 6).IsLock = false;
                        }

                    }
                }
                startRow++;
            }
            if (lstPOC.Count > 0)
            {
                if (EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    if (isComemnting == 0)
                    {
                        sheet.GetRange(9, 1, 11 + lstPOC.Count - 1, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    }
                    else
                    {
                        sheet.GetRange(9, 1, 11 + lstPOC.Count - 1, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    }
                }
                else
                {
                    sheet.GetRange(11, 1, 11 + lstPOC.Count - 1, 20).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
            }
        }
        private List<SubjectCatBO> GetListSubject(int ClassID, int SemesterID)
        {
            bool ViewAll = false;

            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SemesterID, ClassID, ViewAll).Where(u => SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                              join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                              where s.IsActive == true
                              && s.IsApprenticeshipSubject == false
                              select new SubjectCatBO()
                              {
                                  SubjectCatID = s.SubjectCatID,
                                  DisplayName = s.DisplayName,
                                  IsCommenting = cs.IsCommenting,
                                  OrderInSubject = s.OrderInSubject,
                                  Abbreviation = s.Abbreviation
                              }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            return lsClassSubject;
        }
        private void GetListViewData(List<PupilOfClassBO> lstPOC, IDictionary<string, object> dic)
        {
            int EvaluationID = SMAS.Business.Common.Utils.GetInt(dic, "TypeID");
            List<EvaluationGridModel> lstResult = new List<EvaluationGridModel>();
            EvaluationGridModel objResult = null;
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID,
                                        }).ToList();
            }
            else
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID
                                        }).ToList();
            }

            RatedCommentPupilBO objRatedComment = null;
            RatedCommentPupilBO objRatedtmp = null;
            List<RatedCommentPupilBO> lsttmpBO = new List<RatedCommentPupilBO>();
            PupilOfClassBO objPOC = null;
            int PupilID = 0;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objResult = new EvaluationGridModel();
                objPOC = lstPOC[i];
                PupilID = objPOC.PupilID;
                objResult.PupilID = PupilID;
                objResult.FullName = objPOC.PupilFullName;
                objResult.Status = objPOC.Status;
                if (EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    objRatedComment = lstRatedCommentPupil.Where(p => p.PupilID == PupilID).FirstOrDefault();
                    if (objRatedComment != null)
                    {
                        objResult.PeriodicMiddleMark = objRatedComment.PeriodicMiddleMark;
                        objResult.MiddleEvaluationName = objRatedComment.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objRatedComment.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        objResult.PeriodicEndingMark = objRatedComment.PeriodicEndingMark;
                        objResult.EndingEvaluationName = objRatedComment.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objRatedComment.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        objResult.PeriodicMiddleJudgement = objRatedComment.PeriodicMiddleJudgement;
                        objResult.PeriodicEndingJudgement = objRatedComment.PeriodicEndingJudgement;
                        objResult.CreateTime = objRatedComment.CreateTime;
                        objResult.UpdateTime = objRatedComment.UpdateTime;
                        objResult.EvaluationID = objRatedComment.EvaluationID;
                        objResult.FullNameComment = objRatedComment.FullNameComment;
                        objResult.LogChangeID = objRatedComment.LogChangeID;
                        objResult.Comment = objRatedComment.Comment;
                    }
                }
                else
                {
                    lsttmpBO = lstRatedCommentPupil.Where(p => p.PupilID == PupilID).ToList();
                    objRatedtmp = lsttmpBO.FirstOrDefault();
                    List<EvaluationCriteriaModel> lstEvaluationCriteriaModel = new List<EvaluationCriteriaModel>();
                    EvaluationCriteriaModel objCriteriaModel = null;
                    for (int j = 0; j < lsttmpBO.Count; j++)
                    {
                        objRatedComment = lsttmpBO[j];
                        objCriteriaModel = new EvaluationCriteriaModel();
                        objCriteriaModel.EvaluationCriteriaID = objRatedComment.SubjectID;
                        if (objRatedComment != null)
                        {
                            if (objRatedComment.EvaluationID == GlobalConstants.EVALUATION_CAPACITY)
                            {
                                objCriteriaModel.GK = objRatedComment.CapacityMiddleEvaluation;
                                objCriteriaModel.GKName = objRatedComment.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                                objCriteriaModel.CK = objRatedComment.CapacityEndingEvaluation;
                                objCriteriaModel.CKName = objRatedComment.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";

                            }
                            else
                            {
                                objCriteriaModel.GK = objRatedComment.QualityMiddleEvaluation;
                                objCriteriaModel.GKName = objRatedComment.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                                objCriteriaModel.CK = objRatedComment.QualityEndingEvaluation;
                                objCriteriaModel.CKName = objRatedComment.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            }
                            objCriteriaModel.CreateTime = objRatedComment.CreateTime;
                            objCriteriaModel.UpdateTime = objRatedComment.UpdateTime;
                            objCriteriaModel.EvaluationID = objRatedComment.EvaluationID;
                            objCriteriaModel.LogChangeID = objRatedComment.LogChangeID;
                            lstEvaluationCriteriaModel.Add(objCriteriaModel);
                        }
                    }
                    objResult.Comment = objRatedtmp != null ? objRatedtmp.Comment : "";
                    objResult.lstEvalutionCriteria = lstEvaluationCriteriaModel;
                }
                lstResult.Add(objResult);
            }

            List<int> lstEmployeeID = new List<int>();
            RatedCommentPupilBO lastUpdate = new RatedCommentPupilBO();
            Employee lastUser = null;
            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            List<int> lstLastUpdatePupilID = new List<int>();
            string lastUpdateTime = string.Empty;
            List<Employee> lstEmployeeChange = new List<Employee>();
            lstEmployeeID = lstRatedCommentPupil.Where(p => p.LogChangeID > 0).Select(p => p.LogChangeID).Distinct().ToList();
            if (EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
            {
                lastUpdate = lstRatedCommentPupil.Where(p => p.PeriodicMiddleMark > 0 || p.MiddleEvaluation > 0 || !string.IsNullOrEmpty(p.PeriodicMiddleJudgement)
                                        || p.PeriodicEndingMark > 0 || p.EndingEvaluation > 0 || !string.IsNullOrEmpty(p.PeriodicEndingJudgement) || !string.IsNullOrEmpty(p.Comment)).OrderByDescending(o => o.UpdateTime).FirstOrDefault();
            }
            else
            {
                lastUpdate = lstRatedCommentPupil.Where(p => p.CapacityMiddleEvaluation.HasValue || p.CapacityEndingEvaluation.HasValue
                                        || p.QualityMiddleEvaluation.HasValue || p.QualityEndingEvaluation.HasValue || !string.IsNullOrEmpty(p.Comment)).OrderByDescending(o => o.UpdateTime).FirstOrDefault();
            }

            if (lstEmployeeID.Count > 0)
            {
                lstEmployeeChange = (from e in EmployeeBusiness.All
                                     where e.SchoolID == _globalInfo.SchoolID
                                     && lstEmployeeID.Contains(e.EmployeeID)
                                     select e).ToList();
            }
            if (lastUpdate != null)
            {
                string lastUserName = String.Empty;
                if (lastUpdate.LogChangeID == 0)
                {
                    lastUserName = "Quản trị trường";
                }
                else
                {
                    lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdate.LogChangeID).FirstOrDefault();
                    if (lastUser != null)
                    {
                        lastUserName = lastUser.FullName;
                    }
                }
                if (lastUpdate.UpdateTime.HasValue)
                {
                    tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                            lastUpdate.UpdateTime.Value.Hour.ToString(),
                            lastUpdate.UpdateTime.Value.Minute.ToString("D2"),
                            lastUpdate.UpdateTime,
                            !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                            lastUserName});
                }
                else
                {
                    tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                            lastUpdate.CreateTime.Hour.ToString(),
                            lastUpdate.CreateTime.Minute.ToString("D2"),
                            lastUpdate.CreateTime,
                            !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                            lastUserName});
                }
                strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);

                lastUpdateTime = lastUpdate.UpdateTime != null ? lastUpdate.UpdateTime.Value.ToString("dd/MM/yyyy hh:mm") : lastUpdate.CreateTime.ToString("dd/MM/yyyy hh:mm");
                if (EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    lstLastUpdatePupilID = lstRatedCommentPupil.Where(p => (p.UpdateTime.HasValue ? p.UpdateTime.Value.ToString("dd/MM/yyyy hh:mm") == lastUpdateTime : p.CreateTime.ToString("dd/MM/yyyy hh:mm") == lastUpdateTime)
                                        && (p.PeriodicMiddleMark > 0 || p.MiddleEvaluation > 0 || !string.IsNullOrEmpty(p.PeriodicMiddleJudgement)
                                            || p.PeriodicEndingMark > 0 || p.EndingEvaluation > 0 || !string.IsNullOrEmpty(p.PeriodicEndingJudgement) || !string.IsNullOrEmpty(p.Comment)))
                                        .Select(p => p.PupilID).Distinct().ToList();
                }
                else
                {
                    lstLastUpdatePupilID = lstRatedCommentPupil.Where(p => (p.UpdateTime.HasValue ? p.UpdateTime.Value.ToString("dd/MM/yyyy hh:mm") == lastUpdateTime : p.CreateTime.ToString("dd/MM/yyyy hh:mm") == lastUpdateTime)
                                        && (p.CapacityMiddleEvaluation.HasValue || p.CapacityEndingEvaluation.HasValue || p.QualityMiddleEvaluation.HasValue || p.QualityEndingEvaluation.HasValue || !string.IsNullOrEmpty(p.Comment)))
                                        .Select(p => p.PupilID).Distinct().ToList();
                }

            }
            ViewData[RatedCommentPupilConstants.LAST_UPDATE_PERIOD_MARK] = lstLastUpdatePupilID;
            ViewData[RatedCommentPupilConstants.LAST_UPDATE_STRING] = strLastUpdate;
            ViewData[RatedCommentPupilConstants.LIST_PUPIL_EVALUATION] = lstResult;
        }
        private List<EvaluationGridModel> GetListRatedCommentBO(List<PupilOfClassBO> lstPOC, IDictionary<string, object> dic, AcademicYear objAy)
        {
            List<RatedCommentPupilBO> lstRatedBO = new List<RatedCommentPupilBO>();
            List<EvaluationGridModel> lstResult = new List<EvaluationGridModel>();
            EvaluationGridModel objResult = null;
            int EvaluationID = SMAS.Business.Common.Utils.GetInt(dic, "TypeID");
            List<int> lstSubjectID = SMAS.Business.Common.Utils.GetIntList(dic, "lstSubjectID");
            if (EvaluationID == GlobalConstants.TYPE_EVALUATION_CAPACITYANDQUALITY)
            {
                dic.Remove("TypeID");
            }
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedBO = (from p in RatedCommentPupilHistoryBusiness.Search(dic)
                              where ((EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP && p.EvaluationID == EvaluationID)
                                     || (p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY))
                              select new RatedCommentPupilBO()
                              {
                                  PupilID = p.PupilID,
                                  ClassID = p.ClassID,
                                  SubjectID = p.SubjectID,
                                  EvaluationID = p.EvaluationID,
                                  SemesterID = p.SemesterID,
                                  PeriodicMiddleMark = p.PeriodicMiddleMark,
                                  PeriodicMiddleJudgement = p.PeriodicMiddleJudgement,
                                  PeriodicEndingMark = p.PeriodicEndingMark,
                                  PeriodicEndingJudgement = p.PeriodicEndingJudgement,
                                  MiddleEvaluation = p.MiddleEvaluation,
                                  EndingEvaluation = p.EndingEvaluation,
                                  CapacityMiddleEvaluation = p.CapacityMiddleEvaluation,
                                  CapacityEndingEvaluation = p.CapacityEndingEvaluation,
                                  QualityMiddleEvaluation = p.QualityMiddleEvaluation,
                                  QualityEndingEvaluation = p.QualityEndingEvaluation,
                                  CreateTime = p.CreateTime,
                                  UpdateTime = p.UpdateTime,
                                  Comment = p.Comment
                              }).ToList();
            }
            else
            {
                lstRatedBO = (from p in RatedCommentPupilBusiness.Search(dic)
                              where ((EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP && p.EvaluationID == EvaluationID)
                                     || (p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY))
                              select new RatedCommentPupilBO()
                              {
                                  PupilID = p.PupilID,
                                  ClassID = p.ClassID,
                                  SubjectID = p.SubjectID,
                                  EvaluationID = p.EvaluationID,
                                  SemesterID = p.SemesterID,
                                  PeriodicMiddleMark = p.PeriodicMiddleMark,
                                  PeriodicMiddleJudgement = p.PeriodicMiddleJudgement,
                                  PeriodicEndingMark = p.PeriodicEndingMark,
                                  PeriodicEndingJudgement = p.PeriodicEndingJudgement,
                                  MiddleEvaluation = p.MiddleEvaluation,
                                  EndingEvaluation = p.EndingEvaluation,
                                  CapacityMiddleEvaluation = p.CapacityMiddleEvaluation,
                                  CapacityEndingEvaluation = p.CapacityEndingEvaluation,
                                  QualityMiddleEvaluation = p.QualityMiddleEvaluation,
                                  QualityEndingEvaluation = p.QualityEndingEvaluation,
                                  CreateTime = p.CreateTime,
                                  UpdateTime = p.UpdateTime,
                                  Comment = p.Comment
                              }).ToList();
            }
            RatedCommentPupilBO objRatedComment = null;
            RatedCommentPupilBO objRatedtmp = null;
            List<RatedCommentPupilBO> lsttmpBO = new List<RatedCommentPupilBO>();
            PupilOfClassBO objPOC = null;
            int PupilID = 0;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                PupilID = objPOC.PupilID;
                if (EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    for (int j = 0; j < lstSubjectID.Count; j++)
                    {
                        objResult = new EvaluationGridModel();
                        objResult.PupilID = PupilID;
                        objResult.FullName = objPOC.PupilFullName;
                        objResult.Status = objPOC.Status;
                        objResult.ClassID = objPOC.ClassID;
                        objRatedComment = lstRatedBO.Where(p => p.PupilID == PupilID && p.SubjectID == lstSubjectID[j]).FirstOrDefault();
                        if (objRatedComment != null)
                        {
                            objResult.PeriodicMiddleMark = objRatedComment.PeriodicMiddleMark;
                            objResult.MiddleEvaluationName = objRatedComment.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objRatedComment.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            objResult.PeriodicEndingMark = objRatedComment.PeriodicEndingMark;
                            objResult.EndingEvaluationName = objRatedComment.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objRatedComment.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            objResult.PeriodicMiddleJudgement = objRatedComment.PeriodicMiddleJudgement;
                            objResult.PeriodicEndingJudgement = objRatedComment.PeriodicEndingJudgement;
                            objResult.CreateTime = objRatedComment.CreateTime;
                            objResult.UpdateTime = objRatedComment.UpdateTime;
                            objResult.EvaluationID = objRatedComment.EvaluationID;
                            objResult.SubjectID = objRatedComment.SubjectID;
                            objResult.FullNameComment = objRatedComment.FullNameComment;
                            objResult.LogChangeID = objRatedComment.LogChangeID;
                            objResult.Comment = objRatedComment.Comment;
                        }
                        lstResult.Add(objResult);
                    }
                }
                else
                {
                    objResult = new EvaluationGridModel();
                    objResult.PupilID = PupilID;
                    objResult.FullName = objPOC.PupilFullName;
                    objResult.Status = objPOC.Status;
                    objResult.ClassID = objPOC.ClassID;

                    lsttmpBO = lstRatedBO.Where(p => p.PupilID == PupilID).OrderBy(p => p.SubjectID).ToList();
                    objRatedtmp = lsttmpBO.FirstOrDefault();
                    List<EvaluationCriteriaModel> lstEvaluationCriteriaModel = new List<EvaluationCriteriaModel>();
                    EvaluationCriteriaModel objCriteriaModel = null;
                    for (int j = 0; j < lsttmpBO.Count; j++)
                    {
                        objRatedComment = lsttmpBO[j];
                        objCriteriaModel = new EvaluationCriteriaModel();
                        objCriteriaModel.EvaluationCriteriaID = objRatedComment.SubjectID;
                        if (objRatedComment.EvaluationID == GlobalConstants.EVALUATION_CAPACITY)
                        {
                            objCriteriaModel.GK = objRatedComment.CapacityMiddleEvaluation;
                            objCriteriaModel.GKName = objRatedComment.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            objCriteriaModel.CK = objRatedComment.CapacityEndingEvaluation;
                            objCriteriaModel.CKName = objRatedComment.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            objCriteriaModel.TypeID = GlobalConstants.EVALUATION_CAPACITY;

                        }
                        else
                        {
                            objCriteriaModel.GK = objRatedComment.QualityMiddleEvaluation;
                            objCriteriaModel.GKName = objRatedComment.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            objCriteriaModel.CK = objRatedComment.QualityEndingEvaluation;
                            objCriteriaModel.CKName = objRatedComment.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            objCriteriaModel.TypeID = GlobalConstants.EVALUATION_QUALITY;
                        }
                        objCriteriaModel.CreateTime = objRatedComment.CreateTime;
                        objCriteriaModel.UpdateTime = objRatedComment.UpdateTime;
                        objCriteriaModel.EvaluationID = objRatedComment.EvaluationID;
                        objCriteriaModel.LogChangeID = objRatedComment.LogChangeID;
                        objCriteriaModel.Comment = objRatedComment.Comment;
                        lstEvaluationCriteriaModel.Add(objCriteriaModel);
                    }
                    objResult.lstEvalutionCriteria = lstEvaluationCriteriaModel;

                    lstResult.Add(objResult);
                }

            }
            return lstResult;
        }
        private void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                    semester1 = true;
                else semester2 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[RatedCommentPupilConstants.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[RatedCommentPupilConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[RatedCommentPupilConstants.LIST_CLASS] = listClass;
                var listCP = listClass.ToList();
                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            // Ưu tiên trước đối với giáo viên bộ môn
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                            List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                            {
                                cp = listSubjectTeacher.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                        .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listHead != null && listHead.Count > 0)
                                {
                                    cp = listHead.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            //lay list mon hoc
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (cp != null)
            {
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && cp.ClassProfileID > 0)
                {
                    lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, _globalInfo.Semester.Value, cp.ClassProfileID, _globalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                        //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .ToList();
                }
            }
            else
            {
                ViewData[RatedCommentPupilConstants.CLASS_NULL] = true;
            }

            ViewData[RatedCommentPupilConstants.SubjectID] = lsClassSubject.Select(c => c.SubjectID).FirstOrDefault();
        }
        private int CheckSubjectCommenting(int subjectID, int ClassID)
        {
            int isCommenting = -1;
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            if (subjectID > 0 && ClassID > 0)
            {
                ClassSubject obj = ClassSubjectBusiness.All.Where(p => p.ClassID == ClassID && p.SubjectID == subjectID && p.Last2digitNumberSchool == partitionId).FirstOrDefault();
                if (obj != null)
                {
                    return obj.IsCommenting.HasValue ? obj.IsCommenting.Value : -1;
                }
            }
            return isCommenting;
        }
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        private void SetVisbileButton(int semester, int classID, int subjectID, int TypeID)
        {
            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGV = TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP ? UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, classID, subjectID)
                           : UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (!(isAdminShcool || isGV))
            {
                ViewData[RatedCommentPupilConstants.IS_VISIBLED_BUTTON] = false;
            }
            else if (isAdminShcool)
            {
                ViewData[RatedCommentPupilConstants.IS_VISIBLED_BUTTON] = true;
            }
            else if (isGV)
            {
                if (semester == 1)
                {
                    if (dateTimeNow < objAca.FirstSemesterStartDate.Value || dateTimeNow > objAca.FirstSemesterEndDate)
                    {
                        ViewData[RatedCommentPupilConstants.IS_VISIBLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[RatedCommentPupilConstants.IS_VISIBLED_BUTTON] = true;
                    }
                }
                else if (semester == 2)
                {
                    if (dateTimeNow < objAca.SecondSemesterStartDate.Value || dateTimeNow > objAca.SecondSemesterEndDate.Value)
                    {
                        ViewData[RatedCommentPupilConstants.IS_VISIBLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[RatedCommentPupilConstants.IS_VISIBLED_BUTTON] = true;
                    }
                }
            }
        }
        #endregion
        public const int SubjectMark = 1;
        public const int SubjectJudge = 2;
        [HttpPost]
        public int GetTypeSubject(int? SubjectID)
        {
            int typeSubject = 0;
            //const 1 mon tinh diem va mon tinh diem ket hop nhan xet; 2 mon nhan xet

            if (Session["ListSubject"] != null)
            {
                List<SubjectCatBO> lsClassSubject = Session["ListSubject"] as List<SubjectCatBO>;

                var lclassSubject = lsClassSubject.Where(o => o.SubjectCatID == SubjectID.Value);

                if (lclassSubject != null && lclassSubject.Count() > 0)
                {
                    SubjectCatBO classSubject = lclassSubject.FirstOrDefault();

                    if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        return SubjectMark;
                    }
                    else
                    {
                        return SubjectJudge;
                    }
                }
            }
            return typeSubject;
        }
    }
}