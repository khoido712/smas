﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.RatedCommentPupilArea
{
    public class RatedCommentPupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RatedCommentPupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RatedCommentPupilArea_default",
                "RatedCommentPupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
