﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.CommuneArea
{
    public class CommuneAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CommuneArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CommuneArea_default",
                "CommuneArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
