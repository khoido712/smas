﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.CommuneArea.Models;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.CommuneArea.Controllers
{
    public class CommuneController : BaseController
    {
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;

        public CommuneController(ICommuneBusiness communeBusiness, IProvinceBusiness provinceBusiness, IDistrictBusiness districtBusiness)
        {
            this.CommuneBusiness = communeBusiness;
            this.ProvinceBusiness = provinceBusiness;
            this.DistrictBusiness = districtBusiness;
        }

		
		//
        // GET: /Commune/

        public ActionResult Index()
        {
            SetViewDataPermission("Commune", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            //Dua du lieu ra combobox Tinh thanh pho
            GetProvinceList();
            //Dua du lieu ra grid phuong xa
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            Paginate<CommuneViewModel> lst = this._Search(SearchInfo, 1, "");
            ViewData[CommuneConstants.LIST_COMMUNE] = lst;
            //Dua du lieu ra combobox quan huyen bang rong
            List<District> lstDistrict = new List<District>();
            ViewData[CommuneConstants.LIST_DISTRICT] = new SelectList(new string[] { });
            ViewData[CommuneConstants.LIST_DISTRICT_CREATE] = new SelectList(new string[] {});
            return View();

        }

		//
        // GET: /Commune/Search

        public PartialViewResult Search(SearchViewModel frm, int page = 1, string orderBy = "")
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["DistrictID"] = frm.DistrictID2;
            SearchInfo["CommuneName"] = frm.CommuneName;
            SearchInfo["ProvinceID"] = frm.ProvinceID2;
            SearchInfo["CommuneCode"] = frm.CommuneCode;

            Paginate<CommuneViewModel> lst = this._Search(SearchInfo, page, orderBy);
            ViewData[CommuneConstants.LIST_COMMUNE] = lst;
            
            //GetProvinceList();

            return PartialView("_List");

        }
        public PartialViewResult _Create()
        {
            GetProvinceList();
            ViewData[CommuneConstants.LIST_DISTRICT_CREATE] = new SelectList(new string[] { });
            return PartialView("_Create");
        }

		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Commune", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Commune commune = new Commune();
            TryUpdateModel(commune);
            commune.IsActive = true;
            Utils.Utils.TrimObject(commune);

            this.CommuneBusiness.Insert(commune);
            this.CommuneBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));            
        }

        //public ActionResult _Edit(int CommuneId)
        //{
        //    SetViewData();
        //    Employee ep = EmployeeBusiness.Find(EmployeeId);
        //    EmployeeObject epObj = new EmployeeObject();
        //    Utils.Utils.BindTo(ep, epObj, true);
        //    return PartialView("_Edit", epObj);
        //}
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int CommuneID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Commune", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Commune commune = this.CommuneBusiness.Find(CommuneID);                       
            TryUpdateModel(commune);
            Utils.Utils.TrimObject(commune);
            this.CommuneBusiness.Update(commune);
            this.CommuneBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Commune", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CommuneBusiness.Delete(id);
            this.CommuneBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
        [SkipCheckRole]
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridSelect(SearchViewModel frm, int page = 1, string orderBy = "")
        {
            GlobalInfo global = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["DistrictID"] = frm.DistrictID2;
            SearchInfo["CommuneName"] = frm.CommuneName;
            SearchInfo["ProvinceID"] = frm.ProvinceID2;
            SearchInfo["CommuneCode"] = frm.CommuneCode;

            Paginate<CommuneViewModel> paging = _Search(SearchInfo, page, orderBy);
            GridModel<CommuneViewModel> gm = new GridModel<CommuneViewModel>(paging.List);
            gm.Total = paging.total;

            return View(gm);
        }

		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private Paginate<CommuneViewModel> _Search(IDictionary<string, object> SearchInfo, int page = 1, string orderBy = "")
        {
            SetViewDataPermission("Commune", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<Commune> query = this.CommuneBusiness.Search(SearchInfo);
            IQueryable<CommuneViewModel> lst = query.Select(o => new CommuneViewModel
            {
						CommuneID = o.CommuneID,								
						DistrictID = o.DistrictID,
                        ProvinceID = o.District.Province.ProvinceID ,
						CommuneCode = o.CommuneCode,								
						CommuneName = o.CommuneName,								
						ShortName = o.ShortName,								
						Description = o.Description,								
						CreatedDate = o.CreatedDate,								
						IsActive = o.IsActive,								
						ModifiedDate = o.ModifiedDate,		
						ProvinceName = o.District.Province.ProvinceName,
					    DistrictName = o.District.DistrictName
				
            });
            lst = lst.OrderBy(c => c.CommuneCode).ThenBy(c => c.CommuneName);
            Paginate<CommuneViewModel> Paging = null;
            Paging = new Paginate<CommuneViewModel>(lst);
            Paging.page = page;
            Paging.size = Config.PageSize;
            Paging.paginate();
            return Paging;
        }
        private void GetProvinceList()
        {
            IDictionary<string, object> ProvinceSearchInfo = new Dictionary<string, object>();
            ProvinceSearchInfo["IsActive"] = true;
            List<Province> lstProvince = this.ProvinceBusiness.Search(ProvinceSearchInfo).OrderBy(o => o.ProvinceName).ToList();
            ViewData[CommuneConstants.LIST_PROVINCE] = lstProvince;
            ViewData[CommuneConstants.LIST_PROVINCE_CREATE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingDistrict(int? provinceId)
        {
            IEnumerable<District> lst = new List<District>();
            if (provinceId != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ProvinceID"] = provinceId;
                dic["IsActive"] = true;
                lst = this.DistrictBusiness.Search(dic);
            }
            if (lst == null)
                lst = new List<District>();
            return Json(new SelectList(lst, "DistrictID", "DistrictName"));
        }

        #region Detail

        [ValidateAntiForgeryToken]
        public PartialViewResult Detail(int id)
        {
            Commune commune = this.CommuneBusiness.Find(id);
            CommuneViewModel frm = new CommuneViewModel();
            Utils.Utils.BindTo(commune, frm);
            frm.ProvinceName = commune.District.Province.ProvinceName;
            frm.DistrictName = commune.District.DistrictName;

            return PartialView("_Detail", frm);
        }
        #endregion
    }
}





