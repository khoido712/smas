/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.CommuneArea.Models
{
    public class CommuneViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 CommuneID { get; set; }


        [ResourceDisplayName("Commune_Label_Province")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CommuneConstants.LIST_PROVINCE_CREATE)]
        [AdditionalMetadata("OnChange", "AjaxLoadDistrictCreate(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? ProvinceID { get; set; }								

        [ResourceDisplayName("Commune_Label_District")]
        [UIHint("Combobox")]     
        [AdditionalMetadata("ViewDataKey",CommuneConstants.LIST_DISTRICT_CREATE)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("PlaceHolder", 0)]
		public int? DistrictID { get; set; }

        [ResourceDisplayName("Commune_Label_CommuneCode")]      
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String CommuneCode { get; set; }

        [ResourceDisplayName("Commune_Label_CommuneName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String CommuneName { get; set; }

        [ResourceDisplayName("Commune_Label_ShortName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String ShortName { get; set; }

        [ResourceDisplayName("Commune_Label_Description")]        
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
		public System.String Description { get; set; }

        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> CreatedDate { get; set; }
        
        [ScaffoldColumn(false)]							
		public System.Nullable<System.Boolean> IsActive { get; set; }

        [ScaffoldColumn(false)]				
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Commune_Label_Province")]     
        public System.String ProvinceName { get; set; }

        [ResourceDisplayName("Commune_Label_District")] 
        [ScaffoldColumn(false)]
        public System.String DistrictName { get; set; }										
	       
    }
}


