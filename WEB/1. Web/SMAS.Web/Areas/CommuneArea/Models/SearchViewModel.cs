/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.CommuneArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Commune_Label_CommuneName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string CommuneName { get; set; }

        [ResourceDisplayName("Commune_Label_CommuneCode")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string CommuneCode { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Commune_Label_Province")]     
        public Nullable<int> ProvinceID2 { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Commune_Label_District")]
        public Nullable<int> DistrictID2 { get; set; }
    }
}
