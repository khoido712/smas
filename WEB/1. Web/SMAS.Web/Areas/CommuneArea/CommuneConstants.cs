/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.CommuneArea
{
    public class CommuneConstants
    {
        public const string LIST_COMMUNE = "listCommune";
        public const string LIST_PROVINCE = "listProvince";
        public const string LIST_DISTRICT = "listDistrict";
        public const string LIST_PROVINCE_CREATE = "listProvince";
        public const string LIST_DISTRICT_CREATE = "listDistrict";
    }
}