﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EthnicArea
{
    public class EthnicAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EthnicArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EthnicArea_default",
                "EthnicArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
