﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.EthnicArea.Models;
using SMAS.Web.Areas.EthnicArea;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.EthnicArea.Controllers
{
    public class EthnicController : BaseController
    {
        private readonly IEthnicBusiness EthnicBusiness;

        public EthnicController(IEthnicBusiness ethnicBusiness)
        {
            this.EthnicBusiness = ethnicBusiness;
        }

        //
        // GET: /Ethnic/


        #region SetViewData
        private void SetViewData()
        {

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IEnumerable<EthnicViewModel> lst = this._Search(SearchInfo);
            SearchInfo["IsActive"] = true;
            ViewData[EthnicConstants.LIST_ETHNIC] = lst;

            List<ComboObject> listIsMinority = new List<ComboObject>();
            listIsMinority.Add(new ComboObject("true", Res.Get("Ethnic_IsMinority_Value_True")));
            listIsMinority.Add(new ComboObject("false", Res.Get("Ethnic_IsMinority_Value_False")));
      

            ViewData[EthnicConstants.LIST_IsMinority] = new SelectList(listIsMinority, "key", "value");


        }

        #endregion

        #region Index
        public ActionResult Index()
        {
            SetViewDataPermission("Ethnic", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SetViewData();
            return View();
        }

        #endregion

        #region Search

        //
        // GET: /Ethnic/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["EthnicCode"] = frm.EthnicCode;
            SearchInfo["EthnicName"] = frm.EthnicName;
            SearchInfo["IsMinority"] = frm.IsMinority;
            SearchInfo["IsActive"] = true;

            IEnumerable<EthnicViewModel> lst = this._Search(SearchInfo);
            ViewData[EthnicConstants.LIST_ETHNIC] = lst;

            //Get view data here

            return PartialView("_List");
        }
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>

        private IEnumerable<EthnicViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("Ethnic", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<Ethnic> query = this.EthnicBusiness.Search(SearchInfo);
            IQueryable<EthnicViewModel> lst = query.Select(o => new EthnicViewModel
            {
                EthnicID = o.EthnicID,
                EthnicCode = o.EthnicCode,
                EthnicName = o.EthnicName,
                IsMinority = o.IsMinority,
                Description = o.Description,


            });

            List<EthnicViewModel> Lst1 = lst.ToList();
            foreach (var item in Lst1)
            {
                if (item.IsMinority != true)
                {
                    item.Minority = Res.Get("Ethnic_IsMinority_Value_False");

                }
                else
                {
                    item.Minority = Res.Get("Ethnic_IsMinority_Value_True");
                }
            }
            return Lst1.OrderBy(o=>o.EthnicName).ToList();
        }

        #endregion

        #region Create

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Ethnic", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Ethnic ethnic = new Ethnic();
            TryUpdateModel(ethnic);
            ethnic.IsActive = true;
            Utils.Utils.TrimObject(ethnic);

            this.EthnicBusiness.Insert(ethnic);
            this.EthnicBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        #endregion

        #region Detail

        public PartialViewResult Detail(int id)

        {
            Ethnic ethnic = this.EthnicBusiness.Find(id);
            EthnicViewModel frm = new EthnicViewModel();
            Utils .Utils.BindTo(ethnic,frm,true);
            if (frm.IsMinority == true)
            {
                frm.Minority = Res.Get("Ethnic_IsMinority_Value_True");
            }

            else
            {

                frm.Minority = Res.Get("Ethnic_IsMinority_Value_False");
            }
            return PartialView("_Detail",frm );
        }

        

        public ActionResult GetDetailEthnic(int EthnicID)
        {
            Ethnic ep = EthnicBusiness.Find(EthnicID);
            return PartialView("_Detail", ep);
        }
        #endregion

        #region Edit
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EthnicID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Ethnic", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Ethnic ethnic = this.EthnicBusiness.Find(EthnicID);
            TryUpdateModel(ethnic);
            Utils.Utils.TrimObject(ethnic);
            this.EthnicBusiness.Update(ethnic);
            this.EthnicBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }


        public ActionResult GetEditEthnic(int EthnicID)
        {
            SetViewData();
            Ethnic ep = EthnicBusiness.Find(EthnicID);
            EthnicViewModel epObj = new EthnicViewModel();
            Utils.Utils.BindTo(ep, epObj, true);
            return PartialView("_Edit", epObj);
        }


        #endregion

        #region Delete

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Ethnic", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.EthnicBusiness.Delete(id);
            this.EthnicBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }


        #endregion


    }
}





