/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.EthnicArea.Models
{
    public class SearchViewModel
    {


        [ResourceDisplayName("Ethnic_Label_Name")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string EthnicName { get; set; }

        [ResourceDisplayName("Ethnic_Label_Code")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string EthnicCode { get; set; }


        [ResourceDisplayName("Ethnic_Label_Minority")]
        public System.String Minority { get; set; }

        [ResourceDisplayName("Ethnic_Label_IsMinority")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", EthnicConstants.LIST_IsMinority)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public bool? IsMinority { get; set; }

    }
}