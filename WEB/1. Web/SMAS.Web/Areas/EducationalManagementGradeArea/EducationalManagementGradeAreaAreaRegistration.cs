﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EducationalManagementGradeArea
{
    public class EducationalManagementGradeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EducationalManagementGradeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EducationalManagementGradeArea_default",
                "EducationalManagementGradeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
