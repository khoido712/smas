/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.EducationalManagementGradeArea.Models
{
    public class EducationalManagementGradeViewModel
    {
        [ScaffoldColumn(false)]
		public int EducationalManagementGradeID { get; set; }

        [ResourceDisplayName("EducationalManagementGrade_Label_Resolusion")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]		
		public string Resolution { get; set; }

        [ResourceDisplayName("EducationalManagementGrade_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }								
    }
}


