﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.EducationalManagementGradeArea.Models;

namespace SMAS.Web.Areas.EducationalManagementGradeArea.Controllers
{
    // TODO : HieuND, Chua xu li phan phan quyen, bo phan SkipCheckRole
    public class EducationalManagementGradeController : BaseController
    {        
        private readonly IEducationalManagementGradeBusiness EducationalManagementGradeBusiness;

		public EducationalManagementGradeController (IEducationalManagementGradeBusiness educationalmanagementgradeBusiness)
		{
			this.EducationalManagementGradeBusiness = educationalmanagementgradeBusiness;
		}
		
		//
        // GET: /EducationalManagementGrade/
        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here
            SetViewDataPermission("EducationalManagementGrade", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IEnumerable<EducationalManagementGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[EducationalManagementGradeConstants.LIST_EDUCATIONALMANAGEMENTGRADE] = lst;
            return View();
        }

		//
        // GET: /EducationalManagementGrade/Search
        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;

            IEnumerable<EducationalManagementGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[EducationalManagementGradeConstants.LIST_EDUCATIONALMANAGEMENTGRADE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            EducationalManagementGrade educationalmanagementgrade = new EducationalManagementGrade();
            TryUpdateModel(educationalmanagementgrade); 
            Utils.Utils.TrimObject(educationalmanagementgrade);

            this.EducationalManagementGradeBusiness.Insert(educationalmanagementgrade);
            this.EducationalManagementGradeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EducationalManagementGradeID)
        {
            EducationalManagementGrade educationalmanagementgrade = this.EducationalManagementGradeBusiness.Find(EducationalManagementGradeID);
            TryUpdateModel(educationalmanagementgrade);
            Utils.Utils.TrimObject(educationalmanagementgrade);
            this.EducationalManagementGradeBusiness.Update(educationalmanagementgrade);
            this.EducationalManagementGradeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EducationalManagementGradeBusiness.Delete(id);
            this.EducationalManagementGradeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EducationalManagementGradeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<EducationalManagementGrade> query = this.EducationalManagementGradeBusiness.Search(SearchInfo);
            IQueryable<EducationalManagementGradeViewModel> lst = query.Select(o => new EducationalManagementGradeViewModel {               
						EducationalManagementGradeID = o.EducationalManagementGradeID,								
						Resolution = o.Resolution,								
						Description = o.Description 
            });
            return lst.ToList();
        }        
    }
}





