﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.VTUtils.Excel.ExportXML;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ReportVEmisArea.Controllers
{
    public class ReportVEmisController : BaseController
    {
        //
        // GET: /ReportVEmisArea/ReportVEmis/

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IVMarkRecordBusiness VMarkRecordBusiness;
        private readonly IVJudgeRecordBusiness VJudgeRecordBusiness;
        private readonly IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        public ReportVEmisController(ITranscriptOfPrimaryClassBusiness TranscriptOfPrimaryClassBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IMarkRecordBusiness MarkRecordBusiness,
            IJudgeRecordBusiness JudgeRecordBusiness,
            ITeachingAssignmentBusiness TeachingAssignmentBusiness,
            ISummedUpRecordBusiness SummedUpRecordBusiness,
            IVMarkRecordBusiness VMarkRecordBusiness,
            IVJudgeRecordBusiness VJudgeRecordBusiness,
            IVSummedUpRecordBusiness VSummedUpRecordBusiness
            )
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.VMarkRecordBusiness = VMarkRecordBusiness;
            this.VJudgeRecordBusiness = VJudgeRecordBusiness;
            this.VSummedUpRecordBusiness = VSummedUpRecordBusiness;
        }
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        public ActionResult _Index()
        {
            this.SetViewData();
            return PartialView();
        }
        private void SetViewData()
        {
            //lay thong tin cho form tim kiem

            //lay khoi
            ViewData[ReportVEmisConstants.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[ReportVEmisConstants.ClassCode] = 0;
            int ClassID = 0;
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (_globalInfo.EducationLevels.Count > 0)
            {

                int EducationLevelID = _globalInfo.EducationLevels.FirstOrDefault().EducationLevelID;
                if (EducationLevelID > 0)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                    dic["EducationLevelID"] = EducationLevelID;
                    if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager && !_globalInfo.IsRolePrincipal)
                    {
                        dic["UserAccountID"] = _globalInfo.UserAccountID;
                        dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                    }
                    lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
                }
                if (lstClass.Count() > 0)
                {
                    ClassProfile objClass = lstClass.FirstOrDefault();
                    ClassID = objClass.ClassProfileID;
                    ViewData[ReportVEmisConstants.ClassCode] = objClass.VemisCode;
                }
                else
                {
                    ViewData[ReportVEmisConstants.ClassCode] = "";
                }
            }
            //Lay Thong tin ma truong ma lop
            ViewData[ReportVEmisConstants.SchoolCode] = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value).SchoolCode;
            // Neu la nam hoc hien tai thi lay hoc ky tuong ung con khong phai thi se lay HK 2
            int semester = _globalInfo.IsCurrentYear ? _globalInfo.Semester.Value : SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            ViewData[ReportVEmisConstants.LIST_SEMESTER] = new SelectList(Utils.CommonList.Semester(), "key", "value", semester);
            //lay lop
            ViewData[ReportVEmisConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            //lay mon hoc theo lop
            if (ClassID != 0)
            {
                ViewData[ReportVEmisConstants.LIST_SUBJECT] = GetClassSubject(ClassID, _globalInfo.Semester.Value);
            }
            else
            {
                ViewData[ReportVEmisConstants.LIST_SUBJECT] = new List<ViettelCheckboxList>();
            }
        }
        /// <summary>
        /// Load lai lop
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public PartialViewResult GetRecordTab(int ClassID, int Semester)
        {
            ViewData[ReportVEmisConstants.LIST_SUBJECT] = GetClassSubject(ClassID, Semester);
            return PartialView("_SubjectList");
        }

        //lay du lieu mon hoc
        public List<ViettelCheckboxList> GetClassSubject(int ClassID, int Semester)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            //dic["ClassID"] =ClassID;
            dic["Semester"] = Semester;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["IsVNEN"] = true;
            List<ClassSubject> ListClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, dic).OrderBy(u => u.SubjectCat.OrderInSubject).ToList();
            List<ViettelCheckboxList> listCheckBox = new List<ViettelCheckboxList>();
            if (ListClassSubject != null && ListClassSubject.Count() > 0)
            {
                ListClassSubject.ForEach(u =>
                {
                    listCheckBox.Add(new ViettelCheckboxList(u.SubjectCat.DisplayName, u.SubjectID, true, false));
                });
            }
            //listCheckBox = listCheckBox.OrderBy(u => u.Label).ToList();
            return listCheckBox;
        }

        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public String LoadVemisClassCode(int ClassID)
        {
            string code = "";
            var class_Profile = ClassProfileBusiness.Find(ClassID);
            if (class_Profile != null)
            {
                return "";
            }
            return code;
        }

        //load danh sach lop
        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            IEnumerable<ClassProfile> lst = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager && !_globalInfo.IsRolePrincipal)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                }
                lst = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        public FileResult ExportRecordReport(FormCollection form)
        {
            DateTime startTime = DateTime.Now;

            ///
            //Cac bien o TieuChiTimKiem
            #region Thong Tin Dau vao
            int EducationLevelID = Business.Common.Utils.GetInt(form["EducationLevelID"]);
            int ClassID = Business.Common.Utils.GetInt(form["ClassID"]);
            string ClassCode = form["ClassCode"];
            string SchoolCode = form["SchoolCode"];
            int Semester = Business.Common.Utils.GetInt(form["Semester"]);
            string ListStrSubjectID = form["ListSubjectID"];
            string[] lstSubjectChoice = ListStrSubjectID.Split(',');
            int AppliedLevel = _globalInfo.AppliedLevel.Value;
            #endregion
            //lay các thông tin trên form

            if (ClassCode.Length <= 0)
            {
                throw new BusinessException("Chưa nhập thông tin mã lớp");
            }
            if (SchoolCode.Length <= 0)
            {
                throw new BusinessException("Chưa nhập thông tin mã trường");
            }
            if (ClassID <= 0)
            {
                throw new BusinessException("Chưa chọn lớp học");
            }

            //lay danh sach mon hoc that

            Dictionary<string, object> dic = new Dictionary<string, object>();
            //dic["ClassID"] =ClassID;
            dic["Semester"] = Semester;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            List<ClassSubjectBO> listSubjectAll = ClassSubjectBusiness.SearchByClass(ClassID, dic)
            .Select(o => new ClassSubjectBO
            {
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
                SubjectName = o.SubjectCat.DisplayName,
                DisplayName = o.SubjectCat.DisplayName,
                SubjectCode = o.SubjectCat.Code,
                IsCommenting = o.IsCommenting,
                OrderInSubject = o.SubjectCat.OrderInSubject
            })
            .OrderBy(u => u.OrderInSubject).ThenBy(o => o.SubjectName).ToList();

            AcademicYear ac = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);

            ClassProfile cl = ClassProfileBusiness.Find(ClassID);

            IDictionary<string, object> dicS = new Dictionary<string, object>();
            dicS["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;
            dicS["CurrentClassID"] = ClassID;

            //Tim Kiem Thong Tin Hoc Sinh trong lop
            List<PupilProfileBO> ListPupil = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicS)
                .Where(o => o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                .ToList()
                .OrderBy(o => o.OrderInClass)
                .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name))
                .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName)).ToList();

            List<int> ListPupilID = ListPupil.Select(o => o.PupilProfileID).ToList();
            int cntPupil = ListPupil.Count;
            //Kiem tra tinh dung dan cua mon hoc truyen len
            List<ClassSubjectBO> listCS = new List<ClassSubjectBO>();
            List<int> listSubjectID = new List<int>();
            foreach (var sb in listSubjectAll)
            {
                if (lstSubjectChoice.ToList().Any(a => sb.SubjectID.ToString().Equals(a.Trim())))
                {
                    listCS.Add(sb);
                    listSubjectID.Add(sb.SubjectID);
                }
            }
            listSubjectAll = listCS;

            if (listSubjectAll.Count <= 0)
            {
                throw new BusinessException("Quý thầy cô chưa chọn môn học!");
            }

            //Lay thong tin phan cong giang day
            List<TeachingAssignmentBO> listTeacherForSubject = TeachingAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
            {
                {"ClassID",ClassID},
                {"Semester",Semester},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            }).Where(u => listSubjectID.Contains(u.SubjectID))
            .Select(o => new TeachingAssignmentBO
            {
                ClassID = o.ClassID,
                TeacherID = o.TeacherID,
                SubjectID = o.SubjectID,
                TeacherName = o.Employee.FullName,
            }).ToList();

            //neu la cap 2,3
            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY ||
                AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                #region Neu la cap 23
                string fileNameTemplate = "";
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    fileNameTemplate = ReportVEmisConstants.BangDiemCap23HK1;
                }
                else
                {
                    fileNameTemplate = ReportVEmisConstants.BangDiemCap23HK2;
                }

                string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "VEMIS", fileNameTemplate);
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet sheet = oBook.GetSheet(1);

                IVTRange rangeCopy = sheet.GetRange("A8", "AF8");
                IVTRange rangeEndCopy = sheet.GetRange("A3", "AF3");
                List<IVTWorksheet> listSheet = new List<IVTWorksheet>();

                //danh sach cac sheet
                listSheet.Add(sheet);
                //copy so sheet cho so lop
                for (int numSubject = 1; numSubject < listSubjectAll.Count(); numSubject++)
                {
                    //copy sheet
                    oBook.CopySheetToLast(sheet);
                    IVTWorksheet sheetNew = oBook.GetSheet(numSubject + 1);
                    //dua vao danh sach
                    listSheet.Add(sheetNew);
                }

                int odSubject = 0;
                //doc tung sheet da copy
                foreach (IVTWorksheet sheetH in listSheet)
                {
                    //get mon hoc tuong ung
                    var Subject = listSubjectAll[odSubject];
                    String SubjectName = Subject.DisplayName;

                    IDictionary<string, object> kingDic = new Dictionary<string, object>();
                    #region dieu kien truyen vao cho tim kiem diem
                    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                    SearchInfo["SchoolID"] = _globalInfo.SchoolID;
                    SearchInfo["SubjectID"] = Subject.SubjectID;
                    SearchInfo["Semester"] = Semester;
                    SearchInfo["ClassID"] = ClassID;
                    #endregion

                    List<object> QueenDic = new List<object>();

                    string TypeSubject = "";

                    int startRow = 7;

                    //neu la mon tinh diem
                    if (Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        #region Xu ly cho mon tinh diem
                        TypeSubject = "1";
                        //Lay danh sach diem cua mon hoc
                        var listMark = this.MarkRecordBusiness.GetMardRecordOfClassNew(SearchInfo);
                        List<MarkRecordNewBO> listMarkRecord = listMark != null && listMark.Count() > 0 ? listMark.OrderBy(dt => dt.ORDER_IN_CLASS).ThenBy(dt => dt.S_NAME).ThenBy(dt => dt.FULL_NAME).ToList() : new List<MarkRecordNewBO>();
                        //chay tung hoc sinh de ghep vao dictionary
                        int Stt = 0;
                        foreach (MarkRecordNewBO MarkRecord in listMarkRecord)
                        {
                            Dictionary<string, object> PupilDic = new Dictionary<string, object>();
                            var pupilInfo = ListPupil.Where(u => u.PupilProfileID == MarkRecord.PUPIL_ID).FirstOrDefault();
                            //chi hien thi hoc sinh co trang thai la dang hoc va da tot nghiep
                            if (pupilInfo == null)
                                continue;
                            if (pupilInfo != null && pupilInfo.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_GRADUATED && pupilInfo.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                continue;
                            }
                            Stt++;
                            PupilDic.Add("STT", Stt);
                            PupilDic.Add("HoTen", MarkRecord.FULL_NAME);
                            PupilDic.Add("NgaySinh", pupilInfo.BirthDate.ToString("dd/MM/yyyy"));
                            PupilDic.Add("MaHocSinh", pupilInfo.PupilCode);
                            //cac thong tin diem

                            #region Nap diem cho tung dau diem
                            PupilDic.Add("M1", MarkRecord.M1);
                            PupilDic.Add("M2", MarkRecord.M2);
                            PupilDic.Add("M3", MarkRecord.M3);
                            PupilDic.Add("M4", MarkRecord.M4);
                            PupilDic.Add("M5", MarkRecord.M5);
                            PupilDic.Add("P1", MarkRecord.P1);
                            PupilDic.Add("P2", MarkRecord.P2);
                            PupilDic.Add("P3", MarkRecord.P3);
                            PupilDic.Add("P4", MarkRecord.P4);
                            PupilDic.Add("P5", MarkRecord.P5);
                            PupilDic.Add("V1", MarkRecord.V1);
                            PupilDic.Add("V2", MarkRecord.V2);
                            PupilDic.Add("V3", MarkRecord.V3);
                            PupilDic.Add("V4", MarkRecord.V4);
                            PupilDic.Add("V5", MarkRecord.V5);
                            PupilDic.Add("V6", MarkRecord.V6);
                            PupilDic.Add("V7", MarkRecord.V7);
                            PupilDic.Add("V8", MarkRecord.V8);
                            #endregion

                            //cac thong tin diem HK
                            PupilDic.Add("HK", MarkRecord.HK);

                            //trung binh mon neu la hoc ky 1
                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                PupilDic.Add("TB1", MarkRecord.HK1);
                            else
                            {
                                PupilDic.Add("TB2", MarkRecord.HK2);
                                PupilDic.Add("CN", MarkRecord.CN);
                            }
                            QueenDic.Add(PupilDic);
                            sheetH.CopyPaste(rangeCopy, startRow, 1, true);

                            //voi dong cuoi gach chan duoi
                            if (Stt == cntPupil)
                            {
                                sheetH.GetRange(startRow, 1, startRow, 32).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                            }

                            //tang dong de copy template
                            startRow++;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Xu ly cho mon nhan xet
                        TypeSubject = "0";
                        //Lay danh sach diem cua mon hoc
                        var listJudge = this.JudgeRecordBusiness.GetJudgeRecordOfClassNew(SearchInfo);
                        List<JudgeRecordNewBO> listMarkRecord = listJudge != null && listJudge.Count() > 0 ? listJudge.OrderBy(dt => dt.ORDER_IN_CLASS).ThenBy(dt => dt.S_NAME).ThenBy(dt => dt.FULL_NAME).ToList() : new List<JudgeRecordNewBO>();
                        //chay tung hoc sinh de ghep vao dictionary
                        int Stt = 0;
                        foreach (JudgeRecordNewBO JudgeRecord in listMarkRecord)
                        {
                            Dictionary<string, object> PupilDic = new Dictionary<string, object>();
                            var pupilInfo = ListPupil.Where(u => u.PupilProfileID == JudgeRecord.PUPIL_ID).FirstOrDefault();
                            if (pupilInfo == null)
                                continue;
                            if (pupilInfo.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_GRADUATED && pupilInfo.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                continue;
                            }
                            Stt++;
                            PupilDic.Add("STT", Stt);
                            PupilDic.Add("HoTen", JudgeRecord.FULL_NAME);
                            PupilDic.Add("NgaySinh", pupilInfo.BirthDate.ToString("dd/MM/yyyy"));
                            PupilDic.Add("MaHocSinh", pupilInfo.PupilCode);
                            //cac thong tin diem

                            #region Nap diem cho tung dau diem
                            PupilDic.Add("M1", JudgeRecord.M1);
                            PupilDic.Add("M2", JudgeRecord.M2);
                            PupilDic.Add("M3", JudgeRecord.M3);
                            PupilDic.Add("M4", JudgeRecord.M4);
                            PupilDic.Add("M5", JudgeRecord.M5);
                            PupilDic.Add("P1", JudgeRecord.P1);
                            PupilDic.Add("P2", JudgeRecord.P2);
                            PupilDic.Add("P3", JudgeRecord.P3);
                            PupilDic.Add("P4", JudgeRecord.P4);
                            PupilDic.Add("P5", JudgeRecord.P5);
                            PupilDic.Add("V1", JudgeRecord.V1);
                            PupilDic.Add("V2", JudgeRecord.V2);
                            PupilDic.Add("V3", JudgeRecord.V3);
                            PupilDic.Add("V4", JudgeRecord.V4);
                            PupilDic.Add("V5", JudgeRecord.V5);
                            PupilDic.Add("V6", JudgeRecord.V6);
                            PupilDic.Add("V7", JudgeRecord.V7);
                            PupilDic.Add("V8", JudgeRecord.V8);
                            #endregion

                            //cac thong tin diem HK
                            PupilDic.Add("HK", JudgeRecord.HK);

                            //trung binh mon neu la hoc ky 1
                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                PupilDic.Add("TB1", JudgeRecord.HK1);
                            else
                            {
                                PupilDic.Add("TB2", JudgeRecord.HK2);
                                PupilDic.Add("CN", JudgeRecord.CN);
                            }
                            QueenDic.Add(PupilDic);

                            sheetH.CopyPaste(rangeCopy, startRow, 1, true);

                            //voi dong cuoi gach chan duoi
                            if (Stt == cntPupil)
                            {
                                sheetH.GetRange(startRow, 1, startRow, 32).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                            }


                            //tang dong de copy template
                            startRow++;

                        }
                        #endregion
                    }


                    #region Dien thong tin vao file excel
                    Dictionary<string, object> KingDic = new Dictionary<string, object>();


                    string SubjectCode = Subject.SubjectCode != null ? Subject.SubjectCode.Trim() : string.Empty;
                    string SemesterStr = "0" + Semester.ToString();
                    string EmisCode = string.Format("{0}-{1}{2}-{3}-{4}-{5}", ClassCode, SubjectCode, TypeSubject, SemesterStr, ac.Year, cntPupil);

                    KingDic.Add("SchoolName", sp.SchoolName);
                    KingDic.Add("SchoolCode", SchoolCode);

                    string SemesterName = _globalInfo.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "Học kỳ 1" : "Học kỳ 2";


                    string AcademicYearName = ac.Year.ToString() + " - " + (ac.Year + 1).ToString();
                    KingDic.Add("Semester", SemesterName);
                    KingDic.Add("AcademicYear", AcademicYearName);

                    KingDic.Add("ClassCode", ClassCode);
                    KingDic.Add("SubjectName", SubjectName.ToUpper());
                    KingDic.Add("ClassName", cl.DisplayName);

                    KingDic.Add("CountNumber", cntPupil);
                    sheetH.SetCellValue("C4", EmisCode);
                    KingDic.Add("Rows", QueenDic);

                    //lay giao vien bo mon
                    var taList = listTeacherForSubject.Where(u => u.SubjectID == Subject.SubjectID);
                    string TeacherName = "";
                    if (taList != null && taList.Count() > 0)
                    {
                        TeacherName = taList.FirstOrDefault().TeacherName;
                    }

                    KingDic.Add("TeacherName", TeacherName);
                    #endregion

                    //fill thong tin
                    sheetH.FillVariableValue(KingDic);
                    sheetH.Name = SubjectName;
                    odSubject++;
                }

                var excel = oBook.ToStream(true);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                string SemesterFile = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII";

                result.FileDownloadName = "BangDiem" + cl.DisplayName + "_" + SemesterFile + "_" + ac.Year.ToString() + ".xls";

                return result;
                #endregion

            }
            else if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                //Neu la cap 1

                #region Neu la cap 1
                string fileNameTemplate = "";
                fileNameTemplate = ReportVEmisConstants.BangDiemCap1;

                string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "VEMIS", fileNameTemplate);
                //su dung thu vien IXVT
                IXVTWorkbook oBook = XVTExportExcel.OpenWorkbook(templatePath);
                IXVTWorksheet sheet = oBook.GetSheet(1);
                IXVTWorksheet sheetTV = oBook.GetSheet(2);
                List<IXVTWorksheet> sheetNx = new List<IXVTWorksheet>();
                sheetNx.Add(oBook.GetSheet(3));
                sheetNx.Add(oBook.GetSheet(4));
                sheetNx.Add(oBook.GetSheet(5));
                sheetNx.Add(oBook.GetSheet(6));
                sheetNx.Add(oBook.GetSheet(7));
                sheetNx.Add(oBook.GetSheet(8));
                sheetNx.Add(oBook.GetSheet(9));

                IXVTRange rangeCopy = sheet.GetRange("A8", "AT8");
                IXVTRange rangeEndCopy = sheet.GetRange("A3", "AT3");

                List<IXVTWorksheet> listSheet = new List<IXVTWorksheet>();
                //danh sach cac sheet
                //listSheet.Add(sheet);
                //copy so sheet cho so lop
                int indexJudge = 0;
                List<ClassSubjectBO> listClassSubject = new List<ClassSubjectBO>();
                int orderSubject = 0;
                foreach (var Subject in listSubjectAll)
                {
                    string SubjectName = Subject.SubjectName;

                    if (Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        //copy sheet
                        if (SubjectName.ToLower() != "tiếng việt")
                            sheet.CopyTo(SubjectName, orderSubject + 3);
                        else
                            sheetTV.CopyTo(SubjectName, orderSubject + 3);

                        IXVTWorksheet sheetNew = oBook.GetSheet(orderSubject + 3);
                        //dua vao danh sach
                        listSheet.Add(sheetNew);
                    }
                    else
                    {
                        //voi mon nhan xets
                        //copy sheet
                        //oBook.CopySheet(sheetNx,Subject.SubjectCat.DisplayName);
                        IXVTWorksheet sheetNew = oBook.GetSheet(orderSubject + 3);
                        sheetNew.SheetName = SubjectName;
                        indexJudge++;

                        //dua vao danh sach
                        listSheet.Add(sheetNew);
                    }
                    listClassSubject.Add(Subject);
                    orderSubject++;

                }
                int indexDelete = 0;
                //xoa cac sheet thua mon nhan xet
                foreach (IXVTWorksheet sheetJudge in sheetNx)
                {
                    if (indexDelete >= indexJudge)
                    {
                        sheetJudge.Delete();
                    }
                    indexDelete++;

                }

                //Xoa 2 sheet mac dinh ban dau
                sheet.Delete();
                sheetTV.Delete();

                int odSubject = 0;

                #region dieu kien truyen vao cho tim kiem diem
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                //SearchInfo["SchoolID"] = _globalInfo.SchoolID;
                //SearchInfo["SubjectID"] = Subject.SubjectID;
                //SearchInfo["Semester"] = Semester;
                SearchInfo["ClassID"] = ClassID;
                #endregion
                //Lay toan bo diem cho lop o tat ca cac mon hoc

                // Mon tinh diem
                IEnumerable<VMarkRecord> ListMarkRecord = VMarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();
                // Mon nhan xet
                IEnumerable<VJudgeRecord> ListJudgeRecord = VJudgeRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();

                List<VSummedUpRecord> lsSUR = VSummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                                                                            {
                                                                                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                                                                                {"ClassID",ClassID}
                                                                            }).Where(o => listSubjectID.Contains(o.SubjectID)).ToList();
                //Lay diem trung binh mon

                //doc tung sheet da copy
                foreach (IXVTWorksheet sheetH in listSheet)
                {
                    //get mon hoc tuong ung
                    var Subject = listClassSubject[odSubject];
                    String SubjectName = Subject.DisplayName;
                    bool isTVSubject = SubjectName.ToLower() == "tiếng việt";
                    IDictionary<string, object> kingDic = new Dictionary<string, object>();

                    List<object> QueenDic = new List<object>();

                    string TypeSubject = "";

                    int startRow = 6;

                    //neu la mon tinh diem
                    if (Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        #region Xu ly cho mon tinh diem
                        TypeSubject = "1";
                        //Lay danh sach diem cua mon hoc
                        var listMark = ListMarkRecord.Where(u => u.SubjectID == Subject.SubjectID);

                        List<VSummedUpRecord> listSummedUpRecord = lsSUR.Where(u => u.SubjectID == Subject.SubjectID && u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK).ToList();
                        //lay max semester mon cap 1
                        //List<SummedUpRecord> listSummedUpRecordSpec = listSummedUpRecord.Where(u => u.Semester == listSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();

                        //chay tung hoc sinh de ghep vao dictionary
                        int[] listMapMonth = new int[] { 9, 10, 11, 12, 1, 2, 3, 4, 5 };
                        int Stt = 0;
                        sheetH.CopyRow(7, 8, cntPupil + 6);
                        foreach (var PupilID in ListPupilID)
                        {
                            Dictionary<string, object> PupilDic = new Dictionary<string, object>();
                            var pupilInfo = ListPupil.Where(u => u.PupilProfileID == PupilID).FirstOrDefault();

                            //lay diem cho tung hoc sinh
                            var listMarkPupil = listMark.Where(u => u.PupilID == PupilID);
                            Stt++;
                            sheetH.SetCellValue(startRow, 1, Stt.ToString());
                            sheetH.SetCellValue(startRow, 2, pupilInfo.FullName);
                            sheetH.SetCellValue(startRow, 3, pupilInfo.BirthDate.ToString("dd/MM/yyyy"));
                            sheetH.SetCellValue(startRow, 4, pupilInfo.PupilCode);
                            //cac thong tin diem

                            #region Nap diem cho tung dau diem
                            var listMarkForMonth = listMarkPupil.Where(u => u.MarkedDate != null);
                            int MonthColStart = 4;
                            int NumberMonth = 9;
                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            {
                                NumberMonth = 5;
                            }
                            for (int month = 1; month <= NumberMonth; month++)
                            {

                                int MonthMap = listMapMonth[month - 1];
                                var listMarkMonth = listMarkForMonth.Where(u => u.MarkedDate.Value.Month == MonthMap).OrderBy(u => u.OrderNumber);
                                //moi con diem lay toi da 4 con diem
                                for (int order = 1; order <= 4; order++)
                                {
                                    MonthColStart++;
                                    string NameMonth = "T" + month.ToString() + order.ToString();

                                    if (listMarkMonth != null && listMarkMonth.Count() > 0 && order <= listMarkMonth.Count())
                                    {
                                        //lay diem tuong ung
                                        sheetH.SetCellValue(startRow, MonthColStart, listMarkMonth.ToList()[order - 1].Mark.ToString());
                                    }

                                }
                            }

                            //neu la hoc ky I

                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            {
                                if (isTVSubject)
                                {
                                    //Diem DGK1
                                    var MarkDGK1 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.DGK1).FirstOrDefault();
                                    if (MarkDGK1 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 41, MarkDGK1.Mark.ToString());
                                    }

                                    //Diem VGK1
                                    var MarkVGK1 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.VGK1).FirstOrDefault();
                                    if (MarkVGK1 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 42, MarkVGK1.Mark.ToString());
                                    }

                                    //Diem GK1
                                    var MarkGK1 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.GK1).FirstOrDefault();
                                    if (MarkGK1 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 43, MarkGK1.Mark.ToString());
                                    }


                                    //Diem DK1
                                    var MarkDCK1 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.DCK1).FirstOrDefault();
                                    if (MarkDCK1 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 44, MarkDCK1.Mark.ToString());
                                    }

                                    //Diem VK1
                                    var MarkVCK1 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.VCK1).FirstOrDefault();
                                    if (MarkVCK1 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 45, MarkVCK1.Mark.ToString());
                                    }

                                    //Diem CK1
                                    var MarkCK1 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.CK1).FirstOrDefault();
                                    if (MarkCK1 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 46, MarkCK1.Mark.ToString());
                                    }

                                }
                                else
                                {
                                    //Diem GK1
                                    var MarkGK1 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.GK1).FirstOrDefault();
                                    if (MarkGK1 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 41, MarkGK1.Mark.ToString());
                                    }
                                    //Diem CK1
                                    var MarkCK1 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.CK1).FirstOrDefault();
                                    if (MarkCK1 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 42, MarkCK1.Mark.ToString());
                                    }
                                }
                            }
                            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            {
                                PupilDic.Add("GK1", "");
                                PupilDic.Add("CK1", "");
                                if (isTVSubject)
                                {
                                    //Diem DGK2
                                    var MarkDGK2 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.DGK2).FirstOrDefault();
                                    if (MarkDGK2 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 47, MarkDGK2.Mark.ToString());
                                    }

                                    //Diem VGK2
                                    var MarkVGK2 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.VGK2).FirstOrDefault();
                                    if (MarkVGK2 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 48, MarkVGK2.Mark.ToString());
                                    }

                                    //Diem GK2
                                    var MarkGK2 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.GK2).FirstOrDefault();
                                    if (MarkGK2 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 49, MarkGK2.Mark.ToString());
                                    }

                                    //Diem DCN2
                                    var MarkDCN2 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.DCN).FirstOrDefault();
                                    if (MarkDCN2 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 50, MarkDCN2.Mark.ToString());
                                    }

                                    //Diem VCN2
                                    var MarkVCN2 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.VCN).FirstOrDefault();
                                    if (MarkVCN2 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 51, MarkVCN2.Mark.ToString());
                                    }

                                    //Diem CK2
                                    var MarkCN = listMarkPupil.Where(u => u.Title == SystemParamsInFile.CN).FirstOrDefault();
                                    if (MarkCN != null)
                                    {
                                        sheetH.SetCellValue(startRow, 52, MarkCN.Mark.ToString());
                                    }
                                }
                                else
                                {
                                    //Diem GK2
                                    var MarkGK2 = listMarkPupil.Where(u => u.Title == SystemParamsInFile.GK2).FirstOrDefault();
                                    if (MarkGK2 != null)
                                    {
                                        sheetH.SetCellValue(startRow, 43, MarkGK2.Mark.ToString());
                                    }
                                    //Diem CK2
                                    var MarkCN = listMarkPupil.Where(u => u.Title == SystemParamsInFile.CN).FirstOrDefault();
                                    if (MarkCN != null)
                                    {
                                        sheetH.SetCellValue(startRow, 44, MarkCN.Mark.ToString());
                                    }
                                }
                            }
                            #endregion

                            //trung binh mon neu la hoc ky 1
                            VSummedUpRecord surBO = new VSummedUpRecord();
                            var surBO_HKI = new VSummedUpRecord();
                            var surList = listSummedUpRecord.Where(u => u.PupilID == PupilID);
                            //if (surList.Any(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST) && !surList.Any(u => u.Semester == 2))
                            //{
                            //    surBO = surList.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST).FirstOrDefault();
                            //}
                            //else
                            //{
                            //    surBO = surList.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            //}

                            //if (surBO != null && surBO.SummedUpMark != null && !isTVSubject)
                            //{
                            //    sheetH.SetCellValue(startRow, 45, this.GetTypeCapacityPrimary(surBO.SummedUpMark.Value));
                            //}
                            //else if (surBO != null && surBO.SummedUpMark != null && isTVSubject)
                            //{
                            //    sheetH.SetCellValue(startRow, 53, this.GetTypeCapacityPrimary(surBO.SummedUpMark.Value));
                            //}
                            surBO_HKI = surList.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            if (surBO_HKI != null && surBO_HKI.SummedUpMark != null && !isTVSubject)
                            {
                                sheetH.SetCellValue(startRow, 45, this.GetTypeCapacityPrimary(surBO_HKI.SummedUpMark.Value));
                            }
                            if (surBO_HKI != null && surBO_HKI.SummedUpMark != null && isTVSubject)
                            {
                                sheetH.SetCellValue(startRow, 53, this.GetTypeCapacityPrimary(surBO_HKI.SummedUpMark.Value));
                            }
                            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            {
                                surBO = new VSummedUpRecord();

                                //surList = listSummedUpRecord.Where(u => u.PupilID == PupilID);
                                //if (surList.Any(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST))
                                //{
                                //    surBO = surList.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST).FirstOrDefault();
                                //}
                                //else
                                //{
                                //    surBO = surList.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                //}

                                surBO = surList.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                if (surBO != null && surBO.SummedUpMark != null && !isTVSubject)
                                {
                                    sheetH.SetCellValue(startRow, 46, this.GetTypeCapacityPrimary(surBO.SummedUpMark.Value));
                                }
                                else if (surBO != null && surBO.SummedUpMark != null && isTVSubject)
                                {
                                    sheetH.SetCellValue(startRow, 54, this.GetTypeCapacityPrimary(surBO.SummedUpMark.Value));
                                }
                            }
                            QueenDic.Add(PupilDic);
                            //sheetH.(rangeCopy, startRow, 1, true);

                            //voi dong cuoi gach chan duoi
                            if (Stt == cntPupil && !isTVSubject)
                            {
                                sheetH.GetRange(startRow, 1, startRow, 46).SetBorder(XLVTBorderStyleValues.Thick, XVTBorderIndex.BottomBorder);
                            }
                            else if (Stt == cntPupil && isTVSubject)
                            {
                                sheetH.GetRange(startRow, 1, startRow, 54).SetBorder(XLVTBorderStyleValues.Thick, XVTBorderIndex.BottomBorder);
                            }
                            //tang dong de copy template
                            startRow++;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Xu ly cho mon nhan xet
                        TypeSubject = "0";
                        //Lay danh sach diem cua mon hoc
                        var listMark = ListJudgeRecord.Where(u => u.SubjectID == Subject.SubjectID);

                        var lsSUR_HKI = lsSUR.Where(u => u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
                        var lsSUR_HKII = new List<VSummedUpRecord>();
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            lsSUR_HKII = lsSUR.Where(u => u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
                        }

                        //chay tung hoc sinh de ghep vao dictionary
                        int[] listMapMonth = new int[] { 1, 2, 3, 4, 5, 1, 2, 3, 4, 5 };
                        int[] listSemester = new int[] { 1, 1, 1, 1, 1, 2, 2, 2, 2, 2 };
                        //doi voi cap 1 cap 2 lay cac dau diem 1234 voi HK1 5678 voi HK2
                        if (EducationLevelID == 1 || EducationLevelID == 2)
                        {
                            listMapMonth = new int[] { 1, 2, 3, 4, 1, 2, 3, 4, 5, 6 };
                            listSemester = new int[] { 1, 1, 1, 1, 2, 2, 2, 2, 2, 2 };
                        }
                        int Stt = 0;
                        sheetH.CopyRow(7, 8, ListPupilID.Count() + 6);
                        foreach (var PupilID in ListPupilID)
                        {
                            Dictionary<string, object> PupilDic = new Dictionary<string, object>();
                            var pupilInfo = ListPupil.Where(u => u.PupilProfileID == PupilID).FirstOrDefault();

                            //lay diem cho tung hoc sinh
                            var listMarkPupil = listMark.Where(u => u.PupilID == PupilID);
                            Stt++;
                            sheetH.SetCellValue(startRow, 1, Stt.ToString());
                            sheetH.SetCellValue(startRow, 2, pupilInfo.FullName);
                            sheetH.SetCellValue(startRow, 3, pupilInfo.BirthDate.ToString("dd/MM/yyyy"));
                            sheetH.SetCellValue(startRow, 4, pupilInfo.PupilCode);
                            //cac thong tin diem

                            #region Nap diem cho tung dau diem
                            //lay tat ca diem
                            var listMarkForMonth = listMarkPupil.Where(u => u.Judgement != null);

                            //Khối 1, 2: học kỳ I điền 4 cột kết quả nhận xét 1, 2, 3, 4. 
                            //Học kỳ II điền các cột 1,2 ,3 ,4, 5,6,7,8
                            //Khối 3, 4, 5: học kỳ I điền các cột 1,2,3,4,5 học kỳ II điền các cột 1,2,3,4,5,6,7,8,9,10


                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            {
                                int startSemester = 4;
                                for (int orderNumber = 1; orderNumber <= 5; orderNumber++)
                                {

                                    var listPupil = listMarkForMonth.Where(u => u.OrderNumber == orderNumber
                                        && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST);
                                    if (listPupil != null && listPupil.Count() > 0)
                                    {
                                        //co điểm luu
                                        sheetH.SetCellValue(startRow, startSemester + orderNumber, "1");
                                    }

                                }
                            }
                            else
                            {
                                int startSemester = 4;
                                for (int orderNumber = 1; orderNumber <= 10; orderNumber++)
                                {
                                    var listPupil = listMarkForMonth.Where(u => u.OrderNumber == listMapMonth[orderNumber - 1]
                                       && u.Semester == listSemester[orderNumber - 1]);
                                    if (listPupil != null && listPupil.Count() > 0)
                                    {
                                        //co điểm luu
                                        sheetH.SetCellValue(startRow, startSemester + orderNumber, "1");
                                    }

                                }
                            }
                            #endregion

                            //trung binh mon neu la hoc ky 1
                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            {
                                var SummedUp = lsSUR_HKI.Where(o => o.PupilID == PupilID && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                                if (SummedUp != null)
                                {
                                    sheetH.SetCellValue(startRow, 15, SummedUp.JudgementResult);
                                }
                            }
                            else
                            {
                                var SummedUp = lsSUR_HKII.Where(o => o.PupilID == PupilID && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                                if (SummedUp != null)
                                {
                                    sheetH.SetCellValue(startRow, 16, SummedUp.JudgementResult);
                                }
                                var summedUP_HKI = lsSUR_HKI.Where(o => o.PupilID == PupilID && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                                if (summedUP_HKI != null)
                                {
                                    sheetH.SetCellValue(startRow, 15, summedUP_HKI.JudgementResult);
                                }
                            }
                            QueenDic.Add(PupilDic);
                            //sheetH.(rangeCopy, startRow, 1, true);

                            //voi dong cuoi gach chan duoi
                            if (Stt == cntPupil)
                            {
                                sheetH.GetRange(startRow, 1, startRow, 16).SetBorder(XLVTBorderStyleValues.Thick, XVTBorderIndex.BottomBorder);
                            }


                            //tang dong de copy template
                            startRow++;
                        }
                        #endregion
                    }


                    //Xoa cac dong khong co hoc sinh
                    for (int i = startRow; i <= 77; i++)
                    {
                        sheetH.HideRow(i);
                    }

                    #region Dien thong tin vao file excel
                    GlobalInfo glo = new GlobalInfo();

                    Dictionary<string, object> KingDic = new Dictionary<string, object>();


                    string SubjectCode = Subject.SubjectCode != null ? Subject.SubjectCode.Trim() : string.Empty;
                    string SemesterStr = "0" + Semester.ToString();
                    string EmisCode = string.Format("{0}-{1}{2}-{3}-{4}-4-4-{5}", ClassCode, SubjectCode, TypeSubject, SemesterStr, ac.Year, cntPupil);

                    sheetH.SetCellValue("A1", "Trường: " + sp.SchoolName);

                    string SemesterName = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "Học kỳ 1" : "Học kỳ 2";


                    string AcademicYearName = ac.Year.ToString() + "-" + (ac.Year + 1).ToString();

                    sheetH.SetCellValue("A2", "Năm học: " + AcademicYearName);

                    sheetH.SetCellValue("E2", SemesterName);

                    sheetH.SetCellValue("A3", "Lớp: " + cl.DisplayName);

                    sheetH.SetCellValue("E1", "BẢNG ĐIỂM CHI TIẾT MÔN " + SubjectName.ToUpper());

                    KingDic.Add("ClassCode", ClassCode);
                    KingDic.Add("SubjectName", SubjectName.ToUpper());

                    sheetH.SheetName = Business.Common.ReportUtils.StripVNSign(SubjectName);

                    sheetH.SetCellValue("C3", EmisCode);
                    KingDic.Add("Rows", QueenDic);

                    //lay giao vien bo mon
                    var taList = listTeacherForSubject.Where(u => u.SubjectID == Subject.SubjectID);
                    string TeacherName = "";
                    if (taList != null && taList.Count() > 0)
                    {
                        TeacherName = taList.FirstOrDefault().TeacherName;
                    }
                    sheetH.SetCellValue("a4", "Giáo viên: " + TeacherName);
                    #endregion

                    //fill thong tin
                    odSubject++;
                }

                var excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                string SemesterFile = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII";

                result.FileDownloadName = "BangDiem" + cl.DisplayName + "_" + SemesterFile + "_" + ac.Year.ToString() + ".xls";

                return result;
                #endregion
            }
            return null;
        }

        public string GetTypeCapacityPrimary(decimal mark)
        {
            if (mark >= 9)
                return "G";

            if (mark >= 7 && mark < 9)
                return "K";

            if (mark >= 5 && mark < 7)
                return "Tb";

            if (mark < 5)
                return "Y";

            return "";
        }

        public string GetTypeCapacity(decimal mark)
        {
            if (mark >= 9)
                return SystemParamsInFile.XL_GIOI;

            if (mark >= 7 && mark < 9)
                return SystemParamsInFile.XL_KHA;

            if (mark >= 5 && mark < 7)
                return SystemParamsInFile.XL_TRUNGBINH;

            if (mark < 5)
                return SystemParamsInFile.XL_YEU;

            return "";
        }

        public bool ValidateCheck(string code)
        {
            foreach (char ch in code)
            {
                if (!((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122) || (ch == 95) || (ch == 46) || (ch == 44) || (ch >= 48 && ch <= 57)))
                {
                    return false;
                }
            }
            return true;
        }

        //export pupil report
        public FileResult ExportFile(FormCollection form)
        {
            Stream excel;

            //Cac bien o TieuChiTimKiem
            int EducationLevelID = Business.Common.Utils.GetInt(form["EducationLevelID"]);
            int ClassID = Business.Common.Utils.GetInt(form["ClassID"]);
            string ClassCode = form["ClassCode"];
            string SchoolCode = form["SchoolCode"];

            //kiem tra ma lop
            if (!ValidateCheck(ClassCode))
            {
                throw new BusinessException("Mã lớp, mã trường: gồm các ký tự chữ không dấu, số và các ký tự . , _ ");
            }

            //lay các thông tin trên form
            #region Lay thong tin dauvao
            string chkPupilCode = form["chkPupilCode"];
            string chkPupilName = form["chkPupilName"];
            string chkBirthDate = form["chkBirthDate"];
            string chkBirthPlace = form["chkBirthPlace"];
            string chkGenre = form["chkGenre"];
            string chkEthnic = form["chkEthnic"];
            string chkReligion = form["chkReligion"];
            string chkAddress = form["chkAddress"];
            string chkProvince = form["chkProvince"];
            string chkDistrict = form["chkDistrict"];
            string chkCommnune = form["chkCommnune"];
            string chkIsYoungPioneerMember = form["chkIsYoungPioneerMember"];
            string chkDateInYoungPioneerMember = form["chkDateInYoungPioneerMember"];
            string chkIsCommunistPartyMember = form["chkIsCommunistPartyMember"];
            string chkDateCommunistPartyMember = form["chkDateCommunistPartyMember"];
            string chkPolicyTarget = form["chkPolicyTarget"];
            string chkPriority = form["chkPriority"];
            string chkDisabled = form["chkDisabled"];
            string chkMobile = form["chkMobile"];
            string chkFatherFullName = form["chkFatherFullName"];
            string chkFatherBirthDate = form["chkFatherBirthDate"];
            string chkPhone = form["chkPhone"];
            string chkFatherJob = form["chkFatherJob"];
            string chkFatherMobile = form["chkFatherMobile"];
            string chkMotherFullName = form["chkMotherFullName"];
            string chkMotherBirthDate = form["chkMotherBirthDate"];
            string chkMotherJob = form["chkMotherJob"];
            string chkMotherMobile = form["chkMotherMobile"];
            #endregion

            //Duong dan file
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "VEMIS", ReportVEmisConstants.TEMPLATE_PUPILPROFILE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);

            //Tien Hanh Xoa Cot
            //Bien cot dau tien kiem tra
            //Bien vitri dong tieu de
            string on = "on";
            //Doi tuong dic ghep cac bien dau vao va tieu de cot
            #region Map sau va bien
            Dictionary<string, object> dic = new Dictionary<string, object>() { 
                {"Ngày sinh",chkBirthDate},
                {"Nơi sinh",chkBirthPlace},
                {"Giới tính",chkGenre},
                {"Quê quán",chkAddress},
                {"Dân tộc",chkEthnic},
                {"Tôn giáo",chkReligion},
                {"Hộ khẩu (Xã/Phường)",chkCommnune},
                {"Hộ khẩu (Quận/Huyện)",chkDistrict},
                {"Hộ khẩu (Tỉnh/Thành)",chkProvince},
                {"Điện thoại",chkMobile},
                {"Diện ưu tiên",chkPriority},
                {"Khuyết tật",chkDisabled},
                {"Đoàn đội",chkIsYoungPioneerMember},
                {"Ngày kết nạp (Đoàn/Đội)",chkDateInYoungPioneerMember},
                {"Đảng viên",chkDateCommunistPartyMember},
                {"Ngày kết nạp đảng",chkDateCommunistPartyMember},
                {"Họ và tên bố",chkFatherFullName},
                {"Năm sinh bố",chkFatherBirthDate},
                {"Nghề nghiệp bố",chkFatherJob},
                {"Điện thoại bố",chkFatherMobile},
                {"Họ và tên mẹ",chkMotherFullName},
                {"Năm sinh mẹ",chkMotherBirthDate},
                {"Nghề nghiệp mẹ",chkMotherJob},
                {"Điện thoại mẹ",chkMotherMobile}
            };
            #endregion

            //Tim Kiem Thong Tin Hoc Sinh trong lop            
            IDictionary<string, object> dicS = new Dictionary<string, object>();
            dicS["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;
            dicS["CurrentClassID"] = ClassID;

            //Tim Kiem Thong Tin Hoc Sinh trong lop
            List<PupilProfileBO> ListPupil = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicS)
                .Where(o => o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                .ToList()
                .OrderBy(u => u.OrderInClass).ThenBy(u => SMAS.Business.Common.Utils.SortABC(u.Name + " " + u.FullName))
                .ToList();
            List<int> ListPupilID = ListPupil.Select(o => o.PupilProfileID).ToList();
            int cntPupil = ListPupil.Count;
            List<object> QueenDic = new List<object>();

            int Stt = 0;
            int startRow = 7;
            foreach (PupilProfileBO pupil in ListPupil)
            {
                Stt++;
                #region DuLieuMacDinh
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = pupil.PupilCode;
                princeDic["HoTen"] = pupil.FullName;
                princeDic["ThuTu"] = "01";
                princeDic["NgaySinh"] = "";
                princeDic["NoiSinh"] = "";
                princeDic["GioiTinh"] = "";
                princeDic["QueQuan"] = "";
                princeDic["QuocTich"] = "1";
                princeDic["DanToc"] = "";
                princeDic["TonGiao"] = "0";
                princeDic["HoKhauXa"] = "";
                princeDic["HoKhauQuan"] = "";
                princeDic["HoKhauTinh"] = "";
                princeDic["DienThoai"] = "";
                princeDic["DienUuTien"] = "";
                princeDic["DienUuDai"] = "00";
                princeDic["DoanDoi"] = "";
                princeDic["NgayKetNapDoan"] = "";
                princeDic["DangVien"] = "";
                princeDic["NgayKetNapDang"] = "";
                princeDic["HoTenBo"] = "";
                princeDic["NamSinhBo"] = "";
                princeDic["NgheNghiepBo"] = "";
                princeDic["DienThoaiBo"] = "";
                princeDic["HoTenMe"] = "";
                princeDic["NamSinhMe"] = "";
                princeDic["NgheNghiepMe"] = "";
                princeDic["DienThoaiMe"] = "";
                #endregion
                #region KiemTraCheckTrenGiaoDienDeFillDuLieuVao
                if (Check(chkBirthDate))
                {
                    princeDic["NgaySinh"] = pupil.BirthDate.ToString("dd/MM/yyyy");
                }
                if (Check(chkBirthPlace))
                {
                    princeDic["NoiSinh"] = pupil.BirthPlace;
                }
                if (Check(chkGenre))
                {
                    // Gioi tinh SMAS 3 dang luu nguoc voi VEMIS
                    princeDic["GioiTinh"] = 1 - pupil.Genre;
                }
                if (Check(chkAddress))
                {
                    princeDic["QueQuan"] = pupil.HomeTown;
                }
                if (Check(chkEthnic))
                {
                    princeDic["DanToc"] = pupil.EthnicCode;
                }
                if (Check(chkReligion))
                {
                    if (pupil.ReligionCode != null)
                    {
                        princeDic["TonGiao"] = pupil.ReligionCode;
                    }
                }
                if (Check(chkCommnune))
                {
                    princeDic["HoKhauXa"] = pupil.CommnuneCode;
                }
                if (Check(chkDistrict))
                {
                    princeDic["HoKhauQuan"] = pupil.DistrictCode;
                }
                if (Check(chkProvince))
                {
                    princeDic["HoKhauTinh"] = pupil.ProvinceCode;
                }
                if (Check(chkMobile))
                {
                    princeDic["DienThoai"] = pupil.Mobile;
                }
                if (Check(chkPriority))
                {
                    princeDic["DienUuTien"] = pupil.PolicyTargetCode;
                }
                string value = "";

                bool? doivien = pupil.IsYoungPioneerMember;
                bool? doanvien = pupil.IsYouthLeageMember;
                if (doivien.HasValue && doivien.Value)
                {
                    if (doanvien.HasValue && doanvien.Value)
                    {
                        value = "2";
                    }
                    else
                    {
                        value = "1";
                    }
                }
                if (doanvien.HasValue && doanvien.Value)
                {
                    value = "2";
                }
                if (Check(chkIsYoungPioneerMember))
                {
                    //dien thong tin doan
                    princeDic["DoanDoi"] = value;
                }
                //ngay ket nap doan
                if (Check(chkDateInYoungPioneerMember))
                {
                    if (value.Equals("1"))
                    {
                        if (pupil.YoungPioneerJoinedDate != null)
                            princeDic["NgayKetNapDoan"] = pupil.YoungPioneerJoinedDate.Value.ToString("dd/MM/yyyy");
                    }
                    if (value.Equals("2"))
                    {
                        if (pupil.YouthLeagueJoinedDate != null)
                            princeDic["NgayKetNapDoan"] = pupil.YouthLeagueJoinedDate.Value.ToString("dd/MM/yyyy");
                    }
                }
                //Dang vien
                if (Check(chkIsCommunistPartyMember))
                {
                    if (pupil.IsCommunistPartyMember.HasValue && pupil.IsCommunistPartyMember.Value)
                    {
                        princeDic["DangVien"] = "True";
                    }
                }

                //Thong tin bo
                if (Check(chkFatherFullName))
                {
                    princeDic["HoTenBo"] = pupil.FatherFullName;
                }
                if (Check(chkFatherBirthDate) && pupil.FatherBirthDate != null)
                {
                    princeDic["NamSinhBo"] = pupil.FatherBirthDate.Value.Year.ToString();
                }
                if (Check(chkFatherJob))
                {
                    princeDic["NgheNghiepBo"] = pupil.FatherJob;
                }
                if (Check(chkFatherMobile))
                {
                    princeDic["DienThoaiBo"] = pupil.FatherMobile;
                }

                //Thong tin me
                if (Check(chkMotherFullName))
                {
                    princeDic["HoTenMe"] = pupil.MotherFullName;
                }
                if (Check(chkMotherBirthDate) && pupil.MotherBirthDate != null)
                {
                    princeDic["NamSinhMe"] = pupil.MotherBirthDate.Value.Year.ToString();
                }
                if (Check(chkMotherJob))
                {
                    princeDic["NgheNghiepMe"] = pupil.MotherJob;
                }
                if (Check(chkMotherMobile))
                {
                    princeDic["DienThoaiMe"] = pupil.MotherMobile;
                }
                #endregion
                //copystyle for pupil
                IVTRange range = sheet.GetRange("A7", "AP7");
                sheet.CopyPaste(range, startRow, 1, true);
                startRow++;
                QueenDic.Add(princeDic);
            }
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            KingDic.Add("SchoolName", sp.SchoolName);
            KingDic.Add("SchoolCode", SchoolCode);

            string SemesterName = _globalInfo.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "Học kỳ 1" : "Học kỳ 2";

            AcademicYear ac = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            string AcademicYearName = ac.Year.ToString() + "-" + (ac.Year + 1).ToString();
            KingDic.Add("Semester", SemesterName);
            KingDic.Add("AcademicYear", AcademicYearName);

            KingDic.Add("ClassCode", ClassCode);

            string className = ClassProfileBusiness.Find(ClassID).DisplayName;

            KingDic.Add("ClassName", className);

            KingDic.Add("Rows", QueenDic);
            sheet.FillVariableValue(KingDic);

            excel = oBook.ToStream(true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "DanhSachHocSinh_" + className + ".xls";

            return result;
        }

        public bool Check(string str)
        {
            if (str != null && str.Equals("on"))
            {
                return true;
            }
            return false;
        }

    }
}
