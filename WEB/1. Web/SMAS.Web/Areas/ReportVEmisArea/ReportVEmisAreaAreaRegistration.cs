﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportVEmisArea
{
    public class ReportVEmisAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportVEmisArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportVEmisArea_default",
                "ReportVEmisArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
