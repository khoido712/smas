﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportVEmisArea
{
    public class ReportVEmisConstants
    {
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_SUBJECT = "LIST_SUBJECT";

        public const string TEMPLATE_PUPILPROFILE = "DanhSachHocSinh.xls";
        public const string BangDiemCap23HK1 = "BangDiemCap23HK1.xls";
        public const string BangDiemCap23HK2 = "BangDiemCap23HK2.xls";

        public const string BangDiemCap1 = "BangDiemCap1.xlsx";

        public const string ClassCode = "ClassCode";
        public const string SchoolCode = "SchoolCode";


    }
}