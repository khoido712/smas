﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryViolateArea.Models
{
    public class ExamSupervisoryViolateViewModel
    {
        //PK
        public long ExamSupervisoryViolateID { get; set; }

        //FK
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public long ExamSupervisoryID { get; set; }
        public int SubjectID { get; set; }
        public long ExamRoomID { get; set; }
        public int? ExamViolatetionTypeID { get; set; }

        [ResourceDisplayName("ExamSupervisoryViolate_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }
        [ResourceDisplayName("ExamSupervisoryViolate_Label_EmployeeName")]
        public string EmployeeName { get; set; }
        [ResourceDisplayName("ExamSupervisoryViolate_Label_SubjectName")]
        public string SubjectName { get; set; }
        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExamRoomCode")]
        public string ExamRoomCode { get; set; }
        [ResourceDisplayName("ExamSupervisoryViolate_Label_ContentViolated")]
        public string ContentViolated { get; set; }
        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExamViolatetionTypeResolusion")]
        public string ExamViolatetionTypeResolusion { get; set; }
        public string EthnicCode { get; set; }
        public int IsCheck { get; set; }
    }
}