﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryViolateArea.Models
{
    public class CreateSearchViewModel
    {
        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryViolateConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "_onExaminationsChange()")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public long? _ExaminationsID { get; set; }

        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryViolateConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "_onExamGroupChange()")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public long? _ExamGroupID { get; set; }

        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryViolateConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("OnChange", "_onExamSubjectChange()")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public long? _SubjectID { get; set; }

        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExamRoomID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryViolateConstants.CBO_EXAM_ROOM)]
        [AdditionalMetadata("OnChange", "_onExamRoomChange()")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public long? _ExamRoomID { get; set; }
    }
}