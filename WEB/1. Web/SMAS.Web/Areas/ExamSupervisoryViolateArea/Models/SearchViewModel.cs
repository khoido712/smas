﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryViolateArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryViolateConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public long? ExaminationsID { get; set; }

        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryViolateConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public long? ExamGroupID { get; set; }

        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryViolateConstants.CBO_EXAM_SUBJECT)]
        public long? SubjectID { get; set; }

        [ResourceDisplayName("ExamSupervisoryViolate_Label_ExamRoomID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryViolateConstants.CBO_EXAM_ROOM)]
        public long? ExamRoomID { get; set; }

        [ResourceDisplayName("ExamSupervisoryViolate_Label_SupervisoryName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SupervisoryName { get; set; }

    }
}