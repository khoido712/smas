﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryViolateArea
{
    public class ExamSupervisoryViolateAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamSupervisoryViolateArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamSupervisoryViolateArea_default",
                "ExamSupervisoryViolateArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
