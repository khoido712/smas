﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ExamSupervisoryViolateArea;
using SMAS.Web.Areas.ExamSupervisoryViolateArea.Models;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using System.IO;

namespace SMAS.Web.Areas.ExamSupervisoryViolateArea.Controllers
{
    public class ExamSupervisoryViolateController : BaseController
    {
          #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness;
        private readonly IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness;
        private readonly IExamViolationTypeBusiness ExamViolationTypeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamSubjectBO> listExamSubject;
        private List<ExamRoom> listExamRoom;
        #endregion

        #region Constructor
        public ExamSupervisoryViolateController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness, IExamRoomBusiness ExamRoomBusiness, IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness,
            IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness, IExamViolationTypeBusiness ExamViolationTypeBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ExamSupervisoryViolateBusiness = ExamSupervisoryViolateBusiness;
            this.ExamSupervisoryAssignmentBusiness = ExamSupervisoryAssignmentBusiness;
            this.ExamViolationTypeBusiness = ExamViolationTypeBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamSupervisoryViolateArea/ExamSupervisoryViolate/

        public ActionResult Index()
        {
            SetViewData();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Lay ky thi mac dinh
            long? defaultExamID = null;
            if (listExaminations.Count > 0)
            {
                defaultExamID = listExaminations.First().ExaminationsID;
            }

            if (defaultExamID == null)
            {
                ViewData[ExamSupervisoryViolateConstants.LIST_RESULT] = new List<ExamSupervisoryViolateViewModel>();
                ViewData[ExamSupervisoryViolateConstants.TOTAL] = 0;
                CheckCommandPermission(null);
                return View();
            }

            dic["ExaminationsID"] = defaultExamID;

            IEnumerable<ExamSupervisoryViolateViewModel> iq = _Search(dic);
            int totalRecord = iq.Count();
            ViewData[ExamSupervisoryViolateConstants.TOTAL] = totalRecord;
            List<ExamSupervisoryViolateViewModel> listResult = iq.Take(ExamSupervisoryViolateConstants.PageSize).ToList();
            ViewData[ExamSupervisoryViolateConstants.LIST_RESULT] = listResult;

            Examinations defaultExaminations = ExaminationsBusiness.Find(defaultExamID);
            CheckCommandPermission(defaultExaminations);

            ViewData[ExamSupervisoryViolateConstants.HIDDEN_EXAMINATIONS_ID] = defaultExamID;

            return View();
        }

        // GET: /ExamSupervisoryViolate/Search
        public PartialViewResult Search(SearchViewModel form, GridCommand command)
        {
            Utils.Utils.TrimObject(form);

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;
            long? examRoomID = form.ExamRoomID;
            string supervisoryName = form.SupervisoryName;

            if (examinationsID == null)
            {
                ViewData[ExamSupervisoryViolateConstants.TOTAL] = 0;
                CheckCommandPermission(null);
                return PartialView("_List", new List<ExamSupervisoryViolateViewModel>());
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SubjectID"] = subjectID;
            dic["ExamRoomID"] = examRoomID;
            dic["ExamSupervisoryName"] = supervisoryName;

            IEnumerable<ExamSupervisoryViolateViewModel> iq = _Search(dic);
            int totalRecord = iq.Count();
            ViewData[ExamSupervisoryViolateConstants.TOTAL] = totalRecord;
            List<ExamSupervisoryViolateViewModel> listResult = iq.Take(ExamSupervisoryViolateConstants.PageSize).ToList();

            CheckCommandPermission(ExaminationsBusiness.Find(examinationsID));

            ViewData[ExamSupervisoryViolateConstants.HIDDEN_EXAMINATIONS_ID] = form.ExaminationsID;
            ViewData[ExamSupervisoryViolateConstants.HIDDEN_EXAM_GROUP_ID] = form.ExamGroupID;
            ViewData[ExamSupervisoryViolateConstants.HIDDEN_SUBJECT_ID] = form.SubjectID;
            ViewData[ExamSupervisoryViolateConstants.HIDDEN_EXAM_ROOM_ID] = form.ExamRoomID;
            ViewData[ExamSupervisoryViolateConstants.HIDDEN_SUPERVISORY_NAME] = form.SupervisoryName;

            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(SearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;
            long? examRoomID = form.ExamRoomID;
            string supervisoryName = form.SupervisoryName;

            if (examinationsID == null)
            {
                return View(new GridModel<ExamSupervisoryViolateViewModel>
                {
                    Total = 0,
                    Data = new List<ExamSupervisoryViolateViewModel>()
                });
            }

            //Add search info - Navigate to Search function in business
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SubjectID"] = subjectID;
            dic["ExamRoomID"] = examRoomID;
            dic["ExamSupervisoryName"] = supervisoryName;
            IEnumerable<ExamSupervisoryViolateViewModel> iq = _Search(dic);
            int totalRecord = iq.Count();
            List<ExamSupervisoryViolateViewModel> listResult = iq.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

            ViewData[ExamSupervisoryViolateConstants.HIDDEN_EXAMINATIONS_ID] = form.ExaminationsID;
            ViewData[ExamSupervisoryViolateConstants.HIDDEN_EXAM_GROUP_ID] = form.ExamGroupID;
            ViewData[ExamSupervisoryViolateConstants.HIDDEN_SUBJECT_ID] = form.SubjectID;
            ViewData[ExamSupervisoryViolateConstants.HIDDEN_EXAM_ROOM_ID] = form.ExamRoomID;
            ViewData[ExamSupervisoryViolateConstants.HIDDEN_SUPERVISORY_NAME] = form.SupervisoryName;

            return View(new GridModel<ExamSupervisoryViolateViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        //
        // GET: /ExamSupervisoryViolate/Create
        public ActionResult Create(long examinationsID, long? examGroupID, int? subjectID, long? examRoomID)
        {
            SetViewDataForCreate(examinationsID, examGroupID, subjectID, examRoomID);
           
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Lay ky thi mac dinh
            long? defaultExamID = null;
            if (examinationsID != null && listExaminations.Any(o => o.ExaminationsID == examinationsID))
                defaultExamID = examinationsID;
            else if (listExaminations.Count > 0)
                defaultExamID = listExaminations.First().ExaminationsID;

            //Lay nhom thi mac dinh
            long? defaultExamGroupID = null;
            if (examGroupID != null && listExamGroup.Any(o=>o.ExamGroupID==examGroupID))
            {
                defaultExamGroupID = examGroupID;
            }
            else if (listExamGroup.Count > 0)
            {
                defaultExamGroupID = listExamGroup.First().ExamGroupID;
            }

            //Lay mon thi mac dinh
            int? defaultSubjectID = null;
            if (subjectID != null && listExamSubject.Any(o=>o.SubjectID==subjectID))
            {
                defaultSubjectID = subjectID;
            }
            else if (listExamSubject.Count > 0)
            {
                defaultSubjectID = listExamSubject.First().SubjectID;
            }

            //Lay phong thi mac dinh
            long? defaultExamRoomID = null;
            if (examRoomID != null && listExamRoom.Any(o=>o.ExamRoomID==examRoomID))
            {
                defaultExamRoomID = examRoomID;
            }
            else if (listExamRoom.Count > 0)
            {
                defaultExamRoomID = listExamRoom.First().ExamRoomID;
            }

            CreateSearchViewModel model = new CreateSearchViewModel();
            model._ExaminationsID = examinationsID;
            model._ExamGroupID = examGroupID;
            model._SubjectID = subjectID;
            model._ExamRoomID = examRoomID;
            ViewData[ExamSupervisoryViolateConstants.CREATE_SEARCH_DATA] = model;

            if (defaultExamID==null|| defaultExamGroupID == null || defaultSubjectID == null||defaultExamRoomID==null)
            {
                ViewData[ExamSupervisoryViolateConstants.LIST_RESULT] = new List<ExamSupervisoryViolateViewModel>();
                ViewData[ExamSupervisoryViolateConstants.CREATE_OR_EDIT] = ExamSupervisoryViolateConstants.CREATE;
                return View();
            }

            dic["ExamRoomID"] = defaultExamRoomID;

            List<ExamSupervisoryViolateViewModel> listResult = _CreateSearch(defaultExamID.Value,defaultExamGroupID.Value,defaultSubjectID.Value,dic).ToList();
          
            ViewData[ExamSupervisoryViolateConstants.LIST_RESULT] = listResult;
            ViewData[ExamSupervisoryViolateConstants.CREATE_OR_EDIT] = ExamSupervisoryViolateConstants.CREATE;

            return View();
        }

        // GET: /ExamSupervisoryViolate/CreateSearch
        public PartialViewResult CreateSearch(CreateSearchViewModel form)
        {
            ViewData[ExamSupervisoryViolateConstants.CREATE_OR_EDIT] = ExamSupervisoryViolateConstants.CREATE;
            //Lay du lieu combobox hinh thuc xu ly
            List<ExamViolationType> listExamViolateType = this.ExamViolationTypeBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID)
                                                                                            .Where(o => o.AppliedForInvigilator == true)
                                                                                            .Where(o=>o.IsActive==true)
                                                                                            .ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.Resolution)).ToList();

            ViewData[ExamSupervisoryViolateConstants.CBO_EXAM_VIOLATE_TYPE] = listExamViolateType;

            Utils.Utils.TrimObject(form);

            long? examinationsID = form._ExaminationsID;
            long? examGroupID = form._ExamGroupID;
            long? subjectID = form._SubjectID;
            long? examRoomID = form._ExamRoomID;

            if (examinationsID == null || examGroupID == null || subjectID == null || examRoomID==null)
            {
                return PartialView("_CreateList", new List<ExamSupervisoryViolateViewModel>());
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExamRoomID"] = examRoomID;

            List<ExamSupervisoryViolateViewModel> listResult = _CreateSearch(examinationsID.Value, examGroupID.Value, subjectID.Value, dic).ToList();
            return PartialView("_CreateList", listResult);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(List<ExamSupervisoryViolateViewModel> ResultList, long examinationsID, long examGroupID, int subjectID, long examRoomID)
        {
            if (GetMenupermission("ExamSupervisoryViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (ResultList.Where(o => o.IsCheck == 1).Count() == 0)
            {
                throw new BusinessException("ExamSupervisoryViolate_Validate_Create_NoChoice");
            }

            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            if (exam.MarkInput==true || exam.MarkClosing==true)
            {
                throw new BusinessException("ExamSupervisoryViolate_Validate_InvalidExam");
            }

            ExamSupervisoryViolate entity;
            for (int i = 0; i < ResultList.Count; i++)
            {
                ExamSupervisoryViolateViewModel esv=ResultList[i];
                if (esv.IsCheck!=1)
                    continue;
                entity = new ExamSupervisoryViolate();
                entity.ContentViolated = esv.ContentViolated.Trim();
                entity.CreateTime = DateTime.Now;
                entity.ExamGroupID = examGroupID;
                entity.ExaminationsID = examinationsID;
                entity.ExamRoomID = examRoomID;
                entity.ExamSupervisoryID = esv.ExamSupervisoryID;
                entity.ExamSupervisoryViolateID = ExamSupervisoryViolateBusiness.GetNextSeq<long>();
                entity.ExamViolationTypeID = esv.ExamViolatetionTypeID;
                entity.SubjectID = subjectID;

                ExamSupervisoryViolateBusiness.Insert(entity);

            }

            ExamSupervisoryViolateBusiness.Save();
            return Json(new JsonMessage(Res.Get("ExamSupervisoryViolate_Message_CreateSuccess")));
        }

        //
        // GET: /ExamSupervisoryViolate/Edit
        public ActionResult Edit(long examSupervisoryViolateID, long examinationsID, long examGroupID, int subjectID,long examRoomID)
        {
            SetViewDataForCreate(examinationsID, examGroupID, subjectID, examRoomID);

            ExamSupervisoryViolate entity = this.ExamSupervisoryViolateBusiness.Find(examSupervisoryViolateID);

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["ExamRoomID"] = examRoomID;

            List<ExamSupervisoryViolateViewModel> listResult = _CreateSearch(examinationsID, examGroupID, subjectID, dic).Where(o=>o.ExamSupervisoryID==entity.ExamSupervisoryID).ToList();

            for (int i = 0; i < listResult.Count; i++)
            {
                ExamSupervisoryViolateViewModel esv = listResult[i];
                if (esv.ExamSupervisoryID == entity.ExamSupervisoryID)
                {
                    esv.ExamSupervisoryViolateID = entity.ExamSupervisoryViolateID;
                    esv.ContentViolated = entity.ContentViolated;
                    esv.ExamViolatetionTypeID = entity.ExamViolationTypeID;
                    break;
                }
            }

            ViewData[ExamSupervisoryViolateConstants.LIST_RESULT] = listResult;
            ViewData[ExamSupervisoryViolateConstants.CREATE_OR_EDIT] = ExamSupervisoryViolateConstants.EDIT;

            CreateSearchViewModel model = new CreateSearchViewModel();
            model._ExaminationsID = examinationsID;
            model._ExamGroupID = examGroupID;
            model._SubjectID = subjectID;
            model._ExamRoomID = examRoomID;
            ViewData[ExamSupervisoryViolateConstants.CREATE_SEARCH_DATA] = model;

            return View("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(List<ExamSupervisoryViolateViewModel> ResultList, long examinationsID, long examGroupID, int subjectID, long examRoomID)
        {
            if (GetMenupermission("ExamSupervisoryViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            if (exam.MarkInput == true || exam.MarkClosing == true)
            {
                throw new BusinessException("ExamSupervisoryViolate_Validate_InvalidExam");
            }

            ExamSupervisoryViolate entity;
            for (int i = 0; i < ResultList.Count; i++)
            {
                ExamSupervisoryViolateViewModel esv = ResultList[i];
                if (esv.ExamSupervisoryViolateID != 0)
                {
                    entity = this.ExamSupervisoryViolateBusiness.Find(esv.ExamSupervisoryViolateID);
                    entity.ContentViolated = esv.ContentViolated.Trim();
                    entity.ExamViolationTypeID = esv.ExamViolatetionTypeID;
                    entity.UpdateTime = DateTime.Now;

                    ExamSupervisoryViolateBusiness.Update(entity);
                    break;
                }
            }

            ExamSupervisoryViolateBusiness.Save();
            return Json(new JsonMessage(Res.Get("ExamSupervisoryViolate_Message_CreateSuccess")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(string arrayID)
        {
            if (GetMenupermission("ExamSupervisoryViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            string[] IDArr;
            if (arrayID != "")
            {
                arrayID = arrayID.Remove(arrayID.Length - 1);
                IDArr = arrayID.Split(',');
            }
            else
            {
                IDArr = new string[] { };
            }
            List<long> listExamSupervisoryViolateID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList() :
                                            new List<long>();
            ExamSupervisoryViolateBusiness.DeleteList(listExamSupervisoryViolateID);

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamSubject(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamSubjectBO>(), "SubjectID", "SubjectName"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            List<ExamSubjectBO> listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o=>o.OrderInSubject).ToList();

            return Json(new SelectList(listExamSubject, "SubjectID", "SubjectName"));
        }

        public JsonResult AjaxLoadExamRoom(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamRoom>(), "ExamRoomID", "ExamRoomCode"));
            }

            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                  .Where(o => o.ExamGroupID == examGroupID)
                                                  .OrderBy(o => o.ExamRoomCode)
                                                  .ToList();

            return Json(new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode"));
        }

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["SubjectID"] = form.SubjectID;
            dic["ExamRoomID"] = form.ExamRoomID;
            dic["ExamSupervisoryName"] = form.SupervisoryName;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;


            string reportCode = SystemParamsInFile.REPORT_GIAM_THI_VI_PHAM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ExamSupervisoryViolateBusiness.CreateExamSupervisoryViolateReport(dic);
                processedReport = ExamSupervisoryViolateBusiness.InsertExamSupervisoryViolateReport(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamSupervisoryViolateConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            List<ExamGroup> listExamGroup;
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamSupervisoryViolateConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;

            //Lay danh sach mon thi
            List<ExamSubjectBO> listExamSubject= new List<ExamSubjectBO>();
            ViewData[ExamSupervisoryViolateConstants.CBO_EXAM_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");

            //Lay danh sach phong thi
            List<ExamRoom> listExamRoom= new List<ExamRoom>();
            ViewData[ExamSupervisoryViolateConstants.CBO_EXAM_ROOM] = new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode");
        }

        private void SetViewDataForCreate(long? selectedExamID, long? selectedExamGroupID, int? selectedSubjectID, long? selectedExamRoomID)
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .Where(o=>o.MarkInput!=true)
                                            .Where(o=>o.MarkClosing!=true)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamSupervisoryViolateConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");

            long? defaultExamID = null;

            if (selectedExamID != null && listExaminations.Any(o=>o.ExaminationsID==selectedExamID))
                defaultExamID = selectedExamID;
            else if (listExaminations.Count > 0) 
                defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }

            ViewData[ExamSupervisoryViolateConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");
          
            long? defaultExamGroupID = null;
            if (selectedExamGroupID != null && listExamGroup.Any(o=>o.ExamGroupID==selectedExamGroupID))
                defaultExamGroupID = selectedExamGroupID;
            else if (listExamGroup.Count > 0) 
                defaultExamGroupID = listExamGroup.First().ExamGroupID;

            //Lay danh sach mon thi
            if (defaultExamGroupID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = defaultExamID;
                dic["ExamGroupID"] = defaultExamGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o=>o.OrderInSubject).ToList();
            }
            else
            {
                listExamSubject = new List<ExamSubjectBO>();
            }

            ViewData[ExamSupervisoryViolateConstants.CBO_EXAM_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");

            //Lay danh sach phong thi
            if (defaultExamGroupID != null)
            {
                listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == defaultExamID.Value)
                                                 .Where(o => o.ExamGroupID == defaultExamGroupID.Value)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            }
            else
            {
                listExamRoom = new List<ExamRoom>();
            }

            ViewData[ExamSupervisoryViolateConstants.CBO_EXAM_ROOM] = new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode");
           
             //Lay du lieu combobox hinh thuc xu ly
            List<ExamViolationType> listExamViolateType = this.ExamViolationTypeBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID)
                                                                                            .Where(o => o.AppliedForInvigilator == true)
                                                                                            .Where(o=>o.IsActive==true)
                                                                                            .ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.Resolution)).ToList();

            ViewData[ExamSupervisoryViolateConstants.CBO_EXAM_VIOLATE_TYPE] = listExamViolateType;
        }

        private IEnumerable<ExamSupervisoryViolateViewModel> _Search(IDictionary<string, object> dic)
        {
            IEnumerable<ExamSupervisoryViolateViewModel> list = this.ExamSupervisoryViolateBusiness.Search(dic)
                                                                    .Select(o => new ExamSupervisoryViolateViewModel
                                                                    {
                                                                        ExaminationsID=o.ExaminationsID,
                                                                        ExamGroupID=o.ExamGroupID,
                                                                        ContentViolated = o.ContentViolated,
                                                                        EmployeeCode = o.EmployeeCode,
                                                                        EmployeeName = o.EmployeeName,
                                                                        ExamRoomCode = o.ExamRoomCode,
                                                                        EthnicCode=o.EthnicCode,
                                                                        ExamRoomID = o.ExamRoomID,
                                                                        ExamSupervisoryViolateID = o.ExamSupervisoryViolateID,
                                                                        ExamViolatetionTypeID = o.ExamViolatetionTypeID,
                                                                        ExamViolatetionTypeResolusion = o.ExamViolatetionTypeResolusion,
                                                                        SubjectID = o.SubjectID,
                                                                        SubjectName = o.SubjectName,
                                                                        ExamSupervisoryID = o.ExamSupervisoryID
                                                                    }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode)));

            return list;
                       
        }

        private IEnumerable<ExamSupervisoryViolateViewModel> _CreateSearch(long examinationsID, long examGroupID, long subjectID, IDictionary<string, object> dic)
        {
            IEnumerable<ExamSupervisoryViolateViewModel> list = this.ExamSupervisoryAssignmentBusiness.SearchByExamSubject(_globalInfo.AcademicYearID.Value,_globalInfo.SchoolID.Value,
                                                                    examinationsID,examGroupID,subjectID,dic)
                                                                    .Select(o => new ExamSupervisoryViolateViewModel
                                                                    {
                                                                        ExaminationsID = o.ExaminationsID,
                                                                        ExamGroupID = o.ExamGroupID,
                                                                        ExamRoomID = o.ExamRoomID,
                                                                        EmployeeCode = o.EmployeeCode,
                                                                        EmployeeName = o.EmployeeFullName,
                                                                        SubjectID = o.SubjectID,
                                                                        ExamSupervisoryID = o.ExamSupervisoryID,
                                                                        EthnicCode=o.EthnicCode
                                                                    }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode)));

            return list;
        }
        private void CheckCommandPermission(Examinations exam)
        {
            SetViewDataPermission("ExamSupervisoryViolate", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            bool checkCreate = false;
            bool checkEdit = false;
            bool checkDelete = false;
            if (exam != null)
            {
                if (exam.MarkInput != true && exam.MarkClosing != true && _globalInfo.IsCurrentYear)
                {
                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE])
                    {
                        checkCreate = true;
                    }

                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_UPDATE])
                    {
                        checkEdit = true;
                    }

                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE])
                    {
                        checkDelete = true;
                    }
                }
            }

            ViewData[ExamSupervisoryViolateConstants.PER_CHECK_CREATE] = checkCreate;
            ViewData[ExamSupervisoryViolateConstants.PER_CHECK_EDIT] = checkEdit;
            ViewData[ExamSupervisoryViolateConstants.PER_CHECK_DELETE] = checkDelete;
        }
        #endregion
    }
}