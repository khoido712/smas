﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryViolateArea
{
    public class ExamSupervisoryViolateConstants
    {
        //Viewdata
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string CBO_EXAM_SUBJECT = "cbo_exam_subject";
        public const string CBO_EXAM_ROOM = "cbo_exam_room";
        public const string CBO_EXAM_VIOLATE_TYPE = "cbo_exam_violate_type";
        public const string LIST_RESULT = "list_result";
        public const string CREATE_SEARCH_DATA = "create_search_data";
        public const string PER_CHECK_CREATE= "per_check_create";
        public const string PER_CHECK_EDIT = "per_check_edit";
        public const string PER_CHECK_DELETE = "per_check_delete";
        public const string TOTAL = "total";
        public const string PAGE ="page";
        public const string CREATE_OR_EDIT = "create_or_edit";
        public const string EDIT_SUPERVISORY_ID = "edit_supervisory_id";

        public const string HIDDEN_EXAMINATIONS_ID = "hidden_examinations_id";
        public const string HIDDEN_EXAM_GROUP_ID = "hidden_exam_group_id";
        public const string HIDDEN_SUBJECT_ID = "hidden_subject_id";
        public const string HIDDEN_EXAM_ROOM_ID = "hidden_exam_room_id";
        public const string HIDDEN_SUPERVISORY_NAME = "hidden_supervisory_name";

        public const int PageSize = 20;
        public const int CREATE = 1;
        public const int EDIT = 2;
    }
}