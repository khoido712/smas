/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.MovementAcceptanceArea
{
    public class MovementAcceptanceConstants
    {
        public const string LIST_MOVEMENTACCEPTANCE = "listMovementAcceptance";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        
    }
}