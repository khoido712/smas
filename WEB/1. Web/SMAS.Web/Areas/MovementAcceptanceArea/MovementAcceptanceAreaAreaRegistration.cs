﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MovementAcceptanceArea
{
    public class MovementAcceptanceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MovementAcceptanceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MovementAcceptanceArea_default",
                "MovementAcceptanceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
