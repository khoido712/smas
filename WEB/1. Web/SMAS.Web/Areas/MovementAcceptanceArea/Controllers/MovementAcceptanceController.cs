﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MovementAcceptanceArea.Models;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Configuration;

namespace SMAS.Web.Areas.MovementAcceptanceArea.Controllers
{
    public class MovementAcceptanceController : BaseController
    {        
        private readonly IMovementAcceptanceBusiness MovementAcceptanceBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public MovementAcceptanceController(IClassProfileBusiness classProfileBusiness, IMovementAcceptanceBusiness movementacceptanceBusiness, ISchoolMovementBusiness schoolMovementBusiness, IAcademicYearBusiness academicYearBusiness)
		{
			this.MovementAcceptanceBusiness = movementacceptanceBusiness;
            this.SchoolMovementBusiness = schoolMovementBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
		}

        private void SetViewData()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ViewData[MovementAcceptanceConstants.LIST_SEMESTER] = CommonFunctions.GetListSemester();
            List<EducationLevel> ListEducationLevel = GlobalInfo.EducationLevels;
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }
            ViewData[MovementAcceptanceConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution");

            // Danh sách lớp chuyen den
            List<ClassProfile> ListClassProfile = new List<ClassProfile>();
            ViewData[MovementAcceptanceConstants.LIST_CLASS] = new SelectList(ListClassProfile, "ClassProfileID", "DisplayName");
            
        
        }

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SetViewDataPermission("MovementAcceptance", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            Search();
            SetViewData();
            return View();
        }

		//
        // GET: /MovementAcceptance/Search

        
        public PartialViewResult Search()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IEnumerable<MovementAcceptanceViewModel> lst = this._Search(SearchInfo);
            ViewData[MovementAcceptanceConstants.LIST_MOVEMENTACCEPTANCE] = lst;
            SetViewDataPermission("MovementAcceptance", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            GlobalInfo Global = new GlobalInfo();
            if (GetMenupermission("MovementAcceptance", Global.UserAccountID, Global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

            MovementAcceptance movementacceptance = new MovementAcceptance();
            TryUpdateModel(movementacceptance); 
            Utils.Utils.TrimObject(movementacceptance);
            using (TransactionScope scopce = new TransactionScope())
            {
                movementacceptance.MovedDate = DateTime.Now;
                movementacceptance.SchoolID = Global.SchoolID.Value;
                movementacceptance.AcademicYearID = Global.AcademicYearID.Value;

                this.MovementAcceptanceBusiness.InsertMovementAcceptance(Global.UserAccountID, movementacceptance, isMovedHistory);
                this.MovementAcceptanceBusiness.Save();
                SchoolMovement sm = new SchoolMovement();
                TryUpdateModel(sm);
                int SchoolMovementID = sm.SchoolMovementID;
                SchoolMovement SchoolMovement = SchoolMovementBusiness.Find(SchoolMovementID);
                SchoolMovement.MovedToClassID = movementacceptance.ClassID;
                SchoolMovementBusiness.UpdateSchoolMovement(Global.UserAccountID, SchoolMovement);
                this.SchoolMovementBusiness.Save();
                
                scopce.Complete();
            }
            return Json(new JsonMessage(Res.Get("MovementAcceptance_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int MovementAcceptanceID)
        {
            GlobalInfo Global = new GlobalInfo();
            if (GetMenupermission("MovementAcceptance", Global.UserAccountID, Global.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            MovementAcceptance movementacceptance = this.MovementAcceptanceBusiness.Find(MovementAcceptanceID);
            TryUpdateModel(movementacceptance);
            Utils.Utils.TrimObject(movementacceptance);
            using (TransactionScope scope = new TransactionScope())
            {


                this.MovementAcceptanceBusiness.Update(movementacceptance);
                this.MovementAcceptanceBusiness.Save();
                scope.Complete();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo Global = new GlobalInfo();
            if (GetMenupermission("MovementAcceptance", Global.UserAccountID, Global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.MovementAcceptanceBusiness.Delete(id);
            this.MovementAcceptanceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<MovementAcceptanceViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            SearchInfo["MovedToSchoolID"] = GlobalInfo.SchoolID;
            SearchInfo["AppliedLevel"] = GlobalInfo.AppliedLevel;
            AcademicYear acaYear = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID.Value);
            var lstQSchool = SchoolMovementBusiness.Search(SearchInfo).Where(u => !u.MovedToClassID.HasValue && u.MovedDate >= acaYear.FirstSemesterStartDate && u.MovedDate <= acaYear.SecondSemesterEndDate);
            List<SchoolMovement> lstSchool = lstQSchool.ToList();
            List<MovementAcceptanceViewModel> lst = lstSchool.Select(o => new MovementAcceptanceViewModel
            {
                FullName = o.PupilProfile.FullName,
                PupilCode = o.PupilProfile.PupilCode,
                BirthDate = o.PupilProfile.BirthDate,
                Genre = CommonConvert.Genre(o.PupilProfile.Genre),
                SchoolID = o.SchoolID,
                SchoolName = o.SchoolProfile.SchoolName,
                Semester = o.Semester,
                EducationLevelID = o.EducationLevelID,
                Resolution = o.EducationLevel.Resolution,
                ClassID = o.ClassID,
                MovedToClassID = o.MovedToClassID,
                MovedFromClassID = o.ClassID,
                ClassName = null,
                canEdit = GlobalInfo.IsCurrentYear,
                PupilID = o.PupilProfile.PupilProfileID,
                SchoolMovementID = o.SchoolMovementID
            }).ToList();
            return lst;
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            GlobalInfo Global = new GlobalInfo();
            IEnumerable<ClassProfile> lst = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = EducationLevelID;
                dic["AcademicYearID"] = Global.AcademicYearID;

  //              Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
  //+ Dictionnary[“UserAccountID”] = UserInfo.UserAccountID   
  //                     + Dictionary[“Type”] = TEACHER_ROLE_HEADTEACHER\
                if(!Global.IsAdminSchoolRole)
                {
                    dic["UserAccountID"] = Global.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                }
                lst = this.ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }
    }
}





