/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using System.ComponentModel;
using SMAS.Models.CustomAttribute;


namespace SMAS.Web.Areas.MovementAcceptanceArea.Models
{
    public class MovementAcceptanceViewModel
    {
		public System.Int32 MovementAcceptanceID { get; set; }
								
		public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MovementAcceptanceConstants.LIST_CLASS)]				
		public int? MovedToClassID { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_FullName")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string FullName { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_PupilCode")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string PupilCode { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_BirthDate")]
        [UIHint("Display")]
        [ReadOnly(true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? BirthDate { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_Genre")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string Genre { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_School")]
        [UIHint("Display")]
        [ReadOnly(true)]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_Semester")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public System.Nullable<System.Int32> Semester { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_EducationLevel")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string Resolution { get; set; }

        [ResourceDisplayName("MovementAcceptance_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [AdditionalMetadata("ViewDataKey", MovementAcceptanceConstants.LIST_CLASS)]	
        public string ClassName { get; set; }

        public int SchoolMovementID { get; set; }

        public bool canEdit { get; set; }

        public int ClassID { get; set; }

		public System.Int32 EducationLevelID { get; set; }	
							
		public System.Int32 SchoolID { get; set; }	
							
		public System.Int32 AcademicYearID { get; set; }	
							
		public System.Nullable<System.Int32> MovedFromClassID { get; set; }								
		public System.Nullable<System.Int32> MovedFromSchoolID { get; set; }
								
		public System.String MovedFromClassName { get; set; }
								
		public System.String MovedFromSchoolName { get; set; }	
							
		public System.DateTime MovedDate { get; set; }	
							
		public System.String Description { get; set; }								
	       
    }
}


