/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.MarkRecordArea.Models
{
    public class MarkRecordViewModel
    {
        public string PupilCode { get; set; }
        public int PupilID { get; set; }
        public string Fullname { get; set; }
        public string SummedUpMark { get; set; }
        public int Status { get; set; }
        public decimal[] InterviewMark { get; set; }
        public decimal[] WritingMark { get; set; }
        public decimal[] TwiceCoeffiecientMark { get; set; }
        public string Comment { get; set; }
        public string Disable { get; set; }
        public bool? Pass { get; set; }
        public string Note { get; set; }
        public string MarkTBM { get; set; }
        public int? SubjectID { get; set; }
        public int? ClassID { get; set; }
        public int? PeriodID { get; set; }
        public int? SemesterID { get; set; }
        public bool DisplayMark { get; set; }
        public bool flag { get; set; }
        public string SummedUpMarkIncrease { get; set; }//TBM tang cuong
        public int? isCommenting { get; set; }
        public string[] LogMarkM { get; set; }
        public string[] LogMarkP { get; set; }
        public string[] LogMarkV { get; set; }
        public string LogMarkHK { get; set; }

        public Dictionary<string, string> ListMark { get; set; }
        public decimal? MarkSemester { get; set; }
    }
}


