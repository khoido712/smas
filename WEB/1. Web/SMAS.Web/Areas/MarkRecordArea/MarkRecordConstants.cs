/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.MarkRecordArea
{
    public class MarkRecordConstants
    {
        public const string LIST_MARKRECORD = "listMarkRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_PERIOD = "listPeriod";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SUBJECT = "listSubject";
        public const string SemesterID = "SemesterID";
        public const string EducationLevelID = "EducationLevelID";
        public const string ClassID = "ClassID";
        public const string SubjectID = "SubjectID";
        public const string ShowList = "ShowList";
        public const string PeriodId = "PeriodId";
        public const string Title = "Title";
        public const string InterviewMark = "InterviewMark";
        public const string StartIndexOfInterviewMark = "StartIndexOfInterviewMark";
        public const string WritingMark = "WritingMark";
        public const string StartIndexOfWritingMark = "StartIndexOfWritingMark";
        public const string TwiceCoeffiecientMark = "TwiceCoeffiecientMark";
        public const string StartIndexOfTwiceCoeffiecientMark = "StartIndexOfTwiceCoeffiecientMark";
        public const string lblLockInfo = "lblLockInfo";
        public const string colComment = "colComment";
        public const string colSemester = "COLSEMESTER";
        public const string listlockmark = "listlockmark";
        public const string IsExemptedSubject = "IsExemptedSubject";
        public const string TeacherPermision = "teacherPermision";
        public const string TYPE_REPORT = "TypeReport";

        public const string STR_INTERVIEW_MARK = "strInterviewMark";
        public const string STR_WRITINGVIEW_MARK = "strWritingViewMark";
        public const string STR_TWICEVIEW_MARK = "strTwiceViewMark";


        public const string titleImport = "TitleImport";
        public const string listMarkRecordViewModel = "listMarkRecordViewModel";
        public const string ListTypeMarkMImport = "ListTypeMarkMImport";
        public const string ListTypeMarkPImport = "ListTypeMarkPImport";
        public const string ListTypeMarkVImport = "ListTypeMarkVImport";

        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string CLASS_12 = "CLASS_12";
        public const string SEMESTER_1 = "SEMESTER_1";

        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";

        public const string ERROR_IMPORT = "ERROR_IMPORT";

        public const string LOCK_IMPORT = "LOCK_IMPORT";
        public const string COUNT = "count";
        public const string ENABLE_ENTRYMARK = "enable_entryMark";

        public const string COLMARKSEMESTER = "COLMARKSEMESTER";

        public const string CoefficientM = "CoefficientM";
        public const string CoefficientP = "CoefficientP";
        public const string CoefficientV = "CoefficientV";
        public const string CoefficientHK = "CoefficientHK";

        public const string IS_CREASE = "IsCrease";
        public const string SUBJECT_CREASE_NAME = "SubjectCreaseName";

        public const string LAST_UPDATE_MARK_RECORD = "LAST_UPDATE_MARK_RECORD";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string TITLE_STRING = "TITLE_STRING";
        public const string LIST_LOCK_TITLE = "LIST_LOCK_TITLE";
        public const string LIST_CHANGE_MARK = "LIST_CHANGE_MARK";
        public const string IS_LOCK_INPUT = "IsLockInput";
        public const string LOCK_USER_NAME = "LockUserName";
        
    }
}