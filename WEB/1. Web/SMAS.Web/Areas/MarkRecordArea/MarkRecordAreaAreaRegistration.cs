﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkRecordArea
{
    public class MarkRecordAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkRecordArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkRecordArea_default",
                "MarkRecordArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
