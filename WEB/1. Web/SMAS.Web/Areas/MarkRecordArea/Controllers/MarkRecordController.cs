﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MarkRecordArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.MarkRecordArea;
using SMAS.Business.BusinessObject;
using System.Threading;
using System.Globalization;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Models.Models.CustomModels;
using System.Text;
using System.Configuration;
using SMAS.VTUtils.Log;
using Newtonsoft.Json;

namespace SMAS.Web.Areas.MarkRecordArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class MarkRecordController : BaseController
    {
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        public const int STATUS_MARK_NO_DISPLAY = 12;
        public const int STATUS_MARK_NO_HAS = 11;

        public MarkRecordController(IMarkRecordBusiness markrecordBusiness,
                                    IMarkRecordHistoryBusiness markRecordHistoryBusiness,
                                    ISummedUpRecordHistoryBusiness summedUpRecordHistoryBusiness,
                                    IPeriodDeclarationBusiness PeriodDeclarationBusiness,
                                    IClassProfileBusiness ClassProfileBusiness,
                                    IClassSubjectBusiness ClassSubjectBusiness,
                                    ITeachingAssignmentBusiness TeachingAssignmentBusiness,
                                    ISubjectCatBusiness SubjectCatBusiness,
                                    ISummedUpRecordBusiness SummedUpRecordBusiness,
                                    IPupilOfClassBusiness PupilOfClassBusiness,
                                    IPupilProfileBusiness PupilProfileBusiness,
                                    ILockedMarkDetailBusiness LockedMarkDetailBusiness,
                                    IMarkTypeBusiness MarkTypeBusiness,
                                    IExemptedSubjectBusiness ExemptedSubjectBusiness,
                                    IAcademicYearBusiness AcademicYearBusiness,
                                    IUserAccountBusiness UserAccountBusiness,
                                    ISemeterDeclarationBusiness SemesterDeclarationBusiness,
                                    IJudgeRecordBusiness judgerecordBusiness,
                                    IJudgeRecordHistoryBusiness judgeRecordHistoryBusiness,
                                    IEmployeeBusiness employeeBusiness,
                                    IRestoreDataBusiness RestoreDataBusiness,
                                    IRestoreDataDetailBusiness RestoreDataDetailBusiness,
                                    ISchoolProfileBusiness SchoolProfileBusiness
            )
        {
            this.MarkRecordBusiness = markrecordBusiness;
            this.MarkRecordHistoryBusiness = markRecordHistoryBusiness;
            this.SummedUpRecordHistoryBusiness = summedUpRecordHistoryBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.LockedMarkDetailBusiness = LockedMarkDetailBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.SemesterDeclarationBusiness = SemesterDeclarationBusiness;
            this.JudgeRecordBusiness = judgerecordBusiness;
            this.JudgeRecordHistoryBusiness = judgeRecordHistoryBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
            this.RestoreDataDetailBusiness = RestoreDataDetailBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }

        //
        // GET: /MarkRecord/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            if (_globalInfo.Semester.Value == 1)
                semester1 = true;
            else semester2 = true;
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[MarkRecordConstants.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in _globalInfo.EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[MarkRecordConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["Semester"] = _globalInfo.Semester.Value;

            IEnumerable<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
            List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
            if (lstPeriod != null && lstPeriod.Count() > 0)
            {
                foreach (var item in lstPeriod)
                {
                    PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                    periodDeclaration = item;
                    if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";

                    listPeriodDeclaration.Add(periodDeclaration);
                }
            }


            int period = 0;
            //Chiendd: Sua Dot hien tai
            if (listPeriodDeclaration != null && listPeriodDeclaration.Count() != 0)
            {
                PeriodDeclaration objCurrentPeriod = listPeriodDeclaration.FirstOrDefault(s => s.FromDate <= DateTime.Now && DateTime.Now <= s.EndDate);
                if (objCurrentPeriod != null)
                {
                    period = objCurrentPeriod.PeriodDeclarationID;
                }
                else
                {
                    period = listPeriodDeclaration.FirstOrDefault().PeriodDeclarationID;
                }

            }

            //ViewData[MarkRecordConstants.LIST_PERIOD] = listPeriodDeclaration;
            ViewData[MarkRecordConstants.LIST_PERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", period);

            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["EducationLevelID"] = listEducationLevel.FirstOrDefault().Value;
                if (_globalInfo.IsAdminSchoolRole == false && !_globalInfo.IsViewAll)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
                ViewData[MarkRecordConstants.LIST_CLASS] = listClass;
            }

            ViewData[MarkRecordConstants.LIST_SUBJECT] = null;

            ViewData[MarkRecordConstants.ShowList] = false;



            IEnumerable<MarkRecordViewModel> lst = null;
            ViewData[MarkRecordConstants.LIST_MARKRECORD] = lst;


            ViewData[MarkRecordConstants.HAS_ERROR_DATA] = false;
            ViewData[MarkRecordConstants.LIST_IMPORTDATA] = null;

            ViewData[MarkRecordConstants.CLASS_12] = false;
            ViewData[MarkRecordConstants.SEMESTER_1] = true;
            ViewData[MarkRecordConstants.ERROR_BASIC_DATA] = false;
            ViewData[MarkRecordConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[MarkRecordConstants.ERROR_IMPORT] = true;
            ViewData[MarkRecordConstants.ENABLE_ENTRYMARK] = "true";
            return View();

        }

        public void GetViewDataForImport()
        {
            ViewData[MarkRecordConstants.LIST_SUBJECT] = null;
            ViewData[MarkRecordConstants.ShowList] = false;
            ViewData[MarkRecordConstants.HAS_ERROR_DATA] = false;
            ViewData[MarkRecordConstants.LIST_IMPORTDATA] = null;

            ViewData[MarkRecordConstants.CLASS_12] = false;
            ViewData[MarkRecordConstants.SEMESTER_1] = true;
            ViewData[MarkRecordConstants.ERROR_BASIC_DATA] = false;
            ViewData[MarkRecordConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[MarkRecordConstants.ERROR_IMPORT] = true;
            ViewData[MarkRecordConstants.ENABLE_ENTRYMARK] = "true";
        }


        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboPeriod(int? SemesterID)
        {
            if (SemesterID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["Semester"] = SemesterID.Value;
                IEnumerable<PeriodDeclaration> listPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
                List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
                if (listPeriod != null && listPeriod.Count() > 0)
                {
                    foreach (var item in listPeriod)
                    {
                        PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                        periodDeclaration = item;
                        if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                            periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                        listPeriodDeclaration.Add(periodDeclaration);
                    }
                }
                ViewData[MarkRecordConstants.LIST_SEMESTER] = SemesterID.Value;
                ViewData[MarkRecordConstants.ShowList] = false;
                return Json(new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution"));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult GetClassPanel(int? id)
        {
            if (id.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["EducationLevelID"] = id.Value;
                if (_globalInfo.IsAdminSchoolRole == false && !_globalInfo.IsViewAll)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                ViewData[MarkRecordConstants.EducationLevelID] = id.Value;
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
                ViewData[MarkRecordConstants.LIST_CLASS] = listClass;


            }
            return PartialView("_ListClass");
        }

        [HttpPost]
        public PartialViewResult GetSubjectPanel()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int semester = semesterid == null ? 0 : semesterid.Value;
            ViewData[MarkRecordConstants.ClassID] = classid.Value;

            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (classid.HasValue && semesterid.HasValue)
            {
                lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value,
                    _globalInfo.SchoolID.Value, semesterid.Value, classid.Value, _globalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                    .Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .ToList();
            }
            List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();


            foreach (ClassSubject item in lsClassSubject)
            {
                listSJ.Add(new ViettelCheckboxList(item.SubjectCat.DisplayName, item.SubjectID, false, false));
            }

            ViewData[MarkRecordConstants.LIST_SUBJECT] = listSJ;
            ViewData[MarkRecordConstants.ShowList] = false;
            return PartialView("_ListSubject");
        }


        [HttpPost]

        public PartialViewResult GetDataNew()
        {
            int? subjectid = SMAS.Business.Common.Utils.GetInt(Request["subjectid"]);
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["classid"]);
            int? educationlevelid = SMAS.Business.Common.Utils.GetInt(Request["educationlevelid"]);
            int schoolID = _globalInfo.SchoolID.Value;
            int academicYearID = _globalInfo.AcademicYearID.Value;
            SchoolProfile objSP = SchoolProfileBusiness.Find(schoolID);
            if (Request["periodid"] == "null")
            {
                throw new BusinessException("Common_Null_PeriodID");
            }
            int? periodid = SMAS.Business.Common.Utils.GetInt(Request["periodid"]);
            int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["semesterid"]);
            string classname = Request["classname"];
            GetViewDataForImport();

            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(periodid);
            if (Period == null)
                throw new BusinessException("Validate_School_NotMarkPeriod");
            bool permission = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, classid.Value, subjectid.Value, semesterid.GetValueOrDefault());
            if (_globalInfo.IsCurrentYear == false)
            {
                ViewData[MarkRecordConstants.LOCK_IMPORT] = "disabled";
            }
            else
            {

                if (permission)
                {
                    if (_globalInfo.IsAdminSchoolRole == true)
                    {
                        ViewData[MarkRecordConstants.LOCK_IMPORT] = "";
                    }
                    else if (!_globalInfo.isCurrentSemester(aca, semesterid.Value) || Period.IsLock == true)
                    {
                        //Neu hoc ky da ket thuc thi khong cho nhap diem
                        //Neu dot bi khoa thi khong cho nhap diem
                        ViewData[MarkRecordConstants.LOCK_IMPORT] = "disabled";
                    }

                    else if (_globalInfo.isCurrentSemester(aca, semesterid.Value) && Period.IsLock == false && Period.EndDate < DateTime.Now && Period.IsEditMark == true)
                    {
                        //Neu la hoc ky hien tai, dot da ket thuc , 
                        //dot chua bi khoa va giao vien duoc phep nhap diem thì cho phep nhap diem
                        ViewData[MarkRecordConstants.LOCK_IMPORT] = "";
                    }
                    else if (Period.FromDate.Value.Date <= DateTime.Now.Date && DateTime.Now.Date <= Period.EndDate.Value.Date && Period.IsLock == false)
                    {
                        //Neu la la dot ien tai va chua bi khoa thi cho phep nhap diem
                        ViewData[MarkRecordConstants.LOCK_IMPORT] = "";
                    }
                    else
                    {
                        ViewData[MarkRecordConstants.LOCK_IMPORT] = "disabled";
                    }

                }
                else
                {
                    ViewData[MarkRecordConstants.LOCK_IMPORT] = "disabled";
                }
            }

            //truyen gia tri bien an vao form
            ViewData[MarkRecordConstants.ClassID] = classid.Value.ToString();
            ViewData[MarkRecordConstants.SubjectID] = subjectid.Value.ToString();
            ViewData[MarkRecordConstants.PeriodId] = periodid.Value.ToString();
            ViewData[MarkRecordConstants.SemesterID] = semesterid.Value.ToString();

            SubjectCat subject = SubjectCatBusiness.Find(subjectid);

            // kiem tra quyen cua giao vien chu nhiem va thoi gian hien tai phai nho hon ngay cuoi cung cua dot
            ViewData[MarkRecordConstants.TeacherPermision] = permission && Period.EndDate <= DateTime.Now.Date;
            ViewData[MarkRecordConstants.Title] = Res.Get("MarkRecordPeriod_Label_HeaderGrid") + " " + subject.DisplayName.ToUpper() + " - " + Period.Resolution.ToUpper();
            ViewData[MarkRecordConstants.ShowList] = true;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //kiem tra xem mon hoc co phai mon tang cuong hay khong
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(schoolID, new Dictionary<string, object>()
                                                                               {
                                                                                   {"AcademicYearID",academicYearID},
                                                                                   {"ClassID",classid},
                                                                                   {"EducationLevelID",educationlevelid}
                                                                                   //{"SubjectID",subjectid}
                                                                               }).ToList();
            ClassSubject objClassSubject = lstClassSubject.Where(p => p.SubjectID == subjectid).FirstOrDefault();
            ClassSubject objClassSubjectTC = null;
            bool isInCrease = false;
            if (semesterid == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                if (objClassSubject != null && objClassSubject.SubjectIDIncrease > 0)
                {
                    objClassSubjectTC = lstClassSubject.Where(p => p.SubjectID == objClassSubject.SubjectIDIncrease).FirstOrDefault();
                    isInCrease = objClassSubjectTC != null && objClassSubjectTC.SectionPerWeekFirstSemester > 0;
                }
            }
            else if (semesterid == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                if (objClassSubject != null && objClassSubject.SubjectIDIncrease > 0)
                {
                    objClassSubjectTC = lstClassSubject.Where(p => p.SubjectID == objClassSubject.SubjectIDIncrease).FirstOrDefault();
                    isInCrease = objClassSubjectTC != null && objClassSubjectTC.SectionPerWeekSecondSemester > 0;
                }
            }
            ViewData[MarkRecordConstants.IS_CREASE] = isInCrease;
            List<MarkRecordNewBO> listIncrease = new List<MarkRecordNewBO>();
            if (isInCrease)//la mon tang cuong
            {
                //lay diem mon tang cuong
                dic["AcademicYearID"] = academicYearID;
                dic["SchoolID"] = schoolID;
                dic["SubjectID"] = objClassSubject.SubjectIDIncrease;
                dic["Semester"] = semesterid;
                dic["ClassID"] = classid;
                dic["PeriodID"] = periodid;
                dic["Check"] = "Check";
                var tmp = MarkRecordBusiness.GetMardRecordOfClassNew(dic);
                if (tmp != null && tmp.Count() > 0)
                {
                    listIncrease = tmp.ToList();
                }
                ViewData[MarkRecordConstants.SUBJECT_CREASE_NAME] = objClassSubjectTC.SubjectCat.DisplayName;

            }
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["SchoolID"] = schoolID;
            dic["SubjectID"] = subjectid;
            dic["Semester"] = semesterid;
            dic["ClassID"] = classid;
            dic["PeriodID"] = periodid;
            dic["Check"] = "Check";
            //Lay diem moi

            bool isNotShowPupil = aca.IsShowPupil.HasValue && aca.IsShowPupil.Value;
            Dictionary<string, object> dicSearchPOC = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"ClassID",classid}
            };
            IQueryable<PupilOfClass> iqueryPOC = PupilOfClassBusiness.Search(dicSearchPOC).AddPupilStatus(isNotShowPupil);
            List<int> lstPupilID = iqueryPOC.Select(p => p.PupilID).Distinct().ToList();

            List<MarkRecordNewBO> ListMR = new List<MarkRecordNewBO>();
            var listtmp = MarkRecordBusiness.GetMardRecordOfClassNew(dic);
            if (listtmp != null)
            {
                listtmp = listtmp.Where(p => lstPupilID.Contains(p.PUPIL_ID)).ToList();
            }
            string MarkTitle = MarkRecordBusiness.GetMarkTitle(periodid.Value) + ",";

            //lay ra danh logchange cua hoc sinh theo tung con diem
            List<BookRecordLogChangeBO> listLogChange = new List<BookRecordLogChangeBO>();
            if (UtilsBusiness.IsMoveHistory(aca))
            {
                listLogChange = (from mr in MarkRecordHistoryBusiness.All
                                 where mr.AcademicYearID == _globalInfo.AcademicYearID
                                 && mr.SchoolID == _globalInfo.SchoolID
                                 && mr.ClassID == classid
                                 && mr.SubjectID == subjectid
                                 && mr.Semester == semesterid
                                 && mr.Last2digitNumberSchool == _globalInfo.SchoolID % 100
                                 && lstPupilID.Contains(mr.PupilID)
                                 && MarkTitle.Contains(mr.Title)
                                 select new BookRecordLogChangeBO
                                 {
                                     PupilID = mr.PupilID,
                                     Title = mr.Title,
                                     LogChange = mr.LogChange,
                                     ModifiedDate = mr.ModifiedDate,
                                     CreateDate = mr.CreatedDate,
                                     UpdateDate = mr.ModifiedDate.HasValue ? mr.ModifiedDate : mr.CreatedDate
                                 }).Where(o => o.UpdateDate != null).ToList();
            }
            else
            {
                listLogChange = (from mr in MarkRecordBusiness.All
                                 where mr.AcademicYearID == _globalInfo.AcademicYearID
                                 && mr.SchoolID == _globalInfo.SchoolID
                                 && mr.ClassID == classid
                                 && mr.SubjectID == subjectid
                                 && mr.Semester == semesterid
                                 && mr.Last2digitNumberSchool == _globalInfo.SchoolID % 100
                                 && lstPupilID.Contains(mr.PupilID)
                                 && MarkTitle.Contains(mr.Title)
                                 select new BookRecordLogChangeBO
                                 {
                                     PupilID = mr.PupilID,
                                     Title = mr.Title,
                                     LogChange = mr.LogChange,
                                     ModifiedDate = mr.ModifiedDate,
                                     CreateDate = mr.CreatedDate,
                                     UpdateDate = mr.ModifiedDate.HasValue ? mr.ModifiedDate : mr.CreatedDate
                                 }).Where(o => o.UpdateDate != null).ToList();
            }
            List<int?> lstEmployeeID = listLogChange.Select(p => p.LogChange).Distinct().ToList();

            //viethd4: lay ra cac cap nhat gan nhat
            var lastUpdate = listLogChange.OrderByDescending(o => o.UpdateDate).FirstOrDefault();
            List<MarkRecordBO> lstLastMarkRecordID = new List<MarkRecordBO>();

            if (lastUpdate != null)
            {
                string lastUpdateTime = lastUpdate.UpdateDate != null ? lastUpdate.UpdateDate.Value.ToString("MM/dd/yyyy hh:mm") : null;
                lstLastMarkRecordID = listLogChange.Where(o => lastUpdateTime != null && o.UpdateDate != null ? o.UpdateDate.Value.ToString("MM/dd/yyyy hh:mm") == lastUpdateTime : false).Select(o =>
                                    new MarkRecordBO
                                    {
                                        PupilID = o.PupilID,
                                        Title = o.Title
                                    }).ToList();

            }
            ViewData[MarkRecordConstants.LAST_UPDATE_MARK_RECORD] = lstLastMarkRecordID;

            List<Employee> lstEmployeeChange = new List<Employee>();
            if (lstEmployeeID.Count > 0)
            {
                lstEmployeeChange = (from e in EmployeeBusiness.All
                                     where e.SchoolID == _globalInfo.SchoolID
                                     && lstEmployeeID.Contains(e.EmployeeID)
                                     select e).ToList();
            }
            Employee objEmployee = null;

            //viethd4: lay user va time cap nhat sau cung

            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            if (lastUpdate != null)
            {
                string lastUserName = String.Empty;
                if (lastUpdate.LogChange == 0)
                {
                    lastUserName = "Quản trị trường";
                }
                else
                {
                    Employee lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdate.LogChange).FirstOrDefault();
                    if (lastUser != null)
                    {
                        lastUserName = lastUser.FullName;
                    }
                }

                tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                        lastUpdate.UpdateDate.Value.Hour.ToString(),
                        lastUpdate.UpdateDate.Value.Minute.ToString("D2"),
                        lastUpdate.UpdateDate,
                        !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                        lastUserName});

                strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);
            }

            ViewData[MarkRecordConstants.LAST_UPDATE_STRING] = strLastUpdate;

            if (listtmp != null && listtmp.Count() > 0)
            {
                ListMR = listtmp.OrderBy(i => i.ORDER_IN_CLASS).ThenBy(i => i.S_NAME).ThenBy(i => i.FULL_NAME).ToList();
            }
            //List<MarkRecordNewBO> lstMarkRecordOfClass = new List<MarkRecordNewBO>();
            //if (ListMR != null && ListMR.Count() > 0)
            //    lstMarkRecordOfClass = ListMR.ToList().OrderBy(i => i.ORDER_IN_CLASS).ThenBy(i => i.S_NAME).ThenBy(i => i.FULL_NAME).ToList();
            // danh sach diem trung binh trong lop
            List<int?> listSemester = new List<int?>() { semesterid };
            if (semesterid.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                if (semesterid.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    listSemester.Add(SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                }
            }

            // AnhVD 20131216 - Lay He so cac con diem
            SemeterDeclaration SemeterDeclaration = SemesterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            if (SemeterDeclaration != null)
            {
                ViewData[MarkRecordConstants.CoefficientM] = SemeterDeclaration.CoefficientInterview;
                ViewData[MarkRecordConstants.CoefficientP] = SemeterDeclaration.CoefficientWriting;
                ViewData[MarkRecordConstants.CoefficientV] = SemeterDeclaration.CoefficientTwice;
                ViewData[MarkRecordConstants.CoefficientHK] = SemeterDeclaration.CoefficientSemester;
            }

            // End 20131212

            List<ExemptedSubject> lstExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(classid.Value, semesterid.Value).ToList();

            List<MarkRecordViewModel> ListMarkRecordViewModel = new List<MarkRecordViewModel>();

            //truyem so con diem mieng
            ViewData[MarkRecordConstants.InterviewMark] = Period.InterviewMark;
            ViewData[MarkRecordConstants.WritingMark] = Period.WritingMark;
            ViewData[MarkRecordConstants.TwiceCoeffiecientMark] = Period.TwiceCoeffiecientMark;

            ViewData[MarkRecordConstants.STR_INTERVIEW_MARK] = !string.IsNullOrEmpty(Period.StrInterviewMark) ? Period.StrInterviewMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
            ViewData[MarkRecordConstants.STR_WRITINGVIEW_MARK] = !string.IsNullOrEmpty(Period.StrWritingMark) ? Period.StrWritingMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
            ViewData[MarkRecordConstants.STR_TWICEVIEW_MARK] = !string.IsNullOrEmpty(Period.StrTwiceCoeffiecientMark) ? Period.StrTwiceCoeffiecientMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;


            int InterviewMark = Period.InterviewMark.Value;
            //int StartIndexOfInterviewMark = Period.StartIndexOfInterviewMark.Value;
            int WritingMark = Period.WritingMark.Value;
            //int StartIndexOfWritingMark = Period.StartIndexOfWritingMark.Value;
            int TwiceCoeffiecientMark = Period.TwiceCoeffiecientMark.Value;
            //int StartIndexOfTwiceCoeffiecientMark = Period.StartIndexOfTwiceCoeffiecientMark.Value;

            dic = new Dictionary<string, object>();
            dic["SubjectID"] = subjectid;
            dic["Semester"] = semesterid;

            //ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classid.Value, dic).SingleOrDefault();
            //SubjectCat sc = SubjectCatBusiness.Find(subjectid);

            ViewData[MarkRecordConstants.colComment] = subject != null && subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE;

            dic = new Dictionary<string, object>();
            dic["ClassID"] = classid;
            dic["SubjectID"] = subjectid;
            dic["Semester"] = semesterid;

            int semesterid_m = semesterid.Value;
            int subjectid_m = subjectid.Value;
            string strLockMark = MarkRecordBusiness.GetLockMarkTitle(schoolID, academicYearID, classid.Value, semesterid_m, subjectid_m, periodid.Value);
            string[] listLockMark;
            string strLockMarkHas = MarkRecordBusiness.GetMarkTitle(periodid.Value);
            string[] listMarkPeriod = null;

            if (strLockMarkHas != null && strLockMarkHas.Count() > 0)
            {
                //cat cac con diem boi dau ','
                listMarkPeriod = strLockMarkHas.Split(new Char[] { ',' });
                if (listMarkPeriod.Contains("HK"))
                {
                    ViewData[MarkRecordConstants.COLMARKSEMESTER] = true;
                }
                else
                {
                    ViewData[MarkRecordConstants.COLMARKSEMESTER] = false;
                }
            }
            else
            {
                ViewData[MarkRecordConstants.COLMARKSEMESTER] = false;
            }
            ViewData[MarkRecordConstants.lblLockInfo] = "";

            // dua da sach cac con diem bi khoa
            string lockMark = "";
            if (strLockMark != null && strLockMark.Count() > 0)
            {
                listLockMark = strLockMark.Split(new Char[] { ',' });

                if (listLockMark.Count() > 0)
                {
                    lockMark = strLockMark;
                    if (listLockMark.Contains("LHK"))
                    {
                        ViewData[MarkRecordConstants.lblLockInfo] = Res.Get("MarkRecordPeriod_Label_Note3");
                    }
                    else
                    {
                        ViewData[MarkRecordConstants.lblLockInfo] = Res.Get("MarkRecordPeriod_Label_Note") + " " + lockMark + " " + Res.Get("MarkRecordPeriod_Label_Note2");
                    }
                }

            }

            //TungNT48 30/03/2013 start edit
            ViewData[MarkRecordConstants.listlockmark] = _globalInfo.IsAdminSchoolRole ? "" : lockMark;

            string tmpStrLockMark = MarkRecordBusiness.GetLockMarkTitle(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classid.Value, semesterid_m, subjectid_m);
            ViewData[MarkRecordConstants.LIST_LOCK_TITLE] = tmpStrLockMark.Split(new Char[] { ',' }).ToList();
            //end
            MarkRecordNewBO objIncrease = null;
            AcademicYear academicYear = AcademicYearBusiness.Find(Period.AcademicYearID);

            if (ListMR != null && ListMR.Count > 0)
            {
                foreach (MarkRecordNewBO itemMR in ListMR)
                {

                    MarkRecordViewModel mrv = new MarkRecordViewModel();
                    objEmployee = null;
                    mrv.PupilCode = "";
                    mrv.Fullname = itemMR.FULL_NAME;
                    mrv.Status = itemMR.STATUS;
                    mrv.Disable = "";
                    mrv.PupilID = itemMR.PUPIL_ID;
                    mrv.DisplayMark = false;
                    mrv.flag = false;
                    mrv.InterviewMark = new decimal[InterviewMark + 1];
                    mrv.WritingMark = new decimal[WritingMark + 1];
                    mrv.TwiceCoeffiecientMark = new decimal[TwiceCoeffiecientMark + 1];
                    mrv.LogMarkM = new string[InterviewMark + 1];
                    mrv.LogMarkP = new string[WritingMark + 1];
                    mrv.LogMarkV = new string[TwiceCoeffiecientMark + 1];

                    int InterviewMarkCount = mrv.InterviewMark.Count();
                    int WritingMarkCount = mrv.WritingMark.Count();
                    int TwiceCoeffiecientMarkCount = mrv.TwiceCoeffiecientMark.Count();

                    DateTime? endSemester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate : academicYear.SecondSemesterEndDate;
                    DateTime? endDate = itemMR.END_DATE;

                    bool showPupilData = false;

                    if (semesterid == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    {
                        if (itemMR.STATUS == SystemParamsInFile.PUPIL_STATUS_STUDYING || (itemMR.STATUS == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                        {
                            showPupilData = true;
                        }
                    }
                    else
                    {
                        showPupilData = itemMR.STATUS == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                        || itemMR.STATUS == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                        || (itemMR.STATUS == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (itemMR.STATUS == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (itemMR.STATUS == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                    }

                    if (itemMR.STATUS != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        mrv.Disable = "disabled";

                    if (!showPupilData)
                    {
                        mrv.SummedUpMark = "";
                        mrv.MarkSemester = null;
                        mrv.Comment = "";
                        mrv.DisplayMark = false;
                    }

                    //dat la 11 de se khong hien thi neu diem chua nhap
                    for (int i = 0; i < InterviewMarkCount; i++)
                    {
                        mrv.InterviewMark[i] = STATUS_MARK_NO_HAS;
                    }
                    for (int i = 0; i < WritingMarkCount; i++)
                    {
                        mrv.WritingMark[i] = STATUS_MARK_NO_HAS;
                    }
                    for (int i = 0; i < TwiceCoeffiecientMarkCount; i++)
                    {
                        mrv.TwiceCoeffiecientMark[i] = STATUS_MARK_NO_HAS;
                    }

                    if (showPupilData)
                    {
                        mrv.DisplayMark = true;
                        mrv.flag = true;
                        //lay diem vao tung dau diem
                        //List<MarkRecordBO> listPupilMarkRecord = lstMarkRecordOfClass.Where(o => o.PupilID == itemMR.PupilID).OrderBy(o => o.OrderNumber).ToList();
                        int indexInterviewMark = 1;
                        int indexWritingMark = 1;
                        int indexTwiceCoeffiecientMark = 1;

                        foreach (string TitleMarkPeriod in listMarkPeriod)
                        {
                            object Mark = Utils.Utils.GetValueByProperty(itemMR, TitleMarkPeriod);
                            if (Mark != null)
                            {
                                mrv.Comment = itemMR.S_COMMENT;
                                if (TitleMarkPeriod.Trim().Length > 0)
                                {
                                    var objLogChange = listLogChange.Where(p => p.PupilID == itemMR.PUPIL_ID && p.Title.Equals(TitleMarkPeriod)).FirstOrDefault();
                                    objEmployee = null;
                                    if (objLogChange != null && objLogChange.LogChange > 0)
                                    {
                                        objEmployee = lstEmployeeChange.Where(p => p.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                                    }

                                    if (TitleMarkPeriod.Contains("M"))
                                    {
                                        mrv.InterviewMark[indexInterviewMark] = decimal.Parse(Mark.ToString());
                                        mrv.LogMarkM[indexInterviewMark] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                                        indexInterviewMark++;
                                    }
                                    else if (TitleMarkPeriod.Contains("P"))
                                    {
                                        mrv.WritingMark[indexWritingMark] = decimal.Parse(Mark.ToString());
                                        mrv.LogMarkP[indexWritingMark] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                                        indexWritingMark++;
                                    }
                                    else if (TitleMarkPeriod.Contains("V"))
                                    {
                                        mrv.TwiceCoeffiecientMark[indexTwiceCoeffiecientMark] = decimal.Parse(Mark.ToString());
                                        mrv.LogMarkV[indexTwiceCoeffiecientMark] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                                        indexTwiceCoeffiecientMark++;
                                    }
                                    else if (TitleMarkPeriod.Contains("HK"))
                                    {
                                        mrv.MarkSemester = decimal.Parse(Mark.ToString());
                                        mrv.LogMarkHK = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                                    }
                                }
                            }
                            else
                            {
                                if (TitleMarkPeriod.Contains("M"))
                                {
                                    indexInterviewMark++;
                                }
                                else if (TitleMarkPeriod.Contains("P"))
                                {
                                    indexWritingMark++;
                                }
                                else if (TitleMarkPeriod.Contains("V"))
                                {
                                    indexTwiceCoeffiecientMark++;
                                }
                                continue;
                            }
                        }

                        if (lstExemptedSubject.Any(u => u.PupilID == itemMR.PUPIL_ID && u.SubjectID == subjectid.Value)) mrv.Disable = "disabled";

                        mrv.Comment = itemMR.S_COMMENT;

                        //Cat Comment
                        if (mrv.Comment != null && mrv.Comment != "")
                        {
                            string Comment = mrv.Comment + "|";
                            string[] CmSemester = Comment.Split('|');
                            if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            {
                                mrv.Comment = CmSemester[0];
                            }
                            else if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            {
                                mrv.Comment = CmSemester[1];
                            }
                        }

                        string semester1 = string.Empty;
                        if (itemMR.HK1.HasValue)
                        {
                            if (itemMR.HK1 == 0)
                            {
                                semester1 = "0";
                            }
                            else if (itemMR.HK1 == 10)
                            {
                                semester1 = "10";
                            }
                            else
                            {
                                semester1 = itemMR.HK1.Value.ToString("0.0");
                            }
                        }
                        string semester2 = string.Empty;
                        if (itemMR.HK2.HasValue)
                        {
                            if (itemMR.HK2 == 0)
                            {
                                semester2 = "0";
                            }
                            else if (itemMR.HK2 == 10)
                            {
                                semester2 = "10";
                            }
                            else
                            {
                                semester2 = itemMR.HK2.Value.ToString("0.0");
                            }
                        }
                        mrv.SummedUpMark = semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? semester1 : semester2;
                        if (isInCrease)
                        {
                            objIncrease = listIncrease.Where(p => p.PUPIL_ID == itemMR.PUPIL_ID).FirstOrDefault();
                            string InCrease1 = objIncrease != null && objIncrease.HK1.HasValue ? objIncrease.HK1.Value.ToString("0.0").Replace(",", ".") : null;
                            string InCrease2 = objIncrease != null && objIncrease.HK2.HasValue ? objIncrease.HK2.Value.ToString("0.0").Replace(",", ".") : null;
                            if ("10.0".Equals(InCrease1))
                            {
                                InCrease1 = "10";
                            }
                            if ("10.0".Equals(InCrease2))
                            {
                                InCrease2 = "10";
                            }
                            mrv.SummedUpMarkIncrease = semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? InCrease1 : InCrease2;
                        }

                    }

                    ListMarkRecordViewModel.Add(mrv);
                }
                ViewData[MarkRecordConstants.LIST_MARKRECORD] = ListMarkRecordViewModel;
                Session["ListMarkRecordViewModel"] = ListMarkRecordViewModel;
            }
            int count = ListMarkRecordViewModel.Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).Count();
            ViewData[MarkRecordConstants.COUNT] = count.ToString();

            //Kiem tra khoa nhap lieu
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle))
            {
                isLockInput = objLockInput.LockTitle.Contains("HK" + semesterid);
            }
            ViewData[MarkRecordConstants.IS_LOCK_INPUT] = isLockInput;

            ClassProfile cp = ClassProfileBusiness.Find(classid);
            ViewData[MarkRecordConstants.TITLE_STRING] = String.Format("sổ điểm môn <span style='color:#d56900 '>{0}</span> học kỳ <span style='color:#d56900 '>{1}</span> đợt <span style='color:#d56900 '>{2}</span>, lớp <span style='color:#d56900 '>{3}</span>",
                            subject != null ? subject.DisplayName : String.Empty, semesterid.ToString(), Period.Resolution, cp != null ? cp.DisplayName : string.Empty);
            if (objSP.TrainingTypeID == 3)//truong GDTX
            {
                return PartialView("_ListGDTX");
            }
            else
            {
                return PartialView("_List");
            }

        }

        //
        // GET: /MarkRecord/Search


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //

            IEnumerable<MarkRecordViewModel> lst = this._Search(SearchInfo);
            ViewData[MarkRecordConstants.LIST_MARKRECORD] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns>
        /// NAMTA
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Create(MarkRecordViewModel model, FormCollection form)
        {
            string keyNames = "";
            try
            {
                //MarkRecordBusiness.SetAutoDetectChangesEnabled(false);
                bool isDBChange = false;
                //Lay gia tri tu cac hidden
                int AcademicYearID = _globalInfo.AcademicYearID.Value;
                int SchoolID = _globalInfo.SchoolID.Value;
                int ClassID = SMAS.Business.Common.Utils.GetInt(form["classid"]);
                int SubjectID = SMAS.Business.Common.Utils.GetShort(form["subjectid"]);
                int SemesterID = SMAS.Business.Common.Utils.GetByte(form["semesterid"]);
                int PeriodID = SMAS.Business.Common.Utils.GetInt(form["periodid"]);
                string lockTitle = form["LockTitle"];
                AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
                List<int> lstPupilID = new List<int>();

                //Lock action de tranh giao dich dong thoi
                List<string> lstKeyName = new List<string>();
                lstKeyName.Add(_globalInfo.UserAccountID.ToString());
                lstKeyName.Add(ClassID.ToString());//
                lstKeyName.Add(SemesterID.ToString());
                lstKeyName.Add(PeriodID.ToString());
                lstKeyName.Add(SubjectID.ToString());
                keyNames = VTUtils.LockAction.LockManager.CreatedKey(lstKeyName);
                if (!VTUtils.LockAction.LockManager.Lock(keyNames, Session.SessionID))
                {
                    return Json(new
                    {
                        Message = Res.Get("MarkRecord_Label_SavingMark"),
                        Type = JsonMessage.ERROR,
                        isDBChange = isDBChange
                    });
                }

                List<string> lstChangeMark = new List<string>();
                // Kiem tra co mo khoa diem hay khong de khong bi xoa nham diem vua duoc mo khoa
                LockedMarkDetailBusiness.CheckCurrentLockMark(lockTitle, new Dictionary<string, object>()
            {
                {"Semester", SemesterID},
                {"ClassID", ClassID},
                {"SubjectID", SubjectID},
                {"SchoolID", SchoolID},
                {"AcademicYearID", AcademicYearID},
            });

                bool Selected = false;

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = ClassID;
                dic["Check"] = "check";
                List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).ToList();

                string strMarkTypePeriod = MarkRecordBusiness.GetMarkTitle(PeriodID);

                string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });

                List<MarkRecord> listMarkRecordObject = new List<MarkRecord>();
                List<SummedUpRecord> listSummedUpRecord = new List<SummedUpRecord>();

                IDictionary<string, object> search = new Dictionary<string, object>();
                search["AppliedLevel"] = _globalInfo.AppliedLevel.Value;

                IEnumerable<MarkType> listMarkTypeSearch = MarkTypeBusiness.Search(search).ToList();

                if (listPupilOfClass.Count == 0)
                {
                    return Json(new
                    {
                        Message = Res.Get("Common_Label_UpdateMarkSuccess"),
                        Type = JsonMessage.ERROR,
                        isDBChange = isDBChange
                    });
                }

                //SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
                PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
                // Tạo data ghi log
                SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                StringBuilder isInsertLogstr = new StringBuilder();
                StringBuilder inforLog = null;
                StringBuilder oldobjtmp = null;

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                StringBuilder isInsertLogstrtmp = new StringBuilder();
                int iLog = 0;
                bool isInsertLog = false;
                List<OldMark> lstOldMark = new List<OldMark>();
                List<MarkRecordHistory> listTempHistory = null;
                List<MarkRecord> listTemp = null;
                if (isMovedHistory)
                {
                    Dictionary<string, object> Dics = new Dictionary<string, object>();
                    Dics.Add("ClassID", ClassID);
                    Dics.Add("AcademicYearID", acaYear.AcademicYearID);
                    Dics.Add("SchoolID", acaYear.SchoolID);
                    Dics.Add("SubjectID", SubjectID);
                    Dics.Add("Semester", SemesterID);
                    listTempHistory = MarkRecordHistoryBusiness.SearchMarkRecordPrimaryHistory(Dics).ToList();
                }
                else
                {
                    int modSchoolID = _globalInfo.SchoolID.Value % 100;
                    listTemp = (from m in MarkRecordBusiness.All
                                where
                                //lsPupilID.Contains(m.PupilID.ToString()) && 
                                m.Last2digitNumberSchool == modSchoolID
                               && m.AcademicYearID == acaYear.AcademicYearID
                               && m.Semester == SemesterID
                               && m.SubjectID == SubjectID
                               && m.ClassID == ClassID
                               && listMarkType.Contains(m.Title) //Chi lay con diem cua dot
                                select m).ToList();
                }
                foreach (PupilOfClass pupil in listPupilOfClass)
                {
                    int PupilID = pupil.PupilID;
                    lstOldMark = new List<OldMark>();
                    isInsertLog = false;
                    string chk_pupil = form["chk_" + PupilID.ToString()];
                    string comment = form["cm_" + PupilID.ToString()];
                    string markSemester = form["DHK_" + PupilID.ToString()];
                    if (chk_pupil == null)
                        continue;

                    // Tạo dữ liệu ghi log
                    PupilOfClass pop = pupil;
                    inforLog = new StringBuilder();
                    oldobjtmp = new StringBuilder();
                    inforLog.Append("Cập nhật điểm HS " + pop.PupilProfile.FullName);
                    inforLog.Append(", mã " + pop.PupilProfile.PupilCode);
                    inforLog.Append(", Lớp " + pop.ClassProfile.DisplayName);
                    inforLog.Append("/" + subjectCat.SubjectName);
                    inforLog.Append("/Học kỳ " + SemesterID);
                    inforLog.Append("/Đợt " + period.Resolution);
                    inforLog.Append("/" + pop.Year.Value + "-" + (pop.Year.Value + 1));
                    if (isMovedHistory)
                    {
                        if (listTempHistory != null)
                        {
                            List<MarkRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == PupilID && p.ClassID == ClassID && p.SubjectID == SubjectID && p.Semester == SemesterID).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                MarkRecordHistory record = listOldRecord[j];
                                OldMark objOldMark = new OldMark();
                                if (form["isChangePeriod_" + PupilID + record.Title] != null && "1".Equals(form["isChangePeriod_" + PupilID + record.Title]))
                                {
                                    oldobjtmp.Append(record.Title + ":" + record.Mark);
                                    isInsertLog = true;
                                    objOldMark.MarkRecordID = record.MarkRecordID;
                                    objOldMark.Title = record.Title;
                                    objOldMark.Mark = record.Mark;
                                    lstOldMark.Add(objOldMark);
                                    if (j < listOldRecord.Count - 1)
                                    {
                                        oldobjtmp.Append(", ");
                                    }
                                }
                            }
                            oldobjtmp.Append("}");
                            // end
                        }
                    }
                    else
                    {
                        if (listTemp != null)
                        {
                            List<MarkRecord> listOldRecord = listTemp.Where(p => p.PupilID == PupilID && p.ClassID == ClassID && p.SubjectID == SubjectID && p.Semester == SemesterID).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                MarkRecord record = listOldRecord[j];
                                OldMark objOldMark = new OldMark();
                                if (form["isChangePeriod_" + PupilID + record.Title] != null && "1".Equals(form["isChangePeriod_" + PupilID + record.Title]))
                                {
                                    oldobjtmp.Append(record.Title + ":" + record.Mark);
                                    isInsertLog = true;
                                    objOldMark.MarkRecordID = record.MarkRecordID;
                                    objOldMark.Title = record.Title;
                                    objOldMark.Mark = record.Mark;
                                    lstOldMark.Add(objOldMark);
                                    if (j < listOldRecord.Count - 1)
                                    {
                                        oldobjtmp.Append(",");
                                    }
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                        //// end
                    }
                    descriptionStrtmp.Append("Update mark record pupil_id:" + PupilID);
                    paramsStrtmp.Append("pupil_id:" + PupilID);
                    userFuntionsStrtmp.Append("Sổ điểm");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                    userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                    objectIDStrtmp.Append(PupilID);
                    oldObjectStrtmp.Append(oldobjtmp);
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                    if (chk_pupil.Equals("on"))
                    {
                        lstPupilID.Add(PupilID);
                        Selected = true;
                        OldMark objOM = null;
                        foreach (string MarkType in listMarkType)
                        {
                            if (MarkType == "" || "HK".Equals(MarkType))
                                continue;
                            string Mark = form[MarkType + "_" + PupilID.ToString()];
                            decimal numberMark = 0;
                            bool checkParse = decimal.TryParse(String.IsNullOrEmpty(Mark) ? Mark : Mark.Replace(".", ","), out numberMark);
                            string title = MarkType[0].ToString();
                            string strOrderNumber = MarkType.Substring(1);
                            //if (Mark != null && Mark.Length > 0 && checkParse && numberMark <= 10 && numberMark >= 0)
                            //{
                            int mt = listMarkTypeSearch.Where(o => o.Title.Contains(title)).FirstOrDefault().MarkTypeID;
                            MarkRecord mr = new MarkRecord();
                            mr.PupilID = PupilID;
                            mr.ClassID = ClassID;
                            mr.AcademicYearID = AcademicYearID;
                            mr.SchoolID = SchoolID;
                            mr.SubjectID = SubjectID;
                            mr.MarkedDate = DateTime.Now.Date;
                            mr.CreatedAcademicYear = acaYear.Year;
                            mr.Semester = SemesterID;
                            mr.MarkTypeID = mt;
                            mr.Title = MarkType;
                            if (form["isChangePeriod_" + PupilID + mr.Title] != null && "1".Equals(form["isChangePeriod_" + PupilID + mr.Title]))
                            {
                                lstChangeMark.Add(form["isChangePeriod_" + PupilID + mr.Title]);
                                if (lstOldMark.Count > 0)
                                {
                                    objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                                    if (objOM != null)
                                    {
                                        userDescriptionsStrtmp.Append(mr.Title + "(" + decimal.Parse(SMAS.Business.Common.Utils.FormatMark(objOM.Mark)) + ":");
                                        mr.MarkRecordID = objOM.MarkRecordID;
                                    }
                                    else
                                    {
                                        userDescriptionsStrtmp.Append(mr.Title + "(");
                                    }
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(mr.Title + "(");
                                }
                                if (Mark != null && Mark.Length > 0 && checkParse && numberMark <= 10 && numberMark >= 0)
                                {
                                    mr.Mark = numberMark;
                                    userDescriptionsStrtmp.Append(mr.Mark + "); ");
                                    newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);

                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append("); ");
                                    newObjectStrtmp.Append(mr.Title + ":");
                                    mr.Mark = -1;
                                }
                                newObjectStrtmp.Append(", ");
                                isInsertLog = true;
                            }
                            else
                            {
                                mr.Mark = -1;
                            }
                            mr.OrderNumber = SMAS.Business.Common.Utils.GetByte(strOrderNumber);
                            listMarkRecordObject.Add(mr);
                        }

                        SummedUpRecord sur = new SummedUpRecord();
                        sur.PupilID = PupilID;
                        sur.ClassID = ClassID;
                        sur.AcademicYearID = AcademicYearID;
                        sur.SchoolID = SchoolID;
                        sur.SubjectID = SubjectID;
                        sur.CreatedAcademicYear = acaYear.Year;
                        sur.Semester = SemesterID;
                        sur.PeriodID = PeriodID;
                        sur.Comment = string.Empty;

                        //if (markSemester != null && markSemester.Length > 0)
                        //{
                        int mthk = listMarkTypeSearch.Where(o => o.Title.Equals("HK")).FirstOrDefault().MarkTypeID;
                        MarkRecord mrhk = new MarkRecord();
                        mrhk.PupilID = PupilID;
                        mrhk.ClassID = ClassID;
                        mrhk.AcademicYearID = AcademicYearID;
                        mrhk.SchoolID = SchoolID;
                        mrhk.SubjectID = SubjectID;
                        mrhk.MarkedDate = DateTime.Now.Date;
                        mrhk.CreatedAcademicYear = acaYear.Year;
                        mrhk.Semester = SemesterID;
                        mrhk.MarkTypeID = mthk;
                        mrhk.Title = "HK";
                        if (form["isChangePeriod_" + PupilID + mrhk.Title] != null && "1".Equals(form["isChangePeriod_" + PupilID + mrhk.Title]))
                        {
                            lstChangeMark.Add(form["isChangePeriod_" + PupilID + mrhk.Title]);
                            if (lstOldMark.Count > 0)
                            {
                                objOM = lstOldMark.Where(p => p.Title.Equals(mrhk.Title)).FirstOrDefault();
                                if (objOM != null)
                                {
                                    userDescriptionsStrtmp.Append(mrhk.Title + "(" + decimal.Parse(SMAS.Business.Common.Utils.FormatMark(objOM.Mark)) + ":");
                                    mrhk.MarkRecordID = objOM.MarkRecordID;
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(mrhk.Title + "(");
                                }
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(mrhk.Title + "(");
                            }
                            if (markSemester != null && markSemester.Length > 0)
                            {
                                mrhk.Mark = decimal.Parse(markSemester.Replace(".", ","));
                                userDescriptionsStrtmp.Append(mrhk.Mark + "); ");
                                newObjectStrtmp.Append(mrhk.Title + ":" + mrhk.Mark);

                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(mrhk.Title + ":");
                                mrhk.Mark = -1;
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            mrhk.Mark = -1;
                        }
                        mrhk.OrderNumber = 1;
                        listMarkRecordObject.Add(mrhk);


                        if (subjectCat.IsCommenting == SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT && comment != null)
                        {
                            sur.Comment = comment;
                        }
                        listSummedUpRecord.Add(sur);
                    }
                    else
                    {
                        continue;
                    }

                    // Tạo dữ liệu ghi log
                    if (isInsertLog)
                    {
                        string newObj = string.Empty;
                        newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                        newObj += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        string tmp = string.Empty;
                        objectIDStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        newObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        tmp = userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2);
                        tmp += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        objectIDStr.Append(objectIDStrtmp);
                        descriptionStr.Append(descriptionStrtmp);
                        paramsStr.Append(paramsStrtmp);
                        oldObjectStr.Append(oldObjectStrtmp);
                        newObjectStr.Append(newObj);
                        userFuntionsStr.Append(userFuntionsStrtmp);
                        userActionsStr.Append(userActionsStrtmp);
                        userDescriptionsStr.Append(tmp);
                    }
                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    isInsertLogstrtmp = new StringBuilder();
                    iLog++;
                }
                if (listMarkRecordObject.Count > 0)
                {
                    if (isMovedHistory)
                    {
                        List<MarkRecordHistory> lstMarkRecordHistory = new List<MarkRecordHistory>();
                        List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                        MarkRecordHistory objMarkRecordHistory = null;
                        MarkRecord objMarkRecord = null;
                        SummedUpRecord objSummedUpRecord = null;
                        SummedUpRecordHistory objSummedUpRecordHistory = null;
                        for (int i = 0; i < listMarkRecordObject.Count; i++)
                        {
                            objMarkRecordHistory = new MarkRecordHistory();
                            objMarkRecord = new MarkRecord();
                            objMarkRecord = listMarkRecordObject[i];
                            objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                            objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                            objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                            objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                            objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                            objMarkRecordHistory.Mark = objMarkRecord.Mark;
                            objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                            objMarkRecordHistory.Semester = objMarkRecord.Semester;
                            objMarkRecordHistory.Title = objMarkRecord.Title;
                            objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                            objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                            objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                            objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                            objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                            objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                            objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                            lstMarkRecordHistory.Add(objMarkRecordHistory);
                        }
                        for (int i = 0; i < listSummedUpRecord.Count; i++)
                        {
                            objSummedUpRecord = new SummedUpRecord();
                            objSummedUpRecordHistory = new SummedUpRecordHistory();
                            objSummedUpRecord = listSummedUpRecord[i];
                            objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                            objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                            objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                            objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                            objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                            objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                            objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                            objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                            objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                            objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                            objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                            objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                            objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                            objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                            objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                            objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                            objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                            lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                        }

                        isDBChange = MarkRecordHistoryBusiness.InsertMarkRecordHistory(_globalInfo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, SemesterID, PeriodID, null, SchoolID, AcademicYearID, _globalInfo.AppliedLevel.Value, ClassID, SubjectID, _globalInfo.EmployeeID, acaYear.Year);
                        //MarkRecordHistoryBusiness.Save();
                    }
                    else
                    {
                        isDBChange = MarkRecordBusiness.InsertMarkRecord(_globalInfo.UserAccountID, listMarkRecordObject, listSummedUpRecord, SemesterID, PeriodID, null, SchoolID, AcademicYearID, _globalInfo.AppliedLevel.Value, ClassID, SubjectID, _globalInfo.EmployeeID);
                        //MarkRecordBusiness.Save();
                    }
                }
                else
                {
                    if (isMovedHistory)
                    {
                        MarkRecordHistoryBusiness.DeleteMarkRecordHistory(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SemesterID, PeriodID, SubjectID, lstPupilID);
                        SummedUpRecordHistoryBusiness.DeleteSummedUpRecordHistory(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SemesterID, PeriodID, SubjectID, lstPupilID);
                    }
                    else
                    {
                        MarkRecordBusiness.DeleteMarkRecord(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SemesterID, PeriodID, SubjectID, lstPupilID);
                        SummedUpRecordBusiness.DeleteSummedUpRecord(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SemesterID, PeriodID, SubjectID, lstPupilID);
                    }
                }
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);
                //SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());
                ///Luu danh sach cac o diem thay doi
                ViewData[MarkRecordConstants.LIST_CHANGE_MARK] = lstChangeMark;

                if (!Selected)
                {
                    return Json(new
                    {
                        Message = Res.Get("MarkRecordPeriod_Label_Noselect"),
                        Type = "success",
                        isDBChange = false
                    });
                }

                return Json(new
                {
                    Message = Res.Get("Common_Label_UpdateMarkSuccess"),
                    Type = "success",
                    isDBChange = isDBChange
                });
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "Create", "null", ex);
                return Json(new
                {
                    Message = "Lỗi trong quá trình thực hiện lưu điểm",
                    Type = "success",
                    isDBChange = false
                });
            }
            finally
            {
                //Giai phong log action
                VTUtils.LockAction.LockManager.ReleaseLock(keyNames, Session.SessionID);
                //MarkRecordBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(int MarkRecordID)
        {
            MarkRecord markrecord = this.MarkRecordBusiness.Find(MarkRecordID);
            TryUpdateModel(markrecord);
            Utils.Utils.TrimObject(markrecord);
            this.MarkRecordBusiness.Update(markrecord);
            this.MarkRecordBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Delete(MarkRecordViewModel model, FormCollection form)
        {
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int SchoolID = _globalInfo.SchoolID.Value;
            int ClassID = SMAS.Business.Common.Utils.GetInt(form["classid"]);
            int SubjectID = SMAS.Business.Common.Utils.GetShort(form["subjectid"]);
            int SemesterID = SMAS.Business.Common.Utils.GetByte(form["semesterid"]);
            int PeriodID = SMAS.Business.Common.Utils.GetInt(form["periodid"]);
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

            List<int> lstPupilID = form.AllKeys.Where(u => u.StartsWith("chk_")).Select(u => int.Parse(u.Substring(4))).ToList();

            // Tạo dữ liệu ghi log
            List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => lstPupilID.Contains(o.PupilProfileID)).ToList();
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            SubjectCat subject = SubjectCatBusiness.Find(SubjectID);
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
            StringBuilder oldObject = new StringBuilder();
            StringBuilder objectID = new StringBuilder();
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            StringBuilder userDescriptionstmp = new StringBuilder();
            StringBuilder oldObjecttmp = new StringBuilder();
            StringBuilder markStr = null;
            int modSchoolID = SchoolID % 100;

            List<string> lstStrInterviewMark = !string.IsNullOrEmpty(period.StrInterviewMark) ? period.StrInterviewMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>();
            List<string> lstStrWritingMark = !string.IsNullOrEmpty(period.StrWritingMark) ? period.StrWritingMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>();
            List<string> lstStrTwiceMark = !string.IsNullOrEmpty(period.StrTwiceCoeffiecientMark) ? period.StrTwiceCoeffiecientMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>();

            List<string> markTitle = new List<string>();
            for (int i = 0; i < lstStrInterviewMark.Count; i++)
            {
                markTitle.Add(lstStrInterviewMark[i]);
            }
            for (int i = 0; i < lstStrWritingMark.Count; i++)
            {
                markTitle.Add(lstStrWritingMark[i]);
            }
            for (int i = 0; i < lstStrTwiceMark.Count; i++)
            {
                markTitle.Add(lstStrTwiceMark[i]);
            }
            if (period.ContaintSemesterMark.HasValue && period.ContaintSemesterMark.Value)
            {
                markTitle.Add("HK");
            }

            //Luu thong tin de phuc hoi du lieu

            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = AcademicYearID,
                SCHOOL_ID = SchoolID,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_MARK,
                SHORT_DESCRIPTION = string.Format("Điểm HS lớp: {0}, học kỳ:{1}, môn: {2}, ", classProfile.DisplayName, SemesterID, subject.DisplayName),
            };
            if (period != null)
            {
                objRes.SHORT_DESCRIPTION += string.Format(" đợt:{0}, ", period.Resolution);
            }
            objRes.SHORT_DESCRIPTION += string.Format(" năm học: {0}, ", classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1));

            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();

            var query = from m in MarkRecordBusiness.All
                        where lstPupilID.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID
                        && m.AcademicYearID == AcademicYearID && markTitle.Contains(m.Title)
                        && m.SubjectID == SubjectID
                        && m.ClassID == ClassID
                        && m.Semester == SemesterID
                        orderby m.Title, m.MarkTypeID
                        select m;
            var queryHis = from m in MarkRecordHistoryBusiness.All
                           where lstPupilID.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID
                           && m.AcademicYearID == AcademicYearID && markTitle.Contains(m.Title)
                           && m.SubjectID == SubjectID
                           && m.ClassID == ClassID
                           && m.Semester == SemesterID
                           orderby m.Title, m.MarkTypeID
                           select m;
            List<MarkRecord> allMarks = null;
            List<MarkRecordHistory> allMarksHis = null;
            if (isMovedHistory)
            {
                allMarksHis = queryHis.ToList();
            }
            else
            {
                allMarks = query.ToList();
            }
            bool isCheckDelete = false;
            List<int> lstPupilDeleteID = new List<int>();
            for (int i = 0, size = listPupilProfile.Count; i < size; i++)
            {
                string strHK = string.Empty;
                isCheckDelete = false;
                oldObjecttmp = new StringBuilder();
                oldObjecttmp.Append("Giá trị trước khi xóa: ");
                markStr = new StringBuilder();
                if (isMovedHistory)
                {
                    if (allMarksHis != null)
                    {
                        MarkRecordHistory mark = null;
                        List<MarkRecordHistory> marks = allMarksHis.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                        if (marks.Count > 0)
                        {
                            for (int j = 0; j < marks.Count; j++)
                            {
                                mark = marks[j];
                                if ("HK".Equals(mark.Title))
                                {
                                    strHK = mark.Title + ":" + decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mark.Mark)) + "; ";
                                }
                                else
                                {
                                    oldObjecttmp.Append(mark.Title + ":" + decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mark.Mark)));
                                    oldObjecttmp.Append("; ");
                                }
                            }
                            oldObjecttmp.Append(strHK);
                            isCheckDelete = true;
                            lstPupilDeleteID.Add(listPupilProfile[i].PupilProfileID);
                        }
                    }
                }
                else
                {
                    if (allMarks != null)
                    {
                        MarkRecord mark = null;
                        markStr = new StringBuilder();
                        List<MarkRecord> marks = allMarks.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                        if (marks.Count > 0)
                        {
                            ///Luu gia tri diem da xoa de phuc hoi diem                        
                            RESTORE_DATA_DETAIL objRestoreDetail;
                            for (int j = 0; j < marks.Count; j++)
                            {
                                mark = marks[j];
                                if ("HK".Equals(mark.Title))
                                {
                                    strHK = mark.Title + ":" + decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mark.Mark)) + "; ";
                                }
                                else
                                {
                                    oldObjecttmp.Append(mark.Title + ":" + decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mark.Mark)));
                                    oldObjecttmp.Append("; ");
                                }

                                #region Luu thong tin phuc vu phuc hoi du lieu

                                //Luu thong tin khoi phuc diem
                                KeyDelMarkBO objKeyMark = new KeyDelMarkBO
                                {
                                    AcademicYearID = mark.AcademicYearID,
                                    ClassID = mark.ClassID,
                                    MarkTypeID = mark.MarkTypeID,
                                    PeriodID = mark.PeriodID,
                                    PupilID = mark.PupilID,
                                    SchoolID = mark.SchoolID,
                                    Semester = mark.Semester,
                                    SubjectID = mark.SubjectID
                                };

                                BakMarkRecordBO objDelMark = new BakMarkRecordBO
                                {
                                    AcademicYearID = mark.AcademicYearID,
                                    ClassID = mark.ClassID,
                                    CreatedAcademicYear = mark.CreatedAcademicYear,
                                    CreatedDate = mark.CreatedDate,
                                    IsOldData = mark.IsOldData,
                                    Last2digitNumberSchool = mark.Last2digitNumberSchool,
                                    LogChange = mark.LogChange,
                                    Mark = mark.Mark,
                                    MarkedDate = mark.MarkedDate,
                                    MarkRecordID = mark.MarkRecordID,
                                    MarkTypeID = mark.MarkTypeID,
                                    ModifiedDate = mark.ModifiedDate,
                                    MSourcedb = mark.MSourcedb,
                                    M_MarkRecord = mark.M_MarkRecord,
                                    M_OldID = mark.M_OldID,
                                    OldMark = mark.OldMark,
                                    OrderNumber = mark.OrderNumber,
                                    PeriodID = mark.PeriodID,
                                    PupilID = mark.PupilID,
                                    SchoolID = mark.SchoolID,
                                    Semester = mark.Semester,
                                    SubjectID = mark.SubjectID,
                                    SynchronizeID = mark.SynchronizeID,
                                    Title = mark.Title,
                                    Year = mark.Year
                                };
                                objRestoreDetail = new RESTORE_DATA_DETAIL
                                {
                                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                                    CREATED_DATE = DateTime.Now,
                                    END_DATE = null,
                                    IS_VALIDATE = 1,
                                    LAST_2DIGIT_NUMBER_SCHOOL = mark.Last2digitNumberSchool,
                                    ORDER_ID = 1,
                                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                                    SCHOOL_ID = objRes.SCHOOL_ID,
                                    SQL_DELETE = JsonConvert.SerializeObject(objKeyMark),
                                    SQL_UNDO = JsonConvert.SerializeObject(objDelMark),
                                    TABLE_NAME = RestoreDataConstant.TABLE_MARK_RECORD
                                };

                                lstRestoreDetail.Add(objRestoreDetail);
                                #endregion

                            }
                            oldObjecttmp.Append(strHK);
                            isCheckDelete = true;
                            lstPupilDeleteID.Add(listPupilProfile[i].PupilProfileID);


                        }
                    }
                }

                if (isCheckDelete)
                {
                    userDescriptionstmp = new StringBuilder();
                    string tmp = string.Empty;
                    tmp = oldObjecttmp != null ? oldObjecttmp.ToString().Substring(0, oldObjecttmp.Length - 2) : "";
                    objectID.Append(listPupilProfile[i].PupilProfileID.ToString());
                    descriptionObject.Append("Delete mark_record:" + listPupilProfile[i].PupilProfileID.ToString());
                    paramObject.Append(listPupilProfile[i].PupilProfileID.ToString());
                    userFunctions.Append("Sổ điểm");
                    userActions.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                    userDescriptionstmp.Append("Xóa điểm HS " + listPupilProfile[i].FullName
                        + ", mã " + listPupilProfile[i].PupilCode
                        + ", Lớp " + classProfile.DisplayName
                        + "/" + subject.SubjectName
                        + "/Học kỳ " + SemesterID
                        + "/Đợt " + period.Resolution
                        + "/" + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1)
                        + ". " + tmp);
                    if (i < size - 1)
                    {
                        oldObject.Append(tmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        objectID.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFunctions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userDescriptionstmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    }
                    userDescriptions.Append(userDescriptionstmp);

                    objRes.SHORT_DESCRIPTION += string.Format(" HS: {0}, mã: {1}", listPupilProfile[i].FullName, listPupilProfile[i].PupilCode);
                }
            }
            // end
            if (isMovedHistory)
            {
                MarkRecordHistoryBusiness.DeleteMarkRecordHistory(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SemesterID, PeriodID, SubjectID, lstPupilDeleteID);
                SummedUpRecordHistoryBusiness.DeleteSummedUpRecordHistory(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SemesterID, PeriodID, SubjectID, lstPupilDeleteID);
                // MarkRecordHistoryBusiness.Save();
            }
            else
            {
                MarkRecordBusiness.DeleteMarkRecord(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SemesterID, PeriodID, SubjectID, lstPupilDeleteID);

                List<SummedUpRecord> lstSummed = SummedUpRecordBusiness.DeleteSummedUpRecord(_globalInfo.UserAccountID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SemesterID, PeriodID, SubjectID, lstPupilDeleteID);
                // MarkRecordBusiness.Save();
                if (lstSummed != null)
                {
                    List<RESTORE_DATA_DETAIL> lstResSummed = SummedUpRecordBusiness.BackUpSummedMark(lstSummed, objRes);
                    if (lstResSummed != null)
                    {
                        lstRestoreDetail.AddRange(lstResSummed);
                        if (lstRestoreDetail != null && lstRestoreDetail.Count > 0)
                        {
                            if (objRes.SHORT_DESCRIPTION.Length > 1000)
                            {
                                objRes.SHORT_DESCRIPTION = objRes.SHORT_DESCRIPTION.Substring(0, 1000) + "...";
                            }

                            RestoreDataBusiness.Insert(objRes);
                            RestoreDataBusiness.Save();
                            RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
                        }
                    }
                }
            }

            if (lstPupilDeleteID.Count > 0)
            {
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectID.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActions.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFunctions.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptions.ToString()}
                };
                SetViewDataActionAudit(dicLog);
            }

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        private IEnumerable<MarkRecordViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<MarkRecord> query = this.MarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo);
            IQueryable<MarkRecordViewModel> lst = query.Select(o => new MarkRecordViewModel { });

            return lst.ToList();
        }

        //[HttpPost]
        [ActionAudit]
        public FileResult ExportFile()
        {
            int TypeExport = SMAS.Business.Common.Utils.GetInt(Request["TypeExport"]);
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int SchoolID = _globalInfo.SchoolID.Value;
            int ClassID = SMAS.Business.Common.Utils.GetInt(Request["ClassID"]);
            int SubjectID = SMAS.Business.Common.Utils.GetShort(Request["SubjectID"]);
            int SemesterID = SMAS.Business.Common.Utils.GetByte(Request["SemesterID"]);
            int PeriodID = SMAS.Business.Common.Utils.GetInt(Request["PeriodID"]);
            int IsCommenting = SMAS.Business.Common.Utils.GetInt(Request["isCommenting"]);
            int EducationLevelID = SMAS.Business.Common.Utils.GetInt(Request["EducationLevelID"]);
            string strSubjectID = SMAS.Business.Common.Utils.GetString(Request["ArrSubjectID"]);
            string strClassID = SMAS.Business.Common.Utils.GetString(Request["ArrClassID"]);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();

            Dictionary<string, object> dic = new Dictionary<string, object>();
            String filename = "";
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            SubjectCat subject = SubjectCatBusiness.Find(SubjectID);
            PeriodDeclaration periodDeclaration = PeriodDeclarationBusiness.Find(PeriodID);
            Stream excel = null;
            dic = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"ClassID",ClassID},
                {"Semester",SemesterID},
                {"PeriodID",PeriodID},
                {"SubjectID",SubjectID},
                {"EducationLevelID",EducationLevelID},
                {"isCommenting",IsCommenting},
                {"AppliedLevelID",_globalInfo.AppliedLevel.Value}
            };
            if (TypeExport == 1 || TypeExport == 2)
            {
                if (string.IsNullOrEmpty(strSubjectID))
                {
                    strSubjectID += SubjectID;
                }
                dic.Add("ArrSubjectID", strSubjectID);
                dic.Add("TypeExport", TypeExport);
                excel = this.MarkRecordBusiness.ExportAnySubject(dic, out filename, null, _globalInfo.IsAdminSchoolRole);
                if (TypeExport == 1)
                {
                    descriptionObject.Append("Export excel mark record class_profile_id: " + classProfile.ClassProfileID.ToString());
                    descriptionObject.Append(", SubjectID:" + SubjectID).Append(", SemesterID: " + SemesterID);
                    userDescriptions.Append("Xuất excel bảng điểm lớp {");
                    userDescriptions.Append(classProfile.DisplayName).Append("}, ");
                    userDescriptions.Append("môn {").Append(subject.DisplayName).Append("}, ");
                    userDescriptions.Append(" Học kỳ " + SemesterID).Append(", Đợt " + periodDeclaration.Resolution).Append(", năm học ").Append(classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1));
                    paramObject.Append("ClassID: " + ClassID + ", SubjectID: " + SubjectID + ", SemesterID: " + SemesterID);
                }
                else
                {
                    List<int> listSubjectId = strSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                    List<SubjectCat> lstSC = (from sc in SubjectCatBusiness.All
                                              where sc.AppliedLevel == _globalInfo.AppliedLevel
                                              && sc.IsActive
                                              && listSubjectId.Contains(sc.SubjectCatID)
                                              select sc).ToList();

                    descriptionObject.Append("Export excel mark record class_profile_id: " + classProfile.ClassProfileID.ToString());
                    userDescriptions.Append("Xuất excel bảng điểm lớp {");
                    userDescriptions.Append(classProfile.DisplayName).Append("}, ");
                    userDescriptions.Append("môn {");
                    paramObject.Append("ClassID: " + ClassID + ", SubjectID: {");
                    SubjectCat objSC = null;
                    for (int i = 0; i < lstSC.Count; i++)
                    {
                        objSC = lstSC[i];
                        descriptionObject.Append(", SubjectID:{" + objSC.SubjectCatID);
                        userDescriptions.Append(objSC.SubjectName);
                        paramObject.Append(objSC.SubjectCatID);
                        if (i < lstSC.Count - 1)
                        {
                            descriptionObject.Append(",");
                            userDescriptions.Append(", ");
                            paramObject.Append(",");
                        }
                    }
                    userDescriptions.Append("}, Học kỳ " + SemesterID).Append(", Đợt " + periodDeclaration.Resolution).Append(", năm học ").Append(classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1)); ;
                    descriptionObject.Append("}, SemesterID: " + SemesterID);
                    paramObject.Append(", SemesterID: " + SemesterID);
                }
            }
            else
            {
                dic.Add("ArrClassID", strClassID);
                excel = this.MarkRecordBusiness.ExportSubjectOfClass(dic, out filename, null, _globalInfo.IsAdminSchoolRole);
                List<int> listClassId = strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                List<ClassProfile> lstCP = (from cp in ClassProfileBusiness.All
                                            where cp.AcademicYearID == AcademicYearID
                                            && cp.SchoolID == SchoolID
                                            && listClassId.Contains(cp.ClassProfileID)
                                            && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                            select cp).ToList();
                descriptionObject.Append("Export excel mark record class_profile_id:{");
                userDescriptions.Append("Xuất excel bảng điểm lớp {");
                //userDescriptions.Append(classProfile.DisplayName).Append("} ");
                paramObject.Append("ClassID: {");
                ClassProfile objCP = null;
                for (int i = 0; i < lstCP.Count; i++)
                {
                    objCP = lstCP[i];
                    descriptionObject.Append(objCP.ClassProfileID);
                    userDescriptions.Append(objCP.DisplayName);
                    paramObject.Append(objCP.ClassProfileID);
                    if (i < lstCP.Count - 1)
                    {
                        descriptionObject.Append(",");
                        userDescriptions.Append(", ");
                        paramObject.Append(",");
                    }
                }
                userDescriptions.Append("}, môn {" + subject.SubjectName).Append("}, Học kỳ " + SemesterID).Append(", Đợt " + periodDeclaration.Resolution).Append(", năm học ").Append(objAcademicYear.Year + "-" + (objAcademicYear.Year + 1));
                descriptionObject.Append("}, môn {" + subject.SubjectName).Append("} SemesterID: " + SemesterID);
                paramObject.Append("}, SubjectID: " + SubjectID + "SemesterID: " + SemesterID);
            }
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;

            // Tạo dữ liệu ghi log
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,""},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,""},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionObject.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramObject.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,SMAS.Business.Common.GlobalConstants.ACTION_EXPORT},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,"Sổ điểm"},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptions.ToString()}
            };
            SetViewDataActionAudit(dicLog);
            return result;
        }


        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");

                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                return Json(new JsonMessage(""));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SavaImportOnSubject(int semesterid, int ClassID, int PeriodID, int SubjectID, int EducationLevelID, string arrSubjectID, string arrClassID)
        {
            string Error = "";
            string physicalPath = (string)Session["FilePath"];
            arrSubjectID = "";
            arrSubjectID = SubjectID.ToString();
            int TypeImport = 0;
            List<MarkRecordViewModel> lstMarkRecordViewModel = getDataToFileImport(physicalPath, semesterid, ClassID, PeriodID, EducationLevelID, SubjectID, arrSubjectID, arrClassID, out Error, out TypeImport);
            if (!string.IsNullOrEmpty(Error))
            {
                return Json(new JsonMessage(Error, "error"));
            }
            else
            {
                if (lstMarkRecordViewModel.Any(o => o.Pass == false))
                {
                    ViewData[MarkRecordConstants.LIST_IMPORTDATA] = lstMarkRecordViewModel.Where(p => p.Pass == false).ToList();
                    return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "grid"));
                }
                else
                {
                    List<MarkRecordViewModel> lstMarkRecord = lstMarkRecordViewModel.Where(p => p.isCommenting == SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK).ToList();
                    List<MarkRecordViewModel> lstJudgeRecord = lstMarkRecordViewModel.Where(p => p.isCommenting == SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT).ToList();
                    if (string.IsNullOrEmpty(arrClassID))
                    {
                        arrClassID += ClassID;
                    }
                    if (string.IsNullOrEmpty(arrSubjectID))
                    {
                        arrSubjectID += SubjectID;
                    }
                    if (lstMarkRecord.Count > 0)
                    {
                        //luu mon tinh diem
                        SetDataInputToDb(PeriodID, semesterid, arrSubjectID, arrClassID, EducationLevelID, TypeImport, lstMarkRecord);
                    }

                    if (lstJudgeRecord.Count > 0)
                    {
                        //luu mon nhan xet
                        this.SetDataInputToDbJudge(PeriodID, semesterid, arrSubjectID, arrClassID, TypeImport, lstJudgeRecord);
                    }
                    return Json(new JsonMessage(Res.Get("MarkRecord_Label_ImportDataSuccess"), "success"));
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult SaveImportFile(FormCollection frm)
        {

            int? semesterid = Int32.Parse(frm["Semester"]);
            int? ClassID = Int32.Parse(frm["ClassID"]);
            int? PeriodID = Int32.Parse(frm["PeriodID"]);
            int? SubjectID = Int32.Parse(frm["SubjectID"]);
            int EducationLevelID = Int32.Parse(frm["EducationLevelID"]);
            string arrSubjectID = frm["arrSubjectID"];
            string arrClassID = frm["arrClassID"];
            string Error = "";
            string physicalPath = (string)Session["FilePath"];
            int TypeImport = 0;
            List<MarkRecordViewModel> lstMarkRecordViewModel = getDataToFileImport(physicalPath, semesterid, ClassID, PeriodID, EducationLevelID, SubjectID, arrSubjectID, arrClassID, out Error, out TypeImport);
            if (!string.IsNullOrEmpty(Error))
            {
                return Json(new JsonMessage(Error, "Error"));
            }
            else
            {
                if (lstMarkRecordViewModel.Any(o => o.Pass == false))
                {
                    ViewData[MarkRecordConstants.LIST_IMPORTDATA] = lstMarkRecordViewModel.Where(p => p.Pass == false).ToList();
                    return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "grid"));
                }
                else
                {
                    List<MarkRecordViewModel> lstMarkRecord = lstMarkRecordViewModel.Where(p => p.isCommenting == SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK).ToList();
                    List<MarkRecordViewModel> lstJudgeRecord = lstMarkRecordViewModel.Where(p => p.isCommenting == SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT).ToList();
                    if (string.IsNullOrEmpty(arrClassID))
                    {
                        arrClassID += ClassID;
                    }
                    if (string.IsNullOrEmpty(arrSubjectID))
                    {
                        arrSubjectID += SubjectID;
                    }
                    if (lstMarkRecord.Count > 0)
                    {
                        //luu mon tinh diem
                        SetDataInputToDb(PeriodID.Value, semesterid.Value, arrSubjectID, arrClassID, EducationLevelID, TypeImport, lstMarkRecord);
                    }

                    if (lstJudgeRecord.Count > 0)
                    {
                        //luu mon nhan xet
                        this.SetDataInputToDbJudge(PeriodID.Value, semesterid.Value, arrSubjectID, arrClassID, TypeImport, lstJudgeRecord);
                    }
                    return Json(new JsonMessage(Res.Get("MarkRecord_Label_ImportDataSuccess")));
                }
            }
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public List<MarkRecordViewModel> getDataToFileImport(string filename, int? semesterid, int? ClassID, int? PeriodID, int EducationLevelID, int? SubjectID, string strSubjectID, string strClassID, out string Error, out int TypeImport)
        {
            int SchoolID = _globalInfo.SchoolID.Value;
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int AppliedLevel = _globalInfo.AppliedLevel.Value;

            List<int> lstSubjectID = strSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
            List<int> lstClassID = !string.IsNullOrEmpty(strClassID) ? strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList() : new List<int>();
            if (lstSubjectID.Count == 0 && SubjectID.HasValue)
            {
                lstSubjectID.Add(SubjectID.Value);
            }
            if (lstClassID.Count == 0 && ClassID.HasValue)
            {
                lstClassID.Add(ClassID.Value);
            }

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"SemesterID",semesterid},
                {"lstClassID",lstClassID}
            };


            ClassProfile classProfile = null;
            SubjectCat subject = null;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);

            string FilePath = filename;
            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);

            List<MarkRecordViewModel> lstMarkRecordViewModel = new List<MarkRecordViewModel>();
            List<IVTWorksheet> lstSheet = oBook.GetSheets();
            Error = "";
            string errSheetName = "";
            IVTWorksheet sheettmp = oBook.GetSheet(1);
            int ImportType = 0;
            int tmp = 0;
            if (sheettmp.GetCellValue("B3") != null && int.TryParse(sheettmp.GetCellValue("B3").ToString(), out tmp))
            {
                ImportType = tmp;
            }
            TypeImport = ImportType;
            IDictionary<string, object> dicCS = new Dictionary<string, object>();
            dicCS["SchoolID"] = SchoolID;
            dicCS["AcademicYearID"] = AcademicYearID;
            dicCS["lstClassID"] = lstClassID;
            dicCS["lstSubjectID"] = lstSubjectID;
            List<ClassSubject> lstCS = null;
            ClassSubject objCS = null;
            #region IMPORT THEO MON
            if (ImportType == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_ONE_SUBJECT || ImportType == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_SUBJECT_ID)
            {
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SchoolID"] = SchoolID;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                List<SubjectCat> lstSubjectCat = SubjectCatBusiness.Search(dic).Where(p => lstSubjectID.Contains(p.SubjectCatID)).ToList();
                lstCS = ClassSubjectBusiness.SearchByClass(ClassID.Value, dicCS).ToList();
                int subjectID = 0;
                string sheetName = "";
                List<int> lstCreaseID = lstCS.Where(p => p.SubjectIDIncrease.HasValue).Select(p => p.SubjectIDIncrease.Value).ToList();
                classProfile = ClassProfileBusiness.Find(ClassID);
                IVTWorksheet sheet = null;
                //Lấy sheet 
                for (int i = 0; i < lstSubjectID.Count; i++)
                {
                    subjectID = lstSubjectID[i];
                    objCS = lstCS.Where(p => p.SubjectID == subjectID).FirstOrDefault();
                    subject = lstSubjectCat.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                    if (lstCreaseID.Contains(subjectID))
                    {
                        sheetName = SMAS.Business.Common.Utils.StripVNSignAndSpace(subject.SubjectName);
                    }
                    else
                    {
                        sheetName = SMAS.Business.Common.Utils.StripVNSignAndSpace(subject.SubjectName);
                    }
                    if (ImportType == 1)
                    {
                        sheet = oBook.GetSheet(1);
                    }
                    else
                    {
                        sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(sheetName));
                        if (sheet == null)
                        {
                            throw new BusinessException("Không tồn tại môn " + subject.SubjectName + " trong file excel.");
                        }
                        if (lstSubjectID.Count > 1)
                        {
                            errSheetName = "Sheet " + sheetName + " ";
                        }
                    }

                    #region Kiem tra du lieu chon dau vao
                    //string ClassName = SMAS.Business.Common.Utils.StripVNSignAndSpace(classProfile.DisplayName);
                    //string SubjectName = SMAS.Business.Common.Utils.StripVNSignAndSpace(subject.DisplayName);
                    string SemesterName = ReportUtils.ConvertSemesterForReportName(semesterid.Value);

                    string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                    string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                    string AcademicYearNameFromExcel = (string)sheet.GetCellValue("A5");
                    string Title = (string)sheet.GetCellValue("A4");
                    string strMarkTypePeriod = string.Empty;
                    string[] listMarkType = null;
                    if (Title.Contains(subject.DisplayName.ToUpper()) == false)
                    {
                        Error = "- " + errSheetName + Res.Get("MarkRecord_Label_SubjectError") + " " + subject.DisplayName;
                    }
                    if (Title.Contains(classProfile.DisplayName.ToUpper()) == false)
                    {
                        if (Error == "")
                            Error = "- " + errSheetName + Res.Get("MarkRecord_Label_ClassError") + " " + classProfile.DisplayName;
                        else Error = Error + "</br>" + "- " + errSheetName + Res.Get("MarkRecord_Label_ClassError") + " " + classProfile.DisplayName;
                    }
                    if (Title.Contains(SemesterName.ToUpper() + " ") == false)
                    {
                        if (Error == "")
                            Error = "- " + Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterName;
                        else Error = Error + "</br>" + "- " + errSheetName + Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterName;
                    }
                    if (PeriodID.HasValue && PeriodID.Value > 0)
                    {
                        strMarkTypePeriod = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
                        PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
                        if (period != null)
                        {
                            string periodName = period.Resolution;
                            if (Title.ToUpper().Contains(periodName.ToUpper()) == false)
                            {
                                string periodError = "- " + errSheetName + Res.Get("MarkRecord_Label_PeriodError");
                                if (periodName.ToLower().Contains(Res.Get("MarkRecord_Label_Period").ToLower()))
                                {
                                    periodError = periodError.Replace(Res.Get("MarkRecord_Label_Period").ToLower(), string.Empty);
                                }
                                if (Error == "")
                                    Error = periodError + " " + periodName;
                                else Error = Error + "</br>" + periodError + " " + periodName;
                            }
                        }
                    }
                    if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                    {
                        if (Error == "")
                            Error = "- " + errSheetName + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                        else Error = Error + "</br>" + "- " + errSheetName + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                    }
                    // Neu du lieu chon dau vao khong hop le thi khong can check du lieu chi tiet
                    if (Error != string.Empty)
                    {
                        return new List<MarkRecordViewModel>();
                    }
                    #endregion
                    #region Kiem tra so dau diem
                    string error = "";
                    int startColMark = 5;
                    int RowMark = 7;
                    int startRow = 8;
                    string strMarkTypeLock = MarkRecordBusiness.GetLockMarkTitle(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID.Value, semesterid.Value, subjectID, PeriodID ?? 0);


                    string[] listMarkTypeLock = strMarkTypeLock.Split(',').Where(u => u != string.Empty).ToArray();

                    int lengthMarkType = listMarkType.Length - 1;

                    for (int col = startColMark; col <= lengthMarkType; col++)
                    {
                        string valuePeriodMark = (string)sheet.GetCellValue(RowMark, col);
                        if (valuePeriodMark != null)
                        {
                            if (!strMarkTypePeriod.Contains(valuePeriodMark))
                            {
                                Error += "</br>" + Res.Get("MarkRecord_Label_MarkTypeError");
                                break;
                            }
                        }
                    }
                    #endregion
                    #region Doc du lieu tu file
                    var listPupilOfClass = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> {
                        { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "ClassID", ClassID}, { "Check", "Check" }
                        })
                                            join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                            select new { poc.PupilID, pp.FullName, pp.PupilCode, poc.Status }
                                    )
                        .ToList();

                    int P_Nhanxet = startColMark + lengthMarkType;
                    int P_TBM = P_Nhanxet + 1;

                    bool Pass = true;

                    Dictionary<string, object> dicCheckLockMark = new Dictionary<string, object>();
                    dicCheckLockMark["ClassID"] = ClassID;
                    dicCheckLockMark["AcademicYearID"] = AcademicYearID;
                    dicCheckLockMark["SubjectID"] = subjectID;
                    dicCheckLockMark["Semester"] = semesterid;
                    dicCheckLockMark["PeriodID"] = PeriodID;
                    dicCheckLockMark["AppliedLevel"] = AppliedLevel;
                    if (academicYear != null)
                    {
                        dicCheckLockMark["Year"] = academicYear.Year;
                    }
                    List<MarkRecord> listLockMarkRecord = MarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCheckLockMark)
                                                                        .ToList()
                                                                        .Where(u => listMarkTypeLock.Contains(u.Title))
                                                                        .ToList();


                    // Kiem tra trung du lieu hay khong?
                    List<string> lstPupilCode = new List<string>();
                    bool checkDuplicate = false;
                    while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null)
                    {
                        Pass = true;
                        error = "";
                        string pupilcode = (string)sheet.GetCellValue(startRow, 3);
                        MarkRecordViewModel Pupil = new MarkRecordViewModel();
                        if (lstPupilCode.Count() == 0)
                        {
                            // add vao list ma hoc sinh:
                            lstPupilCode.Add(pupilcode);
                        }
                        else
                        {
                            if (lstPupilCode.Contains(pupilcode))
                            {
                                checkDuplicate = true;
                            }
                            else
                            {
                                checkDuplicate = false;
                                lstPupilCode.Add(pupilcode);
                            }
                        }
                        var poc = listPupilOfClass.FirstOrDefault(u => u.PupilCode == pupilcode);
                        if (poc == null)
                        {
                            error = "- " + errSheetName + Res.Get("MarkRecord_Label_PupilIDError");
                            Pass = false;
                        }
                        else
                        {
                            Pupil.PupilID = poc.PupilID;
                        }

                        // Check trùng dữ liệu:
                        if (checkDuplicate)
                        {
                            Pass = false;
                            if (string.Empty.Equals(error))
                            {
                                error = "- " + errSheetName + "Trùng mã học sinh.";
                            }
                            else
                            {
                                error += "</br>- " + errSheetName + "Trùng mã học sinh.";
                            }
                        }
                        Pupil.PupilCode = pupilcode;
                        string pupilname = (string)sheet.GetCellValue(startRow, 4);
                        if (poc != null && !poc.FullName.Equals(pupilname, StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (string.Empty.Equals(error))
                            {
                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_NameError"); ;
                            }
                            else
                            {
                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_NameError"); ;
                            }
                            Pass = false;
                        }

                        Pupil.Fullname = pupilname;
                        Pupil.ClassID = ClassID;
                        Pupil.PeriodID = PeriodID;
                        Pupil.SubjectID = subjectID;
                        Pupil.PeriodID = PeriodID;
                        if (poc != null)
                        {
                            Pupil.PupilID = poc.PupilID;
                            Pupil.Status = poc.Status;
                        }

                        int index = 0;

                        Pupil.ListMark = new Dictionary<string, string>();

                        Dictionary<string, string> MarkList = new Dictionary<string, string>();
                        MarkRecord lockMark = new MarkRecord();
                        int col = 0;

                        if (objSP.TrainingTypeID == 3)
                        {
                            #region truong GDTX
                            for (col = startColMark; col < startColMark + lengthMarkType; col++)
                            {
                                var valuePeriodMark = sheet.GetCellValue(startRow, col);
                                // Neu la null hoac rong thi gan lai la null de xu ly o duoi
                                if (valuePeriodMark == null || valuePeriodMark.ToString().Trim().Equals(string.Empty))
                                {
                                    valuePeriodMark = null;
                                }
                                string TypeMark = listMarkType[index];

                                if (poc != null)
                                {
                                    lockMark = listLockMarkRecord.FirstOrDefault(u => u.PupilID == poc.PupilID && u.Title == TypeMark);
                                }
                                if (TypeMark[0] == 'M')
                                {
                                    if (valuePeriodMark == null)
                                        MarkList[TypeMark] = "";
                                    else
                                    {
                                        int result = 0;
                                        bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);
                                        if (result > 10 || result < 0)
                                        {
                                            temp = false;
                                        }
                                        if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                        {
                                            if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                            {
                                                MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                            }
                                            else
                                            {
                                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }
                                        }
                                        else
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkM"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkM"); ;
                                            }
                                            Pass = false;
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                    }
                                }
                                else if (TypeMark[0] == 'P')
                                {
                                    if (valuePeriodMark == null)
                                        MarkList[TypeMark] = "";
                                    else
                                    {
                                        int result = 0;
                                        bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);

                                        if (result > 10 || result < 0)
                                        {
                                            temp = false;
                                        }
                                        if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                        {
                                            if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                            {
                                                MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                            }
                                            else
                                            {
                                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }

                                        }
                                        else
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkP"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkP");
                                            }
                                            Pass = false;
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                    }
                                }
                                else if (TypeMark[0] == 'V')
                                {
                                    if (valuePeriodMark == null)
                                        MarkList[TypeMark] = "";
                                    else
                                    {
                                        int result = 0;
                                        bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);

                                        if (result > 10 || result < 0)
                                        {
                                            temp = false;
                                        }
                                        if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                        {
                                            if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                            {
                                                MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                            }
                                            else
                                            {
                                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }
                                        }
                                        else
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkV"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkV"); ;
                                            }
                                            Pass = false;
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                    }
                                }

                                index++;

                            }
                            //col++;
                            //cot TBM
                            if (listMarkType.Where(o => o.Equals("HK")).ToList().Count() > 0)
                            {
                                var MarkHK = sheet.GetCellValue(startRow, col);
                                if (MarkHK == null || string.Empty.Equals(MarkHK.ToString().Trim()))
                                {
                                    MarkList["HK"] = "";
                                }
                                else
                                {
                                    float resultHK = 0;
                                    bool tempHK = float.TryParse(MarkHK != null ? MarkHK.ToString() : "", out resultHK);
                                    if (resultHK > 10 || resultHK < 0)
                                    {
                                        tempHK = false;
                                    }
                                    if (objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        if (!("Đ".Equals(MarkHK) || "CĐ".Equals(MarkHK)))
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                            }
                                            Pass = false;
                                        }
                                        MarkList["HK"] = MarkHK.ToString();
                                    }
                                    else
                                    {
                                        if (!tempHK)
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK");
                                            }
                                            Pass = false;
                                            MarkList["HK"] = resultHK.ToString();
                                        }
                                        else
                                        {
                                            MarkList["HK"] = this.GetMarkHKOfGDTX(resultHK.ToString());
                                        }

                                    }
                                }
                                col++;
                            }
                            #endregion
                        }


                        else
                        {
                            #region truong binh thuong
                            for (col = startColMark; col < startColMark + lengthMarkType; col++)
                            {
                                var valuePeriodMark = sheet.GetCellValue(startRow, col);
                                // Neu la null hoac rong thi gan lai la null de xu ly o duoi
                                if (valuePeriodMark == null || valuePeriodMark.ToString().Trim().Equals(string.Empty))
                                {
                                    valuePeriodMark = null;
                                }
                                string TypeMark = listMarkType[index];

                                if (poc != null)
                                {
                                    lockMark = listLockMarkRecord.FirstOrDefault(u => u.PupilID == poc.PupilID && u.Title == TypeMark);
                                }
                                if (TypeMark[0] == 'M')
                                {
                                    if (valuePeriodMark == null)
                                        MarkList[TypeMark] = "";
                                    else
                                    {
                                        int result = 0;
                                        bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);
                                        if (result > 10 || result < 0)
                                        {
                                            temp = false;
                                        }
                                        if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                        {
                                            if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                            {
                                                MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                                // valuePeriodMark =  lockMark != null ? lockMark.Mark : 0;
                                                // MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }
                                            else
                                            {
                                                //error = Res.Get("MarkRecord_Label_MarkType") + " " + TypeMark + Res.Get("MarkRecord_Label_MarkError");
                                                //Pass = false;
                                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }
                                        }
                                        else
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkM"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkM"); ;
                                            }
                                            Pass = false;
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                    }
                                }
                                else if (TypeMark[0] == 'P')
                                {
                                    if (valuePeriodMark == null)
                                        MarkList[TypeMark] = "";
                                    else
                                    {
                                        float result = 0;
                                        bool temp = float.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);

                                        if (result > 10 || result < 0)
                                        {
                                            temp = false;
                                        }
                                        if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                        {
                                            if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                            {
                                                MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                                // valuePeriodMark = lockMark != null ? lockMark.Mark : 0;
                                                // MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }
                                            else
                                            {
                                                //error = Res.Get("MarkRecord_Label_MarkType") + " " + TypeMark + Res.Get("MarkRecord_Label_MarkError");
                                                //Pass = false;
                                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }

                                        }
                                        else
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkP"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkP");
                                            }
                                            Pass = false;
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                    }
                                }
                                else if (TypeMark[0] == 'V')
                                {
                                    if (valuePeriodMark == null)
                                        MarkList[TypeMark] = "";
                                    else
                                    {
                                        float result = 0;
                                        bool temp = float.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);

                                        if (result > 10 || result < 0)
                                        {
                                            temp = false;
                                        }
                                        if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                        {
                                            if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                            {
                                                MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                                // valuePeriodMark = lockMark != null ? lockMark.Mark : 0;
                                                // MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }
                                            else
                                            {
                                                //error = Res.Get("MarkRecord_Label_MarkType") + " " + TypeMark + Res.Get("MarkRecord_Label_MarkError");
                                                //Pass = false;
                                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                            }
                                        }
                                        else
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkV"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkV"); ;
                                            }
                                            Pass = false;
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                    }
                                }

                                index++;

                            }
                            //col++;
                            //cot TBM
                            if (listMarkType.Where(o => o.Equals("HK")).ToList().Count() > 0)
                            {
                                var MarkHK = sheet.GetCellValue(startRow, col);
                                if (MarkHK == null || string.Empty.Equals(MarkHK.ToString().Trim()))
                                {
                                    MarkList["HK"] = "";
                                }
                                else
                                {
                                    float resultHK = 0;
                                    bool tempHK = float.TryParse(MarkHK != null ? MarkHK.ToString() : "", out resultHK);
                                    if (resultHK > 10 || resultHK < 0)
                                    {
                                        tempHK = false;
                                    }
                                    if (objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        if (!("Đ".Equals(MarkHK) || "CĐ".Equals(MarkHK)))
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                            }
                                            Pass = false;
                                        }
                                        MarkList["HK"] = MarkHK.ToString();
                                    }
                                    else
                                    {
                                        if (!tempHK)
                                        {
                                            if (string.Empty.Equals(error))
                                            {
                                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                            }
                                            else
                                            {
                                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                            }
                                            Pass = false;
                                        }
                                        MarkList["HK"] = resultHK.ToString();
                                    }
                                }
                                col++;
                            }
                            #endregion
                        }


                        string MarkTBM = sheet.GetCellValue(startRow, col) != null ? sheet.GetCellValue(startRow, col).ToString() : string.Empty;

                        if (objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            Pupil.MarkTBM = MarkTBM;
                        }
                        else
                        {
                            float resultTBM = 0;
                            bool tempM = float.TryParse(MarkTBM != null ? MarkTBM.ToString() : "", out resultTBM);
                            Pupil.MarkTBM = resultTBM.ToString();
                            col++;
                        }



                        // Kiểm tra môn có cột nhận xét không
                        if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            var commentString = sheet.GetCellValue(startRow, col);
                            if (commentString != null)
                                Pupil.Comment = commentString.ToString();
                            double widthComment = sheet.GetColumnWidth(col);
                            if (widthComment == 0)
                            {
                                Pupil.Disable = "false";
                            }
                            else
                                Pupil.Disable = "true";
                        }
                        Pupil.ListMark = MarkList;
                        Pupil.Pass = Pass;
                        Pupil.SemesterID = semesterid;
                        Pupil.Note = error;
                        Pupil.isCommenting = objCS.IsCommenting;
                        lstMarkRecordViewModel.Add(Pupil);
                        startRow++;
                    }
                    #endregion
                }
            }
            #endregion
            #region IMPORT THEO LOP
            else
            {
                bool Pass = true;
                int classID = 0;
                List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(SchoolID, dic).ToList();
                lstCS = ClassSubjectBusiness.SearchByClass(ClassID.Value, dicCS).ToList();
                ClassProfile objCP = null;
                objCS = lstCS.FirstOrDefault();
                subject = SubjectCatBusiness.Find(SubjectID);
                for (int i = 0; i < lstClassProfile.Count; i++)
                {
                    objCP = lstClassProfile[i];
                    classID = objCP.ClassProfileID;
                    string ClassName = SMAS.Business.Common.Utils.StripVNSignAndSpace(objCP.DisplayName);
                    IVTWorksheet sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(ClassName));
                    if (sheet == null)
                    {
                        throw new BusinessException("Không tồn tại lớp " + objCP.DisplayName + " trong file excel.");
                    }
                    errSheetName = "Sheet " + ClassName + " ";
                    #region Kiem tra du lieu chon dau vao

                    string SubjectName = subject.DisplayName;
                    string SemesterName = ReportUtils.ConvertSemesterForReportName(semesterid.Value);

                    string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                    string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                    string AcademicYearNameFromExcel = (string)sheet.GetCellValue("A5");
                    string Title = (string)sheet.GetCellValue("A4");

                    if (Title.Contains(SubjectName.ToUpper()) == false)
                    {
                        Error = Res.Get("MarkRecord_Label_SubjectError") + " " + SubjectName;
                    }
                    else if (Title.Contains(objCP.DisplayName.ToUpper()) == false)
                    {
                        Error = Res.Get("MarkRecord_Label_ClassError") + " " + ClassName;
                    }
                    else if (Title.Contains(SemesterName.ToUpper() + " ") == false)
                    {
                        Error = Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterName;
                    }
                    else if (PeriodID.HasValue)
                    {
                        PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
                        if (period != null)
                        {
                            string periodName = period.Resolution;
                            if (Title.ToUpper().Contains(periodName.ToUpper()) == false)
                            {
                                string periodError = Res.Get("MarkRecord_Label_PeriodError");
                                if (periodName.ToLower().Contains(Res.Get("MarkRecord_Label_Period").ToLower()))
                                {
                                    periodError = periodError.Replace(Res.Get("MarkRecord_Label_Period").ToLower(), string.Empty);
                                }
                                Error = periodError + " " + periodName;
                            }
                        }
                    }
                    else if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                    {
                        Error = Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                    }
                    // Neu du lieu chon dau vao khong hop le thi khong can check du lieu chi tiet
                    if (Error != string.Empty)
                    {
                        return new List<MarkRecordViewModel>();
                    }
                    #endregion
                    #region Kiem tra so dau diem
                    string error = "";
                    int startColMark = 5;
                    int RowMark = 7;
                    int startRow = 8;
                    string strMarkTypePeriod = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                    string strMarkTypeLock = MarkRecordBusiness.GetLockMarkTitle(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classID, (int)semesterid, (int)SubjectID, PeriodID ?? 0);

                    string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
                    string[] listMarkTypeLock = strMarkTypeLock.Split(',').Where(u => u != string.Empty).ToArray();

                    int lengthMarkType = listMarkType.Length - 1;

                    for (int col = startColMark; col <= lengthMarkType; col++)
                    {
                        string valuePeriodMark = (string)sheet.GetCellValue(RowMark, col);
                        if (valuePeriodMark != null)
                        {
                            if (!strMarkTypePeriod.Contains(valuePeriodMark))
                            {
                                Error += "</br>" + Res.Get("MarkRecord_Label_MarkTypeError");
                                break;
                            }
                        }
                    }
                    #endregion
                    #region Doc du lieu tu file
                    var listPupilOfClass = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> {
                        { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "ClassID", classID}, { "Check", "Check" }
                        })
                                            join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                            select new { poc.PupilID, pp.FullName, pp.PupilCode, poc.Status }
                                    )
                        .ToList();

                    int P_Nhanxet = startColMark + lengthMarkType;
                    int P_TBM = P_Nhanxet + 1;

                    Dictionary<string, object> dicCheckLockMark = new Dictionary<string, object>();
                    dicCheckLockMark["ClassID"] = classID;
                    dicCheckLockMark["AcademicYearID"] = AcademicYearID;
                    dicCheckLockMark["SubjectID"] = SubjectID;
                    dicCheckLockMark["Semester"] = semesterid;
                    dicCheckLockMark["PeriodID"] = PeriodID;
                    dicCheckLockMark["AppliedLevel"] = AppliedLevel;
                    if (academicYear != null)
                    {
                        dicCheckLockMark["Year"] = academicYear.Year;
                    }
                    List<MarkRecord> listLockMarkRecord = MarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCheckLockMark)
                                                                        .ToList()
                                                                        .Where(u => listMarkTypeLock.Contains(u.Title))
                                                                        .ToList();


                    // Kiem tra trung du lieu hay khong?
                    List<string> lstPupilCode = new List<string>();
                    bool checkDuplicate = false;
                    while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null)
                    {
                        Pass = true;
                        error = "";
                        string pupilcode = (string)sheet.GetCellValue(startRow, 3);
                        MarkRecordViewModel Pupil = new MarkRecordViewModel();
                        if (lstPupilCode.Count() == 0)
                        {
                            // add vao list ma hoc sinh:
                            lstPupilCode.Add(pupilcode);
                        }
                        else
                        {
                            if (lstPupilCode.Contains(pupilcode))
                            {
                                checkDuplicate = true;
                            }
                            else
                            {
                                checkDuplicate = false;
                                lstPupilCode.Add(pupilcode);
                            }
                        }
                        var poc = listPupilOfClass.FirstOrDefault(u => u.PupilCode == pupilcode);
                        if (poc == null)
                        {
                            error = "- " + errSheetName + Res.Get("MarkRecord_Label_PupilIDError");
                            Pass = false;
                        }
                        else
                        {
                            Pupil.PupilID = poc.PupilID;
                        }

                        // Check trùng dữ liệu:
                        if (checkDuplicate)
                        {
                            Pass = false;
                            if (string.Empty.Equals(error))
                            {
                                error = "- " + errSheetName + "Trùng mã học sinh.";
                            }
                            else
                            {
                                error += "</br>- " + errSheetName + "Trùng mã học sinh.";
                            }
                        }
                        Pupil.PupilCode = pupilcode;
                        string pupilname = (string)sheet.GetCellValue(startRow, 4);
                        if (poc != null && !poc.FullName.Equals(pupilname, StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (string.Empty.Equals(error))
                            {
                                error = "- " + errSheetName + Res.Get("MarkRecord_Label_NameError"); ;
                            }
                            else
                            {
                                error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_NameError"); ;
                            }
                            Pass = false;
                        }

                        Pupil.Fullname = pupilname;
                        Pupil.ClassID = classID;
                        Pupil.PeriodID = PeriodID;
                        Pupil.SubjectID = SubjectID;
                        Pupil.PeriodID = PeriodID;
                        if (poc != null)
                        {
                            Pupil.PupilID = poc.PupilID;
                            Pupil.Status = poc.Status;
                        }

                        int index = 0;

                        Pupil.ListMark = new Dictionary<string, string>();

                        Dictionary<string, string> MarkList = new Dictionary<string, string>();

                        int col = 0;
                        for (col = startColMark; col < startColMark + lengthMarkType; col++)
                        {
                            var valuePeriodMark = sheet.GetCellValue(startRow, col);
                            // Neu la null hoac rong thi gan lai la null de xu ly o duoi
                            if (valuePeriodMark == null || valuePeriodMark.ToString().Trim().Equals(string.Empty))
                            {
                                valuePeriodMark = null;
                            }
                            string TypeMark = listMarkType[index];

                            if (TypeMark[0] == 'M')
                            {
                                if (valuePeriodMark == null)
                                    MarkList[TypeMark] = "";
                                else
                                {
                                    int result = 0;
                                    bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);
                                    if (result > 10 || result < 0)
                                    {
                                        temp = false;
                                    }
                                    if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        MarkRecord lockMark = listLockMarkRecord.FirstOrDefault(u => u.PupilID == poc.PupilID && u.Title == TypeMark);
                                        if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                        {
                                            MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                            // valuePeriodMark = lockMark != null ? lockMark.Mark : 0;
                                            // MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                        else
                                        {
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (string.Empty.Equals(error))
                                        {
                                            error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkM"); ;
                                        }
                                        else
                                        {
                                            error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkM"); ;
                                        }
                                        Pass = false;
                                        MarkList[TypeMark] = valuePeriodMark.ToString();
                                    }
                                }
                            }
                            else if (TypeMark[0] == 'P')
                            {
                                if (valuePeriodMark == null)
                                    MarkList[TypeMark] = "";
                                else
                                {
                                    float result = 0;
                                    bool temp = float.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);

                                    if (result > 10 || result < 0)
                                    {
                                        temp = false;
                                    }
                                    if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        MarkRecord lockMark = listLockMarkRecord.FirstOrDefault(u => u.PupilID == poc.PupilID && u.Title == TypeMark);
                                        if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                        {
                                            MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                            // valuePeriodMark = lockMark != null ? lockMark.Mark : 0;
                                            // MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                        else
                                        {
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }

                                    }
                                    else
                                    {
                                        if (string.Empty.Equals(error))
                                        {
                                            error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkP"); ;
                                        }
                                        else
                                        {
                                            error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkP"); ;
                                        }
                                        Pass = false;
                                        MarkList[TypeMark] = valuePeriodMark.ToString();
                                    }
                                }
                            }
                            else if (TypeMark[0] == 'V')
                            {
                                if (valuePeriodMark == null)
                                    MarkList[TypeMark] = "";
                                else
                                {
                                    float result = 0;
                                    bool temp = float.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "", out result);

                                    if (result > 10 || result < 0)
                                    {
                                        temp = false;
                                    }
                                    if (temp || objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        MarkRecord lockMark = listLockMarkRecord.FirstOrDefault(u => u.PupilID == poc.PupilID && u.Title == TypeMark);
                                        if (listMarkTypeLock.Contains(TypeMark) && !_globalInfo.IsAdminSchoolRole)
                                        {
                                            MarkList[TypeMark] = lockMark != null ? lockMark.Mark.ToString() : string.Empty;
                                            // valuePeriodMark = lockMark != null ? lockMark.Mark : 0;
                                            // MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                        else
                                        {
                                            MarkList[TypeMark] = valuePeriodMark.ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (string.Empty.Equals(error))
                                        {
                                            error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkV"); ;
                                        }
                                        else
                                        {
                                            error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkV"); ;
                                        }
                                        Pass = false;
                                        MarkList[TypeMark] = valuePeriodMark.ToString();
                                    }
                                }
                            }

                            index++;

                        }
                        //col++;
                        //cot TBM
                        if (listMarkType.Where(o => o.Equals("HK")).ToList().Count() > 0)
                        {
                            var MarkHK = sheet.GetCellValue(startRow, col);
                            if (MarkHK == null || string.Empty.Equals(MarkHK.ToString().Trim()))
                            {
                                MarkList["HK"] = "";
                            }
                            else
                            {
                                float resultHK = 0;
                                bool tempHK = float.TryParse(MarkHK != null ? MarkHK.ToString() : "", out resultHK);
                                if (resultHK > 10 || resultHK < 0)
                                {
                                    tempHK = false;
                                }
                                if (objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                {
                                    if (!("Đ".Equals(MarkHK) || "CĐ".Equals(MarkHK)))
                                    {
                                        if (string.Empty.Equals(error))
                                        {
                                            error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                        }
                                        else
                                        {
                                            error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                        }
                                        Pass = false;
                                    }
                                    MarkList["HK"] = MarkHK.ToString();
                                }
                                else
                                {
                                    if (!tempHK)
                                    {
                                        if (string.Empty.Equals(error))
                                        {
                                            error = "- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                        }
                                        else
                                        {
                                            error += "</br>- " + errSheetName + Res.Get("MarkRecord_Label_MarkHK"); ;
                                        }
                                        Pass = false;
                                        MarkList["HK"] = resultHK.ToString();
                                    }
                                    else
                                    {
                                        if (objSP.TrainingTypeID == 3)
                                        {
                                            MarkList["HK"] = this.GetMarkHKOfGDTX(resultHK.ToString());
                                        }
                                        else
                                        {
                                            MarkList["HK"] = resultHK.ToString();
                                        }

                                    }
                                }
                            }
                            col++;
                        }
                        string MarkTBM = sheet.GetCellValue(startRow, col) != null ? sheet.GetCellValue(startRow, col).ToString() : string.Empty;
                        if (objCS.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            Pupil.MarkTBM = !string.IsNullOrEmpty(MarkTBM) ? MarkTBM.ToString() : "";
                        }
                        else
                        {
                            float resultTBM = 0;
                            bool tempM = float.TryParse(MarkTBM != null ? MarkTBM.ToString() : "", out resultTBM);
                            Pupil.MarkTBM = resultTBM.ToString();
                            col++;
                        }
                        // Kiểm tra môn có cột nhận xét không
                        if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            var commentString = sheet.GetCellValue(startRow, col);
                            if (commentString != null)
                                Pupil.Comment = commentString.ToString();
                            double widthComment = sheet.GetColumnWidth(col);
                            if (widthComment == 0)
                            {
                                Pupil.Disable = "false";
                            }
                            else
                                Pupil.Disable = "true";
                        }
                        Pupil.ListMark = MarkList;
                        Pupil.Pass = Pass;
                        Pupil.SemesterID = semesterid;
                        Pupil.Note = error;
                        Pupil.isCommenting = objCS.IsCommenting;
                        lstMarkRecordViewModel.Add(Pupil);
                        startRow++;
                    }
                    #endregion
                }
            }
            #endregion
            return lstMarkRecordViewModel;
        }

        public void ViewDataImportExcel()
        {
            List<MarkRecordViewModel> ListPupil = (List<MarkRecordViewModel>)Session["ListMarkRecord"];
            //Đưa thông tin lên MarkRecord_Label_Message

            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(ListPupil[0].PeriodID);
            ViewData[MarkRecordConstants.titleImport] = Res.Get("MarkRecord_Label_MarkRecordReportTitle") + " - " + SubjectCatBusiness.Find(ListPupil[0].SubjectID).DisplayName.ToUpper() + " - " + ClassProfileBusiness.Find(ListPupil[0].ClassID).DisplayName.ToUpper() + "  - " + Period.Resolution.ToUpper() + " (" + Period.FromDate.Value.ToString("dd/MM/yyyy") + " - " + Period.EndDate.Value.ToString("dd/MM/yyyy") + ")";

            string MarkType = MarkRecordBusiness.GetMarkTitle(Period.PeriodDeclarationID);

            string[] listMarkType = MarkType.Split(new Char[] { ',' });

            List<string> listMarkM = new List<string>();
            List<string> listMarkP = new List<string>();
            List<string> listMarkV = new List<string>();

            foreach (string item in listMarkType)
            {
                if (item.Length < 2)
                    continue;
                else
                {
                    if (item[0] == 'M')
                        listMarkM.Add(item);
                    if (item[0] == 'P')
                        listMarkP.Add(item);
                    if (item[0] == 'V')
                        listMarkV.Add(item);
                }
            }



            if (ListPupil != null && ListPupil.Count > 0)
            {
                ViewData[MarkRecordConstants.colComment] = ListPupil[0].Disable;
                if (ListPupil[0].Disable == "true")
                    ViewData[MarkRecordConstants.colComment] = true;
                else ViewData[MarkRecordConstants.colComment] = false;
            }

            ViewData[MarkRecordConstants.ListTypeMarkMImport] = listMarkM;
            ViewData[MarkRecordConstants.ListTypeMarkPImport] = listMarkP;
            ViewData[MarkRecordConstants.ListTypeMarkVImport] = listMarkV;
            // Lay thong tin hien thi cot diem HK
            string strLockMarkHas = MarkRecordBusiness.GetMarkTitle(ListPupil[0].PeriodID.Value);
            string[] listMarkPeriod = null;
            bool isShowHK = false;
            if (strLockMarkHas != null && strLockMarkHas.Count() > 0)
            {
                //cat cac con diem boi dau ','
                listMarkPeriod = strLockMarkHas.Split(new Char[] { ',' });
                if (listMarkPeriod.Contains("HK"))
                {
                    isShowHK = true;
                }
            }
            ViewData[MarkRecordConstants.COLMARKSEMESTER] = isShowHK;
            ViewData[MarkRecordConstants.listMarkRecordViewModel] = ListPupil;
        }

        public void SetDataInputToDb(int PeriodID, int SemesterID, string ArrSubjectID, string ArrClassID, int EducationLevelID, int typeImport, List<MarkRecordViewModel> ListPupil = null)
        {
            try
            {

                IDictionary<string, object> dicParam = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"PeriodID",PeriodID},
                {"SemesterID",SemesterID},
                {"Grade",_globalInfo.Grade},
                {"IsInsertAnySubject",!string.IsNullOrEmpty(ArrSubjectID) ? true : false},
                {"EducationLevelID",EducationLevelID}
            };
                //Lay cac hoc sinh co trang thai dang hoc va hop le
                ListPupil = ListPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && u.Pass.HasValue && u.Pass.Value).ToList();
                AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
                PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                dic["IsActive"] = true;
                List<MarkType> listMarkType = MarkTypeBusiness.Search(dic).ToList();
                List<int> lstPupilID = ListPupil.Select(p => p.PupilID).Distinct().ToList();
                int marktypeid = 0;
                string strMarkTypePeriod = MarkRecordBusiness.GetMarkTitle(PeriodID);
                List<string> lstTitlePeriod = strMarkTypePeriod.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                List<int> lstClassID = ArrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                List<int> lstSubjectID = ArrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                List<ClassProfile> lstCP = (from cp in ClassProfileBusiness.All
                                            where cp.AcademicYearID == _globalInfo.AcademicYearID
                                            && cp.SchoolID == _globalInfo.SchoolID
                                            && lstClassID.Contains(cp.ClassProfileID)
                                            && (cp.IsActive.HasValue && cp.IsActive.Value)
                                            select cp).ToList();

                List<SubjectCat> lstSC = (from sc in SubjectCatBusiness.All
                                          where sc.AppliedLevel == _globalInfo.AppliedLevel
                                          && sc.IsActive
                                          && lstSubjectID.Contains(sc.SubjectCatID)
                                          select sc).ToList();
                SubjectCat objSC = null;
                ClassProfile objCP = null;
                List<MarkRecordHistory> listMarkRecordHistoryDB = null;
                List<MarkRecord> listMarkRecordDB = null;

                // Tạo data ghi log
                List<OldMark> lstOldMark = new List<OldMark>();
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                StringBuilder isInsertLogstr = new StringBuilder();
                StringBuilder inforLog = null;
                StringBuilder oldobjtmp = null;

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                StringBuilder isInsertLogstrtmp = new StringBuilder();
                int iLog = 0;
                bool isInsertLog = false;
                int modSchoolID = _globalInfo.SchoolID.Value % 100;
                if (isMovedHistory)
                {

                    listMarkRecordHistoryDB = (from m in MarkRecordHistoryBusiness.All
                                               where
                                               m.Last2digitNumberSchool == modSchoolID
                                               && m.AcademicYearID == acaYear.AcademicYearID
                                               && m.Semester == SemesterID
                                               && lstClassID.Contains(m.ClassID)
                                               && lstSubjectID.Contains(m.SubjectID)
                                               && lstTitlePeriod.Contains(m.Title)
                                               && lstPupilID.Contains(m.PupilID)
                                               select m).ToList();
                }
                else
                {
                    listMarkRecordDB = (from m in MarkRecordBusiness.All
                                        where
                                        m.Last2digitNumberSchool == modSchoolID
                                        && m.AcademicYearID == acaYear.AcademicYearID
                                        && m.Semester == SemesterID
                                        && lstClassID.Contains(m.ClassID)
                                        && lstSubjectID.Contains(m.SubjectID)
                                        && lstTitlePeriod.Contains(m.Title)
                                        && lstPupilID.Contains(m.PupilID)
                                        select m).ToList();
                }
                List<MarkRecord> listMarkRecord = new List<MarkRecord>();
                List<SummedUpRecord> listSummedUpRecord = new List<SummedUpRecord>();
                List<MarkRecord> lsttmp = new List<MarkRecord>();
                List<MarkRecordHistory> lsttmpHis = new List<MarkRecordHistory>();

                string markString = string.Empty;
                decimal mark = 0;
                bool isChangeMark = false;
                bool ParseMark = false;
                OldMark objOM = null;
                #region xu ly du lieu va luu log
                foreach (MarkRecordViewModel model in ListPupil)
                {
                    int id = model.PupilID;
                    isInsertLog = false;
                    lstOldMark = new List<OldMark>();
                    objCP = lstCP.Where(p => p.ClassProfileID == model.ClassID).FirstOrDefault();
                    objSC = lstSC.Where(p => p.SubjectCatID == model.SubjectID).FirstOrDefault();

                    // Tạo dữ liệu ghi log
                    inforLog = new StringBuilder();
                    oldobjtmp = new StringBuilder();
                    inforLog.Append("Import điểm HS " + model.Fullname);
                    inforLog.Append(", mã " + model.PupilCode);
                    inforLog.Append(", Lớp " + objCP.DisplayName);
                    inforLog.Append("/" + objSC.SubjectName);
                    inforLog.Append("/Học kỳ " + SemesterID);
                    inforLog.Append("/Đợt " + period.Resolution);
                    inforLog.Append("/" + acaYear.Year + "-" + (acaYear.Year + 1));
                    if (isMovedHistory)
                    {
                        if (listMarkRecordHistoryDB != null)
                        {
                            List<MarkRecordHistory> listOldRecord = listMarkRecordHistoryDB.Where(p => p.PupilID == id
                                                                                                    && p.ClassID == model.ClassID
                                                                                                    && p.SubjectID == model.SubjectID
                                                                                                    && p.Semester == model.SemesterID).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                MarkRecordHistory record = listOldRecord[j];
                                OldMark objOldMark = new OldMark();
                                oldobjtmp.Append(record.Title + ":" + decimal.Parse(SMAS.Business.Common.Utils.FormatMark(record.Mark)));
                                objOldMark.MarkRecordID = record.MarkRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(record.Mark));
                                lstOldMark.Add(objOldMark);
                                if (j < listOldRecord.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    else
                    {
                        if (listMarkRecordDB != null)
                        {
                            List<MarkRecord> listOldRecord = listMarkRecordDB.Where(p => p.PupilID == id
                                                                                        && p.ClassID == model.ClassID
                                                                                        && p.SubjectID == model.SubjectID
                                                                                        && p.Semester == model.SemesterID).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                MarkRecord record = listOldRecord[j];
                                OldMark objOldMark = new OldMark();
                                oldobjtmp.Append(record.Title + ":" + decimal.Parse(SMAS.Business.Common.Utils.FormatMark(record.Mark)));
                                objOldMark.MarkRecordID = record.MarkRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(record.Mark));
                                lstOldMark.Add(objOldMark);
                                if (j < listOldRecord.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    descriptionStrtmp.Append("Update mark record pupil_id:" + id);
                    paramsStrtmp.Append("pupil_id:" + id);
                    userFuntionsStrtmp.Append("Sổ điểm");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                    userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                    objectIDStrtmp.Append(id);
                    oldObjectStrtmp.Append(oldobjtmp);
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): {");

                    if (model.Pass.Value)
                    {
                        foreach (string key in model.ListMark.Keys)
                        {
                            isChangeMark = false;
                            markString = model.ListMark[key].Replace(".", ",");
                            MarkRecord mr = new MarkRecord();
                            objOM = null;
                            mr.PupilID = model.PupilID;
                            mr.ClassID = model.ClassID.Value;
                            mr.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            mr.SchoolID = _globalInfo.SchoolID.Value;
                            mr.SubjectID = model.SubjectID.Value;
                            mr.MarkedDate = DateTime.Now.Date;
                            mr.CreatedAcademicYear = acaYear.Year;
                            mr.Semester = model.SemesterID;
                            mr.Title = key;
                            mark = 0;
                            ParseMark = Decimal.TryParse(markString, out mark);
                            mr.MarkTypeID = (int)marktypeid;
                            if (key != "HK")
                            {
                                mr.OrderNumber = int.Parse((key.Substring(1).ToString()));
                                mr.MarkTypeID = listMarkType.Where(o => key.Contains(o.Title)).FirstOrDefault().MarkTypeID;
                            }
                            else
                            {
                                mr.OrderNumber = 1;
                                mr.MarkTypeID = listMarkType.Where(o => o.Title == "HK").FirstOrDefault().MarkTypeID;
                            }
                            if (lstOldMark.Count > 0)
                            {
                                objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                                if (objOM != null)
                                {
                                    if (objOM.Mark > -1 && objOM.Mark != mark)
                                    {
                                        isChangeMark = true;
                                    }
                                }
                                else if (!string.IsNullOrEmpty(markString))
                                {
                                    isChangeMark = true;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(markString))
                                {
                                    isChangeMark = true;
                                }
                            }


                            if (isChangeMark)
                            {
                                if (objOM != null)
                                {
                                    userDescriptionsStrtmp.Append(mr.Title + "(" + objOM.Mark + ":");
                                    mr.MarkRecordID = objOM.MarkRecordID;
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(mr.Title + "(");
                                }

                                if (ParseMark)
                                {
                                    mr.Mark = mark;
                                    userDescriptionsStrtmp.Append(mr.Mark + "); ");
                                    newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append("); ");
                                    newObjectStrtmp.Append(mr.Title + ":");
                                    mr.Mark = -1;
                                }
                                newObjectStrtmp.Append(", ");
                                isInsertLog = true;
                            }
                            else
                            {
                                mr.Mark = -1;
                            }
                            listMarkRecord.Add(mr);
                        }
                        SummedUpRecord sur = new SummedUpRecord();
                        sur.PupilID = model.PupilID;
                        sur.ClassID = model.ClassID.Value;
                        sur.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sur.SchoolID = _globalInfo.SchoolID.Value;
                        sur.SubjectID = model.SubjectID.Value;
                        sur.CreatedAcademicYear = acaYear.Year;
                        sur.Semester = model.SemesterID;
                        sur.PeriodID = model.PeriodID;
                        sur.Comment = model.isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE && model.Comment != null ? model.Comment : string.Empty;
                        listSummedUpRecord.Add(sur);
                    }
                    // Tạo dữ liệu ghi log
                    if (isInsertLog)
                    {
                        string newObj = string.Empty;
                        newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                        newObj += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        string tmp = string.Empty;
                        objectIDStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        newObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                        tmp += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        objectIDStr.Append(objectIDStrtmp);
                        descriptionStr.Append(descriptionStrtmp);
                        paramsStr.Append(paramsStrtmp);
                        oldObjectStr.Append(oldObjectStrtmp);
                        newObjectStr.Append(newObj);
                        userFuntionsStr.Append(userFuntionsStrtmp);
                        userActionsStr.Append(userActionsStrtmp);
                        userDescriptionsStr.Append(tmp);
                    }

                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    isInsertLogstrtmp = new StringBuilder();
                    iLog++;
                }

                if (listMarkRecord != null && listMarkRecord.Count > 0 && listSummedUpRecord != null && listSummedUpRecord.Count > 0)
                {
                    if (isMovedHistory)
                    {
                        List<MarkRecordHistory> lstMarkRecordHistory = new List<MarkRecordHistory>();
                        List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                        MarkRecordHistory objMarkRecordHistory = null;
                        MarkRecord objMarkRecord = null;
                        SummedUpRecord objSummedUpRecord = null;
                        SummedUpRecordHistory objSummedUpRecordHistory = null;
                        for (int i = 0; i < listMarkRecord.Count; i++)
                        {
                            objMarkRecordHistory = new MarkRecordHistory();
                            objMarkRecord = new MarkRecord();
                            objMarkRecord = listMarkRecord[i];
                            objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                            objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                            objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                            objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                            objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                            objMarkRecordHistory.Mark = objMarkRecord.Mark;
                            objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                            objMarkRecordHistory.Semester = objMarkRecord.Semester;
                            objMarkRecordHistory.Title = objMarkRecord.Title;
                            objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                            objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                            objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                            objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                            objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                            objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                            objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                            lstMarkRecordHistory.Add(objMarkRecordHistory);
                        }
                        for (int i = 0; i < listSummedUpRecord.Count; i++)
                        {
                            objSummedUpRecord = new SummedUpRecord();
                            objSummedUpRecordHistory = new SummedUpRecordHistory();
                            objSummedUpRecord = listSummedUpRecord[i];
                            objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                            objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                            objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                            objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                            objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                            objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                            objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                            objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                            objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                            objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                            objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                            objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                            objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                            objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                            objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                            objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                            objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                            lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                        }
                        if (typeImport == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_ONE_SUBJECT || typeImport == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_SUBJECT_ID)
                        {
                            int ClassID = Int32.Parse(ArrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                            lstSubjectID = listMarkRecord.Select(p => p.SubjectID).Distinct().ToList();
                            MarkRecordHistoryBusiness.InsertMarkRecordHistoryBySubjectID(_globalInfo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, SemesterID, PeriodID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, 0, ClassID, lstSubjectID, _globalInfo.EmployeeID);
                        }
                        else if (typeImport == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_CLASS_ID)
                        {
                            int SubjectID = Int32.Parse(ArrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                            lstClassID = listMarkRecord.Select(p => p.ClassID).Distinct().ToList();
                            MarkRecordHistoryBusiness.InsertMarkRecordHistoryByClassID(_globalInfo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, SemesterID, PeriodID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, 0, lstClassID, SubjectID, _globalInfo.EmployeeID);
                        }
                    }
                    else
                    {
                        if (typeImport == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_ONE_SUBJECT || typeImport == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_SUBJECT_ID)
                        {
                            int ClassID = Int32.Parse(ArrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                            lstSubjectID = ArrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();

                            //Sap xep mon tang cuong truoc
                            List<ClassSubject> listClassSubject = ClassSubjectBusiness.GetListSubjectByClass(_globalInfo.AcademicYearID.Value, ClassID, lstSubjectID);
                            List<int> listSubjectIDIncrease = new List<int>();
                            if (listClassSubject != null && listClassSubject.Count > 0)
                            {
                                listSubjectIDIncrease = listClassSubject.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease.Value).ToList();
                                lstSubjectID = listSubjectIDIncrease.Union(lstSubjectID).Distinct().ToList();
                            }
                            MarkRecordBusiness.InsertMarkRecordByListSubject(_globalInfo.UserAccountID, listMarkRecord, listSummedUpRecord, SemesterID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, 0, ClassID, lstSubjectID, _globalInfo.EmployeeID, PeriodID);
                        }
                        else if (typeImport == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_CLASS_ID)
                        {
                            int SubjectID = Int32.Parse(ArrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                            lstClassID = ArrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                            MarkRecordBusiness.InsertMarkRecordByListClass(_globalInfo.UserAccountID, listMarkRecord, listSummedUpRecord, SemesterID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, 0, lstClassID, SubjectID, _globalInfo.EmployeeID, PeriodID);
                        }
                    }
                }
                #endregion
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);
            }
            finally
            {
                Session["FilePath"] = null;
            }
        }

        public void SetDataInputToDbJudge(int PeriodID, int SemesterID, string ArrSubjectID, string ArrClassID, int ImportType, List<MarkRecordViewModel> ListPupil = null)
        {
            try
            {
                IDictionary<string, object> dicParam = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"PeriodID",PeriodID},
                {"SemesterID",SemesterID},
                {"Grade",_globalInfo.Grade},
                {"IsInsertAnySubject",!string.IsNullOrEmpty(ArrSubjectID) ? true : false}
            };
                //Lay cac hoc sinh co trang thai dang hoc va hop le
                List<MarkRecordViewModel> ListPupilMark = ListPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && u.Pass.HasValue && u.Pass.Value).ToList();

                AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAca);
                PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                dic["IsActive"] = true;
                List<MarkType> lsttMarkType = MarkTypeBusiness.Search(dic).ToList();
                List<JudgeRecord> lstJudgeRecord = new List<JudgeRecord>();
                List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>();
                #region ghi log du lieu
                // Tạo data ghi log
                List<OldJudge> lstOldMark = new List<OldJudge>();
                StringBuilder objectIDStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.ObjectID] != null ? ViewData[CommonKey.AuditActionKey.ObjectID].ToString() : "");
                StringBuilder descriptionStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.Description] != null ? ViewData[CommonKey.AuditActionKey.Description].ToString() : "");
                StringBuilder paramsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.Parameter] != null ? ViewData[CommonKey.AuditActionKey.Parameter].ToString() : "");
                StringBuilder oldObjectStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.OldJsonObject] != null ? ViewData[CommonKey.AuditActionKey.OldJsonObject].ToString() : "");
                StringBuilder newObjectStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.NewJsonObject] != null ? ViewData[CommonKey.AuditActionKey.NewJsonObject].ToString() : "");
                StringBuilder userFuntionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userFunction] != null ? ViewData[CommonKey.AuditActionKey.userFunction].ToString() : "");
                StringBuilder userActionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userAction] != null ? ViewData[CommonKey.AuditActionKey.userAction].ToString() : "");
                StringBuilder userDescriptionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userDescription] != null ? ViewData[CommonKey.AuditActionKey.userDescription].ToString() : "");
                StringBuilder inforLog = null;
                StringBuilder oldobjtmp = null;

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                StringBuilder isInsertLogstrtmp = new StringBuilder();
                int iLog = 0;
                bool isInsertLog = false;

                List<JudgeRecordHistory> listTempHistory = null;
                List<JudgeRecord> listTemp = null;
                List<int> lstClassID = ArrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                List<int> lstSubjectID = ArrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                if (isMovedHistory)
                {
                    Dictionary<string, object> Dics = new Dictionary<string, object>();
                    Dics.Add("AcademicYearID", objAca.AcademicYearID);
                    Dics.Add("SchoolID", objAca.SchoolID);
                    Dics.Add("Semester", SemesterID);
                    listTempHistory = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(Dics).Where(p => lstClassID.Contains(p.ClassID) && lstSubjectID.Contains(p.SubjectID)).ToList();
                }
                else
                {
                    int modSchoolID = _globalInfo.SchoolID.Value % 100;
                    listTemp = (from m in JudgeRecordBusiness.All
                                where m.Last2digitNumberSchool == modSchoolID
                                && m.AcademicYearID == objAca.AcademicYearID
                                && m.Semester == SemesterID
                                && lstClassID.Contains(m.ClassID)
                                && lstSubjectID.Contains(m.SubjectID)
                                select m).ToList();
                }
                List<ClassProfile> lstCP = (from cp in ClassProfileBusiness.All
                                            where cp.AcademicYearID == _globalInfo.AcademicYearID
                                            && cp.SchoolID == _globalInfo.SchoolID
                                            && lstClassID.Contains(cp.ClassProfileID)
                                            && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                            select cp).ToList();

                List<SubjectCat> lstSC = (from sc in SubjectCatBusiness.All
                                          where sc.AppliedLevel == _globalInfo.AppliedLevel
                                          && sc.IsActive
                                          && lstSubjectID.Contains(sc.SubjectCatID)
                                          select sc).ToList();
                SubjectCat objSC = null;
                ClassProfile objCP = null;
                string judgetmp = string.Empty;
                string markString = string.Empty;
                string mark = string.Empty;
                bool isChangeMark = false;
                OldJudge objOM = null;
                int id = 0;

                foreach (MarkRecordViewModel model in ListPupilMark)
                {

                    Dictionary<string, string> listMark = model.ListMark;
                    id = model.PupilID;
                    isInsertLog = false;
                    lstOldMark = new List<OldJudge>();
                    objCP = lstCP.Where(p => p.ClassProfileID == model.ClassID).FirstOrDefault();
                    objSC = lstSC.Where(p => p.SubjectCatID == model.SubjectID).FirstOrDefault();

                    // Tạo dữ liệu ghi log
                    inforLog = new StringBuilder();
                    oldobjtmp = new StringBuilder();
                    inforLog.Append("Import điểm HS " + model.Fullname);
                    inforLog.Append(", mã " + model.PupilCode);
                    inforLog.Append(", Lớp " + objCP.DisplayName);
                    inforLog.Append("/" + objSC.SubjectName);
                    inforLog.Append("/Học kỳ " + SemesterID);
                    inforLog.Append("/Đợt " + period.Resolution);
                    inforLog.Append("/" + objAca.Year + "-" + (objAca.Year + 1));
                    if (isMovedHistory)
                    {
                        if (listTempHistory != null)
                        {
                            List<JudgeRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == id
                                                                                            && p.SubjectID == model.SubjectID
                                                                                            && p.ClassID == model.ClassID
                                                                                            && p.Semester == model.SemesterID).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                JudgeRecordHistory record = listOldRecord[j];
                                OldJudge objOldMark = new OldJudge();
                                oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                objOldMark.JudgeRecordID = record.JudgeRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Judgement = record.Judgement;
                                lstOldMark.Add(objOldMark);
                                if (j < listOldRecord.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    else
                    {
                        if (listTemp != null)
                        {
                            List<JudgeRecord> listOldRecord = listTemp.Where(p => p.PupilID == id
                                                                                && p.SubjectID == model.SubjectID
                                                                                && p.ClassID == model.ClassID
                                                                                && p.Semester == model.SemesterID).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                JudgeRecord record = listOldRecord[j];
                                OldJudge objOldMark = new OldJudge();
                                oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                objOldMark.JudgeRecordID = record.JudgeRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Judgement = record.Judgement;
                                lstOldMark.Add(objOldMark);
                                if (j < listOldRecord.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    descriptionStrtmp.Append("Update mark record pupil_id:" + id);
                    paramsStrtmp.Append("pupil_id:" + id);
                    userFuntionsStrtmp.Append("Sổ điểm");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                    userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                    objectIDStrtmp.Append(id);
                    oldObjectStrtmp.Append(oldobjtmp);
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                    foreach (string key in listMark.Keys)
                    {
                        isChangeMark = false;
                        JudgeRecord jr = new JudgeRecord();
                        objOM = null;
                        jr.PupilID = model.PupilID;
                        jr.ClassID = model.ClassID.Value;
                        jr.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        jr.SchoolID = _globalInfo.SchoolID.Value;
                        jr.SubjectID = model.SubjectID.Value;
                        jr.MarkedDate = DateTime.Now;
                        jr.CreatedAcademicYear = objAca.Year;
                        jr.Semester = model.SemesterID;
                        jr.Title = key;
                        markString = listMark[key];

                        if (key != "HK")
                        {
                            jr.OrderNumber = int.Parse((key.Substring(1).ToString()));
                            jr.MarkTypeID = lsttMarkType.FirstOrDefault(u => key.Contains(u.Title)).MarkTypeID;
                        }
                        else
                        {
                            jr.OrderNumber = 1;
                            jr.MarkTypeID = lsttMarkType.FirstOrDefault(u => u.Title == "HK").MarkTypeID;
                        }

                        if (lstOldMark.Count > 0)
                        {
                            objOM = lstOldMark.Where(p => p.Title.Equals(jr.Title)).FirstOrDefault();
                            if (objOM != null)
                            {
                                if (!string.IsNullOrEmpty(objOM.Judgement) && objOM.Judgement != markString)
                                {
                                    isChangeMark = true;
                                }
                            }
                            else if (!string.IsNullOrEmpty(markString))
                            {
                                isChangeMark = true;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(markString))
                            {
                                isChangeMark = true;
                            }
                        }
                        if (isChangeMark)
                        {
                            if (objOM != null)
                            {
                                userDescriptionsStrtmp.Append(jr.Title + "(" + objOM.Judgement + ":");
                                jr.JudgeRecordID = objOM.JudgeRecordID;
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(jr.Title + "(");
                            }

                            if (!string.IsNullOrEmpty(markString))
                            {
                                jr.Judgement = markString;
                                userDescriptionsStrtmp.Append(jr.Judgement + "); ");
                                newObjectStrtmp.Append(jr.Title + ":" + jr.Judgement);
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(jr.Title + ":");
                                jr.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            jr.Judgement = "-1";
                        }
                        lstJudgeRecord.Add(jr);
                    }
                    SummedUpRecord sur = new SummedUpRecord();
                    sur.PupilID = model.PupilID;
                    sur.ClassID = model.ClassID.Value;
                    sur.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    sur.SchoolID = _globalInfo.SchoolID.Value;
                    sur.SubjectID = model.SubjectID.Value;
                    sur.CreatedAcademicYear = objAca.Year;
                    sur.Semester = model.SemesterID;
                    sur.PeriodID = model.PeriodID;
                    sur.JudgementResult = model.MarkTBM;
                    sur.Comment = model.isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE && model.Comment != null ? model.Comment : string.Empty;
                    lstSummedUpRecord.Add(sur);
                    if (isInsertLog)
                    {
                        string newObj = string.Empty;
                        newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                        newObj += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        string tmp = string.Empty;
                        objectIDStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        newObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                        tmp += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        objectIDStr.Append(objectIDStrtmp);
                        descriptionStr.Append(descriptionStrtmp);
                        paramsStr.Append(paramsStrtmp);
                        oldObjectStr.Append(oldObjectStrtmp);
                        newObjectStr.Append(newObj);
                        userFuntionsStr.Append(userFuntionsStrtmp);
                        userActionsStr.Append(userActionsStrtmp);
                        userDescriptionsStr.Append(tmp);
                    }

                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    isInsertLogstrtmp = new StringBuilder();
                    iLog++;

                }
                #endregion
                if (isMovedHistory)
                {
                    List<JudgeRecordHistory> lstJudgeRecordHistory = new List<JudgeRecordHistory>();
                    List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                    JudgeRecordHistory objJudgeRecordHistory = null;
                    JudgeRecord objJudgeRecord = null;
                    SummedUpRecord objSummedUpRecord = null;
                    SummedUpRecordHistory objSummedUpRecordHistory = null;
                    for (int i = 0; i < lstJudgeRecord.Count; i++)
                    {
                        objJudgeRecordHistory = new JudgeRecordHistory();
                        objJudgeRecord = lstJudgeRecord[i];
                        objJudgeRecordHistory.JudgeRecordID = objJudgeRecord.JudgeRecordID;
                        objJudgeRecordHistory.AcademicYearID = objJudgeRecord.AcademicYearID;
                        objJudgeRecordHistory.PupilID = objJudgeRecord.PupilID;
                        objJudgeRecordHistory.SchoolID = objJudgeRecord.SchoolID;
                        objJudgeRecordHistory.ClassID = objJudgeRecord.ClassID;
                        objJudgeRecordHistory.Judgement = objJudgeRecord.Judgement;
                        objJudgeRecordHistory.MarkTypeID = objJudgeRecord.MarkTypeID;
                        objJudgeRecordHistory.Semester = objJudgeRecord.Semester;
                        objJudgeRecordHistory.Title = objJudgeRecord.Title;
                        objJudgeRecordHistory.Last2digitNumberSchool = objJudgeRecord.Last2digitNumberSchool;
                        objJudgeRecordHistory.MarkedDate = objJudgeRecord.MarkedDate;
                        objJudgeRecordHistory.ModifiedDate = objJudgeRecord.ModifiedDate;
                        objJudgeRecordHistory.SubjectID = objJudgeRecord.SubjectID;
                        objJudgeRecordHistory.CreatedDate = objJudgeRecord.CreatedDate;
                        objJudgeRecordHistory.CreatedAcademicYear = objJudgeRecord.CreatedAcademicYear;
                        objJudgeRecordHistory.OrderNumber = objJudgeRecord.OrderNumber;
                        lstJudgeRecordHistory.Add(objJudgeRecordHistory);
                    }
                    for (int i = 0; i < lstSummedUpRecord.Count; i++)
                    {
                        objSummedUpRecordHistory = new SummedUpRecordHistory();
                        objSummedUpRecord = lstSummedUpRecord[i];
                        objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                        objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                        objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                        objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                        objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                        objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                        objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                        objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                        objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                        objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                        objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                        objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                        objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                        objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                    }
                    if (ImportType == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_ONE_SUBJECT || ImportType == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_SUBJECT_ID)
                    {
                        int ClassID = Int32.Parse(ArrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                        lstSubjectID = lstJudgeRecord.Select(p => p.SubjectID).Distinct().ToList();
                        JudgeRecordHistoryBusiness.InsertJudgeRecordHistoryByListSubject(_globalInfo.UserAccountID, lstJudgeRecordHistory, lstSummedUpRecordHistory, SemesterID, PeriodID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, lstSubjectID, _globalInfo.EmployeeID);
                    }
                    else if (ImportType == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_CLASS_ID)
                    {
                        int SubjectID = Int32.Parse(ArrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                        lstClassID = lstJudgeRecord.Select(p => p.ClassID).Distinct().ToList();
                        JudgeRecordHistoryBusiness.InsertJudgeRecordHistoryByListClass(_globalInfo.UserAccountID, lstJudgeRecordHistory, lstSummedUpRecordHistory, SemesterID, PeriodID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, SubjectID, _globalInfo.EmployeeID);
                    }

                }
                else
                {
                    if (ImportType == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_ONE_SUBJECT || ImportType == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_SUBJECT_ID)
                    {
                        int ClassID = Int32.Parse(ArrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                        lstSubjectID = lstJudgeRecord.Select(p => p.SubjectID).Distinct().ToList();
                        JudgeRecordBusiness.InsertJudgeRecordByListSubject(_globalInfo.UserAccountID, lstJudgeRecord, lstSummedUpRecord, SemesterID, PeriodID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, lstSubjectID, _globalInfo.EmployeeID);
                    }
                    else if (ImportType == SMAS.Business.Common.GlobalConstants.EXPORT_MARK_RECORD_LIST_CLASS_ID)
                    {
                        int SubjectID = Int32.Parse(ArrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                        lstClassID = lstJudgeRecord.Select(p => p.ClassID).Distinct().ToList();
                        JudgeRecordBusiness.InsertJudgeRecordByListClass(_globalInfo.UserAccountID, lstJudgeRecord, lstSummedUpRecord, SemesterID, PeriodID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, SubjectID, _globalInfo.EmployeeID);
                    }
                }
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);
            }
            finally
            {
                Session["FilePath"] = null;
            }
        }

        public PartialViewResult _LoadGridDataExport(int? EducationLevelID, int ClassID, int TypeExport)
        {
            int schoolID = _globalInfo.SchoolID.Value;
            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            ViewData[MarkRecordConstants.TYPE_REPORT] = TypeExport;
            if (TypeExport == 2)
            {
                List<ClassSubjectBO> lstClassSubject = new List<ClassSubjectBO>();
                dic.Add("SchoolID", schoolID);
                dic.Add("AcademicYearID", academicYearID);
                lstClassSubject = (from cs in ClassSubjectBusiness.SearchByClass(ClassID, dic)
                                   select new ClassSubjectBO
                                   {
                                       ClassID = cs.ClassID,
                                       SubjectID = cs.SubjectID,
                                       SubjectName = cs.SubjectCat.DisplayName,
                                       OrderInSubject = cs.SubjectCat.OrderInSubject
                                   }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
                ViewData[MarkRecordConstants.LIST_SUBJECT] = lstClassSubject;
            }
            else
            {
                List<ClassProfile> lstClass = new List<ClassProfile>();
                dic.Add("AcademicYearID", academicYearID);
                dic.Add("EducationLevelID", EducationLevelID);
                lstClass = ClassProfileBusiness.SearchBySchool(schoolID, dic).OrderBy(p => p.OrderNumber).ThenBy(p => p.DisplayName).ToList();
                ViewData[MarkRecordConstants.LIST_CLASS] = lstClass;
            }
            return PartialView("_GridDataExport");
        }
        private string GetMarkHKOfGDTX(string mark)
        {
            string result = string.Empty;
            if (mark.Length == 1 || "0".Equals(mark) || "10".Equals(mark))
            {
                result = mark;
            }
            else
            {
                mark = mark.Replace(".", ",");
                List<string> lstOfMark = mark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                int tmp0 = int.Parse(lstOfMark[0]);
                int tmp = int.Parse(lstOfMark[1]);
                if (tmp < 3)
                {
                    result = lstOfMark[0] + "," + "0";
                }
                else if (tmp > 2 && tmp < 8)
                {
                    result = lstOfMark[0] + "," + "5";
                }
                else
                {
                    result = (tmp0 + 1).ToString() + "," + "0";
                }
            }
            return result;
        }
        private class OldMark
        {
            public long MarkRecordID { get; set; }
            public string Title { get; set; }
            public decimal Mark { get; set; }
        }
        private class OldJudge
        {
            public long JudgeRecordID { get; set; }
            public string Title { get; set; }
            public string Judgement { get; set; }
        }

    }
}
