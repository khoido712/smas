﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DefaultGroupArea.Models;

using SMAS.Models.Models;
using AutoMapper;
using SMAS.Web.Areas.RoleArea.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.VTUtils.Utils;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.DefaultGroupArea.Controllers
{
    public class DefaultGroupController : BaseController
    {
        private readonly IDefaultGroupBusiness DefaultGroupBusiness;
        private readonly IRoleBusiness RoleBusiness;
        private readonly IDefaultGroupMenuBusiness DefaultGroupMenuBusiness;
        private readonly IMenuBusiness MenuBusiness;
        private readonly IRoleMenuBusiness RoleMenuBusiness;
        GlobalInfo globalInfo;
        public DefaultGroupController(IDefaultGroupBusiness defaultgroupBusiness,
            IDefaultGroupMenuBusiness defaultGroupMenuBusiness,
            IMenuBusiness menuBusiness,
            IRoleMenuBusiness roleMenuBusiness,
            IRoleBusiness roleBusiness)
        {
            this.DefaultGroupBusiness = defaultgroupBusiness;
            this.RoleBusiness = roleBusiness;
            this.DefaultGroupMenuBusiness = defaultGroupMenuBusiness;
            this.RoleMenuBusiness = roleMenuBusiness;
            this.MenuBusiness = menuBusiness;
            Mapper.CreateMap<DefaultGroup, DefaultGroupViewModel>();
            Mapper.CreateMap<DefaultGroupViewModel, DefaultGroup>();
            this.globalInfo = new GlobalInfo();
        }

        //
        // GET: /DefaultGroup/

        public ActionResult Index()
        {
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SetViewData();
            //Get view data here

            IEnumerable<DefaultGroupViewModel> lst = this._Search(SearchInfo);
            ViewData[DefaultGroupConstants.LIST_DEFAULTGROUP] = lst;
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            SetViewData();
            Utils.Utils.TrimObject(frm);
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            if (!string.IsNullOrEmpty(frm.GroupName))
            {
                SearchInfo.Add("DefaultGroupName", frm.GroupName);
            }
            IEnumerable<DefaultGroupViewModel> lst = this._Search(SearchInfo);
            List<int> listRoleID = new List<int>();
            if (frm.listRoleID != null)
            {
                listRoleID = frm.listRoleID;
            }
            lst = lst.Where(o => listRoleID.Contains(o.RoleID));

            ViewData[DefaultGroupConstants.LIST_DEFAULTGROUP] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="frm">The FRM.</param>
        /// <returns></returns>
        /// <exception cref="BusinessException">DefaultGroup_Label_CheckboxRoleError</exception>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(DefaultGroupViewModel frm)
        {
            if (frm.listRoleID == null || frm.listRoleID.Count == 0)
            {
                throw new BusinessException("DefaultGroup_Label_CheckboxRoleError");
            }
            DefaultGroup defaultgroup = new DefaultGroup();
            TryUpdateModel(defaultgroup);
            Utils.Utils.TrimObject(defaultgroup);
            foreach (int roleID in frm.listRoleID)
            {
                DefaultGroup item = new DefaultGroup();
                Utils.Utils.BindTo(defaultgroup, item);
                item.RoleID = roleID;
                defaultgroup.RoleID = roleID;
                defaultgroup.CreatedDate = DateTime.Now;
                defaultgroup.IsActive = true;
                this.DefaultGroupBusiness.Insert(defaultgroup);
                this.DefaultGroupBusiness.Save();
            }


            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int DefaultGroupID)
        {
            DefaultGroup defaultgroup = this.DefaultGroupBusiness.Find(DefaultGroupID);
            TryUpdateModel(defaultgroup);
            Utils.Utils.TrimObject(defaultgroup);
            this.DefaultGroupBusiness.Update(defaultgroup);
            this.DefaultGroupBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.DefaultGroupBusiness.Delete(id);
            this.DefaultGroupBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }


        public ActionResult ManagePrivileges(int defaultGroupID)
        {
            TempData["DefaultGroupID"] = defaultGroupID;
            DefaultGroup df = DefaultGroupBusiness.Find(defaultGroupID);
            DefaultGroupViewModel DefaultGroupViewModel = new DefaultGroupViewModel();
            Utils.Utils.BindTo(df, DefaultGroupViewModel);

            DefaultGroupViewModel dfViewModel = DefaultGroupViewModel;
            string GroupName = df.DefaultGroupName;
            ViewData[DefaultGroupConstants.DEFAULTGROUP] = dfViewModel;
            ViewData[DefaultGroupConstants.DEFAULTGROUP_ID] = defaultGroupID;
            ViewData[DefaultGroupConstants.DEFAULTGROUP_NAME] = GroupName;
            //lay ra Menu tuong ung voi Role cua Group
            List<Menu> MenuOfRole = RoleMenuBusiness.GetMenuOfRole(df.RoleID).ToList();
            //Lay ra Menu da duoc phan quyen cho Group
            List<DefaultGroupMenu> GroupMenus = DefaultGroupMenuBusiness.GetDefaultGroupMenu(defaultGroupID).ToList();
            //TempData["GroupMenus"] = GroupMenus.ToList();
            //
            List<ItemMenuModel> LsMenuModel = getListItemMenu(MenuOfRole);
            MapPermission(GroupMenus, LsMenuModel);
            return View("_GridMenu", LsMenuModel);
        }

        [HttpPost]
        
        public ActionResult ManagePrivileges(FormCollection formCollection, int[] isAccess, int[] viewPermission, int[] addPermission, int[] editPermission, int[] deletePermission)
        {
            int DefaultGroupID = (int)TempData["DefaultGroupID"];
            DefaultGroup g = DefaultGroupBusiness.Find(DefaultGroupID);

            List<DefaultGroupMenu> listDefaultGroupMenu = new List<DefaultGroupMenu>();
            if (viewPermission != null && viewPermission.Count() > 0)
            {
                foreach (var MenuID in viewPermission)
                {
                    DefaultGroupMenu dgm = new DefaultGroupMenu();
                    dgm.MenuID = MenuID;
                    dgm.DefaultGroupID = DefaultGroupID;

                    dgm.Permission = deletePermission != null && deletePermission.Any(o => o == MenuID)
                                        ? 4
                                        : (editPermission != null && editPermission.Any(o => o == MenuID)
                                            ? 3
                                            : (addPermission != null && addPermission.Any(o => o == MenuID)
                                                ? 2
                                                : (viewPermission != null && viewPermission.Any(o => o == MenuID)
                                                    ? 1
                                                    : 0)));

                    listDefaultGroupMenu.Add(dgm);
                }
            }
            DefaultGroupBusiness.AssignMenu(DefaultGroupID, listDefaultGroupMenu);
            DefaultGroupBusiness.Save();
            return RedirectToAction("Index");
        }

        #region private function
        private List<ItemMenuModel> getListItemMenu(List<Menu> lsMenu)
        {
            //List<Menu> lsMenuTmp = this.MenuBusiness.All.Where(m => m.IsActive == true).ToList();
            MenuBuilder menuBuilder = new MenuBuilder(lsMenu);
            List<ItemMenuModel> menuModelLevel1 = menuBuilder.GetParentMenu();
            return menuModelLevel1;
        }
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DefaultGroupViewModel> _Search(Dictionary<string, object> SearchInfo)
        {
            IQueryable<DefaultGroup> query = this.DefaultGroupBusiness.Search(SearchInfo).OrderBy(o => o.DefaultGroupName);
            return MapToListViewModel(query.ToList());
        }

        private void MapPermission(List<DefaultGroupMenu> GroupMenus, List<ItemMenuModel> LsMenuModel)
        {
            //Voi moi Menu duoc gan cho Group
            //Neu la danh muc thi cap nhat Permission cua ItemMenuModel tuong ung voi MenuID 
            //Neu ko la danh muc thi cap nhat isAccess cua ItemMenuModel tuong ung voi MenuID
            if (GroupMenus != null && GroupMenus.Count > 0)
            {
                foreach (DefaultGroupMenu gm in GroupMenus)
                {
                    //if (!gm.Menu.IsCategory)
                    //{
                    foreach (ItemMenuModel itemMenuModel in LsMenuModel)
                    {
                        if (gm.MenuID == itemMenuModel.ItemMenuID)
                        {
                            if (gm.Permission == 4)
                            {
                                itemMenuModel.deletePermission = true;
                                itemMenuModel.editPermission = true;
                                itemMenuModel.addPermission = true;
                                itemMenuModel.viewPermission = true;
                            }
                            else if (gm.Permission == 3)
                            {
                                itemMenuModel.editPermission = true;
                                itemMenuModel.addPermission = true;
                                itemMenuModel.viewPermission = true;
                            }
                            else if (gm.Permission == 2)
                            {
                                itemMenuModel.addPermission = true;
                                itemMenuModel.viewPermission = true;
                            }
                            else if (gm.Permission == 1)
                            {
                                itemMenuModel.viewPermission = true;
                            }
                        }
                        else
                        {
                            foreach (ItemMenuModel itemMenuModel2 in itemMenuModel.MenuChildren)
                            {
                                if (gm.MenuID == itemMenuModel2.ItemMenuID)
                                {
                                    if (gm.Permission == 4)
                                    {
                                        itemMenuModel2.deletePermission = true;
                                        itemMenuModel2.editPermission = true;
                                        itemMenuModel2.addPermission = true;
                                        itemMenuModel2.viewPermission = true;
                                    }
                                    else if (gm.Permission == 3)
                                    {
                                        itemMenuModel2.editPermission = true;
                                        itemMenuModel2.addPermission = true;
                                        itemMenuModel2.viewPermission = true;
                                    }
                                    else if (gm.Permission == 2)
                                    {
                                        itemMenuModel2.addPermission = true;
                                        itemMenuModel2.viewPermission = true;
                                    }
                                    else if (gm.Permission == 1)
                                    {
                                        itemMenuModel2.viewPermission = true;
                                    }
                                }
                                else
                                {
                                    foreach (ItemMenuModel itemMenuModel3 in itemMenuModel2.MenuChildren)
                                    {
                                        if (gm.MenuID == itemMenuModel3.ItemMenuID)
                                        {
                                            if (gm.Permission == 4)
                                            {
                                                itemMenuModel3.deletePermission = true;
                                                itemMenuModel3.editPermission = true;
                                                itemMenuModel3.addPermission = true;
                                                itemMenuModel3.viewPermission = true;
                                            }
                                            else if (gm.Permission == 3)
                                            {
                                                itemMenuModel3.editPermission = true;
                                                itemMenuModel3.addPermission = true;
                                                itemMenuModel3.viewPermission = true;
                                            }
                                            else if (gm.Permission == 2)
                                            {
                                                itemMenuModel3.addPermission = true;
                                                itemMenuModel3.viewPermission = true;
                                            }
                                            else if (gm.Permission == 1)
                                            {
                                                itemMenuModel3.viewPermission = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //}
                }
            }
        }

        private void SetViewData()
        {
            List<RoleViewModel> listAllRoles = RoleBusiness.GetAll().Select(r => new RoleViewModel { RoleID = r.RoleID, RoleName = r.RoleName }).OrderBy(r => r.RoleID).ToList();
            List<ViettelCheckboxList> listRoleCheckbox = new List<ViettelCheckboxList>();
            foreach (RoleViewModel item in listAllRoles)
            {
                ViettelCheckboxList vtc = new ViettelCheckboxList(item.RoleName, item.RoleID, true, false);
                listRoleCheckbox.Add(vtc);
            }
            ViewData[DefaultGroupConstants.LS_ROLES_SEARCH] = listRoleCheckbox;
            ViewData[DefaultGroupConstants.LS_ROLES_GRID] = listAllRoles;
        }
        #endregion
        #region Mapper
        private DefaultGroupViewModel MapToViewModel(DefaultGroup group)
        {
            return Mapper.Map<DefaultGroup, DefaultGroupViewModel>(group);
        }

        private DefaultGroup MapBackToModel(DefaultGroupViewModel group)
        {
            DefaultGroup DefaultGroup = new DefaultGroup();
            Utils.Utils.BindTo(group, DefaultGroup);
            return DefaultGroup;
        }

        private List<DefaultGroupViewModel> MapToListViewModel(List<DefaultGroup> groups)
        {
            List<DefaultGroupViewModel> groupsViewModel = new List<DefaultGroupViewModel>();
            foreach (var group in groups)
            {
                DefaultGroupViewModel groupViewItem = MapToViewModel(group);
                groupViewItem.RoleName = group.Role.RoleName;
                groupsViewModel.Add(groupViewItem);
            }
            return groupsViewModel;
        }

        private List<DefaultGroup> MapBackToListModel(List<DefaultGroupViewModel> groups)
        {
            List<DefaultGroup> g = new List<DefaultGroup>();
            foreach (var group in groups)
            {
                DefaultGroup groupModel = MapBackToModel(group);
                g.Add(groupModel);
            }
            return g;
        }
        #endregion
    }
}





