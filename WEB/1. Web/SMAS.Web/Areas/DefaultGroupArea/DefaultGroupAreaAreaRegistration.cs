﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DefaultGroupArea
{
    public class DefaultGroupAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DefaultGroupArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DefaultGroupArea_default",
                "DefaultGroupArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
