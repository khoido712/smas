/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.DefaultGroupArea
{
    public class DefaultGroupConstants
    {
        public const string LIST_DEFAULTGROUP = "listDefaultGroup";
        public const string LS_ROLES_SEARCH = "List Roles for search";

        
        public const string GROUP_NAME = "Group Name";

        public const string ROLE_ID = "Role ID";

        public const string LS_ROLES_CREATEOREDIT = "List Roles for Create of edit";

        public const string LS_ROLES_GRID = "List role for grid";

        public const string DEFAULTGROUP_ID = "default Group ID";

        public const string DEFAULTGROUP_NAME = " default Group Name";

        public const string DEFAULTGROUP = "default Group object";
    }
}