﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Areas.GroupCatArea;
namespace SMAS.Web.Areas.DefaultGroupArea.Models
{
    public class DefaultGroupViewModel
    {
        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int DefaultGroupID { get; set; }

        [ResourceDisplayName("DefaultGroup_Label_Name")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DefaultGroupName { get; set; }

        [ScaffoldColumn(false)]
        public List<int> listRoleID { get; set; }

        [ScaffoldColumn(false)]
        public int RoleID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("DefaultGroup_Label_RoleName")]
        public string RoleName { get; set; }

        [ResourceDisplayName("DefaultGroup_Label_Description")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

    }
}


