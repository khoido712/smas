﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ActionAuditArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Tên Controller")]        
        public string ControllerName { get; set; }

        [ResourceDisplayName("Tên Action")]
        public string ActionName { get; set; }

        [ResourceDisplayName("Tên trường")]
        public string SchoolName { get; set; }        

        [ResourceDisplayName("User thực hiện")]
        public string UserName { get; set; }

        [ResourceDisplayName("Ngày bắt đầu")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> FromDate { get; set; }

        [ResourceDisplayName("Ngày kết thúc")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> ToDate { get; set; }

    }
}
