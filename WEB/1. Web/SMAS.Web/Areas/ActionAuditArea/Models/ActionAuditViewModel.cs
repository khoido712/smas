﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ActionAuditArea.Models
{
    public class ActionAuditViewModel
    {

        [ResourceDisplayName("Tên chức năng")]
        public string MenuName { get; set; }

        [ResourceDisplayName("Tên chức năng cha")]
        public string ParentName { get; set; }

        [ScaffoldColumn(false)]
        public int UserID { get; set; }

        [ScaffoldColumn(false)]
        public int? SchoolID { get; set; }

        public string _ControllerName;
        [ResourceDisplayName("Tên Controller")]
        public string ControllerName
        {
            get
            {
                if (this._ControllerName != null && this._ControllerName.Length > 20)
                {
                    return Utils.Utils.AutoInsertSpace(this._ControllerName, 20);
                }
                return this._ControllerName;
            }
            set { this._ControllerName = value; }
        }

        public string _ActionName;
        [ResourceDisplayName("Tên Action")]
        public string ActionName
        {
            get
            {
                if (this._ActionName != null && this._ActionName.Length > 20)
                {
                    return Utils.Utils.AutoInsertSpace(this._ActionName, 20);
                }
                return this._ActionName;
            }
            set { this._ActionName = value; }
        }

        [ResourceDisplayName("Tên trường")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("Mã trường")]
        public string SchoolCode { get; set; }

        [ResourceDisplayName("Bắt đầu thực hiện")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> BeginDate { get; set; }

        [ResourceDisplayName("Kết thúc thực hiện")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> EndDate { get; set; }

        [ResourceDisplayName("Thời gian thực hiện (giây)")]
        public double? TotalTime { get; set; }

        public string _UserName;
        [ResourceDisplayName("User thực hiện")]
        public string UserName
        {
            get
            {
                if (this._UserName != null && this._UserName.Length > 15)
                {
                    return Utils.Utils.AutoInsertSpace(this._UserName, 15);
                }
                return this._UserName;
            }
            set { this._UserName = value; }
        }

    }
}
