﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.ActionAuditArea.Models;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using Telerik.Web.Mvc;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
// Author: AuNH
// Created at: 27/08/2012
// Danh mục tỉnh thành
namespace SMAS.Web.Areas.ActionAuditArea.Controllers
{
    public class ActionAuditController : BaseController
    {
        private readonly IActionAuditBusiness ActionAuditBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        Dictionary<string, string[]> dicSpecial = new Dictionary<string, string[]>();
        string[] RecordSpecial = { "markrecord", "bookmarkrecord", "judgerecord", "judgerecordsemester", "primaryjudgerecord", "markrecordofprimary" };
        string[] SumupSpecial = { "pupilrankingsemester", "summeduprecord" };
        string[] pupilranking = { "pupilrankingofsemester", "pupilrankingperiod" };
        string[] pupilrankingnew = { "pupilranking", "pupilrankingnew" };
        string[] SummedUpRecordEducationLevelNew = { "summeduprecordbyeducationlevel", "summedupeducationlevelbyperiod" };
        string[] EmployeeProfile = { "employeepraisediscipline", "useraccountprofile" };
        string[] ImageParent = { "employeeprofilemanagement", "employee", "pupilprofile" };

        //cac area se bo qua ngay
        string[] MenuCancel = { "/employeeprofilemanagement" };
        List<string> allArray = new List<string>();


        public ActionAuditController(IActionAuditBusiness ActionAuditBusiness, ISchoolProfileBusiness SchoolProfileBusiness,
            IEmployeeBusiness EmployeeBusiness, IUserAccountBusiness UserAccountBusiness)
        {
            this.ActionAuditBusiness = ActionAuditBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            dicSpecial.Add("/bookrecord", RecordSpecial);
            dicSpecial.Add("/summary", SumupSpecial);
            dicSpecial.Add("/conductranking", pupilranking);
            dicSpecial.Add("/pupilrankingnewarea", pupilrankingnew);
            dicSpecial.Add("/summedupeducationlevelnew", SummedUpRecordEducationLevelNew);
            dicSpecial.Add("/employeeprofilemanagement", EmployeeProfile);
            allArray = RecordSpecial
                .Union(SumupSpecial).Union(pupilranking).Union(pupilrankingnew).Union(SummedUpRecordEducationLevelNew)
                .Union(EmployeeProfile)
                .ToList();
        }

        //
        // GET: /ActionAuditArea/ActionAudit/

        public ActionResult Index()
        {
            Paginate<ActionAuditViewModel> lst = this._Search("", "", "", DateTime.Now.Date, null, "", null);
            ViewData[ActionAuditConstants.LIST_ActionAudit] = lst;
            return View();
        }

        //
        // GET: /ActionAuditArea/ActionAudit/Search

        [SkipCheckRole]
        public PartialViewResult Search(string ControllerName, string ActionName, string SchoolName, DateTime? FromDate, DateTime? Todate, string UserName, string MinTime, int page = 1, string orderBy = "")
        {
            int m = 0;
            bool isNumeric = string.IsNullOrWhiteSpace(MinTime) ? true : int.TryParse(MinTime, out m);
            if (!isNumeric)
            {
                throw new BusinessException("Thời gian xử lý tối thiểu phải là số nguyên");
            }
            Paginate<ActionAuditViewModel> lst = this._Search(ControllerName, ActionName, SchoolName, FromDate, Todate, UserName, m, page, orderBy);
            ViewData[ActionAuditConstants.LIST_ActionAudit] = lst;
            return PartialView("_List");
        }


        /// <summary>
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private Paginate<ActionAuditViewModel> _Search(string controllerName, string actionName, string schoolName,
            DateTime? fromDate, DateTime? toDate, string userName, int? minTime, int page = 1, string orderBy = "")
        {
            IQueryable<ActionAuditViewModel> query = (from au in this.ActionAuditBusiness.All
                                                      join e in UserAccountBusiness.All on au.UserID equals e.UserAccountID
                                                      join em in EmployeeBusiness.All on e.EmployeeID equals em.EmployeeID into g1
                                                      from j1 in g1.DefaultIfEmpty()
                                                      select new ActionAuditViewModel
                                                      {
                                                          ControllerName = au.Controller,
                                                          ActionName = au.Action,
                                                          BeginDate = au.BeginAuditTime,
                                                          EndDate = au.EndAuditTime,
                                                          UserName = e.aspnet_Users.UserName,
                                                          SchoolName = j1.SchoolProfile.SchoolName,
                                                          SchoolCode = j1.SchoolProfile.SchoolCode,
                                                          SchoolID = j1.SchoolID,
                                                          UserID = au.UserID,
                                                          TotalTime = 0
                                                      });
            if (!string.IsNullOrWhiteSpace(controllerName))
            {
                query = query.Where(o => o.ControllerName.ToLower().Contains(controllerName.Trim().ToLower()));
            }
            if (!string.IsNullOrWhiteSpace(actionName))
            {
                query = query.Where(o => o.ActionName.ToLower().Contains(actionName.Trim().ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(userName))
            {
                query = query.Where(o => o.UserName.ToLower().Contains(userName.Trim().ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(schoolName))
            {
                var listSChoolAndAcoutID = SchoolProfileBusiness.All.Where(o => o.SchoolName.ToLower().Contains(schoolName.Trim().ToLower()))
                    .Select(o => new { o.SchoolProfileID, o.AdminID }).ToList();
                List<int> listSchoolID = listSChoolAndAcoutID.Select(o => o.SchoolProfileID).ToList();
                List<int> listAdminID = listSChoolAndAcoutID.Where(o => o.AdminID.HasValue).Select(o => o.AdminID.Value).ToList();
                query = query.Where(o => listSchoolID.Contains(o.SchoolID.Value) || listAdminID.Contains(o.UserID));
            }

            if (fromDate.HasValue)
            {
                DateTime date = new DateTime(fromDate.Value.Year, fromDate.Value.Month, fromDate.Value.Day);
                query = query.Where(o => o.BeginDate >= date);
            }

            if (toDate.HasValue)
            {
                DateTime date = toDate.Value.AddDays(1);
                query = query.Where(o => o.BeginDate <= date);
            }

            if (minTime.HasValue && minTime.Value >= 0)
            {
                query = query.Where(o => o.TotalTime >= minTime.Value);
            }
            query = query.OrderBy(o => o.ControllerName).ThenBy(o => o.ActionName).ThenByDescending(o => o.BeginDate);
            if (orderBy.Contains("ControllerName"))
            {
                if (orderBy.Contains("-asc"))
                {
                    query = query.OrderBy(o => o.ControllerName);
                }
                else
                {
                    query = query.OrderByDescending(o => o.ControllerName);
                }
            }
            if (orderBy.Contains("ActionName"))
            {
                if (orderBy.Contains("-asc"))
                {
                    query = query.OrderBy(o => o.ActionName);
                }
                else
                {
                    query = query.OrderByDescending(o => o.ActionName);
                }
            }
            if (orderBy.Contains("BeginDate"))
            {
                if (orderBy.Contains("-asc"))
                {
                    query = query.OrderBy(o => o.BeginDate);
                }
                else
                {
                    query = query.OrderByDescending(o => o.BeginDate);
                }
            }
            if (orderBy.Contains("EndDate"))
            {
                if (orderBy.Contains("-asc"))
                {
                    query = query.OrderBy(o => o.EndDate);
                }
                else
                {
                    query = query.OrderByDescending(o => o.EndDate);
                }
            }
            if (orderBy.Contains("TotalTime"))
            {
                if (orderBy.Contains("-asc"))
                {
                    query = query.OrderBy(o => o.TotalTime);
                }
                else
                {
                    query = query.OrderByDescending(o => o.TotalTime);
                }
            }
            if (orderBy.Contains("UserName"))
            {
                if (orderBy.Contains("-asc"))
                {
                    query = query.OrderBy(o => o.UserName);
                }
                else
                {
                    query = query.OrderByDescending(o => o.UserName);
                }
            }

            // Thuc hien phan trang tung phan

            Paginate<ActionAuditViewModel> Paging = new Paginate<ActionAuditViewModel>(query);
            Paging.page = page;
            if (page <= 0)
            {
                Paging.size = query.Count();
            }
            Paging.paginate();
            List<ActionAuditViewModel> list = Paging != null ? Paging.List.ToList() : new List<ActionAuditViewModel>();
            if (list != null)
            {
                List<Menu> menus = SMAS.Business.Common.StaticData.ListMenu as List<Menu>;
                var lstT = new List<ActionAuditViewModel>();
                // Danh sach adminID cua truong
                List<int> listAdminOp = list.Where(o => string.IsNullOrWhiteSpace(o.SchoolName)).Select(o => o.UserID).ToList();
                var listSp = SchoolProfileBusiness.All.Where(o => listAdminOp.Contains(o.AdminID.Value)).Select(o => new { o.AdminID, o.SchoolName, o.SchoolCode }).ToList();
                Dictionary<int, Menu> dicParentMenu = new Dictionary<int, Menu>();
                foreach (ActionAuditViewModel item in list)
                {
                    item.TotalTime = 0;
                    if (item.BeginDate.HasValue && item.EndDate.HasValue)
                    {
                        item.TotalTime = ((item.EndDate.Value - item.BeginDate.Value).TotalSeconds);
                    }
                    // Ten truong
                    if (string.IsNullOrWhiteSpace(item.SchoolName))
                    {
                        var sp = listSp.Where(o => o.AdminID == item.UserID).FirstOrDefault();
                        if (sp != null)
                        {
                            item.SchoolName = sp != null ? sp.SchoolName : "";
                            item.SchoolCode = sp != null ? sp.SchoolCode : "";
                        }
                    }
                    // Lay ten chuc nang
                    string menuUrl = (item.ControllerName + "area").ToLower() + "/" + item.ControllerName.ToLower();
                    if (!allArray.Contains(item.ControllerName.ToLower()))
                    {
                        var listMenu = menus.FindAll(x => x.URL.ToLower().Contains(menuUrl));
                        if (listMenu.Count > 0)
                        {
                            Menu firstMn = listMenu.First();
                            Menu parMn = null;
                            if (firstMn.ParentID.HasValue)
                            {
                                if (dicParentMenu.ContainsKey(firstMn.ParentID.Value))
                                {
                                    parMn = dicParentMenu[firstMn.ParentID.Value];
                                }
                                else
                                {
                                    parMn = firstMn.Menu2;
                                    dicParentMenu[firstMn.ParentID.Value] = parMn;
                                }
                                string ParentName = (string)HttpContext.GetGlobalResourceObject("MenuResources", parMn.MenuName);
                                item.ParentName = ParentName != null ? ParentName : firstMn.Menu2.MenuName;
                            }
                            string MenuName = (string)HttpContext.GetGlobalResourceObject("MenuResources", firstMn.MenuName);
                            item.MenuName = MenuName != null ? MenuName : firstMn.MenuName;
                        }
                    }
                    else
                    {
                        foreach (string keySpecial in dicSpecial.Keys)
                        {
                            if (dicSpecial[keySpecial].Any(o => item.ControllerName.Trim().ToLower().Contains(o)))
                            {
                                var listS = menus.FindAll(x => x.URL.ToLower().Contains(keySpecial));
                                if ((listS == null || listS.Count == 0) && !MenuCancel.Contains(keySpecial))
                                {
                                    var listT = menus.FindAll(x => x.URL.ToLower().Contains(menuUrl));
                                    if (listT != null && listT.Count > 0)
                                    {
                                        Menu firstMn = listT.First();
                                        Menu parMn = null;
                                        if (firstMn.ParentID.HasValue)
                                        {
                                            if (dicParentMenu.ContainsKey(firstMn.ParentID.Value))
                                            {
                                                parMn = dicParentMenu[firstMn.ParentID.Value];
                                            }
                                            else
                                            {
                                                parMn = firstMn.Menu2;
                                                dicParentMenu[firstMn.ParentID.Value] = parMn;
                                            }
                                            string ParentName = (string)HttpContext.GetGlobalResourceObject("MenuResources", parMn.MenuName);
                                            item.ParentName = ParentName != null ? ParentName : firstMn.Menu2.MenuName;
                                        }
                                        string MenuName = (string)HttpContext.GetGlobalResourceObject("MenuResources", firstMn.MenuName);
                                        item.MenuName = MenuName != null ? MenuName : firstMn.MenuName;
                                    }
                                }
                                else
                                {
                                    if (listS != null && listS.Count > 0)
                                    {
                                        Menu firstMn = listS.First();
                                        Menu parMn = null;
                                        if (firstMn.ParentID.HasValue)
                                        {
                                            if (dicParentMenu.ContainsKey(firstMn.ParentID.Value))
                                            {
                                                parMn = dicParentMenu[firstMn.ParentID.Value];
                                            }
                                            else
                                            {
                                                parMn = firstMn.Menu2;
                                                dicParentMenu[firstMn.ParentID.Value] = parMn;
                                            }
                                            string ParentName = (string)HttpContext.GetGlobalResourceObject("MenuResources", parMn.MenuName);
                                            item.ParentName = ParentName != null ? ParentName : firstMn.Menu2.MenuName;
                                        }
                                        string MenuName = (string)HttpContext.GetGlobalResourceObject("MenuResources", firstMn.MenuName);
                                        item.MenuName = MenuName != null ? MenuName : firstMn.MenuName;
                                    }
                                }
                            }
                        }
                    }
                    lstT.Add(item);
                }
                Paging.List = lstT;
            }
            var count = Paging != null ? Paging.total : 0;

            return Paging;
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult _GridBinding(string ControllerName, string ActionName, string SchoolName,
            DateTime? FromDate, DateTime? ToDate, string UserName, string MinTime, int page = 1, string orderBy = "")
        {
            int m = 0;
            bool isNumeric = string.IsNullOrWhiteSpace(MinTime) ? true : int.TryParse(MinTime, out m);
            if (!isNumeric)
            {
                throw new BusinessException("Thời gian xử lý tối thiểu phải là số nguyên");
            }
            Paginate<ActionAuditViewModel> lst = this._Search(ControllerName, ActionName, SchoolName, FromDate, ToDate, UserName, m, page, orderBy);
            if (lst != null)
            {
                GridModel<ActionAuditViewModel> gm = new GridModel<ActionAuditViewModel>(lst.List);
                gm.Total = lst.total;
                return View(gm);
            }
            else
            {
                return View();
            }
        }

        public FileResult ExportExcel(string ControllerName, string ActionName, string SchoolName,
            string FromDate, string ToDate, string UserName)
        {
            if (string.IsNullOrWhiteSpace(FromDate) || string.IsNullOrWhiteSpace(ToDate))
            {
                throw new BusinessException("Bạn phải nhập ngày tác động để lấy báo cáo chính xác");
            }
            DateTime dFromDate = DateTime.Parse(FromDate);
            DateTime dToDate = DateTime.Parse(ToDate);
            Paginate<ActionAuditViewModel> p = this._Search(ControllerName, ActionName, SchoolName, dFromDate, dToDate,
                UserName, null, 0);
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HT", "HT_TH_TacDongHeThong.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            var listSchoolName = p.List.Select(o => new { o.SchoolName, o.SchoolCode }).Distinct().ToList();
            var listDataAll = p.List.GroupBy(o => new { o.SchoolName, o.UserName, o.MenuName, o.ParentName })
                .Select(o => new { o.Key.SchoolName, o.Key.UserName, o.Key.MenuName, o.Key.ParentName, Count = o.Count() }).ToList();
            IVTRange bottomRow = firstSheet.GetRange("A8", "F8");
            int countAdmin = 0;
            foreach (var sp in listSchoolName)
            {
                countAdmin++;
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                if (sp.SchoolCode != null)
                {
                    if (sp.SchoolCode.Length > 30)
                    {
                        sheet.Name = sp.SchoolCode.Substring(0, 30);
                    }
                    else
                    {
                        sheet.Name = sp.SchoolCode;
                    }
                }
                else
                {
                    sheet.Name = "ADMIN" + countAdmin;
                }
                var listData = listDataAll.Where(o => o.SchoolName == sp.SchoolName).OrderBy(o => o.ParentName).ThenBy(o => o.MenuName).ToList();
                List<object> ExportDic = new List<object>();
                int currentRow = 8;
                for (int i = 0; i < listData.Count; i++)
                {
                    var d = listData[i];
                    sheet.CopyPasteSameRowHeigh(bottomRow, currentRow, true);
                    Dictionary<string, object> dataDic = new Dictionary<string, object>();
                    dataDic["Order"] = i + 1;
                    dataDic["ParentName"] = d.ParentName;
                    dataDic["MenuName"] = d.MenuName;
                    dataDic["ActionCount"] = d.Count;
                    dataDic["UserName"] = d.UserName;
                    ExportDic.Add(dataDic);
                    currentRow++;
                }
                Dictionary<string, object> KingDic = new Dictionary<string, object>();
                KingDic.Add("list", ExportDic);
                KingDic.Add("SchoolName", sp.SchoolName);
                KingDic.Add("StartDate", dFromDate.ToString("dd/MM/yyyy"));
                KingDic.Add("EndDate", dToDate.ToString("dd/MM/yyyy"));
                sheet.FillVariableValue(KingDic);
            }
            firstSheet.Delete();
            Stream excel = oBook.ToStream();

            // Fix chrome file type
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportUtils.StripVNSign("Tong_hop_tac_dong_he_thong.xls");
            return result;
        }
    }
}
