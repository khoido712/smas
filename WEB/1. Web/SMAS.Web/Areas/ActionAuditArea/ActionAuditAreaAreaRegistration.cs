﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ActionAuditArea
{
    public class ActionAuditAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ActionAuditArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ActionAuditArea_default",
                "ActionAuditArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
