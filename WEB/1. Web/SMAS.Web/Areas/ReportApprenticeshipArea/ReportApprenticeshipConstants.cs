﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportApprenticeshipArea
{
    public class ReportApprenticeshipConstants
    {
        public const string LIST_ApprenticeshipClass = "LIST_ApprenticeshipClass";
        public const string LIST_PUPIL_APPRENTICESHIP = "LIST_Pupil_Apprenticeship";
    }
}