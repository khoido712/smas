﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.ReportApprenticeshipArea.Models;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;

namespace SMAS.Web.Areas.ReportApprenticeshipArea.Controllers
{
    public class ReportApprenticeshipController : Controller
    {
        private readonly IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness;
        private readonly IApprenticeshipClassBusiness ApprenticeshipClassBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;

        public ReportApprenticeshipController(IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness, IApprenticeshipClassBusiness ApprenticeshipClassBusiness, ISummedUpRecordBusiness SummedUpRecordBusiness)
        {
            this.ApprenticeshipTrainingBusiness = ApprenticeshipTrainingBusiness;
            this.ApprenticeshipClassBusiness = ApprenticeshipClassBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
        }

        //
        // GET: /DistrictArea/District/

        public ActionResult Index(SearchViewModel frm)
        {
            GetApprenticeshipClassList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            GlobalInfo glo = new GlobalInfo();
            dic["AcademicYearID"] = glo.AcademicYearID.GetValueOrDefault();
            dic["SchoolID"] = glo.SchoolID.Value;
            dic["ApprenticeshipClassID"] = frm.ApprenticeshipClassID;
            dic["PupilCode"] = frm.PupilCode;
            dic["FullName"] = frm.FullName;
            List<ApprenticeshipTrainingBO> lst = ApprenticeshipTrainingBusiness.GetListPupilApprenticeshipTraining(dic) ;
            ViewData[ReportApprenticeshipConstants.LIST_PUPIL_APPRENTICESHIP] = lst;
            return View();
        }

        //
        // GET: /DistrictArea/District/Search

        
        [SkipCheckRole]
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo glo = new GlobalInfo();
            SearchInfo["ApprenticeshipClassID"] = frm.ApprenticeshipClassID;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.GetValueOrDefault();
            SearchInfo["SchoolID"] = glo.SchoolID.GetValueOrDefault();

            List<ApprenticeshipTrainingBO> lst = ApprenticeshipTrainingBusiness.GetListPupilApprenticeshipTraining(SearchInfo);
            ViewData[ReportApprenticeshipConstants.LIST_PUPIL_APPRENTICESHIP] = lst;

            //GetApprenticeshipClassList();

            return PartialView("_List");
        }
        

        /// <summary>
        /// Lấy về danh sách các tỉnh thành và gán vào ViewData
        /// </summary>
        private void GetApprenticeshipClassList()
        {
            GlobalInfo glo = new GlobalInfo();
            int schoolID = glo.SchoolID.Value;
            int academicYearID = glo.AcademicYearID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            ViewData[ReportApprenticeshipConstants.LIST_ApprenticeshipClass] = new SelectList(ApprenticeshipClassBusiness.SearchBySchool(schoolID,dic).ToList(),"ApprenticeshipClassID", "ClassName");
        }

        //Xuất báo cáo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public FileResult ExportExcel(FormCollection col, ApprenticeshipTrainingBO reportViewModel)
        {
            GlobalInfo global = new GlobalInfo();
          

            int? apprenticeshipClassID = 0;
            if (reportViewModel.ApprenticeshipClassID != 0)
            {
                apprenticeshipClassID = reportViewModel.ApprenticeshipClassID;
            }


            string pupilCode = "";
            if (reportViewModel.PupilCode != null)
            {
                pupilCode = reportViewModel.PupilCode;
            }
            string fullName = "";
            if (reportViewModel.FullName != null)
            {
                fullName = reportViewModel.FullName;
            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ApprenticeshipClassID"] = apprenticeshipClassID;
            SearchInfo["PupilCode"] = pupilCode;
            SearchInfo["FullName"] = fullName;
            SearchInfo["AcademicYearID"] = global.AcademicYearID.GetValueOrDefault();
            SearchInfo["SchoolID"] = global.SchoolID.GetValueOrDefault();

            List<ApprenticeshipTrainingBO> lst = ApprenticeshipTrainingBusiness.GetListPupilApprenticeshipTraining(SearchInfo);
            //if (reportViewModel.FacultyID != null)
            //{
            //    FacultyID = reportViewModel.FacultyID.Value;
            //}
            List<ApprenticeshipTrainingBO> lstApp = new List<ApprenticeshipTrainingBO>();
            foreach (var p in lst)
            {
                int ApprenticeshipClassID = p.ApprenticeshipClassID.Value;
                int PupilID = p.PupilID;
                string valCheck = col["SemesterApp" + ApprenticeshipClassID.ToString() + PupilID.ToString()];
                if (valCheck != null && valCheck.ToLower() == "true")
                {
                    lstApp.Add(p);
                    //-----
                }
            }
            Stream excel = ApprenticeshipTrainingBusiness.CreateReportApprenticeshipTraining(global.SchoolID.Value, global.AcademicYearID.Value, apprenticeshipClassID, pupilCode, fullName, lstApp);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
           
            string ReportName = "HS_[SchoolLevel]_PhieuHocNghe.xls";
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
            //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);
            //ReportName = ReportName.Replace("[Semester]", semester);
            ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

            result.FileDownloadName = ReportName;

            return result;
        }
    }
}
