using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportApprenticeshipArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ApprenticeshipTraining_Label_ApprenticeshipClassID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ReportApprenticeshipConstants.LIST_ApprenticeshipClass)]
        public Nullable<int> ApprenticeshipClassID { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode{get;set;}

        [ResourceDisplayName("ApprenticeshipTraining_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }
    }
}
