﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportApprenticeshipArea
{
    public class ReportApprenticeshipAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportApprenticeshipArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportApprenticeshipArea_default",
                "ReportApprenticeshipArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
