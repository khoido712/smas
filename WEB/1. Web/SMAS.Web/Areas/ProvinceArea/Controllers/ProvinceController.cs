﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ProvinceArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.ProvinceArea.Controllers
{
    public class ProvinceController : BaseController
    {
        private readonly IProvinceBusiness ProvinceBusiness;

        public ProvinceController(IProvinceBusiness provinceBusiness)
        {
            this.ProvinceBusiness = provinceBusiness;
        }
        #region index
        //
        // GET: /Province/

        public ActionResult Index()
        {
            SetViewDataPermission("Province", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here

            IEnumerable<ProvinceViewModel> lst = this._Search(SearchInfo);
            ViewData[ProvinceConstants.LIST_PROVINCE] = lst;
            return View();
        }

        #endregion
        #region Search

        //
        // GET: /Province/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceName"] = frm.ProvinceName;
            SearchInfo["ProvinceCode"] = frm.ProvinceCode;
            //add search info
            //

            IEnumerable<ProvinceViewModel> lst = this._Search(SearchInfo);
            ViewData[ProvinceConstants.LIST_PROVINCE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ProvinceViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("Province", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<Province> query = this.ProvinceBusiness.Search(SearchInfo);
            IQueryable<ProvinceViewModel> lst = query.Select(o => new ProvinceViewModel
            {
                ProvinceID = o.ProvinceID,
                ProvinceName = o.ProvinceName,
                ProvinceCode = o.ProvinceCode,
                ShortName = o.ShortName,
                Description = o.Description,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate


            });

            return lst.OrderBy(o=>o.ProvinceName).ToList();
        }
        #endregion

        #region Create
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Province", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Province province = new Province();
            TryUpdateModel(province);
            province.IsActive = true;
            Utils.Utils.TrimObject(province);

            this.ProvinceBusiness.Insert(province);
            this.ProvinceBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ProvinceID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Province", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Province province = this.ProvinceBusiness.Find(ProvinceID);
            TryUpdateModel(province);
            Utils.Utils.TrimObject(province);
            this.ProvinceBusiness.Update(province);
            this.ProvinceBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        #region Update
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Province", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.ProvinceBusiness.Delete(id);
            this.ProvinceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion


        #region Detail

        [ValidateAntiForgeryToken]
        public PartialViewResult Detail(int id)
        {
            Province province = this.ProvinceBusiness.Find(id);
            ProvinceViewModel frm = new ProvinceViewModel();
            Utils.Utils.BindTo(province, frm);
            return PartialView("_Detail", frm);
        }
        #endregion
    }
}





