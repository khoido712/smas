﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ProvinceArea
{
    public class ProvinceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ProvinceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ProvinceArea_default",
                "ProvinceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
