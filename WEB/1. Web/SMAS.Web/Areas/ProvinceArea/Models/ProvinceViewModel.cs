/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ProvinceArea.Models
{
    public class ProvinceViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 ProvinceID { get; set; }

        [ResourceDisplayName("Province_Label_ProvinceName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String ProvinceName { get; set; }

        [ResourceDisplayName("Province_Label_ProvinceCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String ProvinceCode { get; set; }

        [ResourceDisplayName("Province_Label_ShortName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String ShortName { get; set; }

        [ResourceDisplayName("Province_Label_Description")]       
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [AdditionalMetadata("rows", "5")]
        [DataType(DataType.MultilineText)]
		public System.String Description { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Province_Label_CreatedDate")]      
		public System.Nullable<System.DateTime> CreatedDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Province_Label_IsActive")]      
		public System.Nullable<System.Boolean> IsActive { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Province_Label_ModifiedDate")]      
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }								
	       
    }
}


