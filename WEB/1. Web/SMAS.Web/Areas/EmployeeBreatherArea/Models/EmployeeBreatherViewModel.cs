﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using SMAS.Models.CustomAttribute;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Areas.EmployeeBreatherArea;
namespace SMAS.Web.Areas.EmployeeBreatherArea.Models
{
    public class EmployeeBreatherViewModel
    {
        [ScaffoldColumn(false)]
        public Int32 EmployeeBreatherID { get; set; }

        [ScaffoldColumn(false)]
        public Int32 TeacherID { get; set; }

        [ScaffoldColumn(false)]
        public string SchoolID { get; set; }

        [ScaffoldColumn(false)]
        public Int32 AcademicYearID { get; set; }

        [ResourceDisplayName("Employee_Label_DateBreather")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("DateTimePickerBeforeNow")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public Nullable<DateTime> DateBreather { get; set; }

        [ResourceDisplayName("Employee_Label_ReasonBreather")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        // [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        //[DataType(DataType.MultilineText)]
        // [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("ViewDataKey", EmployeeBreatherConstant.LIST_REASON)]
        [AdditionalMetadata("OnChange", "ReasonChange(this)")]
        public int VacationReasonID { get; set; }

        [ResourceDisplayName("Common_Label_Note")]
        [UIHint("Textbox")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        //[DataType(DataType.MultilineText)]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string Note { get; set; }

        [ResourceDisplayName("Lbl_Com_Back_Do_Work")]
        public bool? IsComeBack { get; set; }

        [ResourceDisplayName("Employee_Label_StartWorkBreather")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("DateTimePickerBeforeNow")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public Nullable<DateTime> DateStartWork { get; set; }

    }
}


