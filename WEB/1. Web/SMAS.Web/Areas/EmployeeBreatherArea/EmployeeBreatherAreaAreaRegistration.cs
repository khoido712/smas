﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeBreatherArea
{
    public class EmployeeBreatherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeBreatherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeBreatherArea_default",
                "EmployeeBreatherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
