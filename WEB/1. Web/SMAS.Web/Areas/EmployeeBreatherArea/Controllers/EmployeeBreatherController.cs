﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.EmployeeBreatherArea.Models;

namespace SMAS.Web.Areas.EmployeeBreatherArea.Controllers
{
    public class EmployeeBreatherController : BaseController
    {
        private readonly IEmployeeBreatherBusiness EmployeeBreatherBusiness;
        public EmployeeBreatherController(IEmployeeBreatherBusiness employeeBreatherBusiness)
        {
            this.EmployeeBreatherBusiness = employeeBreatherBusiness;
        }
        /// <summary>
        /// ham insert hoac update can bo tam nghi
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult InsertOrUpdate(FormCollection frm)
        {

            int employeeBreatherID = frm["EmployeeBreatherID"] != "" ? int.Parse(frm["EmployeeBreatherID"]) : 0;
            EmployeeBreather employeeBreather = new SMAS.Models.Models.EmployeeBreather();
            EmployeeBreatherViewModel ebvm = new EmployeeBreatherViewModel();
            bool isComeBack = !string.IsNullOrEmpty(frm["IsComeBack"]) ? ("false".Equals(frm["IsComeBack"]) ? false : true) : false;
            TryUpdateModel(employeeBreather);
            TryUpdateModel(ebvm);
            //if (!ebvm.DateBreather.HasValue)
            //{
            //    return Json(new JsonMessage(Res.Get("EmployeeSeverance_Label_DateFormat"), "error"));
            //}
            Utils.Utils.TrimObject(employeeBreather);
            employeeBreather.EmployeeBreatherID = employeeBreatherID;
            employeeBreather.SchoolID = _globalInfo.SchoolID.Value;
            employeeBreather.AcademicYearID = _globalInfo.AcademicYearID.Value;
            employeeBreather.DateBreather = ebvm.DateBreather.Value;
            employeeBreather.DateStartWork = ebvm.DateStartWork;
            employeeBreather.Note = ebvm.Note;
            employeeBreather.IsComeBack = ebvm.IsComeBack;
            employeeBreather.VacationReasonID = ebvm.VacationReasonID;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"IsChecked",isComeBack},
            };
            this.EmployeeBreatherBusiness.InsertOrUpdateBreather(employeeBreather,dic);
            this.EmployeeBreatherBusiness.Save();
            return Json(new JsonMessage(string.Format(Res.Get("EmployeeBreather_Success_Edit"), frm["EmployeeName"])));
        }

        /// <summary>
        /// Delete can bo tam nghi
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EmployeeBreatherBusiness.DeleteBreather(id, _globalInfo.SchoolID.Value);
            this.EmployeeBreatherBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

    }
}





