/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.MessageArea
{
    public class MessageConstants
    {
        public const string LIST_MESSAGE = "listMessage";
        public const string LIST_STATUS = "listStatus";

        public const string LIST_SEND_TYPE = "listSendType";
        public const string LIST_SCHOOL_FACULTY = "listSchoolFaculty";

        public const string LIST_GROUP_USER = "listGroupUser";
        public const string LIST_DISTRICT = "listDistrict";

        public const string LIST_EMPLOYEE = "listEmployee";

        public const string VIEW_DISTRICT = "viewDistrict";
        public const string GROUPUSER_EMPLOYEE = "guEmployee";
        public const string GROUPUSER_DEPTADMIN = "guDeptAdmin";
        public const string GROUPUSER_SCHOOLADMIN = "guSchoolAdmin";

        public const int EMPLOYEE = 1;
        public const int SUB_SUPERVISINGDEPT_ADMIN = 2;
        public const int SCHOOL_ADMIN = 3;
    }
}