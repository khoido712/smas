﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MessageArea
{
    public class MessageAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MessageArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MessageArea_default",
                "MessageArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
