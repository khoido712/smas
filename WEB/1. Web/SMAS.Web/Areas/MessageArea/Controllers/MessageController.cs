﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MessageArea.Models;

namespace SMAS.Web.Areas.MessageArea.Controllers
{
    public class MessageController : BaseController
    {        
        private readonly IMessageBusiness MessageBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
		
		public MessageController (IMessageBusiness MessageBusiness,
            IUserAccountBusiness UserAccountBusiness,
            IEmployeeBusiness EmployeeBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            IDistrictBusiness DistrictBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness)
		{
			this.MessageBusiness = MessageBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
		}
		
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region search
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			SearchInfo["Title"] = frm.TitleMessage;
            SearchInfo["IsRead"] = frm.IsRead;
            SearchInfo["FromDate"] = frm.FromDate;
            SearchInfo["ToDate"] = frm.ToDate;
            SearchInfo["UserNameSend"] = frm.UserNameSend;

            UserAccount UserAccount = this.UserAccountBusiness.Find(global.UserAccountID);
            SearchInfo["UserNameReceive"] = UserAccount.aspnet_Users.UserName;

            IEnumerable<MessageViewModel> lst = this._Search(SearchInfo);
            ViewData[MessageConstants.LIST_MESSAGE] = lst;

            return PartialView("_List");
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<MessageViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<Message> query = this.MessageBusiness.Search(SearchInfo);
            IQueryable<MessageViewModel> lst = query.Select(o => new MessageViewModel {               
						MessageID = o.MessageID,								
						UserNameSend = o.UserNameSend,								
						UserNameReceive = o.UserNameReceive,								
						Title = o.Title,								
						Content = o.Content,								
						SendDate = o.SendDate,								
						EmailReceive = o.EmailReceive,								
						isRead = o.IsRead,
		                TitleMessage = o.Title
				
            });

            List<MessageViewModel> lsMessageViewModelViewModel = lst.ToList();
            foreach (MessageViewModel item in lsMessageViewModelViewModel)
            {
                item.Status = Res.Get("Common_Label_UnreadMessage");
                if (item.isRead == true)
                {
                    item.Status = Res.Get("Common_Label_ReadMessage");
                }
            }
            return lsMessageViewModelViewModel;
        }

        private IEnumerable<SendMessageViewModel> _SearchEmployee(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<Employee> query = null;
            List<SendMessageViewModel> lsMessageViewModelViewModel = new List<SendMessageViewModel>();
            if (global.IsSchoolRole)
                query = this.EmployeeBusiness.SearchTeacher(global.SchoolID.Value, SearchInfo);
            else if (global.IsSubSuperVisingDeptRole || global.IsSuperVisingDeptRole)
                query = this.EmployeeBusiness.SearchEmployee(global.SupervisingDeptID.Value, SearchInfo);

            if (query!=null && query.Count()>0)
            {
            IQueryable<SendMessageViewModel> lst = query.Select(o => new SendMessageViewModel
            {
                ReceiverID = o.EmployeeID,
                FullName = o.FullName,
                SchoolFacultyID = o.SchoolFacultyID,
                Email = o.Email

            });
                lsMessageViewModelViewModel = lst.ToList();
                IDictionary<string, object> SearchAccount = new Dictionary<string, object>();
                foreach (SendMessageViewModel item in lsMessageViewModelViewModel)
                {
                    SearchAccount["EmployeeID"] = item.ReceiverID;
                    UserAccount objUserAccount = UserAccountBusiness.SearchUser(SearchAccount).FirstOrDefault();
                    if (objUserAccount != null)
                        item.UserName = objUserAccount.aspnet_Users.UserName;
                    item.ViewCheckbox = true;
                }
            }
            
            return lsMessageViewModelViewModel;
        }

        #endregion search

        #region setviewdata

        public void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            ViewData[MessageConstants.LIST_STATUS] = new SelectList(CommonList.MessageReadStatus(), "key", "value");

            // Danh sach Message
            UserAccount UserAccount = this.UserAccountBusiness.Find(global.UserAccountID);
            SearchInfo["UserNameReceive"] = UserAccount.aspnet_Users.UserName;

            IEnumerable<MessageViewModel> lstMessage = this._Search(SearchInfo);
            ViewData[MessageConstants.LIST_MESSAGE] = lstMessage;

            // Danh sach Nhan vien
            if (global.IsSuperVisingDeptRole)
                SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
            IEnumerable<SendMessageViewModel> lst = this._SearchEmployee(SearchInfo);
            ViewData[MessageConstants.LIST_EMPLOYEE] = lst;

            // Hinh thuc gui tin
            ViewData[MessageConstants.LIST_SEND_TYPE] = new SelectList(CommonList.SendNotificationType(), "key", "value");

            // To bo mon
            if (global.IsSchoolRole)
            {
                SearchInfo["SchoolID"] = global.SchoolID;
                IQueryable<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo);
                ViewData[MessageConstants.LIST_SCHOOL_FACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");
            }

            // Quan huyen
            SearchInfo = new Dictionary<string, object>();
            List<District> lstDistrict = new List<District>();
            if (global.IsSuperVisingDeptRole)
            {
                SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
                lstDistrict = DistrictBusiness.Search(SearchInfo).ToList();
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                District district = SupervisingDeptBusiness.Find(global.SupervisingDeptID).District;
                lstDistrict.Add(district);
            }
            ViewData[MessageConstants.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName");

            // Nhom nguoi su dung
            List<ComboObject> listGroupUser = new List<ComboObject>();
            listGroupUser.Add(new ComboObject(MessageConstants.EMPLOYEE.ToString(), Res.Get("Message_Label_GroupUserEmployee")));
            listGroupUser.Add(new ComboObject(MessageConstants.SUB_SUPERVISINGDEPT_ADMIN.ToString(), Res.Get("Message_Label_GroupUserDeptAdmin")));
            listGroupUser.Add(new ComboObject(MessageConstants.SCHOOL_ADMIN.ToString(), Res.Get("Message_Label_GroupUserSchoolAdmin")));
            ViewData[MessageConstants.LIST_GROUP_USER] = new SelectList(listGroupUser, "key", "value");

            ViewData[MessageConstants.VIEW_DISTRICT] = false;
            ViewData[MessageConstants.GROUPUSER_EMPLOYEE] = true;
            ViewData[MessageConstants.GROUPUSER_DEPTADMIN] = false;
            ViewData[MessageConstants.GROUPUSER_SCHOOLADMIN] = false;
        }

        #endregion setviewdata

        #region reload grid


        [ValidateAntiForgeryToken]
        public ActionResult ReloadGrid(int? SendType, int? SchoolFacultyID, int? GroupUser, int? DistrictID)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<SendMessageViewModel> lst = null;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            if (global.IsSchoolRole)
            {
                SearchInfo["FacultyID"] = SchoolFacultyID;
                IQueryable<Employee> query = EmployeeBusiness.SearchTeacher(global.SchoolID.Value, SearchInfo);
                lst = query.Select(o => new SendMessageViewModel
                {
                    ReceiverID = o.EmployeeID,
                    FullName = o.FullName,
                    SchoolFacultyID = o.SchoolFacultyID,
                    Email = o.Email,

                    ViewCheckbox = SendType.HasValue && ((SendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_SYSTEM_ACCOUNT)
                                            || (SendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_EMAIL_ACCOUNT && o.Email != null && o.Email.Length > 0))
                });

                List<SendMessageViewModel> lsMessageViewModelViewModel = lst.ToList();
                IDictionary<string, object> SearchAccount = new Dictionary<string, object>();
                foreach (SendMessageViewModel item in lst)
                {
                    SearchAccount["EmployeeID"] = item.ReceiverID;
                    UserAccount objUserAccount = UserAccountBusiness.SearchUser(SearchAccount).FirstOrDefault();
                    if (objUserAccount != null)
                        item.UserName = objUserAccount.aspnet_Users.UserName;
                }
            }
            else
            {
                SearchInfo["ProvinceID"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).ProvinceID;
                SearchInfo["DistrictID"] = DistrictID;

                if (GroupUser.HasValue)
                {
                    ViewData[MessageConstants.VIEW_DISTRICT] = (GroupUser == 2 || GroupUser == 3);

                    ViewData[MessageConstants.GROUPUSER_EMPLOYEE] = false;
                    ViewData[MessageConstants.GROUPUSER_DEPTADMIN] = false;
                    ViewData[MessageConstants.GROUPUSER_SCHOOLADMIN] = false;

                    if (GroupUser == MessageConstants.EMPLOYEE)
                    {
                        ViewData[MessageConstants.GROUPUSER_EMPLOYEE] = true;
                        SearchInfo["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF;
                        IQueryable<Employee> query = EmployeeBusiness.SearchByArea(SearchInfo);
                        lst = query.Select(o => new SendMessageViewModel
                        {
                            ReceiverID = o.EmployeeID,
                            FullName = o.FullName,
                            SchoolFacultyID = o.SchoolFacultyID,
                            Email = o.Email,
                            ViewCheckbox = SendType.HasValue && ((SendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_SYSTEM_ACCOUNT)
                                            || (SendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_EMAIL_ACCOUNT && o.Email != null && o.Email.Length > 0))
                        });

                        List<SendMessageViewModel> lsMessageViewModelViewModel = lst.ToList();
                        IDictionary<string, object> SearchAccount = new Dictionary<string, object>();
                        foreach (SendMessageViewModel item in lst)
                        {
                            SearchAccount["EmployeeID"] = item.ReceiverID;
                            UserAccount objUserAccount = UserAccountBusiness.SearchUser(SearchAccount).FirstOrDefault();
                            if (objUserAccount != null)
                                item.UserName = objUserAccount.aspnet_Users.UserName;
                        }
                    }
                    else if (GroupUser == MessageConstants.SUB_SUPERVISINGDEPT_ADMIN)
                    {
                        ViewData[MessageConstants.GROUPUSER_DEPTADMIN] = true;
                        SearchInfo["TraversalPath"] = SupervisingDeptBusiness.Find(global.SupervisingDeptID).TraversalPath;
                        IQueryable<SupervisingDept> query = SupervisingDeptBusiness.Search(SearchInfo);

                        lst = query.Select(o => new SendMessageViewModel
                        {
                            ReceiverID = o.SupervisingDeptID,
                            DeptName = o.SupervisingDeptName,
                            DistrictID = o.DistrictID,
                            DistrictName = o.District.DistrictName,
                            UserName = o.UserAccount.aspnet_Users.UserName,
                            Email = o.Email,
                            ViewCheckbox = SendType.HasValue && ((SendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_SYSTEM_ACCOUNT)
                                            || (SendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_EMAIL_ACCOUNT && o.Email != null && o.Email.Length > 0))

                        });
                    }
                    else if (GroupUser == MessageConstants.SCHOOL_ADMIN)
                    {
                        ViewData[MessageConstants.GROUPUSER_SCHOOLADMIN] = true;
                        SearchInfo["SupervisingDept"] = global.SupervisingDeptID.Value;
                        IQueryable<SchoolProfile> query = SchoolProfileBusiness.Search(SearchInfo);
                        lst = query.Select(o => new SendMessageViewModel
                        {
                            ReceiverID = o.SchoolProfileID,
                            SchoolName = o.SchoolName,
                            DistrictID = o.DistrictID,
                            Email = o.Email,
                            DistrictName = o.District.DistrictName,
                            UserName = o.UserAccount.aspnet_Users.UserName,
                            ViewCheckbox = SendType.HasValue && ((SendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_SYSTEM_ACCOUNT)
                                            || (SendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_EMAIL_ACCOUNT && o.Email != null && o.Email.Length > 0))
                        });
                    }
                }
            }
            
            ViewData[MessageConstants.LIST_EMPLOYEE] = lst;
            return View("_SendList");
        }

        #endregion reload grid

        #region Send Message
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendMessage(int[] checkedToSend, string TitleMessage, string ContentMessage, int cboSendType, int? cboGroupUser)
        {
            if (TitleMessage.Trim().Length == 0)
                return Json(new JsonMessage(Res.Get("Message_Label_ErrorTitleEmpty")));
            if (ContentMessage.Trim().Length == 0)
                return Json(new JsonMessage(Res.Get("Message_Label_ErrorContentEmpty")));

            GlobalInfo global = new GlobalInfo();
            List<Message> lstMessage = new List<Message>();

            foreach (var item in checkedToSend)
            {
                Message m = new Message();
                UserAccount UserAccount = this.UserAccountBusiness.Find(global.UserAccountID);
                m.UserNameSend = UserAccount.aspnet_Users.UserName;
                m.Title = TitleMessage;
                m.Content = ContentMessage;
                m.SendDate = DateTime.Now;
                m.IsRead = false;

                string UserNameReceiver = "";
                string EmailReceiver = "";
                if (global.IsSchoolRole)
                {
                    Employee e = EmployeeBusiness.Find(item);
                    EmailReceiver = e.Email;

                    IDictionary<string, object> SearchAccount = new Dictionary<string, object>();
                    SearchAccount["EmployeeID"] = item;
                    UserAccount objUserAccount = UserAccountBusiness.SearchUser(SearchAccount).FirstOrDefault();
                    if (objUserAccount != null)
                        UserNameReceiver = objUserAccount.aspnet_Users.UserName;                    
                }
                else
                {
                    if (cboGroupUser == MessageConstants.EMPLOYEE)
                    {
                        Employee e = EmployeeBusiness.Find(item);
                        EmailReceiver = e.Email;

                        IDictionary<string, object> SearchAccount = new Dictionary<string, object>();
                        SearchAccount["EmployeeID"] = item;
                        UserAccount objUserAccount = UserAccountBusiness.SearchUser(SearchAccount).FirstOrDefault();
                        if (objUserAccount != null)
                            UserNameReceiver = objUserAccount.aspnet_Users.UserName;
                    }
                    else if (cboGroupUser == MessageConstants.SUB_SUPERVISINGDEPT_ADMIN)
                    {
                        SupervisingDept dept = SupervisingDeptBusiness.Find(item);
                        UserNameReceiver = dept.UserAccount.aspnet_Users.UserName;
                        EmailReceiver = dept.Email;
                    }
                    else if (cboGroupUser == MessageConstants.SCHOOL_ADMIN)
                    {
                        SchoolProfile school = SchoolProfileBusiness.Find(item);
                        UserNameReceiver = school.UserAccount.aspnet_Users.UserName;
                        EmailReceiver = school.Email;
                    }
                }

                if (cboSendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_SYSTEM_ACCOUNT)
                {
                    m.UserNameReceive = UserNameReceiver;
                }
                else if (cboSendType == SystemParamsInFile.SEND_NOTIFICATION_TYPE_EMAIL_ACCOUNT)
                {
                    m.EmailReceive = EmailReceiver;
                }
                MessageBusiness.Insert(m);
                MessageBusiness.Save();
            }
            
            //MessageBusiness.Insert(lstMessage);
            //MessageBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion Send Message

        #region Delete Message

        [ValidateAntiForgeryToken]
        public ActionResult DeleteMessage(int[] checkedToDelete)
        {
            List<Message> lstMessage = new List<Message>();

            foreach (var item in checkedToDelete)
            {
                Message message = MessageBusiness.Find(item);
                if (message != null)
                {
                    MessageBusiness.Delete(item);
                    MessageBusiness.Save();
                }
            }
            SetViewData();
            return View("_List");
        }
        #endregion Delete Message

        #region Detail

        [ValidateAntiForgeryToken]
        public PartialViewResult Detail(int id)
        {
            Message message = this.MessageBusiness.Find(id);
            MessageViewModel frm = new MessageViewModel();
            Utils.Utils.BindTo(message, frm);
            frm.TitleMessage = frm.Title;
            frm.Status = Res.Get("Common_Label_UnreadMessage");
            if (frm.isRead)
            {
                frm.Status = Res.Get("Common_Label_ReadMessage");
            }
            return PartialView("_Detail", frm);
        }
        #endregion

        #region Update Message Status

        public void UpdateMessageStatus(int MessageID)
        {
            Message message = this.MessageBusiness.Find(MessageID);
            if (!message.IsRead)
            {
                message.IsRead = true;
                MessageBusiness.Update(message);
                MessageBusiness.Save();
            }
        }

        #endregion Update Message Status
    }
}





