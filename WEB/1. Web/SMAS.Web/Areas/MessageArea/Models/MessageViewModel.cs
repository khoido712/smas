/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.MessageArea.Models
{
    public class MessageViewModel
    {
		public System.Int32 MessageID { get; set; }

        public System.String UserNameReceive { get; set; }

        public System.String Content { get; set; }

        public System.String EmailReceive { get; set; }

        public System.String Title { get; set; }

        [ResourceDisplayName("Message_Label_Title")]
        public System.String TitleMessage { get; set; }

        [ResourceDisplayName("Message_Label_UserNameSend")]
        public System.String UserNameSend { get; set; }

        [ResourceDisplayName("Message_Label_SendDate")]
        [UIHint("DateTimePicker")]
		public System.DateTime SendDate { get; set; }

		public System.Boolean isRead { get; set; }

        [ResourceDisplayName("Message_Label_Status")]
        public string Status { get; set; }			
	       
    }
}


