/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.MessageArea.Models
{
    public class SendMessageViewModel
    {
        public int ReceiverID { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolName")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("Messsage_Label_DeptName")]
        public string DeptName { get; set; }

        [ResourceDisplayName("UserAccount_Label_UserName")]
        public string UserName { get; set; }

        [ResourceDisplayName("Employee_Label_Email")]
        public string Email { get; set; }

        [ResourceDisplayName("SchoolFaculty_Control_FacultyName")]
        public string SchoolFacultyName { get; set; }

        public Nullable<int> SchoolFacultyID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_District")]
        public string DistrictName { get; set; }

        public System.Nullable<System.Int32> DistrictID { get; set; }

        public bool ViewCheckbox { get; set; }
	       
    }
}


