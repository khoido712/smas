/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.MessageArea.Models
{
    public class SendSearchViewModel
    {
        [ResourceDisplayName("Employee_Label_SendType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MessageConstants.LIST_SEND_TYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "onSendTypeChange(this)")]
        public int? SendType { get; set; }

        [ResourceDisplayName("Employee_Label_SchoolFaculty")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MessageConstants.LIST_SCHOOL_FACULTY)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "onSchoolFacultyChange(this)")]
        public int? SchoolFaculty { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MessageConstants.LIST_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "onDistrictChange(this)")]
        public int? District { get; set; }
    }
}