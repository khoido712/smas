/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.MessageArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Message_Label_Title")]
        [UIHint("Textbox")]
        public string TitleMessage { get; set; }

        [ResourceDisplayName("Message_Label_Status")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MessageConstants.LIST_STATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        public bool? IsRead { get; set; }

        [ResourceDisplayName("Message_Label_FromDate")]
        [UIHint("DateTimePicker")]
        public DateTime? FromDate { get; set; }

        [ResourceDisplayName("Message_Label_ToDate")]
        [UIHint("DateTimePicker")]
        public DateTime? ToDate { get; set; }

        [ResourceDisplayName("Message_Label_UserNameSend")]
        [UIHint("Textbox")]
        public string UserNameSend { get; set; }
    }
}