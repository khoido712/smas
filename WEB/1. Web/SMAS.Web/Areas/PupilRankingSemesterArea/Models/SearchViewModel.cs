/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PupilRankingSemesterArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilRanking_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilRankingConstants.LS_EDUCATIONLEVEL)]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("OnChange", "onCboEducationLevelCascadeChange(this.options[this.selectedIndex].value);")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("PupilRanking_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilRankingConstants.LS_CLASS)]
        [AdditionalMetadata("PlaceHolder", "1")]
        public int ClassID { get; set; }

        [ResourceDisplayName("PupilRanking_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilRankingConstants.LS_SEMESTER)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? Semester { get; set; }

        public string Warning { get; set; }
    }
}
