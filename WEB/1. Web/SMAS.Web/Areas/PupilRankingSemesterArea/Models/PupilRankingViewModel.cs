/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.PupilRankingSemesterArea.Models
{
    public class PupilRankingViewModel
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }

        public int ClassID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationLevelID { get; set; }

        public long? PupilRankingID { get; set; }
        public string CapacityLevel { get; set; }
        public int? CapacityLevelID { get; set; }
        public string ConductLevel { get; set; }
        public int? ConductLevelID { get; set; }
        public decimal? AverageMark { get; set; }
        public int? Rank { get; set; }

        public DateTime? EndDate { get; set; }        

        public bool IsShowData { get; set; }
        public int Status { get; set; }

        public List<SubjectViewModel> ListSubject { get; set; }
    }

    public class SubjectViewModel
    {
        public string SubjectName { get; set; }
        public int SubjectID { get; set; }
        public int IsCommenting { get; set; }
        public int? AppliedType { get; set; }
        public bool IsExempted { get; set; }
        public string DisplayMark { get; set; }
        public decimal? SummedUpMark { get; set; }
        public string JudgementResult { get; set; }
        public decimal? ReTestMark { get; set; }
        public string ReTestJudgement { get; set; }
    }

}


