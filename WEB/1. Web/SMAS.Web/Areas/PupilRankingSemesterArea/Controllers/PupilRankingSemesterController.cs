﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.ClassSubjectArea.Models;
using SMAS.Web.Areas.PupilRankingSemesterArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System.Transactions;
using SMAS.Web.Filter;
using System.Configuration;

namespace SMAS.Web.Areas.PupilRankingSemesterArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilRankingSemesterController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IConductConfigByCapacityBusiness ConductConfigByCapacityBu;
        private readonly IConductConfigByViolationBusiness ConductConfigByViolationBu;
        private readonly IMarkTypeBusiness MarkTypeBu;
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBu;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness;
        private readonly IPupilRankingHistoryBusiness PupilRankingHistoryBusiness;

        public PupilRankingSemesterController(IJudgeRecordBusiness JudgeRecordBusiness, ISchoolProfileBusiness SchoolProfileBusiness
                                                , IReportDefinitionBusiness ReportDefinitionBusiness, IPupilRankingBusiness pupilrankingBusiness
                                                , IAcademicYearBusiness AcademicYearBusiness, IPupilOfClassBusiness PupilOfClassBusiness
                                                , IExemptedSubjectBusiness ExemptedSubjectBusiness, ISummedUpRecordBusiness SummedUpRecordBusiness
                                                , IClassSubjectBusiness ClassSubjectBusiness, IClassProfileBusiness ClassProfileBusiness
                                                , ISemeterDeclarationBusiness semeterDeclarationBusiness, ISchoolMovementBusiness schoolMovementBusiness
                                                , IClassMovementBusiness classMovementBusiness, IPupilLeavingOffBusiness pupilLeavingOffBusiness
                                                , ISummedUpRecordClassBusiness summedUpRecordClassBusiness, IMarkRecordBusiness markRecordBusiness
                                                , IConductConfigByCapacityBusiness conductConfigByCapacityBu, IConductConfigByViolationBusiness conductConfigByViolationBu
                                                , IMarkTypeBusiness markTypeBu, ILockedMarkDetailBusiness lockMarkDetailBu
                                                , IPupilProfileBusiness PupilProfileBusiness
                                                , ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness
                                                , IRegisterSubjectSpecializeBusiness registerSubjectSpecializeBusiness,
                                                IPupilRankingHistoryBusiness _PupilRankingHistoryBusiness
                            )
        {
            this.PupilRankingBusiness = pupilrankingBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.SemeterDeclarationBusiness = semeterDeclarationBusiness;
            this.SchoolMovementBusiness = schoolMovementBusiness;
            this.ClassMovementBusiness = classMovementBusiness;
            this.PupilLeavingOffBusiness = pupilLeavingOffBusiness;
            this.SummedUpRecordClassBusiness = summedUpRecordClassBusiness;
            this.MarkRecordBusiness = markRecordBusiness;
            this.ConductConfigByCapacityBu = conductConfigByCapacityBu;
            this.ConductConfigByViolationBu = conductConfigByViolationBu;
            this.MarkTypeBu = markTypeBu;
            this.LockedMarkDetailBu = lockMarkDetailBu;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
            this.RegisterSubjectSpecializeBusiness = registerSubjectSpecializeBusiness;
            this.PupilRankingHistoryBusiness = _PupilRankingHistoryBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo globalInfo = new GlobalInfo();
            // disable button xep hang
            ViewData[PupilRankingConstants.ENABLE_RANK] = false;
            if (!globalInfo.HasHeadTeacherPermission(0))
                throw new BusinessException("PupilRankingSemester_Validate_NotPermission");

            ViewData[PupilRankingConstants.LS_CLASS] = new SelectList(new string[] { });
            ViewData[PupilRankingConstants.LS_ClassSubject] = new List<ClassSubjectBO>();
            ViewData[PupilRankingConstants.LIST_PUPILONGRID] = new List<PupilRankingViewModel>();
            LoadSemester();
            LoadEducationLevel();
            return View();
        }

        #region LoadEducationLevel

        public void LoadEducationLevel()
        {
            List<EducationLevel> lsEducationLevel = _globalInfo.EducationLevels;
            if (lsEducationLevel != null)
            {
                ViewData[PupilRankingConstants.LS_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[PupilRankingConstants.LS_EDUCATIONLEVEL] = new SelectList(new string[] { });
            }
        }

        #endregion LoadEducationLevel

        #region LoadSemester

        private void LoadSemester()
        {
            List<ComboObject> lsSemester = new List<ComboObject>();
            lsSemester.Add(new ComboObject(SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_FIRST.ToString(), Res.Get("Common_Label_FirstSemester")));
            lsSemester.Add(new ComboObject(SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_SECOND.ToString(), Res.Get("Common_Label_SecondSemester")));
            lsSemester.Add(new ComboObject(SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_ALL.ToString(), Res.Get("PupilRanking_Label_SemesterAll")));

            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int semester = acaYear.FirstSemesterEndDate.HasValue && acaYear.FirstSemesterEndDate > DateTime.Now
                                ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                : (acaYear.SecondSemesterEndDate.HasValue && acaYear.SecondSemesterEndDate > DateTime.Now
                                        ? SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        : SystemParamsInFile.SEMESTER_OF_YEAR_ALL);

            ViewData[PupilRankingConstants.LS_SEMESTER] = new SelectList(lsSemester, "key", "value", semester.ToString());
        }

        #endregion LoadSemester

        #region Load Class

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingOldClass(int? EducationLevelId)
        {
            Session["lstSummedUpRecord"] = null;
            Session["lstPupilRanking"] = null;
            return _GetClass(EducationLevelId);
        }

        private JsonResult _GetClass(int? ID)
        {
            if (ID.HasValue)
            {
                //•	AcademicYearID = UserInfo.AcademicYearID
                GlobalInfo globalInfo = new GlobalInfo();
                int? AcademicYearID = globalInfo.AcademicYearID;
                List<ClassProfile> lsClass = _GetClassProfile(AcademicYearID, ID);
                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        private List<ClassProfile> _GetClassProfile(int? AcademicYearID, int? EducationLevelID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", AcademicYearID.Value);
            dic.Add("EducationLevelID", EducationLevelID);
            if (!globalInfo.IsAdminSchoolRole && !globalInfo.IsViewAll)
            {
                dic.Add("UserAccountID", globalInfo.UserAccountID);
                dic.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER);
            }
            dic.Add("IsVNEN", true);
            List<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            return lsClass;
        }

        #endregion Load Class


        public PartialViewResult LoadGrid(int ClassID, int Semester)
        {
            bool permissionBGH = _globalInfo.IsEmployeeManager;
            if (ClassID == 0 && !permissionBGH)
            {
                ViewData[PupilRankingConstants.LS_CLASS] = new SelectList(new string[] { });
                ViewData[PupilRankingConstants.LS_ClassSubject] = new List<ClassSubjectBO>();
                ViewData[PupilRankingConstants.LIST_PUPILONGRID] = new List<PupilRankingViewModel>();
                LoadSemester();
                LoadEducationLevel();
                return PartialView("Index");
            }
            string semesterStr = " Học Kỳ " + Semester;
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //Load danh sach mon hoc
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = ClassID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
            dicSubject["IsVNEN"] = true;
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                dicSubject["Semester"] = Semester;
                iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
            }
            else
            {
                semesterStr = " Cả năm ";
                iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject).Where(u => u.SectionPerWeekFirstSemester > 0 || u.SectionPerWeekSecondSemester > 0);
            }

            List<int?> lstIncreseID = iqSubject.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease > 0).Select(p => p.SubjectIDIncrease).ToList();
            //loc bo ra nhung mon tang cuong
            iqSubject = iqSubject.Where(p => !lstIncreseID.Contains(p.SubjectID));

            List<ClassSubjectBO> lstSubject = iqSubject.Select(o => new ClassSubjectBO()
            {
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                DisplayName = o.SubjectCat.DisplayName,
                SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                IsCommenting = o.IsCommenting,
                AppliedType = o.AppliedType,
                Is_Specialize_Subject = o.IsSpecializedSubject
            }).ToList().OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            // Load danh sach hoc sinh
            List<PupilRankingViewModel> lstPupilView = SearchData(ClassID, Semester, lstSubject);
            // Sắp xếp danh sách học sinh
            lstPupilView = lstPupilView.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            ViewData[PupilRankingConstants.LIST_PUPILONGRID] = lstPupilView;
            string className = ClassProfileBusiness.Find(ClassID).DisplayName;
            ViewData[PupilRankingConstants.TITLE_PAGE] = Res.Get("PupilRankingSemester_List") + semesterStr + " lớp " + className;

            ViewData[PupilRankingConstants.LS_ClassSubject] = lstSubject;
            ViewData[PupilRankingConstants.SELECTED_CLASS] = ClassID;
            ViewData[PupilRankingConstants.SELECTED_SEMESTER] = Semester;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["Semester"] = Semester;
            SemeterDeclaration semesterDeClaration = SemeterDeclarationBusiness.Search(dic).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            if (semesterDeClaration != null)
            {
                if (semesterDeClaration.IsLock == true)
                {
                    ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = false;
                    ViewData[PupilRankingConstants.ENABLE_RANK] = false;
                }
                if (semesterDeClaration.IsLock == false)
                {
                    ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = true;
                }
            }

            //Lay ngay ket thuc cua hoc ky
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                ViewData[PupilRankingConstants.SEMESTER_END_DATE] = acaYear.FirstSemesterEndDate;
            else
                ViewData[PupilRankingConstants.SEMESTER_END_DATE] = acaYear.SecondSemesterEndDate;

            // Sử dụng để check disable/enable button Xếp hạng
            int countRank = 0;
            foreach (PupilRankingViewModel pr in lstPupilView)
            {
                if (pr.CapacityLevelID != null)
                {
                    countRank++;
                }
            }
            ViewData[PupilRankingConstants.ENABLE_RANK] = false;
            #region kiem tra enable nut tong ket
            int countLockedSubject = 0;
            Dictionary<int, string> dicMarkType = MarkRecordBusiness.GetLockMarkTitleBySubject(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, Semester);
            foreach (var subject in lstSubject)
            {
                if (dicMarkType.ContainsKey(subject.SubjectID))
                {
                    string strSubjectMarkTitle = dicMarkType[subject.SubjectID];
                    if (strSubjectMarkTitle.Contains(SystemParamsInFile.MARK_TYPE_LHK)) countLockedSubject++;
                }
            }
            if (_globalInfo.IsCurrentYear == false)
            {
                ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = false;
                ViewData[PupilRankingConstants.ENABLE_RANK] = false;
            }
            else
            {
                bool check = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
                if (check == false)
                {
                    ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = false;
                    ViewData[PupilRankingConstants.ENABLE_RANK] = false;
                }
                else
                {
                    if (_globalInfo.IsAdminSchoolRole)
                    {
                        ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = true;
                        // Kiem tra xem neu da tong ket roi thi enable button Xep hang
                        if (countRank > 0)
                        {
                            ViewData[PupilRankingConstants.ENABLE_RANK] = true;
                        }
                    }
                    else
                    {
                        if (countLockedSubject != lstSubject.Count())
                        {
                            ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                      ? (acaYear != null && acaYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= acaYear.FirstSemesterEndDate)
                                                                                      : (acaYear != null && acaYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= acaYear.SecondSemesterEndDate);
                            // Kiem tra xem neu da tong ket roi thi enable button Xep hang
                            ViewData[PupilRankingConstants.ENABLE_RANK] = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                      ? (acaYear != null && acaYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= acaYear.FirstSemesterEndDate)
                                                                                      : (acaYear != null && acaYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= acaYear.SecondSemesterEndDate);
                        }
                        else
                        {
                            ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = false;
                            ViewData[PupilRankingConstants.ENABLE_RANK] = false;
                        }
                    }
                }
            }
            if (lstPupilView == null)
            {
                ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = false;
                ViewData[PupilRankingConstants.ENABLE_RANK] = false;
            }
            // Lấy thông tin cấu hình xếp loại hạnh kiểm
            int rankingType = GetRankingType();
            /* Nếu cấu hình xếp loại hạnh kiểm theo vi phạm (rankingType = 1) hoặc cấu hình xếp loại hạnh kiểm theo học lực (rankingType = 0) và 
                không chọn tiêu chí xếp hạng theo hạnh kiểm thì button Xep hang disable */
            if (rankingType == 1 || (rankingType == 0 && (acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK ||
                                                          acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_CAPACITY ||
                                                          acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)))
            {
                ViewData[PupilRankingConstants.ENABLE_RANK] = false;
            }
            // Kiem tra enable nut xuat excel
            ViewData[PupilRankingConstants.ENABLE_EXCEL] = true;
            if (lstPupilView.Count() == 0 || lstSubject == null || lstSubject.Count == 0)
            {
                ViewData[PupilRankingConstants.ENABLE_EXCEL] = false;
                ViewData[PupilRankingConstants.ENABLE_SUMMEDUP] = false;
                ViewData[PupilRankingConstants.ENABLE_RANK] = false;
            }
            #endregion


            return PartialView("_List");
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        [ValidateAntiForgeryToken]
        public JsonResult SumUp(int ClassID, int Semester)
        {
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            if (!UtilsBusiness.HasHeadTeacherPermissionSupervising(_globalInfo.UserAccountID, ClassID) && !UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID))
                return Json(new JsonMessage(Res.Get("PupilRankingSemester_Validate_NotPermission"), JsonMessage.ERROR));

            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                            ? (acaYear == null || (acaYear.FirstSemesterStartDate > DateTime.Now && DateTime.Now > acaYear.FirstSemesterEndDate))
                                            : (acaYear == null || (acaYear.SecondSemesterStartDate > DateTime.Now && DateTime.Now > acaYear.SecondSemesterEndDate)))
                return Json(new JsonMessage(Res.Get("PupilRankingSemester_Validate_NotInSemester"), JsonMessage.ERROR));

            SummedUpRecordClass objSummedUpRecordClass = new SummedUpRecordClass
            {
                AcademicYearID = acaYear.AcademicYearID,
                ClassID = ClassID,
                PeriodID = null,
                Semester = Semester,
            };
            //Kiem tra lop co dang thuc hien xep loai HK, tong ket, xep loai khong
            if (SummedUpRecordClassBusiness.CheckExistsSummedExcuting(objSummedUpRecordClass))
            {
                return Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_Summeding"), JsonMessage.ERROR));
            }

            ///Insert trang thai ban ghi dang thuc hien tong ket diem
            objSummedUpRecordClass.CreatedDate = DateTime.Now;
            objSummedUpRecordClass.EducationLevelID = classProfile.EducationLevelID;
            objSummedUpRecordClass.SchoolID = acaYear.SchoolID;
            objSummedUpRecordClass.NumberOfPupil = 0;
            objSummedUpRecordClass.Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1;
            objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING;
            objSummedUpRecordClass.SynchronizeID = 0;
            SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);

            //Load danh sach mon hoc
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = ClassID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["IsVNEN"] = true;
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL) dicSubject["Semester"] = Semester;
            // Mon hoc cho lop va mon hoc
            IQueryable<ClassSubject> iqSC = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
            //Loc ra nhung mon tang cuong
            List<int?> lstIncreaseID = iqSC.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease > 0).Select(p => p.SubjectIDIncrease).ToList();
            iqSC = iqSC.Where(p => !lstIncreaseID.Contains(p.SubjectID));
            var lstSubjectClass = (from o in iqSC
                                   select new
                                   {
                                       cs = o,
                                       subjectCat = o.SubjectCat
                                   }).ToList();


            // Mon hoc cho lop
            List<ClassSubject> lstSubject = lstSubjectClass.Select(o => o.cs)
                .ToList();
            // Danh sach mon hoc
            List<SubjectCat> listSubjectCat = lstSubjectClass.Select(o => o.subjectCat).ToList();
            List<ClassSubjectBO> lstSubjectBO = new List<ClassSubjectBO>();
            foreach (ClassSubject cs in lstSubject)
            {
                SubjectCat subject = listSubjectCat.Find(o => o.SubjectCatID == cs.SubjectID);
                lstSubjectBO.Add(new ClassSubjectBO()
                {
                    ClassID = cs.ClassID,
                    SubjectID = cs.SubjectID,
                    OrderInSubject = subject.OrderInSubject,
                    DisplayName = subject.DisplayName,
                    SectionPerWeekSecondSemester = cs.SectionPerWeekSecondSemester,
                    SectionPerWeekFirstSemester = cs.SectionPerWeekFirstSemester,
                    IsCommenting = cs.IsCommenting,
                    AppliedType = cs.AppliedType
                });
            }

            List<PupilRankingViewModel> lstPupilView = SearchData(ClassID, Semester, lstSubjectBO)
                                                            .Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                                                            .ToList();

            //Lấy những học sinh đang ký môn chuyên,môn tự chọn dành cho trường chuyên
            IDictionary<string, object> dicRegister = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                //{"SemesterID",Semester},
                {"YearID",acaYear.Year}
            };
            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegister);

            // Lấy những học sinh đã có đủ điểm của các môn khong phai mien giam va mon bat buoc tinh diem
            lstPupilView = lstPupilView.Where(u => !u.ListSubject.Any(v => !v.SummedUpMark.HasValue && string.IsNullOrEmpty(v.JudgementResult) && !v.ReTestMark.HasValue
                                                                        && string.IsNullOrEmpty(v.ReTestJudgement)
                                                                        && v.AppliedType.HasValue && v.AppliedType.Value == SystemParamsInFile.SUBJECT_TYPE_MANDATORY
                                                                        && !v.IsExempted)).ToList();

            List<SummedUpRecord> lstSummedUp = new List<SummedUpRecord>();
            List<PupilRanking> lstPupilRanking = new List<PupilRanking>();
            PupilRankingViewModel pupilView = null;
            PupilRanking pr = null;
            SubjectViewModel subjectView = null;
            SummedUpRecord sur = null;
            for (int i = 0; i < lstPupilView.Count; i++)
            {
                pupilView = lstPupilView[i];
                //}
                //foreach (PupilRankingViewModel pupilView in lstPupilView)
                //{
                pr = new PupilRanking();
                pr.AcademicYearID = pupilView.AcademicYearID;
                pr.PupilID = pupilView.PupilID;
                pr.SchoolID = _globalInfo.SchoolID.Value;
                pr.ClassID = pupilView.ClassID;
                pr.EducationLevelID = pupilView.EducationLevelID;
                pr.CreatedAcademicYear = acaYear.Year;
                pr.Semester = Semester;
                pr.PeriodID = null;
                pr.AverageMark = pupilView.AverageMark;
                pr.CapacityLevelID = pupilView.CapacityLevelID;
                pr.ConductLevelID = pupilView.ConductLevelID;
                pr.Rank = pupilView.Rank;
                //+ Môn: SubjectID
                lstPupilRanking.Add(pr);
                // Bo cac mon mien giam va cac mon khong co diem
                var listTempSubject = pupilView.ListSubject.Where(u => !u.IsExempted
                    && (u.SummedUpMark.HasValue || !string.IsNullOrEmpty(u.JudgementResult) || u.ReTestMark.HasValue
                                                                        || !string.IsNullOrEmpty(u.ReTestJudgement))
                    ).ToList();
                for (int j = 0; j < listTempSubject.Count; j++)
                {
                    subjectView = listTempSubject[j];
                    //}
                    //foreach (SubjectViewModel subjectView in listTempSubject)
                    //{
                    sur = new SummedUpRecord();
                    sur.AcademicYearID = pupilView.AcademicYearID;
                    sur.ClassID = pupilView.ClassID;
                    sur.IsCommenting = subjectView.IsCommenting;
                    sur.PeriodID = null;
                    sur.PupilID = pupilView.PupilID;
                    sur.SchoolID = _globalInfo.SchoolID.Value;
                    sur.Semester = Semester;
                    sur.SubjectID = subjectView.SubjectID;
                    sur.SubjectCat = listSubjectCat.Find(o => o.SubjectCatID == subjectView.SubjectID);
                    sur.SummedUpDate = DateTime.Now;
                    if (subjectView.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        sur.JudgementResult = !string.IsNullOrEmpty(subjectView.ReTestJudgement) ? subjectView.ReTestJudgement : subjectView.JudgementResult;
                    else
                        sur.SummedUpMark = subjectView.ReTestMark.HasValue ? subjectView.ReTestMark.Value : subjectView.SummedUpMark;
                    sur.CreatedAcademicYear = acaYear.Year;
                    lstSummedUp.Add(sur);
                }
            }

            // Lấy thông tin cấu hình xếp loại hạnh kiểm
            int rankingType = GetRankingType();

            /* Nếu cấu hình xếp loại hạnh kiểm theo vi phạm (rankingType = 1) hoặc cấu hình xếp loại hạnh kiểm theo học lực (rankingType = 0) và 
                không chọn tiêu chí xếp hạng theo hạnh kiểm thì button Tổng kết sẽ thực hiện cả nhiệm vụ tổng kết lẫn xếp hạng */
            if (rankingType == 1 || (rankingType == 0 && (acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK ||
                                                          acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_CAPACITY ||
                                                          acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)))
            {
                #region Tổng kết điểm kỳ gồm cả xếp hạng, tổng kết điểm kỳ theo khối
                // Thuc hien tong ket
                try
                {
                    if (UtilsBusiness.IsMoveHistory(acaYear))
                    {
                        PupilRankingHistoryBusiness.RankingPupilOfClassHistory(_globalInfo.UserAccountID, lstSummedUp, lstPupilRanking, null, lstSubject, false, GlobalConstants.NOT_AUTO_MARK, lstRegisterSubjectBO);
                    }
                    else
                    {
                        this.PupilRankingBusiness.RankingPupilOfClass(_globalInfo.UserAccountID, lstSummedUp, lstPupilRanking, null, lstSubject, false, GlobalConstants.NOT_AUTO_MARK, lstRegisterSubjectBO);
                    }

                }
                finally
                {
                    // Thuc hien insert thong tin vao SummedUpClass
                    SummedUpRecordClass summedUpRecordClass = new SummedUpRecordClass
                    {
                        AcademicYearID = acaYear.AcademicYearID,
                        Semester = Semester,
                        PeriodID = null,
                        ClassID = ClassID,
                        SchoolID = acaYear.SchoolID,
                        Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1,
                        EducationLevelID = classProfile.EducationLevelID,
                        NumberOfPupil = lstPupilRanking.Count,
                        SynchronizeID = 0,
                        Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE

                    };
                    SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(summedUpRecordClass);
                }



                #endregion
            }
            else
            {
                // Nếu cấu hình xếp loại hạnh kiểm theo học lực (rankingType = 0) và Cấu hình tiêu chí xếp hạng có chọn tiêu chí Hạnh kiểm thì sau khi tổng kết xong,
                // enable button xếp hạng và hiển thị message nhắc nhở người dùng xếp loại hạnh kiểm trước khi xếp hạng.
                #region Tổng kết điểm kỳ không bao gồm xếp hạng, tổng kết điểm kỳ theo khối
                // Thuc hien tong ket
                try
                {
                    if (UtilsBusiness.IsMoveHistory(acaYear))
                    {
                        this.PupilRankingHistoryBusiness.RankingPupilOfClassNotRankHistory(_globalInfo.UserAccountID, lstSummedUp, lstPupilRanking, null, lstSubject, false, lstRegisterSubjectBO);
                    }
                    else
                    {
                        this.PupilRankingBusiness.RankingPupilOfClassNotRank(_globalInfo.UserAccountID, lstSummedUp, lstPupilRanking, null, lstSubject, false, lstRegisterSubjectBO);
                    }
                }
                finally
                {
                    //this.PupilRankingBusiness.Save();
                    // Thuc hien insert thong tin vao SummedUpClass
                    SummedUpRecordClass summedUpRecordClass = new SummedUpRecordClass
                    {
                        AcademicYearID = acaYear.AcademicYearID,
                        Semester = Semester,
                        PeriodID = null,
                        ClassID = ClassID,
                        SchoolID = acaYear.SchoolID,
                        Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1,
                        EducationLevelID = classProfile.EducationLevelID,
                        NumberOfPupil = lstPupilRanking.Count,
                        SynchronizeID = 0,
                        Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE

                    };
                    SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(summedUpRecordClass);
                }
                ViewData[PupilRankingConstants.ENABLE_RANK] = true;
                #endregion
            }
            JsonResult message = null;
            if (rankingType != 1) // Cau hinh theo hoc luc
            {
                if (acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT ||
                                            acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_CONDUCT ||
                                            acaYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT)
                {
                    message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_SumupRankWarning")));
                }
                else
                {
                    message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_SumupRankSuccess")));
                }
            }
            if (rankingType == 1) // cau hinh theo vi pham
            {
                message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_SumupSuccess")));
            }
            return message;
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        [ValidateAntiForgeryToken]
        public JsonResult Rank(int ClassID, int Semester)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            JsonResult message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_RankSuccess")));
            AcademicYear academicYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID);
            int year = academicYear.Year;
            int SchoolID = globalInfo.SchoolID.Value;
            int modSchoolID = SchoolID % 100;
            List<PupilOfClassRanking> lstPupilRanking = new List<PupilOfClassRanking>();
            // Lay danh sach hoc sinh da tong ket de xep hang
            Dictionary<int, int?> dicOldConduct = new Dictionary<int, int?>();
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                lstPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, Semester, ClassID, null).ToList();
            }
            else
            {
                lstPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, Semester, ClassID, null).ToList();
                int semesterBackTraining = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                List<PupilOfClassRanking> listBackTraining = PupilRankingBusiness.GetPupilRankingOfClass(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, semesterBackTraining, ClassID, null).Where(o => o.PupilRankingID != null).ToList();
                if (listBackTraining != null && listBackTraining.Count() > 0)
                {
                    foreach (PupilOfClassRanking item in lstPupilRanking)
                    {
                        PupilOfClassRanking pupilBackTraing = listBackTraining.FirstOrDefault(o => o.PupilID == item.PupilID);
                        if (pupilBackTraing != null && pupilBackTraing.ConductLevelID.HasValue)
                        {
                            dicOldConduct[item.PupilID] = item.ConductLevelID;
                            item.ConductLevelID = pupilBackTraing.ConductLevelID;
                        }
                    }
                }
            }
            List<PupilRanking> lstPupil = new List<PupilRanking>();
            // Lấy thông tin cấu hình xếp loại hạnh kiểm
            int rankingType = GetRankingType();
            bool isRelateConduct = true;
            /* Nếu cấu hình xếp loại hạnh kiểm theo vi phạm (rankingType = 1) hoặc cấu hình xếp loại hạnh kiểm theo học lực (rankingType = 0) và 
                không chọn tiêu chí xếp hạng theo hạnh kiểm thì button Xep hang disable */
            if (rankingType == 1 || (rankingType == 0 && (academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK ||
                                                          academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_CAPACITY ||
                                                          academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)))
            {
                lstPupil = (from lst in lstPupilRanking
                            where lst.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && lst.PupilRankingID.HasValue
                            select new PupilRanking
                            {
                                PupilRankingID = lst.PupilRankingID.Value,
                                PupilID = lst.PupilID,
                                ClassID = lst.ClassID.Value,
                                SchoolID = lst.SchoolID,
                                AcademicYearID = lst.AcademicYearID,
                                CreatedAcademicYear = year,
                                Last2digitNumberSchool = modSchoolID,
                                Semester = lst.Semester,
                                AverageMark = lst.AverageMark,
                                CapacityLevelID = lst.CapacityLevelID,
                                ConductLevelID = lst.ConductLevelID,
                                PeriodID = lst.PeriodID,
                                EducationLevelID = lst.EducationLevelID.Value,
                                RankingDate = DateTime.Now,
                                PupilRankingComment = lst.PupilRankingComment
                            }).ToList();

                isRelateConduct = true;
            }
            else
            {
                lstPupil = (from lst in lstPupilRanking
                            where lst.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && lst.PupilRankingID.HasValue
                            select new PupilRanking
                            {
                                PupilRankingID = lst.PupilRankingID.Value,
                                PupilID = lst.PupilID,
                                ClassID = lst.ClassID.Value,
                                SchoolID = lst.SchoolID,
                                AcademicYearID = lst.AcademicYearID,
                                CreatedAcademicYear = year,
                                Last2digitNumberSchool = modSchoolID,
                                Semester = lst.Semester,
                                AverageMark = lst.AverageMark,
                                CapacityLevelID = lst.CapacityLevelID,
                                ConductLevelID = lst.ConductLevelID,
                                PeriodID = lst.PeriodID,
                                EducationLevelID = lst.EducationLevelID.Value,
                                RankingDate = DateTime.Now,
                                PupilRankingComment = lst.PupilRankingComment
                            }).ToList();
            }
            // Neu khong lien quan den hanh kiem ma chua co HS nao co diem TBM thi bao loi
            if (!isRelateConduct)
            {
                if (lstPupil.Where(o => o.AverageMark.HasValue).Count() == 0)
                {
                    message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_SumupRankWarningMark"), JsonMessage.ERROR)); // Thầy/cô thực hiện xếp loại hạnh kiểm để xếp hạng cho học sinh
                }
            }
            else
            {
                if (lstPupil.Where(o => o.ConductLevelID.HasValue).Count() == 0)
                {
                    message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_SumupRankWarningConduct"), JsonMessage.ERROR)); // Thầy/cô thực hiện xếp loại hạnh kiểm để xếp hạng cho học sinh
                }
            }
            if (lstPupil.Count == 0)
            {
                // Khong co thong tin hoc sinh thi bao loi
                message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_NoPupilRanking"), JsonMessage.ERROR)); // Thầy/cô thực hiện xếp loại hạnh kiểm để xếp hạng cho học sinh
            }
            else
            {
                // Neu la truong GDTX thi tu dong lay hanh kiem Tot doi voi HS ko thuoc dien xep loai HK
                #region GDTX - Gan lai hanh kiem
                SchoolProfile schoolProfile = academicYear.SchoolProfile;
                bool isGDTX = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
                // Lay danh sach hoc sinh de kiem tra dieu kien co thuoc dien xep hanh kiem hay khong                
                int applyLevel = _globalInfo.AppliedLevel.Value;
                if (isGDTX)
                {
                    List<int> listPupilID = lstPupil.Select(o => o.PupilID).ToList();
                    var listPupilGDTX = PupilProfileBusiness.All.Where(o => o.CurrentSchoolID == _globalInfo.SchoolID && o.CurrentAcademicYearID == _globalInfo.AcademicYearID
                                 && o.CurrentClassID == ClassID && listPupilID.Contains(o.PupilProfileID))
                                 .Select(o => new
                                 {
                                     o.PupilProfileID,
                                     o.BirthDate,
                                     o.PupilLearningType
                                 })
                                 .ToList();
                    int count = lstPupil.Count;
                    for (int i = count - 1; i >= 0; i--)
                    {
                        var item = lstPupil[i];
                        bool isNotConductRank = false;
                        var pupilInfo = listPupilGDTX.FirstOrDefault(o => o.PupilProfileID == item.PupilID);
                        if (pupilInfo != null)
                        {
                            if (pupilInfo.PupilLearningType.HasValue
                                && (pupilInfo.PupilLearningType.Value == SystemParamsInFile.PUPIL_LEARNING_TYPE_SELF_LEARNING
                                || pupilInfo.PupilLearningType.Value == SystemParamsInFile.PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING))
                            {
                                isNotConductRank = true;
                            }
                            else
                            {
                                if (applyLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY && (DateTime.Now - pupilInfo.BirthDate).Days / 365 >= 20)
                                {
                                    isNotConductRank = true;
                                }
                                else if (applyLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && (DateTime.Now - pupilInfo.BirthDate).Days / 365 >= 25)
                                {
                                    isNotConductRank = true;
                                }
                            }
                        }
                        if (isNotConductRank)
                        {
                            // Thuoc dien khong xep loai hanh kiem thi gan la HK Tot nhung ko luu vao CSDL                           
                            if (applyLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                            {
                                item.ConductLevelID = SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY;
                            }
                            else if (applyLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            {
                                item.ConductLevelID = SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY;
                            }
                            dicOldConduct[item.PupilID] = null;
                        }
                        else
                        {
                            // Neu thuoc dien phai co hanh kiem
                            // Va viec xep hang co lien quan den hanh kiem
                            // nhung chua co hanh kiem thi bo qua khong xep hang
                            if (!item.ConductLevelID.HasValue)
                            {
                                if (academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT
                                    || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT)
                                {
                                    lstPupil.RemoveAt(i);
                                }
                            }
                        }
                    }
                }
                #endregion
                // Thực hiện xếp hạng
                PupilRankingBusiness.Rank(academicYear.RankingCriteria, lstPupil, false);
                // Thuc hien luu vao db
                if (UtilsBusiness.IsMoveHistory(academicYear))
                {
                    List<PupilRankingHistory> lstHistory = PupilRankingHistoryBusiness.ConvertListToHistory(lstPupil);
                    foreach (PupilRankingHistory item in lstHistory)
                    {
                        item.MSourcedb = "0";//truong de phan biet voi tool TKD tu dong
                        // Lay lai hanh kiem cu cua ca nam
                        if (dicOldConduct.ContainsKey(item.PupilID))
                        {
                            item.ConductLevelID = dicOldConduct[item.PupilID];
                        }
                        if (item.PupilRankingID == 0)
                        {
                            PupilRankingHistoryBusiness.Insert(item);
                        }
                        else
                        {
                            PupilRankingHistoryBusiness.Update(item);
                        }
                    }
                    PupilRankingHistoryBusiness.Save();
                }
                else
                {
                    foreach (PupilRanking item in lstPupil)
                    {
                        item.MSourcedb = "0";//truong de phan biet voi tool TKD tu dong
                        // Lay lai hanh kiem cu cua ca nam
                        if (dicOldConduct.ContainsKey(item.PupilID))
                        {
                            item.ConductLevelID = dicOldConduct[item.PupilID];
                        }
                        if (item.PupilRankingID == 0)
                        {
                            PupilRankingBusiness.Insert(item);
                        }
                        else
                        {
                            PupilRankingBusiness.Update(item);
                        }
                    }
                    PupilRankingBusiness.Save();
                }
            }
            return message;
        }

        private List<PupilRankingViewModel> SearchData(int ClassID, int Semester, List<ClassSubjectBO> listSubject)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            Dictionary<string, object> dicPupil = new Dictionary<string, object>();
            dicPupil["ClassID"] = ClassID;
            dicPupil["AcademicYearID"] = globalInfo.AcademicYearID.Value;

            AcademicYear acaYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID);
            DateTime semesterEndDate = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate.Value : acaYear.SecondSemesterEndDate.Value;
            // Lay danh sach hoc sinh
            bool isNotShowPupil = acaYear.IsShowPupil.HasValue && acaYear.IsShowPupil.Value;
            List<PupilOfClassBO> listPupil = new List<PupilOfClassBO>();
            if (isNotShowPupil)
            {
                listPupil = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                                .OrderBy(u => u.OrderInClass)
                                                                .ThenBy(u => u.Name)
                                                                .ThenBy(u => u.PupilFullName)
                                                                .ToList();
            }
            else
            {
                listPupil = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .OrderBy(u => u.OrderInClass)
                                                                .ThenBy(u => u.Name)
                                                                .ThenBy(u => u.PupilFullName)
                                                                .ToList();
            }


            //Lay danh sach diem trung binh mon
            // List<SummedUpRecordBO> listSummedUp = SummedUpRecordBusiness.GetSummedUpRecord(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, Semester, ClassID, null).ToList();
            List<SummedUpRecordBO> listSummedUp = new List<SummedUpRecordBO>();
            int Year = acaYear.Year;


            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

            if (!isMovedHistory)
            {
                listSummedUp = SummedUpRecordBusiness.GetSummedUpRecordCurrentYear(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, Semester, ClassID, null, false).ToList();
            }
            else
            {
                listSummedUp = SummedUpRecordHistoryBusiness.GetSummedUpRecordHistory(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, Semester, ClassID, null, false).ToList();
            }

            //neu hoc ky la Ca nam thi lay them danh sach diem trung binh mon cua hoc ky 1
            List<SummedUpRecordBO> listSummedUpHKI = new List<SummedUpRecordBO>();
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                if (!isMovedHistory)
                {
                    listSummedUpHKI = SummedUpRecordBusiness.GetSummedUpRecordCurrentYear(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, ClassID, null, false).ToList();
                }
                else
                {
                    listSummedUpHKI = SummedUpRecordHistoryBusiness.GetSummedUpRecordHistory(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, ClassID, null, false).ToList();
                }
            }


            List<PupilOfClassRanking> listRanking = listRanking = PupilRankingBusiness.GetPupilRankingOfClass(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, Semester, ClassID, null).ToList();
            //Lay danh sach xep loai va xep hang SEMESTER_OF_YEAR_BACKTRAINING

            // Danh sach ren luyen lai
            //List<PupilOfClassRanking> listBackTraining = new List<PupilOfClassRanking>();
            /*if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                //int semesterBackTraining = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                
                //listBackTraining = PupilRankingBusiness.GetPupilRankingOfClass(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value, semesterBackTraining, ClassID, null).ToList();
            }*/

            // Lay danh sach hoc sinh mien giảm
            // ky 1
            List<ExemptedSubject> lstExemptedI = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            // ky 2
            List<ExemptedSubject> lstExemptedII = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();

            //Lay danh sach hoc sinh dang ky môn tự chọn
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"YearID",acaYear.Year},
                {"SubjectTypeID",PupilRankingConstants.SUBJECT_TYPE_SPECIALIZE}
            };

            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dic);

            Dictionary<int, bool> dicLearingII = new Dictionary<int, bool>();
            Dictionary<int, bool> dicLearingI = new Dictionary<int, bool>();
            listSubject.ForEach(u =>
            {
                dicLearingII.Add(u.SubjectID, u.SectionPerWeekSecondSemester.HasValue && u.SectionPerWeekSecondSemester.Value > 0);
                dicLearingI.Add(u.SubjectID, u.SectionPerWeekFirstSemester.HasValue && u.SectionPerWeekFirstSemester.Value > 0);
            });

            List<PupilRankingViewModel> lstPupilView = new List<PupilRankingViewModel>();
            List<RegisterSubjectSpecializeBO> lstRegister = null;
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            int countSubjectRegister = 0;
            PupilOfClassBO pupil = null;
            PupilOfClassRanking pocRanking = null;
            //Chiendd1: Bo luong hien thi diem sau thi lai. HK Ca nam thi chi lay dien ca nam
            //PupilOfClassRanking pocBackTraining = null;
            PupilRankingViewModel pupilView = null;
            for (int i = 0; i < listPupil.Count; i++)
            {
                pupil = listPupil[i];
                //}
                //foreach (var pupil in listPupil)
                //{
                pupilView = new PupilRankingViewModel();
                pocRanking = listRanking.FirstOrDefault(u => u.PupilID == pupil.PupilID);
                //pocBackTraining = listBackTraining.FirstOrDefault(u => u.PupilID == pupil.PupilID);

                pupilView.FullName = pupil.PupilFullName;
                pupilView.Name = pupil.Name;
                pupilView.PupilCode = pupil.PupilCode;
                pupilView.ClassID = pupil.ClassID;
                pupilView.EducationLevelID = classProfile.EducationLevelID;
                pupilView.AcademicYearID = pupil.AcademicYearID;
                pupilView.AverageMark = pocRanking.AverageMark;
                pupilView.CapacityLevel = pocRanking.CapacityLevel;
                pupilView.CapacityLevelID = pocRanking.CapacityLevelID;
                pupilView.ConductLevelID = pocRanking.ConductLevelID;
                pupilView.ConductLevel = pocRanking.ConductLevel;
                /*if (pocBackTraining != null)
                {
                    if (pocBackTraining.AverageMark.HasValue)
                    {
                        pupilView.AverageMark = pocBackTraining.AverageMark;
                    }
                    if (pocBackTraining.ConductLevelID.HasValue)
                    {
                        pupilView.ConductLevelID = pocBackTraining.ConductLevelID;
                        pupilView.ConductLevel = pocBackTraining.ConductLevel;
                    }
                    if (pocBackTraining.CapacityLevelID.HasValue)
                    {
                        pupilView.CapacityLevel = pocBackTraining.CapacityLevel;
                        pupilView.CapacityLevelID = pocBackTraining.CapacityLevelID;
                    }
                }*/
                pupilView.PupilID = pupil.PupilID;
                pupilView.PupilRankingID = pocRanking.PupilRankingID;
                pupilView.Rank = pocRanking.Rank;
                pupilView.Status = pupil.Status;
                pupilView.OrderInClass = pupil.OrderInClass;

                pupilView.IsShowData = pupilView.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || pupilView.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED;

                // Neu khac 2 trang thai tren
                if (pupilView.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || pupilView.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                    || pupilView.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                    )
                {
                    pupilView.EndDate = pupil.EndDate;
                    pupilView.IsShowData = pupil.EndDate.HasValue && pupil.EndDate > semesterEndDate;
                }

                pupilView.ListSubject = new List<SubjectViewModel>();
                lstRegister = lstRegisterSubjectBO.Where(p => p.PupilID == pupil.PupilID).ToList();

                List<SummedUpRecordBO> pupilSummed = listSummedUp.Where(u => u.PupilID == pupil.PupilID).ToList();
                foreach (ClassSubjectBO cs in listSubject)
                {
                    SubjectViewModel subjectView = new SubjectViewModel();
                    subjectView.IsCommenting = cs.IsCommenting.Value;
                    subjectView.SubjectID = cs.SubjectID;
                    subjectView.SubjectName = cs.DisplayName;
                    subjectView.AppliedType = cs.AppliedType;
                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        #region Neu nguoi dung chon HK 1 hoac HK 2
                        // ExemptedSubjectBusiness.IsExemptedSubject(pupil.PupilID, pupil.ClassID, globalInfo.AcademicYearID.Value, cs.SubjectID, Semester);
                        subjectView.IsExempted = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                    ? lstExemptedI.Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID)
                                                    : lstExemptedII.Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID);
                        //summed chi co mot ban ghi doi voi 1 hoc ky (1 hoac 2)
                        SummedUpRecordBO summed = pupilSummed.Where(u => u.SubjectID == cs.SubjectID).FirstOrDefault();
                        if (subjectView.IsExempted)
                        {
                            subjectView.DisplayMark = "MG";
                        }
                        else
                        {
                            if (cs.Is_Specialize_Subject.HasValue && cs.Is_Specialize_Subject.Value)
                            {
                                countSubjectRegister = lstRegister.Where(p => p.SubjectID == cs.SubjectID).Count();
                            }
                            else
                            {
                                countSubjectRegister = lstRegister.Where(p => p.SubjectID == cs.SubjectID && p.SemesterID == Semester &&
                                                                                    (p.SubjectTypeID == cs.AppliedType)).Count();
                            }

                            if (!(cs.Is_Specialize_Subject.HasValue && cs.Is_Specialize_Subject.Value) && (cs.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || cs.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                                || cs.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE) && countSubjectRegister == 0)
                            {
                                subjectView.DisplayMark = "KH";
                            }
                            else if (summed != null)
                            {
                                if (subjectView.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                {
                                    subjectView.DisplayMark = subjectView.JudgementResult = (!string.IsNullOrEmpty(summed.JudgementResult) ? summed.JudgementResult : "CT");
                                }
                                else
                                {
                                    subjectView.SummedUpMark = summed.SummedUpMark;
                                    subjectView.DisplayMark = subjectView.SummedUpMark.HasValue ? subjectView.SummedUpMark.Value.ToString("0.0") : "CT";
                                }
                            }
                            else
                            {
                                subjectView.DisplayMark = "CT";
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region neu nguoi dung chon Ca Nam
                        bool exemptedI = lstExemptedI.Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID);
                        bool exemptedII = lstExemptedII.Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID);
                        // Lay diem thi lai
                        var retestResult = pupilSummed.FirstOrDefault(u => u.SubjectID == cs.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST);
                        //neu Mon hoc duoc hoc o HKII thi tbm lay o ca nam, neu khong duoc hoc o HKII thi tbm  lay o HKI
                        SummedUpRecordBO summed = !dicLearingII[cs.SubjectID] || exemptedII
                                                    ? listSummedUpHKI.Where(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID).FirstOrDefault()
                                                    : (retestResult != null ? retestResult : pupilSummed.Where(u => u.SubjectID == cs.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault());
                        //if (subjectView.IsExempted) //Neu duoc mien giam ca nam
                        subjectView.IsExempted = (exemptedI || !dicLearingI[cs.SubjectID]) && (exemptedII || !dicLearingII[cs.SubjectID]);
                        if (subjectView.IsExempted)
                        {
                            subjectView.DisplayMark = "MG";

                        }
                        else if (exemptedI || exemptedII)
                        {
                            if (summed != null)
                            {
                                if (subjectView.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                {
                                    subjectView.DisplayMark = subjectView.JudgementResult = summed.JudgementResult;
                                }
                                else
                                {
                                    subjectView.SummedUpMark = summed.SummedUpMark;
                                    subjectView.DisplayMark = subjectView.SummedUpMark.HasValue ? subjectView.SummedUpMark.Value.ToString("0.0") : "";
                                }
                            }
                        }
                        else //Neu khong duoc mien giam
                        {
                            SummedUpRecordBO summedRetest = pupilSummed.Where(u => u.SubjectID == cs.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST).FirstOrDefault();
                            countSubjectRegister = lstRegister.Where(p => p.SubjectID == cs.SubjectID && p.SubjectTypeID == cs.AppliedType).Count();
                            if ((!(cs.Is_Specialize_Subject.HasValue && cs.Is_Specialize_Subject.Value) && cs.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || cs.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                                || cs.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE) && countSubjectRegister == 0)
                            {
                                subjectView.DisplayMark = "KH";
                            }
                            else if (summed != null || summedRetest != null) //neu ton tai diem tong ket hoc ky
                            {
                                if (summedRetest != null) //neu co diem thi lai
                                {
                                    if (subjectView.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        subjectView.DisplayMark = subjectView.ReTestJudgement = summedRetest.ReTestJudgement;
                                    }
                                    else
                                    {
                                        subjectView.ReTestMark = summedRetest.ReTestMark;
                                        subjectView.DisplayMark = subjectView.ReTestMark.HasValue ? subjectView.ReTestMark.Value.ToString("0.0") : "";
                                    }
                                }
                                else //Neu khong co diem thi lai
                                {
                                    if (subjectView.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        subjectView.DisplayMark = subjectView.JudgementResult = summed.JudgementResult;
                                    }
                                    else
                                    {
                                        subjectView.SummedUpMark = summed.SummedUpMark;
                                        subjectView.DisplayMark = subjectView.SummedUpMark.HasValue ? subjectView.SummedUpMark.Value.ToString("0.0") : "";
                                    }
                                }
                            }
                            else
                            {
                                subjectView.DisplayMark = "CT";
                            }
                        }
                        #endregion
                    }
                    pupilView.ListSubject.Add(subjectView);
                }

                lstPupilView.Add(pupilView);
            }

            return lstPupilView;
        }

        #region Export Excel
        public FileResult ExportExcel(int ClassID, int Semester)
        {
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            DateTime? endSemester = null;
            //Lay ngay ket thuc cua hoc ky
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                endSemester = acaYear.FirstSemesterEndDate;
            else
                endSemester = acaYear.SecondSemesterEndDate;

            if (classProfile == null || acaYear == null)
                throw new BusinessException(Res.Get("Common_Label_Error_Parameter"));

            if (!UtilsBusiness.HasHeadTeacherPermissionSupervising(_globalInfo.UserAccountID, ClassID) && !UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID))
                throw new BusinessException(Res.Get("PupilRankingSemester_Validate_NotPermission"));

            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                            ? (acaYear == null || (acaYear.FirstSemesterStartDate > DateTime.Now && DateTime.Now > acaYear.FirstSemesterEndDate))
                                            : (acaYear == null || (acaYear.SecondSemesterStartDate > DateTime.Now && DateTime.Now > acaYear.SecondSemesterEndDate)))
                throw new BusinessException(Res.Get("PupilRankingSemester_Validate_NotInSemester"));

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TONG_KET_DIEM_HOC_KY);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;

            IVTWorkbook oBook;

            try { oBook = VTExport.OpenWorkbook(templatePath); }
            catch
            {
                throw new BusinessException(Res.Get("Import_Validate_FileInUsed"));
            }

            //Lấy sheet
            IVTWorksheet sheet = oBook.GetSheet(1);

            //Load danh sach mon hoc
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = ClassID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL) dicSubject["Semester"] = Semester;

            IQueryable<ClassSubject> iqCS = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
            //Loc ra nhung mon tang cuong
            List<int?> lstIncreaseID = iqCS.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease > 0).Select(p => p.SubjectIDIncrease).ToList();
            iqCS = iqCS.Where(p => !lstIncreaseID.Contains(p.SubjectID));

            List<ClassSubjectBO> lstSubject = (from o in iqCS
                                               select new ClassSubjectBO
                                               {
                                                   ClassID = o.ClassID,
                                                   SubjectID = o.SubjectID,
                                                   OrderInSubject = o.SubjectCat.OrderInSubject,
                                                   DisplayName = o.SubjectCat.DisplayName,
                                                   SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                                                   SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                                                   IsCommenting = o.IsCommenting,
                                                   AppliedType = o.AppliedType,
                                                   Is_Specialize_Subject = o.IsSpecializedSubject
                                               }).ToList();
            if (lstSubject != null)
            {
                lstSubject = lstSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            List<PupilRankingViewModel> lstPupilView = SearchData(ClassID, (int)Semester, lstSubject);
            lstPupilView = lstPupilView.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.FullName).ToList();

            // Tạo header template
            IVTRange rangeHeader = sheet.GetRange("D9", "D9");
            IVTRange rangeHeaderButtom = sheet.GetRange("D10", "D15");
            IVTRange rangeHeaderLast = sheet.GetRange("E9", "E15");
            IVTRange rangeWidthSubject = sheet.GetRange("D10", "D10");
            IVTRange rangeHLHKTBCM = sheet.GetRange("Z9", "Z15");
            IVTRange rangeXH = sheet.GetRange("AA9", "AA15");
            int startcolhearder = 4;
            for (int i = 0; i < lstSubject.Count(); i++)
            {
                string titleHeaarder = "";
                if (i == 0)
                {
                    titleHeaarder = "ĐIỂM TRUNG BÌNH CÁC MÔN";
                }
                sheet.CopyPasteSameSize(rangeHeader, 9, startcolhearder);
                sheet.SetCellValue(9, startcolhearder, titleHeaarder);
                sheet.CopyPasteSameSize(rangeHeaderButtom, 10, startcolhearder);
                sheet.CopyPasteSameColumnWidth(rangeWidthSubject, 10, startcolhearder);
                sheet.SetCellValue(10, startcolhearder, lstSubject[i].DisplayName);

                startcolhearder++;

            }

            sheet.MergeRow(9, 4, startcolhearder - 1);
            sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            sheet.MergeColumn(startcolhearder, 9, 10);
            sheet.CopyPasteSameColumnWidth(rangeHLHKTBCM, 9, startcolhearder);
            sheet.SetCellValue(9, startcolhearder, "TBcm");


            startcolhearder++;
            sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            sheet.MergeColumn(startcolhearder, 9, 10);
            sheet.CopyPasteSameColumnWidth(rangeHLHKTBCM, 9, startcolhearder);
            sheet.SetCellValue(9, startcolhearder, "Học lực");

            startcolhearder++;
            sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            sheet.MergeColumn(startcolhearder, 9, 10);
            sheet.CopyPasteSameColumnWidth(rangeHLHKTBCM, 9, startcolhearder);
            sheet.SetCellValue(9, startcolhearder, "Hạnh kiểm");

            startcolhearder++;
            sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            sheet.MergeColumn(startcolhearder, 9, 10);
            sheet.CopyPasteSameColumnWidth(rangeXH, 9, startcolhearder);
            sheet.SetCellValue(9, startcolhearder, "Xếp hạng");

            IVTRange temRowRange = sheet.GetRange(12, 1, 12, startcolhearder);
            IVTRange grayRowRange = sheet.GetRange(13, 1, 13, startcolhearder);
            IVTRange redRowRange = sheet.GetRange(14, 1, 14, startcolhearder);

            int rowIndex = 16;
            foreach (var pupil in lstPupilView)
            {
                if (pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                    sheet.CopyPaste(redRowRange, rowIndex, 1, true);
                else
                    if (pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pupil.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    sheet.CopyPaste(grayRowRange, rowIndex, 1, true);
                else
                    sheet.CopyPaste(temRowRange, rowIndex, 1, true);

                sheet.SetCellValue(rowIndex, 1, rowIndex - 15);
                sheet.SetCellValue(rowIndex, 2, pupil.FullName);
                sheet.SetCellValue(rowIndex, 3, pupil.PupilCode);

                int colIndex = 4;

                bool showData = pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                || pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || (pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && pupil.EndDate.HasValue && pupil.EndDate.Value > endSemester)
                                || (pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && pupil.EndDate.HasValue && pupil.EndDate.Value > endSemester)
                                || (pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && pupil.EndDate.HasValue && pupil.EndDate.Value > endSemester);

                if (showData)
                {
                    foreach (ClassSubjectBO subject in lstSubject)
                    {
                        SubjectViewModel subjectView = pupil.ListSubject.Where(u => u.SubjectID == subject.SubjectID).SingleOrDefault();
                        if (subjectView.DisplayMark == null)
                        {
                            sheet.SetCellValue(rowIndex, colIndex++, "");
                        }
                        else
                        {
                            if (subjectView.DisplayMark == "10,0")
                            {
                                sheet.SetCellValue(rowIndex, colIndex++, "10");
                            }
                            else
                            {
                                sheet.SetCellValue(rowIndex, colIndex++, subjectView.DisplayMark.Replace(",", "."));
                            }
                        }
                    }
                    if (pupil.AverageMark == null)
                    {
                        sheet.SetCellValue(rowIndex, startcolhearder - 3, "");
                    }
                    else
                    {
                        if (pupil.AverageMark != 10.0m)
                        {
                            sheet.SetCellValue(rowIndex, startcolhearder - 3, pupil.AverageMark.Value.ToString("0.0#").Replace(",", "."));
                        }
                        else
                        {
                            sheet.SetCellValue(rowIndex, startcolhearder - 3, pupil.AverageMark.Value.ToString("0"));
                        }
                    }
                    sheet.SetCellValue(rowIndex, startcolhearder - 2, pupil.CapacityLevel);
                    sheet.SetCellValue(rowIndex, startcolhearder - 1, pupil.ConductLevel);
                    sheet.SetCellValue(rowIndex, startcolhearder, pupil.Rank);
                }
                if (rowIndex > 16 && rowIndex % 5 == 1)
                {
                    sheet.GetRange(rowIndex - 1, 1, rowIndex - 1, startcolhearder).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                }
                rowIndex++;
            }

            string semesterName = ReportUtils.ConvertSemesterForReportName(Semester);

            Dictionary<string, object> dicVariable = new Dictionary<string, object>();
            dicVariable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVariable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value);
            dicVariable["SchoolName"] = acaYear.SchoolProfile.SchoolName;
            dicVariable["Semester"] = semesterName;
            dicVariable["ProvinceDate"] = (acaYear.SchoolProfile.District != null ? acaYear.SchoolProfile.District.DistrictName : "") + ", " + string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            dicVariable["ClassName"] = classProfile.DisplayName.ToUpper().Replace("LỚP", "").Replace(" ", "");

            //xoa cac row template
            for (int i = 0; i < 5; i++) sheet.DeleteRow(11);

            sheet.FillVariableValue(dicVariable);
            sheet.DeleteRow(192);
            sheet.DeleteRow(193);
            sheet.DeleteRow(194);
            sheet.DeleteRow(195);
            sheet.HideColumn(26);
            sheet.HideColumn(27);
            // Vẽ đường viền
            sheet.GetRange(10, 1, 10 + lstPupilView.Count, startcolhearder).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
            sheet.GetRange(10, 1, 10 + lstPupilView.Count, startcolhearder).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
            // Tao chu ky
            sheet.SetCellValue(12 + lstPupilView.Count, startcolhearder - 4, "NGƯỜI LẬP BÁO CÁO");
            sheet.GetRange(12 + lstPupilView.Count, startcolhearder - 4, 12 + lstPupilView.Count, startcolhearder - 2).SetFontStyle(true, null, false, null, false, false);
            sheet.FitAllColumnsOnOnePage = true;
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semesterName));
            outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(classProfile.DisplayName));

            string ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            result.FileDownloadName = ReportName;
            return result;
        }

        #endregion Export Excel

        private int GetRankingType()
        {
            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", glo.SchoolID.Value);
            dic.Add("AppliedLevel", glo.AppliedLevel.Value);
            dic.Add("IsActive", true);

            bool capacity = this.ConductConfigByCapacityBu.Search(dic).Any();
            if (capacity) return 0;

            var violation = this.ConductConfigByViolationBu.Search(dic).Any();
            if (violation) return 1;

            return -1;
        }
    }
}
