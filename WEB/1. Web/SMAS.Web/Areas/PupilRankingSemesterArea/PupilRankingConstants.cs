﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilRankingSemesterArea
{
    public class PupilRankingConstants
    {
        public const string LIST_PUPILRANKING = "listPupilRanking";
        public const string LS_EDUCATIONLEVEL = "listEducation";
        public const string LS_CLASS = "listClass";
        public const string LS_SEMESTER = "listSemester";
        public const string LS_ClassSubject = "listSubject";
        public const string LS_SUMMEDUPRECORD = "listSummedUp";
        public const string LIST_PUPILONGRID = "listPUPILONGRID";
        public const string ENABLE_SUMMEDUP = "ENABLE_SUMMEDUP";
        public const string PERMISSION = "Permission";
        public const string SELECTED_CLASS = "selected_class";
        public const string SELECTED_SEMESTER = "selected_semester";
        public const string SEMESTER_END_DATE = "semester_end_date";
        public const string TITLE_PAGE = "TITLE_PAGE";
        public const string ENABLE_EXCEL = "ENABLE_EXCEL";
        public const string ENABLE_RANK = "ENABLE_RANK";
        public const int SUBJECT_TYPE_SPECIALIZE = 0;//môn chuyên
        public const int SUBJECT_TYPE_NOT_SPECIALIZE = 1;//môn tự chọn
    }
}