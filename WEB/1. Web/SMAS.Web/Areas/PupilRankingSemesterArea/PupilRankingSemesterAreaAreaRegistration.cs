﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilRankingSemesterArea
{
    public class PupilRankingSemesterAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilRankingSemesterArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilRankingSemesterArea_default",
                "PupilRankingSemesterArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
