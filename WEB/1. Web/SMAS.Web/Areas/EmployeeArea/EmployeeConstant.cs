﻿namespace SMAS.Web.Areas.EmployeeArea
{
    public class EmployeeConstant
    {
        public const string LS_EMPLOYEE = "ListEMPLOYEE";
        public const string LS_SEX = "ListSex";
        public const string LS_RELIGION = "ListReligion";
        public const string LS_ETHNIC = "ListEthnic";
        public const string LS_FamilyTypeBusiness = "ListFamilyTypeBusiness";
        public const string LS_QualificationTypeBusiness = "ListQualificationTypeBusiness";

        public const string LS_SpecialityCat = "ListSpecialityCat";
        public const string LS_QualificationLevelBusiness = "ListQualificationLevelBusiness";
        public const string LS_ForeignLanguageGradeBusiness = "ListForeignLanguageGradeBusiness";

        public const string LS_ITQualificationLevelBusiness = "ListITQualificationLevelBusiness";
        public const string LS_StateManagementGradeBusiness = "ListStateManagementGradeBusiness";

        public const string LS_PoliticalGradeBusiness = "ListPoliticalGradeBusiness";

        public const string LS_EducationalManagementGradeBusiness = "ListEducationalManagementGradeBusiness";

        public const string LS_ContractBusiness = "ListContractBusiness";

        public const string LS_StaffPositionBusiness = "ListStaffPositionBusiness";

        public const string LS_SupervisingDeptBusiness = "ListSupervisingDeptBusiness";

        public const string LS_GroupCatBusiness = "ListGroupCatBusiness";
        public const string LS_EMPLOYEE2 = "LS_EMPLOYEE2";
        public const string LS_GraduationLevelBusiness = "LS_GraduationLevelBusiness";
        public const string LS_TrainingLevelBusiness = "LS_TrainingLevelBusiness";

        public const string FORBACK = "FORBACK";


    }
}