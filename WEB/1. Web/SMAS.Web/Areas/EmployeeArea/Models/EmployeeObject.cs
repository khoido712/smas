using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Models.CustomAttribute;
namespace SMAS.Web.Areas.EmployeeArea.Models
{
    public class EmployeeObject
    {
        #region thuoc tinh lay trong metadata

        [ResourceDisplayName("Employee")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EmployeeID { get; set; }

        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string EmployeeCode { get; set; }

        [ResourceDisplayName("Employee Type")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EmployeeType { get; set; }

        [ResourceDisplayName("Employment Status")]
        public Nullable<int> EmploymentStatus { get; set; }

        [ResourceDisplayName("Employee_Label_SupervisingDept")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> SupervisingDeptID { get; set; }

        [ResourceDisplayName("School")]
        public Nullable<int> SchoolID { get; set; }

        [ResourceDisplayName("Name")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Name { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]        
        public string FullName { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage="Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public System.Nullable<System.DateTime> BirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_BirthPlace")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string BirthPlace { get; set; }

        [ResourceDisplayName("Employee_Label_Telephone")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Telephone { get; set; }

        [ResourceDisplayName("Employee_Label_Mobile")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Mobile { get; set; }

        [ResourceDisplayName("Employee_Label_Email")]
        [StringLength(60, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Email { get; set; }

        [ResourceDisplayName("Employee_Label_TempResidentalAddress")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string TempResidentalAddress { get; set; }

        [ResourceDisplayName("Employee_Label_PermanentResidentalAddress")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PermanentResidentalAddress { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool Genre { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        public string GenreName { get; set; }

        [ResourceDisplayName("Employee_Label_Alias")]
        [StringLength(100 ,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Alias { get; set; }

        [ResourceDisplayName("Employee_Label_MariageStatus")]
        public Nullable<int> MariageStatus { get; set; }

        [ResourceDisplayName("Employee_Label_ForeignLanguageQualification")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ForeignLanguageQualification { get; set; }

        [ResourceDisplayName("Health Status")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string HealthStatus { get; set; }

        [ResourceDisplayName("Employee_Label_JoinedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage="Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public Nullable<System.DateTime> JoinedDate { get; set; }

        [ResourceDisplayName("Employee_Label_StartingDate")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage="Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DataConstraint(DataConstraintAttribute.LESS, "JoinedDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        //[DataConstraint(DataConstraintAttribute.LESS, "BirthDate.Value.AddYears(17)", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public Nullable<System.DateTime> StartingDate { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityNumber")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string IdentityNumber { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityIssuedDate")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage="Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public Nullable<System.DateTime> IdentityIssuedDate { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityIssuedPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string IdentityIssuedPlace { get; set; }

        [ResourceDisplayName("Employee_Label_HomeTown")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string HomeTown { get; set; }

        [ResourceDisplayName("Image")]
        public byte[] Image { get; set; }

        [ResourceDisplayName("Dedicated For Young League")]
        public Nullable<bool> DedicatedForYoungLeague { get; set; }

        [ResourceDisplayName("Employee_Label_FamilyType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> FamilyTypeID { get; set; }

        [ResourceDisplayName("Employee_Label_FamilyType")]
        public string FamilyTypeName { get; set; }

        [ResourceDisplayName("Employee_Label_StaffPosition")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> StaffPositionID { get; set; }

        [ResourceDisplayName("Employee_Label_Ethnic")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> EthnicID { get; set; }

        [ResourceDisplayName("Employee_Label_Ethnic")]
        public string EthnicName { get; set; }

        [ResourceDisplayName("Employee_Label_Religion")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> ReligionID { get; set; }


        [ResourceDisplayName("Employee_Label_Religion")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string ReligionName { get; set; }

        [ResourceDisplayName("Employee_Label_GraduationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> TrainingLevelID { get; set; }

        [ResourceDisplayName("Employee_Label_GraduationLevel")]
        public string TrainingLevelName { get; set; }

        [ResourceDisplayName("Employee_Label_ContractType")]
        public Nullable<int> ContractTypeID { get; set; }

        [ResourceDisplayName("Employee_Label_ContractType")]
        public string ContractTypeName { get; set; }

        [ResourceDisplayName("Contract")]
        public Nullable<int> ContractID { get; set; }

        [ResourceDisplayName("Employee_Label_QualificationType")]
        public Nullable<int> QualificationTypeID { get; set; }

        [ResourceDisplayName("Employee_Label_QualificationType")]
        public string QualificationTypeName { get; set; }

        [ResourceDisplayName("Employee_Label_SpecialityCat")]
        public Nullable<int> SpecialityCatID { get; set; }

        [ResourceDisplayName("Employee_Label_SpecialityCat")]
        public string SpecialityCatName { get; set; }

        [ResourceDisplayName("Employee_Label_QualificationLevel")]
        public Nullable<int> QualificationLevelID { get; set; }


        [ResourceDisplayName("Employee_Label_QualificationLevel")]
        public string QualificationLevelName { get; set; }

        [ResourceDisplayName("Employee_Label_ITQualificationLevel")]
        public Nullable<int> ITQualificationLevelID { get; set; }

        [ResourceDisplayName("Employee_Label_ITQualificationLevel")]
        public string ITQualificationLevelName { get; set; }

        [ResourceDisplayName("Employee_Label_ForeignLanguageGrade")]
        public Nullable<int> ForeignLanguageGradeID { get; set; }

        [ResourceDisplayName("Employee_Label_ForeignLanguageGrade")]
        public string ForeignLanguageGradeName { get; set; }

        [ResourceDisplayName("Employee_Label_PoliticalGrade")]
        public Nullable<int> PoliticalGradeID { get; set; }


        [ResourceDisplayName("Employee_Label_PoliticalGrade")]
        public string PoliticalGradeName { get; set; }


        [ResourceDisplayName("Employee_Label_StateManagementGrade")]
        public Nullable<int> StateManagementGradeID { get; set; }

        [ResourceDisplayName("Employee_Label_StateManagementGrade")]
        public string StateManagementGradeName { get; set; }


        [ResourceDisplayName("Employee_Label_EducationalManagementGrade")]
        public Nullable<int> EducationalManagementGradeID { get; set; }

        [ResourceDisplayName("Employee_Label_EducationalManagementGrade")]
        public string EducationalManagementGradeName { get; set; }

        [ResourceDisplayName("Employee_Label_WorkTypeID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> WorkTypeID { get; set; }

        [ResourceDisplayName("Primarily Assigned Subject")]
        public Nullable<int> PrimarilyAssignedSubjectID { get; set; }

        [ResourceDisplayName("Employee_Label_SchoolFaculty")]        
        public Nullable<int> SchoolFacultyID { get; set; }

        [ResourceDisplayName("Is Youth Leage Member")]
        public bool IsYouthLeageMember { get; set; }

        [ResourceDisplayName("Employee_Label_YouthLeagueJoinedDate")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$",  ErrorMessage="Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public Nullable<System.DateTime> YouthLeagueJoinedDate { get; set; }


        [ResourceDisplayName("Youth League Joined Place")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string YouthLeagueJoinedPlace { get; set; }

        [ResourceDisplayName("Is Communist Party Member")]
        public bool IsCommunistPartyMember { get; set; }

        [ResourceDisplayName("Employee_Label_CommunistPartyJoinedDate")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage="Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DataConstraint(DataConstraintAttribute.GREATER, "YouthLeagueJoinedDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public Nullable<System.DateTime> CommunistPartyJoinedDate { get; set; }


        [ResourceDisplayName("Employee_Label_CommunistPartyJoinedPlace")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string CommunistPartyJoinedPlace { get; set; }

        [ResourceDisplayName("Employee_Label_FatherFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FatherFullName { get; set; }

        [ResourceDisplayName("Employee_Label_FatherBirthDate")]
        public Nullable<System.DateTime> FatherBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_FatherBirthDate")]
        public int? iFatherBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_FatherJob")]
        [StringLength(60, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FatherJob { get; set; }

        [ResourceDisplayName("Employee_Label_FatherWorkingPlace")]
        [StringLength(60, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FatherWorkingPlace { get; set; }

        [ResourceDisplayName("Employee_Label_MotherFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string MotherFullName { get; set; }

        [ResourceDisplayName("Mother Birth Date")]
        public Nullable<System.DateTime> MotherBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_MotherBirthDate")]
        public int? iMotherBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_MotherJob")]
        [StringLength(60, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string MotherJob { get; set; }

        [ResourceDisplayName("Employee_Label_MotherWorkingPlace")]
        [StringLength(60, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string MotherWorkingPlace { get; set; }

        [ResourceDisplayName("Employee_Label_SpouseFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SpouseFullName { get; set; }

        [ResourceDisplayName("Spouse Birth Date")]
        public Nullable<System.DateTime> SpouseBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_SpouseBirthDate")]
        public int? iSpouseBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_SpouseJob")]
        [StringLength(60, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SpouseJob { get; set; }

        [ResourceDisplayName("Employee_Label_SpouseWorkingPlace")]
        [StringLength(60, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SpouseWorkingPlace { get; set; }

        [ResourceDisplayName("Employee_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Description { get; set; }

        //[ResourceDisplayName("Created Date")]
       // public Nullable<System.DateTime> CreatedDate { get; set; }

        [ResourceDisplayName("Is Active")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsActive { get; set; }

        [ResourceDisplayName("Modified Date")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        //[ResDisplayName("Employee_Label_IntoSchoolDate")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        //[UIHint("DateTimePicker")]
        //[RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage="Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        //public System.Nullable<System.DateTime> IntoSchoolDate { get; set; }

        #endregion thuoc tinh lay trong metadata

        #region thuoc tinh ben ngoai

        [ResourceDisplayName("Employee_Label_SupervisingDept")]
        public string SupervisingDeptName { get; set; }

        [ResourceDisplayName("User Accounts")]
        public string UserAccounts { get; set; }

        [ResourceDisplayName("Employee_Label_StaffPosition")]
        public string StaffPosition { get; set; }

        [ResourceDisplayName("User Name")]
        public string UserName { get; set; }

        public string HeadClass { get; set; }

        #endregion thuoc tinh ben ngoai
    }
}
