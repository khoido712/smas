﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeArea.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Filter;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.EmployeeArea.Controllers
{
    public class EmployeeController : BaseController
    {
        #region member

        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;

        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IQualificationLevelBusiness QualificationLevelBusiness;

        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;

        private readonly IStateManagementGradeBusiness StateManagementGradeBusiness;
        private readonly IPoliticalGradeBusiness PoliticalGradeBusiness;

        private readonly IEducationalManagementGradeBusiness EducationalManagementGradeBusiness;

        private readonly IContractBusiness ContractBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;

        private readonly IStaffPositionBusiness StaffPositionBusiness;
        //private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private const string TEMPLATE_EMPLOYEE = "DanhsachNhanVien.xls";
        #endregion member

        public EmployeeController(IEmployeeBusiness employeeBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness,
            IGroupCatBusiness GroupBusiness, IEthnicBusiness EthnicBusiness, IReligionBusiness ReligionBusiness
            , IFamilyTypeBusiness FamilyTypeBusiness, IQualificationTypeBusiness QualificationTypeBusiness,
            ISpecialityCatBusiness SpecialityCatBusiness, IQualificationLevelBusiness QualificationLevelBusiness,
            IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness, IITQualificationLevelBusiness ITQualificationLevelBusiness,
            IStateManagementGradeBusiness StateManagementGradeBusiness,
            IPoliticalGradeBusiness PoliticalGradeBusiness,
            IEducationalManagementGradeBusiness EducationalManagementGradeBusiness,
            IContractBusiness ContractBusiness,
            IStaffPositionBusiness StaffPositionBusiness,
            ITrainingLevelBusiness TrainingLevelBusiness
            , IContractTypeBusiness ContractTypeBusiness,
            IUserAccountBusiness UserAccountBusiness
            )
        {
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.GroupCatBusiness = GroupBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.ReligionBusiness = ReligionBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.QualificationLevelBusiness = QualificationLevelBusiness;
            this.ForeignLanguageGradeBusiness = ForeignLanguageGradeBusiness;
            this.ITQualificationLevelBusiness = ITQualificationLevelBusiness;
            this.StateManagementGradeBusiness = StateManagementGradeBusiness;
            this.PoliticalGradeBusiness = PoliticalGradeBusiness;

            this.EducationalManagementGradeBusiness = EducationalManagementGradeBusiness;
            this.ContractBusiness = ContractBusiness;
            this.StaffPositionBusiness = StaffPositionBusiness;
            this.TrainingLevelBusiness = TrainingLevelBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
        }

        #region index

        // GET: /Employee/

        public ViewResult Index(int? LoadSS)
        {
            SetViewDataPermission("Employee", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SetViewData();
            if (LoadSS == 1)
            {
                Dictionary<string, object> dicMoi = (Dictionary<string, object>)Session["forBack"];
                ViewData[EmployeeConstant.FORBACK] = dicMoi;
                string employeeCode = dicMoi != null ? SMAS.Business.Common.Utils.GetString(dicMoi["EmployeeCode"]) : "";//dicMoi["EmployeeCode"] = EmployeeCode;
                string fullName = dicMoi != null ? SMAS.Business.Common.Utils.GetString(dicMoi["FullName"]) : "";//dicMoi["FullName"] = FullName;
                int staffPosition = dicMoi != null ? SMAS.Business.Common.Utils.GetInt(dicMoi["StaffPosition"]) : 0;//dicMoi["StaffPosition"] = StaffPosition;
                int supervisingDept = dicMoi != null ? SMAS.Business.Common.Utils.GetInt(dicMoi["SupervisingDept"]) : 0;//dicMoi["SupervisingDept"] = SupervisingDept;
                int trainingLevel = dicMoi != null ? SMAS.Business.Common.Utils.GetInt(dicMoi["TrainingLevel"]) : 0;//dicMoi["TrainingLevel"] = TrainingLevel;
                int page = dicMoi != null ? SMAS.Business.Common.Utils.GetInt(dicMoi["page"]) : 0;//dicMoi["page"] = page;
                string orderBy = dicMoi != null ? SMAS.Business.Common.Utils.GetString(dicMoi["orderBy"]) : "";//dicMoi["orderBy"] = orderBy;
                Paginate<EmployeeObject> paging = this._Search(employeeCode, fullName, staffPosition, supervisingDept, trainingLevel, page, orderBy);
                ViewData[EmployeeConstant.LS_EMPLOYEE] = paging;
                //Session.Remove("forBack");
            }
            GlobalInfo GlobalInfo = new GlobalInfo();
            ViewData[SystemParamsInFile.PERMISSION] = GetMenupermission("Employee", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin);
            return View("Index");
        }

        #endregion index

        #region Create

        public ActionResult CreateOrEdit()
        {
            SetViewData();
            return PartialView("Create");
        }

        //
        // POST: /Employee/Create

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult CreateEmployee(EmployeeObject EmployeeObject)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Employee", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //CheckPermissionForAction(EmployeeObject.EmployeeID, "Employee");

            GlobalInfo global = new GlobalInfo();

            //if (ModelState.IsValid)
            {
                Employee Employee = new Employee();
                Utils.Utils.BindTo(EmployeeObject, Employee);

                //VALIDATE nam sinh,doi du lieu tu nam sinh object sang nam sinh cua entity
                if (EmployeeObject.iFatherBirthDate.HasValue)
                {
                    if (EmployeeObject.iFatherBirthDate < 1900 || EmployeeObject.iFatherBirthDate > DateTime.Now.Year)
                    {
                        throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Father_Error_Edit");
                    }
                    Employee.FatherBirthDate = new DateTime(EmployeeObject.iFatherBirthDate.Value, 1, 1);
                }
                if (EmployeeObject.iMotherBirthDate.HasValue)
                {
                    if (EmployeeObject.iMotherBirthDate < 1900 || EmployeeObject.iMotherBirthDate > DateTime.Now.Year)
                    {
                        throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Mother_Error_Edit");
                    }
                    Employee.MotherBirthDate = new DateTime(EmployeeObject.iMotherBirthDate.Value, 1, 1);
                }
                if (EmployeeObject.iSpouseBirthDate.HasValue)
                {
                    if (EmployeeObject.iSpouseBirthDate < 1900 || EmployeeObject.iSpouseBirthDate > DateTime.Now.Year)
                    {
                        throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_SPouse_Error_Edit");
                    }
                    Employee.SpouseBirthDate = new DateTime(EmployeeObject.iSpouseBirthDate.Value, 1, 1);
                }
                if (EmployeeObject.StartingDate.HasValue)
                {
                    if (EmployeeObject.StartingDate <= EmployeeObject.BirthDate.Value.AddYears(17))
                    {
                        throw new SMAS.Business.Common.BusinessException("Employee_Label_CheckStartingDate");
                    }
                }
                if (EmployeeObject.StartingDate.HasValue && EmployeeObject.JoinedDate.HasValue)
                {
                    if (EmployeeObject.StartingDate.Value > EmployeeObject.JoinedDate.Value)
                    {
                        throw new SMAS.Business.Common.BusinessException("Employee_Label_CheckStartingDate_JoinedDate");
                    }
                }
                //
                using (TransactionScope scope = new TransactionScope())
                {
                    int space = Employee.FullName.LastIndexOf(" ");
                    if (space == -1)
                    {
                        Employee.Name = Employee.FullName;
                    }
                    else
                    {
                        Employee.Name = Employee.FullName.Substring(space + 1);
                    }

                    //dua image vao employee
                    object imagePath = Session["ImagePath"];
                    if (imagePath != null)
                    {
                        byte[] imageBytes = FileToByteArray((string)imagePath);
                        Employee.Image = imageBytes;
                    }


                    Employee.EmployeeType = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT;

                    //them tham so he thong
                    //Employee.SupervisingDeptID = global.SupervisingDeptID;
                    Employee.SupervisingDeptID = EmployeeObject.SupervisingDeptID;
                    Employee.IntoSchoolDate = DateTime.Now;
                    Employee.CreatedDate = DateTime.Now;
                    this.EmployeeBusiness.InsertEmployee(Employee);
                    this.EmployeeBusiness.Save();

                    scope.Complete();
                }
                ViewData[SMAS.Web.Constants.GlobalConstants.JSON_MSG_LIST] = "['" + Res.Get("Common_Label_AddNewMessage") + "']";

                SetViewData();
                ModelState.Clear();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }

            // Lay thong bao loi va tra ve kieu JSON. View se hien thi JSON nay
            //string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            //ViewData[GlobalConstants.JSON_MSG_LIST] = jsonErrList;
            //SetViewData();
            //ModelState.Clear();
            // return Json(new JsonMessage(jsonErrList));
        }

        #endregion Create

        #region Edit


        public ActionResult GetEditEmployee(int EmployeeId)
        {
            CheckPermissionForAction(EmployeeId, "Employee");
            SetViewData();
            Employee ep = EmployeeBusiness.Find(EmployeeId);
            EmployeeObject epObj = new EmployeeObject();
            Utils.Utils.BindTo(ep, epObj, true);
            if (ep.FatherBirthDate.HasValue)
            {
                epObj.iFatherBirthDate = ep.FatherBirthDate.Value.Year;
            }
            if (ep.MotherBirthDate.HasValue)
            {
                epObj.iMotherBirthDate = ep.MotherBirthDate.Value.Year;
            }
            if (ep.SpouseBirthDate.HasValue)
            {
                epObj.iSpouseBirthDate = ep.SpouseBirthDate.Value.Year;
            }

            return PartialView("_EditEmployee", epObj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult EditEmployee(EmployeeObject EmployeeObject)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Employee", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(EmployeeObject.EmployeeID, "Employee");
            Utils.Utils.TrimObject(EmployeeObject);
            //if (ModelState.IsValid)
            {
                Employee Employee = EmployeeBusiness.Find(EmployeeObject.EmployeeID);
                Utils.Utils.BindTo(EmployeeObject, Employee);
                //VALIDATE nam sinh,doi du lieu tu nam sinh object sang nam sinh cua entity
                if (EmployeeObject.iFatherBirthDate.HasValue)
                {
                    if (EmployeeObject.iFatherBirthDate < 1900 || EmployeeObject.iFatherBirthDate > DateTime.Now.Year)
                    {
                        throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Father_Error_Edit");
                    }
                    Employee.FatherBirthDate = new DateTime(EmployeeObject.iFatherBirthDate.Value, 1, 1);
                }
                if (EmployeeObject.iMotherBirthDate.HasValue)
                {
                    if (EmployeeObject.iMotherBirthDate < 1900 || EmployeeObject.iMotherBirthDate > DateTime.Now.Year)
                    {
                        throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Mother_Error_Edit");
                    }
                    Employee.MotherBirthDate = new DateTime(EmployeeObject.iMotherBirthDate.Value, 1, 1);
                }
                if (EmployeeObject.iSpouseBirthDate.HasValue)
                {
                    if (EmployeeObject.iSpouseBirthDate < 1900 || EmployeeObject.iSpouseBirthDate > DateTime.Now.Year)
                    {
                        throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_SPouse_Error_Edit");
                    }
                    Employee.SpouseBirthDate = new DateTime(EmployeeObject.iSpouseBirthDate.Value, 1, 1);
                }
                if (EmployeeObject.StartingDate.HasValue)
                {
                    if (EmployeeObject.StartingDate <= EmployeeObject.BirthDate.Value.AddYears(17))
                    {
                        throw new SMAS.Business.Common.BusinessException("Employee_Label_CheckStartingDate");
                    }
                }
                if (EmployeeObject.StartingDate.HasValue && EmployeeObject.JoinedDate.HasValue)
                {
                    if (EmployeeObject.StartingDate.Value > EmployeeObject.JoinedDate.Value)
                    {
                        throw new SMAS.Business.Common.BusinessException("Employee_Label_CheckStartingDate_JoinedDate");
                    }
                }

                using (TransactionScope scope = new TransactionScope())
                {
                    int space = Employee.FullName.LastIndexOf(" ");
                    if (space == -1)
                    {
                        Employee.Name = Employee.FullName;
                    }
                    else
                    {
                        Employee.Name = Employee.FullName.Substring(space + 1);
                    }

                    //dua image vao employee
                    object imagePath = Session["ImagePath"];
                    if (imagePath != null)
                    {
                        byte[] imageBytes = FileToByteArray((string)imagePath);
                        Employee.Image = imageBytes;
                    }

                    //IntoSchoolDate chua dc them vao metadata,tam thoi cho fix

                    //if (!EmployeeObject.IntoSchoolDate.HasValue)
                    //{
                    //    return Json(new JsonMessage(Res.Get("Employee_Error_IntoSchoolDateBlank")));
                    //}
                    //else
                    //{
                    //    Employee.IntoSchoolDate = EmployeeObject.IntoSchoolDate.Value;
                    //}

                    Employee.EmployeeType = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT; ;
                    this.EmployeeBusiness.UpdateEmployee(Employee, true);
                    this.EmployeeBusiness.Save();

                    scope.Complete();
                }

                ViewData[SMAS.Web.Constants.GlobalConstants.JSON_MSG_LIST] = "['" + Res.Get("Common_Label_UpdateSuccessMessage") + "']";
                SetViewData();

                // return View("Index");
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            //// Lay thong bao loi va tra ve kieu JSON. View se hien thi JSON nay
            //string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            //ViewData[GlobalConstants.JSON_MSG_LIST] = jsonErrList;

            //SetViewData();
            ////return View("Index");
            //return Json(new JsonMessage(jsonErrList));
        }

        #endregion Edit

        #region Detail

        public ActionResult GetDetailEmployee(int EmployeeId)
        {
            CheckPermissionForAction(EmployeeId, "Employee");
            SetViewData();
            Employee ep = EmployeeBusiness.Find(EmployeeId);
            EmployeeObject epObj = new EmployeeObject();
            Utils.Utils.BindTo(ep, epObj, true);
            if (ep.FatherBirthDate.HasValue)
            {
                epObj.iFatherBirthDate = ep.FatherBirthDate.Value.Year;
            }
            if (ep.MotherBirthDate.HasValue)
            {
                epObj.iMotherBirthDate = ep.MotherBirthDate.Value.Year;
            }
            if (ep.SpouseBirthDate.HasValue)
            {
                epObj.iSpouseBirthDate = ep.SpouseBirthDate.Value.Year;
            }

            epObj.ContractTypeName = ep.ContractType != null ? ep.ContractType.Resolution : "";
            epObj.EducationalManagementGradeName = ep.EducationalManagementGrade != null ? ep.EducationalManagementGrade.Resolution : "";
            epObj.EthnicName = ep.Ethnic != null ? ep.Ethnic.EthnicName : "";
            epObj.FamilyTypeName = ep.FamilyType != null ? ep.FamilyType.Resolution : "";
            epObj.ForeignLanguageGradeName = ep.ForeignLanguageGrade != null ? ep.ForeignLanguageGrade.Resolution : "";
            epObj.GenreName = ep.Genre ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female");
            epObj.ITQualificationLevelName = ep.ITQualificationLevel != null ? ep.ITQualificationLevel.Resolution : "";
            epObj.PoliticalGradeName = ep.PoliticalGrade != null ? ep.PoliticalGrade.Resolution : "";
            epObj.QualificationLevelName = ep.QualificationLevel != null ? ep.QualificationLevel.Resolution : "";
            epObj.QualificationTypeName = ep.QualificationType != null ? ep.QualificationType.Resolution : "";
            epObj.ReligionName = ep.Religion != null ? ep.Religion.Resolution : "";
            epObj.SpecialityCatName = ep.SpecialityCat != null ? ep.SpecialityCat.Resolution : "";
            epObj.StateManagementGradeName = ep.StateManagementGrade != null ? ep.StateManagementGrade.Resolution : "";
            epObj.SupervisingDeptName = ep.SupervisingDept != null ? ep.SupervisingDept.SupervisingDeptName : "";
            epObj.TrainingLevelName = ep.TrainingLevel != null ? ep.TrainingLevel.Resolution : "";
            epObj.StaffPosition = ep.StaffPosition != null ? ep.StaffPosition.Resolution : "";
            return PartialView("Detail", epObj);
        }

        #endregion

        #region delete

        /**
         * Thuc hien xoa du lieu
         **/

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmp(int id)
        {
            if (GetMenupermission("Employee", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(id, "Employee");
            // DungVA - 17/06/2013 - Lấy lên danh sách nhân viên đã được cấp Account
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = id;
            // Tìm kiếm SuppervisingdeptID của từng nhân viên theo EmployeeID
            UserAccount ua = UserAccountBusiness.SearchUser(SearchInfo).FirstOrDefault();
            // End of DungVA
            if (ua == null)
            {
                Employee employee = EmployeeBusiness.Find(id);
                if (employee.SchoolID == null || employee.SchoolID == 0)
                {
                    this.EmployeeBusiness.DeleteEmployee(employee.SupervisingDeptID.Value, id);
                    this.EmployeeBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Employee_Label_CannotDelete"), "error"));
                }

            }
            else
            {
                return Json(new JsonMessage(Res.Get("Employee_Label_CannotDelete"), "error"));
            }

        }

        #endregion delete

        #region Search

        public PartialViewResult Search(string EmployeeCode, string FullName, int? StaffPosition, int? SupervisingDept, int? TrainingLevel, int page = 1, string orderBy = "")
        {
            Paginate<EmployeeObject> paging = this._Search(EmployeeCode, FullName, StaffPosition, SupervisingDept, TrainingLevel, page, orderBy);
            ViewData[EmployeeConstant.LS_EMPLOYEE] = paging;
            // DungVA -13/06 - Chuẩn bị dữ liệu cho việc quay lại trạng thái màn hình tìm kiếm
            Dictionary<string, object> dicForBack = new Dictionary<string, object>();
            dicForBack["EmployeeCode"] = EmployeeCode;
            dicForBack["FullName"] = FullName;
            dicForBack["StaffPosition"] = StaffPosition;
            dicForBack["SupervisingDept"] = SupervisingDept;
            dicForBack["TrainingLevel"] = TrainingLevel;
            dicForBack["page"] = page;
            dicForBack["orderBy"] = orderBy;
            Session[EmployeeConstant.FORBACK] = dicForBack;
            // End of DungVA -13/06 - Chuẩn bị dữ liệu cho việc quay lại trạng thái màn hình tìm kiếm
            return PartialView("_GridEmployee");
        }

        private Paginate<EmployeeObject> _Search(string EmployeeCode, string FullName, int? StaffPosition, int? SupervisingDept, int? TrainingLevel, int page = 1, string orderBy = "")
        {
            SetViewDataPermission("Employee", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            // Check dicForBack - nếu khác null thì quay lại đúng trạng thái tìm kiếm ban đầu
            // Dieu kien truy van du lieu
            GlobalInfo global = new GlobalInfo();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["EmployeeType"] = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT;
            dic["IsActive"] = true;
            dic["EmployeeCode"] = EmployeeCode;
            dic["FullName"] = FullName;
            dic["StaffPosition"] = StaffPosition;
            // DungVA - 06/06 tìm kiếm theo cấp quản lý và các cấp con
            dic["TrainingLevelID"] = TrainingLevel;
            IEnumerable<Employee> res;
            int hierachyLevel = 0;
            if (global.IsSuperVisingDeptRole)
            {
                hierachyLevel = 4;
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                hierachyLevel = 6;
            }

            if (!SupervisingDept.HasValue || SupervisingDept == 0)
            {
                SupervisingDept = global.SupervisingDeptID;
            }

            SupervisingDept svd = SupervisingDeptBusiness.Find(SupervisingDept);
            dic["SupervisingDeptID"] = SupervisingDept.Value;
            string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            dic["TraversalPath"] = tp;
            Dictionary<string, object> searchSD = new Dictionary<string, object>();
            searchSD["HierachyLevel"] = hierachyLevel;
            List<int> lstSupervisingDeptID = SupervisingDeptBusiness.Search(searchSD).Select(o => o.SupervisingDeptID).ToList();
            lstSupervisingDeptID.Add(global.SupervisingDeptID.Value);
            res = EmployeeBusiness.Search(dic).Where(o => lstSupervisingDeptID.Contains(o.SupervisingDeptID.Value));

            //if (SupervisingDept.HasValue && SupervisingDept != 0)
            //{
            //    SupervisingDept svd = SupervisingDeptBusiness.Find(SupervisingDept.Value);
            //    string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            //    // Nếu lựa chọn đơn vị quản lý
            //    dic["SupervisingDeptID"] = SupervisingDept.Value;
            //    //dic["TraversalPath"] = tp;
            //    res = EmployeeBusiness.Search(dic);
            //}
            //else//Tất cả
            //{
            //    SupervisingDept svd = SupervisingDeptBusiness.Find(new GlobalInfo().SupervisingDeptID.Value);
            //    // Nếu không lựa chọn đơn vị quản lý thì lấy cấp quản lý và các cấp con
            //    dic["SupervisingDeptID"] = global.SupervisingDeptID.Value;
            //    //dic["ParentID"] = new GlobalInfo().SupervisingDeptID.Value;
            //    string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            //    dic["TraversalPath"] = tp;
            //    Dictionary<string, object> searchSD = new Dictionary<string, object>();
            //    searchSD["ParentID"] = global.SupervisingDeptID.Value;
            //    searchSD["HierachyLevel"] = hierachyLevel;
            //    List<int> lstSupervisingDeptID = SupervisingDeptBusiness.Search(searchSD).Select(o => o.SupervisingDeptID).ToList();
            //    lstSupervisingDeptID.Add(global.SupervisingDeptID.Value);
            //    res = EmployeeBusiness.Search(dic).Where(o => lstSupervisingDeptID.Contains(o.SupervisingDeptID.Value));
            //}
            // End DungVA


            //res = EmployeeBusiness.SearchEmployee(globale.SupervisingDeptID.Value, dic);
            //res = EmployeeBusiness.SearchEmployee(SupervisingDept.Value, dic);
            //Lấy danh sách SupervisingDeptID


            if (res != null)
            {
                res = res.OrderBy(o => o.EmployeeID);

                IEnumerable<EmployeeObject> lsEmpObj = res.Select(o => new EmployeeObject
                {
                    EmployeeID = o.EmployeeID,
                    EmployeeCode = o.EmployeeCode,
                    EmployeeType = o.EmployeeType,
                    EmploymentStatus = o.EmploymentStatus,
                    SupervisingDeptID = o.SupervisingDeptID,
                    SupervisingDeptName = o.SupervisingDept.SupervisingDeptName,
                    SchoolID = o.SchoolID,
                    Name = o.Name,
                    FullName = o.FullName,
                    BirthDate = o.BirthDate,
                    BirthPlace = o.BirthPlace,
                    Telephone = o.Telephone,
                    Mobile = o.Mobile,
                    Email = o.Email,
                    TempResidentalAddress = o.TempResidentalAddress,
                    PermanentResidentalAddress = o.PermanentResidentalAddress,
                    Genre = o.Genre,
                    Alias = o.Alias,
                    MariageStatus = o.MariageStatus,
                    ForeignLanguageQualification = o.ForeignLanguageQualification,
                    HealthStatus = o.HealthStatus,
                    JoinedDate = o.JoinedDate,
                    StartingDate = o.StartingDate,
                    IdentityNumber = o.IdentityNumber,
                    IdentityIssuedDate = o.IdentityIssuedDate,
                    IdentityIssuedPlace = o.IdentityIssuedPlace,
                    HomeTown = o.HomeTown,
                    DedicatedForYoungLeague = (o.DedicatedForYoungLeague.HasValue) ? o.DedicatedForYoungLeague.Value : false,
                    FamilyTypeID = o.FamilyTypeID,
                    StaffPositionID = o.StaffPositionID,
                    EthnicID = o.EthnicID,
                    ReligionID = o.ReligionID,
                    TrainingLevelID = o.TrainingLevelID,
                    ContractID = o.ContractID,
                    ContractTypeID = o.ContractTypeID,
                    QualificationTypeID = o.QualificationTypeID,
                    SpecialityCatID = o.SpecialityCatID,
                    QualificationLevelID = o.QualificationLevelID,
                    ITQualificationLevelID = o.ITQualificationLevelID,
                    ForeignLanguageGradeID = o.ForeignLanguageGradeID,
                    PoliticalGradeID = o.PoliticalGradeID,
                    StateManagementGradeID = o.StateManagementGradeID,
                    EducationalManagementGradeID = o.EducationalManagementGradeID,
                    WorkTypeID = o.WorkTypeID,
                    PrimarilyAssignedSubjectID = o.PrimarilyAssignedSubjectID,
                    SchoolFacultyID = o.SchoolFacultyID,
                    IsYouthLeageMember = (o.IsYouthLeageMember.HasValue) ? o.IsYouthLeageMember.Value : false,
                    YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
                    YouthLeagueJoinedPlace = o.YouthLeagueJoinedPlace,
                    IsCommunistPartyMember = (o.IsCommunistPartyMember.HasValue) ? o.IsCommunistPartyMember.Value : false,
                    CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
                    CommunistPartyJoinedPlace = o.CommunistPartyJoinedPlace,
                    FatherFullName = o.FatherFullName,
                    FatherBirthDate = o.FatherBirthDate,
                    FatherJob = o.FatherJob,
                    FatherWorkingPlace = o.FatherWorkingPlace,
                    MotherFullName = o.MotherFullName,
                    MotherBirthDate = o.MotherBirthDate,
                    MotherJob = o.MotherJob,
                    MotherWorkingPlace = o.MotherWorkingPlace,
                    SpouseFullName = o.SpouseFullName,
                    SpouseBirthDate = o.SpouseBirthDate,
                    SpouseJob = o.SpouseJob,
                    SpouseWorkingPlace = o.SpouseWorkingPlace,
                    Description = o.Description,
                    //CreatedDate = o.CreatedDate,
                    IsActive = o.IsActive,
                    ModifiedDate = o.ModifiedDate,
                    //UserName = (o.UserAccounts.FirstOrDefault() != null) ? o.UserAccounts.FirstOrDefault().aspnet_Users.UserName : "",
                    StaffPosition = (o.StaffPosition != null) ? o.StaffPosition.Resolution : "",

                    //UserAccounts = (o.UserAccounts.FirstOrDefault() != null) ? (o.UserAccounts.FirstOrDefault().aspnet_Users.aspnet_Membership.Password) : ""
                    UserAccounts = (o.UserAccounts.FirstOrDefault() != null) ? ("*****") : ""
                });

                // Thuc hien phan trang tung phan
                Paginate<EmployeeObject> Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderBy(o => o.Name).ThenBy(o => o.FullName));
                string[] orderbyAndDirection = orderBy != null ? orderBy.Split('-') : new string[0];
                string orderby = orderbyAndDirection.Count() >= 1 ? orderbyAndDirection[0] : "FullName";
                string direction = orderbyAndDirection.Count() >= 2 ? orderbyAndDirection[1] : "asc";
                switch (orderby)
                {
                    // Sap theo ma
                    case "EmployeeCode":
                        switch (direction)
                        {
                            case "desc":
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderByDescending(o => o.EmployeeCode));
                                break;
                            case "asc":
                            default:
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderBy(o => o.EmployeeCode));
                                break;
                        }
                        break;
                    //Sap theo so dien thoai
                    case "Mobile":
                        switch (direction)
                        {
                            case "desc":
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderByDescending(o => o.Mobile));
                                break;
                            case "asc":
                            default:
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderBy(o => o.Mobile));
                                break;
                        }
                        break;
                    // Sap xep theo ten chuc vu
                    case "StaffPosition":
                        switch (direction)
                        {
                            case "desc":
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderByDescending(o => o.StaffPosition));
                                break;
                            case "asc":
                            default:
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderBy(o => o.StaffPosition));
                                break;
                        }
                        break;
                    // Sap xep theo ten don vi
                    case "SupervisingDeptName":
                        switch (direction)
                        {
                            case "desc":
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderByDescending(o => o.SupervisingDeptName));
                                break;
                            case "asc":
                            default:
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderBy(o => o.SupervisingDeptName));
                                break;
                        }
                        break;
                    // Sap theo ten (mac dinh)
                    case "FullName":
                    default:
                        switch (direction)
                        {
                            case "desc":
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderByDescending(o => o.Name).ThenByDescending(o => o.FullName));
                                break;
                            case "asc":
                            default:
                                Paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderBy(o => o.Name).ThenBy(o => o.FullName));
                                break;
                        }
                        break;
                }
                Paging.page = page;
                Paging.paginate();

                // Chuẩn bị dữ liệu cho việc quay lại trạng thái màn hình tìm kiếm
                Dictionary<string, object> dicForBack = new Dictionary<string, object>();
                dicForBack["EmployeeCode"] = EmployeeCode;
                dicForBack["FullName"] = FullName;
                dicForBack["StaffPosition"] = StaffPosition;
                dicForBack["SupervisingDept"] = SupervisingDept;
                dicForBack["TrainingLevel"] = TrainingLevel;
                dicForBack["page"] = page;
                dicForBack["orderBy"] = orderBy;
                Session[EmployeeConstant.FORBACK] = dicForBack;

                return Paging;
            }
            return null;
        }

        #endregion Search

        #region paging

        [GridAction(EnableCustomBinding = true)]

        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(string EmployeeCode, string Fullname, int? StaffPosition, int? SupervisingDept, int? TrainingLevel, int page = 1, string orderBy = "")
        {
            Paginate<EmployeeObject> paging = this._Search(EmployeeCode, Fullname, StaffPosition, SupervisingDept, TrainingLevel, page, orderBy);
            if (paging != null)
            {
                GridModel<EmployeeObject> gm = new GridModel<EmployeeObject>(paging.List);
                gm.Total = paging.total;
                return View(gm);
            }
            else
            {
                return View();
            }
        }

        #endregion paging

        #region SetViewData

        public void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();

            //List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.GetDependantDept(UserInfo.SupervisingDeptID()).ToList();
            List<SupervisingDept> listSupervisingDept = new List<SupervisingDept>();
            List<SupervisingDept> listSupervisingDeptTemp = new List<SupervisingDept>();
            SupervisingDept spvd = SupervisingDeptBusiness.GetSupervisingDeptOfUser(global.UserAccountID);
            // Load danh sach phong ban truc thuoc cua supervisingdept Hungnd8 25/04/2013 
            List<SupervisingDept> lstFullSupervisingDept = new List<SupervisingDept>();
            if (spvd != null)
            {
                listSupervisingDeptTemp.Add(spvd);
                lstFullSupervisingDept = SupervisingDeptBusiness.GetFollwerSupervising(spvd.SupervisingDeptID).ToList();
                foreach (SupervisingDept objSupervisingDept in lstFullSupervisingDept)
                {
                    listSupervisingDeptTemp.Add(objSupervisingDept);
                }
            }

            // end Load danh sach phong ban.....
            int hierachyLevel = 0;
            if (global.IsSuperVisingDeptRole)
            {
                hierachyLevel = 4;
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                hierachyLevel = 6;
            }
            List<int> lstSupervisingDeptID = listSupervisingDeptTemp.Where(o => o.HierachyLevel == hierachyLevel).Select(o => o.SupervisingDeptID).Distinct().ToList();
            lstSupervisingDeptID.Add(new GlobalInfo().SupervisingDeptID.Value);
            // End DungVA
            SupervisingDept svd = SupervisingDeptBusiness.Find(new GlobalInfo().SupervisingDeptID.Value);
            string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            Dictionary<string, object> dicFirst = new Dictionary<string, object>();
            dicFirst["SupervisingDeptID"] = new GlobalInfo().SupervisingDeptID.Value;
            dicFirst["TraversalPath"] = tp;
            dicFirst["EmployeeType"] = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT;
            //IQueryable<Employee> res = EmployeeBusiness.SearchEmployee(global.SupervisingDeptID.Value, new Dictionary<string, object>(){});
            // DungVA - 06/06 tìm kiếm theo cấp quản lý và các cấp con
            IEnumerable<Employee> res = EmployeeBusiness.Search(dicFirst).Where(o => lstSupervisingDeptID.Contains(o.SupervisingDeptID.Value)).ToList();
            res = res.OrderBy(x => x.EmployeeCode);
            IEnumerable<EmployeeObject> lsEmpObj = res.Select(o => new EmployeeObject
            {
                EmployeeID = o.EmployeeID,
                EmployeeCode = o.EmployeeCode,
                EmployeeType = o.EmployeeType,
                EmploymentStatus = o.EmploymentStatus,
                SupervisingDeptID = o.SupervisingDeptID,
                SupervisingDeptName = o.SupervisingDept.SupervisingDeptName,
                SchoolID = o.SchoolID,
                Name = o.Name,
                FullName = o.FullName,
                BirthDate = o.BirthDate,
                BirthPlace = o.BirthPlace,
                Telephone = o.Telephone,
                Mobile = o.Mobile,
                Email = o.Email,
                TempResidentalAddress = o.TempResidentalAddress,
                PermanentResidentalAddress = o.PermanentResidentalAddress,
                Genre = o.Genre,
                Alias = o.Alias,
                MariageStatus = o.MariageStatus,
                ForeignLanguageQualification = o.ForeignLanguageQualification,
                HealthStatus = o.HealthStatus,
                JoinedDate = o.JoinedDate,
                StartingDate = o.StartingDate,
                IdentityNumber = o.IdentityNumber,
                IdentityIssuedDate = o.IdentityIssuedDate,
                IdentityIssuedPlace = o.IdentityIssuedPlace,
                HomeTown = o.HomeTown,
                DedicatedForYoungLeague = (o.DedicatedForYoungLeague.HasValue) ? o.DedicatedForYoungLeague.Value : false,
                FamilyTypeID = o.FamilyTypeID,
                StaffPositionID = o.StaffPositionID,
                EthnicID = o.EthnicID,
                ReligionID = o.ReligionID,
                TrainingLevelID = o.TrainingLevelID,
                ContractID = o.ContractID,
                ContractTypeID = o.ContractTypeID,
                QualificationTypeID = o.QualificationTypeID,
                SpecialityCatID = o.SpecialityCatID,
                QualificationLevelID = o.QualificationLevelID,
                ITQualificationLevelID = o.ITQualificationLevelID,
                ForeignLanguageGradeID = o.ForeignLanguageGradeID,
                PoliticalGradeID = o.PoliticalGradeID,
                StateManagementGradeID = o.StateManagementGradeID,
                EducationalManagementGradeID = o.EducationalManagementGradeID,
                WorkTypeID = o.WorkTypeID,
                PrimarilyAssignedSubjectID = o.PrimarilyAssignedSubjectID,
                SchoolFacultyID = o.SchoolFacultyID,
                IsYouthLeageMember = (o.IsYouthLeageMember.HasValue) ? o.IsYouthLeageMember.Value : false,
                YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
                YouthLeagueJoinedPlace = o.YouthLeagueJoinedPlace,
                IsCommunistPartyMember = (o.IsCommunistPartyMember.HasValue) ? o.IsCommunistPartyMember.Value : false,
                CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
                CommunistPartyJoinedPlace = o.CommunistPartyJoinedPlace,
                FatherFullName = o.FatherFullName,
                FatherBirthDate = o.FatherBirthDate,
                FatherJob = o.FatherJob,
                FatherWorkingPlace = o.FatherWorkingPlace,
                MotherFullName = o.MotherFullName,
                MotherBirthDate = o.MotherBirthDate,
                MotherJob = o.MotherJob,
                MotherWorkingPlace = o.MotherWorkingPlace,
                SpouseFullName = o.SpouseFullName,
                SpouseBirthDate = o.SpouseBirthDate,
                SpouseJob = o.SpouseJob,
                SpouseWorkingPlace = o.SpouseWorkingPlace,
                Description = o.Description,
                //CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate,
                //UserName = (o.UserAccounts.FirstOrDefault() != null) ? o.UserAccounts.FirstOrDefault().aspnet_Users.UserName : "",
                StaffPosition = (o.StaffPosition != null) ? o.StaffPosition.Resolution : "",

                //UserAccounts = (o.UserAccounts.FirstOrDefault() != null) ? (o.UserAccounts.FirstOrDefault().aspnet_Users.aspnet_Membership.Password) : ""
                //UserAccounts = (o.UserAccounts.FirstOrDefault() != null) ? ("*****") : ""
            });

            Paginate<EmployeeObject> paging = new Paginate<EmployeeObject>(lsEmpObj.AsQueryable().OrderBy(o => o.Name).ThenBy(o => o.FullName));
            paging.page = 1;
            paging.paginate();

            ViewData[EmployeeConstant.LS_EMPLOYEE] = paging;
            ViewData[EmployeeConstant.LS_EMPLOYEE2] = res.ToList();

            if (spvd != null)
            {
                listSupervisingDept.Add(spvd);
                lstFullSupervisingDept = SupervisingDeptBusiness.GetFollwerSupervising(spvd.SupervisingDeptID).ToList();
                foreach (SupervisingDept objSupervisingDept in lstFullSupervisingDept)
                {
                    //objSupervisingDept.SupervisingDeptName = "....." + objSupervisingDept.SupervisingDeptName;
                    SupervisingDept temp = objSupervisingDept.BindTo<SupervisingDept>();
                    temp.SupervisingDeptName = "....." + temp.SupervisingDeptName;
                    listSupervisingDept.Add(temp);
                }
            }
            if (listSupervisingDept.Count() > 0)
            {
                ViewData[EmployeeConstant.LS_SupervisingDeptBusiness] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName");
            }
            else
            {
                ViewData[EmployeeConstant.LS_SupervisingDeptBusiness] = new SelectList(new string[] { });
            }

            List<ComboObject> listSex = new List<ComboObject>();

            //listSex.Add(new ComboObject("0", Res.Get("Common_Combo_All")));
            listSex.Add(new ComboObject("true", Res.Get("Common_Label_Male")));
            listSex.Add(new ComboObject("false", Res.Get("Common_Label_Female")));

            ViewData[EmployeeConstant.LS_SEX] = new SelectList(listSex, "key", "value");

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("IsActive", true);

            List<Ethnic> listEthnic = EthnicBusiness.Search(dic).OrderBy(o => o.EthnicName).ToList();

            //List<Ethnic> listEthnic= EthnicBusiness.All.Where(em => (em.IsActive == true)).ToList();
            ViewData[EmployeeConstant.LS_ETHNIC] = new SelectList(listEthnic, "EthnicID", "EthnicName", SMAS.Business.Common.SystemParamsInFile.DEFAULT_ETHNIC);

            //List<Religion> listReligion = ReligionBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<Religion> listReligion = ReligionBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            ViewData[EmployeeConstant.LS_RELIGION] = new SelectList(listReligion, "ReligionID", "Resolution", SMAS.Business.Common.SystemParamsInFile.DEFAULT_RELIGION);

            // List<FamilyType> listFamilyType = FamilyTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<FamilyType> listFamilyType = FamilyTypeBusiness.Search(dic).ToList();
            ViewData[EmployeeConstant.LS_FamilyTypeBusiness] = new SelectList(listFamilyType, "FamilyTypeID", "Resolution");

            //List<QualificationType> listQualificationType = QualificationTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<QualificationType> listQualificationType = QualificationTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            ViewData[EmployeeConstant.LS_QualificationTypeBusiness] = new SelectList(listQualificationType, "QualificationTypeID", "Resolution");

            //List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            ViewData[EmployeeConstant.LS_SpecialityCat] = new SelectList(listSpecialityCat, "SpecialityCatID", "Resolution");

            // List< QualificationLevel> listQualificationLevel =  QualificationLevelBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<QualificationLevel> listQualificationLevel = QualificationLevelBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            ViewData[EmployeeConstant.LS_QualificationLevelBusiness] = new SelectList(listQualificationLevel, "QualificationLevelID", "Resolution");

            //List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(dic).ToList();
            ViewData[EmployeeConstant.LS_ForeignLanguageGradeBusiness] = new SelectList(listForeignLanguageGrade, "ForeignLanguageGradeID", "Resolution");

            //List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            ViewData[EmployeeConstant.LS_ITQualificationLevelBusiness] = new SelectList(listITQualificationLevel, "ITQualificationLevelID", "Resolution");

            //List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.Search(dic).ToList();
            ViewData[EmployeeConstant.LS_StateManagementGradeBusiness] = new SelectList(listStateManagementGrade, "StateManagementGradeID", "Resolution");

            //List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            ViewData[EmployeeConstant.LS_PoliticalGradeBusiness] = new SelectList(listPoliticalGrade, "PoliticalGradeID", "Resolution");

            //List<EducationalManagementGrade> listEducationalManagementGrade =  EducationalManagementGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<EducationalManagementGrade> listEducationalManagementGrade = EducationalManagementGradeBusiness.Search(dic).ToList();
            ViewData[EmployeeConstant.LS_EducationalManagementGradeBusiness] = new SelectList(listEducationalManagementGrade, "EducationalManagementGradeID", "Resolution");

            List<ContractType> listContractType = ContractTypeBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.Resolution).ToList();

            //List<Contract> listContract = ContractBusiness.Search(dic).ToList();
            ViewData[EmployeeConstant.LS_ContractBusiness] = new SelectList(listContractType, "ContractTypeID", "Resolution");

            List<StaffPosition> listStaffPosition = StaffPositionBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.Resolution).ToList();

            // List<StaffPosition> listStaffPosition = StaffPositionBusiness.Search(dic).ToList();
            ViewData[EmployeeConstant.LS_StaffPositionBusiness] = new SelectList(listStaffPosition, "StaffPositionID", "Resolution");



            // List<GroupCat> listGroupCat = GroupCatBusiness.All.Where(em => (em.IsActive == true)).ToList();
            List<GroupCat> listGroupCat = GroupCatBusiness.GetGroupByRoleAndCreatedUser(global.RoleID, global.UserAccountID).ToList();
            //IQueryable<SupervisingDept> lsSuperTemp = SupervisingDeptBusiness.All.Where(em=>(em.SupervisingDeptID==UserInfo.SupervisingDeptID()));
            //List<GroupCat> listGroupCat = GroupCatBusiness.GetGroupByRoleAndCreatedUser(UserInfo.RoleID(),lsSuperTemp.FirstOrDefault().AdminID.Value).ToList();
            ViewData[EmployeeConstant.LS_GroupCatBusiness] = new SelectList(listGroupCat, "GroupCatID", "GroupName");

            //List<Contract> listContract = ContractBusiness.All.Where(em => (em.IsActive == true)).ToList();
            IDictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("IsActive", true);

            //dic2.Add("AppliedLevel", UserInfo.AppliedLevel());
            //List<GraduationLevel> listGraduationLevel = GraduationLevelBusiness.Search(dic2).ToList();
            //ViewData[EmployeeConstant.LS_GraduationLevelBusiness] = new SelectList(listGraduationLevel, "GraduationLevelID", "Resolution");

            List<TrainingLevel> listTrainingLevel = TrainingLevelBusiness.Search(dic).ToList();
            ViewData[EmployeeConstant.LS_TrainingLevelBusiness] = new SelectList(listTrainingLevel, "TrainingLevelID", "Resolution");
        }

        #endregion SetViewData

        #region test image


        [ValidateAntiForgeryToken]
        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        {
            GlobalInfo global = new GlobalInfo();

            if (ImageUploader == null || ImageUploader.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = ImageUploader.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> imageExtension = new List<string>();
                imageExtension.Add(".JPG");
                imageExtension.Add(".JPEG");
                imageExtension.Add(".PNG");
                imageExtension.Add(".BMP");
                imageExtension.Add(".ICO");
                imageExtension.Add(".GIF");
                var extension = Path.GetExtension(file.FileName);
                if (!imageExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
                file.SaveAs(physicalPath);
                Session["ImagePath"] = physicalPath;

                string relativePath = "/Uploads/Photos/" + fileName;
                JsonResult res = Json(new JsonMessage(fileName));
                res.ContentType = "text/plain";
                return res;
            }

            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }


        [ValidateAntiForgeryToken]
        public ActionResult RemoveImage(string[] fileNames)
        {
            //// The parameter of the Remove action must be called "fileNames"
            //foreach (var fullName in fileNames)
            //{
            //    var fileName = Path.GetFileName(fullName);
            //    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

            //    // TODO: Verify user permissions
            //    if (System.IO.File.Exists(physicalPath))
            //    {
            //        // The files are not actually removed in this demo
            //        System.IO.File.Delete(physicalPath);
            //    }
            //}

            // Return an empty string to signify success
            return Content("");
        }

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }

        #endregion test image

        #region excel
        public FileResult ExportExcel(string EmployeeCode, string FullName, int? StaffPosition, int? SupervisingDept, int? TrainingLevel)
        {
            #region lay cac sheet ra
            GlobalInfo global = new GlobalInfo();
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HT", TEMPLATE_EMPLOYEE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);

            #endregion

            #region fill
            IVTRange templateRange = sheet.GetRange("A1", "F75");

            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            List<object> QueenDic = new List<object>();

            #region lay du lieu tu grid


            int hierachyLevel = 0;
            if (global.IsSuperVisingDeptRole)
            {
                hierachyLevel = 4;
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                hierachyLevel = 6;
            }
            if (!SupervisingDept.HasValue || SupervisingDept == 0)
            {
                SupervisingDept = global.SupervisingDeptID;
            }
            List<SupervisingDept> listSupervisingDeptTemp = new List<SupervisingDept>();
            SupervisingDept spvd = SupervisingDeptBusiness.GetSupervisingDeptOfUser(global.UserAccountID);
            // Load danh sach phong ban truc thuoc cua supervisingdept Hungnd8 25/04/2013 
            List<SupervisingDept> lstFullSupervisingDept = new List<SupervisingDept>();
            if (spvd != null)
            {
                listSupervisingDeptTemp.Add(spvd);
                lstFullSupervisingDept = SupervisingDeptBusiness.GetFollwerSupervising(spvd.SupervisingDeptID).ToList();
                foreach (SupervisingDept objSupervisingDept in lstFullSupervisingDept)
                {
                    listSupervisingDeptTemp.Add(objSupervisingDept);
                }
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["EmployeeType"] = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT;
            dic["IsActive"] = true;
            dic["EmployeeCode"] = EmployeeCode;
            dic["FullName"] = FullName;
            dic["StaffPosition"] = StaffPosition;
            dic["SupervisingDeptID"] = SupervisingDept;
            dic["TrainingLevelID"] = TrainingLevel;
            //GlobalInfo globale = new GlobalInfo();
            //List<int> lstSupervisingDeptID = listSupervisingDeptTemp.Where(o => o.HierachyLevel == hierachyLevel).Select(o => o.SupervisingDeptID).Distinct().ToList();
            //lstSupervisingDeptID.Add(globale.SupervisingDeptID.Value);
            SupervisingDept svd = SupervisingDeptBusiness.Find(SupervisingDept);
            dic["SupervisingDeptID"] = SupervisingDept.Value;
            string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            dic["TraversalPath"] = tp;
            Dictionary<string, object> searchSD = new Dictionary<string, object>();
            searchSD["HierachyLevel"] = hierachyLevel;
            List<int> lstSupervisingDeptID = SupervisingDeptBusiness.Search(searchSD).Select(o => o.SupervisingDeptID).ToList();
            lstSupervisingDeptID.Add(global.SupervisingDeptID.Value);
            List<Employee> res = EmployeeBusiness.Search(dic).Where(o => lstSupervisingDeptID.Contains(o.SupervisingDeptID.Value)).ToList();
            //List<Employee> res = EmployeeBusiness.SearchEmployee(globale.SupervisingDeptID.Value, dic).OrderBy(o => o.Name).ToList();

            List<EmployeeObject> lsEmpObj = res.Select(o => new EmployeeObject
            {
                EmployeeID = o.EmployeeID,
                EmployeeCode = o.EmployeeCode,
                EmployeeType = o.EmployeeType,
                EmploymentStatus = o.EmploymentStatus,
                SupervisingDeptID = o.SupervisingDeptID,
                SupervisingDeptName = o.SupervisingDept.SupervisingDeptName,
                SchoolID = o.SchoolID,
                Name = o.Name,
                FullName = o.FullName,
                BirthDate = o.BirthDate,
                BirthPlace = o.BirthPlace,
                Telephone = o.Telephone,
                Mobile = o.Mobile,
                Email = o.Email,
                TempResidentalAddress = o.TempResidentalAddress,
                PermanentResidentalAddress = o.PermanentResidentalAddress,
                Genre = o.Genre,
                Alias = o.Alias,
                MariageStatus = o.MariageStatus,
                ForeignLanguageQualification = o.ForeignLanguageQualification,
                HealthStatus = o.HealthStatus,
                JoinedDate = o.JoinedDate,
                StartingDate = o.StartingDate,
                IdentityNumber = o.IdentityNumber,
                IdentityIssuedDate = o.IdentityIssuedDate,
                IdentityIssuedPlace = o.IdentityIssuedPlace,
                HomeTown = o.HomeTown,
                DedicatedForYoungLeague = (o.DedicatedForYoungLeague.HasValue) ? o.DedicatedForYoungLeague.Value : false,
                FamilyTypeID = o.FamilyTypeID,
                StaffPositionID = o.StaffPositionID,
                EthnicID = o.EthnicID,
                ReligionID = o.ReligionID,
                TrainingLevelID = o.TrainingLevelID,
                ContractID = o.ContractID,
                ContractTypeID = o.ContractTypeID,
                QualificationTypeID = o.QualificationTypeID,
                SpecialityCatID = o.SpecialityCatID,
                QualificationLevelID = o.QualificationLevelID,
                ITQualificationLevelID = o.ITQualificationLevelID,
                ForeignLanguageGradeID = o.ForeignLanguageGradeID,
                PoliticalGradeID = o.PoliticalGradeID,
                StateManagementGradeID = o.StateManagementGradeID,
                EducationalManagementGradeID = o.EducationalManagementGradeID,
                WorkTypeID = o.WorkTypeID,
                PrimarilyAssignedSubjectID = o.PrimarilyAssignedSubjectID,
                SchoolFacultyID = o.SchoolFacultyID,
                IsYouthLeageMember = (o.IsYouthLeageMember.HasValue) ? o.IsYouthLeageMember.Value : false,
                YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
                YouthLeagueJoinedPlace = o.YouthLeagueJoinedPlace,
                IsCommunistPartyMember = (o.IsCommunistPartyMember.HasValue) ? o.IsCommunistPartyMember.Value : false,
                CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
                CommunistPartyJoinedPlace = o.CommunistPartyJoinedPlace,
                FatherFullName = o.FatherFullName,
                FatherBirthDate = o.FatherBirthDate,
                FatherJob = o.FatherJob,
                FatherWorkingPlace = o.FatherWorkingPlace,
                MotherFullName = o.MotherFullName,
                MotherBirthDate = o.MotherBirthDate,
                MotherJob = o.MotherJob,
                MotherWorkingPlace = o.MotherWorkingPlace,
                SpouseFullName = o.SpouseFullName,
                SpouseBirthDate = o.SpouseBirthDate,
                SpouseJob = o.SpouseJob,
                SpouseWorkingPlace = o.SpouseWorkingPlace,
                Description = o.Description,
                //CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate,
                UserName = (o.UserAccounts.FirstOrDefault() != null) ? o.UserAccounts.FirstOrDefault().aspnet_Users.UserName : "",
                StaffPosition = (o.StaffPosition != null) ? o.StaffPosition.Resolution : "",

                //UserAccounts = (o.UserAccounts.FirstOrDefault() != null) ? (o.UserAccounts.FirstOrDefault().aspnet_Users.aspnet_Membership.Password) : ""
                UserAccounts = (o.UserAccounts.FirstOrDefault() != null) ? ("*****") : ""
            }).OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            #endregion

            for (int i = 0; i < lsEmpObj.Count(); i++)
            {
                EmployeeObject thisModel = lsEmpObj[i];
                Employee ep = EmployeeBusiness.Find(thisModel.EmployeeID);


                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["EmployeeCode"] = thisModel.EmployeeCode;
                princeDic["FullName"] = thisModel.FullName;
                princeDic["Mobile"] = thisModel.Mobile;
                princeDic["StaffPosition"] = ep.StaffPosition != null ? ep.StaffPosition.Resolution : "";
                princeDic["SupervisingDept"] = ep.SupervisingDept != null ? ep.SupervisingDept.SupervisingDeptName : "";
                QueenDic.Add(princeDic);
            }

            SupervisingDept sd = SupervisingDeptBusiness.Find(global.SupervisingDeptID);
            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SupervisingDeptName", sd != null ? sd.SupervisingDeptName : "");
            sheet.FillVariableValue(KingDic);
            sheet.FitAllColumnsOnOnePage = true;
            #endregion

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = TEMPLATE_EMPLOYEE;
            return result;
        }
        #endregion
    }
}
