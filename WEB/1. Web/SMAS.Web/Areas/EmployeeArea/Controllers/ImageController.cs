﻿using System;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Log;
using log4net;

namespace SMAS.Web.Areas.EmployeeArea.Controllers
{
    public class ImageController : Controller
    {
        //
        // GET: /EmployeeArea/Image/

        private readonly IEmployeeBusiness EmployeeBusiness;
        public ImageController(IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeBusiness = EmployeeBusiness;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region show image from byte[](database) to image tag in view


        public ActionResult Show(int id)
        {
            string defaultImagePath = Server.MapPath("~/Content/images/question_mark.png");
            Employee ep = EmployeeBusiness.Find(id);
            byte[] imageData;
            if (ep != null)
            {
                if (ep.Image != null)
                {
                    imageData = ep.Image;
                    return File(imageData, "image/jpg");
                }
                else
                {
                    imageData = FileToByteArray(defaultImagePath);
                    return File(imageData, "image/jpg");
                }
            }

            //neu ko co tra ve anh default
            imageData = FileToByteArray(defaultImagePath);
            return File(imageData, "image/jpg");
        }

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }

        #endregion show image from byte[](database) to image tag in view
    }
}
