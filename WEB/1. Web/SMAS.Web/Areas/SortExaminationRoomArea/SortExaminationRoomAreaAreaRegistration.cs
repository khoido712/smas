﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SortExaminationRoomArea
{
    public class SortExaminationRoomAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SortExaminationRoomArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SortExaminationRoomArea_default",
                "SortExaminationRoomArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
