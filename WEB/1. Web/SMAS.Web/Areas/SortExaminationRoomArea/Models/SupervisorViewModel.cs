﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.SortExaminationRoomArea.Models
{
    public class SupervisorViewModel
    {
        public int ExaminationID { get; set; }
        public int EmployeeID { get; set; }
        [ResourceDisplayName("Candidate_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }
        [ResourceDisplayName("Candidate_Label_EmployeeName")]
        public string FullName { get; set;}
        [ResourceDisplayName("Employee_Column_BirthDay")]
        public DateTime? BirthDate { get; set; }
        
        public int? SchoolFacultyID { get; set; }
        [ResourceDisplayName("Employee_Column_Telephone")]
        public string Telephone { get; set; }
        [ResourceDisplayName("Employee_Label_SchoolFaculty")]
        public string FacultyName { get; set; }
        [ResourceDisplayName("Employee_Label_PermanentResidentalAddress")]
        public string Adress { get; set; }

        public bool abc { get; set; }

        public int? SchoolFacultyID2 { get; set; }
        [ResourceDisplayName("Employee_Label_SchoolFaculty")]
        public string FacultyName2 { get; set; }
    }
}