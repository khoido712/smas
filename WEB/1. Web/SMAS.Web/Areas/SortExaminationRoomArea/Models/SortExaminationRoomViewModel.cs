/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Utils;
namespace SMAS.Web.Areas.SortExaminationRoomArea.Models
{
    public class SortExaminationRoomViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 ExaminationRoomID { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.Int32> ExaminationID { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 ExaminationSubjectID { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 EducationLevelID { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 Type { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 NumberOf { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 TotalCandidate { get; set; }
        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("CandidateRoom_Label_CandidateNumber")]
        [Range(1, 999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public System.Int32 CandidateNumber { get; set; }
        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("CandidateRoom_Label_Room")]
        [Range(1, 99, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public System.Int32 RoomNumber { get; set; }
        [ScaffoldColumn(false)]
        public bool chechSubject { get; set; }
        [ScaffoldColumn(false)]
        public int? CountRoom { get; set; }

        [ResourceDisplayName("ExaminationRoom_Label_RoomTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(99, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String RoomTitle { get; set; }
        
        [ResourceDisplayName("ExaminationRoom_Label_NumberOfSeat")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [Range(1, 999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int NumberOfSeat { get; set; }


        [ResourceDisplayName("ExaminationRoom_Label_Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Description { get; set; }

    }
}


