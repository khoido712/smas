﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.SortExaminationRoomArea.Models
{
    public class SearchSupervisor
    {
        [ResourceDisplayName("ExaminationRoom_Label_Examination")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ExaminationID { get; set; }

        public int? SchoolFacultyID { get; set; }
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string EmployeeCode { get; set; }
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]	
        public string EmployeeName { get; set; }
        
    }
}