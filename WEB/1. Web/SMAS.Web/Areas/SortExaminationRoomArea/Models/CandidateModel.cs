/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SortExaminationRoomArea.Models
{
    public class CandidateModel
    {
        public System.Int32 CandidateID { get; set; }

        public System.Nullable<System.Int32> ExaminationID { get; set; }

        public System.Nullable<System.Int32> SubjectID { get; set; }

        public System.Nullable<System.Int32> RoomID { get; set; }

        public System.Nullable<System.Int32> ClassID { get; set; }

        public System.Nullable<System.Int32> OrderNumber { get; set; }

        public System.Nullable<System.Decimal> Mark { get; set; }

        public System.Nullable<System.Int32> EducationLevelID { get; set; }

        public System.String Judgement { get; set; }

        public System.Nullable<System.Int32> NamedListNumber { get; set; }

        public System.Nullable<System.Boolean> HasReason { get; set; }

        public System.String ReasonDetail { get; set; }
        
        public string Name { get; set; }

        public string EthnicCode { get; set; }

        #region grid
        public int PupilID { get; set; }
        public System.Int32 CandidateID1 { get; set; }
        public System.Int32 CandidateID2 { get; set; }


        [ResourceDisplayName("Candidate_Label_NamedListCode")]
        public System.String NamedListCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime BirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthPlace")]
        public System.String BirthPlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        [ResourceDisplayName("Candidate_Label_NamedListCode")]
        public System.String NamedListCode1 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName1 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName1 { get; set; }

        [ResourceDisplayName("Candidate_Label_NamedListCode")]
        public System.String NamedListCode2 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName2 { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName2 { get; set; }

        #endregion grid
    }
}
