﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revisimoon: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.SortExaminationRoomArea.Models;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Web.Areas.SortExaminationRoomArea.Controllers
{
    public class SortExaminationRoomController : BaseController
    {
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IInvigilatorBusiness InvigilatorBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IInvigilatorAssignmentBusiness InvigilatorAssignmentBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        public SortExaminationRoomController(IInvigilatorAssignmentBusiness InvigilatorAssignmentBusiness, IEducationLevelBusiness EducationLevelBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IEmployeeBusiness EmployeeBusiness, IInvigilatorBusiness InvigilatorBusiness, ISchoolFacultyBusiness schoolFacultyBusiness, ICandidateBusiness CandidateBusiness, ISubjectCatBusiness SubjectCatBusiness, IExaminationRoomBusiness examinationroomBusiness, IExaminationBusiness ExaminationBusiness, IExaminationSubjectBusiness ExaminationSubjectBusiness,
            IClassProfileBusiness ClassProfileBusiness, IPupilProfileBusiness PupilProfileBusiness)
        {
            this.InvigilatorAssignmentBusiness = InvigilatorAssignmentBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.ExaminationSubjectBusiness = ExaminationSubjectBusiness;
            this.ExaminationBusiness = ExaminationBusiness;
            this.ExaminationRoomBusiness = examinationroomBusiness;
            this.CandidateBusiness = CandidateBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.InvigilatorBusiness = InvigilatorBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
        }
        private GlobalInfo Global = new GlobalInfo();
        #region Index
        /// <summary>
        /// Index
        /// </summary>
        /// <returns>
        /// ActionResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>
        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            SetViewData();
            //UserInfo.IsCurrentYear = TRUE
            SortExaminationRoomViewModel srm = new SortExaminationRoomViewModel();
            ViewData[SortExaminationRoomConstants.LIST_SORTEXAMINATIONROOM] = new List<CandidateModel>();
            ViewData[SortExaminationRoomConstants.BOOL_BUTTON] = true;
            ViewData[SortExaminationRoomConstants.MOVE_RIGHT] = "";
            ViewData[SortExaminationRoomConstants.MOVE_LEFT] = "";
            ViewData[SortExaminationRoomConstants.BUTTON_EXPORT] = "";

            return View();
        }
        #endregion

        #region Search
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="frm">The FRM.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            ViewData[SortExaminationRoomConstants.LIST_SORTEXAMINATIONROOM] = new List<CandidateModel>();
            return PartialView("_List");
        }
        #endregion

        #region ButtonAutoAssign
        public bool checkButtonAutoAssign(int? idExamination, int? idEducationLevel, int? idExaminationSubject)
        {
            //Hiển thị nút “Xếp tự động” khi
            //- UserInfo.IsCurrentYear = TRUE
            //- Examination(cboExamination.Value).CurentStage < EXAMINATION_STAGE_HEAD_ATTACHED
            //- cboExamination, cboEducationLevel được chọn
            //- Nếu Examination(cboExamination.Value).UsingSeparateList = 0  thì cboExaminationSubject phải được chọn
            if (Global.IsCurrentYear == true && idExamination != null && idEducationLevel != null)
            {
                Examination Exam = ExaminationBusiness.Find(idExamination.Value);
                if (Exam.CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
                {
                    if (Exam.UsingSeparateList == 1)
                    {
                        if (idExaminationSubject != null)
                        {
                            return false;
                        }
                    }
                    if (Exam.UsingSeparateList == 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion

        #region Create

        /// <summary>
        /// PrepareCreate
        /// </summary>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <param name="idExaminationSubject">The id examination subject.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <date>11/15/2012</date>
        /// <author>
        /// hath
        /// </author>

        [ValidateAntiForgeryToken]
        public PartialViewResult PrepareCreate(int? idEducationLevel, int? idExamination, int? idExaminationSubject)
        {
            if (idExamination == null)
            {
                idExamination = 0;
            }
            if (idEducationLevel == null)
            {
                idEducationLevel = 0;
            }
            if (idExaminationSubject == null)
            {
                idExaminationSubject = 0;
            }




            SortExaminationRoomViewModel serm = new SortExaminationRoomViewModel();

            serm.ExaminationID = idExamination;
            serm.EducationLevelID = idEducationLevel.Value;
            serm.ExaminationSubjectID = idExaminationSubject.Value;

            return PartialView("_Create", serm);

        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(SortExaminationRoomViewModel frm)
        {
            ExaminationRoom examinationroom = new ExaminationRoom();
            TryUpdateModel(examinationroom);
            Utils.Utils.TrimObject(examinationroom);
            if (examinationroom.NumberOfSeat == null)
            {
                examinationroom.NumberOfSeat = 0;
            }

            ExaminationSubject es = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == frm.ExaminationID.Value && o.EducationLevelID == frm.EducationLevelID).FirstOrDefault();
            if (frm.chechSubject == true)
            {
                examinationroom.ExaminationSubjectID = es.ExaminationSubjectID;

            }

            int AppliedLevel = int.Parse(Global.AppliedLevel.Value.ToString());
            ExaminationRoomBusiness.Insert(Global.SchoolID.Value, AppliedLevel, examinationroom);
            this.ExaminationRoomBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion


        #region Edit
        /// <summary>
        /// PrepareEdit
        /// </summary>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <param name="idExaminationSubject">The id examination subject.</param>
        /// <param name="idRoom">The id room.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>
        public PartialViewResult PrepareEdit(int? idEducationLevel, int? idExamination, int? idExaminationSubject, int? idRoom)
        {
            ExaminationRoom er = ExaminationRoomBusiness.Find(idRoom);
            SortExaminationRoomViewModel serm = new SortExaminationRoomViewModel();
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            if (idExaminationSubject == null) // ko được chọn
            {
                // AnhVD9 20141023 - Hotfix
                //Dictionary["AcademicYearID"] = Global.AcademicYearID;
                //Dictionary["AppliedLevel"] = Global.AppliedLevel;
                //Dictionary["EducationLevelID"] = idEducationLevel;
                //Dictionary["ExaminationID"] = idExamination;
                idExaminationSubject = 0; // Đánh dấu để cập nhật tên phòng
            }
            if (er != null)
            {
                serm.ExaminationID = idExamination;
                serm.EducationLevelID = idEducationLevel.Value;
                serm.ExaminationSubjectID = idExaminationSubject.Value;
                serm.ExaminationRoomID = er.ExaminationRoomID;
                serm.RoomTitle = er.RoomTitle;
                serm.NumberOfSeat = er.NumberOfSeat.Value;
                serm.Description = er.Description;
            }
            return PartialView("_Edit", serm);

        }
        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="ExaminationRoomID">The examination room ID.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ExaminationRoomID)
        {
            ExaminationRoom examinationroom = this.ExaminationRoomBusiness.Find(ExaminationRoomID);
            CheckPermissionForAction(examinationroom.ExaminationID, "Examination");

            // AnhVD9 - Cập nhật phòng thi cho tất cả các môn
            string ExaminationSubjectID = Request.Form.Get("ExaminationSubjectID");
            string RoomTitle = Request.Form.Get("RoomTitle");
            int NumberOfSeat = Convert.ToInt32(Request.Form.Get("NumberOfSeat").Trim());
            string Description = Request.Form.Get("Description");
            if (string.IsNullOrEmpty(ExaminationSubjectID) || ExaminationSubjectID.Equals("0"))
            {
                // Cập nhật lại toàn bộ Tên phòng cho 
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = examinationroom.EducationLevelID;
                dic["ExaminationID"] = examinationroom.ExaminationID;
                dic["RoomTitle"] = examinationroom.RoomTitle;
                var LstExamRoom = ExaminationRoomBusiness
                    .SearchBySchool(Global.SchoolID.Value, dic)
                    .Where(p=>p.RoomTitle.ToLower() == examinationroom.RoomTitle.ToLower())
                    .ToList();
                foreach (var er in LstExamRoom)
                {
                    er.RoomTitle = RoomTitle;
                    er.NumberOfSeat = NumberOfSeat;
                    er.Description = Description;
                    ExaminationRoomBusiness.Update(er);
                }
            }
            // End _ AnhVD9 20141023
            else
            {
            TryUpdateModel(examinationroom);
            Utils.Utils.TrimObject(examinationroom);
            int AppliedLevel = int.Parse(Global.AppliedLevel.Value.ToString());
            ExaminationRoomBusiness.Update(Global.SchoolID.Value, AppliedLevel, examinationroom);
            }
            
            this.ExaminationRoomBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        #region Delete
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="idRoom">The id room.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(List<int> idRoom)
        {

            foreach (int RoomID in idRoom)
            {
                this.ExaminationRoomBusiness.Delete(Global.SchoolID.Value, (int)Global.AppliedLevel.Value, RoomID);
            }
            this.ExaminationRoomBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));

        }
        #endregion

        #region Xếp tự động
        /// <summary>
        /// PrepareAutoAssign
        /// </summary>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <param name="idExaminationSubject">The id examination subject.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult PrepareAutoAssign(int? idEducationLevel, int? idExamination, int? idExaminationSubject)
        {

            if (idExaminationSubject == null)
            {
                idExaminationSubject = 0;
            }


            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = idExamination;
            Dictionary["RoomID"] = 0;
            Examination exam = ExaminationBusiness.Find(idExamination.Value);
            if (exam.UsingSeparateList == 0)
            {
                var lsExamSub1 = ExaminationSubjectBusiness.SearchBySchool(Global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"EducationLevelID",idEducationLevel}
                    ,{"ExaminationID",idExamination}
                }).Select(o => o.ExaminationSubjectID);
                if (lsExamSub1.Count() > 0)
                {
                    Dictionary["ExaminationSubjectID"] = lsExamSub1.FirstOrDefault();
                }
            }
            else
            {
                Dictionary["ExaminationSubjectID"] = idExaminationSubject;
            }
            IQueryable<Candidate> query = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);


            SortExaminationRoomViewModel serm = new SortExaminationRoomViewModel();
            serm.ExaminationID = idExamination;
            serm.EducationLevelID = idEducationLevel.Value;
            serm.ExaminationSubjectID = idExaminationSubject.Value;
            serm.TotalCandidate = query.Count();
            IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == idExamination.Value && o.EducationLevelID == idEducationLevel.Value && o.ExaminationSubjectID == idExaminationSubject.Value);
            serm.CountRoom = lsExamSub.Count();


            return PartialView("_AutoAssign", serm);

        }
        /// <summary>
        /// AutoAssign
        /// </summary>
        /// <param name="serm">The serm.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult AutoAssign(SortExaminationRoomViewModel serm)
        {
            ExaminationRoom examinationroom = new ExaminationRoom();
            TryUpdateModel(examinationroom);
            Utils.Utils.TrimObject(examinationroom);

            int AppliedLevel = int.Parse(Global.AppliedLevel.Value.ToString());
            int Type = serm.Type;
            int NumberOf = 0;
            if (serm.CandidateNumber != 0)
            {
                NumberOf = serm.CandidateNumber;
            }
            if (serm.RoomNumber != 0)
            {
                NumberOf = serm.RoomNumber;
            }
            ExaminationRoomBusiness.AutoAssign(Global.SchoolID.Value, AppliedLevel, serm.ExaminationID.Value, serm.EducationLevelID, serm.ExaminationSubjectID, (int)Type, NumberOf);
            ExaminationRoomBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AutoAssignSuccessMessage")));
        }
        #endregion

        #region SetViewData
        /// <summary>
        /// SetViewData
        /// </summary>
        /// <author>hath</author>
        /// <date>11/15/2012</date>
        public void SetViewData()
        {

            //cboExamination: ExaminationBusiness_SearchBySchool(UserInfo_SchoolID, Dictionary)
            // + Dictionary[“AcademicYearID”] = UserInfo_AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo_AppliedLevel
            IDictionary<string, object> SearchExam = new Dictionary<string, object>();
            SearchExam["AcademicYearID"] = Global.AcademicYearID;
            SearchExam["AppliedLevel"] = Global.AppliedLevel;
            List<Examination> LsExamination = ExaminationBusiness.SearchBySchool(Global.SchoolID.Value, SearchExam).OrderByDescending(o => o.ToDate).ToList();

            if (LsExamination.Count() > 0)
            {
                string examID = Request["ExaminationID"];
                int ExaminationID = 0;
                if (examID == null || examID.Trim().Equals(string.Empty) || !int.TryParse(examID, out ExaminationID))
                {
                    ViewData[SortExaminationRoomConstants.CBO_EXAMINATION] = new SelectList(LsExamination, "ExaminationID", "Title");
                    List<EducationLevel> lsEL = new List<EducationLevel>();
                    ViewData[SortExaminationRoomConstants.CBO_EDUCATIONLEVEL] = new SelectList(lsEL, "EducationLevelID", "Resolution");
                }
                else
                {
                    ViewData[SortExaminationRoomConstants.CBO_EXAMINATION] = new SelectList(LsExamination, "ExaminationID", "Title", ExaminationID);
                    // Lay thong tin khoi theo ky thi
                    List<EducationLevel> lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"AppliedLevel",_globalInfo.AppliedLevel.Value},
                    {"ExaminationID",ExaminationID}
                }).Select(o => o.EducationLevel).Distinct().OrderBy(o => o.EducationLevelID).ToList();
                    ViewData[SortExaminationRoomConstants.CBO_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
                }
            }
            else
            {
                ViewData[SortExaminationRoomConstants.CBO_EXAMINATION] = new SelectList(new List<Examination>(), "ExaminationID", "Title");
                ViewData[SortExaminationRoomConstants.CBO_EDUCATIONLEVEL] = new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            }
            ViewData[SortExaminationRoomConstants.CBO_EXAMINATONSUBJECT] = new List<ExaminationSubject>();
            ViewData[SortExaminationRoomConstants.BOOL_SUBJECT] = false;
            ViewData[SortExaminationRoomConstants.LIST_EXAMINATIONROOM] = new List<DataListRoomBO>();
            ViewData[SortExaminationRoomConstants.CBO_EXAMINATIONROOM] = new List<ExaminationRoom>();
            ViewData[SortExaminationRoomConstants.BOOL_AUTOASSIGN] = true;
            ViewData[SortExaminationRoomConstants.BOOL_DELETE] = true;
        }
        #endregion

        #region Load Combo Subject


        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEduationLevelExam(int? ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            List<EducationLevel> lsEducationLevel = new List<EducationLevel>();
            if (ExaminationID.HasValue)
            {
                lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"AppliedLevel",global.AppliedLevel.Value},
                    {"ExaminationID",ExaminationID}
                }).Select(o => o.EducationLevel).Distinct().OrderBy(o => o.EducationLevelID).ToList();
            }
            Examination exam = ExaminationBusiness.Find(ExaminationID);
            return Json(new SelectList(lsEducationLevel, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// AjaxLoadSubject
        /// </summary>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/15/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadSubject(int? idEducationLevel, int? idExamination)
        {
            // Neu khong co du lieu thi khong hien thi gi ca
            if (!idEducationLevel.HasValue || !idExamination.HasValue)
            {
                ViewData[SortExaminationRoomConstants.BOOL_AUTOASSIGN] = false;
                ViewData[SortExaminationRoomConstants.BOOL_BUTTON] = false;
                ViewData[SortExaminationRoomConstants.CBO_EXAMINATONSUBJECT] = new List<ExaminationSubject>();
                return PartialView("_ComboboxSubject");
            }
            ViewData[SortExaminationRoomConstants.BOOL_AUTOASSIGN] = true;
            ViewData[SortExaminationRoomConstants.BOOL_BUTTON] = true;

            //- UserInfo.IsCurrentYear = TRUE
            //- Examination(cboExamination.Value).CurentStage < EXAMINATION_STAGE_HEAD_ATTACHED
            if (Global.IsCurrentYear == true && ExaminationBusiness.Find(idExamination).CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
            {
                ViewData[SortExaminationRoomConstants.BOOL_BUTTON] = false;
            }
            //cboExaminationSubject: ExaminationSubjectBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) để fill dữ liệu vào cboExaminationSubject
            // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            // + Dictionary[“EducationLevelID”] = cboEducationLevel.Value
            // + Dictionary[“ExaminationID”] = cboExamination.Value 
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = idExamination;
            IQueryable<ExaminationSubject> lsExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            List<ExaminationSubject> lstExaminationSubject = new List<ExaminationSubject>();
            if (lsExaminationSubject.Count() > 0)
            {
                Examination Exam = ExaminationBusiness.Find(idExamination);
                if (Exam != null)
                {
                    //Nếu Examination(cboExamination.Value).UsingSeparateList = 1 Hệ thống enable cboExaminationSubject
                    if (Exam.UsingSeparateList == 1)
                    {
                        lstExaminationSubject = lsExaminationSubject.ToList();
                        ViewData[SortExaminationRoomConstants.BOOL_SUBJECT] = false;
                        ViewData[SortExaminationRoomConstants.CBO_EXAMINATONSUBJECT] = lstExaminationSubject;
                        return PartialView("_ComboboxSubject");
                    }
                    //Nếu Examination(cboExamination.Value).UsingSeparateList != 1 hệ thống disable cboExaminationSubject 
                    //và gán cboExaminationSubject.Value = giá trị đầu trong danh sách (ngoại trừ giá trị 0) sao đó hệ thống thực hiện gọi hàm
                    //ExaminationRoomBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) để fill dữ liệu vào dlRoom
                    // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                    // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
                    // + Dictionary[“EducationLevelID”] = cboEducationLevel.Value
                    // + Dictionary[“ExaminationID”] = cboExamination.Value 
                    // + Dictionary[“ExaminationSubjectID”] = cboExaminationSubject.Value
                    else
                    {
                        lstExaminationSubject = lsExaminationSubject.ToList();
                        ViewData[SortExaminationRoomConstants.BOOL_SUBJECT] = true;
                        ViewData[SortExaminationRoomConstants.CBO_EXAMINATONSUBJECT] = lstExaminationSubject;
                        return PartialView("_ComboboxSubject");
                    }
                }
            }
            lstExaminationSubject = lsExaminationSubject.ToList();
            ViewData[SortExaminationRoomConstants.BOOL_SUBJECT] = false;
            ViewData[SortExaminationRoomConstants.CBO_EXAMINATONSUBJECT] = lstExaminationSubject;
            return PartialView("_ComboboxSubject");
        }
        #endregion

        #region Load DataList
        /// <summary>
        /// AjaxSelectSubject
        /// </summary>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <param name="idExaminationSubject">The id examination subject.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxSelectSubject(int? idEducationLevel, int? idExamination, int? idExaminationSubject, string checkSubject)
        {
            ViewData[SortExaminationRoomConstants.LIST_EXAMINATIONROOM] = new List<DataListRoomBO>();
            Examination exam = ExaminationBusiness.Find(idExamination);
            ViewData[SortExaminationRoomConstants.BOOL_BUTTON] = true;
            if (Global.IsCurrentYear == true && exam.CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
            {
                ViewData[SortExaminationRoomConstants.BOOL_BUTTON] = false;
            }
            ViewData[SortExaminationRoomConstants.BOOL_AUTOASSIGN] = true;
            if (idExamination != null)
            {
                if (exam.UsingSeparateList == 0)
                {
                    ViewData[SortExaminationRoomConstants.BOOL_AUTOASSIGN] = checkButtonAutoAssign(idExamination, idEducationLevel, null);
                }
                if (exam.UsingSeparateList == 1)
                {
                    ViewData[SortExaminationRoomConstants.BOOL_AUTOASSIGN] = checkButtonAutoAssign(idExamination, idEducationLevel, idExaminationSubject);
                }
            }
            if (checkSubject == "True")
            {
                IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == idExamination.Value && o.EducationLevelID == idEducationLevel.Value);
                if (lsExamSub.Count() > 0)
                {
                    idExaminationSubject = lsExamSub.First().ExaminationSubjectID;
                }
                else
                {
                    return PartialView("_DataList");
                }
            }
            if (exam.UsingSeparateList == 1 && idExaminationSubject == null)
            {

                return PartialView("_DataList");
            }

            IDictionary<string, object> DictionaryNull = new Dictionary<string, object>();
            DictionaryNull["AcademicYearID"] = Global.AcademicYearID;
            DictionaryNull["AppliedLevel"] = Global.AppliedLevel;
            DictionaryNull["EducationLevelID"] = idEducationLevel;
            DictionaryNull["ExaminationID"] = idExamination;
            DictionaryNull["ExaminationSubjectID"] = idExaminationSubject;
            DictionaryNull["RoomID"] = null;
            int SoHS = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, DictionaryNull).Count();

            List<DataListRoomBO> lstER = new List<DataListRoomBO>();
            DataListRoomBO ExamRoom = new DataListRoomBO();
            ExamRoom.ExaminationRoomID = 0;
            ExamRoom.RoomDetail = SMAS.Web.Constants.GlobalConstants.CHUA_XEP_PHONG_THI + "(" + SoHS + ")";
            lstER.Add(ExamRoom);
            IQueryable<ExaminationRoom> lsExaminationRoom = null;
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = idExamination;
            Dictionary["ExaminationSubjectID"] = idExaminationSubject;

            lsExaminationRoom = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            if (lsExaminationRoom.Count() != 0)
            {
                List<ExaminationRoom> lstExamRoom = lsExaminationRoom.OrderBy(o => o.RoomTitle).ToList();
                foreach (ExaminationRoom er in lstExamRoom)
                {
                    DictionaryNull["RoomID"] = er.ExaminationRoomID;
                    SoHS = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, DictionaryNull).Count();
                    DataListRoomBO dr = new DataListRoomBO();
                    dr.ExaminationRoomID = er.ExaminationRoomID;
                    dr.RoomDetail = er.RoomTitle + "(" + SoHS + "/" + er.NumberOfSeat.ToString() + ")";
                    lstER.Add(dr);
                }
            }
            ViewData[SortExaminationRoomConstants.LIST_EXAMINATIONROOM] = lstER;
            return PartialView("_DataList");

        }
        #endregion

        #region Load Grid
        /// <summary>
        /// AjaxLoadGrid
        /// </summary>
        /// <param name="idRoom">The id room.</param>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadGrid(int? idRoom, int? idEducationLevel, int? idExamination, int? idExaminationSubject, string checkSubject)
        {
            if (idRoom == null || idEducationLevel == null || idExamination == null)
            {
                ViewData[SortExaminationRoomConstants.LIST_SORTEXAMINATIONROOM] = new List<CandidateModel>();
                return PartialView("_List");
            }
            
            List<CandidateModel> lst = this.getListCandidateModel(idRoom, idEducationLevel, idExamination, idExaminationSubject, checkSubject);

            ViewData[SortExaminationRoomConstants.LIST_SORTEXAMINATIONROOM] = lst;
            if (lst.Count() > 0)
            {
                ViewData[SortExaminationRoomConstants.BUTTON_EXPORT] = "enabled";
            }
            else
            {
                ViewData[SortExaminationRoomConstants.BUTTON_EXPORT] = "disabled";
            }
            return PartialView("_List");

        }
        #endregion

        #region Load Room
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationRoom(int? idEducationLevel, int? idExamination, int? idExaminationSubject, string checkSubject)
        {
            ViewData[SortExaminationRoomConstants.CBO_EXAMINATIONROOM] = new List<ExaminationRoom>();
            if (idEducationLevel == null || idExamination == null)
            {

                return Json(new SelectList(new List<ExaminationRoom>(), "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
            }
            if (checkSubject == "True")
            {
                IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == idExamination.Value && o.EducationLevelID == idEducationLevel.Value);
                if (lsExamSub.Count() > 0)
                {
                    idExaminationSubject = lsExamSub.FirstOrDefault().ExaminationSubjectID;
                }
                else
                {
                    return Json(new SelectList(new List<ExaminationRoom>(), "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
                }
            }
            //hệ thống thực hiện gọi hàm
            //ExaminationRoomBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) để fill dữ liệu vào cboExaminationRoomFirst, cboExaminationRoomSecond
            // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            // + Dictionary[“EducationLevelID”] = cboEducationLevel.Value
            // + Dictionary[“ExaminationID”] = cboExamination.Value 
            // + Dictionary[“ExaminationSubjectID”] = cboExaminationSubject.Value

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = idExamination;
            Dictionary["ExaminationSubjectID"] = idExaminationSubject;
            List<ExaminationRoom> lstExamRoom = new List<ExaminationRoom>();

            IQueryable<ExaminationRoom> lsExaminationRoom = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            if (lsExaminationRoom.Count() > 0)
            {
                lstExamRoom = lsExaminationRoom.ToList().OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.RoomTitle)).ToList();
            }
            ViewData[SortExaminationRoomConstants.CBO_EXAMINATIONROOM] = lstExamRoom;
            return Json(new SelectList(lstExamRoom, "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
        }
        #endregion
        /// <summary>
        /// Lay danh sach thi sinh voi cac dieu kien dua tren cac tham so dau vao
        /// </summary>
        /// <param name="idRoom"></param>
        /// <param name="idEducationLevel"></param>
        /// <param name="idExamination"></param>
        /// <param name="idExaminationSubject"></param>
        /// <param name="checkSubject"></param>
        /// <returns></returns>
        private List<CandidateModel> getListCandidateModel(int? idRoom, int? idEducationLevel, int? idExamination, int? idExaminationSubject, string checkSubject)
        {
            if (idRoom == 0) idRoom = null;
            if (checkSubject == "True")
            {
                IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == idExamination.Value && o.EducationLevelID == idEducationLevel.Value);
                if (lsExamSub.Count() > 0)
                {
                    idExaminationSubject = lsExamSub.FirstOrDefault().ExaminationSubjectID;
                }
            }
            // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            // + Dictionary[“EducationLevelID”] = cboEducationLevel.Value
            // + Dictionary[“ExaminationID”] = cboExamination.Value 
            // + Dictionary[“RoomID”] = dlRoom.Value
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = idExamination;
            Dictionary["RoomID"] = idRoom;
            Dictionary["ExaminationSubjectID"] = idExaminationSubject;
            IQueryable<Candidate> query = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");

            List<CandidateModel> lst = (from c in query
                                        join pp in PupilProfileBusiness.All on c.PupilID equals pp.PupilProfileID
                                        join cp in ClassProfileBusiness.All on c.ClassID.Value equals cp.ClassProfileID
                                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                        select new CandidateModel()
                                        {
                                            NamedListNumber = c.NamedListNumber,
                                            NamedListCode = c.NamedListCode,
                                            FullName = pp.FullName,
                                            ClassName = cp.DisplayName,
                                            BirthDate = pp.BirthDate,
                                            EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null,
                                            BirthPlace = pp.BirthPlace,
                                            Name = pp.Name,
                                            PupilCode = pp.PupilCode,
                                            GenreName = pp.Genre == SystemParamsInFile.GENRE_MALE ? male : female
                                        })
                                        .ToList()
                                        .OrderBy(o => o.NamedListNumber).ThenBy(p=>p.NamedListCode)
                                        .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName.getOrderingName(o.EthnicCode)))
                                        .ThenBy(o => o.BirthDate)
                                        .ToList();
            return lst;
        }

        #region Hiệu chỉnh
        /// <summary>
        /// AjaxLoadGrid1
        /// </summary>
        /// <param name="idRoom">The id room.</param>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadGrid1(int? idRoom, int? idEducationLevel, int? idExamination, int? idExaminationSubject, string checkSubject)
        {
            if (idEducationLevel == null || idExamination == null)
            {
                ViewData[SortExaminationRoomConstants.LIST_CANDIDATEROOM1] = new List<CandidateModel>();
                return PartialView("_ListCandidate1");
            }
            if (checkSubject == "True")
            {
                IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == idExamination.Value && o.EducationLevelID == idEducationLevel.Value);
                if (lsExamSub.Count() > 0)
                {
                    idExaminationSubject = lsExamSub.FirstOrDefault().ExaminationSubjectID;
                }
            }
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = idExamination;
            Dictionary["RoomID"] = idRoom;
            if (idExaminationSubject != null)
            {
                Dictionary["ExaminationSubjectID"] = idExaminationSubject;
            }


            IQueryable<Candidate> query = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            IOrderedEnumerable<CandidateModel> lst = query.Select(o => new CandidateModel
                                                                            {
                                                                                NamedListNumber = o.NamedListNumber,
                                                                                NamedListCode1 = o.NamedListCode,
                                                                                FullName1 = o.PupilProfile.FullName,
                                                                                ClassName1 = o.ClassProfile.DisplayName,
                                                                                CandidateID1 = o.CandidateID,
                                                                                EthnicCode = o.PupilProfile.Ethnic != null ? o.PupilProfile.Ethnic.EthnicCode : null,
                                                                                BirthDate = o.PupilProfile.BirthDate
                                                                            })
                                                            .ToList()
                                                            .OrderBy(c => c.NamedListNumber)
                                                            .ThenBy(c => c.NamedListCode1)
                                                            .ThenBy(c => SMAS.Business.Common.Utils.SortABC(c.FullName1.getOrderingName(c.EthnicCode)))
                                                            .ThenBy(c => c.BirthDate);
            ViewData[SortExaminationRoomConstants.LIST_CANDIDATEROOM1] = lst.ToList();
            if (lst.Count() <= 0)
            {
                ViewData[SortExaminationRoomConstants.MOVE_RIGHT] = "disabled";
            }
            else
            {
                ViewData[SortExaminationRoomConstants.MOVE_RIGHT] = "enabled";
            }
            return PartialView("_ListCandidate1");

        }

        /// <summary>
        /// AjaxLoadGrid2
        /// </summary>
        /// <param name="idRoom">The id room.</param>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadGrid2(int? idRoom, int? idEducationLevel, int? idExamination, int? idExaminationSubject, string checkSubject)
        {
            if (idEducationLevel == null || idExamination == null)
            {
                ViewData[SortExaminationRoomConstants.LIST_CANDIDATEROOM2] = new List<CandidateModel>();
                return PartialView("_ListCandidate2");
            }
            if (checkSubject == "True")
            {
                IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == idExamination.Value && o.EducationLevelID == idEducationLevel.Value);
                if (lsExamSub.Count() > 0)
                {
                    idExaminationSubject = lsExamSub.FirstOrDefault().ExaminationSubjectID;
                }
            }
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = idExamination;
            Dictionary["RoomID"] = idRoom;
            if (idExaminationSubject != null)
            {
                Dictionary["ExaminationSubjectID"] = idExaminationSubject;
            }

            IQueryable<Candidate> query = CandidateBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary);
            IOrderedEnumerable<CandidateModel> lst = query.Select(o => new CandidateModel
            {
                NamedListNumber = o.NamedListNumber,
                NamedListCode2 = o.NamedListCode,
                FullName2 = o.PupilProfile.FullName,
                BirthDate = o.PupilProfile.BirthDate,
                EthnicCode = o.PupilProfile.Ethnic != null ? o.PupilProfile.Ethnic.EthnicCode : null,
                ClassName2 = o.ClassProfile.DisplayName,
                CandidateID2 = o.CandidateID
            })
            .ToList()
            .OrderBy(c => c.NamedListNumber)
            .ThenBy(c => c.NamedListCode2)                                
            .ThenBy(c => SMAS.Business.Common.Utils.SortABC(c.FullName2.getOrderingName(c.EthnicCode)))
            .ThenBy(c => c.BirthDate);
            ViewData[SortExaminationRoomConstants.LIST_CANDIDATEROOM2] = lst.ToList();
            if (lst.Count() <= 0)
            {
                ViewData[SortExaminationRoomConstants.MOVE_LEFT] = "disabled";
            }
            else
            {
                ViewData[SortExaminationRoomConstants.MOVE_LEFT] = "enabled";
            }
            return PartialView("_ListCandidate2");

        }

        /// <summary>
        /// AjaxLoadExaminationRoom
        /// </summary>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <param name="idExaminationSubject">The id examination subject.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>


        #endregion

        #region Move
        /// <summary>
        /// ClickMoveRight
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/16/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClickMoveRight(FormCollection fc)
        {
            //CandidateBusiness.UpdateRoom(UserInfo.SchoolID, UserInfo.AppliedLevel, CandidateID, cboExaminationRoomSecond.value) 
            //với CandidateID tương ứng thí sinh được chọn

            //Load lại 2 grid thí sinh

            GlobalInfo global = new GlobalInfo();
            int RoomID = 0;
            int ExaminationID = 0;
            int EducationLevelID = 0;
            int ExaminationSubjectID = 0;
            string strExaminationID = fc["ExaminationID"];
            string strExaminationRoomID = fc["ExaminationRoom2"];
            string strEducationLevelID = fc["EducationLevelID"];
            string checkSubject = fc["checkSubjectCandidate"];
            if (strExaminationRoomID == "")
            {
                return Json(new JsonMessage(Res.Get("ExaminationRoom_Validate_RoomNotChoice")));
            }
            if (strExaminationID != "")
            {
                ExaminationID = int.Parse(fc["ExaminationID"]);
            }
            if (strExaminationRoomID != "")
            {
                RoomID = int.Parse(fc["ExaminationRoom2"]);
            }
            if (strEducationLevelID != "")
            {
                EducationLevelID = int.Parse(fc["EducationLevelID"]);
            }

            var liststringid = fc["chkStatus1"];
            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (RoomID == 0)
            {
                dic["RoomID"] = null;
            }
            else
            {
                dic["RoomID"] = RoomID;
            }
            if (ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, dic).Count() < 1)
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_RoomNotExist")));
            }
            if (ExaminationBusiness.All.Where(o => o.ExaminationID == ExaminationID && o.IsActive == true).Count() < 1)
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_ExaminationNotExist")));
            }
            if (checkSubject == "True")
            {
                IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == ExaminationID && o.EducationLevelID == EducationLevelID);
                if (lsExamSub.Count() > 0)
                {
                    ExaminationSubjectID = lsExamSub.FirstOrDefault().ExaminationSubjectID;
                }
                else
                {
                    return Json(new SelectList(new List<ExaminationRoom>(), "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (fc["ExaminationSubjectID"] != "")
                {
                    ExaminationSubjectID = int.Parse(fc["ExaminationSubjectID"]);
                }
            }

            if (liststringid != null)
            {
                string[] lstid = liststringid.Split(new Char[] { ',' });
                if (lstid.Count() > 0)
                {
                    if (RoomID != 0)
                    {
                        int NumOfSeat = ExaminationRoomBusiness.Find(RoomID).NumberOfSeat.HasValue ? ExaminationRoomBusiness.Find(RoomID).NumberOfSeat.Value : 0;
                        //tim so luong thi sinh da co trong phong roomid
                        var lsCandidate = CandidateBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                        {
                           {"RoomID",RoomID}
                            ,{"ExaminationID",ExaminationID}
                            ,{"EducationLevelID",EducationLevelID}
                            ,{"ExaminationSubjectID",ExaminationSubjectID}
                        }).Count();
                        if (lstid.Count() > (NumOfSeat - lsCandidate))
                        {
                            return Json(new JsonMessage(Res.Get("Common_Validate_NumberOfSeat"), JsonMessage.ERROR));
                        }

                        else
                        {
                            foreach (var item in lstid)
                            {
                                var id = int.Parse(item);
                                CandidateBusiness.UpdateRoom(Global.SchoolID.Value, (int)Global.AppliedLevel.Value, id, RoomID);
                            }
                            CandidateBusiness.Save();
                        }
                    }
                    else
                    {
                        foreach (var item in lstid)
                        {
                            var id = int.Parse(item);
                            CandidateBusiness.UpdateRoom(Global.SchoolID.Value, (int)Global.AppliedLevel.Value, id, RoomID);
                        }
                        CandidateBusiness.Save();
                    }
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_NotSelectPupil")));
            }

            return Json(new JsonMessage(Res.Get("Common_Label_MoveSuccessMessage")));
        }

        /// <summary>
        /// ClickMoveLeft
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/16/2012</date>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClickMoveLeft(FormCollection fc)
        {
            //CandidateBusiness.UpdateRoom(UserInfo.SchoolID, UserInfo.AppliedLevel, CandidateID, cboExaminationRoomSecond.value) 
            //với CandidateID tương ứng thí sinh được chọn

            //Load lại 2 grid thí sinh
            GlobalInfo global = new GlobalInfo();

            int RoomID = 0;
            int ExaminationID = 0;
            string strExaminationID = fc["ExaminationID"];
            string strExaminationRoomID = fc["ExaminationRoom1"];
            int? idEducationLevel = int.Parse(fc["EducationLevelID"]);
            int ExaminationSubjectID = 0;
            if (strExaminationRoomID == "")
            {
                return Json(new JsonMessage(Res.Get("ExaminationRoom_Validate_RoomNotChoice")));
            }
            if (strExaminationID != "")
            {
                ExaminationID = int.Parse(strExaminationID);
            }
            if (strExaminationRoomID != "")
            {
                RoomID = int.Parse(strExaminationRoomID);
            }
            var liststringid = fc["chkSatus2"];
            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (RoomID == 0)
            {
                dic["RoomID"] = null;
            }
            else
            {
                dic["RoomID"] = RoomID;
            }
            if (ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, dic).Count() < 1)
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_RoomNotExist")));
            }
            if (ExaminationBusiness.All.Where(o => o.ExaminationID == ExaminationID && o.IsActive == true).Count() < 1)
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_ExaminationNotExist")));
            }

            string checkSubject = fc["checkSubjectCandidate"];
            if (checkSubject == "True")
            {
                IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == ExaminationID && o.EducationLevelID == idEducationLevel.Value);
                if (lsExamSub.Count() > 0)
                {
                    ExaminationSubjectID = lsExamSub.FirstOrDefault().ExaminationSubjectID;
                }
                else
                {
                    return Json(new SelectList(new List<ExaminationRoom>(), "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (fc["ExaminationSubjectID"] != "")
                {
                    ExaminationSubjectID = int.Parse(fc["ExaminationSubjectID"]);
                }
            }
            if (liststringid != null)
            {
                string[] lstid = liststringid.Split(new Char[] { ',' });
                if (lstid.Count() > 0)
                {
                    if (RoomID != 0)
                    {
                        int NumOfSeat = ExaminationRoomBusiness.Find(RoomID).NumberOfSeat.HasValue ? ExaminationRoomBusiness.Find(RoomID).NumberOfSeat.Value : 0;
                        //tim so luong thi sinh da co trong phong roomid
                        var lsCandidate = CandidateBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                        {
                            {"RoomID",RoomID}
                            ,{"ExaminationID",ExaminationID}
                            ,{"EducationLevelID",idEducationLevel}
                            ,{"ExaminationSubjectID",ExaminationSubjectID}
                        }).Count();
                        if (lstid.Count() > (NumOfSeat - lsCandidate))
                        {
                            return Json(new JsonMessage(Res.Get("Common_Validate_NumberOfSeat")));
                        }

                        else
                        {
                            foreach (var item in lstid)
                            {
                                var id = int.Parse(item);
                                CandidateBusiness.UpdateRoom(Global.SchoolID.Value, (int)Global.AppliedLevel.Value, id, RoomID);
                            }
                            CandidateBusiness.Save();
                        }
                    }
                    else
                    {
                        foreach (var item in lstid)
                        {
                            var id = int.Parse(item);
                            CandidateBusiness.UpdateRoom(Global.SchoolID.Value, (int)Global.AppliedLevel.Value, id, RoomID);
                        }
                        CandidateBusiness.Save();
                    }
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_NotSelectPupil")));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_MoveSuccessMessage")));
        }



        #endregion

        #region chia deu cho cac phong
        /// <summary>
        /// Average
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns>
        /// JsonResult
        /// </returns>
        /// <date>11/16/2012</date>
        /// <author>
        /// hath
        /// </author>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Average(FormCollection fc)
        {

            //ExaminationRoomBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
            //ListRoom
            // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            // + Dictionary[“EducationLevelID”] = cboEducationLevel.Value
            // + Dictionary[“ExaminationID”] = cboExamination.Value 
            // + Dictionary[“ExaminationSubjectID”] = cboExaminationSubject.Value
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            int? idExamination = int.Parse(fc["ExaminationID"]);
            int? idEducationLevel = int.Parse(fc["EducationLevelID"]);
            Dictionary["AcademicYearID"] = Global.AcademicYearID;
            Dictionary["AppliedLevel"] = Global.AppliedLevel;
            Dictionary["EducationLevelID"] = idEducationLevel;
            Dictionary["ExaminationID"] = int.Parse(fc["ExaminationID"]);
            int ExaminationSubjectID = 0;
            string checkSubject = fc["checkSubjectCandidate"];
            int RoomID = 0;

            if (fc["ExaminationRoom1"] != "")
            {
                RoomID = int.Parse(fc["ExaminationRoom1"]);
            }
            ExaminationRoom ExamRoom = ExaminationRoomBusiness.Find(RoomID);
            if (checkSubject == "True")
            {
                IQueryable<ExaminationSubject> lsExamSub = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == idExamination.Value && o.EducationLevelID == idEducationLevel.Value);
                if (lsExamSub.Count() > 0)
                {
                    ExaminationSubjectID = lsExamSub.FirstOrDefault().ExaminationSubjectID;
                }
                else
                {
                    return Json(new SelectList(new List<ExaminationRoom>(), "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (fc["ExaminationSubjectID"] != "")
                {
                    ExaminationSubjectID = int.Parse(fc["ExaminationSubjectID"]);
                }
            }
            Dictionary["ExaminationSubjectID"] = ExaminationSubjectID;
            IQueryable<ExaminationRoom> lsExamRoom = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary).Where(o => o.ExaminationRoomID != RoomID);
            List<ExaminationRoom> lstExamRoom = new List<ExaminationRoom>();
            //Các CandidateID của bản ghi được check ở grvCandidateFirst tạo thành danh sách ListCandidateID

            //Lấy từng CandidateID trong ListCandidateID ghép với một RoomID trong ListRoom (RoomID được lấy tuần tự, 
            //khi nào phòng đã hêt chỗ thì bỏ qua không lấy) 
            //rồi gọi hàm CandidateBusiness.UpdateRoom(UserInfo.SchoolID, UserInfo.AppliedLevel, CandidateID, RoomID)

            List<int> ListCandidateID = new List<int>();

            var liststringid = fc["chkStatus1"];
            if (liststringid != null)
            {
                string[] lstid = liststringid.Split(new Char[] { ',' });
                if (lstid.Count() > 0)
                {
                    foreach (var item in lstid)
                    {
                        int id = int.Parse(item);
                        ListCandidateID.Add(id);
                    }
                }
            }
            if (ListCandidateID.Count > 0)
            {
                if (lsExamRoom.Count() > 0)
                {
                    lstExamRoom = lsExamRoom.ToList();
                    if (ExamRoom != null)
                    {
                        lstExamRoom.Remove(ExamRoom);
                    }
                    ExaminationRoomBusiness.AverageAssign(Global.SchoolID.Value, Global.AppliedLevel.Value, ListCandidateID, lstExamRoom);
                    ExaminationRoomBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_AverageSuccessMessage")));
                }
                else return Json(new JsonMessage(Res.Get("ExaminationRoom_Validate_NoRoom")));
            }


            return Json(new JsonMessage(Res.Get("Common_Validate_NotAverage")));
        }
        #endregion

        #region Lap danh sach giam thi

        [ValidateAntiForgeryToken]
        public JsonResult CheckButtonAdd(int? ExaminationID)
        {
            if (ExaminationID != null)
            {
                //kiem tra xem ky thi nay duoc xep phong chua,chua xep thi an nut them moi
                var checkExam = ExaminationRoomBusiness.SearchBySchool(Global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                    ,{"AcademicYearID",Global.AcademicYearID}
                });
                if (Global.IsCurrentYear == true
                    && ExaminationBusiness.Find(ExaminationID.Value).CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED
                    && checkExam.Count() > 0)
                {
                    return Json(JsonMessage.SUCCESS);
                }
            }
            return Json(JsonMessage.ERROR);
        }


        /// <summary>
        /// SetViewDataSupervisor
        /// </summary>
        /// <author>hath</author>
        /// <date>11/22/2012</date>
        public void SetViewDataSupervisor()
        {

            //cboExamination: ExaminationBusiness_SearchBySchool(UserInfo_SchoolID, Dictionary)
            // + Dictionary[“AcademicYearID”] = UserInfo_AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo_AppliedLevel
            IDictionary<string, object> SearchExam = new Dictionary<string, object>();
            SearchExam["AcademicYearID"] = Global.AcademicYearID;
            SearchExam["AppliedLevel"] = Global.AppliedLevel;
            IQueryable<Examination> LsExamination = ExaminationBusiness.SearchBySchool(Global.SchoolID.Value, SearchExam).OrderByDescending(o => o.ToDate);
            ViewData[SortExaminationRoomConstants.CBO_EXAMINATION] = new List<Examination>();
            if (LsExamination.Count() > 0)
            {
                ViewData[SortExaminationRoomConstants.CBO_EXAMINATION] = LsExamination.ToList();
            }


            ViewData[SortExaminationRoomConstants.CBO_FACULTY] = new List<SchoolFaculty>();
            IQueryable<SchoolFaculty> lsFaculty = SchoolFacultyBusiness.SearchBySchool(Global.SchoolID.Value, SearchExam);
            if (lsFaculty.Count() > 0)
            {
                var orderedListFacuty = lsFaculty.OrderBy(p => p.FacultyName).ToList();
                ViewData[SortExaminationRoomConstants.CBO_FACULTY] = orderedListFacuty;
            }

            ViewData[SortExaminationRoomConstants.BOOL_DELETE] = true;
            ViewData[SortExaminationRoomConstants.BOOL_ADD] = true;



        }


        /// <summary>
        /// Supervisor
        /// </summary>
        /// <returns>
        /// ActionResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>

        public ActionResult Supervisor(int? idExamination)
        {
            if (idExamination == null)
            {
                ViewData[SortExaminationRoomConstants.INDEX] = 0;
                SetViewDataSupervisor();
                ViewData[SortExaminationRoomConstants.LIST_SUPERVISOR] = new List<SupervisorViewModel>();

                return View();
            }
            else
            {
                SetViewDataSupervisor();
                ViewData[SortExaminationRoomConstants.INDEX] = 0;
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();

                //cbo Examination
                //ExaminationBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
                Dictionary["AcademicYearID"] = Global.AcademicYearID;
                Dictionary["AppliedLevel"] = Global.AppliedLevel;
                IQueryable<Examination> lsExamination = ExaminationBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary).OrderByDescending(o => o.ToDate);
                if (lsExamination.Count() > 0)
                {
                    List<Examination> ListExam = new List<Examination>();
                    ListExam = lsExamination.ToList();
                    ViewData[SortExaminationRoomConstants.CBO_EXAMINATION] = ListExam;
                    ViewData[SortExaminationRoomConstants.INDEX] = idExamination;

                }

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

                //Get view data here

                //InvigilatorAssignmentBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
                // + Dictionary[“ExaminationID”] = cboExamination.Value
                ViewData[SortExaminationRoomConstants.LIST_INVI] = new List<InvigilatorAssignment>();
                SearchInfo["AcademicYearID"] = Global.AcademicYearID;
                SearchInfo["AppliedLevel"] = Global.AppliedLevel;
                SearchInfo["ExaminationID"] = idExamination;
                IQueryable<InvigilatorAssignment> lsInvi = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, SearchInfo);
                if (lsInvi.Count() > 0)
                {
                    ViewData[SortExaminationRoomConstants.LIST_INVI] = lsInvi.ToList();
                }
                if (idExamination != null)
                {
                    if (Global.IsCurrentYear == true && ExaminationBusiness.Find(idExamination.Value).CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
                    {
                        ViewData[SortExaminationRoomConstants.BOOL_ADD] = false;
                    }
                }
                ViewData[SortExaminationRoomConstants.LIST_SUPERVISOR] = new List<SupervisorViewModel>();
                return View();
            }
        }


        /// <summary>
        /// SearchSupervisor
        /// </summary>
        /// <param name="frm">The FRM.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSupervisor(SearchSupervisor frm)
        {
            Utils.Utils.TrimObject(frm);
            //+ Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            //+ Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = frm.ExaminationID;
            if (frm.SchoolFacultyID != null)
            {
                SearchInfo["SchoolFacultyID"] = frm.SchoolFacultyID;
            }
            SearchInfo["EmployeeCode"] = frm.EmployeeCode;
            SearchInfo["EmployeeName"] = frm.EmployeeName;
            SearchInfo["AcademicYearID"] = Global.AcademicYearID;
            SearchInfo["AppliedLevel"] = Global.AppliedLevel;

            IEnumerable<SupervisorViewModel> lst = this._SearchSupervisor(SearchInfo);
            var orderedListSupervisor = lst.OrderBy(p => p.FullName.getLastWord()).ThenBy(p => p.FullName).ToList();
            ViewData[SortExaminationRoomConstants.LIST_SUPERVISOR] = orderedListSupervisor;
            ViewData[SortExaminationRoomConstants.BOOL_DELETE] = true;
            if (Global.IsCurrentYear == true && orderedListSupervisor.Count() > 0)
            {
                ViewData[SortExaminationRoomConstants.BOOL_DELETE] = false;
            }

            ViewData[SortExaminationRoomConstants.LIST_INVI] = new List<InvigilatorAssignment>();
            SearchInfo["AcademicYearID"] = Global.AcademicYearID;
            SearchInfo["AppliedLevel"] = Global.AppliedLevel;
            SearchInfo["ExaminationID"] = frm.ExaminationID;
            IQueryable<InvigilatorAssignment> lsInvi = InvigilatorAssignmentBusiness.SearchBySchool(Global.SchoolID.Value, SearchInfo);
            if (lsInvi.Count() > 0)
            {
                ViewData[SortExaminationRoomConstants.LIST_INVI] = lsInvi.ToList();
            }
            return PartialView("_ListSupervisor");

        }

        /// <summary>
        /// _SearchSupervisor
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IEnumerable{SupervisorViewModel}
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>
        private IEnumerable<SupervisorViewModel> _SearchSupervisor(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<Invigilator> query = this.InvigilatorBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            IQueryable<SupervisorViewModel> lst = query.Select(o => new SupervisorViewModel
            {
                Adress = o.Employee.PermanentResidentalAddress,
                BirthDate = o.Employee.BirthDate,
                EmployeeCode = o.Employee.EmployeeCode,
                EmployeeID = o.TeacherID,
                FacultyName = (o.Employee != null && o.Employee.SchoolFaculty != null) ? o.Employee.SchoolFaculty.FacultyName : string.Empty,
                FullName = o.Employee.FullName,
                SchoolFacultyID = o.Employee.SchoolFacultyID,
                Telephone = o.Employee.Telephone

            });
            return lst.ToList();

        }

        #region InserSupervisor
        /// <summary>
        /// PrepareCreateSupervisor
        /// </summary>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult PrepareCreateSupervisor(int? idExamination)
        {

            IDictionary<string, object> SearchExam = new Dictionary<string, object>();
            SearchExam["AcademicYearID"] = Global.AcademicYearID;
            SearchExam["AppliedLevel"] = Global.AppliedLevel;
            SupervisorViewModel spm = new SupervisorViewModel();
            spm.ExaminationID = idExamination.Value;
            spm.abc = true;
            ViewData[SortExaminationRoomConstants.CBO_FACULTY] = new List<SchoolFaculty>();
            IQueryable<SchoolFaculty> lsFaculty = SchoolFacultyBusiness.SearchBySchool(Global.SchoolID.Value, SearchExam);
            if (lsFaculty.Count() > 0)
            {
                var orderedListFacuty = lsFaculty.OrderBy(p => p.FacultyName).ToList();
                ViewData[SortExaminationRoomConstants.CBO_FACULTY] = orderedListFacuty;
            }
            AjaxLoadGridSupervisorCreate(0, idExamination.Value);

            return PartialView("_CreateSupervisor", spm);
        }

        /// <summary>
        /// CreateSupervisor
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns>
        /// ActionResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>
        /// 
        

        [ValidateAntiForgeryToken]
        public ActionResult CreateSupervisor(FormCollection fc)
        {

            var liststringid = fc["chkStatusSuper2"];
            string strExamination = fc["HideExaminationID"];
            int? ExaminationID = null;
            if (strExamination != "")
            {
                ExaminationID = int.Parse(strExamination);
            }
            if (liststringid != null)
            {

                string[] lstid = liststringid.Split(new Char[] { ',' });
                if (lstid.Count() > 0)
                {
                    List<int> ListEpID = new List<int>();
                    foreach (var item in lstid)
                    {
                        int p = int.Parse(item);
                        ListEpID.Add(p);
                    }
                    //Hệ thống xoá giám thị trong kỳ thi bằng cách gọi hàm InvigilatorBusiness.DeleteAll(UserInfo.SchoolID, UserInfo.AppliedLevel, ListInvigilatorID) 
                    // với ListInvigilatorID là danh sách các InvigilatorID tương ứng bản ghi được check trên giao diện
                    foreach (int ID in ListEpID)
                    {
                        if (InvigilatorBusiness.All.Where(o => o.TeacherID == ID && o.ExaminationID == ExaminationID).Count() > 0)
                        {
                            return Json(new JsonMessage(Res.Get("CreateSupervisor_Validate_Exist")));
                        }
                    }

                    InvigilatorBusiness.InsertAll(Global.SchoolID.Value, Global.AppliedLevel.Value, ExaminationID.Value, ListEpID);
                    InvigilatorBusiness.Save();
                }

            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_NotSelectSuper")));
            }
            SupervisorViewModel svm = new SupervisorViewModel();
            svm.ExaminationID = ExaminationID.Value;
            return Json(new JsonMessage(Res.Get("Common_Label_CreateSuccessMessage")));
        }
        #endregion

        #region Delete All

        /// <summary>
        /// DeleteSupervisor
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns>
        /// ActionResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>
        /// 
        

        [ValidateAntiForgeryToken]
        public ActionResult DeleteSupervisor(FormCollection fc)
        {

            var liststringid = fc["chkStatusSuper1"];
            int ExaminationID = int.Parse(fc["idExamination"]);
            if (liststringid != null)
            {
                string[] lstid = liststringid.Split(new Char[] { ',' });
                if (lstid.Count() > 0)
                {
                    List<int> ListInvigilatorID = new List<int>();
                    foreach (var item in lstid)
                    {
                        int p = int.Parse(item);
                        IQueryable<Invigilator> lsiv = InvigilatorBusiness.All.Where(o => o.TeacherID == p && o.ExaminationID == ExaminationID);
                        if (lsiv.Count() > 0)
                        {
                            foreach (var iv in lsiv.ToList())
                            {
                                ListInvigilatorID.Add(iv.InvigilatorID);
                            }
                        }
                    }
                    //Hệ thống xoá giám thị trong kỳ thi bằng cách gọi hàm InvigilatorBusiness.DeleteAll(UserInfo.SchoolID, UserInfo.AppliedLevel, ListInvigilatorID) 
                    // với ListInvigilatorID là danh sách các InvigilatorID tương ứng bản ghi được check trên giao diện
                    InvigilatorBusiness.DeleteAll(Global.SchoolID.Value, Global.AppliedLevel.Value, ListInvigilatorID);
                    InvigilatorBusiness.Save();
                }
                else
                {
                    throw new BusinessException("Validate_NotSelectInvigilator");
                }
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion


        #region AjaxLoadGridSupervisorCreate
        /// <summary>
        /// AjaxLoadGridSupervisorCreate
        /// </summary>
        /// <param name="idFaculty">The id faculty.</param>
        /// <param name="idExamination">The id examination.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>11/22/2012</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadGridSupervisorCreate(int? idFaculty, int idExamination)
        {
            if (idFaculty == null)
            {
                idFaculty = 0;
            }
            SupervisorViewModel spm = new SupervisorViewModel();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = Global.AcademicYearID;
            SearchInfo["AppliedLevel"] = Global.AppliedLevel;
            SearchInfo["ExaminationID"] = idExamination;

            List<Employee> lstEmployee = EmployeeBusiness.SearchWorkingTeacherByFaculty(Global.SchoolID.Value, idFaculty.Value).ToList();
            List<Invigilator> lstInvigilator = InvigilatorBusiness.SearchBySchool(Global.SchoolID.Value, SearchInfo).ToList();

            List<Employee> lsEmployee = lstEmployee.Where(u => !lstInvigilator.Any(v => v.TeacherID == u.EmployeeID)).ToList();

            List<SupervisorViewModel> lst = lsEmployee.Select(o => new SupervisorViewModel
            {
                Adress = o.PermanentResidentalAddress,
                BirthDate = o.BirthDate,
                EmployeeCode = o.EmployeeCode,
                EmployeeID = o.EmployeeID,
                FacultyName = o.SchoolFaculty != null ? o.SchoolFaculty.FacultyName : string.Empty,
                FullName = o.FullName,
                SchoolFacultyID = o.SchoolFacultyID,
                Telephone = o.Telephone
            })
            .ToList()
            .OrderBy(p=>SMAS.Business.Common.Utils.SortABC(p.FullName.getOrderingName()))
            .ThenBy(p=>SMAS.Business.Common.Utils.SortABC(p.FullName))
            .ToList();
            ViewData[SortExaminationRoomConstants.LIST_SUPERVISOR_CREATE] = lst;

            return PartialView("_GridCreateSupervisor", spm);
        }
        #endregion
        #endregion


        #region ExportExcel
        public FileResult ExportExcel(int? ExaminationID, int? EducationLevelID, int? ExaminationSubjectID, int? idRoom, string checkSubject)
        {
            if (idRoom == 0)
            {
                idRoom = null;
            }
            List<CandidateModel> lstCandidate = this.getListCandidateModel(idRoom, EducationLevelID, ExaminationID, ExaminationSubjectID, checkSubject);

            ReportDefinition reportDef = new ReportDefinition();
            string templatePath = "";
            string ReportName = "";
            string Subject = "Tất cả";
            Examination e = ExaminationBusiness.Find(ExaminationID);
            EducationLevel Edu = EducationLevelBusiness.Find(EducationLevelID);
            if (ExaminationSubjectID != null)
            {
                Subject = ExaminationSubjectBusiness.Find(ExaminationSubjectID).SubjectCat.DisplayName;
            }
            if (idRoom != null)
            {
                ExaminationRoom er = ExaminationRoomBusiness.Find(idRoom);
                SheetData SheetData = new SheetData("QLT_Phongthi_Thichatluonghocky2");
                SheetData.Data["AcademicYear"] = e.AcademicYear.DisplayTitle; ;
                SheetData.Data["SchoolName"] = e.SchoolProfile.SchoolName;
                SheetData.Data["ProvinceName"] = e.SchoolProfile.Province.ProvinceName;
                SheetData.Data["SupervisingDeptName"] = e.SchoolProfile.SupervisingDept.SupervisingDeptName;
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["EducationLevelName"] = Edu.Resolution;
                SheetData.Data["SubjectName"] = Subject;
                SheetData.Data["Examination"] = e.Title;
                SheetData.Data["RoomTitle"] = er.RoomTitle;
                reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.QLT_PHONGTHI_THICHATLUONGHOCKY2);
                templatePath = ReportUtils.GetTemplatePath(reportDef);
                ReportName = "QLT_Phong thi_" + ReportUtils.RemoveSpecialCharacters(e.Title) + ".xls";
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet Template = oBook.GetSheet(1);
                IVTWorksheet Sheet = oBook.GetSheet(2);
                Sheet.CopyPasteSameSize(Template.GetRange("A1", "G13"), 1, 1);
                Sheet.FillVariableValue(SheetData.Data);
                int StartRow = 11;
                int StartCol = 1;
                for (int i = 0; i < lstCandidate.Count; i++)
                {
                    CandidateModel ca = lstCandidate.ElementAt(i);
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, ca.NamedListCode);
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, ca.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 3, string.Format("{0:dd/MM/yyyy}", ca.BirthDate));
                    Sheet.SetCellValue(StartRow + i, StartCol + 4, ca.GenreName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, ca.ClassName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 6, ca.BirthPlace);
                    Sheet.CopyPaste(Sheet.GetRange("A" + StartRow.ToString(), "G" + StartRow.ToString()), StartRow + i, 1, true);
                }
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                oBook.GetSheet(1).Delete();
                result.FileDownloadName = ReportName;
                return result;
            }
            else
            {
                SheetData SheetData = new SheetData("QLT_Phongthi_Chuaxep_Thichatluonghocky2");
                SheetData.Data["AcademicYear"] = e.AcademicYear.DisplayTitle; ;
                SheetData.Data["SchoolName"] = e.SchoolProfile.SchoolName;
                SheetData.Data["ProvinceName"] = e.SchoolProfile.Province.ProvinceName;
                SheetData.Data["SupervisingDeptName"] = e.SchoolProfile.SupervisingDept.SupervisingDeptName;
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["EducationLevelName"] = Edu.Resolution;
                SheetData.Data["SubjectName"] = Subject;
                SheetData.Data["Examination"] = e.Title;
                SheetData.Data["RoomTitle"] = "Chưa Xếp";
                reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.QLT_PHONGTHI_CHUAXEP_THICHATLUONGHOCKY2);
                templatePath = ReportUtils.GetTemplatePath(reportDef);
                ReportName = "QLT_Phong thi_ChuaXep_" + ReportUtils.RemoveSpecialCharacters(e.Title) + ".xls";
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet Template = oBook.GetSheet(1);
                IVTWorksheet Sheet = oBook.GetSheet(2);
                Sheet.CopyPasteSameSize(Template.GetRange("A1", "G13"), 1, 1);
                Sheet.FillVariableValue(SheetData.Data);
                int StartRow = 11;
                int StartCol = 1;
                for (int i = 0; i < lstCandidate.Count; i++)
                {
                    CandidateModel ca = lstCandidate.ElementAt(i);
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, ca.NamedListCode);
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, ca.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 3, string.Format("{0:dd/MM/yyyy}", ca.BirthDate));
                    Sheet.SetCellValue(StartRow + i, StartCol + 4, ca.GenreName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, ca.ClassName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 6, ca.BirthPlace);
                    Sheet.CopyPaste(Sheet.GetRange("A" + StartRow.ToString(), "G" + StartRow.ToString()), StartRow + i, 1, true);
                }
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = ReportName;
                return result;
            }
        }
        public FileResult ExportExcelGV(SearchSupervisor SearchForm)
        {


            Utils.Utils.TrimObject(SearchForm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = SearchForm.ExaminationID;
            SearchInfo["SchoolFacultyID"] = SearchForm.SchoolFacultyID;
            SearchInfo["EmployeeCode"] = SearchForm.EmployeeCode;
            SearchInfo["EmployeeName"] = SearchForm.EmployeeName;

            IQueryable<Invigilator> lst = this.InvigilatorBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            ReportDefinition reportDef = new ReportDefinition();
            string templatePath = "";
            string ReportName = "";
            Examination e = ExaminationBusiness.Find(SearchForm.ExaminationID);
            SheetData SheetData = new SheetData("GV_THCS_DSGiamThi");
            SheetData.Data["AcademicYear"] = e.AcademicYear.DisplayTitle;
            SheetData.Data["SchoolName"] = e.SchoolProfile.SchoolName;
            SheetData.Data["ProvinceName"] = e.SchoolProfile.Province.ProvinceName;
            SheetData.Data["SupervisingDeptName"] = e.SchoolProfile.SupervisingDept.SupervisingDeptName;
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["Examination"] = e.Title;
            reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.GV_THCS_DSGIAMTHI);
            templatePath = ReportUtils.GetTemplatePath(reportDef);
            ReportName = "GV_THCS_DSGiamThi" + ReportUtils.RemoveSpecialCharacters(e.Title) + ".xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);

            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.CopyPasteSameSize(Template.GetRange("A1", "H15"), 1, 1);
            Sheet.FillVariableValue(SheetData.Data);
            if (lst.Count() > 0)
            {
                List<Invigilator> lstInvigilator = lst.OrderBy(o => o.Employee.Name).ToList();
                int StartRow = 11;
                int StartCol = 1;
                for (int i = 0; i < lstInvigilator.Count; i++)
                {
                    Invigilator iv = lstInvigilator[i];
                    int gt = 0;
                    if (iv.Employee.Genre == true)
                    {
                        gt = 1;
                    }
                    if ((i % 5 == 0) && i != 0)
                    {
                        Sheet.CopyPaste(Template.GetRange("A12", "H12"), StartRow + i - 1, 1, true);
                    }

                    if (i % 5 != 0)
                    {
                        Sheet.CopyPaste(Template.GetRange("A11", "H11"), StartRow + i - 1, 1, true);
                    }
                    string birthDate = "";
                    if (iv.Employee.BirthDate.HasValue)
                    {
                        birthDate = string.Format("{0:dd/MM/yyyy}", iv.Employee.BirthDate.Value);
                    }
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, iv.Employee.EmployeeCode);
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, iv.Employee.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 3, birthDate);
                    Sheet.SetCellValue(StartRow + i, StartCol + 4, Utils.CommonConvert.Genre(gt));
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, (iv.Employee != null && iv.Employee.SchoolFaculty != null) ? iv.Employee.SchoolFaculty.FacultyName : string.Empty);
                    Sheet.SetCellValue(StartRow + i, StartCol + 6, iv.Employee.Telephone);
                    Sheet.SetCellValue(StartRow + i, StartCol + 7, iv.Employee.PermanentResidentalAddress);// Dia chi thuong tru
                    //Sheet.SetCellValue(StartRow + i, StartCol + 8, iv.Employee.TempResidentalAddress); // Dia chi tam tru
                }

                Sheet.CopyPaste(Template.GetRange("A12", "H12"), StartRow + lstInvigilator.Count - 1, 1, true);

            }

            Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            oBook.GetSheet(1).Delete();
            result.FileDownloadName = ReportName;
            return result;

        }
        #endregion


    }
}





