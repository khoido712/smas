/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.SortExaminationRoomArea
{
    public class SortExaminationRoomConstants
    {
        public const string LIST_SORTEXAMINATIONROOM = "listSortExaminationRoom";
        public const string CBO_EXAMINATION = "CboExamination";
        public const string CBO_EDUCATIONLEVEL = "CboEducationLevel";
        public const string CBO_EXAMINATONSUBJECT = "CboExaminationSubject";
        public const string BOOL_SUBJECT = "BoolSubject";
        public const string LIST_EXAMINATIONROOM = "listExaminationRoom";
        public const string LIST_CANDIDATEROOM1 = "listCandidateRoom1";
        public const string LIST_CANDIDATEROOM2 = "listCandidateRoom2";
        public const string CBO_EXAMINATIONROOM = "CboExaminationRoom";
        public const string CBO_FACULTY = "CboFaculty";
        public const string LIST_SUPERVISOR = "listSupervisor";
        public const string LIST_INVI = "listInvi";
        public const string LIST_SUPERVISOR_CREATE = "listSupervisorCreate";
        public const string INDEX = "Examination";
        public const string BOOL_AUTOASSIGN = "BoolAutoAssign";
        public const string BOOL_BUTTON = "BoolButton";
        public const string BOOL_DELETE = "BoolDelete";
        public const string BOOL_ADD = "BoolAdd";
        public const string MOVE_RIGHT = "MoveRight";
        public const string MOVE_LEFT = "MoveLeft";
        public const string BUTTON_EXPORT = "ButtonExport";
    }
}