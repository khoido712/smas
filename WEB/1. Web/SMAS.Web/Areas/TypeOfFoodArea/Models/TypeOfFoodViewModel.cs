/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.TypeOfFoodArea.Models
{
    public class TypeOfFoodViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 TypeOfFoodID { get; set; }

        [ResourceDisplayName("TypeOfFood_Label_TypeOfFoodName")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]		
		public System.String TypeOfFoodName { get; set; }

        [ResourceDisplayName("TypeOfFood_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Description { get; set; }

        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }						
	       
    }
}


