﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.TypeOfFoodArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.TypeOfFoodArea.Controllers
{
    public class TypeOfFoodController : BaseController
    {        
        private readonly ITypeOfFoodBusiness TypeOfFoodBusiness;
        private readonly IFoodCatBusiness IFoodCatBusiness;
        public TypeOfFoodController(ITypeOfFoodBusiness typeoffoodBusiness, IFoodCatBusiness IFoodCatBusiness)
		{
			this.TypeOfFoodBusiness = typeoffoodBusiness;
            this.IFoodCatBusiness = IFoodCatBusiness;
		}
		
		//
        // GET: /TypeOfFood/       
        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here
            SearchInfo["IsActive"] = true;

            IEnumerable<TypeOfFoodViewModel> lst = this._Search(SearchInfo);
            ViewData[TypeOfFoodConstants.LIST_TYPEOFFOOD] = lst;
            return View();
        }

		//
        // GET: /TypeOfFood/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
            SearchInfo["TypeOfFoodName"] = frm.TypeOfFoodName;
            SearchInfo["IsActive"] = true;

            IEnumerable<TypeOfFoodViewModel> lst = this._Search(SearchInfo);
            ViewData[TypeOfFoodConstants.LIST_TYPEOFFOOD] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            TypeOfFood typeoffood = new TypeOfFood();
            TryUpdateModel(typeoffood); 
            Utils.Utils.TrimObject(typeoffood);
            typeoffood.ModifiedDate = null;
            typeoffood.IsActive = true;
            this.TypeOfFoodBusiness.Insert(typeoffood);
            this.TypeOfFoodBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int TypeOfFoodID)
        {
            TypeOfFood typeoffood = this.TypeOfFoodBusiness.Find(TypeOfFoodID);
            TryUpdateModel(typeoffood);
            Utils.Utils.TrimObject(typeoffood);
            typeoffood.ModifiedDate = DateTime.Now;
            this.TypeOfFoodBusiness.Update(typeoffood);
            this.TypeOfFoodBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.TypeOfFoodBusiness.Delete(id);
            this.TypeOfFoodBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<TypeOfFoodViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<TypeOfFood> query = this.TypeOfFoodBusiness.Search(SearchInfo);
            IQueryable<TypeOfFoodViewModel> lst = query.Select(o => new TypeOfFoodViewModel {               
						TypeOfFoodID = o.TypeOfFoodID,								
						TypeOfFoodName = o.TypeOfFoodName,								
						Description = o.Description							
					
				
            });
            IQueryable<FoodCat> listFoodCat = IFoodCatBusiness.SearchBySchool(-1, new Dictionary<string, object>() { { "IsActive", true } });
            List<TypeOfFoodViewModel> list =  new List<TypeOfFoodViewModel>();
            foreach (TypeOfFoodViewModel obj in lst)
            {
                var listContraint = listFoodCat.Where(o => o.TypeOfFoodID == obj.TypeOfFoodID);
                if (listContraint != null && listContraint.Count() > 0)
                {
                    obj.DeleteAble = false;
                }
                else
                {
                    obj.DeleteAble = true;
                }
                list.Add(obj);
            }
            return list.OrderBy(o => o.TypeOfFoodName);
        }        
    }
}





