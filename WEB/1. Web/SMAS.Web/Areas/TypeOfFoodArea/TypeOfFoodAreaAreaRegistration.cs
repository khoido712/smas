﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TypeOfFoodArea
{
    public class TypeOfFoodAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TypeOfFoodArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TypeOfFoodArea_default",
                "TypeOfFoodArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
