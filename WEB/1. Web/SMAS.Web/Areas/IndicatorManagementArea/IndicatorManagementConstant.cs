﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.IndicatorManagementArea
{
    public class IndicatorManagementConstant
    {
        public const string LIST_TITLE_TEMPLATE = "LIST_TITLE_TEMPLATE";
        public const string HTML_CONTENT_CODE_ID = "HTML_CONTENT_CODE_ID";
        public const string PERIOD_ID = "PeriodId";
        public const int PAGE_SIZE = 20;
        public const string LIST_INDICATOR = "LIST_INDICATOR";
        public const string TOTAL_RECORD = "TOTAL_RECORD";
    }
}