﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.IndicatorManagementArea.Models
{
    public class IndicatorManagementViewModel
    {
        public string CellAddress { get; set; }
        public DateTime CreateTime { get; set; }
        public string Formula { get; set; }
        public string FormulaValidate { get; set; }
        public int HtmlTemplateID { get; set; }
        public string IndicatorCode { get; set; }
        public int IndicatorID { get; set; }
        public bool IsSum { get; set; }
        public int? MaxLength { get; set; }
        public int? MaxValue { get; set; }
        public string Note { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}