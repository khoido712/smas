﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.IndicatorManagementArea
{
    public class IndicatorManagementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "IndicatorManagementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "IndicatorManagementArea_default",
                "IndicatorManagementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
