﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.CommuneArea.Models;
using Telerik.Web.Mvc;
using SMAS.Web.Areas.IndicatorManagementArea.Models;

namespace SMAS.Web.Areas.IndicatorManagementArea.Controllers
{
    public class IndicatorManagementController : BaseController
    {
        int totalRecord = 0;
        private IHtmlContentCodeBusiness HtmlContentCodeBusiness;
        private IHtmlTemplateBusiness HtmlTemplateBusiness;
        private IIndicatorBusiness IndicatorBusiness;
        public IndicatorManagementController(IHtmlContentCodeBusiness HtmlContentCodeBusiness, IHtmlTemplateBusiness HtmlTemplateBusiness
            , IIndicatorBusiness IndicatorBusiness)
        {
            this.HtmlContentCodeBusiness = HtmlContentCodeBusiness;
            this.HtmlTemplateBusiness = HtmlTemplateBusiness;
            this.IndicatorBusiness = IndicatorBusiness;
        }

        public ActionResult Index()
        {
            List<HtmlContentCode> listContent = HtmlContentCodeBusiness.GetListHtmlContentCode(SystemParamsInFile.REPORT_PHYSICAL_FACILITIES);
            int htmlContentCodeId = 0;
            if (listContent != null && listContent.Count > 0)
            {
                htmlContentCodeId = listContent.Select(p => p.HtmlContentCodeID).FirstOrDefault();
                //Lay danh sach indicator
                int totalRecord = 0;
                List<IndicatorManagementViewModel> listIndicator = this._Search(htmlContentCodeId,string.Empty, ref totalRecord);
                ViewData[IndicatorManagementConstant.LIST_TITLE_TEMPLATE] = listContent;
                ViewData[IndicatorManagementConstant.HTML_CONTENT_CODE_ID] = htmlContentCodeId;
                ViewData[IndicatorManagementConstant.TOTAL_RECORD] = totalRecord;
                ViewData[IndicatorManagementConstant.LIST_INDICATOR] = listIndicator;
            }
            return View();
        }

        //
        // GET: /Commune/Search

        
        public PartialViewResult Search(FormCollection fr)
        {
            int htmlContentCodeId = fr["IdIndicator"] != null ? Int32.Parse(fr["IdIndicator"]) : 0;
            string indicatorCode = fr["CodeIndicator"];
            List<IndicatorManagementViewModel> listResult = this._Search(htmlContentCodeId, indicatorCode, ref totalRecord);
            ViewData[IndicatorManagementConstant.TOTAL_RECORD] = totalRecord;
            ViewData[IndicatorManagementConstant.LIST_INDICATOR] = listResult;
            return PartialView("_GridIndicator");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>     
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(Indicator indica, FormCollection fr)
        {
            if (string.IsNullOrEmpty(indica.CellAddress))
            {
                return Json(new JsonMessage("Tên ô trong excel không được rỗng"));
            }
            if (string.IsNullOrEmpty(indica.IndicatorCode))
            {
                return Json(new JsonMessage("Mã tiêu chí không được rỗng"));
            }
            if (indica.MaxLength.HasValue)
            {
                if (indica.MaxLength.Value <= 0)
                {
                    return Json(new JsonMessage("Độ dài chuỗi nhập phải > 0"));
                }
            }
            if (indica.MaxValue.HasValue)
            {
                if (indica.MaxValue.Value <= 0)
                {
                    return Json(new JsonMessage("Giá trị lớn nhất cho phép nhập phải > 0"));
                }
            }
            int htmlContentCodeId = fr["txtContentCodeid"] != null ? Int32.Parse(fr["txtContentCodeid"]) : 0;
            HtmlTemplate htmlTemplatebj = HtmlTemplateBusiness.All.Where(p => p.HtmlContentCodeID == htmlContentCodeId).FirstOrDefault();
            Indicator indicator = new Indicator();
            indicator.CellAddress = indica.CellAddress;
            indicator.Formula = indica.Formula;
            indicator.FormulaValidate = indica.FormulaValidate;
            indicator.HtmlTemplateID = htmlTemplatebj.HtmlTemplateID;
            indicator.IndicatorCode = indica.IndicatorCode;
            indicator.IsSum = indica.IsSum;
            indicator.MaxLength = indica.MaxLength;
            indicator.MaxValue = indica.MaxValue;
            indicator.Note = indica.Note;
            indicator.CreateTime = DateTime.Now;

            IndicatorBusiness.Insert(indicator);
            IndicatorBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(Indicator indica)
        {

            if (string.IsNullOrEmpty(indica.CellAddress))
            {
                return Json(new JsonMessage("Tên ô trong excel không được rỗng"));
            }
            if (string.IsNullOrEmpty(indica.IndicatorCode))
            {
                return Json(new JsonMessage("Mã tiêu chí không được rỗng"));
            }
            if (indica.MaxLength.HasValue)
            {
                if (indica.MaxLength.Value <= 0)
                {
                    return Json(new JsonMessage("Độ dài chuỗi nhập phải > 0"));
                }
            }
            if (indica.MaxValue.HasValue)
            {
                if (indica.MaxValue.Value <= 0)
                {
                    return Json(new JsonMessage("Giá trị lớn nhất cho phép nhập phải > 0"));
                }
            }

            Indicator indic = IndicatorBusiness.Find(indica.IndicatorID);
            if (indic != null)
            {
                indic.UpdateTime = DateTime.Now;
                indic.CellAddress = indica.CellAddress;
                indic.Formula = indica.Formula;
                indic.FormulaValidate = indica.FormulaValidate;
                indic.HtmlTemplateID = indica.HtmlTemplateID;
                indic.IndicatorCode = indica.IndicatorCode;
                indic.IsSum = indica.IsSum;
                indic.MaxLength = indica.MaxLength;
                indic.MaxValue = indica.MaxValue;
                indic.Note = indica.Note;

                IndicatorBusiness.Update(indic);
                IndicatorBusiness.Save();
            }
            else
            {
                return Json(new JsonMessage("Mã tiêu chí không tồn tại"));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int indicatorId)
        {
            Indicator indic = IndicatorBusiness.Find(indicatorId);
            if (indic != null)
            {
                IndicatorBusiness.Delete(indic.IndicatorID);
                IndicatorBusiness.Save();
            }
            else
            {
                return Json(new JsonMessage("Mã tiêu chí không tồn tại"));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        [SkipCheckRole]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult _GridSelect(GridCommand grid, FormCollection fr)
        {
            int htmlContentCodeId = fr["IdIndicator"] != null ? Int32.Parse(fr["IdIndicator"]) : 0;
            string indicatorCode = fr["CodeIndicator"];
            List<IndicatorManagementViewModel> listResult = this._Search(htmlContentCodeId, indicatorCode, ref totalRecord, grid.Page);
            GridModel<IndicatorManagementViewModel> gm = new GridModel<IndicatorManagementViewModel>(listResult);
            gm.Total = totalRecord;
            return View(gm);
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<IndicatorManagementViewModel> _Search(int htmlContentCodeid,string indicatorCode, ref int totalRecord, int page = 1)
        {
            IQueryable<Indicator> query = from a in HtmlTemplateBusiness.All
                                          join b in IndicatorBusiness.All on a.HtmlTemplateID equals b.HtmlTemplateID
                                          where a.HtmlContentCodeID == htmlContentCodeid
                                          select b;
            if (!string.IsNullOrEmpty(indicatorCode))
            {
                query = query.Where(p => p.IndicatorCode.Contains(indicatorCode));
            }
            IQueryable<IndicatorManagementViewModel> lst = query.Select(o => new IndicatorManagementViewModel
            {
                CellAddress = o.CellAddress,
                CreateTime = o.CreateTime,
                Formula = o.Formula,
                FormulaValidate = o.FormulaValidate,
                HtmlTemplateID = o.HtmlTemplateID,
                IndicatorCode = o.IndicatorCode,
                IndicatorID = o.IndicatorID,
                IsSum = o.IsSum,
                MaxLength = o.MaxLength,
                MaxValue = o.MaxValue,
                Note = o.Note,
                UpdateTime = o.UpdateTime
            });
            totalRecord = query.Count();
            List<IndicatorManagementViewModel> lstResult = lst.OrderBy(c => c.HtmlTemplateID).Skip((page - 1) * IndicatorManagementConstant.PAGE_SIZE).Take(IndicatorManagementConstant.PAGE_SIZE).OrderBy(c => c.CellAddress).ThenBy(p=>p.IndicatorID).ToList();
            return lstResult;
        }
    }
}





