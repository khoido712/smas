﻿namespace SMAS.Web.Areas.SearchCandidateMarkArea
{
    public class SearchCandidateMarkConstants
    {
        public const string LISTEXAMINATION = "LISTEXAMINATION";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTEXAMINATIONSUBJECT = "LISTEXAMINATIONSUBJECT";
        public const string LISTEXAMINATIONROOM = "LISTEXAMINATIONROOM";
        public const string LISTCLASS = "LISTCLASS";
        public const string LISTSEARCHCANDIDATEMARK = "LISTSEARCHCANDIDATEMARK";
        public const string DEFAULT_EXAMINATION = "DEFAULT_EXAMINATION";
        public const string DEFAULT_EDUCATIONLEVEL = "DEFAULT_EDUCATIONLEVEL";
        public const string ENABEL_EXCEL = "ENABEL_EXCEL";
        public const string PAGING_ENABLE = "enablePaging";
    }
}