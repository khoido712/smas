﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SearchCandidateMarkArea
{
    public class SearchCandidateMarkAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SearchCandidateMarkArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SearchCandidateMarkArea_default",
                "SearchCandidateMarkArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}