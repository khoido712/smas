﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SearchCandidateMarkArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Examination_Label_TitleChoice")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SearchCandidateMarkConstants.LISTEXAMINATION)]
        [AdditionalMetadata("OnChange", "onExaminationChange(this)")]
        public System.Int32 ExaminationID { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_EducationLevelChoice")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SearchCandidateMarkConstants.LISTEDUCATIONLEVEL)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange(this)")]
        public int EducationLevel { get; set; }

        [ResourceDisplayName("Candidate_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("ViewDataKey", SearchCandidateMarkConstants.LISTEXAMINATIONSUBJECT)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [AdditionalMetadata("OnChange", "onExaminationSubjectChange(this)")]
        public int? ExaminationSubjectID { get; set; }

        [ResourceDisplayName("SearchCandidateMark_Label_ExaminationRoom")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", SearchCandidateMarkConstants.LISTEXAMINATIONROOM)]
        [AdditionalMetadata("OnChange", "onExaminationRoomChange(this)")]
        public int? ExaminationRoomID { get; set; }

        [ResourceDisplayName("Candidate_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SearchCandidateMarkConstants.LISTCLASS)]
        [AdditionalMetadata("OnChange", "onClassChange(this)")]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("SearchCandidateMark_Label_NamedListCode")]
        [MaxLength(16)]
        [StringLength(16)]
        public string NamedListCode { get; set; }

        [MaxLength(30)]
        [StringLength(30)]
        [ResourceDisplayName("Candidate_Label_PupilCode")]
        public string PupilCode { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        [ResourceDisplayName("Candidate_Label_FullName")]
        public string FullName { get; set; }
    }
}