﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SearchCandidateMarkArea.Models;
using SMAS.Web.Utils;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.SearchCandidateMarkArea.Controllers
{
    public class SearchCandidateMarkController : Controller
    {
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IViolatedCandidateBusiness ViolatedCandidateBusiness;
        private readonly IExamViolationTypeBusiness ExamViolationTypeBusiness;

        public SearchCandidateMarkController(IExaminationBusiness ExaminationBusiness
            , IExaminationSubjectBusiness ExaminationSubjectBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , ICandidateBusiness CandidateBusiness
            , IExaminationRoomBusiness ExaminationRoomBusiness
            , IProcessedReportBusiness ProcessedReportBusiness
            , IViolatedCandidateBusiness ViolatedCandidateBusiness
            , IExamViolationTypeBusiness ExamViolationTypeBusiness)
        {
            this.ExaminationBusiness = ExaminationBusiness;
            this.ExaminationSubjectBusiness = ExaminationSubjectBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.CandidateBusiness = CandidateBusiness;
            this.ExaminationRoomBusiness = ExaminationRoomBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ViolatedCandidateBusiness = ViolatedCandidateBusiness;
            this.ExamViolationTypeBusiness = ExamViolationTypeBusiness;
        }

        //
        // GET: /SearchCandidateMarkArea/SearchCandidateMark/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region search

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["ExaminationID"] = frm.ExaminationID;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["ExaminationSubjectID"] = frm.ExaminationSubjectID;
            SearchInfo["RoomID"] = frm.ExaminationRoomID.HasValue ? frm.ExaminationRoomID.Value : 0;
            SearchInfo["PupilName"] = frm.FullName;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["NamedListCode"] = frm.NamedListCode;

            //
            IEnumerable<SearchCandidateMarkViewModel> lst = this._Search(SearchInfo);
            ViewData[SearchCandidateMarkConstants.LISTSEARCHCANDIDATEMARK] = lst;
            ViewData[SearchCandidateMarkConstants.ENABEL_EXCEL] = lst.Count() > 0;
            if (frm.EducationLevel > 0 && frm.ClassID.HasValue == false)
            {

                ViewData[SearchCandidateMarkConstants.PAGING_ENABLE] = true;
            }
            else
            {
                ViewData[SearchCandidateMarkConstants.PAGING_ENABLE] = false;

            }
            //
            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<SearchCandidateMarkViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<CandidateMark> query = CandidateBusiness.SearchMark(global.SchoolID.Value, (int)global.AppliedLevel.Value, SearchInfo);

            IEnumerable<SearchCandidateMarkViewModel> lst = query.Select(o => new SearchCandidateMarkViewModel
            {
                CandidateID = o.CandidateID,
                ClassID = o.ClassID,
                PupilCode = o.PupilCode,
                FullName = o.PupilName,
                RoomID = o.RoomID,
                NamedListCode = o.NamedListCode,
                ExamMark = o.ExamMark,
                RealMark = o.RealMark,
                Description = o.Description,
                PupilID = o.PupilID,
                ShortName = o.Name
            });

            //List<SearchCandidateMarkViewModel> lsSCNVN = lst
            //    .OrderBy(o => o.NamedListNumber).ThenBy(o => o.FullName)
            //    .ToList();

            List<SearchCandidateMarkViewModel> lsSCNVN = lst
               .OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.ShortName +" "+ o.FullName))
                .ToList();
            int ExaminationID = (int)SearchInfo["ExaminationID"];
            // Lay danh sach lop hoc, phong thi va danh sach thi sinh vi pham
            List<int> listClassId = lsSCNVN.Where(o => o.ClassID.HasValue).Select(o => o.ClassID.Value).ToList();
            List<int> listRoomId = lsSCNVN.Where(o => o.RoomID.HasValue).Select(o => o.RoomID.Value).ToList();
            List<ClassProfile> listClassProfile = ClassProfileBusiness.SearchByAcademicYear(global.AcademicYearID.Value)
                .Where(o => listClassId.Contains(o.ClassProfileID)).ToList();
            List<ExaminationRoom> listExaminationRoom = ExaminationRoomBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                }).ToList();
            List<ViolatedCandidate> lsExamViolationExam = ViolatedCandidateBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                }).ToList();
            List<int> listViolatedTypeId = lsExamViolationExam.Where(o => o.ExamViolationTypeID.HasValue).Select(o => o.ExamViolationTypeID.Value).ToList();
            List<ExamViolationType> allExamViolationType = ExamViolationTypeBusiness.All.Where(o => listViolatedTypeId.Contains(o.ExamViolationTypeID)).ToList();

            foreach (var item in lsSCNVN)
            {
                if (item.RoomID.HasValue)
                {
                    ExaminationRoom er = listExaminationRoom.FirstOrDefault(o => o.ExaminationRoomID == item.RoomID.Value);
                    if (er != null)
                    {
                        item.RoomTitle = er.RoomTitle;
                    }
                }

                if (item.ClassID.HasValue)
                {
                    ClassProfile cp = listClassProfile.FirstOrDefault(o => o.ClassProfileID == item.ClassID.Value);
                    if (cp != null)
                    {
                        item.ClassName = cp.DisplayName;
                    }
                }

                //note:hình thức vi phạm quy chế thi, % điểm trừ của thí sinh nếu có
                //Exam,pupilID =>ViolatedCandidate->ExamViolationTypeID->Resolution:PenalizedMark,…
                var lsExamViolationType = lsExamViolationExam.Where(o => o.PupilID == item.PupilID && o.CandidateID == item.CandidateID).Select(o => o.ExamViolationTypeID).ToList();
                if (lsExamViolationType.Count() > 0)
                {
                    var listExamViolationTypeByPupil = allExamViolationType.Where(o => lsExamViolationType.Contains(o.ExamViolationTypeID)).ToList();
                    if (listExamViolationTypeByPupil.Count() > 0)
                    {
                        string note = listExamViolationTypeByPupil[0].Resolution + ":" + listExamViolationTypeByPupil[0].PenalizedMark + "%";
                        for (int i = 1; i < listExamViolationTypeByPupil.Count(); i++)
                        {
                            note += "," + listExamViolationTypeByPupil[i].Resolution + ":" + listExamViolationTypeByPupil[i].PenalizedMark + "%";
                        }
                        item.Note = note;
                    }
                    else
                    {
                        item.Note = "";
                    }
                }
                else
                {
                    item.Note = "";
                }
            }
            return lsSCNVN;
        }

        #endregion search

        #region setViewData

        private void SetViewData()
        {
            //fill du lieu combobox
            GlobalInfo global = new GlobalInfo();
            ViewData[SearchCandidateMarkConstants.PAGING_ENABLE] = true;
            List<Examination> lsE = ExaminationBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value}
            }).OrderByDescending(o => o.ToDate).ToList();

            int defaultExamination = lsE.Count > 0 ? lsE.FirstOrDefault().ExaminationID : 0;
            ViewData[SearchCandidateMarkConstants.LISTEXAMINATION] = new SelectList(lsE, "ExaminationID", "Title");
            ViewData[SearchCandidateMarkConstants.DEFAULT_EXAMINATION] = defaultExamination;

            List<EducationLevel> lsEL = new List<EducationLevel>();
            if (defaultExamination > 0)
            {
                lsEL = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"AppliedLevel",global.AppliedLevel.Value},
                    {"ExaminationID",defaultExamination}
                }).Select(o => o.EducationLevel).Distinct().OrderBy(o => o.EducationLevelID).ToList();
            }
            int defaultEL = lsEL.Count > 0 ? lsEL.FirstOrDefault().EducationLevelID : 0;
            ViewData[SearchCandidateMarkConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEL, "EducationLevelID", "Resolution");
            ViewData[SearchCandidateMarkConstants.DEFAULT_EDUCATIONLEVEL] = defaultEL;

            List<ExaminationSubjectObject> lsESO = new List<ExaminationSubjectObject>();
            List<ClassProfile> lsCP = new List<ClassProfile>();
            if (defaultExamination > 0 && defaultEL > 0)
            {
                lsESO = GetExaminationSubjectObject(defaultEL, defaultExamination).ToList();
                lsCP = GetClassFromEducationLevel(defaultEL, defaultExamination).ToList();
            }
            ViewData[SearchCandidateMarkConstants.LISTEXAMINATIONSUBJECT] = new SelectList(lsESO, "ExaminationSubjectID", "SubjectName");

            ViewData[SearchCandidateMarkConstants.LISTEXAMINATIONROOM] = new SelectList(new string[] { });

            ViewData[SearchCandidateMarkConstants.LISTCLASS] = new SelectList(lsCP, "ClassProfileID", "DisplayName");

            ViewData[SearchCandidateMarkConstants.ENABEL_EXCEL] = false;

        }

        #endregion setViewData

        #region ho tro

        private IQueryable<ExaminationSubjectObject> GetExaminationSubjectObject(int EducationLevelID, int ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<ExaminationSubject> lsES = GetExaminationSubject(ExaminationID, EducationLevelID);
            IQueryable<ExaminationSubjectObject> lsESO = lsES.Select(o => new ExaminationSubjectObject
            {
                DurationInMinute = o.DurationInMinute,
                EducationLevelID = o.EducationLevelID,
                ExaminationID = o.ExaminationID,
                ExaminationSubjectID = o.ExaminationSubjectID,
                HasDetachableHead = o.HasDetachableHead,
                IsLocked = o.IsLocked,
                OrderNumberPrefix = o.OrderNumberPrefix,
                StartTime = o.StartTime,
                SubjectID = o.SubjectID,
                SubjectName = o.SubjectCat.DisplayName,
                OrderInSubject = o.SubjectCat.OrderInSubject
            });
            return lsESO;
        }

        public IQueryable<ExaminationSubject> GetExaminationSubject(int? ExaminationID, int? EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            return ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"EducationLevelID",EducationLevelID},
                {"ExaminationID",ExaminationID}
            });
        }

        public IQueryable<ClassProfile> GetClassFromEducationLevel(int EducationLevelID, int ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();

            List<int?> lsClassID = CandidateBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                 {"AppliedLevel",global.AppliedLevel.Value},
                {"ExaminationID",ExaminationID},
                {"EducationLevelID",EducationLevelID}
            }).Select(o => o.ClassID).Distinct().ToList();

            return ClassProfileBusiness.All.Where(o => lsClassID.Contains(o.ClassProfileID) && o.IsActive.Value).OrderBy(o => o.DisplayName);
        }

        public IQueryable<ExaminationRoom> GetExaminationRoom(int ExaminationSubjectID, int? ExaminationID, int? EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            return ExaminationRoomBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"ExaminationSubjectID",ExaminationSubjectID}
                ,{"ExaminationID",ExaminationID},
                {"EducationLevelID",EducationLevelID}
            });
        }

        #endregion ho tro

        #region ajax load combobox

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID, int? ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            if (EducationLevelID.HasValue && ExaminationID.HasValue)
            {
                List<ClassProfile> lsCP = GetClassFromEducationLevel(EducationLevelID.Value, ExaminationID.Value).ToList();
                return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationSubject(int EducationLevelID, int ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            List<ExaminationSubjectObject> lsESO = GetExaminationSubjectObject(EducationLevelID, ExaminationID)
                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.SubjectName).ToList();
            return Json(new SelectList(lsESO, "ExaminationSubjectID", "SubjectName"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationRoom(int? ExaminationSubjectID, int? ExaminationID, int? EducationLevelID)
        {
            if (ExaminationSubjectID.HasValue)
            {
                IQueryable<ExaminationRoom> lsESO = GetExaminationRoom(ExaminationSubjectID.Value, ExaminationID, EducationLevelID).OrderBy(o => o.RoomTitle);
                return Json(new SelectList(lsESO, "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new string[] { }), JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadEducationLevel(int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            if (examinationID.HasValue && examinationID.Value > 0)
            {
                dicExaminationSubject.Add("ExaminationID", examinationID.Value);
            }
            else
            {
                return Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            var lstEdu = lstExamSubject.Select(u => new { u.EducationLevelID, u.EducationLevel.Resolution }).Distinct()
                .OrderBy(o => o.EducationLevelID).ToList();

            return Json(new SelectList(lstEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }

        #endregion ajax load combobox

        #region report

        
        public FileResult ExportExcel()
        {
            GlobalInfo global = new GlobalInfo();
            int ExaminationID = SMAS.Business.Common.Utils.GetInt(Request["ExaminationID"]);
            int EducationLevel = SMAS.Business.Common.Utils.GetInt(Request["EducationLevel"]);
            int ExaminationSubjectID = SMAS.Business.Common.Utils.GetInt(Request["ExaminationSubjectID"]);
            int ClassID = SMAS.Business.Common.Utils.GetInt(Request["ClassID"]);
            string FullName = SMAS.Business.Common.Utils.GetString(Request["FullName"]);
            string PupilCode = SMAS.Business.Common.Utils.GetString(Request["PupilCode"]);
            int ExaminationRoomID = SMAS.Business.Common.Utils.GetInt(Request["ExaminationRoomID"]);
            string NamedListCode = SMAS.Business.Common.Utils.GetString(Request["NamedListCode"]);
            CandidateBO cbo = new CandidateBO();
            cbo.AcademicYearID = global.AcademicYearID.Value;
            cbo.AppliedLevel = global.AppliedLevel.Value;
            cbo.ClassID = ClassID;
            cbo.EducationLevel = EducationLevel;
            cbo.ExaminationID = ExaminationID;
            cbo.ExaminationSubjectID = ExaminationSubjectID;
            cbo.FullName = FullName;
            cbo.ProvinceID = global.ProvinceID.Value;
            cbo.RoomID = ExaminationRoomID;
            cbo.PupilCode = PupilCode;
            cbo.SchoolID = global.SchoolID.Value;
            cbo.SupervisingDeptID = global.SupervisingDeptID.Value;
            cbo.NamedListCode = NamedListCode;
            string fileName = "";
            Stream excel = CandidateBusiness.CreateSearchCandidateMark(cbo, out fileName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;

            return result;
        }
        #endregion report
    }
}
