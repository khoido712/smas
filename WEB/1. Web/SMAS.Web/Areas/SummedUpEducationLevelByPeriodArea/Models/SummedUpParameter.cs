﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SummedUpEducationLevelByPeriodArea.Models
{
    public class SummedUpParameter
    {
        public int? SchoolID { get; set; }
        public int? AcademicYearID { get; set; }
        public int? UserAccountID { get; set; }
        public int[] HiddenClassID { get; set; }
        public int?[] Check { get; set; }
        public int? EducationLevelID { get; set; }
        public int? Semester { get; set; }
        public int? Period { get; set; }
    }
}