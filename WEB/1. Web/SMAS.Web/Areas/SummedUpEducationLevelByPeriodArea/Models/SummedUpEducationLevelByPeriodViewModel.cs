/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SummedUpEducationLevelByPeriodArea.Models
{
    public class SummedUpEducationLevelByPeriodViewModel
    {
        public int SummedUpRecordClassID { get; set; }
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int AcademicYearID { get; set; }
        [ResourceDisplayName("SummedUpEducationLevelByPeriod_Label_Status")]
        public string SummedUpStatus { get; set; }
        public int? Status { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int Semester { get; set; }
        public Nullable<int> PeriodID { get; set; }
        public int Type { get; set; }
        [ResourceDisplayName("SummedUpEducationLevelByPeriod_Label_ClassName")]
        public string ClassName { get; set; }
        [ResourceDisplayName("SummedUpEducationLevelByPeriod_Label_TotalPupilSummedUp")]
        public string TotalSummedPupil { get; set; }
        [ResourceDisplayName("SummedUpEducationLevelByPeriod_Label_LastSummedUp")]
        public DateTime? LastSummedUp { get; set; }
        public int? NumberOfPupil { get; set; }
        public bool enable_checkbox { get; set; }
        public bool tick_checkbox { get; set; }
        public string ModDate { get; set; }
        public int? EducationLevel2 { get; set; }
        public int? Semester2 { get; set; }
        public int? PeriodID2 { get; set; }
        public int? ClassOrderNumber { get; set; }
    }
}


