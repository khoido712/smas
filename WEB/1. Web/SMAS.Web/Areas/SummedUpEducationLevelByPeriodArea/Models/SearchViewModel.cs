/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SummedUpEducationLevelByPeriodArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]      
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SummedUpEducationLevelByPeriodConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("OnChange", "")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("ApprenticeshipClass_Label_Semester")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]      
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SummedUpEducationLevelByPeriodConstants.LISTSEMESTER)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "LoadPeriod(this, this.options[this.selectedIndex].value)")]
        public int? Semester { get; set; }

        [ResourceDisplayName("MarkRecord_Label_Period")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]      
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SummedUpEducationLevelByPeriodConstants.LISTPERIOD)]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("OnChange", "search()")]
        public int? Period { get; set; }
    }
}