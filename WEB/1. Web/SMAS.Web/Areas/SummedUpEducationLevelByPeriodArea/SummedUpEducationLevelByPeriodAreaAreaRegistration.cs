﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SummedUpEducationLevelByPeriodArea
{
    public class SummedUpEducationLevelByPeriodAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SummedUpEducationLevelByPeriodArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SummedUpEducationLevelByPeriodArea_default",
                "SummedUpEducationLevelByPeriodArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
