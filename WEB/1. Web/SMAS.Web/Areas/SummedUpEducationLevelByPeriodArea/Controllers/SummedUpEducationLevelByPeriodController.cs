﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.SummedUpEducationLevelByPeriodArea.Models;
using System.Transactions;
using SMAS.Business.BusinessObject;
using log4net;

namespace SMAS.Web.Areas.SummedUpEducationLevelByPeriodArea.Controllers
{
    public class SummedUpEducationLevelByPeriodController : ThreadController
    {
        private readonly ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        private static ILog iLog;

        private int Search_Index = 0;
        GlobalInfo global = new GlobalInfo();

        public SummedUpEducationLevelByPeriodController(IClassProfileBusiness ClassProfileBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness, ISummedUpRecordClassBusiness SummedUpRecordClassBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.SummedUpRecordClassBusiness = SummedUpRecordClassBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo global = new GlobalInfo();

            List<EducationLevel> lsEducationLevel = global.EducationLevels;

            if (lsEducationLevel != null)
                ViewData[SummedUpEducationLevelByPeriodConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            else
                ViewData[SummedUpEducationLevelByPeriodConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });

            ViewData[SummedUpEducationLevelByPeriodConstants.LISTSEMESTER] = new SelectList(CommonList.Semester(), "key", "value", global.Semester);

            IDictionary<string, object> PeriodSearchInfo = new Dictionary<string, object>();
            PeriodSearchInfo["AcademicYearID"] = global.AcademicYearID;
            PeriodSearchInfo["Semester"] = global.Semester;
            List<PeriodDeclaration> listPeriodDeclaration = PeriodDeclarationBusiness.SearchBySchool(global.SchoolID.Value, PeriodSearchInfo).ToList();
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            DateTime nowDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            int PeriodDeclarationIDSelected = listPeriodDeclaration.FirstOrDefault().PeriodDeclarationID;
            foreach (PeriodDeclaration item in listPeriodDeclaration)
            {
                PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                periodDeclaration = item;
                if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                {
                    periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + Res.Get("Common_Label_From") + " " + periodDeclaration.FromDate.Value.ToShortDateString() + " " + Res.Get("Common_Label_To") + " " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                }
                listPeriodDeclaration.Add(periodDeclaration);
                startDate = new DateTime(item.FromDate.Value.Year, item.FromDate.Value.Month, item.FromDate.Value.Day, 0, 0, 0);
                endDate = new DateTime(item.EndDate.Value.Year, item.EndDate.Value.Month, item.EndDate.Value.Day, 0, 0, 0);
                if (startDate <= nowDate && nowDate <= endDate)
                {
                    PeriodDeclarationIDSelected = item.PeriodDeclarationID;
                }
            }
            ViewData[SummedUpEducationLevelByPeriodConstants.LISTPERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", PeriodDeclarationIDSelected);

            ViewData[SummedUpEducationLevelByPeriodConstants.LISTSUMMEDUPRECORDBYEDUCATIONLEVEL] = null;
            ViewData[SummedUpEducationLevelByPeriodConstants.LIST_SUMMEDUP] = new List<SummedUpEducationLevelByPeriodViewModel>();
            ViewData[SummedUpEducationLevelByPeriodConstants.ENABLE_BUTTON_SUMMEDUP] = false;

            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadPeriod(int semester)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> PeriodSearchInfo = new Dictionary<string, object>();
            PeriodSearchInfo["AcademicYearID"] = global.AcademicYearID;
            PeriodSearchInfo["Semester"] = semester;
            IQueryable<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(global.SchoolID.Value, PeriodSearchInfo);
            List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
            if (lstPeriod != null && lstPeriod.Count() > 0)
            {
                foreach (PeriodDeclaration item in lstPeriod)
                {
                    PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                    periodDeclaration = item;
                    if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        periodDeclaration.Resolution = periodDeclaration.Resolution + " (từ " + periodDeclaration.FromDate.Value.ToShortDateString() + " đến " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                    listPeriodDeclaration.Add(periodDeclaration);
                }
            }
            ViewData[SummedUpEducationLevelByPeriodConstants.LISTPERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution");
            return Json(new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution"));
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSummedUpRecord(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            if (frm.EducationLevel != null && frm.Semester != null && frm.Period != null)
            {
                int count = 0;
                PeriodDeclaration per = PeriodDeclarationBusiness.Find(frm.Period);

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = global.AcademicYearID;
                SearchInfo["Semester"] = frm.Semester;
                SearchInfo["PeriodID"] = frm.Period;
                SearchInfo["EducationLevelID"] = frm.EducationLevel;
                SearchInfo["SchoolID"] = global.SchoolID;
                SearchInfo["Type"] = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1;
                List<SummedUpEducationLevelByPeriodViewModel> lstSummedUpEducationLevelByPeriodViewModel = SummedUpRecordClassBusiness
                                                                                                            .GetListClassByEducationLevel(SearchInfo)
                                                                                                            .Select(o => new SummedUpEducationLevelByPeriodViewModel
                                                                                                                                {
                                                                                                                                    ClassName = o.ClassName,
                                                                                                                                    ClassID = o.ClassID,
                                                                                                                                    ClassOrderNumber = o.ClassOrderNumber,
                                                                                                                                    Status = o.Status,
                                                                                                                                    NumberOfPupil = o.NumberOfPupil,
                                                                                                                                    LastSummedUp = o.ModifiedDate
                                                                                                                                })
                                                                                                            .OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName)
                                                                                                            .ToList();

                List<int> lstClassID = lstSummedUpEducationLevelByPeriodViewModel.Select(u => u.ClassID).Distinct().ToList();
                Dictionary<int, bool> dicHeadteacherPermission = UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, lstClassID);

                foreach (var item in lstSummedUpEducationLevelByPeriodViewModel)
                {
                    item.enable_checkbox = true;
                    item.tick_checkbox = false;
                    item.SummedUpStatus = CommonConvert.SummedUpRecordClass_Status(item.Status, SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1);

                    if (item.NumberOfPupil.HasValue)
                        item.TotalSummedPupil = item.NumberOfPupil.ToString() + "/" + PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { { "ClassID", item.ClassID }, { "Check", "Check" } }).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED).Count();

                    if (item.LastSummedUp.HasValue)
                        item.ModDate = String.Format("{0:dd/MM/yyyy HH:mm:ss}", item.LastSummedUp.Value);

                    item.enable_checkbox = global.IsCurrentYear && dicHeadteacherPermission[item.ClassID] && item.Status != SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING && (global.IsAdminSchoolRole || (per.IsLock != true && per.EndDate.Value.AddDays(1) > DateTime.Now));
                    item.tick_checkbox = dicHeadteacherPermission[item.ClassID] && (!item.Status.HasValue || item.Status == SystemParamsInFile.STATUS_SUR_CLASS_NOTCOMPLETE) && item.enable_checkbox;
                }

                ViewData[SummedUpEducationLevelByPeriodConstants.COUNT] = count.ToString();
                ViewData[SummedUpEducationLevelByPeriodConstants.ENABLE_BUTTON_SUMMEDUP] = global.IsCurrentYear && lstSummedUpEducationLevelByPeriodViewModel.Count > 0
                                                                                             && lstSummedUpEducationLevelByPeriodViewModel.Any(u => dicHeadteacherPermission[u.ClassID] && u.enable_checkbox)
                                                                                             && (global.IsAdminSchoolRole || per.EndDate.Value.AddDays(1) > DateTime.Now);

                ViewData[SummedUpEducationLevelByPeriodConstants.LIST_SUMMEDUP] = lstSummedUpEducationLevelByPeriodViewModel;
                Session["LIST_CLASS"] = lstSummedUpEducationLevelByPeriodViewModel;
            }
            else
            {
                ViewData[SummedUpEducationLevelByPeriodConstants.LIST_SUMMEDUP] = new List<SummedUpEducationLevelByPeriodViewModel>();
                ViewData[SummedUpEducationLevelByPeriodConstants.COUNT] = 0;
                ViewData[SummedUpEducationLevelByPeriodConstants.ENABLE_BUTTON_SUMMEDUP] = false;
            }
            return PartialView("_ListSummedUpRecord");
        }

        

        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult SummedUpRecord(FormCollection form, SummedUpEducationLevelByPeriodViewModel frm)
        {
            int? EducationLevelID = frm.EducationLevel2;
            int? Semester = frm.Semester2;
            int? PeriodID = frm.PeriodID2;
            List<SummedUpEducationLevelByPeriodViewModel> lst = (List<SummedUpEducationLevelByPeriodViewModel>)Session["LIST_CLASS"];
            List<int> lsCheckClassID = new List<int>();
            List<int> lsUnCheckClassID = new List<int>();
            List<ClassProfile> lsCheckClass = new List<ClassProfile>();
            List<ClassProfile> lsUnCheckClass = new List<ClassProfile>();
            for (int i = 0; i < lst.Count; i++)
            {
                String checkbox = (String)form["Check_" + lst[i].ClassID];
                if (checkbox != null && lst[i].enable_checkbox)
                {
                    if (checkbox.ToLower().Equals("on"))
                    {
                        ClassProfile cp = ClassProfileBusiness.Find(lst[i].ClassID);
                        lsCheckClass.Add(cp);
                    }
                }
                else if (lst[i].enable_checkbox)
                {
                    ClassProfile cp = ClassProfileBusiness.Find(lst[i].ClassID);
                    lsUnCheckClass.Add(cp);
                }
            }

            if (lsCheckClass.Count == 0)
                throw new BusinessException("SummedUpRecordClass_Validate_Class");

            string ThreadName = Web.Constants.GlobalConstants.Thread_SummedUpPeriod + global.SchoolID.ToString() + "_" + EducationLevelID.Value.ToString(); ;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["UserAccountID"] = global.UserAccountID;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["Semester"] = Semester;
            SearchInfo["PeriodID"] = PeriodID;
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["Type"] = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1;
            SearchInfo["lsCheckClass"] = lsCheckClass;
            SearchInfo["lsUnCheckClass"] = lsUnCheckClass;
            SearchInfo["ThreadName"] = ThreadName;

            this.StartThreadForLongAction(ThreadName, new LongAction(SummedUpThead), SearchInfo);
            return Json(new JsonMessage(Res.Get("SummedRedcord_Label_RankThread"), ThreadName));
        }

        public void SummedUpThead(IDictionary<string, object> dic)
        {
            ISummedUpRecordClassBusiness SummedUpRecordClassBusinessNew = new SMAS.Business.Business.SummedUpRecordClassBusiness(iLog, new SMASEntities());
            int UserAccountID = (int)dic["UserAccountID"];
            string ThreadName = (string)dic["ThreadName"];
            List<ClassProfile> lsUnCheckClass = dic["lsUnCheckClass"] as List<ClassProfile>;
            List<ClassProfile> lsCheckClass = dic["lsCheckClass"] as List<ClassProfile>;

            try
            {
                SummedUpRecordClassBusinessNew.InsertOrUpdateSummedUpRecordClass(UserAccountID, lsUnCheckClass, lsCheckClass, dic);
                SummedUpRecordClassBusinessNew.Save();
            }
            catch (Exception ex)
            {
                SummedUpRecordClassBusinessNew.UpdateStatusSummedUp(UserAccountID, lsUnCheckClass, lsCheckClass, dic);
                SummedUpRecordClassBusinessNew.Save();
                this.DeleteThread(ThreadName);
            }
        }
    }
}





