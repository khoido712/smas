/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.SummedUpEducationLevelByPeriodArea
{
    public class SummedUpEducationLevelByPeriodConstants
    {
        public const string LIST_SUMMEDUP = "lstSummedUp";
        public const string LISTSUMMEDUPRECORDBYEDUCATIONLEVEL = "LISTSUMMEDUPRECORDBYEDUCATIONLEVEL";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTSEMESTER = "LISTSEMESTER";
        public const string SEMESTER = "SEMESTER";
        public const string LISTPERIOD = "LISTPERIOD";
        public const string ENABLE_BUTTON_SUMMEDUP = "enable";
        public const string COUNT = "count";
    }
}