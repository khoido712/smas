﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.MarkRecordCareerArea.Models;
using SMAS.Web.Constants;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using SMAS.Web.Filter;



namespace SMAS.Web.Areas.MarkRecordCareerArea.Controllers
{
    public class MarkRecordCareerController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IApprenticeshipClassBusiness ApprenticeshipClassBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private GlobalInfo GlobalInfo = new GlobalInfo();
        public MarkRecordCareerController(IAcademicYearBusiness AcademicYearBusiness,
            IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness,
            ISummedUpRecordBusiness SummedUpRecordBusiness,
            IApprenticeshipClassBusiness ApprenticeshipClassBusiness,
            ISemeterDeclarationBusiness SemeterDeclarationBusiness,
            IMarkTypeBusiness MarkTypeBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
             IReportDefinitionBusiness reportdefinitionBusiness,
            IProcessedReportBusiness processedreportBusiness,
            IMarkRecordBusiness MarkRecordBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            IExemptedSubjectBusiness ExemptedSubjectBusiness,
            IClassProfileBusiness ClassProfileBusiness)
        {
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ApprenticeshipTrainingBusiness = ApprenticeshipTrainingBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.ApprenticeshipClassBusiness = ApprenticeshipClassBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.SemeterDeclarationBusiness = SemeterDeclarationBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.ProcessedReportBusiness = processedreportBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
        }

        //public int Mark_Type_M = 1;
        //public int Mark_Type_P = 2;
        //public int Mark_Type_V = 3;
        //public int Mark_Type_HK = 4;
        public int Mark_Type_M = 0;
        public int Mark_Type_P = 0;
        public int Mark_Type_V = 0;
        public int Mark_Type_HK = 0;
        public double Mark_G = 8.0;
        public double Mark_K = 6.5;
        public double Mark_TB = 5.0;



        public ActionResult Index()
        {
            // Thông tin học kỳ
            List<ComboObject> lstSemester = CommonList.Semester();
            List<ViettelCheckboxList> lstRadioSemester = new List<ViettelCheckboxList>();
            int i = 0;
            foreach (ComboObject item in lstSemester)
            {
                lstRadioSemester.Add(new ViettelCheckboxList(item.value, item.key, item.value.Equals(new GlobalInfo().Semester.Value.ToString()), false));
                i++;
            }

            ViewData[MarkRecordCareerConstants.LIST_SEMESTER] = lstRadioSemester;

            // Thông tin lớp
            List<ApprenticeshipClass> ListClass = new List<ApprenticeshipClass>();
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = GlobalInfo.AcademicYearID;
            Dictionary["Semester"] = GlobalInfo.Semester;

            ListClass = this.ApprenticeshipClassBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, Dictionary).ToList();
            ViewData[MarkRecordCareerConstants.LIST_CLASS] = ListClass;
            //ViewData[MarkRecordCareerConstants.LIST_COLUMN] = new List<GridColumn>();
            //ViewData[MarkRecordCareerConstants.SEMESTER] = 1;
            // Kiem tra xem truong khai bao con diem chua
            ViewData[MarkRecordCareerConstants.MESSAGE] = false;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            AcademicYear ay = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID);
            if (ay != null)
            {
                dic["Year"] = ay.Year;
            }
            dic["SchoolID"] = GlobalInfo.SchoolID;
            dic["Semester"] = GlobalInfo.Semester;
            SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).FirstOrDefault();
            if (SemeterDeclaration == null)
            {
                ViewData[MarkRecordCareerConstants.MESSAGE] = true;
            }
            return View();
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult GetListSubject(int? Semester)
        {
            // Thông tin lớp
            List<ApprenticeshipClass> ListClass = new List<ApprenticeshipClass>();
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = GlobalInfo.AcademicYearID;
            Dictionary["Semester"] = Semester;

            ListClass = this.ApprenticeshipClassBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, Dictionary).ToList();
            ViewData[MarkRecordCareerConstants.LIST_CLASS] = ListClass;
            return PartialView("_ListSubjectApprenticeShip");
        }

        public PartialViewResult Search(int? Semester, int? ClassID)
        {
            IEnumerable<MarkType> ListMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", GlobalInfo.AppliedLevel }, { "Active", true } }).ToList();
            List<MarkType> ListMarkType1 = ListMarkType.Where(o => o.Title != "HK" && o.Title != "LHK").OrderBy(u => u.Title).ToList(); // Danh sach cac con diem M, P, V
            List<MarkType> ListMarkType2 = ListMarkType.Where(o => o.Title == "HK").ToList(); // Con diem HK
            Mark_Type_M = ListMarkType.Where(o => o.Title == "M").SingleOrDefault().MarkTypeID;
            Mark_Type_P = ListMarkType.Where(o => o.Title == "P").SingleOrDefault().MarkTypeID;
            Mark_Type_V = ListMarkType.Where(o => o.Title == "V").SingleOrDefault().MarkTypeID;
            Mark_Type_HK = ListMarkType.Where(o => o.Title == "HK").SingleOrDefault().MarkTypeID;
            if (Semester == null || ClassID == null)
            {
                return new PartialViewResult();
            }
            ViewData[MarkRecordCareerConstants.LOCK_IMPORT] = "";
            GlobalInfo global = new GlobalInfo();
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime SecondEndDate = academicYear.SecondSemesterEndDate.Value;
            DateTime FirstEndDate = academicYear.FirstSemesterEndDate.Value;

            // Nếu account là quản trị trường hoặc giáo viên phụ trách lớp nghề thì cho phép nhập điểm
            // global.IsAdminSchoolRole = true ||
            int acc = global.UserAccountID;
            bool checkTeacherPermission = SMAS.Business.Common.UtilsBusiness.HasHeadTeacherApprenticeShipPermission(acc, ClassID.Value);
            if (!global.IsAdminSchoolRole || !checkTeacherPermission)
            {
                ViewData[MarkRecordCareerConstants.LOCK_IMPORT] = "disabled";
            }
            //if (DateTime.Now > SecondEndDate)
            //{
            //    ViewData[MarkRecordCareerConstants.LOCK_IMPORT] = "disabled";
            //}
            //else if (Semester == 1 && (DateTime.Now < academicYear.FirstSemesterStartDate || DateTime.Now > FirstEndDate))
            //{
            //    ViewData[MarkRecordCareerConstants.LOCK_IMPORT] = "disabled";
            //}
            //else if (Semester == 2 && DateTime.Now <= FirstEndDate)
            //{
            //    ViewData[MarkRecordCareerConstants.LOCK_IMPORT] = "disabled";
            //}

            ApprenticeshipSubject ApprenticeshipSubject = ApprenticeshipClassBusiness.Find(ClassID).ApprenticeshipSubject;
            int SubjectID = ApprenticeshipSubject.SubjectID;
            string SubjectName = ApprenticeshipSubject.SubjectName;
            string ClassName = ApprenticeshipClassBusiness.Find(ClassID).ClassName;
            string SemesterName = CommonList.Semester()[Semester.Value - 1].value;

            // Lay thoi gian hoc mon nghe
            ApprenticeshipClass apprenticeshipClass = ApprenticeshipClassBusiness.Find(ClassID);
            ViewData[MarkRecordCareerConstants.LEARN_SEMESTER] = apprenticeshipClass.Semester;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["ApprenticeshipClassID"] = ClassID;
            List<ApprenticeshipTraining> ListApprenticeshipTraining = ApprenticeshipTrainingBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            dic["Year"] = academicYear.Year;
            List<MarkRecord> ListMarkRecord = MarkRecordBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();
            List<SummedUpRecordBO> ListSummedUpRecord = new List<SummedUpRecordBO>();

            // Nếu chỉ học trong học kỳ 1
            if (apprenticeshipClass.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["Semester"] = Semester;
                ListSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordCareerOfClass(GlobalInfo.AcademicYearID.Value, GlobalInfo.SchoolID.Value, 0, SubjectID).ToList();
            }
            else
            {
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["Semester"] = Semester;
                ListSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordOfClass(GlobalInfo.AcademicYearID.Value, GlobalInfo.SchoolID.Value, Semester.Value, 0, null, SubjectID).ToList();
            }

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            AcademicYear ay = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID);
            if (ay != null)
            {
                dic["Year"] = ay.Year;
            }
            dic["SchoolID"] = GlobalInfo.SchoolID;
            dic["Semester"] = Semester;
            SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).FirstOrDefault();
            if (SemeterDeclaration == null)
            {
                throw new BusinessException(Res.Get("SemeterDeclaration_Null"));
            }

            // Tạo danh sách các tiêu đề cho các cột sinh động trong Grid
            List<GridColumn> ListColumn = new List<GridColumn>();
            GridColumn GridColumn = new GridColumn();
            foreach (MarkType mt in ListMarkType1)
            {
                GridColumn = new GridColumn(mt.Resolution, mt.Title, mt.MarkTypeID, 0);
                if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_M)
                {
                    for (int i = 1; i <= SemeterDeclaration.InterviewMark; i++)
                    {
                        GridColumn.SubColumns.Add(new GridColumn(SMAS.Web.Constants.GlobalConstants.MARK_TYPE_M + i, SMAS.Web.Constants.GlobalConstants.MARK_TYPE_M + i, mt.MarkTypeID, i));
                    }
                }
                else if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_P)
                {
                    for (int i = 1; i <= SemeterDeclaration.WritingMark; i++)
                    {
                        GridColumn.SubColumns.Add(new GridColumn(SMAS.Web.Constants.GlobalConstants.MARK_TYPE_P + i, SMAS.Web.Constants.GlobalConstants.MARK_TYPE_P + i, mt.MarkTypeID, i));
                    }
                }
                else if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_V)
                {
                    for (int i = 1; i <= SemeterDeclaration.TwiceCoeffiecientMark; i++)
                    {
                        GridColumn.SubColumns.Add(new GridColumn(SMAS.Web.Constants.GlobalConstants.MARK_TYPE_V + i, SMAS.Web.Constants.GlobalConstants.MARK_TYPE_V + i, mt.MarkTypeID, i));
                    }
                }

                ListColumn.Add(GridColumn);
            }
            foreach (MarkType mt in ListMarkType2)
            {
                GridColumn = new GridColumn(mt.Title, mt.Title, mt.MarkTypeID, 0);
                ListColumn.Add(GridColumn);
            }
            // Chuẩn hóa dữ liệu để hiển thị trên grid
            List<MarkRecordCareerViewModel> lst = new List<MarkRecordCareerViewModel>();
            foreach (ApprenticeshipTraining at in ListApprenticeshipTraining)
            {
                MarkRecordCareerViewModel MarkRecordCareerViewModel = new MarkRecordCareerViewModel();
                MarkRecordCareerViewModel.PupilID = at.PupilID;
                MarkRecordCareerViewModel.PupilCode = at.PupilProfile.PupilCode;
                MarkRecordCareerViewModel.FullName = at.PupilProfile.FullName;
                MarkRecordCareerViewModel.ClassID = at.ClassID.Value;

                List<MarkRecord> PupilMarkRecord = ListMarkRecord.Where(o => o.PupilID == at.PupilID).ToList();
                SummedUpRecordBO SummedUpRecordBO = null;
                // Neu la hoc ky 1 thi da lay dung diem cua hoc ky 1 trong cau truy van
                // Neu la hoc ky 2 thi lay ca diem ca nam nen phai loc theo semester de lay dung
                // diem cua hoc ky 2
                if (Semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    SummedUpRecordBO = ListSummedUpRecord.Where(o => o.PupilID == at.PupilID).FirstOrDefault();
                else
                    SummedUpRecordBO = ListSummedUpRecord.Where(o => o.PupilID == at.PupilID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();

                foreach (MarkRecord mr in PupilMarkRecord)
                {
                    MarkRecordCareerViewModel.MarkData[mr.Title] = mr.Mark;
                }

                if (SummedUpRecordBO != null)
                {
                    if (SummedUpRecordBO.IsCommenting == SMAS.Business.Common.SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                        SummedUpRecordBO.IsCommenting == SMAS.Business.Common.SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        if (SummedUpRecordBO.ReTestMark != null)
                        {
                            MarkRecordCareerViewModel.TBM = SummedUpRecordBO.ReTestMark;
                        }
                        else
                        {
                            MarkRecordCareerViewModel.TBM = SummedUpRecordBO.SummedUpMark;
                        }
                    }

                    MarkRecordCareerViewModel.Comment = SummedUpRecordBO.Comment;
                }
                // Neu la hoc ky 2 hoac lop chi hoc ky 1 thi lay them diem tong ket ca nam
                if (Semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || apprenticeshipClass.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL ||
                    apprenticeshipClass.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    SummedUpRecordBO yearAllSummedUpRecord = ListSummedUpRecord.Where(o => o.PupilID == at.PupilID)
                    .Where(o => o.Semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                    if (yearAllSummedUpRecord != null)
                    {
                        if (yearAllSummedUpRecord.IsCommenting == SMAS.Business.Common.SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                            yearAllSummedUpRecord.IsCommenting == SMAS.Business.Common.SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            if (yearAllSummedUpRecord.ReTestMark != null)
                            {
                                MarkRecordCareerViewModel.TBCN = yearAllSummedUpRecord.ReTestMark;

                                // Xep loai
                                if (yearAllSummedUpRecord.ReTestMark >= (decimal)Mark_G) // loai gioi
                                {
                                    MarkRecordCareerViewModel.XL = "G";
                                }
                                if (yearAllSummedUpRecord.ReTestMark >= (decimal)Mark_K && yearAllSummedUpRecord.ReTestMark < (decimal)Mark_G) // loai kha
                                {
                                    MarkRecordCareerViewModel.XL = "K";
                                }
                                if (yearAllSummedUpRecord.ReTestMark >= (decimal)Mark_TB && yearAllSummedUpRecord.ReTestMark < (decimal)Mark_K) // loai Trung binh
                                {
                                    MarkRecordCareerViewModel.XL = "TB";
                                }
                                if (yearAllSummedUpRecord.ReTestMark < (decimal)Mark_TB) // loai Ye
                                {
                                    MarkRecordCareerViewModel.XL = "Y";
                                }
                            }
                            else
                            {
                                MarkRecordCareerViewModel.TBCN = yearAllSummedUpRecord.SummedUpMark;
                                // Xep loai
                                if (yearAllSummedUpRecord.SummedUpMark >= (decimal)Mark_G) // loai gioi
                                {
                                    MarkRecordCareerViewModel.XL = "G";
                                }
                                if (yearAllSummedUpRecord.SummedUpMark >= (decimal)Mark_K && yearAllSummedUpRecord.SummedUpMark < (decimal)Mark_G) // loai kha
                                {
                                    MarkRecordCareerViewModel.XL = "K";
                                }
                                if (yearAllSummedUpRecord.SummedUpMark >= (decimal)Mark_TB && yearAllSummedUpRecord.SummedUpMark < (decimal)Mark_K) // loai Trung binh
                                {
                                    MarkRecordCareerViewModel.XL = "TB";
                                }
                                if (yearAllSummedUpRecord.SummedUpMark < (decimal)Mark_TB) // loai Ye
                                {
                                    MarkRecordCareerViewModel.XL = "Y";
                                }
                            }
                        }
                    }
                }

                // Kiem tra de hien thi xem co duoc nhap diem hay khong
                PupilProfile pupil = at.PupilProfile;
                if (pupil.ProfileStatus == SMAS.Business.Common.SystemParamsInFile.PUPIL_STATUS_STUDYING ||
                    pupil.CurrentSchoolID.Equals(GlobalInfo.SchoolID))
                {
                    MarkRecordCareerViewModel.Editable = true;
                }

                //if (MarkRecordCareerViewModel.Editable)
                //{
                //    bool IsAcepted = ExemptedSubjectBusiness.IsExemptedSubject(at.PupilID, at.ClassID.Value, at.AcademicYearID.Value, SubjectID, (int)Semester.Value);
                //    if (IsAcepted)
                //    {
                //        MarkRecordCareerViewModel.Editable = false;
                //    }
                //}

                lst.Add(MarkRecordCareerViewModel);
                // TODO: AuNH Các cột điểm đang bị khóa cũng không cho nhập (nếu UserInfo.IsAdminSchoolRole() = true thì cho phép nhập)
            }

            ViewData[MarkRecordCareerConstants.LIST_COLUMN] = ListColumn;
            ViewData[MarkRecordCareerConstants.LIST_MARK] = lst;
            ViewData[MarkRecordCareerConstants.TITLE] = string.Format(Res.Get("MarkRecordCareer_Label_Title"), SubjectName, ClassName, SemesterName);
            ViewData[MarkRecordCareerConstants.SEMESTER] = Semester.Value;
            return PartialView("_List");
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult Save(List<CellData> ListCellData, int? ClassID, int? Semester)
        {
            int SubjectID = ApprenticeshipClassBusiness.Find(ClassID).ApprenticeshipSubject.SubjectID;
            SubjectCat SubjectCat = SubjectCatBusiness.Find(SubjectID);
            AcademicYear Year = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID);

            List<MarkRecord> lstMarkRecord = new List<MarkRecord>();
            Dictionary<int, string> ListPupilID = new Dictionary<int, string>();
            Dictionary<int, int> ListPupilClassID = new Dictionary<int, int>();
            foreach (CellData CellData in ListCellData)
            {
                if (CellData.Value == null || !CellData.Checked)
                {
                    continue;
                }
                MarkRecord MarkRecord = new MarkRecord();
                MarkRecord.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                MarkRecord.SchoolID = GlobalInfo.SchoolID.Value;
                MarkRecord.ClassID = CellData.ClassID;
                MarkRecord.PupilID = CellData.PupilID;
                MarkRecord.SubjectID = SubjectID;
                MarkRecord.MarkTypeID = (int)CellData.MarkTypeID;
                MarkRecord.OrderNumber = (int)CellData.OrderNumber;
                MarkRecord.Title = CellData.MarkTitle;
                MarkRecord.CreatedAcademicYear = Year.Year;
                MarkRecord.Semester = Semester;
                MarkRecord.Mark = Utils.Utils.ParseDecimal(CellData.Value);
                MarkRecord.ModifiedDate = DateTime.Now;

                lstMarkRecord.Add(MarkRecord);
                if (!ListPupilID.ContainsKey(CellData.PupilID))
                {
                    ListPupilID[CellData.PupilID] = CellData.Comment;
                    ListPupilClassID[CellData.PupilID] = CellData.ClassID;
                }
            }

            List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>();
            foreach (int PupilID in ListPupilID.Keys)
            {
                SummedUpRecord SummedUpRecord = new SummedUpRecord();
                SummedUpRecord.PupilID = PupilID;
                SummedUpRecord.ClassID = ListPupilClassID[PupilID];
                SummedUpRecord.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                SummedUpRecord.SchoolID = GlobalInfo.SchoolID.Value;
                SummedUpRecord.SubjectID = SubjectID;
                SummedUpRecord.IsCommenting = SubjectCat.IsCommenting;
                SummedUpRecord.CreatedAcademicYear = Year.Year;
                SummedUpRecord.Semester = Semester;
                SummedUpRecord.Comment = (SubjectCat.IsCommenting == SMAS.Business.Common.SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? ListPupilID[PupilID] : null);
                lstSummedUpRecord.Add(SummedUpRecord);
            }

            if (ListPupilID.Count > 0)
            {
                MarkRecordBusiness.InsertMarkRecord(GlobalInfo.UserAccountID, lstMarkRecord, lstSummedUpRecord, Semester.Value, null, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, ClassID.Value, SubjectID, _globalInfo.EmployeeID);
                MarkRecordBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("MarkRecordCareer_NoSelectToSave"), JsonMessage.ERROR));
            }
        }
        

        [ValidateAntiForgeryToken]
        public JsonResult Delete(string[] check, int? ClassID, int? Semester)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["SubjectID"] = ApprenticeshipClassBusiness.Find(ClassID).ApprenticeshipSubject.SubjectID;
            dic["Semester"] = Semester;
            dic["Year"] = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID).Year;
            foreach (string PupilID in check)
            {
                string[] arr = PupilID.Split('_');
                dic["PupilID"] = int.Parse(arr[0]);
                dic["ClassID"] = int.Parse(arr[1]);

                List<MarkRecord> ListMarkRecord = MarkRecordBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();
                foreach (MarkRecord MarkRecord in ListMarkRecord)
                {
                    MarkRecordBusiness.DeleteMarkRecord(GlobalInfo.UserAccountID, MarkRecord.MarkRecordID, GlobalInfo.SchoolID.Value);
                }

                List<SummedUpRecord> ListSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();
                foreach (SummedUpRecord SummedUpRecord in ListSummedUpRecord)
                {
                    SummedUpRecordBusiness.DeleteSummedUpRecord(GlobalInfo.UserAccountID, SummedUpRecord.SummedUpRecordID, GlobalInfo.SchoolID.Value);
                }
            }
            MarkRecordBusiness.Save();
            SummedUpRecordBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #region Excel

        public FileResult DownloadReport(int? Semester, int? ClassID)
        {
            IDictionary<string, object> SearchExcel = new Dictionary<string, object>();
            SearchExcel["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchExcel["ApprenticeShipClassID"] = ClassID.Value;
            SearchExcel["AppliedLevel"] = new GlobalInfo().AppliedLevel.Value;
            SearchExcel["SchoolID"] = new GlobalInfo().SchoolID.Value;
            SearchExcel["Semester"] = Semester.Value;

            MarkRecord MarkRecord = new MarkRecord();
            MarkRecord.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            MarkRecord.SchoolID = new GlobalInfo().SchoolID.Value;
            MarkRecord.ClassID = ClassID.Value;

            string input = this.MarkRecordBusiness.GetHashKey(MarkRecord);

            int FileID = this.MarkRecordBusiness.CreatExcelMarkRecord(input, SearchExcel);

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(FileID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = SMAS.Business.Common.ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }



        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int SemesterImport, int ClassIDImport)
        {
            if (SemesterImport <= 0 || ClassIDImport <= 0)
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));

            // lấy dữ liệu từ excel
            string FilePath = (string)Session["FilePath"];
            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);

            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //string ClassName = ApprenticeshipClassBusiness.All.Where(o => o.ApprenticeshipClassID == ClassIDImport).FirstOrDefault().ClassName;
            string ClassName = ApprenticeshipClassBusiness.Find(ClassIDImport).ClassName;
            string SubjectName = ApprenticeshipClassBusiness.All.Where(o => o.ApprenticeshipClassID == ClassIDImport).FirstOrDefault().ApprenticeshipSubject.SubjectName;
            string SemesterName;
            if (SemesterImport == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";
            }
            else
            {
                SemesterName = "HỌC KỲ II";
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
            string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);

            string Title = (string)sheet.GetCellValue(4, 1);
            string TitleYear = (string)sheet.GetCellValue(5, 1);
            string Error = "";
            if (Title == null || TitleYear == null)
            {
                Error = Res.Get("Common_Label_ExcelExtensionError").ToUpper();
                return Json(new { success = Error });
            }
            if (Title.Contains(SubjectName.ToUpper()) == false)
            {
                Error = Res.Get("MarkRecord_Label_SubjectError") + " " + SubjectName;
            }
            if (Title.Contains(SemesterName.ToUpper()) == false)
            {
                if (Error == "")
                    Error = Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterName;
                else Error = Error + "</br>" + Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterName;
            }
            if (Title.Contains(ClassName.ToUpper()) == false)
            {
                if (Error == "")
                    Error = Res.Get("MarkRecord_Label_ClassError") + " " + ClassName;
                else Error = Error + "</br>" + Res.Get("MarkRecord_Label_ClassError") + " " + ClassName;
            }
            if (TitleYear.Contains(AcademicYearName.ToUpper()) == false && TitleYear.Contains(_AcademicYearName.ToUpper()) == false)
            {
                if (Error == "")
                    Error = Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                else Error = Error + "</br>" + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
            }
            if (Error != "")
            {
                return Json(new { success = Error });
            }

            ApprenticeshipSubject ApprenticeshipSubject = ApprenticeshipClassBusiness.Find(ClassIDImport).ApprenticeshipSubject;

            int SubjectID = ApprenticeshipSubject.SubjectID;

            IEnumerable<MarkType> ListMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", new GlobalInfo().AppliedLevel.Value }, { "Active", true } }).ToList();
            List<MarkType> ListMarkType1 = ListMarkType.Where(o => o.Title != "HK").OrderBy(u => u.Title).ToList(); // Danh sach cac con diem M, P, V
            List<MarkType> ListMarkType2 = ListMarkType.Where(o => o.Title == "HK").ToList(); // Con diem HK

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            if (academicYear != null) dic["Year"] = academicYear.Year;
            dic["SchoolID"] = GlobalInfo.SchoolID;
            dic["Semester"] = SemesterImport;

            SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).FirstOrDefault();

            List<MarkRecordCareerViewModel> lstMarkRecord = new List<MarkRecordCareerViewModel>();

            for (int i = 8; i < 200; i++)
            {
                if (sheet.GetCellValue(i, 3) == null)
                {
                    break;
                }
                MarkRecordCareerViewModel mrc = new MarkRecordCareerViewModel();
                string FullName = sheet.GetCellValue(i, 4) == null ? null : sheet.GetCellValue(i, 4).ToString();
                string PupilCode = sheet.GetCellValue(i, 3) == null ? null : sheet.GetCellValue(i, 3).ToString();
                //mrc.FullName = FullName;
                //mrc.PupilCode = PupilCode;

                int col = 5;
                foreach (var mt in ListMarkType1)
                {
                    if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_M)
                    {
                        for (int j = 1; j <= SemeterDeclaration.InterviewMark; j++)
                        {
                            mrc = new MarkRecordCareerViewModel();
                            mrc.FullName = FullName;
                            mrc.PupilCode = PupilCode;
                            mrc.Title = SMAS.Web.Constants.GlobalConstants.MARK_TYPE_M + j;
                            mrc.Mark = sheet.GetCellValue(i, col) == null ? null : sheet.GetCellValue(i, col).ToString();
                            mrc.MarkTypeID = mt.MarkTypeID;
                            mrc.OrderInNumber = j;
                            mrc.PASS = true;
                            col++;
                            lstMarkRecord.Add(mrc);
                        }

                    }

                    if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_P)
                    {
                        for (int j = 1; j <= SemeterDeclaration.WritingMark; j++)
                        {
                            mrc = new MarkRecordCareerViewModel();
                            mrc.FullName = FullName;
                            mrc.PupilCode = PupilCode;
                            mrc.Title = SMAS.Web.Constants.GlobalConstants.MARK_TYPE_P + j;
                            mrc.Mark = sheet.GetCellValue(i, col) == null ? null : sheet.GetCellValue(i, col).ToString();
                            mrc.MarkTypeID = mt.MarkTypeID;
                            mrc.OrderInNumber = j;
                            mrc.PASS = true;
                            col++;
                            lstMarkRecord.Add(mrc);
                        }
                    }

                    if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_V)
                    {
                        for (int j = 1; j <= SemeterDeclaration.TwiceCoeffiecientMark; j++)
                        {
                            mrc = new MarkRecordCareerViewModel();
                            mrc.FullName = FullName;
                            mrc.PupilCode = PupilCode;
                            mrc.Title = SMAS.Web.Constants.GlobalConstants.MARK_TYPE_V + j;
                            mrc.Mark = sheet.GetCellValue(i, col) == null ? null : sheet.GetCellValue(i, col).ToString();
                            mrc.MarkTypeID = mt.MarkTypeID;
                            mrc.OrderInNumber = j;
                            mrc.PASS = true;
                            col++;
                            lstMarkRecord.Add(mrc);
                        }
                    }
                }

                foreach (var mt in ListMarkType2)
                {
                    mrc = new MarkRecordCareerViewModel();
                    mrc.FullName = FullName;
                    mrc.PupilCode = PupilCode;
                    mrc.Title = "HK";
                    mrc.Mark = sheet.GetCellValue(i, col) == null ? null : sheet.GetCellValue(i, col).ToString();
                    mrc.MarkTypeID = mt.MarkTypeID;
                    mrc.PASS = true;
                    col++;
                    lstMarkRecord.Add(mrc);
                }
            }

            bool PASS = true;

            if (lstMarkRecord.Count == 0)
            {
                Error = "Dữ liệu đầu vào không hợp lệ";
                return Json(new { success = Error });
            }

            List<ApprenticeshipTraining> lstAppTraining = ApprenticeshipTrainingBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, new Dictionary<string, object> { { "ApprenticeShipClassID", ClassIDImport } }).ToList();

            foreach (var item in lstMarkRecord)
            {
                int m;
                decimal d;
                ApprenticeshipTraining appTraining = lstAppTraining.Where(u => u.PupilCode.Equals(item.PupilCode, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

                if (appTraining == null)
                {
                    item.PASS = false;
                    PASS = false;
                    item.Note = Res.Get("PupilProfile_Validate_NotExistOrStudying");
                }
                else
                {
                    item.PupilID = appTraining.PupilID;
                    item.PupilCode = appTraining.PupilCode;
                }

                if (appTraining != null && !appTraining.PupilProfile.FullName.Equals(item.FullName, StringComparison.InvariantCultureIgnoreCase))
                {
                    PASS = false;
                    item.PASS = false;
                }

                if (!string.IsNullOrEmpty(item.Mark))
                {
                    if (item.Title[0] == 'M')
                    {
                        if (!int.TryParse(item.Mark, out m))
                        {
                            PASS = false;
                            item.PASS = false;
                            item.Note = "Điểm thành phần ko đúng định dạng. ";
                        }
                        else
                        {
                            if (m < 0 || m > 10)
                            {
                                PASS = false;
                                item.PASS = false;
                                item.Note = "Điểm thành phần phải nằm trong khoang 0 > 10. ";
                            }
                            else
                            {
                                item.MarkValue = m;
                            }
                        }
                    }
                    else
                    {
                        if (!decimal.TryParse(item.Mark, out d))
                        {
                            PASS = false;
                            item.PASS = false;
                            item.Note = "Điểm thành phần ko đúng định dạng. ";
                        }
                        else
                        {
                            if (d < 0 || d > 10)
                            {
                                PASS = false;
                                item.PASS = false;
                                item.Note = "Điểm thành phần phải nằm trong khoang 0 > 10. ";
                            }
                            else
                            {
                                item.MarkValue = d;
                            }
                        }
                    }
                }
            }

            if (PASS == true)
            {
                //không có lỗi thì hệ thống thực hiện giống chức năng nhập điểm môn tính điểm
                Session["lstMarkRecord"] = lstMarkRecord;
                Session["Semester"] = SemesterImport;
                Session["ClassID"] = ClassIDImport;
                ImportMark();
                return Json(new { success = 1 });
            }
            else
            {
                Session["lstMarkRecord"] = lstMarkRecord;
                Session["Semester"] = SemesterImport;
                Session["ClassID"] = ClassIDImport;
                return Json(new { success = 0 });
                //Có lỗi thì hệ thống hiển thị màn hình Lựa chọn thao tác import
            }

        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportMark()
        {
            List<MarkRecordCareerViewModel> lstAppTest = (List<MarkRecordCareerViewModel>)Session["lstMarkRecord"];
            int Semester = (int)Session["Semester"];
            int ClassID = (int)Session["ClassID"];

            ApprenticeshipClass AppClass = ApprenticeshipClassBusiness.Find(ClassID);

            int SubjectID = AppClass.ApprenticeshipSubject.SubjectID;

            SubjectCat SubjectCat = SubjectCatBusiness.Find(SubjectID);
            AcademicYear Year = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID);

            List<MarkRecord> lstMarkRecord = new List<MarkRecord>();
            Dictionary<int, string> ListPupilID = new Dictionary<int, string>();
            Dictionary<int, int> ListPupilClassID = new Dictionary<int, int>();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            List<SummedUpRecordBO> ListSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordOfClass(GlobalInfo.AcademicYearID.Value, GlobalInfo.SchoolID.Value, Semester, 0, null, SubjectID).ToList();

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["ApprenticeshipClassID"] = ClassID;
            List<ApprenticeshipTraining> ListApprenticeshipTraining = ApprenticeshipTrainingBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();

            int ApprenticeshipTrainingClassID = ListApprenticeshipTraining.Where(o => o.ApprenticeShipClassID == ClassID).FirstOrDefault().ClassID.Value;
            foreach (var item in lstAppTest)
            {
                if (item.PASS && string.IsNullOrEmpty(item.Note) && item.MarkValue.HasValue)
                {
                    SummedUpRecordBO SummedUpRecordBO = ListSummedUpRecord.Where(o => o.PupilID == item.PupilID).FirstOrDefault();
                    MarkRecord MarkRecord = new MarkRecord();
                    MarkRecord.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                    MarkRecord.SchoolID = GlobalInfo.SchoolID.Value;
                    MarkRecord.ClassID = ApprenticeshipTrainingClassID;
                    MarkRecord.PupilID = item.PupilID;
                    MarkRecord.SubjectID = SubjectID;
                    MarkRecord.SubjectCat = SubjectCat;
                    MarkRecord.MarkTypeID = (int)item.MarkTypeID;
                    MarkRecord.Title = item.Title;
                    MarkRecord.OrderNumber = (int)item.OrderInNumber;
                    MarkRecord.CreatedAcademicYear = Year.Year;
                    MarkRecord.Semester = (int)Semester;
                    MarkRecord.ModifiedDate = DateTime.Now;
                    MarkRecord.Mark = item.MarkValue.Value;
                    lstMarkRecord.Add(MarkRecord);

                    if (!ListPupilID.ContainsKey(item.PupilID))
                    {
                        ListPupilID[item.PupilID] = SummedUpRecordBO != null ? SummedUpRecordBO.Comment : string.Empty;
                        ListPupilClassID[item.PupilID] = ClassID;
                    }
                }
            }

            List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>();
            foreach (int PupilID in ListPupilID.Keys)
            {
                SummedUpRecord SummedUpRecord = new SummedUpRecord();
                SummedUpRecord.PupilID = PupilID;
                SummedUpRecord.ClassID = ApprenticeshipTrainingClassID;
                SummedUpRecord.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                SummedUpRecord.SchoolID = GlobalInfo.SchoolID.Value;
                SummedUpRecord.SubjectID = SubjectID;
                SummedUpRecord.IsCommenting = SubjectCat.IsCommenting;
                SummedUpRecord.CreatedAcademicYear = Year.Year;
                SummedUpRecord.Semester = (int)Semester;
                SummedUpRecord.Comment = (SubjectCat.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? ListPupilID[PupilID] : null);
                lstSummedUpRecord.Add(SummedUpRecord);
            }

            if (ListPupilID.Count > 0)
            {
                MarkRecordBusiness.InsertMarkRecord(GlobalInfo.UserAccountID, lstMarkRecord, lstSummedUpRecord, Semester, null, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, ClassID, SubjectID, _globalInfo.EmployeeID);
                MarkRecordBusiness.Save();
                SummedUpRecordBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("MarkRecordCareer_NoSelectToSave"), JsonMessage.ERROR));
            }
        }

        public PartialViewResult ViewDataImportExcel()
        {
            List<MarkRecordCareerViewModel> lstAppTest = (List<MarkRecordCareerViewModel>)Session["lstMarkRecord"];
            int Semester = (int)Session["Semester"];
            int ClassID = (int)Session["ClassID"];

            List<string> lstPupilCode = (from s in lstAppTest select s.PupilCode).Distinct().ToList();

            ApprenticeshipSubject ApprenticeshipSubject = ApprenticeshipClassBusiness.Find(ClassID).ApprenticeshipSubject;
            int SubjectID = ApprenticeshipSubject.SubjectID;
            string SubjectName = ApprenticeshipSubject.SubjectName;
            string ClassName = ApprenticeshipClassBusiness.Find(ClassID).ClassName;
            string SemesterName = CommonList.Semester()[Semester - 1].value;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            List<SummedUpRecordBO> ListSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordOfClass(GlobalInfo.AcademicYearID.Value, GlobalInfo.SchoolID.Value, Semester, 0, null, SubjectID).ToList();

            IEnumerable<MarkType> ListMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", GlobalInfo.AppliedLevel }, { "Active", true } }).ToList();
            List<MarkType> ListMarkType1 = ListMarkType.Where(o => o.Title != "HK" && o.Title != "LHK").OrderBy(u => u.Title).ToList(); // Danh sach cac con diem M, P, V
            List<MarkType> ListMarkType2 = ListMarkType.Where(o => o.Title == "HK").ToList(); // Con diem HK

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            AcademicYear ay = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID);
            if (ay != null) dic["Year"] = ay.Year;
            dic["SchoolID"] = GlobalInfo.SchoolID;
            dic["Semester"] = Semester;
            SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).FirstOrDefault();

            // Tạo danh sách các tiêu đề cho các cột sinh động trong Grid
            List<GridColumn> ListColumn = new List<GridColumn>();
            GridColumn GridColumn = new GridColumn();
            foreach (MarkType mt in ListMarkType1)
            {
                GridColumn = new GridColumn(mt.Resolution, mt.Title, mt.MarkTypeID, 0);
                if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_M)
                {
                    for (int i = 1; i <= SemeterDeclaration.InterviewMark; i++)
                    {
                        GridColumn.SubColumns.Add(new GridColumn(SMAS.Web.Constants.GlobalConstants.MARK_TYPE_M + i, SMAS.Web.Constants.GlobalConstants.MARK_TYPE_M + i, mt.MarkTypeID, i));
                    }
                }

                if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_P)
                {
                    for (int i = 1; i <= SemeterDeclaration.WritingMark; i++)
                    {
                        GridColumn.SubColumns.Add(new GridColumn(SMAS.Web.Constants.GlobalConstants.MARK_TYPE_P + i, SMAS.Web.Constants.GlobalConstants.MARK_TYPE_P + i, mt.MarkTypeID, i));
                    }
                }

                if (mt.Title == SMAS.Web.Constants.GlobalConstants.MARK_TYPE_V)
                {
                    for (int i = 1; i <= SemeterDeclaration.TwiceCoeffiecientMark; i++)
                    {
                        GridColumn.SubColumns.Add(new GridColumn(SMAS.Web.Constants.GlobalConstants.MARK_TYPE_V + i, SMAS.Web.Constants.GlobalConstants.MARK_TYPE_V + i, mt.MarkTypeID, i));
                    }
                }
                ListColumn.Add(GridColumn);
            }
            foreach (MarkType mt in ListMarkType2)
            {
                GridColumn = new GridColumn(mt.Title, mt.Title, mt.MarkTypeID, 0);
                ListColumn.Add(GridColumn);
            }
            List<MarkRecordCareerViewModel> lst = new List<MarkRecordCareerViewModel>();
            foreach (var pc in lstPupilCode)
            {
                MarkRecordCareerViewModel MarkRecordCareerViewModel = new MarkRecordCareerViewModel();
                List<MarkRecordCareerViewModel> PupilMarkRecord = lstAppTest.Where(o => o.PupilCode == pc).ToList();
                MarkRecordCareerViewModel.PupilCode = pc;
                MarkRecordCareerViewModel.FullName = PupilMarkRecord.FirstOrDefault().FullName;
                MarkRecordCareerViewModel.PASS = !PupilMarkRecord.Any(u => !u.PASS);
                MarkRecordCareerViewModel.Note = string.Join(",", PupilMarkRecord.Where(u => !string.IsNullOrEmpty(u.Note)).Select(u => u.Note).Distinct());
                foreach (MarkRecordCareerViewModel mr in PupilMarkRecord)
                    MarkRecordCareerViewModel.MarkData[mr.Title] = mr.Mark;
                lst.Add(MarkRecordCareerViewModel);
            }

            ViewData[MarkRecordCareerConstants.LIST_COLUMN] = ListColumn;
            ViewData[MarkRecordCareerConstants.LIST_MARK] = lst;
            ViewData[MarkRecordCareerConstants.TITLE] = string.Format(Res.Get("MarkRecordCareer_Label_Title"), SubjectName, ClassName, SemesterName);
            ViewData[MarkRecordCareerConstants.SEMESTER] = Semester;
            return PartialView("ViewData");
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportDataPass()
        {
            ImportMark();
            return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage")));
        }


        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            GlobalInfo global = new GlobalInfo();

            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                return Json(new JsonMessage(""));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }


        #endregion

    }
}

