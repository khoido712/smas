﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordCareerArea
{
    public class MarkRecordCareerConstants
    {
        public const string LIST_CLASS = "ListClass";
        public const string LIST_SEMESTER = "ListSemester";
        public const string LIST_COLUMN = "ListColumn";
        public const string LIST_MARK = "ListMark";
        public const string TITLE = "title";
        public const int All_YEAR = 3;
        public const string SEMESTER = "semester";
        public const string LOCK_IMPORT = "lock_import";
        public const string LEARN_SEMESTER = "learn_semester";
        public const string MESSAGE = "message";
    }
}