﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordCareerArea.Models
{
    public class MarkRecordCareerViewModel
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int ClassID { get; set; }
        public Dictionary<string, object> MarkData { get; set; }
        public object TBM { get; set; }
        public object TBCN { get; set; }
        public object XL { get; set; }
        public string Comment { get; set; }
        public int MarkTypeID { get; set; }
        public int OrderInNumber { get; set; }
        public decimal? MarkValue { get; set; }

        public MarkRecordCareerViewModel()
        {
            MarkData = new Dictionary<string, object>();
        }

        public bool Editable { get; set; }

        public string Title { get; set; }
        public string Mark { get; set; }
        public string SummedUpMark { get; set; }

        public bool PASS { get; set; }
        public string Note { get; set; }
    }
}