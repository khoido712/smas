﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordCareerArea.Models
{
    public class GridColumn
    {
        public string Title { get; set; }
        public string Code { get; set; }
        public int MarkTypeID { get; set; }
        public int OrderNumber { get; set; }
        public List<GridColumn> SubColumns { get; set; }
        public GridColumn()
        {
            SubColumns = new List<GridColumn>();
        }
        public GridColumn(string Title, string Code, int MarkTypeID, int OrderNumber)
        {
            this.Title = Title;
            this.Code = Code;
            this.MarkTypeID = MarkTypeID;
            this.OrderNumber = OrderNumber;
            SubColumns = new List<GridColumn>();
        }

        public GridColumn(string Title, string Code, int MarkTypeID, int OrderNumber, List<GridColumn> SubColumns)
        {
            this.Title = Title;
            this.Code = Code;
            this.OrderNumber = OrderNumber;
            this.MarkTypeID = MarkTypeID;
            this.SubColumns = SubColumns;
        }
    }
}