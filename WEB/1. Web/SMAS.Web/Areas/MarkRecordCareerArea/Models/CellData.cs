﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordCareerArea.Models
{
    public class CellData
    {
        public int PupilID { get; set; }
        public string Value { get; set; }
        public int MarkTypeID { get; set; }
        public int OrderNumber { get; set; }
        public string MarkTitle { get; set; }
        public bool Checked { get; set; }
        public string Comment { get; set; }
        public int ClassID { get; set; }

        public CellData()
        {

        }
        
    }
}