﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkRecordCareerArea
{
    public class MarkRecordCareerAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkRecordCareerArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkRecordCareerArea_default",
                "MarkRecordCareerArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
