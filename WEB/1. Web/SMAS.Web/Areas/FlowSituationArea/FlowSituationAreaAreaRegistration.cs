﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.FlowSituationArea
{
    public class FlowSituationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FlowSituationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FlowSituationArea_default",
                "FlowSituationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
