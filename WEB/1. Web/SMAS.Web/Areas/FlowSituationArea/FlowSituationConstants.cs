﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.FlowSituationArea
{
    public class FlowSituationConstants
    {
        public const string LIST_SEMESTER = "listSemester";
        public const string DF_SEMESTER = "defaultSemester";
    }
}