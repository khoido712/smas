﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Web.Areas.FlowSituationArea.Controllers
{
    public class FlowSituationController : Controller
    {     
        private readonly IFlowSituationBusiness  FlowSituationBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
     
         public FlowSituationController(
                       IFlowSituationBusiness flowSituationBusiness,
                       IProcessedReportBusiness processedReportBusiness)
        {
            this.FlowSituationBusiness = flowSituationBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;          
        }
        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();
            ViewData[FlowSituationConstants.LIST_SEMESTER] = CommonList.SemesterAndAll();
            int defaultSemester = (Global.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_ALL : Global.Semester.GetValueOrDefault();
            ViewData[FlowSituationConstants.DF_SEMESTER] = defaultSemester;
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetClassMovement(FlowSituationBO flowSituationBO)
        {
            GlobalInfo global = new GlobalInfo();
            flowSituationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            flowSituationBO.SchoolID = global.SchoolID.Value;
            flowSituationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = FlowSituationBusiness.GetReportDefinitionOfClassMovement(flowSituationBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                 processedReport = FlowSituationBusiness.ExcelGetClassMovement(flowSituationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = FlowSituationBusiness.ExcelCreateClassMovement(flowSituationBO);
                processedReport = FlowSituationBusiness.ExcelInsertClassMovement(flowSituationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewClassMovement(FlowSituationBO flowSituationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            flowSituationBO.AcademicYearID = glo.AcademicYearID.Value;
            flowSituationBO.SchoolID = glo.SchoolID.Value;
            flowSituationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = FlowSituationBusiness.ExcelCreateClassMovement(flowSituationBO);
            ProcessedReport processedReport = FlowSituationBusiness.ExcelInsertClassMovement(flowSituationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetSchoolMovement(FlowSituationBO flowSituationBO)
        {
            GlobalInfo global = new GlobalInfo();
            flowSituationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            flowSituationBO.SchoolID = global.SchoolID.Value;
            flowSituationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = FlowSituationBusiness.GetReportDefinitionOfSchoolMovement(flowSituationBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                processedReport = FlowSituationBusiness.ExcelGetSchoolMovement(flowSituationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = FlowSituationBusiness.ExcelCreateSchoolMovement(flowSituationBO);
                processedReport = FlowSituationBusiness.ExcelInsertSchoolMovement(flowSituationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewSchoolMovement(FlowSituationBO flowSituationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            flowSituationBO.AcademicYearID = glo.AcademicYearID.Value;
            flowSituationBO.SchoolID = glo.SchoolID.Value;
            flowSituationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = FlowSituationBusiness.ExcelCreateSchoolMovement(flowSituationBO);
            ProcessedReport processedReport = FlowSituationBusiness.ExcelInsertSchoolMovement(flowSituationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetMovementAcceptance(FlowSituationBO flowSituationBO)
        {
            GlobalInfo global = new GlobalInfo();
            flowSituationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            flowSituationBO.SchoolID = global.SchoolID.Value;
            flowSituationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = FlowSituationBusiness.GetReportDefinitionOfSchoolMovement(flowSituationBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                processedReport = FlowSituationBusiness.ExcelGetMovementAcceptance(flowSituationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = FlowSituationBusiness.ExcelCreateMovementAcceptance(flowSituationBO);
                processedReport = FlowSituationBusiness.ExcelInsertMovementAcceptance(flowSituationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewMovementAcceptance(FlowSituationBO flowSituationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            flowSituationBO.AcademicYearID = glo.AcademicYearID.Value;
            flowSituationBO.SchoolID = glo.SchoolID.Value;
            flowSituationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = FlowSituationBusiness.ExcelCreateMovementAcceptance(flowSituationBO);
            ProcessedReport processedReport = FlowSituationBusiness.ExcelInsertMovementAcceptance(flowSituationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetPupilLeavingOff(FlowSituationBO flowSituationBO)
        {
            GlobalInfo global = new GlobalInfo();
            flowSituationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            flowSituationBO.SchoolID = global.SchoolID.Value;
            flowSituationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = FlowSituationBusiness.GetReportDefinitionOfPupilLeavingOff(flowSituationBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                processedReport = FlowSituationBusiness.ExcelGetPupilLeavingOff(flowSituationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = FlowSituationBusiness.ExcelCreatePupilLeavingOff(flowSituationBO);
                processedReport = FlowSituationBusiness.ExcelInsertPupilLeavingOff(flowSituationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewPupilLeavingOff(FlowSituationBO flowSituationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            flowSituationBO.AcademicYearID = glo.AcademicYearID.Value;
            flowSituationBO.SchoolID = glo.SchoolID.Value;
            flowSituationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = FlowSituationBusiness.ExcelCreatePupilLeavingOff(flowSituationBO);
            ProcessedReport processedReport = FlowSituationBusiness.ExcelInsertPupilLeavingOff(flowSituationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //Học sinh thuộc diện chính sách

        [ValidateAntiForgeryToken]
        public JsonResult GetPolicyTarget(FlowSituationBO flowSituationBO)
        {
            GlobalInfo global = new GlobalInfo();
            flowSituationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            flowSituationBO.SchoolID = global.SchoolID.Value;
            flowSituationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = FlowSituationBusiness.GetReportDefinitionOfPolicyTarget(flowSituationBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                processedReport = FlowSituationBusiness.ExcelGetPolicyTarget(flowSituationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = FlowSituationBusiness.ExcelCreatePolicyTarget(flowSituationBO);
                processedReport = FlowSituationBusiness.ExcelInsertPolicyTarget(flowSituationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewPolicyTarget(FlowSituationBO flowSituationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            flowSituationBO.AcademicYearID = glo.AcademicYearID.Value;
            flowSituationBO.SchoolID = glo.SchoolID.Value;
            flowSituationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = FlowSituationBusiness.ExcelCreatePolicyTarget(flowSituationBO);
            ProcessedReport processedReport = FlowSituationBusiness.ExcelInsertPolicyTarget(flowSituationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        
        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_LOP_A4,
                SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_TRUONG_A4,
                SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_DEN_A4,
                SystemParamsInFile.REPORT_HOC_SINH_THOI_HOC_A4,
                SystemParamsInFile.REPORT_HOC_SINH_DIEN_CHINH_SACH_A4
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, GlobalInfo.SchoolID.Value, GlobalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}
