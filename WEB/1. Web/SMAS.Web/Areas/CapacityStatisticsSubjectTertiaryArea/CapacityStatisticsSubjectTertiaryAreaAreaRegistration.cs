﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.CapacityStatisticsSubjectTertiaryArea
{
    public class CapacityStatisticsSubjectTertiaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CapacityStatisticsSubjectTertiaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CapacityStatisticsSubjectTertiaryArea_default",
                "CapacityStatisticsSubjectTertiaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
