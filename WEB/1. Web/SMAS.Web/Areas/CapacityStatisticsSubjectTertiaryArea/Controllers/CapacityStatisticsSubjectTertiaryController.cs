﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.CapacityStatisticsSubjectTertiaryArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
namespace SMAS.Web.Areas.CapacityStatisticsSubjectTertiaryArea.Controllers
{
    public class CapacityStatisticsSubjectTertiaryController : BaseController
    {
         private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ICapacityStatisticBusiness CapacityStatisticBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
                
        public CapacityStatisticsSubjectTertiaryController(IMarkStatisticBusiness markStatisticBusiness, IDistrictBusiness districtBusiness,
            ITrainingTypeBusiness trainingTypeBusiness, IEducationLevelBusiness educationLevelBusiness, ISubjectCatBusiness subjectCatBusiness,
            IAcademicYearBusiness academicYearBusiness, IReportDefinitionBusiness reportDefinitionBusiness, IProcessedReportBusiness processedReportBusiness,
            ICapacityStatisticBusiness capacityStatisticBusiness, ISubCommitteeBusiness subCommitteeBusiness)
        {
            this.MarkStatisticBusiness = markStatisticBusiness;
            this.DistrictBusiness = districtBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
        }

        public ActionResult Index()
        {
            IDictionary<string, object> DistrictsearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            if (global.IsSuperVisingDeptRole == true)
            {
                DistrictsearchInfo["ProvinceID"] = global.ProvinceID;
                IQueryable<District> lstDistrict = this.DistrictBusiness.Search(DistrictsearchInfo);
                ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName");
                ViewData[CapacityStatisticsSubjectTertiaryConstants.DisableDistrict] = false;
            }
            else
            {
                DistrictsearchInfo["DistrictID"] = global.DistrictID;
                IQueryable<District> lstDistrict = this.DistrictBusiness.Search(DistrictsearchInfo);
                ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName", global.DistrictID);
                ViewData[CapacityStatisticsSubjectTertiaryConstants.DisableDistrict] = true;
            }            

            List<int> lstAca = this.AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value).ToList();
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            for (int i = 0; i < lstAca.Count(); i++)
            {
                AcademicYear AcademicYear = new AcademicYear();
                AcademicYear.Year = (int)lstAca[i];
                AcademicYear.DisplayTitle = lstAca[i] + "-" + (lstAca[i] + 1);
                lstAcademicYear.Add(AcademicYear);
            }
            List<AcademicYear> ListAcademicYear = lstAcademicYear.OrderBy(o => o.Year).ToList();
            ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_AcademicYear] = new SelectList(ListAcademicYear, "Year", "DisplayTitle");

            ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_Semester] = new SelectList(CommonList.SemesterAndAll(), "key", "value");

            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(o => o.Grade == SystemParamsInFile.EDUCATION_GRADE_TERTIARY).ToList();
            ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_EducationLevel] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            IDictionary<string, object> TrainingTypesearchInfo = new Dictionary<string, object>();
            List<TrainingType> ListTrainingType = TrainingTypeBusiness.Search(TrainingTypesearchInfo).ToList();
            ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_TrainingType] = new SelectList(ListTrainingType, "TrainingTypeID", "Resolution");

            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();
            MarkStatisticsearchInfo["Year"] = ListAcademicYear.FirstOrDefault().Year;
            MarkStatisticsearchInfo["EducationLevelID"] = 0;
            MarkStatisticsearchInfo["Semester"] = 1;
            MarkStatisticsearchInfo["TrainingTypeID"] = 0;
            MarkStatisticsearchInfo["ReportCode"] = "ThongKeHocLucMonCap3";
            MarkStatisticsearchInfo["SentToSupervisor"] = true;
            MarkStatisticsearchInfo["SubCommitteeID"] = 0;
            if (global.IsSuperVisingDeptRole == true)
            {
                MarkStatisticsearchInfo["ProvinceID"] = global.ProvinceID;
            }
            else
            {
                MarkStatisticsearchInfo["SupervisingDeptID "] = global.SupervisingDeptID;
            }
            List<SubjectCatBO> ListSubject = CapacityStatisticBusiness.SearchSubjectHasReport(MarkStatisticsearchInfo).ToList();
            ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_Subject] = new SelectList(ListSubject, "SubjectCatID", "DisplayName");
            ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_MARKSTATISTICS] = null;
            IDictionary<string, object> subCommittee = new Dictionary<string, object>();
            List<SubCommittee> lstSubcommittee = SubCommitteeBusiness.Search(subCommittee).ToList();
            ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_Subcommittee] = new SelectList(lstSubcommittee, "SubCommitteeID", "Resolution");
            return View();
        }

        //Tổng hợp
        public ActionResult Search(SearchViewModel frm)
        {
            //string type = JsonReportMessage.NEW;
            //ProcessedReport processedReport = null;
            GlobalInfo global = new GlobalInfo();
            Utils.Utils.TrimObject(frm);
            //Nếu là account cấp Sở: hàm UserInfo.IsSupervisingDeptRole()trả về true            
            if (global.IsSuperVisingDeptRole == true)
            {
                MarkReportBO MarkReport = new MarkReportBO();
                MarkReport.Year = frm.AcademicYearID.Value;
                MarkReport.EducationLevelID = (frm.EducationLevelID == null) ? 0 : frm.EducationLevelID;
                MarkReport.Semester = frm.Semester.Value;
                MarkReport.TrainingTypeID = (frm.TrainingTypeID == null) ? 0 : frm.TrainingTypeID;
                MarkReport.SubcommitteeID = (frm.SubcommitteeID == null)? 0: frm.SubcommitteeID;
                MarkReport.SubjectID = frm.SubjectID;
                MarkReport.ProvinceID = global.ProvinceID;
                MarkReport.DistrictID = (frm.DistrictID == null) ? 0 : frm.DistrictID;

                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(MarkReport);
                string ReportCode = SystemParamsInFile.REPORT_SGD_THPT_ThongKeHLM;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = ProcessedReportID });
                }
             
            }
            
            return null;           
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? academicYearID, int? educationLevelID, int? trainingTypeID, int? semester, int? subCommitteeID)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<SubjectCatBO> lst = new List<SubjectCatBO>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = academicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["Semester"] = semester;
            dic["TrainingTypeID"] = trainingTypeID;
            dic["ReportCode"] = "ThongKeHocLucMonCap3";
            dic["SentToSupervisor"] = true;
            dic["SubCommitteeID"] = subCommitteeID;
            if (global.IsSuperVisingDeptRole == true)
            {
                dic["ProvinceID"] = global.ProvinceID;
            }
            //else
            //{
            //    dic["SupervisingDeptID "] = global.SupervisingDeptID;
            //}
            lst = CapacityStatisticBusiness.SearchSubjectHasReport(dic).ToList();
            if (lst == null)
                lst = new List<SubjectCatBO>();
            return Json(new SelectList(lst, "SubjectCatID", "DisplayName"));
        }
        public FileResult DownloadReport(int ProcessedReportID)
        {

            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SupervisingDeptID", GlobalInfo.SupervisingDeptID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_SGD_THPT_ThongKeHLM
              
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(ProcessedReportID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(SearchViewModel frm)
        {        
            GlobalInfo global = new GlobalInfo();
            Utils.Utils.TrimObject(frm);
            //Nếu là account cấp Sở: hàm UserInfo.IsSupervisingDeptRole()trả về true            
            if (global.IsSuperVisingDeptRole == true)
            {
                MarkReportBO MarkReport = new MarkReportBO();
                MarkReport.Year = frm.AcademicYearID.Value;
                MarkReport.EducationLevelID = (frm.EducationLevelID == null) ? 0 : frm.EducationLevelID;
                MarkReport.Semester = frm.Semester.Value;
                MarkReport.TrainingTypeID = (frm.TrainingTypeID == null) ? 0 : frm.TrainingTypeID;
                MarkReport.SubcommitteeID = (frm.SubcommitteeID == null) ? 0 : frm.SubcommitteeID;
                MarkReport.SubjectID = frm.SubjectID;
                MarkReport.ProvinceID = global.ProvinceID;
                MarkReport.DistrictID = (frm.DistrictID == null) ? 0 : frm.DistrictID;

                int FileID;
                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(MarkReport);
                List<CapacityStatisticsBO> lstCapacityStatisticsOfProvinceSecondary = new List<CapacityStatisticsBO>();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ReportCode"] = "ThongKeHocLucMonCap3";
                dic["Year"] = MarkReport.Year;
                dic["EducationLevelID"] = MarkReport.EducationLevelID;
                dic["Semester"] = MarkReport.Semester;
                dic["TrainingTypeID"] = MarkReport.TrainingTypeID;
                dic["SentToSupervisor"] = true;
                dic["SubCommitteeID"] = MarkReport.SubcommitteeID;
                dic["ProvinceID"] = MarkReport.ProvinceID;
                dic["DistrictID"] = MarkReport.DistrictID;
                dic["SubjectID"] = MarkReport.SubjectID;
                dic["SupervisingDeptID"] = global.SupervisingDeptID;
                dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;

                lstCapacityStatisticsOfProvinceSecondary = this.CapacityStatisticBusiness.CreateSGDCapacitySubjectStatisticsTertiary(dic, InputParameterHashKey,out FileID);

                IEnumerable<CapacityStatisticsSubjectViewModel> lst = lstCapacityStatisticsOfProvinceSecondary.Select(o => new CapacityStatisticsSubjectViewModel
                {
                    SchoolName = o.SchoolName,
                    SchoolID = o.SchoolID,
                    SentDate = o.SentDate,
                    TotalExcellent = o.TotalExcellent,
                    TotalGood = o.TotalGood,
                    TotalNormal = o.TotalNormal,
                    TotalWeak = o.TotalWeak,
                    TotalPoor = o.TotalPoor,
                    TotalPass = o.TotalPass,
                    TotalFail = o.TotalFail,
                    PercentExcellent =o.PercentExcellent,
                    PercentGood = o.PercentGood,
                    PercentNormal = o.PercentNormal,
                    PercentWeak = o.PercentWeak,
                    PercentPoor = o.PercentPoor,
                    PercentPass = o.PercentPass,
                    PercentFail = o.PercentFail,
                    TotalPupil = o.TotalPupil,
                    TotalAboveAverage = o.TotalAboveAverage,
                    PercentAboveAverage = o.PercentAboveAverage,
                });
                ViewData[CapacityStatisticsSubjectTertiaryConstants.LIST_MARKSTATISTICS] = lst.ToList();
                ViewData[CapacityStatisticsSubjectTertiaryConstants.GRID_REPORT_ID] = FileID;
            }
          
            return PartialView("List");
        }
    }
}
