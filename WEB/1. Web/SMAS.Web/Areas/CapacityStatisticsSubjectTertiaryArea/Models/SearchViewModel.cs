﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SMAS.Web.Areas.CapacityStatisticsSubjectTertiaryArea.Models
{
    public class SearchViewModel
    {

        [ResourceDisplayName("MarkStatistics_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectTertiaryConstants.LIST_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? DistrictID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectTertiaryConstants.LIST_AcademicYear)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? AcademicYearID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectTertiaryConstants.LIST_EducationLevel)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_TrainingType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectTertiaryConstants.LIST_TrainingType)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? TrainingTypeID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_Semmeter")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectTertiaryConstants.LIST_Semester)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? Semester { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectTertiaryConstants.LIST_Subject)]
        public int SubjectID { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_SubCommittee")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectTertiaryConstants.LIST_Subcommittee)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? SubcommitteeID { get; set; }
    }
}