﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SummedEvaluationArea
{
    public class SummedEvaluationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SummedEvaluationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SummedEvaluationArea_default",
                "SummedEvaluationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
