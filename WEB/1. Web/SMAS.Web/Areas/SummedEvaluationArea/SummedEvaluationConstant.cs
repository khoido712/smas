﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SummedEvaluationArea
{
    public class SummedEvaluationConstant
    {
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string CLASS_PROFILE = "clasProfile";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_SUMMED_EVALUATION = "listSummedEvaluation";
        public const string SEMESTER_ID = "SemesterID";
        public const string CHECK_CLASS_NULL = "CheckClassNull";
        public const string CHECK_SUBJECT_NULL = "CheckSubjectNull";
        public const string DISABLED_BUTTON = "DisabledButton";
        public const string FORMAT_DATETIME = "dd/MM/yyyy";
        public const string EVALUATION_RESULT_HT = "Hoàn thành";
        public const string EVALUATION_RESULT_CHT = "Chưa hoàn thành";
        public const string EVALUATION_RESULT_CDG = "Chưa đánh giá";
        public const string LIST_RATE_ADD = "ListRateAdd";
        public const string ENABLE_BTN_SAVE_RATEADD = "EnablebtnSaveRateAdd";
        public const int CHECK_RATE_HT = 1;
        public const int CHECK_RATE_CHT = 2;
        public const int CHECK_RATE_CDG = 3;
    }
}