﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SummedEvaluationArea.Models
{
    public class SummedEvaluationViewModel
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDay { get; set; }
        public int Status { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }
        public string Capacity { get; set; }//năng lực
        public string Quality { get; set; }//phẩm chất
        public string EvaluationResult { get; set; }//kết quả đánh giá
        public bool IsMove { get; set; }
        public int? RateAdd { get; set; }

        public int ClassID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationLevelID { get; set; }
        public List<SubjectViewModel> lstSubject { get; set; }
    }

    public class SubjectViewModel
    {
        public string SubjectName { get; set; }
        public int SubjectID { get; set; }
        public int? IsCommenting { get; set; }
        public bool IsExempted { get; set; }
        public string EqualComment { get; set; }
        public string Display { get; set; }
        public string EvaluationEnding { get; set; }
        public int? PeriodicEndingMark { get; set; }
        public bool isCheckRate { get; set; }
    }
}