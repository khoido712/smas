﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Utils;
using SMAS.Web.Areas.SummedEvaluationArea;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SummedEvaluationArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Filter;
using System.IO;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.SummedEvaluationArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class SummedEvaluationController : BaseController
    {
        //
        // GET: /SummedEvaluationArea/SummedEvaluation/

        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness; 
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public SummedEvaluationController(IClassProfileBusiness classProfileBusiness, ISummedEndingEvaluationBusiness summedEndingEvaluationBusiness,ISummedEvaluationBusiness summedEvaluationBusiness,
                                           IPupilOfClassBusiness pupilOfClassBusiness, IClassSubjectBusiness classSubjectBusiness, IExemptedSubjectBusiness exemptedSubjectBusiness,
                                           IAcademicYearBusiness academicYearBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SummedEndingEvaluationBusiness = summedEndingEvaluationBusiness;
            this.SummedEvaluationBusiness = summedEvaluationBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }
        public ActionResult Index(int? ClassID)
        {            
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            SetViewData(ClassID);
            return PartialView();
        }

        private void SetViewData(int? ClassID)
        {
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                    semester1 = true;
                else semester2 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[SummedEvaluationConstant.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();
            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }
            ViewData[SummedEvaluationConstant.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[SummedEvaluationConstant.LIST_CLASS] = listClass;
                if (listClass.Count() == 0)
	            {
                    ViewData[SummedEvaluationConstant.CHECK_CLASS_NULL] = true;
	            }
                else
                {
                    var listCP = listClass.ToList();

                    if (listCP != null && listCP.Count() > 0)
                    {
                        //Tính ra lớp cần được chọn đầu tiên
                        cp = listCP.First();
                        if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                        {
                            cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                        }
                        else
                        {
                            // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                            {
                                // Ưu tiên trước đối với giáo viên bộ môn
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                                List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                                .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                                {
                                    cp = listSubjectTeacher.First();
                                }
                                else
                                {
                                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                    List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                    if (listHead != null && listHead.Count > 0)
                                    {
                                        cp = listHead.First();
                                    }
                                }
                            }
                        }
                    }
                    ViewData[SummedEvaluationConstant.CLASS_PROFILE] = cp;
                }
            }
        }
        [HttpPost]
        public ActionResult AjaxLoadGrid(int ClassID,int SemesterID)
        {
            //Load danh sach mon hoc
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = ClassID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
            dicSubject["Semester"] = SemesterID;
            List<ClassSubjectBO> lstSubject = new List<ClassSubjectBO>();
            if (ClassID > 0 && SemesterID > 0)
            {
                iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
                lstSubject = iqSubject.Select(o => new ClassSubjectBO()
                {
                    ClassID = o.ClassID,
                    SubjectID = o.SubjectID,
                    OrderInSubject = o.SubjectCat.OrderInSubject,
                    DisplayName = o.SubjectCat.DisplayName,
                    SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                    SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                    IsCommenting = o.IsCommenting
                }).ToList().OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
                if (lstSubject.Count == 0)
                {
                    ViewData[SummedEvaluationConstant.CHECK_SUBJECT_NULL] = true;
                }
            }

            if (SemesterID == 2)
            {
                SetVisbileButton(SemesterID, ClassID);
            }
            ViewData[SummedEvaluationConstant.LIST_SUBJECT] = lstSubject;
            List<SummedEvaluationViewModel> lstSummedEvaluation = new List<SummedEvaluationViewModel>();
            lstSummedEvaluation = this.SearchData(ClassID, SemesterID, lstSubject);
            ViewData[SummedEvaluationConstant.LIST_SUMMED_EVALUATION] = lstSummedEvaluation;
            ViewData[SummedEvaluationConstant.SEMESTER_ID] = SemesterID;
            ViewData[SummedEvaluationConstant.LIST_RATE_ADD] = CommonList.RateAdd();

            return PartialView("_GridViewSummedEvaluation");
        }

        [ValidateAntiForgeryToken]
        public JsonResult EvaluationRate(FormCollection frm, string arrayID, int ClassID, int SemesterID, string arrayAddID)
        {
            try
            {
                List<SummedEndingEvaluation> lstSummedEnding = new List<SummedEndingEvaluation>();
                SummedEndingEvaluation objSummedEvaluation = null; ;

                string[] ArrayID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                List<string> IDStrList = ArrayID.ToList();
                List<long> idList = IDStrList.Select(long.Parse).ToList();

                string[] RateID = arrayAddID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                List<string> RateStrList = RateID.ToList();
                IDictionary<int, int> dicArrAddID = new Dictionary<int, int>();
                string tmp = string.Empty;
                List<string> lstRate = null;
                int rateID = 0;
                for (int i = 0; i < RateStrList.Count; i++)
                {
                    tmp = RateStrList[i];
                    lstRate = new List<string>();
                    lstRate = tmp.Split(new string[] {"_"},StringSplitOptions.RemoveEmptyEntries).ToList();
                    dicArrAddID.Add(int.Parse(lstRate[0]),int.TryParse(lstRate[1],out rateID) ? rateID : 0);
                }


                IQueryable<ClassSubject> iqSubject;
                Dictionary<string, object> dicSubject = new Dictionary<string, object>();
                dicSubject["ClassID"] = ClassID;
                dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicSubject["IsApprenticeShipSubject"] = false;
                dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
                dicSubject["Semester"] = SemesterID;
                iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
                List<ClassSubjectBO> lstSubject = iqSubject.Select(o => new ClassSubjectBO()
                {
                    ClassID = o.ClassID,
                    SubjectID = o.SubjectID,
                    OrderInSubject = o.SubjectCat.OrderInSubject,
                    DisplayName = o.SubjectCat.DisplayName,
                    SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                    SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                    IsCommenting = o.IsCommenting
                }).ToList().OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                List<SummedEvaluationViewModel> lstSummedEvaluation = new List<SummedEvaluationViewModel>();
                SummedEvaluationViewModel objViewModel = null;
                lstSummedEvaluation = this.SearchData(ClassID, SemesterID, lstSubject);
                // Lay danh sach hoc sinh mien giảm
                // ky 1
                List<ExemptedSubject> lstExemptedI = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
                // ky 2
                List<ExemptedSubject> lstExemptedII = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
                SubjectViewModel objSubjectView = null;
                int checkResult = 0;
                int countHT = 0;
                int countCHT = 0;
                for (int i = 0; i < lstSummedEvaluation.Count; i++)
                {
                    objViewModel = lstSummedEvaluation[i];
                    if (objViewModel.Status != 1 || !idList.Contains(objViewModel.PupilID))
                    {
                        continue;
                    }
                    #region Tinh diem danh gia
                    countHT = countCHT = 0;
                    for (int j = 0; j < objViewModel.lstSubject.Count; j++)
                    {
                        objSubjectView = objViewModel.lstSubject[j];
                        if ((SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                    ? lstExemptedI.Any(u => u.PupilID == objViewModel.PupilID && u.SubjectID == objSubjectView.SubjectID)
                                                    : lstExemptedII.Any(u => u.PupilID == objViewModel.PupilID && u.SubjectID == objSubjectView.SubjectID)))
                        {
                            continue;
                        }
                        checkResult = 0;
                        if (objSubjectView != null)
                        {
                            //mon tinh diem
                            if (objSubjectView.IsCommenting == 0)
                            {
                                if (!string.IsNullOrEmpty(objSubjectView.EvaluationEnding) && objSubjectView.PeriodicEndingMark.HasValue && !string.IsNullOrEmpty(objViewModel.Capacity) 
                                    && !string.IsNullOrEmpty(objViewModel.Quality))
                                {
                                    if ("HT".Equals(objSubjectView.EvaluationEnding) && objSubjectView.PeriodicEndingMark >= 5 && "Đ".Equals(objViewModel.Capacity) && "Đ".Equals(objViewModel.Quality))
                                    {
                                        countHT++;
                                    }
                                    else
                                    {
                                        countCHT++;
                                        //break;
                                    }
                                }
                                else
                                {
                                    checkResult = SummedEvaluationConstant.CHECK_RATE_CDG;
                                    break;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objSubjectView.EvaluationEnding) && !string.IsNullOrEmpty(objViewModel.Capacity)
                                    && !string.IsNullOrEmpty(objViewModel.Quality))
                                {
                                    if ("HT".Equals(objSubjectView.EvaluationEnding) && "Đ".Equals(objViewModel.Capacity) && "Đ".Equals(objViewModel.Quality))
                                    {
                                        countHT++;
                                    }
                                    else
                                    {
                                        countCHT++;
                                    }
                                }
                                else
                                {
                                    checkResult = SummedEvaluationConstant.CHECK_RATE_CDG;
                                    break;
                                }
                            }
                        }
                    }
                    #endregion
                    objSummedEvaluation = new SummedEndingEvaluation();
                    objSummedEvaluation.SummedEndingEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<int>();
                    objSummedEvaluation.PupilID = objViewModel.PupilID;
                    objSummedEvaluation.SemesterID = GlobalConstants.SEMESTER_OF_EVALUATION_RESULT;
                    objSummedEvaluation.EvaluationID = GlobalConstants.EVALUATION_RESULT;
                    objSummedEvaluation.SchoolID = _globalInfo.SchoolID.Value;
                    objSummedEvaluation.LastDigitSchoolID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                    objSummedEvaluation.ClassID = ClassID;
                    objSummedEvaluation.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objSummedEvaluation.EndingEvaluation = checkResult != SummedEvaluationConstant.CHECK_RATE_CDG ? (countCHT > 0 ? "CHT" :"HT") : "";
                    objSummedEvaluation.CreateTime = DateTime.Now;
                    objSummedEvaluation.RateAdd = "HT".Equals(objSummedEvaluation.EndingEvaluation) ? 0 : dicArrAddID[objViewModel.PupilID];
                    lstSummedEnding.Add(objSummedEvaluation);
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",ClassID}
                };
                //gọi hàm insert or update summedEvaluationEnding
                SummedEndingEvaluationBusiness.InsertOrUpdate(dic, lstSummedEnding);
                return Json(new JsonMessage(Res.Get("Đánh giá thành công")));
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("arrayID={0}, ClassID={1}, SemesterID={2}, arrayAddID={3}", arrayID, ClassID, SemesterID, arrayAddID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "EvaluationRate", paramList, ex);
                return Json(new JsonMessage(Res.Get("Có lỗi trong quá trình tổng hợp dữ liệu.Thầy/cô vui lòng thử lại.")));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult DeleteEvaluationResult(FormCollection frm)
        {
            try
            {
                int classID = int.Parse(frm["HfClassID"]);
                List<long> lstpupilID = new List<long>();
                // Lay danh sach hoc sinh
                List<PupilOfClassBO> listPupil = PupilOfClassBusiness.GetPupilInClass(classID)
                                                                    .OrderBy(u => u.OrderInClass)
                                                                    .ThenBy(u => u.Name)
                                                                    .ThenBy(u => u.PupilFullName)
                                                                    .ToList();
                PupilOfClassBO objPupil = null;

                for (int i = 0; i < listPupil.Count; i++)
                {
                    objPupil = listPupil[i];
                    if ("on".Equals(frm["Chk_" + objPupil.PupilID]))
                    {
                        lstpupilID.Add(objPupil.PupilID);
                    }
                }
                IDictionary<string,object>dic = new Dictionary<string,object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };
                SummedEndingEvaluationBusiness.DeleteSummedEvaluation(dic, lstpupilID);
                return Json(new JsonMessage(Res.Get("Xóa đánh giá thành công")));
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteEvaluationResult", "FormCollection", ex);
                return Json(new JsonMessage(Res.Get("Có lỗi trong quá trình xóa dữ liệu.Thầy/cô vui lòng thử lại.")));
            }
        }

        public FileResult ExportExcel(int classID, int semesterID)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            string templatePath = string.Empty;
            string OutPutName = string.Empty;
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", "BangTongHopDanhGiaLopK1_1Cuoinam.xls");
            
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //Lay danh sach mon hoc
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = classID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
            dicSubject["Semester"] = semesterID;
            iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
            List<ClassSubjectBO> lstSubject = iqSubject.Select(o => new ClassSubjectBO()
            {
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                DisplayName = o.SubjectCat.DisplayName,
                SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                IsCommenting = o.IsCommenting
            }).ToList().OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();

            List<SummedEvaluationViewModel> lstSummedEvaluation = new List<SummedEvaluationViewModel>();
            lstSummedEvaluation = this.SearchData(classID, semesterID, lstSubject);
            //lstSummedEvaluation = lstSummedEvaluation.OrderBy(p => p.lstSubject.OrderByDescending(s => s.IsCommenting)).ToList();

            OutPutName = SetCellValueExcel(sheet, lstSummedEvaluation, classID, semesterID);
            //sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = OutPutName;
            return result;
        }

        public FileResult ExportRateAdd(int classID, int semesterID)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            string templatePath = string.Empty;
            string OutPutName = string.Empty;

            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", "HS_TH_DSDanhGiaBoSung_1D.xls");

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //Lay danh sach mon hoc
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = classID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
            dicSubject["Semester"] = semesterID;
            iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
            List<ClassSubjectBO> lstSubject = iqSubject.Select(o => new ClassSubjectBO()
            {
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                DisplayName = o.SubjectCat.DisplayName,
                SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                IsCommenting = o.IsCommenting
            }).ToList().OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();

            List<SummedEvaluationViewModel> lstSummedEvaluation = new List<SummedEvaluationViewModel>();
            lstSummedEvaluation = this.SearchData(classID, semesterID, lstSubject);
            //lstSummedEvaluation = lstSummedEvaluation.OrderBy(p => p.lstSubject.OrderByDescending(s => s.IsCommenting)).ToList();

            lstSummedEvaluation = lstSummedEvaluation.Where(p => !string.IsNullOrEmpty(p.EvaluationResult) && p.EvaluationResult.Equals("CHT")).ToList();//Loc ra nhung hoc sinh so ket qua danh gia CHT


            OutPutName = SetCellValueRateAdd(sheet, lstSummedEvaluation, classID, semesterID);
            //sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = OutPutName;
            return result;
        }

        private string SetCellValueExcel(IVTWorksheet sheet,List<SummedEvaluationViewModel> lstData,int classID,int semesterID)
        {
            #region fill tieu de

            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            string Title = string.Empty;
            string className = ClassProfileBusiness.Find(classID).DisplayName;
            string strClassName = string.Empty;

            if (Utils.Utils.StripVNSign(className.ToUpper()).Contains("LOP"))
            {
                strClassName = className;
            }
            else
            {
                strClassName = "lớp " + className; 
            }

            if (semesterID == 1)
            {
                Title = string.Format("BẢNG TỔNG HỢP ĐÁNH GIÁ HỌC SINH CUỐI HỌC KỲ I - {0}", strClassName.ToUpper());
            }
            else if (semesterID == 2)
            {
                Title = string.Format("BẢNG TỔNG HỢP ĐÁNH GIÁ HỌC SINH CUỐI NĂM - {0}", strClassName.ToUpper());
            }
            sheet.SetCellValue("A3", Title);
            string TitleYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).DisplayTitle;
            sheet.SetCellValue("A4", "NĂM HỌC " + TitleYear);
            #endregion

            #region fill du lieu
            int countMark = 0;
            int countJudge = 0;
            bool fistMark = true;
            bool fistJudge = true;
            int startRow = 8;
            int starRowHeader = 6;
            int startColumn = 5;
            int colSemester = 0;
            if (semesterID == 1)
            {
                colSemester = 1;
            }
            else
            {
                colSemester = 2;
            }

            //lay danh sach hoc sinh chuyen truong chuyen lop thoi hoc
            List<PupilOfClassBO> lstPupilMove = new List<PupilOfClassBO>();
            PupilOfClassBO objPupilMove = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"SemesterID",semesterID}
            };
            lstPupilMove = PupilOfClassBusiness.GetPupilOfClassStatusOrtherStudy(dic);

            SummedEvaluationViewModel objSum = null;
            SubjectViewModel objSubject = new SubjectViewModel();
            List<SubjectViewModel> lstSubject = new List<SubjectViewModel>();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool checkPupilMove = true;

            for (int i = 0; i < lstData.Count; i++)
            {
                objSum = lstData[i];
                sheet.SetCellValue(startRow, 1, i + 1);//STT
                sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                sheet.SetCellValue(startRow, 2, objSum.PupilCode);//Mã HS
                sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                sheet.SetCellValue(startRow, 3, objSum.FullName);//Họ tên
                sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                sheet.SetCellValue(startRow, 4, objSum.BirthDay.ToString(SummedEvaluationConstant.FORMAT_DATETIME));//Ngày sinh
                sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                lstSubject = objSum.lstSubject.OrderBy(s=>s.IsCommenting).ToList();
                objPupilMove = lstPupilMove.Where(p => p.PupilID == objSum.PupilID).FirstOrDefault();
                //fill môn học
                for (int j = 0; j < lstSubject.Count; j++)
                {
                    objSubject = lstSubject[j];
                    if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (objSum.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && objPupilMove.LeavingDate > objAcademicYear.FirstSemesterEndDate))
                        {
                            checkPupilMove = true;
                        }
                        else
                        {
                            checkPupilMove = false;
                        }
                    }
                    else
                    {
                        if (objSum.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && objPupilMove.LeavingDate > objAcademicYear.SecondSemesterEndDate))
                        {
                            checkPupilMove = true;
                        }
                        else
                        {
                            checkPupilMove = false;
                        }
                    }

                    if (objSubject.IsCommenting == 0)//Mon tinh diem
                    {
                        sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSubject.EvaluationEnding : string.Empty);//Đánh giá
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                        startColumn++;

                        sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSubject.PeriodicEndingMark : null);//KTDK
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                        startColumn++;
                        if (fistMark)
                        {
                            countMark++;
                        }
                        
                    }
                    else
                    {
                        sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSubject.EvaluationEnding : string.Empty);//Đánh giá
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                        startColumn++;
                        if (fistJudge)
                        {
                            countJudge++;
                        }
                        
                    }
                    
                }
                fistMark = fistJudge = false;
                //fill năng lực
                sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSum.Capacity:string.Empty);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                startColumn++;

                //fill phẩm chất
                sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSum.Quality : string.Empty);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                if (semesterID == 2)
                {
                    startColumn++;
                    //fill đánh giá nếu là HKII
                    sheet.SetCellValue(startRow, startColumn, checkPupilMove ? ("HT".Equals(objSum.EvaluationResult) ? SummedEvaluationConstant.EVALUATION_RESULT_HT : "CHT".Equals(objSum.EvaluationResult) ? SummedEvaluationConstant.EVALUATION_RESULT_CHT : "") : string.Empty);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                    sheet.SetColumnWidth(startColumn,15);
                }

                if (objSum.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, startColumn).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, 1, startRow, startColumn).IsLock = true;
                }

                startColumn = 5;

                if ((i + 1) % 5 == 0)
                {
                    sheet.GetRange(startRow, 1, startRow, startColumn + countMark*2 + countJudge + colSemester).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }
                else
                {
                    sheet.GetRange(startRow, 1, startRow, startColumn + countMark * 2 + countJudge + colSemester).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }

                if ((i + 1) == lstData.Count())
                {
                    sheet.GetRange(startRow, 1, startRow, startColumn + countMark * 2 + countJudge + colSemester).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }

                startRow++;
            }

            #region fill header
            lstSubject = new List<SubjectViewModel>();
            lstSubject = lstData.Select(p => p.lstSubject).FirstOrDefault();
            lstSubject = lstSubject.OrderBy(s => s.IsCommenting).ToList();
            startColumn = 5;
            for (int i = 0; i < lstSubject.Count; i++)
            {
                objSubject = lstSubject[i];
                if (objSubject.IsCommenting == 0)
                {
                    sheet.SetCellValue(starRowHeader, startColumn, objSubject.SubjectName);//tên môn
                    sheet.GetRange(starRowHeader, startColumn, starRowHeader, startColumn + 1).Merge();
                    

                    sheet.SetCellValue(starRowHeader + 1, startColumn, "Đánh giá");
                    sheet.SetCellValue(starRowHeader + 1, startColumn + 1, "KTDK");

                    startColumn = startColumn + 2;
                }
                else
                {
                    sheet.SetCellValue(starRowHeader, startColumn, objSubject.SubjectName);//tên môn
                    sheet.GetRange(starRowHeader, startColumn, starRowHeader + 1, startColumn).Merge();
                    startColumn++;
                }
            }
            //header nang luc
            sheet.SetCellValue(starRowHeader, startColumn, "Năng lực");
            sheet.GetRange(starRowHeader, startColumn, starRowHeader + 1, startColumn).Merge();
            sheet.SetColumnWidth(startColumn,13);
            sheet.SetCellValue(starRowHeader, startColumn + 1, "Phẩm chất");
            sheet.GetRange(starRowHeader, startColumn + 1, starRowHeader + 1, startColumn + 1).Merge();
            sheet.SetColumnWidth(startColumn + 1, 13);
            if (semesterID == 2)
            {
                sheet.SetCellValue(starRowHeader, startColumn + 2, "Đánh giá");
                sheet.GetRange(starRowHeader, startColumn + 2, starRowHeader + 1, startColumn + 2).Merge();
            }
            startColumn = 5;
            sheet.GetRange(starRowHeader, startColumn, starRowHeader + 1, startColumn + countMark * 2 + countJudge + colSemester).FillColor(System.Drawing.Color.RosyBrown);
            sheet.GetRange(starRowHeader, startColumn, starRowHeader + 1, startColumn + countMark * 2 + countJudge + colSemester).SetBorder(VTBorderStyle.Solid,VTBorderWeight.Thin,VTBorderIndex.All);
            #endregion
            #endregion

            string SemesterName = string.Empty;
            if (semesterID == 1)
            {
                SemesterName = "CuoiHK1";
            }
            else if (semesterID == 2)
            {
                SemesterName = "CuoiNam";
            }
            return string.Format("BangTongHopDanhGia_{0}_{1}.xls", Utils.Utils.StripVNSign(strClassName.Replace(" ","")), SemesterName);
        }
        private string SetCellValueRateAdd(IVTWorksheet sheet, List<SummedEvaluationViewModel> lstData, int classID, int semesterID)
        {
            #region fill tieu de

            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            string Title = string.Empty;
            string className = ClassProfileBusiness.Find(classID).DisplayName;
            string strClassName = string.Empty;

            if (Utils.Utils.StripVNSign(className.ToUpper()).Contains("LOP"))
            {
                strClassName = className;
            }
            else
            {
                strClassName = "Lớp " + className;
            }

            string TitleYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).DisplayTitle;
            sheet.SetCellValue("A4", string.Format("{0}, Năm học {1} ", strClassName, TitleYear));
            #endregion

            #region fill du lieu
            int countMark = 0;
            int countJudge = 0;
            bool fistMark = true;
            bool fistJudge = true;
            int startRow = 8;
            int starRowHeader = 6;
            int startColumn = 5;
            int colSemester = 0;
            if (semesterID == 1)
            {
                colSemester = 1;
            }
            else
            {
                colSemester = 2;
            }

            //lay danh sach hoc sinh chuyen truong chuyen lop thoi hoc
            List<PupilOfClassBO> lstPupilMove = new List<PupilOfClassBO>();
            PupilOfClassBO objPupilMove = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"SemesterID",semesterID}
            };
            lstPupilMove = PupilOfClassBusiness.GetPupilOfClassStatusOrtherStudy(dic);

            SummedEvaluationViewModel objSum = null;
            SubjectViewModel objSubject = new SubjectViewModel();
            List<SubjectViewModel> lstSubject = new List<SubjectViewModel>();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool checkPupilMove = true;

            if (lstData.Count > 0)
            {
                for (int i = 0; i < lstData.Count; i++)
                {
                    objSum = lstData[i];
                    sheet.SetCellValue(startRow, 1, i + 1);//STT
                    sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                    sheet.SetCellValue(startRow, 2, objSum.PupilCode);//Mã HS
                    sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                    sheet.SetCellValue(startRow, 3, objSum.FullName);//Họ tên
                    sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                    sheet.SetCellValue(startRow, 4, objSum.BirthDay.ToString(SummedEvaluationConstant.FORMAT_DATETIME));//Ngày sinh
                    sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                    lstSubject = objSum.lstSubject.OrderBy(s => s.IsCommenting).ToList();
                    objPupilMove = lstPupilMove.Where(p => p.PupilID == objSum.PupilID).FirstOrDefault();
                    //fill môn học
                    for (int j = 0; j < lstSubject.Count; j++)
                    {
                        objSubject = lstSubject[j];
                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (objSum.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && objPupilMove.LeavingDate > objAcademicYear.FirstSemesterEndDate))
                            {
                                checkPupilMove = true;
                            }
                            else
                            {
                                checkPupilMove = false;
                            }
                        }
                        else
                        {
                            if (objSum.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && objPupilMove.LeavingDate > objAcademicYear.SecondSemesterEndDate))
                            {
                                checkPupilMove = true;
                            }
                            else
                            {
                                checkPupilMove = false;
                            }
                        }

                        if (objSubject.IsCommenting == 0)//Mon tinh diem
                        {
                            sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSubject.EvaluationEnding : string.Empty);//Đánh giá
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                            if (!string.IsNullOrEmpty(objSubject.EvaluationEnding) && "CHT".Equals(objSubject.EvaluationEnding))
                            {
                                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetFontColour(System.Drawing.Color.Red);
                            }
                            startColumn++;

                            sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSubject.PeriodicEndingMark : null);//KTDK
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                            if (objSubject.PeriodicEndingMark.HasValue && objSubject.PeriodicEndingMark.Value < 5)
                            {
                                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetFontColour(System.Drawing.Color.Red);
                            }
                            startColumn++;
                            if (fistMark)
                            {
                                countMark++;
                            }

                        }
                        else
                        {
                            sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSubject.EvaluationEnding : string.Empty);//Đánh giá
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                            if (!string.IsNullOrEmpty(objSubject.EvaluationEnding) && "CHT".Equals(objSubject.EvaluationEnding))
                            {
                                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetFontColour(System.Drawing.Color.Red);
                            }
                            startColumn++;
                            if (fistJudge)
                            {
                                countJudge++;
                            }

                        }

                    }
                    fistMark = fistJudge = false;
                    //fill năng lực
                    sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSum.Capacity : string.Empty);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                    if (!string.IsNullOrEmpty(objSum.Capacity) && "CĐ".Equals(objSum.Capacity))
                    {
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetFontColour(System.Drawing.Color.Red);
                    }
                    startColumn++;

                    //fill phẩm chất
                    sheet.SetCellValue(startRow, startColumn, checkPupilMove ? objSum.Quality : string.Empty);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                    if (!string.IsNullOrEmpty(objSum.Quality) && "CĐ".Equals(objSum.Quality))
                    {
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetFontColour(System.Drawing.Color.Red);
                    }

                    if (semesterID == 2)
                    {
                        startColumn++;
                        //fill đánh giá nếu là HKII
                        sheet.SetCellValue(startRow, startColumn, checkPupilMove ? ((objSum.RateAdd.HasValue && objSum.RateAdd == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT) ? SummedEvaluationConstant.EVALUATION_RESULT_HT : (objSum.RateAdd.HasValue && objSum.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT) ? SummedEvaluationConstant.EVALUATION_RESULT_CHT : "") : string.Empty);
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetHAlign(VTHAlign.xlHAlignLeft);
                        if (objSum.RateAdd.HasValue && objSum.RateAdd == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT)
                        {
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).SetFontColour(System.Drawing.Color.Red);
                        }
                        sheet.SetColumnWidth(startColumn, 15);
                    }

                    if (objSum.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 1, startRow, startColumn).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, startColumn).IsLock = true;
                    }

                    startColumn = 5;

                    if ((i + 1) % 5 == 0)
                    {
                        sheet.GetRange(startRow, 1, startRow, startColumn + countMark * 2 + countJudge + colSemester).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                    else
                    {
                        sheet.GetRange(startRow, 1, startRow, startColumn + countMark * 2 + countJudge + colSemester).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    if ((i + 1) == lstData.Count())
                    {
                        sheet.GetRange(startRow, 1, startRow, startColumn + countMark * 2 + countJudge + colSemester).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    startRow++;
                }
                #region fill header
                lstSubject = new List<SubjectViewModel>();
                lstSubject = lstData.Select(p => p.lstSubject).FirstOrDefault();
                lstSubject = lstSubject.OrderBy(s => s.IsCommenting).ToList();
                startColumn = 5;
                for (int i = 0; i < lstSubject.Count; i++)
                {
                    objSubject = lstSubject[i];
                    if (objSubject.IsCommenting == 0)
                    {
                        sheet.SetCellValue(starRowHeader, startColumn, objSubject.SubjectName);//tên môn
                        sheet.GetRange(starRowHeader, startColumn, starRowHeader, startColumn + 1).Merge();


                        sheet.SetCellValue(starRowHeader + 1, startColumn, "Đánh giá");
                        sheet.SetCellValue(starRowHeader + 1, startColumn + 1, "KTDK");

                        startColumn = startColumn + 2;
                    }
                    else
                    {
                        sheet.SetCellValue(starRowHeader, startColumn, objSubject.SubjectName);//tên môn
                        sheet.GetRange(starRowHeader, startColumn, starRowHeader + 1, startColumn).Merge();
                        startColumn++;
                    }
                }
                //header nang luc
                sheet.SetCellValue(starRowHeader, startColumn, "Năng lực");
                sheet.GetRange(starRowHeader, startColumn, starRowHeader + 1, startColumn).Merge();
                sheet.SetColumnWidth(startColumn, 7);
                sheet.SetCellValue(starRowHeader, startColumn + 1, "Phẩm chất");
                sheet.GetRange(starRowHeader, startColumn + 1, starRowHeader + 1, startColumn + 1).Merge();
                sheet.SetColumnWidth(startColumn + 1, 7);
                if (semesterID == 2)
                {
                    sheet.SetCellValue(starRowHeader, startColumn + 2, "Đánh giá bổ sung");
                    sheet.GetRange(starRowHeader, startColumn + 2, starRowHeader + 1, startColumn + 2).Merge();
                }
                startColumn = 5;
                sheet.GetRange(starRowHeader, startColumn, starRowHeader + 1, startColumn + countMark * 2 + countJudge + colSemester).FillColor(System.Drawing.Color.RosyBrown);
                sheet.GetRange(starRowHeader, startColumn, starRowHeader + 1, startColumn + countMark * 2 + countJudge + colSemester).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                #endregion
            }
            #endregion

            sheet.SetCellValue(startRow + 1, startColumn + countMark * 2 + countJudge + colSemester, "Giáo viên chủ nhiệm");
            return string.Format("HS_TH_DSDanhGiaBoSung_{0}.xls", Utils.Utils.StripVNSign(strClassName.Replace(" ", "")));
        }

        private List<SummedEvaluationViewModel> SearchData(int classID,int semesterID,List<ClassSubjectBO> listClassSubject)
        {
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<SummedEvaluationViewModel> lstSummedEvaluationViewModel = new List<SummedEvaluationViewModel>();
            // Lay danh sach hoc sinh
            List<PupilOfClassBO> listPupil = PupilOfClassBusiness.GetPupilInClass(classID)
                                                                .OrderBy(u => u.OrderInClass)
                                                                .ThenBy(u => u.Name)
                                                                .ThenBy(u => u.PupilFullName)
                                                                .ToList();
            
            //lay danh sach hoc sinh chuyen truong chuyen lop thoi hoc
            List<PupilOfClassBO> lstPupilOfClassBO = new List<PupilOfClassBO>();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"SemesterID",semesterID}
            };

            lstPupilOfClassBO = PupilOfClassBusiness.GetPupilOfClassStatusOrtherStudy(dic);

            // Lay danh sach hoc sinh mien giảm
            // ky 1
            List<ExemptedSubject> lstExemptedI = ExemptedSubjectBusiness.GetListExemptedSubject(classID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            // ky 2
            List<ExemptedSubject> lstExemptedII = ExemptedSubjectBusiness.GetListExemptedSubject(classID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();

            dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"SemesterID",semesterID},
                {"Year",objAca.Year},
                {"EvaluationID",GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY}
            };
            List<SummedEvaluationBO> lstSummedEvaluation = SummedEvaluationBusiness.GetSummedEvaluationByClass(dic);


            dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"Year",objAca.Year}
            };
            List<SummedEndingEvaluation> lstSummedEnding = SummedEndingEvaluationBusiness.GetSummedEndingEvaluation(dic);
            List<SummedEvaluationBO> lstTmp = null;

            SummedEvaluationViewModel objViewModel = null;
            PupilOfClassBO objPupilOfClass = null;
            PupilOfClassBO objPupilMove = null;
            SummedEvaluationBO objSummedEvaluationBO = null;
            ClassSubjectBO objClassSubject = null;
            SubjectViewModel objSubject = null;
            DateTime moveDate = new DateTime();
            int EnablebntSaveRateAdd = 0;
            for (int i = 0; i < listPupil.Count; i++)
            {
                objPupilOfClass = listPupil[i];
                objViewModel = new SummedEvaluationViewModel();
                objViewModel.PupilID = objPupilOfClass.PupilID;
                objViewModel.PupilCode = objPupilOfClass.PupilCode;
                objViewModel.FullName = objPupilOfClass.PupilFullName;
                objViewModel.Status = objPupilOfClass.Status;
                objViewModel.BirthDay = objPupilOfClass.Birthday;
                objPupilMove = lstPupilOfClassBO.Where(p => p.PupilID == objPupilOfClass.PupilID).FirstOrDefault();
                if (objPupilMove != null)
                {
                    moveDate = objPupilMove.LeavingDate.HasValue ? objPupilMove.LeavingDate.Value : new DateTime();
                }
                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    if (objPupilOfClass.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && moveDate.Date > objAca.FirstSemesterEndDate))
                    {
                        objViewModel.Capacity = lstSummedEnding.Where(p => p.PupilID == objPupilOfClass.PupilID && p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY && p.SemesterID == semesterID).Select(p => p.EndingEvaluation).FirstOrDefault();
                        objViewModel.Quality = lstSummedEnding.Where(p => p.PupilID == objPupilOfClass.PupilID && p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY && p.SemesterID == semesterID).Select(p => p.EndingEvaluation).FirstOrDefault();
                    }
                    else
                    {
                        objViewModel.IsMove = true;
                    }
                }
                else
                {
                    if (objPupilOfClass.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && moveDate.Date > objAca.SecondSemesterEndDate))
                    {
                        objViewModel.Capacity = lstSummedEnding.Where(p => p.PupilID == objPupilOfClass.PupilID && p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY && p.SemesterID == semesterID).Select(p => p.EndingEvaluation).FirstOrDefault();
                        objViewModel.Quality = lstSummedEnding.Where(p => p.PupilID == objPupilOfClass.PupilID && p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY && p.SemesterID == semesterID).Select(p => p.EndingEvaluation).FirstOrDefault();
                        objViewModel.EvaluationResult = lstSummedEnding.Where(p => p.PupilID == objPupilOfClass.PupilID && p.EvaluationID == GlobalConstants.EVALUATION_RESULT && p.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT).Select(p => p.EndingEvaluation).FirstOrDefault();
                        objViewModel.RateAdd = lstSummedEnding.Where(p => p.PupilID == objPupilOfClass.PupilID && p.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT).Select(p => p.RateAdd).FirstOrDefault();
                        if ("CHT".Equals(objViewModel.EvaluationResult))
                        {
                            EnablebntSaveRateAdd++;
                        }
                    }
                    else
                    {
                        objViewModel.IsMove = true;
                    }

                }
                lstTmp = lstSummedEvaluation.Where(p => p.PupilID == objPupilOfClass.PupilID).ToList();

                objViewModel.lstSubject = new List<SubjectViewModel>();
                for (int j = 0; j < listClassSubject.Count; j++)
                {
                    objClassSubject = listClassSubject[j];
                    objSubject = new SubjectViewModel();
                    objSubject.SubjectID = objClassSubject.SubjectID;
                    objSubject.SubjectName = objClassSubject.DisplayName;
                    objSubject.IsCommenting = objClassSubject.IsCommenting;
                    objSubject.IsExempted = semesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                    ? lstExemptedI.Any(u => u.PupilID == objPupilOfClass.PupilID && u.SubjectID == objClassSubject.SubjectID)
                                                    : lstExemptedII.Any(u => u.PupilID == objPupilOfClass.PupilID && u.SubjectID == objClassSubject.SubjectID);
                    objSummedEvaluationBO = lstTmp.Where(p => p.EvaluationCriteriaID == objClassSubject.SubjectID).FirstOrDefault();
                    if (objSubject.IsExempted)
                    {
                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (objPupilOfClass.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && moveDate.Date > objAca.FirstSemesterEndDate))
                            {
                                objSubject.Display = "MG";
                            }
                        }
                        else
                        {
                            if (objPupilOfClass.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && moveDate.Date > objAca.SecondSemesterEndDate))
                            {
                                objSubject.Display = "MG";
                            }
                        }
                        
                    }
                    else if (objSummedEvaluationBO != null)
                    {
                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (objPupilOfClass.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && moveDate.Date > objAca.FirstSemesterEndDate))
                            {
                                if (!string.IsNullOrEmpty(objSummedEvaluationBO.EndingEvaluation))
                                {
                                    objSubject.EvaluationEnding = objSummedEvaluationBO.EndingEvaluation;
                                    objSubject.isCheckRate = true;
                                }
                                else
                                {
                                    objSubject.isCheckRate = false;
                                }
                            }
                            objSubject.PeriodicEndingMark = objSummedEvaluationBO.Mark;
                        }
                        else
                        {
                            if (objPupilOfClass.Status == GlobalConstants.PUPIL_STATUS_STUDYING || (objPupilMove != null && moveDate.Date > objAca.SecondSemesterEndDate))
                            {
                                if (!string.IsNullOrEmpty(objSummedEvaluationBO.EndingEvaluation))
                                {
                                    objSubject.EvaluationEnding = objSummedEvaluationBO.EndingEvaluation;
                                    objSubject.isCheckRate = true;
                                    
                                }
                                else
                                {
                                    objSubject.isCheckRate = false;
                                }
                            }
                            objSubject.PeriodicEndingMark = objSummedEvaluationBO.Mark;
                        }
                    }
                    objViewModel.lstSubject.Add(objSubject);
                }
                lstSummedEvaluationViewModel.Add(objViewModel);
            }
            ViewData[SummedEvaluationConstant.ENABLE_BTN_SAVE_RATEADD] = EnablebntSaveRateAdd;
            return lstSummedEvaluationViewModel;
        }

        private void SetVisbileButton(int semester, int classID)
        {
            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (!(isAdminShcool || isGVCN))
            {
                ViewData[SummedEvaluationConstant.DISABLED_BUTTON] = false;
            }
            else if (isAdminShcool)
            {
                if (dateTimeNow < objAca.FirstSemesterStartDate.Value || (objAca.FirstSemesterEndDate < dateTimeNow && dateTimeNow < objAca.SecondSemesterStartDate) 
                    || dateTimeNow > objAca.SecondSemesterEndDate)//ngoài thời gian năm học
                {
                    ViewData[SummedEvaluationConstant.DISABLED_BUTTON] = false;
                }
                else
                {
                    ViewData[SummedEvaluationConstant.DISABLED_BUTTON] = true;
                }
            }
            else if (isGVCN)//chỉ được thao tác trong học kỳ hiện tại
            {
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    if (dateTimeNow < objAca.FirstSemesterStartDate.Value || dateTimeNow > objAca.FirstSemesterEndDate)
                    {
                        ViewData[SummedEvaluationConstant.DISABLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[SummedEvaluationConstant.DISABLED_BUTTON] = true;
                    }
                }
                else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    if (dateTimeNow < objAca.SecondSemesterStartDate.Value || dateTimeNow > objAca.SecondSemesterEndDate.Value)
                    {
                        ViewData[SummedEvaluationConstant.DISABLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[SummedEvaluationConstant.DISABLED_BUTTON] = true;
                    }
                }
            }
        }

    }
}
