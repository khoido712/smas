﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamInputMarkArea
{
    public class ExamInputMarkConstants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";
        public const string IS_HAVE_DATA = "IsHaveData";
        public const string IS_ANABLE_BUTTON = "IsEnableButton";

        public const string LIST_EXAM_INPUT_MARK = "ListExamInputMark";
        public const string LIST_EXAM_GROUP = "ListExamGroup";
        public const string LIST_EXAMINATIONS = "ListExaminations";
        public const string LIST_EXAM_SUBJECT = "ListExamSubject";

        public const string LIST_EXAM_ROOM = "ListExamRoom";
        public const string LIST_EXAM_CANDENCE_BAG = "ListExamCandenceBag";

        public const string FIRST_EXAMINATIONS = "FirstExaminations";
        public const string FIRST_SUBJECT = "FirstSubject";
        public const string FIRST_ROOM = "FirstRoom";
        public const string FIRST_CANDENCE_BAG = "FirstCandenceBag";

        public const string SUBJECT_TYPE_IS_COMMENTING = "SubjectTypeIsCommenting";
        public const int SUBJECT_IS_COMMENTING_JUDGE = 1;
        public const int SUBJECT_IS_COMMENTING_MARK = 0;

        public const int TRUONG_CAP_I = 1;
        public const int TRUONG_CAP_II = 2;
        public const int TRUONG_CAP_III = 3;

        public const string MARK_INPUT_TYPE = "MarkInputType";
        public const int MARK_INPUT_TYPE_DETACHABLE_BAG = 1;
        public const int MARK_INPUT_TYPE_EXAM_ROOM = 2;
        public const int MARK_INPUT_TYPE_PUPIL_CODE = 3;
        public const int MARK_INPUT_TYPE_EXAMINEE_CODE = 4;

        public const string SHOW_MESSAGE_ERROR = "SHOW_MESSAGE_ERROR";
        public const string INVISIBLE_SUBCOMMITTEE = "INVISIBLE_SUBCOMMITTEE";
        public const string DISABLE_BTNSAVE = "DISABLE_BTNSAVE";

        public const string LIST_INPUT_MARK_INSERT = "ListInputMarkInsert";
        public const string LIST_INPUT_MARK_ERROR = "ListInputMarkError";
        public const string CONDITION_SEARCH = "ConditionSearch";
        public const string MARK_INPUT_TYPE_SESSION = "MarkInputTypeSession";

        public const string COUNT_INPUT_MARK_ERROR = "CountInputMarkError";
    }
}