﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using System.Collections.Generic;
using SMAS.Web.Areas.ExamInputMarkArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using System.Text;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.ExamInputMarkArea.Controllers
{
    public class ExamInputMarkController : BaseController
    {
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamInputMarkBusiness ExamInputMarkBusiness;
        private readonly IExamCandenceBagBusiness ExamCandenceBagBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IExamDetachableBagBusiness ExamDetachableBagBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExamSupervisoryBusiness ExamSupervisoryBusiness;
        private readonly IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness;
        private readonly IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness;

        public ExamInputMarkController(IExaminationsBusiness ExaminationsBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness,
            IExamInputMarkBusiness ExamInputMarkBusiness,
            IExamCandenceBagBusiness ExamCandenceBagBusiness,
            IExamRoomBusiness ExamRoomBusiness,
            IExamDetachableBagBusiness ExamDetachableBagBusiness,
            IProcessedReportBusiness ProcessedReportBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            ISubCommitteeBusiness SubCommitteeBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            IExamSupervisoryBusiness ExamSupervisoryBusiness,
            IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness,
            IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamInputMarkBusiness = ExamInputMarkBusiness;
            this.ExamCandenceBagBusiness = ExamCandenceBagBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ExamDetachableBagBusiness = ExamDetachableBagBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.ExamSupervisoryBusiness = ExamSupervisoryBusiness;
            this.ExamSupervisoryAssignmentBusiness = ExamSupervisoryAssignmentBusiness;
            this.ExamInputMarkAssignedBusiness = ExamInputMarkAssignedBusiness;
        }

       
        public ActionResult Index()
        {
            
            SetViewData();
            return View();
        }

        #region SetViewData

        public void SetViewData()
        {
            SetViewDataPermission("ExamInputMarkArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            ViewData[ExamInputMarkConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExamInputMarkConstants.CURRENT_SEMESTER] = _globalInfo.Semester;

            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamInputMarkConstants.LIST_EXAMINATIONS] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");

            if (listExam.Count > 0)
            {
                Examinations firstExam = listExam.FirstOrDefault();
                ViewData[ExamInputMarkConstants.MARK_INPUT_TYPE] = firstExam.MarkInputType;

                search["ExaminationsID"] = firstExam.ExaminationsID;
                List<ExamGroup> listGroup = this.GetExamGroupFromExaminations(firstExam.ExaminationsID);
                if (listGroup.Count > 0)
                {
                    ViewData[ExamInputMarkConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");
                    search["ExamGroupID"] = listGroup.FirstOrDefault().ExamGroupID;

                    // Neu co nhom thi, tim danh sach mon thi
                    List<ExamSubjectBO> listSubject = this.GetExamSubjectFromExamGroup(firstExam.ExaminationsID, listGroup.FirstOrDefault().ExamGroupID);
                    if (listSubject.Count > 0)
                    {
                        search["SubjectID"] = listSubject.FirstOrDefault().SubjectID;
                        ViewData[ExamInputMarkConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");
                        ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = listSubject.FirstOrDefault();

                        // Kiem tra Ky thi, Nhom thi, Mon thi co du dieu kien de Vao diem thi khong
                        bool checkCri = CheckCriteria(firstExam.ExaminationsID, listGroup.FirstOrDefault().ExamGroupID, listSubject.FirstOrDefault().SubjectID);
                        ViewData[ExamInputMarkConstants.IS_ANABLE_BUTTON] = checkCri;
                    }
                    else
                    {
                        ViewData[ExamInputMarkConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                        ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = null;
                    }

                    if (firstExam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_DETACHABLE_BAG)
                    {
                        if (listSubject.Count > 0)
                        {
                            ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = listSubject.FirstOrDefault();

                            List<ExamCandenceBag> listCandence = ExamCandenceBagBusiness.Search(search).OrderBy(o => o.ExamCandenceBagCode).ToList();

                            //Viethd4: loc lai cac tui phach cua cac phong duoc phan cong nhap diem
                            if (!_globalInfo.IsAdmin)
                            {

                                List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(listExam.First().ExaminationsID, listGroup.First().ExamGroupID, listSubject.First().SubjectID);
                                //Lay ra cac tui phach chua cac thi sinh theo phong
                                List<long> lstExamCandenceBagID = ExamCandenceBagBusiness.GetExamCandenceBagByRoom(listExam.First().ExaminationsID, listGroup.First().ExamGroupID, lstAssignedExamRoomID)
                                    .Select(o => o.ExamCandenceBagID).ToList();
                                listCandence = listCandence.Where(o => lstExamCandenceBagID.Contains(o.ExamCandenceBagID)).ToList();
                                search.Add("ListExamRoomID", lstAssignedExamRoomID);
                            }

                            if (listCandence.Count > 0)
                            {
                                search["ExamCandenceBagID"] = listCandence.FirstOrDefault().ExamCandenceBagID;
                                ViewData[ExamInputMarkConstants.LIST_EXAM_CANDENCE_BAG] = listCandence;
                                ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = listCandence.FirstOrDefault();

                                // Dung de kiem tra mon nhan xet hay mon tinh diem
                                search["AppliedLevel"] = _globalInfo.AppliedLevel;

                                // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
                                SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(search).OrderBy(o => o.EducationLevelID).FirstOrDefault();
                                if (schoolSubject != null && schoolSubject.IsCommenting.HasValue)
                                {
                                    ViewData[ExamInputMarkConstants.SUBJECT_TYPE_IS_COMMENTING] = schoolSubject.IsCommenting.Value;
                                }

                                List<ExamInputMarkViewModel> listInputMark = this._SearchByCandenceBag(search).ToList();
                                ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = listInputMark;
                            }
                            else
                            {
                                ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = null;
                                ViewData[ExamInputMarkConstants.LIST_EXAM_CANDENCE_BAG] = new List<ExamCandenceBag>();
                                ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = new List<ExamInputMarkViewModel>();
                            }
                        }
                        else
                        {
                            ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = null;
                            ViewData[ExamInputMarkConstants.LIST_EXAM_CANDENCE_BAG] = new List<ExamCandenceBag>();
                            ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = new List<ExamInputMarkViewModel>();
                        }
                    }
                    else
                    {
                        List<ExamRoom> listRoom = ExamRoomBusiness.Search(search).OrderBy(o => o.ExamRoomCode).ToList();
                        //viethd4: loc lai cac phong thi duoc phan cong
                        
                        if (!_globalInfo.IsAdmin)
                        {
                            if (listSubject.Count > 0)
                            {
                                List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(firstExam.ExaminationsID, listGroup.FirstOrDefault().ExamGroupID, listSubject.First().SubjectID);
                                listRoom = listRoom.Where(o => lstAssignedExamRoomID.Contains(o.ExamRoomID)).ToList();
                            }
                            else
                            {
                                listRoom = new List<ExamRoom>();
                            }

                        }

                        if (listRoom.Count > 0)
                        {
                            search["ExamRoomID"] = listRoom.FirstOrDefault().ExamRoomID;
                            search["MarkInputType"] = firstExam.MarkInputType;
                            ViewData[ExamInputMarkConstants.FIRST_ROOM] = listRoom.FirstOrDefault();
                            ViewData[ExamInputMarkConstants.LIST_EXAM_ROOM] = listRoom;

                            // Dung de kiem tra mon nhan xet hay mon tinh diem
                            search["AppliedLevel"] = _globalInfo.AppliedLevel;

                            // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
                            SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(search).OrderBy(o => o.EducationLevelID).FirstOrDefault();
                            if (schoolSubject != null && schoolSubject.IsCommenting.HasValue)
                            {
                                ViewData[ExamInputMarkConstants.SUBJECT_TYPE_IS_COMMENTING] = schoolSubject.IsCommenting.Value;
                            }

                            IEnumerable<ExamInputMarkViewModel> list = this._SearchByExamRoom(search);
                            ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = list;
                        }
                        else
                        {
                            ViewData[ExamInputMarkConstants.FIRST_ROOM] = null;
                            ViewData[ExamInputMarkConstants.LIST_EXAM_ROOM] = new List<ExamRoom>();
                            ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = new List<ExamInputMarkViewModel>();
                        }
                    }
                }
                else
                {
                    ViewData[ExamInputMarkConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });

                    ViewData[ExamInputMarkConstants.MARK_INPUT_TYPE] = 1;
                    ViewData[ExamInputMarkConstants.LIST_EXAM_CANDENCE_BAG] = new List<ExamCandenceBag>();
                    ViewData[ExamInputMarkConstants.LIST_EXAM_ROOM] = new List<ExamRoom>();
                    ViewData[ExamInputMarkConstants.FIRST_ROOM] = null;
                    ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = null;
                    ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = null;
                    ViewData[ExamInputMarkConstants.FIRST_EXAMINATIONS] = null;
                    ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = new List<ExamInputMarkViewModel>();

                    // Kiem tra cho phep Vao diem thi
                    ViewData[ExamInputMarkConstants.IS_ANABLE_BUTTON] = false;
                }
            }
            else
            {
                ViewData[ExamInputMarkConstants.LIST_EXAMINATIONS] = new SelectList(new string[] { });

                ViewData[ExamInputMarkConstants.MARK_INPUT_TYPE] = 1;
                ViewData[ExamInputMarkConstants.LIST_EXAM_CANDENCE_BAG] = new List<ExamCandenceBag>();
                ViewData[ExamInputMarkConstants.LIST_EXAM_ROOM] = new List<ExamRoom>();
                ViewData[ExamInputMarkConstants.FIRST_ROOM] = null;
                ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = null;
                ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = null;
                ViewData[ExamInputMarkConstants.FIRST_EXAMINATIONS] = null;
                ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = new List<ExamInputMarkViewModel>();

                // Kiem tra cho phep Vao diem thi
                ViewData[ExamInputMarkConstants.IS_ANABLE_BUTTON] = false;
            }

            if (!_globalInfo.SchoolID.HasValue)
            {
                throw new BusinessException(Res.Get("Common_Validate_NotDataPermintion"));
            }
            ViewData[ExamInputMarkConstants.SHOW_MESSAGE_ERROR] = true;
        }

        // Kiem tra dieu kien cho phep Vao diem thi
        private bool CheckCriteria(long ExaminationsID, long ExamGroupID, int SubjectID)
        {
            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            if (exam != null)
            {
                // Nam hoc hien tai - Ky thi tich chon vao diem thi va chua chot diem thi
                bool criteriaI = false;
                if (_globalInfo.IsAdminSchoolRole)
                {
                    criteriaI = _globalInfo.IsCurrentYear && (exam.MarkClosing.HasValue ? exam.MarkClosing.Value == false : true);
                }
                else
                {
                    criteriaI = _globalInfo.IsCurrentYear && exam.MarkInput.HasValue && exam.MarkClosing.HasValue && exam.MarkInput.Value == true && exam.MarkClosing.Value == false;
                }
                // GV da duoc phan quyen nhap diem nhom thi va mon thi
                bool criteriaII = false;
                if (_globalInfo.IsAdminSchoolRole)
                {
                    criteriaII = true;
                }
                else
                {
                    IDictionary<string, object> search = new Dictionary<string, object>()
                    {
                        { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                        { "ExaminationsID", ExaminationsID },
                        { "ExamGroupID", ExamGroupID },
                        { "TeacherID", _globalInfo.EmployeeID },
                        { "SubjectID", SubjectID }
                    };

                    ExamInputMarkAssigned assigned = ExamInputMarkAssignedBusiness.SearchAssigned(search).FirstOrDefault();

                    if (assigned != null)
                    {
                        criteriaII = true;
                    }
                }

                return criteriaI && criteriaII;
            }
            return false;
        }

        #endregion SetViewData

        #region LoadComboBox

        private List<ExamGroup> GetExamGroupFromExaminations(long ExaminationsID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID", ExaminationsID},
            };

            List<ExamGroup> listGroup = null;
            if (_globalInfo.IsAdminSchoolRole)
            {
                listGroup = ExamGroupBusiness.Search(dic).OrderBy(o => o.ExamGroupCode.ToUpper()).ToList();
            }
            else
            {
                IDictionary<string, object> search = new Dictionary<string, object>()
                {
                    { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                    { "ExaminationsID", ExaminationsID },
                    { "TeacherID", _globalInfo.EmployeeID }
                };

                // Load danh sach nhom duoc phan quyen nhap diem
                listGroup = ExamInputMarkBusiness.GetGroupAssignment(search);
            }

            return listGroup;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamGroup(long? ExaminationsID)
        {
            if (ExaminationsID.HasValue)
            {
                List<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID.Value);
                return Json(new SelectList(listGroup, "ExamGroupID", "ExamGroupName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private List<ExamSubjectBO> GetExamSubjectFromExamGroup(long ExaminationsID, long ExamGroupID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID", ExaminationsID},
                {"ExamGroupID", ExamGroupID}
            };

            List<ExamSubjectBO> listSubject = null;
            if (_globalInfo.IsAdminSchoolRole)
            {
                listSubject = ExamSubjectBusiness.GetListExamSubject(dic).ToList();
            }
            else
            {
                IDictionary<string, object> search = new Dictionary<string, object>()
                {
                    { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                    { "ExaminationsID", ExaminationsID },
                    { "ExamGroupID", ExamGroupID }, 
                    { "TeacherID", _globalInfo.EmployeeID }
                };

                // Load danh sach mon theo nhom duoc phan quyen nhap diem
                listSubject = ExamInputMarkBusiness.GetSubjectAssignment(search);
            }
            return listSubject;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamSubject(long? ExaminationsID, long? ExamGroupID)
        {
            if (ExaminationsID.HasValue && ExamGroupID.HasValue)
            {
                List<ExamSubjectBO> listSubject = GetExamSubjectFromExamGroup(ExaminationsID.Value, ExamGroupID.Value);
                return Json(new SelectList(listSubject, "SubjectID", "SubjectName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        public PartialViewResult AjaxLoadExamInputMark(long ExaminationsID, long ExamGroupID, int SubjectID)
        {
            SetViewDataPermission("ExamInputMarkArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            if (ExamGroupID > 0 && SubjectID > 0)
            {
                IDictionary<string, object> search = new Dictionary<string, object>()
                {
                    {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                    {"SchoolID", _globalInfo.SchoolID.Value},
                    {"ExaminationsID", ExaminationsID},
                    {"ExamGroupID", ExamGroupID},
                    {"SubjectID", SubjectID},
                    // Dung de kiem tra mon nhan xet hay mon tinh diem
                    {"AppliedLevel", _globalInfo.AppliedLevel}
                };

                // Kiem tra Ky thi, Nhom thi, Mon thi co du dieu kien de Vao diem thi khong
                bool checkCri = CheckCriteria(ExaminationsID, ExamGroupID, SubjectID);
                ViewData[ExamInputMarkConstants.IS_ANABLE_BUTTON] = checkCri;

                // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
                SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(search).OrderBy(o => o.EducationLevelID).FirstOrDefault();
                if (schoolSubject != null && schoolSubject.IsCommenting.HasValue)
                {
                    ViewData[ExamInputMarkConstants.SUBJECT_TYPE_IS_COMMENTING] = schoolSubject.IsCommenting.Value;
                }

                // Lay thong tin mon hoc cho lan dau
                ExamSubjectBO subject = ExamSubjectBusiness.GetListExamSubject(search).FirstOrDefault();
                ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = subject;

                if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_DETACHABLE_BAG)
                {
                    List<ExamCandenceBag> listCandenceBag = ExamCandenceBagBusiness.Search(search).OrderBy(o => o.ExamCandenceBagCode).ToList();

                    //Viethd4: loc lai cac tui phach cua cac phong duoc phan cong nhap diem
                    if (!_globalInfo.IsAdmin)
                    {
                        
                        List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(ExaminationsID, ExamGroupID, SubjectID);
                        //Lay ra cac tui phach chua cac thi sinh theo phong
                        List<long> lstExamCandenceBagID = ExamCandenceBagBusiness.GetExamCandenceBagByRoom(ExaminationsID, ExamGroupID, lstAssignedExamRoomID)
                            .Select(o => o.ExamCandenceBagID).ToList();
                        listCandenceBag = listCandenceBag.Where(o => lstExamCandenceBagID.Contains(o.ExamCandenceBagID)).ToList();
                        search.Add("ListExamRoomID", lstAssignedExamRoomID);

                    }

                    ViewData[ExamInputMarkConstants.LIST_EXAM_CANDENCE_BAG] = listCandenceBag;
                    if (listCandenceBag.Count > 0)
                    {
                        ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = listCandenceBag.FirstOrDefault();
                        search.Add("ExamCandenceBagID", listCandenceBag.FirstOrDefault().ExamCandenceBagID);
                        List<ExamInputMarkViewModel> listInputMark = this._SearchByCandenceBag(search).ToList();
                        ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = listInputMark;
                    }
                    else
                    {
                        ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = new ExamCandenceBag();
                        ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = new List<ExamInputMarkViewModel>();
                    }
                }
                else
                {
                    List<ExamRoom> listRoom = ExamRoomBusiness.Search(search).OrderBy(o => o.ExamRoomCode).ToList();

                    if (!_globalInfo.IsAdmin)
                    {
                        
                        List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(ExaminationsID, ExamGroupID, SubjectID);
                        listRoom = listRoom.Where(o => lstAssignedExamRoomID.Contains(o.ExamRoomID)).ToList();
                    }

                    ViewData[ExamInputMarkConstants.LIST_EXAM_ROOM] = listRoom;
                    if (listRoom.Count > 0)
                    {
                        ViewData[ExamInputMarkConstants.FIRST_ROOM] = listRoom.FirstOrDefault();
                        search.Add("ExamRoomID", listRoom.FirstOrDefault().ExamRoomID);
                        search.Add("MarkInputType", exam.MarkInputType);
                        List<ExamInputMarkViewModel> listInputMark = this._SearchByExamRoom(search).ToList();
                        ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = listInputMark;
                    }
                    else
                    {
                        ViewData[ExamInputMarkConstants.FIRST_ROOM] = new ExamRoom();
                        ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = new List<ExamInputMarkViewModel>();
                    }
                }
            }
            else
            {
                ViewData[ExamInputMarkConstants.IS_ANABLE_BUTTON] = false;
                ViewData[ExamInputMarkConstants.SUBJECT_TYPE_IS_COMMENTING] = 0;
                ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = new ExamSubjectBO();

                ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = new ExamCandenceBag();
                ViewData[ExamInputMarkConstants.LIST_EXAM_CANDENCE_BAG] = new List<ExamCandenceBag>();

                ViewData[ExamInputMarkConstants.FIRST_ROOM] = new ExamRoom();
                ViewData[ExamInputMarkConstants.LIST_EXAM_ROOM] = new List<ExamRoom>();

                ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = new List<ExamInputMarkViewModel>();
            }

            string partialViewName = string.Empty;
            if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_DETACHABLE_BAG)
            {
                partialViewName = "_ListExamCandenceBag";
            }
            else
                if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_EXAM_ROOM)
                {
                    partialViewName = "_ListExamRoom";
                }
                else
                    if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_PUPIL_CODE)
                    {
                        partialViewName = "_ListExamPupilCode";
                    }
                    else
                        if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_EXAMINEE_CODE)
                        {
                            partialViewName = "_ListExamineeCode";
                        }
            return PartialView(partialViewName);
        }

        #endregion LoadComboBox

        #region Search

        
        public PartialViewResult SearchByCandenceBag(long ExaminationsID, long ExamGroupID, int SubjectID, long ExamCandenceBagID)
        {
            
            SetViewDataPermission("ExamInputMarkArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            ViewData[ExamInputMarkConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("ExaminationsID", ExaminationsID);
            search.Add("ExamGroupID", ExamGroupID);
            search.Add("SubjectID", SubjectID);
            search.Add("ExamCandenceBagID", ExamCandenceBagID);
            // Dung de kiem tra mon nhan xet hay mon tinh diem
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);
            if (!_globalInfo.IsAdmin)
            {
                search.Add("ListExamRoomID", GetAssignedExamRoomID(ExaminationsID, ExamGroupID, SubjectID));
            }

            // Kiem tra Ky thi, Nhom thi, Mon thi co du dieu kien de Vao diem thi khong
            bool checkCri = CheckCriteria(ExaminationsID, ExamGroupID, SubjectID);
            ViewData[ExamInputMarkConstants.IS_ANABLE_BUTTON] = checkCri;

            // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
            SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(search).OrderBy(o => o.EducationLevelID).FirstOrDefault();
            if (schoolSubject != null && schoolSubject.IsCommenting.HasValue)
            {
                ViewData[ExamInputMarkConstants.SUBJECT_TYPE_IS_COMMENTING] = schoolSubject.IsCommenting.Value;
            }

            IEnumerable<ExamInputMarkViewModel> list = this._SearchByCandenceBag(search);
            ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = list;

            ExamSubjectBO subject = ExamSubjectBusiness.GetListExamSubject(search).FirstOrDefault();
            ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = subject;
            ExamCandenceBag candenceBag = ExamCandenceBagBusiness.Search(search).FirstOrDefault();
            ViewData[ExamInputMarkConstants.FIRST_CANDENCE_BAG] = candenceBag;

            return PartialView("_ListInputMarkDetachableBag");
        }

        private IEnumerable<ExamInputMarkViewModel> _SearchByCandenceBag(IDictionary<string, object> search)
        {
            
            SetViewDataPermission("ExamInputMarkArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<ExamInputMarkViewModel> listInputMark = ExamInputMarkBusiness.GetListInputMarkCandenceBag(search)
                .Select(o => new ExamInputMarkViewModel
                {
                    ExamDetachableBagID = o.ExamDetachableBagID,
                    ExamDetachableBagCode = o.ExamDetachableBagCode,
                    ExamPupilID = o.ExamPupilID,
                    PupilID = o.PupilID,
                    ClassID = o.ClassID,
                    ExamMarkInput = o.ExamMarkInput,
                    ActualMarkInput = o.ActualMarkInput,
                    isAbsence = o.isAbsence,
                    penalizedMark = o.penalizedMark

                }).ToList();
            return listInputMark;
        }

        
        public PartialViewResult SearchByExamRoom(long ExaminationsID, long ExamGroupID, int SubjectID, long ExamRoomID)
        {
            
            SetViewDataPermission("ExamInputMarkArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            ViewData[ExamInputMarkConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("ExaminationsID", ExaminationsID);
            search.Add("ExamGroupID", ExamGroupID);
            search.Add("SubjectID", SubjectID);
            search.Add("ExamRoomID", ExamRoomID);
            search.Add("MarkInputType", exam.MarkInputType);
            // Dung de kiem tra mon nhan xet hay mon tinh diem
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            // Kiem tra Ky thi, Nhom thi, Mon thi co du dieu kien de Vao diem thi khong
            bool checkCri = CheckCriteria(ExaminationsID, ExamGroupID, SubjectID);
            ViewData[ExamInputMarkConstants.IS_ANABLE_BUTTON] = checkCri;

            // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
            SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(search).OrderBy(o => o.EducationLevelID).FirstOrDefault();
            if (schoolSubject != null && schoolSubject.IsCommenting.HasValue)
            {
                ViewData[ExamInputMarkConstants.SUBJECT_TYPE_IS_COMMENTING] = schoolSubject.IsCommenting.Value;
            }

            IEnumerable<ExamInputMarkViewModel> list = this._SearchByExamRoom(search);
            ViewData[ExamInputMarkConstants.LIST_EXAM_INPUT_MARK] = list;

            ExamSubjectBO subject = ExamSubjectBusiness.GetListExamSubject(search).FirstOrDefault();
            ViewData[ExamInputMarkConstants.FIRST_SUBJECT] = subject;
            ExamRoom room = ExamRoomBusiness.Search(search).FirstOrDefault(c => c.ExamRoomID == ExamRoomID);
            ViewData[ExamInputMarkConstants.FIRST_ROOM] = room;

            string partialViewName = string.Empty;
            if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_EXAM_ROOM)
            {
                partialViewName = "_ListInputMarkExamRoom";
            }
            if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_PUPIL_CODE)
            {
                partialViewName = "_ListInputMarkPupilCode";
            }
            if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_EXAMINEE_CODE)
            {
                partialViewName = "_ListInputMarkExamineeCode";
            }
            return PartialView(partialViewName);
        }

        private IEnumerable<ExamInputMarkViewModel> _SearchByExamRoom(IDictionary<string, object> search)
        {
            
            SetViewDataPermission("ExamInputMarkArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            int markInputType = SMAS.Business.Common.Utils.GetInt(search, "MarkInputType");
            List<ExamInputMarkViewModel> listInputMark = null;
            if (markInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_EXAM_ROOM)
            {
                listInputMark = ExamInputMarkBusiness.GetListInputMarkExamRoom(search)
                    .Select(o => new ExamInputMarkViewModel
                    {
                        ExamDetachableBagID = o.ExamDetachableBagID,
                        ExamDetachableBagCode = o.ExamDetachableBagCode,
                        ExamPupilID = o.ExamPupilID,
                        ExamineeNumber = o.ExamineeNumber,
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        ExamMarkInput = o.ExamMarkInput,
                        ActualMarkInput = o.ActualMarkInput,
                        isAbsence = o.isAbsence,
                        penalizedMark = o.penalizedMark

                    }).OrderBy(o => o.ExamDetachableBagCode).ToList();
            }
            if (markInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_EXAMINEE_CODE)
            {
                listInputMark = ExamInputMarkBusiness.GetListInputMarkExamRoom(search)
                    .Select(o => new ExamInputMarkViewModel
                    {
                        ExamDetachableBagID = o.ExamDetachableBagID,
                        ExamDetachableBagCode = o.ExamDetachableBagCode,
                        ExamPupilID = o.ExamPupilID,
                        ExamineeNumber = o.ExamineeNumber,
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        ExamMarkInput = o.ExamMarkInput,
                        ActualMarkInput = o.ActualMarkInput,
                        isAbsence = o.isAbsence,
                        penalizedMark = o.penalizedMark

                    }).OrderBy(o => o.ExamineeNumber).ToList();
            }
            if (markInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_PUPIL_CODE)
            {
                listInputMark = ExamInputMarkBusiness.GetListInputMarkExamRoom(search)
                    .Select(o => new ExamInputMarkViewModel
                    {
                        ExamDetachableBagID = o.ExamDetachableBagID,
                        ExamDetachableBagCode = o.ExamDetachableBagCode,
                        ExamPupilID = o.ExamPupilID,
                        PupilID = o.PupilID,
                        PupilCode = o.PupilCode,
                        FullName = o.FullName,
                        ClassID = o.ClassID,
                        ClassName = o.ClassName,
                        ExamMarkInput = o.ExamMarkInput,
                        ActualMarkInput = o.ActualMarkInput,
                        isAbsence = o.isAbsence,
                        penalizedMark = o.penalizedMark

                    }).ToList();
            }
            return listInputMark;
        }

        #endregion Search

        #region InputMark

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult InputMark(long ExaminationsID, long ExamGroupID, int SubjectID,
            int[] HiddenPupilID, long[] CheckPupil, string[] ExamMark, string[] HiddenActualMark, int[] HiddenClassID)
        {

            if (GetMenupermission("ExamInputMarkArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            
            IDictionary<string, object> info = new Dictionary<string, object>()
            {
                { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                { "SchoolID", _globalInfo.SchoolID.Value },
                { "ExaminationsID", ExaminationsID },
                { "ExamGroupID", ExamGroupID },
                { "SubjectID", SubjectID },
                // Dung de kiem tra mon nhan xet hay mon tinh diem
                {"AppliedLevel", _globalInfo.AppliedLevel}
            };

            ExamInputMarkBusiness.InputMark(info, HiddenPupilID, CheckPupil, ExamMark, HiddenActualMark, HiddenClassID);

            int countExam = CheckPupil != null ? CheckPupil.Length : 0;
            int totalExam = HiddenPupilID != null ? HiddenPupilID.Length : 0;
            return Json(new JsonMessage(string.Format(Res.Get("ExamInputMark_Common_InputMark"), countExam.ToString(), totalExam.ToString())));
        }

        #endregion InputMark

        #region ExportExcel

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            
            Examinations exam = ExaminationsBusiness.Find(form.Examinations);

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["ExaminationsID"] = form.Examinations;
            dic["ExamGroupID"] = form.ExamGroup;
            dic["SubjectID"] = form.ExamSubject;
            dic["IsGetAll"] = form.IsGetAll;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;

            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_DETACHABLE_BAG)
            {
                dic["ExamCandenceBagID"] = form.ExamCandenceBag;
                if (!_globalInfo.IsAdmin)
                {
                    List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(form.Examinations.GetValueOrDefault(), form.ExamGroup.GetValueOrDefault(), (int)form.ExamSubject.GetValueOrDefault());
                    //Lay ra cac tui phach chua cac thi sinh theo phong
                    List<long> lstExamCandenceBagID = ExamCandenceBagBusiness.GetExamCandenceBagByRoom(form.Examinations.GetValueOrDefault(), form.ExamGroup.GetValueOrDefault(), lstAssignedExamRoomID)
                        .Select(o => o.ExamCandenceBagID).ToList();
                    dic.Add("ListExamCandenceBagID", lstExamCandenceBagID);
                    dic.Add("ListExamRoomID", lstAssignedExamRoomID);
                }

                Stream excel = ExamInputMarkBusiness.CreateReportDetachableBag(dic);
                processedReport = ExamInputMarkBusiness.InsertReportDetachableBag(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();

            }
            else if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_EXAM_ROOM)
            {
                dic["ExamRoomID"] = form.ExamRoom;
                dic["MarkInputType"] = exam.MarkInputType;
                if (!_globalInfo.IsAdmin)
                {
                    List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(form.Examinations.GetValueOrDefault(), form.ExamGroup.GetValueOrDefault(), (int)form.ExamSubject.GetValueOrDefault());
                    dic.Add("ListExamRoomID", lstAssignedExamRoomID);
                }

                Stream excel = ExamInputMarkBusiness.CreateReportExamRoom(dic);
                processedReport = ExamInputMarkBusiness.InsertReportExamRoom(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();

            }
            else if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_PUPIL_CODE)
            {
                dic["ExamRoomID"] = form.ExamRoom;
                dic["MarkInputType"] = exam.MarkInputType;
                if (!_globalInfo.IsAdmin)
                {
                    List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(form.Examinations.GetValueOrDefault(), form.ExamGroup.GetValueOrDefault(), (int)form.ExamSubject.GetValueOrDefault());
                    dic.Add("ListExamRoomID", lstAssignedExamRoomID);
                }

                Stream excel = ExamInputMarkBusiness.CreateReportPupilCode(dic);
                processedReport = ExamInputMarkBusiness.InsertReportPupilCode(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();

            }
            else
            {
                dic["ExamRoomID"] = form.ExamRoom;
                dic["MarkInputType"] = exam.MarkInputType;
                if (!_globalInfo.IsAdmin)
                {
                    List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(form.Examinations.GetValueOrDefault(), form.ExamGroup.GetValueOrDefault(), (int)form.ExamSubject.GetValueOrDefault());
                    dic.Add("ListExamRoomID", lstAssignedExamRoomID);
                }

                Stream excel = ExamInputMarkBusiness.CreateReportExamineeCode(dic);
                processedReport = ExamInputMarkBusiness.InsertReportExamineeCode(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        #endregion ExportExcel

        #region Import
        [ActionAudit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult ImportExcel(long ExaminationsID, long ExamGroupID, int SubjectID, long? ExamCandenceBagID, long? ExamRoomID)
        {
            
            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            SubjectCat subject = SubjectCatBusiness.Find(SubjectID);

            IDictionary<string, object> search = new Dictionary<string, object>()
            {
                { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                { "SchoolID", _globalInfo.SchoolID.Value },
                { "ExaminationsID", ExaminationsID },
                { "ExamGroupID", ExamGroupID },
                { "SubjectID", SubjectID },
                { "AppliedLevel", _globalInfo.AppliedLevel }
            };
            if (!_globalInfo.IsAdmin)
            {
                List<long> lstAssignedExamRoomID = GetAssignedExamRoomID(ExaminationsID, ExamGroupID, SubjectID);
                search.Add("ListExamRoomID", lstAssignedExamRoomID);
            }
            // -- Kiem tra mon nhan xet hay mon tinh diem
            int isCommenting = 0;
            SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(search).OrderBy(o => o.EducationLevelID).FirstOrDefault();
            if (schoolSubject != null && schoolSubject.IsCommenting.HasValue && schoolSubject.IsCommenting.Value == 1)
            {
                isCommenting = 1;
            }

            // Luu file len server
            HttpPostedFileBase file = (HttpPostedFileBase)Session["stream"];
            if (file != null)
            {
                String FileName = string.Format("{0}-{1}-{2}{3}", Path.GetFileNameWithoutExtension(file.FileName), _globalInfo.UserAccountID, Guid.NewGuid().ToString(), Path.GetExtension(file.FileName));
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), FileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
            }

            // Thao tac lay du lieu tren Sheet
            string FilePath = (string)Session["FilePath"];
            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);

            // Lấy tat ca cac sheet 
            List<IVTWorksheet> sheets = oBook.GetSheets();
             IVTWorksheet sheet = null;
            //IVTWorksheet sheet = oBook.GetSheet(1);
            //IVTRange range = sheet.GetRow(1);

            // Khởi tạo dữ liệu
            List<ExamInputMarkBO> lstImportInputMark = new List<ExamInputMarkBO>();
            List<ExamInputMarkBO> lstImportInputMarkInsert = new List<ExamInputMarkBO>();
            List<ExamInputMarkBO> lstImportInputMarkError = new List<ExamInputMarkBO>();
            ExamInputMarkBO importInputMark;

            #region Import theo so phach
            string examCandenceBagCode = string.Empty;
            bool isImport = true;
            if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_DETACHABLE_BAG)
            {
                // Danh sach ma tui phach cua ky thi, nhom thi, mon thi hien dang xet                
                List<ExamDetachableBagBO> listDetachable = ExamInputMarkBusiness.GetListDetachableContains(search);
                if (sheets != null && sheets.Count > 0)
                {
                    #region //Validate cac sheet

                    if (sheets.Count < sheets.Distinct().Count())
                    {
                        throw new BusinessException("Tên sheet trùng nhau");
                    }

                    for (int i = 0; i < sheets.Count; i++)
                    {
                        //Ten sheet = ma tui phach
                        examCandenceBagCode = sheets[i].Name.Trim();
                        //Lay sheet
                        sheet = sheets.FirstOrDefault(p => p.Name.Equals(examCandenceBagCode));

                        // Kiểm tra tên kỳ thi
                        object examinationsName = sheet.GetCellValue(4, 1);
                        if (examinationsName == null || (examinationsName.ToString().ToUpper()).Equals("KỲ THI " + exam.ExaminationsName.ToUpper()) == false)
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_ImportError_ExaminationsName"));
                        }

                        object nameValue = sheet.GetCellValue(7, 1);
                        if (nameValue == null || (nameValue.ToString().Contains("-") == false && nameValue.ToString().Contains(",") == false))
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_Import_Error"));
                        }

                        string nameFile = (string)sheet.GetCellValue(7, 1);
                        string[] cutName = nameFile.Split('-');
                        if (cutName.Length > 1)
                        {
                            // -- Kiem tra mon thi trong file Excel co trung voi mon dang chon hay khong
                            if (cutName[0].Trim().ToUpper().Equals("MÔN THI: " + subject.SubjectName.ToUpper()) == false)
                            {
                                throw new BusinessException(string.Format("ExamInputMatk_Import_Error_Subject"), subject.SubjectName);
                            }

                            // -- Kiem tra tui phach trong file Excel co trung voi tui phach dang chon
                            if (cutName[1].Trim().ToUpper().Equals("TÚI PHÁCH: " + examCandenceBagCode.ToUpper()) == false)
                            {
                                throw new BusinessException(Res.Get("ExamInputMark_Import_Error_CandenceBag"));
                            }
                        }
                        else
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_Import_Error"));
                        }
                    }
                    #endregion

                    #region Lay du lieu trong sheet
                    int startRow = 0;
                    string examDetachableBag, examMark, note;
                    ExamInputMarkBO duplicate = null;
                    //Lap qua danh sach cac sheet
                    for (int j = 0; j < sheets.Count; j++)
                    {
                        startRow = SystemParamsInFile.START_ROW_REPORT_DETACHABLE_BAG;
                        //Ten sheet = ma tui phach
                        examCandenceBagCode = sheets[j].Name.Trim();
                        //Lay sheet
                        sheet = sheets.FirstOrDefault(p => p.Name.Equals(examCandenceBagCode));

                        //Lap qua ca row trong sheet
                        while (sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null || sheet.GetCellValue(startRow, 4) != null)
                        {
                            examDetachableBag = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : string.Empty;
                            object value = sheet.GetCellValue(startRow, 3);
                            examMark = value != null ? sheet.GetCellValue(startRow, 3).ToString() : string.Empty;
                            note = sheet.GetCellValue(startRow, 4) != null ? sheet.GetCellValue(startRow, 4).ToString() : string.Empty;

                            // Danh sách diem thi tu file Import
                            importInputMark = new ExamInputMarkBO();
                            importInputMark.ExamDetachableBagCode = examDetachableBag;
                            importInputMark.ExamCandenceBagCode = examCandenceBagCode;
                            importInputMark.ExamMarkInput = importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true ? examMark : string.Empty;

                            if (string.IsNullOrEmpty(examDetachableBag))
                            {
                                importInputMark.Error = true;
                                importInputMark.MesageError = "Số phách không được rỗng.";
                            }
                            else
                            {
                                if (!listDetachable.Exists(p => p.ExamDetachableBagCode.ToUpper().Equals(examDetachableBag.ToUpper())))
                                {
                                    importInputMark.Error = true;
                                    importInputMark.MesageError = "Số phách không hợp lệ.";
                                }
                            }

                            if (importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true)
                            {
                                // -- Kiem tra diem nhap trong file Excel co hop le khong
                                if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {
                                    int mark = 0;
                                    if (int.TryParse(examMark, out mark) == true)
                                    {
                                        if (mark < 0 || mark > 10)
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(examMark))
                                    {
                                        importInputMark.Error = true;
                                        importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                    }
                                }
                                else
                                {
                                    if (isCommenting == SMAS.Business.Common.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT) // mon nhan xet
                                    {
                                        if (examMark != "CĐ" && examMark != "Đ" && !string.IsNullOrEmpty(examMark))
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                    else
                                    {
                                        //Mon tinh diem
                                        decimal mark = 0;
                                        if (decimal.TryParse(examMark.Replace(".",","), out mark) == true)
                                        {
                                            if (mark < 0 || mark > 10)
                                            {
                                                importInputMark.Error = true;
                                                importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(examMark))
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                }
                            }

                            duplicate = lstImportInputMark.Where(o => o.ExamDetachableBagCode == importInputMark.ExamDetachableBagCode && o.ExamCandenceBagCode == importInputMark.ExamCandenceBagCode).FirstOrDefault();
                            if (duplicate != null)
                            {
                                if (importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true)
                                {
                                    importInputMark.Error = true;
                                    importInputMark.MesageError = Res.Get("Exam_Candence_Duplicate");
                                }
                                lstImportInputMark.Add(importInputMark);
                            }
                            else
                            {
                                lstImportInputMark.Add(importInputMark);
                            }
                            // Tăng chỉ số dòng
                            startRow++;
                        }
                    }

                    lstImportInputMarkInsert = lstImportInputMark.Where(p => string.IsNullOrEmpty(p.MesageError)).ToList();
                    if (lstImportInputMarkInsert != null && lstImportInputMarkInsert.Count > 0)
                    {
                        Session[ExamInputMarkConstants.LIST_INPUT_MARK_INSERT] = lstImportInputMarkInsert;
                        Session[ExamInputMarkConstants.CONDITION_SEARCH] = search;
                        Session[ExamInputMarkConstants.MARK_INPUT_TYPE_SESSION] = exam.MarkInputType;
                    }

                    lstImportInputMarkError = lstImportInputMark.Where(P => P.Error == true).ToList();
                    if (lstImportInputMarkError != null && lstImportInputMarkError.Count > 0)
                    {
                        isImport = false;
                        ViewData[ExamInputMarkConstants.LIST_INPUT_MARK_ERROR] = lstImportInputMarkError;
                        Session[ExamInputMarkConstants.COUNT_INPUT_MARK_ERROR] = lstImportInputMarkError.Count;
                        ViewData[ExamInputMarkConstants.MARK_INPUT_TYPE] = ExamInputMarkConstants.MARK_INPUT_TYPE_DETACHABLE_BAG;
                        return Json(new JsonMessage(RenderPartialViewToString("_ViewErrorImport", null), "CreateViewError"));
                    }
                    #endregion
                }
            }

            #endregion

            #region Import theo phong thi
            else if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_EXAM_ROOM)
            {
                string examRoomCode = string.Empty;
                search.Add("MarkInputType", exam.MarkInputType);
                // Danh sach ma tui phach cua ky thi, nhom thi, mon thi hien dang xet
                List<ExamDetachableBagBO> listDetachable = ExamInputMarkBusiness.GetListDetachableContains(search);
                if (sheets != null && sheets.Count > 0)
                {
                    #region Validate File
                    if (sheets.Count < sheets.Distinct().Count())
                    {
                        throw new BusinessException("Tên sheet trùng nhau");
                    }

                    for (int i = 0; i < sheets.Count; i++)
                    {
                        //Ten sheet = ma phong thi
                        examRoomCode = sheets[i].Name.Trim();
                        //Lay sheet
                        sheet = sheets.FirstOrDefault(p => p.Name.Equals(examRoomCode));

                        // Kiểm tra tên kỳ thi
                        object examinationsName = sheet.GetCellValue(4, 1);
                        if (examinationsName == null || (examinationsName.ToString().ToUpper()).Equals("KỲ THI " + exam.ExaminationsName.ToUpper()) == false)
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_ImportError_ExaminationsName"));
                        }

                        object nameValue = sheet.GetCellValue(7, 1);
                        if (nameValue == null || (nameValue.ToString().Contains("-") == false && nameValue.ToString().Contains(",") == false))
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_Import_Error"));
                        }

                        string nameFile = (string)sheet.GetCellValue(7, 1);
                        string[] cutName = nameFile.Split(',');
                        if (cutName.Length > 1)
                        {
                            // -- Kiem tra mon thi trong file Excel co trung voi mon dang chon hay khong
                            if (cutName[1].Trim().ToUpper().Equals("MÔN THI: " + subject.SubjectName.ToUpper()) == false)
                            {
                                throw new BusinessException(string.Format(Res.Get("ExamInputMatk_Import_Error_Subject"), subject.SubjectName));
                            }

                            // -- Kiem tra phong thi trong file Excel co trung voi phong thi dang chon
                                    if (cutName[0].Trim().ToUpper().Equals("PHÒNG THI: " + examRoomCode.ToUpper()) == false)
                            {
                                throw new BusinessException("ExamInputMark_Import_Error_ExamRoom");
                            }
                        }
                        else
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_Import_Error"));
                        }
                    }
                    #endregion 

                    #region Lay du lieu trong excel
                    int startRow = 0;
                    string examDetachableBag, examMark, note;
                    for (int k = 0; k < sheets.Count; k++)
                    {
                        startRow = SystemParamsInFile.START_ROW_REPORT_EXAM_ROOM;
                        examRoomCode = sheets[k].Name.ToUpper();
                        sheet = sheets.FirstOrDefault(p => p.Name.ToUpper().Equals(examRoomCode));

                        //Lap qua ca row trong sheet
                        while (sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null || sheet.GetCellValue(startRow, 4) != null)
                        {
                            examDetachableBag = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : string.Empty;
                            object value = sheet.GetCellValue(startRow, 3);
                            examMark = value != null ? sheet.GetCellValue(startRow, 3).ToString() : string.Empty;
                            note = sheet.GetCellValue(startRow, 4) != null ? sheet.GetCellValue(startRow, 4).ToString() : string.Empty;

                            // Danh sách hiển thị lên
                            importInputMark = new ExamInputMarkBO();
                            importInputMark.ExamRoomCode = examRoomCode;
                            importInputMark.ExamDetachableBagCode = examDetachableBag;

                            if (string.IsNullOrEmpty(examDetachableBag))
                            {
                                importInputMark.Error = true;
                                importInputMark.MesageError = "Số phách không được rỗng.";
                            }
                            else
                            {
                                if (!listDetachable.Exists(p => p.ExamDetachableBagCode.ToUpper().Equals(examDetachableBag.ToUpper())))
                                {
                                    importInputMark.Error = true;
                                    importInputMark.MesageError = "Số phách không hợp lệ.";
                                }
                            }

                            importInputMark.ExamMarkInput = importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true ? examMark : string.Empty;

                            if (importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true)
                            {
                                // -- Kiem tra diem nhap trong file Excel co hop le khong
                                if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {
                                    int mark = 0;
                                    if (int.TryParse(examMark, out mark) == true)
                                    {
                                        if (mark < 0 || mark > 10)
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                    else if (!String.IsNullOrEmpty(examMark))
                                    {
                                        importInputMark.Error = true;
                                        importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                    }
                                }
                                else
                                {
                                    if (isCommenting == SMAS.Business.Common.GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        if (examMark != "CĐ" && examMark != "Đ" && !String.IsNullOrEmpty(examMark))
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                    else
                                    {
                                        decimal mark = 0;
                                        if (decimal.TryParse(examMark.Replace(".", ","), out mark) == true)
                                        {
                                            if (mark < 0 || mark > 10)
                                            {
                                                importInputMark.Error = true;
                                                importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(examMark))
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                }
                            }
                            
                            ExamInputMarkBO duplicate = lstImportInputMark.Where(o => o.ExamDetachableBagCode == examDetachableBag && o.ExamRoomCode == importInputMark.ExamRoomCode).FirstOrDefault();
                            if (duplicate != null)
                            {
                                if (!string.IsNullOrEmpty(examDetachableBag))
                                {
                                    if (importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true)
                                    {
                                        importInputMark.Error = true;
                                        importInputMark.MesageError = Res.Get("Exam_Candence_Duplicate");
                                    }
                                }
                                lstImportInputMark.Add(importInputMark);
                            }
                            else
                            {
                                lstImportInputMark.Add(importInputMark);
                            }

                            // Tăng chỉ số dòng
                            startRow++;
                        }
                    }

                    lstImportInputMarkInsert = lstImportInputMark.Where(p => string.IsNullOrEmpty(p.MesageError)).ToList();
                    if (lstImportInputMarkInsert != null && lstImportInputMarkInsert.Count > 0)
                    {
                        Session[ExamInputMarkConstants.LIST_INPUT_MARK_INSERT] = lstImportInputMarkInsert;
                        Session[ExamInputMarkConstants.CONDITION_SEARCH] = search;
                        Session[ExamInputMarkConstants.MARK_INPUT_TYPE_SESSION] = exam.MarkInputType;
                    }

                    lstImportInputMarkError = lstImportInputMark.Where(P => P.Error == true).ToList();
                    if (lstImportInputMarkError != null && lstImportInputMarkError.Count > 0)
                    {
                        isImport = false;
                        ViewData[ExamInputMarkConstants.LIST_INPUT_MARK_ERROR] = lstImportInputMarkError;
                        Session[ExamInputMarkConstants.COUNT_INPUT_MARK_ERROR] = lstImportInputMarkError.Count;
                        ViewData[ExamInputMarkConstants.MARK_INPUT_TYPE] = ExamInputMarkConstants.MARK_INPUT_TYPE_EXAM_ROOM;
                        return Json(new JsonMessage(RenderPartialViewToString("_ViewErrorImport", null), "CreateViewError"));

                    }
                   
                    #endregion
                }
            }
            #endregion

            #region Import theo ma hoc sinh
            else if (exam.MarkInputType == ExamInputMarkConstants.MARK_INPUT_TYPE_PUPIL_CODE)
            {
                string examRoomCode = string.Empty;
                // Danh sach thi sinh cua ky thi, nhom thi, mon thi hien dang xet
                search.Add("MarkInputType", exam.MarkInputType);
                 List<ExamPupilInfoBO> listExamPupil = ExamInputMarkBusiness.GetListPupilContains(search).ToList();                
                if (sheets != null && sheets.Count > 0)
                {
                    #region Validate File
                    if (sheets.Count < sheets.Distinct().Count())
                    {
                        throw new BusinessException("Tên sheet trùng nhau");
                    }

                    for (int i = 0; i < sheets.Count; i++)
                    {
                        examRoomCode = sheets[i].Name.Trim();
                        sheet = sheets.FirstOrDefault(p => p.Name.Equals(examRoomCode));

                        // Kiểm tra tên kỳ thi
                        object examinationsName = sheet.GetCellValue(4, 1);
                        if (examinationsName == null || (examinationsName.ToString().ToUpper()).Equals("KỲ THI " + exam.ExaminationsName.ToUpper()) == false)
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_ImportError_ExaminationsName"));
                        }

                        object nameValue = sheet.GetCellValue(7, 1);
                        if (nameValue == null || (nameValue.ToString().Contains("-") == false && nameValue.ToString().Contains(",") == false))
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_Import_Error"));
                        }

                        string nameFile = (string)sheet.GetCellValue(7, 1);
                        string[] cutName = nameFile.Split(',');
                        if (cutName.Length > 1)
                        {
                            // -- Kiem tra mon thi trong file Excel co trung voi mon dang chon hay khong
                            if (cutName[1].Trim().ToUpper().Equals("MÔN THI: " + subject.SubjectName.ToUpper()) == false)
                            {
                                throw new BusinessException(string.Format(Res.Get("ExamInputMatk_Import_Error_Subject"), subject.SubjectName));
                            }

                            // -- Kiem tra phong thi trong file Excel co trung voi phong thi dang chon
                            if (cutName[0].Trim().ToUpper().Equals("PHÒNG THI: " + examRoomCode.ToUpper()) == false)
                            {
                                throw new BusinessException("ExamInputMark_Import_Error_ExamRoom");
                            }
                        }
                        else
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_Import_Error"));
                        }
                    }
                    #endregion

                    #region Lay du lieu trong file excel
                    for (int k = 0; k < sheets.Count; k++)
                    {
                        //lay sheet
                        examRoomCode = sheets[k].Name.ToUpper();
                        sheet = sheets.FirstOrDefault(p => p.Name.ToUpper().Equals(examRoomCode));

                        int startRow = SystemParamsInFile.START_ROW_REPORT_PUPIL_CODE;
                        string pupilCode, fullName, examMark, note;
                        while (sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null || sheet.GetCellValue(startRow, 4) != null)
                        {
                            pupilCode = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : string.Empty;
                            fullName = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString(): string.Empty;
                            object value = sheet.GetCellValue(startRow, 4);
                            examMark = value != null ? sheet.GetCellValue(startRow, 4).ToString() : string.Empty;
                            note = sheet.GetCellValue(startRow, 5) != null ? sheet.GetCellValue(startRow, 5).ToString(): string.Empty;

                            // Danh sách hiển thị lên
                            importInputMark = new ExamInputMarkBO();
                            importInputMark.PupilCode = pupilCode;
                            importInputMark.ExamRoomCode = examRoomCode;
                            importInputMark.FullName = fullName;

                            if (!string.IsNullOrEmpty(pupilCode))
                            {
                                importInputMark.ClassName = listExamPupil.Where(p => p.PupilCode == pupilCode).Select(p => p.ClassName).FirstOrDefault();
                            }
                            else
                            {
                                importInputMark.ClassName = string.Empty;
                            }

                            if (string.IsNullOrEmpty(pupilCode))
                            {
                                importInputMark.Error = true;
                                importInputMark.MesageError = "Mã học sinh không để trống.";
                            }
                            else
                            {
                                if (!listExamPupil.Exists(p => p.PupilCode.ToUpper().Equals(pupilCode.ToUpper())))
                                {
                                    importInputMark.Error = true;
                                    importInputMark.MesageError = "Mã học sinh không tồn tại.";
                                }
                            }

                            importInputMark.ExamMarkInput = importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true ? examMark : string.Empty;

                            if (importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true)
                            {
                                // -- Kiem tra diem nhap trong file Excel co hop le khong
                                if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {
                                    int mark = 0;
                                    if (int.TryParse(examMark, out mark) == true)
                                    {
                                        if (mark < 0 || mark > 10)
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                    else if (!String.IsNullOrEmpty(examMark))
                                    {
                                        importInputMark.Error = true;
                                        importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                    }
                                }
                                else
                                {
                                    if (isCommenting == 1)
                                    {
                                        if (examMark != "CĐ" && examMark != "Đ" && !String.IsNullOrEmpty(examMark))
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                    else
                                    {
                                        decimal mark = 0;
                                        if (decimal.TryParse(examMark.Replace(".", ","), out mark) == true)
                                        {
                                            if (mark < 0 || mark > 10)
                                            {
                                                importInputMark.Error = true;
                                                importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(examMark))
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                }
                            }
                            
                            ExamInputMarkBO duplicate = lstImportInputMark.Where(o => o.PupilCode == pupilCode && o.ExamRoomCode == importInputMark.ExamRoomCode).FirstOrDefault();
                            if (duplicate != null)
                            {
                                if (!string.IsNullOrEmpty(pupilCode))
                                {
                                    if (importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true)
                                    {
                                        importInputMark.Error = true;
                                        importInputMark.MesageError = Res.Get("Exam_PupilCode_Duplicate");
                                    }
                                }
                                lstImportInputMark.Add(importInputMark);
                            }
                            else
                            {
                                lstImportInputMark.Add(importInputMark);
                            }

                            // Tăng chỉ số dòng
                            startRow++;
                        }
                    }

                    lstImportInputMarkInsert = lstImportInputMark.Where(p => string.IsNullOrEmpty(p.MesageError)).ToList();
                    if (lstImportInputMarkInsert != null && lstImportInputMarkInsert.Count > 0)
                    {
                        Session[ExamInputMarkConstants.LIST_INPUT_MARK_INSERT] = lstImportInputMarkInsert;
                        Session[ExamInputMarkConstants.CONDITION_SEARCH] = search;
                        Session[ExamInputMarkConstants.MARK_INPUT_TYPE_SESSION] = exam.MarkInputType;
                    }

                    lstImportInputMarkError = lstImportInputMark.Where(P => P.Error == true).ToList();
                    if (lstImportInputMarkError != null && lstImportInputMarkError.Count > 0)
                    {
                        isImport = false;
                        ViewData[ExamInputMarkConstants.LIST_INPUT_MARK_ERROR] = lstImportInputMarkError;
                        Session[ExamInputMarkConstants.COUNT_INPUT_MARK_ERROR] = lstImportInputMarkError.Count;
                        ViewData[ExamInputMarkConstants.MARK_INPUT_TYPE] = ExamInputMarkConstants.MARK_INPUT_TYPE_PUPIL_CODE;
                        return Json(new JsonMessage(RenderPartialViewToString("_ViewErrorImport", null), "CreateViewError"));
                    }
                    #endregion
                }
            }
            #endregion

            #region Import theo so bao danh
            else
            {
                string ExamineeCode = string.Empty;
                search.Add("MarkInputType", exam.MarkInputType);
                // Danh sach ma tui phach cua ky thi, nhom thi, mon thi hien dang xet
                List<ExamPupilInfoBO> listExamPupil = ExamInputMarkBusiness.GetListPupilContains(search).ToList();

                if (sheets != null && sheets.Count > 0)
                {
                    #region Validate File
                    if (sheets.Count < sheets.Distinct().Count())
                    {
                        throw new BusinessException("Tên sheet trùng nhau");
                    }
                    for (int i = 0; i < sheets.Count; i++)
                    {
                        ExamineeCode = sheets[i].Name.ToUpper();
                        sheet = sheets.FirstOrDefault(p => p.Name.ToUpper().Equals(ExamineeCode));

                        // Kiểm tra tên kỳ thi
                        object examinationsName = sheet.GetCellValue(4, 1);
                        if (examinationsName == null || (examinationsName.ToString().ToUpper()).Equals("KỲ THI " + exam.ExaminationsName.ToUpper()) == false)
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_ImportError_ExaminationsName"));
                        }

                        object nameValue = sheet.GetCellValue(7, 1);
                        if (nameValue == null || (nameValue.ToString().Contains("-") == false && nameValue.ToString().Contains(",") == false))
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_Import_Error"));
                        }

                        string nameFile = (string)sheet.GetCellValue(7, 1);
                        string[] cutName = nameFile.Split(',');
                        if (cutName.Length > 1)
                        {
                            // -- Kiem tra mon thi trong file Excel co trung voi mon dang chon hay khong
                            if (cutName[1].Trim().ToUpper().Equals("MÔN THI: " + subject.SubjectName.ToUpper()) == false)
                            {
                                throw new BusinessException(string.Format(Res.Get("ExamInputMatk_Import_Error_Subject"), subject.SubjectName));
                            }

                            // -- Kiem tra phong thi trong file Excel co trung voi phong thi dang chon
                            if (cutName[0].Trim().ToUpper().Equals("PHÒNG THI: " + ExamineeCode.ToUpper()) == false)
                            {
                                throw new BusinessException("ExamInputMark_Import_Error_ExamRoom");
                            }
                        }
                        else
                        {
                            throw new BusinessException(Res.Get("ExamInputMark_Import_Error"));
                        }
                    }
                    #endregion

                    #region Lay du lieu trong file excel
                    for (int k = 0; k < sheets.Count; k++)
                    {
                        //Lay sheet
                        ExamineeCode = sheets[k].Name.ToUpper();
                        sheet = sheets.FirstOrDefault(p => p.Name.ToUpper().Equals(ExamineeCode));

                        int startRow = SystemParamsInFile.START_ROW_REPORT_EXAMINEE_CODE;
                        string examineeCode, examMark, note;
                        while (sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null || sheet.GetCellValue(startRow, 4) != null)
                        {
                            examineeCode = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : string.Empty;
                            // Danh sách hiển thị lên
                            importInputMark = new ExamInputMarkBO();
                            importInputMark.ExamineeNumber = examineeCode;
                            importInputMark.ExamRoomCode = ExamineeCode;

                            if (string.IsNullOrEmpty(examineeCode))
                            {
                                importInputMark.Error = true;
                                importInputMark.MesageError = "SBD không được rỗng.";
                            }
                            else
                            {
                                if (!listExamPupil.Exists(p => p.ExamineeCode.ToUpper().Equals(examineeCode.ToUpper())))
                                {
                                    importInputMark.Error = true;
                                    importInputMark.MesageError = "Số báo danh không tồn tại.";
                                }
                            }

                            object value = sheet.GetCellValue(startRow, 3);
                            examMark = value != null ? sheet.GetCellValue(startRow, 3).ToString() : string.Empty;
                            importInputMark.ExamMarkInput = importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true ? examMark : string.Empty;
                            note = (string)sheet.GetCellValue(startRow, 4);

                            if (importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true)
                            {
                                // -- Kiem tra diem nhap trong file Excel co hop le khong
                                if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {
                                    int mark = 0;
                                    if (int.TryParse(examMark, out mark) == true)
                                    {
                                        if (mark < 0 || mark > 10)
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                    else if (!String.IsNullOrEmpty(examMark))
                                    {
                                        importInputMark.Error = true;
                                        importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                    }
                                }
                                else
                                {
                                    if (isCommenting == SMAS.Business.Common.GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        if (examMark != "CĐ" && examMark != "Đ" && !String.IsNullOrEmpty(examMark))
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                    else
                                    {
                                        decimal mark = 0;
                                        if (decimal.TryParse(examMark.Replace(".", ","), out mark) == true)
                                        {
                                            if (mark < 0 || mark > 10)
                                            {
                                                importInputMark.Error = true;
                                                importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(examMark))
                                        {
                                            importInputMark.Error = true;
                                            importInputMark.MesageError = Res.Get("ExamInputMark_Import_Error_Mark");
                                        }
                                    }
                                }
                            }

                            ExamInputMarkBO duplicate = lstImportInputMark.Where(o => o.ExamineeNumber == importInputMark.ExamineeNumber && o.ExamRoomCode == importInputMark.ExamRoomCode).FirstOrDefault();
                            if (duplicate != null)
                            {
                                if (importInputMark.Error == false && string.IsNullOrEmpty(importInputMark.MesageError) == true)
                                {
                                    importInputMark.Error = true;
                                    importInputMark.MesageError = Res.Get("Exam_ExamineeCode_Duplicate");
                                }
                                lstImportInputMark.Add(importInputMark);
                            }
                            else
                            {
                                lstImportInputMark.Add(importInputMark);
                            }

                            // Tăng chỉ số dòng
                            startRow++;
                        }
                    }

                    lstImportInputMarkInsert = lstImportInputMark.Where(p => string.IsNullOrEmpty(p.MesageError)).ToList();
                    if (lstImportInputMarkInsert != null && lstImportInputMarkInsert.Count > 0)
                    {
                        Session[ExamInputMarkConstants.LIST_INPUT_MARK_INSERT] = lstImportInputMarkInsert;
                        Session[ExamInputMarkConstants.CONDITION_SEARCH] = search;
                        Session[ExamInputMarkConstants.MARK_INPUT_TYPE_SESSION] = exam.MarkInputType;
                    }

                    lstImportInputMarkError = lstImportInputMark.Where(P => P.Error == true).ToList();
                    if (lstImportInputMarkError != null && lstImportInputMarkError.Count > 0)
                    {
                        isImport = false;
                        ViewData[ExamInputMarkConstants.LIST_INPUT_MARK_ERROR] = lstImportInputMarkError;
                        Session[ExamInputMarkConstants.COUNT_INPUT_MARK_ERROR] = lstImportInputMarkError.Count;
                        ViewData[ExamInputMarkConstants.MARK_INPUT_TYPE] = ExamInputMarkConstants.MARK_INPUT_TYPE_EXAMINEE_CODE;
                        return Json(new JsonMessage(RenderPartialViewToString("_ViewErrorImport", null), "CreateViewError"));
                    }
                    #endregion
                }
            }
            #endregion

            #region Kiem tra ket qua Import
            if (isImport)
            {
                int count = lstImportInputMark.Count;
                if (count > 0)
                {
                    int successCount = 0;
                    //viethd4: Fix nhap diem theo phong
                    
                    ExamInputMarkBusiness.ImportInputMark(search, lstImportInputMark, exam.MarkInputType, ref successCount);
                    //ghi log
                    SetViewDataActionAudit(String.Empty, String.Empty
                    , String.Empty
                    , Res.Get("ExamInputMark_Log_Import")
                    , String.Empty
                    , Res.Get("ExamInputMark_Log_FunctionName")
                    , SMAS.Business.Common.GlobalConstants.ACTION_IMPORT
                    , Res.Get("ExamInputMark_Log_Import"));
                        return Json(new JsonMessage(String.Format(Res.Get("ExamInputMark_Import_Success"), successCount, count)));
                }
                else
                {
                    return Json(new { success = 0 });
                }
            }
            return null;
            #endregion
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveImportAccess()
        {
            
            if (Session[ExamInputMarkConstants.LIST_INPUT_MARK_INSERT] != null)
            {
                int successCount = 0;
                IDictionary<string, object> search = (IDictionary<string, object>)Session[ExamInputMarkConstants.CONDITION_SEARCH];
                int markInputType = (int)Session[ExamInputMarkConstants.MARK_INPUT_TYPE_SESSION];
                int countError = 0;
                List<ExamInputMarkBO> listInsert = (List<ExamInputMarkBO>)Session[ExamInputMarkConstants.LIST_INPUT_MARK_INSERT];
                if (Session[ExamInputMarkConstants.COUNT_INPUT_MARK_ERROR] != null)
                {
                    countError = (int)Session[ExamInputMarkConstants.COUNT_INPUT_MARK_ERROR];
                }

                ExamInputMarkBusiness.ImportInputMark(search, listInsert, markInputType, ref successCount);

                // Remove session
                Session.Remove(ExamInputMarkConstants.LIST_INPUT_MARK_INSERT);
                Session.Remove(ExamInputMarkConstants.CONDITION_SEARCH);
                Session.Remove(ExamInputMarkConstants.MARK_INPUT_TYPE_SESSION);

                // Ghi log
                SetViewDataActionAudit(String.Empty, String.Empty
                , String.Empty
                , Res.Get("ExamInputMark_Log_Import")
                , String.Empty
                , Res.Get("ExamInputMark_Log_FunctionName")
                , SMAS.Business.Common.GlobalConstants.ACTION_IMPORT
                , Res.Get("ExamInputMark_Log_Import"));
                return Json(new JsonMessage(String.Format(Res.Get("ExamInputMark_Import_Success"), successCount, listInsert.Count + countError)));
            }
            return Json(new JsonMessage(Res.Get("ExamInputMark_Import_DataError"), "error"));
        }

        //Xoa session neu chon dong
        public JsonResult CloseNotAccessImport()
        {
            
            //remove session
            Session.Remove(ExamInputMarkConstants.LIST_INPUT_MARK_INSERT);
            Session.Remove(ExamInputMarkConstants.CONDITION_SEARCH);
            Session.Remove(ExamInputMarkConstants.MARK_INPUT_TYPE_SESSION);
            return Json(new JsonMessage("", "error"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            var file = attachments.FirstOrDefault();
            Session["stream"] = file;
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }
                JsonResult res1 = Json(new JsonMessage());
                res1.ContentType = "text/plain";
                return res1;

            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }

       

        #endregion Import

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        /// Lay danh sach phong thi duoc phan cong nhap diem
        /// </summary>
        /// <param name="examinationsId"></param>
        /// <param name="examGroupId"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        private List<long> GetAssignedExamRoomID(long examinationsId, long examGroupId, int subjectId)
        {
            List<long> lstExamRoomID = new List<long>();

            if (examinationsId != 0 && examGroupId != 0 && subjectId != 0)
            {
                IDictionary<string, object> search = new Dictionary<string, object>()
                    {
                        { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                        { "ExaminationsID", examinationsId },
                        { "ExamGroupID", examGroupId },
                        { "TeacherID", _globalInfo.EmployeeID },
                        { "SubjectID", subjectId }
                    };

                ExamInputMarkAssigned assigned = ExamInputMarkAssignedBusiness.SearchAssigned(search).FirstOrDefault();



                if (assigned != null)
                {
                    lstExamRoomID = GetIDsFromString(assigned.ExamRoomID);
                }
            }
            return lstExamRoomID;
        }

        private List<long> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                IDArr = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                IDArr = new string[] { };
            }
            List<long> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList() :
                                            new List<long>();

            return listID;
        } 
    }
}