﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamInputMarkArea
{
    public class ExamInputMarkAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamInputMarkArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamInputMarkArea_default",
                "ExamInputMarkArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
