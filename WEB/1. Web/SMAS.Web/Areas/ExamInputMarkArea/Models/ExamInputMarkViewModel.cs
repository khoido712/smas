﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamInputMarkArea.Models
{
    public class ExamInputMarkViewModel
    {
        // Exam Detachable Bag
        public long ExamDetachableBagID { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Label_DetachableHeadNumber")]
        public string ExamDetachableBagCode { get; set; }
 
        // Exam Pupil Code
        public long PupilID { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_PupilID")]
        public string PupilCode { get; set; }

        // Examinee Code
        public long ExamPupilID { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_ExamineeNumber")]
        public string ExamineeNumber { get; set; }

        // Exam Room
        public long ExamRoomID { get; set; }

        [ResourceDisplayName("ExamRoomAssign_Label_ExamRoom")]
        public string ExamRoomCode { get; set; }

        // List Input Mark
        [ResourceDisplayName("ExamPupil_Grid_Label_PupilName")]
        public string FullName { get; set; }

        public int ClassID { get; set; }

        [ResourceDisplayName("ExamPupil_Label_Class")]
        public string ClassName { get; set; }

        [ResourceDisplayName("InputExaminationMark_Label_ExamMark")]
        public decimal? ExamMark { get; set; }

        [ResourceDisplayName("InputExaminationMark_Label_RealMark")]
        public decimal? ActualMark { get; set; }

        public string Note { get; set; }

        public bool Pass { get; set; }
        public string Description { get; set; }

        public int isAbsence { get; set; }
        public int penalizedMark { get; set; }

        public string ExamMarkInput { get; set; }
        public string ActualMarkInput { get; set; }
    }
}