﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamInputMarkArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamGroup(this)")]
        public long? Examinations { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkConstants.LIST_EXAM_GROUP)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamSubject(this)")]
        public long? ExamGroup { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkConstants.LIST_EXAM_SUBJECT)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamInputMark()")]
        public long? ExamSubject { get; set; }

        public long ExamRoom { get; set; }
        public long ExamCandenceBag { get; set; }

        public int IsGetAll { get; set; }
    }
}