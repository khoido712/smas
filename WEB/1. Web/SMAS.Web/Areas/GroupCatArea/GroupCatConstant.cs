/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.GroupCatArea
{
    public class GroupCatConstants
    {
        public const string LIST_GROUPCAT = "listGroupCat";

        public const string LSMENUS = "ListMenus";

        public const string LS_ROLES_SEARCH = "List of role to search";

        public const string GROUP_NAME = "group name to search";

        public const string ROLE_ID = "roleid";



        public const string LS_ROLES_GRID = "list roles for grid";

        public const string GROUP_ID = "group ID";
    }
}