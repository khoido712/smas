﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.GroupCatArea
{
    public class GroupCatAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "GroupCatArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "GroupCatArea_default",
                "GroupCatArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
