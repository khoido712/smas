/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.GroupCatArea.Models
{
    public class SearchViewModel
    {
        
        [ResourceDisplayName("GroupCat_Label_Name")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string GroupName { get; set; }

        /*
        [ResDisplayName("GroupCat_Label_Role")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", GroupCatConstants.LS_ROLES_SEARCH)]
        [AdditionalMetadata("PlaceHolder","All")] 
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Nullable<int> RoleID { get; set; }
        */
    }
}