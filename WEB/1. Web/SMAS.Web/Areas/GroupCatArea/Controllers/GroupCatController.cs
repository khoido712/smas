﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.GroupCatArea.Models;
using AutoMapper;
using SMAS.Web.Areas.RoleArea.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.VTUtils.Utils;

namespace SMAS.Web.Areas.GroupCatArea.Controllers
{
    public class GroupCatController : BaseController
    {
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IRoleBusiness RoleBusiness;
        private readonly IGroupMenuBusiness GroupMenuBusiness;
        private readonly IMenuBusiness MenuBusiness;
        private readonly IRoleMenuBusiness RoleMenuBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        GlobalInfo globalInfo;
        public GroupCatController(IGroupCatBusiness groupcatBusiness,
            IRoleBusiness rolebusiness,
            IMenuBusiness menuBusiness,
            IRoleMenuBusiness roleMenuBusiness,
            IGroupMenuBusiness groupMenuBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness
            )
        {
            this.GroupCatBusiness = groupcatBusiness;
            this.RoleBusiness = rolebusiness;
            this.GroupMenuBusiness = groupMenuBusiness;
            RoleMenuBusiness = roleMenuBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            MenuBusiness = menuBusiness;
            Mapper.CreateMap<GroupCatViewModel, GroupCat>();
            Mapper.CreateMap<GroupCat, GroupCatViewModel>();
            globalInfo = new GlobalInfo();
        }

        //
        // GET: /GroupCat/

        public ActionResult Index(int? FromSavePriv)
        {
            SetViewDataPermission("GroupCat", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SetViewData();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //SearchInfo.Add("RoleID", globalInfo.RoleID);
            SearchInfo["RoleID"] = globalInfo.RoleID;
            if (globalInfo.SupervisingDeptID.HasValue && !globalInfo.SchoolID.HasValue)
            {
                SupervisingDept objSupervisingDept = SupervisingDeptBusiness.Find(globalInfo.SupervisingDeptID);
                SearchInfo["AdminUserID"] = objSupervisingDept.AdminID;
            }
            if (globalInfo.SchoolID.HasValue)
            {
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value);
                if (schoolProfile != null)
                {
                    SearchInfo.Add("AdminUserID", schoolProfile.AdminID);
                }
            }
            //SearchInfo.Add("CreatedUserID", globalInfo.UserAccountID);
            //Get view data here

            IEnumerable<GroupCatViewModel> lst = this._Search(SearchInfo);
            ViewData[GroupCatConstants.LIST_GROUPCAT] = lst.OrderBy(o => o.GroupName).ToList();
            if (FromSavePriv.HasValue && FromSavePriv.Value == 1)
            {
                // Thong bao thanh cong
                ViewData["FromSavePriv"] = 1;
            }
            return View();
        }

        //
        // GET: /GroupCat/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            if (!string.IsNullOrEmpty(frm.GroupName))
            {
                SearchInfo.Add("GroupCatName", frm.GroupName);
            }
            SearchInfo.Add("RoleID", globalInfo.RoleID);
            //SearchInfo.Add("CreatedUserID", globalInfo.UserAccountID);
            // Phải tìm được admin cấp sở
            if (globalInfo.SupervisingDeptID.HasValue && !globalInfo.SchoolID.HasValue)
            {
                SupervisingDept objSupervisingDept = SupervisingDeptBusiness.Find(globalInfo.SupervisingDeptID);
                SearchInfo.Add("AdminUserID", objSupervisingDept.AdminID);
            }
            if (globalInfo.SchoolID.HasValue)
            {
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value);
                if (schoolProfile != null)
                {
                    SearchInfo.Add("AdminUserID", schoolProfile.AdminID);
                }
            }

            IEnumerable<GroupCatViewModel> lst = this._Search(SearchInfo);
            ViewData[GroupCatConstants.LIST_GROUPCAT] = lst.OrderBy(o=>o.GroupName).ToList();

            //Get view data here
            //SetViewData();
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("GroupCat", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            GlobalInfo Global = new GlobalInfo();
            GroupCat groupcat = new GroupCat();
            TryUpdateModel(groupcat);
            groupcat.CreatedUserID = globalInfo.UserAccountID;
            groupcat.CreatedDate = DateTime.Now;
            groupcat.IsActive = true;
            groupcat.RoleID = globalInfo.RoleID;
            Utils.Utils.TrimObject(groupcat);
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // Tim xem ten nhom da bi trung chua
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            SearchInfo.Add("GroupCatName", groupcat.GroupName);
            SearchInfo.Add("RoleID", globalInfo.RoleID);
            // Phải tìm được admin cấp sở
            if (globalInfo.SupervisingDeptID.HasValue && !globalInfo.SchoolID.HasValue)
            {
                SupervisingDept objSupervisingDept = SupervisingDeptBusiness.Find(globalInfo.SupervisingDeptID);
                SearchInfo.Add("AdminUserID", objSupervisingDept.AdminID);
            }
            if (globalInfo.SchoolID.HasValue)
            {
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value);
                if (schoolProfile != null)
                {
                    SearchInfo.Add("AdminUserID", schoolProfile.AdminID);
                }
            }
            IEnumerable<GroupCatViewModel> lst = this._Search(SearchInfo);
            if (lst != null && lst.Count() > 0)
            {
                throw new BusinessException("Tên nhóm đã tồn tại");
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //Doi voi Role cap phong lay ID Admin phong gan cho AdminUserID
            if (globalInfo.RoleID == SMAS.Web.Constants.GlobalConstants.ROLE_PHONG
                || globalInfo.RoleID == SMAS.Web.Constants.GlobalConstants.ROLE_SO)
            {
                groupcat.AdminUserID = SupervisingDeptBusiness.Find(globalInfo.SupervisingDeptID.Value).AdminID.Value;
            }
            else if (globalInfo.RoleID == SMAS.Web.Constants.GlobalConstants.ROLE_CAPCAO) //Doi voi Role Cap cao lay ID dang nhap gan cho AdminUserID
            {
                groupcat.AdminUserID = globalInfo.UserAccountID;
            }
            else // Doi voi Role cap truong lai SchoolProfile.AdminID  gan cho AdminUserID
            {
                groupcat.AdminUserID = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value).AdminID.Value;
            }
            this.GroupCatBusiness.InsertGroup(groupcat);
            this.GroupCatBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int GroupCatID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("GroupCat", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(GroupCatID, "GroupCat", 2, null, "AdminUserID");
            GlobalInfo Global = new GlobalInfo();
            GroupCat groupcat = this.GroupCatBusiness.Find(GroupCatID);
            TryUpdateModel(groupcat);
            Utils.Utils.TrimObject(groupcat);
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // Tim xem ten nhom da bi trung chua
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            SearchInfo.Add("GroupCatName", groupcat.GroupName);
            SearchInfo.Add("RoleID", globalInfo.RoleID);
            // Phải tìm được admin cấp sở
            if (globalInfo.SupervisingDeptID.HasValue && !globalInfo.SchoolID.HasValue)
            {
                SupervisingDept objSupervisingDept = SupervisingDeptBusiness.Find(globalInfo.SupervisingDeptID);
                SearchInfo.Add("AdminUserID", objSupervisingDept.AdminID);
            }
            if (globalInfo.SchoolID.HasValue)
            {
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value);
                if (schoolProfile != null)
                {
                    SearchInfo.Add("AdminUserID", schoolProfile.AdminID);
                }
            }
            IEnumerable<GroupCatViewModel> lst = this._Search(SearchInfo);
            foreach (var o in lst)
            {
                if (o.GroupCatID != groupcat.GroupCatID && o.GroupName.ToLower() == groupcat.GroupName.ToLower())
                {
                    throw new BusinessException("Tên nhóm đã tồn tại");
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //Doi voi Role cap phong lay ID Admin phong gan cho AdminUserID
            if (globalInfo.RoleID == SMAS.Web.Constants.GlobalConstants.ROLE_PHONG
                || globalInfo.RoleID == SMAS.Web.Constants.GlobalConstants.ROLE_SO)
            {
                groupcat.AdminUserID = SupervisingDeptBusiness.Find(globalInfo.SupervisingDeptID.Value).AdminID.Value;
            }
            else if (globalInfo.RoleID == SMAS.Web.Constants.GlobalConstants.ROLE_CAPCAO) //Doi voi Role Cap cao lay ID dang nhap gan cho AdminUserID
            {
                groupcat.AdminUserID = globalInfo.UserAccountID;
            }
            else // Doi voi Role cap truong lai SchoolProfile.AdminID  gan cho AdminUserID
            {
                groupcat.AdminUserID = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value).AdminID.Value;
            }
            this.GroupCatBusiness.Update(groupcat);
            this.GroupCatBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int? id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("GroupCat", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(id.Value, "GroupCat", 2, null, "AdminUserID");
            this.GroupCatBusiness.Delete(id.Value);
            this.GroupCatBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Phan quyen chuc nang cho Group
        /// </summary>
        /// <param name="GroupCatID">ID nhom duoc phan quyen</param>
        /// <returns></returns>

        public ActionResult ManagePrivileges(int GroupCatID)
        {
            SetViewDataPermission("GroupCat", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            CheckPermissionForAction(GroupCatID, "GroupCat", 2, null, "AdminUserID");

            TempData["GroupCatID"] = GroupCatID;
            string GroupName = GroupCatBusiness.Find(GroupCatID).GroupName;
            ViewData[GroupCatConstants.GROUP_ID] = GroupCatID;
            ViewData[GroupCatConstants.GROUP_NAME] = GroupName;
            //lay ra phien ban truong dang chon
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            //lay ra Menu tuong ung voi Role cua Group
            string strProductVersion = objSP != null && objSP.ProductVersion.HasValue ? objSP.ProductVersion.Value.ToString() : "";
            List<Menu> MenuOfRole;
            if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                MenuOfRole = RoleMenuBusiness.GetMenuOfRole(globalInfo.RoleID, true).ToList();
            }
            else
            {
                MenuOfRole = RoleMenuBusiness.GetMenuOfRole(globalInfo.RoleID, false).Where(p => p.ProductVersion.Contains(strProductVersion)).ToList();
            }
            //Lay ra Menu da duoc phan quyen cho Group
            IQueryable<GroupMenu> GroupMenus = GroupMenuBusiness.GetGroupMenus(GroupCatID);
            //TempData["GroupMenus"] = GroupMenus.ToList();
            //
            List<ItemMenuModel> LsMenuModel = getListItemMenu(MenuOfRole);
            MapPermission(MenuOfRole, GroupMenus, LsMenuModel);
            return View("_GridMenuGroupCat", LsMenuModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult ManagePrivileges(FormCollection formCollection, int[] isAccess, int[] viewPermission, int[] addPermission, int[] editPermission, int[] deletePermission)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("GroupCat", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //List<GroupMenu>  GroupMenus = TempData["GroupMenus"] as List<GroupMenu>;
            int GroupCatID = (int)TempData["GroupCatID"];
            GroupCat g = GroupCatBusiness.Find(GroupCatID);
            if (g.GroupMenus.Count() > 0)
            {
                g.GroupMenus = new List<GroupMenu>();
                //foreach (GroupMenu gm in g.GroupMenus)
                //{
                // GroupMenuBusiness.DeleteAll(g.GroupMenus);
                //}
            }
            //if (isAccess != null)
            //{
            //    for (int i = 0; i < isAccess.Length; i++)
            //    {
            //        int menuAccessID = isAccess[i];
            //        GroupMenu gm = new GroupMenu { GroupID = GroupCatID, MenuID = menuAccessID };
            //        GroupMenuBusiness.Insert(gm);
            //    }
            //}

            //added by namdv3
            Dictionary<int, int?> ListPermissionMenu = new Dictionary<int, int?>();
            if (viewPermission != null && viewPermission.Count() > 0)
            {
                int delPermis = 0;
                int editPermis = 0;
                int addPermis = 0;
                int key = 0;

                foreach (var MenuID in viewPermission)
                {
                    key = MenuID;                   
                    int? value = 1;

                    if (addPermission != null && addPermission.Count() > 0)
                    {
                        addPermis = addPermission.Where(o => o == MenuID).FirstOrDefault();
                        if (addPermis > 0)
                        {
                            value = 2;
                        }
                    }
                    if (editPermission != null && editPermission.Count() > 0)
                    {
                        editPermis = editPermission.Where(o => o == MenuID).FirstOrDefault();
                        if (editPermis > 0)
                        {
                            value = 3;
                        }
                    }
                    if (deletePermission != null && deletePermission.Count() > 0)
                    {
                        delPermis = deletePermission.Where(o => o == MenuID).FirstOrDefault();
                        if (delPermis > 0)
                        {
                            value = 4;
                        }
                    }
                    ListPermissionMenu.Add(key, value);
                }
            }
            GroupCatBusiness.AssignMenu(GroupCatID, ListPermissionMenu);
            GroupCatBusiness.Save();

            return RedirectToAction("Index", new { FromSavePriv = 1 });
        }

        private void MapPermission(List<Menu> MenuOfRole, IQueryable<GroupMenu> GroupMenus, List<ItemMenuModel> LsMenuModel)
        {
            //Voi moi Menu duoc gan cho Group
            //Neu la danh muc thi cap nhat Permission cua ItemMenuModel tuong ung voi MenuID 
            //Neu ko la danh muc thi cap nhat isAccess cua ItemMenuModel tuong ung voi MenuID
            List<GroupMenu> listGroupMemu = GroupMenus.ToList();
            if (listGroupMemu != null && listGroupMemu.Count > 0)
            {                
                foreach (GroupMenu gm in GroupMenus.ToList())
                {
                    //if (gm.Menu.IsCategory)
                    //{
                    foreach (ItemMenuModel itemMenuModel in LsMenuModel)
                    {
                        if (gm.MenuID == itemMenuModel.ItemMenuID)
                        {
                            if (gm.Permission == 4)
                            {
                                itemMenuModel.deletePermission = true;
                                itemMenuModel.editPermission = true;
                                itemMenuModel.addPermission = true;
                                itemMenuModel.viewPermission = true;
                            }
                            else if (gm.Permission == 3)
                            {
                                itemMenuModel.editPermission = true;
                                itemMenuModel.addPermission = true;
                                itemMenuModel.viewPermission = true;
                            }
                            else if (gm.Permission == 2)
                            {
                                itemMenuModel.addPermission = true;
                                itemMenuModel.viewPermission = true;
                            }
                            else if (gm.Permission == 1)
                            {
                                itemMenuModel.viewPermission = true;
                            }
                        }
                        else
                        {
                            foreach (ItemMenuModel itemMenuModel2 in itemMenuModel.MenuChildren)
                            {
                                if (gm.MenuID == itemMenuModel2.ItemMenuID)
                                {
                                    if (gm.Permission == 4)
                                    {
                                        itemMenuModel2.deletePermission = true;
                                        itemMenuModel2.editPermission = true;
                                        itemMenuModel2.addPermission = true;
                                        itemMenuModel2.viewPermission = true;
                                    }
                                    else if (gm.Permission == 3)
                                    {
                                        itemMenuModel2.editPermission = true;
                                        itemMenuModel2.addPermission = true;
                                        itemMenuModel2.viewPermission = true;
                                    }
                                    else if (gm.Permission == 2)
                                    {
                                        itemMenuModel2.addPermission = true;
                                        itemMenuModel2.viewPermission = true;
                                    }
                                    else if (gm.Permission == 1)
                                    {
                                        itemMenuModel2.viewPermission = true;
                                    }
                                }
                                else
                                {
                                    foreach (ItemMenuModel itemMenuModel3 in itemMenuModel2.MenuChildren)
                                    {
                                        if (gm.MenuID == itemMenuModel3.ItemMenuID)
                                        {
                                            if (gm.Permission == 4)
                                            {
                                                itemMenuModel3.deletePermission = true;
                                                itemMenuModel3.editPermission = true;
                                                itemMenuModel3.addPermission = true;
                                                itemMenuModel3.viewPermission = true;
                                            }
                                            else if (gm.Permission == 3)
                                            {
                                                itemMenuModel3.editPermission = true;
                                                itemMenuModel3.addPermission = true;
                                                itemMenuModel3.viewPermission = true;
                                            }
                                            else if (gm.Permission == 2)
                                            {
                                                itemMenuModel3.addPermission = true;
                                                itemMenuModel3.viewPermission = true;
                                            }
                                            else if (gm.Permission == 1)
                                            {
                                                itemMenuModel3.viewPermission = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //}
                    }
                }
            }
        }


        #region private function
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<GroupCatViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("GroupCat", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<GroupCat> query = this.GroupCatBusiness.Search(SearchInfo);
            return MapToListViewModel(query.ToList());
        }

        private void SetViewData()
        {
            IQueryable<RoleViewModel> listAllRoles = RoleBusiness.GetAll().Select(r => new RoleViewModel { RoleID = r.RoleID, RoleName = r.RoleName }).OrderBy(r => r.RoleID);
            ViewData[GroupCatConstants.LS_ROLES_SEARCH] = new SelectList(listAllRoles, "RoleID", "RoleName");
            ViewData[GroupCatConstants.LS_ROLES_GRID] = listAllRoles;
        }

        private List<ItemMenuModel> getListItemMenu(List<Menu> lsMenu)
        {
            //List<Menu> lsMenuTmp = this.MenuBusiness.All.Where(m => m.IsActive == true).ToList();
            MenuBuilder menuBuilder = new MenuBuilder(lsMenu);
            List<ItemMenuModel> menuModelLevel1 = menuBuilder.GetParentMenu().Where(p => p.MenuChildren.Count > 0).ToList();
            return menuModelLevel1;
        }
        #endregion

        #region Mapper
        private GroupCatViewModel MapToViewModel(GroupCat group)
        {
            GroupCatViewModel GroupCatViewModel = new GroupCatViewModel();
            Utils.Utils.BindTo(group, GroupCatViewModel);
            return GroupCatViewModel;
        }

        private GroupCat MapBackToModel(GroupCatViewModel group)
        {
            GroupCat GroupCat = new GroupCat();
            Utils.Utils.BindTo(group, GroupCat);
            return GroupCat;
        }

        private List<GroupCatViewModel> MapToListViewModel(List<GroupCat> groups)
        {
            List<GroupCatViewModel> groupsViewModel = new List<GroupCatViewModel>();
            foreach (var group in groups)
            {
                GroupCatViewModel groupViewItem = MapToViewModel(group);
                groupViewItem.RoleName = group.Role.RoleName;
                groupsViewModel.Add(groupViewItem);
            }
            return groupsViewModel;
        }

        private List<GroupCat> MapBackToListModel(List<GroupCatViewModel> groups)
        {
            List<GroupCat> g = new List<GroupCat>();
            foreach (var group in groups)
            {
                GroupCat groupModel = MapBackToModel(group);
                g.Add(groupModel);
            }
            return g;
        }
        #endregion
    }
}





