﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.AcademicYearOfProvinceArea.Models;
using System.Configuration;

namespace SMAS.Web.Areas.AcademicYearOfProvinceArea.Controllers
{
    public class AcademicYearOfProvinceController : BaseController
    {
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IProvinceSubjectBusiness ProvinceSubjectBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        public AcademicYearOfProvinceController(IPeriodDeclarationBusiness PeriodDeclarationBusiness, IAcademicYearOfProvinceBusiness academicyearofprovinceBusiness, IProvinceSubjectBusiness ProvinceSubjectBusiness)
        {
            this.AcademicYearOfProvinceBusiness = academicyearofprovinceBusiness;
            this.ProvinceSubjectBusiness = ProvinceSubjectBusiness;

        }

        //
        // GET: /AcademicYearOfProvince/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here

            IEnumerable<AcademicYearOfProvinceViewModel> lst = this._Search(SearchInfo);

            if (lst == null || lst.Count() == 0)
            {
                lst = new List<AcademicYearOfProvinceViewModel>();
            }
            ViewData[AcademicYearOfProvinceConstants.LIST_ACADEMICYEAROFPROVINCE] = lst;
            SetViewDataPermission("AcademicYearOfProvince", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return View();
        }

        //
        // GET: /AcademicYearOfProvince/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            SetViewDataPermission("AcademicYearOfProvince", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            //

            IEnumerable<AcademicYearOfProvinceViewModel> lst = this._Search(SearchInfo);
            ViewData[AcademicYearOfProvinceConstants.LIST_ACADEMICYEAROFPROVINCE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        private AcademicYearOfProvinceViewModel PrepareCreate()
        {
            AcademicYearOfProvinceViewModel frm = new AcademicYearOfProvinceViewModel();

            String StringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"] + DateTime.Now.Year.ToString();
            String StringFirstEndDate = ConfigurationManager.AppSettings["EndFirstAcademicYear"] + (DateTime.Now.Year + 1).ToString();
            String StringSecondStartDate = ConfigurationManager.AppSettings["StartSecondAcademicYear"] + (DateTime.Now.Year + 1).ToString();
            String StringSecondEndDate = ConfigurationManager.AppSettings["EndSecondAcademicYear"] + (DateTime.Now.Year + 1).ToString();
            DateTime FirstStartDate = DateTime.Parse(StringFirstStartDate);
            DateTime FirstEndDate = DateTime.Parse(StringFirstEndDate);
            DateTime SecondStartDate = DateTime.Parse(StringSecondStartDate);
            DateTime SecondEndDate = DateTime.Parse(StringSecondEndDate);

            frm.Year = (int)DateTime.Now.Year;
            frm.Year2 = (int)DateTime.Now.Year + 1;
            frm.FirstSemesterStartDate = FirstStartDate;
            frm.FirstSemesterEndDate = FirstEndDate;
            frm.SecondSemesterStartDate = SecondStartDate;
            frm.SecondSemesterEndDate = SecondEndDate;
            frm.IsInheritData = false;
            frm.IsSchoolEdit = true;
            frm.Disabled = "";

            IQueryable<AcademicYearOfProvince> listAcademicYearOfProvince = AcademicYearOfProvinceBusiness.SearchByProvince(new GlobalInfo().ProvinceID.Value, new Dictionary<string, object>() { }).Where(u => u.Year < frm.Year).OrderByDescending(o => o.Year);

            if (listAcademicYearOfProvince == null)

                return null;
            else
            {
                //ProvinceSubjectBusiness.SearchByProvince(entity.Province, Dic)
                //với Dic[“AcademicYearOfProvinceID”] = aca.AcademicYearOfProvinceID
                //Lấy danh sách ProvinceSubject  tìm được. Tạo bản ghi mới thay thế aca.AcademicYearOfProvinceID = entity.AcademicYearOfProvince rồi insert vào bảng ProvinceSubject

                IDictionary<string, object> dic = new Dictionary<string, object>();
                if (listAcademicYearOfProvince != null && listAcademicYearOfProvince.Count() > 0)
                {
                    AcademicYearOfProvince entity = listAcademicYearOfProvince.FirstOrDefault();
                    if (entity != null)
                    {
                        dic["AcademicYearOfProvinceID"] = entity.AcademicYearOfProvinceID;
                        IQueryable<ProvinceSubject> listProvinceSubject = ProvinceSubjectBusiness.SearchByProvince(new GlobalInfo().ProvinceID.Value, dic);
                        if (listProvinceSubject != null)
                        {
                        }
                        else
                        {
                            frm.Disabled = "disabled";
                        }
                    }

                }


            }


            return frm;
        }

        private AcademicYearOfProvinceViewModel PrepareEdit(int id)
        {
            //this.CheckPermissionForAction(id, "AcademicYearOfProvince", 5);
            AcademicYearOfProvinceViewModel frm = new AcademicYearOfProvinceViewModel();

            AcademicYearOfProvince AcademicYearOfProvince = AcademicYearOfProvinceBusiness.Find(id);
            frm.AcademicYearOfProvinceID = AcademicYearOfProvince.AcademicYearOfProvinceID;
            frm.Year = AcademicYearOfProvince.Year.Value;
            frm.Year2 = AcademicYearOfProvince.Year.Value + 1;
            frm.FirstSemesterStartDate = AcademicYearOfProvince.FirstSemesterStartDate.Value;
            frm.FirstSemesterEndDate = AcademicYearOfProvince.FirstSemesterEndDate;
            frm.SecondSemesterStartDate = AcademicYearOfProvince.SecondSemesterStartDate;
            frm.SecondSemesterEndDate = AcademicYearOfProvince.SecondSemesterEndDate;
            frm.IsInheritData = AcademicYearOfProvince.IsInheritData == null ? false : AcademicYearOfProvince.IsInheritData;
            frm.IsSchoolEdit = AcademicYearOfProvince.IsSchoolEdit == null ? false : AcademicYearOfProvince.IsSchoolEdit;
            frm.Disabled = "disabled";
            return frm;
        }

        /**
         * Hien thi trang Tao moi
         **/
        [HttpGet]
        public ActionResult Create()
        {
            return View(PrepareCreate());
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            this.CheckPermissionForAction(id, "AcademicYearOfProvince", 5);
            return View("_Edit", PrepareEdit(id));
        }

        #region action
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Create(AcademicYearOfProvinceViewModel form)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("AcademicYearOfProvince", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            AcademicYearOfProvince ay = new AcademicYearOfProvince();

            //Đổ dữ liệu
            TryUpdateModel(ay);
            Utils.Utils.TrimObject(ay);
            GlobalInfo global = new GlobalInfo();
            //Lấy ra năm học cũ trên sở
            AcademicYearOfProvince AcademicYearOfProLast = AcademicYearOfProvinceBusiness.All.Where(o => o.ProvinceID == global.ProvinceID && o.Year < form.Year).OrderByDescending(o => o.Year).FirstOrDefault();


            List<object> Params = new List<object>();
            Params.Add(ay.Year.ToString());
            Params.Add((ay.Year + 1).ToString());
            //Năm học sau khi sửa phải nằm trong khoảng năm học hiển thị
            if (form.FirstSemesterStartDate.Value.Year < ay.Year.Value || form.FirstSemesterStartDate.Value.Year > (ay.Year + 1))
            {
                throw new BusinessException("Validate_StartDate_Year", Params);
            }

            if (form.SecondSemesterEndDate.Value.Year < ay.Year.Value || form.SecondSemesterEndDate.Value.Year > (ay.Year + 1))
            {
                throw new BusinessException("Validate_SecondDate_Year", Params);
            }

            //So sánh ngày bắt đầu của năm học với ngày kết thúc của năm học trước
            DateTime FirstSemesterStartDateY = form.FirstSemesterStartDate.Value;
            if (AcademicYearOfProLast != null)
            {
                DateTime SecondEndDate = AcademicYearOfProLast.SecondSemesterEndDate.Value;
                if (FirstSemesterStartDateY <= SecondEndDate)
                {
                    return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_EditYearErrorMessage"), "error"));
                }
            }
            var checkDuplicate = this.AcademicYearOfProvinceBusiness.SearchByProvince(new GlobalInfo().ProvinceID.Value, new Dictionary<string, object>() { { "Year", ay.Year }, { "IsActive", true } });
            if (checkDuplicate != null && checkDuplicate.Count() > 0)
            {
                return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_DuplicateYear"), "error"));
            }
            if (form.IsInheritData == null)
            {
                ay.IsInheritData = false;
            }
            else
            {
                ay.IsInheritData = true;
            }

            if (form.IsSchoolEdit == null)
            {
                ay.IsSchoolEdit = false;
            }
            else
            {
                ay.IsSchoolEdit = true;
            }
            ay.ProvinceID = new GlobalInfo().ProvinceID.Value;
            ay.IsActive = true;
            AcademicYearOfProvinceBusiness.Insert(ay);


            this.AcademicYearOfProvinceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int AcademicYearOfProvinceID, AcademicYearOfProvinceViewModel form)
        {
            this.CheckPermissionForAction(AcademicYearOfProvinceID, "AcademicYearOfProvince", 5);
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("AcademicYearOfProvince", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            AcademicYearOfProvince academicyearofprovince = this.AcademicYearOfProvinceBusiness.Find(AcademicYearOfProvinceID);
            GlobalInfo global = new GlobalInfo();
            //Nam hoc truoc
            AcademicYearOfProvince AcademicYearOfProLast = AcademicYearOfProvinceBusiness.All.Where(o => o.ProvinceID == global.ProvinceID && o.Year < academicyearofprovince.Year).OrderByDescending(o => o.Year).FirstOrDefault();

            List<object> Params = new List<object>();
            Params.Add(academicyearofprovince.Year.ToString());
            Params.Add((academicyearofprovince.Year + 1).ToString());
            //Năm học sau khi sửa phải nằm trong khoảng năm học hiển thị
            if (form.FirstSemesterStartDate.Value.Year < academicyearofprovince.Year.Value || form.FirstSemesterStartDate.Value.Year > (academicyearofprovince.Year + 1))
            {
                throw new BusinessException("Validate_StartDate_Year", Params);
            }

            if (form.SecondSemesterEndDate.Value.Year < academicyearofprovince.Year.Value || form.SecondSemesterEndDate.Value.Year > (academicyearofprovince.Year + 1))
            {
                throw new BusinessException("Validate_SecondDate_Year", Params);
            }

            //So sánh ngày bắt đầu của năm học với ngày kết thúc của năm học trước
            if (form.FirstSemesterStartDate == null)
            {
                return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_FirstSemesterStartDateEmpty"), "error"));
            }
            if (form.FirstSemesterEndDate == null)
            {
                return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_FirstSemesterEndDateEmpty"), "error"));
            }
            if (form.SecondSemesterStartDate == null)
            {
                return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_SecondSemesterStartDateEmpty"), "error"));
            }
            if (form.SecondSemesterEndDate == null)
            {
                return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_SecondSemesterEndDateEmpty"), "error"));
            }
            //Ngay bat dau ki 1
            DateTime FirstSemesterStartDateY = form.FirstSemesterStartDate.Value;
            if (AcademicYearOfProLast != null)
            {
                //Ngay ket thuc cua kia 2 nam hoc truoc
                DateTime SecondEndDate = AcademicYearOfProLast.SecondSemesterEndDate.Value;
                if (FirstSemesterStartDateY < SecondEndDate)
                {
                    throw new BusinessException("AcademicYearOfProvince_Validate_FirstYear");
                }
            }
            //Ngay ket thuc cua hoc ki 2 phai nho hon ngay bat dau cua nam hoc sau
            DateTime SecondSemesterEndDateY = form.SecondSemesterEndDate.Value;
            AcademicYearOfProvince AcademicYearOfProSecond = AcademicYearOfProvinceBusiness.All.Where(o => o.ProvinceID == global.ProvinceID && o.Year > academicyearofprovince.Year).OrderBy(o => o.Year).FirstOrDefault();
            if (AcademicYearOfProSecond != null)
            {
                DateTime SecondStartDate = AcademicYearOfProSecond.FirstSemesterStartDate.Value;
                if (SecondSemesterEndDateY > SecondStartDate)
                {
                    throw new BusinessException("AcademicYearOfProvince_Validate_SecondYear");
                }
            }


            if (academicyearofprovince.IsActive == false)
            {
                return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_EditErrorMessage")));
            }
            TryUpdateModel(academicyearofprovince);
            if (academicyearofprovince.IsActive == false)
            {
                return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_EditErrorMessage"), "error"));
            }
            var checkDuplicate = this.AcademicYearOfProvinceBusiness.SearchByProvince(new GlobalInfo().ProvinceID.Value, new Dictionary<string, object>() { { "Year", academicyearofprovince.Year }, { "IsActive", true } }).Where(o => o.AcademicYearOfProvinceID != academicyearofprovince.AcademicYearOfProvinceID);
            if (checkDuplicate != null && checkDuplicate.Count() > 0)
            {
                return Json(new JsonMessage(Res.Get("AcademicYearOfProvince_Label_DuplicateYear"), "error"));
            }

            Utils.Utils.TrimObject(academicyearofprovince);


            if (form.IsSchoolEdit == null)
            {
                academicyearofprovince.IsSchoolEdit = false;
            }
            else
            {
                academicyearofprovince.IsSchoolEdit = true;
            }

            academicyearofprovince.ProvinceID = new GlobalInfo().ProvinceID.Value;
            academicyearofprovince.IsActive = true;

            Utils.Utils.TrimObject(academicyearofprovince);
            this.AcademicYearOfProvinceBusiness.Update(academicyearofprovince);
            this.AcademicYearOfProvinceBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "AcademicYearOfProvince", 5);
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("AcademicYearOfProvince", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.AcademicYearOfProvinceBusiness.Delete(id, new GlobalInfo().ProvinceID.Value);
            this.AcademicYearOfProvinceBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<AcademicYearOfProvinceViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            int provinceID = global.ProvinceID.Value;
            IQueryable<AcademicYearOfProvince> query = AcademicYearOfProvinceBusiness.SearchByProvince(provinceID, SearchInfo);
            if (query != null && query.Count() > 0)
            {
                query = query.OrderByDescending(o => o.Year).ThenByDescending(o => o.DisplayTitle);
            }
            else
            {
                return null;
            }
            IQueryable<AcademicYearOfProvinceViewModel> lst = query.Select(o => new AcademicYearOfProvinceViewModel
            {
                AcademicYearOfProvinceID = o.AcademicYearOfProvinceID,
                ProvinceID = o.ProvinceID,
                DisplayTitle = o.DisplayTitle,
                Year = o.Year.Value,
                FirstSemesterStartDate = o.FirstSemesterStartDate.Value,
                FirstSemesterEndDate = o.FirstSemesterEndDate.Value,
                SecondSemesterStartDate = o.SecondSemesterStartDate.Value,
                SecondSemesterEndDate = o.SecondSemesterEndDate.Value,
                IsInheritData = o.IsInheritData,
                IsSchoolEdit = o.IsSchoolEdit,
                IsActive = o.IsActive
            });

            return lst.ToList();
        }
    }
}





