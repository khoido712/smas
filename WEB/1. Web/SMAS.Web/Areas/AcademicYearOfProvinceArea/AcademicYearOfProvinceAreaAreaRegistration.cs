﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.AcademicYearOfProvinceArea
{
    public class AcademicYearOfProvinceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AcademicYearOfProvinceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AcademicYearOfProvinceArea_default",
                "AcademicYearOfProvinceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
