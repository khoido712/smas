/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.AcademicYearOfProvinceArea
{
    public class AcademicYearOfProvinceConstants
    {
        public const string LIST_ACADEMICYEAROFPROVINCE = "listAcademicYearOfProvince";


    }
}