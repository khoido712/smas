/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
namespace SMAS.Web.Areas.AcademicYearOfProvinceArea.Models
{
    public class AcademicYearOfProvinceViewModel
    {
        [ResourceDisplayName("AcademicYearOfProvince_Label_AcademicYearOfProvinceID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
		public System.Int32 AcademicYearOfProvinceID { get; set; }

        [ResourceDisplayName("AcademicYearOfProvince_Label_ProvinceID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
		public System.Int32 ProvinceID { get; set; }		
						
		public System.String DisplayTitle { get; set; }

        [ResourceDisplayName("AcademicYearOfProvince_Label_Year")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]					
		public int Year { get; set; }

        
        public int Year2 { get; set; }

        [ResourceDisplayName("AcademicYearOfProvince_Label_FirstSemesterStartDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[DataConstraint(DataConstraintAttribute.LESS, "FirstSemesterEndDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
		public DateTime? FirstSemesterStartDate { get; set; }

        [ResourceDisplayName("AcademicYearOfProvince_Label_FirstSemesterEndDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.GREATER, "FirstSemesterStartDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        //[DataConstraint(DataConstraintAttribute.LESS, "SecondSemesterStartDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? FirstSemesterEndDate { get; set; }

        [ResourceDisplayName("AcademicYear_Label_SecondSemesterStartDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.GREATER, "FirstSemesterEndDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
       //[DataConstraint(DataConstraintAttribute.LESS, "SecondSemesterEndDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? SecondSemesterStartDate { get; set; }

        [ResourceDisplayName("AcademicYear_Label_SecondSemesterEndDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataConstraint(DataConstraintAttribute.GREATER, "SecondSemesterStartDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? SecondSemesterEndDate { get; set; }

        public string Disabled { get; set; }

		public System.Nullable<System.Boolean> IsInheritData { get; set; }								
		public System.Nullable<System.Boolean> IsSchoolEdit { get; set; }								
		public System.Nullable<System.Boolean> IsActive { get; set; }								
	       
    }
}


