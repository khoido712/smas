﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HonourAchivementTeacherArea
{
    public class HonourAchivementTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HonourAchivementTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HonourAchivementTeacherArea_default",
                "HonourAchivementTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
