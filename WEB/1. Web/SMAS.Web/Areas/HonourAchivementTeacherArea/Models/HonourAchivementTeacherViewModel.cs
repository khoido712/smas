﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using Resources;

namespace SMAS.Web.Areas.HonourAchivementTeacherArea.Models
{
    public class HonourAchivementTeacherViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 HonourAchivementID { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 AcademicYearID { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 SchoolID { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.Int32> ObjectID { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> AchivedDate { get; set; }
        [ScaffoldColumn(false)]
        public string Name { get; set; }


        [ScaffoldColumn(false)]
        [ResourceDisplayName("HonourAchivement_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FacultyName
        {
            get;
            set;
        }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("HonourAchivement_Label_HonourAchivementType")]
        public string Resolution { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("HonourAchivement_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("HonourAchivement_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("HonourAchivement_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HonourAchivementTeacherConstants.LIST_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacherOfFaculty(this)")]
        public int SchoolFacultyID { get; set; }

        [ResourceDisplayName("HonourAchivement_Label_TeacherOfFaculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HonourAchivementTeacherConstants.LIST_EMPLOYEE)]
        public int TeacherOfFacultyID { get; set; }

        [ResourceDisplayName("HonourAchivement_Label_HonourAchivementType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HonourAchivementTeacherConstants.LIST_HONOUR_ACHIVEMENT_TYPE)]
        public int HonourAchivementTypeID { get; set; }

        //[ResDisplayName("TeacherGrading_Label_TeacherGrade")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", TeacherGradingConstants.LIST_TEACHERGRADE)]
        //public int TeacherGradeID { get; set; }

        [ResourceDisplayName("HonourAchivement_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}
