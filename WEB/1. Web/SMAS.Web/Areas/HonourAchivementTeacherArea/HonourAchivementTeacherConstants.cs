﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HonourAchivementTeacherArea
{
    public class HonourAchivementTeacherConstants
    {
        public const string LIST_HONOURACHIVEMENT = "listHonourAchivement";
        public const string LIST_FACULTY = "listFaculty";
        public const string LIST_TEACHEROFFACULTY = "listTeacherOfFaculty";
        public const string ISYEAR = "ISYEAR";
        public const string LIST_HONOUR_ACHIVEMENT_TYPE = "listHonourAchivementType";
        public const string LIST_EMPLOYEE = "listEmployee";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
    }
}