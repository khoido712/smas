﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.BusinessExtensions;
using SMAS.Web.Areas.HonourAchivementTeacherArea.Models;

namespace SMAS.Web.Areas.HonourAchivementTeacherArea.Controllers
{
    public class HonourAchivementTeacherController : BaseController
    {
        private readonly IHonourAchivementBusiness HonourAchivementBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IHonourAchivementTypeBusiness HonourAchivementTypeBusiness;
        public HonourAchivementTeacherController(IHonourAchivementTypeBusiness honourAchivementTypeBusiness, ISchoolFacultyBusiness schoolFacultyBusiness, IHonourAchivementBusiness honourachivementBusiness, IEmployeeBusiness employeeBusiness)
        {
            this.HonourAchivementBusiness = honourachivementBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.HonourAchivementTypeBusiness = honourAchivementTypeBusiness;
        }

        //
        // GET: /HonourAchivementTeacher/

        public ActionResult Index()
        {
            SetViewData();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //Get view data here
            GlobalInfo Global = new GlobalInfo();
            SearchInfo["AcademicYearID"] = Global.AcademicYearID;
            IEnumerable<HonourAchivementTeacherViewModel> lst = this._Search(SearchInfo);
            ViewData[HonourAchivementTeacherConstants.LIST_HONOURACHIVEMENT] = lst;
            ViewData[HonourAchivementTeacherConstants.ISYEAR] = true;
            return View();
        }

        //
        // GET: /HonourAchivement/Search

        
        public PartialViewResult Search(int? FacultyID)
        {
            //Utils.Utils.TrimObject(frm);
            if (FacultyID == null)
            { FacultyID = 0; }
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            //
            SearchInfo["AcademicYearID"] = Global.AcademicYearID;
            SearchInfo["FacultyID"] = FacultyID;
            //SearchInfo["AppliedLevel"] = Global.AppliedLevel;
            IEnumerable<HonourAchivementTeacherViewModel> lst = this._Search(SearchInfo);
            ViewData[HonourAchivementTeacherConstants.LIST_HONOURACHIVEMENT] = lst;
            //Get view data here

            ViewData[HonourAchivementTeacherConstants.ISYEAR] = false;
            if (!Global.IsCurrentYear)
            {
                ViewData[HonourAchivementTeacherConstants.ISYEAR] = true;
            }
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>


        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(HonourAchivementTeacherViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("HonourAchivement", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            HonourAchivement honour = new HonourAchivement();
            honour.HonourAchivementTypeID = (int)frm.HonourAchivementTypeID;
            honour.EmployeeID = frm.TeacherOfFacultyID;
            honour.FacultyID = frm.SchoolFacultyID;
            honour.Description = frm.Description;
            honour.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            honour.SchoolID = GlobalInfo.SchoolID.Value;
            IDictionary<string, object> EmployeeSearchInfo = new Dictionary<string, object>();
            EmployeeSearchInfo["EmployeeID"] = frm.TeacherOfFacultyID;
            EmployeeSearchInfo["FacultyID"] = frm.SchoolFacultyID;
            EmployeeSearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            List<HonourAchivement> lstHonourAchivementEmployee = HonourAchivementBusiness.SearchHonourAchivementForEmployee(GlobalInfo.SchoolID.Value, EmployeeSearchInfo).ToList();
            if (lstHonourAchivementEmployee.Where(o => o.HonourAchivementTypeID == honour.HonourAchivementTypeID).Count() > 0)
            {
                HonourAchivementType honourAchivementType = HonourAchivementTypeBusiness.Find((int)frm.HonourAchivementTypeID);
                string str = Res.Get("HonourAchivementTeacher_Label_DuplicateHonourAchivement") + " " + honourAchivementType.Resolution;
                return Json(new JsonMessage(str, JsonMessage.ERROR));
            }
            this.HonourAchivementBusiness.InsertHonourAchivementForEmployee(honour);
            this.HonourAchivementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));

        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int HonourAchivementID)
        {
            this.CheckPermissionForAction(HonourAchivementID, "HonourAchivement");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("HonourAchivement", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            HonourAchivement honourachivement = this.HonourAchivementBusiness.Find(HonourAchivementID);
            TryUpdateModel(honourachivement);
            Utils.Utils.TrimObject(honourachivement);
            IDictionary<string, object> EmployeeSearchInfo = new Dictionary<string, object>();
            EmployeeSearchInfo["EmployeeID"] = honourachivement.EmployeeID;
            EmployeeSearchInfo["FacultyID"] = honourachivement.FacultyID;
            EmployeeSearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            List<HonourAchivement> lstHonourAchivementEmployee = HonourAchivementBusiness.SearchHonourAchivementForEmployee(GlobalInfo.SchoolID.Value, EmployeeSearchInfo).ToList();
            if (lstHonourAchivementEmployee.Where(o => o.HonourAchivementTypeID == honourachivement.HonourAchivementTypeID && o.HonourAchivementID != HonourAchivementID).Count() > 0)
            {
                HonourAchivementType honourAchivementType = HonourAchivementTypeBusiness.Find(honourachivement.HonourAchivementTypeID);
                string str = Res.Get("HonourAchivementTeacher_Label_DuplicateHonourAchivement") + " " + honourAchivementType.Resolution;
                return Json(new JsonMessage(str, JsonMessage.ERROR));
            }
            this.HonourAchivementBusiness.UpdateHonourAchivementForEmployee(honourachivement);
            this.HonourAchivementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "HonourAchivement");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("HonourAchivement", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.HonourAchivementBusiness.DeleteHonourAchivementForEmployee(new GlobalInfo().SchoolID.Value, id);
            this.HonourAchivementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<HonourAchivementTeacherViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[HonourAchivementTeacherConstants.HAS_PERMISSION] = GetMenupermission("HonourAchivementTeacher", global.UserAccountID, global.IsAdmin);
            IQueryable<HonourAchivement> query = from p in this.HonourAchivementBusiness.SearchHonourAchivementForEmployee(new GlobalInfo().SchoolID.Value, SearchInfo)
                                                 join q in EmployeeBusiness.All on p.EmployeeID equals q.EmployeeID
                                                 where q.IsActive == true
                                                 select p;
            List<HonourAchivementTeacherViewModel> lst = query.Select(o => new HonourAchivementTeacherViewModel
            {
                HonourAchivementID = o.HonourAchivementID,
                AcademicYearID = o.AcademicYearID,
                SchoolID = o.SchoolID,
                HonourAchivementTypeID = o.HonourAchivementTypeID,
                SchoolFacultyID = o.FacultyID,
                TeacherOfFacultyID = o.EmployeeID.Value,
                AchivedDate = o.AchivedDate,
                Description = o.Description,
                Resolution = o.HonourAchivementType.Resolution,
                EmployeeCode = o.Employee.EmployeeCode,
                FullName = o.Employee.FullName,
                FacultyName = o.SchoolFaculty.FacultyName,
                Name= o.Employee.Name
            }).ToList();
            List<HonourAchivementTeacherViewModel> list = new List<HonourAchivementTeacherViewModel>();
            if (lst.Count() > 0)
            {
                list = lst.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
            }
            return list;
        }

        #region SetViewData
        private void SetViewData()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            this._Search(dic);
            GlobalInfo GlobalInfo = new GlobalInfo();


            //Load combobox SchoolFaculty
            IDictionary<string, object> FacultySearchInfo = new Dictionary<string, object>();
            FacultySearchInfo["IsActive"] = true;

            FacultySearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            List<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, FacultySearchInfo).OrderBy(o => o.FacultyName).ToList();
            ViewData[HonourAchivementTeacherConstants.LIST_FACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");

            //Load Grid HonourAchivement
            IDictionary<string, object> HonourInfo = new Dictionary<string, object>();
            HonourInfo["IsActive"] = true;
           // HonourInfo["AppliedLevel"] = GlobalInfo.AppliedLevel;
            IEnumerable<HonourAchivementTeacherViewModel> lst = this._Search(HonourInfo);
            ViewData[HonourAchivementTeacherConstants.LIST_HONOURACHIVEMENT] = lst;

            //cbb HonourAchivementType
            IDictionary<string, object> HonourTypeInfo = new Dictionary<string, object>();
            HonourTypeInfo["IsActive"] = true;
            HonourTypeInfo["Type"] = SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_EMPLOYEE;
            List<HonourAchivementType> lsHonourType = HonourAchivementTypeBusiness.Search(HonourTypeInfo).OrderBy(o => o.Resolution).ToList();
            ViewData[HonourAchivementTeacherConstants.LIST_HONOUR_ACHIVEMENT_TYPE] = new SelectList(lsHonourType, "HonourAchivementTypeID", "Resolution");

            //cbb Employee



        }
        #endregion

        #region Load Combobox

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadTeacherOfFaculty(int? SchoolFacultyId)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            IEnumerable<Employee> lst = new List<Employee>();
            if (SchoolFacultyId.ToString() != "")
            {

                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(GlobalInfo.SchoolID.Value, SchoolFacultyId.Value).OrderBy(o => o.Name).ThenBy(o => o.FullName);
            }
            if (lst == null)
                lst = new List<Employee>();

           // lst = lst.Where(o => o.AppliedLevel == GlobalInfo.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}





