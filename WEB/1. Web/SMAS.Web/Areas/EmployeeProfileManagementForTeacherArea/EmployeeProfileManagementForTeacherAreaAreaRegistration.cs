﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea
{
    public class EmployeeProfileManagementForTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeProfileManagementForTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeProfileManagementForTeacherArea_default",
                "EmployeeProfileManagementForTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
