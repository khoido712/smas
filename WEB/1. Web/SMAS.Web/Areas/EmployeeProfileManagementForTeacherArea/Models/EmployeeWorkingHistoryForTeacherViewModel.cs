﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Models
{
    public class EmployeeWorkingHistoryForTeacherViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Employee Working History")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EmployeeWorkingHistoryID { get; set; }

        [ScaffoldColumn(false)]
        [DisplayName("Employee")]
        public Nullable<int> EmployeeID { get; set; }

        [ScaffoldColumn(false)]
        [DisplayName("Supervising Dept")]
        public Nullable<int> SupervisingDeptID { get; set; }

        [ScaffoldColumn(false)]
        [DisplayName("School")]
        public Nullable<int> SchoolID { get; set; }

        #region nhung truong hien tren grid

        [ResourceDisplayName("Employee_Column_FullName")]
        [UIHint("Display")]
        public string EmployeeName { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_Organization")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Organization { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Resolution { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_Department")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Department { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_Position")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Position { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_FromDate")]
        [UIHint("DateTimePicker")]
        //[DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public Nullable<System.DateTime> FromDate { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_EndDate")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ToDate { get; set; }

        #endregion nhung truong hien tren grid
    }
}