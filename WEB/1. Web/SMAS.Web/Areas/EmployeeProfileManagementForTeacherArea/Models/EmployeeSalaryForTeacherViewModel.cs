﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Models
{
    public class EmployeeSalaryForTeacherViewModel
    {
        [ScaffoldColumn(false)]
        public int EmployeeSalaryID { get; set; }

        [ScaffoldColumn(false)]
        public int EmployeeID { get; set; }

        [ResourceDisplayName("Salary_Label_EmployeeName")]
        [UIHint("Display")]
        public string EmployeeName { get; set; }

        [ResourceDisplayName("Salary_Label_Scale")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeSalaryForTeacherConstants.LIST_SALARYSCALE)]
        [AdditionalMetadata("OnChange", "AjaxLoadCoefficient(this)")]
        public Nullable<int> EmployeeScaleID { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<int> SalaryLevelID { get; set; }

        [ResourceDisplayName("Salary_Label_Scale")]
        [ScaffoldColumn(false)]
        public string EmployeeScaleName { get; set; }

        [ResourceDisplayName("Salary_Label_SalaryLevel")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeSalaryForTeacherConstants.LIST_SALARYLEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadCoefficient(this)")]
        public Nullable<int> SubLevel { get; set; }

        [ResourceDisplayName("Salary_Label_SalaryLevel")]
        [ScaffoldColumn(false)]
        public string SalaryLevelName { get; set; }

        [ResourceDisplayName("SalaryLevel_Label_Coefficient")]
        [UIHint("Display")]
        public decimal? Coefficient { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<int> SchoolID { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<int> SupervisingDeptID { get; set; }

        [ResourceDisplayName("Salary_Label_AppliedDate")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Nullable<DateTime> AppliedDate { get; set; }

        [ResourceDisplayName("Salary_Label_SalaryResolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SalaryResolution { get; set; }

        [ResourceDisplayName("Salary_Label_SalaryAmount")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[Range(0, 100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        //[RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [AdditionalMetadata("class", "interger")]
        public decimal? SalaryAmount { get; set; }

        [ResourceDisplayName("Qualification_Control_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public bool IsActive { get; set; }
    }
}