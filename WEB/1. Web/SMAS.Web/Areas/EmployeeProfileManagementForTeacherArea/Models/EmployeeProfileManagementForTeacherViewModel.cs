﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Models
{
    public class EmployeeProfileManagementForTeacherViewModel
    {
        #region nhung truong ko hien tren grid

        [DisplayName("Employee")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EmployeeID { get; set; }

        [DisplayName("Employee Type")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EmployeeType { get; set; }

        [DisplayName("Employment Status")]
        public Nullable<int> EmploymentStatus { get; set; }

        [ResourceDisplayName("Employee_Label_IntoSchoolDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IntoSchoolDate { get; set; }

        [DisplayName("Supervising Dept")]
        public Nullable<int> SupervisingDeptID { get; set; }

        [DisplayName("School")]
        public Nullable<int> SchoolID { get; set; }

        [DisplayName("Name")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Name { get; set; }

        [DisplayName("Birth Place")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string BirthPlace { get; set; }

        [ResourceDisplayName("Employee_Label_Telephone")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Telephone { get; set; }

        [ResourceDisplayName("Employee_Label_Mobile")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Mobile { get; set; }

        [ResourceDisplayName("Employee_Label_Email")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Email { get; set; }

        [DisplayName("Temp Residental Address")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string TempResidentalAddress { get; set; }

        [ResourceDisplayName("Employee_Label_PermanentResidentalAddress")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PermanentResidentalAddress { get; set; }

        [ResourceDisplayName("Employee_Label_Alias")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Alias { get; set; }

        [DisplayName("Mariage Status")]
        public Nullable<int> MariageStatus { get; set; }

        [DisplayName("Foreign Language Qualification")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ForeignLanguageQualification { get; set; }

        [ResourceDisplayName("Employee_Label_HealthStatus")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string HealthStatus { get; set; }

        [ResourceDisplayName("Employee_Label_JoinedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> JoinedDate { get; set; }

        [ResourceDisplayName("Employee_Label_StartingDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartingDate { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityNumber")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string IdentityNumber { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityIssuedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> IdentityIssuedDate { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityIssuedPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string IdentityIssuedPlace { get; set; }

        [ResourceDisplayName("Employee_Label_HomeTown")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string HomeTown { get; set; }

        [DisplayName("Image")]
        public byte[] Image { get; set; }

        [DisplayName("Employee_Label_DedicatedForYoungLeague")]
        public bool DedicatedForYoungLeague { get; set; }

        [DisplayName("Employee_Label_RegularRefresher")]
        public bool RegularRefresher { get; set; }

        [DisplayName("Family Type")]
        public Nullable<int> FamilyTypeID { get; set; }

        [DisplayName("Staff Position")]
        public Nullable<int> StaffPositionID { get; set; }

        [DisplayName("Ethnic")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> EthnicID { get; set; }

        [DisplayName("Religion")]
        public Nullable<int> ReligionID { get; set; }

        [DisplayName("Graduation Level")]
        public Nullable<int> GraduationLevelID { get; set; }

        [DisplayName("Contract")]
        public Nullable<int> ContractID { get; set; }

        [DisplayName("Qualification Type")]
        public Nullable<int> QualificationTypeID { get; set; }

        [DisplayName("Speciality Cat")]
        public Nullable<int> SpecialityCatID { get; set; }

        [DisplayName("Qualification Level")]
        public Nullable<int> QualificationLevelID { get; set; }

        [DisplayName("I T Qualification Level")]
        public Nullable<int> ITQualificationLevelID { get; set; }

        [DisplayName("Foreign Language Grade")]
        public Nullable<int> ForeignLanguageGradeID { get; set; }

        [DisplayName("Political Grade")]
        public Nullable<int> PoliticalGradeID { get; set; }

        [DisplayName("State Management Grade")]
        public Nullable<int> StateManagementGradeID { get; set; }

        [DisplayName("Educational Management Grade")]
        public Nullable<int> EducationalManagementGradeID { get; set; }

        [ResourceDisplayName("Employee_Label_WorkTypeID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> WorkTypeID { get; set; }


        [ResourceDisplayName("Employee_Label_WorkGroupType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? WorkGroupTypeID { get; set; }

        [DisplayName("Primarily Assigned Subject")]
        public Nullable<int> PrimarilyAssignedSubjectID { get; set; }

        [ResourceDisplayName("SchoolFaculty_Control_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<int> SchoolFacultyID { get; set; }

        [DisplayName("Is Youth Leage Member")]
        public bool IsYouthLeageMember { get; set; }

        [ResourceDisplayName("Employee_Label_YouthLeagueJoinedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> YouthLeagueJoinedDate { get; set; }

        [ResourceDisplayName("Employee_Label_YouthLeagueJoinedPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string YouthLeagueJoinedPlace { get; set; }

        [DisplayName("Is Communist Party Member")]
        public bool IsCommunistPartyMember { get; set; }

        [ResourceDisplayName("Employee_Label_CommunistPartyJoinedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> CommunistPartyJoinedDate { get; set; }

        [ResourceDisplayName("Employee_Label_CommunistPartyJoinedPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string CommunistPartyJoinedPlace { get; set; }

        [DisplayName("Lbl_Is_Syndicate")]
        public bool IsSyndicate { get; set; }

        [ResourceDisplayName("Lbl_Join_Sysdicate_Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> SyndicateDate { get; set; }

        [ResourceDisplayName("Employee_Label_FatherFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FatherFullName { get; set; }

        [DisplayName("Father Birth Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FatherBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_FatherJob")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FatherJob { get; set; }

        [ResourceDisplayName("Employee_Label_FatherWorkingPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FatherWorkingPlace { get; set; }

        [ResourceDisplayName("Employee_Label_MotherFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string MotherFullName { get; set; }

        [DisplayName("Mother Birth Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> MotherBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_MotherJob")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string MotherJob { get; set; }

        [ResourceDisplayName("Employee_Label_MotherWorkingPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string MotherWorkingPlace { get; set; }

        [ResourceDisplayName("Employee_Label_SpouseFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SpouseFullName { get; set; }

        [DisplayName("Spouse Birth Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> SpouseBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_SpouseJob")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SpouseJob { get; set; }

        [ResourceDisplayName("Employee_Label_SpouseWorkingPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SpouseWorkingPlace { get; set; }

        [DisplayName("Lbl_Motel_Outside")]
        public bool MotelRoomOutsite { get; set; }

        [DisplayName("Lbl_Teacher_Duties")]
        public bool TeacherDuties { get; set; }

        [DisplayName("Lbl_Need_Teacher_Duties")]
        public bool NeedTeacherDuties { get; set; }


        [ResourceDisplayName("Employee_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Description { get; set; }

        [DisplayName("Created Date")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [DisplayName("Is Active")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsActive { get; set; }

        [DisplayName("Modified Date")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool Genre { get; set; }

        [ResourceDisplayName("Employee_Label_AppliedLevel")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? AppliedLevel { get; set; }

        [ResourceDisplayName("Employee_Label_ContractType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? ContractTypeID { get; set; }

        [ResourceDisplayName("Employee_Label_GraduationLevel")]
        public Nullable<int> TrainingLevelID { get; set; }

        #endregion nhung truong ko hien tren grid

        #region nhung truong hien len tren grid

        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string EmployeeCode { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> BirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string Sex { get; set; }

        [ResourceDisplayName("SchoolFaculty_Control_FacultyName")]
        public string SchoolFacultyName { get; set; }

        [ResourceDisplayName("Employee_Label_WorkType")]
        public string WorkTypeName { get; set; }

        [ResourceDisplayName("Employee_Label_Contract")]
        public string ContractName { get; set; }

        [ResourceDisplayName("Employee_Label_GraduationLevel")]
        public string TrainingLevelName { get; set; }

        [ResourceDisplayName("Employee_Label_EmploymentStatus")]
        public string EmploymentStatusName { get; set; }

        [ResourceDisplayName("Employee_Label_Religion")]
        public string ReligionName { get; set; }

        [ResourceDisplayName("Employee_Label_Ethnic")]
        public string EthnicName { get; set; }

        [ResourceDisplayName("Employee_Label_FamilyType")]
        public string FamilyTypeName { get; set; }

        [ResourceDisplayName("Employee_Label_QualificationType")]
        public string QualificationTypeName { get; set; }

        [ResourceDisplayName("Employee_Label_SpecialityCat")]
        public string SpecialityCatName { get; set; }

        [ResourceDisplayName("Employee_Label_QualificationLevel")]
        public string QualificationLevelName { get; set; }

        [ResourceDisplayName("Employee_Label_ForeignLanguageGrade")]
        public string ForeignLanguageGradeName { get; set; }

        [ResourceDisplayName("Employee_Label_ITQualificationLevel")]
        public string ITQualificationLevelName { get; set; }

        [ResourceDisplayName("Employee_Label_PoliticalGrade")]
        public string PoliticalGradeName { get; set; }

        [ResourceDisplayName("Employee_Label_StateManagementGrade")]
        public string StateManagementGradeName { get; set; }

        [ResourceDisplayName("Employee_Label_EducationalManagementGrade")]
        public string EducationalManagementGradeName { get; set; }

        [ResourceDisplayName("Employee_Label_WorkGroupType")]
        public string WorkGroupTypeName { get; set; }

        [ResourceDisplayName("Employee_Label_PrimarilyAssignedSubject")]
        public string PrimarilyAssignedSubjectName { get; set; }

        [ResourceDisplayName("Employee_Label_Note")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Note { get; set; }
        #endregion nhung truong hien len tren grid

        #region addtional

        [ResourceDisplayName("Employee_Label_SpouseBirthDate")]
        public int? iSpouseBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_MotherBirthDate")]
        public int? iMotherBirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_FatherBirthDate")]
        public int? iFatherBirthDate { get; set; }

        public bool AutoGenCode { get; set; }

        [ResourceDisplayName("Employee_Label_Contract")]
        public string ContractTypeName { get; set; }

        public bool IsUsed { get; set; }


        #endregion addtional
        #region cap nhat them hieund/25-04-2013 - Lay trang thai movement cuoi cung cua giao vien
        public int? EmployeeWorkMovementID { get; set; }
        public int? FromSchoolID { get; set; }
        public string ResolutionDocument { get; set; }
        public int? ToSchoolID { get; set; }
        public int? ToSupervisingDeptID { get; set; }
        public int? ToProvinceID { get; set; }
        public int? ToDistrictID { get; set; }
        public int? ToCommuneID { get; set; }
        public int? MovementType { get; set; }
        public string MoveTo { get; set; }
        public string DescriptionMovement { get; set; }
        public DateTime? MovedDate { get; set; }
        public bool? IsAccepted { get; set; }
        public int? FacultyID { get; set; }
        [ResourceDisplayName("Employee_Label_AppliedLevel")]
        public string AppliedLevelName { get; set; }
        #endregion
        //bo sung thuoc tinh cho giao vien tam nghi
        public int? EmployeeBreatherID { get; set; }
        public DateTime? DateBreather { get; set; }
        public int VacationReasonID { get; set; }
        public DateTime? DateStartWork { get; set; }
        public bool? IsComeBack { get; set; }
    }
}