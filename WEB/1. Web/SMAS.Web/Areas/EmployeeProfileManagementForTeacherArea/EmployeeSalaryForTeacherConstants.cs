﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea
{
    public class EmployeeSalaryForTeacherConstants
    {
        public const string LIST_EMPLOYEESALARY = "listEmployeeSalary";
        public const string LIST_SALARYSCALE = "listSalaryScale";
        public const string LIST_SALARYLEVEL = "listSalaryLevel";
        public const int MAX_GRADE = 12;
        public const string HAS_PERMISSION = "HAS_PERMISSION";
    }
}