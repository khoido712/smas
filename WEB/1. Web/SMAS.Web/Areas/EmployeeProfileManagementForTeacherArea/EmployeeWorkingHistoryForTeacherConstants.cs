﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea
{
    public class EmployeeWorkingHistoryForTeacherConstants
    {
        public const string IS_PERMISS = "IS_PERMISS";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
        public const string LIST_EMPLOYEEWORKINGHISTORY = "listEmployeeWorkingHistory";
    }
}