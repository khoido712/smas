﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System.Linq;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Controllers
{
    [SkipCheckRole]
    public class EmployeeQualificationForTeacherController : BaseController
    {
        private readonly IEmployeeQualificationBusiness EmployeeQualificationBusiness;
        private readonly IQualificationGradeBusiness QualificationGradeBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;

        public EmployeeQualificationForTeacherController(IEmployeeQualificationBusiness EmployeeQualificationBusiness,
            IQualificationGradeBusiness QualificationGradeBusiness,
            IGraduationLevelBusiness GraduationLevelBusiness,
            ISpecialityCatBusiness SpecialityCatBusiness,
            IQualificationTypeBusiness QualificationTypeBusiness,
            IEmployeeBusiness EmployeeBusiness,
            ITrainingLevelBusiness TrainingLevelBusiness)
        {
            this.EmployeeQualificationBusiness = EmployeeQualificationBusiness;
            this.QualificationGradeBusiness = QualificationGradeBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.TrainingLevelBusiness = TrainingLevelBusiness;
        }
        //
        // GET: /EmployeeProfileManagementForTeacherArea/EmployeeQualificationForTeacher/

        //public PartialViewResult Index() 
        //{
        //    int EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0;//_globalInfo.UserAccountID;
        //    CheckPermissionForAction(EmployeeID, "Employee");
        //    GlobalInfo global = new GlobalInfo();
        //    Employee employee = this.EmployeeBusiness.Find(EmployeeID);
        //    this.LoadEmployeeQualification(employee);
        //    EmployeeProfileManagementForTeacherViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementForTeacherViewModel();
        //    SetViewData();
        //    if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
        //    {
        //        ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
        //    }
        //    else
        //    {
        //        ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
        //    }
        //    ViewData["checkReadonly"] = false;       
        //    return PartialView("../EmployeeQualificationForTeacher/Index", employee);
        //}

        //private void LoadEmployeeQualification(Employee employee)
        //{
        //    GlobalInfo globalInfo = new GlobalInfo();
        //    ViewData[EmployeeQualificationForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
        //    //Get view data here

        //    IEnumerable<EmployeeQualificationForTeacherViewModel> IQEmployeeQualification = new EmployeeInfoForTeacher().SearchEmployeeQualification(EmployeeQualificationBusiness, new Dictionary<string, object> { { "EmployeeID", employee.EmployeeID } }); ;
        //    ViewData[EmployeeQualificationForTeacherConstants.LIST_EMPLOYEEQUALIFICATION] = IQEmployeeQualification.OrderByDescending(o => o.FromDate).ToList();

        //    // Lay du lieu combobox

        //    IQueryable<TrainingLevel> IQGraduationLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
        //    SelectList listIQGraduationLevel = new SelectList((IEnumerable<TrainingLevel>)IQGraduationLevel.ToList(), "TrainingLevelID", "Resolution");
        //    ViewData[EmployeeQualificationForTeacherConstants.LIST_GRADUATIONLEVEL] = listIQGraduationLevel;

        //    // QualificationType
        //    IQueryable<QualificationType> IQQualificationType = QualificationTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });
        //    SelectList listQualificationType = new SelectList(IQQualificationType.ToList(), "QualificationTypeID", "Resolution");
        //    ViewData[EmployeeQualificationForTeacherConstants.LIST_QUALIFICATIONTYPE] = listQualificationType;

        //    // QualificationGrade
        //    IQueryable<QualificationGrade> IQQualificationGrade = QualificationGradeBusiness.All.Where(o => o.IsActive == true);
        //    SelectList listQualificationGrade = new SelectList((IEnumerable<QualificationGrade>)IQQualificationGrade.ToList(), "QualificationGradeID", "Resolution");
        //    ViewData[EmployeeQualificationForTeacherConstants.LIST_QUALIFICATIONGRADE] = listQualificationGrade;

        //    // SpecialityCat
        //    IQueryable<SpecialityCat> IQSpecialityCat = SpecialityCatBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });
        //    SelectList listSpecialityCat = new SelectList((IEnumerable<SpecialityCat>)IQSpecialityCat.ToList(), "SpecialityCatID", "Resolution");
        //    ViewData[EmployeeQualificationForTeacherConstants.LIST_SPECIALITYCAT] = listSpecialityCat;

        //    // Ket qua
        //    List<ComboObject> listCobObj = new List<ComboObject>();
        //    listCobObj.Add(new ComboObject(EmployeeQualificationForTeacherConstants.FIRST_TRAINING_VAL, Res.Get("EmployeeQualification_Label_FirstTraining")));
        //    listCobObj.Add(new ComboObject(EmployeeQualificationForTeacherConstants.RE_TRAINING_VAL, Res.Get("EmployeeQualification_Label_ReTraining")));
        //    SelectList listResult = new SelectList(listCobObj, "key", "value");
        //    ViewData[EmployeeQualificationForTeacherConstants.LIST_ISREQUALIFIED] = listResult;
        //}

        public PartialViewResult Search()
        {

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = _globalInfo.EmployeeID;

            //add search info
            //
            GlobalInfo global = new GlobalInfo();
            ViewData[EmployeeQualificationForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);
            IEnumerable<EmployeeQualificationForTeacherViewModel> lst = new EmployeeInfoForTeacher().SearchEmployeeQualification(EmployeeQualificationBusiness, SearchInfo);
            lst = lst.OrderByDescending(o => o.FromDate);
            ViewData[EmployeeQualificationForTeacherConstants.LIST_EMPLOYEEQUALIFICATION] = lst;
            Employee employee = EmployeeBusiness.Find(_globalInfo.EmployeeID);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
            }
            //Get view data here
            ViewData["checkReadonly"] = false;
            return PartialView("../EmployeeQualificationForTeacher/_List");
        }

        #region setViewData

        public void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = GlobalInfo.getInstance();
            var hasPermission = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);            
            if (global.IsCurrentYear)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = false;
            }
        }

        #endregion setViewData

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create()
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeQualification employeequalification = new EmployeeQualification();
            TryUpdateModel(employeequalification);
            Utils.Utils.TrimObject(employeequalification);
            employeequalification.SchoolID = new GlobalInfo().SchoolID.Value;

            this.EmployeeQualificationBusiness.Insert(employeequalification);
            this.EmployeeQualificationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(int EmployeeQualificationID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            CheckPermissionForAction(EmployeeQualificationID, "EmployeeQualification");
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeQualification employeequalification = this.EmployeeQualificationBusiness.Find(EmployeeQualificationID);
            TryUpdateModel(employeequalification);
            Utils.Utils.TrimObject(employeequalification);
            this.EmployeeQualificationBusiness.Update(employeequalification);
            this.EmployeeQualificationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]

        public JsonResult Delete(int id)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            CheckPermissionForAction(id, "EmployeeQualification");
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.EmployeeQualificationBusiness.Delete(id);
            this.EmployeeQualificationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

    }
}
