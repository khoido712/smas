﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using Newtonsoft.Json;
using System.Text;
using Telerik.Web.Mvc;
using SMAS.Web.Areas.EmployeeBreatherArea;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Controllers
{
  
    public class EmployeeProfileManagementForTeacherController : BaseController
    {
        //
        // GET: /EmployeeProfileManagementForTeacherArea/EmployeeProfileManagementForTeacher/

        #region variable
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBreatherBusiness EmployeeBreatherBusiness;

        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IQualificationLevelBusiness QualificationLevelBusiness;

        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;

        private readonly IStateManagementGradeBusiness StateManagementGradeBusiness;
        private readonly IPoliticalGradeBusiness PoliticalGradeBusiness;

        private readonly IEducationalManagementGradeBusiness EducationalManagementGradeBusiness;

        private readonly IStaffPositionBusiness StaffPositionBusiness;
        private readonly IContractBusiness ContractBusiness;
        private readonly IWorkGroupTypeBusiness WorkGroupTypeBusiness;
        private readonly IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IQualificationGradeBusiness QualificationGradeBusiness;
        private IEmployeeQualificationBusiness EmployeeQualificationBusiness;
        private IEmployeeSalaryBusiness EmployeeSalaryBusiness;
        private IEmployeeScaleBusiness EmployeeScaleBusiness;
        private string GRADE = Res.Get("Common_Label_Grade");
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private string TEMPLATE_EMPLOYEE = "GV_DanhSachCanBo.xls";
        private string TEMPLATE_EMPLOYEE_NAME = "GV_DanhSachCanBo_{0}.xls";
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness;

        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IVacationReasonBusiness VacationReasonBusiness;

        #endregion variable

        #region contructor

        public EmployeeProfileManagementForTeacherController(IEmployeeBusiness employeeBusiness
            , ISchoolFacultyBusiness SchoolFacultyBusiness
            , IWorkTypeBusiness WorkTypeBusiness
            , IGraduationLevelBusiness GraduationLevelBusiness
            , IContractTypeBusiness ContractTypeBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness,
            IGroupCatBusiness GroupBusiness, IEthnicBusiness EthnicBusiness, IReligionBusiness ReligionBusiness
            , IFamilyTypeBusiness FamilyTypeBusiness, IQualificationTypeBusiness QualificationTypeBusiness,
            ISpecialityCatBusiness SpecialityCatBusiness, IQualificationLevelBusiness QualificationLevelBusiness,
            IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness, IITQualificationLevelBusiness ITQualificationLevelBusiness,
            IStateManagementGradeBusiness StateManagementGradeBusiness,
            IPoliticalGradeBusiness PoliticalGradeBusiness,
            IEducationalManagementGradeBusiness EducationalManagementGradeBusiness,
            IStaffPositionBusiness StaffPositionBusiness,
            IContractBusiness ContractBusiness
            , IWorkGroupTypeBusiness WorkGroupTypeBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness
            , IQualificationGradeBusiness QualificationGradeBusiness
            , IEmployeeQualificationBusiness EmployeeQualificationBusiness
            , IEmployeeSalaryBusiness EmployeeSalaryBusiness
            , IEmployeeScaleBusiness EmployeeScaleBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ICodeConfigBusiness CodeConfigBusiness
            , IProvinceBusiness ProvinceBusiness
            , ITrainingLevelBusiness TrainingLevelBusiness
            , IEmployeeWorkMovementBusiness employeeWorkMovementBusiness
            , IEmployeeBreatherBusiness employeeBreatherBusiness
            , IUserAccountBusiness UserAccountBusiness
            , IVacationReasonBusiness VacationReasonBusiness)
        {
            this.TrainingLevelBusiness = TrainingLevelBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.WorkTypeBusiness = WorkTypeBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.GroupCatBusiness = GroupBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.ReligionBusiness = ReligionBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.QualificationLevelBusiness = QualificationLevelBusiness;
            this.ForeignLanguageGradeBusiness = ForeignLanguageGradeBusiness;
            this.ITQualificationLevelBusiness = ITQualificationLevelBusiness;
            this.StateManagementGradeBusiness = StateManagementGradeBusiness;
            this.PoliticalGradeBusiness = PoliticalGradeBusiness;

            this.EducationalManagementGradeBusiness = EducationalManagementGradeBusiness;
            this.StaffPositionBusiness = StaffPositionBusiness;
            this.ContractBusiness = ContractBusiness;
            this.WorkGroupTypeBusiness = WorkGroupTypeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.EmployeeWorkingHistoryBusiness = EmployeeWorkingHistoryBusiness;
            this.QualificationGradeBusiness = QualificationGradeBusiness;
            this.EmployeeQualificationBusiness = EmployeeQualificationBusiness;
            this.EmployeeSalaryBusiness = EmployeeSalaryBusiness;
            this.EmployeeScaleBusiness = EmployeeScaleBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.EmployeeWorkMovementBusiness = employeeWorkMovementBusiness;
            this.EmployeeBreatherBusiness = employeeBreatherBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.VacationReasonBusiness = VacationReasonBusiness;
        }

        #endregion contructor

        public ActionResult Index()
        {
            int EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0 ;//_globalInfo.UserAccountID;
            CheckPermissionForAction(EmployeeID, "Employee");
            GlobalInfo global = new GlobalInfo();
            Employee employee = this.EmployeeBusiness.Find(EmployeeID);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
            }
            EmployeeProfileManagementForTeacherViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementForTeacherViewModel();

            SetViewData();
            #region load du lieu cho tab thu nhat
            Utils.Utils.BindTo(employee, EmployeeProfileManagementViewModel);

            EmployeeProfileManagementViewModel.AutoGenCode = CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER);

            if (employee.FatherBirthDate.HasValue) EmployeeProfileManagementViewModel.iFatherBirthDate = employee.FatherBirthDate.Value.Year;
            if (employee.MotherBirthDate.HasValue) EmployeeProfileManagementViewModel.iMotherBirthDate = employee.MotherBirthDate.Value.Year;
            if (employee.SpouseBirthDate.HasValue) EmployeeProfileManagementViewModel.iSpouseBirthDate = employee.SpouseBirthDate.Value.Year;

            //EmployeeProfileManagementViewModel.WorkGroupTypeID = employee.WorkTypeID.HasValue ? employee.WorkType.WorkGroupTypeID : new Nullable<int>();

            //vi employee chua co workgrouptypeid nen phai them vao dua vao worktypeid
            if (EmployeeProfileManagementViewModel.WorkGroupTypeID.HasValue)
            {
                //them vao viewdata worktype
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["WorkGroupTypeID"] = EmployeeProfileManagementViewModel.WorkGroupTypeID.Value;
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic);
                ViewData[EmployeeProfileManagementForTeacherConstant.LS_WORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

                IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() {
                { "AppliedLevel", employee.AppliedLevel },
                { "IsActive", true } }).OrderBy(o => o.DisplayName);
                ViewData[EmployeeProfileManagementForTeacherConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(lsSubjectCat, "SubjectCatID", "DisplayName");
            }
            #endregion load du lieu cho tab thu nhat

            #region load du lieu cho tab thu 2
            //Nếu UserInfo.IsSchoolRole = TRUE: kiểm tra nếu Employee(EmployeeID).SchoolID <> UserInfo.SchoolID =>
            //Chuyển sang trang thông báo lỗi: “Bạn không có quyền truy cập vào dữ liệu này -Common_Label_HasHeadTeacherPermissionError
            bool isPermiss = true;
            if (global.IsSchoolRole)
            {
                if (employee.SchoolID.HasValue)
                    isPermiss &= employee.SchoolID.Value == global.SchoolID.Value;
            }
            else if (global.IsSuperVisingDeptRole)
            {
                if (employee.SupervisingDeptID.HasValue)
                    isPermiss &= employee.SupervisingDeptID.Value == global.SupervisingDeptID.Value;
            }
            else
            {
                isPermiss &= false;
            }


            ViewData[EmployeeWorkingHistoryForTeacherConstants.IS_PERMISS] = isPermiss;
            ViewData[EmployeeWorkingHistoryForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            IEnumerable<EmployeeWorkingHistoryForTeacherViewModel> lst = employee.EmployeeWorkingHistories
                                                                        .Select(o => new EmployeeWorkingHistoryForTeacherViewModel
                                                                        {
                                                                            EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
                                                                            EmployeeID = o.EmployeeID,
                                                                            EmployeeName = o.Employee.FullName,
                                                                            SupervisingDeptID = o.SupervisingDeptID,
                                                                            SchoolID = o.SchoolID,
                                                                            Organization = o.Organization,
                                                                            Department = o.Department,
                                                                            Position = o.Position,
                                                                            Resolution = o.Resolution,
                                                                            FromDate = o.FromDate,
                                                                            ToDate = o.ToDate
                                                                        })
                                                                        .OrderByDescending(o => o.FromDate)
                                                                        .ThenBy(u => u.EmployeeName)
                                                                        .ToList();
            ViewData[EmployeeWorkingHistoryForTeacherConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;
            ViewData[EmployeeProfileManagementForTeacherConstant.CHOSENEMPLOYEE] = EmployeeProfileManagementViewModel;
            #endregion load du lieu cho tab thu 2

            #region Load du lieu cho tab 3 - Thong tin dao tao

            this.LoadEmployeeQualification(employee);

            #endregion Load du lieu cho tab 3 - Thong tin dao tao

            #region Load du lieu tab 4 - Thong tin luong

            this.LoadEmployeeSalary(employee);

            #endregion Load du lieu tab 4 - Thong tin luong
            ViewData["checkReadonly"] = false;            
            return View();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Edit(EmployeeProfileManagementForTeacherViewModel EmployeeProfileManagementViewModel)
        {
            //EmployeeProfileManagementViewModel.EmployeeID = _globalInfo.EmployeeID.;
            CheckPermissionForAction(EmployeeProfileManagementViewModel.EmployeeID, "Employee");
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Employee employee = EmployeeBusiness.Find(EmployeeProfileManagementViewModel.EmployeeID);
            //string oldObject = JsonConvert.SerializeObject(SMAS.Business.Common.Utils.BindTo<EmployeeBO>(employee), new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects });// convert to json string for old object
            string oldObject = "{EmployeeID:" + employee.EmployeeID + ", EmployeeCode:\"" + employee.EmployeeCode + "\", FullName:\"" + employee.FullName + "\", BirthDate:\"" + employee.BirthDate.ToString() + "\",EthnicID:" + employee.EthnicID + ",SchoolFacultyID:" + employee.SchoolFacultyID + ",IntoSchoolDate:\"" + employee.IntoSchoolDate.ToString() + "\",ContractTypeID:" + employee.ContractTypeID + ",WorkGroupTypeID:" + employee.WorkGroupTypeID + ",WorkTypeID:" + employee.WorkTypeID + ", AppliedLevel:" + employee.AppliedLevel + "}";
            if (!CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
            {
                EmployeeProfileManagementViewModel.EmployeeCode = employee.EmployeeCode;
            }
            EmployeeProfileManagementViewModel.Image = employee.Image;
            Utils.Utils.BindTo(EmployeeProfileManagementViewModel, employee);
            if (EmployeeProfileManagementViewModel.IsSyndicate)
            {
                if (EmployeeProfileManagementViewModel.SyndicateDate.HasValue && EmployeeProfileManagementViewModel.SyndicateDate.Value.Date > DateTime.Now.Date)
                {
                    throw new SMAS.Business.Common.BusinessException("Message_Syndicate_Error");
                }
            }
            if (EmployeeProfileManagementViewModel.iFatherBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Father_Error");
                }
                if (EmployeeProfileManagementViewModel.iFatherBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_Father_Error");
                }
                employee.FatherBirthDate = new DateTime(EmployeeProfileManagementViewModel.iFatherBirthDate.Value, 1, 1);
            }
            if (EmployeeProfileManagementViewModel.iMotherBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Mother_Error");
                }
                if (EmployeeProfileManagementViewModel.iMotherBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_Mother_Error");
                }
                employee.MotherBirthDate = new DateTime(EmployeeProfileManagementViewModel.iMotherBirthDate.Value, 1, 1);
            }
            if (EmployeeProfileManagementViewModel.iSpouseBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iSpouseBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_SPouse_Error");
                }
                if (EmployeeProfileManagementViewModel.iSpouseBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_SPouse_Error");
                }
                employee.SpouseBirthDate = new DateTime(EmployeeProfileManagementViewModel.iSpouseBirthDate.Value, 1, 1);
            }

            if (EmployeeProfileManagementViewModel.ContractTypeID != 0 && EmployeeProfileManagementViewModel.ContractTypeID != null)
            {
                ContractType contractType = ContractTypeBusiness.Find(EmployeeProfileManagementViewModel.ContractTypeID);
                if (contractType.Resolution.ToLower() == SystemParamsInFile.CONTRACT_TYPE_STAFF_NAME)
                {
                    if (EmployeeProfileManagementViewModel.JoinedDate == null)
                    {
                        throw new SMAS.Business.Common.BusinessException("Employee_ContractType_Label_JoinedDate");
                    }
                }
            }

            if ((employee.WorkTypeID == 10 || employee.WorkTypeID == 12) && (!employee.AppliedLevel.HasValue || employee.AppliedLevel == 0))
            {
                throw new SMAS.Business.Common.BusinessException("Thầy/cô chưa chọn cấp dạy chính");
            }
            //truong Name lay tu FullName
            int space = employee.FullName.LastIndexOf(" ");
            if (space == -1)
            {
                employee.Name = employee.FullName;
            }
            else
            {
                employee.Name = employee.FullName.Substring(space + 1);
            }

            //dua image vao employee
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                employee.Image = imageBytes;
            }

            employee.SchoolID = global.SchoolID;
            employee.IsActive = true;


            if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER)     //Neu truong duoc tu dong generate code
                && CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER) //Truong duoc sua
                && EmployeeProfileManagementViewModel.AutoGenCode)                                                                       //Nguoi dung chon sua
            {
                employee.EmployeeCode = CodeConfigBusiness.CreateTeacherCode(global.SchoolID.Value);
            }

            employee.EmploymentStatus = SMAS.Business.Common.GlobalConstants.EMPLOYMENT_STATUS_WORKING;
            this.EmployeeBusiness.UpdateTeacher(employee);
            this.EmployeeBusiness.Save();

            // Tạo giá trị ghi log
            //string newObject = JsonConvert.SerializeObject(SMAS.Business.Common.Utils.BindTo<EmployeeBO>(employee), new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
            string newObject = "{EmployeeID:" + employee.EmployeeID + ", EmployeeCode:\"" + employee.EmployeeCode + "\", FullName:\"" + employee.FullName + "\", BirthDate:\"" + employee.BirthDate.ToString() + "\",EthnicID:" + employee.EthnicID + ",SchoolFacultyID:" + employee.SchoolFacultyID + ",IntoSchoolDate:\"" + employee.IntoSchoolDate.ToString() + "\",ContractTypeID:" + employee.ContractTypeID + ",WorkGroupTypeID:" + employee.WorkGroupTypeID + ",WorkTypeID:" + employee.WorkTypeID + ", AppliedLevel:" + employee.AppliedLevel + "}";
            SetViewDataActionAudit(oldObject, newObject, employee.EmployeeID.ToString(), "Update employee_id:" + employee.EmployeeID.ToString(), employee.EmployeeID.ToString(), "Hồ sơ cán bộ/nhân viên", GlobalConstants.ACTION_UPDATE, "Cập nhật hồ sơ " + employee.FullName + " mã " + employee.EmployeeCode);

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        public ActionResult GetEditEmployee(int EmployeeId, FormCollection fc)
        {
            
            CheckPermissionForAction(EmployeeId, "Employee");
            GlobalInfo global = new GlobalInfo();
            Employee employee = this.EmployeeBusiness.Find(EmployeeId);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
            }
            EmployeeProfileManagementForTeacherViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementForTeacherViewModel();

            SetViewData();
            #region load du lieu cho tab thu nhat
            Utils.Utils.BindTo(employee, EmployeeProfileManagementViewModel);

            EmployeeProfileManagementViewModel.AutoGenCode = CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER);

            if (employee.FatherBirthDate.HasValue) EmployeeProfileManagementViewModel.iFatherBirthDate = employee.FatherBirthDate.Value.Year;
            if (employee.MotherBirthDate.HasValue) EmployeeProfileManagementViewModel.iMotherBirthDate = employee.MotherBirthDate.Value.Year;
            if (employee.SpouseBirthDate.HasValue) EmployeeProfileManagementViewModel.iSpouseBirthDate = employee.SpouseBirthDate.Value.Year;

            //EmployeeProfileManagementViewModel.WorkGroupTypeID = employee.WorkTypeID.HasValue ? employee.WorkType.WorkGroupTypeID : new Nullable<int>();

            //vi employee chua co workgrouptypeid nen phai them vao dua vao worktypeid
            if (EmployeeProfileManagementViewModel.WorkGroupTypeID.HasValue)
            {
                //them vao viewdata worktype
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["WorkGroupTypeID"] = EmployeeProfileManagementViewModel.WorkGroupTypeID.Value;
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic);
                ViewData[EmployeeProfileManagementForTeacherConstant.LS_WORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

                IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() {
                { "AppliedLevel", employee.AppliedLevel },
                { "IsActive", true } }).OrderBy(o => o.DisplayName);
                ViewData[EmployeeProfileManagementForTeacherConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(lsSubjectCat, "SubjectCatID", "DisplayName");
            }
            #endregion load du lieu cho tab thu nhat

            #region load du lieu cho tab thu 2
            //Nếu UserInfo.IsSchoolRole = TRUE: kiểm tra nếu Employee(EmployeeID).SchoolID <> UserInfo.SchoolID =>
            //Chuyển sang trang thông báo lỗi: “Bạn không có quyền truy cập vào dữ liệu này -Common_Label_HasHeadTeacherPermissionError
            bool isPermiss = true;
            if (global.IsSchoolRole)
            {
                if (employee.SchoolID.HasValue)
                    isPermiss &= employee.SchoolID.Value == global.SchoolID.Value;
            }
            else if (global.IsSuperVisingDeptRole)
            {
                if (employee.SupervisingDeptID.HasValue)
                    isPermiss &= employee.SupervisingDeptID.Value == global.SupervisingDeptID.Value;
            }
            else
            {
                isPermiss &= false;
            }


            ViewData[EmployeeWorkingHistoryForTeacherConstants.IS_PERMISS] = isPermiss;
            ViewData[EmployeeWorkingHistoryForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            IEnumerable<EmployeeWorkingHistoryForTeacherViewModel> lst = employee.EmployeeWorkingHistories
                                                                        .Select(o => new EmployeeWorkingHistoryForTeacherViewModel
                                                                        {
                                                                            EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
                                                                            EmployeeID = o.EmployeeID,
                                                                            EmployeeName = o.Employee.FullName,
                                                                            SupervisingDeptID = o.SupervisingDeptID,
                                                                            SchoolID = o.SchoolID,
                                                                            Organization = o.Organization,
                                                                            Department = o.Department,
                                                                            Position = o.Position,
                                                                            Resolution = o.Resolution,
                                                                            FromDate = o.FromDate,
                                                                            ToDate = o.ToDate
                                                                        })
                                                                        .OrderByDescending(o => o.FromDate)
                                                                        .ThenBy(u => u.EmployeeName)
                                                                        .ToList();
            ViewData[EmployeeWorkingHistoryForTeacherConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;
            ViewData[EmployeeProfileManagementForTeacherConstant.CHOSENEMPLOYEE] = EmployeeProfileManagementViewModel;
            #endregion load du lieu cho tab thu 2

            #region Load du lieu cho tab 3 - Thong tin dao tao

            this.LoadEmployeeQualification(employee);

            #endregion Load du lieu cho tab 3 - Thong tin dao tao

            #region Load du lieu tab 4 - Thong tin luong

            this.LoadEmployeeSalary(employee);

            #endregion Load du lieu tab 4 - Thong tin luong
            ViewData["checkReadonly"] = false;   
            return View("_Edit");
        }

        #region _GetcboWorkType

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetcboWorkType(int? GroupTypeId)
        {
            if (GroupTypeId.HasValue)
            {
                //Nếu cboWorkGroupType.Value <> 0 -> lấy danh sách từ WorkType Search(Dictionary)
                //với Dictionary[“WorkGroupTypeID”] = cboWorkGroupType.Value

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["WorkGroupTypeID"] = GroupTypeId.Value;
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic).OrderBy(o => o.Resolution);

                return Json(new SelectList(lsWorkType, "WorkTypeID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion _GetcboWorkType

        #region _GetPrimarilyAssignedSubject

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetPrimarilyAssignedSubject(int? AppliedLevel)
        {
            if (AppliedLevel.HasValue)
            {
                IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", AppliedLevel }, { "IsActive", true } }).OrderBy(o => o.SubjectName);
                return Json(new SelectList(lsSubjectCat, "SubjectCatID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region test image


        [ValidateAntiForgeryToken]
        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        {
            GlobalInfo global = new GlobalInfo();

            if (ImageUploader == null || ImageUploader.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = ImageUploader.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> imageExtension = new List<string>();
                imageExtension.Add(".JPG");
                imageExtension.Add(".JPEG");
                imageExtension.Add(".PNG");
                imageExtension.Add(".BMP");
                imageExtension.Add(".ICO");
                imageExtension.Add(".GIF");
                var extension = Path.GetExtension(file.FileName);
                if (!imageExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
                file.SaveAs(physicalPath);
                Session["ImagePath"] = physicalPath;

                string relativePath = "/Uploads/Photos/" + fileName;
                JsonResult res = Json(new JsonMessage(fileName));
                res.ContentType = "text/plain";
                return res;
            }

            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }


        [ValidateAntiForgeryToken]
        public ActionResult RemoveImage(string[] fileNames)
        {
            //// The parameter of the Remove action must be called "fileNames"
            //foreach (var fullName in fileNames)
            //{
            //    var fileName = Path.GetFileName(fullName);
            //    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

            //    // TODO: Verify user permissions
            //    if (System.IO.File.Exists(physicalPath))
            //    {
            //        // The files are not actually removed in this demo
            //        System.IO.File.Delete(physicalPath);
            //    }
            //}

            // Return an empty string to signify success
            return Content("");
        }

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }

        #endregion test image     

        
        #region EmployeeWorkingHistory

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult CreateEmployeeWorkingHistory(int EmployeeProfileManagementViewModelID)
        {
            CheckPermissionForAction(EmployeeProfileManagementViewModelID, "Employee");
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData["checkReadonly"] = false;
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeWorkingHistory employeeworkinghistory = new EmployeeWorkingHistory();
            TryUpdateModel(employeeworkinghistory);
            Utils.Utils.TrimObject(employeeworkinghistory);

            //fix cứng
            employeeworkinghistory.EmployeeID = EmployeeProfileManagementViewModelID;
            //tu employeeid lay supervisingdeptid va schoolid tong vao object
            Employee temp = EmployeeBusiness.All.Where(o => (o.EmployeeID == employeeworkinghistory.EmployeeID)).FirstOrDefault();

            employeeworkinghistory.SchoolID = temp.SchoolID;
            employeeworkinghistory.SupervisingDeptID = temp.SupervisingDeptID;

            this.EmployeeWorkingHistoryBusiness.Insert(employeeworkinghistory);
            this.EmployeeWorkingHistoryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }


        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult EditEmployeeWorkingHistory(int EmployeeProfileManagementViewModelID, int EmployeeWorkingHistoryID)
        {
            CheckPermissionForAction(EmployeeProfileManagementViewModelID, "Employee");
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData["checkReadonly"] = false;
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeWorkingHistory employeeworkinghistory = this.EmployeeWorkingHistoryBusiness.Find(EmployeeWorkingHistoryID);
            TryUpdateModel(employeeworkinghistory);
            Utils.Utils.TrimObject(employeeworkinghistory);
            ViewData["checkReadonly"] = false;
            //fix cứng
            employeeworkinghistory.EmployeeID = EmployeeProfileManagementViewModelID;

            //tu employeeid lay supervisingdeptid va schoolid tong vao object
            Employee temp = EmployeeBusiness.All.Where(o => (o.EmployeeID == employeeworkinghistory.EmployeeID)).FirstOrDefault();

            employeeworkinghistory.SchoolID = temp.SchoolID;
            employeeworkinghistory.SupervisingDeptID = temp.SupervisingDeptID;

            this.EmployeeWorkingHistoryBusiness.Update(employeeworkinghistory);
            this.EmployeeWorkingHistoryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        public PartialViewResult SearchEmployeeWorkingHistory(int EmployeeID, FormCollection fc)
        {
            GlobalInfo global = new GlobalInfo();
            //Nếu UserInfo.IsSchoolRole = TRUE: kiểm tra nếu Employee(EmployeeID).SchoolID <> UserInfo.SchoolID =>
            //Chuyển sang trang thông báo lỗi: “Bạn không có quyền truy cập vào dữ liệu này -Common_Label_HasHeadTeacherPermissionError
            Employee em = EmployeeBusiness.Find(EmployeeID);
            if (em.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
            }
            bool isPermiss = true;
            if (global.IsSchoolRole)
            {
                if (em.SchoolID.HasValue)
                {
                    if (em.SchoolID.Value != global.SchoolID.Value)
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= false;
                    }
                    else
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= true;
                    }
                }
            }
            else if (global.IsSuperVisingDeptRole)
            {
                if (em.SupervisingDeptID.HasValue)
                {
                    if (em.SupervisingDeptID.Value != global.SupervisingDeptID.Value)
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= false;
                    }
                    else
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= true;
                    }
                }
            }
            else
            {
                isPermiss &= false;
            }


            ViewData[EmployeeWorkingHistoryForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);

            ViewData[EmployeeWorkingHistoryForTeacherConstants.IS_PERMISS] = isPermiss;
            ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = global.IsCurrentYear;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //
            SearchInfo["EmployeeID"] = EmployeeID; //tam thoi cho fix cung id
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;

            IEnumerable<EmployeeWorkingHistoryForTeacherViewModel> lst = this._SearchEmployeeWorkingHistory(SearchInfo);
            ViewData[EmployeeWorkingHistoryForTeacherConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;

            //Get view data here
            ViewData["checkReadonly"] = false;
            return PartialView("../EmployeeWorkingHistoryForTeacher/_ListEmployeeWorkingHistory", lst);
        }

        private IEnumerable<EmployeeWorkingHistoryForTeacherViewModel> _SearchEmployeeWorkingHistory(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<EmployeeWorkingHistory> query = EmployeeWorkingHistoryBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            IQueryable<EmployeeWorkingHistoryForTeacherViewModel> lst = query.Select(o => new EmployeeWorkingHistoryForTeacherViewModel
            {
                EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
                EmployeeID = o.EmployeeID,
                EmployeeName = o.Employee.FullName,
                SupervisingDeptID = o.SupervisingDeptID,
                SchoolID = o.SchoolID,
                Organization = o.Organization,
                Department = o.Department,
                Position = o.Position,
                Resolution = o.Resolution,
                FromDate = o.FromDate,
                ToDate = o.ToDate
            });

            List<EmployeeWorkingHistoryForTeacherViewModel> lsEmployeeWorkingHistoryViewModel = lst.ToList();
            return lsEmployeeWorkingHistoryViewModel.OrderByDescending(o => o.FromDate).OrderBy(o => o.EmployeeName).ToList();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmployeeWorkingHistory(int id)
        {
            CheckPermissionForAction(id, "EmployeeWorkingHistory");
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //Nếu UserInfo.IsSchoolRole = TRUE:
            //EmployeeWorkingHistoryBusiness.Delete(UserInfo.SchoolID, EmployeeWorkingHistoryID)
            if (global.IsSchoolRole)
            {
                this.EmployeeWorkingHistoryBusiness.Delete(global.SchoolID.Value, id);
                this.EmployeeWorkingHistoryBusiness.Save();
            }
            if (global.IsSuperVisingDeptRole)
            {
                this.EmployeeWorkingHistoryBusiness.Delete(global.SupervisingDeptID.Value, id);
                this.EmployeeWorkingHistoryBusiness.Save();
            }
            ViewData["checkReadonly"] = false;
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion EmployeeWorkingHistory

        #region Load du lieu cho tab thong tin dao tao cua can bo
        /// <summary>
        /// Load du lieu
        /// </summary>
        /// <param name="EmployeeID"></param>
        private void LoadEmployeeQualification(Employee employee)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData[EmployeeQualificationForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            //Get view data here

            IEnumerable<EmployeeQualificationForTeacherViewModel> IQEmployeeQualification = new EmployeeInfoForTeacher().SearchEmployeeQualification(EmployeeQualificationBusiness, new Dictionary<string, object> { { "EmployeeID", employee.EmployeeID } }); ;
            ViewData[EmployeeQualificationForTeacherConstants.LIST_EMPLOYEEQUALIFICATION] = IQEmployeeQualification.OrderByDescending(o => o.FromDate).ToList();

            // Lay du lieu combobox

            IQueryable<TrainingLevel> IQGraduationLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
            SelectList listIQGraduationLevel = new SelectList((IEnumerable<TrainingLevel>)IQGraduationLevel.ToList(), "TrainingLevelID", "Resolution");
            ViewData[EmployeeQualificationForTeacherConstants.LIST_GRADUATIONLEVEL] = listIQGraduationLevel;

            // QualificationType
            IQueryable<QualificationType> IQQualificationType = QualificationTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });
            SelectList listQualificationType = new SelectList(IQQualificationType.ToList(), "QualificationTypeID", "Resolution");
            ViewData[EmployeeQualificationForTeacherConstants.LIST_QUALIFICATIONTYPE] = listQualificationType;

            // QualificationGrade
            IQueryable<QualificationGrade> IQQualificationGrade = QualificationGradeBusiness.All.Where(o => o.IsActive == true);
            SelectList listQualificationGrade = new SelectList((IEnumerable<QualificationGrade>)IQQualificationGrade.ToList(), "QualificationGradeID", "Resolution");
            ViewData[EmployeeQualificationForTeacherConstants.LIST_QUALIFICATIONGRADE] = listQualificationGrade;

            // SpecialityCat
            IQueryable<SpecialityCat> IQSpecialityCat = SpecialityCatBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });
            SelectList listSpecialityCat = new SelectList((IEnumerable<SpecialityCat>)IQSpecialityCat.ToList(), "SpecialityCatID", "Resolution");
            ViewData[EmployeeQualificationForTeacherConstants.LIST_SPECIALITYCAT] = listSpecialityCat;

            // Ket qua
            List<ComboObject> listCobObj = new List<ComboObject>();
            listCobObj.Add(new ComboObject(EmployeeQualificationForTeacherConstants.FIRST_TRAINING_VAL, Res.Get("EmployeeQualification_Label_FirstTraining")));
            listCobObj.Add(new ComboObject(EmployeeQualificationForTeacherConstants.RE_TRAINING_VAL, Res.Get("EmployeeQualification_Label_ReTraining")));
            SelectList listResult = new SelectList(listCobObj, "key", "value");
            ViewData[EmployeeQualificationForTeacherConstants.LIST_ISREQUALIFIED] = listResult;
        }
        #endregion Load du lieu cho tab thong tin dao tao cua can bo

        #region Load du lieu cho tab thong tin luong cua can bo
        private void LoadEmployeeSalary(Employee employee)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();

            IEnumerable<EmployeeSalaryForTeacherViewModel> lst = new EmployeeInfoForTeacher().SearchEmployeeSalary(EmployeeSalaryBusiness, new Dictionary<string, object> { { "EmployeeID", employee.EmployeeID } });
            ViewData[EmployeeSalaryForTeacherConstants.LIST_EMPLOYEESALARY] = lst;

            // Lay du lieu combobox
            SearchInfo.Clear();
            IQueryable<EmployeeScale> IQSalaryLevel = EmployeeScaleBusiness.Search(SearchInfo);
            SelectList listSalaryLevel = new SelectList((IEnumerable<EmployeeScale>)IQSalaryLevel.ToList(), "EmployeeScaleID", "Resolution");
            ViewData[EmployeeSalaryForTeacherConstants.LIST_SALARYSCALE] = listSalaryLevel;

            // Scale
            List<ComboObject> listComboObj = new List<ComboObject>();
            for (int i = 1; i <= EmployeeSalaryForTeacherConstants.MAX_GRADE; i++)
            {
                listComboObj.Add(new ComboObject(i.ToString(), GRADE + " " + i.ToString()));
            }
            SelectList listSalaryScale = new SelectList((IEnumerable<ComboObject>)listComboObj, "key", "value");
            ViewData[EmployeeSalaryForTeacherConstants.LIST_SALARYLEVEL] = listSalaryScale;
        }
        #endregion Load du lieu cho tab thong tin luong cua can bo

        #region setViewData

        public void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = GlobalInfo.getInstance();
            var hasPermission = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);
            //ViewData[EmployeeWorkingHistoryForTeacherConstants.HAS_PERMISSION] = hasPermission;
            //ViewData[EmployeeQualificationForTeacherConstants.HAS_PERMISSION] = hasPermission;
            //ViewData[EmployeeSalaryForTeacherConstants.HAS_PERMISSION] = hasPermission;
            //ViewData[EmployeeProfileManagementForTeacherConstant.Has_Permission_TeachingAssignment] = hasPermission;
            ViewData[EmployeeProfileManagementForTeacherConstant.HAS_PERMISSION] = hasPermission;

            // SchoolFacultyBussiness.SearchBySchool(IDictionary)
            //với IDictionary[“SchoolID”] = UserInfo.SchoolID
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            IQueryable<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo).OrderBy(o => o.FacultyName);
            ViewData[EmployeeProfileManagementForTeacherConstant.LISTSCHOOLFACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");

            //cboWorkType: Lấy danh sách từ WorkTypeBusiness.Search()
            //IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(new Dictionary<string, object>() { 
            //    //{ "SchoolID", global.SchoolID.Value }, 
            //    { "IsActive", true } })
            //    .OrderBy(o => o.Resolution);
            List<WorkType> lsWorkType = this.getListOfWorkType();
            ViewData[EmployeeProfileManagementForTeacherConstant.LISTWORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

            //cboEmploymentStatus:
            //+ EMPLOYMENT_STATUS_WORKING : Đang làm việc
            //+ EMPLOYMENT_STATUS_WORK_CHANGED : Chuyển công tác
            //+ EMPLOYMENT_STATUS_RETIRED : Nghỉ hưu
            //+ EMPLOYMENT_STATUS_BREATHER: Tạm nghỉ
            List<ComboObject> listEmploymentStatus = new List<ComboObject>();
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementForTeacherConstant.EMPLOYMENT_STATUS_WORKING.ToString(), Res.Get("Employee_Label_EmployeeStatusWorking")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementForTeacherConstant.EMPLOYMENT_STATUS_WORK_CHANGED.ToString(), Res.Get("Employee_Label_EmployeeStatusWorkingChange")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementForTeacherConstant.EMPLOYMENT_STATUS_RETIRED.ToString(), Res.Get("Employee_Label_EmployeeStatusRetired")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementForTeacherConstant.EMPLOYMENT_STATUS_SEVERANCE.ToString(), Res.Get("Employee_Label_EmployeeStatusSeverance")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementForTeacherConstant.EMPLOYMENT_STATUS_BREATHER.ToString(), Res.Get("Common_Label_EmployeeStatusLeavedOff")));
            ViewData[EmployeeProfileManagementForTeacherConstant.LISTEMPLOYMENTSTATUS] = new SelectList(listEmploymentStatus, "key", "value");

            //   cboSex:
            //+ 1 : Nam
            //+ 2 : Nữ
            List<ComboObject> listSex = new List<ComboObject>();
            listSex.Add(new ComboObject("true", Res.Get("Common_Label_Male")));
            listSex.Add(new ComboObject("false", Res.Get("Common_Label_Female")));
            ViewData[EmployeeProfileManagementForTeacherConstant.LISTSEX] = new SelectList(listSex, "key", "value");

            //cboTrainingLevel: TrainingLevelBussiness.Search()    
            //IQueryable<TrainingLevel> lsTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution);
            List<TrainingLevel> lsTrainingLevel = this.getListOfTrainingLevel();
            ViewData[EmployeeProfileManagementForTeacherConstant.LISTGRADUATIONLEVEL] = new SelectList(lsTrainingLevel, "TrainingLevelID", "Resolution");

            //cboGraduationLevel: Lấy danh sách từ GraduationLevelBussiness.Search(Dictionary) với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel ->cho form create,edit
            //IQueryable<TrainingLevel> lsGraduationLevelUpdateOrEdit = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution);
            List<TrainingLevel> lsGraduationLevelUpdateOrEdit = this.getListOfTrainingLevel();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_GRADUATIONLEVEL] = new SelectList(lsGraduationLevelUpdateOrEdit, "TrainingLevelID", "Resolution");

            //cboContractType: Lấy danh sách từ ContractTypeBusiness.Search()
            //IQueryable<ContractType> lsContractType = ContractTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution);
            List<ContractType> lsContractType = this.getListOfContractType();
            ViewData[EmployeeProfileManagementForTeacherConstant.LISTCONTRACTTYPE] = new SelectList(lsContractType, "ContractTypeID", "Resolution");

            // Danh sach cac dan toc
            //IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic.Add("IsActive", true);
            //List<Ethnic> listEthnic = EthnicBusiness.Search(dic).OrderBy(o => o.EthnicName).ToList();
            List<Ethnic> listEthnic = this.getListOfEthnic();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_ETHNIC] = new SelectList(listEthnic, "EthnicID", "EthnicName", SMAS.Business.Common.SystemParamsInFile.DEFAULT_ETHNIC);

            //List<Religion> listReligion = ReligionBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<Religion> listReligion = ReligionBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<Religion> listReligion = this.getListOfReligion();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_RELIGION] = new SelectList(listReligion, "ReligionID", "Resolution", SMAS.Business.Common.SystemParamsInFile.DEFAULT_RELIGION);

            // List<FamilyType> listFamilyType = FamilyTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<FamilyType> listFamilyType = FamilyTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<FamilyType> listFamilyType = this.getListOfFamilyType();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_FAMILYTYPE] = new SelectList(listFamilyType, "FamilyTypeID", "Resolution");

            //List<QualificationType> listQualificationType = QualificationTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<QualificationType> listQualificationType = QualificationTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<QualificationType> listQualificationType = this.getListOfQualificationType();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_QUALIFICATIONTYPE] = new SelectList(listQualificationType, "QualificationTypeID", "Resolution");

            //List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<SpecialityCat> listSpecialityCat = this.getListOfSpecialityCat();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_SPECIALITYCAT] = new SelectList(listSpecialityCat, "SpecialityCatID", "Resolution");

            // List< QualificationLevel> listQualificationLevel =  QualificationLevelBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<QualificationLevel> listQualificationLevel = QualificationLevelBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<QualificationLevel> listQualificationLevel = this.getListOfQualificationLevel();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_QUALIFICATIONLEVEL] = new SelectList(listQualificationLevel, "QualificationLevelID", "Resolution");

            //List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<ForeignLanguageGrade> listForeignLanguageGrade = this.getListOfForeignLanguageGrade();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_FOREIGNLANGUAGEGRADE] = new SelectList(listForeignLanguageGrade, "ForeignLanguageGradeID", "Resolution");

            //List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<ITQualificationLevel> listITQualificationLevel = this.getListOfITQualificationLevel();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_ITQUALIFICATIONLEVEL] = new SelectList(listITQualificationLevel, "ITQualificationLevelID", "Resolution");

            //List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<StateManagementGrade> listStateManagementGrade = this.getListOfStateManagementGrade();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_STATEMANAGEMENTGRADE] = new SelectList(listStateManagementGrade, "StateManagementGradeID", "Resolution");

            //List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<PoliticalGrade> listPoliticalGrade = this.getListOfPoliticalGrade();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_POLITICALGRADE] = new SelectList(listPoliticalGrade, "PoliticalGradeID", "Resolution");

            //List<EducationalManagementGrade> listEducationalManagementGrade =  EducationalManagementGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<EducationalManagementGrade> listEducationalManagementGrade = EducationalManagementGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<EducationalManagementGrade> listEducationalManagementGrade = this.getListOfEducationalManagementGrade();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_EDUCATIONALMANAGEMENTGRADE] = new SelectList(listEducationalManagementGrade, "EducationalManagementGradeID", "Resolution");

            //List<ContractType> listContractType = ContractTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            ////List<Contract> listContract = ContractBusiness.Search(dic).ToList();
            //ViewData[EmployeeProfileManagementConstant.LS_CONTRACT] = new SelectList(listContractType, "ContractTypeID", "Resolution");

            //List<Contract> listContract = ContractBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<Contract> listContract = ContractBusiness.Search(dic).ToList();
            List<Contract> listContract = this.getListOfContract();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_CONTRACT] = new SelectList(listContract, "ContractID", "ContractID");


            // List<StaffPosition> listStaffPosition = StaffPositionBusiness.Search(dic).ToList();
            //List<StaffPosition> listStaffPosition = StaffPositionBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.Resolution).ToList();
            List<StaffPosition> listStaffPosition = this.getListOfStaffPosition();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_STAFFPOSITION] = new SelectList(listStaffPosition, "StaffPositionID", "Resolution");

            //List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.GetDependantDept(UserInfo.SupervisingDeptID()).ToList();
            //List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.Search(dic).OrderBy(o => o.SupervisingDeptName).ToList();
            List<SupervisingDept> listSupervisingDept = this.getListOfSupervisingDept();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_SUPERVISINGDEPT] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName");

            //List<GroupCat> listGroupCat = GroupCatBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.GroupName).ToList();

            //IQueryable<SupervisingDept> lsSuperTemp = SupervisingDeptBusiness.All.Where(em=>(em.SupervisingDeptID==UserInfo.SupervisingDeptID()));
            //List<GroupCat> listGroupCat = GroupCatBusiness.GetGroupByRoleAndCreatedUser(UserInfo.RoleID(),lsSuperTemp.FirstOrDefault().AdminID.Value).ToList();
            //ViewData[EmployeeProfileManagementConstant.LS_GROUPCAT] = new SelectList(listGroupCat, "GroupCatID", "GroupName");

            //cboWorkGroupType: Lấy danh sách từ WorkGroupTypeBussiness.Search()
            //IQueryable<WorkGroupType> lsWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution);
            List<WorkGroupType> lsWorkGroupType = this.getListOfWorkGroupType();
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_WORKGROUPTYPE] = new SelectList(lsWorkGroupType, "WorkGroupTypeID", "Resolution");

            //cboWorkType: Không có dữ liệu
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_WORKTYPE] = new SelectList(new string[] { });

            //cboPrimarilyAssignedSubject: Lấy danh sách từ SubjectCat.Search(Dictionary)
            //với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", global.AppliedLevel.Value }, { "IsActive", true } }).OrderBy(o => o.DisplayName);
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(new string[] { });

            //SchoolProfileBusiness.GetListAppliedLevel(UserInfo.SchoolID)
            ViewData[EmployeeProfileManagementForTeacherConstant.LS_APPLIEDLEVEL] = new SelectList(new List<ComboObject>(), "key", "value");
            ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK] = false;
            ViewData[EmployeeProfileManagementForTeacherConstant.IS_SCHOOL_MODIFIED] = false;
            ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = false;
            ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = true;
            if (global.SchoolID != null)
            {
                List<int> lsAL = SchoolProfileBusiness.GetListAppliedLevel(global.SchoolID.Value);
                lsAL.Sort();
                List<ComboObject> listAppliedLevel = new List<ComboObject>();

                //listAppliedLevel.Add(new ComboObject("1", "Cấp 1"));
                //listAppliedLevel.Add(new ComboObject("2", "Cấp 2"));
                //listAppliedLevel.Add(new ComboObject("3", "Cấp 3"));
                //listAppliedLevel.Add(new ComboObject("4", "Cấp 4"));
                foreach (var item in lsAL)
                {
                    listAppliedLevel.Add(new ComboObject(item.ToString(), CommonConvert.ConvertToResolutionApplied(item)));
                }
                ViewData[EmployeeProfileManagementForTeacherConstant.LS_APPLIEDLEVEL] = new SelectList(listAppliedLevel, "key", "value");

                //Autogen code
                //Nếu  CodeConfigBusiness.IsAutoGenCode(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE thì chkAutoCode được chọn
                if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK] = true;
                }
                else
                {
                    ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK] = false;
                }

                //-	Nếu CodeConfigBusiness.IsSchoolModify(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE
                if (CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = true;
                    ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = false;
                    ViewData[EmployeeProfileManagementForTeacherConstant.IS_SCHOOL_MODIFIED] = true;
                }
                else
                {
                    ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = false;
                    ViewData[EmployeeProfileManagementForTeacherConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = true;
                    ViewData[EmployeeProfileManagementForTeacherConstant.IS_SCHOOL_MODIFIED] = false;
                }
            }
            //var lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ProvinceID, u.ProvinceName }).OrderBy(o => o.ProvinceName).ToList();
            var lstProvince = this.getListOfProvince();
            ViewData[EmployeeProfileManagementForTeacherConstant.LIST_PROVINCE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");
            if (global.IsCurrentYear)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = false;
            }
        }

        #endregion setViewData

        #region Các danh sách cần thiết
        #region Lấy danh sách loại công việc
        int CachingTime = 2;
        public List<TrainingLevel> getListOfTrainingLevel()
        {
            string key = "CK_LISTGRADUATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<TrainingLevel> lsTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsTrainingLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsTrainingLevel;
            }
            else
            {
                return value as List<TrainingLevel>;
            }
        }

        #endregion
        #region Lấy danh sách các tỉnh thành
        public List<Province> getListOfProvince()
        {
            string key = "CK_LIST_PROVINCE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<Province> lsProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsProvince, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsProvince;
            }
            else
            {
                return value as List<Province>;
            }
        }
        #endregion
        #region Lấy danh sách nhóm công việc
        public List<WorkGroupType> getListOfWorkGroupType()
        {
            string key = "CK_LS_WORKGROUPTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<WorkGroupType> lsWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsWorkGroupType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsWorkGroupType;
            }
            else
            {
                return value as List<WorkGroupType>;
            }
        }
        #endregion
        #region Lấy danh sách loại công việc
        public List<WorkType> getListOfWorkType()
        {
            string key = "CK_LISTWORKTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<WorkType> lsWorkType = this.WorkTypeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsWorkType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsWorkType;
            }
            else
            {
                return value as List<WorkType>;
            }
        }
        #endregion

        public List<StaffPosition> getListOfStaffPosition()
        {
            string key = "CK_LS_STAFFPOSITION";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<StaffPosition> listStaffPosition = StaffPositionBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listStaffPosition, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listStaffPosition;
            }
            else
            {
                return value as List<StaffPosition>;
            }
        }

        public List<QualificationType> getListOfQualificationType()
        {
            string key = "CK_LS_QUALIFICATIONTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<QualificationType> listQualificationType = QualificationTypeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listQualificationType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listQualificationType;
            }
            else
            {
                return value as List<QualificationType>;
            }
        }

        public List<SpecialityCat> getListOfSpecialityCat()
        {
            string key = "CK_LS_SPECIALITYCAT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listSpecialityCat, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listSpecialityCat;
            }
            else
            {
                return value as List<SpecialityCat>;
            }
        }

        public List<QualificationLevel> getListOfQualificationLevel()
        {
            string key = "CK_LS_QUALIFICATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<QualificationLevel> listQualificationLevel = QualificationLevelBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listQualificationLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listQualificationLevel;
            }
            else
            {
                return value as List<QualificationLevel>;
            }
        }

        public List<ITQualificationLevel> getListOfITQualificationLevel()
        {
            string key = "CK_LS_ITQUALIFICATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listITQualificationLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listITQualificationLevel;
            }
            else
            {
                return value as List<ITQualificationLevel>;
            }
        }

        public List<StateManagementGrade> getListOfStateManagementGrade()
        {
            string key = "CK_LS_STATEMANAGEMENTGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listStateManagementGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listStateManagementGrade;
            }
            else
            {
                return value as List<StateManagementGrade>;
            }
        }

        public List<PoliticalGrade> getListOfPoliticalGrade()
        {
            string key = "CK_LS_POLITICALGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listPoliticalGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listPoliticalGrade;
            }
            else
            {
                return value as List<PoliticalGrade>;
            }
        }

        public List<Ethnic> getListOfEthnic()
        {
            string key = "CK_LS_ETHNIC";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<Ethnic> listEthnic = EthnicBusiness.Search(paras).OrderBy(o => o.EthnicName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listEthnic, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listEthnic;
            }
            else
            {
                return value as List<Ethnic>;
            }
        }

        /// <summary>
        /// Lay danh sach cac ton giao
        /// </summary>
        /// <returns></returns>
        public List<Religion> getListOfReligion()
        {
            string key = "CK_LS_RELIGION";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<Religion> listReligion = ReligionBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listReligion, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listReligion;
            }
            else
            {
                return value as List<Religion>;
            }
        }

        public List<FamilyType> getListOfFamilyType()
        {
            string key = "CK_LS_FAMILYTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<FamilyType> listFamilyType = FamilyTypeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listFamilyType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listFamilyType;
            }
            else
            {
                return value as List<FamilyType>;
            }
        }

        public List<ForeignLanguageGrade> getListOfForeignLanguageGrade()
        {
            string key = "CK_LS_FOREIGNLANGUAGEGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listForeignLanguageGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listForeignLanguageGrade;
            }
            else
            {
                return value as List<ForeignLanguageGrade>;
            }
        }

        public List<SupervisingDept> getListOfSupervisingDept()
        {
            string key = "CK_LS_SUPERVISINGDEPT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.Search(paras).OrderBy(o => o.SupervisingDeptName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listSupervisingDept, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listSupervisingDept;
            }
            else
            {
                return value as List<SupervisingDept>;
            }
        }

        public List<EducationalManagementGrade> getListOfEducationalManagementGrade()
        {
            string key = "CK_LS_EDUCATIONALMANAGEMENTGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<EducationalManagementGrade> listEducationalManagementGrade = EducationalManagementGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listEducationalManagementGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listEducationalManagementGrade;
            }
            else
            {
                return value as List<EducationalManagementGrade>;
            }
        }

        public List<Contract> getListOfContract()
        {
            string key = "CK_LS_CONTRACT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<Contract> listContract = ContractBusiness.All.Where(em => (em.IsActive == true)).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listContract, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listContract;
            }
            else
            {
                return value as List<Contract>;
            }
        }

        public List<ContractType> getListOfContractType()
        {
            string key = "CK_LISTCONTRACTTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<ContractType> lsContractType = ContractTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsContractType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsContractType;
            }
            else
            {
                return value as List<ContractType>;
            }
        }

        #endregion
    }
}
