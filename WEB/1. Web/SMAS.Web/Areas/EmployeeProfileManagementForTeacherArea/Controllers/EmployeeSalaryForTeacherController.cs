﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Controllers
{
    [SkipCheckRole]
    public class EmployeeSalaryForTeacherController : BaseController
    {
        //
        // GET: /EmployeeProfileManagementForTeacherArea/EmployeeSalaryForTeacher/

        private readonly IEmployeeSalaryBusiness EmployeeSalaryBusiness;
        private readonly ISalaryLevelBusiness SalaryLevelBusiness;
        private readonly IEmployeeScaleBusiness EmployeeScaleBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;

        public EmployeeSalaryForTeacherController(IEmployeeSalaryBusiness employeesalaryBusiness,
            ISalaryLevelBusiness SalaryLevelBusiness,
            IEmployeeScaleBusiness EmployeeScaleBusiness,
            IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeSalaryBusiness = employeesalaryBusiness;
            this.SalaryLevelBusiness = SalaryLevelBusiness;
            this.EmployeeScaleBusiness = EmployeeScaleBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        //public PartialViewResult Index()
        //{
        //    int EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0;//_globalInfo.UserAccountID;
        //    CheckPermissionForAction(EmployeeID, "Employee");
        //    GlobalInfo global = new GlobalInfo();
        //    Employee employee = this.EmployeeBusiness.Find(EmployeeID);
        //    this.LoadEmployeeSalary(employee);
        //    EmployeeProfileManagementForTeacherViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementForTeacherViewModel();
        //    SetViewData();
        //    if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
        //    {
        //        ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
        //    }
        //    else
        //    {
        //        ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
        //    }
        //    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
        //    SearchInfo["EmployeeID"] = EmployeeID;
        //    IEnumerable<EmployeeSalaryForTeacherViewModel> lst = new EmployeeInfoForTeacher().SearchEmployeeSalary(EmployeeSalaryBusiness, SearchInfo);
        //    ViewData[EmployeeSalaryForTeacherConstants.LIST_EMPLOYEESALARY] = lst;
        //    ViewData["checkReadonly"] = false;
        //    ViewData[EmployeeSalaryForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);
        //    return PartialView("../EmployeeSalaryForTeacher/Index", employee);
        //}

        //private void LoadEmployeeSalary(Employee employee)
        //{
        //    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
        //    GlobalInfo globalInfo = new GlobalInfo();

        //    IEnumerable<EmployeeSalaryForTeacherViewModel> lst = new EmployeeInfoForTeacher().SearchEmployeeSalary(EmployeeSalaryBusiness, new Dictionary<string, object> { { "EmployeeID", employee.EmployeeID } });
        //    ViewData[EmployeeSalaryForTeacherConstants.LIST_EMPLOYEESALARY] = lst;

        //    // Lay du lieu combobox
        //    SearchInfo.Clear();
        //    IQueryable<EmployeeScale> IQSalaryLevel = EmployeeScaleBusiness.Search(SearchInfo);
        //    SelectList listSalaryLevel = new SelectList((IEnumerable<EmployeeScale>)IQSalaryLevel.ToList(), "EmployeeScaleID", "Resolution");
        //    ViewData[EmployeeSalaryForTeacherConstants.LIST_SALARYSCALE] = listSalaryLevel;

        //    // Scale
        //    List<ComboObject> listComboObj = new List<ComboObject>();
        //    for (int i = 1; i <= EmployeeSalaryForTeacherConstants.MAX_GRADE; i++)
        //    {
        //        listComboObj.Add(new ComboObject(i.ToString(), Res.Get("Common_Label_Grade") + " " + i.ToString()));
        //    }
        //    SelectList listSalaryScale = new SelectList((IEnumerable<ComboObject>)listComboObj, "key", "value");
        //    ViewData[EmployeeSalaryForTeacherConstants.LIST_SALARYLEVEL] = listSalaryScale;
        //}

        #region setViewData

        //public void SetViewData()
        //{
        //    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
        //    GlobalInfo global = GlobalInfo.getInstance();
        //    var hasPermission = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);
        //    if (global.IsCurrentYear)
        //    {
        //        ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = true;
        //    }
        //    else
        //    {
        //        ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = false;
        //    }
        //}

        #endregion setViewData


        /// <summary>
        /// Lay he so
        /// </summary>
        /// <param name="salaryLevelID"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public JsonResult GetCoefficientByLevel(int EmployeeScaleID, int SubLevel)
        {
            IDictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam["ScaleID"] = EmployeeScaleID;
            dicParam["SubLevel"] = SubLevel;
            List<SalaryLevel> ListSalaryLevel = SalaryLevelBusiness.Search(dicParam).ToList();
            EmployeeSalaryForTeacherViewModel EmployeeSalaryViewModel = new EmployeeSalaryForTeacherViewModel();
            if (ListSalaryLevel.Count > 0)
            {
                SalaryLevel SalaryLevel = ListSalaryLevel[0];
                EmployeeSalaryViewModel.Coefficient = SalaryLevel.Coefficient;
                EmployeeSalaryViewModel.SalaryLevelID = SalaryLevel.SalaryLevelID;
            }
            return Json(EmployeeSalaryViewModel);
        }

        //
        // GET: /EmployeeSalary/Search


        public ActionResult Search()
        {
            int EmployeeID = _globalInfo.EmployeeID.HasValue ?_globalInfo.EmployeeID.Value : 0;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = EmployeeID;
            GlobalInfo global = new GlobalInfo();
            ViewData[EmployeeProfileManagementForTeacherConstant.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);
            IEnumerable<EmployeeSalaryForTeacherViewModel> lst = new EmployeeInfoForTeacher().SearchEmployeeSalary(EmployeeSalaryBusiness, SearchInfo);
            ViewData[EmployeeSalaryForTeacherConstants.LIST_EMPLOYEESALARY] = lst;
            Employee em = EmployeeBusiness.Find(EmployeeID);
            if (em.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
            }
            //Get view data here
            ViewData["checkReadonly"] = false;
            return PartialView("../EmployeeSalaryForTeacher/_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create()
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeSalary employeesalary = new EmployeeSalary();
            TryUpdateModel(employeesalary);
            Utils.Utils.TrimObject(employeesalary);
            employeesalary.SchoolID = globalInfo.SchoolID;
            employeesalary.SupervisingDeptID = globalInfo.SupervisingDeptID;

            this.EmployeeSalaryBusiness.Insert(employeesalary);
            this.EmployeeSalaryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(int EmployeeSalaryID)
        {
            GlobalInfo globalInfo = new GlobalInfo();           
            CheckPermissionForAction(EmployeeSalaryID, "EmployeeSalary");
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeSalary employeesalary = this.EmployeeSalaryBusiness.Find(EmployeeSalaryID);
            TryUpdateModel(employeesalary);
            Utils.Utils.TrimObject(employeesalary);
            employeesalary.SchoolID = globalInfo.SchoolID;
            employeesalary.SupervisingDeptID = globalInfo.SupervisingDeptID;
            this.EmployeeSalaryBusiness.Update(employeesalary);
            this.EmployeeSalaryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo globalInfo = new GlobalInfo();         
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(id, "EmployeeSalary");
            this.EmployeeSalaryBusiness.Delete(id);
            this.EmployeeSalaryBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

    }
}
