﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using Newtonsoft.Json;
using System.Text;
using Telerik.Web.Mvc;
using SMAS.Web.Areas.EmployeeBreatherArea;
using SMAS.VTUtils.Log;
namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Controllers
{
    [SkipCheckRole]
    public class EmployeeWorkingHistoryForTeacherController : BaseController
    {
        //
        // GET: /EmployeeProfileManagementForTeacherArea/EmployeeWorkingHistoryForTeacher/

        #region variable
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBreatherBusiness EmployeeBreatherBusiness;

        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IQualificationLevelBusiness QualificationLevelBusiness;

        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;

        private readonly IStateManagementGradeBusiness StateManagementGradeBusiness;
        private readonly IPoliticalGradeBusiness PoliticalGradeBusiness;

        private readonly IEducationalManagementGradeBusiness EducationalManagementGradeBusiness;

        private readonly IStaffPositionBusiness StaffPositionBusiness;
        private readonly IContractBusiness ContractBusiness;
        private readonly IWorkGroupTypeBusiness WorkGroupTypeBusiness;
        private readonly IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IQualificationGradeBusiness QualificationGradeBusiness;
        private IEmployeeQualificationBusiness EmployeeQualificationBusiness;
        private IEmployeeSalaryBusiness EmployeeSalaryBusiness;
        private IEmployeeScaleBusiness EmployeeScaleBusiness;
        private string GRADE = Res.Get("Common_Label_Grade");
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private string TEMPLATE_EMPLOYEE = "GV_DanhSachCanBo.xls";
        private string TEMPLATE_EMPLOYEE_NAME = "GV_DanhSachCanBo_{0}.xls";
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness;

        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IVacationReasonBusiness VacationReasonBusiness;

        #endregion variable

        #region contructor

        public EmployeeWorkingHistoryForTeacherController(IEmployeeBusiness employeeBusiness
            , ISchoolFacultyBusiness SchoolFacultyBusiness
            , IWorkTypeBusiness WorkTypeBusiness
            , IGraduationLevelBusiness GraduationLevelBusiness
            , IContractTypeBusiness ContractTypeBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness,
            IGroupCatBusiness GroupBusiness, IEthnicBusiness EthnicBusiness, IReligionBusiness ReligionBusiness
            , IFamilyTypeBusiness FamilyTypeBusiness, IQualificationTypeBusiness QualificationTypeBusiness,
            ISpecialityCatBusiness SpecialityCatBusiness, IQualificationLevelBusiness QualificationLevelBusiness,
            IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness, IITQualificationLevelBusiness ITQualificationLevelBusiness,
            IStateManagementGradeBusiness StateManagementGradeBusiness,
            IPoliticalGradeBusiness PoliticalGradeBusiness,
            IEducationalManagementGradeBusiness EducationalManagementGradeBusiness,
            IStaffPositionBusiness StaffPositionBusiness,
            IContractBusiness ContractBusiness
            , IWorkGroupTypeBusiness WorkGroupTypeBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness
            , IQualificationGradeBusiness QualificationGradeBusiness
            , IEmployeeQualificationBusiness EmployeeQualificationBusiness
            , IEmployeeSalaryBusiness EmployeeSalaryBusiness
            , IEmployeeScaleBusiness EmployeeScaleBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ICodeConfigBusiness CodeConfigBusiness
            , IProvinceBusiness ProvinceBusiness
            , ITrainingLevelBusiness TrainingLevelBusiness
            , IEmployeeWorkMovementBusiness employeeWorkMovementBusiness
            , IEmployeeBreatherBusiness employeeBreatherBusiness
            , IUserAccountBusiness UserAccountBusiness
            , IVacationReasonBusiness VacationReasonBusiness)
        {
            this.TrainingLevelBusiness = TrainingLevelBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.WorkTypeBusiness = WorkTypeBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.GroupCatBusiness = GroupBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.ReligionBusiness = ReligionBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.QualificationLevelBusiness = QualificationLevelBusiness;
            this.ForeignLanguageGradeBusiness = ForeignLanguageGradeBusiness;
            this.ITQualificationLevelBusiness = ITQualificationLevelBusiness;
            this.StateManagementGradeBusiness = StateManagementGradeBusiness;
            this.PoliticalGradeBusiness = PoliticalGradeBusiness;

            this.EducationalManagementGradeBusiness = EducationalManagementGradeBusiness;
            this.StaffPositionBusiness = StaffPositionBusiness;
            this.ContractBusiness = ContractBusiness;
            this.WorkGroupTypeBusiness = WorkGroupTypeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.EmployeeWorkingHistoryBusiness = EmployeeWorkingHistoryBusiness;
            this.QualificationGradeBusiness = QualificationGradeBusiness;
            this.EmployeeQualificationBusiness = EmployeeQualificationBusiness;
            this.EmployeeSalaryBusiness = EmployeeSalaryBusiness;
            this.EmployeeScaleBusiness = EmployeeScaleBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.EmployeeWorkMovementBusiness = employeeWorkMovementBusiness;
            this.EmployeeBreatherBusiness = employeeBreatherBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.VacationReasonBusiness = VacationReasonBusiness;
        }

        #endregion contructor

        #region EmployeeWorkingHistory

        public ActionResult Index() 
        {
            bool isPermiss = true;
            int EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0;//_globalInfo.UserAccountID;
            CheckPermissionForAction(EmployeeID, "Employee");
            GlobalInfo global = new GlobalInfo();
            Employee employee = this.EmployeeBusiness.Find(EmployeeID);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
            }
            EmployeeProfileManagementForTeacherViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementForTeacherViewModel();

            SetViewData();

            if (global.IsSchoolRole)
            {
                if (employee.SchoolID.HasValue)
                    isPermiss &= employee.SchoolID.Value == global.SchoolID.Value;
            }
            else if (global.IsSuperVisingDeptRole)
            {
                if (employee.SupervisingDeptID.HasValue)
                    isPermiss &= employee.SupervisingDeptID.Value == global.SupervisingDeptID.Value;
            }
            else
            {
                isPermiss &= false;
            }


            ViewData[EmployeeWorkingHistoryForTeacherConstants.IS_PERMISS] = isPermiss;
            ViewData[EmployeeWorkingHistoryForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            IEnumerable<EmployeeWorkingHistoryForTeacherViewModel> lst = employee.EmployeeWorkingHistories
                                                                        .Select(o => new EmployeeWorkingHistoryForTeacherViewModel
                                                                        {
                                                                            EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
                                                                            EmployeeID = o.EmployeeID,
                                                                            EmployeeName = o.Employee.FullName,
                                                                            SupervisingDeptID = o.SupervisingDeptID,
                                                                            SchoolID = o.SchoolID,
                                                                            Organization = o.Organization,
                                                                            Department = o.Department,
                                                                            Position = o.Position,
                                                                            Resolution = o.Resolution,
                                                                            FromDate = o.FromDate,
                                                                            ToDate = o.ToDate
                                                                        })
                                                                        .OrderByDescending(o => o.FromDate)
                                                                        .ThenBy(u => u.EmployeeName)
                                                                        .ToList();
            ViewData[EmployeeWorkingHistoryForTeacherConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;
            ViewData[EmployeeProfileManagementForTeacherConstant.CHOSENEMPLOYEE] = EmployeeProfileManagementViewModel;
            ViewData["checkReadonly"] = false;
            return PartialView("../EmployeeWorkingHistoryForTeacher/IndexEmployeeWorkingHistory", employee);
        }

        #region setViewData

        public void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = GlobalInfo.getInstance();
            var hasPermission = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);
            if (global.IsCurrentYear)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = false;
            }
        }

        #endregion setViewData


        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult CreateEmployeeWorkingHistory(int EmployeeProfileManagementViewModelID)
        {
            CheckPermissionForAction(EmployeeProfileManagementViewModelID, "Employee");
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData["checkReadonly"] = false;
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeWorkingHistory employeeworkinghistory = new EmployeeWorkingHistory();
            TryUpdateModel(employeeworkinghistory);
            Utils.Utils.TrimObject(employeeworkinghistory);

            //fix cứng
            employeeworkinghistory.EmployeeID = EmployeeProfileManagementViewModelID;
            //tu employeeid lay supervisingdeptid va schoolid tong vao object
            Employee temp = EmployeeBusiness.All.Where(o => (o.EmployeeID == employeeworkinghistory.EmployeeID)).FirstOrDefault();

            employeeworkinghistory.SchoolID = temp.SchoolID;
            employeeworkinghistory.SupervisingDeptID = temp.SupervisingDeptID;

            this.EmployeeWorkingHistoryBusiness.Insert(employeeworkinghistory);
            this.EmployeeWorkingHistoryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }


        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult EditEmployeeWorkingHistory(int EmployeeProfileManagementViewModelID, int EmployeeWorkingHistoryID)
        {
            CheckPermissionForAction(EmployeeProfileManagementViewModelID, "Employee");
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData["checkReadonly"] = false;
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeWorkingHistory employeeworkinghistory = this.EmployeeWorkingHistoryBusiness.Find(EmployeeWorkingHistoryID);
            TryUpdateModel(employeeworkinghistory);
            Utils.Utils.TrimObject(employeeworkinghistory);
            ViewData["checkReadonly"] = false;
            //fix cứng
            employeeworkinghistory.EmployeeID = EmployeeProfileManagementViewModelID;

            //tu employeeid lay supervisingdeptid va schoolid tong vao object
            Employee temp = EmployeeBusiness.All.Where(o => (o.EmployeeID == employeeworkinghistory.EmployeeID)).FirstOrDefault();

            employeeworkinghistory.SchoolID = temp.SchoolID;
            employeeworkinghistory.SupervisingDeptID = temp.SupervisingDeptID;

            this.EmployeeWorkingHistoryBusiness.Update(employeeworkinghistory);
            this.EmployeeWorkingHistoryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        public PartialViewResult SearchEmployeeWorkingHistory(int EmployeeID, FormCollection fc)
        {
            GlobalInfo global = new GlobalInfo();
            //Nếu UserInfo.IsSchoolRole = TRUE: kiểm tra nếu Employee(EmployeeID).SchoolID <> UserInfo.SchoolID =>
            //Chuyển sang trang thông báo lỗi: “Bạn không có quyền truy cập vào dữ liệu này -Common_Label_HasHeadTeacherPermissionError
            Employee em = EmployeeBusiness.Find(EmployeeID);
            if (em.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementForTeacherConstant.EMPLOYEE_WORKING] = "false";
            }
            bool isPermiss = true;
            if (global.IsSchoolRole)
            {
                if (em.SchoolID.HasValue)
                {
                    if (em.SchoolID.Value != global.SchoolID.Value)
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= false;
                    }
                    else
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= true;
                    }
                }
            }
            else if (global.IsSuperVisingDeptRole)
            {
                if (em.SupervisingDeptID.HasValue)
                {
                    if (em.SupervisingDeptID.Value != global.SupervisingDeptID.Value)
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= false;
                    }
                    else
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= true;
                    }
                }
            }
            else
            {
                isPermiss &= false;
            }


            ViewData[EmployeeWorkingHistoryForTeacherConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin);

            ViewData[EmployeeWorkingHistoryForTeacherConstants.IS_PERMISS] = isPermiss;
            ViewData[EmployeeProfileManagementForTeacherConstant.IS_CURRENT_YEAR] = global.IsCurrentYear;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //
            SearchInfo["EmployeeID"] = EmployeeID; //tam thoi cho fix cung id
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;

            IEnumerable<EmployeeWorkingHistoryForTeacherViewModel> lst = this._SearchEmployeeWorkingHistory(SearchInfo);
            ViewData[EmployeeWorkingHistoryForTeacherConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;

            //Get view data here
            ViewData["checkReadonly"] = false;
            return PartialView("../EmployeeWorkingHistoryForTeacher/_ListEmployeeWorkingHistory", lst);
        }

        private IEnumerable<EmployeeWorkingHistoryForTeacherViewModel> _SearchEmployeeWorkingHistory(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<EmployeeWorkingHistory> query = EmployeeWorkingHistoryBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            IQueryable<EmployeeWorkingHistoryForTeacherViewModel> lst = query.Select(o => new EmployeeWorkingHistoryForTeacherViewModel
            {
                EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
                EmployeeID = o.EmployeeID,
                EmployeeName = o.Employee.FullName,
                SupervisingDeptID = o.SupervisingDeptID,
                SchoolID = o.SchoolID,
                Organization = o.Organization,
                Department = o.Department,
                Position = o.Position,
                Resolution = o.Resolution,
                FromDate = o.FromDate,
                ToDate = o.ToDate
            });

            List<EmployeeWorkingHistoryForTeacherViewModel> lsEmployeeWorkingHistoryViewModel = lst.ToList();
            return lsEmployeeWorkingHistoryViewModel.OrderByDescending(o => o.FromDate).OrderBy(o => o.EmployeeName).ToList();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmployeeWorkingHistory(int id)
        {
            CheckPermissionForAction(id, "EmployeeWorkingHistory");
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("EmployeeProfileManagementForTeacher", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //Nếu UserInfo.IsSchoolRole = TRUE:
            //EmployeeWorkingHistoryBusiness.Delete(UserInfo.SchoolID, EmployeeWorkingHistoryID)
            if (global.IsSchoolRole)
            {
                this.EmployeeWorkingHistoryBusiness.Delete(global.SchoolID.Value, id);
                this.EmployeeWorkingHistoryBusiness.Save();
            }
            if (global.IsSuperVisingDeptRole)
            {
                this.EmployeeWorkingHistoryBusiness.Delete(global.SupervisingDeptID.Value, id);
                this.EmployeeWorkingHistoryBusiness.Save();
            }
            ViewData["checkReadonly"] = false;
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion EmployeeWorkingHistory

    }
}
