﻿using SMAS.Web.Utils;
using System.Collections.Generic;
using System.Linq;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Models;

namespace SMAS.Web.Areas.EmployeeProfileManagementForTeacherArea.Controllers
{
    public class EmployeeInfoForTeacher
    {
        private string GRADE = Res.Get("Common_Label_Grade");

        #region Tim kiem thong tin dao tao cua can bo

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeQualificationForTeacherViewModel> SearchEmployeeQualification(IEmployeeQualificationBusiness EmployeeQualificationBusiness,
            IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<EmployeeQualification> query = EmployeeQualificationBusiness.All;
            if (global.SchoolID != null)
            {
                query = EmployeeQualificationBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            }
            else
            {
                query = EmployeeQualificationBusiness.All.Where(o => o.EmployeeID == global.EmployeeID);
            }
            IQueryable<EmployeeQualificationForTeacherViewModel> lst = query.Select(o => new EmployeeQualificationForTeacherViewModel
            {
                EmployeeQualificationID = o.EmployeeQualificationID,
                EmployeeID = o.EmployeeID,
                EmployeeName = o.Employee.FullName,
                SchoolID = o.SchoolID,
                SupervisingDeptID = o.SupervisingDeptID,
                SpecialityID = o.SpecialityID,
                SpecialityName = o.SpecialityCat.Resolution,
                QualificationTypeID = o.QualificationTypeID,
                QualificationTypeName = o.QualificationType.Resolution,
                //TrainingLevelID = (int?)o.Employee.TrainingLevelID,
                //TrainingLevelName = o.Employee.TrainingLevel.Resolution,
                TrainingLevelID = (int?)o.TrainingLevelID,
                TrainingLevelName = o.TrainingLevel.Resolution,
                //QualificationGradeID = o.QualificationGradeID,
                QualificationGradeName = o.QualificationGrade.Resolution,
                Result = o.Result,
                QualifiedAt = o.QualifiedAt,
                FromDate = o.FromDate,
                QualifiedDate = o.QualifiedDate,
                IsPrimaryQualification = o.IsPrimaryQualification,
                IsRequalified = o.IsRequalified,
                Description = o.Description
            });

            IEnumerable<EmployeeQualificationForTeacherViewModel> listRes = lst.ToList();
            foreach (EmployeeQualificationForTeacherViewModel item in listRes)
            {
                item.IsRequalifiedName = (item.IsRequalified.HasValue) ?
                    (item.IsRequalified.Value ?
                    Res.Get("EmployeeQualification_Label_ReTraining") : Res.Get("EmployeeQualification_Label_FirstTraining"))
                    : string.Empty;
            }
            return listRes;
        }

        #endregion Tim kiem thong tin dao tao cua can bo

        #region Tim kiem thong tin luong cua can bo

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeSalaryForTeacherViewModel> SearchEmployeeSalary(IEmployeeSalaryBusiness EmployeeSalaryBusiness,
            IDictionary<string, object> SearchInfo)
        {
            IQueryable<EmployeeSalary> query = EmployeeSalaryBusiness.Search(SearchInfo);
            IQueryable<EmployeeSalaryForTeacherViewModel> lst = query.Select(o => new EmployeeSalaryForTeacherViewModel
            {
                EmployeeSalaryID = o.EmployeeSalaryID,
                EmployeeID = o.EmployeeID,
                EmployeeName = o.Employee != null ? o.Employee.FullName : string.Empty,
                EmployeeScaleID = o.EmployeeScaleID,
                EmployeeScaleName = o.EmployeeScale != null ? o.EmployeeScale.Resolution : string.Empty,
                SalaryLevelID = o.SalaryLevelID,
                SubLevel = o.SalaryLevel != null ? o.SalaryLevel.SubLevel : 0,
                Coefficient = o.SalaryLevel != null ? o.SalaryLevel.Coefficient : null,
                SchoolID = o.SchoolID,
                SupervisingDeptID = o.SupervisingDeptID,
                AppliedDate = o.AppliedDate,
                SalaryResolution = o.SalaryResolution,
                SalaryAmount = o.SalaryAmount,
                Description = o.Description
            });

            IEnumerable<EmployeeSalaryForTeacherViewModel> listRes = lst.ToList();
            foreach (EmployeeSalaryForTeacherViewModel item in listRes)
            {
                item.SalaryLevelName = item.SubLevel != 0 ? GRADE + " " + item.SubLevel : string.Empty;
            }
            return listRes.OrderByDescending(o => o.AppliedDate);
        }

        #endregion Tim kiem thong tin luong cua can bo
    }
}