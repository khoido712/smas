﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilAwayArea.Models
{
    public class PupilByDateViewModel
    {
        public int PupilID { get; set; }
        public DateTime? AbsenceDate { get; set; }
        public string ClassName { get; set; }
        public string FullName { get; set; }
    }
}