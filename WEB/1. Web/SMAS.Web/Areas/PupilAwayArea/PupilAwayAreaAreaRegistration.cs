﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilAwayArea
{
    public class PupilAwayAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilAwayArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilAwayArea_default",
                "PupilAwayArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
