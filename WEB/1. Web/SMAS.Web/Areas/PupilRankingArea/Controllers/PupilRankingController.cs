﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv3
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PupilRankingArea.Models;

namespace SMAS.Web.Areas.PupilRankingArea.Controllers
{
    using GlobalConstants = SMAS.Web.Constants.GlobalConstants;
    using SMAS.VTUtils.HtmlHelpers;
    using SMAS.Business.BusinessObject;
    using System.Transactions;

    [ViewableBySupervisingDeptFilter]
    public class PupilRankingController : BaseController
    {
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IHonourAchivementTypeBusiness HonourAchivementTypeBusiness;
        private readonly IStudyingJudgementBusiness StudyingJudgementBusiness;
        private readonly ICapacityLevelBusiness CapacityLevelBusiness;
        private readonly IPupilEmulationBusiness PupilEmulationBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        private readonly IPupilRankingHistoryBusiness PupilRankingHistoryBusiness;
        public PupilRankingController(IPupilRankingBusiness pupilrankingBusiness, IClassProfileBusiness classprofilebusiness, IConductLevelBusiness conductlevelbusiness,
            IHonourAchivementTypeBusiness honourachivementtypebusiness, IStudyingJudgementBusiness studyingjudgementbusiness, ICapacityLevelBusiness capacitylevelbusiness,
            IPupilEmulationBusiness pupilemulationbusiness, IEducationLevelBusiness EducationLevelBusiness, IPupilAbsenceBusiness PupilAbsenceBusiness, ISchoolMovementBusiness SchoolMovementBusiness, IAcademicYearBusiness AcademicYearBusiness,
            ISummedUpRecordClassBusiness summedUpRecordClassBusiness,
            IPupilRankingHistoryBusiness _PupilRankingHistoryBusiness)
        {
            this.PupilRankingBusiness = pupilrankingBusiness;
            this.ClassProfileBusiness = classprofilebusiness;
            this.ConductLevelBusiness = conductlevelbusiness;
            this.HonourAchivementTypeBusiness = honourachivementtypebusiness;
            this.StudyingJudgementBusiness = studyingjudgementbusiness;
            this.CapacityLevelBusiness = capacitylevelbusiness;
            this.PupilEmulationBusiness = pupilemulationbusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.PupilAbsenceBusiness = PupilAbsenceBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SummedUpRecordClassBusiness = summedUpRecordClassBusiness;
            this.PupilRankingHistoryBusiness = _PupilRankingHistoryBusiness;
        }

        //
        // GET: /PupilRanking/

        public ActionResult Index()
        {
            var global = new GlobalInfo();
            if (global.HasHeadTeacherPermission(0) || global.IsViewAll)
            {
                //- rdoSemester: Nếu UserInfo.AppliedLevel = APPLIED_LEVEL_PRIMARY(Cấp 1) Chỉ lấy ra học kỳ 2: 2
                // Nếu UserInfo.AppliedLevel = APPLIED_LEVEL_SECONDARY hoặc  = APPLIED_LEVEL_TERTIARY (Cấp 2 hoặc 3):
                //   =>CommonList.SemesterAndAll
                List<ViettelCheckboxList> lstViettelCheckboxListSemester = new List<ViettelCheckboxList>();
                if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    //lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(Res.Get("PupilRanking_Label_SemesterChecked"), 2, true, false));
                    var lstSemester = CommonList.Semester();
                    for (int i = 0; i < lstSemester.Count(); i++)
                    {
                        if (int.Parse(lstSemester[i].key) == global.Semester)
                        {
                            lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(lstSemester[i].value, lstSemester[i].key, true, false));
                        }
                        else
                        {
                            lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(lstSemester[i].value, lstSemester[i].key, false, false));
                        }

                    }
                }
                if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    ////int? academic = global.AcademicYearID;
                    //int? semester = global.Semester;
                    //bool chk = false;
                    var lstSemester = CommonList.SemesterAndAll();
                    for (int i = 0; i < lstSemester.Count(); i++)
                    {
                        if (int.Parse(lstSemester[i].key) == global.Semester)
                        {
                            lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(lstSemester[i].value, lstSemester[i].key, true, false));
                        }
                        else
                        {
                            lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(lstSemester[i].value, lstSemester[i].key, false, false));
                        }

                    }
                }
                ViewData[PupilRankingConstants.LIST_SEMESTER] = lstViettelCheckboxListSemester;
                //- rptEducationLevel: UserInfo.EducationLevels
                var lstEducationlevel = global.EducationLevels;
                List<ViettelCheckboxList> lstViettelCheckboxListEducationlevel = new List<ViettelCheckboxList>();
                lstViettelCheckboxListEducationlevel.Add(new ViettelCheckboxList(lstEducationlevel[0].Resolution, lstEducationlevel[0].EducationLevelID, true, false));
                foreach (var item in lstEducationlevel.Skip(1))
                {
                    lstViettelCheckboxListEducationlevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, false, false));
                }
                ViewData[PupilRankingConstants.LIST_EDUCATIONLEVEL] = lstViettelCheckboxListEducationlevel;
                //- rptClass: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary) với   
                //  + Dictionnary[“EducationLevelID”] = rptEducationLevel.Value
                //  + Dictionnary[“AcademicYearID”] = UserInfo.AcademicYearID
                //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
                //  + Dictionnary[“UserAccountID”] = UserInfo.UserAccountID
                //  + Dictionary[“Type”] = TEACHER_ROLE_HEADTEACHER (GVCN)
                IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
                ClassSearchInfo["EducationLevelID"] = lstEducationlevel.FirstOrDefault().EducationLevelID;
                ClassSearchInfo["AcademicYearID"] = global.AcademicYearID;
                //edited by namdv - 18/04/2013- bugs 167554
                if (!global.IsAdminSchoolRole && !global.IsViewAll)
                {
                    ClassSearchInfo["UserAccountID"] = global.UserAccountID;
                    ClassSearchInfo["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                var lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, ClassSearchInfo).OrderBy(o => o.DisplayName).ToList();
                ViewData[PupilRankingConstants.LIST_CLASS] = lstClass;

                ViewData[PupilRankingConstants.DISABLESAVEBTN] = true;
                ViewData["Semester"] = global.Semester;
                ViewData["AppliedLevel"] = global.AppliedLevel;
                ViewData["TitleSearch"] = string.Empty;
                ViewData["TitleNote"] = string.Empty;
                return View();
            }
            return RedirectToAction("LogOn", "Home");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult GetListClass(int? id)
        {
            if (id.HasValue)
            {
                var global = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = global.AcademicYearID.Value;
                dic["EducationLevelID"] = id.Value;
                //edited by namdv - 18/04/2013- bugs 167554
                if (!global.IsAdminSchoolRole && !global.IsViewAll)
                {
                    dic["UserAccountID"] = global.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
                ViewData[PupilRankingConstants.LIST_CLASS] = listClass;
            }
            return PartialView("_ListClass");
        }

        //
        // GET: /PupilRanking/Search

        [HttpPost]
        public PartialViewResult Search()
        {
            var global = new GlobalInfo();
            //int studyJudgementID;
            var r = Request;
            List<PupilRankingBO> ListPupilRankingBO = new List<PupilRankingBO>();
            ViewData[PupilRankingConstants.DISABLESAVEBTN] = false;
            int classid = string.IsNullOrWhiteSpace(r["classid"]) ? 0 : int.Parse(r["classid"]);
            int semesterid = string.IsNullOrWhiteSpace(r["semesterid"]) ? 0 : int.Parse(r["semesterid"]);
            int appliedlevelid = string.IsNullOrWhiteSpace(r["appliedlevelid"]) ? 0 : int.Parse(r["appliedlevelid"]);
            List<HonourAchivementType> lstHonourAchievementType = HonourAchivementTypeBusiness.Search(new Dictionary<string, object> { { "Type", SystemParamsInFile.HONOUR_ACHIVEMENT_TYPE_PUPIL } }).ToList();
            List<StudyingJudgement> lstStudyingJudgement = StudyingJudgementBusiness.All.ToList();
            #region check quyen
            bool check = UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, classid);

            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime? EndDateSemester = DateTime.Now;
            if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                EndDateSemester = academicYear.FirstSemesterEndDate;
            }
            else if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                EndDateSemester = academicYear.SecondSemesterEndDate;
            }

            if (check == false || global.IsCurrentYear == false)
            {
                ViewData[PupilRankingConstants.DISABLESAVEBTN] = true;
            }
            else
            {
                if (global.IsAdminSchoolRole)
                {
                    ViewData[PupilRankingConstants.DISABLESAVEBTN] = false;
                }
                else
                {

                    if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (academicYear != null && (academicYear.FirstSemesterStartDate > DateTime.Now || DateTime.Now > academicYear.FirstSemesterEndDate))
                        {
                            ViewData[PupilRankingConstants.DISABLESAVEBTN] = true;
                        }
                    }
                    if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                    {
                        if (academicYear != null && (academicYear.SecondSemesterStartDate > DateTime.Now || DateTime.Now > academicYear.SecondSemesterEndDate))
                        {
                            ViewData[PupilRankingConstants.DISABLESAVEBTN] = true;
                        }
                    }
                }
            }
            #endregion
            //1.	Nếu UserInfo.AppliedLevel = APPLIED_LEVEL_PRIMARY(Cấp 1)
            #region Cấp I
            if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                /*
                 Gọi hàm PupilRankingBusiness.GetPupilToRankPrimary(Dictionary) với
                + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                + Dictionary[“ClassID”]: rptClass.Value
                + Dictionary[“SchoolID”]: UserInfo.SchoolID
                	lstPupilToRank Lấy kết quả thu được đưa lên Grid
                 */
                IDictionary<string, object> DicPupilRanking = new Dictionary<string, object>();
                DicPupilRanking["AcademicYearID"] = global.AcademicYearID;
                DicPupilRanking["ClassID"] = classid;
                DicPupilRanking["SchoolID"] = global.SchoolID;
                DicPupilRanking["Semester"] = semesterid;
                //var lstPupilToRank = PupilRankingBusiness.GetPupilToRankPrimary(DicPupilRanking);
                List<PupilRankingBO> lstPupilToRank = PupilRankingBusiness.GetPupilToRankPrimaryv2(DicPupilRanking);
                List<CapacityLevel> lstCapacityLevel = CapacityLevelBusiness.All.ToList();
                List<ConductLevel> lstConductLevel = ConductLevelBusiness.All.ToList();
                //DungVA
                IDictionary<string, object> DicTotal = new Dictionary<string, object>();
                DicTotal["AcademicYearID"] = global.AcademicYearID;
                DicTotal["ClassID"] = classid;
                DicTotal["SchoolID"] = global.SchoolID;
                //DicTotal["Semester"] = string.IsNullOrWhiteSpace(r["semesterid"]) ? 0 : int.Parse(r["semesterid"]);
                //List<PupilAbsence> totalAbsentDaysWithoutPermission = PupilAbsenceBusiness.SearchBySchool(global.SchoolID.Value, DicTotal).ToList();

                if (lstPupilToRank != null && lstPupilToRank.Count() > 0)
                {
                    //if (lstPupilToRank.Any(o => o.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING) &&
                    //    (lstPupilToRank.Any(o => o.CapacityLevelID == null) || lstPupilToRank.Any(o => o.ConductLevelID == null)))
                    if (!lstPupilToRank.Any(o => o.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING && o.ConductLevelID.HasValue && o.CapacityLevelID.HasValue))
                    //(lstPupilToRank.Any(o => o.ConductLevelID == null)))
                    {
                        ViewData[PupilRankingConstants.DISABLESAVEBTN] = true;
                        ViewData[PupilRankingConstants.MESSAGE_ERROR] = Res.Get("PupilRanking_Label_ExitsPupilNotCapacity");
                    }
                    DateTime transferDate = DateTime.Now;
                    AcademicYear ay = AcademicYearBusiness.Find(global.AcademicYearID);
                    foreach (var pr in lstPupilToRank)
                    {
                        //if (pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL ||
                        //    pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_LEAVED_OFF ||
                        //    pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                        //    pr.Enable = false;
                        //else pr.Enable = true;
                        pr.Enable = pr.ShowMark;

                        if (pr.Genre.HasValue && pr.Genre.Value == GlobalConstants.GENRE_MALE)
                        {
                            pr.GenreDetail = GlobalConstants.GENRE_MALE_TITLE;
                        }
                        if (pr.Genre.HasValue && pr.Genre.Value == GlobalConstants.GENRE_FEMALE)
                        {
                            pr.GenreDetail = GlobalConstants.GENRE_FEMALE_TITLE;
                        }

                        // Lấy ra ngày chuyển trường, chuyển lớp, thôi học với các trường hợp này
                        // Nếu ngày chuyển lớn hơn ngày kết thúc học kỳ đang thao tác thì cho hiển thị dữ liệu các đối tượng chuyển
                        // Nếu ngày chuyển nhỏ hơn ngày kết thúc học kỳ đang thao tác thì cho hiển thị dữ liệu các đối tượng chuyển
                        // các đối tượng đang học thì hiển thị bình thường
                        DicTotal["PupilID"] = pr.PupilID;
                        if (pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL ||
                            pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_LEAVED_OFF ||
                            pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                        {
                            if (pr.ShowMark)
                            {
                                var conductlevel = lstConductLevel.Where(o => o.ConductLevelID == pr.ConductLevelID).FirstOrDefault();
                                pr.ConductLevelName = conductlevel != null ? CommonConvert.DisplayConductLevel(conductlevel.Resolution) : string.Empty;
                                var capacitylevel = lstCapacityLevel.Where(o => o.CapacityLevelID == pr.CapacityLevelID).FirstOrDefault();
                                pr.CapacityLevel = capacitylevel != null ? capacitylevel.Description : string.Empty;
                                var honourachivementtype = HonourAchivementTypeBusiness.All.Where(o => o.HonourAchivementTypeID == pr.HonourAchivementTypeID).FirstOrDefault();
                                pr.HonourAchivementTypeResolution = honourachivementtype != null ? honourachivementtype.Resolution : string.Empty;
                                var studyingjudgement = lstStudyingJudgement.Where(o => o.StudyingJudgementID == pr.StudyingJudgementID).FirstOrDefault();
                                if (studyingjudgement != null)
                                {
                                    if (studyingjudgement.StudyingJudgementID == 5)
                                    {
                                        if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                        {
                                            pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation1");
                                        }
                                        else if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                                        {
                                            pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation2");
                                        }
                                        else if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                                        {
                                            pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation3");
                                        }

                                    }
                                    else
                                    {
                                        pr.StudyingJudgementResolution = studyingjudgement.Resolution;
                                    }
                                }
                                else
                                {
                                    pr.StudyingJudgementResolution = string.Empty;
                                }
                            }
                            else
                            {
                                // Ngày chuyển nhỏ hơn hơn thì không hiển thị
                                pr.ConductLevelName = string.Empty;
                                pr.CapacityLevel = string.Empty;
                                pr.HonourAchivementTypeResolution = string.Empty;
                                pr.StudyingJudgementResolution = string.Empty;
                            }

                        }
                        else
                        {
                            // Trường hợp đang học hiển thị bình thường
                            var conductlevel = lstConductLevel.Where(o => o.ConductLevelID == pr.ConductLevelID).FirstOrDefault();
                            pr.ConductLevelName = conductlevel != null ? CommonConvert.DisplayConductLevel(conductlevel.Resolution) : string.Empty;
                            var capacitylevel = lstCapacityLevel.Where(o => o.CapacityLevelID == pr.CapacityLevelID).FirstOrDefault();
                            pr.CapacityLevel = capacitylevel != null ? capacitylevel.Description : string.Empty;
                            var honourachivementtype = lstHonourAchievementType.Where(o => o.HonourAchivementTypeID == pr.HonourAchivementTypeID).FirstOrDefault();
                            pr.HonourAchivementTypeResolution = honourachivementtype != null ? honourachivementtype.Resolution : string.Empty;
                            var studyingjudgement = StudyingJudgementBusiness.All.Where(o => o.StudyingJudgementID == pr.StudyingJudgementID).FirstOrDefault();
                            if (studyingjudgement != null)
                            {
                                if (studyingjudgement.StudyingJudgementID == 5)
                                {
                                    if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation1");
                                    }
                                    else if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                                    {
                                        pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation2");
                                    }
                                    else if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                                    {
                                        pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation3");
                                    }

                                }
                                else
                                {
                                    pr.StudyingJudgementResolution = studyingjudgement.Resolution;
                                }
                            }
                            else
                            {
                                pr.StudyingJudgementResolution = string.Empty;
                            }
                        }

                        //if (totalAbsentDaysWithoutPermission != null)
                        //{
                        //    pr.ToTalAbsentDay = totalAbsentDaysWithoutPermission.Where(o => o.PupilID == pr.PupilID).Count();
                        //}
                        //else
                        //{
                        //    pr.ToTalAbsentDay = 0;
                        //}

                        if (pr.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || pr.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            pr.ColorDisplay = "";
                        }
                        else
                        {
                            pr.ColorDisplay = "red";
                        }
                        ListPupilRankingBO.Add(pr);
                    }
                }
            }
            #endregion

            #region Cấp II, Cấp III
            //2.	Nếu UserInfo.AppliedLevel = APPLIED_LEVEL_SECONDARY hoặc  = APPLIED_LEVEL_TERTIARY (Cấp 2 hoặc 3)
            if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                /* Gọi hàm PupilRankingBusiness.GetPupilToRank (Dictionary) với
                    + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                    + Dictionary[“ClassID”]: rptClass.Value
                    + Dictionary[“SchoolID”]: UserInfo.SchoolID
                    + Dictionary[“Semester”] = rptSemester.Value
                    ð	lstPupilToRank Lấy kết quả thu được đưa lên Grid */
                IDictionary<string, object> DicPupilRanking = new Dictionary<string, object>();
                DicPupilRanking["AcademicYearID"] = global.AcademicYearID;
                DicPupilRanking["ClassID"] = classid;
                DicPupilRanking["SchoolID"] = global.SchoolID;
                DicPupilRanking["Semester"] = semesterid;

                //var lstPupilToRank = PupilRankingBusiness.GetPupilToRank(DicPupilRanking).ToList();
                //var lstPupilToRank = PupilRankingBusiness.GetPupilToRank(DicPupilRanking).ToList();
                List<PupilRankingBO> lstPupilToRank = PupilRankingBusiness.GetPupilToRank(DicPupilRanking, false).ToList();
                /*Kiểm tra trong lstPupilToRank. Nếu tồn tại lstPupilToRank[i].Status = 1 và
                 * lstPupilToRank[i].CapacityLevelID = null hoặc lstPupilToRank[i].ConductLevelID = null 
                 * thì thông báo Tồn tại học sinh chưa có Học lực/Hạnh kiểm. Disable button Lưu*/
                List<CapacityLevel> lstCapacityLevel = CapacityLevelBusiness.All.Where(o => o.IsActive == true).ToList();
                List<ConductLevel> lstConductLevel = ConductLevelBusiness.All.Where(o => o.IsActive == true).ToList();
                if (lstPupilToRank != null && lstPupilToRank.Count > 0)
                {
                    DateTime? MoveDate = DateTime.Now;
                    var lstPupilRankingStudying = lstPupilToRank.Where(o => o.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();
                    if (!lstPupilRankingStudying.Any(o => o.CapacityLevelID.HasValue && (o.ConductLevelID.HasValue || o.IsKXLHK)) && UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, classid))
                    {
                        ViewData[PupilRankingConstants.DISABLESAVEBTN] = true;
                        ViewData[PupilRankingConstants.MESSAGE_ERROR] = Res.Get("PupilRanking_Label_ExitsPupilNotCapacity");
                    }
                    if (UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, classid) == false && global.IsViewAll)
                    {
                        ViewData[PupilRankingConstants.DISABLESAVEBTN] = true;
                    }
                    /*
                     Những học sinh có Status = PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL hoặc = PUPIL_STATUS_LEAVED_OFF hoặc = PUPIL_STATUS_MOVED_TO_OTHER_CLASS thì disable
                     */

                    foreach (var pr in lstPupilToRank)
                    {
                        pr.Enable = pr.ShowMark;

                        //if (pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL ||
                        //    pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_LEAVED_OFF ||
                        //    pr.ProfileStatus == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                        //{
                        //    //thời gian chuyển so với đợt hiện tại
                        //    if (pr.ProfileStatus == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                        //    {
                        //        MoveDate = pr.ClassMoveDate;
                        //    }
                        //    else if (pr.ProfileStatus == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                        //    {
                        //        MoveDate = pr.SchoolMoveDate;
                        //    }
                        //    else if (pr.ProfileStatus == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_LEAVED_OFF)
                        //    {
                        //        MoveDate = pr.LeavingDate;
                        //    }
                        //    if (MoveDate > EndDateSemester)
                        //    {
                        //        pr.Enable = true;
                        //    }
                        //    else
                        //    {
                        //        pr.Enable = false;
                        //    }
                        //}
                        if (pr.Genre.HasValue && pr.Genre.Value == GlobalConstants.GENRE_MALE)
                        {
                            pr.GenreDetail = GlobalConstants.GENRE_MALE_TITLE;
                        }
                        if (pr.Genre.HasValue && pr.Genre.Value == GlobalConstants.GENRE_FEMALE)
                        {
                            pr.GenreDetail = GlobalConstants.GENRE_FEMALE_TITLE;
                        }
                        pr.ToTalAbsentDay = Convert.ToInt16(pr.TotalAbsentDaysWithoutPermission + pr.TotalAbsentDaysWithPermission);
                        var conductlevel = lstConductLevel.Where(o => o.ConductLevelID == pr.ConductLevelID).FirstOrDefault();
                        pr.ConductLevelName = conductlevel != null ? conductlevel.Resolution : string.Empty;
                        var honourachivementtype = lstHonourAchievementType.Where(o => o.HonourAchivementTypeID == pr.HonourAchivementTypeID).FirstOrDefault();
                        pr.HonourAchivementTypeResolution = honourachivementtype != null ? honourachivementtype.Resolution : string.Empty;
                        var studyingjudgement = lstStudyingJudgement.Where(o => o.StudyingJudgementID == pr.StudyingJudgementID).FirstOrDefault();

                        //if(studyingjudgement!=null)
                        //{
                        //    studyJudgementID = studyingjudgement.StudyingJudgementID;
                        //}

                        //int appliedLevel = EducationLevelBusiness.Find(pr.EducationLevelID).Grade;
                        //DungVA - Thuộc diện
                        if (studyingjudgement != null)
                        {
                            if (studyingjudgement.StudyingJudgementID == 5)
                            {
                                if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {
                                    pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation1");
                                }
                                else if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                                {
                                    pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation2");
                                }
                                else if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                                {
                                    pr.StudyingJudgementResolution = Res.Get("PupilRanking_Label_Graduation3");
                                }

                            }
                            else
                            {
                                pr.StudyingJudgementResolution = studyingjudgement.Resolution;
                            }
                        }
                        else
                        {
                            pr.StudyingJudgementResolution = string.Empty;
                        }


                        var capacitylevel = lstCapacityLevel.Where(o => o.CapacityLevelID == pr.CapacityLevelID).FirstOrDefault();
                        pr.CapacityLevel = capacitylevel != null ? capacitylevel.Description : string.Empty;
                        if (pr.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || pr.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            pr.ColorDisplay = "";
                        }
                        else
                        {
                            pr.ColorDisplay = "red";
                        }
                        ListPupilRankingBO.Add(pr);
                    }
                }
            }
            #endregion

            ViewData[PupilRankingConstants.LIST_PUPILRANKING] = ListPupilRankingBO.OrderBy(o => o.OrderInClass).ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name)).ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName)).ToList();
            if (ListPupilRankingBO.Count() == 0)
            {
                ViewData[PupilRankingConstants.DISABLESAVEBTN] = true;
            }
            ViewData["ClassID"] = classid;
            ViewData["Semester"] = string.IsNullOrWhiteSpace(r["semesterid"]) ? 0 : int.Parse(r["semesterid"]);
            ViewData["AppliedLevel"] = global.AppliedLevel;
            var classprofile = ClassProfileBusiness.Find(classid);
            ViewData["TitleSearch"] = string.Format("{0} - Học kỳ {1} - Lớp {2}", Res.Get("PupilRanking_Label_TitleSearch"), semesterid, classprofile.DisplayName);
            var note_format = "<table class='tblNote'><tr><td><span class='spannote'>{0}</span></td><td>- {1} <br/>- {2}</td></tr></table>";
            ViewData["TitleNote"] = string.Format(note_format, Res.Get("PupilRanking_Label_Note"), Res.Get("PupilRanking_Lable_NoteOne"), Res.Get("PupilRanking_Lable_NoteTwo"));
            //int.Parse(ViewData["Semester"].ToString()) == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_ALL

            if ((semesterid == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_ALL && (global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY))
                || (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY && semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND))
            {
                ViewData["EnableStudyingJudgement"] = true;
            }
            else
            {
                ViewData["EnableStudyingJudgement"] = false;
            }
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_ADD)]
        public JsonResult Create(FormCollection col)
        {
            int semester = string.IsNullOrWhiteSpace(col["txtsemester"]) ? 0 : int.Parse(col["txtsemester"]);
            /*1.	Nếu UserInfo.AppliedLevel = APPLIED_LEVEL_PRIMARY(Cấp 1)
                Gọi hàm PupilRankingBusiness.ClassifyPupilPrimary(lstPupilToRank) để tính danh hiệu và thuộc diện cho học sinh.
                Hiển thị kết quả trên Grid.
            */
            var global = new GlobalInfo();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(global.AcademicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);

            int classID = string.IsNullOrWhiteSpace(col["txtclassid"]) ? 0 : int.Parse(col["txtclassid"]);
            IDictionary<string, object> DicPupilRanking = new Dictionary<string, object>();
            DicPupilRanking["AcademicYearID"] = global.AcademicYearID;
            DicPupilRanking["ClassID"] = classID;
            DicPupilRanking["SchoolID"] = global.SchoolID;
            DicPupilRanking["Semester"] = semester;
            List<PupilRankingBO> lstPupilRankingBO = new List<PupilRankingBO>();

            SummedUpRecordClass Surc = new SummedUpRecordClass();
            Surc.SchoolID = global.SchoolID.Value;
            Surc.ClassID = classID;
            Surc.EducationLevelID = ClassProfileBusiness.Find(classID).EducationLevelID;
            Surc.AcademicYearID = global.AcademicYearID.Value;
            Surc.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING;
            Surc.CreatedDate = DateTime.Now;
            Surc.ModifiedDate = DateTime.Now;
            Surc.NumberOfPupil = 0;
            Surc.Semester = semester;
            Surc.PeriodID = null;
            Surc.Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_2;


            if (SummedUpRecordClassBusiness.CheckExistsSummedExcuting(Surc))
            {
                return Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_Summeding"), JsonMessage.ERROR));
            }
            //Danh dau lop dang thuc hien tinh tong ket, xep loai
            SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(Surc);

            try
            {
                if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {

                    lstPupilRankingBO = PupilRankingBusiness.GetPupilToRankPrimaryv2(DicPupilRanking);

                    lstPupilRankingBO = lstPupilRankingBO.Where(o => o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
                    PupilRankingBusiness.ClassifyPupilPrimaryv2(lstPupilRankingBO, semester);
                }
                /*
                 2.	Nếu UserInfo.AppliedLevel = APPLIED_LEVEL_SECONDARY hoặc  = APPLIED_LEVEL_TERTIARY (Cấp 2 hoặc 3)
                    Gọi hàm PupilRankingBusiness.ClassifyPupil(lstPupilToRank, rptSemeser.Value) để tính danh hiệu và thuộc diện cho học sinh.
                    Hiển thị kết quả trên Grid.

                 */
                if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {


                    lstPupilRankingBO = PupilRankingBusiness.GetPupilToRank(DicPupilRanking, false).ToList();
                    lstPupilRankingBO = lstPupilRankingBO.Where(o => (o.ConductLevelID.HasValue || o.IsKXLHK) && o.CapacityLevelID.HasValue && o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED).ToList();
                    if (isMovedHistory)
                    {
                        PupilRankingHistoryBusiness.ClassifyPupilHistory(lstPupilRankingBO, semester, false, Business.Common.GlobalConstants.NOT_AUTO_MARK);
                    }
                    else
                    {
                        PupilRankingBusiness.ClassifyPupil(lstPupilRankingBO, semester, false, Business.Common.GlobalConstants.NOT_AUTO_MARK);
                    }

                }
            }
            finally
            {
                Surc.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                Surc.NumberOfPupil = lstPupilRankingBO.Count;
                SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(Surc);
            }

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));

        }
    }
}





