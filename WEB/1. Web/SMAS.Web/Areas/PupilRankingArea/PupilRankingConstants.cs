/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilRankingArea
{
    public class PupilRankingConstants
    {
        public const string LIST_PUPILRANKING = "listPupilRanking";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_PERIOD = "listPeriod";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_SUMMEDUPRECORDOFCLASS = "listSummedUpRecordOfClass";
        public const string LIST_LOCKEDMARKDETAIL = "listLockedMarkDetail";
        public const string LIST_JUDGERECORDOFCLASS = "listJudgeRecordOfClass";
        public const string LIST_PERIODDECLARATION = "listPeriodDeclaration";
        public const string MESSAGE_ERROR = "messageerror";
        public const string DISABLESAVEBTN = "DisableSaveBtn";
        public const string SEMESTER_END_DATE = "semester_end_date";
        public const string TRANSFER_DATE = "transfer_date";
    }
}