﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilRankingArea
{
    public class PupilRankingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilRankingArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilRankingArea_default",
                "PupilRankingArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
