﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.GeneralEvaluationDevelopmentArea
{
    public class GeneralEvaluationDevelopmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "GeneralEvaluationDevelopmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "GeneralEvaluationDevelopmentArea_default",
                "GeneralEvaluationDevelopmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
