﻿//author: minhh

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.GeneralEvaluationDevelopmentArea.Models;
using SMAS.Web.Utils;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.GeneralEvaluationDevelopmentArea.Controllers
{
    public class GeneralEvaluationDevelopmentController : BaseController
    {

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IDevelopmentOfChildrenReportBusiness DevelopmentOfChildrenReportBusiness;

        public GeneralEvaluationDevelopmentController(IClassProfileBusiness classprofileBusiness, IDevelopmentOfChildrenReportBusiness DevelopmentOfChildrenReportBusiness,
            IReportDefinitionBusiness reportdefinitionBusiness,
            IProcessedReportBusiness processedreportBusiness
                                             
                                              )
        {

            this.ClassProfileBusiness = classprofileBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
            this.ProcessedReportBusiness = processedreportBusiness;
            this.DevelopmentOfChildrenReportBusiness = DevelopmentOfChildrenReportBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        private void SetViewData()
        {
            // load combobox EducationLevel
            List<EducationLevel> ListEducationLevel = new GlobalInfo().EducationLevels;
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }

            ViewData[GeneralEvaluationDevelopmentConstants.LIST_EDUCATION_LEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution");

            List<ClassProfile> lstClass = new List<ClassProfile>();
            ViewData[GeneralEvaluationDevelopmentConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            // load combobox Semester
            ViewData[GeneralEvaluationDevelopmentConstants.LIST_SEMESTER] = ViewData[GeneralEvaluationDevelopmentConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
            List<ViettelCheckboxList> lstReporteType;
            if (GlobalInfo.getInstance().AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN) // Mau giao
            {
                lstReporteType = new List<ViettelCheckboxList>() 
                {
                    new ViettelCheckboxList{ Label= @Res.Get("GeneralEvaluationDevelopment_Label_GeneralEvaluation"), Value=1, cchecked=true, disabled= false},
                    new ViettelCheckboxList{Label=@Res.Get("GeneralEvaluationDevelopment_Label_CollectEvaluation"), Value=3, cchecked=false, disabled= false}
                };
            }
            else // Nha tre
            {
                lstReporteType = new List<ViettelCheckboxList>() 
                {
                    new ViettelCheckboxList{ Label= @Res.Get("GeneralEvaluationDevelopment_Label_ResultEvaluation"), Value=2, cchecked=true, disabled= false}
                };
            }
            ViewData[GeneralEvaluationDevelopmentConstants.LIST_TYPESTATICS] = lstReporteType;
        }

        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass2(int? EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;

            IEnumerable<ClassProfile> lstClass = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            }
            if (lstClass == null) lstClass = new List<ClassProfile>();

            ViewData[GeneralEvaluationDevelopmentConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ViewModel form)
        {
            GlobalInfo gl = new GlobalInfo();
            if (form.EducationLevelID == 0)
            {
                throw new BusinessException(Res.Get("GeneralEvaluationDevelopment_Validate_EducationLevelID"));
            }
            ExcelDevelopmentOfChildren ExcelDevelopmentOfChildren = new ExcelDevelopmentOfChildren();
            if (gl.AcademicYearID.HasValue) ExcelDevelopmentOfChildren.AcademicYearID = gl.AcademicYearID.Value;

            ExcelDevelopmentOfChildren.ClassID = form.ClassID.HasValue ? form.ClassID.Value : 0;
            ExcelDevelopmentOfChildren.EducationLevelID = form.EducationLevelID;
            ExcelDevelopmentOfChildren.Semester = form.Semester;
            ExcelDevelopmentOfChildren.ReportType = form.ReportType;

            string reportCode = "";

            if (form.ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DANH_GIA_SU_PHAT_TRIEN;
            }
            else if (form.ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_KET_QUA_DANH_GIA_TRE;
            }
            else if (form.ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_GENERALEVALUATIONDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_TONG_HOP_KET_QUA_DANH_GIA;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = DevelopmentOfChildrenReportBusiness.GetDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren, form.ReportType);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = DevelopmentOfChildrenReportBusiness.CreateDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren, gl.SchoolID.Value, gl.AppliedLevel.Value, form.ReportType);
                processedReport = DevelopmentOfChildrenReportBusiness.InsertDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ViewModel form)
        {
            GlobalInfo gl = GlobalInfo.getInstance();

            ExcelDevelopmentOfChildren ExcelDevelopmentOfChildren = new ExcelDevelopmentOfChildren();
            if (gl.AcademicYearID.HasValue) ExcelDevelopmentOfChildren.AcademicYearID = gl.AcademicYearID.Value;

            ExcelDevelopmentOfChildren.ClassID = form.ClassID.HasValue ? form.ClassID.Value : 0;
            ExcelDevelopmentOfChildren.EducationLevelID = form.EducationLevelID;
            ExcelDevelopmentOfChildren.Semester = form.Semester;
            ExcelDevelopmentOfChildren.ReportType = form.ReportType;

            Stream excel = DevelopmentOfChildrenReportBusiness.CreateDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren, gl.SchoolID.Value, gl.AppliedLevel.Value, form.ReportType);
            ProcessedReport processedReport = this.DevelopmentOfChildrenReportBusiness.InsertDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}
