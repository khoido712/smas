﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.GeneralEvaluationDevelopmentArea.Models
{
    public class ViewModel
    {
        [ResourceDisplayName("TranscriptsBySemester_Label_EducationLevel_MN")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "CHOICE")]
        [AdditionalMetadata("ViewDataKey", GeneralEvaluationDevelopmentConstants.LIST_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int EducationLevelID { get; set; }


        [ResourceDisplayName("Mamnon_Label_Class_MN")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", GeneralEvaluationDevelopmentConstants.LIST_CLASS)]
        public int? ClassID { get; set; }

        [ResourceDisplayName("Mamnon_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", GeneralEvaluationDevelopmentConstants.LIST_SEMESTER)]
        public int Semester { get; set; }

        [ScaffoldColumn(false)]
        public int ReportType { get; set; }

    }
}
