﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GeneralEvaluationDevelopmentArea.Models
{
    public class SearchViewModel
    {
        public int ReportType { get; set; }
        public int EducationLevelID { get; set; }
        public int? Semester { get; set; }
        public int? ClassID { get; set; }
    }
}