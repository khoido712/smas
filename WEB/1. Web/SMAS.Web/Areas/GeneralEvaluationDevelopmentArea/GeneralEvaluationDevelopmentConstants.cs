﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GeneralEvaluationDevelopmentArea
{
    public class GeneralEvaluationDevelopmentConstants
    {
        
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATION_LEVEL = "listEdu";
        public const string LIST_CLASS = "listClass";
        public const  string LIST_TYPESTATICS = "listType";
       
    }
}