﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Web.Areas.ExaminationsArea.Models;
using SMAS.Web.Areas.EmployeeArea.Models;
using System.Transactions;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Filter;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.ExaminationsArea.Controllers
{
    public class ExaminationsController : BaseController
    {
        private readonly IExaminationsBusiness ExaminationsBusiness;

        public ExaminationsController(IExaminationsBusiness ExaminationsBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            SetViewInsert();
            return View();
        }

        #region SetViewData

        public void SetViewData()
        {
            SetViewDataPermission("Examinations", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            ViewData[ExaminationsContants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExaminationsContants.CURRENT_SEMESTER] = _globalInfo.Semester.Value;
            ViewData[SystemParamsInFile.PERMISSION] = GetMenupermission("Examinations", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
        }

        public void SetViewInsert()
        {
            // Thong tin ki thi
            Examinations exam = new Examinations();
            exam.ExaminationsID = 0;
            exam.ExaminationsName = string.Empty;
            exam.SemesterID = _globalInfo.Semester.Value;
            exam.DataInheritance = false;
            exam.InheritanceExam = 0;
            exam.InheritanceData = string.Empty;
            exam.MarkInputType = 0;
            exam.MarkInput = false;
            exam.MarkClosing = false;
            ViewData[ExaminationsContants.EXAMINATIONS_OBJECT] = exam;

            // Ki thi ke thua
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            search["SchoolID"] = _globalInfo.SchoolID.Value;
            search["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            IEnumerable<ExaminationsViewModel> list = this._Search(search);
            ViewData[ExaminationsContants.LIST_EXAMINATIONS] = list;
            ViewData[ExaminationsContants.LIST_EXAMINATIONS_INHERITANCE] = new SelectList(list, "ExaminationsID", "ExaminationsName");

            // List hoc ki
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("Common_Label_Combo_FirstSemester"), ExaminationsContants.SEMESTER_I, false, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("Common_Label_Combo_SecondSemester"), ExaminationsContants.SEMESTER_II, false, false));
            ViewData[ExaminationsContants.LIST_RADIO_SEMESTER] = listSemester;

            // List du lieu ke thua
            List<ViettelCheckboxList> listInheritanceData = new List<ViettelCheckboxList>();
            listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_ExamGroup"), ExaminationsContants.INHERITANCE_DATA_EXAM_GROUP, false, false));
            listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_ExamPupil"), ExaminationsContants.INHERITANCE_DATA_EXAM_PUPIL, false, false));
            listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_Supervisory"), ExaminationsContants.INHERITANCE_DATA_SUPERVISORY, false, false));
            ViewData[ExaminationsContants.LIST_INHERITANCE] = listInheritanceData;

            // List kieu nhap diem thi
            List<ViettelCheckboxList> listMarkInputType = new List<ViettelCheckboxList>();
            listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_OrderNumber"), ExaminationsContants.MARK_INPUT_TYPE_ORDER_NUMBER, false, false));
            listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_ExamRoom"), ExaminationsContants.MARK_INPUT_TYPE_EXAM_ROOM, false, false));
            listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_PupilCode"), ExaminationsContants.MARK_INPUT_TYPE_PUPIL_CODE, false, false));
            listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_DatachableHead"), ExaminationsContants.MARK_INPUT_TYPE_DATACHABLE_HEAD, false, false));
            ViewData[ExaminationsContants.LIST_INPUT_TYPE] = listMarkInputType;
        }

        public void SetViewUpdate(long ExaminationsID)
        {
            // Thong tin ki thi
            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            ViewData[ExaminationsContants.EXAMINATIONS_OBJECT] = exam;

            // List ki thi ke thua
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            search["SchoolID"] = _globalInfo.SchoolID.Value;
            search["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            IEnumerable<ExaminationsViewModel> list = this._Search(search);
            ViewData[ExaminationsContants.LIST_EXAMINATIONS] = list;

            // Kỳ thi kế thừa
            search["ExaminationsID"] = ExaminationsID;
            search["IsUpdate"] = 1;
            List<Examinations> listExamInheritance = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            if (exam.DataInheritance == true && exam.InheritanceExam.HasValue)
            {
                ViewData[ExaminationsContants.LIST_EXAMINATIONS_INHERITANCE] = new SelectList(listExamInheritance, "ExaminationsID", "ExaminationsName", exam.InheritanceExam.Value);
            }
            else
            {
                ViewData[ExaminationsContants.LIST_EXAMINATIONS_INHERITANCE] = new SelectList(listExamInheritance, "ExaminationsID", "ExaminationsName");
            }

            // List hoc ki
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            if (exam.SemesterID == ExaminationsContants.SEMESTER_I)
            {
                listSemester.Add(new ViettelCheckboxList(Res.Get("Common_Label_Combo_FirstSemester"), ExaminationsContants.SEMESTER_I, true, false));
                listSemester.Add(new ViettelCheckboxList(Res.Get("Common_Label_Combo_SecondSemester"), ExaminationsContants.SEMESTER_II, false, false));
            }
            else
            {
                listSemester.Add(new ViettelCheckboxList(Res.Get("Common_Label_Combo_FirstSemester"), ExaminationsContants.SEMESTER_I, false, false));
                listSemester.Add(new ViettelCheckboxList(Res.Get("Common_Label_Combo_SecondSemester"), ExaminationsContants.SEMESTER_II, true, false));
            }
            ViewData[ExaminationsContants.LIST_RADIO_SEMESTER] = listSemester;

            // List du lieu ke thua
            List<ViettelCheckboxList> listInheritanceData = new List<ViettelCheckboxList>();
            if (exam.DataInheritance == true && exam.InheritanceExam.HasValue)
            {
                // Nếu có kế thừa thì mặc định kế thừa đầu tiên luôn có
                listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_ExamGroup"), ExaminationsContants.INHERITANCE_DATA_EXAM_GROUP, true, true));

                if (exam.InheritanceData != null)
                {
                    // Kế thừa thí sinh
                    if (exam.InheritanceData.Contains(ExaminationsContants.INHERITANCE_DATA_EXAM_PUPIL.ToString()))
                    {
                        listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_ExamPupil"), ExaminationsContants.INHERITANCE_DATA_EXAM_PUPIL, true, true));
                    }
                    else
                    {
                        listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_ExamPupil"), ExaminationsContants.INHERITANCE_DATA_EXAM_PUPIL, false, false));
                    }
                    // Kế thừa giám thị
                    if (exam.InheritanceData.Contains(ExaminationsContants.INHERITANCE_DATA_SUPERVISORY.ToString()))
                    {
                        listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_Supervisory"), ExaminationsContants.INHERITANCE_DATA_SUPERVISORY, true, true));
                    }
                    else
                    {
                        listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_Supervisory"), ExaminationsContants.INHERITANCE_DATA_SUPERVISORY, false, false));
                    }
                }
                else
                {
                    listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_ExamPupil"), ExaminationsContants.INHERITANCE_DATA_EXAM_PUPIL, false, false));
                    listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_Supervisory"), ExaminationsContants.INHERITANCE_DATA_SUPERVISORY, false, false));
                }
            }
            else
            {
                listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_ExamGroup"), ExaminationsContants.INHERITANCE_DATA_EXAM_GROUP, false, false));
                listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_ExamPupil"), ExaminationsContants.INHERITANCE_DATA_EXAM_PUPIL, false, false));
                listInheritanceData.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Check_Supervisory"), ExaminationsContants.INHERITANCE_DATA_SUPERVISORY, false, false));
            }
            ViewData[ExaminationsContants.LIST_INHERITANCE] = listInheritanceData;

            // List kieu nhap diem thi
            List<ViettelCheckboxList> listMarkInputType = new List<ViettelCheckboxList>();
            if (exam.MarkInputType == ExaminationsContants.MARK_INPUT_TYPE_ORDER_NUMBER)
            {
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_OrderNumber"), ExaminationsContants.MARK_INPUT_TYPE_ORDER_NUMBER, true, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_ExamRoom"), ExaminationsContants.MARK_INPUT_TYPE_EXAM_ROOM, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_PupilCode"), ExaminationsContants.MARK_INPUT_TYPE_PUPIL_CODE, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_DatachableHead"), ExaminationsContants.MARK_INPUT_TYPE_DATACHABLE_HEAD, false, false));
            }
            else if (exam.MarkInputType == ExaminationsContants.MARK_INPUT_TYPE_EXAM_ROOM)
            {
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_OrderNumber"), ExaminationsContants.MARK_INPUT_TYPE_ORDER_NUMBER, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_ExamRoom"), ExaminationsContants.MARK_INPUT_TYPE_EXAM_ROOM, true, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_PupilCode"), ExaminationsContants.MARK_INPUT_TYPE_PUPIL_CODE, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_DatachableHead"), ExaminationsContants.MARK_INPUT_TYPE_DATACHABLE_HEAD, false, false));
            }
            else if (exam.MarkInputType == ExaminationsContants.MARK_INPUT_TYPE_PUPIL_CODE)
            {
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_OrderNumber"), ExaminationsContants.MARK_INPUT_TYPE_ORDER_NUMBER, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_ExamRoom"), ExaminationsContants.MARK_INPUT_TYPE_EXAM_ROOM, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_PupilCode"), ExaminationsContants.MARK_INPUT_TYPE_PUPIL_CODE, true, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_DatachableHead"), ExaminationsContants.MARK_INPUT_TYPE_DATACHABLE_HEAD, false, false));
            }
            else if (exam.MarkInputType == ExaminationsContants.MARK_INPUT_TYPE_DATACHABLE_HEAD)
            {
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_OrderNumber"), ExaminationsContants.MARK_INPUT_TYPE_ORDER_NUMBER, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_ExamRoom"), ExaminationsContants.MARK_INPUT_TYPE_EXAM_ROOM, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_PupilCode"), ExaminationsContants.MARK_INPUT_TYPE_PUPIL_CODE, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_DatachableHead"), ExaminationsContants.MARK_INPUT_TYPE_DATACHABLE_HEAD, true, false));
            }
            else
            {
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_OrderNumber"), ExaminationsContants.MARK_INPUT_TYPE_ORDER_NUMBER, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_ExamRoom"), ExaminationsContants.MARK_INPUT_TYPE_EXAM_ROOM, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_PupilCode"), ExaminationsContants.MARK_INPUT_TYPE_PUPIL_CODE, false, false));
                listMarkInputType.Add(new ViettelCheckboxList(Res.Get("Examinations_Label_Radio_DatachableHead"), ExaminationsContants.MARK_INPUT_TYPE_DATACHABLE_HEAD, false, false));
            }

            ViewData[ExaminationsContants.LIST_INPUT_TYPE] = listMarkInputType;
        }

        #endregion SetViewData

        #region Event

        
        public PartialViewResult Search()
        {
            ViewData[ExaminationsContants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;

            IDictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            search["SchoolID"] = _globalInfo.SchoolID.Value;
            search["AppliedLevel"] = _globalInfo.AppliedLevel.Value;

            IEnumerable<ExaminationsViewModel> list = this._Search(search);
            ViewData[ExaminationsContants.LIST_EXAMINATIONS] = list;

            return PartialView("_List");
        }

        private IEnumerable<ExaminationsViewModel> _Search(IDictionary<string, object> search)
        {
            SetViewDataPermission("Examinations", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<ExaminationsViewModel> list = ExaminationsBusiness.GetListExaminations(search).Select(o => new ExaminationsViewModel
            {
                ExaminationsID = o.ExaminationsID,
                ExaminationsName = o.ExaminationsName,
                Semester = o.Semester,
                DataInheritance = o.DataInheritance,
                InheritanceExam = o.InheritanceExam,
                InheritanceData = o.InheritanceData,
                MarkInputType = o.MarkInputType,
                MarkInput = o.MarkInput,
                MarkClosing = o.MarkClosing,
                CreateTime = o.CreateTime,
                IsDelete = o.IsDelete

            }).OrderByDescending(o => o.CreateTime).ToList();

            return list;
        }

        private Examinations GetDataFromView(FormCollection form)
        {
            Examinations exam = new Examinations();
            exam.AcademicYearID = _globalInfo.AcademicYearID.Value;
            exam.SchoolID = _globalInfo.SchoolID.Value;
            exam.SemesterID = _globalInfo.Semester.Value;
            exam.AppliedLevel = _globalInfo.AppliedLevel.Value;

            exam.ExaminationsID = Request["ExaminationsIDHidden"] != null ? long.Parse(Request["ExaminationsIDHidden"]) : 0;
            exam.ExaminationsName = Request["ExaminationsName"] != null ? Request["ExaminationsName"].ToString() : string.Empty;
            exam.SemesterID = Request["Semester"] != null ? int.Parse(Request["Semester"]) : 0;
            exam.DataInheritance = Request["DataInheritance"] != null ? true : false;
            exam.InheritanceExam = Request["InheritanceExam"] != null && exam.DataInheritance == true ? long.Parse(Request["InheritanceExam"]) : 0;
            // Nếu có kế thừa thì mặc định có kế thừa nhóm, môn và phòng (1)
            if (exam.DataInheritance == true)
            {
                exam.InheritanceData = Request["InheritanceData"] != null ? "1," + Request["InheritanceData"].ToString() : "1";
            }
            else
            {
                exam.InheritanceData = string.Empty;
            }
            exam.MarkInputType = Request["MarkInputType"] != null ? int.Parse(Request["MarkInputType"]) : 0;
            exam.MarkInput = Request["MarkInput"] != null ? true : false;
            exam.MarkClosing = Request["MarkClosing"] != null ? true : false;

            return exam;
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Create(FormCollection form)
        {
            if (GetMenupermission("Examinations", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            Examinations exam = GetDataFromView(form);
            exam.CreateTime = DateTime.Now;
            ExaminationsBusiness.InsertExaminations(exam);
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Edit(FormCollection form, string DataInheritanceHidden, long? InheritanceExamHidden, string InheritanceDataHidden)
        {
            if (GetMenupermission("Examinations", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            long examinationsID = Request["ExaminationsIDHidden"] != null ? long.Parse(Request["ExaminationsIDHidden"]) : 0;
            Examinations exam = ExaminationsBusiness.Find(examinationsID);

            // Thong tin cua ky thi
            exam.ExaminationsName = Request["ExaminationsName"] != null ? Request["ExaminationsName"].ToString() : string.Empty;
            exam.SemesterID = Request["Semester"] != null ? int.Parse(Request["Semester"]) : 0;

            // Chuan hoa thong tin ke thua cu
            bool dataInheritance = DataInheritanceHidden != null && DataInheritanceHidden == "True" ? true : false;
            InheritanceExamHidden = InheritanceExamHidden.HasValue ? InheritanceExamHidden.Value : 0;
            InheritanceDataHidden = InheritanceDataHidden != null ? InheritanceDataHidden : string.Empty;

            // Lay thong tin tu form neu ky thi chua ke thua
            if (DataInheritanceHidden != "True")
            {
                exam.DataInheritance = Request["DataInheritance"] != null ? true : false;
                exam.InheritanceExam = Request["InheritanceExam"] != null && exam.DataInheritance == true ? long.Parse(Request["InheritanceExam"]) : 0;
            }

            // Lay thong tin ke thua moi, neu co
            if (exam.DataInheritance == true)
            {
                if (dataInheritance == true)
                {
                    exam.InheritanceData = Request["InheritanceData"] != null ? Request["InheritanceData"].ToString() : string.Empty;
                }
                else
                {
                    exam.InheritanceData = Request["InheritanceData"] != null ? "1," + Request["InheritanceData"].ToString() : "1";
                }
            }
            else
            {
                exam.InheritanceData = string.Empty;
            }
            
            // Cac thong tin con lai cua ky thi
            exam.MarkInputType = Request["MarkInputType"] != null ? int.Parse(Request["MarkInputType"]) : 0;
            exam.MarkInput = Request["MarkInput"] != null ? true : false;
            exam.MarkClosing = Request["MarkClosing"] != null ? true : false;
            exam.UpdateTime = DateTime.Now;

            ExaminationsBusiness.UpdateExaminations(exam, dataInheritance, InheritanceExamHidden.Value, InheritanceDataHidden);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        public PartialViewResult GetEdit(long ExaminationsID)
        {
            SetViewUpdate(ExaminationsID);
            return PartialView("_Edit");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int ExaminationsID)
        {
            if (GetMenupermission("Examinations", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ExaminationsBusiness.DeleteExaminations(ExaminationsID, _globalInfo.SchoolID.Value);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion Event
    }
}
