﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExaminationsArea
{
    public class ExaminationsContants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";

        public const string EXAMINATIONS_OBJECT = "ExaminationsObject";
        public const string LIST_EXAMINATIONS = "ListExamination";
        public const string LIST_RADIO_SEMESTER = "ListRadioSemester";
        public const string LIST_INHERITANCE = "ListInheritance";
        public const string LIST_INPUT_TYPE = "ListInputType";
        public const string LIST_EXAMINATIONS_INHERITANCE = "ListExaminationsInheritance";

        public const int SEMESTER_I = 1;
        public const int SEMESTER_II = 2;

        public const int MARK_INPUT_TYPE_ORDER_NUMBER = 1;
        public const int MARK_INPUT_TYPE_EXAM_ROOM = 2;
        public const int MARK_INPUT_TYPE_PUPIL_CODE = 3;
        public const int MARK_INPUT_TYPE_DATACHABLE_HEAD = 4;

        public const int INHERITANCE_DATA_EXAM_GROUP = 1;
        public const int INHERITANCE_DATA_EXAM_PUPIL = 2;
        public const int INHERITANCE_DATA_SUPERVISORY = 3;
    }
}