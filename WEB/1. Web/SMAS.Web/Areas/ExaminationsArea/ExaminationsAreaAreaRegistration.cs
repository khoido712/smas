﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExaminationsArea
{
    public class ExaminationsAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExaminationsArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExaminationsArea_default",
                "ExaminationsArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
