﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExaminationsArea.Models
{
    public class ExaminationsViewModel
    {
        public long ExaminationsID { get; set; }

        [ResourceDisplayName("Examinations_Label_ExaminationsName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExaminationsName_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string ExaminationsName { get; set; }

        public int? Semester { get; set; }

        public bool? DataInheritance { get; set; }

        /*[ResDisplayName("Examinations_Label_ExaminationsInheritance")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExaminationsContants.LIST_EXAMINATIONS_INHERITANCE)]
        [AdditionalMetadata("Placeholder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Examinations_Error_InheritanceExam")]*/
        public long? InheritanceExam { get; set; }

        public string InheritanceData { get; set; }

        public int? MarkInputType { get; set; }

        [ResourceDisplayName("Examinations_Label_MarkInput")]
        public bool? MarkInput { get; set; }

        [ResourceDisplayName("Examinations_Label_MarkClosing")]
        public bool? MarkClosing { get; set; }

        public DateTime CreateTime { get; set; }

        public bool IsDelete { get; set; }
    }
}