﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportTeachingAssignmentArea
{
    public class ReportTeachingAssignmentConstants
    {
        public const string LS_SEMESTER = "lstSemester";
        public const string DF_SEMESTER = "defaultSemester";
        public const string LS_EDUCATION_LEVEL = "lstEducationLevel";
        public const string LS_CLASS = "lstClass";
    }
}
