﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.ReportTeachingAssignmentArea.Controllers
{
    public class ReportTeachingAssignmentController : BaseController
    {


        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportTeachingAssignmentBusiness ReportTeachingAssignmentBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        public ReportTeachingAssignmentController(IClassProfileBusiness classProfileBusiness,
            IEducationLevelBusiness educationLevelBusiness,
            IReportTeachingAssignmentBusiness reportTeachingAssignmentBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness)
        {
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ReportTeachingAssignmentBusiness = reportTeachingAssignmentBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
        }


        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();

            //Danh sách khối học
            ViewData[ReportTeachingAssignmentConstants.LS_EDUCATION_LEVEL] = Global.EducationLevels;

            //Danh sách lớp học
           
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = Global.EducationLevels.FirstOrDefault().EducationLevelID;
            dicClass["AcademicYearID"] = Global.AcademicYearID;
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dicClass);
            if (lsClass.Count() != 0)
            {
                List<ClassProfile> lstClass = lsClass.ToList();
                ViewData[ReportTeachingAssignmentConstants.LS_CLASS] = lstClass;
            }
            else
            {
                ViewData[ReportTeachingAssignmentConstants.LS_CLASS] = new List<ClassProfile>();
            }
            //Danh sách học kỳ
            ViewData[ReportTeachingAssignmentConstants.LS_SEMESTER] = CommonList.Semester();
            int defaultSemester = (Global.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : Global.Semester.GetValueOrDefault();
            ViewData[ReportTeachingAssignmentConstants.DF_SEMESTER] = defaultSemester;

            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? idEducationLevel)
        {
           
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = idEducationLevel;
            dicClass["AcademicYearID"] = GlobalInfo.AcademicYearID;

            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dicClass);
            List<ClassProfile> lstClass = lsClass.ToList();
            if (lsClass.Count() != 0)
            {             
                ViewData[ReportTeachingAssignmentConstants.LS_CLASS] = lstClass;
            }
            else
            {
                ViewData[ReportTeachingAssignmentConstants.LS_CLASS] = new List<ClassProfile>();
            }
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
        }



        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportTeachingAssignmentBO reportTeachingAssignment)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            reportTeachingAssignment.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            reportTeachingAssignment.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            string reportCode = SystemParamsInFile.REPORT_PHAN_CONG_GIANG_DAY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                string inputParameterHashKey = ReportTeachingAssignmentBusiness.GetHashKey(reportTeachingAssignment);
                processedReport = ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportTeachingAssignmentBusiness.ExcelCreateTeachingAssignment(reportTeachingAssignment);
                processedReport = ReportTeachingAssignmentBusiness.ExcelInsertTeachingAssignment(reportTeachingAssignment, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportTeachingAssignmentBO reportTeachingAssignment)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            reportTeachingAssignment.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            reportTeachingAssignment.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();

            Stream excel = ReportTeachingAssignmentBusiness.ExcelCreateTeachingAssignment(reportTeachingAssignment);
            ProcessedReport processedReport = ReportTeachingAssignmentBusiness.ExcelInsertTeachingAssignment(reportTeachingAssignment, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_PHAN_CONG_GIANG_DAY
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;
            return result;

            
        }
    }
}
