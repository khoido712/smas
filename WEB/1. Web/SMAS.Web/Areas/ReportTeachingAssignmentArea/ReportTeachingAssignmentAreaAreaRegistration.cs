﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportTeachingAssignmentArea
{
    public class ReportTeachingAssignmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportTeachingAssignmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportTeachingAssignmentArea_default",
                "ReportTeachingAssignmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
