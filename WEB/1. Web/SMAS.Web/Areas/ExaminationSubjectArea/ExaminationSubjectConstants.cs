/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.ExaminationSubjectArea
{
    public class ExaminationSubjectConstants
    {
        public const string LIST_EXAMINATIONSUBJECT = "listExaminationSubject";
        public const string LISTEXAMINATION = "LISTEXAMINATION";
        public const string LISTAPPLIEDAREA = "LISTAPPLIEDAREA";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string ENABLE_SAVE = "ENABLE_SAVE";
        public const string HAS_CANDIDATE = "HAS_CANDIDATE";

        public const string EXAMINATION_ID = "Examination_ID";
    }
}