/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ExaminationSubjectArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Examination_Label_TitleChoice")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExaminationSubjectConstants.LISTEXAMINATION)]
        //[AdditionalMetadata("OnChange", "onExaminationChange(this)")]
        public System.Int32 ExaminationID { get; set; }

        //[ResDisplayName("ExaminationSubject_Label_AppliedArea")]

        //// [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", ExaminationSubjectConstants.LISTAPPLIEDAREA)]
        //[AdditionalMetadata("OnChange", "onAppliedAreaChange(this)")]
        //public int? AppliedArea { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_EducationLevelChoice")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExaminationSubjectConstants.LISTEDUCATIONLEVEL)]
        //[AdditionalMetadata("OnChange", "onEducationLevelChange(this)")]
        public int? EducationLevel { get; set; }
    }
}