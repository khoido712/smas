/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ExaminationSubjectArea.Models
{
    public class ExaminationSubjectViewModel
    {
        public System.Int32 ExaminationSubjectID { get; set; }

        public System.Int32 ExaminationID { get; set; }

        public System.Int32 SubjectID { get; set; }

        public System.Int32 EducationLevelID { get; set; }

        public System.String OrderNumberPrefix { get; set; }

        public System.Boolean IsLocked { get; set; }

        #region grid

        [ResourceDisplayName("ExaminationSubject_Label_Subject")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_StartTime")]
        public System.Nullable<System.DateTime> StartTime { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_DurationInMinute")]
        public System.Nullable<System.Int32> DurationInMinute { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_HasDetachableHead")]
        public System.Boolean HasDetachableHead { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_StartTime")]
        public string StartTimeDate { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_StartHour")]
        public string StartTimeHour { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_Choice")]
        public bool Choose { get; set; }

        #endregion grid
    }
}
