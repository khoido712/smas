﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.ExaminationSubjectArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ExaminationSubjectArea.Controllers
{
    public class ExaminationSubjectController : BaseController
    {
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ICandidateBusiness CandidateBusiness;

        public ExaminationSubjectController(IExaminationSubjectBusiness examinationsubjectBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , IExaminationBusiness ExaminationBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , ICandidateBusiness CandidateBusiness)
        {
            this.ExaminationSubjectBusiness = examinationsubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.ExaminationBusiness = ExaminationBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.CandidateBusiness = CandidateBusiness;
        }

        //
        // GET: /ExaminationSubject/

        public ActionResult Index()
        {
            SetViewData();

            //GlobalInfo gi = new GlobalInfo();
            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>()
            //{
            //     {"AcademicYearID",gi.AcademicYearID.Value},
            //    {"AppliedLevel",gi.AppliedLevel.Value}
            //};

            ////Get view data here
            //IEnumerable<ExaminationSubjectViewModel> lst = this._Search(SearchInfo);
            //ViewData[ExaminationSubjectConstants.LIST_EXAMINATIONSUBJECT] = lst;
            return View();
        }

        //
        // GET: /ExaminationSubject/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //

            IEnumerable<ExaminationSubjectViewModel> lst = this._Search(SearchInfo);
            ViewData[ExaminationSubjectConstants.LIST_EXAMINATIONSUBJECT] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            ExaminationSubject examinationsubject = new ExaminationSubject();
            TryUpdateModel(examinationsubject);
            Utils.Utils.TrimObject(examinationsubject);

            this.ExaminationSubjectBusiness.Insert(examinationsubject);
            this.ExaminationSubjectBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ExaminationSubjectID)
        {
            ExaminationSubject examinationsubject = this.ExaminationSubjectBusiness.Find(ExaminationSubjectID);
            CheckPermissionForAction(examinationsubject.ExaminationID, "Examination");
            TryUpdateModel(examinationsubject);
            Utils.Utils.TrimObject(examinationsubject);
            this.ExaminationSubjectBusiness.Update(examinationsubject);
            this.ExaminationSubjectBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            ExaminationSubject examinationsubject = this.ExaminationSubjectBusiness.Find(id);
            CheckPermissionForAction(examinationsubject.ExaminationID, "Examination");
            this.ExaminationSubjectBusiness.Delete(id);
            this.ExaminationSubjectBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ExaminationSubjectViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo gi = new GlobalInfo();
            IQueryable<ExaminationSubject> query = this.ExaminationSubjectBusiness.SearchBySchool(gi.SchoolID.Value, SearchInfo)
                .Where(o=>o.SubjectCat.IsActive)
                .Where(o => o.SubjectCat.AppliedLevel == gi.AppliedLevel.Value).OrderBy(o => o.SubjectCat.OrderInSubject);
            IQueryable<ExaminationSubjectViewModel> lst = query.Select(o => new ExaminationSubjectViewModel
            {
                ExaminationSubjectID = o.ExaminationSubjectID,
                ExaminationID = o.ExaminationID,
                SubjectID = o.SubjectID,
                EducationLevelID = o.EducationLevelID,
                StartTime = o.StartTime,
                DurationInMinute = o.DurationInMinute,
                OrderNumberPrefix = o.OrderNumberPrefix,
                IsLocked = o.IsLocked,
                HasDetachableHead = o.HasDetachableHead
            });

            List<ExaminationSubjectViewModel> lsESVM = lst.ToList();
            foreach (var item in lsESVM)
            {
                SubjectCat sc = SubjectCatBusiness.Find(item.SubjectID);
                item.SubjectName = sc.DisplayName;

                item.StartTimeDate = String.Format("{0:dd/MM/yyyy}", item.StartTime);
                item.StartTimeHour = String.Format("{0:hh:mm:ss}", item.StartTime);
            }
            return lsESVM;
        }

        #region setvviewData

        private void SetViewData()
        {
            //ExaminationBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
            // + Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            // + Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            ViewData[ExaminationSubjectConstants.ENABLE_SAVE] = false;
            DateTime datenow = DateTime.Now;
            GlobalInfo gi = new GlobalInfo();
            //thứ tự giảm dần của ngày kết thúc kỳ thi
            List<Examination> lsE = ExaminationBusiness.SearchBySchool(gi.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",gi.AcademicYearID.Value},
                {"AppliedLevel",gi.AppliedLevel.Value}
            }).OrderByDescending(o=>o.ToDate).ToList();

            for (int i = lsE.Count() - 1; i >= 0; i--)
            {
                Examination obj = lsE[i];
                DateTime todate = obj.ToDate.Value;
                if (todate >= datenow)
                {
                    ViewData[ExaminationSubjectConstants.EXAMINATION_ID] = obj.ExaminationID;
                    break;
                }
            }

            ViewData[ExaminationSubjectConstants.LISTEXAMINATION] = new SelectList(lsE, "ExaminationID", "Title");

            //cboEducationLevel: UserInfo.EducationLevels
            ViewData[ExaminationSubjectConstants.LISTEDUCATIONLEVEL] = new SelectList(gi.EducationLevels, "EducationLevelID", "Resolution");

            //cboAppliedArea:
            //+ 1: Áp dụng cho toàn trường
            //+ 2: Áp dụng cho toàn khối
            List<ComboObject> lsCO = new List<ComboObject>();
            ComboObject co1 = new ComboObject("1", Res.Get("ExaminationSubject_Label_SchoolApplied"));
            ComboObject co2 = new ComboObject("2", Res.Get("ExaminationSubject_Label_EducationApplied"));
            lsCO.Add(co1);
            lsCO.Add(co2);
            ViewData[ExaminationSubjectConstants.LISTAPPLIEDAREA] = new SelectList(lsCO, "key", "value");
            ViewData[ExaminationSubjectConstants.LIST_EXAMINATIONSUBJECT] = null;
            if (gi.IsCurrentYear)
            {
                ViewData[ExaminationSubjectConstants.ENABLE_SAVE] = true;
            }
        }

        #endregion setvviewData

        #region reload grid

        public void ReloadGridHelper(int? ExaminationID, int? EducationLevel)
        {
            GlobalInfo global = new GlobalInfo();
            
            //if (!ExaminationID.HasValue)
            //{
            //    throw new BusinessException("Common_Error_InternalError");
            //}
            if (ExaminationID.HasValue)
            {
                //cboAppliedArea.Value = 1 hoặc cboEducationLevel != 0)
                if ((EducationLevel.HasValue && EducationLevel.Value != 0))
                {
                    IQueryable<ClassSubject> lsCS = ClassSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"AppliedLevel",global.AppliedLevel.Value},
                    {"EducationLevelID",EducationLevel}
                }).Where(o => o.SubjectCat.IsActive);

                    IQueryable<ExaminationSubject> lsES = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"AppliedLevel",global.AppliedLevel.Value},
                    {"ExaminationID",ExaminationID},
                    {"EducationLevelID",EducationLevel}
                }); 

                    List<ExaminationSubjectViewModel> lsESVM = new List<ExaminationSubjectViewModel>();
                    List<SubjectCat> lstSubjectCat = lsCS.Select(o => o.SubjectCat).Distinct().ToList().OrderBy(o => o.OrderInSubject).ToList();
                    List<int> lsCSID = lstSubjectCat.Select(o => o.SubjectCatID).ToList();

                    List<int> lsSubjectIDInES = lsES.Select(o => o.SubjectID).ToList();

                    //all subject trong class subject
                    foreach (var item in lsCSID)
                    {
                        ExaminationSubjectViewModel esvm = new ExaminationSubjectViewModel();

                        //nhung truong da co san thi dua vao entity
                        esvm.ExaminationID = ExaminationID.Value;
                        esvm.SubjectID = item;

                        // esvm.EducationLevelID = (int)EducationLevel.Value;
                        //xet xet da co trong examinationsubjec chua
                        if (lsSubjectIDInES.Contains(item))
                        {
                            int minEdu = 1;
                            if (EducationLevel.HasValue)
                            {
                                minEdu = (int)EducationLevel.Value;
                            }
                            else
                            {
                         minEdu = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                        {
                            {"ExaminationID",ExaminationID},
                            {"SubjectID",item},
                        }).Min(o => o.EducationLevelID);
                            }

                            //ok
                            ExaminationSubject es = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                        {
                            {"ExaminationID",ExaminationID},
                            {"SubjectID",item},
                            {"EducationLevelID",EducationLevel}
                        }).FirstOrDefault();
                            esvm.Choose = true;
                            esvm.DurationInMinute = es.DurationInMinute;
                            esvm.EducationLevelID = es.EducationLevelID;
                            esvm.ExaminationSubjectID = es.ExaminationSubjectID;
                            esvm.HasDetachableHead = es.HasDetachableHead;
                            esvm.IsLocked = es.IsLocked;
                            esvm.StartTime = es.StartTime;
                            esvm.StartTimeDate = String.Format("{0:dd/MM/yyyy}", es.StartTime);
                            esvm.StartTimeHour = String.Format("{0:HH:mm}", es.StartTime);
                        }
                        SubjectCat sc = SubjectCatBusiness.Find(item);
                        esvm.SubjectName = sc.DisplayName;

                        lsESVM.Add(esvm);
                    }
                    ViewData[ExaminationSubjectConstants.LIST_EXAMINATIONSUBJECT] = lsESVM;
                }
                else
                {
                    List<ExaminationSubjectViewModel> lsESVM = new List<ExaminationSubjectViewModel>();
                    ViewData[ExaminationSubjectConstants.LIST_EXAMINATIONSUBJECT] = lsESVM;
                }
            }
            else
            {
                List<ExaminationSubjectViewModel> lsESVM = new List<ExaminationSubjectViewModel>();
                ViewData[ExaminationSubjectConstants.LIST_EXAMINATIONSUBJECT] = lsESVM;
            }
        }


        [ValidateAntiForgeryToken]
        public ActionResult ReloadGrid(int? ExaminationID, int? EducationLevel)
        {
            ReloadGridHelper(ExaminationID, EducationLevel);
            List<ExaminationSubjectViewModel> lstExaminationSubject = (List<ExaminationSubjectViewModel>)ViewData[ExaminationSubjectConstants.LIST_EXAMINATIONSUBJECT];
            GlobalInfo gi = new GlobalInfo();

            if (!ExaminationID.HasValue)
            {
                ViewData[ExaminationSubjectConstants.ENABLE_SAVE] = false;
            }
            else
            {
                Examination exam = ExaminationBusiness.Find(ExaminationID);
                //if (exam.CurrentStage >= SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
                if (exam.CurrentStage > SystemParamsInFile.EXAMINATION_STAGE_CREATED
                    || lstExaminationSubject.Count() == 0)
                {
                    ViewData[ExaminationSubjectConstants.ENABLE_SAVE] = false;
                }
                else
                {
                    ViewData[ExaminationSubjectConstants.ENABLE_SAVE] = true && gi.IsCurrentYear;
                }
            }
            Dictionary<string, object> dic = new Dictionary<string,object>();
            dic["ExaminationID"] = ExaminationID;
            dic["EducationLevel"] = EducationLevel;

            IQueryable<Candidate> iqCandidate = CandidateBusiness.SearchBySchool(gi.SchoolID.Value, dic);
            if (iqCandidate != null && iqCandidate.Count() > 0)
            {
                ViewData[ExaminationSubjectConstants.HAS_CANDIDATE] = "1";
            }
            else {
                ViewData[ExaminationSubjectConstants.HAS_CANDIDATE] = "0";
            }

            return View("_List");
        }

        #endregion reload grid

        #region SaveExaminationSubject

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult SaveExaminationSubject(int? AppliedArea, int? EducationLevel, int? ExaminationID, int[] DurationInMinute,
             int[] HasDetachableHead, DateTime[] StartTimeDate, DateTime[] StartTimeHour, int[] checkedExaminationSubject, int[] SubjectID, FormCollection form)
        {
            GlobalInfo global = new GlobalInfo();
            List<int> checkedLocation = new List<int>();
            if (GetMenupermission("ExaminationSubject", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //tim location cua checked subject
            for (int i = 0; i < SubjectID.Count(); i++)
            {
                if (checkedExaminationSubject != null)
                {
                    if (checkedExaminationSubject.Contains(SubjectID[i]))
                    {
                        checkedLocation.Add(i);
                    }
                }
            }

            //tu checked location ta co nhung List tuong ung
            List<int> lsDurationInMinute = new List<int>();
            List<bool> lsHasDetachableHead = new List<bool>();
            List<DateTime> lsStartTime = new List<DateTime>();
            List<int> lsSubjectID = new List<int>();
            if (checkedLocation.Count() == 0)
            {
                return Json(new JsonMessage(Res.Get("ExaminationSubject_Label_ErrorNoChoiceSubject")));
            }
            //dua cac thong tin can thiet vao list
            foreach (var item in checkedLocation)
            {
                lsDurationInMinute.Add(DurationInMinute[item]);

                bool HDH = (HasDetachableHead != null) ? HasDetachableHead.Contains(SubjectID[item]) : false;
                lsHasDetachableHead.Add(HDH);

                //DateTime date =  StartTimeDate[item];
                //DateTime hour = StartTimeHour[item];
                try
                {
                    string date = form["StartTimeDate" + SubjectID[item]];
                    string hour = form["StartTimeHour" + SubjectID[item]];
                    //string datetime = string.Format("{0:dd/MM/yyyy}", date) + " " + string.Format("{0:HH:mm}", hour);
                    string datetime = date +" "+ hour;
                    DateTime dt = new DateTime();
                    dt = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm", null);
                    lsStartTime.Add(dt);
                }
                catch (Exception ex)
                {
                    throw new BusinessException("ExaminationSubject_Label_DateTimeFormatError");
                }

                lsSubjectID.Add(SubjectID[item]);
            }
            if (!EducationLevel.HasValue)
            {
                EducationLevel = 0;
            }
            ExaminationSubjectBusiness.UpdateAll(global.SchoolID.Value, (int)global.AppliedLevel.Value, ExaminationID.Value, EducationLevel.Value,
                lsSubjectID, lsStartTime, lsDurationInMinute, lsHasDetachableHead);
            ExaminationSubjectBusiness.Save();
            return Json(new JsonMessage(Res.Get("ExaminationSubject_Label_SaveSuccess")));
        }

        #endregion SaveExaminationSubject


        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult CancelButtonClick(int? EducationLevel, int? ExaminationID, int[] hiddenExaminationSubject, FormCollection form)
        {
            GlobalInfo global = new GlobalInfo();
            if (GetMenupermission("ExaminationSubject", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            List<int> lsSubjectID = new List<int>();
            if (hiddenExaminationSubject == null || hiddenExaminationSubject.Count() == 0)
            {
                return Json(new JsonMessage(Res.Get("ExaminationSubject_Label_ErrorNoChoiceSubject")));
            }
            //dua cac thong tin can thiet vao list
            foreach (var item in hiddenExaminationSubject)
            {
                lsSubjectID.Add(item);
            }
            if (!EducationLevel.HasValue)
            {
                EducationLevel = 0;
            }
            ExaminationSubjectBusiness.CancelExaminationSubject(global.SchoolID.Value, (int)global.AppliedLevel.Value, ExaminationID.Value, EducationLevel.Value, lsSubjectID);
            ExaminationSubjectBusiness.Save();
            return Json(new JsonMessage(Res.Get("ExaminationSubject_Label_CancelSuccess")));
        }


    }
}
