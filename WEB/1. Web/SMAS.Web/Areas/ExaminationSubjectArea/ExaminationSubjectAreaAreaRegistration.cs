﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExaminationSubjectArea
{
    public class ExaminationSubjectAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExaminationSubjectArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExaminationSubjectArea_default",
                "ExaminationSubjectArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}