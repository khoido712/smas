﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using System.Web.Mvc;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Areas.PhysicalExaminationArea;
using SMAS.Web.Areas.PhysicalExaminationArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Text;
using SMAS.Web.Filter;
using Newtonsoft.Json;

namespace SMAS.Web.Areas.PhysicalExaminationArea.Controllers
{
    public class PhysicalExaminationController : BaseController
    {
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IBookmarkedFunctionBusiness BookmarkedFunctionBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        private readonly ICollectionCommentsBusiness CollectionCommentsBusiness;
        private readonly IPhysicalExaminationBusiness _PhysicalExaminationBusiness;
        private readonly IEducationLevelBusiness _EducationLevelBusiness;
        private readonly IReportDefinitionBusiness _ReportDefinitionBusiness;


        public PhysicalExaminationController(IRatedCommentPupilBusiness ratedCommentPupilBusiness, IAcademicYearBusiness academicYearBusiness,
         IClassProfileBusiness classProfileBusiness, IClassSubjectBusiness classSubjectBusiness, ISubjectCatBusiness subjectCatBusiness,
         IClassSupervisorAssignmentBusiness classSupervisorAssignmentBusiness, IUserAccountBusiness userAccountBusiness,
         ITeachingAssignmentBusiness teachingAssignmentBusiness, IBookmarkedFunctionBusiness bookmarkedFunctionBusiness,
         IPupilOfClassBusiness pupilOfClassBusiness, ILockRatedCommentPupilBusiness lockRatedCommentPupilBusiness,
         IRatedCommentPupilHistoryBusiness ratedCommentPupilHistoryBusiness, ISchoolProfileBusiness schoolProfileBusiness, IEmployeeBusiness employeeBusiness,
         IEvaluationCriteriaBusiness evaluationCriteriaBusiness, ICollectionCommentsBusiness collectionCommentsBusiness, IPhysicalExaminationBusiness physicalExaminationBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IEducationLevelBusiness EducationLevelBusiness)
        {
            this.RatedCommentPupilBusiness = ratedCommentPupilBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassSupervisorAssignmentBusiness = classSupervisorAssignmentBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.BookmarkedFunctionBusiness = bookmarkedFunctionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.LockRatedCommentPupilBusiness = lockRatedCommentPupilBusiness;
            this.RatedCommentPupilHistoryBusiness = ratedCommentPupilHistoryBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.EvaluationCriteriaBusiness = evaluationCriteriaBusiness;
            this.CollectionCommentsBusiness = collectionCommentsBusiness;
            this._PhysicalExaminationBusiness = physicalExaminationBusiness;
            _EducationLevelBusiness = EducationLevelBusiness;
            _ReportDefinitionBusiness = reportDefinitionBusiness;
        }

        private void SetViewDataForIndexView(int? classID)
        {
            ViewData[PhysicalExaminationConstants.LIST_CLASS] = new List<EducationLevelViewModel>();
            ViewBag.HasClass = false;
            ViewBag.IsTeacher = false;
            ViewBag.ClassIdSelected = 0;
            ViewBag.EducationIdSelected = 0;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;

            //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false
                && UtilsBusiness.IsBGH(_globalInfo.UserAccountID) == false && _globalInfo.IsEmployeeManager == false)
            {
                ViewBag.IsTeacher = true;
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
            }
            
            var educationLevels = _globalInfo.EducationLevels.Select(e => new EducationLevelViewModel()
            {
                Id = e.EducationLevelID,
                Name = e.Resolution
            }).ToList();
            bool _trackSetViewBag = false;
            for (int i = 0; i < educationLevels.Count; i++)
            {
                dic["EducationLevelID"] = educationLevels[i].Id;
                educationLevels[i].ClassProfiles = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                            .OrderBy(x => x.OrderNumber).Select(c => new ClassProfileViewModel()
                                                                            {
                                                                                Id = c.ClassProfileID,
                                                                                Name = c.DisplayName
                                                                            }).ToList();
                if (educationLevels[i].ClassProfiles.Count > 0)
                {
                    ViewBag.HasClass = true;
                }
                if (educationLevels[i].ClassProfiles.Count > 0 && _trackSetViewBag == false && (bool)ViewBag.IsTeacher == true)
                {
                    ViewBag.EducationIdSelected = educationLevels[i].Id;
                    educationLevels[i].Expand = true;
                    educationLevels[i].ClassProfiles[0].Selected = true;
                    ViewBag.ClassIdSelected = educationLevels[i].ClassProfiles[0].Id;
                    _trackSetViewBag = true;
                }
            }

            ViewData[PhysicalExaminationConstants.LIST_CLASS] = educationLevels;
            ViewData[PhysicalExaminationConstants.LIST_MONTH] = getListMonth();
        }

        public ActionResult Index(int? ClassID)
        {
            //this.SetViewData(ClassID);
            SetViewDataForIndexView(ClassID);
            return View();
        }

        private List<KeyValuePair<int, int>> getListMonth()
        {
            return _PhysicalExaminationBusiness.GetListMonthByAcademiId(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value);
        }

        [HttpPost]
        public PartialViewResult GetSubjectPanel()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            //int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            //int OldSubjectID = SMAS.Business.Common.Utils.GetInt(Request["oldSubjectID"]);
            //int semester = semesterid == null ? 0 : semesterid.Value;
            ViewData[PhysicalExaminationConstants.ClassID] = classid.Value;
            //bool ViewAll = false;

            //if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            //{
            //    ViewAll = true;
            //}
            int schoolId = _globalInfo.SchoolID.Value;
            var lsMonthOfYear = new List<KeyValuePair<int, int>>();
            if (classid.HasValue)
            {
                lsMonthOfYear = getListMonth();
            }
            ViewData[PhysicalExaminationConstants.LIST_MONTH] = lsMonthOfYear;
            ViewData[PhysicalExaminationConstants.ShowList] = false;
            ViewData[PhysicalExaminationConstants.GVCN_TEACHER] = UtilsBusiness.HasChargeTeacherPemission(_globalInfo.UserAccountID, classid.Value);
            return PartialView("_ViewMonth");
        }

        private void SetViewData(int? ClassID)// tobeRemove
        {
            //Set LIST_EDUCATIONLEVEL
            #region [LIST_EDUCATIONLEVEL]
            //List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();

            //var educationLevels = _globalInfo.EducationLevels;
            //for (int i = 0; i < educationLevels.Count; i++)
            //{
            //    listEducationLevel.Add(new ViettelCheckboxList(educationLevels[i].Resolution, educationLevels[i].EducationLevelID, i == 0, false));
            //}

            //ViewData[PhysicalExaminationConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;
            #endregion

            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            if (_globalInfo.EducationLevels.Count > 0)
            {
                #region [dic]
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                #endregion

                IEnumerable<ClassProfile> listClassProfiles = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).AsEnumerable();

                ViewData[PhysicalExaminationConstants.LIST_CLASS] = listClassProfiles;

                //Get ClassProfile
                #region [ClassProfile]
                if (listClassProfiles.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listClassProfiles.First();
                    if (listClassProfiles.Any(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listClassProfiles.Where(x => x.ClassProfileID == ClassID.GetValueOrDefault()).FirstOrDefault();
                    }
                    else
                    {
                        // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            // Ưu tiên trước đối với giáo viên bộ môn
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                            List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                            {
                                cp = listSubjectTeacher.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                        .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listHead != null && listHead.Count > 0)
                                {
                                    cp = listHead.First();
                                }
                            }
                        }
                    }
                }
                #endregion
                ViewData["ClassProfile"] = cp;
            }

            //lay list mon hoc
            //List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (cp != null && _globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && cp.ClassProfileID > 0)
            {
                //lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, _globalInfo.Semester.Value, cp.ClassProfileID, _globalInfo.IsViewAll)
                //    .Where(o => o.SubjectCat.IsActive == true)
                //    //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                //    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                //    .ToList();
            }
            else
            {
                ViewData[PhysicalExaminationConstants.CLASS_NULL] = true;
            }
            var listMonths = getListMonth();
            //int monthSelectedId = 0;
            //foreach (int month in listMonths.Keys)
            //{
            //    if (listMonths[month] == DateTime.Now.Year && month == DateTime.Now.Month)
            //    {
            //        monthSelectedId = month;
            //        break;
            //    }
            //}
            //ViewData[PhysicalExaminationConstants.MONTH_SELECT_ID] = monthSelectedId == 0 ? listMonths.Keys.First() : monthSelectedId;
            ViewData[PhysicalExaminationConstants.LIST_MONTH] = listMonths;
        }

        [HttpPost]
        public PartialViewResult AjaxLoadGridView(int year, int ClassID, int MonthSelected)
        {
            var pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(ClassID)
                .OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

            ViewData[PhysicalExaminationConstants.LatestModified_Name] = null;
            ViewData[PhysicalExaminationConstants.LatestModified_Date] = null;
            #region [Set Info Value For Above]
            var listPupilIds = pupilOfClassList.Select(x => x.PupilID).ToList();
            var latestModified = _PhysicalExaminationBusiness.All.Where(x => listPupilIds.Contains(x.PupilID)).OrderByDescending(x => x.ModifiedDate).Select(x => new { x.ModifiedDate, x.LogID }).FirstOrDefault();
            if (latestModified != null)
            {
                ViewData[PhysicalExaminationConstants.LatestModified_Date] = latestModified.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy");
                if (latestModified.LogID == null)
                {
                    ViewData[PhysicalExaminationConstants.LatestModified_Name] = PhysicalExaminationConstants.IS_ADMIN;
                }
                else
                {
                    ViewData[PhysicalExaminationConstants.LatestModified_Name] = this.EmployeeBusiness.All.Where(x => x.EmployeeID == latestModified.LogID.Value).Select(x => x.FullName).FirstOrDefault();
                }
            }
            #endregion

            SetVisbileButton(ClassID);

            var lstResult = this.GetDataPhysicalExamination(pupilOfClassList, ClassID);
            var listMonth = getListMonth();

            if (MonthSelected == 0 && listMonth.Count > 0)
            {
                MonthSelected = getListMonth().FirstOrDefault().Key;
            }
            ViewData[PhysicalExaminationConstants.LIST_RESULT] = lstResult;
            ViewData[PhysicalExaminationConstants.LIST_MONTH] = listMonth;
            ViewData[PhysicalExaminationConstants.MONTH_SELECT_ID] = MonthSelected;
            string lsthtmlResult = "";
            lsthtmlResult = JsonConvert.SerializeObject(lstResult);
            ViewData[PhysicalExaminationConstants.HTML_RESULT] = lsthtmlResult;
            ViewData["Year"] = year;
            return PartialView("_AjaxLoadGridView");
        }

        private List<PhysicalExaminationViewModel> GetDataPhysicalExamination(List<PupilOfClassBO> lstPOC, int ClassID)
        {
            var lstResult = new List<PhysicalExaminationViewModel>();

            for (int i = 0; i < lstPOC.Count; i++)
            {
                var objPOC = lstPOC[i];
                var objResult = new PhysicalExaminationViewModel();
                objResult.PupilID = objPOC.PupilID;
                objResult.PupilName = objPOC.PupilFullName.Replace("\t","\\t");
                objResult.Birthday = objPOC.Birthday;
                objResult.Status = objPOC.Status;
                objResult.Gender = objPOC.Genre;
                objResult.listPE = _PhysicalExaminationBusiness.All.Where(p => p.PupilID == objPOC.PupilID && p.ClassID == ClassID && p.SchoolID == _globalInfo.SchoolID.Value)
                    .Select(x => new PhysicalExaminationModel()
                    {
                        PhysicalExaminationID = x.PhysicalExaminationID,
                        Height = x.Height,
                        Weight = x.Weight,
                        MonthID = x.MonthID,
                        PhysicalHeightStatus = x.PhysicalHeightStatus,
                        PhysicalWeightStatus = x.PhysicalWeightStatus
                    })
                    .ToList();

                //var lstPE = _PhysicalExaminationBusiness.All.Where(p => p.PupilID == objPOC.PupilID && p.SchoolID == _globalInfo.SchoolID.Value).ToList();
                //if (lstPE.Count > 0)
                //{
                //    var lstPEmodel = new List<PhysicalExaminationModel>();
                //    for (int j = 0; j < lstPE.Count; j++)
                //    {
                //        var PEmodel = new PhysicalExaminationModel();
                //        PEmodel.PhysicalExaminationID = lstPE[j].PhysicalExaminationID;
                //        int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;
                //        int monthCount = GetMonthDifference(objResult.Birthday, DateTime.Parse("1" + "/" + MonthSelected + "/" + year));
                //        string sttHeight = _PhysicalExaminationBusiness.checkStandardPE(lstPE[j].Height, "H", monthCount, objResult.Gender);
                //        string sttWeight = _PhysicalExaminationBusiness.checkStandardPE(lstPE[j].Weight, "W", monthCount, objResult.Gender);
                //        PEmodel.Weight = lstPE[j].Weight;
                //        PEmodel.MessageWeight = sttWeight;
                //        PEmodel.Height = lstPE[j].Height;
                //        PEmodel.MessageHeight = sttHeight;
                //        PEmodel.MonthID = lstPE[j].MonthID;
                //        PEmodel.PhysicalExaminationID = lstPE[j].PhysicalExaminationID;
                //        lstPEmodel.Add(PEmodel);
                //    }
                //    objResult.listPE = lstPEmodel;
                //}
                lstResult.Add(objResult);
            }
            return lstResult;
        }

        private bool SetVisbileButton(int classID)
        {
            bool result = UtilsBusiness.HasChargeTeacherPemission(_globalInfo.UserAccountID, classID);
            ViewData[PhysicalExaminationConstants.IS_VISIBLED_BUTTON] = result;

            return result;
        }

        [HttpPost]
        public PartialViewResult AjaxLoadAddPhysical(int ClassID, string MonthName, int PupilID)
        {
            var model = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(x => x.PupilID == PupilID && x.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                .Select(x => new PhysicalExaminationViewModel()
                {
                    PupilID = x.PupilID,
                    PupilName = x.PupilFullName
                }).FirstOrDefault();

            //lay danh sach hoc sinh cua lop
            //List<PhysicalExaminationViewModel> lstPupil = (from poc in PupilOfClassBusiness.GetPupilInClass(ClassID).Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
            //                                               select new PhysicalExaminationViewModel
            //                                        {
            //                                            PupilID = poc.PupilID,
            //                                            PupilName = poc.PupilFullName
            //                                        }).OrderBy(c => c.PupilName).ToList();

            //ViewData[PhysicalExaminationConstants.LIST_PUPIL] = lstPupil;
            //ViewData[PhysicalExaminationConstants.PUPIL_ID] = PupilID;
            //AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            return PartialView("_AddPhysical");
        }

        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public JsonResult AddPhyExPupil(FormCollection frm)
        {
            int ClassID = frm["ClassID"] != null ? int.Parse(frm["ClassID"]) : 0;
            int MonthID = frm["hdfMonthID"] != null ? int.Parse(frm["hdfMonthID"]) : 0;
            string MonthName = frm["hdfMonthNameAdd"];
            int PupilID = frm["PupilID"] != null ? int.Parse(frm["PupilID"]) : 0;

            if (ClassID == 0 || MonthID == 0 || PupilID == 0)
            {
                return Json(new JsonMessage(Res.Get("Error_Save"), "error"));
            }

            List<PhysicalExaminationBO> lstPhyExBO = new List<PhysicalExaminationBO>();
            PhysicalExaminationBO objPhyExBO = new PhysicalExaminationBO();
            objPhyExBO.SchoolID = _globalInfo.SchoolID.Value;
            objPhyExBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
            objPhyExBO.PupilID = PupilID;
            objPhyExBO.ClassID = ClassID;
            objPhyExBO.MonthID = MonthID;
            if (!_globalInfo.UserAccount.IsAdmin)
            {
                objPhyExBO.LogID = _globalInfo.UserAccount.EmployeeID.Value;
            }

            lstPhyExBO.Add(objPhyExBO);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},                
                {"MonthID",MonthID}                
            };
            _PhysicalExaminationBusiness.InsertOrUpdatePE(lstPhyExBO, dic);
            return Json(new JsonMessage(Res.Get("success_Save"), "success"));
        }

        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public JsonResult SavePEsPupil(FormCollection frm)
        {
            int year = frm["year"] != null ? int.Parse(frm["year"]) : 0;
            int classID = frm["ClassID"] != null ? int.Parse(frm["ClassID"]) : 0;
            int monthID = frm["hdfMonthName"] != null ? int.Parse(frm["hdfMonthName"]) : 0;

            if (classID <= 0 || monthID <= 0 || year < 0)
            {
                return Json(new JsonMessage(Res.Get("Comment_Of_Pupil_SaveError"), "error"));
            }

            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVCN = UtilsBusiness.HasChargeTeacherPemission(_globalInfo.UserAccountID, classID);
            if (!(isAdminShcool || isGVCN))
            {
                return Json(new JsonMessage(Res.Get("Comment_Of_Pupil_SaveError"), "error"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"MonthID",YearAndMonth(year, monthID)}
            };

            List<List<string>> newLst = new List<List<string>>();
            #region [Get Info Into Above]
            string strPE = frm["lstSelectedPupil"].ToString();
            //strPE = strPE.Substring(0, strPE.Length - 1);
            strPE = strPE.TrimEnd(';');
            List<string> strHW = strPE.Split(new string[] { ";" }, StringSplitOptions.None).Select(p => p).ToList();

            for (int i = 0; i < strHW.Count; i++)
            {
                var hw = strHW[i].Split(new string[] { "_" }, 3, StringSplitOptions.None).Select(p => p).ToList();
                newLst.Add(hw);
            }
            #endregion


            List<PhysicalExaminationBO> lstPhysicalExaminationBO = new List<PhysicalExaminationBO>();
            var pupilIdSelected = new List<int>();
            //lay danh sach hoc sinh
            List<PupilOfClassBO> pupilOfClassStuding = PupilOfClassBusiness.GetPupilInClass(classID).Where(x => x.Status == GlobalConstants.PUPIL_STATUS_STUDYING).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            int LogID = _globalInfo.UserAccount.IsAdmin == true ? 0 : _globalInfo.UserAccount.EmployeeID.Value;

                    for (int j = 0; j < newLst.Count; j++)
                    {
                        if (newLst[j].Count >= 3)
                        {
                    int pupilId = 0; int.TryParse(newLst[j][2], out pupilId);
                    var objPOC = pupilOfClassStuding.Where(x => x.PupilID == pupilId && x.Status == GlobalConstants.PUPIL_STATUS_STUDYING).FirstOrDefault();

                    if (objPOC != null && "on".Equals(frm["chk_" + objPOC.PupilID]))
                            {
                        pupilIdSelected.Add(pupilId);
                        var objPhysicalExaminationBO = new PhysicalExaminationBO();
                        objPhysicalExaminationBO.Height = newLst[j][0].ToDecimal();
                        objPhysicalExaminationBO.Weight = newLst[j][1].ToDecimal();

                                if (objPhysicalExaminationBO.Height != null || objPhysicalExaminationBO.Weight != null)
                                {
                                    objPhysicalExaminationBO.SchoolID = _globalInfo.SchoolID.Value;
                                    objPhysicalExaminationBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                    objPhysicalExaminationBO.PupilID = objPOC.PupilID;
                                    objPhysicalExaminationBO.ClassID = objPOC.ClassID;
                                    objPhysicalExaminationBO.MonthID = YearAndMonth(year, monthID);
                                    objPhysicalExaminationBO.LogID = LogID;
                                    objPhysicalExaminationBO.PhysicalHeightStatus = _PhysicalExaminationBusiness.GetPhysicalStatus(MonthForStandard(objPOC.Birthday, year, monthID), objPOC.Genre.Value, 0, objPhysicalExaminationBO.Height);
                                    objPhysicalExaminationBO.PhysicalWeightStatus = _PhysicalExaminationBusiness.GetPhysicalStatus(MonthForStandard(objPOC.Birthday, year, monthID), objPOC.Genre.Value, 1, objPhysicalExaminationBO.Weight);
                                    lstPhysicalExaminationBO.Add(objPhysicalExaminationBO);
                                }
                            }
                        }
                    }

            #region [Ole]
            //for (int i = 0; i < pupilOfClassStuding.Count; i++)
            //{
            //    var objPOC = pupilOfClassStuding[i];
            //    if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            //    {
            //        continue;
            //    }

            //    objPhysicalExaminationBO = new PhysicalExaminationBO();
            //    if ("on".Equals(frm["chk_" + objPOC.PupilID]))
            //    {
            //        for (int j = 0; j < newLst.Count; j++)
            //        {
            //            if (newLst[j].Count >= 3)
            //            {
            //                if (int.Parse(newLst[j][2]) == objPOC.PupilID)
            //                {
            //                    if (!string.IsNullOrWhiteSpace(newLst[j][0])) { objPhysicalExaminationBO.Height = decimal.Parse(newLst[j][0].ValidateStringToDecimal()); }
            //                    if (!string.IsNullOrWhiteSpace(newLst[j][1])) { objPhysicalExaminationBO.Weight = decimal.Parse(newLst[j][1].ValidateStringToDecimal()); }

            //                    if (objPhysicalExaminationBO.Height != null || objPhysicalExaminationBO.Weight != null)
            //                    {
            //                        objPhysicalExaminationBO.SchoolID = _globalInfo.SchoolID.Value;
            //                        objPhysicalExaminationBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
            //                        objPhysicalExaminationBO.PupilID = objPOC.PupilID;
            //                        objPhysicalExaminationBO.ClassID = objPOC.ClassID;
            //                        objPhysicalExaminationBO.MonthID = YearAndMonth(year, monthID);
            //                        objPhysicalExaminationBO.LogID = LogID;
            //                        objPhysicalExaminationBO.PhysicalHeightStatus = _PhysicalExaminationBusiness.GetPhysicalStatus(MonthForStandard(objPOC.Birthday, year, monthID), objPOC.Genre.Value, 0, objPhysicalExaminationBO.Height);
            //                        objPhysicalExaminationBO.PhysicalWeightStatus = _PhysicalExaminationBusiness.GetPhysicalStatus(MonthForStandard(objPOC.Birthday, year, monthID), objPOC.Genre.Value, 1, objPhysicalExaminationBO.Weight);
            //                        lstPhysicalExaminationBO.Add(objPhysicalExaminationBO);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion

            
            if (lstPhysicalExaminationBO != null)
            {
                dic["lstPupilID"] = pupilIdSelected;
                _PhysicalExaminationBusiness.InsertOrUpdatePE(lstPhysicalExaminationBO, dic);
            }
            return Json(new JsonMessage(Res.Get("success_Save"), "success"));
        }

        private int YearAndMonth(int year, int month)
        {
            if (month < 10)
            {
                return int.Parse(string.Format("{0}0{1}", year, month));
            }
            else
            {
                return int.Parse(string.Format("{0}{1}", year, month));
            }
        }
        private int MonthForStandard(DateTime birthday, int year, int month)
        {
            var result = (month - birthday.Month) + 12 * (year - birthday.Year);
            return result < 0 ? 0 : result;
        }

        public JsonResult SaveSinglePEPupil(FormCollection frm)
        {
            int year = frm["dialog_Year"] != null ? int.Parse(frm["dialog_Year"]) : 0;
            int ClassID = frm["dialog_ClassID"] != null ? int.Parse(frm["dialog_ClassID"]) : 0;
            int MonthID = frm["dialog_MonthID"] != null ? int.Parse(frm["dialog_MonthID"]) : 0;
            int PupilID = frm["dialog_PupilID"] != null ? int.Parse(frm["dialog_PupilID"]) : 0;

            decimal? height = null;
            if (!string.IsNullOrWhiteSpace(frm["Height"]))
            {
                height = frm["Height"].ToDecimal();
            }
            decimal? weight = null;
            if (!string.IsNullOrWhiteSpace(frm["Weight"]))
            {
                weight = frm["Weight"].ToDecimal();
            }

            int physicalExaminationID = !string.IsNullOrEmpty(frm["dialog_PhysicalExaminationID"]) ? int.Parse(frm["dialog_PhysicalExaminationID"]) : 0;

            if (ClassID <= 0 || MonthID <= 0 || PupilID <= 0 || year <= 0)
            {
                return Json(new JsonMessage(Res.Get("Error_Save"), "error"));
            }


            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},                
                {"MonthID",YearAndMonth(year, MonthID)},
                {"PhysicalExaminationID",physicalExaminationID}
            };
            var pupilEntity = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(x => x.PupilID == PupilID).Select(x => new { x.Genre, x.Status, x.Birthday }).FirstOrDefault();

            if (pupilEntity == null || pupilEntity.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                return Json(new JsonMessage(Res.Get("Error_Save"), "error"));
            }

            PhysicalExaminationBO model = new PhysicalExaminationBO();
            model.PhysicalExaminationID = physicalExaminationID;
            model.Height = height;
            model.Weight = weight;
            model.SchoolID = _globalInfo.SchoolID.Value;
            model.AcademicYearID = _globalInfo.AcademicYearID.Value;
            model.ClassID = ClassID;
            model.MonthID = YearAndMonth(year, MonthID);
            model.PupilID = PupilID;
            model.PhysicalHeightStatus = _PhysicalExaminationBusiness.GetPhysicalStatus(MonthForStandard(pupilEntity.Birthday, year, MonthID), pupilEntity.Genre.Value, 0, height);
            model.PhysicalWeightStatus = _PhysicalExaminationBusiness.GetPhysicalStatus(MonthForStandard(pupilEntity.Birthday, year, MonthID), pupilEntity.Genre.Value, 1, weight);
            if (!_globalInfo.UserAccount.IsAdmin)
            {
                model.LogID = _globalInfo.UserAccount.EmployeeID.Value;
            }
            _PhysicalExaminationBusiness.InsertOrUpdateSinglePE(model, dic, physicalExaminationID);

            return Json(new JsonMessage(Res.Get("success_Save"), "success"));
        }

        public JsonResult AjaxLoadPEByPupil(int ClassID, int MonthID, int PupilID)
        {
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(x => x.PupilID == PupilID && x.Status == GlobalConstants.PUPIL_STATUS_STUDYING).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

            PhysicalExaminationModel modelPE = null;
            var objPEview = GetDataPhysicalExamination(lstPOC, ClassID).FirstOrDefault();
            if (objPEview != null && objPEview.listPE != null)
            {
                modelPE = objPEview.listPE.Where(x => x.MonthID == MonthID).FirstOrDefault();
            }
            PEbyMonthModel result = new PEbyMonthModel();
            result.PupilID = objPEview.PupilID;
            result.PupilName = objPEview.PupilName;
            if (modelPE != null)
            {
                result.PhyExID = modelPE.PhysicalExaminationID;
                result.MonthID = modelPE.MonthID;
                result.Height = modelPE.Height;
                result.Weight = modelPE.Weight;
                int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;
                int monthCount = DateTime.Parse("1" + "/" + modelPE.MonthID + "/" + year).Month - objPEview.Birthday.Month;
                string sttHeight = _PhysicalExaminationBusiness.checkStandardPE(result.Height, "H", monthCount, objPEview.Gender);
                string sttWeight = _PhysicalExaminationBusiness.checkStandardPE(result.Weight, "W", monthCount, objPEview.Gender);
                result.MessageHeight = sttHeight;
                result.MessageWeight = sttWeight;
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePhysicalExamination(string ArrPEDelete, int year, int ClassID, int MonthID)
        {

            if (ClassID <= 0 || MonthID <= 0 || year <= 0)
            {
                return Json(new JsonMessage(Res.Get("Error_Save"), "error"));
            }

            if (MonthID < 10)
            {
                MonthID = int.Parse(string.Format("{0}0{1}", year, MonthID));
            }
            else
            {
                MonthID = int.Parse(string.Format("{0}{1}", year, MonthID));
            }

            List<int> lstPupilID = ArrPEDelete.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"MonthID",MonthID},
                {"lstPupilID",lstPupilID}
            };
            _PhysicalExaminationBusiness.DeletePE(MonthID, dic);
            return Json(new JsonMessage("Xóa thành công"));
        }

        public int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }

        public class MonthComment
        {
            public DateTime DateTime { get; set; }
            public int MonthID { get; set; }
            public string MonthName { get; set; }
        }

        [ValidateAntiForgeryToken]
        public JsonResult FirstExport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;


            ReportDefinition reportDef = _ReportDefinitionBusiness.GetByCode(SystemParamsInFile.CAN_D0_SUC_KHOE);
            IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        {"AcademicYearID",_globalInfo.AcademicYearID},
                                                        {"SchoolID",_globalInfo.SchoolID},
                                                        {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                        {"EducationLevelID",educationLevelID}
                                                    };
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBusiness.GetChildrenByAgeReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = PupilOfClassBusiness.CreateChildrenByAgeReport(dic);
                processedReport = PupilOfClassBusiness.InsertChildrenByAgeReport(dic, excel, _globalInfo.AppliedLevel.Value);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult CreateNewReport(int year, int classId, int monthId, int educationLevelID)
        {
            if (classId == 0 || monthId == 0 || year == 0)
            {
                return Json(new JsonMessage(Res.Get("Error_Save"), "error"));
            }

            ProcessedReport processedReport = null;

            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID}
            };
            string className = ClassProfileBusiness.All.Where(x => x.ClassProfileID == classId).Select(x => x.DisplayName).FirstOrDefault() ?? "";

            Stream excel = CreateChildrenByAgeReport(year, classId, className, monthId);
            processedReport = _PhysicalExaminationBusiness.InsertPhysicalExaminationReport(dicInsert, monthId, className, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);

            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }


        private IEnumerable<PhysicalExaminationExcelViewModel> GetDataExport(int year, int classId, int monthId)
        {
            var pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classId)
                .OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).Select(x => new PhysicalExaminationExcelViewModel
                {
                    Id = x.PupilID,
                    Code = x.PupilCode,
                    BirthDay = x.Birthday,
                    Name = x.PupilFullName,
                    Status = x.Status,
                    Gender = x.Genre
                }).ToList();
            int _yearAndMonth = YearAndMonth(year, monthId);
            int PartitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);

            for (int i = 0; i < pupilOfClassList.Count; i++)
            {
                int pupilId = pupilOfClassList[i].Id;
                var physicalExaminationEntiy = _PhysicalExaminationBusiness.All.Where(p => p.PupilID == pupilId && p.MonthID == _yearAndMonth && p.SchoolID == _globalInfo.SchoolID.Value && p.LastDigitSchoolID == PartitionID && p.ClassID == classId).Select(x => new { x.Weight, x.Height, x.PhysicalExaminationID }).FirstOrDefault();
                if (physicalExaminationEntiy != null)
                {
                    pupilOfClassList[i].Height = physicalExaminationEntiy.Height;
                    pupilOfClassList[i].Weight = physicalExaminationEntiy.Weight;
                    pupilOfClassList[i].PhysicalExaminationId = physicalExaminationEntiy.PhysicalExaminationID;
                }

                yield return pupilOfClassList[i];
            }
        }

        private Stream CreateChildrenByAgeReport(int year, int classId, string className, int monthId)
        {
            //AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.CAN_D0_SUC_KHOE;
            ReportDefinition rp = _ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy ra sheet đầu tiên
            IVTWorksheet sheet = oBook.GetSheet(1);

            sheet.Name = string.Format("Thang {0}", monthId);

            string _superVisingDeptName = _globalInfo.SuperVisingDeptName;
            if (string.IsNullOrEmpty(_superVisingDeptName) && _globalInfo.SchoolID.HasValue)
            {
                _superVisingDeptName = SchoolProfileBusiness.All.Where(x => x.SchoolProfileID == _globalInfo.SchoolID.Value).Select(x => x.SupervisingDept.SupervisingDeptName).FirstOrDefault();
            }

            sheet.SetCellValue("A2", _superVisingDeptName.ToUpper());
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpperInvariant());


            sheet.SetCellValue("A6", string.Format("Lớp {0}, Tháng {1}, Năm học {2}", className, monthId,
                AcademicYearBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID.Value).Select(x => x.DisplayTitle).FirstOrDefault() ?? ""));

            int soThuTu = 1;
            int columnStart = 1;
            int rowStart = 9;
            int rowCurrent = rowStart;
            int columnCurrent = columnStart;
            foreach (PhysicalExaminationExcelViewModel item in GetDataExport(year, classId, monthId))
            {
                columnCurrent = columnStart;

                //So Thu Tu
                sheet.SetCellValue(rowCurrent, columnCurrent++, soThuTu);
                //Ma Tre
                if (item.Status != SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(rowCurrent, columnCurrent, rowCurrent, columnCurrent + 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                sheet.SetCellValue(rowCurrent, columnCurrent++, item.Code);
                //Ho Ten                
                sheet.SetCellValue(rowCurrent, columnCurrent++, item.Name);
                //Ngay Sinh
                sheet.GetRange(rowCurrent, columnCurrent, rowCurrent, columnCurrent).NumberFormat("dd/mm/yyyy");
                sheet.GetRange(rowCurrent, columnCurrent, rowCurrent, columnCurrent).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(rowCurrent, columnCurrent++, item.BirthDay);
                //Cao
                sheet.GetRange(rowCurrent, columnCurrent, rowCurrent, columnCurrent).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(rowCurrent, columnCurrent++, item.Height);
                //Nặng
                sheet.GetRange(rowCurrent, columnCurrent, rowCurrent, columnCurrent).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(rowCurrent, columnCurrent++, item.Weight);

                //Set Font
                sheet.GetRange(rowCurrent, columnStart, rowCurrent, columnCurrent).SetFontName("Times New Roman", 12);


                rowCurrent++;
                soThuTu++;
            }
            sheet.GetRange(rowStart, columnStart, rowCurrent - 1, columnCurrent - 1).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);

            sheet.SetCellValue(1000, 50, "Viettel");

            sheet.FitAllColumnsOnOnePage = true;

            return oBook.ToStream();
        }

        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImportCap(IEnumerable<HttpPostedFileBase> attachmentsCap, int year, int classId, int monthId)
        {
            if (attachmentsCap == null || attachmentsCap.Count() <= 0 || classId == 0 || monthId == 0 || year == 0)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachmentsCap.FirstOrDefault();
            if (file != null)
            {
                #region [Validate File]
                //string className = ClassProfileBusiness.All.Where(x => x.ClassProfileID == classId).Select(x => x.DisplayName).FirstOrDefault() ?? "";
                //if (file.FileName != _PhysicalExaminationBusiness.GetReportNameForPhysicalExaminationReport(monthId, className))
                //{
                //    //Ten file khac dinh dang
                //    return Json(new JsonMessage("File excel không hợp lệ", "error"));
                //}
                //if (file.ContentLength / 1024 > 5120)
                //{
                //    return Json(new JsonMessage(Res.Get("Common_Max_Size_FileExcel"), "error"));
                //}
                #endregion

                //var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                //fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                //var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                //file.SaveAs(physicalPath);
                //Session["FilePathCap"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(file.InputStream);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;

                #region [Validate Sheet]
                if (lstSheets == null || lstSheets.Count == 0)
                {
                    return Json(new JsonMessage("File excel không hợp lệ", "error"));
                }
                sheet = oBook.GetSheet(1);
                //var checkFILE = sheet.GetCellValue(1000, 50);
                //if (checkFILE == null || checkFILE.ToString().Trim().Equals("Viettel", StringComparison.OrdinalIgnoreCase) == false)
                //{
                //    return Json(new JsonMessage("File excel không hợp lệ", "error"));
                //}
                string className = ClassProfileBusiness.All.Where(x => x.ClassProfileID == classId).Select(x => x.DisplayName).FirstOrDefault() ?? "";
                string valueCellTitle = string.Format("Lớp {0}, Tháng {1}, Năm học {2}", className, monthId,
                AcademicYearBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID.Value).Select(x => x.DisplayTitle).FirstOrDefault() ?? "");
                var valueCellTitleOnSheet = sheet.GetCellValue("A6");
                if (valueCellTitleOnSheet == null || string.IsNullOrEmpty(valueCellTitleOnSheet.ToString())
                    || valueCellTitleOnSheet.ToString().Trim().Equals(valueCellTitle, StringComparison.OrdinalIgnoreCase) == false)
                {
                    return Json(new JsonMessage("Tháng cân đo không đúng", "error"));
                }
                #endregion

                IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classId},
                {"MonthID",YearAndMonth(year, monthId)}
            };

                int columnCode = 2;
                int columnName = 3;
                int columnHeight = 5;
                int columnWeight = 6;
                int rowCurrent = 9;
                int LogID = _globalInfo.UserAccount.IsAdmin == true ? 0 : _globalInfo.UserAccount.EmployeeID.Value;
                var entitiesUpdate = new List<PhysicalExaminationBO>();

                var pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classId)
                    .Select(x => new PhysicalExaminationExcelViewModel
                    {
                        Id = x.PupilID,
                        Code = x.PupilCode,
                        BirthDay = x.Birthday,
                        Name = x.PupilFullName,
                        Status = x.Status,
                        Gender = x.Genre
                    }).ToList();

                var pupilIdSelected = new List<int>();

                bool stopWhile = true;
                while (stopWhile)
                {
                    string valueCellCode = (sheet.GetCellValue(rowCurrent, columnCode) ?? "").ToString().Trim();
                    string valueCellName = (sheet.GetCellValue(rowCurrent, columnName) ?? "").ToString().Trim();

                    if (string.IsNullOrEmpty(valueCellCode) && string.IsNullOrEmpty(valueCellName))
                    {
                        stopWhile = false;
                    }
                    else
                    {
                        //Tim hoc sinh co code va trang thai dang hoc
                        var pupil = pupilOfClassList.Where(x => x.Code.Equals(valueCellCode, StringComparison.OrdinalIgnoreCase) == true && x.Status == SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING).FirstOrDefault();
                        if (pupil != null)
                        {
                            pupilIdSelected.Add(pupil.Id);

                            var entityUpdate = new PhysicalExaminationBO()
                            {
                                Height = (sheet.GetCellValue(rowCurrent, columnHeight) ?? "").ToString().ToDecimal(),
                                Weight = (sheet.GetCellValue(rowCurrent, columnWeight) ?? "").ToString().ToDecimal()
                            };


                            if (entityUpdate.Height >= 1000) entityUpdate.Height = 999;
                            if (entityUpdate.Weight >= 100) entityUpdate.Weight = 99;

                            if (entityUpdate.Height == 0) entityUpdate.Height = null;
                            if (entityUpdate.Weight == 0) entityUpdate.Weight = null;

                            if (entityUpdate.Height.HasValue || entityUpdate.Weight.HasValue)
                            {
                                entityUpdate.SchoolID = _globalInfo.SchoolID.Value;
                                entityUpdate.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                entityUpdate.PupilID = pupil.Id;
                                entityUpdate.ClassID = classId;
                                entityUpdate.MonthID = YearAndMonth(year, monthId);
                                entityUpdate.LogID = LogID;
                                entityUpdate.PhysicalHeightStatus = _PhysicalExaminationBusiness.GetPhysicalStatus(MonthForStandard(pupil.BirthDay, year, monthId), pupil.Gender.Value, 0, entityUpdate.Height);
                                entityUpdate.PhysicalWeightStatus = _PhysicalExaminationBusiness.GetPhysicalStatus(MonthForStandard(pupil.BirthDay, year, monthId), pupil.Gender.Value, 1, entityUpdate.Weight);
                                entitiesUpdate.Add(entityUpdate);
                            }  
                        }
                                              
                    }
                    rowCurrent++;
                }//End While
                
                if (entitiesUpdate.Count > 0)
                {
                    dic["lstPupilID"] = pupilIdSelected;
                    _PhysicalExaminationBusiness.InsertOrUpdatePE(entitiesUpdate, dic);
                }

            }
            return Json(new JsonMessage("Cập nhật từ Excel thành công"));
        }

        #region [Ve Bieu Do]
        private const int numMonthOfBirthDay = 60;

        [HttpPost]
        public PartialViewResult ChartByPupil(int classId, int pupilId)
        {
            #region [Get All PhysicalExamination By Pupil]
            int PartitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);

            var pupilEntity = PupilOfClassBusiness.GetPupilInClass(classId).Where(x => x.PupilID == pupilId).FirstOrDefault();
            if (pupilEntity == null || pupilEntity.Genre == null)
            {
                return null;
            }

            var physicalExaminationEntities = _PhysicalExaminationBusiness.All.Where(p => p.PupilID == pupilId && p.SchoolID == _globalInfo.SchoolID.Value
                && p.LastDigitSchoolID == PartitionID && p.ClassID == classId)
                .Select(x => new ChartByPupilViewModel
                {
                    YearAndMonth = x.MonthID,
                    Height = x.Height,
                    Weight = x.Weight,
                }).ToList();
            for (int i = 0; i < physicalExaminationEntities.Count; i++)
            {
                physicalExaminationEntities[i].PupilBirthday = pupilEntity.Birthday;
            }
            #endregion
            var list_result = new ChartByPupilDataSource()
            {
                PupilId = pupilEntity.PupilID,
                PupilBirthday = pupilEntity.Birthday,
                PupilName = pupilEntity.PupilFullName,
                PupilAddress = pupilEntity.ResidentalAddress,
                Gender = pupilEntity.Genre
            };
            for (int i = 0; i <= numMonthOfBirthDay; i++)
            {
                var item = new ChartByPupilDataSourceDetail()
                {
                    Month = i
                };


                var physicalExaminationEntity = physicalExaminationEntities.Where(x => x.Month == i).FirstOrDefault();
                if (physicalExaminationEntity != null)
                {
                    item.Height = physicalExaminationEntity.Height;
                    item.Weight = physicalExaminationEntity.Weight;
                }
                list_result.DataChart.Add(item);
            }
            return PartialView("_DialogPrintChart", list_result);
        }
        #endregion

    }
}
