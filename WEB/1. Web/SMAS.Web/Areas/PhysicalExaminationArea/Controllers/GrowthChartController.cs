﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Web.Utils;
using System.Text.RegularExpressions;
using System.Drawing;
using Ionic.Zip;
using System.Diagnostics;
using System.Web.Script.Serialization;
using SMAS.Business.Common;
using SMAS.Web.Areas.PhysicalExaminationArea.Models;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.PhysicalExaminationArea.Controllers
{
    /// <summary>
    /// Growth chart controller
    /// </summary>
    /// <auth>HaiVT</auth>
    /// <date>24/04/2013</date>
    [SkipCheckRole]
    public class GrowthChartController : BaseController, IDisposable
    {
        private IMonitoringBookBusiness MonitoringBookBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        private IClassProfileBusiness ClassProfileBusiness;
        private IPhysicalTestBusiness PhysicalTestBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPhysicalExaminationBusiness _PhysicalExaminationBusiness;

        private const int numMonthOfBirthDay = 120;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="monitoringBookBusiness"></param>
        /// <param name="pupilProfileBusiness"></param>
        /// <param name="classProfileBusiness"></param>
        public GrowthChartController
            (
                IMonitoringBookBusiness monitoringBookBusiness,
                IPupilProfileBusiness pupilProfileBusiness,
                IClassProfileBusiness classProfileBusiness,
                IPhysicalTestBusiness physicalTestBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IPhysicalExaminationBusiness physicalExaminationBusiness
            )
        {
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PhysicalTestBusiness = physicalTestBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            _PhysicalExaminationBusiness = physicalExaminationBusiness;
        }

        #region ve bieu do
        [HttpPost]
        public ActionResult Index(int pupilId, int classId)
        {
            if (pupilId <= 0 || classId <= 0)
            {
                return View(new ChartByPupilDataSource());
            }
            return View(GetMonthlyGrowthOfPupil(classId, pupilId));
        }
        #endregion

        #region Draw all class
        /// <summary>
        /// Load du lieu SVG data de xuat hinh cho toan lop
        /// </summary>
        /// <param name="classID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadSvgData(int classId, int pupilID)
        {
            //var cls = ClassProfileBusiness.Find(classId);
            if (pupilID == 0)
            {
                return PartialView("_AllClass", GetMonthlyGrowthOfClass(classId));               
            }
            else
            {
                return PartialView("_AllClass", new List<ChartByPupilDataSource>() { GetMonthlyGrowthOfPupil(classId, pupilID) });
            }
        }

        /// <summary>
        /// Tao file hinh tu CSVG data from client
        /// </summary>
        /// <param name="lstSvg"></param>
        /// <param name="classId"></param>
        /// <param name="lstPP"></param>
        /// <param name="lstMapW"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateSVG(List<string> lstSvg, int? classId, List<int> lstPP, List<int> lstMapW, bool compress = false)
        {
            lstSvg = new JavaScriptSerializer().Deserialize<List<string>>(Request["lstSvg"]);
            lstPP = new JavaScriptSerializer().Deserialize<List<int>>(Request["lstPP"]);
            lstMapW = new JavaScriptSerializer().Deserialize<List<int>>(Request["lstMapW"]);
            ClassProfile cls = ClassProfileBusiness.Find(classId);
            if (cls != null)
            {
                emptyFolder(cls.ClassProfileID);

                string pathFolder = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
                DirectoryInfo dir = new DirectoryInfo(pathFolder);
                if (!dir.Exists) dir.Create();
                string classPath = Path.Combine(pathFolder, classId.ToString());
                dir = new DirectoryInfo(classPath);
                if (!dir.Exists) dir.Create();

                IDictionary<string, object> dic = new Dictionary<string, object>() { 
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"ClassID",classId},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"lstPupilID",lstPP.Distinct().ToList()}
                };

                var listPupil = PupilOfClassBusiness.Search(dic).Select(x => new PupilProfileBO()
                {
                    PupilProfileID = x.PupilProfile.PupilProfileID,
                    PupilCode = x.PupilProfile.PupilCode,
                    FullName = x.PupilProfile.FullName,
                    Genre = x.PupilProfile.Genre,
                    PermanentResidentalAddress = x.PupilProfile.PermanentResidentalAddress,
                    BirthDate = x.PupilProfile.BirthDate
                }).ToList();

                int counter = 0;
                for (int i = 0; i < lstPP.Count; i++)
                {
                    PupilProfileBO pp = listPupil.Where(x => x.PupilProfileID == lstPP[i]).FirstOrDefault();
                    if (pp != null)
                    {
                        string FullName = pp.FullName;
                        string fileName = "";
                        switch (lstMapW[i])
                        {
                            case 1: fileName = string.Format(TEMPLATE_FILE_PNG_WEIGHT, (pp.PupilCode ?? "").ToUpper(), RemoveSpecialCharacters(FullName));
                                break;
                            case 2: fileName = string.Format(TEMPLATE_FILE_PNG_HEIGHT, (pp.PupilCode ?? "").ToUpper(), RemoveSpecialCharacters(FullName));
                                break;
                            case 3: fileName = string.Format(TEMPLATE_FILE_PNG_HEIGHT_2, (pp.PupilCode ?? "").ToUpper(), RemoveSpecialCharacters(FullName));
                                break;
                            case 4: fileName = string.Format(TEMPLATE_FILE_PNG_WEIGHT_2, (pp.PupilCode ?? "").ToUpper(), RemoveSpecialCharacters(FullName));
                                break;
                            case 5: fileName = string.Format(TEMPLATE_FILE_PNG_BMI, (pp.PupilCode ?? "").ToUpper(), RemoveSpecialCharacters(FullName));
                                break;
                        }

                        string svg = string.Empty;
                        //if (compress)
                        //{
                        //    svg = (lstSvg.Count > i) ? Utils.DeJSend.GetData(Utils.DeJSend.DecodeFrom64(lstSvg[i])) : string.Empty;
                        //}
                        //else
                        //{
                           // svg = (lstSvg.Count > i) ? lstSvg[i] : string.Empty;
                        //}
                        svg = (lstSvg.Count > i) ? System.Text.ASCIIEncoding.ASCII.GetString(System.Convert.FromBase64String(lstSvg[i])) : string.Empty;
                        var physicalPath = Path.Combine(classPath, fileName);
                        var file = createFile(svg, fileName, classId.Value, physicalPath, lstMapW[i], pp);
                        if (file) counter++;
                    }
                }
                if (counter > 0)
                {
                    string ZipFolderName = string.Format(TEMPLATE_FILE_ZIP, listPupil.Count > 1 ? RemoveSpecialCharacters(cls.DisplayName) : string.Format("{0}_{1}", RemoveSpecialCharacters(listPupil[0].PupilCode), RemoveSpecialCharacters(listPupil[0].FullName)));

                    ZipFolder(ZipFolderName, classPath, classPath);
                    return Json(new JsonMessage(string.Empty));
                }
            }
            return Json(new JsonMessage("Xuất file không thành công.", "error"));
        }

        /// <summary>
        /// Save file to client
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        public FileResult DowloadZipped(int classId, int pupilId)
        {
            ClassProfile cls = ClassProfileBusiness.Find(classId);
            if (cls == null)
            {
                return null;
            }

            //Check path
            string pathFolder = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
            DirectoryInfo dir = new DirectoryInfo(pathFolder);
            if (!dir.Exists) dir.Create();
            string classPath = Path.Combine(pathFolder, classId.ToString());
            dir = new DirectoryInfo(classPath);
            if (!dir.Exists) dir.Create();
            string ZipFolderName = string.Format(TEMPLATE_FILE_ZIP, RemoveSpecialCharacters(cls.DisplayName));

            if (pupilId > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>() { 
                    {"ClassID",classId},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"PupilProfileID",pupilId}
                };
                var pupilEntity = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).FirstOrDefault();
                if (pupilEntity != null)
                {
                    ZipFolderName = string.Format(TEMPLATE_FILE_ZIP, string.Format("{0}_{1}", RemoveSpecialCharacters(pupilEntity.PupilCode), RemoveSpecialCharacters(pupilEntity.FullName)));
                }
            }
            //End of Check path            
            string ZipPathOnServer = Path.Combine(classPath, ZipFolderName);
            FileInfo fileInf = new FileInfo(ZipPathOnServer);
            if (!fileInf.Exists)
            {
                return null;
            }

            PdfExport pdf = new PdfExport();
            MemoryStream ms = pdf.ConvertZipFileToStream(ZipPathOnServer);
            FileStreamResult result1 = new FileStreamResult(ms, "application/octet-stream");
            // đặt tên file download
            result1.FileDownloadName = ZipFolderName + ".zip";

            //Xoa file duoc luu
            emptyFolder(classId);
            return result1;

        }
        #endregion

        #region Private methods
        /// <summary>
        /// Template file name png height,
        /// [MHS]_[Tenhocsinh]_BieuDoTangTruong_ChieuCao.png
        /// </summary>
        private const string TEMPLATE_FILE_PNG_HEIGHT = "{0}_{1}_BieuDoTangTruong_ChieuCao.png";
        /// <summary>
        /// Template file name png height,
        /// [MHS]_[Tenhocsinh]_BieuDoTangTruong_ChieuCao.png
        /// </summary>
        private const string TEMPLATE_FILE_PNG_HEIGHT_2 = "{0}_{1}_BieuDoTangTruong_ChieuCao_2.png";
        /// <summary>
        /// Template file name png weight,
        /// [MHS]_[Tenhocsinh]_BieuDoTangTruong_CanNang.png
        /// </summary>
        private const string TEMPLATE_FILE_PNG_WEIGHT = "{0}_{1}_BieuDoTangTruong_CanNang.png";
        /// <summary>
        /// Template file name png weight,
        /// [MHS]_[Tenhocsinh]_BieuDoTangTruong_CanNang.png
        /// </summary>
        private const string TEMPLATE_FILE_PNG_WEIGHT_2 = "{0}_{1}_BieuDoTangTruong_CanNang_2.png";
        /// <summary>
        /// Template file name png weight,
        /// [MHS]_[Tenhocsinh]_BieuDoTangTruong_CanNang.png
        /// </summary>
        private const string TEMPLATE_FILE_PNG_BMI = "{0}_{1}_BieuDoTangTruong_BMI.png";
        /// <summary>
        /// Template file name zip,
        /// [Tenlop]_BieuDoTangTruong
        /// </summary>
        private const string TEMPLATE_FILE_ZIP = "{0}_BieuDoTangTruong";
        /// <summary>
        /// SVG Image converter application
        /// </summary>
        private const string INKSCAPE_PATH = @"~/Components/inkscape/inkscape.exe";
        /// <summary>
        /// SVG Background Image
        /// </summary>
        private const string IMG_BG_HEIGHT_BOY = "~/Content/images/bieudochieucaobetrai.jpg";
        /// <summary>
        /// background bieu do can nang nam
        /// </summary>
        private const string IMG_BG_WEIGHT_BOY = "~/Content/images/bieudocannangbetrai.jpg";
        /// <summary>
        /// background bieu do chieu cao nu
        /// </summary>
        private const string IMG_BG_HEIGHT_GIRL = "~/Content/images/bieudochieucaobegai.jpg";
        /// <summary>
        /// background bieu do can nang nu
        /// </summary>
        private const string IMG_BG_WEIGHT_GIRL = "~/Content/images/bieudocannangbegai.jpg";
        /// <summary>
        /// background bieu do can nang 2
        /// </summary>
        private const string IMG_BG_WEIGHT_BOY_2 = "~/Content/images/CanNang_BeTrai.jpg";
        /// <summary>
        /// background bieu do can nang 2
        /// </summary>
        private const string IMG_BG_HEIGHT_BOY_2 = "~/Content/images/ChieuCao_BeTrai.jpg";
        /// <summary>
        /// background bieu do can nang 2
        /// </summary>
        private const string IMG_BG_BMI_BOY = "~/Content/images/BMI_BeTrai.jpg";
        /// <summary>
        /// background bieu do can nang 2
        /// </summary>
        private const string IMG_BG_WEIGHT_GIRL_2 = "~/Content/images/CanNang_BeGai.jpg";
        /// <summary>
        /// background bieu do can nang 2
        /// </summary>
        private const string IMG_BG_HEIGHT_GIRL_2 = "~/Content/images/ChieuCao_BeGai.jpg";
        /// <summary>
        /// background bieu do can nang 2
        /// </summary>
        private const string IMG_BG_BMI_GIRL = "~/Content/images/BMI_BeGai.jpg";
        /// <summary>
        /// pixel margin left cua bieu do chieu cao
        /// </summary>
        private const int HEIGHT_IMG_BG_LEFT_PADDING = 56;//46
        /// <summary>
        /// pixel margin top cua bieu do chieu cao
        /// </summary>
        private const int HEIGHT_IMG_BG_TOP_PADDING = 29;//29
        /// <summary>
        /// pixel margin trai cua bieu do can nang
        /// </summary>
        private const int WEIGHT_IMG_BG_LEFT_PADDING = 56;//46
        /// <summary>
        /// pixel margin tren cua hinh bieu do can nang
        /// </summary>
        private const int WEIGHT_IMG_BG_TOP_PADDING = 21;//16
        /// <summary>
        /// MAP WIDTH
        /// </summary>
        private const string IMAGE_CHART_WITH = "849px";
        /// <summary>
        /// MAP HEIGHT
        /// </summary>
        private const string IMAGE_CHART_HEIGHT = "498px";

        /// <summary>
        /// Tao file hinh
        /// </summary>
        /// <param name="svg"></param>
        /// <param name="fileName"></param>
        /// <param name="classId"></param>
        /// <param name="outImage"></param>
        /// <param name="isMapWeigth"></param>
        /// <param name="gender"></param>
        /// <returns></returns>
        private bool createFile(string svg, string fileName, int classId, string outImage, int isMapWeigth, PupilProfileBO pupilObj)
        {
            try
            {
                string pathFolder = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
                DirectoryInfo dir = new DirectoryInfo(pathFolder);
                if (!dir.Exists) dir.Create();
                string classPath = Path.Combine(pathFolder, classId.ToString() + "_SVG");
                dir = new DirectoryInfo(classPath);
                if (!dir.Exists) dir.Create();
                string file = Path.Combine(classPath, fileName);
                if (svg != string.Empty)
                {
                    var svgText = HttpUtility.UrlDecode(svg);
                    //Save to file
                    var svgFile = file + ".svg";
                    System.IO.File.WriteAllText(svgFile, svgText);
                    //Creaet file
                    //Su dung svg render engine de tao hinh tu file SVG
                    //Mot so format/shape khong fill dung
                    Svg.SvgDocument doc = Svg.SvgDocument.Open(svgFile);
                    if (doc != null)
                    {
                        doc.Draw().Save(outImage);
                    }

                    if (System.IO.File.Exists(outImage))
                    {
                        MakeBackground(pupilObj.Genre, isMapWeigth, outImage, pupilObj);
                    }
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                logger.Error("Loi", ex);
                return false;
            }
        }

        /// <summary>
        /// Tao background
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="mapWeight"></param>
        /// <param name="mapFile"></param>
        private void MakeBackground(int gender, int mapWeight, string mapFile, PupilProfileBO pp)
        {
            int padding_left = HEIGHT_IMG_BG_LEFT_PADDING;
            int padding_top = HEIGHT_IMG_BG_TOP_PADDING;
            string fileBackground = string.Empty;
            if (gender == GlobalConstants.GENRE_MALE)
            {
                //Nam
                switch (mapWeight)
                {
                    case 1: fileBackground = IMG_BG_WEIGHT_BOY;
                        break;
                    case 2: fileBackground = IMG_BG_HEIGHT_BOY;
                        break;
                    case 3: fileBackground = IMG_BG_HEIGHT_BOY_2;
                        break;
                    case 4: fileBackground = IMG_BG_WEIGHT_BOY_2;
                        break;
                    case 5: fileBackground = IMG_BG_BMI_BOY;
                        break;
                }
            }
            else
            {
                switch (mapWeight)
                {
                    case 1: fileBackground = IMG_BG_WEIGHT_GIRL;
                        break;
                    case 2: fileBackground = IMG_BG_HEIGHT_GIRL;
                        break;
                    case 3: fileBackground = IMG_BG_HEIGHT_GIRL_2;
                        break;
                    case 4: fileBackground = IMG_BG_WEIGHT_GIRL_2;
                        break;
                    case 5: fileBackground = IMG_BG_BMI_GIRL;
                        break;
                }
            }
            if (mapWeight == 1)
            {
                //Thong so khi tao background voi dieu do can nang
                padding_left = WEIGHT_IMG_BG_LEFT_PADDING;
                padding_top = WEIGHT_IMG_BG_TOP_PADDING;
            }

            fileBackground = Server.MapPath(fileBackground);
          
            if (System.IO.File.Exists(fileBackground))
            {
                Stream bgStream = System.IO.File.OpenRead(fileBackground);
                Stream mapStream = System.IO.File.OpenRead(mapFile);
                Image mapImg = Image.FromStream(mapStream);
                Image saveImg = Image.FromStream(bgStream);
                //Create background
                Graphics bkImg = Graphics.FromImage(saveImg);
                if (bkImg != null)
                {
                    //bkImg.DrawImage(mapImg, padding_left, padding_top, mapImg.Width, mapImg.Height);
                    int nameWidth = 300;
                    int nameHeight = 60;                    
                    int marginRight = 44;
                    int marginBottom = 59;//56
                    if (mapWeight == 3 || mapWeight == 4 || mapWeight == 5)
                    {
                        nameWidth = nameWidth * 2 + 50;
                        nameHeight = nameHeight * 2 + 50;
                        marginRight = marginRight - 24;
                        marginBottom = marginBottom * 2 + 40;
                    }

                    bool twoLineAddress = false;
                    int maxLengthInline = 40;
                    string addressLine1 = string.Empty;
                    string addressLine2 = string.Empty;
                    if (!string.IsNullOrEmpty(pp.PermanentResidentalAddress) && pp.PermanentResidentalAddress.Length > 80)
                    {
                        pp.PermanentResidentalAddress = pp.PermanentResidentalAddress.Substring(0, 80) + "...";
                    }

                    if (!string.IsNullOrEmpty(pp.PermanentResidentalAddress) && pp.PermanentResidentalAddress.Length > maxLengthInline)
                    {
                        twoLineAddress = true;
                        nameHeight += 25;
                        int tempSpace = 0;
                        for (int i = 0; i < pp.PermanentResidentalAddress.Length; i++)
                        {
                            if (pp.PermanentResidentalAddress[i] == ' ')
                            {
                                if (i <= maxLengthInline)
                                {
                                    tempSpace = i;
                                }
                                else
                                {
                                    addressLine1 = pp.PermanentResidentalAddress.Substring(0, tempSpace + 1);
                                    addressLine2 = pp.PermanentResidentalAddress.Substring(tempSpace, pp.PermanentResidentalAddress.Length - tempSpace);
                                    break;
                                }
                            }
                        }
                    }

                    PointF namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 20, saveImg.Height - marginBottom - nameHeight + 10 - 35);
                    if (mapWeight == 3 || mapWeight == 4 || mapWeight == 5)
                    {
                        // Vẽ khung họ tên
                        bkImg.FillRectangle(System.Drawing.Brushes.White, saveImg.Width - marginRight - nameWidth, saveImg.Height - marginBottom - nameHeight - 35, nameWidth - 10, nameHeight + 35);
                        namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 40, saveImg.Height - marginBottom - nameHeight + 20 - 35);
                        bkImg.DrawString("Họ tên: ", new Font(new FontFamily("Arial"), 18F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                        namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 130, saveImg.Height - marginBottom - nameHeight + 20 - 35);
                        bkImg.DrawString(pp.FullName, new Font(new FontFamily("Arial"), 18F, FontStyle.Bold), Brushes.Black, namePoint);

                        namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 40, saveImg.Height - marginBottom - nameHeight + 70 - 35);
                        bkImg.DrawString("Địa chỉ: ", new Font(new FontFamily("Arial"), 18F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);

                        if (twoLineAddress)
                        {
                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 130, saveImg.Height - marginBottom - nameHeight + 70 - 35);
                            bkImg.DrawString(addressLine1, new Font(new FontFamily("Arial"), 18F, FontStyle.Regular), Brushes.Black, namePoint);

                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 130, saveImg.Height - marginBottom - nameHeight + 120 - 35);
                            bkImg.DrawString(addressLine2, new Font(new FontFamily("Arial"), 18F, FontStyle.Regular), Brushes.Black, namePoint);

                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 40, saveImg.Height - marginBottom - nameHeight + 170 - 35);
                            bkImg.DrawString("Ngày sinh: ", new Font(new FontFamily("Arial"), 18F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 160, saveImg.Height - marginBottom - nameHeight + 170 - 35);
                            bkImg.DrawString(pp.BirthDate.ToString("dd/MM/yyyy"), new Font(new FontFamily("Arial"), 18F, FontStyle.Regular), Brushes.Black, namePoint);
                        }
                        else
                        {
                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 130, saveImg.Height - marginBottom - nameHeight + 70 - 35);
                            bkImg.DrawString(pp.PermanentResidentalAddress, new Font(new FontFamily("Arial"), 18F, FontStyle.Regular), Brushes.Black, namePoint);

                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 40, saveImg.Height - marginBottom - nameHeight + 120 - 35);
                            bkImg.DrawString("Ngày sinh: ", new Font(new FontFamily("Arial"), 18F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 160, saveImg.Height - marginBottom - nameHeight + 120 - 35);
                            bkImg.DrawString(pp.BirthDate.ToString("dd/MM/yyyy"), new Font(new FontFamily("Arial"), 18F, FontStyle.Regular), Brushes.Black, namePoint);
                        }
                    }
                    else 
                    {
                        bkImg.FillRectangle(System.Drawing.Brushes.White, saveImg.Width - marginRight - nameWidth, saveImg.Height - marginBottom - nameHeight - 35, nameWidth, nameHeight + 35);
                        bkImg.DrawString("Họ tên: ", new Font(new FontFamily("Arial"), 12F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                        namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 65, saveImg.Height - marginBottom - nameHeight + 10 - 35);
                        bkImg.DrawString(pp.FullName, new Font(new FontFamily("Arial"), 12F, FontStyle.Bold), Brushes.Black, namePoint);

                        namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 20, saveImg.Height - marginBottom - nameHeight + 35 - 35);
                        bkImg.DrawString("Địa chỉ: ", new Font(new FontFamily("Arial"), 12F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);

                        if (twoLineAddress)
                        {
                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 65, saveImg.Height - marginBottom - nameHeight + 35 - 35);
                            bkImg.DrawString(addressLine1, new Font(new FontFamily("Arial"), 12F, FontStyle.Regular), Brushes.Black, namePoint);

                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 65, saveImg.Height - marginBottom - nameHeight + 60 - 35);
                            bkImg.DrawString(addressLine2, new Font(new FontFamily("Arial"), 12F, FontStyle.Regular), Brushes.Black, namePoint);

                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 20, saveImg.Height - marginBottom - nameHeight + 85 - 35);
                            bkImg.DrawString("Ngày sinh: ", new Font(new FontFamily("Arial"), 12F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 80, saveImg.Height - marginBottom - nameHeight + 85 - 35);
                            bkImg.DrawString(pp.BirthDate.ToString("dd/MM/yyyy"), new Font(new FontFamily("Arial"), 12F, FontStyle.Regular), Brushes.Black, namePoint);
                        }
                        else
                        {
                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 65, saveImg.Height - marginBottom - nameHeight + 35 - 35);
                            bkImg.DrawString(pp.PermanentResidentalAddress, new Font(new FontFamily("Arial"), 12F, FontStyle.Regular), Brushes.Black, namePoint);

                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 20, saveImg.Height - marginBottom - nameHeight + 60 - 35);
                            bkImg.DrawString("Ngày sinh: ", new Font(new FontFamily("Arial"), 12F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                            namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 80, saveImg.Height - marginBottom - nameHeight + 60 - 35);
                            bkImg.DrawString(pp.BirthDate.ToString("dd/MM/yyyy"), new Font(new FontFamily("Arial"), 12F, FontStyle.Regular), Brushes.Black, namePoint);
                        }
                    }

                    if (mapWeight == 4 || mapWeight == 3 || mapWeight == 5)
                    {
                        padding_top = mapImg.Height * 36 / 100;
                        padding_left = mapImg.Width * 18 / 100;
                    }                   
                    bkImg.DrawImage(mapImg, padding_left, padding_top, mapImg.Width, mapImg.Height);
                }
                Stream tmpStream = new System.IO.MemoryStream();
                saveImg.Save(tmpStream, System.Drawing.Imaging.ImageFormat.Png);
                mapStream.Close();
                bgStream.Close();
                mapImg = Image.FromStream(tmpStream);
                mapImg.Save(mapFile);
                //release object
                tmpStream.Close();
                mapImg.Dispose();
                saveImg.Dispose();
                mapStream.Dispose();
                bgStream.Dispose();
                tmpStream.Dispose();
            }
        }

        /// <summary>
        /// Tao folder zipped
        /// </summary>
        /// <param name="ZipFileName"></param>
        /// <param name="ZippedPath"></param>
        /// <param name="savePath"></param>
        private void ZipFolder(string ZipFileName, string ZippedPath, string savePath)
        {
            ZipFile zip = new ZipFile();
            ZipFileName = Path.Combine(savePath, ZipFileName);
            zip.AddDirectory(ZippedPath);
            zip.Save(ZipFileName);
        }

        /// <summary>
        /// Remove Vietnamese
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string RemoveSpecialCharacters(string str)
        {
            str = CommonFunctions.RemoveSign(str);
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }

        /// <summary>
        /// Load data for class
        /// </summary>
        /// <param name="classID"></param>
        private List<ChartByPupilDataSource> GetMonthlyGrowthOfClass(int classId)
        {
            #region [Get All PhysicalExamination By Pupil]
            int PartitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);

            var pupilEntities = PupilOfClassBusiness.GetPupilInClass(classId).ToList();
            if (pupilEntities.Count <= 0)
            {
                return new List<ChartByPupilDataSource>();
            }

            var list_result = new List<ChartByPupilDataSource>();
            for (int i = 0; i < pupilEntities.Count; i++)
            {
                var pupilEntity = pupilEntities[i];
                var physicalExaminationEntities = _PhysicalExaminationBusiness.All.Where(p => p.PupilID == pupilEntity.PupilID && p.SchoolID == _globalInfo.SchoolID.Value
                && p.LastDigitSchoolID == PartitionID && p.ClassID == classId && p.MonthID > 99)
                .Select(x => new ChartByPupilViewModel
                {
                    YearAndMonth = x.MonthID,
                    Height = x.Height,
                    Weight = x.Weight,
                }).ToList();
                for (int j = 0; j < physicalExaminationEntities.Count; j++)
                {
                    physicalExaminationEntities[j].PupilBirthday = pupilEntity.Birthday;
                }
                #endregion
                int day = ((DateTime.Now.Year - pupilEntity.Birthday.Year) * 12) + DateTime.Now.Month - pupilEntity.Birthday.Month;
                var item = new ChartByPupilDataSource()
                {
                    PupilId = pupilEntity.PupilID,
                    PupilBirthday = pupilEntity.Birthday,
                    PupilName = pupilEntity.PupilFullName,
                    PupilAddress = pupilEntity.PermanentResidentalAddress,
                    Gender = pupilEntity.Genre,
                    ClassId = pupilEntity.ClassID,
                    IsBigger60 = day > 60 ? true : false
                };

                for (int k = 0; k <= 84; k++)
                {
                    if (k <= 60)
                    {
                        var itemChild = new ChartByPupilDataSourceDetail()
                        {
                            Month = k
                        };

                        var physicalExaminationEntity = physicalExaminationEntities.Where(x => x.Month == k).FirstOrDefault();
                        if (physicalExaminationEntity != null)
                        {
                            itemChild.Height = physicalExaminationEntity.Height;
                            itemChild.Weight = physicalExaminationEntity.Weight;
                        }
                        item.DataChart.Add(itemChild);
                    }
                    if (k >= 60)
                    {
                        var itemChild = new ChartByPupilDataSourceDetail2()
                        {
                            Month = k
                        };

                        if (k != 60)
                        {
                            var physicalExaminationEntity = physicalExaminationEntities.Where(x => x.Month == k).FirstOrDefault();
                            if (physicalExaminationEntity != null)
                            {
                                itemChild.Height = physicalExaminationEntity.Height;
                                itemChild.Weight = physicalExaminationEntity.Weight;
                            }
                        }
                        item.DataChart2.Add(itemChild);
                    }
                }
                list_result.Add(item);
            }


            return list_result;
        }

        /// <summary>
        /// Load data for class
        /// </summary>
        /// <param name="classID"></param>
        private ChartByPupilDataSource GetMonthlyGrowthOfPupil(int classId, int pupilId)
        {
            #region [Get All PhysicalExamination By Pupil]
            int PartitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);

            var pupilEntity = PupilOfClassBusiness.GetPupilInClass(classId).Where(x => x.PupilID == pupilId).FirstOrDefault();
            if (pupilEntity == null || pupilEntity.Genre == null)
            {
                return new ChartByPupilDataSource();
            }

            var physicalExaminationEntities = _PhysicalExaminationBusiness.All.Where(p => p.PupilID == pupilId && p.SchoolID == _globalInfo.SchoolID.Value
                && p.LastDigitSchoolID == PartitionID && p.ClassID == classId && p.MonthID > 99)
                .Select(x => new ChartByPupilViewModel
                {
                    YearAndMonth = x.MonthID,
                    Height = x.Height,
                    Weight = x.Weight,
                }).ToList();
            for (int i = 0; i < physicalExaminationEntities.Count; i++)
            {
                physicalExaminationEntities[i].PupilBirthday = pupilEntity.Birthday;
            }
            #endregion
            int day = ((DateTime.Now.Year - pupilEntity.Birthday.Year) * 12) + DateTime.Now.Month - pupilEntity.Birthday.Month;
            var list_result = new ChartByPupilDataSource()
            {
                PupilId = pupilEntity.PupilID,
                PupilBirthday = pupilEntity.Birthday,
                PupilName = pupilEntity.PupilFullName,
                PupilAddress = pupilEntity.PermanentResidentalAddress,
                Gender = pupilEntity.Genre,
                ClassId = pupilEntity.ClassID,
                IsBigger60 = day > 60 ? true : false
            };
            for (int i = 0; i <= 84; i++)
            {
                if (i <= 60)
                {
                    var item = new ChartByPupilDataSourceDetail()
                    {
                        Month = i
                    };

                    var physicalExaminationEntity = physicalExaminationEntities.Where(x => x.Month == i).FirstOrDefault();
                    if (physicalExaminationEntity != null)
                    {
                        item.Height = physicalExaminationEntity.Height;
                        item.Weight = physicalExaminationEntity.Weight;
                    }
                    list_result.DataChart.Add(item);
                }
                if (i >= 60)
                {
                    var item = new ChartByPupilDataSourceDetail2()
                    {
                        Month = i
                    };

                    if (i != 60)
                    {
                        var physicalExaminationEntity = physicalExaminationEntities.Where(x => x.Month == i).FirstOrDefault();
                        if (physicalExaminationEntity != null)
                        {
                            item.Height = physicalExaminationEntity.Height;
                            item.Weight = physicalExaminationEntity.Weight;
                        }
                    }
                    list_result.DataChart2.Add(item);
                }
            }

            return list_result;
        }

        private void emptyFolder(int classID)
        {
            ClassProfile cls = ClassProfileBusiness.Find(classID);
            if (cls == null)
            {
                return;
            }
            string pathFolder = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
            string classPath = Path.Combine(pathFolder, classID.ToString());
            DirectoryInfo dir = new DirectoryInfo(classPath);
            if (dir.Exists)
            {
                dir.Delete(true);
            }
            classPath = classPath + "_SVG";
            dir = new DirectoryInfo(classPath);
            if (dir.Exists)
            {
                dir.Delete(true);
            }
        }
        #endregion
    }
}
