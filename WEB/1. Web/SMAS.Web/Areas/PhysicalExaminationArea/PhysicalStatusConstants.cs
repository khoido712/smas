﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Web.Areas.PhysicalExaminationArea
{
    public static class PhysicalStatusConstants
    {

        public static string GetHeights(int? status)
        {
            switch (status)
            {
                case 1:
                    return "Cao hơn so với tuổi";
                case 2:
                    return "Bình thường";
                case 3:
                    return "Thấp còi độ 1";
                case 4:
                    return "Thấp còi độ 2";
                default:
                    return "Chưa nhập dữ liệu";
            }
        }

        public static string GetWeights(int? status)
        {
            switch (status)
            {
                case 1:
                    return "Cân nặng hơn so với tuổi";
                case 2:
                    return "Bình thường";
                case 3:
                    return "Suy dinh dưỡng nhẹ";
                case 4:
                    return "Suy dinh dưỡng nặng";
                default:
                    return "Chưa nhập dữ liệu";
            }
        }

        public static string FormatDecimal(this decimal? value, System.Globalization.CultureInfo customCulture = null, string formatString = "#.##")
        {
            if (customCulture == null)
            {
                customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
                customCulture.NumberFormat.NumberDecimalSeparator = ".";
            }

            return value.HasValue ? value.Value.ToString(formatString, customCulture) : "";
        }

        public static string ValidateStringToDecimal(this string value)
        {
            if(string.IsNullOrEmpty(value)) return value;
            return value.Replace(".", ",");
        }

        public static decimal? ToDecimal(this string value, int decimals = 1)
        {
            decimal _n = 0; decimal.TryParse(value.ValidateStringToDecimal(), out _n);
            if (_n == 0) return null;
            return decimal.Round(_n, decimals);
        }
    }
}
