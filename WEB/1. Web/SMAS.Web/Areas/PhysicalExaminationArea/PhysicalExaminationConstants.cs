﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PhysicalExaminationArea
{
    public class PhysicalExaminationConstants
    {
        public const string LIST_MONTH = "listMonth";
        public const string CLASS_NULL = "classnull";
        public const string ACCOUNT_ID = "account_user";
        public const string IS_ADMIN = "Quản trị trường";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string ClassID = "ClassID";
        public const string TYPE_SELECT_ID = "TypeSelectID";
        public const string MONTH_SELECT_ID = "MonthSelectID";
        public const string DAY_SELECT_ID = "DaySelectID";
        public const string ShowList = "ShowList";
        public const string OLD_SUBJECT_ID = "OldSubjectID";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string IS_VISIBLED_BUTTON = "IsVisibledButton";
        public const string HEAD_TEACHE = "HeadTeacher";
        public const string GVBM_TEACHER = "GVBMTeacher";
        public const string GVCN_TEACHER = "GVCNTeacher";
        public const string LAST_UPDATE = "LAST_UPDATE";
        public const string LIST_RESULT = "ListResult";
        public const string HTML_RESULT = "HTMLResult";
        public const string LIST_PUPIL = "ListPupil";
        public const string TEMPLATE_FILE = "SoGhiNhanXet.xls";
        public const string FROM_DATE = "FromDate";
        public const string TO_DATE = "ToDate";
        public const string PUPIL_ID = "PupilID";
        public const string LatestModified_Name = "LatestModified_Name";
        public const string LatestModified_Date = "LatestModified_Date";


        // Growth chart
        public const string LIST_CLASS_Chart = "lst class by permission";
        public const string LIST_PUPIL_CHART = "lst pupil of class";
        public const string LIST_CHART = "lst monitoring book";
        public const string LIST_CHART_CLASS = "lst monitoring book of class";
        public const string CLASSID_SELECT = "classID select";
        public const string FULL_NAME = "nam of pupil";
        public const string BIRTH_DAY = "birthday of pupil";
        public const string GENRE = "genre of pupil";
        public const string CLASS_NAME = "class of pupil";
        public const string CLASS_ID = "classid of pupil";
        public const string HISTORY_YOUR_SELF = "history yourseft";
        public const string PUPIL_ID_Chart = "id of pupil";
        public const string LIST_MONTH_Chart = "lst 12 month";
        public const string LIST_EDUCATION_LEVEL = "List of levelid";
        public const string FIRST_LEVEL_SELECTED = "First of level";

        
    }
}