﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PhysicalExaminationArea
{
    public class PhysicalExaminationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PhysicalExaminationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PhysicalExaminationArea_default",
                "PhysicalExaminationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
