﻿using SMAS.Web.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PhysicalExaminationArea.Models
{
    public class ChartByPupilViewModel
    {
        public DateTime PupilBirthday { get; set; }
        public int YearAndMonth { get; set; }
        public int Month
        {
            get
            {
                try
                {
                    int _year = int.Parse(this.YearAndMonth.ToString().Substring(0, 4));
                    int _month = int.Parse(this.YearAndMonth.ToString().Substring(4, 2));
                    int result = (_month - this.PupilBirthday.Month) + (12 * (_year - this.PupilBirthday.Year));
                    return result < 0 ? 0 : result;
                }
                catch
                {
                    return 0;
                }
            }
        }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
    }

    public class ChartByPupilDataSourceDetail
    {
        public int Month { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
    }

    public class ChartByPupilDataSourceDetail2
    {
        public int Month { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public double? BMI {
            get {
                if (Height == null || Weight == null || Height == 0)
                    return null;
                return Math.Round(Convert.ToDouble(Weight / ((Height / 100) * (Height / 100))), 2);
            }          
       }
    }

    public class ChartByPupilDataSource
    {
        public ChartByPupilDataSource()
        {
            DataChart = new List<ChartByPupilDataSourceDetail>();
            DataChart2 = new List<ChartByPupilDataSourceDetail2>();
        }

        public int PupilId { get; set; }
        public bool IsBigger60 { get; set; }
        public string PupilName { get; set; }
        public string PupilAddress { get; set; }
        public DateTime PupilBirthday { get; set; }
        public int? Gender { get; set; }
        public int ClassId { get; set; }
        public bool IsMale
        {
            get
            {
                switch (this.Gender)
                {
                    case GlobalConstants.GENRE_FEMALE:
                        {
                            return false;
                        }
                    case GlobalConstants.GENRE_MALE:
                        {
                            return true;
                        }
                    default:
                        return false;
                }
            }
        }
        public string GenderName
        {
            get
            {
                switch (this.Gender)
                {
                    case GlobalConstants.GENRE_FEMALE:
                        {
                            return GlobalConstants.GENRE_FEMALE_TITLE;
                        }
                    case GlobalConstants.GENRE_MALE:
                        {
                            return GlobalConstants.GENRE_MALE_TITLE;
                        }
                    default:
                        return GlobalConstants.GENRE_MALE_TITLE;
                }
            }
        }
        public List<ChartByPupilDataSourceDetail> DataChart { get; set; }
        public List<ChartByPupilDataSourceDetail2> DataChart2 { get; set; }
    }
}