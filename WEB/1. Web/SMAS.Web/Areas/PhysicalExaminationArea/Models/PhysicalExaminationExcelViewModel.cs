﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PhysicalExaminationArea.Models
{
    public class PhysicalExaminationExcelViewModel
    {
        public int PhysicalExaminationId { get; set; }
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public DateTime BirthDay { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public int? Gender { get; set; }
    }
}