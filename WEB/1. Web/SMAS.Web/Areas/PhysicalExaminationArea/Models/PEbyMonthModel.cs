﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PhysicalExaminationArea.Models
{
    public class PEbyMonthModel
    {
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public int? PhyExID { get; set; }
        public int MonthID { get; set; }
        public decimal? Weight { get; set; }
        public string MessageWeight { get; set; }
        public decimal? Height { get; set; }
        public string MessageHeight { get; set; }
    }
}