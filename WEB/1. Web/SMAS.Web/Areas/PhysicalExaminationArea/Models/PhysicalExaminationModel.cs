﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PhysicalExaminationArea.Models
{
    public class PhysicalExaminationModel
    {
        public int? PhysicalExaminationID { get; set; }
        public int MonthID { get; set; }
        public decimal? Weight { get; set; }
        public string MessageHeight { get; set; }
        public string MessageWeight { get; set; }
        public string MessageStatusWeight
        {
            get
            {
                return PhysicalStatusConstants.GetWeights(PhysicalWeightStatus);
            }
        }
        public decimal? Height { get; set; }
        public string MessageStatusHeight
        {
            get
            {
                return PhysicalStatusConstants.GetHeights(PhysicalHeightStatus);
            }
        }
        public int? PhysicalHeightStatus { get; set; }
        public int? PhysicalWeightStatus { get; set; }
        public int Month
        {
            get
            {
                if (this.MonthID.ToString().Length < 6)
                {
                    return this.MonthID;
                }
                return int.Parse(this.MonthID.ToString().Substring(4, 2));
            }
        }
        public int Year
        {
            get
            {
                if (this.MonthID.ToString().Length < 6)
                {
                    return 0;
                }
                return int.Parse(this.MonthID.ToString().Substring(0, 4));
            }
        }
    }
}