﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PhysicalExaminationArea.Models
{
    public class ClassProfileViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }

    public class EducationLevelViewModel
    {
        public EducationLevelViewModel() {
            ClassProfiles = new List<ClassProfileViewModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Expand { get; set; }

        public IList<ClassProfileViewModel> ClassProfiles { get; set; }
    }
}