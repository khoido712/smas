﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PhysicalExaminationArea.Models
{
    public class PhysicalExaminationViewModel
    {
        public int PhysicalExaminationID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public DateTime Birthday { get; set; }
        public int? Gender { get; set; }
        public int Status { get; set; }      
        public int LogID { get; set; }
        public List<PhysicalExaminationModel> listPE { get; set; }
        public string LastDigitSchoolID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}