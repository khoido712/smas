﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMenuDirectionArea
{
    public class ReportMenuDirectionConstant
    {
        public const string CBO_SEMESTER = "CboSemeseter";
        public const string CBO_PERIOD = "CboPeriod";
        public const string CBO_PERIOD_SELECTED = "CboPeriodSelected";
        public const string CBO_EDUCATIONLEVEL = "CBoEducationLevel";
        public const string CBO_ISCOMMENTING = "CboCommenting";
        public const string CBO_SUBJECT = "CboSubject";
        public const string List_SUBJECT = "List_SUBJECT";
        public const string LIST_RADIOREPORT = "ListRadioReport";
        public const string LIST_RADIOREPORTSEMESTER = "ListRadioReportSemester";
        public const string DEFAULT_SEMESTER = "DEFAULT_SEMESTER";
        public const string DEFAULT_STATSICTIC_LEVEL = "DEFAULT_STATSICTIC_LEVEL";
        public const string CBO_STATISTIC_LEVEL = "CBO_STATISTIC_LEVEL";
        public const string CBO_MARK_TYPE = "listMarkType";
        public const string CBO_MARK_COLUMN = "listMarkColumn";
        public const string CBO_EMPLOYEE = "cboEmployee";
        public const string CHECK_DEFAULT = "CheckDefault";
        public const string SUBJECT_REPORT = "SubjectReportID";
        public const string LIST_EDUCATIONLEVEL = "listEducation";
        public const string CBO_FACULTY = "CboFaculty";
        public const string CBO_MATH = "CboMath";
        public const string LS_SEMESTER = "lstSemester";
        public const string DF_SEMESTER = "defaultSemester";
        public const string LS_EDUCATION_LEVEL = "lstEducationLevel";
        public const string LS_CLASS = "lstClass";
        public const string LS_SUBJECT = "lstSubject";
        public const string LS_TEACHER = "lstTeacher";
        public const string ACADEMICYEAR = "Academic_Year";
        public const string DISABLED_PERIOD = "DisabledPeriod";
        public const int MARK_TYPE_PERIODIC = 1; //Kiểm tra định kỳ
        public const int MARK_TYPE_AVERAGE = 2; //Trung bình môn
        public const string TITLE_CREATED = "Người lập báo cáo";
        public const string TITLE_SUMMARY = "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng";
        #region Emis
        public const string YEAR = "YEAR";
        public const string StatusReport = "StatusReport";
        public const string MESSAGE_ERROR = "MessageError";
        public const int Primary = 1;
        public const int Secondary = 2;
        public const int Tertiary = 3;
        public const int Tertiary_End = 4;
        public const string EDUCATION_GRADE = "Education_Grade";
        public const int TrainTypeID = 3;//truong GDTX
        public const int Primary_Secondary = 3;
        public const int Secondary_Teriary = 6;
        public const int Primary_Secondary_Teriary = 7;
        public const int All_Grade = 31;
        public const string REPORT_PERIOD = "Report Period";

        public const int FIRST_PERIOD = 1;
        public const int SECOND_PERIOD = 1;
        public const int END_PERIOD = 1;

        public const string M_REPORT_CODE_PRIMARY = "HoSo_TieuHoc_T12";
        public const string M_REPORT_CODE_SECONDARY = "HoSo_THCS_T12";
        public const string M_REPORT_CODE_SECONDARY_TERIARY = "HoSo_PTTH_T12";
        public const string M_REPORT_CODE_TERIARY = "HoSo_THPT_T12";
        public const string M_REPORT_CODE_PRIMARY_SECONDARY = "HoSo_PTCS_T12";
        public const string M_REPORT_CODE_PST = "HoSo_PT_T12";
        public const string E_TTGDTX_CODE_MID = "HoSo_TTGDTX_T12";

        public const string E_REPORT_CODE_PRIMARY = "HoSo_TH_T5";
        public const string E_REPORT_CODE_SECONDARY = "HoSo_THCS_T5";
        public const string E_REPORT_CODE_SECONDARY_TERIARY = "HoSo_PTTH_T5";
        public const string E_REPORT_CODE_TERIARY = "HoSo_THPT_T5";
        public const string E_REPORT_CODE_PRIMARY_SECONDARY = "HoSo_PTCS_T5";
        public const string E_REPORT_CODE_PST = "HoSo_PT_T5";
        public const string E_TTGDTX_CODE = "HoSo_TTGDTX_T5";

        public const string E_REPORT_CODE_PRIMARY_F = "HoSo_TH_T9";
        public const string E_REPORT_CODE_SECONDARY_F = "HoSo_THCS_T9";
        public const string E_REPORT_CODE_SECONDARY_TERIARY_F = "HoSo_PTTH_T9";
        public const string E_REPORT_CODE_TERIARY_F = "HoSo_THPT_T9";
        public const string E_REPORT_CODE_PRIMARY_SECONDARY_F = "HoSo_PTCS_T9";
        public const string E_REPORT_CODE_PST_F = "HoSo_PT_T9";
        public const string E_TTGDTX_CODE_F = "HoSo_TTGDTX_T9";




        /////////////Tên các sheet cấn fill dữ liêu cấp1 ////////////////////////
        public const string SCHOOL_INFO_PRIMARY = "Truong";
        public const string CLASS_PRIMARY = "LopHoc_TH";
        public const string PUPIL_PRIMARY = "HocSinh_TH";
        public const string PUPIL_ETHNIC_PRIMARY = "HocSinh_TH_DT";
        public const string TEACHER_PRIMARY = "NhanSu_TH";
        public const string PUPIL_RATE_PRIMARY = "DanhGiaHS_TH";
        public const string TEACHER_RATE_PRIMARY = "DanhGia_CBGVNV";
        public const string TEACHER_RATE_PRIMARY_END = "DanhGia_CBGVNV";
        public const string TEACHER_PRIMARY_FIRST = "NhanSu_TH";
        public const string COSOVC_TH = "CoSoVC_TH";
        public const string PUPIL_PRIMARY_ADDITION = "HocSinh_THBS";
        public const string PUPIL_RATE_PRIMARY_ADDITION = "DanhGiaHS_THBS";
        public const string TAECHER_PRIMARY_ADDITION = "NhanSu_THBS";
        public const string PUPIL_POLICY_TARGET_PRIMARY = "HocSinh_CS_TH";

        //////////Tên các sheet cần fill dữ liệu cấp 2////////////////
        public const string SCHOOL_INFO_SECONDARY = "Truong";
        public const string CLASS_SECONDARY = "LopHoc_THCS";
        public const string PUPIL_SECONDARY = "HocSinh_THCS";
        public const string PUPIL_ETHNIC_SECONDARY = "HocSinh_THCS_DT";
        public const string TEACHER_SECONDARY = "NhanSu_THCS";
        public const string PUPIL_RATE_SECONDARY = "DanhGiaHS_THCS";
        public const string SUBJECT_RATE_SECONDARY = "DanhGiaMH_THCS";
        public const string SUBJECT_RATE_SECONDARY_ADDITION = "DanhGiaMH_THCSBS";
        public const string TEACHER_RATE_SECONDARY = "DanhGia_CBGVNV";
        public const string TEACHER_RATE_SECONDARY_END = "DanhGia_CBGVNV";
        public const string CLASS_SECONDARY_ADDITION = "LopHoc_THCSBS";
        public const string PUPIL_SECONDARY_ADDITION = "HocSinh_THCSBS";
        public const string COSOVC_THCS = "CoSoVC_THCS";
        public const string PUPIL_RATE_SECONDARY_ADDITION = "DanhGiaHS_THCSBS";
        public const string TEACHER_SECONDARY_ADDITION = "NhanSu_THCSBS";
        public const string PUPIL_POLICY_TARGET_SECONDARY = "HocSinh_CS_THCS";

        //////////Tên các sheet cần fill dữ liệu cấp 3////////////////
        public const string SCHOOL_INFO_TERIARY = "Truong";
        public const string CLASS_TERIARY = "LopHoc_THPT";
        public const string PUPIL_TERIARY = "HocSinh_THPT";
        public const string PUPIL_ETHNIC_TERIARY = "HocSinh_THPT_DT";
        public const string TEACHER_TERIARY = "NhanSu_THPT";
        public const string PUPIL_RATE_TERIARY = "DanhGiaHS_THPT";
        public const string PUPIL_RATE_TERIARY_ADDITION = "DanhGiaHS_THPTBS";
        public const string SUBJECT_RATE_TERIARY = "DanhGiaMH_THPT";
        public const string SUBJECT_RATE_TERIARY_ADDITION = "DanhGiaMH_THPTBS";
        public const string TEACHER_RATE_TERIARY = "DanhGia_CBGVNV";
        public const string COSOVC_THPT = "CoSoVC_THPT";
        public const string TEACHER_RATE_TERIARY_END = "DanhGia_CBGVNV";
        public const string PUPIL_TERIARY_ADDITION = "HocSinh_THPTBS";
        public const string TEACHER_TERIARY_ADDITION = "NhanSu_THPTBS";
        public const string PUPIL_POLICY_TARGET_TERIARY = "HocSinh_CS_THPT";

        ////////////Tên các sheet cần fill dữ liệu cấp 2,3//////////////////
        public const string SCHOOL_INFO_SECONDARY_TERIARY = "Truong";
        public const string CLASS_SECONDARY_TERIARY = "LopHoc_PTTH";
        public const string TEACHER_SECONDARY_TERIARY = "NhanSu_PTTH";
        public const string TEACHER_RATE_SECONDARY_TERIARY = "DanhGia_CBGVNV";
        public const string TEACHER_RATE_SECONDARY_TERIARY_END = "DanhGia_CBGVNV";
        public const string CLASS_SECONDARY_TERIARY_ADDITION = "LopHoc_THCSBS";
        public const string PUPIL_ST_ADDITION = "HocSinh_PTTHBS";
        public const string TEACHER_SECONDARY_TERIARY_ADDITION = "NhanSu_PTTHBS";
        public const string COSOVC_PTTH = "CoSoVC_PTTH";

        /////////////Tên các sheet cân fill dữ liệu cấp 1,2 //////////////////
        public const string SCHOOL_INFO_PRIMARY_SECONDARY = "Truong";
        public const string CLASS_PRIMARY_SECONDARY = "LopHoc_PTCS";
        public const string TEACHER_PRIMARY_SECONDARY = "NhanSu_PTCS";
        public const string TEACHER_RATE_PRIMARY_SECONDARY = "DanhGia_CBGVNV";
        public const string TEACHER_RATE_PRIMARY_SECONDARY_END = "DanhGia_CBGVNV";
        public const string CLASS_PRIMARY_SECONDARY_ADDITION = "LopHoc_THCSBS";
        public const string PUPIL_PS_ADDITION = "HocSinh_PTCSBS";
        public const string TEACHER_PRIMARY_SECONDARY_ADDITION = "NhanSu_PTCSBS";
        public const string COSOVC_PTCS = "CoSoVC_PTCS";

        //////////////Tên các sheet cần fill dữ liệu trường 3 cấp 1,2,3////////////////
        public const string SCHOOL_INFO_PST = "Truong";
        public const string CLASS_PST = "LopHoc_PT";
        public const string TEACHER_PST = "NhanSu_PT";
        public const string CLASS_PST_ADDITION = "LopHoc_THCSBS";
        public const string PUPIL_PST_ADDITION = "HocSinh_PTBS";
        public const string TEACHER_PST_ADDITION = "NhanSu_PTBS";
        public const string TEACHER_RATE_PST = "DanhGia_CBGVNV";
        public const string TEACHER_RATE_PST_END = "DanhGia_CBGVNV";
        public const string COSOVC_PT = "CoSoVC_PT";

        //////////////Tên các sheet cần fill dữ liệu trường GDTX/////////////////
        public const string SCHOOL_INFO_GDTX = "Trungtam";
        public const string CLASS_GDTX = "Lophoc_TTGDTX";
        public const string PUPIL_GDTX = "Hocvien_TTGDTX";
        public const string TRAINING_PUPIL_GDTX = "Daotao_GDTX";
        public const string PUPIL_RATE_SECONDARY_GDTX = "DanhgiaHV_THCS";
        public const string PUPIL_RATE_TERIARY_GDTX = "DanhgiaHV_THPT";
        public const string PUPIL_RATE_ORTHER_GDTX = "DanhgiaHV_KHAC";
        public const string TEACHER_RATE_GDTX = "Danhgia_CBGVNV";
        public const string TEACHER_TTGDTX = "Nhansu_TTGDTX";
        public const string TRAINING_GDTX = "Daotao_GDTX";
        public const string COSOVC_TTGDTX = "CoSoVC_TTGDTX";
        public const string TEACHER_TTGDTX_ADDITION = "Nhansu_TTGDTXBS";

        public const string REPORTTIME = "ReportTime";

        //csvc
        public const string FACILITIES_RRIMARY = "CoSoVC_TH";
        public const string FACILITIES_SECONDARY = "CoSoVC_THCS";
        public const string FACILITIES_TERTIARY = "CoSoVC_THPT";
        public const string FACILITIES_PS = "CoSoVC_PTCS";
        public const string FACILITIES_ST = "CoSoVC_PTTH";
        public const string FACILITIES_PST = "CoSoVC_PT";
        public const string FACILITIES_GDTX = "CosoVC_TTGDTX";

        #endregion
        #region Emis gửi phòng sở
        public const string LIST_EDUCATION_LEVEL = "ListEducationLevel";
        public const string LIST_SEMESTER = "ListSemester";
        public const string LIST_SEMESTER_ALL = "ListSemesterAll";
        public const string LIST_SUBCOMMITTEE = "ListSubCommittee";

        public const string LIST_BELOW_AVERAGE = "ListBelowAverage";
        public const string LIST_ABOVE_AVERAGE = "ListAboveAverage";

        public const string LIST_JUDGE = "ListJudge";
        public const string LIST_MARK = "ListMark";

        public const string LIST_CONDUCT_LEVEL = "ListConductLevel";
        public const string LIST_CAPACITY_LEVEL = "ListCapacityLevel";

        public const string GRID_STATISTICS_PERIOD = "GridStatisticsPeriod";
        public const string GRID_STATISTICS_SEMESTER = "GridStatisticsSemester";
        public const string GRID_STATISTICS_CAPACITY_SUBJECT = "GridStatisticsCapacitySubject";
        public const string GRID_STATISTICS_CONDUCT = "GridStatisticsConduct";
        public const string GRID_STATISTICS_CAPACITY = "GridStatisticsCapacity";
        public const string COUNT_DATA = "CountData";
        #endregion

        //bao cao tot nghiep
        public const string GRADUATION_THCS = "Bao_Cao_TN_THCS";
        public const string GRADUATION_THPT = "Bao_Cao_TN_THPT";
        public const string GRADUATION_THCSTHPT = "Bao_Cao_TN_THCSTHPT";
        public const string SCHOOL_INFO = "Truong";
        public const string TN_THCS = "TN_THCS";
        public const string TN_THPT = "TN_THPT";
        public const int GRADUATION_REPORT = 4;


        #region BAO CAO QD5363
        public const string QD5363_CODE_C1 = "TKGD_TH_T9";
        public const string QD5363_CODE_C2 = "TKGD_THCS_T9";
        public const string QD5363_CODE_C3 = "TKGD_THPT_T9";

        public const string QD5363_C1_SHEET1 = "06-C1-D(1)";
        public const string QD5363_C1_SHEET2 = "06-C1-D(2)";
        public const string QD5363_C1_SHEET3 = "06-C1-D(3)";
        public const string QD5363_C1_SHEET4 = "DoiTuongCS";

        public const string QD5363_C2_SHEET1 = "08-C2-D(1)";
        public const string QD5363_C2_SHEET2 = "08-C2-D(2)";
        public const string QD5363_C2_SHEET3 = "08-C2-D(3)";
        public const string QD5363_C2_SHEET4 = "DoiTuongCS";

        public const string QD5363_C3_SHEET1 = "10-C3-D(1)";
        public const string QD5363_C3_SHEET2 = "10-C3-D(2)";
        public const string QD5363_C3_SHEET3 = "10-C3-D(3)";
        public const string QD5363_C3_SHEET4 = "DoiTuongCS";
        #endregion

        #region Bao cao Bo hoc
        public const string BAOCAO_BOHOC_SHEET1 = "Bo hoc 2 hoc ky";
        public const string BAOCAO_BOHOC_RESULT_CODE = "BCBH_T9";
        #endregion
    }
}