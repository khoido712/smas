﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    public class HeadTeacherReportController : BaseController
    {
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IReportEmployeeProfileBusiness ReportEmployeeProfileBusiness;
        public HeadTeacherReportController(IProcessedReportBusiness processedReportBusiness, IClassProfileBusiness classProfileBusiness,
            ISchoolSubjectBusiness schoolSubjectBusiness, IReportDefinitionBusiness reportDefinitionBusiness,IReportEmployeeProfileBusiness reportEmployeeProfileBusiness)
        {
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ReportEmployeeProfileBusiness = reportEmployeeProfileBusiness;
        }
        public ActionResult Index()
        {
            ViewData[ReportMenuDirectionConstant.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            EducationLevel educationLevel = _globalInfo.EducationLevels.FirstOrDefault();
            return View();
        }
        [ValidateAntiForgeryToken]
        public JsonResult GetHeadTeacherReport(FormCollection frm)
        {
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_GVCN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",EducationLevelID}
            };
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportEmployeeProfileBusiness.GetReportHeadTeacherProcess(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportEmployeeProfileBusiness.CreateHeadTeacherReport(dic);
                processedReport = ReportEmployeeProfileBusiness.InsertHeadTeacherProcessReport(dic, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        [ValidateAntiForgeryToken]
        public JsonResult GetNewHeadTeacherReport(FormCollection frm)
        {
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",EducationLevelID}
            };
            Stream excel = ReportEmployeeProfileBusiness.CreateHeadTeacherReport(dic);
            ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertHeadTeacherProcessReport(dic, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadReportPdf(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}