﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.ReportMenuDirectionArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class LearningCapacityReportController:BaseController
    {
        public readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        public readonly IProcessedReportBusiness ProcessedReportBusiness;
        public readonly IStatisticLevelReportBusiness StatisticLevelReportBusiness;
        public readonly ISynthesisCapacityConductBusiness SynthesisCapacityConductBusiness;
        public readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        public readonly IClassifiedCapacityBusiness ClassifiedCapacityBusiness;
        public readonly IClassifiedConductBusiness ClassifiedConductBusiness;
        public readonly IReportResultEndOfYearBusiness ReportResultEndOfYearBusiness;
        public readonly IEmulationTitleBusiness EmulationTitleBusiness;
        private readonly IReportSituationOfViolationBusiness ReportSituationOfViolationBusiness;
        public readonly IClassifiedCapacityByPeriodBusiness ClassifiedCapacityByPeriodBusiness;
        public readonly IClassifiedConductByPeriodBusiness ClassifiedConductByPeriodBusiness;
        private readonly IReportSynthesisCapacityConductByPeriodBusiness ReportSynthesisCapacityConductByPeriodBusiness;
        public LearningCapacityReportController(IAcademicYearBusiness academicYearBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness,
            IProcessedReportBusiness processedReportBusiness, IStatisticLevelReportBusiness statisticLevelReportBusiness,
            ISynthesisCapacityConductBusiness SynthesisCapacityConductBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
            IClassifiedCapacityBusiness classifiedcapacityBusiness, IClassifiedConductBusiness ClassifiedConductBusiness,
            IReportResultEndOfYearBusiness ReportResultEndOfYearBusiness, IEmulationTitleBusiness EmulationTitleBusiness,
            IReportSituationOfViolationBusiness situationOfViolationBusiness, IClassifiedCapacityByPeriodBusiness ClassifiedCapacityByPeriodBusiness,
            IClassifiedConductByPeriodBusiness ClassifiedConductByPeriodBusiness, IReportSynthesisCapacityConductByPeriodBusiness ReportSynthesisCapacityConductByPeriodBusiness)
        {
            this.AcademicYearBusiness = academicYearBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.StatisticLevelReportBusiness = statisticLevelReportBusiness;
            this.SynthesisCapacityConductBusiness = SynthesisCapacityConductBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ClassifiedCapacityBusiness = classifiedcapacityBusiness;
            this.ClassifiedConductBusiness = ClassifiedConductBusiness;
            this.ReportResultEndOfYearBusiness = ReportResultEndOfYearBusiness;
            this.EmulationTitleBusiness = EmulationTitleBusiness;
            this.ReportSituationOfViolationBusiness = situationOfViolationBusiness;
            this.ClassifiedCapacityByPeriodBusiness = ClassifiedCapacityByPeriodBusiness;
            this.ClassifiedConductByPeriodBusiness = ClassifiedConductByPeriodBusiness;
            this.ReportSynthesisCapacityConductByPeriodBusiness = ReportSynthesisCapacityConductByPeriodBusiness;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _index()
        {
            return PartialView();
        }
        public PartialViewResult LoadCapacityReport()//Thống kê xếp loại học lực
        {
            this.SetViewData();
            return PartialView("_ClassifiedCapacity");
        }
        public PartialViewResult LoadConductReport()//Thống kê xép loại hạnh kiểm
        {
            this.SetViewData();
            return PartialView("_ClassifiedConduct");
        }
        public PartialViewResult LoadConductAndCapacityReport()//Thống kê xép loại học lực-hạnh kiểm
        {
            this.SetViewData();
            return PartialView("_ClassifiedConductAndCapacity");
        }
        public PartialViewResult LoadEmulationReport()//Thống kê danh hiệu thi đua
        {
            this.SetViewData();
            return PartialView("_Emulation");
        }
        public PartialViewResult LoadResultEndOfYearReport()//Thống kê học sinh lên lớp ở lại
        {
            this.SetViewData();
            return PartialView("_ReportResultEndOfYear");
        }
        public PartialViewResult LoadSituationOfViolationReport()//Thống kê vi phạm các lớp
        {
            this.SetViewData();
            return PartialView("_SituationOfViolation");
        }
        private void SetViewData()
        {
            //cbb hoc ky
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            int Semester = DateTime.Now < acaYear.SecondSemesterStartDate.Value
                                ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                : (DateTime.Now < acaYear.SecondSemesterEndDate.Value
                                        ? SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        : SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
            ViewData[ReportMenuDirectionConstant.DEFAULT_SEMESTER] = Semester;

            ViewData[ReportMenuDirectionConstant.CBO_SEMESTER] = CommonList.SemesterAndAll();
            ViewData[ReportMenuDirectionConstant.CBO_PERIOD] = new List<PeriodDeclaration>();

            // Neu co thong tin ve hoc ky thi lay luon thong tin ve dot
            if (_globalInfo.Semester.HasValue)
            {
                IDictionary<string, object> PeriodSearchInfo = new Dictionary<string, object>();
                PeriodSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                PeriodSearchInfo["Semester"] = _globalInfo.Semester.Value;
                List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, PeriodSearchInfo).ToList();
                int a = 0;
                if (lstPeriod != null && lstPeriod.Count() > 0)
                {
                    a = 1;
                    for (int k = 0; k < lstPeriod.Count; k++)
                    {

                        PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                        periodDeclaration = lstPeriod[k];
                        if (periodDeclaration.FromDate < DateTime.Now && periodDeclaration.EndDate > DateTime.Now)
                        {
                            a = k;
                        }
                    }
                }
                ViewData[ReportMenuDirectionConstant.CHECK_DEFAULT] = a;
                ViewData[ReportMenuDirectionConstant.CBO_PERIOD] = new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", a);
                //lấy ra mức thống kê
                List<StatisticLevelReport> lstStatisticLevel = StatisticLevelReportBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } }).ToList();
                lstStatisticLevel = lstStatisticLevel.OrderBy(o => o.Resolution).ToList();
                if (lstStatisticLevel == null)
                {
                    lstStatisticLevel = new List<StatisticLevelReport>();
                }
                ViewData[ReportMenuDirectionConstant.CBO_STATISTIC_LEVEL] = lstStatisticLevel;

                var lstEducation = _globalInfo.EducationLevels;
                ViewData[ReportMenuDirectionConstant.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPeriod(int? idSemester)
        {
            if (idSemester == null)
            {
                return Json(new SelectList(new List<PeriodDeclaration>(), "PeriodDeclarationID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            if (idSemester == 0)
            {
                idSemester = 3;
            }
            //-	cboPeriod: Lấy ra danh sách các đợt bằng cách gọi hàm PeriodDeclarationBusiness.SearchBySemester() với các tham số truyền vào:
            //+ AcadmicYearID: UserInfo.AcademicYear
            //+ SchoolID: UserInfo.SchoolID
            //+ SemesterID: cboSemester.SelectedValue
            //Kết quả thu được lstPeriodBySemester. Giá trị mặc định: Kiểm tra ngày hiện tại với ngày bắt đầu và ngày kết thúc của đợt để mặc định đợt hiện tại.

            //cbo Periord
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = _globalInfo.AcademicYearID;
            Dictionary["SchoolID"] = _globalInfo.SchoolID;
            Dictionary["Semester"] = idSemester;

            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySemester(Dictionary);
            int periodSelected = 0;
            for (int i = 0; i < lstPeriod.Count; i++)
            {
                periodSelected = 1;
                PeriodDeclaration pd = lstPeriod[i];
                if (pd.FromDate.HasValue && pd.EndDate.HasValue)
                {
                    if (pd.FromDate < DateTime.Now && pd.EndDate > DateTime.Now)
                    {
                        periodSelected = pd.PeriodDeclarationID;
                        break;
                    }
                }
            }
            return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", periodSelected), JsonRequestBehavior.AllowGet);

        }
        [ValidateAntiForgeryToken]
        public JsonResult GetReportBySemester(SubjectReportViewModel form)
        {
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            #region xuất excel theo đợt
            if (form.chkPeriod == 1)
            {
                #region Thống kê xếp loại học lực
                if (form.rdoReportBySemester == 1)
                {
                    type = JsonReportMessage.NEW;
                    TranscriptOfClass entity = new TranscriptOfClass();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.PeriodDeclarationID = form.Period.HasValue ? form.Period.Value : 0;
                    entity.Semester = form.Semester.Value;

                    processedReport = ClassifiedCapacityByPeriodBusiness.GetClassifiedCapacityByPeriod(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = ClassifiedCapacityByPeriodBusiness.CreateClassifiedCapacityByPeriod(entity);
                        processedReport = ClassifiedCapacityByPeriodBusiness.InsertClassifiedCapacityByPeriod(entity, excel);
                        excel.Close();
                    }
                }
                #endregion
                #region Thống kê xếp loại hạnh kiểm
                else if (form.rdoReportBySemester == 2)
                {
                    type = JsonReportMessage.NEW;
                    ClassifiedConductByPeriod entity = new ClassifiedConductByPeriod();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.PeriodID = form.Period.Value;

                    processedReport = ClassifiedConductByPeriodBusiness.GetClassifiedConductByPeriod(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = ClassifiedConductByPeriodBusiness.CreateClassifiedConductByPeriod(entity);
                        processedReport = ClassifiedConductByPeriodBusiness.InsertClassifiedConductByPeriod(entity, excel);
                        excel.Close();
                    }
                }
                #endregion
                #region Thống kê xếp loại học lực - hạnh kiểm
                else if (form.rdoReportBySemester == 3)
                {
                    TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
                    TranscriptOfClass.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    TranscriptOfClass.SchoolID = _globalInfo.SchoolID.Value;
                    TranscriptOfClass.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    TranscriptOfClass.PeriodDeclarationID = form.Period.Value;
                    TranscriptOfClass.Semester = form.Semester.Value;
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM_THEO_DOT; ;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = this.ReportSynthesisCapacityConductByPeriodBusiness.GetSynthesisCapacityConductByPeriod(TranscriptOfClass);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = this.ReportSynthesisCapacityConductByPeriodBusiness.CreateSynthesisCapacityConductByPeriod(TranscriptOfClass);
                        processedReport = this.ReportSynthesisCapacityConductByPeriodBusiness.InsertSynthesisCapacityConductByPeriod(TranscriptOfClass, excel);
                        excel.Close();

                    }
                }
                #endregion
            }
            #endregion
            #region xuất excel theo kỳ
            else
            {
                #region Thống kê xếp loại học lực
                if (form.rdoReportBySemester == 1)
                {
                    ClassifiedCapacityBO ClassifiedCapacityBO = new ClassifiedCapacityBO();
                    if (form.Semester.HasValue)
                    {
                        ClassifiedCapacityBO.Semester = form.Semester.Value;
                    }
                    ClassifiedCapacityBO.SchoolID = _globalInfo.SchoolID.Value;
                    ClassifiedCapacityBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    ClassifiedCapacityBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        ClassifiedCapacityBO.FemaleChecked = true;
                    }
                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        ClassifiedCapacityBO.EthnicChecked = true;
                    }
                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        ClassifiedCapacityBO.FemaleEthnicChecked = true;
                    }

                    string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_HOC_LUC; ;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = this.ClassifiedCapacityBusiness.GetClassifiedCapacity(ClassifiedCapacityBO);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = this.ClassifiedCapacityBusiness.CreateClassifiedCapacity(ClassifiedCapacityBO);
                        processedReport = this.ClassifiedCapacityBusiness.InsertClassifiedCapacity(ClassifiedCapacityBO, excel);
                        excel.Close();

                    }
                }
                #endregion
                #region Thống kê xếp loại hạnh kiểm
                else if (form.rdoReportBySemester == 2)
                {
                    ClassifiedConduct entity = new ClassifiedConduct();
                    entity.Semester = form.Semester.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        entity.FemaleChecked = true;
                    }
                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        entity.EthnicChecked = true;
                    }
                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        entity.FemaleEthnicChecked = true;
                    }

                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HANH_KIEM;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                        processedReport = ClassifiedConductBusiness.GetClassifiedConduct(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = ClassifiedConductBusiness.CreateClassifiedConduct(entity);
                        processedReport = ClassifiedConductBusiness.InsertClassifiedConduct(entity, excel);
                        excel.Close();
                    }
                }
                #endregion
                #region thống kê tổng hợp học lực hạnh kiểm
                else if (form.rdoReportBySemester == 3)
                {
                    SynthesisCapacityConduct entity = new SynthesisCapacityConduct();
                    entity.Semester = form.Semester.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = SynthesisCapacityConductBusiness.GetSynthesisCapacityConduct(entity);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = SynthesisCapacityConductBusiness.CreateSynthesisCapacityConduct(entity);
                        processedReport = SynthesisCapacityConductBusiness.InsertSynthesisCapacityConduct(entity, excel);
                        excel.Close();
                    }
                }
                #endregion
                #region Thống kê danh hiệu thi đua
                else if (form.rdoReportBySemester == 4)
                {
                    EmulationTitle entity = new EmulationTitle();
                    entity.Semester = form.Semester.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    entity.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? int.Parse(form.FemaleEthnicType) : 0;
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_DANH_HIEU_THI_DUA;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = EmulationTitleBusiness.GetEmulationTitle(entity);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = EmulationTitleBusiness.CreateEmulationTitle(entity);
                        processedReport = EmulationTitleBusiness.InsertEmulationTitle(entity, excel);
                        excel.Close();
                    }
                }
                #endregion
                #region Thống kê học sinh lên lớp - ở lại
                else if (form.rdoReportBySemester == 5)
                {
                    TranscriptOfClass ResultEndOfYear = new TranscriptOfClass();
                    ResultEndOfYear.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    ResultEndOfYear.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    ResultEndOfYear.SchoolID = _globalInfo.SchoolID.Value;
                    ResultEndOfYear.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                    ResultEndOfYear.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    ResultEndOfYear.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? Int32.Parse(form.FemaleEthnicType) : 0;
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_KET_QUA_CUOI_NAM; ;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = this.ReportResultEndOfYearBusiness.GetReportResultEndOfYear(ResultEndOfYear);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = this.ReportResultEndOfYearBusiness.CreateReportResultEndOfYear(ResultEndOfYear);
                        processedReport = this.ReportResultEndOfYearBusiness.InsertResultEndOfYear(ResultEndOfYear, excel);
                        excel.Close();

                    }
                }
                #endregion
                #region Thống kê vi phạm các lớp
                else if (form.rdoReportBySemester == 6)
                {
                    ReportSituationOfViolationBO objReportSituationOfViolationBO = new ReportSituationOfViolationBO();
                    objReportSituationOfViolationBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                    objReportSituationOfViolationBO.SchoolID = _globalInfo.SchoolID.Value;
                    objReportSituationOfViolationBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    objReportSituationOfViolationBO.FromDate = form.FromDate;
                    objReportSituationOfViolationBO.ToDate = form.ToDate;
                    objReportSituationOfViolationBO.EducationLevelID = form.EducationLevel.Value;
                    ReportDefinition reportDefinition = ReportSituationOfViolationBusiness.GetReportDefinitionOfPupilFaultByClass(objReportSituationOfViolationBO);
                    if (reportDefinition.IsPreprocessed == true)
                    {
                        processedReport = ReportSituationOfViolationBusiness.ExcelGetPupilFaultByClass(objReportSituationOfViolationBO);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFaultByClass(objReportSituationOfViolationBO);
                        processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFaultByClass(objReportSituationOfViolationBO, excel);
                        excel.Close();
                    }
                    return Json(new JsonReportMessage(processedReport, type));
                }
                #endregion
            }
            #endregion
            return Json(new JsonReportMessage(processedReport, type));
        }
        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySemester(SubjectReportViewModel form)
        {
            ProcessedReport processedReport = null;
            #region xuất excel theo đợt
            if (form.chkPeriod == 1)
            {
                #region Thống kê xếp loại học lực
                if (form.rdoReportBySemester == 1)
                {
                    TranscriptOfClass entity = new TranscriptOfClass();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.PeriodDeclarationID = form.Period.Value;
                    entity.Semester = form.Semester.Value;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? Int32.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? Int32.Parse(form.EthnicType) : 0;
                    Stream excel = ClassifiedCapacityByPeriodBusiness.CreateClassifiedCapacityByPeriod(entity);
                    processedReport = ClassifiedCapacityByPeriodBusiness.InsertClassifiedCapacityByPeriod(entity, excel);
                    excel.Close();
                }
                #endregion
                #region Thống kê xếp loại hạnh kiểm
                else if (form.rdoReportBySemester == 2)
                {
                    ClassifiedConductByPeriod entity = new ClassifiedConductByPeriod();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.PeriodID = form.Period.Value;
                    entity.SemesterID = form.Semester.Value;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? Int32.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? Int32.Parse(form.EthnicType) : 0;
                    processedReport = ClassifiedConductByPeriodBusiness.GetClassifiedConductByPeriod(entity);
                    Stream excel = ClassifiedConductByPeriodBusiness.CreateClassifiedConductByPeriod(entity);
                    processedReport = ClassifiedConductByPeriodBusiness.InsertClassifiedConductByPeriod(entity, excel);
                    excel.Close();
                }
                #endregion
                #region Thống kê xếp loại học lực - hạnh kiểm
                else if (form.rdoReportBySemester == 3)
                {
                    TranscriptOfClass TranscriptOfClass = new TranscriptOfClass();
                    TranscriptOfClass.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    TranscriptOfClass.SchoolID = _globalInfo.SchoolID.Value;
                    TranscriptOfClass.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    TranscriptOfClass.PeriodDeclarationID = form.Period.Value;
                    TranscriptOfClass.Semester = form.Semester.Value;
                    Stream excel = this.ReportSynthesisCapacityConductByPeriodBusiness.CreateSynthesisCapacityConductByPeriod(TranscriptOfClass);
                    processedReport = this.ReportSynthesisCapacityConductByPeriodBusiness.InsertSynthesisCapacityConductByPeriod(TranscriptOfClass, excel);
                    excel.Close();
                }
                #endregion
            }
            #endregion
            #region xuất excel theo kỳ
            else
            {
                #region Thống kê xếp loại học lực
                if (form.rdoReportBySemester == 1)
                {
                    ClassifiedCapacityBO ClassifiedCapacityBO = new ClassifiedCapacityBO();
                    if (form.Semester.HasValue)
                    {
                        ClassifiedCapacityBO.Semester = form.Semester.Value;
                    }
                    ClassifiedCapacityBO.SchoolID = _globalInfo.SchoolID.Value;
                    ClassifiedCapacityBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    ClassifiedCapacityBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        ClassifiedCapacityBO.FemaleChecked = true;
                    }
                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        ClassifiedCapacityBO.EthnicChecked = true;
                    }
                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        ClassifiedCapacityBO.FemaleEthnicChecked = true;
                    }
                    Stream excel = this.ClassifiedCapacityBusiness.CreateClassifiedCapacity(ClassifiedCapacityBO);
                    processedReport = this.ClassifiedCapacityBusiness.InsertClassifiedCapacity(ClassifiedCapacityBO, excel);
                    excel.Close();
                }
                #endregion
                #region Thống kê xếp loại hạnh kiểm
                else if (form.rdoReportBySemester == 2)
                {
                    ClassifiedConduct entity = new ClassifiedConduct();
                    entity.Semester = form.Semester.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        entity.FemaleChecked = true;
                    }
                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        entity.EthnicChecked = true;
                    }
                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        entity.FemaleEthnicChecked = true;
                    }

                    Stream excel = ClassifiedConductBusiness.CreateClassifiedConduct(entity);
                    processedReport = ClassifiedConductBusiness.InsertClassifiedConduct(entity, excel);
                    excel.Close();
                }
                #endregion
                #region thống kê tổng hợp học lực hạnh kiểm
                else if (form.rdoReportBySemester == 3)
                {
                    SynthesisCapacityConduct entity = new SynthesisCapacityConduct();
                    entity.Semester = form.Semester.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? Int32.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? Int32.Parse(form.EthnicType) : 0;

                    Stream excel = SynthesisCapacityConductBusiness.CreateSynthesisCapacityConduct(entity);
                    processedReport = SynthesisCapacityConductBusiness.InsertSynthesisCapacityConduct(entity, excel);
                    excel.Close();
                }
                #endregion
                #region Thống kê danh hiệu thi đua
                else if (form.rdoReportBySemester == 4)
                {
                    EmulationTitle entity = new EmulationTitle();
                    entity.Semester = form.Semester.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    entity.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? int.Parse(form.FemaleEthnicType) : 0;

                    Stream excel = EmulationTitleBusiness.CreateEmulationTitle(entity);
                    processedReport = EmulationTitleBusiness.InsertEmulationTitle(entity, excel);
                    excel.Close();
                }
                #endregion
                #region Thống kê học sinh lên lớp - ở lại
                else if (form.rdoReportBySemester == 5)
                {
                    TranscriptOfClass ResultEndOfYear = new TranscriptOfClass();
                    ResultEndOfYear.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    ResultEndOfYear.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    ResultEndOfYear.SchoolID = _globalInfo.SchoolID.Value;
                    ResultEndOfYear.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? Int32.Parse(form.FemaleType) : 0;
                    ResultEndOfYear.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? Int32.Parse(form.EthnicType) : 0;
                    ResultEndOfYear.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? Int32.Parse(form.FemaleEthnicType) : 0;
                    Stream excel = ReportResultEndOfYearBusiness.CreateReportResultEndOfYear(ResultEndOfYear);
                    processedReport = ReportResultEndOfYearBusiness.InsertResultEndOfYear(ResultEndOfYear, excel);
                    excel.Close();
                }
                #endregion
                #region Thống kê vi phạm các lớp
                else if (form.rdoReportBySemester == 6)
                {
                    ReportSituationOfViolationBO objReportSituationOfViolationBO = new ReportSituationOfViolationBO();
                    objReportSituationOfViolationBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objReportSituationOfViolationBO.SchoolID = _globalInfo.SchoolID.Value;
                    objReportSituationOfViolationBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    objReportSituationOfViolationBO.FromDate = form.FromDate;
                    objReportSituationOfViolationBO.ToDate = form.ToDate;
                    objReportSituationOfViolationBO.EducationLevelID = form.EducationLevel.Value;
                    Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFaultByClass(objReportSituationOfViolationBO);
                    processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFaultByClass(objReportSituationOfViolationBO, excel);
                    excel.Close();
                    return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                }
                #endregion
            }
            #endregion
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}