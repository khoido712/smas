﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportMenuDirectionArea.Models;
using SMAS.Web.Areas.ReportMenuDirectionArea;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class SubjectStatisticPrimaryReportController:BaseController
    {
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IReportMarkRecordBusiness ReportMarkRecordBusiness;
        private readonly IReportPupilRankingBusiness ReportPupilRankingBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportSituationOfViolationBusiness ReportSituationOfViolationBusiness;
        public SubjectStatisticPrimaryReportController(ISchoolSubjectBusiness schoolSubjectBusiness, IAcademicYearBusiness AcademicYearBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness, IReportMarkRecordBusiness reportMarkRecordBusiness,
            IReportPupilRankingBusiness reportPupilRankingBusiness, IProcessedReportBusiness processedReportBusiness,
            IReportSituationOfViolationBusiness situationOfViolationBusiness)
        {
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ReportMarkRecordBusiness = reportMarkRecordBusiness;
            this.ReportPupilRankingBusiness = reportPupilRankingBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ReportSituationOfViolationBusiness = situationOfViolationBusiness;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _index()
        {
            return PartialView();
        }
        public PartialViewResult LoadMarkSemesterReport()
        {
            this.SetViewData();
            return PartialView("_MarkSemesterReport");
        }
        public PartialViewResult LoadFinalResuiltReport()
        {
            ViewData[ReportMenuDirectionConstant.LS_SEMESTER] = CommonList.SemesterPrimary();
            return PartialView("_FinalResuiltReport");
        }
        public PartialViewResult LoadSituationOfViolationReport()
        {
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[ReportMenuDirectionConstant.LS_EDUCATION_LEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution"); 
            return PartialView("_SituationOfViolationReport");
        }
        private void SetViewData()
        {
            ViewData[ReportMenuDirectionConstant.LS_EDUCATION_LEVEL] = _globalInfo.EducationLevels;
            int selectedSemester = 0;
            //Danh sách môn học
            List<SubjectCatBO> lstSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { 
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"AppliedLevel", _globalInfo.AppliedLevel},            
            })
                .Select(o => new SubjectCatBO
                {
                    SubjectCatID = o.SubjectID,
                    DisplayName = o.SubjectCat.DisplayName,
                    OrderInSubject = o.SubjectCat.OrderInSubject
                })
                .Distinct()
                .OrderBy(x => x.OrderInSubject)
                .ThenBy(x => x.DisplayName)
                .ToList();
            ViewData[ReportMenuDirectionConstant.LS_SUBJECT] = lstSubject.ToList();
            //Danh sách học kỳ
            ViewData[ReportMenuDirectionConstant.LS_SEMESTER] = CommonList.SemesterPrimary();

            var academicYear = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            if (DateTime.Now >= academicYear.FirstSemesterStartDate && DateTime.Now <= academicYear.FirstSemesterEndDate)
            {
                selectedSemester = 1;
            }
            if (DateTime.Now >= academicYear.SecondSemesterStartDate && DateTime.Now <= academicYear.SecondSemesterEndDate)
            {
                selectedSemester = 2;
            }

            ViewData[ReportMenuDirectionConstant.DEFAULT_SEMESTER] = selectedSemester;
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SubjectPrimaryReportViewModel view)
        {
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMoveHistory = UtilsBusiness.IsMoveHistory(academicYearObj);
            
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            if (view.ReportType == 1)//Thống kê điểm thi cuối kỳ
            {
                SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                new Dictionary<string, object>(){
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SubjectID",view.SubjectID},
                }).FirstOrDefault();

                if (schoolSubject == null)
                {
                    throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
                }
                if (schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                {
                    ReportMarkRecordSearchBO bo = new ReportMarkRecordSearchBO();
                    bo.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                    bo.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                    bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                    bo.Semester = view.Semester;
                    bo.SubjectID = schoolSubject.SubjectID;
                    bo.IsCheckedEthnic = view.EthnicType > 0 ? true : false;
                    bo.IsCheckedFemale = view.FemaleType > 0 ? true : false;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = ReportMarkRecordBusiness.GetReportMarkRecordPrimary(bo);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = ReportMarkRecordBusiness.CreateReportMarkRecordPrimaryTT30(bo, isMoveHistory);
                        processedReport = ReportMarkRecordBusiness.InsertReportMarkRecordPrimary(bo, excel);
                        excel.Close();
                    }
                }
                else
                {
                    throw new BusinessException("Subject_Label_Err");
                }
            }
            else if(view.ReportType == 2)//Thống kê kết quả cuối năm
            {
                ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
                bo.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                bo.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                bo.FemaleID = view.FemaleType;
                bo.EthnicID = view.EthnicType;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ReportPupilRankingBusiness.GetReportFinalResultPrimaryTT30(bo);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ReportPupilRankingBusiness.CreateReportFinalResultPrimaryTT30(bo);
                    processedReport = ReportPupilRankingBusiness.InsertReportFinalResultPrimaryTT30(bo, excel);
                    excel.Close();
                }
            }
            else if (view.ReportType == 3)//Thống kê vi phạm các lớp
            {
                ReportSituationOfViolationBO reportSituationOfViolationBO = new ReportSituationOfViolationBO();
                reportSituationOfViolationBO.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                reportSituationOfViolationBO.SchoolID = _globalInfo.SchoolID.Value;
                reportSituationOfViolationBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                reportSituationOfViolationBO.FromDate = view.FromDate;
                reportSituationOfViolationBO.ToDate = view.Todate;
                reportSituationOfViolationBO.EducationLevelID = view.EducationLevelID;
                ReportDefinition reportDefinition = ReportSituationOfViolationBusiness.GetReportDefinitionOfPupilFault(reportSituationOfViolationBO);

                if (reportDefinition.IsPreprocessed == true)
                {
                    processedReport = ReportSituationOfViolationBusiness.ExcelGetPupilFault(reportSituationOfViolationBO);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFaultByClass(reportSituationOfViolationBO);
                    processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFaultByClass(reportSituationOfViolationBO, excel);
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, type));
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SubjectPrimaryReportViewModel view)
        {
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMoveHistory = UtilsBusiness.IsMoveHistory(academicYearObj);
            ProcessedReport processedReport = null;
            if (view.ReportType == 1)//Thống kê điểm thi cuối kỳ
            {
                SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                new Dictionary<string, object>(){
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SubjectID",view.SubjectID},
                }).FirstOrDefault();

                if (schoolSubject == null)
                {
                    throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
                }
                if (schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                {
                    ReportMarkRecordSearchBO bo = new ReportMarkRecordSearchBO();
                    bo.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                    bo.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                    bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                    bo.Semester = view.Semester;
                    bo.SubjectID = schoolSubject.SubjectID;
                    bo.IsCheckedEthnic = view.EthnicType > 0 ? true : false;
                    bo.IsCheckedFemale = view.FemaleType > 0 ? true : false;
                    bo.IsCheckedFemaleEthnic = view.FemaleEthnicType > 0 ? true : false;
                    Stream excel = ReportMarkRecordBusiness.CreateReportMarkRecordPrimaryTT30(bo, isMoveHistory);
                    processedReport = ReportMarkRecordBusiness.InsertReportMarkRecordPrimary(bo, excel);
                    excel.Close();
                }
                else
                {
                    throw new BusinessException("Subject_Label_Err");
                }
            }
            else if (view.ReportType == 2)//Thống kê kết quả cuối năm
            {
                ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
                bo.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                bo.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                bo.Semester = view.Semester;
                bo.FemaleID = view.FemaleType;
                bo.EthnicID = view.EthnicType;
                bo.FemaleEthnicID = view.FemaleEthnicType;
                Stream excel = ReportPupilRankingBusiness.CreateReportFinalResultPrimaryTT30(bo);
                processedReport = ReportPupilRankingBusiness.InsertReportFinalResultPrimaryTT30(bo, excel);
                excel.Close();
            }
            else if (view.ReportType == 3)//Thống kê vi phạm các lớp
            {
                ReportSituationOfViolationBO reportSituationOfViolationBO = new ReportSituationOfViolationBO();
                reportSituationOfViolationBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                reportSituationOfViolationBO.SchoolID = _globalInfo.SchoolID.Value;
                reportSituationOfViolationBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                reportSituationOfViolationBO.FromDate = view.FromDate;
                reportSituationOfViolationBO.ToDate = view.Todate;
                reportSituationOfViolationBO.EducationLevelID = view.EducationLevelID;
                Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFaultByClass(reportSituationOfViolationBO);
                processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFaultByClass(reportSituationOfViolationBO, excel);
                excel.Close();
            }
            
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30,
                SystemParamsInFile.REPORT_THONG_KE_VI_PHAM

            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30,
                SystemParamsInFile.REPORT_THONG_KE_VI_PHAM

            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}