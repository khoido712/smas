﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Excel.ExportXML;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    public partial class EmisReportController
    {
        /// <summary>
        /// sử dụng cho báo cáo tiểu học
        /// </summary>
        const string TEMPLATE_NAME = "BaoCao_TieuHoc_Emis_T9.xlsx";
        private const string RESULT_REPORT_CODE = "TKTH_T9";
        private Dictionary<int, string> PrimaryReportSheetMapping = new Dictionary<int, string>() { 
        { 1, "Truong" }, { 2, "Nhansu" }, { 4, "DTrLopHS" }, { 5, "LopHS" }, { 6, "CSVC" } 
        };

        /// <summary>
        /// Tải view báo cáo tiểu học
        /// </summary>
        /// <returns></returns>
        /// 

        //thuonglv3 
        private void SetViewDataPrimarySchoolEmisReport()
        {
            int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
            if (EducationGrade <= 7)
            {
                int StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS, 1);
                int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
                int appliedlevel = _globalInfo.AppliedLevel.Value; // cap truong
                ViewData[ReportMenuDirectionConstant.EDUCATION_GRADE] = appliedlevel;
                ViewData[ReportMenuDirectionConstant.YEAR] = Year + "-" + (Year + 1);
                ViewData[ReportMenuDirectionConstant.StatusReport] = StatusReport;
                ViewData[ReportMenuDirectionConstant.REPORT_PERIOD] = ReportMenuDirectionConstant.FIRST_PERIOD;
            }
            else
            {
                ViewData[ReportMenuDirectionConstant.MESSAGE_ERROR] = ReportMenuDirectionConstant.MESSAGE_ERROR;
            }
        }

        public PartialViewResult LoadPrimarySchoolEmisReport()
        {
            this.SetViewDataPrimarySchoolEmisReport();
            return PartialView("_PrimarySchoolEmisReport");
        }

        /// <summary>
        /// Xuất excel
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public FileResult ExportExcelOfPrimarySchoolReport(FormCollection col)
        {
            int ReportTime = Convert.ToInt32(col["ReportTime"]);
            //Get status report from EMIS REQUEST
            string reportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS_PS;
            int StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value,
                _globalInfo.AcademicYearID.Value, reportCode, ReportTime);

            int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
            if (StatusReport == 0 || StatusReport == -1 || EducationGrade > 7)
            {
                throw new BusinessException("Báo cáo chưa được tổng hợp");
            }

            string ExportTemplateName = "BaoCao_TieuHoc_Emis_T9.xlsx";

            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Truong", TEMPLATE_NAME);
            int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;

            IXVTWorkbook oBook = XVTExportExcel.OpenWorkbook(templatePath);

            // ghi dữ liệu không có thứ tự
            IDictionary<string, object> data;
            foreach (KeyValuePair<int, string> entry in PrimaryReportSheetMapping)
            {
                data = ProcessedCellDataBusiness.GetListReportEmisAsDic(_globalInfo.SchoolID.Value, Year, RESULT_REPORT_CODE, entry.Value);
                if (data != null && data.Any())
                {
                    oBook.GetSheet(entry.Key).Binding(data);
                }
            }

            //thuonglv3
            // ghi dữ liệu điểm trường phụ
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["IsActive"] = true;
            List<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryBusiness.Search(dic).ToList();
            if (lsSchoolSubsidiary.Count > 0)
            {
                IXVTWorksheet sheet = oBook.GetSheet(1);
                IXVTWorksheet sheetDTrCSVC = oBook.GetSheet(3);
                IXVTWorksheet sheetDtrLopHs = oBook.GetSheet(4);
                string resYear = "";
                if (Year > 0)
                {
                    resYear = Year.ToString() + " - " + (Year + 1).ToString();
                }

                sheet.SetCellValue("A1", "HỒ SƠ TRƯỜNG TIỂU HỌC ĐẦU NĂM             Năm học: " + resYear);

                int firstRow = 11;
                int firstRowDTrCSVC = 6;
                int DtrLopHs = 7;
                decimal? totalSquare = 0;
                for (int i = 0; i < lsSchoolSubsidiary.Count; i++)
                {
                    SchoolSubsidiary subSchool = lsSchoolSubsidiary[i];
                    string subSchoolName = subSchool.SubsidiaryName;
                    string phoneNum = subSchool.Telephone;
                    decimal? distance = subSchool.DistanceFromHQ;
                    decimal? square = subSchool.SquareArea;
                    string address = subSchool.Address;

                    //set value sheet Truong
                    sheet.SetCellValue("A" + firstRow.ToString(), (i + 1).ToString());
                    sheet.SetCellValue("B" + firstRow.ToString(), subSchoolName);
                    sheet.SetCellValue("D" + firstRow.ToString(), address);
                    sheet.SetCellValue("F" + firstRow.ToString(), distance != null ? distance.ToString() : String.Empty);
                    sheet.SetCellValue("G" + firstRow.ToString(), phoneNum);
                    firstRow++;

                    //setvalue sheet DtrCSVC
                    totalSquare = totalSquare + square;
                    sheetDTrCSVC.SetCellValue("A" + firstRowDTrCSVC.ToString(), (i + 1).ToString());
                    sheetDTrCSVC.SetCellValue("B" + firstRowDTrCSVC.ToString(), subSchoolName);
                    sheetDTrCSVC.SetCellValue("C" + firstRowDTrCSVC.ToString(), square != null ? square.ToString() : String.Empty);

                    firstRowDTrCSVC++;

                    //setvalue sheet DtrLopHs
                    sheetDtrLopHs.SetCellValue("A" + DtrLopHs.ToString(), (i + 1).ToString());
                    sheetDtrLopHs.SetCellValue("B" + DtrLopHs.ToString(), subSchoolName);

                    DtrLopHs++;
                }

                sheetDTrCSVC.SetCellValue("C5", totalSquare != null ? totalSquare.ToString() : String.Empty);
            }

            Stream excel = oBook.ToStream();
            return File(excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ExportTemplateName);
        }


        /// <summary>
        /// Gửi yêu cầu tạo dữ liệu
        /// </summary>
        /// <param name="ReportTime"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SyntheticPrimarySchoolReport(string ReportTime)
        {
            int period = 0;
            if (!string.IsNullOrEmpty(ReportTime))
            {
                period = Convert.ToInt32(ReportTime);
            }
            int StatusReport = 0;
            int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
            string reportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS_PS;//_PS
            ProcessedCellDataBusiness.InsertOrUpdateEmisRequest(
                _globalInfo.SchoolID.Value,
                _globalInfo.AcademicYearID.Value,
                Year,
                period,
                _globalInfo.TrainingType.Value,
                _globalInfo.DistrictID.Value,
                _globalInfo.ProvinceID.Value,
                reportCode
            );
            return Json(new { StatusReport = StatusReport });
        }


        /// <summary>
        /// Kiểm tra xem báo cáo đã tính toán xong chưa
        /// </summary>
        /// <param name="ReportTime"></param>
        /// <returns></returns>
        public JsonResult CheckSyntheticPrimarySchoolReport(string ReportTime)
        {
            int period = 0;
            if (!string.IsNullOrEmpty(ReportTime))
            {
                period = Convert.ToInt32(ReportTime);
            }
            int StatusReport = 0;
            string reportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS_PS;//_PS;
            //int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
            StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode, period);
            return Json(new { StatusReport = StatusReport });
        }
    }
}