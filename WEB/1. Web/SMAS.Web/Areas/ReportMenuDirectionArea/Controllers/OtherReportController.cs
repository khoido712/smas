﻿using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportMenuDirectionArea;
using SMAS.Web.Areas.ReportMenuDirectionArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    public class OtherReportController:BaseController
    {
        private readonly IHealthTestReportBusiness HealthTestReportBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public OtherReportController(IHealthTestReportBusiness HealthTestReportBusiness,
                                               IAcademicYearBusiness AcademicYearBusiness,
                                               IProcessedReportBusiness processedreportBusiness,
                                               IReportDefinitionBusiness reportdefinitionBusiness)
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.HealthTestReportBusiness = HealthTestReportBusiness;
            this.ProcessedReportBusiness = processedreportBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
        }
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult LoadHealthTestReport()
        {
            this.SetViewData();
            return PartialView("_HealthTestReport");
        }
        public PartialViewResult LoadHealthTestEyeReport()
        {
            this.SetViewData();
            return PartialView("_HealthTestEye");
        }
        private void SetViewData()
        {
            // load combobox EducationLevel
            ViewData[ReportMenuDirectionConstant.LIST_EDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");


            //load combobox Period
            List<ComboObject> lstPeriod = new List<ComboObject>();
            lstPeriod.Add(new ComboObject("1", SMAS.Web.Constants.GlobalConstants.PERIOD_EXAMINATION_1));
            lstPeriod.Add(new ComboObject("2", SMAS.Web.Constants.GlobalConstants.PERIOD_EXAMINATION_2));
            ViewData[ReportMenuDirectionConstant.CBO_PERIOD] = new SelectList(lstPeriod, "key", "value", 1);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(HealthTestReportViewModel form)
        {

            ExcelHealthTestReportBO ExcelHealthTestReportBO = new ExcelHealthTestReportBO();

            ExcelHealthTestReportBO.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            ExcelHealthTestReportBO.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            ExcelHealthTestReportBO.EducationLevelID = form.EducationLevelID == null ? 0 : form.EducationLevelID.Value;
            ExcelHealthTestReportBO.HealthPeriod = form.HealthPeriod.Value;
            ExcelHealthTestReportBO.SchoolID = new GlobalInfo().SchoolID.Value;


            string reportCode = "";

            if (form.ReportType == 1)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_TRUONG;
            }

            if (form.ReportType == 2)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_MAT;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = HealthTestReportBusiness.GetHealthTestReportForSchool(ExcelHealthTestReportBO, form.ReportType);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = HealthTestReportBusiness.CreateHealthTestReportForSchool(ExcelHealthTestReportBO, form.ReportType);
                processedReport = HealthTestReportBusiness.InsertHealthTestReportForSchool(ExcelHealthTestReportBO, excel, form.ReportType);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(HealthTestReportViewModel form)
        {

            ExcelHealthTestReportBO ExcelHealthTestReportBO = new ExcelHealthTestReportBO();

            ExcelHealthTestReportBO.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            ExcelHealthTestReportBO.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            ExcelHealthTestReportBO.EducationLevelID = form.EducationLevelID == null ? 0 : form.EducationLevelID.Value;
            ExcelHealthTestReportBO.HealthPeriod = form.HealthPeriod.Value;
            ExcelHealthTestReportBO.SchoolID = new GlobalInfo().SchoolID.Value;


            Stream excel = HealthTestReportBusiness.CreateHealthTestReportForSchool(ExcelHealthTestReportBO, form.ReportType);
            ProcessedReport processedReport = HealthTestReportBusiness.InsertHealthTestReportForSchool(ExcelHealthTestReportBO, excel, form.ReportType);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}