﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportMenuDirectionArea;
using SMAS.Web.Areas.ReportMenuDirectionArea.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    public class EmployeeReportController:BaseController
    {
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IReportEmployeeProfileBusiness ReportEmployeeProfileBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportTeachingAssignmentBusiness ReportTeachingAssignmentBusiness;
        public EmployeeReportController(ISchoolFacultyBusiness SchoolFacultyBusiness, IClassProfileBusiness classProfileBusiness,
                                        ISchoolSubjectBusiness SchoolSubjectBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
                                        IReportEmployeeProfileBusiness ReportEmployeeProfileBusiness,IProcessedReportBusiness ProcessedReportBusiness,
                                        IReportTeachingAssignmentBusiness reportTeachingAssignmentBusiness)
        {
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportEmployeeProfileBusiness = ReportEmployeeProfileBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ReportTeachingAssignmentBusiness = reportTeachingAssignmentBusiness;
        }
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult LoadEmployeeList()//báo cáo danh sách cán bộ
        {
            this.SetViewData();
            return PartialView("_EmployeeListReport");
        }
        public PartialViewResult LoadEmployeeContact()//báo cáo danh sách cán bộ hợp đồng
        {
            this.SetViewData();
            return PartialView("_EmployeeContactReport");
        }
        public PartialViewResult LoadEmployeePayroll()//báo cáo danh sách cán bộ biên chế
        {
            this.SetViewData();
            return PartialView("_EmployeePayrollReport");
        }
        public PartialViewResult LoadEmployeeBySenior()//báo cáo danh sách cán bộ theo thâm niên
        {
            this.SetViewData();
            return PartialView("_EmployeeBySeniorReport");
        }
        public PartialViewResult LoadTeachingAssignment()//báo cáo phân công giảng dạy
        {
            //Danh sách khối học
            ViewData[ReportMenuDirectionConstant.LS_EDUCATION_LEVEL] = _globalInfo.EducationLevels;

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = _globalInfo.EducationLevels.FirstOrDefault().EducationLevelID;
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass);
            if (lsClass.Count() != 0)
            {
                List<ClassProfile> lstClass = lsClass.ToList();
                ViewData[ReportMenuDirectionConstant.LS_CLASS] = lstClass;
            }
            else
            {
                ViewData[ReportMenuDirectionConstant.LS_CLASS] = new List<ClassProfile>();
            }

            ViewData[ReportMenuDirectionConstant.LS_SEMESTER] = CommonList.Semester();
            int defaultSemester = (_globalInfo.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : _globalInfo.Semester.GetValueOrDefault();
            ViewData[ReportMenuDirectionConstant.DF_SEMESTER] = defaultSemester;
            return PartialView("_TeachingAssignmentReport");
        }
        public PartialViewResult LoadEmployeeByGraduationLevel()//báo cáo cán bộ theo trình độ chuyên môn
        {
            return PartialView("_EmployeeByGraduationLevelReport");
        }
        public PartialViewResult LoadEmployeeSalary()//báo cáo bảng lương cán bộ
        {
            return PartialView("_EmployeeSalaryReport");
        }
        private void SetViewData()
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["IsActive"] = true;

            List<SchoolFaculty> lsFaculty = SchoolFacultyBusiness.SearchBySchool(Global.SchoolID.Value, Dictionary).OrderBy(o => o.FacultyName).ToList();
            ViewData[ReportMenuDirectionConstant.CBO_FACULTY] = lsFaculty;

            List<SelectListItem> lstMath = new List<SelectListItem>();

            lstMath.Add(new SelectListItem { Text = ">=", Value = "1" });
            lstMath.Add(new SelectListItem { Text = "<=", Value = "2" });
            lstMath.Add(new SelectListItem { Text = "=", Value = "3" });
            ViewData[ReportMenuDirectionConstant.CBO_MATH] = new SelectList(lstMath, "Value", "Text");

        }
        private void SetViewDataCapacityPupil()
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Danh sách học kỳ
            if (Global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                ViewData[ReportMenuDirectionConstant.LS_SEMESTER] = CommonList.Semester();
                int defaultSemester = (Global.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : Global.Semester.GetValueOrDefault();
                ViewData[ReportMenuDirectionConstant.DF_SEMESTER] = defaultSemester;
            }
            else if (Global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || Global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                ViewData[ReportMenuDirectionConstant.LS_SEMESTER] = CommonList.SemesterAndAll();
                int defaultSemester = (Global.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : Global.Semester.GetValueOrDefault();
                ViewData[ReportMenuDirectionConstant.DF_SEMESTER] = defaultSemester;
            }
            //Danh sách môn học
            dic["AppliedLevel"] = Global.AppliedLevel;
            dic["AcademicYearID"] = Global.AcademicYearID;
            dic["SchoolID"] = Global.SchoolID;
            List<SchoolSubject> iquSchoolSubject = SchoolSubjectBusiness.SearchBySchool(Global.SchoolID.Value, dic).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            var listTemp = iquSchoolSubject.Select(s => new { s.SubjectCat.SubjectName, s.SubjectCat.SubjectCatID }).Distinct().ToList();
            List<SelectListItem> lstSubjectCat = new List<SelectListItem>();
            if (listTemp != null && listTemp.Count > 0)
            {
                foreach (var subjectCat in listTemp)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = subjectCat.SubjectName;
                    item.Value = subjectCat.SubjectCatID.ToString();
                    lstSubjectCat.Add(item);
                }
            }
            ViewData[ReportMenuDirectionConstant.LS_SUBJECT] = lstSubjectCat;
            //Grid
            ViewData[ReportMenuDirectionConstant.LS_TEACHER] = new List<ReportCapacityByTeacherViewModel>();
        }
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? idEducationLevel)
        {

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = idEducationLevel;
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID;

            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass);
            List<ClassProfile> lstClass = lsClass.ToList();
            if (lsClass.Count() != 0)
            {
                ViewData[ReportMenuDirectionConstant.LS_CLASS] = lstClass;
            }
            else
            {
                ViewData[ReportMenuDirectionConstant.LS_CLASS] = new List<ClassProfile>();
            }
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
        }

        #region Export Excel
        [ValidateAntiForgeryToken]
        public JsonResult GetReportEmployee(ReportEmpoyeeViewModel frm)
        {
            if (frm.rptReport != 5)//khác thống kê phân công giảng dạy
            {
                DateTime dt = frm.ReportDate;
                if (dt == DateTime.MinValue)
                {
                    throw new BusinessException("ReportEmpoyeeProfile_Validate_DateTimeRequire");
                }
                if (dt > DateTime.Now)
                {
                    throw new BusinessException("ReportEmpoyeeProfile_Validate_DateTimeNow");
                }
            }
            
            switch (frm.rptReport)
            {
                case 1: //Báo cáo danh sách cán bộ
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BCGV;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeProfileBusiness.GetReportEmployeeList(reportEmployeeProfile);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeList(reportEmployeeProfile);
                            processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeList(reportEmployeeProfile, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 2: //Báo cáo danh sách cán bộ hợp đồng
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_HOP_DONG;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeProfileBusiness.GetReportEmployeeContract(reportEmployeeProfile);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeContract(reportEmployeeProfile);
                            processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeContract(reportEmployeeProfile, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 3: //Báo cáo danh sách cán bộ biên chế
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BIEN_CHE;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeProfileBusiness.GetReportEmployeePayroll(reportEmployeeProfile);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeePayroll(reportEmployeeProfile);
                            processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeePayroll(reportEmployeeProfile, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 4: //Báo cáo danh sách cán bộ theo thâm niên
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        if (frm.Math != null)
                        {
                            reportEmployeeProfile.Compare = frm.Math.Value;
                        }
                        reportEmployeeProfile.SeniorYear = frm.SeniorYear;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeProfileBusiness.GetReportEmployeeSenior(reportEmployeeProfile);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeSenior(reportEmployeeProfile);
                            processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeSenior(reportEmployeeProfile, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 5:
                    {
                        ReportTeachingAssignmentBO reportTeachingAssignment = new ReportTeachingAssignmentBO();
                        reportTeachingAssignment.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                        reportTeachingAssignment.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportTeachingAssignment.EducationLevelID = frm.EducationLevelID;
                        reportTeachingAssignment.ClassID = frm.ClassID;
                        reportTeachingAssignment.Semester = frm.SemesterID;
                        reportTeachingAssignment.AppliedLevelID = _globalInfo.AppliedLevel.GetValueOrDefault();
                        string reportCode = SystemParamsInFile.REPORT_PHAN_CONG_GIANG_DAY;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            string inputParameterHashKey = ReportTeachingAssignmentBusiness.GetHashKey(reportTeachingAssignment);
                            processedReport = ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportTeachingAssignmentBusiness.ExcelCreateTeachingAssignment(reportTeachingAssignment);
                            processedReport = ReportTeachingAssignmentBusiness.ExcelInsertTeachingAssignment(reportTeachingAssignment, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportEmpoyee(ReportEmpoyeeViewModel frm)
        {
            switch (frm.rptReport)
            {
                case 1: //Báo cáo danh sách cán bộ
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeList(reportEmployeeProfile);
                        ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeList(reportEmployeeProfile, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 2: //Báo cáo danh sách cán bộ hợp đồng
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeContract(reportEmployeeProfile);
                        ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeContract(reportEmployeeProfile, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 3: //Báo cáo danh sách cán bộ biên chế
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeePayroll(reportEmployeeProfile);
                        ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeePayroll(reportEmployeeProfile, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 4: //Báo cáo danh sách cán bộ theo thâm niên
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        if (frm.Math != null)
                        {
                            reportEmployeeProfile.Compare = frm.Math.Value;
                        }
                        reportEmployeeProfile.SeniorYear = frm.SeniorYear;
                        Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeSenior(reportEmployeeProfile);
                        ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeSenior(reportEmployeeProfile, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 5:
                    {
                        ReportTeachingAssignmentBO reportTeachingAssignment = new ReportTeachingAssignmentBO();
                        reportTeachingAssignment.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                        reportTeachingAssignment.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportTeachingAssignment.EducationLevelID = frm.EducationLevelID;
                        reportTeachingAssignment.ClassID = frm.ClassID;
                        reportTeachingAssignment.Semester = frm.SemesterID;

                        Stream excel = ReportTeachingAssignmentBusiness.ExcelCreateTeachingAssignment(reportTeachingAssignment);
                        ProcessedReport processedReport = ReportTeachingAssignmentBusiness.ExcelInsertTeachingAssignment(reportTeachingAssignment, excel);
                        excel.Close();

                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_HOP_DONG,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BIEN_CHE,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN,
                SystemParamsInFile.REPORT_PHAN_CONG_GIANG_DAY

            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;

        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_HOP_DONG,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BIEN_CHE,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN,
                SystemParamsInFile.REPORT_PHAN_CONG_GIANG_DAY

            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion
    }
}