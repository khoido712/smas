﻿using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportMenuDirectionArea;
using SMAS.Web.Areas.ReportMenuDirectionArea.Models;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.ExportXML;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    public partial class EmisReportController : BaseController
    {
        //private int _ReportTime = 0;
        private readonly IProcessedCellDataBusiness ProcessedCellDataBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IStatisticsConfigBusiness StatisticsConfigBusiness;
        private readonly IStatisticsForUnitBusiness StatisticsForUnitBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ICapacityStatisticBusiness CapacityStatisticBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly IConductStatisticBusiness ConductStatisticBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        IEducationQualityStatisticBusiness EducationQualityStatisticBusiness;
        private ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        public EmisReportController(IProcessedCellDataBusiness processedCellDataBusiness, IProcessedReportBusiness ProcessedReportBusiness,
                                    IStatisticsConfigBusiness statisticsConfigBusiness, IStatisticsForUnitBusiness statisticsForUnitBusiness,
                                    IEducationLevelBusiness educationLevelBusiness, ISubjectCatBusiness subjectCatBusiness,
                                    IMarkStatisticBusiness markStatisticBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
                                    ICapacityStatisticBusiness capacityStatisticBusiness, ISubCommitteeBusiness subCommitteeBusiness,
                                    IConductStatisticBusiness conductStatisticBusiness, IAcademicYearBusiness AcademicYearBusiness,
                                    IEducationQualityStatisticBusiness EducationQualityStatisticBusiness,
                                    ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness, ISchoolProfileBusiness SchoolProfileBusiness,
                                    IPupilProfileBusiness PupilProfileBusiness)
        {
            this.ProcessedCellDataBusiness = processedCellDataBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.StatisticsConfigBusiness = statisticsConfigBusiness;
            this.StatisticsForUnitBusiness = statisticsForUnitBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.MarkStatisticBusiness = markStatisticBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.EducationQualityStatisticBusiness = EducationQualityStatisticBusiness;
            this.SchoolSubsidiaryBusiness = SchoolSubsidiaryBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
        }
        public ActionResult Index()
        {
            return View();
        }
        #region SetViewData
        private void SetViewDataEmisSchool()
        {
            int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
            if (EducationGrade <= 7)
            {
                int StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS, 1);
                int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
                int appliedlevel = _globalInfo.AppliedLevel.Value; // cap truong
                ViewData[ReportMenuDirectionConstant.EDUCATION_GRADE] = appliedlevel;
                ViewData[ReportMenuDirectionConstant.YEAR] = Year + "-" + (Year + 1);
                ViewData[ReportMenuDirectionConstant.StatusReport] = StatusReport;
                ViewData[ReportMenuDirectionConstant.REPORT_PERIOD] = ReportMenuDirectionConstant.FIRST_PERIOD;
            }
            else
            {
                ViewData[ReportMenuDirectionConstant.MESSAGE_ERROR] = ReportMenuDirectionConstant.MESSAGE_ERROR;
            }
        }
        private void SetViewDataQD5363()
        {
            int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
            if (EducationGrade <= 7)
            {
                int StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, SystemParamsInFile.REPORT_QD5363_CODE, 1, _globalInfo.AppliedLevel.Value);
                int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
                int appliedlevel = _globalInfo.AppliedLevel.Value; // cap truong
                ViewData[ReportMenuDirectionConstant.EDUCATION_GRADE] = appliedlevel;
                ViewData[ReportMenuDirectionConstant.YEAR] = Year + "-" + (Year + 1);
                ViewData[ReportMenuDirectionConstant.StatusReport] = StatusReport;
                ViewData[ReportMenuDirectionConstant.REPORT_PERIOD] = ReportMenuDirectionConstant.FIRST_PERIOD;
            }
            else
            {
                ViewData[ReportMenuDirectionConstant.MESSAGE_ERROR] = ReportMenuDirectionConstant.MESSAGE_ERROR;
            }
        }
        private void SetViewData()
        {
            ViewData[ReportMenuDirectionConstant.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[ReportMenuDirectionConstant.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            ViewData[ReportMenuDirectionConstant.LIST_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[ReportMenuDirectionConstant.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
        }
        private void SetViewDataHLHK()
        {
            ViewData[ReportMenuDirectionConstant.LIST_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[ReportMenuDirectionConstant.LIST_SEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value");
            List<SubCommittee> listSubCommittee = SubCommitteeBusiness.Search(new Dictionary<string, object>())
                .OrderBy(o => o.Resolution)
                .ToList();
            ViewData[ReportMenuDirectionConstant.LIST_SUBCOMMITTEE] = new SelectList(listSubCommittee, "SubCommitteeID", "Resolution");
        }
        private void SetViewDataEducationQuality()
        {
            List<Object> listSemester = new List<object>() { new { key = 1, value = Res.Get("Common_Label_Combo_FirstSemester") },
                new { key = 2, value = Res.Get("Common_Label_Combo_SecondSemester") } };

            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime firstSemesterStartDate = ay.FirstSemesterStartDate.GetValueOrDefault();
            DateTime firstSemesterEndDate = ay.FirstSemesterEndDate.GetValueOrDefault();

            int curSemester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            if (DateTime.Compare(DateTime.Now, firstSemesterStartDate) >= 0 && DateTime.Compare(DateTime.Now, firstSemesterEndDate) <= 0)
            {
                curSemester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            ViewData[ReportMenuDirectionConstant.CBO_SEMESTER] = new SelectList(listSemester, "key", "value", curSemester);

            if (ay != null)
            {
                ViewData[ReportMenuDirectionConstant.ACADEMICYEAR] = ay.DisplayTitle;
            }
            else
            {
                ViewData[ReportMenuDirectionConstant.ACADEMICYEAR] = String.Empty;
            }
        }
        private void SetViewDataSemesterPrimary()
        {
            ViewData[ReportMenuDirectionConstant.LIST_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[ReportMenuDirectionConstant.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
            ViewData[ReportMenuDirectionConstant.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[ReportMenuDirectionConstant.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
        }
        #endregion
        #region Load PartialView
        public PartialViewResult LoadEmisSchoolProfileReport()
        {
            this.SetViewDataEmisSchool();
            return PartialView("_EmisSchoolProfileReport");
        }//Emis hồ sơ trường
        public PartialViewResult LoadPeriodStatistic()//Thống kê điểm kiểm tra định kỳ
        {
            this.SetViewData();
            return PartialView("_PeriodStatisticReport");
        }
        public PartialViewResult LoadSemesterStatistic()//Thống kê điểm kiểm tra học kỳ
        {
            this.SetViewData();
            return PartialView("_SemesterStatisticReport");
        }
        public PartialViewResult LoadCapacitySubjectStatistic()//Thống kê học lực môn
        {
            ViewData[ReportMenuDirectionConstant.LIST_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[ReportMenuDirectionConstant.LIST_SEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value");
            ViewData[ReportMenuDirectionConstant.LIST_MARK] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            ViewData[ReportMenuDirectionConstant.LIST_JUDGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            return PartialView("_CapacitySubjectStatisticReport");
        }
        public PartialViewResult LoadCapacityStatistic()//Thống kê học lực
        {
            this.SetViewDataHLHK();
            ViewData[ReportMenuDirectionConstant.LIST_CAPACITY_LEVEL] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);
            return PartialView("_CapacityStatisticReport");
        }
        public PartialViewResult LoadConductStatistic()//Thống kê hạnh kiểm
        {
            this.SetViewDataHLHK();
            ViewData[ReportMenuDirectionConstant.LIST_CONDUCT_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);
            return PartialView("_ConductStatisticReport");
        }
        public PartialViewResult LoadPreliminarySendSGD()//Báo cáo sơ kết học kỳ
        {
            return PartialView("_PreliminarySendSGDReport");
        }
        public PartialViewResult LoadVEmis()//Báo cáo VEmis
        {
            return PartialView("_VEmisReport");
        }
        public PartialViewResult LoadEducationQualityStatisticReport()
        {
            this.SetViewDataEducationQuality();
            return PartialView("_EducationQualityStatisticReport");
        }
        public PartialViewResult LoadSemesterMarkPrimary()
        {
            this.SetViewDataSemesterPrimary();
            return PartialView("_SemesterMarkPrimary");
        }
        public PartialViewResult LoadQD5363Report()
        {
            this.SetViewDataQD5363();
            return PartialView("QD5363Report");
        }
        #endregion
        #region Export Emis
        public FileResult ExportExcel(FormCollection col)
        {
            int ReportTime = Convert.ToInt32(col["ReportTime"]);
            bool chckFacilities = col["chckFacilities"] != null && col["chckFacilities"].Equals("on") ? true : false;
            string reportCode = this.GetReportCode(chckFacilities);
            int StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode, ReportTime);
            int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
            if (StatusReport == 0 || StatusReport == -1 || EducationGrade > 7)
            {
                throw new BusinessException("Báo cáo chưa được tổng hợp");
            }

            string TEMPLATE_NAME = "";
            string templatePath = "";
            string ExportTemplateName = "";
            String pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            try
            {
                int schoolID = _globalInfo.SchoolID.Value;
                int appliedlevel = _globalInfo.AppliedLevel.Value; // cap truong
                int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
                if (chckFacilities && appliedlevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    #region Bao cao LCU
                    ExportTemplateName = "BieuSLTonghop_THPT.xls";
                    if (ReportTime == 2)
                    {


                        templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, SystemParamsInFile.TEMPLATE_NAME_PHYCICAL_FACILITIES_THPT_GN);
                        IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                        IVTWorksheet sheetdata = oBook.GetSheet(1);
                        FillDataLCU(sheetdata, Year, ReportTime);
                        Stream excel = oBook.ToStream();
                        FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                        result.FileDownloadName = ExportTemplateName;
                        return result;
                    }
                    else
                    {

                        templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, SystemParamsInFile.TEMPLATE_NAME_PHYCICAL_FACILITIES_THPT_CN);
                        IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                        IVTWorksheet sheetdata = oBook.GetSheet(1);
                        FillDataLCU(sheetdata, Year, ReportTime);
                        Stream excel = oBook.ToStream();
                        FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                        result.FileDownloadName = ExportTemplateName;
                        return result;
                    }
                    #endregion
                }
                else
                {
                    IXVTWorkbook oBook = null;
                    #region Bao cao EMIS
                    #region EMIS dau ki
                    if (ReportTime == 1)// Nếu là chọn EMIS đầu kì
                    {
                        if (_globalInfo.TrainingType == ReportMenuDirectionConstant.TrainTypeID)//Neu la truong GDTX
                        {
                            TEMPLATE_NAME = "HoSo_TTGDTX_T9.xlsx";
                            ExportTemplateName = "HoSo_TTGDTX_T9.xlsx";
                            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "EMIS Truong", TEMPLATE_NAME);
                            oBook = XVTExportExcel.OpenWorkbook(templatePath);
                            FillDateAndUserReport("K25", "B27", oBook);
                            List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_TTGDTX_CODE_F);
                            FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_GDTX);// Thông tin trừờng
                            FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_GDTX);// Lớp học
                            FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_GDTX);// Học sinh 
                            FillSheet(oBook, 4, list, ReportMenuDirectionConstant.TEACHER_TTGDTX);// Nhan su
                            FillSheet(oBook, 5, list, ReportMenuDirectionConstant.COSOVC_TTGDTX);// CSVC
                            //FillSheet(oBook, 5, list, ReportMenuDirectionConstant.TEACHER_TTGDTX);//Đánh giá cán bộ,giáo viên, nhân viên
                            //FillSheet(oBook, 9, list, ReportMenuDirectionConstant.TEACHER_TTGDTX_ADDITION);//Nhan su bo sung
                        }
                        else
                        {
                            IDictionary<string, object> dic = new Dictionary<string, object>();
                            dic["SchoolID"] = schoolID;
                            dic["IsActive"] = true;
                            List<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryBusiness.Search(dic).ToList();

                            if (EducationGrade == ReportMenuDirectionConstant.Primary)//truong 1 cap va la cap 1
                            {
                                TEMPLATE_NAME = "HoSo_TieuHoc_T9.xlsx";
                                ExportTemplateName = "HoSo_TieuHoc_T9.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("L47", "B49", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_PRIMARY_F);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_PRIMARY);// Thông tin trừờng
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.CLASS_PRIMARY);// Lớp học
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// Học sinh                            
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.TEACHER_PRIMARY_FIRST); //nhan su
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.COSOVC_TH); //CSVC
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.PUPIL_PRIMARY_ADDITION);//hoc sinh bo sung
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.TAECHER_PRIMARY_ADDITION);//nhan su bo sung

                                //Fill thong tin truong phu
                                IXVTWorksheet sheet = oBook.GetSheet(1);
                                FillSubSchoolList(27, sheet, lsSchoolSubsidiary);

                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Secondary) // truong 1 cap va la cap 2
                            {
                                TEMPLATE_NAME = "HoSo_THCS_T9.xlsx";
                                ExportTemplateName = "HoSo_THCS_T9.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("L44", "B46", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_SECONDARY_F);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_SECONDARY);//thông tin trường
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.CLASS_SECONDARY);//Lớp học
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.TEACHER_SECONDARY); //Nhan su 
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.COSOVC_THCS); //CSVC
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.CLASS_SECONDARY_ADDITION);
                                FillSheet(oBook, 9, list, ReportMenuDirectionConstant.PUPIL_SECONDARY_ADDITION);
                                FillSheet(oBook, 10, list, ReportMenuDirectionConstant.TEACHER_SECONDARY_ADDITION);

                                //Fill thong tin truong phu
                                IXVTWorksheet sheet = oBook.GetSheet(1);
                                FillSubSchoolList(29, sheet, lsSchoolSubsidiary);

                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Tertiary_End)//truong 1 cap va la cap 3
                            {
                                TEMPLATE_NAME = "HoSo_THPT_T9.xlsx";
                                ExportTemplateName = "HoSo_THPT_T9.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("L43", "B45", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_TERIARY_F);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_TERIARY);//thông tin trường
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.CLASS_TERIARY);//Lớp học
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// Học sinh
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.TEACHER_TERIARY); //Đánh giá CB-GV-NV
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.COSOVC_THPT); //CSVC
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.PUPIL_TERIARY_ADDITION);
                                FillSheet(oBook, 9, list, ReportMenuDirectionConstant.TEACHER_TERIARY_ADDITION);

                                //Fill thong tin truong phu
                                IXVTWorksheet sheet = oBook.GetSheet(1);
                                FillSubSchoolList(28, sheet, lsSchoolSubsidiary);
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Primary_Secondary)
                            {
                                //Neu truong co cap 1,2
                                TEMPLATE_NAME = "HoSo_PTCS_T9.xlsx";
                                ExportTemplateName = "HoSo_PTCS_T9.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_PRIMARY_SECONDARY_F);
                                FillDateAndUserReport("L43", "B45", oBook);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_PRIMARY_SECONDARY);//thông tin trường
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.CLASS_PRIMARY_SECONDARY);//Lớp học
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// Học sinh TH
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh THCS
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.TEACHER_PRIMARY_SECONDARY);// Danh gia CB-GV-NV  
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.COSOVC_PTCS); //CSVC
                                FillSheet(oBook, 10, list, ReportMenuDirectionConstant.CLASS_PRIMARY_SECONDARY_ADDITION);
                                FillSheet(oBook, 11, list, ReportMenuDirectionConstant.PUPIL_PS_ADDITION);
                                FillSheet(oBook, 12, list, ReportMenuDirectionConstant.TEACHER_PRIMARY_SECONDARY_ADDITION);

                                //Fill thong tin truong phu
                                IXVTWorksheet sheet = oBook.GetSheet(1);
                                FillSubSchoolList(27, sheet, lsSchoolSubsidiary);

                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Secondary_Teriary)
                            {
                                //Neu truong co cap 2,3
                                TEMPLATE_NAME = "HoSo_PTTH_T9.xlsx";
                                ExportTemplateName = "HoSo_PTTH_T9.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_SECONDARY_TERIARY_F);
                                FillDateAndUserReport("L43", "B45", oBook);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_SECONDARY_TERIARY);//thông tin trường
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.CLASS_SECONDARY_TERIARY);//Lớp học
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh THCS
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// Học sinh THPT
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.TEACHER_SECONDARY_TERIARY);//Nhan su
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.COSOVC_PTTH); //CSVC
                                FillSheet(oBook, 10, list, ReportMenuDirectionConstant.CLASS_SECONDARY_TERIARY_ADDITION);
                                FillSheet(oBook, 11, list, ReportMenuDirectionConstant.PUPIL_ST_ADDITION);
                                FillSheet(oBook, 12, list, ReportMenuDirectionConstant.TEACHER_SECONDARY_TERIARY_ADDITION);

                                //Fill thong tin truong phu
                                IXVTWorksheet sheet = oBook.GetSheet(1);
                                FillSubSchoolList(28, sheet, lsSchoolSubsidiary);
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary)
                            {
                                TEMPLATE_NAME = "HoSo_PT_T9.xlsx";
                                ExportTemplateName = "HoSo_PT_T9.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_PST_F);
                                FillDateAndUserReport("L43", "B45", oBook);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_PST);//thong tin truong pho thong
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.CLASS_PST);//thong tin lop pho thong
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// hoc sinh tieu hoc
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// hoc sinh THCS
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// hoc sinh THPT
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.TEACHER_PST);// nhan su
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.COSOVC_PT); //CSVC
                                FillSheet(oBook, 11, list, ReportMenuDirectionConstant.CLASS_PST_ADDITION);// Lop hoc bo sung
                                FillSheet(oBook, 12, list, ReportMenuDirectionConstant.PUPIL_PST_ADDITION);// Hoc sinh bo sung
                                FillSheet(oBook, 13, list, ReportMenuDirectionConstant.TEACHER_PST_ADDITION);//Nhan su bo sung

                                //Fill thong tin truong phu
                                IXVTWorksheet sheet = oBook.GetSheet(1);
                                FillSubSchoolList(28, sheet, lsSchoolSubsidiary);
                            }
                        }
                    }
                    #endregion

                    #region EMIS giua ki
                    if (ReportTime == 2)// Nếu là chọn EMIS giữa kì
                    {
                        if (_globalInfo.TrainingType == ReportMenuDirectionConstant.TrainTypeID)//Neu la truong GDTX
                        {
                            TEMPLATE_NAME = "HoSo_TTGDTX_T12.xlsx";
                            ExportTemplateName = "HoSo_TTGDTX_T12.xlsx";
                            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "2.Giua Nam", "EMIS Truong", TEMPLATE_NAME);
                            oBook = XVTExportExcel.OpenWorkbook(templatePath);
                            FillDateAndUserReport("M22", "B24", oBook);
                            List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_TTGDTX_CODE_MID);
                            FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_GDTX);// Thông tin trừờng
                            FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_GDTX);// Lớp học
                            FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_GDTX);// Học sinh                        
                            FillSheet(oBook, 4, list, ReportMenuDirectionConstant.TEACHER_TTGDTX);//Đánh giá cán bộ,giáo viên, nhân viên
                            FillSheet(oBook, 7, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY_GDTX);//Đánh giá học sinh thcs
                            FillSheet(oBook, 8, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY_GDTX);//Đánh giá học sinh thpt
                            FillSheet(oBook, 9, list, ReportMenuDirectionConstant.PUPIL_RATE_ORTHER_GDTX);//Đánh giá học sinh khác                        
                            FillSheet(oBook, 5, list, ReportMenuDirectionConstant.FACILITIES_GDTX);//CSVC
                        }
                        else
                        {
                            if (EducationGrade == ReportMenuDirectionConstant.Primary)// 1 cap va la cap 1
                            {
                                TEMPLATE_NAME = "HoSo_TieuHoc_T12.xlsx";
                                ExportTemplateName = "HoSo_TieuHoc_T12.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "2.Giua Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("L27", "B29", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.M_REPORT_CODE_PRIMARY);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_PRIMARY);// Thông tin trừờng
                                FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_PRIMARY);// Lớp học
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// Học sinh
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_PRIMARY); // Học sinh dân tộc
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.TEACHER_PRIMARY); //Nhân sự
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY);// Đánh giá học sinh
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.FACILITIES_RRIMARY);//CSVC                                
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Secondary) // truong 1 cap va la cap 2
                            {
                                TEMPLATE_NAME = "HoSo_THCS_T12.xlsx";
                                ExportTemplateName = "HoSo_THCS_T12.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "2.Giua Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("L29", "B31", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.M_REPORT_CODE_SECONDARY);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_SECONDARY);//thông tin trường
                                FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_SECONDARY);//Lớp học
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_SECONDARY);// Học sinh dân tộc
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.TEACHER_SECONDARY); //Nhân sự
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY);// Đánh giá học sinh
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY);//Đánh giá theo môn
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.FACILITIES_SECONDARY);//CSVC
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Tertiary_End)//truong 1 cap va la cap 3
                            {
                                TEMPLATE_NAME = "HoSo_THPT_T12.xlsx";
                                ExportTemplateName = "HoSo_THPT_T12.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "2.Giua Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("L30", "B32", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.M_REPORT_CODE_TERIARY);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_TERIARY);//thông tin trường
                                FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_TERIARY);//Lớp học
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// Học sinh
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_TERIARY);// Học sinh dân tộc
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.TEACHER_TERIARY); //Nhân sự
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY);// Đánh giá học sinh
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY);//Đánh giá theo môn
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.FACILITIES_TERTIARY);//CSVC
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Primary_Secondary)
                            {
                                //Neu truong co cap 1,2
                                TEMPLATE_NAME = "HoSo_PTCS_T12.xlsx";
                                ExportTemplateName = "HoSo_PTCS_T12.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "2.Giua Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.M_REPORT_CODE_PRIMARY_SECONDARY);
                                FillDateAndUserReport("L29", "B31", oBook);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_PRIMARY_SECONDARY);//thông tin trường
                                FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_PRIMARY_SECONDARY);//Lớp học
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// Học sinh TH
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_PRIMARY);// Học sinh dan toc TH DT
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh THCS
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_SECONDARY);// Học sinh dan toc THCS DT
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.TEACHER_PRIMARY_SECONDARY);// Nhan Su PTCS
                                FillSheet(oBook, 9, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY);//Danh gia hoc sinh TH
                                FillSheet(oBook, 10, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY);//Danh gia hoc sinh THCS
                                FillSheet(oBook, 11, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY);//Danh gia mon hoc THCS   
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.FACILITIES_PS);//CSVC
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Secondary_Teriary)
                            {
                                //Neu truong co cap 2,3
                                TEMPLATE_NAME = "HoSo_PTTH_T12.xlsx";
                                ExportTemplateName = "HoSo_PTTH_T12.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "2.Giua Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.M_REPORT_CODE_SECONDARY_TERIARY);
                                FillDateAndUserReport("L29", "B31", oBook);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_SECONDARY_TERIARY);//thông tin trường
                                FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_SECONDARY_TERIARY);//Lớp học
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh THCS
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_SECONDARY);// Học sinh dan toc THCS
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// Học sinh THPT
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_TERIARY);// Học sinh dan toc THPT
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.TEACHER_SECONDARY_TERIARY);// Nhan Su PTTH
                                FillSheet(oBook, 9, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY);//Danh gia hoc sinh THCS
                                FillSheet(oBook, 10, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY);//Danh gia hoc sinh THPT
                                FillSheet(oBook, 11, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY);//Danh gia mon hoc THCS
                                FillSheet(oBook, 12, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY);//Danh gia mon hoc THPT
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.FACILITIES_ST);//CSVC
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary)
                            {
                                TEMPLATE_NAME = "HoSo_PT_T12.xlsx";
                                ExportTemplateName = "HoSo_PT_T12.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "2.Giua Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.M_REPORT_CODE_PST);
                                FillDateAndUserReport("L29", "B31", oBook);
                                FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_PST);//thong tin truong pho thong
                                FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_PST);//thong tin lop pho thong
                                FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// hoc sinh tieu hoc
                                FillSheet(oBook, 4, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_PRIMARY);// hoc sinh tieu hoc dan toc
                                FillSheet(oBook, 5, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// hoc sinh THCS
                                FillSheet(oBook, 6, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_SECONDARY);// hoc sinh THCS dan toc
                                FillSheet(oBook, 7, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// hoc sinh THPT
                                FillSheet(oBook, 8, list, ReportMenuDirectionConstant.PUPIL_ETHNIC_TERIARY);// hoc sinh THPT dan toc
                                FillSheet(oBook, 9, list, ReportMenuDirectionConstant.TEACHER_PST);// Nhan su PT
                                FillSheet(oBook, 11, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY);// Danh gia hoc sinh TH
                                FillSheet(oBook, 12, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY);// Danh gia hoc sinh THCS
                                FillSheet(oBook, 13, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY);// Danh gia hoc sinh THPT
                                FillSheet(oBook, 14, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY);// Danh gia mon hoc THCS
                                FillSheet(oBook, 15, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY);// Danh gia mon hoc THPT
                                FillSheet(oBook, 10, list, ReportMenuDirectionConstant.FACILITIES_PST);//CSVC
                            }
                        }
                    }
                    #endregion

                    #region EMIS cuoi ki
                    if (ReportTime == 3)// Nếu là chọn EMIS cuoi ki
                    {
                        if (_globalInfo.TrainingType == ReportMenuDirectionConstant.TrainTypeID)//Neu la truong GDTX
                        {
                            TEMPLATE_NAME = "HoSo_TTGDTX_T5.xlsx";
                            ExportTemplateName = "HoSo_TTGDTX_T5.xlsx";
                            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "3.Cuoi Nam", "EMIS Truong", TEMPLATE_NAME);
                            oBook = XVTExportExcel.OpenWorkbook(templatePath);
                            FillDateAndUserReport("K27", "B29", oBook);
                            List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_TTGDTX_CODE);
                            FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO_GDTX);// Thông tin trừờng
                            FillSheet(oBook, 2, list, ReportMenuDirectionConstant.CLASS_GDTX);// Lớp học
                            FillSheet(oBook, 3, list, ReportMenuDirectionConstant.PUPIL_GDTX);// Học sinh
                            FillSheet(oBook, 4, list, ReportMenuDirectionConstant.TRAINING_GDTX);// Dao tao
                            FillSheet(oBook, 5, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY_GDTX);//Đánh giá học sinh thcs
                            FillSheet(oBook, 6, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY_GDTX);//Đánh giá học sinh thpt
                            FillSheet(oBook, 7, list, ReportMenuDirectionConstant.PUPIL_RATE_ORTHER_GDTX);//Đánh giá học sinh khác
                            FillSheet(oBook, 8, list, ReportMenuDirectionConstant.TEACHER_RATE_GDTX);//Đánh giá cán bộ,giáo viên, nhân viên
                        }
                        else
                        {
                            if (EducationGrade == ReportMenuDirectionConstant.Primary)//truong 1 cap va la cap 1
                            {
                                TEMPLATE_NAME = "HoSo_TieuHoc_T5.xlsx";
                                ExportTemplateName = "HoSo_TieuHoc_T5.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "3.Cuoi Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("K29", "B31", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_PRIMARY);
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SCHOOL_INFO_PRIMARY, true);// Thông tin trừờng
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.CLASS_PRIMARY);// Lớp học
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// Học sinh
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY);// Đánh giá học sinh
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_PRIMARY_ADDITION); //HSTHBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY_ADDITION); //Đánh giá HSTHBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_PRIMARY_FIRST);
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TAECHER_PRIMARY_ADDITION);
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_PRIMARY);//HOC SINH CHINH SACH
                                
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Secondary) // truong 1 cap va la cap 2
                            {
                                TEMPLATE_NAME = "HoSo_THCS_T5.xlsx";
                                ExportTemplateName = "HoSo_THCS_T5.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "3.Cuoi Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("K32", "B34", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_SECONDARY);
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SCHOOL_INFO_SECONDARY, true);//thông tin trường
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.CLASS_SECONDARY);//Lớp học
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY);// Đánh giá học sinh
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY);//Đánh giá theo môn
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_SECONDARY_ADDITION);//Hoc sinh THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY_ADDITION);//Danh gia Hoc sinh THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY_ADDITION);//Danh gia MH THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_SECONDARY);//Nhan su THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_SECONDARY_ADDITION);//Nhan su THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_SECONDARY);//HOC SINH CHINH SACH
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Tertiary_End)//truong 1 cap va la cap 3
                            {
                                TEMPLATE_NAME = "HoSo_THPT_T5.xlsx";
                                ExportTemplateName = "HoSo_THPT_T5.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "3.Cuoi Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                FillDateAndUserReport("K33", "B35", oBook);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_TERIARY);
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SCHOOL_INFO_TERIARY, true);//thông tin trường
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.CLASS_TERIARY);//Lớp học
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// Học sinh
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY);// Đánh giá học sinh
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY);//Đánh giá theo môn
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_TERIARY_ADDITION);//HS THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY_ADDITION);//Danh gia HS THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY_ADDITION);//Đánh giá MH THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_TERIARY);//Nhan su THPT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_TERIARY_ADDITION);//Nhan su THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_TERIARY);//HOC SINH CHINH SACH
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Primary_Secondary)
                            {
                                //Neu truong co cap 1,2
                                TEMPLATE_NAME = "HoSo_PTCS_T5.xlsx";
                                ExportTemplateName = "HoSo_PTCS_T5.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "3.Cuoi Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_PRIMARY_SECONDARY);
                                FillDateAndUserReport("K32", "B34", oBook);
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SCHOOL_INFO_PRIMARY_SECONDARY, true);//thông tin trường
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.CLASS_PRIMARY_SECONDARY);//Lớp học
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// Học sinh TH
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY);//Danh gia hoc sinh TH
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY);//Danh gia hoc sinh THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY);//Danh gia mon hoc THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_PRIMARY_ADDITION); //HSTHBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_SECONDARY_ADDITION);//Hoc sinh THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY_ADDITION);//Danh gia hoc sinh THBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY_ADDITION);//Danh gia hoc sinh THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY_ADDITION);//Đánh giá MH THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_PRIMARY_SECONDARY);//Nhan su PTCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_PRIMARY_SECONDARY_ADDITION);//Nhan su PTCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_PRIMARY);//HOC SINH CHINH SACH
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_SECONDARY);//HOC SINH CHINH SACH

                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Secondary_Teriary)
                            {
                                //Neu truong co cap 2,3
                                TEMPLATE_NAME = "HoSo_PTTH_T5.xlsx";
                                ExportTemplateName = "HoSo_PTTH_T5.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "3.Cuoi Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_SECONDARY_TERIARY);
                                FillDateAndUserReport("K31", "B33", oBook);
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SCHOOL_INFO_SECONDARY_TERIARY, true);//thông tin trường
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.CLASS_SECONDARY_TERIARY);//Lớp học
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// Học sinh THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// Học sinh THPT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY);//Danh gia hoc sinh THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY);//Danh gia hoc sinh THPT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY);//Danh gia mon hoc THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY);//Danh gia mon hoc THPT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_SECONDARY_ADDITION);//Hoc sinh THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_TERIARY_ADDITION);//HS THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY_ADDITION);//Danh gia Hoc sinh THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY_ADDITION);//Danh gia MH THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY_ADDITION);//Danh gia HS THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY_ADDITION);//Đánh giá MH THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_SECONDARY_TERIARY);//Nhan su PTTH
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_SECONDARY_TERIARY_ADDITION);//Nhan su PTTHBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_SECONDARY);//HOC SINH CHINH SACH
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_TERIARY);//HOC SINH CHINH SACH
                                
                            }
                            else if (EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary)
                            {
                                TEMPLATE_NAME = "HoSo_PT_T5.xlsx";
                                ExportTemplateName = "HoSo_PT_T5.xlsx";
                                templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "3.Cuoi Nam", "EMIS Truong", TEMPLATE_NAME);
                                oBook = XVTExportExcel.OpenWorkbook(templatePath);
                                List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.E_REPORT_CODE_PST);
                                FillDateAndUserReport("K31", "B33", oBook);
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SCHOOL_INFO_PST, true);//thong tin truong pho thong
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.CLASS_PST);//thong tin lop pho thong
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_PRIMARY);// hoc sinh tieu hoc
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_SECONDARY);// hoc sinh THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_TERIARY);// hoc sinh THPT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY);// Danh gia hoc sinh TH
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY);// Danh gia hoc sinh THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY);// Danh gia hoc sinh THPT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY);// Danh gia mon hoc THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY);// Danh gia mon hoc THPT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_PRIMARY_ADDITION); //HSTHBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_SECONDARY_ADDITION);//Hoc sinh THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_TERIARY_ADDITION);//HS THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_PRIMARY_ADDITION); //Đánh giá HSTHBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_SECONDARY_ADDITION);//Danh gia Hoc sinh THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_RATE_TERIARY_ADDITION);//Danh gia HS THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_SECONDARY_ADDITION);//Danh gia MH THCSBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.SUBJECT_RATE_TERIARY_ADDITION);//Đánh giá MH THPTBS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_PST);//Nhan su PT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.TEACHER_PST_ADDITION);//Nhan su PT
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_PRIMARY);//HOC SINH CHINH SACH TH
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_SECONDARY);//HOC SINH CHINH SACH THCS
                                FillSheetBySheetName(oBook, list, ReportMenuDirectionConstant.PUPIL_POLICY_TARGET_TERIARY);//HOC SINH CHINH SACH THPT
                            }
                        }
                    }
                    #endregion
                    //to stream xuat ra file
                    Stream excel = oBook.ToStream();
                    return File(excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ExportTemplateName);
                    #endregion
                }

            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "ExportExcel", "null", ex);

            }
            return null;
        }
        private void FillSheet(IXVTWorkbook oBooks, int sheetIndex, List<ProcessedCellData> lstInfoOriginal, string SheetName)
        {
            //condition là tên từng sheet muốn fill ra
            IXVTWorksheet oSheet = oBooks.GetSheet(sheetIndex);
            if (lstInfoOriginal == null || lstInfoOriginal.Count == 0) { return; }
            List<ProcessedCellData> lstInfo = lstInfoOriginal.Where(p => p.SheetName == SheetName).ToList();
            string valuestr = string.Empty;
            string cell = string.Empty, value = string.Empty;

            for (int i = 0; i < lstInfo.Count; i++)
            {
                cell = lstInfo[i].CellAddress;
                value = lstInfo[i].CellValue;
                if (!String.IsNullOrEmpty(value) && !value.ToLower().Equals("null") && !value.ToLower().Equals("0"))
                {
                    oSheet.SetCellValue(cell, value);
                }
            }
        }
        private void FillSheetBySheetName(IXVTWorkbook oBooks, List<ProcessedCellData> lstInfoOriginal, string SheetName, bool? isSheetSchool = false)
        {
            //condition là tên từng sheet muốn fill ra
            IXVTWorksheet oSheet = oBooks.GetSheet(SheetName);
            if (lstInfoOriginal == null || lstInfoOriginal.Count == 0) { return; }
            List<ProcessedCellData> lstInfo = lstInfoOriginal.Where(p => p.SheetName == SheetName).ToList();
            string valuestr = string.Empty;
            string cell = string.Empty, value = string.Empty;

            for (int i = 0; i < lstInfo.Count; i++)
            {
                cell = lstInfo[i].CellAddress;
                value = lstInfo[i].CellValue;

                if (!String.IsNullOrEmpty(value) && !value.ToLower().Equals("null") && !value.ToLower().Equals("0"))
                {
                    if (isSheetSchool.HasValue && isSheetSchool.Value)
                    {
                        if (cell.Substring(0, 1).Equals("Z"))
                        {
                            oSheet.SetCellValue(cell, value);
                        }
                        else // #Z
                        {
                            valuestr = String.Format("'{0}", value);
                            oSheet.SetCellValue(cell, valuestr);
                        }
                    }
                    else
                    {
                        oSheet.SetCellValue(cell, value);
                    }
                }
            }
        }
        private void FillDataLCU(IVTWorksheet oSheet, int year, int period)
        {
            //condition là tên từng sheet muốn fill ra     
            List<PhysicalFacilitiesBO> listData;
            if (period == 2)
            {
                listData = ProcessedCellDataBusiness.GetListDataFacilities(_globalInfo.SchoolID.Value, year, period, _globalInfo.ProvinceID.Value, SystemParamsInFile.REPORT_FACILITIES_GN);
            }
            else
            {
                listData = ProcessedCellDataBusiness.GetListDataFacilities(_globalInfo.SchoolID.Value, year, period, _globalInfo.ProvinceID.Value, SystemParamsInFile.REPORT_FACILITIES_CN);
            }
            string valuestr = string.Empty;
            string cell = string.Empty, value = string.Empty;
            if (period == 2)
            {
                oSheet.SetCellValue("B2", "BÁO CÁO SỐ LIỆU THỐNG KÊ GIỮA NĂM \n Đơn vị:" + _globalInfo.SchoolName.ToUpper());
            }
            else
            {
                oSheet.SetCellValue("B2", "BÁO CÁO SỐ LIỆU THỐNG KÊ CUỐI NĂM \n Đơn vị:" + _globalInfo.SchoolName.ToUpper());
            }
            oSheet.SetCellValue("B3", "Năm học: " + year + "-" + (year + 1));
            List<string> lstNvsCell = new List<string>() { "C833", "C834", "C835", "C836", "C837", "C838", "C839" };
            for (int i = 0; i < listData.Count; i++)
            {
                if (!string.IsNullOrEmpty(listData[i].CellReportLCU))
                {
                    cell = listData[i].CellReportLCU;
                    value = listData[i].CellValue;

                    if (!String.IsNullOrEmpty(value) && !value.ToLower().Equals("null"))
                    {
                        if (!value.ToLower().Equals("0") || lstNvsCell.Contains(cell))
                        {
                            oSheet.SetCellValue(cell, Int32.Parse(value));
                        }
                    }
                }
            }
        }
        //Fill thong tin truong phu
        private void FillSubSchoolList(int row, IXVTWorksheet sheet, List<SchoolSubsidiary> lstSubSchool)
        {
            int firstRow = row;
            for (int i = 0; i < lstSubSchool.Count; i++)
            {
                SchoolSubsidiary subSchool = lstSubSchool[i];
                string subSchoolName = subSchool.SubsidiaryName;
                decimal? squareArea = subSchool.SquareArea;
                decimal? distance = subSchool.DistanceFromHQ;
                string address = subSchool.Address;

                sheet.SetCellValue("B" + firstRow.ToString(), (i + 1).ToString());
                sheet.SetCellValue("C" + firstRow.ToString(), subSchoolName);
                sheet.SetCellValue("H" + firstRow.ToString(), squareArea != null ? squareArea.ToString() : String.Empty);
                sheet.SetCellValue("J" + firstRow.ToString(), distance != null ? distance.ToString() : String.Empty);
                sheet.SetCellValue("L" + firstRow.ToString(), address);

                firstRow++;

            }
        }
        public void FillDateAndUserReport(string cellDate, string cellUser, IXVTWorkbook oBooks)
        {
            IXVTWorksheet oSheet = oBooks.GetSheet(1);
            DateTime datenow = DateTime.Now;

            string Province = ProcessedCellDataBusiness.GetProvinceName(_globalInfo.ProvinceID.Value);
            int day = datenow.Day;
            int month = datenow.Month;
            int Year = datenow.Year;
            string DateReport = Province + "," + " ngày " + day + " tháng " + month + " năm " + Year;
            if (_globalInfo.IsAdminSchoolRole)
            {
                oSheet.SetCellValue(cellDate, DateReport);
                oSheet.SetCellValue(cellUser, "");
            }
            else
            {
                string UserName = _globalInfo.UserAccount.Employee.FullName;
                oSheet.SetCellValue(cellDate, DateReport);
                oSheet.SetCellValue(cellUser, UserName);
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult Synthetic(string ReportTime, bool chckFacilities)
        {
            int period = 0;
            if (!string.IsNullOrEmpty(ReportTime))
            {
                period = Convert.ToInt32(ReportTime);
            }
            int StatusReport = 0;
            int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
            string reportCode = this.GetReportCode(chckFacilities);
            ProcessedCellDataBusiness.InsertOrUpdateEmisRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, Year, period, _globalInfo.TrainingType.Value, _globalInfo.DistrictID.Value, _globalInfo.ProvinceID.Value, reportCode);
            return Json(new { StatusReport = StatusReport });
        }
        public JsonResult CheckSynthetic(string ReportTime, bool chckFacilities)
        {
            int period = 0;
            if (!string.IsNullOrEmpty(ReportTime))
            {
                period = Convert.ToInt32(ReportTime);
            }
            int StatusReport = 0;
            string reportCode = this.GetReportCode(chckFacilities);
            int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
            StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode, period);
            return Json(new { StatusReport = StatusReport });
        }
        [ValidateAntiForgeryToken]
        public JsonResult SyntheticQD5363(string ReportTime, int ReportType)
        {
            int StatusReport = 0;
            if (ReportType == 2)
            {
                StatusReport = 1;
            }
            else
            {
                int period = 0;
                if (!string.IsNullOrEmpty(ReportTime))
                {
                    period = Convert.ToInt32(ReportTime);
                }
                int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
                string reportCode = "";
                reportCode = SystemParamsInFile.REPORT_QD5363_CODE;
                ProcessedCellDataBusiness.InsertOrUpdateQD5363Request(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value, _globalInfo.AcademicYearID.Value, Year, period, _globalInfo.TrainingType.Value, _globalInfo.DistrictID.Value, _globalInfo.ProvinceID.Value, reportCode);
            }

            return Json(new { StatusReport = StatusReport });
        }
        public JsonResult CheckSyntheticQD5363(string ReportTime, int ReportType)
        {
            int StatusReport = 0;
            if (ReportType == 2)
            {
                StatusReport = 1;
            }
            else
            {
                int period = 0;
                if (!string.IsNullOrEmpty(ReportTime))
                {
                    period = Convert.ToInt32(ReportTime);
                }

                string reportCode = "";
                reportCode = SystemParamsInFile.REPORT_QD5363_CODE;
                int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
                StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode, period, _globalInfo.AppliedLevel.Value);
            }

            return Json(new { StatusReport = StatusReport });
        }
        public FileResult ExportExcelQD5363(FormCollection col)
        {
            int ReportTime = Convert.ToInt32(col["ReportTime"]);
            int ReportType = Convert.ToInt32(col["ReportType"]);
            string reportCode = "";
            if (ReportType == 1)
            {
                reportCode = SystemParamsInFile.REPORT_QD5363_CODE;

                int StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode, ReportTime, _globalInfo.AppliedLevel.Value);
                int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
                if (StatusReport == 0 || StatusReport == -1 || EducationGrade > 7)
                {
                    throw new BusinessException("Báo cáo chưa được tổng hợp");
                }
            }

            string TEMPLATE_NAME = "";
            string templatePath = "";
            string ExportTemplateName = "";
            String pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            try
            {
                int schoolID = _globalInfo.SchoolID.Value;
                int appliedlevel = _globalInfo.AppliedLevel.Value; // cap truong
                int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;

                IXVTWorkbook oBook = null;
                #region Bao cao EMIS
                #region EMIS dau ki
                if (ReportTime == 1)// Nếu là chọn EMIS đầu kì
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["SchoolID"] = schoolID;
                    dic["IsActive"] = true;

                    #region Report QD5363
                    if (ReportType == 1)
                    {
                        if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                        {
                            TEMPLATE_NAME = "Bieu_C1_TH_D.xlsx";
                            ExportTemplateName = "Bieu_C1_TH_D.xlsx";
                            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "QD5363", TEMPLATE_NAME);
                            oBook = XVTExportExcel.OpenWorkbook(templatePath);

                            List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.QD5363_CODE_C1);
                            FillSheet(oBook, 1, list, ReportMenuDirectionConstant.QD5363_C1_SHEET1);
                            FillSheet(oBook, 2, list, ReportMenuDirectionConstant.QD5363_C1_SHEET2);
                            FillSheet(oBook, 3, list, ReportMenuDirectionConstant.QD5363_C1_SHEET3);
                            FillSheet(oBook, 4, list, ReportMenuDirectionConstant.QD5363_C1_SHEET4);

                        }
                        else if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY) // truong 1 cap va la cap 2
                        {
                            TEMPLATE_NAME = "Bieu_C2_THCS_D.xlsx";
                            ExportTemplateName = "Bieu_C2_THCS_D.xlsx";
                            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "QD5363", TEMPLATE_NAME);
                            oBook = XVTExportExcel.OpenWorkbook(templatePath);

                            List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.QD5363_CODE_C2);
                            FillSheet(oBook, 1, list, ReportMenuDirectionConstant.QD5363_C2_SHEET1);
                            FillSheet(oBook, 2, list, ReportMenuDirectionConstant.QD5363_C2_SHEET2);
                            FillSheet(oBook, 3, list, ReportMenuDirectionConstant.QD5363_C2_SHEET3);
                            FillSheet(oBook, 4, list, ReportMenuDirectionConstant.QD5363_C2_SHEET4);

                        }
                        else if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)//truong 1 cap va la cap 3
                        {
                            TEMPLATE_NAME = "Bieu_C3_THPT_D.xlsx";
                            ExportTemplateName = "Bieu_C3_THPT_D.xlsx";
                            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "QD5363", TEMPLATE_NAME);
                            oBook = XVTExportExcel.OpenWorkbook(templatePath);

                            List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.QD5363_CODE_C3);
                            FillSheet(oBook, 1, list, ReportMenuDirectionConstant.QD5363_C3_SHEET1);
                            FillSheet(oBook, 2, list, ReportMenuDirectionConstant.QD5363_C3_SHEET2);
                            FillSheet(oBook, 3, list, ReportMenuDirectionConstant.QD5363_C3_SHEET3);
                            FillSheet(oBook, 4, list, ReportMenuDirectionConstant.QD5363_C3_SHEET4);
                        }
                    }
                    #endregion

                    #region Báo cáo bỏ học
                    //thuonglv3 xuất báo cáo bỏ học
                    if (ReportType == 2)
                    {
                        TEMPLATE_NAME = "Bieu 6_Thong ke hoc sinh bo hoc.xlsx";
                        ExportTemplateName = "Bieu 6_Thong ke hoc sinh bo hoc.xlsx";
                        templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Truong", TEMPLATE_NAME);
                        oBook = XVTExportExcel.OpenWorkbook(templatePath);

                        // Dữ liệu cho sheet 2
                        List<IDictionary<string, object>> sheet4Data = PupilProfileBusiness.GetStatisticForPupilLeavingOffReport(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
                        oBook.GetSheet(2).Binding(sheet4Data);

                        IXVTWorksheet sheet = oBook.GetSheet(2);
                        sheet.SetCellValue("K6", "Đơn vị báo cáo: " + _globalInfo.SchoolName);
                        sheet.SetCellValue("K7", "Đơn vị nhận báo cáo : " + _globalInfo.SuperVisingDeptName);

                        DateTime nowDate = DateTime.Now;
                        string dateReport = "ngày " + nowDate.Day.ToString() + " tháng " + nowDate.Month.ToString() + " năm " + nowDate.Year.ToString();
                        sheet.SetCellValue("A6", dateReport);

                        string yearReportTitle = Year.ToString() + " - " + (Year + 1).ToString();
                        string reportTittle = "                             THỐNG KÊ HỌC SINH PHỔ THÔNG BỎ HỌC NĂM HỌC " + yearReportTitle;
                        sheet.SetCellValue("C1", reportTittle);

                        string solieu = "(Số liệu tính từ ngày 5 tháng 9 năm " + Year.ToString() + " đến ngày 05 tháng 9 năm " + (Year + 1).ToString() + ")";
                        sheet.SetCellValue("E2", solieu);
                    }
                    #endregion
                }
                #endregion

                //to stream xuat ra file
                Stream excel = oBook.ToStream();
                return File(excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ExportTemplateName);
                #endregion
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "ExportExcel", "null", ex);
            }
            return null;
        }

        private string GetReportCode(bool chckFacilities)
        {
            string reportCode = string.Empty;
            if (chckFacilities)
            {
                reportCode = SystemParamsInFile.REPORT_FACILITIES;
            }
            else
            {
                reportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS;
            }
            return reportCode;
        }
        #endregion
        #region Emis gửi phòng sở
        #region Thống kê điểm kiểm tra định kỳ
        [ValidateAntiForgeryToken]
        public PartialViewResult SearchPeriodStatistic(EmisReportViewModel model)
        {
            ViewData[ReportMenuDirectionConstant.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[ReportMenuDirectionConstant.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
            sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
            sfu.SchoolID = _globalInfo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
            sfu.FemaleType = model.FemaleType;
            sfu.EthnicType = model.EthnicType;
            sfu.FemaleEthnicType = model.FemaleEthnicType;

            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                listMarkStatistic = StatisticsForUnitBusiness.SearchPeriodicMarkForSecondary(sfu);
            }
            else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                listMarkStatistic = StatisticsForUnitBusiness.SearchPeriodicMarkForTertiary(sfu);
            }
            //List education
            List<int> listEducationId = listMarkStatistic.Where(p => p.EducationLevelID.HasValue).Select(p => p.EducationLevelID.Value).Distinct().ToList();
            List<EducationLevel> listEducation = EducationLevelBusiness.All.Where(p => listEducationId.Contains(p.EducationLevelID)).ToList();
            //List
            List<int> listSubjectID = listMarkStatistic.Select(p => p.SubjectID).Distinct().ToList();
            List<SubjectCat> listSubjectCat = SubjectCatBusiness.All.Where(p => listSubjectID.Contains(p.SubjectCatID)).ToList();

            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = listEducation.Where(p => p.EducationLevelID == ms.EducationLevelID).FirstOrDefault();
                ms.SubjectCat = listSubjectCat.Where(p => p.SubjectCatID == ms.SubjectID).FirstOrDefault();
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic.OrderBy(p => p.OrderCritea).ThenBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[ReportMenuDirectionConstant.GRID_STATISTICS_PERIOD] = listMarkStatistic;
            ViewData[ReportMenuDirectionConstant.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_PeriodGrid", model);
        }

        [ValidateAntiForgeryToken]
        public JsonResult SendPeriod(EmisReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                sfu.SchoolID = _globalInfo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.FemaleType = model.FemaleType;
                sfu.EthnicType = model.EthnicType;
                List<MarkStatisticBO> data = new List<MarkStatisticBO>();
                if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    data = StatisticsForUnitBusiness.SearchPeriodicMarkForSecondary(sfu);
                }
                else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    data = StatisticsForUnitBusiness.SearchPeriodicMarkForTertiary(sfu);
                }
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }
        #endregion
        #region Thống kê điểm kiểm tra học kỳ
        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSemesterStatistic(EmisReportViewModel model)
        {
            ViewData[ReportMenuDirectionConstant.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[ReportMenuDirectionConstant.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
            sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
            sfu.SchoolID = _globalInfo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
            sfu.FemaleType = model.FemaleType;
            sfu.EthnicType = model.EthnicType;
            sfu.FemaleEthnicType = model.FemaleEthnicType;
            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                listMarkStatistic = StatisticsForUnitBusiness.SearchSemesterMarkForSecondary(sfu);
            }
            else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                listMarkStatistic = StatisticsForUnitBusiness.SearchSemesterMarkForTertiary(sfu);
            }

            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic
                .OrderBy(o => o.OrderCritea)
                .ThenBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[ReportMenuDirectionConstant.GRID_STATISTICS_SEMESTER] = listMarkStatistic;
            ViewData[ReportMenuDirectionConstant.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_SemesterGrid", model);
        }

        [ValidateAntiForgeryToken]
        public JsonResult SendSemester(EmisReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_TERTIARY;
                sfu.SchoolID = _globalInfo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.FemaleType = model.FemaleType;
                sfu.EthnicType = model.EthnicType;
                List<MarkStatisticBO> data = new List<MarkStatisticBO>();
                if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    data = StatisticsForUnitBusiness.SearchSemesterMarkForSecondary(sfu);
                }
                else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    data = StatisticsForUnitBusiness.SearchSemesterMarkForTertiary(sfu);
                }
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        #endregion
        #region Thống kê học lực môn

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchCapacitySubjectStatistic(EmisReportViewModel model)
        {
            ViewData[ReportMenuDirectionConstant.LIST_JUDGE] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            ViewData[ReportMenuDirectionConstant.LIST_MARK] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);

            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
            sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
            sfu.SchoolID = _globalInfo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
            sfu.FemaleType = model.FemaleType;
            sfu.EthnicType = model.EthnicType;
            sfu.FemaleEthnicType = model.FemaleEthnicType;
            List<CapacityStatisticsBO> listCapacitySubjectStatistic = new List<CapacityStatisticsBO>();
            if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                listCapacitySubjectStatistic = StatisticsForUnitBusiness.SearchSubjectCapacityForSecondary(sfu);
            }
            else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                listCapacitySubjectStatistic = StatisticsForUnitBusiness.SearchSubjectCapacityForTertiary(sfu);
            }

            listCapacitySubjectStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });
            // Sap xep theo mon hoc
            listCapacitySubjectStatistic = listCapacitySubjectStatistic.OrderBy(p => p.OrderCritea).ThenBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            ViewData[ReportMenuDirectionConstant.GRID_STATISTICS_CAPACITY_SUBJECT] = listCapacitySubjectStatistic;
            ViewData[ReportMenuDirectionConstant.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listCapacitySubjectStatistic.Count);

            return PartialView("_CapacitySubjectStatisticGrid", model);
        }

        [ValidateAntiForgeryToken]
        public JsonResult SendCapacitySubject(EmisReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                sfu.SchoolID = _globalInfo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.FemaleType = model.FemaleType;
                sfu.EthnicType = model.EthnicType;
                List<CapacityStatisticsBO> data = new List<CapacityStatisticsBO>();
                if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    data = StatisticsForUnitBusiness.SearchSubjectCapacityForSecondary(sfu);
                }
                else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    data = StatisticsForUnitBusiness.SearchSubjectCapacityForTertiary(sfu);
                }
                CapacityStatisticBusiness.SendAll(data);
                CapacityStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }
        #endregion
        #region Thống kê học lực
        [ValidateAntiForgeryToken]
        public PartialViewResult SearchCapacityStatistic(EmisReportViewModel model)
        {
            ViewData[ReportMenuDirectionConstant.LIST_CAPACITY_LEVEL] = StatisticsConfigBusiness.GetList(_globalInfo.AppliedLevel.Value, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);

            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
            sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
            sfu.SchoolID = _globalInfo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
            sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;
            sfu.FemaleType = model.FemaleType;
            sfu.EthnicType = model.EthnicType;
            sfu.FemaleEthnicType = model.FemaleEthnicType;
            List<CapacityStatisticsBO> listData = new List<CapacityStatisticsBO>();
            if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                listData = StatisticsForUnitBusiness.SearchCapacityForSecondary(sfu);
            }
            else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                listData = StatisticsForUnitBusiness.SearchCapacityForTertiary(sfu);
            }

            listData.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubCommittee = SubCommitteeBusiness.Find(ms.SubCommitteeID);
            });

            ViewData[ReportMenuDirectionConstant.GRID_STATISTICS_CAPACITY] = listData;
            ViewData[ReportMenuDirectionConstant.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listData.Count);

            return PartialView("_CapacityStatisticGrid", model);
        }

        [ValidateAntiForgeryToken]
        public JsonResult SendCapacity(EmisReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                sfu.SchoolID = _globalInfo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;
                sfu.FemaleType = model.FemaleType;
                sfu.EthnicType = model.EthnicType;
                List<CapacityStatisticsBO> listData = new List<CapacityStatisticsBO>();
                if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    listData = StatisticsForUnitBusiness.SearchCapacityForSecondary(sfu);
                }
                else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    listData = StatisticsForUnitBusiness.SearchCapacityForTertiary(sfu);
                }
                CapacityStatisticBusiness.SendAll(listData);
                CapacityStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }
        #endregion
        #region Thống kê hạnh kiểm

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchConduct(EmisReportViewModel model)
        {
            ViewData[ReportMenuDirectionConstant.LIST_CONDUCT_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);

            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
            sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
            sfu.SchoolID = _globalInfo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
            sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;
            sfu.FemaleType = model.FemaleType;
            sfu.EthnicType = model.EthnicType;
            sfu.FemaleEthnicType = model.FemaleEthnicType;
            List<ConductStatisticsBO> listData = new List<ConductStatisticsBO>();
            if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                listData = StatisticsForUnitBusiness.SearchConductForSecondary(sfu);
            }
            else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                listData = StatisticsForUnitBusiness.SearchConductForTertiary(sfu);
            }
            listData.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubCommittee = SubCommitteeBusiness.Find(ms.SubCommitteeID);
            });

            ViewData[ReportMenuDirectionConstant.GRID_STATISTICS_CONDUCT] = listData;
            ViewData[ReportMenuDirectionConstant.COUNT_DATA] = ViewData[ReportMenuDirectionConstant.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listData.Count);

            return PartialView("_ConductStatisticGrid", model);
        }

        [ValidateAntiForgeryToken]
        public JsonResult SendConduct(EmisReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                sfu.SchoolID = _globalInfo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;
                sfu.SubCommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : (int)0;
                sfu.FemaleType = model.FemaleType;
                sfu.EthnicType = model.EthnicType;
                sfu.FemaleEthnicType = model.FemaleEthnicType;
                List<ConductStatisticsBO> listData = new List<ConductStatisticsBO>();
                if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    listData = StatisticsForUnitBusiness.SearchConductForSecondary(sfu);
                }
                else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    listData = StatisticsForUnitBusiness.SearchConductForTertiary(sfu);
                }

                //ConductStatisticBusiness.SendAll(listData);
                ConductStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitTertiary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        #endregion
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(EmisReportViewModel form)
        {
            string type = JsonReportMessage.NEW;
            StatisticReportBO objPeriodStatisticReport = new StatisticReportBO();
            ProcessedReport processedReport = null;
            #region Thống kê điểm kiểm tra định kỳ
            if (form.ReportTypeID == 1)
            {
                objPeriodStatisticReport.SemesterID = form.Semester;
                objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                objPeriodStatisticReport.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;

                string reportCode = SystemParamsInFile.THPT_THONGKE_DIEMKT_DINHKY;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.StatisticsForUnitBusiness.GetPeriodStatistic(objPeriodStatisticReport, form.ReportTypeID);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    if (ModelState.IsValid)
                    {
                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)form.Semester;
                        sfu.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;
                        sfu.FemaleType = form.FemaleType;
                        sfu.EthnicType = form.EthnicType;
                        sfu.FemaleEthnicType = form.FemaleEthnicType;
                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreatePeriodicMarkForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreatePeriodicMarkForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, form.ReportTypeID);
                        stream.Close();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            #endregion
            #region Thống kê điểm kiểm tra học kỳ
            else if (form.ReportTypeID == 2)
            {
                objPeriodStatisticReport.SemesterID = form.Semester;
                objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                objPeriodStatisticReport.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;

                string reportCode = SystemParamsInFile.THPT_THONGKE_DIEMKIEMTRA_HOCKY;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.StatisticsForUnitBusiness.GetPeriodStatistic(objPeriodStatisticReport, form.ReportTypeID);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    if (ModelState.IsValid)
                    {
                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)form.Semester;
                        sfu.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;
                        sfu.FemaleType = form.FemaleType;
                        sfu.EthnicType = form.EthnicType;
                        sfu.FemaleEthnicType = form.FemaleEthnicType;
                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateSemesterMarkForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateSemesterMarkForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, form.ReportTypeID);
                        stream.Close();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            #endregion
            #region thống kê học lực môn
            else if (form.ReportTypeID == 3)
            {
                objPeriodStatisticReport.SemesterID = form.Semester;
                objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                objPeriodStatisticReport.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;

                string reportCode = SystemParamsInFile.THPT_THONGKE_DIEMTBM;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.StatisticsForUnitBusiness.GetPeriodStatistic(objPeriodStatisticReport, form.ReportTypeID);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    if (ModelState.IsValid)
                    {
                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)form.Semester;
                        sfu.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;
                        sfu.FemaleType = form.FemaleType;
                        sfu.EthnicType = form.EthnicType;
                        sfu.FemaleEthnicType = form.FemaleEthnicType;
                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateSubjectCapacityForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateSubjectCapacityForTertiaryReport(sfu, out fileName);
                        }

                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, form.ReportTypeID);
                        stream.Close();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            #endregion
            #region Thống kê học lực
            else if (form.ReportTypeID == 4)
            {
                objPeriodStatisticReport.SemesterID = form.Semester;
                objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                objPeriodStatisticReport.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;

                string reportCode = SystemParamsInFile.THPT_THONGKE_XLHOCLUC_THEOBAN;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.StatisticsForUnitBusiness.GetPeriodStatistic(objPeriodStatisticReport, form.ReportTypeID);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    if (ModelState.IsValid)
                    {
                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)form.Semester;
                        sfu.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;
                        sfu.FemaleType = form.FemaleType;
                        sfu.EthnicType = form.EthnicType;
                        sfu.FemaleEthnicType = form.FemaleEthnicType;
                        sfu.SubCommitteeID = form.SubCommitteeID.HasValue ? form.SubCommitteeID.Value : 0;

                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateCapacityForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateCapacityForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, form.ReportTypeID);
                        stream.Close();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            #endregion
            #region Thống kê hạnh kiểm
            else if (form.ReportTypeID == 5)
            {
                objPeriodStatisticReport.SemesterID = form.Semester;
                objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                objPeriodStatisticReport.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;

                string reportCode = SystemParamsInFile.THPT_THONGKE_XLHANHKIEM_THEOBAN;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = this.StatisticsForUnitBusiness.GetPeriodStatistic(objPeriodStatisticReport, form.ReportTypeID);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    if (ModelState.IsValid)
                    {
                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)form.Semester;
                        sfu.EducationLevelID = form.EducationLevelID.HasValue ? form.EducationLevelID.Value : 0;
                        sfu.FemaleType = form.FemaleType;
                        sfu.EthnicType = form.EthnicType;
                        sfu.FemaleEthnicType = form.FemaleEthnicType;
                        sfu.SubCommitteeID = form.SubCommitteeID.HasValue ? form.SubCommitteeID.Value : 0;

                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateConductForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateConductForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, form.ReportTypeID);
                        stream.Close();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            #endregion
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(EmisReportViewModel frm)
        {
            ProcessedReport processedReport = new ProcessedReport();
            StatisticReportBO objPeriodStatisticReport = new StatisticReportBO();
            switch (frm.ReportTypeID)
            {
                case 1: //Thống kê điểm kiểm tra định kỳ
                    {
                        objPeriodStatisticReport.SemesterID = frm.Semester;
                        objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                        objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                        objPeriodStatisticReport.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;

                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)frm.Semester;
                        sfu.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;
                        sfu.FemaleType = frm.FemaleType;
                        sfu.EthnicType = frm.EthnicType;
                        sfu.FemaleEthnicType = frm.FemaleEthnicType;
                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreatePeriodicMarkForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreatePeriodicMarkForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, frm.ReportTypeID);
                        stream.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 2: //Thống kê điểm kiểm tra học kỳ
                    {
                        objPeriodStatisticReport.SemesterID = frm.Semester;
                        objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                        objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                        objPeriodStatisticReport.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;

                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)frm.Semester;
                        sfu.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;
                        sfu.FemaleType = frm.FemaleType;
                        sfu.EthnicType = frm.EthnicType;
                        sfu.FemaleEthnicType = frm.FemaleEthnicType;
                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateSemesterMarkForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateSemesterMarkForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, frm.ReportTypeID);
                        stream.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 3: //Thống kê học lực môn
                    {
                        objPeriodStatisticReport.SemesterID = frm.Semester;
                        objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                        objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                        objPeriodStatisticReport.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;

                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)frm.Semester;
                        sfu.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;
                        sfu.FemaleType = frm.FemaleType;
                        sfu.EthnicType = frm.EthnicType;
                        sfu.FemaleEthnicType = frm.FemaleEthnicType;
                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateSubjectCapacityForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateSubjectCapacityForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, frm.ReportTypeID);
                        stream.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 4: //Thống kê học lực
                    {
                        objPeriodStatisticReport.SemesterID = frm.Semester;
                        objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                        objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                        objPeriodStatisticReport.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;

                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)frm.Semester;
                        sfu.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;
                        sfu.FemaleType = frm.FemaleType;
                        sfu.EthnicType = frm.EthnicType;
                        sfu.FemaleEthnicType = frm.FemaleEthnicType;
                        sfu.SubCommitteeID = frm.SubCommitteeID.HasValue ? frm.SubCommitteeID.Value : 0;

                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateCapacityForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateCapacityForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, frm.ReportTypeID);
                        stream.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 5://Thống kê hạnh kiểm
                    {
                        objPeriodStatisticReport.SemesterID = frm.Semester;
                        objPeriodStatisticReport.SchoolID = _globalInfo.SchoolID.Value;
                        objPeriodStatisticReport.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objPeriodStatisticReport.AppliedLevelID = _globalInfo.AppliedLevel.Value;
                        objPeriodStatisticReport.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;

                        StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                        sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        sfu.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        sfu.SchoolID = _globalInfo.SchoolID.Value;
                        sfu.Semester = (int)frm.Semester;
                        sfu.EducationLevelID = frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0;
                        sfu.FemaleType = frm.FemaleType;
                        sfu.EthnicType = frm.EthnicType;
                        sfu.FemaleEthnicType = frm.FemaleEthnicType;
                        sfu.SubCommitteeID = frm.SubCommitteeID.HasValue ? frm.SubCommitteeID.Value : 0;

                        string fileName = string.Empty;
                        Stream stream = null;
                        if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateConductForSecondaryReport(sfu, out fileName);
                        }
                        else if (_globalInfo.AppliedLevel.Value == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            stream = StatisticsForUnitBusiness.CreateConductForTertiaryReport(sfu, out fileName);
                        }
                        processedReport = this.StatisticsForUnitBusiness.InsertPeriodStatistic(objPeriodStatisticReport, stream, fileName, frm.ReportTypeID);
                        stream.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;

        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion
        #region ExportPrimary
        [ValidateAntiForgeryToken]
        public JsonResult GetReportPrimary(SemesterMarkPrimaryViewModel view)
        {
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            string reportCode = string.Empty;
            if (view.ReportType == 1)//Thống kê CLGD
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["Semester"] = view.Semester;

                reportCode = SystemParamsInFile.REPORT_THONG_KE_CHAT_LUONG_GIAO_DUC;

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = EducationQualityStatisticBusiness.GetEducationQualityStatisticReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = EducationQualityStatisticBusiness.CreateEducationQualityStatisticReport(dic);
                    processedReport = EducationQualityStatisticBusiness.InsertEducationQualityStatisticReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                    excel.Close();
                }
            }
            else if (view.ReportType == 2)//Thống kê điểm kiểm tra cuối kỳ
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"EducationLevelID",view.EducationLevelID},
                    {"SemesterID",view.Semester}
                };

                reportCode = SystemParamsInFile.TH_THONGKE_DIEMKIEMTRA_HOCKY;
                ReportDefinition rdf = ReportDefinitionBusiness.GetByCode(reportCode);

                if (rdf.IsPreprocessed == true)
                {
                    processedReport = StatisticsForUnitBusiness.GetSemesterMarkPrimary(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                    sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                    sfu.SchoolID = _globalInfo.SchoolID.Value;
                    sfu.Semester = (int)view.Semester;
                    sfu.FemaleType = view.FemaleType;
                    sfu.EthnicType = view.EthnicType;
                    sfu.EducationLevelID = view.EducationLevelID.HasValue ? view.EducationLevelID.Value : 0;

                    string fileName = string.Empty;
                    Stream stream = StatisticsForUnitBusiness.CreateSemesterMarkForPrimaryReport(sfu, out fileName);
                    processedReport = StatisticsForUnitBusiness.InsertSemesterMarkPrimary(sfu, stream, fileName);
                    stream.Close();
                }
            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportPrimary(SemesterMarkPrimaryViewModel view)
        {
            ProcessedReport processedReport = null;
            if (view.ReportType == 1)//Thống kê CLGD
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["Semester"] = view.Semester;
                //dic["EducationLevelID"] = view.EducationLevelID;

                Stream excel = EducationQualityStatisticBusiness.CreateEducationQualityStatisticReport(dic);
                processedReport = EducationQualityStatisticBusiness.InsertEducationQualityStatisticReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
            }
            else if (view.ReportType == 2)//Thống kê điểm kiểm tra cuối kỳ
            {
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = _globalInfo.SchoolID.Value;
                sfu.Semester = (int)view.Semester;
                sfu.EducationLevelID = view.EducationLevelID.HasValue ? view.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateSemesterMarkForPrimaryReport(sfu, out fileName);
                processedReport = StatisticsForUnitBusiness.InsertSemesterMarkPrimary(sfu, stream, fileName);
                stream.Close();
            }

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        #endregion
        #region Search Primary
        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSemesterMarkPrimary(SemesterMarkPrimaryViewModel model)
        {
            ViewData[ReportMenuDirectionConstant.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[ReportMenuDirectionConstant.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.FemaleType = model.FemaleType;
            sfu.EthnicType = model.EthnicType;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;

            List<MarkStatisticBO> listMarkStatistic = StatisticsForUnitBusiness.SearchSemesterMarkForPrimary(sfu);

            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[ReportMenuDirectionConstant.GRID_STATISTICS_SEMESTER] = listMarkStatistic;
            ViewData[ReportMenuDirectionConstant.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_SemesterMarkPrimaryGrid", model);
        }
        [ValidateAntiForgeryToken]
        public JsonResult SendSemesterPrimary(SemesterMarkPrimaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = _globalInfo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = _globalInfo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.FemaleType = model.FemaleType;
                sfu.EthnicType = model.EthnicType;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;

                List<MarkStatisticBO> data = StatisticsForUnitBusiness.SearchSemesterMarkForPrimary(sfu);
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitPrimary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }
        #endregion
    }
}