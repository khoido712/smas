﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportMenuDirectionArea;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Pdf;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    public class InformationBookController : BaseController
    {

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IPupilGraduationBusiness PupilGraduationBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly IPupilOfSchoolBusiness PupilOfSchoolBusiness;
        private static int typeExport;
        public InformationBookController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IPupilGraduationBusiness PupilGraduationBusiness,
            ISchoolMovementBusiness SchoolMovementBusiness,
            IClassMovementBusiness ClassMovementBusiness,
            IPupilOfSchoolBusiness PupilOfSchoolBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.PupilGraduationBusiness = PupilGraduationBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.ClassMovementBusiness = ClassMovementBusiness;
            this.PupilOfSchoolBusiness = PupilOfSchoolBusiness;
        }


        //
        // GET: /ReportMenuDirectionArea/InformationBook/
        public ActionResult Index()
        {
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[ReportMenuDirectionConstant.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");

            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();
            return Json(lstClass);
        }

        #region Export Excel
        [ValidateAntiForgeryToken]
        public JsonResult GetInformationBook(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int typeExcel = !string.IsNullOrEmpty(frm["typeText"]) ? int.Parse(frm["typeText"]) : 0;

            typeExport = typeExcel;
            string strCode = string.Empty;
            if (typeExcel == 1)
                strCode = SystemParamsInFile.HS_THPT_SoDangBo;
            else
                strCode = SystemParamsInFile.PDF_HS_THPT_SoDangBo;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(strCode);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                {"ClassID",classID}
            };
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBusiness.GetProcessReportOfChilden(dic, strCode);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExportExcel(educationLevelID, classID, typeExcel);
                processedReport = PupilOfClassBusiness.InsertProcessInformationBook(dic, excel, strCode);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int typeExcel = !string.IsNullOrEmpty(frm["typeText"]) ? int.Parse(frm["typeText"]) : 0;

            typeExport = typeExcel;
            string strCode = string.Empty;
            if (typeExcel == 1)
                strCode = SystemParamsInFile.HS_THPT_SoDangBo;
            else
                strCode = SystemParamsInFile.PDF_HS_THPT_SoDangBo;

            ClassProfile objClassProFile = ClassProfileBusiness.Find(classID);
            ProcessedReport processedReport = null;
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                {"ClassID",classID}
            };
            Stream excel = this.ExportExcel(educationLevelID, classID, typeExcel);
            processedReport = PupilOfClassBusiness.InsertProcessInformationBook(dicInsert, excel, strCode);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            string strCode = string.Empty;
            if (typeExport == 1)
                strCode = SystemParamsInFile.HS_THPT_SoDangBo;
            else
                strCode = SystemParamsInFile.PDF_HS_THPT_SoDangBo;

            List<string> listRC = new List<string> { 
                strCode,
            };

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };

            string strCode = string.Empty;
            if (typeExport == 1)
                strCode = SystemParamsInFile.HS_THPT_SoDangBo;
            else
                strCode = SystemParamsInFile.PDF_HS_THPT_SoDangBo;

            List<string> listRC = new List<string> { 
                strCode,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream ExportExcel(int? educationLevelID, int? classID, int typeExcel)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", SystemParamsInFile.HS_THPT_SoDangBo + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet sheet = null;
            //lay danh sach lop hoc theo khoi
            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", academicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            dicClass.Add("EducationLevelID", educationLevelID);
            dicClass.Add("ClassProfileID", classID);
            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness
                                                        .SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                        .OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber)
                                                        .ThenBy(u => u.DisplayName).ToList();
            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevelID",_globalInfo.AppliedLevel}
            };

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);
            search.Add("EducationLevelID", educationLevelID);
            List<ClassProfile> lstClassProfileNoAcademic = this.ClassProfileBusiness
                                                        .SearchBySchool(_globalInfo.SchoolID.Value, search)
                                                        .OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber)
                                                        .ThenBy(u => u.DisplayName).ToList();

            List<AcademicYear> lstAca = AcademicYearBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value).ToList();

            //lay danh sach hoc sinh trong lop
            List<PupilOfClassBO> lstPupilOfClassBO = this.ListPupilOfClass(lstClassID);
            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
            List<int> lstPupilID = lstPupilOfClassBO.Select(p => p.PupilID).Distinct().ToList();

            //lay danh sach lop hoc nam hoc truoc cua hoc sinh
            AcademicYear objAca = AcademicYearBusiness.Find(academicYearID);
            string TitleYearUnder = objAca.DisplayTitle;
            // List<PupilOfClassBO> lstPOCUnderClass = new List<PupilOfClassBO>();
            //lstPOCUnderClass = this.ListPOCUnderClass(lstPupilID, TitleYearUnder);
            string className = string.Empty;

            List<EducationLevel> lstEdu = new GlobalInfo().EducationLevels;
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();

            if (educationLevelID.Value != 0)
                lstEducationLevel = lstEdu.Where(x => x.EducationLevelID == educationLevelID.Value).ToList();
            else
                lstEducationLevel = lstEdu;

            List<ClassProfile> lstCP = new List<ClassProfile>();

            List<PupilOfClassBO> listPupilOfClassNoAcademic = ListPupilOfClassBO();

            // danh sach hoc sinh dc cap bang tot nghiep
            List<PupilGraduation> lstPupilGraduation = ListPupilGraduation();

            // danh sach hoc sinh chuyen truong
            //List<SchoolMovementBO> lstSchoolMovementBO = ListSchoolMovementBO();

            // danh sach hoc sinh chuyen lop
            List<ClassMovement> lstClassMovement = ListClassMovement();
            foreach (var item in lstEducationLevel)
            {
                lstCP = lstClassProfile.Where(x => x.EducationLevelID == item.EducationLevelID).ToList();
                sheet = oBook.CopySheetToLast(secondSheet);
                this.SetValueToFile(
                    sheet,
                    lstCP,
                    lstClassProfileNoAcademic,
                    listPupilOfClassNoAcademic,
                    lstPupilOfClassBO,
                    lstPupilGraduation,
                    //lstSchoolMovementBO,
                    lstClassMovement,
                    lstAca,
                    TitleYearUnder);

                className = string.Empty;
                sheet.Name = item.Resolution + " - " + TitleYearUnder;
                sheet.PageSize = VTXPageSize.VTxlPaperA3;
                sheet.ZoomPage = 100;

                if (typeExcel == 1)
                {
                sheet.PageMaginBottom = 0.6;
                sheet.PageMaginTop = 0.6;
                }
                else
                {
                    sheet.PageMaginBottom = 0.5;
                    sheet.PageMaginTop = 0.5;
                }
                
                sheet.PageMaginLeft = 0.5;
                sheet.PageMaginRight = 0.5;
                sheet.SetRowHeight(5, 15);
                sheet.FitSheetOnTwoPage = true;
            }

            //bìa sổ đăng bộ
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            firstSheet.SetCellValue("A4", "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
            firstSheet.SetCellValue("A5", "Độc lập - Tự do - Hạnh phúc");
            firstSheet.SetCellValue("A6", "**********");

            firstSheet.SetCellValue("A23", "SỔ ĐĂNG BỘ");

            firstSheet.SetCellValue("D34", objSP.SchoolName);
            string comuneName = string.Empty;
            if (!string.IsNullOrEmpty(objSP.CommuneID.ToString()))
                comuneName = objSP.Commune.CommuneName;
            firstSheet.SetCellValue("D35", "Xã( phường, thị trấn): " + comuneName);
            firstSheet.SetCellValue("D36", "Huyện (quận, TX, TP thuộc tỉnh): " + objSP.District.DistrictName);
            firstSheet.SetCellValue("D37", "Tỉnh(thành phố): " + objSP.Province.ProvinceName);
            firstSheet.SetCellValue("N43", TitleYearUnder);           
            secondSheet.Delete();
            return oBook.ToStream(true);
        }

        // danh sach hoc sinh trong lop cua nam hien tai
        private List<PupilOfClassBO> ListPupilOfClass(List<int> lstClassID)
        {
            return (from poc in PupilOfClassBusiness.All
                    join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                    join cl in ClassProfileBusiness.All on poc.ClassID equals cl.ClassProfileID
                    join pos in PupilOfSchoolBusiness.All on pf.PupilProfileID equals pos.PupilID
                    where poc.AcademicYearID == _globalInfo.AcademicYearID.Value
                    && poc.SchoolID == _globalInfo.SchoolID.Value
                    && lstClassID.Contains(poc.ClassID)
                    && pf.IsActive == true
                    && pos.SchoolID == _globalInfo.SchoolID.Value
                    select new PupilOfClassBO
                    {
                        EducationLevelID = cl.EducationLevelID,
                        AcademicYearID = pf.CurrentAcademicYearID,
                        ProfileStatus = pf.ProfileStatus,
                        Status = poc.Status,
                        CurrentClassId = pf.CurrentClassID,
                        StorageNumber = pf.StorageNumber,
                        PupilID = poc.PupilID,
                        ClassID = poc.ClassID,
                        PupilFullName = poc.PupilProfile.FullName,
                        PupilCode = poc.PupilProfile.PupilCode,
                        Birthday = poc.PupilProfile.BirthDate,
                        Genre = pf.Genre,
                        EthnicName = poc.PupilProfile.Ethnic.EthnicName,
                        OrderInClass = poc.OrderInClass,
                        Name = pf.Name,
                        EnrolmentType = pos.EnrolmentType,
                        FatherJobName = pf.FatherJob,
                        MotherJobName = pf.MotherJob,
                        FatherFullName = pf.FatherFullName,
                        MotherFullName = pf.MotherFullName,
                        SponserJobName = pf.SponsorJob,
                        SponserFullName = pf.SponsorFullName,
                        PolicyTargetName = pf.PolicyTarget.Resolution,
                        PolicyTargetID  = pf.PolicyTargetID,    
                        PriorityTypeName = pf.PriorityType.Resolution,
                        PermanentResidentalAddress = pf.PermanentResidentalAddress, // thường trú
                        TempReidentalAddress = pf.TempResidentalAddress,// tạm trú
                        BirthPlace = pf.BirthPlace, // noi sinh
                    }).OrderBy(p => p.OrderInClass.HasValue ? p.OrderInClass : 0)
                                 .ThenBy(p => p.PupilFullName).ThenBy(p => p.Name).ToList();
        }
        // danh sach hoc sinh trong lop cua cac nam
        private List<PupilOfClassBO> ListPupilOfClassBO()
        {
            return (from poc in PupilOfClassBusiness.All
                    join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                    join ay in AcademicYearBusiness.All on poc.AcademicYearID equals ay.AcademicYearID
                    join cl in ClassProfileBusiness.All on poc.ClassID equals cl.ClassProfileID
                    where poc.SchoolID == _globalInfo.SchoolID.Value
                    && pf.IsActive == true
                    && ay.SchoolID == _globalInfo.SchoolID.Value
                    && cl.SchoolID == _globalInfo.SchoolID.Value
                    select new PupilOfClassBO
                    {
                        EducationLevelID = cl.EducationLevelID,
                        ProfileStatus = pf.ProfileStatus,
                        CurrentClassId = pf.CurrentClassID,
                        AcademicYearID = poc.AcademicYearID,
                        PupilID = poc.PupilID,
                        ClassID = poc.ClassID,
                        OrderInClass = poc.OrderInClass,
                        Name = pf.Name,
                        PupilFullName = poc.PupilProfile.FullName,
                        YearTitle = ay.Year,
                        ClassName = cl.DisplayName
                    }).OrderBy(p => p.OrderInClass.HasValue ? p.OrderInClass : 0)
                                 .ThenBy(p => p.PupilFullName).ThenBy(p => p.Name).ToList();
        }
        //danh sach hoc sinh tot nghiep
        private List<PupilGraduation> ListPupilGraduation()
        {
            return this.PupilGraduationBusiness.All
                .Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                    && (x.GraduationLevel == 3 || x.GraduationLevel == 2))
                .ToList();
        }
        //danh sach hoc sinh da chuyển trường
        private List<SchoolMovementBO> ListSchoolMovementBO()
        {
            return (from lst in SchoolMovementBusiness.All
                        .Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                            && x.AcademicYearID == _globalInfo.AcademicYearID.Value)
                    join pf in PupilProfileBusiness.All.Where(x => x.IsActive == true)
                    on lst.PupilID equals pf.PupilProfileID
                    select new SchoolMovementBO()
                    {
                        AcademicYearID = lst.AcademicYearID,
                        EducationLevelID = lst.EducationLevelID,
                        PupilID = lst.PupilID,
                        ClassID = lst.ClassID,
                    }).ToList();
        }
        // danh sach hoc sinh da chuyen lop, order by ngày chuyển desc
        private List<ClassMovement> ListClassMovement()
        {
            return ClassMovementBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value)
                .OrderByDescending(x => x.ClassMovementID).ToList();
        }

        private void SetValueToFile(
            IVTWorksheet sheet,
            List<ClassProfile> lstClassProfile,
            List<ClassProfile> lstClassProfileNoAcademic,
            List<PupilOfClassBO> listPupilOfClassNoAcademic,
            List<PupilOfClassBO> lstPupilOfClassBO,
            List<PupilGraduation> lstPupilGraduation,
            //List<SchoolMovementBO> lstSchoolMovementBO,
             List<ClassMovement> lstClassMovement,
            List<AcademicYear> lstAca,
            string TitleYearUnder)
        {
            #region fill du lieu
            //SỔ ĐĂNG BỘ
            sheet.GetRange(2, 1, 2, 2).Merge();
            sheet.GetRange(2, 1, 2, 2).SetHAlign(VTHAlign.xlHAlignLeft);
            sheet.SetCellValue("A2", "SỔ ĐĂNG BỘ");
            //NAM HỌC
            sheet.GetRange(3, 1, 3, 2).Merge();
            sheet.GetRange(3, 1, 3, 2).SetHAlign(VTHAlign.xlHAlignLeft);
            string year = string.Format("NĂM HỌC: {0}", TitleYearUnder);
            sheet.SetCellValue("A3", year);
            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
            List<PupilOfClassBO> lstPOCtmpNoAcademic = new List<PupilOfClassBO>();
            AcademicYear objAcaCurrentPOC = null;
            PupilGraduation objPupilGraduation = null;
            //SchoolMovementBO objSM = null;
            ClassMovement objCM = null;
            List<int> lstAcademic_Only = new List<int>();
            List<PupilOfClassBO> lstPOCtmpNoAcademic_Only = new List<PupilOfClassBO>();

            string yearTitle = string.Empty;
            string yearOut = string.Empty;

            int startRow = 6;
            string className = string.Empty;
            int startRowMergePupil = 0;
            foreach (var itemCP in lstClassProfile)
            {               
                lstPOCtmp = lstPupilOfClassBO.Where(p => p.ClassID == itemCP.ClassProfileID).ToList();
                if (lstPOCtmp.Count > 0)
                {
                    startRowMergePupil = startRow;
                    int start_Row = 0;
                    foreach (var itemPOC in lstPOCtmp)
                    {
                        objCM = lstClassMovement.Where(x => x.PupilID == itemPOC.PupilID).FirstOrDefault();
                            //&& x.EducationLevelID == itemCP.EducationLevelID).FirstOrDefault();

                        start_Row = startRow;
                        #region
                        int indexMerge = startRow;
                        // sổ đăng bộ
                        sheet.SetCellValue(startRow, 1, itemPOC.StorageNumber);
                        // họ ten
                        sheet.SetCellValue(startRow, 2, itemPOC.PupilFullName);
                        // gioi tinh
                        sheet.SetCellValue(startRow, 3, itemPOC.Genre.Value == 0 ? "Nữ" : "Nam");
                        // ngay sinh
                        sheet.SetCellValue(startRow, 4, String.Format("{0:dd/MM/yyyy}", itemPOC.Birthday));
                        // noi sinh khai sinh
                        sheet.SetCellValue(startRow, 5, itemPOC.BirthPlace);
                        //Dân tộc, con liệt sĩ, con thương binh (hạng)
                        string strOr = string.Empty;
                        string ethnicName = string.Empty;
                        string policyTargetName = string.Empty;
                        if (!string.IsNullOrEmpty(itemPOC.EthnicName))
                            strOr += itemPOC.EthnicName + ", ";
                        if (itemPOC.PolicyTargetID == 25 || itemPOC.PolicyTargetID == 26)
                        {
                            if(!string.IsNullOrEmpty(itemPOC.PolicyTargetName))
                                strOr += itemPOC.PolicyTargetName + ", ";
                        }
                        if (!string.IsNullOrEmpty(strOr))
                            strOr = strOr.Substring(0, strOr.Length - 2);

                        sheet.SetCellValue(startRow, 6, strOr);
                        //Chỗ ở hiện tại
                        string address = string.Empty;
                        if (!string.IsNullOrEmpty(itemPOC.TempReidentalAddress))
                            address = itemPOC.TempReidentalAddress;
                        else
                            address = itemPOC.PermanentResidentalAddress;
                        sheet.SetCellValue(startRow, 7, address);

                        //họ ten cha me, nguoi giam ho
                        string father = string.Empty;
                        if (!string.IsNullOrEmpty(itemPOC.FatherFullName))
                        {
                            father = itemPOC.FatherFullName;
                            if (!string.IsNullOrEmpty(itemPOC.FatherJobName))
                                father = string.Format("{0} - {1}", father, itemPOC.FatherJobName);
                        }

                        string mother = string.Empty;
                        if (!string.IsNullOrEmpty(itemPOC.MotherFullName))
                        {
                            mother = itemPOC.MotherFullName;
                            if (!string.IsNullOrEmpty(itemPOC.MotherJobName))
                                mother = string.Format("{0} - {1}", mother, itemPOC.MotherJobName);
                        }

                        string sponser = string.Empty;
                        if (!string.IsNullOrEmpty(itemPOC.SponserFullName))
                        {
                            sponser = itemPOC.SponserFullName;
                            if (!string.IsNullOrEmpty(itemPOC.SponserJobName))
                                sponser = string.Format("{0} - {1}", sponser, itemPOC.SponserJobName);
                        }

                        int a = 0;
                        if (!string.IsNullOrEmpty(father))
                        {
                            sheet.SetCellValue(startRow, 8, father);
                            sheet.GetRange(startRow, 8, startRow, 8).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            a = 1;
                            if (!string.IsNullOrEmpty(mother))
                            {
                                int b = startRow + 1;
                                sheet.SetCellValue(b, 8, mother);
                                sheet.GetRange(b, 8, b, 8).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                a = 2;
                                if (!string.IsNullOrEmpty(sponser))
                                {
                                    b++;
                                    sheet.SetCellValue(b, 8, sponser);
                                    sheet.GetRange(b, 8, b, 8).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                    a = 3;
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(mother))
                            {
                                sheet.SetCellValue(startRow, 8, mother);
                                if (!string.IsNullOrEmpty(sponser))
                                {
                                    int sp = startRow + 1;
                                    sheet.SetCellValue(sp, 8, sponser);
                                    sheet.GetRange(sp, 8, sp, 8).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                }
                            }                              
                            else
                            {
                                sheet.SetCellValue(startRow, 8, sponser);
                                sheet.GetRange(startRow, 8, startRow, 8).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            }
                        }

                        lstPOCtmpNoAcademic = listPupilOfClassNoAcademic
                            .Where(p => p.PupilID == itemPOC.PupilID).OrderBy(p => p.AcademicYearID).ToList();

                        int academic = 0;
                        int classEnd = 0;
                        int eduOld = 0;
                        if (objCM != null)
                        {
                            foreach (var itemNo in lstPOCtmpNoAcademic)
                            {
                                if (objCM.PupilID == itemNo.PupilID
                                    && objCM.AcademicYearID == itemNo.AcademicYearID
                                    && objCM.ToClassID == itemNo.ClassID)
                                {
                                    classEnd = itemNo.ClassID;
                                    academic = itemNo.AcademicYearID;
                                    eduOld = itemNo.EducationLevelID;
                                    break;
                                }
                            }

                            lstPOCtmpNoAcademic.RemoveAll(x => x.ClassID != classEnd
                                && x.AcademicYearID == academic
                                && x.EducationLevelID == eduOld);
                        }

                        string enrolmentType = string.Empty;
                        if (itemPOC.EnrolmentType.HasValue)
                        {
                            if (itemPOC.EnrolmentType.Value == SystemParamsInFile.ENROLMENT_TYPE_PASSED_EXAMINATION)
                                enrolmentType = "Trúng tuyển";
                            if (itemPOC.EnrolmentType.Value == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL)
                                enrolmentType = "Chuyển đến";
                            if (itemPOC.EnrolmentType.Value == SystemParamsInFile.ENROLMENT_TYPE_SELECTED)
                                enrolmentType = "Xét tuyển";
                            if (itemPOC.EnrolmentType.Value == SystemParamsInFile.ENROLMENT_TYPE_OTHER)
                                enrolmentType = "Hình thức khác";                           
                        }
                        sheet.SetCellValue(startRow, 11, enrolmentType);
                        sheet.GetRange(startRow, 11, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

                        objAcaCurrentPOC = lstAca.Where(x => x.AcademicYearID == itemPOC.AcademicYearID).FirstOrDefault();                      
                        
                        int startIndex = startRow;
                        foreach (var itemPOCNo in lstPOCtmpNoAcademic)
                        {                           
                            // năm học - vào trường
                            yearTitle = itemPOCNo != null && itemPOCNo.YearTitle>0 ? itemPOCNo.YearTitle.ToString() : string.Empty;
                            yearTitle = !string.IsNullOrEmpty(yearTitle) ? yearTitle.Substring(2) : "";
                            int _year = SMAS.Business.Common.Utils.GetInt(yearTitle);
                            sheet.SetCellValue(startIndex, 9, ((_year) + " - " + (_year + 1)));
                            sheet.GetRange(startIndex, 9, startIndex, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            // lớp - vào trường
                            sheet.SetCellValue(startIndex, 10, itemPOCNo.ClassName);
                            sheet.GetRange(startIndex, 10, startIndex, 10).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                           
                            // ra truong
                            if (itemPOCNo.EducationLevelID == 9 || itemPOCNo.EducationLevelID == 12)
                            {
                                if (itemPOC.Status == 2)
                                {
                                    yearOut = objAcaCurrentPOC.Year.ToString();
                                    yearOut = yearOut.Substring(2);
                                    int _yearOut = SMAS.Business.Common.Utils.GetInt(yearOut);
                                    sheet.SetCellValue(startIndex, 12, ((_yearOut) + " - " + (_yearOut + 1)));
                                    sheet.SetCellValue(startIndex, 13, itemPOCNo.EducationLevelID);
                                    if (itemPOCNo.EducationLevelID == 9)
                                        sheet.SetCellValue(startIndex, 14, "TNTHCS");
                                    else
                                        sheet.SetCellValue(startIndex, 14, "TNTHPT");

                                    // bang tot nghiẹp
                                    objPupilGraduation = lstPupilGraduation.Where(x => x.PupilID == itemPOC.PupilID).FirstOrDefault();
                                    if (objPupilGraduation != null)
                                    {
                                        sheet.SetCellValue(startIndex, 16, "Chính quy");
                                        sheet.SetCellValue(startIndex, 17, objPupilGraduation.IssuedNumber);
                                        sheet.SetCellValue(startIndex, 18, String.Format("{0:dd/MM/yyyy}", objPupilGraduation.IssuedDate));
                                        //sheet.GetRange(startIndex, 18, startIndex, 18).SetHAlign(VTHAlign.xlHAlignLeft);
                                        sheet.GetRange(startIndex, 16, startIndex, 18).SetHAlign(VTHAlign.xlHAlignCenter);
                                    }
                                    yearOut = string.Empty;
                                    sheet.GetRange(startIndex, 12, startIndex, 18).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                }
                            }
                            sheet.GetRange(startIndex, 9, startIndex, 14).SetHAlign(VTHAlign.xlHAlignCenter);
                            startIndex++;
                            yearTitle = string.Empty;
                        }

                        if (itemPOC.Status == 4 || itemPOC.Status == 3)
                        {
                            yearOut = objAcaCurrentPOC != null ? objAcaCurrentPOC.Year.ToString() : "";
                            yearOut = !string.IsNullOrEmpty(yearOut) ? yearOut.Substring(2) : "";
                            int _yearOut = SMAS.Business.Common.Utils.GetInt(yearOut);
                            sheet.SetCellValue((startIndex - 1), 12, ((_yearOut) + " - " + (_yearOut + 1)));
                            sheet.SetCellValue((startIndex - 1), 13, lstPOCtmpNoAcademic[(lstPOCtmpNoAcademic.Count - 1)].EducationLevelID);

                            string strStatus = string.Empty;
                            if (itemPOC.Status == 4)
                                strStatus = "Thôi học";
                            else
                                strStatus = "Chuyển trường";
                            sheet.SetCellValue((startIndex - 1), 14, strStatus);

                            sheet.GetRange(startIndex - 1, 12, startIndex - 1, 12).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            sheet.GetRange(startIndex - 1, 13, startIndex - 1, 13).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            sheet.GetRange(startIndex - 1, 14, startIndex - 1, 14).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            yearOut = string.Empty;
                        }

                        startRow = startRow + 4;
                        if (a != 0 || lstPOCtmpNoAcademic.Count != 0)
                        {
                            for (int i = 1; i <= 7; i++)
                            {
                                sheet.GetRange(indexMerge, i, startRow, i).Merge();
                                if (i == 1)
                                    sheet.GetRange(indexMerge, i, startRow, i).SetHAlign(VTHAlign.xlHAlignCenter);
                                else
                                    sheet.GetRange(indexMerge, i, startRow, i).SetHAlign(VTHAlign.xlHAlignLeft);

                                if (i == 2 || i == 5 || i == 6 || i == 7)
                                {
                                    sheet.GetRange(indexMerge, i, startRow, i).SetHAlign(VTHAlign.xlHAlignLeft);
                                    sheet.GetRange(indexMerge, i, startRow, i).WrapText();
                                }
                            }
                        }

                        #endregion
                        sheet.GetRange(start_Row, 1, startRow, 18).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                        sheet.GetRange(start_Row, 8, startRow, 18).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.InsideHorizontal);
                        startRow++;
                    }
                }
            }
            sheet.SetFontName("Times New Roman", 0);
            #endregion
        }
        #endregion
    }
}
