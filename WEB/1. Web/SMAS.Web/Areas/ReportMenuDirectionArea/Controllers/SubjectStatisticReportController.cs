﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportMenuDirectionArea.Models;
using SMAS.Web.Areas.ReportMenuDirectionArea;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.Web.Filter;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class SubjectStatisticReportController : BaseController
    {
        #region contructor
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        public readonly IClassSubjectBusiness ClassSubjectBusiness;
        public readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        public readonly IPupilRewardReportBusiness PupilRewardReportBusiness;
        public readonly IProcessedReportBusiness ProcessedReportBusiness;
        public readonly IPupilRetestRegistrationBusiness PupilRetestRegistrationBusiness;
        public readonly IPupilRetestBusiness PupilRetestBusiness;
        public readonly IMarkOfSemesterBusiness MarkOfSemesterBusiness;
        public readonly IMarkOfSemesterBySubjectBusiness MarkOfSemesterBySubjectBusiness;
        public readonly IMarkOfSemesterByJudgeSubjectBusiness MarkOfSemesterByJudgeSubjectBusiness;
        public readonly IAverageMarkBySubjectBusiness AverageMarkBySubjectBusiness;
        public readonly IDetailClassifiedBySubjectBusiness DetailClassifiedBySubjectBusiness;
        public readonly ISubjectCatBusiness SubjectCatBusiness;
        public readonly IReportResultEndOfYearBusiness ReportResultEndOfYearBusiness;
        public readonly IClassifiedCapacityBusiness ClassifiedCapacityBusiness;
        public readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        public readonly IAcademicYearBusiness AcademicYearBusiness;
        public readonly IStatisticLevelReportBusiness StatisticLevelReportBusiness;
        public readonly IClassifiedConductBusiness ClassifiedConductBusiness;
        public readonly IClassifiedCapacityBySubjectBusiness ClassifiedCapacityBySubjectBusiness;
        public readonly ISynthesisCapacityConductBusiness SynthesisCapacityConductBusiness;
        public readonly ISynthesisSubjectByEducationLevelBusiness SynthesisSubjectByEducationLevelBusiness;
        public readonly IEmulationTitleBusiness EmulationTitleBusiness;
        public readonly IStatisticLevelConfigBusiness StatisticLevelConfigBusiness;
        private readonly IReportSynthesisCapacityConductByPeriodBusiness ReportSynthesisCapacityConductByPeriodBusiness;
        public readonly IAverageMarkByPeriodBusiness AverageMarkByPeriodBusiness;
        public readonly IDetailClassifiedSubjectByPeriodBusiness DetailClassifiedSubjectByPeriodBusiness;
        public readonly IClassifiedCapacityByPeriodBusiness ClassifiedCapacityByPeriodBusiness;
        public readonly IClassifiedConductByPeriodBusiness ClassifiedConductByPeriodBusiness;
        public readonly ISynthesisSubjectByPeriodBusiness SynthesisSubjectByPeriodBusiness;
        public readonly ISynthesisClassifiedSubjectByPeriodBusiness SynthesisClassifiedSubjectByPeriodBusiness;
        public readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        public readonly IEmployeeBusiness EmployeeBusiness;
        public readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        #endregion

        public SubjectStatisticReportController(IEmulationTitleBusiness EmulationTitleBusiness, IReportResultEndOfYearBusiness ReportResultEndOfYearBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness, IPupilRewardReportBusiness pupilRewardReportBusiness, IProcessedReportBusiness processedReportBusiness,
            IPupilRetestRegistrationBusiness pupilRetestRegistrationBusiness, IPupilRetestBusiness pupilRetestBusiness, IMarkOfSemesterBusiness markOfSemesterBusiness,
            IMarkOfSemesterBySubjectBusiness markOfSemesterBySubjectBusiness, IMarkOfSemesterByJudgeSubjectBusiness markOfSemesterByJudgeSubjectBusiness,
            IAverageMarkBySubjectBusiness averageMarkBySubjectBusiness, IDetailClassifiedBySubjectBusiness detailClassifiedBySubjectBusiness, IClassifiedCapacityBusiness classifiedcapacityBusiness,
            IClassifiedConductBusiness ClassifiedConductBusiness, IClassifiedCapacityBySubjectBusiness ClassifiedCapacityBySubjectBusiness, ISynthesisCapacityConductBusiness SynthesisCapacityConductBusiness,
            ISynthesisSubjectByEducationLevelBusiness SynthesisSubjectByEducationLevelBusiness, ISubjectCatBusiness subjectCatBusiness, ISchoolSubjectBusiness schoolSubjectBusiness, IAcademicYearBusiness academicYearBusiness, IStatisticLevelReportBusiness statisticLevelReportBusiness, IStatisticLevelConfigBusiness StatisticLevelConfigBusiness,
            ISemeterDeclarationBusiness SemesterDeclarationBusiness, IEmployeeBusiness EmployeeBusiness, ITeachingAssignmentBusiness TeachingAssignmentBusiness,
            IReportSynthesisCapacityConductByPeriodBusiness ReportSynthesisCapacityConductByPeriodBusiness, IAverageMarkByPeriodBusiness AverageMarkByPeriodBusiness,
            IDetailClassifiedSubjectByPeriodBusiness DetailClassifiedSubjectByPeriodBusiness, IClassifiedCapacityByPeriodBusiness ClassifiedCapacityByPeriodBusiness,
            IClassifiedConductByPeriodBusiness ClassifiedConductByPeriodBusiness,
            ISynthesisSubjectByPeriodBusiness SynthesisSubjectByPeriodBusiness,
            ISynthesisClassifiedSubjectByPeriodBusiness SynthesisClassifiedSubjectByPeriodBusiness)
        {
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassifiedCapacityBusiness = classifiedcapacityBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.PupilRewardReportBusiness = pupilRewardReportBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ClassifiedConductBusiness = ClassifiedConductBusiness;
            this.ClassifiedCapacityBySubjectBusiness = ClassifiedCapacityBySubjectBusiness;
            this.SynthesisCapacityConductBusiness = SynthesisCapacityConductBusiness;
            this.SynthesisSubjectByEducationLevelBusiness = SynthesisSubjectByEducationLevelBusiness;
            this.PupilRetestRegistrationBusiness = pupilRetestRegistrationBusiness;
            this.PupilRetestBusiness = pupilRetestBusiness;
            this.MarkOfSemesterBusiness = markOfSemesterBusiness;
            this.MarkOfSemesterBySubjectBusiness = markOfSemesterBySubjectBusiness;
            this.MarkOfSemesterByJudgeSubjectBusiness = markOfSemesterByJudgeSubjectBusiness;
            this.AverageMarkBySubjectBusiness = averageMarkBySubjectBusiness;
            this.DetailClassifiedBySubjectBusiness = detailClassifiedBySubjectBusiness;
            this.EmulationTitleBusiness = EmulationTitleBusiness;
            this.ReportResultEndOfYearBusiness = ReportResultEndOfYearBusiness;
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.StatisticLevelReportBusiness = statisticLevelReportBusiness;
            this.StatisticLevelConfigBusiness = StatisticLevelConfigBusiness;
            this.SemesterDeclarationBusiness = SemesterDeclarationBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.ReportSynthesisCapacityConductByPeriodBusiness = ReportSynthesisCapacityConductByPeriodBusiness;
            this.AverageMarkByPeriodBusiness = AverageMarkByPeriodBusiness;
            this.DetailClassifiedSubjectByPeriodBusiness = DetailClassifiedSubjectByPeriodBusiness;
            this.ClassifiedCapacityByPeriodBusiness = ClassifiedCapacityByPeriodBusiness;
            this.ClassifiedConductByPeriodBusiness = ClassifiedConductByPeriodBusiness;
            this.SynthesisSubjectByPeriodBusiness = SynthesisSubjectByPeriodBusiness;
            this.SynthesisClassifiedSubjectByPeriodBusiness = SynthesisClassifiedSubjectByPeriodBusiness;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _index()
        {
            return PartialView();
        }
        public PartialViewResult LoadMarkReport()//Thong ke bai kiem tra dinh ky
        {
            SetViewData(true);
            return PartialView("_MarkReport");
        }
        public PartialViewResult LoadClassificationCapacityReport()//Thong ke xep loai hoc luc mon
        {
            SetViewData();
            return PartialView("_ClassificationCapacity");
        }
        public PartialViewResult LoadClassificationCapacityDetailReport()//Thong ke xep loai chi tiet theo mon
        {
            SetViewData();
            return PartialView("_ClassificationCapacityDetail");
        }
        public PartialViewResult LoadGreneralSubjectByEducationReport()//Thong ke tong hop cac mon theo khoi
        {
            SetViewData();
            return PartialView("_GreneralSubjectByEducation");
        }
        public PartialViewResult LoadSemesterGradeReport()//Thong ke diem thi hoc ky
        {
            SetViewData();
            return PartialView("_SemesterGrade");
        }
        public PartialViewResult LoadSemesterGradeBySubjectReport()//Thong ke diem thi hoc ky theo mon
        {
            SetViewData();
            return PartialView("_SemesterGradeBySubject");
        }

        public void SetViewData(bool isVnenSubjectFilter = false)
        {
            //cbb hoc ky
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            int Semester = DateTime.Now < acaYear.SecondSemesterStartDate.Value
                                ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                : (DateTime.Now < acaYear.SecondSemesterEndDate.Value
                                        ? SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        : SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
            ViewData[ReportMenuDirectionConstant.DEFAULT_SEMESTER] = Semester;

            ViewData[ReportMenuDirectionConstant.CBO_SEMESTER] = CommonList.SemesterAndAll();
            ViewData[ReportMenuDirectionConstant.CBO_PERIOD] = new List<PeriodDeclaration>();

            // Neu co thong tin ve hoc ky thi lay luon thong tin ve dot
            if (_globalInfo.Semester.HasValue)
            {
                IDictionary<string, object> PeriodSearchInfo = new Dictionary<string, object>();
                PeriodSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                PeriodSearchInfo["Semester"] = _globalInfo.Semester.Value;
                List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, PeriodSearchInfo).ToList();
                int a = 0;
                if (lstPeriod != null && lstPeriod.Count() > 0)
                {
                    a = 1;
                    for (int k = 0; k < lstPeriod.Count; k++)
                    {

                        PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                        periodDeclaration = lstPeriod[k];
                        if (periodDeclaration.FromDate < DateTime.Now && periodDeclaration.EndDate > DateTime.Now)
                        {
                            a = k;
                        }
                    }
                }
                ViewData[ReportMenuDirectionConstant.CHECK_DEFAULT] = a;
                ViewData[ReportMenuDirectionConstant.CBO_PERIOD] = new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", a);
            }

            ViewData[ReportMenuDirectionConstant.CBO_EDUCATIONLEVEL] = _globalInfo.EducationLevels;

            IQueryable<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value,
                                                                                new Dictionary<string, object> { 
                                                                                    { "AppliedLevel", _globalInfo.AppliedLevel.Value }
                                                                                });

            List<SubjectCat> lstClassSubject = listSchoolSubject.Select(o => o.SubjectCat).Distinct().ToList();
            lstClassSubject = lstClassSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            List<SubjectViewModel> lstSchoolSubjectModel = listSchoolSubject
                                                            .Select(u => new SubjectViewModel
                                                            {
                                                                SubjectCatID = u.SubjectID,
                                                                DisplayName = u.SubjectCat.DisplayName,
                                                                OrderInSubject = u.SubjectCat.OrderInSubject
                                                                //IsCommenting = u.IsCommenting.Value
                                                            }).Distinct()
                                                            .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName)
                                                            .ToList();

            //Viethd4: Lay cac mon duoc cac lop hoc theo TT58
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (isVnenSubjectFilter)
            {
                dic["IsVNEN"] = true;
            }
            List<int> lstTT58SubjectId = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Select(o => o.SubjectID).Distinct().ToList();

            //viethd4: Loc bo cac mon khong co lop nao hoc theo TT58
            lstSchoolSubjectModel = lstSchoolSubjectModel.Where(o => lstTT58SubjectId.Contains(o.SubjectCatID)).ToList();

            ViewData[ReportMenuDirectionConstant.List_SUBJECT] = lstSchoolSubjectModel;

            ViewData[ReportMenuDirectionConstant.CBO_SUBJECT] = lstClassSubject;
            //lấy ra mức thống kê
            List<StatisticLevelReport> lstStatisticLevel = StatisticLevelReportBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            lstStatisticLevel = lstStatisticLevel.OrderBy(o => o.Resolution).ToList();
            if (lstStatisticLevel == null)
            {
                lstStatisticLevel = new List<StatisticLevelReport>();
            }
            ViewData[ReportMenuDirectionConstant.CBO_STATISTIC_LEVEL] = lstStatisticLevel;
            StatisticLevelReport default_statistic_level = lstStatisticLevel.Where(o => o.Resolution.ToLower().Contains("chuẩn")).FirstOrDefault();
            int statictic_id_default = default_statistic_level != null ? default_statistic_level.StatisticLevelReportID : 0;
            ViewData[ReportMenuDirectionConstant.DEFAULT_STATSICTIC_LEVEL] = statictic_id_default;

            //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
            //Lay danh sach loai diem
            List<ComboObject> lstMarkTypes = new List<ComboObject>();
            lstMarkTypes.Add(new ComboObject(ReportMenuDirectionConstant.MARK_TYPE_PERIODIC.ToString(), Res.Get("ReportResultLearningBySemester_Label_MarkTypePeriod")));
            lstMarkTypes.Add(new ComboObject(ReportMenuDirectionConstant.MARK_TYPE_AVERAGE.ToString(), Res.Get("ReportResultLearningBySemester_Label_MarkTypeAverage")));
            ViewData[ReportMenuDirectionConstant.CBO_MARK_TYPE] = lstMarkTypes;


            //Lay danh sach cot diem
            //Lay ra SemesterDeclaration
            Dictionary<string, object> dicToGetSD = new Dictionary<string, object>();
            dicToGetSD["SchoolID"] = _globalInfo.SchoolID.Value;
            dicToGetSD["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicToGetSD["Semester"] = Semester;

            IQueryable<SemeterDeclaration> lstSD = SemesterDeclarationBusiness.Search(dicToGetSD).OrderByDescending(o => o.AppliedDate);
            SemeterDeclaration sd = lstSD.FirstOrDefault();
            //So con diem mieng
            int interviewMarkNum = 0;
            //So con diem viet he so 1
            int writingMarkNum = 0;
            //So con diem viet he so 2
            int twiceCoeffiecientMarkNum = 0;
            if (sd != null)
            {
                //So con diem mieng
                interviewMarkNum = sd.InterviewMark;
                //So con diem viet he so 1
                writingMarkNum = sd.WritingMark;
                //So con diem viet he so 2
                twiceCoeffiecientMarkNum = sd.TwiceCoeffiecientMark;
            }
            List<ComboObject> lstMarkColumn = new List<ComboObject>();
            string markColumn = string.Empty;

            for (int i = 1; i <= interviewMarkNum; i++)
            {
                markColumn = "M" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            for (int i = 1; i <= writingMarkNum; i++)
            {
                markColumn = "P" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            for (int i = 1; i <= twiceCoeffiecientMarkNum; i++)
            {
                markColumn = "V" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            lstMarkColumn.Add(new ComboObject("HK", "KTHK"));


            ViewData[ReportMenuDirectionConstant.CBO_MARK_COLUMN] = lstMarkColumn;

            if (lstSchoolSubjectModel != null && lstSchoolSubjectModel.Count > 0)
            {
                //Lay danh sach giao vien trong truong
                Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
                dicToGetTeacher["CurrentSchoolID"] = _globalInfo.SchoolID;
                IQueryable<Employee> query = EmployeeBusiness.Search(dicToGetTeacher);

                //Lay danh sach giao vien duong phan cong day mon duoc chon trong hoc ky duoc chon
                IDictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
                dicToGetAssignment["AcademicYearID"] = _globalInfo.AcademicYearID;
                dicToGetAssignment["SubjectID"] = lstSchoolSubjectModel[0].SubjectCatID;
                if (Semester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    dicToGetAssignment["Semester"] = Semester;
                }
                IQueryable<TeachingAssignment> lstTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicToGetAssignment);

                IQueryable<EmployeeCboModel> lstComboEmployee = from q in query
                                                                where lstTeachingAssignment.Any(o => o.TeacherID == q.EmployeeID)
                                                                select new EmployeeCboModel
                                                                {
                                                                    EmployeeID = q.EmployeeID,
                                                                    DisplayName = q.FullName + " - " + q.EmployeeCode

                                                                };

                ViewData[ReportMenuDirectionConstant.CBO_EMPLOYEE] = lstComboEmployee;
            }
            else
            {
                ViewData[ReportMenuDirectionConstant.CBO_EMPLOYEE] = new List<EmployeeCboModel>();
            }
            //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM

            #region Radio Report Semester
            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();

            //Khen thưởng
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_EmulationList");
            viettelradio.cchecked = true;
            viettelradio.disabled = false;
            viettelradio.Value = 1;
            lsradio.Add(viettelradio);

            //Danh sách thi lại
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_PupilRetest");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 2;
            lsradio.Add(viettelradio);

            //Danh sách đăng ký thi lại
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_PupilRetestRegistration");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 3;
            lsradio.Add(viettelradio);

            //Thống kê điểm thi học kỳ
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_MarkOfSemester");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 4;
            lsradio.Add(viettelradio);

            //Thông kê điểm thi học kỳ theo môn tính điểm và nhận xét
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_MarkOfSemesterBySubject");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 5;
            lsradio.Add(viettelradio);

            //Thống kê điểm trung bình môn
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_Mark");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 6;
            lsradio.Add(viettelradio);

            //Thống kê xếp loại chi tiết theo môn   
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_DetailBySubject");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 7;
            lsradio.Add(viettelradio);

            //Thống kê xếp loại học lực
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_CapacityLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 8;
            lsradio.Add(viettelradio);

            //Thống kê xếp loại hạnh kiểm
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_ConductLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 9;
            lsradio.Add(viettelradio);

            //Thông kê xếp loại học lực môn
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_CapacityLevelBySubject");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 10;
            lsradio.Add(viettelradio);

            //Thống kê tổng hợp các môn theo khối
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_SubjectByEducationLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 11;
            lsradio.Add(viettelradio);

            //Thống kê tổng hợp học lực hạnh kiểm
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_CapacityConductByEducationLevel");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 12;
            lsradio.Add(viettelradio);

            //Thống kê danh hiệu thi đua
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_Emulation");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 13;
            lsradio.Add(viettelradio);

            //Thông kê kết quả cuối năm
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportResultLearningBySemester_Label_ResultAtTheEndOfTheYear");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 14;
            lsradio.Add(viettelradio);
            ViewData[ReportMenuDirectionConstant.LIST_RADIOREPORTSEMESTER] = lsradio;
            #endregion

            #region Radio Report Period
            List<ViettelCheckboxList> lsradioPeriod = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradioPeriod = new ViettelCheckboxList();

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_SubjectAverageMark");
            viettelradioPeriod.cchecked = true;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 1;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_DetailBySubject");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 2;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityLevel");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 3;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_ConductLevel");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 4;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityLevelBySubject");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 5;
            lsradioPeriod.Add(viettelradioPeriod);
            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_CapacityConductByEducationLevel");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 6;
            lsradioPeriod.Add(viettelradioPeriod);

            viettelradioPeriod = new ViettelCheckboxList();
            viettelradioPeriod.Label = Res.Get("ReportResultLearningByPeriod_Label_SubjectRankingStatistic");
            viettelradioPeriod.cchecked = false;
            viettelradioPeriod.disabled = false;
            viettelradioPeriod.Value = 7;
            lsradioPeriod.Add(viettelradioPeriod);
            ViewData[ReportMenuDirectionConstant.LIST_RADIOREPORT] = lsradioPeriod;
            #endregion
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPeriod(int? idSemester)
        {
            if (idSemester == null)
            {
                return Json(new SelectList(new List<PeriodDeclaration>(), "PeriodDeclarationID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            if (idSemester == 0)
            {
                idSemester = 3;
            }
            //-	cboPeriod: Lấy ra danh sách các đợt bằng cách gọi hàm PeriodDeclarationBusiness.SearchBySemester() với các tham số truyền vào:
            //+ AcadmicYearID: UserInfo.AcademicYear
            //+ SchoolID: UserInfo.SchoolID
            //+ SemesterID: cboSemester.SelectedValue
            //Kết quả thu được lstPeriodBySemester. Giá trị mặc định: Kiểm tra ngày hiện tại với ngày bắt đầu và ngày kết thúc của đợt để mặc định đợt hiện tại.

            //cbo Periord
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = _globalInfo.AcademicYearID;
            Dictionary["SchoolID"] = _globalInfo.SchoolID;
            Dictionary["Semester"] = idSemester;

            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySemester(Dictionary).OrderByDescending(p => p.EndDate).ToList();
            int periodSelected = 0;
            for (int i = 0; i < lstPeriod.Count; i++)
            {
                periodSelected = 1;
                PeriodDeclaration pd = lstPeriod[i];
                if (pd.FromDate.HasValue && pd.EndDate.HasValue)
                {
                    if (pd.FromDate < DateTime.Now && pd.EndDate > DateTime.Now)
                    {
                        periodSelected = pd.PeriodDeclarationID;
                        break;
                    }
                }
            }
            return Json(new SelectList(lstPeriod, "PeriodDeclarationID", "Resolution", periodSelected), JsonRequestBehavior.AllowGet);

        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReportBySemester(SubjectReportViewModel form)
        {
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            List<StatisticLevelConfig> lstStatisticLevelConfig = new List<StatisticLevelConfig>();
            if (form.rdoReportBySemester == 4 || form.rdoReportBySemester == 5 || form.rdoReportBySemester == 6 || form.rdoReportBySemester == 8 || form.rdoReportBySemester == 7 || form.rdoReportBySemester == 11)
            {
                if (form.StatisticLevelReportID.HasValue)
                {
                    lstStatisticLevelConfig = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", form.StatisticLevelReportID.Value } }).ToList();
                }
            }
            #region xuất excel theo đợt
            if (form.chkPeriod == 1)
            {
                #region Thống kê điểm trung bình môn - AuNH
                if (form.rdoReportBySemester == 6)
                {
                    //Nhom theo khoi
                    #region nhóm theo khối
                    if (form.GroupBy == 1)
                    {
                        if (!form.Subject.HasValue)
                        {
                            throw new BusinessException("ReportResultLearningBySemester_Validate_Subject");
                        }
                        SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { 
                                                                                        { "AcademicYearID", _globalInfo.AcademicYearID.Value } ,
                                                                                        { "SubjectID", form.Subject.Value }
                                                                                        }).FirstOrDefault();

                        if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                            {
                                throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                            }
                        }

                        type = JsonReportMessage.NEW;
                        TranscriptOfClass entity = new TranscriptOfClass();
                        entity.SchoolID = _globalInfo.SchoolID.Value;
                        entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        entity.SubjectID = form.Subject.HasValue ? form.Subject.Value : 0;
                        entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        entity.PeriodDeclarationID = form.Period.HasValue ? form.Period.Value : 0;
                        entity.Semester = form.Semester.HasValue ? form.Semester.Value : 0;
                        entity.TeacherID = form.EmployeeID.HasValue ? form.EmployeeID.Value : 0;
                        entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                        entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                        entity.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? int.Parse(form.FemaleEthnicType) : 0;
                        if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            entity.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                        }

                        processedReport = AverageMarkByPeriodBusiness.GetAverageMarkByPeriod(entity);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }

                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = AverageMarkByPeriodBusiness.CreateAverageMarkByPeriod(entity);
                            processedReport = AverageMarkByPeriodBusiness.InsertAverageMarkByPeriod(entity, excel);
                            excel.Close();
                        }
                    }
                    #endregion
                    //Nhom theo giao vien
                    #region nhóm theo giáo viên
                    else
                    {
                        AverageMarkBySubjectBO AverageMarkBySubjectBO = new AverageMarkBySubjectBO();
                        AverageMarkBySubjectBO.IsPeriod = true;
                        AverageMarkBySubjectBO.PeriodID = form.Period;
                        SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { 
                                                        { "AcademicYearID", _globalInfo.AcademicYearID.Value } ,
                                                        { "SubjectID", form.Subject.Value }
                                                    }).FirstOrDefault();
                        if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                            {
                                throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                            }
                        }
                        if (Convert.ToInt16(form.MarkType) == ReportMenuDirectionConstant.MARK_TYPE_PERIODIC && String.IsNullOrEmpty(form.MarkColumn))
                        {
                            throw new BusinessException("ReportResultLearningBySemester_Validate_MarkColumn");
                        }
                        if (form.Semester.HasValue)
                        {
                            AverageMarkBySubjectBO.Semester = form.Semester.Value;
                        }
                        if (form.Subject.HasValue)
                        {
                            AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                        }
                        if (form.StatisticLevelReportID.HasValue)
                        {
                            AverageMarkBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                        }

                        //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                        if (form.EmployeeID.HasValue)
                        {
                            AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                        }
                        if (!String.IsNullOrEmpty(form.MarkType))
                        {
                            AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                        }
                        else
                        {
                            AverageMarkBySubjectBO.MarkType = ReportMenuDirectionConstant.MARK_TYPE_AVERAGE;
                        }

                        AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                        //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                        AverageMarkBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        AverageMarkBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        AverageMarkBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        AverageMarkBySubjectBO.EducationLevels = _globalInfo.EducationLevels;
                        if (!string.IsNullOrEmpty(form.EthnicType))
                        {
                            AverageMarkBySubjectBO.EthnicChecked = true;
                        }
                        else
                        {
                            AverageMarkBySubjectBO.EthnicChecked = false;
                        }
                        if (!string.IsNullOrEmpty(form.FemaleType))
                        {
                            AverageMarkBySubjectBO.FemaleChecked = true;
                        }
                        else
                        {
                            AverageMarkBySubjectBO.FemaleChecked = false;
                        }

                        if (!string.IsNullOrEmpty(form.FemaleEthnicType))
                        {
                            AverageMarkBySubjectBO.FemaleEthnicChecked = true;
                        }
                        else
                        {
                            AverageMarkBySubjectBO.FemaleEthnicChecked = false;
                        }


                        string reportCode = SystemParamsInFile.HS_THPT_ThongKeDiemKTDK_HKI_GDCD_GV;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        if (reportDef.IsPreprocessed == true)
                        {
                            //if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            //{
                                processedReport = this.AverageMarkBySubjectBusiness.GetAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);

                            //}
                            //else
                            //{

                            //    processedReport = this.AverageMarkBySubjectBusiness.GetAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);

                            //}
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            //if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            //{

                                Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                                processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO, excel);
                                excel.Close();

                            //}
                            //else
                            //{

                            //    Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO);
                            //    processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO, excel);
                            //    excel.Close();

                            //}

                        }
                    }
                    #endregion
                }
                #endregion

                #region Thống kê xếp loại học lực - AuNH
                if (form.rdoReportBySemester == 8)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }
                    type = JsonReportMessage.NEW;
                    SynthesisSubjectByPeriod entity = new SynthesisSubjectByPeriod();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.PeriodID = form.Period.HasValue ? form.Period.Value : 0;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                    entity.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? int.Parse(form.FemaleEthnicType) : 0;

                    processedReport = SynthesisSubjectByPeriodBusiness.GetSynthesisSubjectByPeriod(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = SynthesisSubjectByPeriodBusiness.CreateSynthesisSubjectByPeriod(entity);
                        processedReport = SynthesisSubjectByPeriodBusiness.InsertSynthesisSubjectByPeriod(entity, excel);
                        excel.Close();
                    }


                }
                #endregion

                #region Thống kê xếp loại chi tiết theo môn - AuNH
                if (form.rdoReportBySemester == 7)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }

                    type = JsonReportMessage.NEW;
                    TranscriptOfClass entity = new TranscriptOfClass();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.EducationLevelID = form.EducationLevel.HasValue ? form.EducationLevel.Value : 0;
                    entity.PeriodDeclarationID = form.Period.HasValue ? form.Period.Value : 0;
                    entity.Semester = form.Semester.HasValue ? form.Semester.Value : 0;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;

                    processedReport = DetailClassifiedSubjectByPeriodBusiness.GetDetailClassifiedSubjectByPeriod(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = DetailClassifiedSubjectByPeriodBusiness.CreateDetailClassifiedSubjectByPeriod(entity);
                        processedReport = DetailClassifiedSubjectByPeriodBusiness.InsertDetailClassifiedSubjectByPeriod(entity, excel);
                        excel.Close();
                    }

                }
                #endregion

                #region Thống kê tổng hợp xếp loại các môn - QuangLM
                if (form.rdoReportBySemester == 11)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }

                    type = JsonReportMessage.NEW;
                    SynthesisClassifiedSubjectByPeriod entity = new SynthesisClassifiedSubjectByPeriod();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.PeriodID = form.Period.HasValue ? form.Period.Value : 0;
                    entity.SemesterID = form.Semester.HasValue ? form.Semester.Value : 0;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                    entity.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? int.Parse(form.FemaleEthnicType) : 0;

                    processedReport = SynthesisClassifiedSubjectByPeriodBusiness.GetSynthesisClassifiedSubjectByPeriod(entity);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = SynthesisClassifiedSubjectByPeriodBusiness.CreateSynthesisClassifiedSubjectByPeriod(entity);
                        processedReport = SynthesisClassifiedSubjectByPeriodBusiness.InsertSynthesisClassifiedSubjectByPeriod(entity, excel);
                        excel.Close();
                    }

                }
                #endregion

            }
            #endregion

            #region xuất excel theo kỳ
            else
            {
                #region 6 Thống kê điểm trung bình môn
                if (form.rdoReportBySemester == 6)
                {
                    AverageMarkBySubjectBO AverageMarkBySubjectBO = new AverageMarkBySubjectBO();
                    if (!form.Subject.HasValue)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_Subject");
                    }
                    SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { 
                    { "AcademicYearID", _globalInfo.AcademicYearID.Value } ,
                    { "SubjectID", form.Subject.Value }
                }).FirstOrDefault();
                    if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                        {
                            throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                        }
                    }
                    if (Convert.ToInt16(form.MarkType) == ReportMenuDirectionConstant.MARK_TYPE_PERIODIC && String.IsNullOrEmpty(form.MarkColumn))
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_MarkColumn");
                    }
                    if (form.Semester.HasValue)
                    {
                        AverageMarkBySubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.Subject.HasValue)
                    {
                        AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                    }
                    if (form.StatisticLevelReportID.HasValue)
                    {
                        AverageMarkBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    }
                    //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                    if (form.EmployeeID.HasValue)
                    {
                        AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                    }
                    if (!String.IsNullOrEmpty(form.MarkType))
                    {
                        AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                    }
                    else
                    {
                        AverageMarkBySubjectBO.MarkType = ReportMenuDirectionConstant.MARK_TYPE_AVERAGE;
                    }

                    AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                    //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                    AverageMarkBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    AverageMarkBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                    AverageMarkBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    AverageMarkBySubjectBO.EducationLevels = _globalInfo.EducationLevels;
                    if (!string.IsNullOrEmpty(form.EthnicType))
                    {
                        AverageMarkBySubjectBO.EthnicChecked = true;
                    }
                    else
                    {
                        AverageMarkBySubjectBO.EthnicChecked = false;
                    }
                    if (!string.IsNullOrEmpty(form.FemaleType))
                    {
                        AverageMarkBySubjectBO.FemaleChecked = true;
                    }
                    else
                    {
                        AverageMarkBySubjectBO.FemaleChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType))
                    {
                        AverageMarkBySubjectBO.FemaleEthnicChecked = true;
                    }
                    else
                    {
                        AverageMarkBySubjectBO.FemaleEthnicChecked = false;
                    }


                    string reportCode = SystemParamsInFile.REPORT_ThongKeDiemTBM;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            if (form.GroupBy == 1)
                            {
                                processedReport = this.AverageMarkBySubjectBusiness.GetAverageMarkBySubject(AverageMarkBySubjectBO);
                            }
                            else
                            {
                                processedReport = this.AverageMarkBySubjectBusiness.GetAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                            }
                        }
                        else
                        {
                            if (form.GroupBy == 1)
                            {
                                processedReport = this.AverageMarkBySubjectBusiness.GetAverageMarkBySubjectJudge(AverageMarkBySubjectBO);
                            }
                            else
                            {
                                processedReport = this.AverageMarkBySubjectBusiness.GetAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                            }
                        }
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            if (form.GroupBy == 1)
                            {
                                Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubject(AverageMarkBySubjectBO);
                                processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubject(AverageMarkBySubjectBO, excel);
                                excel.Close();
                            }
                            else
                            {
                                Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                                processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO, excel);
                                excel.Close();
                            }
                        }
                        else
                        {
                            if (form.GroupBy == 1)
                            {
                                Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectJudge(AverageMarkBySubjectBO);
                                processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectJudge(AverageMarkBySubjectBO, excel);
                                excel.Close();
                            }
                            else
                            {
                                Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                                processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO, excel);
                                excel.Close();
                            }
                        }

                    }
                }
                #endregion

                #region 4 Thống kê điểm thi học kỳ
                if (form.rdoReportBySemester == 4)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }
                    MarkOfSemesterBO MarkOfSemesterBO = new MarkOfSemesterBO();
                    if (form.Semester.HasValue)
                    {
                        MarkOfSemesterBO.Semester = form.Semester.Value;
                    }
                    if (form.StatisticLevelReportID.HasValue)
                    {
                        MarkOfSemesterBO.StatisticReportID = form.StatisticLevelReportID.Value;
                    }
                    MarkOfSemesterBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    MarkOfSemesterBO.SchoolID = _globalInfo.SchoolID.Value;
                    MarkOfSemesterBO.AcademicYearID = _globalInfo.AcademicYearID.Value;

                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        MarkOfSemesterBO.FemaleChecked = true;
                    }
                    else
                    {
                        MarkOfSemesterBO.FemaleChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        MarkOfSemesterBO.EthnicChecked = true;
                    }
                    else
                    {
                        MarkOfSemesterBO.EthnicChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        MarkOfSemesterBO.FemaleEthnicChecked = true;
                    }
                    else
                    {
                        MarkOfSemesterBO.FemaleEthnicChecked = false;
                    }

                    string reportCode = SystemParamsInFile.REPORT_TKDiemThiHocKy; ;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = this.MarkOfSemesterBusiness.GetMarkOfSemester(MarkOfSemesterBO);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = this.MarkOfSemesterBusiness.CreateMarkOfSemester(MarkOfSemesterBO);
                        processedReport = this.MarkOfSemesterBusiness.InsertMarkOfSemester(MarkOfSemesterBO, excel);
                        excel.Close();

                    }
                }
                #endregion

                #region 5 Thống kê điểm thi học kỳ theo môn
                if (form.rdoReportBySemester == 5)
                {
                    if (!form.Subject.HasValue)
                    {
                        throw new BusinessException("Please_Choice_Subject");
                    }
                    SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { 
                    { "AcademicYearID", _globalInfo.AcademicYearID.Value } ,
                    { "SubjectID", form.Subject.Value }
                }).FirstOrDefault();

                    if (schoolSubject.IsCommenting.Value != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                        {
                            throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                        }
                        //Thống kê điểm thi học kỳ theo môn tính điểm
                        MarkOfSemesterBySubjectBO MarkOfSemesterBySubjectBO = new MarkOfSemesterBySubjectBO();
                        if (form.Semester.HasValue)
                        {
                            MarkOfSemesterBySubjectBO.Semester = form.Semester.Value;
                        }
                        if (form.Subject.HasValue)
                        {
                            MarkOfSemesterBySubjectBO.SubjectID = form.Subject.Value;
                        }
                        if (form.StatisticLevelReportID.HasValue)
                        {
                            MarkOfSemesterBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                        }
                        MarkOfSemesterBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        MarkOfSemesterBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        MarkOfSemesterBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                        {
                            MarkOfSemesterBySubjectBO.FemaleChecked = true;
                        }
                        else
                        {
                            MarkOfSemesterBySubjectBO.FemaleChecked = false;
                        }

                        if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                        {
                            MarkOfSemesterBySubjectBO.EthnicChecked = true;
                        }
                        else
                        {
                            MarkOfSemesterBySubjectBO.EthnicChecked = false;
                        }

                        if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                        {
                            MarkOfSemesterBySubjectBO.FemaleEthnicChecked = true;
                        }
                        else
                        {
                            MarkOfSemesterBySubjectBO.FemaleEthnicChecked = false;
                        }

                        string reportCode = SystemParamsInFile.REPORT_TKDiemThiHocKyTheoMonTinhDiem; ;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = this.MarkOfSemesterBySubjectBusiness.GetMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = this.MarkOfSemesterBySubjectBusiness.CreateMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO);
                            processedReport = this.MarkOfSemesterBySubjectBusiness.InsertMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO, excel);
                            excel.Close();
                        }
                    }
                    else
                    {
                        //Thống kê điểm thi học kỳ theo môn nhận xét
                        MarkOfSemesterByJudgeSubjectBO MarkOfSemesterByJudgeSubjectBO = new MarkOfSemesterByJudgeSubjectBO();
                        if (form.Semester.HasValue)
                        {
                            MarkOfSemesterByJudgeSubjectBO.Semester = form.Semester.Value;
                        }
                        if (form.Subject.HasValue)
                        {
                            MarkOfSemesterByJudgeSubjectBO.SubjectID = form.Subject.Value;
                        }
                        MarkOfSemesterByJudgeSubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        MarkOfSemesterByJudgeSubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        MarkOfSemesterByJudgeSubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                        {
                            MarkOfSemesterByJudgeSubjectBO.FemaleChecked = true;
                        }
                        else
                        {
                            MarkOfSemesterByJudgeSubjectBO.FemaleChecked = false;
                        }

                        if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                        {
                            MarkOfSemesterByJudgeSubjectBO.EthnicChecked = true;
                        }
                        else
                        {
                            MarkOfSemesterByJudgeSubjectBO.EthnicChecked = false;
                        }
                        if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                        {
                            MarkOfSemesterByJudgeSubjectBO.FemaleEthnicChecked = true;
                        }
                        else
                        {
                            MarkOfSemesterByJudgeSubjectBO.FemaleEthnicChecked = false;
                        }

                        string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK_THEOMON_NHANXET; ;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = this.MarkOfSemesterByJudgeSubjectBusiness.GetMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = this.MarkOfSemesterByJudgeSubjectBusiness.CreateMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO);
                            processedReport = this.MarkOfSemesterByJudgeSubjectBusiness.InsertMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO, excel);
                            excel.Close();

                        }
                    }
                }
                #endregion

                #region 8 Thống kê xếp loại học lực
                if (form.rdoReportBySemester == 8)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }
                    ClassifiedCapacityBySubject entity = new ClassifiedCapacityBySubject();
                    entity.Semester = form.Semester.Value;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        entity.FemaleChecked = true;
                    }
                    else
                    {
                        entity.FemaleChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        entity.EthnicChecked = true;
                    }
                    else
                    {
                        entity.EthnicChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        entity.FemaleEthnicChecked = true;
                    }
                    else
                    {
                        entity.FemaleEthnicChecked = false;
                    }
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HOC_LUC_MON;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = ClassifiedCapacityBySubjectBusiness.GetClassifiedCapacityBySubject(entity);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = ClassifiedCapacityBySubjectBusiness.CreateClassifiedCapacityBySubject(entity);
                        processedReport = ClassifiedCapacityBySubjectBusiness.InsertClassifiedCapacityBySubject(entity, excel);
                        excel.Close();
                    }
                }
                #endregion

                #region 7 Thống kê xếp loại chi tiết môn
                if (form.rdoReportBySemester == 7)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }

                    DetailClassifiedBySubjectBO DetailClassifiedBySubjectBO = new DetailClassifiedBySubjectBO();
                    if (form.Semester.HasValue)
                    {
                        DetailClassifiedBySubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.EducationLevel.HasValue)
                    {
                        DetailClassifiedBySubjectBO.EducationLevelID = form.EducationLevel.Value;
                    }
                    DetailClassifiedBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                    DetailClassifiedBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    DetailClassifiedBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    DetailClassifiedBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_CHITIET_MON; ;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = this.DetailClassifiedBySubjectBusiness.GetDetailClassifiedBySubject(DetailClassifiedBySubjectBO);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }
                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = this.DetailClassifiedBySubjectBusiness.CreateDetailClassifiedBySubject(DetailClassifiedBySubjectBO);
                        processedReport = this.DetailClassifiedBySubjectBusiness.InsertDetailClassifiedBySubject(DetailClassifiedBySubjectBO, excel);
                        excel.Close();

                    }
                }
                #endregion

                #region 11 Thống kê tổng hợp xếp loại mon hoc theo khoi - minhh
                if (form.rdoReportBySemester == 11)
                {
                    if (lstStatisticLevelConfig == null || lstStatisticLevelConfig.Count == 0)
                    {
                        throw new BusinessException("ReportResultLearningBySemester_Validate_StatisticLevelReport");
                    }
                    SynthesisSubjectByEducationLevel entity = new SynthesisSubjectByEducationLevel();
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    entity.Semester = form.Semester.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        entity.FemaleChecked = true;
                    }
                    else
                    {
                        entity.FemaleChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        entity.EthnicChecked = true;
                    }
                    else
                    {
                        entity.EthnicChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        entity.FemaleEthnicChecked = true;
                    }
                    else
                    {
                        entity.FemaleEthnicChecked = false;
                    }
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_CAC_MON_THEO_KHOI;
                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    if (reportDef.IsPreprocessed == true)
                    {
                        processedReport = SynthesisSubjectByEducationLevelBusiness.GetSynthesisSubjectByEducationLevel(entity);
                        if (processedReport != null)
                        {
                            type = JsonReportMessage.OLD;
                        }
                    }

                    if (type == JsonReportMessage.NEW)
                    {
                        Stream excel = SynthesisSubjectByEducationLevelBusiness.CreateSynthesisSubjectByEducationLevel(entity);
                        processedReport = SynthesisSubjectByEducationLevelBusiness.InsertSynthesisSubjectByEducationLevel(entity, excel);
                        excel.Close();
                    }
                }
                #endregion
            }
            #endregion

            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySemester(SubjectReportViewModel form)
        {
            ProcessedReport processedReport = null;
            #region xuất excel theo đợt
            if (form.chkPeriod == 1)
            {
                #region Thống kê điểm trung bình môn - AuNH
                if (form.rdoReportBySemester == 6)
                {
                    #region nhóm theo khối
                    if (form.GroupBy == 1)
                    {
                        TranscriptOfClass entity = new TranscriptOfClass();
                        entity.SchoolID = _globalInfo.SchoolID.Value;
                        entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        entity.SubjectID = form.Subject.HasValue ? form.Subject.Value : 0;
                        entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        entity.PeriodDeclarationID = form.Period.HasValue ? form.Period.Value : 0;
                        entity.Semester = form.Semester.HasValue ? form.Semester.Value : 0;
                        entity.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;
                        entity.TeacherID = form.EmployeeID.HasValue ? form.EmployeeID.Value : 0;
                        entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                        entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                        entity.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? int.Parse(form.FemaleEthnicType) : 0;
                        Stream excel = AverageMarkByPeriodBusiness.CreateAverageMarkByPeriod(entity);
                        processedReport = AverageMarkByPeriodBusiness.InsertAverageMarkByPeriod(entity, excel);
                        excel.Close();
                    }
                    #endregion
                    #region nhóm theo giáo viên
                    else
                    {
                        AverageMarkBySubjectBO AverageMarkBySubjectBO = new AverageMarkBySubjectBO();
                        AverageMarkBySubjectBO.IsPeriod = true;
                        AverageMarkBySubjectBO.PeriodID = form.Period;
                        if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                        {
                            AverageMarkBySubjectBO.FemaleChecked = true;
                        }
                        else
                        {
                            AverageMarkBySubjectBO.FemaleChecked = false;
                        }

                        if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                        {
                            AverageMarkBySubjectBO.EthnicChecked = true;
                        }
                        else
                        {
                            AverageMarkBySubjectBO.EthnicChecked = false;
                        }

                        if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                        {
                            AverageMarkBySubjectBO.FemaleEthnicChecked = true;
                        }
                        else
                        {
                            AverageMarkBySubjectBO.FemaleEthnicChecked = false;
                        }

                        //SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { 
                        //                                                { "AcademicYearID", _globalInfo.AcademicYearID.Value } ,
                        //                                                { "SubjectID", form.Subject.Value }
                        //                                            }).FirstOrDefault();

                        //if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        //{
                        if (form.Semester.HasValue)
                        {
                            AverageMarkBySubjectBO.Semester = form.Semester.Value;
                        }
                        if (form.Subject.HasValue)
                        {
                            AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                        }
                        if (form.StatisticLevelReportID.HasValue)
                        {
                            AverageMarkBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                        }
                        //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM

                        if (form.EmployeeID.HasValue)
                        {
                            AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                        }
                        if (!String.IsNullOrEmpty(form.MarkType))
                        {
                            AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                        }
                        else
                        {
                            AverageMarkBySubjectBO.MarkType = ReportMenuDirectionConstant.MARK_TYPE_AVERAGE;
                        }

                        AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                        //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                        AverageMarkBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        AverageMarkBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        AverageMarkBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        AverageMarkBySubjectBO.EducationLevels = _globalInfo.EducationLevels;

                        Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                        processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO, excel);
                        excel.Close();

                        //else
                        //{
                        //    if (form.Semester.HasValue)
                        //    {
                        //        AverageMarkBySubjectBO.Semester = form.Semester.Value;
                        //    }
                        //    if (form.Subject.HasValue)
                        //    {
                        //        AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                        //    }
                        //    //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                        //    if (form.EmployeeID.HasValue)
                        //    {
                        //        AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                        //    }
                        //    if (!String.IsNullOrEmpty(form.MarkType))
                        //    {
                        //        AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                        //    }
                        //    else
                        //    {
                        //        AverageMarkBySubjectBO.MarkType = ReportMenuDirectionConstant.MARK_TYPE_AVERAGE;
                        //    }
                        //    AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                        //    //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                        //    AverageMarkBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        //    AverageMarkBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        //    AverageMarkBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        //    AverageMarkBySubjectBO.EducationLevels = _globalInfo.EducationLevels;

                        //    Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                        //    processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO, excel);
                        //    excel.Close();

                        //}
                    }
                    #endregion
                }
                #endregion

                #region Thống kê xếp loại học lực - AuNH
                if (form.rdoReportBySemester == 8)
                {

                    SynthesisSubjectByPeriod entity = new SynthesisSubjectByPeriod();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.PeriodID = form.Period.HasValue ? form.Period.Value : 0;
                    entity.SemesterID = form.Semester.HasValue ? form.Semester.Value : 0;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                    entity.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? int.Parse(form.FemaleEthnicType) : 0;
                    Stream excel = SynthesisSubjectByPeriodBusiness.CreateSynthesisSubjectByPeriod(entity);
                    processedReport = SynthesisSubjectByPeriodBusiness.InsertSynthesisSubjectByPeriod(entity, excel);
                    excel.Close();
                }
                #endregion

                #region Thống kê xếp loại chi tiết theo môn - AuNH
                if (form.rdoReportBySemester == 7)
                {
                    TranscriptOfClass entity = new TranscriptOfClass();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.EducationLevelID = form.EducationLevel.HasValue ? form.EducationLevel.Value : 0;
                    entity.PeriodDeclarationID = form.Period.HasValue ? form.Period.Value : 0;
                    entity.Semester = form.Semester.HasValue ? form.Semester.Value : 0;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                    Stream excel = DetailClassifiedSubjectByPeriodBusiness.CreateDetailClassifiedSubjectByPeriod(entity);
                    processedReport = DetailClassifiedSubjectByPeriodBusiness.InsertDetailClassifiedSubjectByPeriod(entity, excel);
                    excel.Close();


                }
                #endregion

                #region Thống kê xếp loại học lực môn - QuangLM
                if (form.rdoReportBySemester == 11)
                {
                    SynthesisClassifiedSubjectByPeriod entity = new SynthesisClassifiedSubjectByPeriod();
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.PeriodID = form.Period.HasValue ? form.Period.Value : 0;
                    entity.SemesterID = form.Semester.HasValue ? form.Semester.Value : 0;
                    entity.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? Int32.Parse(form.FemaleType) : 0;
                    entity.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? Int32.Parse(form.EthnicType) : 0;
                    entity.FemaleEthnicID = !string.IsNullOrEmpty(form.FemaleEthnicType) ? Int32.Parse(form.FemaleEthnicType) : 0;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;
                    processedReport = SynthesisClassifiedSubjectByPeriodBusiness.GetSynthesisClassifiedSubjectByPeriod(entity);

                    Stream excel = SynthesisClassifiedSubjectByPeriodBusiness.CreateSynthesisClassifiedSubjectByPeriod(entity);
                    processedReport = SynthesisClassifiedSubjectByPeriodBusiness.InsertSynthesisClassifiedSubjectByPeriod(entity, excel);
                    excel.Close();

                }
                #endregion

            }
            #endregion

            #region xuất excel theo kỳ
            else
            {
                #region Thống kê điểm trung bình môn
                if (form.rdoReportBySemester == 6)
                {
                    AverageMarkBySubjectBO AverageMarkBySubjectBO = new AverageMarkBySubjectBO();
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        AverageMarkBySubjectBO.FemaleChecked = true;
                    }
                    else
                    {
                        AverageMarkBySubjectBO.FemaleChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        AverageMarkBySubjectBO.EthnicChecked = true;
                    }
                    else
                    {
                        AverageMarkBySubjectBO.EthnicChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        AverageMarkBySubjectBO.FemaleEthnicChecked = true;
                    }
                    else
                    {
                        AverageMarkBySubjectBO.FemaleEthnicChecked = false;
                    }
                    SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { 
                    { "AcademicYearID", _globalInfo.AcademicYearID.Value } ,
                    { "SubjectID", form.Subject.Value }
                }).FirstOrDefault();

                    if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (form.Semester.HasValue)
                        {
                            AverageMarkBySubjectBO.Semester = form.Semester.Value;
                        }
                        if (form.Subject.HasValue)
                        {
                            AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                        }
                        if (form.StatisticLevelReportID.HasValue)
                        {
                            AverageMarkBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                        }
                        //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM

                        if (form.EmployeeID.HasValue)
                        {
                            AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                        }
                        if (!String.IsNullOrEmpty(form.MarkType))
                        {
                            AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                        }
                        else
                        {
                            AverageMarkBySubjectBO.MarkType = ReportMenuDirectionConstant.MARK_TYPE_AVERAGE;
                        }

                        AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                        //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                        AverageMarkBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        AverageMarkBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        AverageMarkBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        AverageMarkBySubjectBO.EducationLevels = _globalInfo.EducationLevels;

                        if (form.GroupBy == 1)
                        {
                            Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubject(AverageMarkBySubjectBO);
                            processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubject(AverageMarkBySubjectBO, excel);
                            excel.Close();
                        }
                        else
                        {
                            Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                            processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO, excel);
                            excel.Close();
                        }
                    }
                    else
                    {
                        if (form.Semester.HasValue)
                        {
                            AverageMarkBySubjectBO.Semester = form.Semester.Value;
                        }
                        if (form.Subject.HasValue)
                        {
                            AverageMarkBySubjectBO.SubjectID = form.Subject.Value;
                        }
                        if (form.StatisticLevelReportID.HasValue)
                        {
                            AverageMarkBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                        }
                        //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                        if (form.EmployeeID.HasValue)
                        {
                            AverageMarkBySubjectBO.EmployeeID = form.EmployeeID.Value;
                        }
                        if (!String.IsNullOrEmpty(form.MarkType))
                        {
                            AverageMarkBySubjectBO.MarkType = Convert.ToInt16(form.MarkType);
                        }
                        else
                        {
                            AverageMarkBySubjectBO.MarkType = ReportMenuDirectionConstant.MARK_TYPE_AVERAGE;
                        }
                        AverageMarkBySubjectBO.MarkColumn = form.MarkColumn;
                        //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
                        AverageMarkBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        AverageMarkBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        AverageMarkBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        AverageMarkBySubjectBO.EducationLevels = _globalInfo.EducationLevels;

                        if (form.GroupBy == 1)
                        {
                            Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectJudge(AverageMarkBySubjectBO);
                            processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectJudge(AverageMarkBySubjectBO, excel);
                            excel.Close();
                        }
                        else
                        {
                            Stream excel = this.AverageMarkBySubjectBusiness.CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO);
                            processedReport = this.AverageMarkBySubjectBusiness.InsertAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO, excel);
                            excel.Close();
                        }
                    }

                }
                #endregion

                #region Thống kê điểm thi học kì
                if (form.rdoReportBySemester == 4)
                {
                    MarkOfSemesterBO MarkOfSemesterBO = new MarkOfSemesterBO();

                    if (form.Semester.HasValue)
                    {
                        MarkOfSemesterBO.Semester = form.Semester.Value;
                    }
                    if (form.StatisticLevelReportID.HasValue)
                    {
                        MarkOfSemesterBO.StatisticReportID = form.StatisticLevelReportID.Value;
                    }
                    MarkOfSemesterBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    MarkOfSemesterBO.SchoolID = _globalInfo.SchoolID.Value;
                    MarkOfSemesterBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        MarkOfSemesterBO.FemaleChecked = true;
                    }
                    else
                    {
                        MarkOfSemesterBO.FemaleChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        MarkOfSemesterBO.EthnicChecked = true;
                    }
                    else
                    {
                        MarkOfSemesterBO.EthnicChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        MarkOfSemesterBO.FemaleEthnicChecked = true;
                    }
                    else
                    {
                        MarkOfSemesterBO.FemaleEthnicChecked = false;
                    }

                    Stream excel = this.MarkOfSemesterBusiness.CreateMarkOfSemester(MarkOfSemesterBO);
                    processedReport = this.MarkOfSemesterBusiness.InsertMarkOfSemester(MarkOfSemesterBO, excel);
                    excel.Close();

                }
                #endregion

                #region Thống kê điểm thi học kì theo môn
                if (form.rdoReportBySemester == 5)
                {
                    SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { 
                    { "AcademicYearID", _globalInfo.AcademicYearID.Value } ,
                    { "SubjectID", form.Subject.Value }
                }).FirstOrDefault();
                    bool isCheckedFemale = false;
                    bool isCheckedEthnic = false;
                    bool isCheckedFemaleEthnic = false;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        isCheckedFemale = true;
                    }

                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        isCheckedEthnic = true;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        isCheckedFemaleEthnic = true;
                    }

                    if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        MarkOfSemesterBySubjectBO MarkOfSemesterBySubjectBO = new MarkOfSemesterBySubjectBO();
                        if (form.Semester.HasValue)
                        {
                            MarkOfSemesterBySubjectBO.Semester = form.Semester.Value;
                        }
                        if (form.Subject.HasValue)
                        {
                            MarkOfSemesterBySubjectBO.SubjectID = form.Subject.Value;
                        }
                        if (form.StatisticLevelReportID.HasValue)
                        {
                            MarkOfSemesterBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                        }
                        MarkOfSemesterBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        MarkOfSemesterBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        MarkOfSemesterBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        MarkOfSemesterBySubjectBO.FemaleChecked = isCheckedFemale;
                        MarkOfSemesterBySubjectBO.EthnicChecked = isCheckedEthnic;
                        MarkOfSemesterBySubjectBO.FemaleEthnicChecked = isCheckedFemaleEthnic;

                        Stream excel = this.MarkOfSemesterBySubjectBusiness.CreateMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO);
                        processedReport = this.MarkOfSemesterBySubjectBusiness.InsertMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO, excel);
                        excel.Close();
                    }
                    else
                    {
                        MarkOfSemesterByJudgeSubjectBO MarkOfSemesterByJudgeSubjectBO = new MarkOfSemesterByJudgeSubjectBO();
                        if (form.Semester.HasValue)
                        {
                            MarkOfSemesterByJudgeSubjectBO.Semester = form.Semester.Value;
                        }
                        if (form.Subject.HasValue)
                        {
                            MarkOfSemesterByJudgeSubjectBO.SubjectID = form.Subject.Value;
                        }
                        if (form.StatisticLevelReportID.HasValue)
                        {
                            MarkOfSemesterByJudgeSubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                        }

                        MarkOfSemesterByJudgeSubjectBO.FemaleChecked = isCheckedFemale;
                        MarkOfSemesterByJudgeSubjectBO.EthnicChecked = isCheckedEthnic;
                        MarkOfSemesterByJudgeSubjectBO.FemaleEthnicChecked = isCheckedFemaleEthnic;
                        MarkOfSemesterByJudgeSubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                        MarkOfSemesterByJudgeSubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                        MarkOfSemesterByJudgeSubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;

                        Stream excel = this.MarkOfSemesterByJudgeSubjectBusiness.CreateMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO);
                        processedReport = this.MarkOfSemesterByJudgeSubjectBusiness.InsertMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO, excel);
                        excel.Close();
                    }
                }
                #endregion

                #region Thống kê xếp loại học lực-mon
                if (form.rdoReportBySemester == 8)
                {
                    ClassifiedCapacityBySubject entity = new ClassifiedCapacityBySubject();
                    entity.Semester = form.Semester.Value;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.Value;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        entity.FemaleChecked = true;
                    }
                    else
                    {
                        entity.FemaleChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        entity.EthnicChecked = true;
                    }
                    else
                    {
                        entity.EthnicChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        entity.FemaleEthnicChecked = true;
                    }
                    else
                    {
                        entity.FemaleEthnicChecked = false;
                    }
                    Stream excel = ClassifiedCapacityBySubjectBusiness.CreateClassifiedCapacityBySubject(entity);
                    processedReport = ClassifiedCapacityBySubjectBusiness.InsertClassifiedCapacityBySubject(entity, excel);
                    excel.Close();
                }
                #endregion

                #region Thống kê đánh giá môn học theo lớp
                if (form.rdoReportBySemester == 7)
                {
                    DetailClassifiedBySubjectBO DetailClassifiedBySubjectBO = new DetailClassifiedBySubjectBO();
                    if (form.Semester.HasValue)
                    {
                        DetailClassifiedBySubjectBO.Semester = form.Semester.Value;
                    }
                    if (form.EducationLevel.HasValue)
                    {
                        DetailClassifiedBySubjectBO.EducationLevelID = form.EducationLevel.Value;
                    }
                    DetailClassifiedBySubjectBO.SchoolID = _globalInfo.SchoolID.Value;
                    DetailClassifiedBySubjectBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    DetailClassifiedBySubjectBO.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    DetailClassifiedBySubjectBO.EthnicID = !string.IsNullOrEmpty(form.EthnicType) ? int.Parse(form.EthnicType) : 0;
                    DetailClassifiedBySubjectBO.FemaleID = !string.IsNullOrEmpty(form.FemaleType) ? int.Parse(form.FemaleType) : 0;
                    DetailClassifiedBySubjectBO.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;

                    Stream excel = this.DetailClassifiedBySubjectBusiness.CreateDetailClassifiedBySubject(DetailClassifiedBySubjectBO);
                    processedReport = this.DetailClassifiedBySubjectBusiness.InsertDetailClassifiedBySubject(DetailClassifiedBySubjectBO, excel);
                    excel.Close();

                }
                #endregion

                #region  Thống kê tổng hợp các môn theo khối  - minhh
                if (form.rdoReportBySemester == 11)
                {
                    SynthesisSubjectByEducationLevel entity = new SynthesisSubjectByEducationLevel();
                    entity.Semester = form.Semester.HasValue ? form.Semester.Value : 0;
                    entity.AppliedLevel = _globalInfo.AppliedLevel.Value;
                    entity.SchoolID = _globalInfo.SchoolID.Value;
                    entity.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    entity.StatisticLevelReportID = form.StatisticLevelReportID.HasValue ? form.StatisticLevelReportID.Value : 0;
                    if (!string.IsNullOrEmpty(form.FemaleType) && Int32.Parse(form.FemaleType) > 0)
                    {
                        entity.FemaleChecked = true;
                    }
                    else
                    {
                        entity.FemaleChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.EthnicType) && Int32.Parse(form.EthnicType) > 0)
                    {
                        entity.EthnicChecked = true;
                    }
                    else
                    {
                        entity.EthnicChecked = false;
                    }

                    if (!string.IsNullOrEmpty(form.FemaleEthnicType) && Int32.Parse(form.FemaleEthnicType) > 0)
                    {
                        entity.FemaleEthnicChecked = true;
                    }
                    else
                    {
                        entity.FemaleEthnicChecked = false;
                    }
                    Stream excel = SynthesisSubjectByEducationLevelBusiness.CreateSynthesisSubjectByEducationLevel(entity);
                    processedReport = SynthesisSubjectByEducationLevelBusiness.InsertSynthesisSubjectByEducationLevel(entity, excel);
                    excel.Close();
                }
                #endregion
            }
            #endregion

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public bool CheckIsCommentting(int? subjectID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["SubjectID"] = subjectID;

            IQueryable<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic);
            int? ss = listSchoolSubject.FirstOrDefault().IsCommenting;
            if (ss == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                return false;
            else return true;
        }

        #region Load combo MarkColumn

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadMarkColum(int paraSemester)
        {
            //Lay ra SemesterDeclaration
            Dictionary<string, object> dicToGetSD = new Dictionary<string, object>();
            dicToGetSD["SchoolID"] = _globalInfo.SchoolID.Value;
            dicToGetSD["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicToGetSD["Semester"] = paraSemester;

            IQueryable<SemeterDeclaration> lstSD = SemesterDeclarationBusiness.Search(dicToGetSD).OrderByDescending(o => o.AppliedDate);
            SemeterDeclaration sd = lstSD.FirstOrDefault();

            //So con diem mieng
            int interviewMarkNum = 0;
            //So con diem viet he so 1
            int writingMarkNum = 0;
            //So con diem viet he so 2
            int twiceCoeffiecientMarkNum = 0;
            if (sd != null)
            {
                //So con diem mieng
                interviewMarkNum = sd.InterviewMark;
                //So con diem viet he so 1
                writingMarkNum = sd.WritingMark;
                //So con diem viet he so 2
                twiceCoeffiecientMarkNum = sd.TwiceCoeffiecientMark;
            }

            List<ComboObject> lstMarkColumn = new List<ComboObject>();
            string markColumn = string.Empty;

            for (int i = 1; i <= interviewMarkNum; i++)
            {
                markColumn = "M" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            for (int i = 1; i <= writingMarkNum; i++)
            {
                markColumn = "P" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }

            for (int i = 1; i <= twiceCoeffiecientMarkNum; i++)
            {
                markColumn = "V" + i.ToString();
                lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
            }
            lstMarkColumn.Add(new ComboObject("HK", "KTHK"));

            ViewData[ReportMenuDirectionConstant.CBO_MARK_COLUMN] = lstMarkColumn;
            return Json(new SelectList(lstMarkColumn, "key", "value"));
        }
        #endregion

        #region Load combo Giao vien theo

        public PartialViewResult AjaxLoadTeacher(int paraSubjectId, int paraSemester)
        {
            //Lay danh sach giao vien trong truong
            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = _globalInfo.SchoolID;
            IQueryable<Employee> query = EmployeeBusiness.Search(dicToGetTeacher);

            //Lay danh sach giao vien duong phan cong day mon duoc chon
            IDictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
            dicToGetAssignment["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicToGetAssignment["SubjectID"] = paraSubjectId;
            if (paraSemester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                dicToGetAssignment["Semester"] = paraSemester;
            }
            IQueryable<TeachingAssignment> lstTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicToGetAssignment);

            IQueryable<EmployeeCboModel> lstComboEmployee = from q in query
                                                            where lstTeachingAssignment.Any(o => o.TeacherID == q.EmployeeID)
                                                            select new EmployeeCboModel
                                                            {
                                                                EmployeeID = q.EmployeeID,
                                                                DisplayName = q.FullName + " - " + q.EmployeeCode

                                                            };

            ViewData[ReportMenuDirectionConstant.CBO_EMPLOYEE] = lstComboEmployee;

            return PartialView("_TeacherCombobox");
        }
        #endregion
    }
}