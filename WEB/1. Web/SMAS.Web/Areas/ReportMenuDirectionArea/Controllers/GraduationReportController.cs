﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportMenuDirectionArea;
using SMAS.Web.Areas.ReportMenuDirectionArea.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.ExportXML;
using SMAS.VTUtils.Excel.Export;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Controllers
{
    public class GraduationReportController : BaseController
    {
        private readonly IProcessedCellDataBusiness ProcessedCellDataBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IReportPreliminarySendSGDBusiness ReportPreliminarySendSGDBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        public GraduationReportController(IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedCellDataBusiness processedCellDataBusiness,
            IReportPreliminarySendSGDBusiness ReportPreliminarySendSGDBusiness,
            IProcessedReportBusiness ProcessedReportBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness)
        {
            this.ProcessedCellDataBusiness = processedCellDataBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportPreliminarySendSGDBusiness = ReportPreliminarySendSGDBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SchoolSubsidiaryBusiness = SchoolSubsidiaryBusiness;
        }
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        public ActionResult _Index()
        {
            this.SetViewData();
            return PartialView();
        }

        public ActionResult ViewHtmTemplateTHPT()
        {
            return PartialView("_HtmlPatternTHPT");
        }
        public ActionResult ViewHtmTemplateTHCS()
        {
            return PartialView("_HtmlPatternTHCS");
        }
        private void SetViewData()
        {
            ViewData[ReportMenuDirectionConstant.LIST_SEMESTER] = CommonList.Semester_SK();
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            ViewData[ReportMenuDirectionConstant.EDUCATION_GRADE] = SchoolProfileBusiness.Find(_globalInfo.SchoolID).EducationGrade;
            DateTime? FirstStartDate = academicYear.FirstSemesterStartDate.Value.Date;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value.Date;
            int defaultSemester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            if (DateTime.Now.Date >= FirstStartDate && DateTime.Now.Date <= FirstEndDate)
            {
                defaultSemester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
            }
            ViewData[ReportMenuDirectionConstant.DF_SEMESTER] = defaultSemester;
        }
        public JsonResult GetReport(ReportPreliminaryBO reportPreliminary)
        {
            int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
            string reportCode = ReportMenuDirectionConstant.GRADUATION_THCS;
            if (EducationGrade == ReportMenuDirectionConstant.Secondary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary)
            {
                reportCode = ReportMenuDirectionConstant.GRADUATION_THCS;
            }
            else if (EducationGrade == ReportMenuDirectionConstant.Tertiary || EducationGrade == ReportMenuDirectionConstant.Tertiary_End)
            {
                reportCode = ReportMenuDirectionConstant.GRADUATION_THPT;
            }
            else if (EducationGrade == ReportMenuDirectionConstant.Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.All_Grade)
            {
                reportCode = ReportMenuDirectionConstant.GRADUATION_THCSTHPT;
            }
            int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
            EmisRequest emisRequestObj = ProcessedCellDataBusiness.GetEmisRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode, ReportMenuDirectionConstant.GRADUATION_REPORT);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = new ProcessedReport();
            if (emisRequestObj == null)//chua co
            {
                type = JsonReportMessage.NEW;
                ProcessedCellDataBusiness.InsertOrUpdateEmisRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, Year, ReportMenuDirectionConstant.GRADUATION_REPORT, _globalInfo.TrainingType.Value, _globalInfo.DistrictID.Value, _globalInfo.ProvinceID.Value, reportCode);
            }
            else
            {
                if (emisRequestObj.Status.Value == 1)
                {
                    type = JsonReportMessage.OLD;
                    processedReport.ProcessedDate = emisRequestObj.UpdateTime.Value;
                    processedReport.ProcessedReportID = 1;
                }
                else
                {
                    type = JsonReportMessage.NEW;
                }
            }


            return Json(new JsonReportMessage(processedReport, type));
        }
        //StatusReport = ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode, period);
        public FileResult ExportExcel()
        {
            //tong hop
            int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
            string reportCode = ReportMenuDirectionConstant.GRADUATION_THCS;
            if (EducationGrade == ReportMenuDirectionConstant.Secondary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary)
            {
                reportCode = ReportMenuDirectionConstant.GRADUATION_THCS;
            }
            else if (EducationGrade == ReportMenuDirectionConstant.Tertiary || EducationGrade == ReportMenuDirectionConstant.Tertiary_End)
            {
                reportCode = ReportMenuDirectionConstant.GRADUATION_THPT;
            }
            else if (EducationGrade == ReportMenuDirectionConstant.Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.All_Grade)
            {
                reportCode = ReportMenuDirectionConstant.GRADUATION_THCSTHPT;
            }
            int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
            //ProcessedCellDataBusiness.RequestGraduationReport(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode);
            ProcessedCellDataBusiness.InsertOrUpdateEmisRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, Year, ReportMenuDirectionConstant.GRADUATION_REPORT, _globalInfo.TrainingType.Value, _globalInfo.DistrictID.Value, _globalInfo.ProvinceID.Value, reportCode);
            //int k = 0;
            //stop 10s for waiting tool sql completed script
            var stopwatch = Stopwatch.StartNew();
            Thread.Sleep(18000);
            stopwatch.Stop();
            //check report status
            //while (ProcessedCellDataBusiness.GetStatusRequest(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, reportCode, ReportMenuDirectionConstant.GRADUATION_REPORT) != 1)
            //{
            //    if (k == 10)
            //    {
            //        break;
            //    }
            //    else
            //    {
            //        k++;
            //        stopwatch = Stopwatch.StartNew();
            //        Thread.Sleep(5000);
            //        stopwatch.Stop();
            //    }
            //}
            //xuat excel

            string TEMPLATE_NAME = "";
            string templatePath = "";
            string ExportTemplateName = "";
            String pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            try
            {
                int schoolID = _globalInfo.SchoolID.Value;
                int appliedlevel = _globalInfo.AppliedLevel.Value; // cap truong
                IVTWorkbook oBook = null;
                if (EducationGrade == ReportMenuDirectionConstant.Secondary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary)
                {
                    //Template THCS
                    TEMPLATE_NAME = "Bao_Cao_TN_THCS.xls";
                    ExportTemplateName = "TN_THCS.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);
                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.GRADUATION_THCS);
                    FillDateAndUserReport(oBook, "M20", "B22");
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.TN_THCS);
                }
                else if (EducationGrade == ReportMenuDirectionConstant.Tertiary || EducationGrade == ReportMenuDirectionConstant.Tertiary_End)
                {
                    //Template THPT
                    TEMPLATE_NAME = "Bao_Cao_TN_THPT.xls";
                    ExportTemplateName = "TN_THPT.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);
                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.GRADUATION_THPT);
                    FillDateAndUserReport(oBook, "M20", "B22");
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.TN_THPT);
                }
                else if (EducationGrade == ReportMenuDirectionConstant.Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.All_Grade)
                {
                    //Template THCS vs THPT
                    TEMPLATE_NAME = "Bao_Cao_TN_THCSTHPT.xls";
                    ExportTemplateName = "TN_THCSTHPT.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);
                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.GRADUATION_THCSTHPT);
                    FillDateAndUserReport(oBook, "M20", "B22");
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.TN_THCS);
                    FillSheet(oBook, 3, list, ReportMenuDirectionConstant.TN_THPT);

                }
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = ExportTemplateName;
                return result;
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "ExportExcel", "null", ex);
            }
            return null;
        }
        public FileResult DownloadFileExcel()
        {
            //tong hop
            int EducationGrade = ProcessedCellDataBusiness.GetEducationGrade(_globalInfo.SchoolID.Value);
            int Year = ProcessedCellDataBusiness.GetYear(_globalInfo.AcademicYearID.Value).Value;
            string TEMPLATE_NAME = "";
            string templatePath = "";
            string ExportTemplateName = "";
            String pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            try
            {
                int schoolID = _globalInfo.SchoolID.Value;
                int appliedlevel = _globalInfo.AppliedLevel.Value; // cap truong
                IVTWorkbook oBook = null;
                if (EducationGrade == ReportMenuDirectionConstant.Secondary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary)
                {
                    //Template THCS
                    TEMPLATE_NAME = "Bao_Cao_TN_THCS.xls";
                    ExportTemplateName = "TN_THCS.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);
                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.GRADUATION_THCS);
                    FillDateAndUserReport(oBook, "M20", "B22");
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.TN_THCS);
                }
                else if (EducationGrade == ReportMenuDirectionConstant.Tertiary || EducationGrade == ReportMenuDirectionConstant.Tertiary_End)
                {
                    //Template THPT
                    TEMPLATE_NAME = "Bao_Cao_TN_THPT.xls";
                    ExportTemplateName = "TN_THPT.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);
                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.GRADUATION_THPT);
                    FillDateAndUserReport(oBook, "M20", "B22");
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.TN_THPT);
                }
                else if (EducationGrade == ReportMenuDirectionConstant.Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.Primary_Secondary_Teriary || EducationGrade == ReportMenuDirectionConstant.All_Grade)
                {
                    //Template THCS vs THPT
                    TEMPLATE_NAME = "Bao_Cao_TN_THCSTHPT.xls";
                    ExportTemplateName = "TN_THCSTHPT.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);
                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmis(schoolID, Year, ReportMenuDirectionConstant.GRADUATION_THCSTHPT);
                    FillDateAndUserReport(oBook, "M20", "B22");
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.SCHOOL_INFO);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.TN_THCS);
                    FillSheet(oBook, 3, list, ReportMenuDirectionConstant.TN_THPT);

                }
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = ExportTemplateName;
                return result;
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now,"ExportExcel", "null", ex);
            }
            return null;
        }

        public void FillDateAndUserReport(IVTWorkbook oBooks, string cellDate, string cellUser)
        {
            IVTWorksheet oSheet = oBooks.GetSheet(1);
            DateTime datenow = DateTime.Now;

            string Province = ProcessedCellDataBusiness.GetProvinceName(_globalInfo.ProvinceID.Value);
            int day = datenow.Day;
            int month = datenow.Month;
            int Year = datenow.Year;
            string DateReport = Province + "," + " ngày " + day + " tháng " + month + " năm " + Year;
            if (_globalInfo.IsAdminSchoolRole)
            {
                oSheet.SetCellValue(cellDate, DateReport);
                oSheet.SetCellValue(cellUser, "");
            }
            else
            {
                string UserName = _globalInfo.UserAccount.Employee.FullName;
                oSheet.SetCellValue(cellDate, DateReport);
                oSheet.SetCellValue(cellUser, UserName);
            }
        }

        private void FillSheet(IVTWorkbook oBooks, int sheetIndex, List<ProcessedCellData> lstInfoOriginal, string SheetName)
        {
            //condition là tên từng sheet muốn fill ra
            IVTWorksheet oSheet = oBooks.GetSheet(sheetIndex);
            if (lstInfoOriginal == null || lstInfoOriginal.Count == 0) { return; }
            List<ProcessedCellData> lstInfo = lstInfoOriginal.Where(p => p.SheetName == SheetName).ToList();
            string valuestr = string.Empty;
            string cell = string.Empty, value = string.Empty;

            for (int i = 0; i < lstInfo.Count; i++)
            {
                cell = lstInfo[i].CellAddress;
                value = lstInfo[i].CellValue;

                if (!String.IsNullOrEmpty(value) && !value.ToLower().Equals("null") && !value.ToLower().Equals("0"))
                {
                    if (sheetIndex == 1)
                    {
                        if (cell.Substring(0, 1).Equals("Z"))
                        {
                            oSheet.SetCellValue(cell, value);
                        }
                        else // #Z
                        {
                            valuestr = String.Format("'{0}", value);
                            oSheet.SetCellValue(cell, valuestr);
                        }
                    }
                    else
                    {
                        oSheet.SetCellValue(cell, value);
                    }
                }
            }
        }

    }

}