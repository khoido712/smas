﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportMenuDirectionArea
{
    public class ReportMenuDirectionAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportMenuDirectionArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportMenuDirectionArea_default",
                "ReportMenuDirectionArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
