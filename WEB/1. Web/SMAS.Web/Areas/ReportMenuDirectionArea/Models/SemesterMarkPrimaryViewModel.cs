﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Models
{
    public class SemesterMarkPrimaryViewModel
    {

        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("Common_Label_Semester")]
        public int? Semester { get; set; }

        public int FemaleType { get; set; }
        public int EthnicType { get; set; }
        public int ReportType { get; set; }
    }
}