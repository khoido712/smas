﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Models
{
    public class ReportCapacityByTeacherViewModel
    {
        #region Grid

        public int EmployeeID { get; set; }
        [ResourceDisplayName("Employee_Label_FullName")]
        public string EmployeeName { get; set; }
        [ResourceDisplayName("Employee_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }

        #endregion
        public int Semester { get; set; }
        public int SubjectID { get; set; }
    }
}