﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Models
{
    public class SubjectReportViewModel
    {
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Semester")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? Semester { get; set; }
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Period")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? Period { get; set; }
        [ResourceDisplayName("ClassProfile_Control_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? EducationLevel { get; set; }
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Subject")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? Subject { get; set; }
        public int? StatisticLevelReportID { get; set; }
        public int rdoReportBySemester { get; set; }

        public int chkPeriod { get; set; }

        public string MarkType { get; set; }

        public string MarkColumn { get; set; }

        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Employee")]
        public int? EmployeeID { get; set; }

        public string FemaleType { get; set; }

        public string EthnicType { get; set; }

        public string FemaleEthnicType { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        //1: Khoi, 2: Giao vien
        public int? GroupBy { get; set; }
    }
}