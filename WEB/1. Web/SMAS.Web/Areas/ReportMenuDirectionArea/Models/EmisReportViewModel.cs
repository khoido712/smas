﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Models
{
    public class EmisReportViewModel
    {
        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("Common_Label_Semester")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int Semester { get; set; }

        [ResourceDisplayName("SubCommittee_Label_AllTitle")]
        public int? SubCommitteeID { get; set; }

        public int FemaleType { get; set; }
        public int EthnicType { get; set; }
        public int FemaleEthnicType { get; set; }
        public int ReportTypeID { get; set; }
    }
}