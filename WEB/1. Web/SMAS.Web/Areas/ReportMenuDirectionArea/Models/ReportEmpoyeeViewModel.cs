﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Models
{
    public class ReportEmpoyeeViewModel
    {
        public int? SchoolFaculty { get; set; }
        public int? Math { get; set; }
        [ResourceDisplayName("ReportEmployeeProfile_Label_SeniorYear")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int SeniorYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public DateTime ReportDate { get; set; }
        public int rptReport { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int SemesterID { get; set; }
    }
}