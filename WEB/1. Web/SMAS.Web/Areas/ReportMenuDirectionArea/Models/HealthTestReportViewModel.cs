﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportMenuDirectionArea;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Models
{
    public class HealthTestReportViewModel
    {
        [ResourceDisplayName("HealthTestReport_Label_Period")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ReportMenuDirectionConstant.CBO_PERIOD)]
        public Nullable<int> HealthPeriod { get; set; }


        [ResourceDisplayName("HealthTestReport_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ReportMenuDirectionConstant.LIST_EDUCATIONLEVEL)]
        public Nullable<int> EducationLevelID { get; set; }

        [ScaffoldColumn(false)]
        public int ReportType { get; set; }

    }
}