﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMenuDirectionArea.Models
{
    public class SubjectPrimaryReportViewModel
    {
        public int EducationLevelID { get; set; }
        public int Semester { get; set; }
        public int SubjectID { get; set; }
        public int ReportType { get; set; }
        public int FemaleType { get; set; }
        public int EthnicType { get; set; }
        public int FemaleEthnicType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime Todate { get; set; }

    }
}