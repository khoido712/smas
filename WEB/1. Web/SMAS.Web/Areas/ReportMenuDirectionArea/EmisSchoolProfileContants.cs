﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EmisSchoolProfileArea
{
    public class EmisSchoolProfileContants
    {
       public const string YEAR = "YEAR";
       public const string StatusReport = "StatusReport";
       public const string MESSAGE_ERROR = "MessageError";
       public const int Primary = 1;
       public const int Secondary = 2;
       public const int Tertiary = 3;
       public const int Tertiary_End = 4;
       public const string EDUCATION_GRADE = "Education_Grade";
       public const int TrainTypeID = 3;//truong GDTX
       public const int Primary_Secondary = 3;
       public const int Secondary_Teriary = 6;
       public const int Primary_Secondary_Teriary = 7;
        
       public const string M_REPORT_CODE_PRIMARY = "HoSo_TieuHoc_T12";
       public const string M_REPORT_CODE_SECONDARY = "HoSo_THCS_T12";
       public const string M_REPORT_CODE_SECONDARY_TERIARY = "HoSo_PTTH_T12";
       public const string M_REPORT_CODE_TERIARY = "HoSo_THPT_T12";
       public const string M_REPORT_CODE_PRIMARY_SECONDARY = "HoSo_PTCS_T12";
       public const string M_REPORT_CODE_PST = "HoSo_PT_T12";

       public const string E_REPORT_CODE_PRIMARY = "HoSo_TH_T5";
       public const string E_REPORT_CODE_SECONDARY = "HoSo_THCS_T5";
       public const string E_REPORT_CODE_SECONDARY_TERIARY = "HoSo_PTTH_T5";
       public const string E_REPORT_CODE_TERIARY = "HoSo_THPT_T5";
       public const string E_REPORT_CODE_PRIMARY_SECONDARY = "HoSo_PTCS_T5";
       public const string E_REPORT_CODE_PST = "HoSo_PT_T5";
       public const string E_TTGDTX_CODE = "HoSo_TTGDTX_T5";

       public const string E_REPORT_CODE_PRIMARY_F = "HoSo_TieuHoc_T9";
       public const string E_REPORT_CODE_SECONDARY_F = "HoSo_THCS_T9";
       public const string E_REPORT_CODE_SECONDARY_TERIARY_F = "HoSo_PTTH_T9";
       public const string E_REPORT_CODE_TERIARY_F = "HoSo_THPT_T9";
       public const string E_REPORT_CODE_PRIMARY_SECONDARY_F = "HoSo_PTCS_T9";
       public const string E_REPORT_CODE_PST_F = "HoSo_PT_T9";
       public const string E_TTGDTX_CODE_F = "HoSo_TTGDTX_T9";
       

        /////////////Tên các sheet cấn fill dữ liêu cấp1 ////////////////////////
       public const string SCHOOL_INFO_PRIMARY = "Truong";
       public const string CLASS_PRIMARY = "LopHoc_TH";
       public const string PUPIL_PRIMARY = "HocSinh_TH";
       public const string PUPIL_ETHNIC_PRIMARY = "HocSinh_TH_DT";
       public const string TEACHER_PRIMARY = "NhanSu_TH";
       public const string PUPIL_RATE_PRIMARY = "DanhGiaHS_TH";
       public const string TEACHER_RATE_PRIMARY = "DanhGia_CBGVNV";
       public const string TEACHER_RATE_PRIMARY_END = "DanhGia_CBGVNV";
       public const string TEACHER_PRIMARY_FIRST = "NhanSu_TH";

        //////////Tên các sheet cần fill dữ liệu cấp 2////////////////
       public const string SCHOOL_INFO_SECONDARY = "Truong";
       public const string CLASS_SECONDARY = "LopHoc_THCS";
       public const string PUPIL_SECONDARY = "HocSinh_THCS";
       public const string PUPIL_ETHNIC_SECONDARY = "HocSinh_THCS_DT";
       public const string TEACHER_SECONDARY = "NhanSu_THCS";
       public const string PUPIL_RATE_SECONDARY = "DanhGiaHS_THCS";
       public const string SUBJECT_RATE_SECONDARY = "DanhGiaMH_THCS";
       public const string TEACHER_RATE_SECONDARY = "DanhGia_CBGVNV";
       public const string TEACHER_RATE_SECONDARY_END = "DanhGia_CBGVNV";

       //////////Tên các sheet cần fill dữ liệu cấp 3////////////////
       public const string SCHOOL_INFO_TERIARY = "Truong";
       public const string CLASS_TERIARY = "LopHoc_THPT";
       public const string PUPIL_TERIARY = "HocSinh_THPT";
       public const string PUPIL_ETHNIC_TERIARY = "HocSinh_THPT_DT";
       public const string TEACHER_TERIARY = "NhanSu_THPT";
       public const string PUPIL_RATE_TERIARY = "DanhGiaHS_THPT";
       public const string SUBJECT_RATE_TERIARY = "DanhGiaMH_THPT";
       public const string TEACHER_RATE_TERIARY = "DanhGia_CBGVNV";
       public const string TEACHER_RATE_TERIARY_END = "DanhGia_CBGVNV";

       ////////////Tên các sheet cần fill dữ liệu cấp 2,3//////////////////
       public const string SCHOOL_INFO_SECONDARY_TERIARY = "Truong";
       public const string CLASS_SECONDARY_TERIARY = "LopHoc_PTTH";
       public const string TEACHER_SECONDARY_TERIARY = "NhanSu_PTTH";
       public const string TEACHER_RATE_SECONDARY_TERIARY = "DanhGia_CBGVNV";
       public const string TEACHER_RATE_SECONDARY_TERIARY_END = "DanhGia_CBGVNV";

       /////////////Tên các sheet cân fill dữ liệu cấp 1,2 //////////////////
       public const string SCHOOL_INFO_PRIMARY_SECONDARY = "Truong";
       public const string CLASS_PRIMARY_SECONDARY = "LopHoc_PTCS";
       public const string TEACHER_PRIMARY_SECONDARY = "NhanSu_PTCS";
       public const string TEACHER_RATE_PRIMARY_SECONDARY = "DanhGia_CBGVNV";
       public const string TEACHER_RATE_PRIMARY_SECONDARY_END = "DanhGia_CBGVNV";

       //////////////Tên các sheet cần fill dữ liệu trường 3 cấp 1,2,3////////////////
       public const string SCHOOL_INFO_PST = "Truong";
       public const string CLASS_PST = "LopHoc_PT";
       public const string TEACHER_PST = "NhanSu_PT";
       public const string TEACHER_RATE_PST = "DanhGia_CBGVNV";
       public const string TEACHER_RATE_PST_END = "DanhGia_CBGVNV";

       //////////////Tên các sheet cần fill dữ liệu trường GDTX/////////////////
       public const string SCHOOL_INFO_GDTX = "Trungtam";
       public const string CLASS_GDTX = "Lophoc_TTGDTX";
       public const string PUPIL_GDTX = "Hocvien_TTGDTX";
       public const string TRAINING_PUPIL_GDTX = "Daotao_GDTX";
       public const string PUPIL_RATE_SECONDARY_GDTX = "DanhgiaHV_THCS";
       public const string PUPIL_RATE_TERIARY_GDTX = "DanhgiaHV_THPT";
       public const string PUPIL_RATE_ORTHER_GDTX = "DanhgiaHV_KHAC";
       public const string TEACHER_RATE_GDTX = "Danhgia_CBGVNV";
       public const string TEACHER_TTGDTX = "Nhansu_TTGDTX";
       public const string TRAINING_GDTX = "Daotao_GDTX";

       public const string REPORTTIME = "ReportTime";
       public const int PROVINCE_ID_LCU = 45;
    }
}