using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.Web.Areas.RankingCriteriaConfigArea.Models;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.RankingCriteriaConfigArea.Controllers
{
    public class RankingCriteriaConfigController : BaseController
    {
        //
        // GET: /RankingCriteriaConfigArea/RankingCriteriaConfig/
         private readonly IAcademicYearBusiness AcademicYearBusiness;

         public RankingCriteriaConfigController(IAcademicYearBusiness academicYearBusiness)
        {
           
            this.AcademicYearBusiness = academicYearBusiness;
        }


        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult SaveRanking(RankingCriteriaConfigForm ccf)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            dic["SchoolID"] = global.SchoolID.Value;
            dic["AcademicYearID "] = global.AcademicYearID.Value;
            List<AcademicYear> lsAcademicYear = AcademicYearBusiness.Search(dic).ToList();
            AcademicYear AcademicYear = lsAcademicYear.FirstOrDefault();
            AcademicYear.RankingCriteria = (int)((ccf.chkCapacity == true ? 1 : 0) * 1 + (ccf.chkConduct == true ? 1 : 0) * 2);
            AcademicYearBusiness.Update(AcademicYear);
            AcademicYearBusiness.Save();

            return View("RankingCriteriaConfig", ccf);
        }
    }
}
