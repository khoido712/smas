﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.RankingCriteriaConfigArea
{
    public class RankingCriteriaConfigAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RankingCriteriaConfigArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RankingCriteriaConfigArea_default",
                "RankingCriteriaConfigArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
