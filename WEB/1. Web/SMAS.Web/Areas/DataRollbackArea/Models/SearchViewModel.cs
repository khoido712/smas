﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DataRollbackArea.Models
{
    public class SearchViewModel
    {

        [ResourceDisplayName("DataRollback_Label_DataType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DataRollbackConstants.CBO_DATA_TYPE)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? DataType { get; set; }

        [ResourceDisplayName("DataRollback_Label_FromDate")]
        [UIHint("DateTimePickerBeforeNow")]
        [AdditionalMetadata("Default", "Pre30")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public DateTime FromDate { get; set; }

        [ResourceDisplayName("DataRollback_Label_ToDate")]
        [UIHint("DateTimePickerBeforeNow")]
        [AdditionalMetadata("Default", "now")]
        public DateTime? ToDate { get; set; }

        [ResourceDisplayName("DataRollback_Label_State")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DataRollbackConstants.CBO_ROLLBACK_STATE)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? RollbackState { get; set; }

        [ResourceDisplayName("DataRollback_Label_Province")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DataRollbackConstants.CBO_PROVINCE)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("OnChange", "onProvinceChange()")]
        public int? ProvinceId { get; set; }

        [ResourceDisplayName("DataRollback_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DataRollbackConstants.CBO_DISTRICT)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("OnChange", "onDistrictChange()")]
        public int? DistrictId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("DataRollback_Label_School")]
        public int? SchoolId { get; set; }
    }
}