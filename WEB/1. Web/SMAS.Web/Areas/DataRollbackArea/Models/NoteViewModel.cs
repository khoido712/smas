﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DataRollbackArea.Models
{
    public class NoteViewModel
    {
        [ResourceDisplayName("DataRollback_Label_DataTypeNote")]
        public string DataType { get; set; }

        [ResourceDisplayName("DataRollback_Label_AffectedData")]
        public string AffectedData { get; set; }
    }
}