﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DataRollbackArea.Models
{
    public class ListViewModel
    {
        public Guid RestoreDataId { get; set; }
        public DateTime DeleteTime { get; set; }

        [ResourceDisplayName("DataRollback_Label_DeleteTime")]
        public string StrDeleteTime
        {
            get
            {
                return DeleteTime.ToString("HH:mm dd/MM/yyyy");
            }
        }

        public int DataType { get; set; }

        [ResourceDisplayName("DataRollback_Label_DataType")]
        public string StrDataType
        {
            get
            {
                switch (this.DataType)
                {
                    case DataRollbackConstants.DATA_TYPE_SCHOOL:
                        return "Trường";
                    case DataRollbackConstants.DATA_TYPE_ACADEMIC_YEAR:
                        return "Năm học";
                    case DataRollbackConstants.DATA_TYPE_CLASS:
                        return "Lớp học";
                    case DataRollbackConstants.DATA_TYPE_PUPIL:
                        return "Học sinh";
                    case DataRollbackConstants.DATA_TYPE_MARK:
                        return "Điểm";
                    default:
                        return string.Empty;
                }
            }
        }

        [ResourceDisplayName("DataRollback_Label_DeleteContent")]
        public string DeleteContent { get; set; }


        public DateTime? RollbackTime { get; set; }

        [ResourceDisplayName("DataRollback_Label_RollbackTime")]
        public string StrRollbackTime
        {
            get
            {
                if (RollbackTime.HasValue && RollbackTime > DateTime.MinValue.AddDays(1))
                {
                    return RollbackTime.Value.ToString("HH:mm dd/MM/yyyy");
                }

                return string.Empty;
            }
        }

        public int RollbackState { get; set; }
    }
}
