﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.DataRollbackArea.Models;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Web.Constants;
using SMAS.Business.Common;
using System.Configuration;
using SMAS.Web.Filter;
using AutoMapper;
using System.Globalization;

/// Author: Aunh
/// 22-08-2012
namespace SMAS.Web.Areas.DataRollbackArea.Controllers
{
    [SkipCheckRole]
    public class DataRollbackController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        public DataRollbackController(IAcademicYearBusiness AcademicYearBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            IClassSubjectBusiness ClassSubject,
            IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            ISemeterDeclarationBusiness SemeterDeclarationBusiness,
            IPeriodDeclarationBusiness PeriodDeclarationBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            IProvinceBusiness ProvinceBusiness,
            IDistrictBusiness DistrictBusiness,
            IRestoreDataBusiness RestoreDataBusiness
            )
        {
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.SemeterDeclarationBusiness = SemeterDeclarationBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
        }

        #region Action
        public ViewResult Index()
        {
            this.SetViewData();
            return View();
        }

        [HttpPost]
        public PartialViewResult Search(SearchViewModel model)
        {

            if (ModelState.IsValid)
            {
                List<ListViewModel> lstResult = this._Search(model.DataType.GetValueOrDefault(), model.FromDate, model.ToDate, model.RollbackState, model.SchoolId);

                return PartialView("_List", lstResult);
            }
            else
            {
                string fieldName = "SchoolId";
                var m = ViewData.ModelState[fieldName];

                if (m != null && m.Errors.Count > 0)
                {
                    ViewData.ModelState.Remove(fieldName);
                    ViewData.ModelState.AddModelError(fieldName, "Thông tin trường không hợp lệ");
                }

                fieldName = "ToDate";
                m = ViewData.ModelState[fieldName];

                if (m != null && m.Errors.Count > 0)
                {
                    ViewData.ModelState.Remove(fieldName);
                    ViewData.ModelState.AddModelError(fieldName, "Thông tin tìm kiếm không hợp lệ, vui lòng kiểm tra lại");
                }

                string errorMessage = string.Join(",", ModelState.Values.Where(u => u.Errors.Count > 0).Select(u => string.Join(",", u.Errors.Select(v => v.ErrorMessage))));
                throw new BusinessException(errorMessage);
            }

        }

        public PartialViewResult OpenConfirmDialog(Guid id)
        {
            RESTORE_DATA rd = RestoreDataBusiness.Find(id);
            ListViewModel model = new ListViewModel
            {
                DataType = rd.RESTORE_DATA_TYPE_ID,
                DeleteContent = rd.SHORT_DESCRIPTION,
                DeleteTime = rd.DELETED_DATE,
                RestoreDataId = rd.RESTORE_DATA_ID,
                RollbackState = rd.RESTORED_STATUS,
                RollbackTime = rd.RESTORED_DATE
            };

            List<NoteViewModel> lstNote = new List<NoteViewModel>()
            {
                new NoteViewModel
                {
                    DataType = "Trường",
                    AffectedData = "Mã trường có thể bị thay đổi"
                },
                new NoteViewModel
                {
                    DataType = "Năm  học",
                    AffectedData = "Thời gian năm học có thể bị thay đổi"
                },
                new NoteViewModel
                {
                    DataType = "Lớp học",
                    AffectedData = "Tên lớp học có thể bị thay đổi"
                },
                new NoteViewModel
                {
                    DataType = "Học sinh",
                    AffectedData = "Mã học sinh có thể bị thay đổi"
                },
                new NoteViewModel
                {
                    DataType = "Điểm",
                    AffectedData = "Điểm môn học hiện tại sẽ bị ghi đè bởi dữ liệu khôi phục"
                },
            };

            ViewData[DataRollbackConstants.LIST_NOTE] = lstNote;

            return PartialView("_ConfirmDialog", model);
        }

        public JsonResult AjaxLoadDistrict(int? provinceId)
        {
            List<District> lstDistrict;

            if (provinceId != null && provinceId != 0)
            {
                lstDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == provinceId && o.IsActive.HasValue && o.IsActive.Value).OrderBy(s => s.DistrictName).ToList();
            }
            else
            {
                lstDistrict = new List<District>();
            }

            return Json(new SelectList(lstDistrict, "DistrictID", "DistrictName"));
        }

        public PartialViewResult AjaxLoadSchool(int? provinceId, int? districtId)
        {
            List<KeyValuePair<int, string>> lstSchoolProfile;
            if (provinceId.HasValue && provinceId != 0 && districtId.HasValue && districtId != 0)
            {
                lstSchoolProfile = SchoolProfileBusiness.All.Where(o => o.ProvinceID == provinceId && o.DistrictID == districtId)
                    .ToList().Select(o => new KeyValuePair<int, string>(o.SchoolProfileID, o.SchoolName)).OrderBy(s => s.Value).ToList();
            }
            else
            {
                lstSchoolProfile = new List<KeyValuePair<int, string>>();
            }
            ViewData[DataRollbackConstants.CBO_SCHOOL] = lstSchoolProfile;
            return PartialView("_SchoolAutoComplete");
        }

        [HttpPost]
        public JsonResult RestoreData(Guid id)
        {
            string retVal = RestoreDataBusiness.RestoreData(id);
            if (!string.IsNullOrEmpty(retVal))
            {
                return Json(new JsonMessage(retVal, "error"));
            }
            return Json(new JsonMessage("Khôi phục dữ liệu thành công"));
        }
        #endregion

        #region Private Methods
        private void SetViewData()
        {
            //Loai du lieu
            List<KeyValuePair<int, string>> lstDataType = new List<KeyValuePair<int, string>>();
            if (_globalInfo.IsSystemAdmin)
            {
                lstDataType.Add(new KeyValuePair<int, string>(DataRollbackConstants.DATA_TYPE_SCHOOL, "Trường"));
            }
            lstDataType.Add(new KeyValuePair<int, string>(DataRollbackConstants.DATA_TYPE_ACADEMIC_YEAR, "Năm học"));
            lstDataType.Add(new KeyValuePair<int, string>(DataRollbackConstants.DATA_TYPE_CLASS, "Lớp học"));
            lstDataType.Add(new KeyValuePair<int, string>(DataRollbackConstants.DATA_TYPE_PUPIL, "Học sinh"));
            lstDataType.Add(new KeyValuePair<int, string>(DataRollbackConstants.DATA_TYPE_MARK, "Điểm"));

            ViewData[DataRollbackConstants.CBO_DATA_TYPE] = new SelectList(lstDataType, "Key", "Value");

            //Trang thai
            List<KeyValuePair<int, string>> lstRollbackState = new List<KeyValuePair<int, string>>()
            {
                new KeyValuePair<int,string>(DataRollbackConstants.ROLLBACK_STATE_DONE, "Đã khôi phục"),
                new KeyValuePair<int,string>(DataRollbackConstants.ROLLBACK_STATE_UNDONE, "Chưa khôi phục"),
            };

            ViewData[DataRollbackConstants.CBO_ROLLBACK_STATE] = new SelectList(lstRollbackState, "Key", "Value");

            //Tinh
            List<Province> lstProvince = ProvinceBusiness.All.Where(o => o.IsActive.HasValue && o.IsActive.Value).OrderBy(s => s.ProvinceName).ToList();
            ViewData[DataRollbackConstants.CBO_PROVINCE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");

            //Quan huyen
            List<District> lstDist = new List<District>();
            ViewData[DataRollbackConstants.CBO_DISTRICT] = new SelectList(lstDist, "DistrictID", "DistrictName");

            //Truong
            ViewData[DataRollbackConstants.CBO_SCHOOL] = new List<KeyValuePair<int, string>>();
        }

        private List<ListViewModel> _Search(int dataType, DateTime fromDate, DateTime? toDate, int? status, int? schoolId)
        {
            List<ListViewModel> lstResult;
            int? schoolID;
            if (_globalInfo.IsSystemAdmin)
            {
                schoolID = schoolId;
            }
            else
            {
                schoolID = _globalInfo.SchoolID.Value;
            }

            //toDate = toDate.HasValue ? toDate.Value.AddHours(23) : null;

            IQueryable<RESTORE_DATA> query = RestoreDataBusiness.All.Where(s => s.DELETED_DATE >= fromDate);
            if (toDate.HasValue)
            {
                toDate = toDate.Value.AddHours(24);
                query = query.Where(s => s.DELETED_DATE <= toDate.Value);
            }

            if (dataType > 0)
            {
                query = query.Where(s => s.RESTORE_DATA_TYPE_ID == dataType);
            }
            if (status.HasValue)
            {
                query = query.Where(s => s.RESTORED_STATUS == status);
            }
            if (schoolID > 0)
            {
                query = query.Where(s => s.SCHOOL_ID == schoolID);
            }

            List<RESTORE_DATA> lstRestoreData = query.ToList();
            lstResult = lstRestoreData.Select(o => new ListViewModel
            {
                RestoreDataId = o.RESTORE_DATA_ID,
                DataType = o.RESTORE_DATA_TYPE_ID,
                DeleteContent = o.SHORT_DESCRIPTION,
                DeleteTime = o.DELETED_DATE,
                RollbackState = o.RESTORED_STATUS,
                RollbackTime = o.RESTORED_DATE
            }).OrderByDescending(s => s.DeleteTime).ToList();

            return lstResult;
        }
        #endregion

    }
}
