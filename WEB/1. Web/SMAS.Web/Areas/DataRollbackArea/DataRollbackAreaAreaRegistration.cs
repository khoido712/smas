﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DataRollbackArea
{
    public class DataRollbackAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DataRollbackArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DataRollbackArea_default",
                "DataRollbackArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
