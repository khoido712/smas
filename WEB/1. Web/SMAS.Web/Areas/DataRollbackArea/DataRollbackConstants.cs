﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DataRollbackArea
{
    public class DataRollbackConstants
    {
        public const string CBO_DATA_TYPE = "CBO_DATA_TYPE";
        public const string CBO_ROLLBACK_STATE = "CBO_ROLLBACK_STATE";
        public const string CBO_PROVINCE = "CBO_PROVINCE";
        public const string CBO_DISTRICT = "CBO_DISTRICT";
        public const string CBO_SCHOOL = "CBO_SCHOOL";
        public const string LIST_NOTE = "LIST_NOTE";
        public const int DATA_TYPE_SCHOOL = 1;
        public const int DATA_TYPE_ACADEMIC_YEAR = 2;
        public const int DATA_TYPE_CLASS = 3;
        public const int DATA_TYPE_PUPIL = 4;
        public const int DATA_TYPE_MARK = 5;
        public const int ROLLBACK_STATE_DONE = 1;
        public const int ROLLBACK_STATE_UNDONE = 0;
    }
}