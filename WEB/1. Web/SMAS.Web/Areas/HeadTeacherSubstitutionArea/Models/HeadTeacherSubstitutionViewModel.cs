/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.HeadTeacherSubstitutionArea.Models
{
    public class HeadTeacherSubstitutionViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 HeadTeacherSubstitutionID { get; set; }

        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("HeadTeacherSubstitution_Label_Class")]
        public System.Int32 ClassID { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 SchoolID { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 AcademicYearID { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 HeadTeacherID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("HeadTeacherSubstitution_Label_SubstituedHeadTeacher")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Int32 SubstituedHeadTeacher { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("HeadTeacherSubstitution_Label_AssignedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public System.Nullable<System.DateTime> AssignedDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("HeadTeacherSubstitution_Label_EndDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public System.Nullable<System.DateTime> EndDate { get; set; }

        #region nhung truong xuat hien tren grid

        [ResourceDisplayName("HeadTeacherSubstitution_Label_ClassDisplayName")]
        public string ClassDisplayName { get; set; }

        [ResourceDisplayName("HeadTeacherSubstitution_Label_HeadTeacher")]
        public string HeadTeacherFullName { get; set; }

        [ResourceDisplayName("HeadTeacherSubstitution_Label_HeadTeacher")]
        public string HeadTeacherName { get; set; }


        [ResourceDisplayName("HeadTeacherSubstitution_Label_HeadTeacher")]
        public string SubstituedHeadTeacherName { get; set; }


        [ResourceDisplayName("HeadTeacherSubstitution_Label_SubstituedHeadTeacher")]
        public string SubstituedHeadTeacherFullName { get; set; }

        [ResourceDisplayName("HeadTeacherSubstitution_Label_FromDateToDate")]
        public string FromDateToDate { get; set; }

        [ResourceDisplayName("HeadTeacherSubstitution_Label_Description")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }

        //truong them trogn form edit
        [ResourceDisplayName("HeadTeacherSubstitution_Label_EducationLevel")]
        public string EducationLevelName { get; set; }

        [ResourceDisplayName("HeadTeacherSubstitution_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int FacultyID { get; set; }

        #endregion nhung truong xuat hien tren grid
    }
}