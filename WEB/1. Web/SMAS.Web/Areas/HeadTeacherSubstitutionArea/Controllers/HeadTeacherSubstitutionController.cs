﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author dungnt
* @version $Revision: $
*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.HeadTeacherSubstitutionArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.HeadTeacherSubstitutionArea.Controllers
{
    public class HeadTeacherSubstitutionController : BaseController
    {
        private readonly IHeadTeacherSubstitutionBusiness HeadTeacherSubstitutionBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;

        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;

        public HeadTeacherSubstitutionController(IHeadTeacherSubstitutionBusiness headteachersubstitutionBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IEmployeeBusiness EmployeeBusiness, IEducationLevelBusiness EducationLevelBusiness
            , ISchoolFacultyBusiness SchoolFacultyBusiness)
        {
            this.HeadTeacherSubstitutionBusiness = headteachersubstitutionBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
        }

        //
        // GET: /HeadTeacherSubstitution/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            GlobalInfo global = new GlobalInfo();

            //Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            SearchInfo["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            SetViewData();
            ViewData[HeadTeacherSubstitutionConstants.HAS_PERMISSION] = GetMenupermission("HeadTeacherSubstitution", global.UserAccountID, global.IsAdmin);
            IEnumerable<HeadTeacherSubstitutionViewModel> lst = this._Search(SearchInfo);
            ViewData[HeadTeacherSubstitutionConstants.LIST_HEADTEACHERSUBSTITUTION] = lst;
            ViewData[HeadTeacherSubstitutionConstants.IS_CURRENT_YEAR] = global.IsCurrentYear;
            return View();
        }

        //
        // GET: /HeadTeacherSubstitution/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            GlobalInfo global = new GlobalInfo();

            //Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            SearchInfo["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            //

            IEnumerable<HeadTeacherSubstitutionViewModel> lst = this._Search(SearchInfo);
            ViewData[HeadTeacherSubstitutionConstants.LIST_HEADTEACHERSUBSTITUTION] = lst;
            ViewData[HeadTeacherSubstitutionConstants.IS_CURRENT_YEAR] = global.IsCurrentYear;
            ViewData[HeadTeacherSubstitutionConstants.HAS_PERMISSION] = GetMenupermission("HeadTeacherSubstitution", global.UserAccountID, global.IsAdmin);
            //Nếu năm học đang thao tác là năm học cũ, disable các link sửa, xóa, button thêm mới
            //??

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("HeadTeacherSubstitution", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            HeadTeacherSubstitution headteachersubstitution = new HeadTeacherSubstitution();
            TryUpdateModel(headteachersubstitution);
            Utils.Utils.TrimObject(headteachersubstitution);
            //Lay giao vien chu nhiem
            Employee headTeacher = GetHeadTeacher(headteachersubstitution.ClassID);
            if (headTeacher == null)
            {
                return Json(new JsonMessage(Res.Get("HeadTeacherSubstitution_Label_HeadTeacherName"), "error"));
            }
            //bổ sung thêm thuộc tính AcademicYearID = UserInfo_AcademicYearID, SchoolID = UserInfo_SchoolID
            headteachersubstitution.AcademicYearID = global.AcademicYearID.Value;
            headteachersubstitution.SchoolID = global.SchoolID.Value;

            //add headtearcher id
            headteachersubstitution.HeadTeacherID = headTeacher.EmployeeID;

            this.HeadTeacherSubstitutionBusiness.Insert(headteachersubstitution);
            this.HeadTeacherSubstitutionBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int HeadTeacherSubstitutionID)
        {
            this.CheckPermissionForAction(HeadTeacherSubstitutionID, "HeadTeacherSubstitution");
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("HeadTeacherSubstitution", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            HeadTeacherSubstitution headteachersubstitution = this.HeadTeacherSubstitutionBusiness.Find(HeadTeacherSubstitutionID);
            TryUpdateModel(headteachersubstitution);
            Utils.Utils.TrimObject(headteachersubstitution);
            //Lay giao vien chu nhiem
            Employee headTeacher = GetHeadTeacher(headteachersubstitution.ClassID);
            if (headTeacher == null)
            {
                return Json(new JsonMessage(Res.Get("HeadTeacherSubstitution_Label_HeadTeacherName"), "error"));
            }
            //add headtearcher id
            headteachersubstitution.HeadTeacherID = headTeacher.EmployeeID;

            this.HeadTeacherSubstitutionBusiness.Update(headteachersubstitution);
            this.HeadTeacherSubstitutionBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            //- Kiểm tra quyền xóa của người đăng nhập

            //??
            this.CheckPermissionForAction(id, "HeadTeacherSubstitution");
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("HeadTeacherSubstitution", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.HeadTeacherSubstitutionBusiness.Delete(global.SchoolID.Value, id);
            this.HeadTeacherSubstitutionBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<HeadTeacherSubstitutionViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<HeadTeacherSubstitution> query = from p in this.HeadTeacherSubstitutionBusiness.Search(SearchInfo)
                                                        join q in EmployeeBusiness.All on p.HeadTeacherID equals q.EmployeeID
                                                        where q.IsActive == true
                                                        select p;
            IQueryable<HeadTeacherSubstitution> queryResult = from p in query
                                                        join q in EmployeeBusiness.All on p.SubstituedHeadTeacher equals q.EmployeeID
                                                        where q.IsActive == true
                                                        select p;
            IQueryable<HeadTeacherSubstitutionViewModel> lst = queryResult.Select(o => new HeadTeacherSubstitutionViewModel
            {
                HeadTeacherSubstitutionID = o.HeadTeacherSubstitutionID,
                ClassID = o.ClassID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                HeadTeacherID = o.HeadTeacherID,
                SubstituedHeadTeacher = o.SubstituedHeadTeacher,
                AssignedDate = o.AssignedDate,
                EndDate = o.EndDate,
                Description = o.Description
            });

            List<HeadTeacherSubstitutionViewModel> lsHeadTeacherSubstitutionViewModel = lst.Where(o => o.SubstituedHeadTeacher != 0).ToList();

            foreach (HeadTeacherSubstitutionViewModel item in lsHeadTeacherSubstitutionViewModel)
            {
                //dien ten lop
                ClassProfile thisClass = ClassProfileBusiness.All.Where(o => (o.ClassProfileID == item.ClassID) && o.IsActive.Value).FirstOrDefault();
                item.ClassDisplayName = thisClass.DisplayName;

                item.FromDateToDate = "";

                //dien tu ngay - den ngay
                if (item.AssignedDate.HasValue)
                {
                    item.FromDateToDate += item.AssignedDate.Value.ToString("dd/MM/yyyy");
                }
                if (item.EndDate.HasValue)
                {
                    item.FromDateToDate += " - " + item.EndDate.Value.ToString("dd/MM/yyyy");
                }

                //dien ten giao vien chu nhiem va giao vien day thay
                Employee objHeadTeacher = EmployeeBusiness.Find(item.HeadTeacherID);
                item.HeadTeacherFullName = objHeadTeacher.FullName;
                item.HeadTeacherName = objHeadTeacher.Name;

                Employee objSubstituedHeadTeacher = EmployeeBusiness.Find(item.SubstituedHeadTeacher);
                item.SubstituedHeadTeacherFullName = objSubstituedHeadTeacher.FullName;
                item.SubstituedHeadTeacherName = objSubstituedHeadTeacher.Name;

                //phuc vu cho form edit,dua ten khoi hoc vao model
                EducationLevel objEducationLevel = EducationLevelBusiness.Find(thisClass.EducationLevelID);
                item.EducationLevelName = objEducationLevel.Resolution;

                //dua facultyID vao  de xu ly trong form edit
                if (objSubstituedHeadTeacher.SchoolFacultyID.HasValue)
                {
                    item.FacultyID = objSubstituedHeadTeacher.SchoolFacultyID.Value;
                }
            }
            GlobalInfo global = new GlobalInfo();
            ViewData[HeadTeacherSubstitutionConstants.HAS_PERMISSION] = GetMenupermission("HeadTeacherSubstitution", global.UserAccountID, global.IsAdmin);
            return lsHeadTeacherSubstitutionViewModel.OrderByDescending(o => o.AssignedDate).ThenBy(o => o.SubstituedHeadTeacherName).ThenBy(o => o.SubstituedHeadTeacherFullName).ToList();
        }

        #region SetviewData

        /// <summary>
        /// truyen du lieu mac dinh cho grid va cac popup create edit
        /// </summary>
        public void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();

            //fill du lieu cho cboEducationLevel
            ViewData[HeadTeacherSubstitutionConstants.LS_EDUCATIONLEVEL] = new SelectList(global.EducationLevels, "EducationLevelID", "Resolution");

            //vi khoi hoc co gia tri mac dinh nhu tren,khi load form ta cung phai set gia tri mac dinh
            //cho cboClass
            int DefaultEducationLevel = global.EducationLevels.FirstOrDefault().EducationLevelID;
            IQueryable<ClassProfile> lsDefaultClass = GetClassFromEducationLevel(DefaultEducationLevel);
            ViewData[HeadTeacherSubstitutionConstants.LS_CLASS] = new SelectList(lsDefaultClass.ToList(), "ClassProfileID", "DisplayName"); ;

            //cboFacultyName: lấy danh sách từ SchoolFacultyBusiness.SearchBySchool(Dictionary<string, object>)
            //với Dictionary[“SchoolID”] = UserInfo.SchoolID và Dictionary[“IsActive”] = 1
            IQueryable<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"SchoolID",global.SchoolID.Value}
                ,{"IsActive",1}
            }).OrderBy(o => o.FacultyName);

            ViewData[HeadTeacherSubstitutionConstants.LS_SHOOLFACULTY] = new SelectList(lsSchoolFaculty.ToList(), "SchoolFacultyID", "FacultyName"); ;

            //cboHeadTeacherSubstitution: Không có dữ liệu
            ViewData[HeadTeacherSubstitutionConstants.LS_HEADTEACHERSUBSTITUTION] = new SelectList(new string[] { });
        }

        #endregion SetviewData

        #region ham ho tro combobox

        /// <summary>
        /// lay lop tu khoi hoc
        /// </summary>
        /// <param name="EducationLevelID">id khoi hoc</param>
        /// <returns>danh sach lop</returns>
        public IQueryable<ClassProfile> GetClassFromEducationLevel(int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();

            //Lấy danh sách từ ClassProfileBusiness.Search(Dictionary) theo các tiêu chí
            //Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID,
            //Dictionary[“EducationLevelID”] = cboEducationLevel.Value.
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() {
            {"AcademicYearID",global.AcademicYearID.Value}
            ,{"EducationLevelID",EducationLevelID}
            }).OrderBy(o => o.DisplayName);

            return lsClass;
        }

        /// <summary>
        /// lay giao vien chu nhiem qua id lop
        /// </summary>
        /// <param name="ClassID">id lop</param>
        /// <returns>giao vien chu nhiem</returns>
        public Employee GetHeadTeacher(int ClassID)
        {
            //hàm ClassProfileBusiness.Search(Dictionary).FirstOrDefault với,
            //Dictionary[“ClassProfileID”] = ClassID

            //ClassProfile ClassHeadTeacher = ClassProfileBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"ClassProfileID",ClassID}
            //}).FirstOrDefault();

            ClassProfile ClassHeadTeacher = ClassProfileBusiness.Find(ClassID);

            //lay HeadTeacherID roi tim qua employee
            Employee HeadTeacher = EmployeeBusiness.Find(ClassHeadTeacher.HeadTeacherID);
            return HeadTeacher;
        }

        public IQueryable<Employee> GetHeadTeacherSubstitution(int FacultyID)
        {
            //EmployeeBussiness.SearchTeacher(IDictionary) với
            //IDictionary[“FacultyID”] = FacultyID và
            //IDictionary[“SchoolID”] = UserInfo.SchoolID
            GlobalInfo global = new GlobalInfo();

            IQueryable<Employee> lsHeadTeacherSubstitution = EmployeeBusiness.SearchWorkingTeacherByFaculty(global.SchoolID.Value, FacultyID).OrderBy(o => o.Name).ThenBy(o => o.FullName);
            //lsHeadTeacherSubstitution = lsHeadTeacherSubstitution.Where(o => o.AppliedLevel == global.AppliedLevel);
            return lsHeadTeacherSubstitution;
        }

        #endregion ham ho tro combobox

        #region cac ham trong combobox

        /// <summary>
        /// ham dc goi khi combobox khoi hoc dc thay doi
        /// </summary>
        /// <param name="educationLevelID">id khoi hoc dc chon</param>
        /// <returns>danh sach lop theo khoi</returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetcboClass(int? educationLevelID)
        {
            if (educationLevelID.HasValue)
            {
                List<ClassProfile> lsClassProfile = GetClassFromEducationLevel(educationLevelID.Value).ToList();
                return Json(new SelectList(lsClassProfile, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ham dc goi khi combobox to bo mon thay doi
        /// </summary>
        /// <param name="FacultyID">id to bo mon dc chon</param>
        /// <returns>danh sach can bo trong to bo mon day</returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetcboFaculty(int? FacultyID, int? classID)
        {
            int headTeacherID = 0;
            if (classID != 0 && classID != null)
            {
                ClassProfile objClass = ClassProfileBusiness.Find(classID);
                if (objClass != null && objClass.HeadTeacherID.HasValue)
                {
                  headTeacherID = ClassProfileBusiness.Find(classID).HeadTeacherID.Value;
                }
            }
            if (FacultyID.HasValue)
            {
                IQueryable<Employee> lsTeacher = GetHeadTeacherSubstitution(FacultyID.Value);

                lsTeacher = lsTeacher.Where(o => o.EmployeeID != headTeacherID);
                return Json(new SelectList(lsTeacher, "EmployeeID", "FullName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// lay ten giao vien chu nhiem dua vao id lop
        /// </summary>
        /// <param name="ClassID">id lop</param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetHeadTeacherName(int? ClassID)
        {
            if (ClassID.HasValue)
            {
                Employee HeadTeacher = GetHeadTeacher(ClassID.Value);

                if (HeadTeacher == null || string.IsNullOrEmpty(HeadTeacher.FullName))
                {
                    return Json(new JsonMessage(Res.Get("HeadTeacherSubstitution_Label_HeadTeacherName"),"error"));
                }

                return Json(new JsonMessage(HeadTeacher.FullName, HeadTeacher.EmployeeID.ToString()));
            }
            else
            {
                return Json(new JsonMessage(""));
            }
        }

        #endregion cac ham trong combobox
    }
}
