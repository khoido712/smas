/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.HeadTeacherSubstitutionArea
{
    public class HeadTeacherSubstitutionConstants
    {
        public const string LIST_HEADTEACHERSUBSTITUTION = "listHeadTeacherSubstitution";
        public const string LS_EDUCATIONLEVEL = "LS_EDUCATIONLEVEL";

        public const int CAP1 = 1;
        public const string CHOSEN_EDUCATIONLEVEL = "CHOSEN_EDUCATIONLEVEL";
        public const int CAP2 = 2;
        public const int CAP3 = 3;
        public const int LOP1 = 1;
        public const int LOP6 = 6;
        public const int LOP10 = 10;
        public const string LS_SHOOLFACULTY = "LS_SHOOLFACULTY";
        public const string LS_CLASS = "LS_CLASS";
        public const string LS_HEADTEACHERSUBSTITUTION = "LS_HEADTEACHERSUBSTITUTION";

        public const string FACULTYID = "FACULTYID";

        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";
        public const string HAS_PERMISSION = "HasPermission";
    }
}