﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HeadTeacherSubstitutionArea
{
    public class HeadTeacherSubstitutionAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HeadTeacherSubstitutionArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HeadTeacherSubstitutionArea_default",
                "HeadTeacherSubstitutionArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}