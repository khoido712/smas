﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DataInputMonitorArea
{
    public class DataInputMonitorAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DataInputMonitorArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DataInputMonitorArea_default",
                "DataInputMonitorArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
