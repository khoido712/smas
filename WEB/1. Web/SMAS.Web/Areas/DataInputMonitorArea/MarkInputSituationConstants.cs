﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DataInputMonitorArea
{
    public class DataInputMonitorConstants
    {
        public const string CBO_EDUCATION_LEVEL = "CBO_EDUCATION_LEVEL";
        public const string CBO_SEMESTER = "CBO_SEMESTER";
        public const string CBO_SUBJECT = "CBO_SUBJECT";
    }
}