﻿using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Business.Business;
using System.IO;
using SMAS.VTUtils.HtmlHelpers.Control;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.DataInputMonitorArea.Controllers
{
    public class DataInputMonitorController : BaseController
    {
        #region properties
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkInputSituationBusiness MarkInputSituationBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IEvaluationCommentsBusiness EvaluationCommentsBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness;
        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private readonly IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;

        #endregion

        #region Constructor
        public DataInputMonitorController(IClassSubjectBusiness ClassSubjectBusiness, IEducationLevelBusiness EducationLevelBusiness,
            IClassProfileBusiness ClassProfileBusiness, ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness,
            ITeachingAssignmentBusiness TeachingAssignmentBusiness, IPupilOfClassBusiness PupilOfClassBusiness,
            IMarkInputSituationBusiness MarkInputSituationBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness, IEvaluationCommentsBusiness EvaluationCommentsBusiness,
            ISummedEvaluationBusiness SummedEvaluationBusiness, ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness,
            IEmployeeBusiness employeeBusiness, IRatedCommentPupilBusiness ratedCommentPupilBusiness, IRatedCommentPupilHistoryBusiness ratedCommentPupilHistoryBusiness)
        {
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.MarkInputSituationBusiness = MarkInputSituationBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ReportMarkInputSituationBusiness = ReportMarkInputSituationBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.EvaluationCommentsBusiness = EvaluationCommentsBusiness;
            this.SummedEvaluationBusiness = SummedEvaluationBusiness;
            this.SummedEndingEvaluationBusiness = SummedEndingEvaluationBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.RatedCommentPupilBusiness = ratedCommentPupilBusiness;
            this.RatedCommentPupilHistoryBusiness = ratedCommentPupilHistoryBusiness;
        }
        #endregion

        #region Actions

        public ActionResult Index()
        {
            SetViewData();

            return View();
        }

        [HttpPost]
        public JsonResult AjaxLoadSubject(int educationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            List<SubjectCatBO> lstSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                            .OrderBy(c => c.SubjectCat.OrderInSubject)
                                            .ThenBy(c => c.SubjectCat.DisplayName)
                                            .Select(o => new SubjectCatBO
                                            {
                                                SubjectCatID = o.SubjectID,
                                                DisplayName = o.SubjectCat.DisplayName,
                                                IsCommenting = o.IsCommenting
                                            }).ToList();

            return Json(new SelectList(lstSubject, "SubjectCatID", "DisplayName"), JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Report
        public FileResult ExportExcel(int semester, int educationLevel, int? subjectId, int type, int evaluationId)
        {
            FileStreamResult result;
            string strSemester = semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII";
            if (type == 1)
            {
                Stream excel = CreateReportSubjectAndEducationGroup(semester, educationLevel, subjectId);
                result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = "HS_TH_TinhHinhNhapDL_Monhoc_" + educationLevel + "_" + strSemester + ".xls";
            }
            else
            {
                Stream excel = CreateReportCapQua(semester, educationLevel, evaluationId);
                result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = "HS_TH_TinhHinhNhapDL_NLPC_" + educationLevel + "_" + strSemester + ".xls";
            }
            return result;
        }
        public FileResult ExportPDF(int semester, int educationLevel, int? subjectId, int type, int evaluationId)
        {
            string strSemester = semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII";
            string FileDownloadName = string.Empty;
            if (type == 1)
            {
                Stream excel = CreateReportSubjectAndEducationGroup(semester, educationLevel, subjectId);
                FileDownloadName = "HS_TH_TinhHinhNhapDL_Monhoc_" + educationLevel + "_" + strSemester + ".xls";
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, FileDownloadName, 0);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF); ;
            }
            else
            {
                Stream excel = CreateReportCapQua(semester, educationLevel, evaluationId);
                FileDownloadName = "HS_TH_TinhHinhNhapDL_NLPC_" + educationLevel + "_" + strSemester + ".xls";
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, FileDownloadName, 0);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF); ;
            }
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            ViewData[DataInputMonitorConstants.CBO_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value", _globalInfo.Semester);

            //Lay danh sach khoi
            List<EducationLevel> lstEducationLevel = _globalInfo.EducationLevels;
            ViewData[DataInputMonitorConstants.CBO_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            //Lay danh sach mon
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = lstEducationLevel.FirstOrDefault().EducationLevelID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            List<SubjectCatBO> lstSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                            .OrderBy(c => c.SubjectCat.OrderInSubject)
                                            .ThenBy(c => c.SubjectCat.DisplayName)
                                            .Select(o => new SubjectCatBO
                                            {
                                                SubjectCatID = o.SubjectID,
                                                DisplayName = o.SubjectCat.DisplayName,
                                                IsCommenting = o.IsCommenting
                                            }).ToList();
            ViewData[DataInputMonitorConstants.CBO_SUBJECT] = new SelectList(lstSubject, "SubjectCatID", "DisplayName");
        }

        private Stream CreateReportSubjectAndEducationGroup(int SemesterID, int EducationLevelID, int? SubjectID)
        {
            string reportCode = SystemParamsInFile.REPORT_DATA_INPUT_MONITOR_SAEG;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet CurrentSheet = oBook.GetSheet(1);
            #region Lay du lieu
            int academicYearId = _globalInfo.AcademicYearID.Value;
            IQueryable<PupilOfClassBO> iqPOC = (from poc in PupilOfClassBusiness.All
                                                join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                where poc.AcademicYearID == _globalInfo.AcademicYearID
                                                && poc.SchoolID == _globalInfo.SchoolID
                                                && cp.EducationLevelID == EducationLevelID
                                                && cp.IsActive.HasValue && cp.IsActive.Value
                                                && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                && cp.AcademicYearID == academicYearId
                                                select new PupilOfClassBO
                                                {
                                                    PupilID = poc.PupilID,
                                                    ClassID = poc.ClassID,
                                                    ClassName = cp.DisplayName,
                                                    OrderInClass = cp.OrderNumber
                                                });
            var lstGroup = (from g in iqPOC
                            group g by new
                            {
                                g.ClassID,
                                g.OrderInClass,
                                g.ClassName,
                            } into gb
                            select new
                            {
                                gb.Key.ClassID,
                                gb.Key.OrderInClass,
                                gb.Key.ClassName,
                                total = gb.Count()
                            }).OrderBy(p => p.OrderInClass).ThenBy(p => p.ClassName).ToList();
            List<int> lstClassID = lstGroup.Select(p => p.ClassID).Distinct().ToList();
            List<int> lstPupilID = iqPOC.Select(p => p.PupilID).Distinct().ToList();
            //list mon hoc
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            if (SubjectID.HasValue)
            {
                dic["SubjectID"] = SubjectID.Value;
            }

            List<SubjectCatBO> lstSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                            .OrderBy(c => c.SubjectCat.OrderInSubject)
                                            .ThenBy(c => c.SubjectCat.DisplayName)
                                            .Select(o => new SubjectCatBO
                                            {
                                                SubjectCatID = o.SubjectID,
                                                DisplayName = o.SubjectCat.DisplayName,
                                                IsCommenting = o.IsCommenting,
                                                Abbreviation = o.SubjectCat.Abbreviation
                                            }).ToList();
            List<int> lstSubjectID = lstSubject.Select(p => p.SubjectCatID).Distinct().ToList();
            SubjectCatBO objSC = new SubjectCatBO();
            //lay danh sach giao vien phan cong giang day cho tung mon
            IDictionary<string, object> dicSearchTA = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevel",EducationLevelID},
                {"Semester",SemesterID},
                {"lstClassID",lstClassID},
                {"lstSubjectID",lstSubjectID}
            };
            List<TeachingAssignmentBO> lstTeachingAssigment = new List<TeachingAssignmentBO>();
            lstTeachingAssigment = TeachingAssignmentBusiness.GetSubjectTeacher(_globalInfo.SchoolID.Value, dicSearchTA).ToList();
            List<TeachingAssignmentBO> lstTAtmp = new List<TeachingAssignmentBO>();
            TeachingAssignmentBO objTABO = null;

            dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevelID",EducationLevelID},
                {"lstClassID",lstClassID},
                {"lstSubjectID",lstSubjectID},
                {"lstPupilID",lstPupilID},
                {"TypeID",GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP},
                {"SemesterID",SemesterID}
            };
            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            //lstRatedCommentPupil = RatedCommentPupilBusiness.Search(dic).ToList();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (UtilsBusiness.IsMoveHistory(objAca))
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID,
                                        }).ToList();
            }
            else
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID
                                        }).ToList();
            }


            List<RatedCommentPupilBO> lstRatedtmp = new List<RatedCommentPupilBO>();
            #endregion
            #region fill du lieu
            int startRow = 0;
            int countVal = 0;
            string Title = string.Empty;

            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            CurrentSheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(objSP.SchoolProfileID, _globalInfo.AppliedLevel.Value).ToUpper());
            CurrentSheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            Title = "BẢNG THỐNG KÊ TÌNH HÌNH NHẬP DỮ LIỆU " + (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? " HỌC KỲ I" : " HỌC KỲ II");
            Title += ", NĂM HỌC " + objAca.DisplayTitle;
            CurrentSheet.SetCellValue("A5", Title);
            if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                CurrentSheet.SetCellValue("E8", "Giữa học kỳ I");
                CurrentSheet.SetCellValue("G8", "Cuối học kỳ I");
            }
            else
            {
                CurrentSheet.SetCellValue("E8", "Giữa học kỳ II");
                CurrentSheet.SetCellValue("G8", "Cuối học kỳ II");
            }
            string strTeachingAssign = string.Empty;
            for (int i = 0; i < lstSubject.Count; i++)
            {
                objSC = lstSubject[i];
                startRow = 10;
                IVTWorksheet sheet = oBook.CopySheetToLast(CurrentSheet);
                sheet.SetCellValue("A6", "Môn " + objSC.DisplayName + " - Khối " + EducationLevelID);
                for (int j = 0; j < lstGroup.Count; j++)
                {
                    strTeachingAssign = string.Empty;
                    var objGroup = lstGroup[j];
                    lstRatedtmp = lstRatedCommentPupil.Where(p => p.SubjectID == objSC.SubjectCatID && p.ClassID == objGroup.ClassID).ToList();
                    lstTAtmp = lstTeachingAssigment.Where(p => p.ClassID == objGroup.ClassID && p.SubjectID == objSC.SubjectCatID).ToList();
                    sheet.SetCellValue(startRow, 1, (j + 1));
                    for (int k = 0; k < lstTAtmp.Count; k++)
                    {
                        objTABO = lstTAtmp[k];
                        if (k < lstTAtmp.Count - 1)
                        {
                            strTeachingAssign += objTABO.TeacherName + ";";
                        }
                        else
                        {
                            strTeachingAssign += objTABO.TeacherName;
                        }
                    }
                    sheet.SetCellValue(startRow, 2, strTeachingAssign);
                    sheet.SetCellValue(startRow, 3, objGroup.ClassName);
                    sheet.SetCellValue(startRow, 4, objGroup.total);
                    //Giua HK
                    if (objSC.IsCommenting == 0)
                    {
                        countVal = lstRatedtmp.Where(p => p.MiddleEvaluation.HasValue).Count();
                        sheet.SetCellValue(startRow, 5, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 5, startRow, 5).SetFontColour(System.Drawing.Color.Red);
                        }
                        if (EducationLevelID > 3 && (objSC.Abbreviation == "Tt" || objSC.Abbreviation == "TV"))
                        {
                            countVal = lstRatedtmp.Where(p => p.PeriodicMiddleMark.HasValue).Count();
                            sheet.SetCellValue(startRow, 6, countVal);
                            if (countVal < objGroup.total)
                            {
                                sheet.GetRange(startRow, 6, startRow, 6).SetFontColour(System.Drawing.Color.Red);
                            }
                        }
                        else
                        {
                            sheet.HideColumn(6);
                        }

                    }
                    else
                    {
                        countVal = lstRatedtmp.Where(p => p.MiddleEvaluation.HasValue).Count();
                        sheet.SetCellValue(startRow, 5, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 5, startRow, 5).SetFontColour(System.Drawing.Color.Red);
                        }
                        if (EducationLevelID > 3 && (objSC.Abbreviation == "Tt" || objSC.Abbreviation == "TV"))
                        {
                            countVal = lstRatedtmp.Where(p => !string.IsNullOrEmpty(p.PeriodicMiddleJudgement)).Count();
                            sheet.SetCellValue(startRow, 6, countVal);
                            if (countVal < objGroup.total)
                            {
                                sheet.GetRange(startRow, 6, startRow, 6).SetFontColour(System.Drawing.Color.Red);
                            }
                        }
                        else
                        {
                            sheet.HideColumn(6);
                        }

                    }
                    //Cuoi HK
                    if (objSC.IsCommenting == 0)
                    {
                        countVal = lstRatedtmp.Where(p => p.EndingEvaluation.HasValue).Count();
                        sheet.SetCellValue(startRow, 7, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 7, startRow, 7).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.PeriodicEndingMark.HasValue).Count();
                        sheet.SetCellValue(startRow, 8, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 8, startRow, 8).SetFontColour(System.Drawing.Color.Red);
                        }
                    }
                    else
                    {
                        countVal = lstRatedtmp.Where(p => p.EndingEvaluation.HasValue).Count();
                        sheet.SetCellValue(startRow, 7, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 7, startRow, 7).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => !string.IsNullOrEmpty(p.PeriodicEndingJudgement)).Count();
                        sheet.SetCellValue(startRow, 8, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 8, startRow, 8).SetFontColour(System.Drawing.Color.Red);
                        }
                    }
                    countVal = lstRatedtmp.Where(p => !string.IsNullOrEmpty(p.Comment)).Count();
                    sheet.SetCellValue(startRow, 9, countVal);
                    if (countVal < objGroup.total)
                    {
                        sheet.GetRange(startRow, 9, startRow, 9).SetFontColour(System.Drawing.Color.Red);
                    }
                    startRow++;
                }
                //ke khung
                sheet.GetRange(10, 1, startRow - 1, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.SetCellValue(startRow + 1, 1, "Ghi chú:\r\n- Số liệu là tổng số học sinh của lớp đã được nhập mức đạt được, điểm KTĐK và nhận xét.\r\n- Số liệu tô đỏ là chưa nhập liệu đầy đủ.");
                sheet.SetRowHeight(startRow + 1, 51);
                sheet.GetRange(startRow + 1, 1, startRow + 1, 9).MergeLeft();
                sheet.GetRange(startRow + 1, 1, startRow + 1, 1).WrapText();
                sheet.Name = Utils.Utils.StripVNSignAndSpace(objSC.DisplayName);
            }
            CurrentSheet.Delete();
            #endregion
            return oBook.ToStream();
        }
        private Stream CreateReportCapQua(int SemesterID, int EducationLevelID, int EvaluationID)
        {
            string reportCode = SystemParamsInFile.REPORT_DATA_INPUT_MONITOR_CQ;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet CurrentSheet1 = oBook.GetSheet(1);
            IVTWorksheet CurrentSheet2 = oBook.GetSheet(2);
            #region Lay du lieu

            int academicYearId = _globalInfo.AcademicYearID.Value;
            IQueryable<PupilOfClassBO> iqPOC = (from poc in PupilOfClassBusiness.All
                                                join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                where poc.AcademicYearID == academicYearId
                                                && poc.SchoolID == _globalInfo.SchoolID
                                                && cp.EducationLevelID == EducationLevelID
                                                && cp.IsActive.HasValue && cp.IsActive.Value
                                                && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                && cp.AcademicYearID == academicYearId
                                                select new PupilOfClassBO
                                                {
                                                    PupilID = poc.PupilID,
                                                    ClassID = poc.ClassID,
                                                    ClassName = cp.DisplayName,
                                                    HeadTeacherName = cp.Employee.FullName,
                                                    OrderInClass = cp.OrderNumber
                                                });
            var lstGroup = (from g in iqPOC
                            group g by new
                            {
                                g.ClassID,
                                g.OrderInClass,
                                g.ClassName,
                                g.HeadTeacherName
                            } into gb
                            select new
                            {
                                gb.Key.ClassID,
                                gb.Key.OrderInClass,
                                gb.Key.ClassName,
                                gb.Key.HeadTeacherName,
                                total = gb.Count()
                            }).OrderBy(p => p.OrderInClass).ThenBy(p => p.ClassName).ToList();
            List<int> lstClassID = lstGroup.Select(p => p.ClassID).Distinct().ToList();
            List<int> lstPupilID = iqPOC.Select(p => p.PupilID).Distinct().ToList();
            List<int> lstEvaluationID = new List<int>();
            if (EvaluationID == 0)
            {
                lstEvaluationID.Add(GlobalConstants.EVALUATION_CAPACITY);
                lstEvaluationID.Add(GlobalConstants.EVALUATION_QUALITY);
            }
            else
            {
                lstEvaluationID.Add(EvaluationID);
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevelID",EducationLevelID},
                {"lstClassID",lstClassID},
                {"lstPupilID",lstPupilID},
                {"SemesterID",SemesterID}
            };
            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID,
                                        }).ToList();
            }
            else
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID
                                        }).ToList();
            }


            lstRatedCommentPupil = lstRatedCommentPupil.Where(p => lstEvaluationID.Contains(p.EvaluationID)).ToList();


            List<RatedCommentPupilBO> lstRatedtmp = new List<RatedCommentPupilBO>();
            #endregion
            #region fill du lieu
            int startRow = 0;
            int countVal = 0;
            string Title = string.Empty;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            CurrentSheet1.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value).ToUpper());
            CurrentSheet1.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            CurrentSheet2.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value).ToUpper());
            CurrentSheet2.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            Title = "BẢNG THỐNG KÊ TÌNH HÌNH NHẬP DỮ LIỆU " + (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? " HỌC KỲ I" : " HỌC KỲ II");
            Title += ", NĂM HỌC " + objAca.DisplayTitle;
            CurrentSheet1.SetCellValue("A5", Title);
            CurrentSheet2.SetCellValue("A5", Title);
            if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                CurrentSheet1.SetCellValue("E8", "Giữa học kỳ I");
                CurrentSheet1.SetCellValue("H8", "Cuối học kỳ I");
                CurrentSheet2.SetCellValue("E8", "Giữa học kỳ I");
                CurrentSheet2.SetCellValue("I8", "Cuối học kỳ I");
            }
            else
            {
                CurrentSheet1.SetCellValue("E8", "Giữa học kỳ II");
                CurrentSheet1.SetCellValue("H8", "Cuối học kỳ II");
                CurrentSheet2.SetCellValue("E8", "Giữa học kỳ II");
                CurrentSheet2.SetCellValue("I8", "Cuối học kỳ II");
            }

            int EvaluationIDtmp = 0;
            IVTWorksheet sheet = null;
            for (int i = 0; i < lstEvaluationID.Count; i++)
            {
                EvaluationIDtmp = lstEvaluationID[i];
                startRow = 10;
                if (EvaluationIDtmp == GlobalConstants.EVALUATION_CAPACITY)//nang luc
                {
                    sheet = oBook.CopySheetToLast(CurrentSheet1, "Z1000");
                }
                else//pham chat
                {
                    sheet = oBook.CopySheetToLast(CurrentSheet2, "Z1000");
                }

                sheet.SetCellValue("A6", (EvaluationIDtmp == GlobalConstants.EVALUATION_CAPACITY ? "Năng lực " : "Phẩm chất ") + " - Khối " + EducationLevelID);
                for (int j = 0; j < lstGroup.Count; j++)
                {
                    var objGroup = lstGroup[j];
                    lstRatedtmp = lstRatedCommentPupil.Where(p => p.EvaluationID == EvaluationIDtmp && p.ClassID == objGroup.ClassID).ToList();
                    sheet.SetCellValue(startRow, 1, (j + 1));
                    sheet.SetCellValue(startRow, 2, objGroup.HeadTeacherName);
                    sheet.SetCellValue(startRow, 3, objGroup.ClassName);
                    sheet.SetCellValue(startRow, 4, objGroup.total);
                    if (EvaluationIDtmp == GlobalConstants.EVALUATION_CAPACITY)
                    {
                        //Giua HK
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 1 && p.CapacityMiddleEvaluation.HasValue).Count();//NL1
                        sheet.SetCellValue(startRow, 5, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 5, startRow, 5).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 2 && p.CapacityMiddleEvaluation.HasValue).Count();//NL2
                        sheet.SetCellValue(startRow, 6, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 6, startRow, 6).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 3 && p.CapacityMiddleEvaluation.HasValue).Count();//NL3
                        sheet.SetCellValue(startRow, 7, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 7, startRow, 7).SetFontColour(System.Drawing.Color.Red);
                        }
                        //Cuoi HK
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 1 && p.CapacityEndingEvaluation.HasValue).Count();//NL1
                        sheet.SetCellValue(startRow, 8, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 8, startRow, 8).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 2 && p.CapacityEndingEvaluation.HasValue).Count();//NL2
                        sheet.SetCellValue(startRow, 9, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 9, startRow, 9).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 3 && p.CapacityEndingEvaluation.HasValue).Count();//NL3
                        sheet.SetCellValue(startRow, 10, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 10, startRow, 10).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => !string.IsNullOrEmpty(p.Comment)).Select(p => p.PupilID).Distinct().Count();
                        sheet.SetCellValue(startRow, 11, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 11, startRow, 11).SetFontColour(System.Drawing.Color.Red);
                        }
                    }
                    else
                    {
                        //Giua HK
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 4 && p.QualityMiddleEvaluation.HasValue).Count();//PC1
                        sheet.SetCellValue(startRow, 5, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 5, startRow, 5).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 5 && p.QualityMiddleEvaluation.HasValue).Count();//PC2
                        sheet.SetCellValue(startRow, 6, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 6, startRow, 6).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 6 && p.QualityMiddleEvaluation.HasValue).Count();//PC3
                        sheet.SetCellValue(startRow, 7, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 7, startRow, 7).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 7 && p.QualityMiddleEvaluation.HasValue).Count();//PC4
                        sheet.SetCellValue(startRow, 8, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 8, startRow, 8).SetFontColour(System.Drawing.Color.Red);
                        }
                        //Cuoi HK
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 4 && p.QualityEndingEvaluation.HasValue).Count();//PC1
                        sheet.SetCellValue(startRow, 9, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 9, startRow, 9).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 5 && p.QualityEndingEvaluation.HasValue).Count();//PC2
                        sheet.SetCellValue(startRow, 10, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 10, startRow, 10).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 6 && p.QualityEndingEvaluation.HasValue).Count();//PC3
                        sheet.SetCellValue(startRow, 11, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 11, startRow, 11).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => p.SubjectID == 7 && p.QualityEndingEvaluation.HasValue).Count();//PC4
                        sheet.SetCellValue(startRow, 12, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 12, startRow, 12).SetFontColour(System.Drawing.Color.Red);
                        }
                        countVal = lstRatedtmp.Where(p => !string.IsNullOrEmpty(p.Comment)).Select(p => p.PupilID).Distinct().Count();
                        sheet.SetCellValue(startRow, 13, countVal);
                        if (countVal < objGroup.total)
                        {
                            sheet.GetRange(startRow, 13, startRow, 13).SetFontColour(System.Drawing.Color.Red);
                        }
                    }

                    startRow++;
                }
                //ke khung
                int endcol = EvaluationIDtmp == GlobalConstants.EVALUATION_CAPACITY ? 11 : 13;
                sheet.GetRange(10, 1, startRow - 1, endcol).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.SetCellValue(startRow + 1, 1, "Ghi chú: \r\n- Số liệu là tổng số học sinh của lớp đã được nhập mức đạt được và nhận xét.\r\n- Số liệu tô đỏ là chưa nhập liệu đầy đủ.");
                sheet.SetRowHeight(startRow + 1, 51);
                sheet.GetRange(startRow + 1, 1, startRow + 1, endcol).MergeLeft();
                sheet.GetRange(startRow + 1, 1, startRow + 1, 1).WrapText();
                sheet.Name = EvaluationIDtmp == GlobalConstants.EVALUATION_CAPACITY ? "Nangluc" : "Phamchat";
                sheet.FitToPage = true;
            }
            CurrentSheet1.Delete();
            CurrentSheet2.Delete();
            #endregion
            return oBook.ToStream();
        }
        #endregion
    }
}