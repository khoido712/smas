﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DataInputMonitorArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Common_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", DataInputMonitorConstants.CBO_SEMESTER)]
        public int? Semester { get; set; }

        [ResourceDisplayName("MarkInputSituation_Education_Level")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DataInputMonitorConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange()")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("Common_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", DataInputMonitorConstants.CBO_SUBJECT)]
        public int? SubjectID { get; set; }

        public int FormType { get; set; }
    }
}