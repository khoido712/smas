using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.StatisticsForUnitPrimaryArea.Models
{
    public class StatisticsForUnitPrimaryViewModel
    {
        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("Common_Label_Semester")]
        public int? Semester { get; set; }
    }
}
