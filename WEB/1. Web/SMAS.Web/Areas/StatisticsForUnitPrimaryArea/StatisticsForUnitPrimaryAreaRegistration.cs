﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticsForUnitPrimaryArea
{
    public class StatisticsForUnitPrimaryAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticsForUnitPrimaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticsForUnitPrimaryArea_default",
                "StatisticsForUnitPrimaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
