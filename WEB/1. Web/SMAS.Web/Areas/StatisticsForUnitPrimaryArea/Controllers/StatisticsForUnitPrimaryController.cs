﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Web.Areas.StatisticsForUnitPrimaryArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System.IO;
using SMAS.Web.Filter;


namespace SMAS.Web.Areas.StatisticsForUnitPrimaryArea.Controllers
{
    public class StatisticsForUnitPrimaryController : BaseController
    {
        IStatisticsForUnitBusiness StatisticsForUnitBusiness;
        IStatisticsConfigBusiness StatisticsConfigBusiness;
        IMarkStatisticBusiness MarkStatisticBusiness;
        ICapacityStatisticBusiness CapacityStatisticBusiness;
        IConductStatisticBusiness ConductStatisticBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        ISubjectCatBusiness SubjectCatBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        public StatisticsForUnitPrimaryController(IStatisticsForUnitBusiness statisticsForUnitBusiness,
                                                    IStatisticsConfigBusiness statisticsConfigBusiness,
                                                    IMarkStatisticBusiness markStatisticBusiness,
                                                    ICapacityStatisticBusiness capacityStatisticBusiness,
                                                    IConductStatisticBusiness conductStatisticBusiness,
                                                    IEducationLevelBusiness educationLevelBusiness,
                                                    ISubjectCatBusiness subjectCatBusiness,
                                                    IAcademicYearBusiness academicYearBusiness)
        {
            this.StatisticsForUnitBusiness = statisticsForUnitBusiness;
            this.StatisticsConfigBusiness = statisticsConfigBusiness;
            this.MarkStatisticBusiness = markStatisticBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
        }
        //
        // GET: /StatisticsForUnitPrimary/StatisticsForUnitPrimary/

        public ActionResult Index()
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[StatisticsForUnitPrimaryConstants.LIST_EDUCATION_LEVEL] = new SelectList(global.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[StatisticsForUnitPrimaryConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
            ViewData[StatisticsForUnitPrimaryConstants.LIST_SEMESTER_ALL] = new SelectList(CommonList.SemesterAndAll(), "key", "value", global.Semester.ToString());
            ViewData[StatisticsForUnitPrimaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_JUDGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_MARK] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_CONDUCT_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_CAPACITY_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);
            ViewData["rdType"] = 1;
            return View();
        }
        #region Period


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchPeriodMark(StatisticsForUnitPrimaryViewModel model)
        {
            ViewData[StatisticsForUnitPrimaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<MarkStatisticBO> listMarkStatistic = StatisticsForUnitBusiness.SearchPeriodicMarkForPrimary(sfu);

            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[StatisticsForUnitPrimaryConstants.GRID_STATISTICS_PERIOD] = listMarkStatistic;
            ViewData[StatisticsForUnitPrimaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_PeriodMarkGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendPeriod(StatisticsForUnitPrimaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<MarkStatisticBO> data = StatisticsForUnitBusiness.SearchPeriodicMarkForPrimary(sfu);
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitPrimary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportPeriod(StatisticsForUnitPrimaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreatePeriodicMarkForPrimaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Semester


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSemesterMark(StatisticsForUnitPrimaryViewModel model)
        {
            ViewData[StatisticsForUnitPrimaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<MarkStatisticBO> listMarkStatistic = StatisticsForUnitBusiness.SearchSemesterMarkForPrimary(sfu);

            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[StatisticsForUnitPrimaryConstants.GRID_STATISTICS_SEMESTER] = listMarkStatistic;
            ViewData[StatisticsForUnitPrimaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_SemesterMarkGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendSemester(StatisticsForUnitPrimaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<MarkStatisticBO> data = StatisticsForUnitBusiness.SearchSemesterMarkForPrimary(sfu);
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitPrimary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportSemester(StatisticsForUnitPrimaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateSemesterMarkForPrimaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region SubjectCapacity


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSubjectCapacity(StatisticsForUnitPrimaryViewModel model, FormCollection col)
        {
            int rdType = int.Parse(col["rdType"]);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_JUDGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            ViewData[StatisticsForUnitPrimaryConstants.LIST_MARK] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<CapacityStatistic> listCapacitySubjectStatistic = rdType == 1 ? StatisticsForUnitBusiness.SearchMarkSubjectCapacityForPrimary(sfu) : StatisticsForUnitBusiness.SearchJudgeSubjectCapacityForPrimary(sfu);

            listCapacitySubjectStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu theo mon hoc
            listCapacitySubjectStatistic = listCapacitySubjectStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[StatisticsForUnitPrimaryConstants.GRID_STATISTICS_CAPACITY_SUBJECT] = listCapacitySubjectStatistic;
            ViewData[StatisticsForUnitPrimaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listCapacitySubjectStatistic.Count);
            ViewData["rdType"] = rdType;
            return PartialView("_SubjectCapacityGrid", model);
        }
        

        [ValidateAntiForgeryToken]
        public JsonResult SendSubjectCapacity(StatisticsForUnitPrimaryViewModel model, FormCollection col)
        {
            if (ModelState.IsValid)
            {
                int rdType = int.Parse(col["rdType"]);
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<CapacityStatistic> data = rdType == 1 ? StatisticsForUnitBusiness.SearchMarkSubjectCapacityForPrimary(sfu) : StatisticsForUnitBusiness.SearchJudgeSubjectCapacityForPrimary(sfu);
                //CapacityStatisticBusiness.SendAll(data);
                CapacityStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitPrimary_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }
        public FileStreamResult ExportMarkCapacitySubject(StatisticsForUnitPrimaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateMarkSubjectCapacityForPrimaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        public FileStreamResult ExportJudgeCapacitySubject(StatisticsForUnitPrimaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_PRIMARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateJudgeSubjectCapacityForPrimaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
