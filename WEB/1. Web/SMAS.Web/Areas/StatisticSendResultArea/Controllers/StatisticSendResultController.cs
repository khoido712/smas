﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.StatisticOfTertiaryArea.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.StatisticSendResultArea;
using SMAS.Web.Areas.StatisticSendResultArea.Models;
using SMAS.Web.SMASEdu;
namespace SMAS.Web.Areas.StatisticSendResultArea.Controllers
{
    public class StatisticSendResultController : Controller
    {
        //
        // GET: /StatisticSendResultArea/StatisticSendResult/

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        ///<author>NamTA</author>
        ///<date>3/6/2013</date>
        ///

        public const string teacher = "teacher";
        public const string pupil = "pupil";

        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISendResultBusiness SendResultBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly ISchoolFacultyBusiness UserAccountBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPackageBusiness PackageBusiness;

        public StatisticSendResultController(IPupilOfClassBusiness PupilOfClassBusiness, IClassProfileBusiness ClassProfileBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, ISchoolProfileBusiness SchoolProfileBusiness, IEmployeeBusiness EmployeeBusiness, ISendResultBusiness SendResultBusiness, IAcademicYearBusiness AcademicYearBusiness, ISchoolFacultyBusiness SchoolFacultyBusiness, IPupilProfileBusiness PupilProfileBusiness, IPackageBusiness PackageBusiness)
        {
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SendResultBusiness = SendResultBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PackageBusiness = PackageBusiness;
        }


        public ActionResult Index()
        {
            SetViewData();
            StatisticSendResultViewModel model = new StatisticSendResultViewModel();
            model.FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            model.ToDate = DateTime.Now;

            return View(model);
        }

        public void SetViewData()
        {
            ViewData[StatisticSendResultConstants.LIST_EDUCATIONLEVELS] = new GlobalInfo().EducationLevels;

            List<ComboObject> listSection = new List<ComboObject>();
            listSection.Add(new ComboObject("", Res.Get("All")));
            ViewData[StatisticSendResultConstants.LIST_CLASS] = new SelectList(listSection, "key", "value");

            ViewData[StatisticSendResultConstants.LIST_SEMESTER] = Utils.CommonList.SemesterAndAll();

        }
        [HttpPost]
        public FileResult ExportFile(FormCollection form)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int EducationLevelID = 0;
            int ClassID = 0;
            int SemesterID = 0;
            bool kt = DateTime.TryParse(form["FromDate"], out FromDate);
            kt = DateTime.TryParse(form["ToDate"], out ToDate);
            kt = int.TryParse(form["EducationLevelID"], out EducationLevelID);
            kt = int.TryParse(form["ClassID"], out ClassID);
            kt = int.TryParse(form["SemesterID"], out SemesterID);
            String filename = "";
            Stream excel;
            if (form["rdoTypeStatistic"] == "teacher")
            {
                excel = this.CreateReportForTeacher(FromDate, ToDate, out filename);
            }
            else
            {
                excel = this.CreateReportForPupil(EducationLevelID, ClassID, SemesterID, out filename);
            }


            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;

            return result;
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboEducationLevel(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = EducationLevelID.Value;
                SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                IQueryable<ClassProfile> lsClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.HasValue ? new GlobalInfo().SchoolID.Value : 0, SearchInfo);
                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        public const int rowTop = 8;
        public const int colTop = 6;
        public Stream CreateReportForTeacher(DateTime fromDte, DateTime toDte, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.SMS_THONGKETINNHANTHEOGIAOVIEN);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            //Lay Danh Sach Giao Vien

            IQueryable<Employee> listTeacher = EmployeeBusiness.SearchTeacher(new GlobalInfo().SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } });

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).Year;
            dic["TypeOfReceiver"] = SystemParamsInFile.TEACHER_RECEIVER;
            dic["FromTime_Send"] = fromDte;
            dic["ToTime_Send"] = toDte;

            IQueryable<SendResult> listSendResult = SendResultBusiness.Search(dic);

            var querry = from lt in listTeacher.ToList()
                         join lsr in listSendResult.ToList() on lt.EmployeeCode equals lsr.EmployeeCode into listTeacherSendResult
                         from listRecord in listTeacherSendResult.DefaultIfEmpty()
                         group listRecord by lt.EmployeeID into grouped
                         select new
                         {
                             EmployeeID = grouped.Key,
                             NumMessage = grouped.Where(o => o != null).Count()
                         };

            int index = 0;
            List<SendResultByTeacher> list = new List<SendResultByTeacher>();
            foreach (var obj in querry.ToList())
            {
                index++;
                SendResultByTeacher srbt = new SendResultByTeacher();

                Employee employee = EmployeeBusiness.Find(obj.EmployeeID);


                srbt.index = index;
                srbt.FullName = employee.FullName;
                srbt.FacultyName = employee.SchoolFaculty != null ? employee.SchoolFaculty.Description : string.Empty;

                //lay thong tin username

                srbt.Account = EmployeeBusiness.GetUserNameForEmployee(obj.EmployeeID);
                srbt.NumSMS = obj.NumMessage;

                if (obj.NumMessage != 0)
                {

                    SendResult sd = SendResultBusiness.Search(dic).Where(o => o.EmployeeCode == employee.EmployeeCode).FirstOrDefault();
                    srbt.TimeSend = sd.TimeSend;
                }
                list.Add(srbt);
            }






            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            IVTRange range = sheet.GetRange(rowTop, 1, rowTop, colTop);

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value);

            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["FromDate"] = fromDte.ToString("dd/MM/yyyy");
            dicVarable["ToDate"] = toDte.ToString("dd/MM/yyyy");

            int i = 0;
            List<object> listData = new List<object>();

            list.ForEach(u =>
            {
                listData.Add(new
                {
                    Index = ++i,
                    FullName = u.FullName,
                    Account = u.Account,
                    FacultyName = u.FacultyName,
                    NumSMS = u.NumSMS,
                    TimeSend = u.TimeSend
                });
                sheet.CopyPaste(range, 8 + i, 1, true);
            });



            dicVarable["Rows"] = listData;
            sheet.FillVariableValue(dicVarable);
            tempSheet.Delete();
            #endregion

            #region reportName
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion

            return oBook.ToStream();
        }

        public const int colTopPupil = 9;

        public Stream CreateReportForPupil(int EducationLevelID, int ClassID, int SemesterID, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.SMS_THONGKETINNHANTHEOHOCSINH);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            //Lay Danh Sach Hoc Sinh

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ClassID"] = ClassID;
            dic["Check"] = "check";


            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic);

            DateTime fromDte = new DateTime();
            DateTime toDte = new DateTime();

            if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                fromDte = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).FirstSemesterStartDate.Value;
                toDte = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).FirstSemesterEndDate.Value;
            }
            else if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                fromDte = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).SecondSemesterStartDate.Value;
                toDte = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).SecondSemesterEndDate.Value;
            }
            else
            {
                fromDte = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).FirstSemesterStartDate.Value;
                toDte = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).SecondSemesterEndDate.Value;
            }



            dic = new Dictionary<string, object>();
            dic["Year"] = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).Year;
            dic["TypeOfReceiver"] = SystemParamsInFile.PARENT_RECEIVER;
            dic["FromTime_Send"] = fromDte;
            dic["ToTime_Send"] = toDte;

            IQueryable<SendResult> listSendResult = SendResultBusiness.Search(dic);



            //Lay danh sach SMS parentContract tu service
            SMASEdu.BCCSService service = new SMAS.Web.SMASEdu.BCCSService();

            //Doi tuong tim kiem

            SMASEdu.SMSParentContractObj SMSParentContractObj = new SMASEdu.SMSParentContractObj();
            SMSParentContractObj.PackageID = 0;
            SMSParentContractObj.SubscriberID = 0;
            SMSParentContractObj.SubscriberTime = DateTime.Now.Date;
            SMSParentContractObj.Status = 0;
            SMSParentContractObj.SubscriptionStatus = 0;
            SMSParentContractObj.Year = 0;
            SMSParentContractObj.TypeOfSubcriptionTime = 0;
            SMSParentContractObj.MaxSMS = -1;
            SMSParentContractObj.NumSMSSent = -1;
            SMASEdu.SMSParentContractResult arSMSParent = service.GetSMSParentContract("", SMSParentContractObj);

            IEnumerable<SMASEdu.SMSParentContractObj> lstSMSParent = arSMSParent.Result.AsEnumerable();
            GlobalInfo glo = new GlobalInfo();
            var querry = from ls in lstSMSParent
                         join lp in listPupilOfClass.ToList() on ls.PupilCode equals lp.PupilProfile.PupilCode
                         join rs in listSendResult.ToList() on new { lp.PupilProfile.PupilCode, ls.Phone } equals new { rs.PupilCode, Phone = rs.ReceiverMobile } into rsu
                         from rsl in rsu.DefaultIfEmpty()
                         group rsl by new { lp.PupilProfile.PupilCode, lp.PupilProfile.FullName, lp.ClassProfile.DisplayName, ls.SubscriberName, ls.PackageID, ls.Subscriber } into grp

                         select new
                         {
                             FullName = grp.Key.FullName,
                             ClassName = grp.Key.DisplayName,
                             FamilyName = grp.Key.SubscriberName,
                             PackageID = grp.Key.PackageID,
                             NumSend = grp.Count(o => o != null),
                             Phone = grp.Key.Subscriber,
                             PupilCode = grp.Key.PupilCode
                         };




            int index = 0;
            List<SendResultByPupil> list = new List<SendResultByPupil>();
            foreach (var obj in querry.ToList())
            {
                index++;
                SendResultByPupil srbt = new SendResultByPupil();


                srbt.FullName = obj.FullName;
                srbt.ClassName = obj.ClassName;
                srbt.ParentName = obj.FamilyName;
                srbt.SendedNumber = obj.NumSend;
                srbt.NumberRegistration = obj.Phone;
                Package pkg = PackageBusiness.Find(obj.PackageID);
                if (pkg != null)
                {
                    srbt.PackageName = pkg.Resolution;
                    srbt.NumberOfPackage = pkg.MaxSMS;
                }

                //lay thong tin username


                if (obj.NumSend != 0)
                {

                    SendResult sd = SendResultBusiness.Search(dic).Where(o => o.PupilCode == obj.PupilCode).FirstOrDefault();
                    srbt.TimeRecived = sd.TimeSend;
                }
                list.Add(srbt);
            }


            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            IVTRange range = sheet.GetRange(rowTop, 1, rowTop, colTopPupil);

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value);


            string SemesterName = "";

            if (SemesterID.ToString() == SystemParamsInFile.SEMESTER_I)
            {
                SemesterName = "Học kỳ I";
            }
            else if (SemesterID.ToString() == SystemParamsInFile.SEMESTER_II)
            {
                SemesterName = "Học kỳ II";
            }
            else
            {
                SemesterName = "Cả Năm";
            }


            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["Semester"] = SemesterName;
            dicVarable["AcademicYear"] = AcademicYearBusiness.Find(glo.AcademicYearID).Year + "-" + AcademicYearBusiness.Find(glo.AcademicYearID).Year + 1;

            int i = 0;
            List<object> listData = new List<object>();

            list.ForEach(u =>
            {
                listData.Add(new
                {
                    Index = ++i,
                    FullName = u.FullName,
                    ClassName = u.ClassName,
                    FamilyName = u.ParentName,
                    NumberRegistration = u.NumberRegistration,
                    NumberOfPackage = u.NumberOfPackage,
                    SendedNumber = u.SendedNumber,
                    TimeRecived = u.TimeRecived,
                    PackageName = u.PackageName,

                });
                sheet.CopyPaste(range, 8 + i, 1, true);
            });



            dicVarable["Rows"] = listData;
            sheet.FillVariableValue(dicVarable);
            tempSheet.Delete();
            #endregion

            #region reportName
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion

            return oBook.ToStream();
        }
    }
}
