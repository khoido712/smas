﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticSendResultArea.Models
{
    public class SendResultByTeacher
    {
        public int index { get; set; }
        public string FullName { get; set; }
        public string Account { get; set; }
        public string FacultyName { get; set; }
        public int? NumSMS { get; set; }
        public string TimeSend { get; set; }
    }
}