﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticSendResultArea.Models
{
    public class SendResultByPupil
    {
        public string FullName { get; set; }
        public string ClassName { get; set; }
        public string ParentName { get; set; }
        public string NumberRegistration { get; set; }
        public string PackageName { get; set; }
        public int? NumberOfPackage { get; set; }
        public int? SendedNumber { get; set; }
        public string TimeRecived { get; set; }
    }
}