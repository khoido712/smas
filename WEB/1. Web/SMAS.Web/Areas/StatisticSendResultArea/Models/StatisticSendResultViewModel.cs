﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticSendResultArea.Models
{
    public class StatisticSendResultViewModel
    {
        [ResourceDisplayName("ClassEmulation_Label_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? FromDate { get; set; }

        [ResourceDisplayName("ClassEmulation_Label_ToDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.GREATER_EQUALS, "FromDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? ToDate { get; set; }
    }
}