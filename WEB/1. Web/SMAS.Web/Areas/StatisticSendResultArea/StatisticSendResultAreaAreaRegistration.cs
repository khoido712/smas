﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticSendResultArea
{
    public class StatisticSendResultAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticSendResultArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticSendResultArea_default",
                "StatisticSendResultArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
