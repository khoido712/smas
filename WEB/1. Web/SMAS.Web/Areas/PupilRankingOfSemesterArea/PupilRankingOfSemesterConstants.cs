/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilRankingOfSemesterArea
{
    public class PupilRankingOfSemesterConstants
    {
        public const string LIST_PUPILRANKING = "listPupilRanking";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CONDUCT = "listConduct";
        public const string LIST_DISABLE = "listDisable";
        public const string SEMESTER_END_DATE = "semester_end_date";
        public const string SEMESTER_ID = "SemesterID";

        public const string BUTTON = "button";
        public const string EXCEL_BUTTON = "button";

        public const string TITLE_PAGE = "TITLE_PAGE";
        public const string ISGDTX = "gdtx";
        public const string ISHK2TOALL = "ISHK2TOALL";
        public const string IS_RETRANING = "is_retraning";
        public const string IS_LOCK_INPUT = "IsLockInput";
        public const string LOCK_USER_NAME = "LockUserName";

        public const string IS_CONFIG_RANKING = "IsConfigRanking";
    }
}