/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PupilRankingOfSemesterArea.Models
{
    public class SearchViewModel
    {

        [ResourceDisplayName("PupilConductByViolation_label_EducationLevel")]
        [UIHint("Combobox")]
        public int EducationlevelID { get; set; }

        [ResourceDisplayName("PupilConductByViolation_label_Class")]
        [UIHint("Combobox")]
        public int ClassID { get; set; }

        [ResourceDisplayName("PupilConductByViolation_label_Semester")]
        [UIHint("Combobox")]
        public int Semester { get; set; }

        [ResourceDisplayName("PupilConductByViolation_label_Warning")]
        public System.String Warning { get; set; }	
        
    }
}