/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.PupilRankingOfSemesterArea.Models
{
    public class PupilRankingOfSemesterViewModel
    {
		public System.Int32 PupilRankingID { get; set; }								
		public System.Int32 PupilID { get; set; }								
		public System.Int32 ClassID { get; set; }								
		public System.Int32 AcademicYearID { get; set; }								
		public System.Int32 EducationLevelID { get; set; }								
		public System.Int32 Year { get; set; }								
		public System.Nullable<System.Int32> Semester { get; set; }								
		public System.Nullable<System.Int32> PeriodID { get; set; }								
		public System.Nullable<System.Decimal> AverageMark { get; set; }								
		public System.Nullable<System.Int32> TotalAbsentDaysWithPermission { get; set; }								
		public System.Nullable<System.Int32> TotalAbsentDaysWithoutPermission { get; set; }								
		public System.Nullable<System.Int32> CapacityLevelID { get; set; }								
		public System.Nullable<System.Int32> ConductLevelID { get; set; }								
		public System.Nullable<System.Int32> Rank { get; set; }								
		public System.Nullable<System.DateTime> RankingDate { get; set; }								
		public System.Nullable<System.Int32> StudyingJudgementID { get; set; }								
		public System.Int32 SchoolID { get; set; }								
	       
    }
}


