﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PupilRankingOfSemesterArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Web.Areas.PupilRankingOfSemesterArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilRankingOfSemesterController : BaseController
    {

        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IPupilPraiseBusiness PupilPraiseBusiness;
        private readonly IPupilDisciplineBusiness PupilDisciplineBusiness;
        private readonly IConductConfigByCapacityBusiness ConductConfigByCapacityBusiness;
        private readonly IConductConfigByViolationBusiness ConductConfigByViolationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilFaultBusiness PupilFaultBusiness;
        private readonly IConfigConductRankingBusiness ConfigConductRankingBusiness;
        private readonly IPupilRankingHistoryBusiness PupilRankingHistoryBusiness;
        private readonly ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;

        public PupilRankingOfSemesterController(IPeriodDeclarationBusiness periodDeclarationBusiness,
                                            IClassProfileBusiness classProfileBusiness,
                                            IPupilProfileBusiness pupilProfileBusiness,
                                            IPupilPraiseBusiness PupilPraiseBusiness,
                                            IReportDefinitionBusiness ReportDefinitionBusiness,
                                            IPupilDisciplineBusiness PupilDisciplineBusiness,
                                            IPupilRankingBusiness pupilRankingBusiness,
                                            IConductLevelBusiness conductLevelBusiness,
                                            IConductConfigByCapacityBusiness conductConfigByCapacityBusiness,
                                            IConductConfigByViolationBusiness conductConfigByViolationBusiness,
                                            IAcademicYearBusiness academicyearBusiness,
                                            IEducationLevelBusiness EducationLevelBusiness,
                                            ISchoolProfileBusiness SchoolProfileBusiness,
                                            IPupilFaultBusiness pupilFaultBusiness,
                                            IConfigConductRankingBusiness configConductRankingBusiness,
                                            IPupilRankingHistoryBusiness _PupilRankingHistoryBusiness,
                                            ISummedUpRecordClassBusiness _SummedUpRecordClassBusiness)
        {
            this.PeriodDeclarationBusiness = periodDeclarationBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.PupilRankingBusiness = pupilRankingBusiness;
            this.PupilPraiseBusiness = PupilPraiseBusiness;
            this.PupilDisciplineBusiness = PupilDisciplineBusiness;
            this.ConductLevelBusiness = conductLevelBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ConductConfigByCapacityBusiness = conductConfigByCapacityBusiness;
            this.ConductConfigByViolationBusiness = conductConfigByViolationBusiness;
            this.AcademicYearBusiness = academicyearBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilFaultBusiness = pupilFaultBusiness;
            this.ConfigConductRankingBusiness = configConductRankingBusiness;
            this.PupilRankingHistoryBusiness = _PupilRankingHistoryBusiness;
            SummedUpRecordClassBusiness = _SummedUpRecordClassBusiness;
        }

        #region SetViewData
        private void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[PupilRankingOfSemesterConstants.LIST_PUPILRANKING] = GetRankingType();

            //int AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            //List<ConductLevel> conductlevel = ConductLevelBusiness.All.Where(o => (o.AppliedLevel == AppliedLevel )).ToList();
            //ViewData[PupilRankingConstants.LIST_CONDUCT] = new SelectList(conductlevel, "ConductLevelID", "Resolution");

            ViewData[PupilRankingOfSemesterConstants.LIST_EDUCATIONLEVEL] = new SelectList(new GlobalInfo().EducationLevels, "EducationLevelID", "Resolution", new GlobalInfo().EducationLevels.FirstOrDefault().EducationLevelID);
            ViewData[PupilRankingOfSemesterConstants.LIST_SEMESTER] = new SelectList(CommonList.SemesterForConduct(), "key", "value", new GlobalInfo().Semester);

            int educationLevelID = new GlobalInfo().EducationLevels.FirstOrDefault().EducationLevelID;
            IEnumerable<ClassProfile> lsClassID = new List<ClassProfile>();
            IDictionary<string, object> Dictionnary = new Dictionary<string, object>();
            Dictionnary["EducationLevelID"] = educationLevelID;
            Dictionnary["AcademicYearID"] = global.AcademicYearID;
            Dictionnary["IsVNEN"] = true;
            if (!global.IsAdminSchoolRole && !global.IsViewAll)
            {
                Dictionnary["UserAccountID"] = global.UserAccountID;
                Dictionnary["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEADTEACHER;
            }

            lsClassID = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, Dictionnary).ToList();

            ViewData[PupilRankingOfSemesterConstants.LIST_CLASS] = new SelectList(lsClassID, "ClassProfileID", "DisplayName");
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetClassList(int? educationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<ClassProfile> lsClassID = new List<ClassProfile>();
            if (educationLevelID.HasValue && educationLevelID != 0)
            {
                IDictionary<string, object> Dictionnary = new Dictionary<string, object>();
                Dictionnary["EducationLevelID"] = educationLevelID;
                Dictionnary["AcademicYearID"] = new GlobalInfo().AcademicYearID;
                if (!global.IsAdminSchoolRole && !global.IsViewAll)
                {
                    Dictionnary["UserAccountID"] = global.UserAccountID;
                    Dictionnary["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEADTEACHER;
                }

                lsClassID = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, Dictionnary).ToList();
                ViewData[PupilRankingOfSemesterConstants.LIST_CLASS] = new SelectList(lsClassID, "ClassProfileID", "DisplayName");
            }

            return Json(new SelectList(lsClassID, "ClassProfileID", "DisplayName"));
        }

        #endregion

        public ActionResult Index()
        {
            if (!new GlobalInfo().HasHeadTeacherPermission(0))
            {
                throw new BusinessException("Common_Error_HasHadTeacherPermission");
            }
            else
            {
                SetViewData();
                return View();
            }

        }

        #region LoadGrid
        /// <summary>
        /// phân biệt loại trường học để đưa ra màn hình cụ thể và tiến hành xếp loại
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="SemesterID"></param>
        /// <param name="ranking"></param>
        /// <returns></returns>


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(int ClassID, int Semester, int? ranking)
        {
            int rankingType = GetRankingType();

            if (rankingType < 0) return PartialView("_List", null);

            ViewData[PupilRankingOfSemesterConstants.SEMESTER_ID] = Semester;

            //Get data for combobox in grid
            ViewData[PupilRankingOfSemesterConstants.LIST_CONDUCT] = this.ConductLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true }, { "AppliedLevel", new GlobalInfo().AppliedLevel.Value } }).ToList();
            ViewData["ClassID"] = ClassID;
            string className = ClassProfileBusiness.Find(ClassID).DisplayName;
            ViewData[PupilRankingOfSemesterConstants.TITLE_PAGE] = Res.Get("PupilRankingPeriod_List") + " - Học Kỳ " + Semester + " Lớp: " + className;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",Semester}
            };
            ViewData[PupilRankingOfSemesterConstants.IS_CONFIG_RANKING] = ConfigConductRankingBusiness.GetAllConfigConductRanking(dic).Count() > 0 ? true : false;


            GlobalInfo global = new GlobalInfo();
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            // Lay thong tin hien thi hanh kiem ky 2 duoc ap dung cho ca nam
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                ViewData[PupilRankingOfSemesterConstants.ISHK2TOALL] = academicYear.IsSecondSemesterToSemesterAll;
            }
            else
            {
                ViewData[PupilRankingOfSemesterConstants.ISHK2TOALL] = false;
            }

            ViewData[PupilRankingOfSemesterConstants.SEMESTER_END_DATE] = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate : academicYear.SecondSemesterEndDate;

            if (Semester == 1 && global.IsCurrentYear)
            {
                if (academicYear.FirstSemesterEndDate < DateTime.Now)
                {
                    ViewData[PupilRankingOfSemesterConstants.LIST_DISABLE] = "true";
                }
                else
                {
                    ViewData[PupilRankingOfSemesterConstants.LIST_DISABLE] = "false";
                }
            }
            if (global.IsCurrentYear && (Semester == 2 || Semester == 3))
            {
                if (academicYear.SecondSemesterEndDate < DateTime.Now)
                {
                    ViewData[PupilRankingOfSemesterConstants.LIST_DISABLE] = "true";
                }
                if (academicYear.SecondSemesterEndDate >= DateTime.Now && academicYear.SecondSemesterStartDate <= DateTime.Now)
                {
                    ViewData[PupilRankingOfSemesterConstants.LIST_DISABLE] = "false";
                }
            }

            if (Semester == 4)
            {
                ViewData[PupilRankingOfSemesterConstants.LIST_DISABLE] = "false";
            }

            if (!global.IsCurrentYear && Semester != 4)
            {
                ViewData[PupilRankingOfSemesterConstants.LIST_DISABLE] = "true";
            }
            if (global.IsAdminSchoolRole == true)
            {
                ViewData[PupilRankingOfSemesterConstants.LIST_DISABLE] = "false";

            }
            // TungNT48 start 29/3/2013
            bool permission = UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, ClassID);
            if (permission)
            {
                if (global.IsAdminSchoolRole == true)
                {
                    if (!global.IsCurrentYear)
                    {
                        ViewData[PupilRankingOfSemesterConstants.BUTTON] = "disabled";
                    }
                    else
                    {
                        ViewData[PupilRankingOfSemesterConstants.BUTTON] = "";
                    }
                }
                else
                {
                    //Kiểm tra thời gian AcademicYear(Semester).EndDate < DateTime.Now => Disable. Ngược lại thì Enable              
                    bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                              ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                              : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                    if (timeYear)
                    {
                        ViewData[PupilRankingOfSemesterConstants.BUTTON] = "";
                    }
                    else
                    {
                        ViewData[PupilRankingOfSemesterConstants.BUTTON] = "disabled";
                    }
                }
            }
            else
            {
                ViewData[PupilRankingOfSemesterConstants.BUTTON] = "disabled";
            }
            // end
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(global.SchoolID.Value);
            bool checkGDTX = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
            ViewData[PupilRankingOfSemesterConstants.ISGDTX] = checkGDTX ? "true" : "false";
            ViewData[PupilRankingOfSemesterConstants.IS_RETRANING] = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;

            List<int> listLearningType = new List<int>() { SystemParamsInFile.PUPIL_LEARNING_TYPE_SELF_LEARNING, SystemParamsInFile.PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING };

            //check xem so co khoa nhap lieu hay khong
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            string semesterTitle = string.Empty;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                semesterTitle = "HK1";
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                semesterTitle = "HK2";
            }
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle) && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
            {
                isLockInput = objLockInput.LockTitle.Contains(semesterTitle);
            }
            ViewData[PupilRankingOfSemesterConstants.IS_LOCK_INPUT] = isLockInput;
            if (isLockInput)
            {
                string LockUserName = string.Empty;
                int LockUserID = Semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST ? objLockInput.UserLock1ID.Value : objLockInput.UserLock2ID.Value;
                int HierachyLevelID = this.GetHierachyLevelIDByUserAccountID(LockUserID);
                LockUserName = HierachyLevelID == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE ? "Sở giáo dục" : HierachyLevelID == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE ? "Phòng giáo dục" : "";
                ViewData[PupilRankingOfSemesterConstants.LOCK_USER_NAME] = "Xếp loại hạnh kiểm đã bị khóa nhập liệu bởi " + LockUserName;
            }
            if (rankingType == 0)
            {
                var model = this.PupilRankingBusiness.GetPupilToRankConductByCapacity(global.SchoolID.Value, global.AcademicYearID.Value, ClassID, Semester, null)
                                                        .OrderBy(o => o.OrderInClass)
                                                        .ThenBy(o => o.Name)
                                                        .ThenBy(o => o.FullName).ToList();

                foreach (var item in model)
                    item.IsKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, global.AppliedLevel.Value, item.LearningType, item.BirthDate);

                if (ranking.HasValue && ranking.Value > 0)
                    foreach (var item in model.Where(u => u.CapacityLevelID.HasValue))
                    {
                        if (!item.IsKXLHK)
                            item.ConductLevelID = GetPupilRankingByCapacityLevel(new GlobalInfo().AppliedLevel.Value, item.CapacityLevelID.Value);
                        else
                            item.ConductLevelID = null;
                    }
                return PartialView("_List", model);
            }

            if (rankingType == 1)
            {
                var model = this.PupilRankingBusiness.GetPupilToRankConductByFault(new GlobalInfo().SchoolID.Value, new GlobalInfo().AcademicYearID.Value, ClassID, Semester, null)
                                                    .OrderBy(o => o.OrderInClass)
                                                    .ThenBy(o => o.Name)
                                                    .ThenBy(o => o.FullName).ToList();
                var lstConductConfig = this.ConductConfigByViolationBusiness.Search(new Dictionary<string, object>() { { "SchoolID", new GlobalInfo().SchoolID.Value }, { "AppliedLevel", new GlobalInfo().AppliedLevel.Value } }).ToList();
                foreach (var item in model)
                    item.IsKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, global.AppliedLevel.Value, item.LearningType, item.BirthDate);

                if (ranking.HasValue && ranking.Value > 0)
                    foreach (var item in model)
                    {
                        if (!item.IsKXLHK)
                            item.ConductLevelID = GetPupilRankingByViolation(lstConductConfig, item.TotalMark.Value);
                        else
                            item.ConductLevelID = null;
                    }
                return PartialView("_ListFault", model);
            }
            return null;
        }

        /// <summary>
        /// Lấy kiểu xét hạnh kiểm
        /// </summary>
        /// <returns>0: Theo học lực, 1 : theo lỗi vi phạm. -1 : Chưa cấu hình.</returns>
        private int GetRankingType()
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(GlobalInfo.getInstance().AcademicYearID.Value);
            if (academicYear.ConductEstimationType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY)
            {
                return 0;
            }
            else if (academicYear.ConductEstimationType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION)
            {
                return 1;
            }
            else
            {
                return -1;
            }
            //IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic.Add("SchoolID", glo.SchoolID.Value);
            //dic.Add("AppliedLevel", glo.AppliedLevel.Value);
            //dic.Add("IsActive", true);

            //var configByCapacity = this.ConductConfigByCapacityBu.Search(dic).FirstOrDefault();
            //if (configByCapacity != null) return 0;

            //var configByViolation = this.ConductConfigByViolationBu.Search(dic).FirstOrDefault();
            //if (configByViolation != null) return 1;

            //return -1;
        }

        private List<ConductConfigByCapacity> mapCapacityToConduct = null;
        /// <summary>
        /// Lấy hạnh kiểm của học sinh theo học lực
        /// </summary>
        /// <param name="AppliedLevel"></param>
        /// <param name="CapacityLevelID"></param>
        /// <returns></returns>
        private int? GetPupilRankingByCapacityLevel(int AppliedLevel, int CapacityLevelID)
        {
            int SchoolID = GlobalInfo.getInstance().SchoolID.Value;
            // Tim du lieu cau hinh xep loai hanh kiem theo hoc luc cua truong
            if (this.mapCapacityToConduct == null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AppliedLevel"] = AppliedLevel;
                this.mapCapacityToConduct = ConductConfigByCapacityBusiness.Search(dic).ToList();
            }
            var Conduct = mapCapacityToConduct.FirstOrDefault(p => p.SchoolID == SchoolID && p.AppliedLevel == AppliedLevel && p.CapacityLevelID == CapacityLevelID);
            if (Conduct != null)
            {
                return Conduct.ConductLevelID;
            }
            // Neu khong tim thay cau hinh thi thuc hien xep loai theo mac dinh
            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT || CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD)
                    return SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL)
                    return SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK)
                    return SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                    return SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY;
            }

            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT || CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD)
                    return SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL)
                    return SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK)
                    return SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                    return SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY;
            }

            return null;
        }

        /// <summary>
        /// Lấy hạnh kiểm của học sinh theo lỗi vi phạm
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private int? GetPupilRankingByViolation(List<ConductConfigByViolation> lstConductConfig, decimal total)
        {
            foreach (var item in lstConductConfig)
                if (total >= item.MinValue.Value)
                    return item.ConductLevelID;

            ConductConfigByViolation lowestLevel = lstConductConfig.Where(u => u.MinValue == lstConductConfig.Min(v => v.MinValue)).SingleOrDefault();

            if (lowestLevel != null && (total < 0 || (lowestLevel.MinValue.HasValue && total <= lowestLevel.MinValue.Value)))
                return lowestLevel.ConductLevelID;

            return null;
        }

        #endregion

        #region Insert

        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult InsertRanking(SearchViewModel model, FormCollection form)
        {
            // DungVA - Lấy EducationLevelID từ chức năng gộp mới
            ClassProfile objCP = new ClassProfile();
            if (model.ClassID > 0)
            {
                objCP = ClassProfileBusiness.Find(model.ClassID);
                model.EducationlevelID = objCP.EducationLevelID;
            }
            int ClassID = model.ClassID;
            int Semester = model.Semester;
            List<PupilRanking> lstpupilranking = new List<PupilRanking>();
            int rankingType = GetRankingType();
            bool isCheckAll = "on".Equals(form["chkAll"]);
            AcademicYear acaYear = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            SchoolProfile schoolProfile = acaYear.SchoolProfile;
            int partitionId = UtilsBusiness.GetPartionId(acaYear.SchoolID);
            bool isGDTX = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
            int pupilID = 0;

            SummedUpRecordClass objSummedUpRecordClass = new SummedUpRecordClass
            {
                AcademicYearID = acaYear.AcademicYearID,
                ClassID = ClassID,
                PeriodID = null,
                Semester = Semester,
                CreatedDate = DateTime.Now,
                EducationLevelID = objCP.EducationLevelID,
                SchoolID = acaYear.SchoolID,
                NumberOfPupil = 0,
                Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1,
                Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING,
                SynchronizeID = 0,
            };
            //12.02.2018 Kiem tra lop co dang thuc hien xep loai HK, tong ket, xep loai khong

            if (SummedUpRecordClassBusiness.CheckExistsSummedExcuting(objSummedUpRecordClass))
            {
                return Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_Summeding"), JsonMessage.ERROR));
            }
            //Danh dau lop dang thuc hien tinh tong ket, xep loai
            SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);


            if (rankingType == 0)
            {
                var modelCapacity = this.PupilRankingBusiness.GetPupilToRankConductByCapacity(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, Semester, null).Where(u => u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
                foreach (var item in modelCapacity)
                {
                    PupilRanking pupilranking = new PupilRanking();
                    pupilID = item.PupilID.Value;
                    pupilranking.EducationLevelID = (int)model.EducationlevelID;
                    pupilranking.SchoolID = acaYear.SchoolID;
                    pupilranking.AcademicYearID = acaYear.AcademicYearID;
                    pupilranking.ClassID = model.ClassID;
                    pupilranking.Semester = (int?)model.Semester;
                    pupilranking.PupilID = pupilID;
                    pupilranking.TotalAbsentDaysWithPermission = item.TotalAbsentDaysWithPermission;
                    pupilranking.TotalAbsentDaysWithoutPermission = item.TotalAbsentDaysWithoutPermission;
                    pupilranking.CreatedAcademicYear = acaYear.Year;
                    pupilranking.PeriodID = null;
                    pupilranking.AverageMark = item.AverageMark;
                    pupilranking.CapacityLevelID = item.CapacityLevelID;
                    pupilranking.Last2digitNumberSchool = partitionId;
                    if (isCheckAll)
                    {
                        pupilranking.PupilRankingComment = form["hdfComment"];
                    }
                    else
                    {
                        pupilranking.PupilRankingComment = form["PupilComment_" + pupilID];
                    }
                    pupilranking.ConductLevelID = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, _globalInfo.AppliedLevel.Value, item.LearningType, item.BirthDate) ? null : GetConductLevel(item.PupilID.Value, form);

                    lstpupilranking.Add(pupilranking);
                }
            }

            if (rankingType == 1)
            {
                var modelFault = this.PupilRankingBusiness.GetPupilToRankConductByFault(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, Semester, null).Where(u => u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
                string comment = string.Empty;
                foreach (var item in modelFault)
                {
                    PupilRanking pupilranking = new PupilRanking();
                    pupilID = item.PupilID.Value;
                    pupilranking.EducationLevelID = (int)model.EducationlevelID;
                    pupilranking.SchoolID = acaYear.SchoolID;
                    pupilranking.AcademicYearID = acaYear.AcademicYearID;
                    pupilranking.ClassID = model.ClassID;
                    pupilranking.Semester = (int?)model.Semester;
                    pupilranking.PupilID = pupilID;
                    pupilranking.TotalAbsentDaysWithPermission = item.TotalAbsentDaysWithPermission;
                    pupilranking.TotalAbsentDaysWithoutPermission = item.TotalAbsentDaysWithoutPermission;
                    pupilranking.CreatedAcademicYear = acaYear.Year;
                    pupilranking.Last2digitNumberSchool = partitionId;
                    pupilranking.PeriodID = null;
                    pupilranking.AverageMark = item.AverageMark;
                    pupilranking.CapacityLevelID = item.CapacityLevelID;
                    if (isCheckAll)
                    {
                        pupilranking.PupilRankingComment = form["hdfComment"];
                    }
                    else
                    {
                        pupilranking.PupilRankingComment = form["PupilComment_" + pupilID];
                    }
                    pupilranking.ConductLevelID = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, _globalInfo.AppliedLevel.Value, item.LearningType, item.BirthDate) ? null : GetConductLevel(item.PupilID.Value, form);

                    lstpupilranking.Add(pupilranking);
                }
            }

            if (lstpupilranking.Count > 0)
            {
                try
                {


                    if (UtilsBusiness.IsMoveHistory(acaYear)) //du lieu lich su
                    {
                        this.PupilRankingHistoryBusiness.RankingPupilConductHistory(_globalInfo.UserAccountID, lstpupilranking, Semester, acaYear.IsSecondSemesterToSemesterAll);
                    }
                    else//Du lieu hien tai
                    {
                        this.PupilRankingBusiness.RankingPupilConduct(_globalInfo.UserAccountID, lstpupilranking, Semester, acaYear.IsSecondSemesterToSemesterAll);
                    }
                }
                finally
                {
                    objSummedUpRecordClass.ModifiedDate = DateTime.Now;
                    objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                    objSummedUpRecordClass.NumberOfPupil = lstpupilranking.Count;
                    SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);
                }
                //this.PupilRankingBusiness.Save();
                SetViewDataActionAudit("", "", "", "Lưu xếp loại hạnh kiểm", "", "Xếp loại hạnh kiểm"
                    , SMAS.Business.Common.GlobalConstants.ACTION_ADD, "Xếp loại hạnh kiểm lớp " + objCP.DisplayName + "Học kỳ " + Semester);
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            else
            {
                objSummedUpRecordClass.ModifiedDate = DateTime.Now;
                objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                objSummedUpRecordClass.NumberOfPupil = lstpupilranking.Count;
                SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter")), JsonMessage.ERROR);
            }
        }

        public static int? GetConductLevel(int pupilID, FormCollection form)
        {
            string conductLevel = form.Get("ConductLevel_" + pupilID);
            if (string.IsNullOrEmpty(conductLevel))
            {
                return null;
            }
            else
            {
                int result = 0;
                if (!string.IsNullOrEmpty(conductLevel) && int.TryParse(conductLevel, out result))
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }


        #endregion

        #region Excel

        public FileResult DownloadReport(int? Semester, int? ClassID, int? ranking)
        {
            int rankingType = GetRankingType();
            List<int> listLearningType = new List<int>() { SystemParamsInFile.PUPIL_LEARNING_TYPE_SELF_LEARNING, SystemParamsInFile.PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING };
            GlobalInfo Global = new GlobalInfo();
            List<ConductLevel> lstConduct = this.ConductLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true }, { "AppliedLevel", new GlobalInfo().AppliedLevel.Value } }).ToList();
            List<PupilRankingBO> model = new List<PupilRankingBO>();

            AcademicYear ay = AcademicYearBusiness.Find(Global.AcademicYearID);
            DateTime startsemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? ay.FirstSemesterStartDate.Value : ay.SecondSemesterStartDate.Value;
            DateTime endsemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? ay.FirstSemesterEndDate.Value : ay.SecondSemesterEndDate.Value;

            if (rankingType == 0)
            {
                model = this.PupilRankingBusiness.GetPupilToRankConductByCapacity(Global.SchoolID.Value, new GlobalInfo().AcademicYearID.Value, ClassID.Value, Semester.Value, null);

                if (ranking.HasValue && ranking.Value > 0)
                    foreach (var item in model)
                    {
                        if (item.CapacityLevelID.HasValue)
                        {
                            item.ConductLevelID = GetPupilRankingByCapacityLevel(Global.AppliedLevel.Value, item.CapacityLevelID.Value);
                            foreach (var itemConduct in lstConduct)
                            {
                                if (item.ConductLevelID == itemConduct.ConductLevelID)
                                {
                                    item.ConductLevel = itemConduct.Resolution;
                                }
                            }
                        }
                    }
            }

            if (rankingType == 1)
            {
                model = this.PupilRankingBusiness.GetPupilToRankConductByFault(Global.SchoolID.Value, new GlobalInfo().AcademicYearID.Value, ClassID.Value, Semester.Value, null);
                var lstConductConfig = this.ConductConfigByViolationBusiness.Search(new Dictionary<string, object>() { { "SchoolID", new GlobalInfo().SchoolID.Value } }).ToList();
                if (ranking.HasValue && ranking.Value > 0)
                    foreach (var item in model)
                    {
                        item.ConductLevelID = GetPupilRankingByViolation(lstConductConfig, item.TotalMark.Value);
                        foreach (var itemConduct in lstConduct)
                        {
                            if (item.ConductLevelID == itemConduct.ConductLevelID)
                            {
                                item.ConductLevel = itemConduct.Resolution;
                            }
                        }
                    }
            }
            ClassProfile cp = ClassProfileBusiness.Find(ClassID.Value);
            int appliedLevel = Global.AppliedLevel.Value;
            bool checkGDTX = ay.SchoolProfile.TrainingTypeID.HasValue && ay.SchoolProfile.TrainingType.Resolution == "GDTX";

            List<PupilDiscipline> pd = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        ? PupilDisciplineBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID }, { "ClassID", ClassID } }).ToList()
                                        : PupilDisciplineBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID }, { "ClassID", ClassID }, { "FromDisciplinedDate", startsemester }, { "ToDisciplinedDate", endsemester } }).ToList();


            List<PupilPraise> ppr = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        ? PupilPraiseBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID }, { "ClassID", ClassID } }).ToList()
                                        : PupilPraiseBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID }, { "ClassID", ClassID }, { "FromPraiseDate", startsemester }, { "ToPraiseDate", endsemester } }).ToList();

            ReportDefinition reportDef = null;
            if (rankingType == 0)
                reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_BANGXEPLOAIHANHKIEM_THEOKY);
            else
                reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_BANGXEPLOAIHANHKIEM_THEOKY_VP);

            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string ReportName = reportDef.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);
            IVTRange topRow = Template.GetRange("A7", "K7");
            IVTRange midRow = Template.GetRange("A8", "K8");
            IVTRange botRow = Template.GetRange("A9", "K9");

            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.Name = "BANGXLHK";
            Sheet.CopyPasteSameSize(Template.GetRange("A1", "K6"), 1, 1);

            Sheet.SetCellValue("A2", cp.SchoolProfile.SchoolName.ToUpper());
            Sheet.SetCellValue("A3", "BẢNG XẾP LOẠI HẠNH KIỂM LỚP " + cp.DisplayName);
            Sheet.SetCellValue("A4", getSemesterName(Semester.Value).ToUpper() + " - NĂM HỌC " + ay.DisplayTitle);
            if (rankingType == 0)
            {
                Sheet.SetCellValue("H6", "Học lực " + getSemesterName(Semester.Value));
            }
            if (model.Count > 0)
            {
                int i = 0;
                int startrow = 7;
                foreach (var item in model)
                {
                    i++;
                    int countDis = pd.Where(o => o.PupilID == item.PupilID).Count();
                    int countPraise = ppr.Where(o => o.PupilID == item.PupilID).Count();

                    Sheet.SetCellValue(startrow, 1, i);
                    Sheet.SetCellValue(startrow, 2, item.PupilCode);
                    Sheet.SetCellValue(startrow, 3, item.FullName);
                    // Chi fill du lieu cho hoc sinh hoc trong ky
                    bool isShow = item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                    isShow = isShow || ((item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                        || item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                        || item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                        && item.EndDate.HasValue && item.EndDate.Value > endsemester);
                    bool isNotRankConduct = PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, _globalInfo.AppliedLevel.Value, item.LearningType, item.BirthDate);
                    if (isShow)
                    {
                        Sheet.SetCellValue(startrow, 4, item.TotalAbsentDaysWithPermission);
                        Sheet.SetCellValue(startrow, 5, item.TotalAbsentDaysWithoutPermission);
                        Sheet.SetCellValue(startrow, 6, countPraise);
                        Sheet.SetCellValue(startrow, 7, countDis);
                        if (rankingType == 0)
                        {
                            Sheet.SetCellValue(startrow, 8, item.CapacityLevel);
                            if (!isNotRankConduct)
                            {
                                Sheet.SetCellValue(startrow, 9, item.ConductLevel);
                            }
                            Sheet.SetCellValue(startrow, 10, item.Comment);
                        }
                        else
                        {
                            Sheet.SetCellValue(startrow, 8, item.NumberOfFault);
                            Sheet.SetCellValue(startrow, 9, item.TotalMark);
                            if (!isNotRankConduct)
                            {
                                Sheet.SetCellValue(startrow, 10, item.ConductLevel);
                            }
                            Sheet.SetCellValue(startrow, 11, item.Comment);
                        }


                    }
                    if (i == model.Count)
                    {
                        Sheet.CopyPaste(botRow, startrow, 1, true);
                    }
                    else
                    {
                        if (i % 5 == 0)
                        {
                            Sheet.CopyPaste(botRow, startrow, 1, true);
                        }
                        else if (i % 5 == 1)
                        {
                            Sheet.CopyPaste(topRow, startrow, 1, true);
                        }
                        else
                        {
                            Sheet.CopyPaste(midRow, startrow, 1, true);
                        }
                    }
                    // Neu hoc sinh khong phai dang hoc hoac tot nghiep thi se boi do dong do
                    if (item.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING && item.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        Sheet.GetRow(startrow).SetFontColour(System.Drawing.Color.Red);
                    }
                    if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        Sheet.GetRange("A" + startrow, "K" + startrow).FillColor(System.Drawing.Color.Yellow);
                    }
                    startrow++;
                }
            }
            Sheet.FitToPage = true;
            Template.Delete();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            //BangXepLoaiHanhKiem_[Semester]_[ClassName]
            ReportName = ReportName.Replace("[Semester]", getSemesterNameForReport(Semester));
            ReportName = ReportName.Replace("[ClassName]", cp.DisplayName);

            result.FileDownloadName = ReportUtils.StripVNSign(ReportName) + "." + reportDef.OutputFormat;
            return result;
        }

        private string getSemesterNameForReport(int? semester)
        {
            if (!semester.HasValue) return "";
            switch (semester)
            {
                case SystemParamsInFile.SEMESTER_OF_YEAR_FIRST: return "HKI";
                case SystemParamsInFile.SEMESTER_OF_YEAR_SECOND: return "HKII";
                case SystemParamsInFile.SEMESTER_OF_YEAR_ALL: return "CaNam";
                case SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING: return "RLL";
                case SystemParamsInFile.SEMESTER_OF_YEAR_RETEST: return "ThiLai";
                default: return "";
            }
        }

        private string getSemesterName(int semester)
        {
            switch (semester)
            {
                case SystemParamsInFile.SEMESTER_OF_YEAR_FIRST: return "HK I";
                case SystemParamsInFile.SEMESTER_OF_YEAR_SECOND: return "HK II";
                case SystemParamsInFile.SEMESTER_OF_YEAR_ALL: return "Cả năm";
                case SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING: return "Rèn luyện lại";
                case SystemParamsInFile.SEMESTER_OF_YEAR_RETEST: return "Thi lại";
                default: return "";
            }
        }
        #endregion
    }
}





