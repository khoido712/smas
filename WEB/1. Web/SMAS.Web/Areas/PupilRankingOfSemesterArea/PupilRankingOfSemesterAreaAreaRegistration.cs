﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilRankingOfSemesterArea
{
    public class PupilRankingOfSemesterAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilRankingOfSemesterArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilRankingOfSemesterArea_default",
                "PupilRankingOfSemesterArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
