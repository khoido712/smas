﻿using System;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilProfileReportArea;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.PhysicalExaminationArea;
using SMAS.DAL.IRepository;
using SMAS.Web.Areas.PhysicalExaminationArea.Models;


namespace SMAS.Web.Areas.MonthlyHealthReportArea.Controllers
{

    public class MonthlyHealthReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IFlowSituationBusiness FlowSituationBusiness;
        private readonly IPolicyTargetBusiness PolicyTargetBusiness;
        private readonly IPriorityTypeBusiness PriorityTypeBusiness;
        private readonly IPhysicalExaminationBusiness PhysicalExaminationBusiness;
        private readonly IDevelopmentStandardRepository DevelopmentStandardRepository;

        public MonthlyHealthReportController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
             IFlowSituationBusiness flowSituationBusiness,
            IPolicyTargetBusiness policyTargetBusiness,
            IPriorityTypeBusiness priorityTypeBusiness,
            IPhysicalExaminationBusiness physicalExaminationBusiness,
            IDevelopmentStandardRepository developmentStandardRepository)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.FlowSituationBusiness = flowSituationBusiness;
            this.PolicyTargetBusiness = policyTargetBusiness;
            this.PriorityTypeBusiness = priorityTypeBusiness;
            this.PhysicalExaminationBusiness = physicalExaminationBusiness;
            this.DevelopmentStandardRepository = developmentStandardRepository;
        }

        //
        // GET: /ReportMenuChildrenArea/ChildrenPolicyReport/

        public ActionResult Index()
        {
            var lsEducationLevel = _globalInfo.EducationLevels.ToList();
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);
            if (checkCombineClass)
            {
                lsEducationLevel.Add(new EducationLevel { Resolution = SystemParamsInFile.Combine_Class_Name, EducationLevelID = SystemParamsInFile.Combine_Class_ID });
            }
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            var listMonth = getListMonth();
            var montCurrent = DateTime.Now.Month;
            ViewData[PhysicalExaminationConstants.LIST_MONTH] = listMonth.Select(s => new SelectListItem { Value = s.Value.ToString() + s.Key.ToString(), Text = "Tháng " + s.Key.ToString(), Selected = (s.Key == montCurrent) ? true : false }).ToList();

            return View();
        }

        private List<KeyValuePair<int, int>> getListMonth()
        {
            return PhysicalExaminationBusiness.GetListMonthByAcademiId(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value);
        }

        public JsonResult GetMonthlyHealthyReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int monthID = !string.IsNullOrEmpty(frm["monthID"]) ? int.Parse(frm["monthID"]) : 0;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.BC_Yte_TKCandosuckhoe);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                {"monthID",monthID}
            };
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PhysicalExaminationBusiness.GetProcessReportOfMonthlyHeathy(dic, SystemParamsInFile.BC_Yte_TKCandosuckhoe);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExcelMonthlyHealthReport(educationLevelID, monthID);
                processedReport = PhysicalExaminationBusiness.InsertProcessReportOfMonthlyHeathy(dic, excel, SystemParamsInFile.BC_Yte_TKCandosuckhoe);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        public JsonResult GetNewReport(FormCollection frm)
        {
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int monthID = !string.IsNullOrEmpty(frm["monthID"]) ? int.Parse(frm["monthID"]) : 0;
            ProcessedReport processedReport = null;
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                {"monthID",monthID}
            };
            Stream excel = this.ExcelMonthlyHealthReport(educationLevelID, monthID);
            processedReport = PhysicalExaminationBusiness.InsertProcessReportOfMonthlyHeathy(dicInsert, excel, SystemParamsInFile.BC_Yte_TKCandosuckhoe);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"MonthInAcademicYear",_globalInfo.MonthInAcademicYear}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.BC_Yte_TKCandosuckhoe,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.BC_Yte_TKCandosuckhoe,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }

        public Stream ExcelMonthlyHealthReport(int? educationLevelID, int? Month)
        {
            if (Month.ToString().Length == 5)
            {
                int _m = int.Parse(Month.ToString().Substring(0, 4) + "0" + Month.ToString().Substring(4, 1));
                Month = _m;
            }
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.BC_Yte_TKCandosuckhoe + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            //Lấy sheet hoc sinh dien chinh sach
            IVTWorksheet sheet = oBook.GetSheet(1);
            sheet.Name = "TK Can do SK";

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            //lay danh sach lop hoc theo khoi
            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", academicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            if(educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dicClass.Add("EducationLevelID", educationLevelID);
          
            //danh sach tat ca cac lop
            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                    .OrderBy(p => p.EducationLevelID)
                                                    .ThenBy(u => u.OrderNumber)
                                                    .ThenBy(u => u.DisplayName).ToList();

            if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                lstClassProfile = lstClassProfile.Where(x => x.IsCombinedClass == true).ToList();
            }
            else if (educationLevelID != 0)
                lstClassProfile = lstClassProfile.Where(x => x.IsCombinedClass == false).ToList();

            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();

            List<int> lstEducationLevelID = lstClassProfile.Select(p => p.EducationLevelID).Distinct().ToList();

            // Kiểm tra trường có lớp ghép hay không
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);

            //Nếu có lớp ghép thêm vào List lstEducationLevelID
            if ((educationLevelID == 0 || educationLevelID == SystemParamsInFile.Combine_Class_ID) && checkCombineClass )
                lstEducationLevelID.Add(SystemParamsInFile.Combine_Class_ID);
          
            // Lấy educationLevelID khi chọn educationLevelID
            if (educationLevelID > 0) {
                lstEducationLevelID = lstEducationLevelID.Select((v, i) => new { v, i })
                        .Where(x => x.v == educationLevelID)
                        .Select(x => x.v).ToList<int>();
            }

            //danh sach hoc sinh tong lop
            List<PupilOfClassBO> lstPupilOfClassBO = ListPupilOfClass(lstClassID);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = academicYearID;
            dic["MonthID"] = Month;

            List<PhysicalExamination> lisExam = this.PhysicalExaminationBusiness.Search(dic).ToList();

            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();

            sheet.SetCellValue("A3", school.SchoolName.ToUpper());
            sheet.SetCellValue("A2", school.SupervisingDept.SupervisingDeptName.ToUpper());
            Month = int.Parse(Month.ToString().Substring(4, 2));
            sheet.SetCellValue("B6", "THÁNG " + Month + " NĂM HỌC " + ay.DisplayTitle);
            ClassProfile classProfile = new ClassProfile();

            ClassProfile objCP = null;
            int startRow = 11;
            int startCol = 3;

            int countWeightOver = 0;
            int countWeightNormal = 0;
            int countWeightLev1 = 0;
            int countWeightLev2 = 0;
            int countHeghtOver = 0;
            int countHeghtNormal = 0;
            int countHeghtLev1 = 0;
            int countHeghtLev2 = 0;

            int countWeightOverF = 0;
            int countWeightNormalF = 0;
            int countWeightLev1F = 0;
            int countWeightLev2F = 0;
            int countHeghtOverF = 0;
            int countHeghtNormalF = 0;
            int countHeghtLev1F = 0;
            int countHeghtLev2F = 0;
            int stt = 1;
            List<ClassProfile> objCPa = null;
            int TotalPupil = 0;
            int totalProcess = 0;
            int totalFemale = 0;
            string[] tableAnpha = new string[] { "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U" };

            int totalEduFillData = 0;
            List<int> listRowTotalEdu = new List<int>();

            #region fill data
                for (int e = 0; e < lstEducationLevelID.Count; e++)
                {
                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 100).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    sheet.GetRange(startRow, 1, startRow, 30).SetHAlign(VTHAlign.xlHAlignCenter);
                    //test
                    sheet.SetFontName("Times New Roman", 12);

                    //danh sach cac lop trong 1 khoi 
                    objCPa = lstClassProfile.ToList();
                    if (lstEducationLevelID[e] != SystemParamsInFile.Combine_Class_ID)
                        objCPa = lstClassProfile.Where(x => x.EducationLevelID == lstEducationLevelID[e] && x.IsCombinedClass == false).ToList();

                    if(lstEducationLevelID[e] == SystemParamsInFile.Combine_Class_ID)
                        objCPa = lstClassProfile.Where(x => x.IsCombinedClass == true).ToList();
                        
                    sheet.SetCellValue(startRow, 1, lstEducationLevelID[e] == SystemParamsInFile.Combine_Class_ID ? SystemParamsInFile.Combine_Class_Name : objCPa[0].EducationLevel.Resolution);

                    if (objCPa != null)
                    {
                        // total danh gia                  
                        for (int m = 0; m < tableAnpha.Length; m++)
                        {
                            string formular = string.Format("=SUM({0}{1}:{0}{2})", tableAnpha[m], startRow + 1, startRow + objCPa.Count);
                            sheet.SetCellValue(startRow, (3 + m), formular);
                        }
                        listRowTotalEdu.Add(startRow);
                        startRow++;

                        #region list class profile
                            for (int i = 0; i < objCPa.Count; i++)
                            {
                                string className = "";
                                objCP = objCPa[i];
                                // danh sach hoc sinh trong 1 lop                      
                                #region danh sach hoc sinh trong 1 lop
                                startCol = 3;
                                lstPOCtmp = lstPupilOfClassBO.Where(p => p.ClassID == objCP.ClassProfileID).ToList();
                                var listExamOfClass = lisExam.Where(o => o.ClassID == objCP.ClassProfileID).ToList();
                                var lstPE1 = lstPOCtmp.Join(listExamOfClass, x => x.PupilID, y => y.PupilID, (x, y)
                                    => new { x.PupilID, y.Height, y.Weight, x.Genre, x.Birthday, y.PhysicalHeightStatus, y.PhysicalWeightStatus }).ToList();
                                TotalPupil = lstPOCtmp.Count();
                                totalProcess = lstPE1.Count();
                                totalFemale = lstPE1.Where(x => x.Genre == 0).Count();
                                int myInt = objCP.ClassProfileID;
                                classProfile = ClassProfileBusiness.Find(myInt);

                                if (classProfile != null)
                                {
                                    className += classProfile.DisplayName.ToUpper();
                                }

                                sheet.SetCellValue(startRow, 1, stt);
                                sheet.SetCellValue(startRow, 2, className);

                                sheet.SetCellValue(startRow, startCol++, TotalPupil);
                                totalEduFillData++;              

                                //fill edu
                                #region [fill edu]

                                #region

                                countWeightOver = lstPE1.Where(o => o.PhysicalWeightStatus == SMAS.Web.Constants.GlobalConstants.WEIGHT_STATUS_OVER_THAN_NORMAL).Count();
                                countWeightNormal = lstPE1.Where(o => o.PhysicalWeightStatus == SMAS.Web.Constants.GlobalConstants.WEIGHT_STATUS_NORMAL).Count();
                                countWeightLev1 = lstPE1.Where(o => o.PhysicalWeightStatus == SMAS.Web.Constants.GlobalConstants.WEIGHT_STATUS_MALNUTRITION_LEVEL1).Count();
                                countWeightLev2 = lstPE1.Where(o => o.PhysicalWeightStatus == SMAS.Web.Constants.GlobalConstants.WEIGHT_STATUS_MALNUTRITION_LEVEL2).Count();

                                countWeightOverF = lstPE1.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.PhysicalWeightStatus == SMAS.Web.Constants.GlobalConstants.WEIGHT_STATUS_OVER_THAN_NORMAL).Count();
                                countWeightNormalF = lstPE1.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.PhysicalWeightStatus == SMAS.Web.Constants.GlobalConstants.WEIGHT_STATUS_NORMAL).Count();
                                countWeightLev1F = lstPE1.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.PhysicalWeightStatus == SMAS.Web.Constants.GlobalConstants.WEIGHT_STATUS_MALNUTRITION_LEVEL1).Count();
                                countWeightLev2F = lstPE1.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.PhysicalWeightStatus == SMAS.Web.Constants.GlobalConstants.WEIGHT_STATUS_MALNUTRITION_LEVEL2).Count();

                                countHeghtOver = lstPE1.Where(o => o.PhysicalHeightStatus == SMAS.Web.Constants.GlobalConstants.HEIGHT_STATUS_OVER_THAN_NORMAL).Count();
                                countHeghtNormal = lstPE1.Where(o => o.PhysicalHeightStatus == SMAS.Web.Constants.GlobalConstants.HEIGHT_STATUS_NORMAL).Count();
                                countHeghtLev1 = lstPE1.Where(o => o.PhysicalHeightStatus == SMAS.Web.Constants.GlobalConstants.HEIGHT_STATUS_STUNTING_LEVEL1).Count();
                                countHeghtLev2 = lstPE1.Where(o => o.PhysicalHeightStatus == SMAS.Web.Constants.GlobalConstants.HEIGHT_STATUS_STUNTING_LEVEL2).Count();

                                countHeghtOverF = lstPE1.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.PhysicalHeightStatus == SMAS.Web.Constants.GlobalConstants.HEIGHT_STATUS_OVER_THAN_NORMAL).Count();
                                countHeghtNormalF = lstPE1.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.PhysicalHeightStatus == SMAS.Web.Constants.GlobalConstants.HEIGHT_STATUS_NORMAL).Count();
                                countHeghtLev1F = lstPE1.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.PhysicalHeightStatus == SMAS.Web.Constants.GlobalConstants.HEIGHT_STATUS_STUNTING_LEVEL1).Count();
                                countHeghtLev2F = lstPE1.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.PhysicalHeightStatus == SMAS.Web.Constants.GlobalConstants.HEIGHT_STATUS_STUNTING_LEVEL2).Count();

                                sheet.SetCellValue(startRow, startCol++, totalProcess);
                                sheet.SetCellValue(startRow, startCol++, totalFemale);

                                sheet.SetCellValue(startRow, startCol++, countWeightOver);
                                sheet.SetCellValue(startRow, startCol++, countWeightOverF);
                                sheet.SetCellValue(startRow, startCol++, countWeightNormal);
                                sheet.SetCellValue(startRow, startCol++, countWeightNormalF);
                                sheet.SetCellValue(startRow, startCol++, countWeightLev1);
                                sheet.SetCellValue(startRow, startCol++, countWeightLev1F);
                                sheet.SetCellValue(startRow, startCol++, countWeightLev2);
                                sheet.SetCellValue(startRow, startCol++, countWeightLev2F);

                                sheet.SetCellValue(startRow, startCol++, countHeghtOver);
                                sheet.SetCellValue(startRow, startCol++, countHeghtOverF);
                                sheet.SetCellValue(startRow, startCol++, countHeghtNormal);
                                sheet.SetCellValue(startRow, startCol++, countHeghtNormalF);
                                sheet.SetCellValue(startRow, startCol++, countHeghtLev1);
                                sheet.SetCellValue(startRow, startCol++, countHeghtLev1F);
                                sheet.SetCellValue(startRow, startCol++, countHeghtLev2);
                                sheet.SetCellValue(startRow, startCol++, countHeghtLev2F);
                                #endregion

                                //indexEducation++;

                                TotalPupil = 0;
                                totalProcess = 0;
                                totalFemale = 0;
                                #endregion

                                #endregion
                                stt++;
                                startRow++;
                            }
                       #endregion
                    }
                }
                #endregion

            string total = string.Empty;
            string indexStr = string.Empty;

            if (totalEduFillData > 0)
            {
                //fill tong cong
                sheet.GetRange(startRow, 1, startRow, 2).Merge();
                sheet.GetRange(startRow, 1, startRow, 100).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                sheet.SetCellValue(startRow, 1, "Tổng");

                for (int m = 0; m < tableAnpha.Length; m++)
                {
                    indexStr = "=SUM(";
                    for (int i = 0; i < listRowTotalEdu.Count; i++)
                    {
                        indexStr += string.Format("{0}{1},", tableAnpha[m], listRowTotalEdu[i]);
                    }
                    indexStr = indexStr.TrimEnd(',') + ")";
                    sheet.SetCellValue(startRow, 3 + m, indexStr);
                }

                startRow++;
                sheet.GetRange(startRow, 1, startRow, 3).Merge();
                sheet.GetRange(startRow, 1, startRow, 100).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                sheet.SetCellValue(startRow, 1, "Tỉ lệ (%)");

                for (int m = 1; m < tableAnpha.Length; m++)
                {
                    indexStr = string.Format("={0}{1}*100/C{1}", tableAnpha[m], startRow - 1);
                    sheet.SetCellValue(startRow, 3 + m, indexStr);
                }
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                var datetime = objSP.Province.ProvinceName + ", ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Date.Month + " năm " + DateTime.Now.Date.Year;
                sheet.GetRange(startRow + 2, 14, startRow + 2, 20).Merge();
                sheet.SetCellValue(startRow + 2, 14, datetime);
                sheet.GetRange(startRow + 2, 14, startRow + 2, 14).SetFontStyle(false, null, true, null, false, false);
                sheet.GetRange(startRow + 3, 14, startRow + 3, 20).Merge();
                sheet.SetCellValue(startRow + 3, 14, "Hiệu trưởng");
                sheet.GetRange(startRow + 3, 14, startRow + 3, 14).SetFontStyle(true, null, false, null, false, false);
                sheet.GetRange(startRow + 7, 14, startRow + 7, 20).Merge();
                sheet.SetCellValue(startRow + 7, 14, SchoolProfileBusiness.Find(_globalInfo.SchoolID).HeadMasterName);
                sheet.GetRange(startRow + 7, 14, startRow + 3, 14).SetFontStyle(true, null, false, null, false, false);
            }
            sheet.GetRange(11, 1, startRow, tableAnpha.Length + 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            sheet.GetRange(11, 1, startRow, tableAnpha.Length + 2).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(startRow, 4, startRow, tableAnpha.Length + 2).NumberFormat("0.00");

            sheet.FitSheetOnOnePage = true;

            return oBook.ToStream();
        }
        //END ExcelMonthlyHealthReport

        private List<PupilOfClassBO> ListPupilOfClass(List<int> lstClassID)
        {
            List<PupilOfClassBO> lstPupilOfClassBO = new List<PupilOfClassBO>();
            lstPupilOfClassBO = (from poc in PupilOfClassBusiness.All
                                 join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                 where poc.AcademicYearID == _globalInfo.AcademicYearID.Value
                                 && poc.SchoolID == _globalInfo.SchoolID
                                 && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                 && lstClassID.Contains(poc.ClassID)
                                 && pf.IsActive == true

                                 select new PupilOfClassBO
                                 {
                                     PupilID = poc.PupilID,
                                     ClassID = poc.ClassID,
                                     PupilFullName = poc.PupilProfile.FullName,
                                     PupilCode = poc.PupilProfile.PupilCode,
                                     Birthday = poc.PupilProfile.BirthDate,
                                     Genre = poc.PupilProfile.Genre,
                                     EthnicName = poc.PupilProfile.Ethnic.EthnicName,
                                     OrderInClass = poc.OrderInClass,
                                     Name = pf.Name,
                                     PolicyTargetName = pf.PolicyTarget.Resolution,
                                     PriorityTypeName = pf.PriorityType.Resolution
                                 }).OrderBy(p => p.OrderInClass.HasValue ? p.OrderInClass : 0)
                                 .ThenBy(p => p.PupilFullName).ThenBy(p => p.Name).ToList();

            return lstPupilOfClassBO;
        }

        private string GetPhysicalState(int? stt, int type)
        {
            //Chieu cao
            if (type == 1)
            {
                switch (stt.Value)
                {
                    case 1:
                        return "Cao hơn so với tuổi";
                    case 2:
                        return "Bình thường";
                    case 3:
                        return "Thấp còi độ 1";
                    case 4:
                        return "Thấp còi độ 2";
                    default:
                        return string.Empty;
                }
            }
            //Can nang
            else
            {
                switch (stt.Value)
                {
                    case 1:
                        return "Cân nặng hơn so với tuổi";
                    case 2:
                        return "Bình thường";
                    case 3:
                        return "Suy dinh dưỡng nhẹ";
                    case 4:
                        return "Suy dinh dưỡng nặng";
                    default:
                        return string.Empty;
                }
            }
        }
    }

}