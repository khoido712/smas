﻿using System;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilProfileReportArea;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.PhysicalExaminationArea;
using SMAS.DAL.IRepository;
using SMAS.Web.Areas.PhysicalExaminationArea.Models;


namespace SMAS.Web.Areas.MonthlyHealthReportArea.Controllers
{

    public class MonthlyHealthDetailsReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IFlowSituationBusiness FlowSituationBusiness;
        private readonly IPolicyTargetBusiness PolicyTargetBusiness;
        private readonly IPriorityTypeBusiness PriorityTypeBusiness;
        private readonly IPhysicalExaminationBusiness PhysicalExaminationBusiness;
        private readonly IDevelopmentStandardRepository DevelopmentStandardRepository;

        public MonthlyHealthDetailsReportController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
             IFlowSituationBusiness flowSituationBusiness,
            IPolicyTargetBusiness policyTargetBusiness,
            IPriorityTypeBusiness priorityTypeBusiness,
            IPhysicalExaminationBusiness physicalExaminationBusiness,
            IDevelopmentStandardRepository developmentStandardRepository)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.FlowSituationBusiness = flowSituationBusiness;
            this.PolicyTargetBusiness = policyTargetBusiness;
            this.PriorityTypeBusiness = priorityTypeBusiness;
            this.PhysicalExaminationBusiness = physicalExaminationBusiness;
            this.DevelopmentStandardRepository = developmentStandardRepository;
        }

        //
        // GET: /ReportMenuChildrenArea/ChildrenPolicyReport/

        public ActionResult Index()
        {
            var lsEducationLevel = _globalInfo.EducationLevels.ToList();
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);
            if (checkCombineClass)
            {
                lsEducationLevel.Add(new EducationLevel { Resolution = SystemParamsInFile.Combine_Class_Name, EducationLevelID = SystemParamsInFile.Combine_Class_ID });
            }
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            var listMonth = getListMonth();
            var montCurrent = DateTime.Now.Month;

            ViewData[PhysicalExaminationConstants.LIST_MONTH] = listMonth.Select(s => new SelectListItem { Value = s.Value.ToString() + s.Key.ToString(), Text = "Tháng " + s.Key.ToString(), Selected = (s.Key == montCurrent) ? true : false }).ToList();
            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            if (eduId != SystemParamsInFile.Combine_Class_ID)
                dicClass.Add("EducationLevelID", eduId);

            List<SelectListItem> lstClass = new List<SelectListItem>();
            if (eduId == SystemParamsInFile.Combine_Class_ID)
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.IsCombinedClass == true).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.CombinedClassCode.ToUpper(), Text = u.CombinedClassCode, Selected = false })
                                                    .Distinct()
                                                    .ToList();
                lstClass = lstClass
                            .GroupBy(p => new { p.Value })
                            .Select(g => g.First())
                            .ToList();                
            }
            else
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.IsCombinedClass != true).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();

            return Json(lstClass);
        }

        private List<KeyValuePair<int, int>> getListMonth()
        {
            return PhysicalExaminationBusiness.GetListMonthByAcademiId(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value);
        }

        public JsonResult GetMonthlyHealthyReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int monthID = !string.IsNullOrEmpty(frm["monthID"]) ? int.Parse(frm["monthID"]) : 0;
            string ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : "0";
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.BC_Yte_KetQuaCanDoSK);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                //{"ClassID",ClassID},
                {"monthID",monthID}
            };
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dic.Add("ClassID", ClassID);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PhysicalExaminationBusiness.GetProcessReportOfMonthlyHeathyDetail(dic, SystemParamsInFile.BC_Yte_KetQuaCanDoSK);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExcelMonthlyHealthReport(educationLevelID, monthID, ClassID);
                processedReport = PhysicalExaminationBusiness.InsertProcessReportOfMonthlyHeathyDetail(dic, excel, SystemParamsInFile.BC_Yte_KetQuaCanDoSK);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        public JsonResult GetNewReport(FormCollection frm)
        {
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int monthID = !string.IsNullOrEmpty(frm["monthID"]) ? int.Parse(frm["monthID"]) : 0;
            string ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : "0";
            //ClassProfile objClassProFile = ClassProfileBusiness.Find(classID);
            ProcessedReport processedReport = null;
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                 {"monthID",monthID}
            };
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dicInsert.Add("ClassID", ClassID);

            Stream excel = this.ExcelMonthlyHealthReport(educationLevelID, monthID, ClassID);
            processedReport = PhysicalExaminationBusiness.InsertProcessReportOfMonthlyHeathyDetail(dicInsert, excel, SystemParamsInFile.BC_Yte_KetQuaCanDoSK);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"MonthInAcademicYear",_globalInfo.MonthInAcademicYear}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.BC_Yte_KetQuaCanDoSK,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.BC_Yte_KetQuaCanDoSK,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream ExcelMonthlyHealthReport(int? educationLevelID, int Month, string classID)
        {
            if (Month.ToString().Length == 5)
            {
                int _m = int.Parse(Month.ToString().Substring(0, 4) + "0" + Month.ToString().Substring(4, 1));
                Month = _m;
            }
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.BC_Yte_KetQuaCanDoSK + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            //Lấy sheet hoc sinh dien chinh sach
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = null;

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            //lay danh sach lop hoc theo khoi
            int academicYearID = _globalInfo.AcademicYearID.Value;

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", academicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                    .OrderBy(p => p.EducationLevelID)
                                                    .ThenBy(u => u.OrderNumber)
                                                    .ThenBy(u => u.DisplayName).ToList();

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                //combobox không chọn tất cả
                if (educationLevelID != 0)
                {
                    int classIDMain = Int32.Parse(classID.ToString());
                    lstClassProfile = lstClassProfile
                                        .Where(x => x.IsCombinedClass != true &&
                                                    x.EducationLevelID == educationLevelID).ToList();
                    if (classIDMain != 0)
                    {
                        lstClassProfile = lstClassProfile.Where(x => x.ClassProfileID == classIDMain).ToList();
                    }
                }
            }
            else
            {
                lstClassProfile = lstClassProfile
                                    .Where(x => x.IsCombinedClass == true).ToList();

                if (classID != "0")
                {
                    lstClassProfile = lstClassProfile.Where(x => x.CombinedClassCode.ToUpper() == classID).ToList();
                }
            }

            if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
                lstClassProfile = lstClassProfile.OrderBy(x => x.CombinedClassCode).ToList();

            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();

            //danh sach hoc sinh tong lop
            List<PupilOfClassBO> lstPupilOfClassBO = ListPupilOfClass(lstClassID);

            //List PhysicalExam fiter theo nam truong 
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = academicYearID;
            dic["MonthID"] = Month;
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dic["ClassID"] = classID;

            List<PhysicalExamination> lisExam = this.PhysicalExaminationBusiness.Search(dic).ToList();
            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
            ClassProfile classProfile = new ClassProfile();
            IQueryable<PupilProfile> lsPupilProfile = PupilProfileBusiness.GetListPupilPolicyTarget(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value);
            ClassProfile objCP = null;

            string ResuftWeigth = string.Empty;
            string ResuftHeigth = string.Empty;

            for (int i = 0; i < lstClassProfile.Count(); i++)
            {
                objCP = lstClassProfile[i];
                sheet = oBook.CopySheetToLast(firstSheet, "I500");
                lstPOCtmp = lstPupilOfClassBO.Where(p => p.ClassID == objCP.ClassProfileID).ToList();

                var listExamOfClass = lisExam.Where(o => o.ClassID == objCP.ClassProfileID).ToList();
                var lstPE1 = lstPOCtmp.Join(listExamOfClass, x => x.PupilID, y => y.PupilID, (x, y)
                        => new { x.PupilID, y.Height, y.Weight, x.Genre, x.Birthday, x.PupilFullName, x.IsCombineClass, x.PupilCode, y.PhysicalHeightStatus, y.PhysicalWeightStatus }).ToList();

                string className = "";
                string combineClassCode = "";
                bool isCombineClass = false;
                if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
                    isCombineClass = true;
                // danh sach hoc sinh trong 1 lop   
                int myInt = objCP.ClassProfileID;
                classProfile = ClassProfileBusiness.Find(myInt);

                if (classProfile != null)
                {
                    className = classProfile.DisplayName;
                    combineClassCode = classProfile.CombinedClassCode;
                }

                SetValueToFile(sheet, lstPE1, className, combineClassCode, isCombineClass, ay.DisplayTitle, Month);
            }

            firstSheet.Delete();

            return oBook.ToStream();
        }

        private void SetValueToFile(IVTWorksheet sheet, 
            IEnumerable<dynamic> list, 
            string className, 
            string combineClassCode, 
            bool isCombineClass, 
            string yearTitle, 
            int? month)
        {
            int stt = 1;
            int StartRow = 10;
            string ResuftWeigth = string.Empty;
            string ResuftHeigth = string.Empty;

            //phong so
            sheet.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
            //truong
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            month = int.Parse(month.ToString().Substring(4, 2));
            sheet.SetCellValue("A5", "KẾT QUẢ CÂN ĐO SỨC KHỎE THÁNG " + month);

            if (isCombineClass == true)
                className = className + "(" + combineClassCode + ")";

            sheet.SetCellValue("A6", "Lớp " +  className + ", Năm học " + yearTitle);

            foreach (dynamic obj in list)
            {
                //201609
                int monthCount = GetMonthDifference(obj.Birthday, DateTime.Parse("1" + "/" + month + "/" + AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year));

                ResuftHeigth = this.GetPhysicalState(obj.PhysicalHeightStatus, 1);
                ResuftWeigth = this.GetPhysicalState(obj.PhysicalWeightStatus, 2);
                sheet.SetCellValue(StartRow, 1, stt);
                sheet.GetRange(StartRow, 1, StartRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);

                sheet.SetCellValue(StartRow, 2, obj.PupilFullName);
                sheet.SetCellValue(StartRow, 3, obj.PupilCode);
                sheet.GetRange(StartRow, 3, StartRow, 3).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(StartRow, 4, obj.Birthday.ToString("dd/MM/yyyy"));
                sheet.GetRange(StartRow, 4, StartRow, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                if (obj.Genre == 1)
                {
                    sheet.SetCellValue(StartRow, 5, "Nam");
                    sheet.GetRange(StartRow, 5, StartRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                }
                else
                {
                    sheet.SetCellValue(StartRow, 5, "Nữ");
                    sheet.GetRange(StartRow, 5, StartRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                }
                sheet.SetCellValue(StartRow, 6, obj.Weight);
                sheet.GetRange(StartRow, 6, StartRow, 6).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(StartRow, 7, ResuftWeigth);

                sheet.SetCellValue(StartRow, 8, obj.Height);
                sheet.GetRange(StartRow, 8, StartRow, 8).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(StartRow, 9, ResuftHeigth);

                sheet.GetRange(StartRow, 8, StartRow, 8).SetHAlign(VTHAlign.xlHAlignCenter);
                StartRow++;
                stt++;
                
            }
            
            sheet.GetRange(9, 1, StartRow - 1, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                
            sheet.Name = Utils.Utils.StripVNSignAndSpace(className);
            sheet.FitToPage = true;
        }

        private List<PupilOfClassBO> ListPupilOfClass(List<int> lstClassID)
        {
            List<PupilOfClassBO> lstPupilOfClassBO = new List<PupilOfClassBO>();
            lstPupilOfClassBO = (from poc in PupilOfClassBusiness.All
                                 join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                 where poc.AcademicYearID == _globalInfo.AcademicYearID.Value
                                 && poc.SchoolID == _globalInfo.SchoolID
                                 && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                 && lstClassID.Contains(poc.ClassID)
                                 && pf.IsActive == true
                                 select new PupilOfClassBO
                                 {
                                     PupilID = poc.PupilID,
                                     ClassID = poc.ClassID,
                                     PupilFullName = poc.PupilProfile.FullName,
                                     PupilCode = poc.PupilProfile.PupilCode,
                                     Birthday = poc.PupilProfile.BirthDate,
                                     Genre = poc.PupilProfile.Genre,
                                     EthnicName = poc.PupilProfile.Ethnic.EthnicName,
                                     OrderInClass = poc.OrderInClass,
                                     Name = pf.Name,
                                     PolicyTargetName = pf.PolicyTarget.Resolution,
                                     PriorityTypeName = pf.PriorityType.Resolution,
                                     Combine_Class_Code = poc.ClassProfile.CombinedClassCode
                                 }).OrderBy(p => p.OrderInClass.HasValue ? p.OrderInClass : 0)
                                 .ThenBy(p => p.PupilFullName).ThenBy(p => p.Name).ToList();

            return lstPupilOfClassBO;
        }

        public int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }

        private List<PhysicalExaminationViewModel> GetDataPhysicalExamination(List<PupilOfClassBO> lstPOC, int ClassID, int MonthSelected)
        {
            List<PhysicalExaminationViewModel> lstResult = new List<PhysicalExaminationViewModel>();
            PhysicalExaminationViewModel objResult = null;
            List<PhysicalExaminationModel> lstPEmodel = null;
            PhysicalExaminationModel PEmodel = null;
            PupilOfClassBO objPOC = null;
            List<PhysicalExamination> lstPE = new List<PhysicalExamination>();

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objResult = new PhysicalExaminationViewModel();
                objResult.PupilID = objPOC.PupilID;
                objResult.PupilName = objPOC.PupilFullName;
                objResult.Birthday = objPOC.Birthday;
                objResult.Status = objPOC.Status;
                objResult.Gender = objPOC.Genre;
                lstPE = this.PhysicalExaminationBusiness.All.Where(p => p.PupilID == objPOC.PupilID).ToList();
                if (lstPE != null && lstPE.Count > 0)
                {
                    lstPEmodel = new List<PhysicalExaminationModel>();
                    for (int j = 0; j < lstPE.Count; j++)
                    {
                        PEmodel = new PhysicalExaminationModel();
                        PEmodel.PhysicalExaminationID = lstPE[j].PhysicalExaminationID;
                        int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;
                        int monthCount = GetMonthDifference(objResult.Birthday, DateTime.Parse("1" + "/" + MonthSelected + "/" + year));
                        string sttHeight = PhysicalExaminationBusiness.checkStandardPE(lstPE[j].Height, "H", monthCount, objResult.Gender);
                        string sttWeight = PhysicalExaminationBusiness.checkStandardPE(lstPE[j].Weight, "W", monthCount, objResult.Gender);
                        PEmodel.Weight = lstPE[j].Weight;
                        PEmodel.MessageWeight = sttWeight;
                        PEmodel.Height = lstPE[j].Height;
                        PEmodel.MessageHeight = sttHeight;
                        PEmodel.MonthID = lstPE[j].MonthID;
                        lstPEmodel.Add(PEmodel);
                    }
                    objResult.listPE = lstPEmodel;
                }
                lstResult.Add(objResult);
            }
            return lstResult;
        }

        private string GetPhysicalState(int? stt, int type)
        {
            //Chieu cao
            if (type == 1)
            {
                switch (stt.Value)
                {
                    case 1:
                        return "Cao hơn so với tuổi";
                    case 2:
                        return "Bình thường";
                    case 3:
                        return "Thấp còi độ 1";
                    case 4:
                        return "Thấp còi độ 2";
                    default:
                        return string.Empty;
                }
            }
            //Can nang
            else
            {
                switch (stt.Value)
                {
                    case 1:
                        return "Cân nặng hơn so với tuổi";
                    case 2:
                        return "Bình thường";
                    case 3:
                        return "Suy dinh dưỡng nhẹ";
                    case 4:
                        return "Suy dinh dưỡng nặng";
                    default:
                        return string.Empty;
                }
            }
        }
    }
}