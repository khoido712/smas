﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MonthlyHealthReportArea
{
    public class MonthlyHealthReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MonthlyHealthReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MonthlyHealthReportArea_default",
                "MonthlyHealthReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
