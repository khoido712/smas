/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.ComponentModel;
namespace SMAS.Web.Areas.EmployeeWorkingHistoryArea.Models
{
    public class EmployeeWorkingHistoryViewModel
    {
        [DisplayName("Employee Working History")]
        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EmployeeWorkingHistoryID { get; set; }

        [DisplayName("Employee")]
        [ScaffoldColumn(false)]
        public Nullable<int> EmployeeID { get; set; }

        [DisplayName("Supervising Dept")]
        [ScaffoldColumn(false)]
        public Nullable<int> SupervisingDeptID { get; set; }

        [DisplayName("School")]
        [ScaffoldColumn(false)]
        public Nullable<int> SchoolID { get; set; }

      								
	


      

        #region nhung truong hien tren grid

        [ResourceDisplayName("Employee_Column_FullName")]
        [UIHint("Display")]
        public string EmployeeName { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_Organization")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Organization { get; set; }


        [ResourceDisplayName("EmployeeWorkingHistory_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Resolution { get; set; }


        [ResourceDisplayName("EmployeeWorkingHistory_Label_Department")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Department { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_Position")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Position { get; set; }

       

        [ResourceDisplayName("EmployeeWorkingHistory_Label_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<System.DateTime> FromDate { get; set; }

        [ResourceDisplayName("EmployeeWorkingHistory_Label_EndDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public Nullable<System.DateTime> ToDate { get; set; }


        #endregion
    }
}


