﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeWorkingHistoryArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("EmployeeWorkingHistory_Label_Employee")]
        [UIHint("Display")]
        public string EmployeeName { get; set; }

    }
}