﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeWorkingHistoryArea
{
    public class EmployeeWorkingHistoryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeWorkingHistoryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeWorkingHistoryArea_default",
                "EmployeeWorkingHistoryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
