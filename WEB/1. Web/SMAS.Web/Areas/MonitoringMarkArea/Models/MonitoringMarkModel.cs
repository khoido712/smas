﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.MonitoringMarkArea.Models
{
    public class MonitoringMarkModel
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public int[] MarkM { get; set; }
        public int[] MarkP { get; set; }
        public int[] MarkV { get; set; }
    }
}