﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MonitoringMarkArea
{
    public class MonitoringMarkConstants
    {
        public const string ListSemester = "ListSemester";
        public const string ListEducation = "ListEducationLevel";
        public const string ListClass = "ListClass";
        public const string GridListClass = "GridListClass";
        public const string ListMarkModelHas = "ListMarkModelHas";
    }
}