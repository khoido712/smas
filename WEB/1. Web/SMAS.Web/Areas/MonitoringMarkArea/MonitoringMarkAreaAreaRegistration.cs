﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MonitoringMarkArea
{
    public class MonitoringMarkAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MonitoringMarkArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MonitoringMarkArea_default",
                "MonitoringMarkArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
