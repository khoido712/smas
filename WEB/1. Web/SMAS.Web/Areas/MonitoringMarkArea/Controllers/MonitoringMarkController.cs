﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MonitoringMarkArea;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.MonitoringMarkArea.Models;
using SMAS.Business.BusinessObject;
using System.Threading;
using System.Globalization;
using System.IO;
using SMAS.VTUtils.Excel.Export;



namespace SMAS.Web.Areas.MonitoringMarkArea.Controllers
{
    public class MonitoringMarkController : BaseController
    {
        //
        // GET: /MonitoringMarkArea/MonitoringMark/

        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public MonitoringMarkController(IMarkRecordBusiness markrecordBusiness,
                                    IClassProfileBusiness ClassProfileBusiness,
                                    IClassSubjectBusiness ClassSubjectBusiness,
                                    ISubjectCatBusiness SubjectCatBusiness,
                                    IPupilOfClassBusiness PupilOfClassBusiness,
                                    IAcademicYearBusiness AcademicYearBusiness)
        {
            this.MarkRecordBusiness = markrecordBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }


        public ActionResult Index()
        {
            GetViewData();
            return View();
        }
        GlobalInfo glo = new GlobalInfo();

        public void GetViewData()
        {
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), SystemParamsInFile.SEMESTER_I, true, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), SystemParamsInFile.SEMESTER_II, false, false));
            ViewData[MonitoringMarkConstants.ListSemester] = listSemester;

            ViewData[MonitoringMarkConstants.ListEducation] = glo.EducationLevels;
        }

        public PartialViewResult LoadViewHasMark()
        {
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), SystemParamsInFile.SEMESTER_I, true, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), SystemParamsInFile.SEMESTER_II, false, false));
            ViewData[MonitoringMarkConstants.ListSemester] = listSemester;

            ViewData[MonitoringMarkConstants.ListEducation] = glo.EducationLevels;
            return PartialView("_SearchHasMark");
        }

        public PartialViewResult LoadViewHasNoMark()
        {
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), SystemParamsInFile.SEMESTER_I, glo.Semester.Value.ToString().Equals(SystemParamsInFile.SEMESTER_I), false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), SystemParamsInFile.SEMESTER_II, glo.Semester.Value.ToString().Equals(SystemParamsInFile.SEMESTER_II), false));
            ViewData[MonitoringMarkConstants.ListSemester] = listSemester;
            ViewData[MonitoringMarkConstants.ListEducation] = glo.EducationLevels;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = glo.EducationLevels.FirstOrDefault().EducationLevelID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            IQueryable<ClassProfile> lsClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.HasValue ? new GlobalInfo().SchoolID.Value : 0, SearchInfo);
            ViewData[MonitoringMarkConstants.ListClass] = lsClass;
            return PartialView("_SearchNoHasMark");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboClassProfile(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = EducationLevelID.Value;
                SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                IQueryable<ClassProfile> lsClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.HasValue ? new GlobalInfo().SchoolID.Value : 0, SearchInfo);
                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }
        //thong ke tinh hinh nhap diem

        [ValidateAntiForgeryToken]
        public PartialViewResult GetListHasMark(int? Semester, int? EducationLevelID)
        {
            //Lay danh sach mon hoc theo khoi
            
            IDictionary<string,object> dic = new Dictionary<string,object>();
            dic["Semester"]=Semester;
            dic["EducationLevelID"] = EducationLevelID;
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            IEnumerable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dic);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = EducationLevelID.Value;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            IEnumerable<ClassProfile> lsClass = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.HasValue ? new GlobalInfo().SchoolID.Value : 0, SearchInfo);

            List<MonitoringMarkModel> listMonitoringMarkModel = new List<MonitoringMarkModel>();

            
            int countClass = 0;

            if (lsClass!=null && lsClass.Count()>0)
            {
                countClass = lsClass.Count();
            }

            int[] MarkDefault = new int[countClass];
            //gan gia tri mac dinh = 0
            MarkDefault.ToList().ForEach(u => u = 0);


            foreach (ClassSubject Subject in listClassSubject)
            {
                MonitoringMarkModel MonitoringMark = new MonitoringMarkModel();
                MonitoringMark.MarkM = MarkDefault;
                MonitoringMark.MarkP = MarkDefault;
                MonitoringMark.MarkV = MarkDefault;
                MonitoringMark.SubjectID = Subject.SubjectID;
                MonitoringMark.SubjectName = Subject.SubjectCat.DisplayName;
                int indexClass = 0;
                foreach (ClassProfile Class in lsClass)
                {
                    MonitoringMark.MarkM[indexClass] = MarkRecordBusiness.GetCountPupilMarkRecordForClass(glo.SchoolID.Value, glo.AcademicYearID.Value, EducationLevelID.Value, Class.ClassProfileID, Subject.SubjectID, "M");
                    MonitoringMark.MarkP[indexClass] = MarkRecordBusiness.GetCountPupilMarkRecordForClass(glo.SchoolID.Value, glo.AcademicYearID.Value, EducationLevelID.Value, Class.ClassProfileID, Subject.SubjectID, "P");
                    MonitoringMark.MarkV[indexClass] = MarkRecordBusiness.GetCountPupilMarkRecordForClass(glo.SchoolID.Value, glo.AcademicYearID.Value, EducationLevelID.Value, Class.ClassProfileID, Subject.SubjectID, "V");

                    indexClass++;
                }
                listMonitoringMarkModel.Add(MonitoringMark);
            }
            ViewData[MonitoringMarkConstants.GridListClass] = lsClass;
            ViewData[MonitoringMarkConstants.ListMarkModelHas] = listMonitoringMarkModel;


            return PartialView("_ListHasMark");
        }

    }
}
