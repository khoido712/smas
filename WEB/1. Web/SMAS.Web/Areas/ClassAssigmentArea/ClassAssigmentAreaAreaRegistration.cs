﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassAssigmentArea
{
    public class ClassAssigmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ClassAssigmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ClassAssigmentArea_default",
                "ClassAssigmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
