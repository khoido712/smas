/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ClassAssigmentArea.Models
{
    public class SearchViewModel
    {

        [ResourceDisplayName("ClassAssignment_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ClassAssigmentConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public Nullable<int> EducationLevelID { get; set; }

        [ResourceDisplayName("ClassAssignment_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ClassAssigmentConstants.LIST_CLASS)]
        public Nullable<int> ClassID { get; set; }

        [ResourceDisplayName("")]
        [UIHint("ListRadio")]
        [AdditionalMetadata("ViewDataKey", ClassAssigmentConstants.LIST_TYPE)]
        public Nullable<int> Type { get; set; }

    }
}