﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ClassAssigmentArea.Models
{
    public class GridEditViewModel
    {
        [ResourceDisplayName("ClassAssignment_Label_FacultyName")]
        public System.String FacultyName { get; set; }

        [ResourceDisplayName("ClassAssignment_Label_FullName")]
        public System.String FullName { get; set; }

        [ResourceDisplayName("ClassAssignment_Label_WorkAssignment")]
        public System.String AssignmentWork { get; set; }

        [ResourceDisplayName("ClassAssignment_Label_IsHeadTeacher")]
        public System.Boolean IsHeadTeacher {get; set;}

        public int TeacherID { get; set; }
        [ResourceDisplayName("ClassAssignment_Label_IsTeacher")]
        public Nullable<bool> IsTeacher { get; set; }


    }
}
