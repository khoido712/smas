/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ClassAssigmentArea.Models
{
    public class ClassAssigmentViewModel
    {
		public Nullable<int> ClassAssigmentID { get; set; }								

        [ResourceDisplayName("ClassAssignment_Label_EducationLevel")]
        public System.String EducationLevelName { get; set; }

        [ResourceDisplayName("ClassAssignment_Column_Class")]
        public int ClassID { get; set; }

        [ResourceDisplayName("ClassAssignment_Column_Class")]
        public System.String ClassName { get; set; }

         [ResourceDisplayName("ClassAssignment_Label_WorkAssignment")]
         public System.String AssignmentWork { get; set; }

         [ResourceDisplayName("ClassAssignment_Label_TeacherName")]
         public System.String TeacherName { get; set; }

         public System.String Name { get; set; }

         [ResourceDisplayName("ClassAssignment_Label_EducationLevel")]
         [ScaffoldColumn(false)]
         public int EducationLevelID { get; set; }
        [ScaffoldColumn(false)]
         public int AssignType { get; set; }
        [ScaffoldColumn(false)]
        public int? OrderClass { get; set; }
        [ResourceDisplayName("ClassAssignment_Label_IsTeacher")]
        public Nullable<bool> IsTeacher { get; set; }
    }
}


