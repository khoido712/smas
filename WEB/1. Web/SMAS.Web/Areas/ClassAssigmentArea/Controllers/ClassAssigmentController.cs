﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ClassAssigmentArea.Models;

using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ClassAssigmentArea.Controllers
{
    public class ClassAssigmentController : BaseController
    {
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEmployeeHistoryStatusBusiness EmployeeHistoryStatusBusiness;
        public ClassAssigmentController(IClassAssigmentBusiness classassigmentBusiness,
                                         IClassProfileBusiness classprofileBusiness,
                                         IEmployeeBusiness employeeBusiness, IEmployeeHistoryStatusBusiness employeeHistoryStatusBusiness, IAcademicYearBusiness academicYearBusiness)
        {
            this.ClassAssigmentBusiness = classassigmentBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.EmployeeHistoryStatusBusiness = employeeHistoryStatusBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }

        public int ASSIGN = 2;
        public int UNASSIGN = 1;
        public int ALL = 0;

        private void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            SearchInfo["SchoolID"] = global.SchoolID.Value;
            SearchInfo["Type"] = 0;
            IEnumerable<ClassAssigmentViewModel> lst = this._Search(SearchInfo);

            ViewData[ClassAssigmentConstants.LIST_CLASSASSIGMENT] = lst;


            List<EducationLevel> ListEducationLevel = global.EducationLevels.OrderBy(p => p.EducationLevelID).ToList();
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }
            ViewData[ClassAssigmentConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution");



            // Phân loại
            List<ViettelCheckboxList> ListType = new List<ViettelCheckboxList>();
            ListType.Add(new ViettelCheckboxList(
                Res.Get("TeachingAssignment_Label_Type_All"),
                "0",
                true,
                false
            ));

            ListType.Add(new ViettelCheckboxList(
                Res.Get("TeachingAssignment_Label_Type_UnAssign"),
                "1",
                false,
                false
            ));

            ListType.Add(new ViettelCheckboxList(
                Res.Get("TeachingAssignment_Label_Type_Assign"),
                "2",
                false,
                false
            ));
            ViewData[ClassAssigmentConstants.LIST_TYPE] = ListType;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            IEnumerable<ClassProfile> lst = new List<ClassProfile>();
            GlobalInfo global = new GlobalInfo();
            if (EducationLevelID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = global.AcademicYearID.Value;
                dic["AppliedLevel"] = global.AppliedLevel.Value;
                dic["EducationLevelID"] = EducationLevelID;
                lst = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).OrderByDescending(o => o.DisplayName).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["ClassProfileID"] = frm.ClassID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            SearchInfo["Type"] = frm.Type;

            List<ClassAssigmentViewModel> lstClassAssigmentBO = this._Search(SearchInfo).ToList();
            ViewData[ClassAssigmentConstants.LIST_CLASSASSIGMENT] = lstClassAssigmentBO;

            //Get view data here

            return PartialView("_List");
        }


        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(FormCollection form)
        {
            GlobalInfo global = new GlobalInfo();
            if (GetMenupermission("ClassAssigment", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (!global.IsCurrentYear)
            {
                return Json(new JsonMessage(Res.Get("ClassAssigment_Error_IsCurrentYear"), "error"));
            }
            else
            {
                int ClassID = Convert.ToInt32(form.Get("ClassID"));
                List<int> ListTeacherSchoolID = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(_globalInfo.SchoolID.Value).Select(o => o.EmployeeID).ToList();
                List<int> lstAssigmentTeacherID = new List<int>();     // Danh sách GV phụ trách
                List<string> lstWorkAssignment = new List<string>();
                List<int> lstIsTeacherID = new List<int>();            // Danh sách Gv giảng dạy

                foreach (int EmployeeID in ListTeacherSchoolID)
                {
                    string check = form.Get("check_" + EmployeeID);
                    if (check != null)
                    {
                        string WorkAssignment = form.Get("WorkAssignment_" + EmployeeID);
                        lstAssigmentTeacherID.Add(EmployeeID);
                        lstWorkAssignment.Add(WorkAssignment);
                    }
                    string checkIsTeacher = form.Get("TeacherID_" + EmployeeID);
                    if (!string.IsNullOrEmpty(checkIsTeacher))
                    {
                        lstIsTeacherID.Add(EmployeeID);
                    }
                }

                if (lstIsTeacherID.Count > 3)
                {
                    return Json(new JsonMessage(Res.Get("ClassAssigment_Error_Maximum_IsTeacher"), "error"));
                }

                ClassAssigmentBusiness.UpdateClass(global.AcademicYearID.Value, global.SchoolID.Value, ClassID, lstAssigmentTeacherID, lstWorkAssignment, lstIsTeacherID);
                ClassAssigmentBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }

        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            if (id != 0)
            {
                this.CheckPermissionForAction(id, "ClassAssigment");
                GlobalInfo global = new GlobalInfo();
                if (GetMenupermission("ClassAssigment", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_DELETE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                this.ClassAssigmentBusiness.Delete(new GlobalInfo().SchoolID.Value, id);
                this.ClassAssigmentBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else if (!new GlobalInfo().IsCurrentYear)
            {
                return Json(new JsonMessage(Res.Get("ClassAssigment_Error_IsCurrentYear"), "error"));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ClassAssigment_Error_Delete"), "error"));
            }
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ClassAssigmentViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            AcademicYear aca = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            SetViewDataPermission("ClassAssigment", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);

            IQueryable<ClassAssigment> lstClassAssigment = this.ClassAssigmentBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            IQueryable<ClassAssigmentBO> lst = from p in lstClassAssigment
                                               join q in EmployeeBusiness.All on p.TeacherID equals q.EmployeeID
                                               join r in EmployeeHistoryStatusBusiness.All on q.EmployeeID equals r.EmployeeID
                                               where r.SchoolID == p.SchoolID
                                               && p.ClassProfile.IsActive.HasValue && p.ClassProfile.IsActive.Value
                                               select new ClassAssigmentBO
                                               {
                                                   ClassID = p.ClassID,
                                                   ClassName = p.ClassProfile.DisplayName,
                                                   ClassAssigmentID = p.ClassAssigmentID,
                                                   AssignmentWork = p.AssignmentWork,
                                                   EducationLevelID = p.ClassProfile.EducationLevelID,
                                                   EducationLevelName = p.ClassProfile.EducationLevel.Resolution,
                                                   OrderClass = p.ClassProfile.OrderNumber,
                                                   TeacherName = q.FullName,
                                                   Name = q.Name,
                                                   IsActive = q.IsActive,
                                                   ToDate = r.ToDate,
                                                   IsTeacher = p.IsTeacher,
                                                   IsHeadTeacher = p.IsHeadTeacher,
                                                   TeacherID = p.TeacherID
                                               };

            List<ClassAssigmentBO> lstResult = new List<ClassAssigmentBO>();

            var lstClassAssTempt = lst.Where(x => x.IsTeacher == true && !x.IsHeadTeacher);
            foreach (var ca in lstClassAssTempt)
            {
                if (ca.IsActive == true && (ca.ToDate == null || ca.ToDate > aca.SecondSemesterEndDate))
                {

                    lstResult.Add(ca);
                }
                else
                {
                    ClassAssigmentBO temp = new ClassAssigmentBO();
                    temp.IsHeadTeacher = ca.IsHeadTeacher;
                    temp.ClassID = ca.ClassID;
                    temp.ClassName = ca.ClassName;
                    temp.ClassAssigmentID = ca.ClassAssigmentID;
                    temp.AssignmentWork = "";
                    temp.EducationLevelID = ca.EducationLevelID;
                    temp.EducationLevelName = ca.EducationLevelName;
                    temp.OrderClass = ca.OrderClass;
                    temp.TeacherName = "";
                    temp.Name = "";
                    lstResult.Add(temp);
                }
            }

            lstClassAssTempt = lst.Where(x => (!x.IsTeacher.HasValue || !x.IsTeacher.Value) && !x.IsHeadTeacher);
            foreach (var ca in lstClassAssTempt)
            {
                if (lstResult.Where(x => x.ClassID == ca.ClassID && x.TeacherID == ca.TeacherID && (x.IsTeacher == true || x.IsHeadTeacher)).Count() > 0)
                    continue;

                if (ca.IsActive == true && (ca.ToDate == null || ca.ToDate > aca.SecondSemesterEndDate))
                {
                    lstResult.Add(ca);
                }
                else
                {
                    ClassAssigmentBO temp = new ClassAssigmentBO();
                    temp.IsHeadTeacher = ca.IsHeadTeacher;
                    temp.ClassID = ca.ClassID;
                    temp.ClassName = ca.ClassName;
                    temp.ClassAssigmentID = ca.ClassAssigmentID;
                    temp.AssignmentWork = "";
                    temp.EducationLevelID = ca.EducationLevelID;
                    temp.EducationLevelName = ca.EducationLevelName;
                    temp.OrderClass = ca.OrderClass;
                    temp.TeacherName = "";
                    temp.Name = "";
                    lstResult.Add(temp);
                }
            }

            lstClassAssTempt = lst.Where(x => x.IsHeadTeacher);
            foreach (var ca in lstClassAssTempt)
            {
                if (lstResult.Where(x => x.ClassID == ca.ClassID && x.TeacherID == ca.TeacherID).Count() > 0)
                    continue;

                ClassAssigmentBO temp = new ClassAssigmentBO();
                temp.IsActive = ca.IsActive;
                temp.IsHeadTeacher = ca.IsHeadTeacher;
                temp.ClassID = ca.ClassID;
                temp.ClassName = ca.ClassName;
                temp.ClassAssigmentID = ca.ClassAssigmentID;
                temp.AssignmentWork = "";
                temp.EducationLevelID = ca.EducationLevelID;
                temp.EducationLevelName = ca.EducationLevelName;
                temp.OrderClass = ca.OrderClass;
                temp.TeacherName = "";
                temp.Name = "";
                lstResult.Add(temp);

            }

            lstResult = lstResult.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderClass).ThenBy(o => o.ClassName).ToList();

            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).Where(p => p.IsActive == true).OrderBy(p => p.EducationLevelID).ThenBy(o => o.DisplayName).ToList();

            List<ClassAssigmentViewModel> lstClassAssigmentBO = new List<ClassAssigmentViewModel>();

            int Type = SMAS.Business.Common.Utils.GetInt(SearchInfo, "Type");

            if (Type == UNASSIGN)
            {
                List<int> lstClassProfileID = new List<int>();
                List<int> lstClassID = new List<int>();

                foreach (var itemListClass in lstClassProfile)
                {
                    int ClassProID = 0;
                    ClassProID = itemListClass.ClassProfileID;
                    lstClassProfileID.Add(ClassProID);
                }

                foreach (var itemListClassAss in lstClassAssigment)
                {
                    if (itemListClassAss.Employee != null && itemListClassAss.ClassProfile != null)
                    {
                        if (itemListClassAss.Employee.IsActive && itemListClassAss.ClassProfile.IsActive.HasValue && itemListClassAss.ClassProfile.IsActive.Value)
                        {
                            int ClassAssID = 0;
                            ClassAssID = itemListClassAss.ClassID;
                            lstClassID.Add(ClassAssID);
                        }
                    }
                }

                List<int> lstClass = lstClassProfileID.Except(lstClassID).ToList();

                foreach (var itemlstClass in lstClass)
                {
                    ClassProfile classprofile = ClassProfileBusiness.Find(itemlstClass);
                    ClassAssigmentViewModel ClassAssigmentBO = new ClassAssigmentViewModel();
                    ClassAssigmentBO.ClassID = itemlstClass;
                    ClassAssigmentBO.ClassName = classprofile.DisplayName;
                    ClassAssigmentBO.ClassAssigmentID = null;
                    ClassAssigmentBO.Name = null;
                    ClassAssigmentBO.TeacherName = null;
                    ClassAssigmentBO.AssignmentWork = null;
                    ClassAssigmentBO.EducationLevelName = classprofile.EducationLevel.Resolution;
                    ClassAssigmentBO.EducationLevelID = classprofile.EducationLevelID;
                    ClassAssigmentBO.OrderClass = classprofile.OrderNumber;
                    ClassAssigmentBO.AssignType = 1;
                    lstClassAssigmentBO.Add(ClassAssigmentBO);
                }

            }

            if (Type == ASSIGN)
            {
                foreach (var itemAssign in lstResult)
                {
                    if (itemAssign.IsActive)
                    {
                        ClassAssigmentViewModel ClassAssigmentBO = new ClassAssigmentViewModel();
                        ClassAssigmentBO.AssignmentWork = itemAssign.AssignmentWork;
                        ClassAssigmentBO.ClassAssigmentID = itemAssign.ClassAssigmentID;
                        ClassAssigmentBO.ClassID = itemAssign.ClassID;
                        ClassAssigmentBO.ClassName = itemAssign.ClassName;
                        ClassAssigmentBO.EducationLevelName = itemAssign.EducationLevelName;
                        ClassAssigmentBO.TeacherName = itemAssign.TeacherName;
                        ClassAssigmentBO.EducationLevelID = itemAssign.EducationLevelID;
                        ClassAssigmentBO.OrderClass = itemAssign.OrderClass;
                        ClassAssigmentBO.AssignType = 2;
                        ClassAssigmentBO.Name = itemAssign.Name;
                        ClassAssigmentBO.IsTeacher = itemAssign.IsTeacher;
                        lstClassAssigmentBO.Add(ClassAssigmentBO);
                    }
                }
            }

            if (Type == ALL)
            {
                List<int> lstClassProfileID = new List<int>();
                List<int> lstClassID = new List<int>();

                foreach (var itemListClass in lstClassProfile)
                {
                    int ClassProID = 0;
                    ClassProID = itemListClass.ClassProfileID;
                    lstClassProfileID.Add(ClassProID);
                }

                foreach (var itemListClassAss in lstClassAssigment)
                {
                    if (itemListClassAss.Employee != null && itemListClassAss.ClassProfile != null)
                    {
                        if (itemListClassAss.Employee.IsActive && itemListClassAss.ClassProfile.IsActive.HasValue && itemListClassAss.ClassProfile.IsActive.Value)
                        {
                            int ClassAssID = 0;
                            ClassAssID = itemListClassAss.ClassID;
                            lstClassID.Add(ClassAssID);
                        }
                    }
                }

                List<int> lstClass = lstClassProfileID.Except(lstClassID).ToList();

                foreach (var itemlstClass in lstClass)
                {
                    ClassProfile classprofile = ClassProfileBusiness.Find(itemlstClass);
                    ClassAssigmentViewModel ClassAssigmentBO = new ClassAssigmentViewModel();
                    ClassAssigmentBO.ClassID = itemlstClass;
                    ClassAssigmentBO.ClassName = classprofile.DisplayName;
                    ClassAssigmentBO.Name = null;
                    ClassAssigmentBO.ClassAssigmentID = null;
                    ClassAssigmentBO.TeacherName = null;
                    ClassAssigmentBO.AssignmentWork = null;
                    ClassAssigmentBO.EducationLevelName = classprofile.EducationLevel.Resolution;
                    ClassAssigmentBO.EducationLevelID = classprofile.EducationLevelID;
                    ClassAssigmentBO.OrderClass = classprofile.OrderNumber;
                    ClassAssigmentBO.AssignType = 0;
                    lstClassAssigmentBO.Add(ClassAssigmentBO);
                }

                foreach (var itemAssign in lstResult)
                {
                    if (itemAssign.IsActive)
                    {
                        ClassAssigmentViewModel ClassAssigmentBO = new ClassAssigmentViewModel();
                        ClassAssigmentBO.AssignmentWork = itemAssign.AssignmentWork;
                        ClassAssigmentBO.ClassAssigmentID = itemAssign.ClassAssigmentID;
                        ClassAssigmentBO.ClassID = itemAssign.ClassID;
                        ClassAssigmentBO.ClassName = itemAssign.ClassName;
                        ClassAssigmentBO.EducationLevelName = itemAssign.EducationLevelName;
                        ClassAssigmentBO.EducationLevelID = itemAssign.EducationLevelID;
                        ClassAssigmentBO.TeacherName = itemAssign.TeacherName;
                        ClassAssigmentBO.Name = itemAssign.Name;
                        ClassAssigmentBO.OrderClass = itemAssign.OrderClass;
                        ClassAssigmentBO.IsTeacher = itemAssign.IsTeacher;
                        if (itemAssign.TeacherName.Length <= 0)
                        {
                            ClassAssigmentBO.AssignType = 1;
                        }
                        lstClassAssigmentBO.Add(ClassAssigmentBO);
                    }
                }
            }

            lstClassAssigmentBO = lstClassAssigmentBO.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderClass).ThenBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.TeacherName).ToList();
            return lstClassAssigmentBO;
        }


        public PartialViewResult GetGridEdit(int? ClassID)
        {
            IQueryable<Employee> IQListTeacher = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value);

            IDictionary<string, object> IDictionary = new Dictionary<string, object>();
            IDictionary["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            IDictionary["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            IDictionary["ClassID"] = ClassID;

            List<ClassAssigment> ListClassAssigment = this.ClassAssigmentBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, IDictionary)
                                                                                    .Where(x => !x.IsHeadTeacher).ToList();

            List<GridEditViewModel> lstGridEdit = new List<GridEditViewModel>();
            List<Employee> ListTeacher = IQListTeacher.ToList();
            ListTeacher = ListTeacher.OrderBy(o => o.SchoolFaculty.FacultyName).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            foreach (var item in ListTeacher)
            {
                GridEditViewModel gridedit = new GridEditViewModel();
                var checkList = ListClassAssigment.Where(x => x.TeacherID == item.EmployeeID).ToList();

                if (checkList.Count() > 0)
                {
                    gridedit.FacultyName = item.SchoolFaculty != null ? item.SchoolFaculty.FacultyName : string.Empty;
                    gridedit.FullName = item.FullName;
                    gridedit.TeacherID = item.EmployeeID;
                    gridedit.AssignmentWork = checkList[0].AssignmentWork;
                    gridedit.IsTeacher = checkList.Where(x => x.IsTeacher.HasValue && x.IsTeacher.Value == true).FirstOrDefault() != null ?
                                         checkList.Where(x => x.IsTeacher.HasValue && x.IsTeacher.Value == true).FirstOrDefault().IsTeacher : false;
                    lstGridEdit.Add(gridedit);
                }
                else
                {
                    gridedit.FacultyName = item.SchoolFaculty != null ? item.SchoolFaculty.FacultyName : string.Empty;
                    gridedit.FullName = item.FullName;
                    gridedit.TeacherID = item.EmployeeID;
                    gridedit.AssignmentWork = null;
                    lstGridEdit.Add(gridedit);
                }
            }

            if (ClassID.HasValue && ClassID != 0)
            {
                ClassProfile classProfile = this.ClassProfileBusiness.Find(ClassID);
                string ClassName = classProfile.DisplayName;
                ViewData["ClassName"] = ClassName;
            }

            ViewData["ListClassAssigment"] = ListClassAssigment;
            return PartialView("_Edit", lstGridEdit);
        }
    }
}



