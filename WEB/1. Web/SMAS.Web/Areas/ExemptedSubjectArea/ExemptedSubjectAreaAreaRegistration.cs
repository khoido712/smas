﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExemptedSubjectArea
{
    public class ExemptedSubjectAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExemptedSubjectArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExemptedSubjectArea_default",
                "ExemptedSubjectArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
