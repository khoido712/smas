﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExemptedSubjectArea
{
    public class ExemptedSubjectConstants
    {
        public const string LIST_EXEMPTED_SUBJECT = "List_Exempted_Subject";
        public const string LIST_EXEMPTED_OBJECT = "List_Exempted_object";
        public const string LIST_EXEMPTED_SCOPE = "List_Exempted_Scope";
        public const string LIST_EXEMPTED_TYPE_WITHOUT_OTHER = "List_Exempted_Type_WithoutOther";
        public const string LIST_EXEMPTED_TYPE = "List_Exempted_Type";
        public const string LIST_EXEMPTED_TYPE_PRACTICE = "list_exempted_type_practice";
        public const string LIST_EDUCATION_LEVEL = "List_Education_Level";
        public const string LIST_CLASS_PROFILE = "List_Class_Profile";
        public const string LIST_PUPIL_PROFILE = "List_Pupil_Profile";
        public const string LIST_SUBJECT_CAT = "List_Subject_Cat";
        public const string LIST_GRID_EXEMPTED_SUBJECT = "List_Grid_Exempted_Subject";
    }
}