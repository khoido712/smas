﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ExemptedSubjectArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Filter;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ExemptedSubjectArea.Controllers
{
    public class ExemptedSubjectController : Controller
    {
        private readonly IExemptedSubjectBusiness ExemptedSubjectBu;
        private readonly IClassProfileBusiness ClassProfileBu;
        private readonly ISubjectCatBusiness SubjectCatBu;
        private readonly IPupilProfileBusiness PupilProfileBu;
        private readonly IExemptedObjectTypeBusiness ExemptedObjectType;
        private readonly IPupilOfClassBusiness PupilOfClassBu;
        private readonly IClassSubjectBusiness ClassSubjectBu;
        private readonly IAcademicYearBusiness AcademicYearBu;

        public ExemptedSubjectController(IExemptedSubjectBusiness exemptedSubjectBu, IClassProfileBusiness classProfileBu, ISubjectCatBusiness subjectCatBu, IPupilProfileBusiness pupilProfileBu, IExemptedObjectTypeBusiness exemptedObjectType, IPupilOfClassBusiness pupilOfClassBu, IClassSubjectBusiness classSubjectBu, IAcademicYearBusiness academicYearBu)
        {
            this.ExemptedSubjectBu = exemptedSubjectBu;
            this.ClassProfileBu = classProfileBu;
            this.SubjectCatBu = subjectCatBu;
            this.PupilProfileBu = pupilProfileBu;
            this.ExemptedObjectType = exemptedObjectType;
            this.PupilOfClassBu = pupilOfClassBu;
            this.ClassSubjectBu = classSubjectBu;
            this.AcademicYearBu = academicYearBu;
        }

        public ActionResult Index()
        {
            GlobalInfo glo = new GlobalInfo();

            if (!UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, 0))
                return View();

            ViewData["HasHeadTeacherPermission"] = true;
            GetViewData();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = glo.AppliedLevel.Value;
            dic["AcademicYearID"] = glo.AcademicYearID;
            ViewData[ExemptedSubjectConstants.LIST_EXEMPTED_SUBJECT] = SearchData(dic);
            return View();
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Create(ExemptedSubjectCreateModel model, FormCollection form)
        {
            GlobalInfo glo = new GlobalInfo();

            if (!UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, model.CreateClassID))
                return Json(new JsonMessage(Res.Get("Common_Label_HasHeadTeacherPermissionError")), JsonMessage.ERROR);

            if (!glo.IsCurrentYear)
                return Json(new JsonMessage(Res.Get("Common_IsCurrentYear_Err")), JsonMessage.ERROR);

            if (ModelState.IsValid && glo.AcademicYearID.HasValue)
            {
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject.Add("ClassID", model.CreateClassID);
                var lstSubject = this.ClassSubjectBu.SearchBySchool(glo.SchoolID.Value, dicClassSubject).ToList();

                List<ExemptedSubject> lstExemptedSubject = new List<ExemptedSubject>();
                int curYear = (int)this.AcademicYearBu.Find(glo.AcademicYearID.Value).Year;
                foreach (var subject in lstSubject)
                {
                    string first = form.Get("FirstSemesterExemptType_" + subject.SubjectID);
                    string second = form.Get("SecondSemesterExemptType_" + subject.SubjectID);

                    int? firstValue = first != null && first.Trim() != "[Lựa chọn]" && first.Trim() != "" ? Convert.ToByte(first) : new Nullable<int>();
                    int? secondValue = second != null && second.Trim() != "[Lựa chọn]" && second.Trim() != "" ? Convert.ToByte(second) : new Nullable<int>();

                    //bool checkSubject = subject.SubjectCat.DisplayName.Contains("GDQP")  //La mon giao duc quoc phong
                    //                    && (
                    //                        (firstValue.HasValue && firstValue.Value != SystemParamsInFile.EXEMPT_TYPE_PRACTICE) //Ky 1 chon khong phai la kieu thuc hanh
                    //                        || (secondValue.HasValue && secondValue.Value != SystemParamsInFile.EXEMPT_TYPE_PRACTICE) //Hoac ky 2 chon khong phai thuc hanh
                    //                    ); //Thi bao loi
                    //if (!checkSubject)
                    //    return Json(new JsonMessage(Res.Get("ExemptedSubject_Lable_GDQPPracticeError"), JsonMessage.ERROR));

                    if (firstValue.HasValue || secondValue.HasValue)
                    {
                        ExemptedSubject exemptedSubject = new ExemptedSubject();
                        exemptedSubject.AcademicYearID = glo.AcademicYearID.Value;
                        exemptedSubject.ClassID = model.CreateClassID;
                        exemptedSubject.Description = string.Empty;
                        exemptedSubject.EducationLevelID = model.CreateEducationLevelID;
                        exemptedSubject.ExemptedObjectID = model.CreateExemptedObjectID;
                        exemptedSubject.FirstSemesterExemptType = firstValue;
                        exemptedSubject.PupilID = model.CreatePupilID;
                        exemptedSubject.SchoolID = glo.SchoolID.Value;
                        exemptedSubject.SecondSemesterExemptType = secondValue;
                        exemptedSubject.SubjectID = subject.SubjectID;
                        exemptedSubject.Year = curYear;
                        lstExemptedSubject.Add(exemptedSubject);
                    }
                }
                if (lstExemptedSubject.Count > 0)
                {
                    ActionAuditDataBO objAC = new ActionAuditDataBO();
                    this.ExemptedSubjectBu.UpdateExemptedSubject(glo.UserAccountID, model.CreatePupilID, model.CreateClassID, model.CreateExemptedObjectID, lstExemptedSubject,ref objAC);
                    this.ExemptedSubjectBu.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("ExemptedSubject_Label_ChooseSubject"), JsonMessage.ERROR));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Update(ExemptedSubjectUpdateModel model, FormCollection form)
        {
            GlobalInfo glo = new GlobalInfo();

            if (!UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, model.UpdateClassID))
                return Json(new JsonMessage(Res.Get("Common_Label_HasHeadTeacherPermissionError")), JsonMessage.ERROR);

            if (!glo.IsCurrentYear)
                return Json(new JsonMessage(Res.Get("Common_IsCurrentYear_Err")), JsonMessage.ERROR);

            if (ModelState.IsValid && glo.AcademicYearID.HasValue)
            {
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject.Add("ClassID", model.UpdateClassID);
                var lstSubject = this.ClassSubjectBu.SearchBySchool(glo.SchoolID.Value, dicClassSubject).ToList();

                List<ExemptedSubject> lstExemptedSubject = new List<ExemptedSubject>();
                int curYear = (int)this.AcademicYearBu.Find(glo.AcademicYearID.Value).Year;
                foreach (var subject in lstSubject)
                {
                    string first = form.Get("FirstSemesterExemptType_" + subject.SubjectID);
                    string second = form.Get("SecondSemesterExemptType_" + subject.SubjectID);

                    int? firstValue = first != null && first.Trim() != "[Lựa chọn]" && first.Trim() != "" ? Convert.ToByte(first) : new Nullable<int>();
                    int? secondValue = second != null && second.Trim() != "[Lựa chọn]" && second.Trim() != "" ? Convert.ToByte(second) : new Nullable<int>();

                    if (firstValue.HasValue || secondValue.HasValue)
                    {
                        ExemptedSubject exemptedSubject = new ExemptedSubject();
                        exemptedSubject.AcademicYearID = glo.AcademicYearID.Value;
                        exemptedSubject.ClassID = model.UpdateClassID;
                        exemptedSubject.Description = string.Empty;
                        exemptedSubject.EducationLevelID = model.UpdateEducationLevelID;
                        exemptedSubject.ExemptedObjectID = model.UpdateExemptedObjectID;
                        exemptedSubject.FirstSemesterExemptType = firstValue;
                        exemptedSubject.PupilID = model.UpdatePupilID;
                        exemptedSubject.SchoolID = glo.SchoolID.Value;
                        exemptedSubject.SecondSemesterExemptType = secondValue;
                        exemptedSubject.SubjectID = subject.SubjectID;
                        exemptedSubject.Year = curYear;
                        lstExemptedSubject.Add(exemptedSubject);
                    }
                }
                ActionAuditDataBO objAC = new ActionAuditDataBO();
                this.ExemptedSubjectBu.UpdateExemptedSubject(glo.UserAccountID, model.UpdatePupilID, model.UpdateClassID, model.UpdateExemptedObjectID, lstExemptedSubject,ref objAC);
                this.ExemptedSubjectBu.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        
        public PartialViewResult Search(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = glo.AppliedLevel.Value;
            if (model.ClassID.HasValue) dic["ClassID"] = model.ClassID.Value;
            if (model.EducationLevelID.HasValue) dic["EducationLevelID"] = model.EducationLevelID.Value;
            if (!string.IsNullOrEmpty(model.FullName)) dic["PupilName"] = model.FullName.Trim();
            if (!string.IsNullOrEmpty(model.PupilCode)) dic["PupilCode"] = model.PupilCode.Trim();

            dic["AcademicYearID"] = glo.AcademicYearID;

            if (model.ExemptScope.HasValue)
            {
                if (model.ExemptScope.Value == SystemParamsInFile.EXEMPT_SCOPE_FISRT_SEMESTER || model.ExemptScope.Value == SystemParamsInFile.EXEMPT_SCOPE_FULL_YEAR)
                    dic.Add("HasFirstSemester", true);

                if (model.ExemptScope.Value == SystemParamsInFile.EXEMPT_SCOPE_SECOND_SEMESTER || model.ExemptScope.Value == SystemParamsInFile.EXEMPT_SCOPE_FULL_YEAR)
                    dic.Add("HasSecondSemester", true);
            }
            ViewData[ExemptedSubjectConstants.LIST_EXEMPTED_SUBJECT] = SearchData(dic);
            return PartialView("_List");
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eid, int? type)
        {
            if (type.HasValue && type.Value > 0 && eid <= 0)
                return Json(new List<ComboObject>());

            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = glo.AcademicYearID.Value;
            dicClass["AppliedLevel"] = glo.AppliedLevel.Value;

            if (eid > 0)
                dicClass.Add("EducationLevelID", eid);

            if (!glo.IsAdminSchoolRole)
            {
                dicClass.Add("UserAccountID", glo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEADTEACHER);
            }

            var lstClass = this.ClassProfileBu
                                .SearchBySchool(glo.SchoolID.Value, dicClass)
                                .OrderBy(u => u.EducationLevelID)
                                .ThenBy(u => u.DisplayName)
                                .ToList()
                                .Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName))
                                .ToList();
            return Json(lstClass);
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadPupil(int cid)
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ClassID", cid);
            dic.Add("Check", "Check");
            dic.Add("Status", SystemParamsInFile.PUPIL_STATUS_STUDYING);
            var lstPupil = this.PupilOfClassBu.SearchBySchool(glo.SchoolID.Value, dic).Select(u => new { u.PupilProfile.FullName, u.PupilID })
                                .ToList()
                                .Select(u => new ComboObject { key = u.PupilID.ToString(), value = u.FullName })
                                .ToList();
            return Json(lstPupil);
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(int? pid, int cid)
        {
            GlobalInfo glo = new GlobalInfo();

            ViewData[ExemptedSubjectConstants.LIST_EXEMPTED_TYPE] = CommonList.ExemptType();
            ViewData[ExemptedSubjectConstants.LIST_EXEMPTED_TYPE_PRACTICE] = CommonList.ExemptType().Where(u => u.key == SystemParamsInFile.EXEMPT_TYPE_PRACTICE.ToString()).ToList();
            ViewData[ExemptedSubjectConstants.LIST_EXEMPTED_TYPE_WITHOUT_OTHER] = CommonList.ExemptType().Where(u => u.key != SystemParamsInFile.EXEMPT_TYPE_OTHER.ToString()).ToList();

            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject.Add("ClassID", cid);
            dicClassSubject.Add("IsExemptible", true);

            var lstSubject = this.ClassSubjectBu.SearchBySchool(glo.SchoolID.Value, dicClassSubject).ToList();
            List<GridExemptedSubjectModel> lstExemptedSubjectModel = new List<GridExemptedSubjectModel>();

            if (pid.HasValue && pid.Value > 0)//Is update
            {
                IDictionary<string, object> dicExemptedSubject = new Dictionary<string, object>();
                dicExemptedSubject.Add("ClassID", cid);
                dicExemptedSubject.Add("PupilID", pid.Value);
                dicExemptedSubject["AppliedLevel"] = glo.AppliedLevel.Value;
                dicExemptedSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
                var lstExemptedSubject = this.ExemptedSubjectBu.SearchBySchool(glo.SchoolID.Value, dicExemptedSubject).ToList();

                foreach (var subject in lstSubject)
                {
                    GridExemptedSubjectModel item = new GridExemptedSubjectModel();
                    var exemptedSubject = lstExemptedSubject.Where(u => u.SubjectID == subject.SubjectID).SingleOrDefault();
                    item.SubjectName = subject.SubjectCat.SubjectName;
                    item.SubjectCatID = subject.SubjectID;
                    item.DisplayName = subject.SubjectCat.DisplayName;
                    if (exemptedSubject != null)
                    {
                        item.ExemptedSubjectID = exemptedSubject.ExemptedSubjectID;
                        item.FirstSemesterExemptType = exemptedSubject.FirstSemesterExemptType;
                        item.SecondSemesterExemptType = exemptedSubject.SecondSemesterExemptType;
                    }
                    item.HasPractice = subject.SubjectCat.HasPractice;
                    lstExemptedSubjectModel.Add(item);
                }
            }
            else //Is Create
            {
                foreach (var subject in lstSubject)
                {
                    GridExemptedSubjectModel item = new GridExemptedSubjectModel();
                    item.SubjectName = subject.SubjectCat.SubjectName;
                    item.SubjectCatID = subject.SubjectID;
                    item.HasPractice = subject.SubjectCat.HasPractice;
                    item.DisplayName = subject.SubjectCat.DisplayName;
                    lstExemptedSubjectModel.Add(item);
                }
            }

            return PartialView("_gridSubject", lstExemptedSubjectModel);
        }

        private IEnumerable<ExemptedSubjectViewModel> SearchData(IDictionary<string, object> searchInfo)
        {
            GlobalInfo glo = new GlobalInfo();
            var data = this.ExemptedSubjectBu
                        .SearchBySchool(glo.SchoolID.Value, searchInfo)
                        .Select(u => new ExemptedSubjectViewModel
                        {
                            ExemptedObjectID = u.ExemptedObjectID,
                            EducationLevelID = u.EducationLevelID,
                            EducationLevelName = u.EducationLevel.Resolution,
                            ExemptedSubjectID = u.ExemptedSubjectID,
                            ClassID = u.ClassID,
                            ClassName = u.ClassProfile.DisplayName,
                            PupilID = u.PupilID,
                            FullName = u.PupilProfile.FullName,
                            PupilCode = u.PupilProfile.PupilCode,
                            SubjectName = u.SubjectCat.SubjectName,
                            FirstSemesterExemptType = u.FirstSemesterExemptType,
                            SecondSemesterExemptType = u.SecondSemesterExemptType,
                            FirstSemesterExemptTypeName = string.Empty,
                            SecondSemesterExemptTypeName = string.Empty
                        }).ToList();

            var lstSemesterExemptType = CommonList.ExemptType();
            foreach (var item in data)
            {
                item.FirstSemesterExemptTypeName = GetSemesterExemptTypeName(item.FirstSemesterExemptType, lstSemesterExemptType);
                item.SecondSemesterExemptTypeName = GetSemesterExemptTypeName(item.SecondSemesterExemptType, lstSemesterExemptType);
                item.EnableEdit = glo.IsCurrentYear && UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, item.ClassID);
            }
            return data;
        }

        private string GetSemesterExemptTypeName(int? semesterExemptType, List<ComboObject> lst)
        {
            if (!semesterExemptType.HasValue) return string.Empty;
            var value = lst.Where(u => u.key.Equals(semesterExemptType.Value.ToString())).FirstOrDefault();
            return value != null ? value.value : string.Empty;
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();

            var lstExemptedScope = CommonList.ExemptScope();
            ViewData[ExemptedSubjectConstants.LIST_EXEMPTED_SCOPE] = new SelectList(lstExemptedScope, "key", "value");

            var lstEducationLevel = glo.EducationLevels.Select(u => new ComboObject(u.EducationLevelID.ToString(), u.Resolution)).ToList();
            ViewData[ExemptedSubjectConstants.LIST_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "key", "value");

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = glo.AcademicYearID.Value;
            dicClass["AppliedLevel"] = glo.AppliedLevel.Value;

            if (!glo.IsAdminSchoolRole)
            {
                dicClass.Add("UserAccountID", 0);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEADTEACHER);
            }

            var lstClass = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(u => u.EducationLevelID).ThenBy(u => u.DisplayName).ToList();
            ViewData[ExemptedSubjectConstants.LIST_CLASS_PROFILE] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            ViewData[ExemptedSubjectConstants.LIST_EXEMPTED_TYPE] = CommonList.ExemptType();

            var lstExemptedObject = this.ExemptedObjectType.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            ViewData[ExemptedSubjectConstants.LIST_EXEMPTED_OBJECT] = new SelectList(lstExemptedObject, "ExemptedObjectTypeID", "Resolution");
        }
    }
}
