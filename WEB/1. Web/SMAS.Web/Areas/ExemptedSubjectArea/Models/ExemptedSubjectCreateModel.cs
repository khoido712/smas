using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ExemptedSubjectArea.Models
{
    public class ExemptedSubjectCreateModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExemptedSubjectConstants.LIST_EDUCATION_LEVEL)]
        public int CreateEducationLevelID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ClassID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int CreateClassID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_PupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int CreatePupilID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ExemptedObjectID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExemptedSubjectConstants.LIST_EXEMPTED_OBJECT)]
        public int CreateExemptedObjectID { get; set; }
    }
}
