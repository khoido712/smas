using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SMAS.Web.Areas.ExemptedSubjectArea.Models
{
    public class GridExemptedSubjectModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_ExemptedSubjectID")]
        public int ExemptedSubjectID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SubjectCatID")]
        public int SubjectCatID { get; set; }

        public string SubjectName { get; set; }

        public string DisplayName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SecondSemesterExemptType")]
        public int? FirstSemesterExemptType { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_SecondSemesterExemptType")]
        public int? SecondSemesterExemptType { get; set; }

        public bool HasPractice { get; set; }
    }
}
