using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using SMAS.Models.Models;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ExemptedSubjectArea.Models
{
    public class ExemptedSubjectUpdateModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int UpdateEducationLevelID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        public string UpdateEducationLevel { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ClassID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int UpdateClassID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ClassID")]
        public string UpdateClassName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_PupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int UpdatePupilID { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_PupilID")]
        public string UpdateFullName { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_ExemptedObjectID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExemptedSubjectConstants.LIST_EXEMPTED_OBJECT)]
        public int UpdateExemptedObjectID { get; set; }
    }
}
