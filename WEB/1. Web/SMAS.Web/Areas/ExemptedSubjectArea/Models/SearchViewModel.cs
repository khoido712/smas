using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ExemptedSubjectArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_ExemptScope")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExemptedSubjectConstants.LIST_EXEMPTED_SCOPE)]
        public int? ExemptScope { get; set; }

        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExemptedSubjectConstants.LIST_EDUCATION_LEVEL)]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("ClassProfile_Column_ClassProfileID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExemptedSubjectConstants.LIST_CLASS_PROFILE)]
        public int? ClassID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string FullName { get; set; }
    }
}
