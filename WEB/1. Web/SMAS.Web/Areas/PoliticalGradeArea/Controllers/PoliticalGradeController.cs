﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Areas.PoliticalGradeArea.Models;

namespace SMAS.Web.Areas.PoliticalGradeArea.Controllers
{
    // TODO : HieuND, Chua xu li phan phan quyen, bo phan SkipCheckRole
    public class PoliticalGradeController : BaseController
    {
        private readonly IPoliticalGradeBusiness PoliticalGradeBusiness;

        public PoliticalGradeController(IPoliticalGradeBusiness politicalgradeBusiness)
        {
            this.PoliticalGradeBusiness = politicalgradeBusiness;
        }

        //
        // GET: /PoliticalGrade/
        public ActionResult Index()
        {
            SetViewDataPermission("PoliticalGrade", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here

            IEnumerable<PoliticalGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[PoliticalGradeConstants.LIST_POLITICALGRADE] = lst;
            return View();
        }

        //
        // GET: /PoliticalGrade/Search
        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
            
            IEnumerable<PoliticalGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[PoliticalGradeConstants.LIST_POLITICALGRADE] = lst;

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            PoliticalGrade politicalgrade = new PoliticalGrade();
            TryUpdateModel(politicalgrade);
            Utils.Utils.TrimObject(politicalgrade);

            this.PoliticalGradeBusiness.Insert(politicalgrade);
            this.PoliticalGradeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int PoliticalGradeID)
        {
            PoliticalGrade politicalgrade = this.PoliticalGradeBusiness.Find(PoliticalGradeID);
            TryUpdateModel(politicalgrade);
            Utils.Utils.TrimObject(politicalgrade);
            this.PoliticalGradeBusiness.Update(politicalgrade);
            this.PoliticalGradeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.PoliticalGradeBusiness.Delete(id);
            this.PoliticalGradeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PoliticalGradeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<PoliticalGrade> query = this.PoliticalGradeBusiness.Search(SearchInfo);
            IQueryable<PoliticalGradeViewModel> lst = query.Select(o => new PoliticalGradeViewModel
            {
                PoliticalGradeID = o.PoliticalGradeID,
                Resolution = o.Resolution,
                Description = o.Description
            });
            return lst.OrderBy(o=>o.Resolution).ToList();
        }
    }
}






