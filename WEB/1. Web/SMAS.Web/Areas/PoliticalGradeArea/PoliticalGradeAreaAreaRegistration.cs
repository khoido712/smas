﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PoliticalGradeArea
{
    public class PoliticalGradeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PoliticalGradeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PoliticalGradeArea_default",
                "PoliticalGradeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
