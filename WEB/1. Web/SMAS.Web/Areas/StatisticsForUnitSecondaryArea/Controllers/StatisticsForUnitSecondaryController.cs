﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Web.Areas.StatisticsForUnitSecondaryArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System.IO;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.StatisticsForUnitSecondaryArea.Controllers
{
    public class StatisticsForUnitSecondaryController : BaseController
    {
        IStatisticsForUnitBusiness StatisticsForUnitBusiness;
        IStatisticsConfigBusiness StatisticsConfigBusiness;
        IMarkStatisticBusiness MarkStatisticBusiness;
        ICapacityStatisticBusiness CapacityStatisticBusiness;
        IConductStatisticBusiness ConductStatisticBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        ISubjectCatBusiness SubjectCatBusiness;
        IAcademicYearBusiness AcademicYearBusiness;

        public StatisticsForUnitSecondaryController(IStatisticsForUnitBusiness statisticsForUnitBusiness, 
                                                    IStatisticsConfigBusiness statisticsConfigBusiness, 
                                                    IMarkStatisticBusiness markStatisticBusiness,
                                                    ICapacityStatisticBusiness capacityStatisticBusiness,
                                                    IConductStatisticBusiness conductStatisticBusiness,
                                                    IEducationLevelBusiness educationLevelBusiness,
                                                    ISubjectCatBusiness subjectCatBusiness,
                                                    IAcademicYearBusiness academicYearBusiness)
        {
            this.StatisticsForUnitBusiness = statisticsForUnitBusiness;
            this.StatisticsConfigBusiness = statisticsConfigBusiness;
            this.MarkStatisticBusiness = markStatisticBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            ViewData[StatisticsForUnitSecondaryConstants.LIST_EDUCATION_LEVEL] = new SelectList(glo.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[StatisticsForUnitSecondaryConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
            ViewData[StatisticsForUnitSecondaryConstants.LIST_SEMESTER_ALL] = new SelectList(CommonList.SemesterAndAll(), "key", "value");
            ViewData[StatisticsForUnitSecondaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitSecondaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            ViewData[StatisticsForUnitSecondaryConstants.LIST_JUDGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            ViewData[StatisticsForUnitSecondaryConstants.LIST_MARK] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            ViewData[StatisticsForUnitSecondaryConstants.LIST_CONDUCT_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);
            ViewData[StatisticsForUnitSecondaryConstants.LIST_CAPACITY_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);
        }

        #region CAPACITY

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchCapacity(StatisticsForUnitSecondaryViewModel model)
        {
            ViewData[StatisticsForUnitSecondaryConstants.LIST_CAPACITY_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<CapacityStatisticsBO> listData = StatisticsForUnitBusiness.SearchCapacityForSecondary(sfu);
            listData.ForEach(ms => ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID));

            ViewData[StatisticsForUnitSecondaryConstants.GRID_STATISTICS_CAPACITY] = listData;
            ViewData[StatisticsForUnitSecondaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listData.Count);

            return PartialView("_CapacityGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendCapacity(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<CapacityStatisticsBO> listData = StatisticsForUnitBusiness.SearchCapacityForSecondary(sfu);
                CapacityStatisticBusiness.SendAll(listData);
                CapacityStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitSecond_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportCapacity(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateCapacityForSecondaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region CAPACITY SUBJECT

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchCapacitySubject(StatisticsForUnitSecondaryViewModel model)
        {
            ViewData[StatisticsForUnitSecondaryConstants.LIST_JUDGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            ViewData[StatisticsForUnitSecondaryConstants.LIST_MARK] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<CapacityStatisticsBO> listCapacitySubjectStatistic = StatisticsForUnitBusiness.SearchSubjectCapacityForSecondary(sfu);

            listCapacitySubjectStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            listCapacitySubjectStatistic = listCapacitySubjectStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            ViewData[StatisticsForUnitSecondaryConstants.GRID_STATISTICS_CAPACITY_SUBJECT] = listCapacitySubjectStatistic;
            ViewData[StatisticsForUnitSecondaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listCapacitySubjectStatistic.Count);

            return PartialView("_CapacitySubjectGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendCapacitySubject(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<CapacityStatisticsBO> data = StatisticsForUnitBusiness.SearchSubjectCapacityForSecondary(sfu);
                CapacityStatisticBusiness.SendAll(data);
                CapacityStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitSecond_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportCapacitySubject(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateSubjectCapacityForSecondaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion
         
        #region CONDUCT

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchConduct(StatisticsForUnitSecondaryViewModel model)
        {
            ViewData[StatisticsForUnitSecondaryConstants.LIST_CONDUCT_LEVEL] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<ConductStatisticsBO> listData = StatisticsForUnitBusiness.SearchConductForSecondary(sfu);
            listData.ForEach(ms => ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID));

            ViewData[StatisticsForUnitSecondaryConstants.GRID_STATISTICS_CONDUCT] = listData;
            ViewData[StatisticsForUnitSecondaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listData.Count);

            return PartialView("_ConductGrid", model);       
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendConduct(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<ConductStatisticsBO> listData = StatisticsForUnitBusiness.SearchConductForSecondary(sfu);
                ConductStatisticBusiness.SendAll(listData);
                ConductStatisticBusiness.Save();

                return Json(new JsonMessage(Res.Get("StatisticsForUnitSecond_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportConduct(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateConductForSecondaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region PERIOD

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchPeriod(StatisticsForUnitSecondaryViewModel model)
        {
            ViewData[StatisticsForUnitSecondaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitSecondaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<MarkStatisticBO> listMarkStatistic = StatisticsForUnitBusiness.SearchPeriodicMarkForSecondary(sfu);

            listMarkStatistic.ForEach(ms => {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[StatisticsForUnitSecondaryConstants.GRID_STATISTICS_PERIOD] = listMarkStatistic;
            ViewData[StatisticsForUnitSecondaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_PeriodGrid", model);        
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendPeriod(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<MarkStatisticBO> data = StatisticsForUnitBusiness.SearchPeriodicMarkForSecondary(sfu);
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitSecond_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportPeriod(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreatePeriodicMarkForSecondaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region SEMESTER

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchSemester(StatisticsForUnitSecondaryViewModel model)
        {
            ViewData[StatisticsForUnitSecondaryConstants.LIST_BELOW_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            ViewData[StatisticsForUnitSecondaryConstants.LIST_ABOVE_AVERAGE] = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            GlobalInfo glo = new GlobalInfo();
            StatisticsForUnitBO sfu = new StatisticsForUnitBO();
            sfu.AcademicYearID = glo.AcademicYearID.Value;
            sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
            sfu.SchoolID = glo.SchoolID.Value;
            sfu.Semester = (int)model.Semester;
            sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

            List<MarkStatisticBO> listMarkStatistic = StatisticsForUnitBusiness.SearchSemesterMarkForSecondary(sfu);

            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu: Khoi Hoc, Mon hoc (Fix #0016137)
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.SubjectCat.OrderInSubject)
                .ThenBy(o => o.SubjectCat.DisplayName).ToList();

            ViewData[StatisticsForUnitSecondaryConstants.GRID_STATISTICS_SEMESTER] = listMarkStatistic;
            ViewData[StatisticsForUnitSecondaryConstants.COUNT_DATA] = string.Format(Res.Get("Common_Label_CountResult"), listMarkStatistic.Count);

            return PartialView("_SemesterGrid", model);
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult SendSemester(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                List<MarkStatisticBO> data = StatisticsForUnitBusiness.SearchSemesterMarkForSecondary(sfu);
                MarkStatisticBusiness.SendAll(data);
                MarkStatisticBusiness.Save();
                return Json(new JsonMessage(Res.Get("StatisticsForUnitSecond_Label_SendSuc")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
        }

        public FileStreamResult ExportSemester(StatisticsForUnitSecondaryViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                StatisticsForUnitBO sfu = new StatisticsForUnitBO();
                sfu.AcademicYearID = glo.AcademicYearID.Value;
                sfu.AppliedLevel = SystemParamsInFile.EDUCATION_GRADE_SECONDARY;
                sfu.SchoolID = glo.SchoolID.Value;
                sfu.Semester = (int)model.Semester;
                sfu.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : (int)0;

                string fileName = string.Empty;
                Stream stream = StatisticsForUnitBusiness.CreateSemesterMarkForSecondaryReport(sfu, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
