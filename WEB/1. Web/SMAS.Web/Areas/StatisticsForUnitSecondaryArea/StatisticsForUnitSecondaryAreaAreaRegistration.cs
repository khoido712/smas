﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticsForUnitSecondaryArea
{
    public class StatisticsForUnitSecondaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticsForUnitSecondaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticsForUnitSecondaryArea_default",
                "StatisticsForUnitSecondaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
