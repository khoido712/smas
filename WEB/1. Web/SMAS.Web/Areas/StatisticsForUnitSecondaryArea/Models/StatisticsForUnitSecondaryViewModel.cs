using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticsForUnitSecondaryArea.Models
{
    public class StatisticsForUnitSecondaryViewModel
    {
        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("Common_Label_Semester")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int Semester { get; set; }
    }
}
