﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticalSemesterMarkOfPrimarySchoolArea
{
    public class StatisticalSemesterMarkOfPrimarySchoolAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticalSemesterMarkOfPrimarySchoolArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticalSemesterMarkOfPrimarySchoolArea_default",
                "StatisticalSemesterMarkOfPrimarySchoolArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}