﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.StatisticalSemesterMarkOfPrimarySchoolArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalSemesterMarkOfPrimarySchoolConstant.LISTACADEMICYEAR)]
        [AdditionalMetadata("Placeholder", "None")]
        [AdditionalMetadata("OnChange", "AjaxAcademicYearChange(this)")]
        public int? AcademicYear { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalSemesterMarkOfPrimarySchoolConstant.LISTSEMESTER)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxSemesterChange(this)")]
        public int? Semester { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalSemesterMarkOfPrimarySchoolConstant.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "Choice")]
        [AdditionalMetadata("OnChange", "AjaxEducationLevelChange(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_TrainingType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalSemesterMarkOfPrimarySchoolConstant.LISTTRAININGTYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxTrainingTypeChange(this)")]
        public int? TrainingType { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalSemesterMarkOfPrimarySchoolConstant.LISTSUBJECT)]
        [AdditionalMetadata("Placeholder", "Choice")]
        [AdditionalMetadata("OnChange", "AjaxSubjectChange(this)")]
        public int? Subject { get; set; }
    }
}