﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PrimaryJudgeRecordArea
{
    public class PrimaryJudgeRecordAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PrimaryJudgeRecordArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PrimaryJudgeRecordArea_default",
                "PrimaryJudgeRecordArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}