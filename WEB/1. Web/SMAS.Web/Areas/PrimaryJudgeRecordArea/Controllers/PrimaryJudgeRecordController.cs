﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.PrimaryJudgeRecordArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System.Text;
using System.Configuration;

namespace SMAS.Web.Areas.PrimaryJudgeRecordArea.Controllers
{
    public class PrimaryJudgeRecordController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IVJudgeRecordBusiness VJudgeRecordBusiness;
        private readonly IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly ILockMarkPrimaryBusiness LockMarkPrimaryBusiness;
        private readonly IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness;
        public PrimaryJudgeRecordController(IJudgeRecordBusiness judgerecordBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , ITeachingAssignmentBusiness TeachingAssignmentBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IPupilProfileBusiness PupilProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , ISummedUpRecordBusiness SummedUpRecordBusiness
            , IExemptedSubjectBusiness ExemptedSubjectBusiness
            , IProcessedReportBusiness ProcessedReportBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ISchoolMovementBusiness SchoolMovementBusiness
            , IClassMovementBusiness ClassMovementBusiness
            , IPupilLeavingOffBusiness PupilLeavingOffBusiness
            , IMarkTypeBusiness MarkTypeBusiness
            , IVJudgeRecordBusiness VJudgeRecordBusiness
            , IVSummedUpRecordBusiness VSummedUpRecordBusiness
            , IMarkRecordBusiness MarkRecordBusiness
            , ILockMarkPrimaryBusiness LockMarkPrimaryBusiness
            , IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness
            )
        {
            this.PupilLeavingOffBusiness = PupilLeavingOffBusiness;
            this.ClassMovementBusiness = ClassMovementBusiness;
            this.JudgeRecordBusiness = judgerecordBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.VSummedUpRecordBusiness = VSummedUpRecordBusiness;
            this.VJudgeRecordBusiness = VJudgeRecordBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.LockMarkPrimaryBusiness = LockMarkPrimaryBusiness;
            this.JudgeRecordHistoryBusiness = JudgeRecordHistoryBusiness;
        }

        //
        // GET: /PrimaryJudgeRecord/

        public ActionResult Index()
        {
            GlobalInfo global = new GlobalInfo();
            SetViewData();
            return View();
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<JudgeRecordViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<JudgeRecord> query = this.JudgeRecordBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            IQueryable<JudgeRecordViewModel> lst = query.Select(o => new JudgeRecordViewModel
            {
                JudgeRecordID = o.JudgeRecordID,
                PupilID = o.PupilID,
                ClassID = o.ClassID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                SubjectID = o.SubjectID,
                MarkTypeID = o.MarkTypeID,
                Year = o.CreatedAcademicYear,
                Semester = o.Semester,
                Judgement = o.Judgement,
                ReTestJudgement = o.ReTestJudgement,
                CreatedDate = o.CreatedDate,
                ModifiedDate = o.ModifiedDate
            });

            return lst.ToList();
        }

        #region setviewData

        private void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            if (!global.Semester.HasValue)
            {
                throw new BusinessException("Common_Error_InternalError");
            }

            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("JudgeRecord_Semester_Of_Year_First"), 1, global.Semester == 1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("JudgeRecord_Semester_Of_Year_Second"), 2, global.Semester == 2, false));
            ViewData[JudgeRecordConstants.LIST_SEMESTER] = listSemester;

            //ba dao =)) -namta
            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();
            int i = 0;
            if (global.EducationLevels != null)
            {
                foreach (EducationLevel item in global.EducationLevels)
                {
                    i++;
                    bool check = false;
                    if (i == 1) check = true;
                    listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
                }
            }
            ViewData[JudgeRecordConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                IQueryable<ClassProfile> listClass = getClassFromEducationLevel(global.EducationLevels.FirstOrDefault().EducationLevelID);
                ViewData[JudgeRecordConstants.LIST_CLASS] = listClass.ToList();
            }
            else
            {
                ViewData[JudgeRecordConstants.LIST_CLASS] = null;
            }

            //list subject
            ViewData[JudgeRecordConstants.LIST_SUBJECT] = null;

            //Get view data here
            List<JudgeRecordViewModel> lst = new List<JudgeRecordViewModel>();
            ViewData[JudgeRecordConstants.LIST_JUDGERECORD] = lst;

            ViewData[JudgeRecordConstants.ListTitle] = "";

            ViewData[JudgeRecordConstants.HIDDEN_J15_J25_GRID] = false;
            ViewData[JudgeRecordConstants.DISABLE_SEMESTER_COLUMN] = 1;
            ViewData[JudgeRecordConstants.ENABLE_BUTTON] = false;
            ViewData[JudgeRecordConstants.ENABLE_BUTTON_EXCEL] = false;
            ViewData[JudgeRecordConstants.VISIBLE_COLUMN2] = false;

            ViewData[JudgeRecordConstants.HAS_ERROR_DATA] = false;
            ViewData[JudgeRecordConstants.LIST_IMPORTDATA] = null;

            ViewData[JudgeRecordConstants.CLASS_12] = false;
            ViewData[JudgeRecordConstants.SEMESTER_1] = true;
            ViewData[JudgeRecordConstants.ERROR_BASIC_DATA] = false;
            ViewData[JudgeRecordConstants.ERROR_IMPORT_MESSAGE] = "";
        }
        #endregion setviewData

        #region _GetClassFromEducationLevel

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult _GetClassFromEducationLevel(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> listClass = getClassFromEducationLevel(EducationLevelID.Value);
                ViewData[JudgeRecordConstants.LIST_CLASS] = listClass.ToList();
            }
            ViewData[JudgeRecordConstants.ListTitle] = "";
            return PartialView("_ListClass");
        }

        #endregion _GetClassFromEducationLevel

        #region Lay thong tin lop hoc, mon hoc

        public IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = global.AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;

            //Hungnd 17/04/2013 Edit permission of teacher
            if (!global.IsAdminSchoolRole && !global.IsViewAll)
            {
                dic["UserAccountID"] = global.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            IQueryable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            listClass = listClass.OrderBy(o => o.DisplayName);
            return listClass;
        }

        public IQueryable<ClassSubject> getSubjectFromClass(int ClassID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            dic["IsCommenting"] = SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
            IQueryable<ClassSubject> listSubject = ClassSubjectBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return listSubject;
        }

        #endregion Lay thong tin lop hoc, mon hoc

        #region _GetSubjectFromClass

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult _GetSubjectFromClass(int? ClassID, int? SemesterID)
        {
            GlobalInfo global = new GlobalInfo();
            if (ClassID.HasValue)
            {
                List<ClassSubject> listSubject = new List<ClassSubject>();
                if (global.AcademicYearID.HasValue && global.SchoolID.HasValue && ClassID.HasValue && SemesterID.HasValue)
                {
                    listSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(global.UserAccountID,
                                        global.AcademicYearID.Value, global.SchoolID.Value
                                        , SemesterID.Value, ClassID.Value, global.IsViewAll)
                                 .Where(o => o.SubjectCat.IsActive == true && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                 .OrderBy(o => o.SubjectCat.OrderInSubject)
                                 .ThenBy(o => o.SubjectCat.SubjectName)
                                 .ToList();
                }

                if (SemesterID.Value == 1)
                {
                    listSubject = listSubject.Where(o => o.SectionPerWeekFirstSemester > 0).ToList();
                }
                else
                    if (SemesterID.Value == 2)
                    {
                        listSubject = listSubject.Where(o => o.SectionPerWeekSecondSemester > 0).ToList();
                    }
                List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();

                foreach (ClassSubject item in listSubject)
                {
                    listSJ.Add(new ViettelCheckboxList(item.SubjectCat.DisplayName, item.SubjectID, false, false));
                }

                ViewData[JudgeRecordConstants.LIST_SUBJECT] = listSJ;
            }
            ViewData[JudgeRecordConstants.ENABLE_BUTTON] = false;
            ViewData[JudgeRecordConstants.ENABLE_BUTTON_EXCEL] = false;
            ViewData[JudgeRecordConstants.ListTitle] = "";
            return PartialView("_ListSubject");
        }

        #endregion _GetSubjectFromClass

        #region LoadDataToGrid

        private List<JudgeRecordViewModel> GetData(int? SubjectID, int? EducationlevelID, int? ClassID, int? SemesterID)
        {
            int Year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;
            GlobalInfo global = new GlobalInfo();
            if (!(SubjectID.HasValue && EducationlevelID.HasValue && ClassID.HasValue && SemesterID.HasValue))
                throw new BusinessException("Common_Error_InternalError");

            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID);

            //-	Nếu là lớp thuộc khối 1,2 thì cột J15 và J25 (tương ứng với đánh dấu 13, 14 trên màn hình)
            ViewData[JudgeRecordConstants.HIDDEN_J15_J25_GRID] = EducationlevelID.Value == 1 || EducationlevelID.Value == 2;
            ViewData[JudgeRecordConstants.DISABLE_SEMESTER_COLUMN] = SemesterID == 1 ? 1 : 2;

            List<PupilOfClass> lsPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value
                                                                                    , new Dictionary<string, object>() {
                                                                                            {"EducationLevelID",EducationlevelID},
                                                                                            {"ClassID",ClassID},
                                                                                            {"AcademicYearID",global.AcademicYearID.Value},
                                                                                            {"SchoolID",global.SchoolID.Value}
                                                                                        })
                                                               .OrderBy(u => u.OrderInClass)
                                                               .ThenBy(u => u.PupilProfile.Name)
                                                               .ToList();

            List<ExemptedSubject> lsExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID.Value, SemesterID.Value).ToList();

            List<JudgeRecordViewModel> lsViewModel = new List<JudgeRecordViewModel>();

            List<JudgeRecordBO> lsJRBO = VJudgeRecordBusiness.SearchJudgeRecordNoView(new Dictionary<string, object>()
                                                                              {
                                                                                {"ClassID",ClassID},
                                                                                {"SchoolID",global.SchoolID.Value},
                                                                                {"AcademicYearID",global.AcademicYearID},
                                                                                {"SubjectID",SubjectID},
                                                                                {"Year",Year}
                                                                              })
                                                                          .ToList();

            List<SummedUpRecordBO> lsSUR = VSummedUpRecordBusiness.SearchSummedUpRecord(new Dictionary<string, object>()
                                                                            {
                                                                                {"SchoolID",_globalInfo.SchoolID},
                                                                                {"AcademicYearID",global.AcademicYearID.Value},
                                                                                {"ClassID",ClassID.Value},
                                                                                {"SubjectID",SubjectID},
                                                                                {"Semester",SemesterID},
                                                                                {"Year",Year},
                                                                                {"IsCommenting", SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE }
                                                                            }).ToList();

            foreach (var item in lsPOC)
            {
                JudgeRecordViewModel jrwm = new JudgeRecordViewModel();
                jrwm.AcademicYearID = global.AcademicYearID.Value;
                jrwm.ClassID = ClassID;
                jrwm.FullName = item.PupilProfile.FullName;
                jrwm.Name = item.PupilProfile.Name;
                jrwm.PupilCode = item.PupilProfile.PupilCode;
                jrwm.OrderInClass = item.OrderInClass;
                jrwm.PupilID = item.PupilID;
                jrwm.SchoolID = item.SchoolID;
                jrwm.SubjectID = SubjectID;
                jrwm.Status = item.Status;
                jrwm.ExemptedSuject = lsExempteds.Any(u => u.PupilID == item.PupilID && u.SubjectID == SubjectID);

                List<JudgeRecordBO> lsJRofPupil = lsJRBO.Where(u => u.PupilID == item.PupilID).ToList();
                List<SummedUpRecordBO> lsSurOfPupil = lsSUR.Where(u => u.PupilID == item.PupilID).ToList();

                if (jrwm.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || jrwm.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                {
                    #region cac con diem
                    foreach (var order in lsJRofPupil)
                    {
                        if (order.Semester == 1)
                        {
                            switch (order.OrderNumber)
                            {
                                case 1:
                                    jrwm.H11 = order.Judgement.Length == 1;
                                    break;

                                case 2:
                                    jrwm.H12 = order.Judgement.Length == 1;
                                    break;

                                case 3:
                                    jrwm.H13 = order.Judgement.Length == 1;
                                    break;

                                case 4:
                                    jrwm.H14 = order.Judgement.Length == 1;
                                    break;

                                case 5:
                                    jrwm.H15 = order.Judgement.Length == 1;
                                    break;

                                default:
                                    break;
                            }
                        }
                        else
                        {
                            switch (order.OrderNumber)
                            {
                                case 1:
                                    jrwm.H21 = order.Judgement.Length == 1;
                                    break;

                                case 2:
                                    jrwm.H22 = order.Judgement.Length == 1;
                                    break;

                                case 3:
                                    jrwm.H23 = order.Judgement.Length == 1;
                                    break;

                                case 4:
                                    jrwm.H24 = order.Judgement.Length == 1;
                                    break;

                                case 5:
                                    jrwm.H25 = order.Judgement.Length == 1;
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    #endregion

                    #region diem tong ket hoc ky

                    if (lsSurOfPupil.Count() > 0)
                    {
                        if (SemesterID == 1)
                        {
                            string temp = lsSurOfPupil.FirstOrDefault().JudgementResult;
                            if (temp == "A+")
                            {
                                jrwm.MarkSemester1 = 1;
                            }
                            else if (temp == "A")
                            {
                                jrwm.MarkSemester1 = 2;
                            }
                            else if (temp == "B")
                            {
                                jrwm.MarkSemester1 = 3;
                            }
                            else
                            {
                                jrwm.MarkSemester1 = 0;
                            }
                        }
                        if (SemesterID == 2)
                        {
                            string temp = lsSurOfPupil.FirstOrDefault().JudgementResult;
                            if (temp == "A+")
                            {
                                jrwm.MarkSemester2 = 1;
                            }
                            else if (temp == "A")
                            {
                                jrwm.MarkSemester2 = 2;
                            }
                            else if (temp == "B")
                            {
                                jrwm.MarkSemester2 = 3;
                            }
                            else
                            {
                                jrwm.MarkSemester2 = 0;
                            }
                        }
                    }
                    #endregion lấy điểm tổng kết từ sumuprecord
                }
                else //chuyen truong,chuyen lop,thoi hoc
                {
                    DateTime dt = new DateTime();
                    if (item.EndDate.HasValue)
                    {
                        dt = item.EndDate.Value;
                    }

                    bool showMarkHK1 = DateTime.Compare(dt, acaYear.FirstSemesterEndDate.Value) >= 0;
                    bool showMarkHK2 = DateTime.Compare(dt, acaYear.SecondSemesterEndDate.Value) >= 0;

                    foreach (var order in lsJRofPupil)
                    {
                        if (order.Semester == 1)
                        {
                            switch (order.OrderNumber)
                            {
                                case 1:
                                    jrwm.H11 = showMarkHK1 && order.Judgement.Length == 1;
                                    break;

                                case 2:
                                    jrwm.H12 = showMarkHK1 && order.Judgement.Length == 1;
                                    break;

                                case 3:
                                    jrwm.H13 = showMarkHK1 && order.Judgement.Length == 1;
                                    break;

                                case 4:
                                    jrwm.H14 = showMarkHK1 && order.Judgement.Length == 1;
                                    break;

                                case 5:
                                    jrwm.H15 = showMarkHK1 && order.Judgement.Length == 1;
                                    break;

                                default:
                                    break;
                            }
                        }
                        else
                        {
                            switch (order.OrderNumber)
                            {
                                case 1:
                                    jrwm.H21 = showMarkHK2 && order.Judgement.Length == 1;
                                    break;

                                case 2:
                                    jrwm.H22 = showMarkHK2 && order.Judgement.Length == 1;
                                    break;

                                case 3:
                                    jrwm.H23 = showMarkHK2 && order.Judgement.Length == 1;
                                    break;

                                case 4:
                                    jrwm.H24 = showMarkHK2 && order.Judgement.Length == 1;
                                    break;

                                case 5:
                                    jrwm.H25 = showMarkHK2 && order.Judgement.Length == 1;
                                    break;

                                default:
                                    break;
                            }
                        }
                    }

                    #region lấy điểm tổng kết từ sumuprecord
                    if (lsSurOfPupil.Count() > 0)
                    {
                        if (SemesterID == 1 && showMarkHK1)
                        {
                            string temp = lsSurOfPupil.FirstOrDefault().JudgementResult;
                            if (temp == "A+")
                            {
                                jrwm.MarkSemester1 = 1;
                            }
                            else if (temp == "A")
                            {
                                jrwm.MarkSemester1 = 2;
                            }
                            else if (temp == "B")
                            {
                                jrwm.MarkSemester1 = 3;
                            }
                            else
                            {
                                jrwm.MarkSemester1 = 0;
                            }
                        }
                        if (SemesterID == 2 && showMarkHK2)
                        {
                            string temp = lsSurOfPupil.FirstOrDefault().JudgementResult;
                            if (temp == "A+")
                            {
                                jrwm.MarkSemester2 = 1;
                            }
                            else if (temp == "A")
                            {
                                jrwm.MarkSemester2 = 2;
                            }
                            else if (temp == "B")
                            {
                                jrwm.MarkSemester2 = 3;
                            }
                            else
                            {
                                jrwm.MarkSemester2 = 0;
                            }
                        }
                    }
                    #endregion lấy điểm tổng kết từ sumuprecord
                }
                lsViewModel.Add(jrwm);
            }
            return lsViewModel;
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        
        public PartialViewResult LoadDataToGrid(int? SubjectID, int? EducationlevelID, int? ClassID, int? SemesterID)
        {
            ViewData[JudgeRecordConstants.HIDDEN_J15_J25_GRID] = false;
            ViewData[JudgeRecordConstants.DISABLE_SEMESTER_COLUMN] = 1;
            ViewData[JudgeRecordConstants.ENABLE_BUTTON] = false;
            ViewData[JudgeRecordConstants.ENABLE_BUTTON_EXCEL] = false;
            ViewData[JudgeRecordConstants.VISIBLE_COLUMN2] = false;

            ViewData[JudgeRecordConstants.HAS_ERROR_DATA] = false;
            ViewData[JudgeRecordConstants.LIST_IMPORTDATA] = null;

            ViewData[JudgeRecordConstants.CLASS_12] = false;
            ViewData[JudgeRecordConstants.SEMESTER_1] = true;
            ViewData[JudgeRecordConstants.ERROR_BASIC_DATA] = false;
            ViewData[JudgeRecordConstants.ERROR_IMPORT_MESSAGE] = "";

            GlobalInfo global = new GlobalInfo();

            string LockTitle = MarkRecordBusiness.GetLockMarkTitlePrimary(global.SchoolID.Value, global.AcademicYearID.Value, ClassID.Value, SubjectID.Value, SemesterID.Value).Replace("_", "");

            ViewData[JudgeRecordConstants.LOCK_TITLE] = !string.IsNullOrWhiteSpace(LockTitle) ? LockTitle.Substring(0, LockTitle.Length - 1) : string.Empty;

            List<JudgeRecordViewModel> lsViewModel = GetData(SubjectID, EducationlevelID, ClassID, SemesterID);
            var listData = lsViewModel.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            ViewData[JudgeRecordConstants.LIST_JUDGERECORD] = listData;
            ViewData[JudgeRecordConstants.ListTitle] = Res.Get("SummedUpRecord_Label_LabelGrid") + " LỚP "
                + ClassProfileBusiness.Find(ClassID.Value).DisplayName.ToUpper()
                + " - " + SubjectCatBusiness.Find(SubjectID).DisplayName.ToUpper();
            Session["ClassID"] = ClassID;

            if (SemesterID == 1)
            {
                ViewData[JudgeRecordConstants.VISIBLE_COLUMN2] = false;
            }
            else if (SemesterID == 2)
            {
                ViewData[JudgeRecordConstants.VISIBLE_COLUMN2] = true;
            }

            //check quyền
            //UserInfo.HasSubjectTeacherPermission(UserInfo.UserAccountID, ClassID, SubjectID):
            bool IsTeacherSubjectPermiss = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, ClassID.Value, SubjectID.Value, SemesterID.GetValueOrDefault());
            bool IsAdminSchool = global.IsAdminSchoolRole;
            bool IsInSemester = false;
            AcademicYear aca = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            if (SemesterID == 1)
            {
                IsInSemester = (DateTime.Compare(DateTime.Now, aca.FirstSemesterEndDate.Value) <= 0) && (DateTime.Compare(DateTime.Now, aca.FirstSemesterStartDate.Value) >= 0);
            }
            else if (SemesterID == 2)
            {
                IsInSemester = (DateTime.Compare(DateTime.Now, aca.SecondSemesterEndDate.Value) <= 0) && (DateTime.Compare(DateTime.Now, aca.SecondSemesterStartDate.Value) >= 0);
            }

            ViewData[JudgeRecordConstants.IS_TEACHER_PERMISS] = IsTeacherSubjectPermiss && (IsAdminSchool || IsInSemester);
            ViewData[JudgeRecordConstants.SEMESTER] = SemesterID;

            // Tạo dữ liệu ghi log
            ClassProfile ClassProfile = ClassProfileBusiness.Find(ClassID);
            SubjectCat subject = SubjectCatBusiness.Find(SubjectID);
            SetViewDataActionAudit(String.Empty, String.Empty, ClassID.ToString(), "View judge_record class_id: " + ClassID + ", subject_id: " + SubjectID + ", semester: " + SemesterID, "class_id: " + ClassID + ", subject_id: " + SubjectID + ", semester: " + SemesterID, "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_VIEW, "Xem sổ điểm lớp " + ClassProfile.DisplayName + " học kì " + SemesterID + " môn học " + subject.SubjectName);

            return PartialView("_List");
        }

        #endregion LoadDataToGrid

        #region SubmitData

        

        [ValidateAntiForgeryToken]
        public JsonResult Create(Dictionary<int, List<int>> ListData, int? Semester, int? EducationLevelID, int? SubjectID, int? ClassID, string LockTitle)
        {
            if (!(Semester.HasValue && EducationLevelID.HasValue && ClassID.HasValue))
            {
                return Json(new JsonMessage(Res.Get("Common_Error_InternalError"), "error"));
            }
            // Kiem tra du lieu khoa tu client truyen len co khop voi server khong
            LockMarkPrimaryBusiness.CheckCurrentLockMark(LockTitle, ClassID.Value, SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
            GlobalInfo global = new GlobalInfo();
            List<JudgeRecord> lsJR = new List<JudgeRecord>();
            List<SummedUpRecord> lssur = new List<SummedUpRecord>();
            AcademicYear ay = AcademicYearBusiness.Find(global.AcademicYearID.Value);

            List<MarkType> lstMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", global.AppliedLevel.Value } }).ToList();
            MarkType markTypeC1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.C1);

            List<int> hk1;
            List<int> hk2;

            if (EducationLevelID == 1 || EducationLevelID == 2)
            {
                hk1 = new List<int>() { 11, 12, 13, 14 };
                hk2 = new List<int>() { 21, 22, 23, 24 };
            }
            else
            {
                hk1 = new List<int>() { 11, 12, 13, 14, 15 };
                hk2 = new List<int>() { 21, 22, 23, 24, 25 };
            }

            // Tạo data ghi log
            List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(ay.SchoolID, new Dictionary<string, object> { { "AcademicYearID", ay.AcademicYearID }, { "ClassID", ClassID.Value }, { "Check", "Check" } }).ToList();
            SubjectCat subject = SubjectCatBusiness.Find(SubjectID.Value);
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            StringBuilder inforLog = null;
            int iLog = 0;
            foreach (KeyValuePair<int, List<int>> pair in ListData)
            {
                int id = pair.Key;
                // Tạo dữ liệu ghi log
                PupilOfClass pop = lstPOC.Where(p => p.PupilID == id).FirstOrDefault();
                objectIDStr.Append(id);
                descriptionStr.Append("Update judge record pupil_id:" + id);
                paramsStr.Append("pupil_id:" + id);
                userFuntionsStr.Append("Sổ điểm");
                userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                inforLog = new StringBuilder();
                inforLog.Append("Cập nhật điểm cho " + pop.PupilProfile.FullName);
                inforLog.Append(" mã " + pop.PupilProfile.PupilCode);
                inforLog.Append(" lớp " + pop.ClassProfile.DisplayName);
                inforLog.Append(" môn " + subject.SubjectName);
                inforLog.Append(" học kì " + Semester.Value + " ");
                inforLog.Append(" năm " + pop.Year.Value + "-" + (pop.Year.Value + 1) + " ");
                userDescriptionsStr.Append(inforLog.ToString());
                //List<MarkRecord> listOldRecord = pop.PupilProfile.MarkRecords.ToList();// chỗ này ko tốn performance vì bản thân lstPOC đã có thông tin điểm
                //oldObjectStr.Append("{");
                //oldObjectStr.Append("pupil_profile_id:" + id + ",");
                //for (int j = 0; j < listOldRecord.Count; j++)
                //{
                //    MarkRecord record = listOldRecord[j];
                //    oldObjectStr.Append("{MarkRecordID:" + record.MarkRecordID + "," + record.Title + ":" + record.Mark + "}");
                //    if (j < listOldRecord.Count - 1)
                //    {
                //        oldObjectStr.Append(",");
                //    }
                //}
                //oldObjectStr.Append("}");
                // end

                if (pair.Value.Count() > 1)
                {
                    //Judgement: lấy từ grid

                    #region SummedUpRecord

                    SummedUpRecord sur = new SummedUpRecord();
                    sur.PupilID = pair.Key;
                    sur.ClassID = ClassID.Value;
                    sur.SubjectID = (int)SubjectID.Value;
                    sur.Semester = (int?)Semester;
                    sur.IsCommenting = SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE;
                    sur.AcademicYearID = global.AcademicYearID.Value;
                    sur.SchoolID = global.SchoolID.Value;
                    if (ay != null)
                    {
                        sur.CreatedAcademicYear = ay.Year;
                    }
                    List<int> lsData = (List<int>)pair.Value;
                    for (int i = 1; i < lsData.Count(); i++)
                    {
                        if (lsData[i] == 1)
                        {
                            sur.JudgementResult = Res.Get("Common_Label_MarkAPlus");
                            break;
                        }
                        else if (lsData[i] == 2)
                        {
                            sur.JudgementResult = Res.Get("Common_Label_MarkA");
                            break;
                        }
                        else if (lsData[i] == 3)
                        {
                            sur.JudgementResult = Res.Get("Common_Label_MarkB");
                            break;
                        }
                    }
                    lssur.Add(sur);

                    #endregion SummedUpRecord

                    #region JudgeRecord

                    if (Semester == 1)
                    {
                        for (int i = 0; i < hk1.Count(); i++)
                        {
                            JudgeRecord jr = new JudgeRecord();
                            jr.PupilID = pair.Key;
                            jr.ClassID = ClassID.Value;
                            jr.SubjectID = (int)SubjectID.Value;
                            jr.Semester = (int?)Semester;
                            jr.MarkTypeID = markTypeC1.MarkTypeID;
                            jr.Title = SystemParamsInFile.C1;
                            jr.AcademicYearID = global.AcademicYearID.Value;
                            jr.SchoolID = global.SchoolID.Value;
                            jr.MarkedDate = DateTime.Now;
                            if (ay != null)
                            {
                                jr.CreatedAcademicYear = ay.Year;
                            }
                            // Chỉ lưu các nhận xét được tích chọn
                            if (lsData.Contains(hk1[i]))
                            {
                                jr.Judgement = GlobalConstants.PASS;
                                jr.OrderNumber = (int)(hk1[i] - 10);
                                lsJR.Add(jr);
                            }
                            //else
                            //{
                            //    jr.Judgement = GlobalConstants.NOPASS;
                            //    jr.OrderNumber = (int)(hk1[i] - 10);
                            //}

                        }
                    }
                    else if (Semester == 2)
                    {
                        for (int i = 0; i < hk2.Count(); i++)
                        {
                            JudgeRecord jr = new JudgeRecord();
                            jr.PupilID = pair.Key;
                            jr.ClassID = ClassID.Value;
                            jr.SubjectID = (int)SubjectID.Value;
                            jr.Semester = (int?)Semester;
                            jr.MarkTypeID = markTypeC1.MarkTypeID;
                            jr.Title = SystemParamsInFile.C1;
                            jr.AcademicYearID = global.AcademicYearID.Value;
                            jr.SchoolID = global.SchoolID.Value;
                            jr.MarkedDate = DateTime.Now;
                            if (ay != null)
                            {
                                jr.CreatedAcademicYear = ay.Year;
                            }
                            // Chỉ lưu các nhận xét được tích chọn
                            if (lsData.Contains(hk2[i]))
                            {
                                jr.Judgement = "Đ";
                                jr.OrderNumber = (int)(hk2[i] - 20);
                                lsJR.Add(jr);
                            }
                            //else
                            //{
                            //    jr.Judgement = "CÐ";
                            //    jr.OrderNumber = (int)(hk2[i] - 20);
                            //}

                        }
                    }
                    #endregion JudgeRecord
                }
                // Tạo dữ liệu ghi log
                if (iLog < ListData.Count - 1)
                {
                    objectIDStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
                iLog++;
            }
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

            if (!isMovedHistory)
            {
                JudgeRecordBusiness.InsertJudgeRecordPrimary(global.UserAccountID, lsJR, lssur);
                JudgeRecordBusiness.Save();
            }
            else
            {
                List<JudgeRecordHistory> lsJRHistory = new List<JudgeRecordHistory>();
                JudgeRecordHistory objJudgeHistory = null;
                JudgeRecord objJudge = null;
                for (int i = 0; i < lsJR.Count; i++)
                {
                    objJudgeHistory = new JudgeRecordHistory();
                    objJudge = lsJR[i];
                    //objJudgeHistory.AcademicYear = objJudge.AcademicYear;
                    objJudgeHistory.AcademicYearID = objJudge.AcademicYearID;
                    objJudgeHistory.ClassID = objJudge.ClassID;
                    //objJudgeHistory.ClassProfile = objJudge.ClassProfile;
                    objJudgeHistory.CreatedAcademicYear = objJudge.CreatedAcademicYear;
                    objJudgeHistory.CreatedDate = objJudge.CreatedDate;
                    objJudgeHistory.IsOldData = objJudge.IsOldData;
                    objJudgeHistory.IsSMS = objJudge.IsSMS;
                    objJudgeHistory.Judgement = objJudge.Judgement;
                    objJudgeHistory.JudgeRecordID = objJudge.JudgeRecordID;
                    objJudgeHistory.Last2digitNumberSchool = objJudge.Last2digitNumberSchool;
                    objJudgeHistory.M_OldID = objJudge.M_OldID;
                    objJudgeHistory.M_ProvinceID = objJudge.M_ProvinceID;
                    objJudgeHistory.MarkedDate = objJudge.MarkedDate;
                    //objJudgeHistory.MarkType = objJudge.MarkType;
                    objJudgeHistory.MarkTypeID = objJudge.MarkTypeID;
                    objJudgeHistory.MJudgement = objJudge.MJudgement;
                    objJudgeHistory.ModifiedDate = objJudge.ModifiedDate;
                    objJudgeHistory.MSourcedb = objJudge.MSourcedb;
                    objJudgeHistory.OldJudgement = objJudge.OldJudgement;
                    objJudgeHistory.OrderNumber = objJudge.OrderNumber;
                    objJudgeHistory.PupilID = objJudge.PupilID;
                    //objJudgeHistory.PupilProfile = objJudge.PupilProfile;
                    objJudgeHistory.ReTestJudgement = objJudge.ReTestJudgement;
                    objJudgeHistory.SchoolID = objJudge.SchoolID;
                    //objJudgeHistory.SchoolProfile = objJudge.SchoolProfile;
                    objJudgeHistory.Semester = objJudge.Semester;
                    //objJudgeHistory.SubjectCat = objJudge.SubjectCat;
                    objJudgeHistory.SubjectID = objJudge.SubjectID;
                    objJudgeHistory.SynchronizeID = objJudge.SynchronizeID;
                    objJudgeHistory.Title = objJudge.Title;
                    lsJRHistory.Add(objJudgeHistory);
                }

                List<SummedUpRecordHistory> lssurHistory = new List<SummedUpRecordHistory>();
                SummedUpRecordHistory objSumHistory = null;
                SummedUpRecord objSum = null;
                for (int j = 0; j < lssur.Count; j++)
                {
                    objSumHistory = new SummedUpRecordHistory();
                    objSum = lssur[j];
                    //objSumHistory.AcademicYear = objSum.AcademicYear;
                    objSumHistory.AcademicYearID = objSum.AcademicYearID;
                    objSumHistory.ClassID = objSum.ClassID;
                    //objSumHistory.ClassProfile = objSum.ClassProfile;
                    objSumHistory.Comment = objSum.Comment;
                    objSumHistory.CreatedAcademicYear = objSum.CreatedAcademicYear;
                    objSumHistory.IsCommenting = objSum.IsCommenting;
                    objSumHistory.IsOldData = objSum.IsOldData;
                    objSumHistory.JudgementResult = objSum.JudgementResult;
                    objSumHistory.Last2digitNumberSchool = objSum.Last2digitNumberSchool;
                    objSumHistory.M_OldID = objSum.M_OldID;
                    objSumHistory.M_ProvinceID = objSum.M_ProvinceID;
                    objSumHistory.MSourcedb = objSum.MSourcedb;
                    //objSumHistory.PeriodDeclaration = objSum.PeriodDeclaration;
                    objSumHistory.PeriodID = objSum.PeriodID;
                    objSumHistory.PupilID = objSum.PupilID;
                    //objSumHistory.PupilProfile = objSum.PupilProfile;
                    objSumHistory.ReTestJudgement = objSum.ReTestJudgement;
                    objSumHistory.ReTestMark = objSum.ReTestMark;
                    objSumHistory.SchoolID = objSum.SchoolID;
                    //objSumHistory.SchoolProfile = objSum.SchoolProfile;
                    objSumHistory.Semester = objSum.Semester;
                    //objSumHistory.SubjectCat = objSum.SubjectCat;
                    objSumHistory.SubjectID = objSum.SubjectID;
                    objSumHistory.SummedUpDate = objSum.SummedUpDate;
                    objSumHistory.SummedUpMark = objSum.SummedUpMark;
                    objSumHistory.SummedUpRecordID = objSum.SummedUpRecordID;
                    objSumHistory.SynchronizeID = objSum.SynchronizeID;
                    lssurHistory.Add(objSumHistory);
                }
                JudgeRecordHistoryBusiness.InsertJudgeRecordPrimaryHistory(global.UserAccountID, lsJRHistory, lssurHistory);
                JudgeRecordHistoryBusiness.Save();
            }

            // Tạo dữ liệu ghi log
            SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateMarkSuccess")));
        }

        #endregion SubmitData

        #region ReloadButton

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult ReloadButton(int? SubjectID, int? EducationlevelID, int? ClassID, int? SemesterID)
        {
            GlobalInfo global = new GlobalInfo();
            bool IsInSemester = false;
            AcademicYear aca = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            //17/04/2013 Hungnd Fix permission for view permission
            AcademicYear ay = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            if (!(SubjectID.HasValue && EducationlevelID.HasValue && ClassID.HasValue && SemesterID.HasValue))
            {
                throw new BusinessException("Common_Error_InternalError");
            }

            List<JudgeRecordViewModel> lsViewModel = GetData(SubjectID, EducationlevelID, ClassID, SemesterID);
            ViewData[JudgeRecordConstants.ENABLE_BUTTON_EXCEL] = lsViewModel.Count() > 0;

            ViewData[JudgeRecordConstants.ENABLE_BUTTON] = false;
            if (global.IsCurrentYear) // If current year
            {
                if (SemesterID != null && SemesterID == 1)
                {
                    IsInSemester = (DateTime.Compare(DateTime.Now, aca.FirstSemesterEndDate.Value) <= 0) && (DateTime.Compare(DateTime.Now, aca.FirstSemesterStartDate.Value) >= 0);
                    if (global.IsAdminSchoolRole || (UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, ClassID.Value, SubjectID.Value, SemesterID.GetValueOrDefault()) && IsInSemester)) // If has permission
                    {
                        ViewData[JudgeRecordConstants.ENABLE_BUTTON] = true;
                    }
                }
                else if (SemesterID != null && SemesterID == 2)
                {
                    IsInSemester = (DateTime.Compare(DateTime.Now, aca.SecondSemesterEndDate.Value) <= 0) && (DateTime.Compare(DateTime.Now, aca.SecondSemesterStartDate.Value) >= 0);
                    if (global.IsAdminSchoolRole || (UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, ClassID.Value, SubjectID.Value, SemesterID.GetValueOrDefault()) && IsInSemester)) // If has permission
                    {
                        ViewData[JudgeRecordConstants.ENABLE_BUTTON] = true;
                    }
                }
            }
            else
            {
                ViewData[JudgeRecordConstants.ENABLE_BUTTON] = false;
            }

            //


            return PartialView("_Button");
        }

        #endregion ReloadButton

        #region report
        
        public JsonResult ExportExcel(JudgeRecordViewModel svm)
        {
            GlobalInfo global = new GlobalInfo();
            JudgeRecordForReportBO cbo = new JudgeRecordForReportBO();
            cbo.AcademicYearID = global.AcademicYearID.Value;
            cbo.AppliedLevel = global.AppliedLevel.Value;
            cbo.ClassID = svm.ClassID;
            cbo.EducationLevelID = svm.EducationLevel;
            cbo.FullName = svm.FullName;
            cbo.PupilCode = svm.PupilCode;
            cbo.SchoolID = global.SchoolID.Value;
            SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID.Value);

            cbo.SupervisingDeptID = sp != null ? sp.SupervisingDeptID.Value : 0;
            cbo.Semester = svm.Semester;
            cbo.SubjectID = svm.SubjectID;

            string type = JsonReportMessage.NEW;

            ProcessedReport processedReport = JudgeRecordBusiness.GetJudgeRecordBO(cbo);

            Stream excel = JudgeRecordBusiness.CreateJudgeRecordBO(cbo);
            processedReport = JudgeRecordBusiness.InsertJudgeRecordBO(cbo, excel);
            excel.Close();

            // Tạo dữ liệu ghi log
            ClassProfile classProfile = ClassProfileBusiness.Find(svm.ClassID);
            SubjectCat subject = SubjectCatBusiness.Find(svm.SubjectID);
            SetViewDataActionAudit(String.Empty, String.Empty,
                classProfile.ClassProfileID.ToString(),
                "Export excel judge record class_profile_id:" + classProfile.ClassProfileID, "ClassID: " + svm.ClassID + ", SubjectID: " + svm.SubjectID + ", SemesterID: " + svm.Semester, "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_EXPORT,
                "Xuất excel sổ điểm lớp " + classProfile.DisplayName + " HK " + svm.Semester + " năm " + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1) + " môn " + subject.SubjectName);


            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(JudgeRecordViewModel svm)
        {
            GlobalInfo global = new GlobalInfo();
            JudgeRecordForReportBO cbo = new JudgeRecordForReportBO();
            cbo.AcademicYearID = global.AcademicYearID.Value;
            cbo.AppliedLevel = global.AppliedLevel.Value;
            cbo.ClassID = svm.ClassID;
            cbo.EducationLevelID = svm.EducationLevel;
            cbo.FullName = svm.FullName;
            cbo.PupilCode = svm.PupilCode;
            cbo.SchoolID = global.SchoolID.Value;
            SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID.Value);

            cbo.SupervisingDeptID = sp != null ? sp.SupervisingDeptID.Value : 0;
            cbo.Semester = svm.Semester;
            cbo.SubjectID = svm.SubjectID;

            ProcessedReport processedReport = JudgeRecordBusiness.GetJudgeRecordBO(cbo);

            Stream excel = JudgeRecordBusiness.CreateJudgeRecordBO(cbo);
            processedReport = JudgeRecordBusiness.InsertJudgeRecordBO(cbo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> {
                SystemParamsInFile.HS_DIEMMONNHANXETCAP1
            };

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        #endregion report

        #region ImportExcel


        [ValidateAntiForgeryToken]
        
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, JudgeRecordViewModel inputData)
        {
            GlobalInfo global = new GlobalInfo();

            //bo xung them du lieu vao inputdata
            inputData.AcademicYearID = global.AcademicYearID.Value;
            inputData.SchoolID = global.SchoolID.Value;
            inputData.Year = AcademicYearBusiness.Find(global.AcademicYearID.Value).Year;

            if (attachments == null || attachments.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
                res.ContentType = "text/plain";
                return res;
            }

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            //kiem tra file extension lan nua
            List<string> excelExtension = new List<string>();
            excelExtension.Add(".XLS");
            excelExtension.Add(".XLSX");
            var extension = Path.GetExtension(file.FileName);
            if (!excelExtension.Contains(extension.ToUpper()))
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                res.ContentType = "text/plain";
                return res;
                //return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
            }

            // luu ra dia
            var fileName = Path.GetFileNameWithoutExtension(file.FileName);
            fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
            var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
            file.SaveAs(physicalPath);

            //check cac loi co ban nhu ten truong,mon,hoc ky,lop,nam hoc
            string basicError = checkBasicErrorDataFromImportFile(physicalPath, inputData);
            if (basicError.Trim().Length == 0) //khong loi co ban
            {
                List<JudgeRecordViewModel> lsTemp = getDataFromImportFile(physicalPath, inputData);
                Session["ImportData"] = lsTemp;
                ViewData[JudgeRecordConstants.LIST_IMPORTDATA] = lsTemp;
                ViewData[JudgeRecordConstants.HAS_ERROR_DATA] = lsTemp.Where(o => !string.IsNullOrEmpty(o.ErrorDescription)).Any();

                if (lsTemp.Count() > 0)
                {
                    ViewData[JudgeRecordConstants.CLASS_12] = lsTemp.FirstOrDefault().EducationLevel < 3;
                }
                else
                {
                    ViewData[JudgeRecordConstants.CLASS_12] = true;
                }
                if (lsTemp.Count() > 0)
                {
                    ViewData[JudgeRecordConstants.SEMESTER_1] = lsTemp.FirstOrDefault().Semester == 1;
                }
                else
                {
                    ViewData[JudgeRecordConstants.SEMESTER_1] = true;
                }

                if (lsTemp.Where(o => !string.IsNullOrEmpty(o.ErrorDescription)).Any())
                {
                    ViewData[JudgeRecordConstants.ERROR_BASIC_DATA] = false;
                    ViewData[JudgeRecordConstants.ERROR_IMPORT_MESSAGE] = "Có lỗi trong file excel";
                    JsonResult res = Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "grid"));
                    res.ContentType = "text/plain";
                    return res;
                }
                else
                {
                    return SaveLegalDataImport(lsTemp);
                }
            }
            else   //co loi co ban,tra ve man hinh co loi va disable nut xem truoc,nut import data hop le
            {
                ViewData[JudgeRecordConstants.LIST_IMPORTDATA] = new List<JudgeRecordViewModel>();
                ViewData[JudgeRecordConstants.ERROR_BASIC_DATA] = true;
                ViewData[JudgeRecordConstants.ERROR_IMPORT_MESSAGE] = basicError;
                ViewData[JudgeRecordConstants.CLASS_12] = true;
                ViewData[JudgeRecordConstants.SEMESTER_1] = true;
                JsonResult res = Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "grid"));
                res.ContentType = "text/plain";
                return res;
            }
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private List<JudgeRecordViewModel> getDataFromImportFile(string filePath, JudgeRecordViewModel inputData)
        {
            List<JudgeRecordViewModel> lsJRVM = new List<JudgeRecordViewModel>();
            IVTWorkbook oBook = VTExport.OpenWorkbook(filePath);
            GlobalInfo global = new GlobalInfo();

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);

            List<PupilOfClass> listPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { { "ClassID", inputData.ClassID }, { "Check", "Check" } }).ToList();

            for (int i = 0; i < 1000; i++)
            {
                bool isLegalBot = true;
                bool IsInClassOrInSchool = true;
                string PupilCode, FullName, BirthDay, H11, H12, H13, H14, H15, H21, H22, H23, H24, H25, HK1, HK2;

                PupilCode = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['B']));
                FullName = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['C']));
                BirthDay = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['D']));

                if (PupilCode == null || PupilCode.Trim().Length == 0)
                {
                    break;
                }
                JudgeRecordViewModel jrvm = new JudgeRecordViewModel();

                PupilOfClass poc = listPOC.Where(u => u.PupilProfile.PupilCode.Equals(PupilCode, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (poc == null)
                {
                    isLegalBot = false;
                    jrvm.ErrorDescription += "-Học sinh không tồn tại trong trường <br>";
                    IsInClassOrInSchool = false;
                    jrvm.Status = 0;
                }
                else
                {
                    jrvm.PupilID = poc.PupilID;
                    jrvm.Status = poc.Status;

                    if (!poc.PupilProfile.FullName.Equals(FullName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        isLegalBot = false;
                        jrvm.ErrorDescription += "-Tên học sinh không phù hợp <br>";
                    }

                    if (poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        isLegalBot = false;
                        IsInClassOrInSchool = false;
                        jrvm.Status = 0;
                    }
                }

                jrvm.SubjectID = inputData.SubjectID;
                jrvm.EducationLevel = inputData.EducationLevel;
                jrvm.ClassID = inputData.ClassID;

                if (inputData.Semester == 1)
                {
                    if (jrvm.EducationLevel < 3)
                    {
                        H11 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['E']));
                        H12 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['F']));
                        H13 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['G']));

                        H14 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['H']));
                        H15 = "";
                        HK1 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['I']));

                        H21 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['J']));
                        H22 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['K']));
                        H23 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['L']));

                        H24 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['M']));
                        H25 = "";
                        HK2 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['N']));
                    }
                    else
                    {
                        H11 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['E']));
                        H12 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['F']));
                        H13 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['G']));

                        H14 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['H']));
                        H15 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['I']));

                        HK1 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['J']));

                        H21 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['K']));
                        H22 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['L']));
                        H23 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['M']));

                        H24 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['N']));
                        H25 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['O']));
                        HK2 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['P']));
                    }
                }
                else
                {
                    if (jrvm.EducationLevel < 3)
                    {
                        H11 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['E']));
                        H12 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['F']));
                        H13 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['G']));

                        H14 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['H']));
                        H15 = "";
                        HK1 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['I']));

                        H21 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['J']));
                        H22 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['K']));
                        H23 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['L']));

                        H24 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['M']));
                        H25 = "";
                        HK2 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['N']));
                    }
                    else
                    {
                        H11 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['E']));
                        H12 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['F']));
                        H13 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['G']));

                        H14 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['H']));
                        H15 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['I']));

                        HK1 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['J']));

                        H21 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['K']));
                        H22 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['L']));
                        H23 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['M']));

                        H24 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['N']));
                        H25 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['O']));
                        HK2 = ConvertToString(sheet.GetCellValue(i + 9, VTVector.dic['P']));
                    }
                }
                jrvm.Semester = inputData.Semester;
                jrvm.PupilCode = PupilCode;
                jrvm.FullName = FullName;
                jrvm.Year = inputData.Year;
                jrvm.isLegal = isLegalBot;

                jrvm.strH11 = H11;
                jrvm.strH12 = H12;
                jrvm.strH13 = H13;
                jrvm.strH14 = H14;
                jrvm.strH15 = H15;

                jrvm.strH21 = H21;
                jrvm.strH22 = H22;
                jrvm.strH23 = H23;
                jrvm.strH24 = H24;
                jrvm.strH25 = H25;

                jrvm.strMarkSemester1 = HK1;
                jrvm.strMarkSemester2 = HK2;

                #region check mark

                if (inputData.Semester == 1)
                {
                    List<string> lsMark = new List<string>() { H11, H12, H13, H14 };
                    if (jrvm.EducationLevel > 2)
                    {
                        lsMark.Add(H15);
                    }
                    List<string> lsMarkTBM = new List<string>() { HK1 };

                    IDictionary<string, object> dic = IsError(lsMark, false);
                    bool isLegalMark = (bool)dic["isLegal"];
                    string ErrorMark = (string)dic["ErrorDescription"];
                    jrvm.isLegal = jrvm.isLegal && isLegalMark;
                    if (IsInClassOrInSchool)
                    {
                        jrvm.ErrorDescription += ErrorMark;
                    }

                    //neu diem dinh ky hop le thi moi ktra tbm,vi rut cuc kiem tra xong cung la them
                    //"diem khong hop le" vao errordescription
                    if (isLegalMark)
                    {
                        dic = IsError(lsMarkTBM, true);
                        isLegalMark = (bool)dic["isLegal"];
                        ErrorMark = (string)dic["ErrorDescription"];
                        jrvm.isLegal = jrvm.isLegal && isLegalMark;
                        if (IsInClassOrInSchool)
                        {
                            jrvm.ErrorDescription += ErrorMark;
                        }
                    }
                }
                else
                {
                    List<string> lsMark = new List<string>() { H21, H22, H23, H24 };
                    if (jrvm.EducationLevel > 2)
                    {
                        lsMark.Add(H25);
                    }
                    List<string> lsMarkTBM = new List<string>() { HK2 };

                    IDictionary<string, object> dic = IsError(lsMark, false);
                    bool isLegalMark = (bool)dic["isLegal"];
                    string ErrorMark = (string)dic["ErrorDescription"];
                    jrvm.isLegal = jrvm.isLegal && isLegalMark;
                    if (IsInClassOrInSchool)
                    {
                        jrvm.ErrorDescription += ErrorMark;
                    }

                    //neu diem dinh ky hop le thi moi ktra tbm,vi rut cuc kiem tra xong cung la them
                    //"diem khong hop le" vao errordescription
                    if (isLegalMark)
                    {
                        dic = IsError(lsMarkTBM, true);
                        isLegalMark = (bool)dic["isLegal"];
                        ErrorMark = (string)dic["ErrorDescription"];
                        jrvm.isLegal = jrvm.isLegal && isLegalMark;
                        if (IsInClassOrInSchool)
                        {
                            jrvm.ErrorDescription += ErrorMark;
                        }
                    }
                }

                #endregion check mark

                lsJRVM.Add(jrvm);
            }

            return lsJRVM;
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportLegalData()
        {
            List<JudgeRecordViewModel> lsTemp = (List<JudgeRecordViewModel>)Session["ImportData"];
            lsTemp = lsTemp.Where(o => o.isLegal).ToList();
            return SaveLegalDataImport(lsTemp);
        }

        private IDictionary<string, object> IsError(List<string> mark, bool isTBMMark)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<string> lsMark = new List<string>();
            if (isTBMMark)
            {
                lsMark.Add("A+");
                lsMark.Add("A");
                lsMark.Add("B");
            }
            else
            {
                lsMark.Add("Đ");
                lsMark.Add("CĐ");
            }
            if (CheckMark(lsMark, mark))
            {
                dic["isLegal"] = true;
                dic["ErrorDescription"] = "";
                return dic;
            }
            else
            {
                dic["isLegal"] = false;
                dic["ErrorDescription"] = "-Điểm không hợp lệ <br>";
                return dic;
            }
        }

        
        [ValidateAntiForgeryToken]
        public JsonResult SaveLegalDataImport(List<JudgeRecordViewModel> LegalData)
        {
            GlobalInfo global = new GlobalInfo();
            List<JudgeRecord> lsJR = new List<JudgeRecord>();
            List<SummedUpRecord> lssur = new List<SummedUpRecord>();
            AcademicYear ay = AcademicYearBusiness.Find(global.AcademicYearID.Value);

            JudgeRecordViewModel data = LegalData.FirstOrDefault();

            List<PupilOfClass> listPC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value,
                                                                                new Dictionary<string, object>() { {"ClassID",data.ClassID}
                                                                                                                  ,{"Check","Check"}
                                                                                                                  ,{"AcademicYearID",data.AcademicYearID}
                                                                                                                  ,{"Semester",data.Semester}}).ToList();
            // Tạo data ghi log
            SubjectCat subject = SubjectCatBusiness.Find(data.SubjectID.Value);
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            StringBuilder inforLog = null;
            int iLog = 0;
            foreach (var item in LegalData)
            {
                var pupil = listPC.FirstOrDefault(U => U.PupilID == item.PupilID);
                if (pupil == null || pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING) // Kiem tra xem hoc sinh co ton tai va dang hoc khong, neu khong thi bo qua.
                {
                    continue;
                }
                int id = item.PupilID.Value;
                // Tạo dữ liệu ghi log
                PupilOfClass pop = listPC.Where(p => p.PupilID == id).FirstOrDefault();
                objectIDStr.Append(id);
                descriptionStr.Append("Import judge record pupil_id:" + id);
                paramsStr.Append("pupil_id:" + id);
                userFuntionsStr.Append("Sổ điểm");
                userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                inforLog = new StringBuilder();
                inforLog.Append("Import điểm cho " + pop.PupilProfile.FullName);
                inforLog.Append(" mã " + pop.PupilProfile.PupilCode);
                inforLog.Append(" lớp " + pop.ClassProfile.DisplayName);
                inforLog.Append(" môn " + subject.SubjectName);
                inforLog.Append(" học kì " + data.Semester.Value + " ");
                inforLog.Append(" năm " + pop.Year.Value + "-" + (pop.Year.Value + 1) + " ");
                userDescriptionsStr.Append(inforLog.ToString());
                //List<MarkRecord> listOldRecord = pop.PupilProfile.MarkRecords.ToList();// chỗ này ko tốn performance vì bản thân lstPOC đã có thông tin điểm
                //oldObjectStr.Append("{");
                //oldObjectStr.Append("pupil_profile_id:" + id + ",");
                //for (int j = 0; j < listOldRecord.Count; j++)
                //{
                //    MarkRecord record = listOldRecord[j];
                //    oldObjectStr.Append("{MarkRecordID:" + record.MarkRecordID + "," + record.Title + ":" + record.Mark + "}");
                //    if (j < listOldRecord.Count - 1)
                //    {
                //        oldObjectStr.Append(",");
                //    }
                //}
                //oldObjectStr.Append("}");
                // end

                #region SummedUpRecord

                
                SummedUpRecord sur = new SummedUpRecord();
                sur.PupilID = item.PupilID.Value;
                sur.ClassID = item.ClassID.Value;
                sur.SubjectID = (int)item.SubjectID.Value;
                sur.Semester = item.Semester.Value;
                sur.IsCommenting = 1;
                sur.AcademicYearID = global.AcademicYearID.Value;
                sur.SchoolID = global.SchoolID.Value;
                if (ay != null)
                {
                    sur.CreatedAcademicYear = ay.Year;
                }
                if (item.Semester == 1)
                {
                    sur.JudgementResult = item.strMarkSemester1;
                }
                else
                {
                    sur.JudgementResult = item.strMarkSemester2;
                }
                lssur.Add(sur);

                #endregion SummedUpRecord

                #region JudgeRecord

                int count = 0;
                if (item.EducationLevel > 2)
                {
                    count = 5;
                }
                else
                {
                    count = 4;
                }
                for (int i = 1; i <= count; i++)
                {
                    JudgeRecord jr = new JudgeRecord();
                    jr.PupilID = item.PupilID.Value;
                    jr.ClassID = item.ClassID.Value;
                    jr.SubjectID = (int)item.SubjectID.Value;
                    jr.Semester = item.Semester.Value;
                    jr.MarkTypeID = 5;
                    jr.Title = "C1";
                    jr.AcademicYearID = global.AcademicYearID.Value;
                    jr.SchoolID = global.SchoolID.Value;
                    jr.MarkedDate = DateTime.Now;
                    if (ay != null)
                    {
                        jr.CreatedAcademicYear = ay.Year;
                    }
                    if (item.Semester == 1)
                    {
                        switch (i)
                        {
                            case 1:
                                jr.Judgement = item.strH11;
                                jr.OrderNumber = 1;
                                break;

                            case 2:
                                jr.Judgement = item.strH12;
                                jr.OrderNumber = 2;
                                break;

                            case 3:
                                jr.Judgement = item.strH13;
                                jr.OrderNumber = 3;
                                break;

                            case 4:
                                jr.Judgement = item.strH14;
                                jr.OrderNumber = 4;
                                break;

                            case 5:
                                jr.Judgement = item.strH15;
                                jr.OrderNumber = 5;
                                break;

                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (i)
                        {
                            case 1:
                                jr.Judgement = item.strH21;
                                jr.OrderNumber = 1;
                                break;

                            case 2:
                                jr.Judgement = item.strH22;
                                jr.OrderNumber = 2;
                                break;

                            case 3:
                                jr.Judgement = item.strH23;
                                jr.OrderNumber = 3;
                                break;

                            case 4:
                                jr.Judgement = item.strH24;
                                jr.OrderNumber = 4;
                                break;

                            case 5:
                                jr.Judgement = item.strH25;
                                jr.OrderNumber = 5;
                                break;

                            default:
                                break;
                        }
                    }
                    if (!string.IsNullOrEmpty(jr.Judgement))
                    {
                        lsJR.Add(jr);
                    }
                }

                #endregion JudgeRecord

                // Tạo dữ liệu ghi log
                if (iLog < LegalData.Count - 1)
                {
                    objectIDStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
                iLog++;
            }

            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            if (!isMovedHistory)
            {
                JudgeRecordBusiness.InsertJudgeRecordPrimary(global.UserAccountID, lsJR, lssur);
                JudgeRecordBusiness.Save();
            }
            else
            {
                List<JudgeRecordHistory> lsJRHistory = new List<JudgeRecordHistory>();
                JudgeRecordHistory objJudgeHistory = null;
                JudgeRecord objJudge = null;
                for (int i = 0; i < lsJR.Count; i++)
                {
                    objJudgeHistory = new JudgeRecordHistory();
                    objJudge = lsJR[i];
                    //objJudgeHistory.AcademicYear = objJudge.AcademicYear;
                    objJudgeHistory.AcademicYearID = objJudge.AcademicYearID;
                    objJudgeHistory.ClassID = objJudge.ClassID;
                    //objJudgeHistory.ClassProfile = objJudge.ClassProfile;
                    objJudgeHistory.CreatedAcademicYear = objJudge.CreatedAcademicYear;
                    objJudgeHistory.CreatedDate = objJudge.CreatedDate;
                    objJudgeHistory.IsOldData = objJudge.IsOldData;
                    objJudgeHistory.IsSMS = objJudge.IsSMS;
                    objJudgeHistory.Judgement = objJudge.Judgement;
                    objJudgeHistory.JudgeRecordID = objJudge.JudgeRecordID;
                    objJudgeHistory.Last2digitNumberSchool = objJudge.Last2digitNumberSchool;
                    objJudgeHistory.M_OldID = objJudge.M_OldID;
                    objJudgeHistory.M_ProvinceID = objJudge.M_ProvinceID;
                    objJudgeHistory.MarkedDate = objJudge.MarkedDate;
                    //objJudgeHistory.MarkType = objJudge.MarkType;
                    objJudgeHistory.MarkTypeID = objJudge.MarkTypeID;
                    objJudgeHistory.MJudgement = objJudge.MJudgement;
                    objJudgeHistory.ModifiedDate = objJudge.ModifiedDate;
                    objJudgeHistory.MSourcedb = objJudge.MSourcedb;
                    objJudgeHistory.OldJudgement = objJudge.OldJudgement;
                    objJudgeHistory.OrderNumber = objJudge.OrderNumber;
                    objJudgeHistory.PupilID = objJudge.PupilID;
                    //objJudgeHistory.PupilProfile = objJudge.PupilProfile;
                    objJudgeHistory.ReTestJudgement = objJudge.ReTestJudgement;
                    objJudgeHistory.SchoolID = objJudge.SchoolID;
                    //objJudgeHistory.SchoolProfile = objJudge.SchoolProfile;
                    objJudgeHistory.Semester = objJudge.Semester;
                    //objJudgeHistory.SubjectCat = objJudge.SubjectCat;
                    objJudgeHistory.SubjectID = objJudge.SubjectID;
                    objJudgeHistory.SynchronizeID = objJudge.SynchronizeID;
                    objJudgeHistory.Title = objJudge.Title;
                    lsJRHistory.Add(objJudgeHistory);
                }

                List<SummedUpRecordHistory> lssurHistory = new List<SummedUpRecordHistory>();
                SummedUpRecordHistory objSumHistory = null;
                SummedUpRecord objSum = null;
                for (int j = 0; j < lssur.Count; j++)
                {
                    objSumHistory = new SummedUpRecordHistory();
                    objSum = lssur[j];
                    //objSumHistory.AcademicYear = objSum.AcademicYear;
                    objSumHistory.AcademicYearID = objSum.AcademicYearID;
                    objSumHistory.ClassID = objSum.ClassID;
                    //objSumHistory.ClassProfile = objSum.ClassProfile;
                    objSumHistory.Comment = objSum.Comment;
                    objSumHistory.CreatedAcademicYear = objSum.CreatedAcademicYear;
                    objSumHistory.IsCommenting = objSum.IsCommenting;
                    objSumHistory.IsOldData = objSum.IsOldData;
                    objSumHistory.JudgementResult = objSum.JudgementResult;
                    objSumHistory.Last2digitNumberSchool = objSum.Last2digitNumberSchool;
                    objSumHistory.M_OldID = objSum.M_OldID;
                    objSumHistory.M_ProvinceID = objSum.M_ProvinceID;
                    objSumHistory.MSourcedb = objSum.MSourcedb;
                    //objSumHistory.PeriodDeclaration = objSum.PeriodDeclaration;
                    objSumHistory.PeriodID = objSum.PeriodID;
                    objSumHistory.PupilID = objSum.PupilID;
                    //objSumHistory.PupilProfile = objSum.PupilProfile;
                    objSumHistory.ReTestJudgement = objSum.ReTestJudgement;
                    objSumHistory.ReTestMark = objSum.ReTestMark;
                    objSumHistory.SchoolID = objSum.SchoolID;
                    //objSumHistory.SchoolProfile = objSum.SchoolProfile;
                    objSumHistory.Semester = objSum.Semester;
                    //objSumHistory.SubjectCat = objSum.SubjectCat;
                    objSumHistory.SubjectID = objSum.SubjectID;
                    objSumHistory.SummedUpDate = objSum.SummedUpDate;
                    objSumHistory.SummedUpMark = objSum.SummedUpMark;
                    objSumHistory.SummedUpRecordID = objSum.SummedUpRecordID;
                    objSumHistory.SynchronizeID = objSum.SynchronizeID;
                    lssurHistory.Add(objSumHistory);
                }
                JudgeRecordHistoryBusiness.InsertJudgeRecordPrimaryHistory(global.UserAccountID, lsJRHistory, lssurHistory);
                JudgeRecordHistoryBusiness.Save();
            }
            // Tạo dữ liệu ghi log
            SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());
            return Json(new JsonMessage(Res.Get("Common_Label_ImportMarkSuccess")));
        }

        public bool CheckMark(List<string> lsMark, List<string> lsInput)
        {
            foreach (var item in lsInput)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }
                if (!lsMark.Contains(item))
                {
                    return false;
                }
            }
            return true;
        }

        //khong loi:rong;loi:khac rong
        public string checkBasicErrorDataFromImportFile(string filePath, JudgeRecordViewModel inputData)
        {
            try
            {
                string error = "";
                IVTWorkbook oBook = VTExport.OpenWorkbook(filePath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                string SupervisingDept = ConvertToString(sheet.GetCellValue(2, VTVector.dic['A']));
                string School = ConvertToString(sheet.GetCellValue(3, VTVector.dic['A']));
                string subjectAndSemesterAndClass = ConvertToString(sheet.GetCellValue(4, VTVector.dic['A']));
                string YearTitle = ConvertToString(sheet.GetCellValue(5, VTVector.dic['A']));
                subjectAndSemesterAndClass = subjectAndSemesterAndClass.ToUpper();
                YearTitle = YearTitle != null ? YearTitle.ToUpper() : "";

                SchoolProfile sp = SchoolProfileBusiness.Find(inputData.SchoolID.Value);
                SubjectCat sc = SubjectCatBusiness.Find(inputData.SubjectID.Value);
                ClassProfile cp = ClassProfileBusiness.Find(inputData.ClassID.Value);
                string compareSemester = "học kỳ " + inputData.Semester.ToString();

                bool isSchoolError = !School.ToUpper().Contains(sp.SchoolName.ToUpper());
                bool isSubjectError = !subjectAndSemesterAndClass.ToUpper().Contains(sc.DisplayName.ToUpper());
                bool isSemesterError = !subjectAndSemesterAndClass.ToUpper().Contains(compareSemester.ToUpper());
                bool isClassError = !subjectAndSemesterAndClass.ToUpper().Contains(cp.DisplayName.ToUpper());
                string strYear = inputData.Year.ToString() + "-" + (inputData.Year + 1).ToString();
                bool isYearError = !YearTitle.Replace(" ", "").Contains(strYear.Trim());
                if (isSchoolError && isSubjectError && isClassError && isYearError)
                {
                    error += "-Có lỗi trong file excel<br>";
                }
                if (isSchoolError)
                {
                    error += "-File excel không phải là file điểm của trường " + sp.SchoolName + "<br>";
                }
                if (isSemesterError)
                {
                    error += "-File excel không phải là file điểm của học kỳ " + inputData.Semester + "<br>";
                }
                if (isSubjectError)
                {
                    error += "-File excel không phải là file điểm của môn " + sc.DisplayName + "<br>";
                }
                if (isClassError)
                {
                    error += "-File excel không phải là file điểm của lớp " + cp.DisplayName + "<br>";
                }
                if (isYearError)
                {
                    error += "-File excel không phải là file điểm của năm " + inputData.Year + "<br>";
                }

                return error;
            }
            catch (Exception ex)
            {
                return "File excel không đúng định dạng" + "<br>";
            }
        }

        private string ConvertToString(object o)
        {
            if (o == null)
            {
                return "";
            }
            return o.ToString();
        }

        #endregion ImportExcel
    }
}
