/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.PrimaryJudgeRecordArea
{
    public class JudgeRecordConstants
    {
        public const string LIST_JUDGERECORD = "listJudgeRecord";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_SUBJECT = "LIST_SUBJECT";

        public const string ListTitle = "ListTitle";
        public const string HIDDEN_J15_J25_GRID = "HIDDEN_J15_J25_GRID";
        public const string DISABLE_SEMESTER_COLUMN = "DISABLE_SEMESTER_COLUMN";
        public const string ENABLE_BUTTON = "ENABLE_BUTTON";
        public const string ENABLE_BUTTON_EXCEL = "ENABLE_BUTTON_EXCEL";

        public const string DISABLE_CHECKBOX = "DISABLE_CHECKBOX";
        public const string VISIBLE_COLUMN2 = "VISIBLE_COLUMN2";

        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string CLASS_12 = "CLASS_12";
        public const string SEMESTER_1 = "SEMESTER_1";

        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";

        public const string IS_TEACHER_PERMISS = "IS_TEACHER_PERMISS";
        public const string SEMESTER = "SEMESTER";

        public const string LOCK_TITLE = "LOCK_TITLE";
    }
}