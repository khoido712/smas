/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PrimaryJudgeRecordArea.Models
{
    public class JudgeRecordViewModel
    {
        public long? JudgeRecordID { get; set; }

        public int? ClassID { get; set; }

        public int? SchoolID { get; set; }

        public int? AcademicYearID { get; set; }

        public int? SubjectID { get; set; }

        public int? MarkTypeID { get; set; }

        public System.Int32 Year { get; set; }

        public System.Nullable<System.Int32> Semester { get; set; }

        public System.Nullable<System.Int32> PeriodID { get; set; }

        public System.String Judgement { get; set; }

        public System.String ReTestJudgement { get; set; }

        public System.Nullable<System.DateTime> CreatedDate { get; set; }

        public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        public int? PupilID { get; set; }

        public int? Status { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        public string Name { get; set; }
        public bool H11 { get; set; }

        public bool H12 { get; set; }

        public bool H13 { get; set; }

        public bool H14 { get; set; }

        public bool H15 { get; set; }

        public bool H21 { get; set; }

        public bool H22 { get; set; }

        public bool H23 { get; set; }

        public bool H24 { get; set; }

        public bool H25 { get; set; }

        public int? OrderInClass { get; set; }

        public int MarkSemester1 { get; set; }

        public int MarkSemester2 { get; set; }

        public bool ExemptedSuject { get; set; }

        public int EducationLevel { get; set; }

        #region import additional

        public bool isLegal { get; set; }

        public string ErrorDescription { get; set; }

        public string strH11 { get; set; }

        public string strH12 { get; set; }

        public string strH13 { get; set; }

        public string strH14 { get; set; }

        public string strH15 { get; set; }

        public string strH21 { get; set; }

        public string strH22 { get; set; }

        public string strH23 { get; set; }

        public string strH24 { get; set; }

        public string strH25 { get; set; }

        public string strMarkSemester1 { get; set; }

        public string strMarkSemester2 { get; set; }

        #endregion import additional
    }
}
