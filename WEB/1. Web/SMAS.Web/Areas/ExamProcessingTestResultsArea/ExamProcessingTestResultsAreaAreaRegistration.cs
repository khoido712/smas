﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamProcessingTestResultsArea
{
    public class ExamProcessingTestResultsAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamProcessingTestResultsArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamProcessingTestResultsArea_default",
                "ExamProcessingTestResultsArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
