﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.StatisticLevel.Models
{
    public class StatisticLevelConfigViewModel : StatisticLevelConfig
    {
        public bool Chosen
        {
            get;
            set;
        }
    }
}