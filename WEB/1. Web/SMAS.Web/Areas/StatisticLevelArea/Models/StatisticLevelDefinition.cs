﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SMAS.Web.Areas.StatisticLevel.Models
{
    public class StatisticLevelDefinition
    {
        /// <summary>
        /// id
        /// </summary>
        public int StatisticLevelReportID
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Thầy/cô chưa nhập mức thống kê")]
        [MaxLength(50,ErrorMessage = "Mức thống kê không được vượt quá 50 ký tự")]
        [DisplayName("Mức thống kê")]
        /// <summary>
        /// Ten muc thong ke
        /// </summary>
        public string Resolution
        {
            get;
            set;
        }

        [MaxLength(100)]
        [DisplayName("Ghi chú")]
        /// <summary>
        /// Mo ta
        /// </summary>
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Co kha nang sua doi
        /// </summary>
        public bool Editable
        {
            get;
            set;
        }

        /// <summary>
        /// Co kha nang sua doi
        /// </summary>
        public bool Deleteable
        {
            get;
            set;
        }

        /// <summary>
        /// Doi tuong se bi xoa
        /// </summary>
        public bool WillBeDeleted
        {
            get;
            set;
        }
    }
}