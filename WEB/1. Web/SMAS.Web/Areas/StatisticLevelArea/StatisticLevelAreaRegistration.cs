﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticLevel
{
    public class StatisticLevelAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticLevelArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticLevelArea_default",
                "StatisticLevelArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
