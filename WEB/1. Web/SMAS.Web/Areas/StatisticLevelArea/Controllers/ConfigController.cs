﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.StatisticLevel.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.StatisticLevel.Controllers
{
    public class ConfigController : BaseController
    {
        IStatisticLevelReportBusiness StatisticLevelReportBusiness;
        IStatisticLevelConfigBusiness StatisticLevelConfigBusiness;
        public ConfigController(
            IStatisticLevelReportBusiness statisticLevelReportBusiness,
            IStatisticLevelConfigBusiness statisticLevelConfigBusiness
        )
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            this.StatisticLevelReportBusiness = statisticLevelReportBusiness;
            this.StatisticLevelConfigBusiness = statisticLevelConfigBusiness;
        }

        public ActionResult Index(int id = 0)
        {
            int SelectedStatisticLevelReportID = id;
            var globalInfo = GlobalInfo.getInstance();
            List<StatisticLevelReport> statisticLevelReports 
                = this.StatisticLevelReportBusiness.getStatisticLevelReportOfSchool(globalInfo.SchoolID.Value);
            StatisticLevelReport selectedStatisticLevelReports;
            if (statisticLevelReports == null)
            {
                selectedStatisticLevelReports = null;
            }
            else
            {
                selectedStatisticLevelReports = statisticLevelReports.FirstOrDefault(p => p.StatisticLevelReportID == SelectedStatisticLevelReportID);
                if (selectedStatisticLevelReports == null)
                {
                    selectedStatisticLevelReports = statisticLevelReports.FirstOrDefault();
                }
            }
            ViewBag.StatisticLevelReports = statisticLevelReports;
            return View(selectedStatisticLevelReports);
        }

        public ActionResult List(int id)
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            int SelectedStatisticLevelReportID = id;
            var reports = this.StatisticLevelReportBusiness.getStatisticLevelReportOfSchool(globalInfo.SchoolID.Value);
            var statisticLevelReport = reports.FirstOrDefault(p => p.StatisticLevelReportID == SelectedStatisticLevelReportID);
            List<StatisticLevelConfigViewModel> configs;
            if (statisticLevelReport == null)
            {
                configs = new List<StatisticLevelConfigViewModel>();
            }
            else
            {
                configs = statisticLevelReport.StatisticLevelConfigs.OrderBy(o=>o.OrderNumber)
                    .Select(p => new StatisticLevelConfigViewModel()
                    {
                        StatisticLevelConfigID = p.StatisticLevelConfigID,
                        StatisticLevelReportID = p.StatisticLevelReportID,
                        Title = p.Title,
                        MinValue = p.MinValue,
                        SignMin = p.SignMin,
                        MaxValue = p.MaxValue,
                        SignMax = p.SignMax,
                        Chosen = false
                    }).ToList();
            }
            int permission = GetMenupermission("StatisticLevel", globalInfo.UserAccountID, globalInfo.IsAdmin);
            ViewBag.Permission = permission;
            ViewBag.SelectedStatisticLevelReportID = SelectedStatisticLevelReportID;
            return PartialView("_List", configs);
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public ActionResult Save(int selectedStatisticLevelReportID
            , List<int> StatisticLevelConfigID
            , List<string> Title
            , List<decimal?> MinValue
            , List<int> SignMin
            , List<decimal?> MaxValue
            , List<int> SignMax
            , List<bool> Chosen
            //, List<StatisticLevelConfigViewModel> configs
            )
        {
            int permission = GetMenupermission("StatisticLevel", GlobalInfo.getInstance().UserAccountID, GlobalInfo.getInstance().IsAdmin);
            if (permission < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            var statisticLevelReport = this.StatisticLevelReportBusiness.Find(selectedStatisticLevelReportID);
            if (StatisticLevelConfigID == null) StatisticLevelConfigID = new List<int>();
            List<StatisticLevelConfigViewModel> configs = new List<StatisticLevelConfigViewModel>();
            if (Title == null) Title = new List<string>();
            if (MinValue == null) MinValue = new List<decimal?>();
            if (SignMin == null) SignMin = new List<int>();
            if (MaxValue == null) MaxValue = new List<decimal?>();
            if (SignMax == null) SignMax = new List<int>();
            List<StatisticLevelConfig> lstStatisticLevelConfig;
            bool error = false;
            string message = Res.Get("StatisticLevel_Config_Msg_Success");
            if (StatisticLevelConfigID.Count != Title.Count
                || StatisticLevelConfigID.Count != MinValue.Count
                || StatisticLevelConfigID.Count != SignMin.Count
                || StatisticLevelConfigID.Count != MaxValue.Count
                || StatisticLevelConfigID.Count != SignMax.Count)
            {
                message = Res.Get("StatisticLevel_Config_Msg_Error_InvalidData");
                error = false;
                if (statisticLevelReport != null)
                    lstStatisticLevelConfig = statisticLevelReport.StatisticLevelConfigs.ToList();
                else
                    lstStatisticLevelConfig = new List<StatisticLevelConfig>();
            }
            else
            {
                // Kiem tra du lieu
                for (int i = 0; i < StatisticLevelConfigID.Count; i++)
                {
                    var newStatisticLevelConfig = new StatisticLevelConfigViewModel()
                    {
                        StatisticLevelReportID = selectedStatisticLevelReportID,
                        Title = Title[i],
                        MinValue = MinValue[i],
                        SignMin = SignMin[i],
                        MaxValue = MaxValue[i],
                        SignMax = SignMax[i],
                        CreateDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        OrderNumber = i + 1
                    };
                    configs.Add(newStatisticLevelConfig);
                    if (!newStatisticLevelConfig.MinValue.HasValue)
                    {
                        // ko co ca MaxValue lan MinValue
                        if (!newStatisticLevelConfig.MaxValue.HasValue)
                        {
                            message = Res.Get("StatisticLevel_Config_Msg_Error_InvalidConfig");
                            error = true;
                        }
                        else
                        {
                            // co MaxValue ma khong co MinValue
                            if (string.IsNullOrEmpty(newStatisticLevelConfig.Title))
                            {
                                newStatisticLevelConfig.Title = newStatisticLevelConfig.SignMax == 3 ? "Điểm "+ newStatisticLevelConfig.MaxValue.Value 
                                    :  "Đ" + (newStatisticLevelConfig.SignMax == 4 ? "<" : "<=") + newStatisticLevelConfig.MaxValue.Value;
                            }
                        }
                    }
                    else
                    {
                        if (!newStatisticLevelConfig.MaxValue.HasValue)
                        {
                            // co MinValue ma khong co MaxValue
                            if (string.IsNullOrEmpty(newStatisticLevelConfig.Title))
                            {
                                newStatisticLevelConfig.Title = "Đ" + (newStatisticLevelConfig.SignMin == 1 ? ">=" : ">") + newStatisticLevelConfig.MinValue.Value;
                            }
                        }
                        else
                        {
                            // co ca MinValue va MaxValue
                            if (newStatisticLevelConfig.SignMax == 3)  //khi khai bao ca minValue va maxValue thi dau signMax phai khac dau =
                            {
                                message = Res.Get("StatisticLevel_Config_Msg_Error_InvalidConfig");
                                error = true;
                            }
                            else if (newStatisticLevelConfig.MaxValue < newStatisticLevelConfig.MinValue)
                            {
                                message = Res.Get("StatisticLevel_Config_Msg_Error_InvalidLevel");
                                error = true;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(newStatisticLevelConfig.Title))
                                {
                                    newStatisticLevelConfig.Title = newStatisticLevelConfig.MinValue.Value +
                                        (newStatisticLevelConfig.SignMin == 1 ? "<=" : "<") +
                                        "Đ" +
                                        (newStatisticLevelConfig.SignMax == 4 ? "<" : "<=") +
                                        newStatisticLevelConfig.MaxValue.Value;
                                }
                            }
                        }
                    }
                }
                if (!error)
                {
                    if (statisticLevelReport == null || statisticLevelReport.SchoolID != GlobalInfo.getInstance().SchoolID)
                    {
                        message = Res.Get("StatisticLevel_Config_Msg_Error_InvalidConfig");
                        error = true;
                        //lstStatisticLevelConfig = new List<StatisticLevelConfig>();
                    }
                    else
                    {
                        // danh sach cac config cu
                        var oldStatisticLevelConfig = statisticLevelReport.StatisticLevelConfigs.ToList();
                        // xoa danh sach config cu
                        this.StatisticLevelConfigBusiness.DeleteAll(oldStatisticLevelConfig);
                        // them cac configs moi
                        int i = 0;
                        foreach (var config in configs)
                        {
                            var newStatisticLevelConfig = new StatisticLevelConfig()
                            {
                                StatisticLevelReportID = selectedStatisticLevelReportID,
                                Title = config.Title,
                                MinValue = config.MinValue,
                                SignMin = config.MinValue.HasValue? config.SignMin : null,
                                MaxValue = config.MaxValue,
                                SignMax = config.MaxValue.HasValue? config.SignMax : null,
                                CreateDate = DateTime.Now,
                                ModifiedDate = DateTime.Now,
                                OrderNumber = i + 1    
                            };
                            ++i;
                            this.StatisticLevelConfigBusiness.Insert(newStatisticLevelConfig);
                        }
                        this.StatisticLevelConfigBusiness.Save();
                        //lstStatisticLevelConfig = statisticLevelReport.StatisticLevelConfigs.ToList();
                    }
                }
            }
            ModelState.Clear();
            ViewBag.Error = error;
            ViewBag.Message = message;
            ViewBag.SelectedStatisticLevelReportID = selectedStatisticLevelReportID;           
            ViewBag.Permission = permission;
            return PartialView("_List",configs);
        }
    }

}
