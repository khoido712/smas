﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.StatisticLevel.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using System.Globalization;

namespace SMAS.Web.Areas.StatisticLevel.Controllers
{
    public class DefinitionController : BaseController //BaseController
    {
        IStatisticLevelReportBusiness StatisticLevelReportBusiness;
        IStatisticLevelConfigBusiness StatisticLevelConfigBusiness;
        public DefinitionController(
            IStatisticLevelReportBusiness statisticLevelReportBusiness
            , IStatisticLevelConfigBusiness statisticLevelConfigBusiness)
        {
            this.StatisticLevelReportBusiness = statisticLevelReportBusiness;
            this.StatisticLevelConfigBusiness = statisticLevelConfigBusiness;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            List<StatisticLevelDefinition> lstDefinition;
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            lstDefinition = this.StatisticLevelReportBusiness.getStatisticLevelReportOfSchool(globalInfo.SchoolID.Value)
                .Select(r => new StatisticLevelDefinition()
                {
                    StatisticLevelReportID = r.StatisticLevelReportID,
                    Resolution = r.Resolution,
                    Description = r.Description,
                    Editable = r.SchoolID.HasValue,
                    Deleteable = r.SchoolID.HasValue,
                    WillBeDeleted = false
                })
                .ToList();
            int permission = GetMenupermission("StatisticLevel", globalInfo.UserAccountID, globalInfo.IsAdmin);
            ViewBag.Permission = permission;
            return this.PartialView("_List",lstDefinition);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var statisticLevelDefinition = new StatisticLevelDefinition() { StatisticLevelReportID = 0 };
            return this.PartialView("_CreateOrEdit", statisticLevelDefinition);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var statisticLevelDefinition = new StatisticLevelDefinition() { StatisticLevelReportID = 0 };
            var statisticLevelReport = this.StatisticLevelReportBusiness.Find(id);
            if (statisticLevelReport != null)
            {
                statisticLevelDefinition.StatisticLevelReportID = statisticLevelReport.StatisticLevelReportID;
                statisticLevelDefinition.Resolution = statisticLevelReport.Resolution;
                statisticLevelDefinition.Description = statisticLevelReport.Description;
            }
            else
            {
                ModelState.AddModelError("ERROR1", "Mức thống kê không tồn tại");
            }
            return View("_CreateOrEdit", statisticLevelDefinition);
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public ActionResult Save(StatisticLevelDefinition definition, bool? IsAutoConfig, decimal? Distance)
        {
            var globalInfo = GlobalInfo.getInstance();
           
            if (ModelState.IsValid)
            {
                if (definition.StatisticLevelReportID <= 0) // Them moi
                {
                    if (GetMenupermission("StatisticLevel", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
                    {
                        throw new BusinessException("Validate_Permission_Teacher");
                    }
                    // Tim xem co muc thong ke nao ttrung ten không
                    var lst = this.StatisticLevelReportBusiness.getStatisticLevelReportOfSchool(globalInfo.SchoolID.Value);
                    var statisticLevelReportHasSameName = lst.FirstOrDefault(r => r.Resolution.ToLower() == definition.Resolution.ToLower());
                    if (statisticLevelReportHasSameName != null)
                    {
                        ModelState.AddModelError("Resolution",
                            "Mức thống kê đã tồn tại trong hệ thống. Thầy/cô vui lòng nhập mức thống kê khác.");
                    }
                    else
                    {
                        var statisticLevelReport = new StatisticLevelReport()
                        {
                            SchoolID = globalInfo.SchoolID,
                            Resolution = definition.Resolution,
                            Description = definition.Description,
                            CreateDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            IsActive = true,
                        };
                        this.StatisticLevelReportBusiness.Insert(statisticLevelReport);
                        this.StatisticLevelReportBusiness.Save();
                        if (IsAutoConfig == true)
                        {
                            List<StatisticLevelConfig> lstConfig = GenerateConfigByDistance(Distance.Value, statisticLevelReport.StatisticLevelReportID);
                            for (int i = 0; i < lstConfig.Count; i++)
                            {
                                this.StatisticLevelConfigBusiness.Insert(lstConfig[i]);
                            }
                        }

                        this.StatisticLevelConfigBusiness.Save();
                        definition = new StatisticLevelDefinition();
                        ViewBag.Message = "Thêm mới thành công";
                        ViewBag.CloseDialog = false;
                        ModelState.Clear();
                    }
                }
                else
                {
                    var statisticLevelReport = this.StatisticLevelReportBusiness.Find(definition.StatisticLevelReportID);
                    this.CheckPermissionForAction(definition.StatisticLevelReportID, "StatisticLevelReport");
                    if (statisticLevelReport != null)
                    {
                        // Tim xem co muc thong ke nao ttrung ten không
                        if (GetMenupermission("StatisticLevel", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
                        {
                            throw new BusinessException("Validate_Permission_Teacher");
                        }
                        var lst = this.StatisticLevelReportBusiness.getStatisticLevelReportOfSchool(globalInfo.SchoolID.Value);
                        var statisticLevelReportHasSameName = lst.FirstOrDefault(r => r.StatisticLevelReportID != definition.StatisticLevelReportID 
                            && r.Resolution.ToLower() == definition.Resolution.ToLower());
                        if (statisticLevelReportHasSameName != null)
                        {
                            ModelState.AddModelError("Resolution",
                                "Mức thống kê đã tồn tại trong hệ thống. Thầy/cô vui lòng nhập mức thống kê khác.");
                        }
                        else
                        {
                            statisticLevelReport.Resolution = definition.Resolution;
                            statisticLevelReport.Description = definition.Description;
                            statisticLevelReport.ModifiedDate = DateTime.Now;
                            this.StatisticLevelReportBusiness.Update(statisticLevelReport);
                            this.StatisticLevelReportBusiness.Save();
                            if (IsAutoConfig == true)
                            {
                                //Xoa cau hinh
                                List<StatisticLevelConfig> lstDeleteConfig = StatisticLevelConfigBusiness.All
                                    .Where(o => o.StatisticLevelReportID == statisticLevelReport.StatisticLevelReportID).ToList();
                                StatisticLevelConfigBusiness.DeleteAll(lstDeleteConfig);
                                StatisticLevelConfigBusiness.Save();

                                List<StatisticLevelConfig> lstConfig = GenerateConfigByDistance(Distance.Value, statisticLevelReport.StatisticLevelReportID);
                                for (int i = 0; i < lstConfig.Count; i++)
                                {
                                    this.StatisticLevelConfigBusiness.Insert(lstConfig[i]);
                                }
                                StatisticLevelConfigBusiness.Save();
                            }
                            
                            
                            definition.StatisticLevelReportID = statisticLevelReport.StatisticLevelReportID;
                            ViewBag.Message = "Cập nhật thành công";
                            ViewBag.CloseDialog = true;
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("ERROR1", "Mức thống kê không tồn tại");
                    }
                }
            }
            return this.PartialView("_CreateOrEdit", definition);
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public ActionResult Delete(List<StatisticLevelDefinition> lstDefinition)
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            if (GetMenupermission("StatisticLevel", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            var lstDefinitionWillBeDeleted = lstDefinition.Where(d=>d.WillBeDeleted);
            var lstDefinitionWillBeDeletedID = lstDefinitionWillBeDeleted.Select(d => d.StatisticLevelReportID);
            var lstReportWillBeDeleted = this.StatisticLevelReportBusiness.All
                .Where(r=>r.SchoolID == globalInfo.SchoolID
                    && lstDefinitionWillBeDeletedID.Contains(r.StatisticLevelReportID))
                .ToList();
            foreach (var report in lstReportWillBeDeleted)
            {
                var lstConfigWillBeDeleted = report.StatisticLevelConfigs.ToList();
                this.StatisticLevelConfigBusiness.DeleteAll(lstConfigWillBeDeleted);
            }
            this.StatisticLevelReportBusiness.DeleteAll(lstReportWillBeDeleted);
            this.StatisticLevelReportBusiness.Save();
            ModelState.Clear();
            return List();
        }

        private List<StatisticLevelConfig> GenerateConfigByDistance(decimal distance, int statisticLevelReportID)
        {
            List<StatisticLevelConfig> lstConfig = new List<StatisticLevelConfig>();
            StatisticLevelConfig config;
            for (int i = 0; i<100 ; i++)
            {
                config = new StatisticLevelConfig();
                config.StatisticLevelReportID = statisticLevelReportID;
                config.CreateDate = DateTime.Now.Date;
                config.MinValue = i * distance;
                config.MaxValue = (i + 1) * distance;
                config.OrderNumber = i + 1;
                
                if (config.MaxValue < 10)
                {
                    config.SignMin = SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN;
                    config.SignMax = SystemParamsInFile.Sign_Compare.SMALLER_SIGN;
                }
                else
                {
                    config.SignMin = SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN;
                    config.SignMax = SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN;
                }

                config.Title = String.Format(CultureInfo.InvariantCulture, "{0:0.##}", config.MinValue) + " - " + String.Format(CultureInfo.InvariantCulture, "{0:0.##}", config.MaxValue);
                
                lstConfig.Add(config);

                if (config.MaxValue >= 10)
                {
                    break;
                }
            }

            return lstConfig;
        }
    }
}
