﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamBagArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamBagConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamGroup(this)")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamBag_Validate_Required_Examinations")]
        public long Examinations { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamBagConstants.LIST_EXAM_GROUP)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamSubject(this)")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamBag_Validate_Required_ExamGroup")]
        public long ExamGroup { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamBagConstants.LIST_EXAM_SUBJECT)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamBag(this)")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamBag_Validate_Required_Subject")]
        public long ExamSubject { get; set; }
    }
}