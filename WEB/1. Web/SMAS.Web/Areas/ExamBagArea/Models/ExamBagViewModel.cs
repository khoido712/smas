﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamBagArea.Models
{
    public class ExamBagViewModel
    {
        public long ExamBagID { get; set; }

        [ResourceDisplayName("ExamBag_Column_ExamBagCode")]
        public string ExamBagCode { get; set; }

        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public long ExamRoomID { get; set; }

        [ResourceDisplayName("ExamRoom_Column_ExamRoomCode")]
        public string ExamRoomCode { get; set; }

        public int SubjectID { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}