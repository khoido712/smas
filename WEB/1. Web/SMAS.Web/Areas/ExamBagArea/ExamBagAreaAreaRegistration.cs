﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamBagArea
{
    public class ExamBagAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamBagArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamBagArea_default",
                "ExamBagArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
