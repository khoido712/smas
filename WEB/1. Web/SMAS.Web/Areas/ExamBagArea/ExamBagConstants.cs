﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamBagArea
{
    public class ExamBagConstants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";
        public const string BTN_COMMON_ENABLE = "BtnCommonEnable";
        public const string IS_CHECK_DETACHABLE = "IsCheckDetachable";

        public const string LIST_EXAM_BAG = "ListExamBag";
        public const string LIST_EXAMINATIONS = "ListExaminations";
        public const string LIST_EXAM_GROUP = "ListExamGroup";
        public const string LIST_EXAM_SUBJECT = "ListExamSubject";

        public const string LIST_CRITERIA = "ListCriteria";
        public const string LIST_CHECK = "ListCheck";

        public const int CRITERIA_EXAM_GROUP_CODE = 1;
        public const int CRITERIA_EXAM_SUBJECT_CODE = 2;
        public const int CRITERIA_LENGTH_CODE = 3;

        public const int CHECK_CURRENT_SUBJECT = 1;
        public const int CHECK_CURRENT_GROUP = 2;
        public const int CHECK_ALL_GROUP_AND_SUBJECT = 3;
    }
}