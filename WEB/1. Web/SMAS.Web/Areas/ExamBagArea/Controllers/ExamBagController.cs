﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using System.Collections.Generic;
using SMAS.Web.Areas.ExamBagArea.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ExamBagArea.Controllers
{
    public class ExamBagController : BaseController
    {
        private readonly IExamBagBusiness ExamBagBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamCandenceBagBusiness ExamCandenceBagBusiness;
        private readonly IExamDetachableBagBusiness ExamDetachableBagBusiness;

        public ExamBagController(IExamBagBusiness ExamBagBusiness,
            IExaminationsBusiness ExaminationsBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness,
            IExamCandenceBagBusiness ExamCandenceBagBusiness,
            IExamDetachableBagBusiness ExamDetachableBagBusiness)
        {
            this.ExamBagBusiness = ExamBagBusiness;
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamCandenceBagBusiness = ExamCandenceBagBusiness;
            this.ExamDetachableBagBusiness = ExamDetachableBagBusiness;
        }

        public ActionResult Index()
        {
            SetViewModel();
            return View();
        }

        #region SetViewModel

        public void SetViewModel()
        {
            SetViewDataPermission("ExamBag", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            ViewData[ExamBagConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExamBagConstants.CURRENT_SEMESTER] = _globalInfo.Semester;

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamBagConstants.LIST_EXAMINATIONS] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");
            
            if (listExam != null && listExam.Count > 0)
            {
                Examinations exam = listExam.FirstOrDefault();
                search["ExaminationsID"] = exam.ExaminationsID;
                List<ExamGroup> listGroup = GetExamGroupFromExaminations(exam.ExaminationsID);

                if (listGroup != null && listGroup.Count > 0)
                {
                    ViewData[ExamBagConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");
                    ExamGroup group = listGroup.FirstOrDefault();
                    List<ExamSubjectBO> listSubject = this.GetExamSubjectFromExamGroup(group.ExamGroupID);
                    ViewData[ExamBagConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");

                    search["ExamGroupID"] = group.ExamGroupID;
                    if (listSubject != null && listSubject.Count > 0)
                    {
                        search["SubjectID"] = listSubject.FirstOrDefault().SubjectID;
                        // Nếu có đủ dữ liệu thì thực hiện tìm kiếm phòng thi và túi phách tương ứng
                        List<ExamBagViewModel> list = this._Search(search);
                        ViewData[ExamBagConstants.LIST_EXAM_BAG] = list;
                    }
                    else
                    {
                        ViewData[ExamBagConstants.LIST_EXAM_BAG] = new List<ExamBagViewModel>();
                    }
                }
                else
                {
                    ViewData[ExamBagConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                    ViewData[ExamBagConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    ViewData[ExamBagConstants.LIST_EXAM_BAG] = new List<ExamBagViewModel>();
                    ViewData[ExamBagConstants.IS_CHECK_DETACHABLE] = false;
                }

                if (_globalInfo.IsCurrentYear == true && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
                {
                    ViewData[ExamBagConstants.BTN_COMMON_ENABLE] = true;
                }
                else
                {
                    ViewData[ExamBagConstants.BTN_COMMON_ENABLE] = false;
                }
            }
            else
            {
                ViewData[ExamBagConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                ViewData[ExamBagConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                ViewData[ExamBagConstants.LIST_EXAM_BAG] = new List<ExamBagViewModel>();
                ViewData[ExamBagConstants.BTN_COMMON_ENABLE] = false;
                ViewData[ExamBagConstants.IS_CHECK_DETACHABLE] = false;
            }
        }

        #endregion SetViewModel

        #region LoadCombobox

        private List<ExamGroup> GetExamGroupFromExaminations(long ExaminationsID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID", ExaminationsID}
            };

            List<ExamGroup> listGroup = ExamGroupBusiness.Search(dic).OrderBy(o => o.ExamGroupCode).ToList();
            return listGroup;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamGroup(long? ExaminationsID)
        {
            if (ExaminationsID.HasValue)
            {
                IEnumerable<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID.Value);
                return Json(new SelectList(listGroup, "ExamGroupID", "ExamGroupName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private List<ExamSubjectBO> GetExamSubjectFromExamGroup(long ExamGroupID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExamGroupID", ExamGroupID}
            };

            List<ExamSubjectBO> listSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();
            return listSubject;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamSubject(long? ExamGroupID)
        {
            if (ExamGroupID.HasValue)
            {
                IEnumerable<ExamSubjectBO> listSubject = GetExamSubjectFromExamGroup(ExamGroupID.Value);
                return Json(new SelectList(listSubject, "SubjectID", "SubjectName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion LoadCombobox

        #region Search

        public PartialViewResult AjaxLoadExamBag(long ExaminationsID, long ExamGroupID, int SubjectID)
        {
            ViewData[ExamBagConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            SetViewDataPermission("ExamBag", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            if (_globalInfo.IsCurrentYear == true && exam != null && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                ViewData[ExamBagConstants.BTN_COMMON_ENABLE] = true;
            }
            else
            {
                ViewData[ExamBagConstants.BTN_COMMON_ENABLE] = false;
            }

            if (ExaminationsID != 0 && ExamGroupID != 0 && SubjectID != 0)
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("ExaminationsID", ExaminationsID);
                search.Add("ExamGroupID", ExamGroupID);
                search.Add("SubjectID", SubjectID);

                List<ExamBagViewModel> list = this._Search(search);
                ViewData[ExamBagConstants.LIST_EXAM_BAG] = list;
            }
            else
            {
                ViewData[ExamBagConstants.LIST_EXAM_BAG] = new List<ExamBagViewModel>();
            }

            return PartialView("_List");
        }

        
        public PartialViewResult Search(SearchViewModel form)
        {
            ViewData[ExamBagConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            SetViewDataPermission("ExamBag", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            Examinations exam = ExaminationsBusiness.Find(form.Examinations);
            if (_globalInfo.IsCurrentYear == true && exam != null && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                ViewData[ExamBagConstants.BTN_COMMON_ENABLE] = true;
            }
            else
            {
                ViewData[ExamBagConstants.BTN_COMMON_ENABLE] = false;
            }

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("ExaminationsID", form.Examinations);
            search.Add("ExamGroupID", form.ExamGroup);
            search.Add("SubjectID", form.ExamSubject);

            List<ExamBagViewModel> list = this._Search(search);
            ViewData[ExamBagConstants.LIST_EXAM_BAG] = list;

            return PartialView("_List");
        }

        private List<ExamBagViewModel> _Search(IDictionary<string, object> search)
        {
            // Kiem tra da danh phach
            ExamCandenceBag checkCandence = ExamCandenceBagBusiness.Search(search).FirstOrDefault();
            ExamDetachableBag checkDetachable = ExamDetachableBagBusiness.Search(search).FirstOrDefault();
            ViewData[ExamBagConstants.IS_CHECK_DETACHABLE] = checkCandence != null && checkDetachable != null ? true : false;

            List<ExamBagViewModel> listExamRoom = ExamBagBusiness.GetListExamBag(search)
                .Select(o => new ExamBagViewModel
                {
                    ExamBagID = o.ExamBagID,
                    ExamBagCode = o.ExamBagCode,
                    ExaminationsID = o.ExaminationsID,
                    ExamGroupID = o.ExamGroupID,
                    ExamRoomID = o.ExamRoomID,
                    ExamRoomCode = o.ExamRoomCode,
                    SubjectID = o.SubjectID

                }).OrderBy(o => o.ExamRoomCode).ToList();

            return listExamRoom;
        }

        #endregion Search

        #region CheckExamBag

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult CheckExamBag(long? HiddenExaminations, long? HiddenExamGroup, int? HiddenExamSubject,
            int[] Criteria, int? CriteriaIIIText, int CheckGroup)
        {
            if (GetMenupermission("ExamBag", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (HiddenExaminations.HasValue && HiddenExamGroup.HasValue && HiddenExamSubject.HasValue)
            {
                int LenghtCode = 0;
                if (CriteriaIIIText.HasValue)
                {
                    LenghtCode = CriteriaIIIText.Value;
                }

                ExamBagBusiness.CheckExamBag(HiddenExaminations.Value, HiddenExamGroup.Value, HiddenExamSubject.Value, Criteria, LenghtCode, CheckGroup);

                // Ghi log hành động đánh mã túi thi
                SetViewDataActionAudit(String.Empty, String.Empty
                    , HiddenExamGroup.ToString()
                    , Res.Get("ExamBag_Title_Page")
                    , "Examinations: " + HiddenExaminations + ", ExamGroup: " + HiddenExamGroup + ", ExamSubject: " + HiddenExamSubject
                    , Res.Get("ExamBag_Title_Page")
                    , SMAS.Business.Common.GlobalConstants.ACTION_ADD
                    , string.Format(Res.Get("ExamBag_WriteLog_UserDescription_Update"), HiddenExamSubject, HiddenExamGroup, HiddenExaminations));
            }

            return Json(new JsonMessage(Res.Get("Common_CheckExamBag_Message")));
        }

        #endregion CheckExamBag

        #region DeleteCheck

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCheck(string stringExamBag, string ExaminationsID, string ExamGroupID, string SubjectID)
        {
            if (GetMenupermission("ExamBag", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            string[] listDelete = stringExamBag.Split(',');
            List<long> listExamBag = new List<long>();
            long tempID = 0;
            for (int i = 0; i < listDelete.Length - 1; i++)
            {
                if(long.TryParse(listDelete[i], out tempID) && tempID != 0)
                {
                    listExamBag.Add(long.Parse(listDelete[i]));
                }
            }
            ExamBagBusiness.DeleteExamBag(listExamBag);

            // Ghi log hanh dong xoa
            string userDescription = string.Format(Res.Get("ExamBag_WriteLog_UserDescription_Delete"), SubjectID, ExamGroupID, ExaminationsID);
            SetViewDataActionAudit(String.Empty, String.Empty
                , ExamGroupID
                , Res.Get("ExamBag_WriteLog_Delete")
                , Res.Get("ExamBag_WriteLog_Delete") + " - ExaminationsID: " + ExaminationsID + ", ExamGroupID: " + ExamGroupID + ", SubjectID: " + SubjectID
                , Res.Get("ExamBag_Title_Page")
                , SMAS.Business.Common.GlobalConstants.ACTION_DELETE
                , userDescription);

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion DeleteCheck
    }
}
