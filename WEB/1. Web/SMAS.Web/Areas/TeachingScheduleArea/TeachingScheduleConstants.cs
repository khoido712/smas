﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeachingScheduleArea
{
    public class TeachingScheduleConstants
    {
        public const string LIST_WEEKNAME = "ListWeekName";
        public const string LIST_SECTION = "ListSection";
        public const string LIST_TEACHER = "ListTeacher";
        public const string LIST_TEACHING_SCHEDULE = "ListTeachingSchedule";
        public const string LIST_SECTION_GRID = "ListSectionGrid";
        public const string LIST_SUBJECT = "ListSubject";
        public const string LIST_ASSIGN_SUBJECT = "ListAssignSubject";
        public const string LIST_CLASS_PROFILE = "ListClassProfile";
        public const string LIST_TEACHING_SCHEDULE_ORDER = "ListTeachingScheduleOrder";
        public const string LIST_DATE = "ListDate";
        public const string SECTION_ID = "SectionID";
        public const string SUBJECT_ORDER = "SubjectOrder";
        public const string OBJ_TEACHING_SCHEDULE = "ObjTeachingSchedule";
        public const string IS_TEACHER = "IsTeacher";
        public const string IS_ADMIN = "IsAdmin";
        public const string TEACHER_ID = "TeacherID";
        public const string LOGGED_TEACHER_ID = "LoggedTeacherID";
        public const string IS_RESULT = "IsResult";
        public const string TEMPLATE_FILE = "PhieuBaoGiangTuan.xls";
        public const string TEMPLATE_FILE_BOOK_TS = "LichBaoGiang_NguyenVanA_20102017.xls";
        public const string SCHOOL_WEEK_NAME = "SchoolWeekName";
        public const string IS_ENABLED = "IsEnabled";
        public const string ENABLED_BUTTON = "EnabledButton";
        public const string TS_ID = "TeachingScheduleID";
        public const string REPORT_CODE = "SoLichBaoGiang";

        public const int TEACHING_SCHEDULE_APPROVAL = 1;
        public const int TEACHING_SCHEDULE_REJECT = 2;
        public const string TEACHING_SCHEDULE_PERMISSION_APPROVAL = "permission_approval";
        public const string TEACHING_SCHEDULE_LOG_APPROVAL = "string_log_approval";
        public const string TEACHING_SCHEDULE_SHOW_APPROVAL = "show_button_approval";
        public const string TEACHING_SCHEDULE_APPROVALED = "permission_approvaled";
        public const string APPLIEd_APPROVALED = "APPLIEd_APPROVALED";

    }
}