﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TeachingScheduleArea
{
    public class TeachingScheduleAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TeachingScheduleArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TeachingScheduleArea_default",
                "TeachingScheduleArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
