﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.TeachingScheduleArea.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Web.Utils;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.TeachingScheduleArea.Controllers
{
    public class TeachingScheduleController : BaseController
    {
        private readonly ITeachingScheduleBusiness TeachingScheduleBusiness;
        private readonly ITeachingScheduleApprovalBusiness TeachingScheduleApprovalBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IDistributeProgramBusiness DistributeProgramBusiness;
        private readonly ISchoolWeekBusiness SchoolWeekBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IAssignSubjectConfigBusiness AssignSubjectConfigBusiness;
        private readonly ICalendarBusiness CalendarBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IDistributeProgramSystemBusiness DistributeProgramSystemBusiness;

        //private static List<WeekDefaultViewModel> ListWeekDefault;

        public TeachingScheduleController(ITeachingScheduleBusiness teachingScheduleBusiness, IAcademicYearBusiness academicYearBusiness,
                                            IEmployeeBusiness employeeBusiness, IClassProfileBusiness classProfileBusiness, ISubjectCatBusiness subjectCatBusiness,
                                            ITeachingAssignmentBusiness teachingAssignmentBusiness, IDistributeProgramBusiness distributeProgramBusiness,
                                            ISchoolWeekBusiness schoolWeekBusiness, ISchoolSubjectBusiness schoolSubjectBusiness, IAssignSubjectConfigBusiness assignSubjectConfigBusiness,
            ICalendarBusiness calendarBusiness, ITeachingScheduleApprovalBusiness teachingScheduleApprovalBusiness,
            ISchoolFacultyBusiness schoolFacultyBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness,
            IDistributeProgramSystemBusiness DistributeProgramSystemBusiness)
        {
            this.TeachingScheduleBusiness = teachingScheduleBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.DistributeProgramBusiness = distributeProgramBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.SchoolWeekBusiness = schoolWeekBusiness;
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.AssignSubjectConfigBusiness = assignSubjectConfigBusiness;
            this.CalendarBusiness = calendarBusiness;
            this.TeachingScheduleApprovalBusiness = teachingScheduleApprovalBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.DistributeProgramSystemBusiness = DistributeProgramSystemBusiness;
        }

        /// <summary>
        /// Trang chủ
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }

        /// <summary>
        /// Tìm kiếm lịch báo giảng trong tuần
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public PartialViewResult Search(FormCollection frm)
        {
            string WeekIDDate = !string.IsNullOrEmpty(frm["WeekID"]) ? frm["WeekID"] : string.Empty;
            int SectionID = !string.IsNullOrEmpty(frm["SectionID"]) ? Int32.Parse(frm["SectionID"]) : 0;
            int TeacherID = 0; Int32.TryParse(frm["EmployeeID"] ?? "0", out TeacherID);

            List<string> lstFromTo = WeekIDDate.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];

            CheckPermissionButtonApproval(TeacherID);
            ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_SHOW_APPROVAL] = true;
            GetTeachingScheduleApprovalCustom(fromDate, TeacherID);

            List<TeachingScheduleViewModel> lstResult = new List<TeachingScheduleViewModel>();

            #region [Get lstResult]
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"lstFromTo",lstFromTo},
                {"SectionID",SectionID},
                {"TeacherID", TeacherID}
            };

            IQueryable<TeachingSchedule> queryTeachingSchedule = TeachingScheduleBusiness.Search(dic);
            lstResult = ListTeachingScheduleVM(queryTeachingSchedule);
            #endregion

            TeachingScheduleViewModel objTS = null;

            //SchooWeek
            /*IQueryable<SchoolWeek> iqWeek =
                SchoolWeekBusiness.All.Where(s => s.AcademicYearID == _globalInfo.AcademicYearID);
            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                DateTime fromDate1 = Convert.ToDateTime(fromDate);
                DateTime toDate1 = Convert.ToDateTime(toDate);
                iqWeek = iqWeek.Where(s => s.FromDate >= fromDate1 && s.FromDate <= toDate1);

            }*/

            IDictionary<string, object> dicDP = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID}
                //{"SchoolWeekID",weekID}
            };

            DistributeProgram objDP = null;

            List<DistributeProgram> lstDistributeProgram = (from dp in DistributeProgramBusiness.Search(dicDP)
                                                            select dp).ToList();
            for (int i = 0; i < lstResult.Count; i++)
            {
                objTS = lstResult[i];
                if (objTS.AssignSubjectID.HasValue)
                {
                    objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTS.SubjectID
                    && p.AssignSubjectID == objTS.AssignSubjectID
                    && p.EducationLevelID == objTS.EducationLevelID
                    && p.ClassID == objTS.ClassID
                    //&& p.SchoolWeekID == objTS.SchoolWeekID
                    && p.DistributeProgramOrder == objTS.TeachingScheduleOrder);
                }
                else
                {
                    objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTS.SubjectID
                    && p.AssignSubjectID.HasValue == false
                    && p.ClassID == objTS.ClassID
                    && p.EducationLevelID == objTS.EducationLevelID
                    //&& p.SchoolWeekID == objTS.SchoolWeekID
                    && p.DistributeProgramOrder == objTS.TeachingScheduleOrder);
                }


                if (objDP != null)
                {
                    objTS.LessonName = objDP.LessonName;
                }
            }

            ViewData[TeachingScheduleConstants.LIST_TEACHING_SCHEDULE] = lstResult;
            //tinh ra cac thu theo tuan
            //SchoolWeek objSW = SchoolWeekBusiness.Find(weekID);
            DateTime input = Convert.ToDateTime(fromDate); //objSW.FromDate.Value;
            int delta = DayOfWeek.Monday - input.DayOfWeek;
            List<DateName> lstDate = new List<DateName>();
            DateName objDate = null;
            DateTime valtmp = Convert.ToDateTime(fromDate); //objSW.FromDate.Value;
            DateTime toDatetime = Convert.ToDateTime(toDate);
            while (valtmp <= toDatetime)
            {
                objDate = new DateName();
                objDate.Date = input.AddDays(delta);
                objDate.Name = this.GetDateName(input.AddDays(delta).DayOfWeek.ToString()) + " (" + input.AddDays(delta).ToString("dd/MM/yyyy") + ")";
                lstDate.Add(objDate);
                valtmp = valtmp.AddDays(1);
                delta++;
            };
            ViewData[TeachingScheduleConstants.LIST_DATE] = lstDate;
            List<int> lstSectionID = new List<int>();
            if (SectionID > 0)
            {
                lstSectionID.Add(SectionID);
            }
            else
            {
                //tinh ra so buoi lon nhat
                List<ClassProfile> lstCP = ListClassProfile();
                //int maxSectionID = lstCP.Count > 0 ? lstCP.Max(p => p.Section).Value : 0;
                lstSectionID = UtilsBusiness.GetListSectionID(lstCP);
            }
            ViewData[TeachingScheduleConstants.LIST_SECTION_GRID] = lstSectionID;
            ViewData[TeachingScheduleConstants.TEACHER_ID] = TeacherID;

            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>()
            {
                {"CurrentSchoolID", _globalInfo.SchoolID}
            };

            ViewData[TeachingScheduleConstants.IS_TEACHER] = _globalInfo.EmployeeID.HasValue && _globalInfo.EmployeeID.Value > 0 &&
                                                        (from q in EmployeeBusiness.Search(dicToGetTeacher)
                                                         where (q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN) && q.EmployeeID == TeacherID
                                                         select q.EmployeeID).Any();

            this.SetViewDataPermission("TeachingSchedule", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime datetimeNow = DateTime.Now;
            bool enabledButton = (objAca.FirstSemesterStartDate <= datetimeNow && datetimeNow < objAca.FirstSemesterEndDate)
                                    || (objAca.SecondSemesterStartDate <= datetimeNow && datetimeNow <= objAca.SecondSemesterEndDate);
            // Trong năm học, và giáo viên chính là người đang đăng nhập
            ViewData[TeachingScheduleConstants.ENABLED_BUTTON] = enabledButton && TeacherID == _globalInfo.EmployeeID;
            return PartialView("_ListTeachingSchedule");
        }

        /// <summary>
        /// Tải view hiển thị để chỉnh sửa, hoặc thêm mới lịch báo giảng
        /// </summary>
        /// <param name="SchoolWeekID"></param>
        /// <param name="ScheduleDate"></param>
        /// <param name="SectionID"></param>
        /// <param name="SubjectOrder"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        public PartialViewResult LoadEditTeachingSchedule(int teachingScheduleID, string SchoolWeekID, string ScheduleDate, int SectionID, int SubjectOrder, int? TeacherID, int? TeachingScheduleID)
        {
            List<string> fomDateToDate = SchoolWeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = fomDateToDate[0];
            //SchoolWeek objSW = SchoolWeekBusiness.Find(SchoolWeekID);
            int academicYearId = _globalInfo.AcademicYearID.Value;
            DateTime DateTeachingSchedule = Convert.ToDateTime(ScheduleDate);
            TeachingScheduleBO objTSBO = (from t in TeachingScheduleBusiness.All
                                          join cp in ClassProfileBusiness.All on t.ClassID equals cp.ClassProfileID
                                          join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                                          where t.AcademicYearID == academicYearId
                                          && t.TeachingScheduleID == teachingScheduleID
                                          && t.SchoolID == _globalInfo.SchoolID
                                          && t.Last2DigitNumberSchool == (_globalInfo.SchoolID % 100)
                                          && t.ScheduleDate == DateTeachingSchedule
                                          && t.SectionID == SectionID
                                          && t.SubjectOrder == SubjectOrder
                                          && t.TeacherID == TeacherID
                                          && cp.IsActive.Value
                                          && sc.AppliedLevel == _globalInfo.AppliedLevel
                                          && cp.AcademicYearID == academicYearId
                                          select new TeachingScheduleBO
                                          {
                                              TeachingScheduleID = t.TeachingScheduleID,
                                              ScheduleDate = t.ScheduleDate,
                                              SectionID = t.SectionID,
                                              ClassID = t.ClassID,
                                              SubjectID = t.SubjectID,
                                              SubjectOrder = t.SubjectOrder,
                                              TeachingScheduleOrder = t.TeachingScheduleOrder,
                                              TeachingUtensil = t.TeachingUtensil,
                                              Quantity = t.Quantity,
                                              InRoom = t.InRoom,
                                              IsSelfMade = t.IsSelfMade,
                                              EducationLevelID = cp.EducationLevelID,
                                              AssignSubjectID = t.AssignSubjectID,
                                              Status = t.Status
                                          }).FirstOrDefault();

            DistributeProgram objDP = null;
            if (objTSBO != null)
            {
               
                IDictionary<string, object> dicDP = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID}
                };
                var queryDistribute = DistributeProgramBusiness.Search(dicDP);

                
                if (objTSBO.AssignSubjectID.HasValue)
                {
                    objDP = queryDistribute.FirstOrDefault(p => p.SubjectID == objTSBO.SubjectID
                    && p.AssignSubjectID == objTSBO.AssignSubjectID
                    && p.EducationLevelID == objTSBO.EducationLevelID
                    && p.DistributeProgramOrder == objTSBO.TeachingScheduleOrder);
                }
                else
                {
                    objDP = queryDistribute.FirstOrDefault(p => p.SubjectID == objTSBO.SubjectID
                    && p.AssignSubjectID.HasValue == false
                    && p.EducationLevelID == objTSBO.EducationLevelID
                    && p.DistributeProgramOrder == objTSBO.TeachingScheduleOrder);
                }
            }

            if (objDP != null)
            {
                objTSBO.LessionName = objDP.LessonName;
            }
            //tinh ra so buoi lon nhat
            List<ClassProfile> lstCP = ListClassProfile().OrderBy(x => x.OrderNumber).ToList();

            //int maxSectionID = lstCP.Count > 0 ? lstCP.Max(p => p.Section).Value : 0;
            ViewData[TeachingScheduleConstants.LIST_SECTION] = new SelectList(this.GetSectionName(lstCP), "key", "value", SectionID);

            //danh sach thu cua tuan
            DateTime tmpDate = new DateTime();
            int delta = DayOfWeek.Monday - tmpDate.DayOfWeek;
            List<DateName> lstDateName = new List<DateName>();
            for (int i = 1; i <= 7; i++)
            {
                DateName objDateName = new DateName();
                tmpDate = DateTeachingSchedule.AddDays(delta);
                objDateName.Date = tmpDate;
                objDateName.Name = this.GetDateName(tmpDate.DayOfWeek.ToString()) + " (" + tmpDate.ToString("dd/MM/yyyy") + ")";
                lstDateName.Add(objDateName);
                delta = delta + 1;
            }
            ViewData[TeachingScheduleConstants.LIST_DATE] = new SelectList(lstDateName, "Date", "Name");
            //lay danh sach mon toan truong
            List<SubjectCatBO> lstSubjectCat = new List<SubjectCatBO>();
            IDictionary<string, object> dicS = new Dictionary<string, object>()
            {
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID}
            };
            lstSubjectCat = (from s in SchoolSubjectBusiness.Search(dicS)
                             join sc in SubjectCatBusiness.All on s.SubjectID equals sc.SubjectCatID
                             where s.SectionPerWeekFirstSemester.HasValue && s.SectionPerWeekFirstSemester.Value > 0
                             && s.SectionPerWeekSecondSemester.HasValue && s.SectionPerWeekSecondSemester.Value > 0
                             && sc.AppliedLevel == _globalInfo.AppliedLevel
                             select new SubjectCatBO
                             {
                                 SubjectCatID = s.SubjectID,
                                 SubjectName = sc.SubjectName,
                                 OrderInSubject = sc.OrderInSubject
                             }).Distinct().OrderBy(p => p.OrderInSubject).ToList();
            //lay danh sach mon duoc phan cong giang day
            IDictionary<string, object> dicSubjectAssign = new Dictionary<string, object>()
            {
                {"TeacherID",TeacherID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel}
            };
            List<TeachingAssignmentBO> lstTeachingAssign = TeachingAssignmentBusiness.GetSubjectTeacher(_globalInfo.SchoolID.Value, dicSubjectAssign).ToList();

            List<SubjectCatBO> lstSubjectAssign = (from a in lstTeachingAssign
                                                   select new SubjectCatBO
                                                   {
                                                       SubjectCatID = a.SubjectID.Value,
                                                       SubjectName = a.SubjectName,
                                                       OrderInSubject = a.OrderInSubject,
                                                   }).OrderBy(p => p.OrderInSubject).ToList();
            List<int> lstSubjectAssignID = lstTeachingAssign.Select(p => p.SubjectID.Value).Distinct().ToList();
            List<int> lstClassAssignID = lstTeachingAssign.Select(p => p.ClassID.Value).Distinct().ToList();
            lstSubjectCat = lstSubjectCat.Where(p => !lstSubjectAssignID.Contains(p.SubjectCatID)).ToList();

            IDictionary<int, string> dicSubjectName = new Dictionary<int, string>();
            lstSubjectAssign.ForEach(p => dicSubjectName[p.SubjectCatID] = p.SubjectName);
            lstSubjectCat.ForEach(p => dicSubjectName[p.SubjectCatID] = p.SubjectName);

            ViewData[TeachingScheduleConstants.LIST_SUBJECT] = new SelectList(dicSubjectName, "Key", "Value", objTSBO != null ? objTSBO.SubjectID : -1);

            #region [Get Assign Subject]
            if (objTSBO != null)
            {
                ViewData[TeachingScheduleConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(objTSBO.SubjectID), "AssignSubjectConfigID", "AssignSubjectName", objTSBO.AssignSubjectID.HasValue ? objTSBO.AssignSubjectID.Value : -1);
            }
            else
            {
                ViewData[TeachingScheduleConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(), "AssignSubjectConfigID", "AssignSubjectName");
            }
            #endregion

            List<ClassProfile> lstCPAssign = (from a in lstTeachingAssign
                                              select new ClassProfile
                                              {
                                                  ClassProfileID = a.ClassID.Value,
                                                  DisplayName = a.ClassName
                                              }).OrderBy(p => p.DisplayName).ToList();
            IDictionary<int, string> dicClassAssign = new Dictionary<int, string>();
            lstCPAssign.ForEach(p => dicClassAssign[p.ClassProfileID] = p.DisplayName);
            lstCP = lstCP.Where(p => !lstClassAssignID.Contains(p.ClassProfileID)).Distinct().ToList();
            lstCP.ForEach(p => dicClassAssign[p.ClassProfileID] = p.DisplayName);
            ViewData[TeachingScheduleConstants.LIST_CLASS_PROFILE] = new SelectList(dicClassAssign, "Key", "Value", objTSBO != null ? objTSBO.ClassID : -1);
            ViewData[TeachingScheduleConstants.SECTION_ID] = SectionID;
            ViewData[TeachingScheduleConstants.SUBJECT_ORDER] = SubjectOrder;
            ViewData[TeachingScheduleConstants.OBJ_TEACHING_SCHEDULE] = objTSBO;
            ViewData[TeachingScheduleConstants.IS_RESULT] = objTSBO != null ? true : false;

            ViewData[TeachingScheduleConstants.SCHOOL_WEEK_NAME] = "";

            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<WeekDefaultViewModel> ListWeekDefault = AutoGenericWeekDefaul(academicYear);

            var objWeekDefault = ListWeekDefault.Where(x => x.FromDate == fromDate).FirstOrDefault();
            if (objWeekDefault != null)
            {
                ViewData[TeachingScheduleConstants.SCHOOL_WEEK_NAME] = string.Format("Từ {0} đến {1}", fromDate, objWeekDefault.ToDate);
            }
            ViewData[TeachingScheduleConstants.TS_ID] = TeachingScheduleID;

            dicSubjectAssign["AppliedLevelID"] = _globalInfo.AppliedLevel.Value;
            dicSubjectAssign["FromDate"] = fromDate;
            dicSubjectAssign["SchoolID"] = _globalInfo.SchoolID.Value;

            var objApproval = this.CheckApproval(dicSubjectAssign);

            ViewData[TeachingScheduleConstants.APPLIEd_APPROVALED] = false;
            if (objApproval != null)
            {
                if (objApproval.Status == TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVAL)
                {
                    ViewData[TeachingScheduleConstants.APPLIEd_APPROVALED] = true;
                }
            }

            return PartialView("_EditTeachingSchedule");
        }

        /// <summary>
        /// Chỉnh sửa lịch báo giảng (lưu dữ liệu vào CSDL)
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        [ValidateInput(true)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditTeachingSchedule(FormCollection frm)
        {
            int TeacherID = !string.IsNullOrEmpty(frm["hdfTeacherID"]) ? Int32.Parse(frm["hdfTeacherID"]) : 0;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime datetimeNow = DateTime.Now;
            bool enabledButton = (objAca.FirstSemesterStartDate <= datetimeNow && datetimeNow < objAca.FirstSemesterEndDate)
                                    || (objAca.SecondSemesterStartDate <= datetimeNow && datetimeNow <= objAca.SecondSemesterEndDate);
            bool isEdit = enabledButton && TeacherID == _globalInfo.EmployeeID;
            if (!isEdit)
            {
                throw new BusinessException(Res.Get("Comment_Update_Failt"));
            }
            DateTime ScheduleDate = !string.IsNullOrEmpty(frm["ScheduleDateID"]) ? Convert.ToDateTime(frm["ScheduleDateID"]) : new DateTime();
            TeachingSchedule objTS = new TeachingSchedule();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"TeacherID",TeacherID},
                {"ScheduDate",ScheduleDate}
            };

            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? Int32.Parse(frm["ClassID"]) : 0;
            List<string> lstFromDateToDate = new List<string>();
            if (!string.IsNullOrEmpty(frm["hdfSchoolWeekID"]))
            {
                lstFromDateToDate = frm["hdfSchoolWeekID"].Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();

                IDictionary<string, object> dicSW = new Dictionary<string, object>();
                dicSW.Add("SchoolID", _globalInfo.SchoolID);
                dicSW.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicSW.Add("ClassID", classID);
                dicSW.Add("fromDateToDate", frm["hdfSchoolWeekID"]);
                SchoolWeek objSW = SchoolWeekBusiness.Search(dicSW).FirstOrDefault();

                if (objSW != null)
                    objTS.SchoolWeekID = (int)objSW.SchoolWeekID;
                else
                    objTS.SchoolWeekID = 0;
            }
            else
            {
                objTS.SchoolWeekID = 0;
            }

            objTS.TeachingScheduleID = !string.IsNullOrEmpty(frm["TeachingScheduleID"]) ? Int32.Parse(frm["TeachingScheduleID"]) : 0;
            objTS.DayOfWeek = this.GetDateOfWeek(ScheduleDate.DayOfWeek.ToString());
            objTS.ScheduleDate = ScheduleDate;
            objTS.SectionID = !string.IsNullOrEmpty(frm["SectionID"]) ? Int32.Parse(frm["SectionID"]) : 0;
            objTS.SubjectOrder = !string.IsNullOrEmpty(frm["SubjectOrderID"]) ? Int32.Parse(frm["SubjectOrderID"]) : 0;
            objTS.SubjectID = !string.IsNullOrEmpty(frm["SubjectID"]) ? Int32.Parse(frm["SubjectID"]) : 0;
            objTS.AssignSubjectID = !string.IsNullOrEmpty(frm["AssignSubjectID"]) ? Int32.Parse(frm["AssignSubjectID"]) : 0;
            objTS.ClassID = classID;
            objTS.TeachingScheduleOrder = !string.IsNullOrEmpty(frm["ScheduleOrderID"]) ? Int32.Parse(frm["ScheduleOrderID"]) : 0;
            if ("on".Equals(frm["RegisterID"]))
            {
                objTS.TeachingUtensil = frm["TeachingUtensilID"];
                objTS.Quantity = !string.IsNullOrEmpty(frm["NumberID"]) ? Int32.Parse(frm["NumberID"]) : 0;
            }
            objTS.AcademicYearID = _globalInfo.AcademicYearID.Value;
            objTS.SchoolID = _globalInfo.SchoolID.Value;
            objTS.Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100;
            objTS.TeacherID = TeacherID;
            objTS.InRoom = "on".Equals(frm["InRoomID"]);
            objTS.IsSelfMade = "on".Equals(frm["IsSelfMade"]);
            objTS.Status = !string.IsNullOrEmpty(frm["hdfStatus"]) ? Int32.Parse(frm["hdfStatus"]) : 0;

            if (objTS.SubjectID == 0)
            {
                throw new BusinessException(Res.Get("Teaching_Schedule_CHUA_CHON_MON"));
            }
            if (objTS.ClassID == 0)
            {
                throw new BusinessException(Res.Get("Teaching_Schedule_CHUA_CHON_LOP"));
            }
            if (objTS.TeachingScheduleOrder == 0)
            {
                throw new BusinessException(Res.Get("Teaching_Schedule_CHUA_CHON_PPCT"));
            }
            if (GetTeachingScheduleApproval(objTS.SchoolWeekID, objTS.TeacherID) == true)
            {
                throw new BusinessException("Phân phối chương trình đã được phê duyệt");
            }

            if (objTS.AssignSubjectID == 0) objTS.AssignSubjectID = null;
            TeachingScheduleBusiness.InsertOrUpdate(objTS, dic);
            return Json(new JsonMessage(Res.Get("Teaching_Schedule_CAP_NHAP_OK"), "success"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ScheduleDate"></param>
        /// <param name="SectionID"></param>
        /// <param name="SubjectOrder"></param>
        /// <param name="SubjectID"></param>
        /// <param name="ClassID"></param>
        /// <param name="TeacherID"></param>
        /// <param name="IsAuto"></param>
        /// <returns></returns>
        public JsonResult AjaxLoadSchedule(string ScheduleDate, int SectionID, int SubjectOrder, int SubjectID, int? AssignSubjectID, int ClassID, int TeacherID, bool IsAuto, bool IsGetAll = false)
        {
            DateTime Date = Convert.ToDateTime(ScheduleDate);
            List<int> lstScheduleOrder = new List<int>();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"SubjectID",SubjectID},
                {"ClassID",ClassID}
            };
            var query = TeachingScheduleBusiness.Search(dic);
            List<TeachingSchedule> lstTeachingSchedule = new List<TeachingSchedule>();
            if (AssignSubjectID.HasValue)
            {
                lstTeachingSchedule = query.Where(x => x.AssignSubjectID == AssignSubjectID).ToList();
            }
            else
            {
                lstTeachingSchedule = query.Where(x => x.AssignSubjectID.HasValue == false).ToList();
            }

            lstScheduleOrder = lstTeachingSchedule.Select(p => p.TeachingScheduleOrder).Distinct().ToList();
            int selected = 0;
            selected = lstTeachingSchedule.Where(p => p.ScheduleDate == Date && p.TeacherID == TeacherID && p.SectionID == SectionID && p.SubjectOrder == SubjectOrder
                                                    && p.SubjectID == SubjectID && p.ClassID == ClassID).Select(p => p.TeachingScheduleOrder).FirstOrDefault();
            if (selected != 0)
            {
                lstScheduleOrder.Remove(selected);
            }

            List<ComboObject> lstResult = new List<ComboObject>();
            ComboObject objResult = new ComboObject();
            for (int i = 0; i < 300; i++)
            {
                objResult = new ComboObject();
                objResult.key = (i + 1).ToString();
                objResult.value = (i + 1).ToString();
                lstResult.Add(objResult);
            }

            if (IsGetAll == false)
            {
                lstResult = lstResult.Where(p => !lstScheduleOrder.Contains(int.Parse(p.key))).ToList();
            }
            return Json(new SelectList(lstResult, "key", "value", selected));
        }
        public JsonResult AjaxLoadLessonName(int SubjectID, int? AssignSubjectID, int ClassID, int ScheduleOrder)
        {
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"SubjectID",SubjectID},
                {"EducationLevelID",objCP.EducationLevelID},
                {"DistributeProgramOrder",ScheduleOrder}
            };
            var query = DistributeProgramBusiness.Search(dic).Where(x => x.ClassID == ClassID);
            DistributeProgram objDP = null;
            if (AssignSubjectID.HasValue)
            {
                objDP = query.Where(x => x.AssignSubjectID == AssignSubjectID).FirstOrDefault();
            }
            else
            {
                objDP = query.Where(x => x.AssignSubjectID.HasValue == false).FirstOrDefault();
            }
            string strLessonName = objDP != null ? objDP.LessonName : string.Empty;
            return Json(new JsonMessage(strLessonName, "success"));
        }

        //Ajax Load Phan Mon
        #region [Ajax Load Assign Subject]
        private IEnumerable<AssignSubjectConfig> GetAssignSubjectListBySubjects(List<int> subjects)
        {
            if (subjects.Count > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>() {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"Subjects",subjects},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };

                var assignSubjectList = AssignSubjectConfigBusiness.Search(dic).AsEnumerable();
                if (assignSubjectList != null)
                {
                    return assignSubjectList;
                }
            }
            return new List<AssignSubjectConfig>();
        }
        private IEnumerable<AssignSubjectConfig> GetAssignSubjectListBySubjectID(int? subjectID = null)
        {
            if (subjectID.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>() {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"SubjectID",subjectID},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };

                var assignSubjectList = AssignSubjectConfigBusiness.Search(dic).AsEnumerable();
                if (assignSubjectList != null)
                {
                    return assignSubjectList;
                }
            }
            return new List<AssignSubjectConfig>();
        }
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadAssignSubjectSearch(int? subjectID)
        {
            return Json(new SelectList(GetAssignSubjectListBySubjectID(subjectID), "AssignSubjectConfigID", "AssignSubjectName"));
        }
        #endregion

        private string GetTeacherName(int employeeID)
        {
            EmployeeBusiness.SetAutoDetectChangesEnabled(false);

            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = _globalInfo.SchoolID;
            dicToGetTeacher["EmployeeID"] = employeeID;

            IQueryable<TeacherComboboxViewModel> query = (from q in EmployeeBusiness.Search(dicToGetTeacher)
                                                          where (q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)
                                                          select new TeacherComboboxViewModel
                                                          {
                                                              EmployeeID = q.EmployeeID,
                                                              DisplayName = q.FullName
                                                          });
            var techerEntity = query.FirstOrDefault();
            EmployeeBusiness.SetAutoDetectChangesEnabled(true);

            if (techerEntity != null) return techerEntity.DisplayName;
            return string.Empty;
        }

        private int GetSchoolWeekOrder(int schoolWeekID)
        {
            SchoolWeekBusiness.SetAutoDetectChangesEnabled(false);

            int AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            int SchoolID = _globalInfo.SchoolID.Value;

            var schoolWeekEntiy = SchoolWeekBusiness.All.Where(x => x.SchoolWeekID == schoolWeekID && x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID).FirstOrDefault();
            SchoolWeekBusiness.SetAutoDetectChangesEnabled(true);

            if (schoolWeekEntiy != null)
                return schoolWeekEntiy.OrderWeek;
            return 0;
        }

        private string GetSchoolWeekName(int schoolWeekID)
        {
            SchoolWeekBusiness.SetAutoDetectChangesEnabled(false);

            int AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
            int SchoolID = _globalInfo.SchoolID.Value;

            var schoolWeekEntiy = SchoolWeekBusiness.All.Where(x => x.SchoolWeekID == schoolWeekID && x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID).FirstOrDefault();
            SchoolWeekBusiness.SetAutoDetectChangesEnabled(true);

            if (schoolWeekEntiy != null) return string.Format("({0} - {1})", schoolWeekEntiy.FromDate.Value.ToString("dd/MM/yyyy"), schoolWeekEntiy.ToDate.Value.ToString("dd/MM/yyyy"));
            return string.Empty;
        }

        /// <summary>
        /// Kiểm tra quyền phê duyệt
        /// </summary>
        /// <param name="teacherID">Mã giáo viên từ seach</param>
        private bool CheckPermissionButtonApproval(int teacherID)
        {
            EmployeeBusiness.SetAutoDetectChangesEnabled(false);
            SchoolFacultyBusiness.SetAutoDetectChangesEnabled(false);

            ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_PERMISSION_APPROVAL] = false;
            if (_globalInfo.IsAdmin || _globalInfo.IsAdminSchoolRole || (_globalInfo.EmployeeID.HasValue && UtilsBusiness.HasEmployeeAdminPermission(_globalInfo.EmployeeID.Value)))
            {
                ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_PERMISSION_APPROVAL] = true;
                return true;
            }
            else if (_globalInfo.EmployeeID.HasValue)
            {
                IDictionary<string, object> SearchInfoEmployee = new Dictionary<string, object>() {
                    {"CurrentSchoolID",_globalInfo.SchoolID.Value},
                    {"EmployeeID",teacherID},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value}
                };
                var employeeEntity = EmployeeBusiness.Search(SearchInfoEmployee).FirstOrDefault();
                if (employeeEntity != null && employeeEntity.SchoolFacultyID.HasValue)
                {
                    IDictionary<string, object> SearchInfoSchoolFaculty = new Dictionary<string, object>() {
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"SchoolFacultyID",employeeEntity.SchoolFacultyID.Value}
                };
                    var schoolFacultyEntity = SchoolFacultyBusiness.Search(SearchInfoSchoolFaculty).FirstOrDefault();
                    if (schoolFacultyEntity != null && !string.IsNullOrEmpty(schoolFacultyEntity.HeadMasterID) && schoolFacultyEntity.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString()))
                    {
                        ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_PERMISSION_APPROVAL] = true;
                        return true;
                    }
                }
            }
            return false;
        }

        private bool GetTeachingScheduleApproval(int schoolWeekID, int teacherID)
        {
            bool result = false;
            ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVALED] = false;
            ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_SHOW_APPROVAL] = true;
            IDictionary<string, object> SearchInfoTeachingScheduleApproval = new Dictionary<string, object>() {
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"TeacherID",teacherID},
                    {"SchoolWeekID",schoolWeekID},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };
            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);
            var TeachingScheduleApprovalEntity = TeachingScheduleApprovalBusiness.Search(SearchInfoTeachingScheduleApproval)
                .OrderByDescending(x => x.TeachingScheduleApprovalID).FirstOrDefault();
            if (TeachingScheduleApprovalEntity != null)
            {
                string statusString = "Đã hủy phê duyệt";
                if (TeachingScheduleApprovalEntity.Status == TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVAL)
                {
                    statusString = "Đã phê duyệt";
                    ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVALED] = true;
                    result = true;
                }
                if (TeachingScheduleApprovalEntity.EmployeeID == 0)
                {
                    ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_LOG_APPROVAL] = statusString + " - bởi Quản trị trường lúc " + TeachingScheduleApprovalEntity.CreateDate.ToString("HH:mm dd/MM/yyyy");
                }
                else
                {
                    IDictionary<string, object> SearchInfoEmployee = new Dictionary<string, object>() {
                        {"CurrentSchoolID",_globalInfo.SchoolID.Value},
                        {"EmployeeID",TeachingScheduleApprovalEntity.EmployeeID},
                        {"AcademicYearID",_globalInfo.AcademicYearID.Value}
                    };
                    var employeeEntity = EmployeeBusiness.Search(SearchInfoEmployee).FirstOrDefault();
                    if (employeeEntity != null)
                    {
                        ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_LOG_APPROVAL] = string.Format("{0} - {1} lúc {2}", statusString, employeeEntity.FullName, TeachingScheduleApprovalEntity.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                    }
                }
                if (TeachingScheduleApprovalEntity.Status == TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVAL)
                {
                    ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_SHOW_APPROVAL] = false;
                }
            }
            return result;
        }

        private bool GetTeachingScheduleApprovalCustom(string fromDate, int teacherID)
        {
            bool result = false;
            ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVALED] = false;
            ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_SHOW_APPROVAL] = true;
            IDictionary<string, object> SearchInfoTeachingScheduleApproval = new Dictionary<string, object>() {
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"TeacherID",teacherID},
                    {"FromDate", fromDate},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };

            var objResult = this.CheckApproval(SearchInfoTeachingScheduleApproval);

            if (objResult != null)
            {
                string msg = string.Empty;
                string statusString = "Đã hủy phê duyệt";
                if (objResult.Status == TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVAL)
                {
                    statusString = "Đã phê duyệt";
                    ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVALED] = true;
                    result = true;
                }

                if (objResult.EmployeeID == 0)
                {
                    msg = string.Format("{0}  - bởi Quản trị trường lúc {1}", statusString, objResult.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                    ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_LOG_APPROVAL] = msg;
                }
                else
                {
                    IDictionary<string, object> SearchInfoEmployee = new Dictionary<string, object>() {
                        {"CurrentSchoolID",_globalInfo.SchoolID.Value},
                        {"EmployeeID",objResult.EmployeeID},
                        {"AcademicYearID",_globalInfo.AcademicYearID.Value}
                    };
                    var employeeEntity = EmployeeBusiness.Search(SearchInfoEmployee).FirstOrDefault();
                    if (employeeEntity != null)
                    {
                        msg = string.Format("{0} - {1} lúc {2}", statusString, employeeEntity.FullName, objResult.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                        ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_LOG_APPROVAL] = msg;
                    }
                }
                if (objResult.Status == TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVAL)
                {
                    ViewData[TeachingScheduleConstants.TEACHING_SCHEDULE_SHOW_APPROVAL] = false;
                }
            }
            return result;
        }

        #region [Approval]
        public PartialViewResult GetApprovalTeachingSchedule(string WeekID, int TeacherID)
        {
            List<string> lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];
            string teacherName = GetTeacherName(TeacherID);

            return PartialView("_ApprovalTeachingSchedule",
                new ApprovalTeachingScheduleModel()
                {
                    DisplayNote = string.Format("Thực hiện phê duyệt lịch báo giảng từ {0} đến {1} của giáo viên {2}", fromDate, toDate, teacherName),
                    WeekIDDate = WeekID,
                    StrFromDate = fromDate,
                    TeacherID = TeacherID,
                    TeacherName = teacherName
                });
        }
        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult ApprovalTeachingSchedule(ApprovalTeachingScheduleModel model)
        {
            if (model != null && model.Note != null && model.Note.Length > 510)
            {
                return Json(new JsonMessage("Cột ghi chú lớn hơn 500 ký tự.", "error"));
            }

            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);

            TeachingScheduleApprovalBusiness.Insert(new TeachingScheduleApproval()
            {
                SchoolID = _globalInfo.SchoolID.Value,
                AcademicYearID = _globalInfo.AcademicYearID.Value,
                AppliedLevelID = _globalInfo.AppliedLevel.Value,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                TeacherID = model.TeacherID,
                Status = TeachingScheduleConstants.TEACHING_SCHEDULE_APPROVAL,
                SchoolWeekID = 0,
                FromDate = Convert.ToDateTime(model.StrFromDate),
                Note = model.Note,
                EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0
            });
            TeachingScheduleApprovalBusiness.Save();
            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(true);
            return Json(new JsonMessage("Lưu thành công"));
        }
        #endregion

        #region [Approval Cancel]
        public PartialViewResult GetApprovalCancelTeachingSchedule(string WeekID, int TeacherID)
        {
            List<string> lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];
            string teacherName = GetTeacherName(TeacherID);

            return PartialView("_ApprovalCancelTeachingSchedule",
                new ApprovalCancelTeachingScheduleModel()
                {
                    DisplayNote = string.Format("Thực hiện hủy phê duyệt lịch báo giảng từ {0} đến {1} của giáo viên {2}", fromDate, toDate, teacherName),
                    StrFromDate = fromDate,
                    WeekIDDate = WeekID,
                    TeacherID = TeacherID,
                    TeacherName = teacherName
                });
        }
        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult ApprovalCancelTeachingSchedule(ApprovalCancelTeachingScheduleModel model)
        {
            if (model != null && model.Note != null && model.Note.Length > 510)
            {
                return Json(new JsonMessage("Cột ghi chú lớn hơn 500 ký tự.", "error"));
            }
            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);

            TeachingScheduleApprovalBusiness.Insert(new TeachingScheduleApproval()
            {
                SchoolID = _globalInfo.SchoolID.Value,
                AcademicYearID = _globalInfo.AcademicYearID.Value,
                AppliedLevelID = _globalInfo.AppliedLevel.Value,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                TeacherID = model.TeacherID,
                SchoolWeekID = 0,
                FromDate = Convert.ToDateTime(model.StrFromDate),
                Status = TeachingScheduleConstants.TEACHING_SCHEDULE_REJECT,
                Note = model.Note,
                EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0
            });
            TeachingScheduleApprovalBusiness.Save();

            return Json(new JsonMessage("Lưu thành công"));
        }
        #endregion

        #region [Schedule Auto]
        private int ScheduleAutoByWeekID(IEnumerable<SchoolWeek> lstSchoolWeek, int TeacherID, int? SectionID = null)
        {
            //B1: get calendar theo giao vien
            //B2: get PPCT theo tuan
            //B3: duyet theo dayOfWeek -> Section -> Tiet hoc

            var calendarEntities = new List<CalendarBO>();
            #region [B1]
            int semesterID = 0;
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (dateTimeNow < objAca.FirstSemesterStartDate || (objAca.FirstSemesterEndDate < dateTimeNow && dateTimeNow < objAca.SecondSemesterStartDate) || objAca.SecondSemesterEndDate < dateTimeNow)
            {
                semesterID = 2;
            }
            else
            {
                semesterID = _globalInfo.Semester.Value;
            }

            IDictionary<string, object> searchInfoCalendar = new Dictionary<string, object>() {
                    {"appliedLevel",_globalInfo.AppliedLevel},
                    {"Semester",semesterID},
                    {"EmployeeID",TeacherID},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };
            //searchInfoCalendar["EmployeeID"] = _globalInfo.EmployeeID;

            calendarEntities = CalendarBusiness.SearchByPermissionTeacher(searchInfoCalendar).ToList();
            #endregion

            if (calendarEntities.Count == 0)
            {
                //"Thầy/cô chưa được xếp “Thời khóa biểu” nên không thể lên lịch báo giảng tự động.";
                return 1;
            }

            var distributeProgramEntities = new List<DistributeProgram>();
            #region [B2]
            IDictionary<string, object> searchInfoDistributeProgram = new Dictionary<string, object>() { 
                //{"SubjectID",model.SubjectID},
                //{"EducationLevelID",model.EducationLevelID},
                {"lstSchoolWeekID",lstSchoolWeek.Select(x=>(int)x.SchoolWeekID).ToList()},
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                //{"ClassID",model.ClassID},
                //{"AssignSubjectID",model.AssignSubjectID}
            };
            distributeProgramEntities = DistributeProgramBusiness.Search(searchInfoDistributeProgram).ToList();
            #endregion

            if (distributeProgramEntities.Count == 0)
            {
                //Hệ thống chưa có phân phối chương trình
                return 2;
            }

            #region [B3]
            var lstSectionID = new List<int>();
            #region [Section]
            if (SectionID.HasValue) { lstSectionID.Add(SectionID.Value); }
            else
            {
                //tinh ra so buoi lon nhat               
                List<ClassProfile> lstCP = ListClassProfile();
                lstSectionID = UtilsBusiness.GetListSectionID(lstCP);
            }
            #endregion

            foreach (var itemSchoolWeek in lstSchoolWeek)
            {
                var lstDate = new List<DateOfWeekModel>();

                #region [Check Week Approvaled]
                if (GetTeachingScheduleApproval((int)itemSchoolWeek.SchoolWeekID, TeacherID) == true)
                {
                    continue;
                }
                #endregion

                #region [List Date]
                //tinh ra cac thu theo tuan
                //SchoolWeek objSW = SchoolWeekBusiness.Find(weekID);
                DateTime input = itemSchoolWeek.FromDate.Value;
                int delta = DayOfWeek.Monday - input.DayOfWeek;
                DateTime valtmp = itemSchoolWeek.FromDate.Value;
                while (valtmp <= itemSchoolWeek.ToDate)
                {
                    // if (!input.AddDays(delta).DayOfWeek.ToString().Equals("Sunday"))
                    // {
                    var objDate = new DateOfWeekModel();
                    objDate.Date = input.AddDays(delta);
                    objDate.DateOfWeek = this.GetDateOfWeek(input.AddDays(delta).DayOfWeek.ToString());
                    lstDate.Add(objDate);
                    //  }
                    valtmp = valtmp.AddDays(1);
                    delta++;
                };
                #endregion

                #region [lstDate Loop]
                //bool isSave = false;
                for (int i = 0; i < lstDate.Count; i++)
                {
                    var objDate = lstDate[i];

                    for (int s = 0; s < lstSectionID.Count; s++)
                    {
                        for (int subjectOrder = 1; subjectOrder <= 5; subjectOrder++)
                        {
                            var calendarInfo = calendarEntities.Where(c => c.DayOfWeek == objDate.DateOfWeek && c.Section == lstSectionID[s]
                                && c.SubjectOrder == subjectOrder).FirstOrDefault();
                            if (calendarInfo != null)
                            {
                                #region [Distribute Program Search]
                                var distributeProgramInfo = distributeProgramEntities.Where(d =>
                                    d.ClassID == calendarInfo.ClassID
                                    && d.SchoolWeekID == (int)itemSchoolWeek.SchoolWeekID
                                    && d.SubjectID == calendarInfo.SubjectID)
                                    .OrderBy(D => D.AssignSubjectID)
                                    .ThenBy(d => d.DistributeProgramOrder).FirstOrDefault();

                                if (distributeProgramInfo != null)
                                {
                                    #region [Insert To Teaching Schedule]
                                    var objTS = new TeachingSchedule();
                                    objTS.SchoolID = _globalInfo.SchoolID.Value;
                                    objTS.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                    objTS.SchoolWeekID = (int)itemSchoolWeek.SchoolWeekID;
                                    objTS.ScheduleDate = objDate.Date;
                                    objTS.DayOfWeek = calendarInfo.DayOfWeek;
                                    objTS.SectionID = lstSectionID[s];
                                    objTS.TeacherID = TeacherID;
                                    objTS.SubjectOrder = calendarInfo.SubjectOrder ?? 0;
                                    objTS.TeachingScheduleOrder = distributeProgramInfo.DistributeProgramOrder ?? 0;
                                    objTS.ClassID = distributeProgramInfo.ClassID;
                                    objTS.SubjectID = calendarInfo.SubjectID;
                                    objTS.Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100;
                                    objTS.CreatedTime = DateTime.Now;
                                    objTS.AssignSubjectID = distributeProgramInfo.AssignSubjectID;

                                    TeachingScheduleBusiness.Insert(objTS);
                                    #endregion

                                    distributeProgramEntities.Remove(distributeProgramInfo);
                                }
                                #endregion
                            }
                        }

                        #region [Something Tobe Remove]
                        //var teachingScheduleInfo = (from c in calendarEntities
                        //                            join d in distributeProgramEntities on new { c.ClassID, c.SubjectID } equals new { d.ClassID, d.SubjectID }
                        //                            where c.DayOfWeek == objDate.DateOfWeek && c.Section == lstSectionID[s]
                        //                            orderby c.SubjectOrder
                        //                            select new
                        //                            {
                        //                                ClassID = c.ClassID,
                        //                                SubjectID = c.SubjectID,
                        //                                AssignSubjectID = d.AssignSubjectID,
                        //                                LessonName = d.LessonName,
                        //                                SubjectOrder = c.SubjectOrder,
                        //                                TeachingScheduleOrder = d.DistributeProgramOrder,
                        //                                DayOfWeek = c.DayOfWeek,
                        //                                Section = c.Section
                        //                            }).FirstOrDefault();
                        //if (teachingScheduleInfo != null)
                        //{
                        //    var objTS = new TeachingSchedule();
                        //    objTS.ScheduleDate = objDate.Date;
                        //    objTS.SchoolWeekID = (int)itemSchoolWeek.SchoolWeekID;
                        //    objTS.SectionID = lstSectionID[s];
                        //    objTS.SubjectOrder = teachingScheduleInfo.SubjectOrder ?? 0;
                        //    objTS.SubjectID = teachingScheduleInfo.SubjectID;
                        //    objTS.AssignSubjectID = teachingScheduleInfo.AssignSubjectID;
                        //    objTS.ClassID = teachingScheduleInfo.ClassID;
                        //    objTS.TeachingScheduleOrder = teachingScheduleInfo.TeachingScheduleOrder ?? 0;
                        //    //if ("on".Equals(frm["RegisterID"]))
                        //    //{
                        //    //    objTS.TeachingUtensil = frm["TeachingUtensilID"];
                        //    //    objTS.Quantity = !string.IsNullOrEmpty(frm["NumberID"]) ? Int32.Parse(frm["NumberID"]) : 0;
                        //    //}
                        //    objTS.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        //    objTS.SchoolID = _globalInfo.SchoolID.Value;
                        //    objTS.Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100;
                        //    objTS.TeacherID = TeacherID;
                        //    //objTS.InRoom = "on".Equals(frm["InRoomID"]);
                        //    //objTS.IsSelfMade = "on".Equals(frm["IsSelfMade"]);
                        //    objTS.CreatedTime = DateTime.Now;
                        //    TeachingScheduleBusiness.Insert(objTS);
                        //    //isSave = true;
                        //}
                        #endregion
                    }
                }
                #endregion
            }
            #endregion

            return 0;
        }

        private int ScheduleAutoByWeekDefault(List<WeekDefaultViewModel> lstWeekDefaultVM, List<string> lstWeekDefault, bool IsPresent, int TeacherID, int? SectionID = null)
        {
            // Lên lịch mới
            //B1: get calendar theo giao vien
            //B2: get PPCT theo tuan
            //B3: duyet theo dayOfWeek -> Section -> Tiet hoc

            #region [B1]
            int semesterID = 0;
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (dateTimeNow < objAca.FirstSemesterStartDate
                || (objAca.FirstSemesterEndDate < dateTimeNow && dateTimeNow < objAca.SecondSemesterStartDate)
                || objAca.SecondSemesterEndDate < dateTimeNow)
            {
                semesterID = 2;
            }
            else
            {
                semesterID = _globalInfo.Semester.Value;
            }

            IDictionary<string, object> dicCalendar = new Dictionary<string, object>()
            {
                    {"appliedLevel",_globalInfo.AppliedLevel},
                    {"Semester",semesterID},
                    {"EmployeeID",TeacherID},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
            };

            var lstCalendarByTeacher = CalendarBusiness.SearchByPermissionTeacher(dicCalendar).ToList();
            var objCalendarOrderByDate = lstCalendarByTeacher.OrderByDescending(x => x.StartDate).FirstOrDefault();
            if (objCalendarOrderByDate != null)
            {
                lstCalendarByTeacher = lstCalendarByTeacher.Where(x => x.StartDate == objCalendarOrderByDate.StartDate).ToList();
            }
            List<CalendarBO> lstCalendar = new List<CalendarBO>();

            List<int> lstClassID = lstCalendarByTeacher.Select(x => x.ClassID).Distinct().ToList();
            List<ClassProfile> lstPC = ClassProfileBusiness.All.Where(x => lstClassID.Contains(x.ClassProfileID)).ToList();

            ClassProfile objPC = null;
            List<CalendarBO> lstCalendarNew = new List<CalendarBO>();
            for (int i = 0; i < lstPC.Count(); i++)
            {
                objPC = lstPC[i];

                switch (objPC.Section)
                {
                    case 1:// Sang
                        lstCalendarNew = lstCalendarByTeacher
                            .Where(x => x.ClassID == objPC.ClassProfileID && x.Section == 1)
                            .ToList();
                        break;
                    case 2: // Chieu
                        lstCalendarNew = lstCalendarByTeacher
                           .Where(x => x.ClassID == objPC.ClassProfileID && x.Section == 2)
                           .ToList();
                        break;
                    case 3://Toi
                        lstCalendarNew = lstCalendarByTeacher
                           .Where(x => x.ClassID == objPC.ClassProfileID && x.Section == 3)
                           .ToList();
                        break;
                    case 4:// Sang - chieu
                        lstCalendarNew = lstCalendarByTeacher
                          .Where(x => x.ClassID == objPC.ClassProfileID && (x.Section == 1 || x.Section == 2))
                          .ToList();
                        break;
                    case 5: // Sang - toi
                        lstCalendarNew = lstCalendarByTeacher
                          .Where(x => x.ClassID == objPC.ClassProfileID && (x.Section == 1 || x.Section == 3))
                          .ToList();
                        break;
                    case 6: // Chieu - toi
                        lstCalendarNew = lstCalendarByTeacher
                         .Where(x => x.ClassID == objPC.ClassProfileID && (x.Section == 2 || x.Section == 3))
                         .ToList();
                        break;
                    case 7: // Tat ca
                        lstCalendarNew = lstCalendarByTeacher
                         .Where(x => x.ClassID == objPC.ClassProfileID)
                         .ToList();
                        break;
                    default:
                        break;
                }

                if (lstCalendarNew.Count > 0)
                {
                    lstCalendar.AddRange(lstCalendarNew);
                }
            }

            #endregion

            if (lstCalendar.Count == 0)
            {
                //"Thầy/cô chưa được xếp “Thời khóa biểu” nên không thể lên lịch báo giảng tự động.";
                return 1;
            }

            // Lấy danh sách tuần theo Từ ngày đến ngày
            IDictionary<string, object> dicSW = new Dictionary<string, object>();
            dicSW.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicSW.Add("SchoolID", _globalInfo.SchoolID);
            string fromDateToDate = string.Empty;

            if (IsPresent)
            {
                // 06/11/2017*13/11/2017
                fromDateToDate = lstWeekDefaultVM.Count != 0 ? lstWeekDefaultVM[0].WeekDefaultID : string.Empty;
            }
            else
            { // tất cả các tuần

                if (lstWeekDefaultVM.Count != 0)
                {
                    string _fromDate = lstWeekDefaultVM[0].FromDate;
                    string _weekDefaultIDAfter = lstWeekDefaultVM[lstWeekDefaultVM.Count - 1].WeekDefaultID;
                    List<string> lstWeekDefaultIDAfter = _weekDefaultIDAfter.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries)
                                                            .Select(x => x).ToList();
                    string _toDate = lstWeekDefaultIDAfter[1];
                    fromDateToDate = string.Format("{0}*{1}", _fromDate, _toDate);
                }
            }

            dicSW.Add("fromDateToDate", fromDateToDate);
            List<SchoolWeek> lstSchoolWeek = SchoolWeekBusiness.Search(dicSW).ToList();
            List<int> lstSchoolWeekID = lstSchoolWeek.Select(x => (int)x.SchoolWeekID).Distinct().ToList();

            // Xóa lịch báo giảng cũ       
            if (lstSchoolWeek.Count > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>() {
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"TeacherID",TeacherID},
                    {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                };
                List<string> lstFromTo = new List<string>();
                string fromDate = lstSchoolWeek[0].FromDate.Value.ToString("dd/MM/yyyy");
                string toDate = lstSchoolWeek[lstSchoolWeek.Count() - 1].ToDate.Value.ToString("dd/MM/yyyy");
                lstFromTo.Add(fromDate);
                lstFromTo.Add(toDate);
                dic["lstFromTo"] = lstFromTo;
                var lstTeachingScheduleNeedRemove = TeachingScheduleBusiness.Search(dic).Select(x => x.TeachingScheduleID).AsEnumerable();
                foreach (var item in lstTeachingScheduleNeedRemove)
                {
                    this.TeachingScheduleBusiness.Delete(item);
                }
            }

            /// Danh sách PPCT theo ngày
            #region [B2]
            IDictionary<string, object> dicDP = new Dictionary<string, object>() {
                {"lstSchoolWeekID", lstSchoolWeekID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
            };
            var lstDistriProgram = DistributeProgramBusiness.Search(dicDP).ToList();
            #endregion

            if (lstDistriProgram.Count == 0)
            {
                //Hệ thống chưa có phân phối chương trình
                return 2;
            }

            #region [B3]
            var lstSectionID = new List<int>();
            #region [Section]
            if (SectionID.HasValue)
            {
                lstSectionID.Add(SectionID.Value);
            }
            else
            {
                //tinh ra so buoi lon nhat             
                List<ClassProfile> lstCP = ListClassProfile();
                lstSectionID = UtilsBusiness.GetListSectionID(lstCP);
            }
            #endregion
            foreach (var itemSchoolWeek in lstSchoolWeek)
            {
                var lstDate = new List<DateOfWeekModel>();

                #region [List Date]
                //tinh ra cac thu theo tuan
                DateTime input = itemSchoolWeek.FromDate.Value;
                int delta = DayOfWeek.Monday - input.DayOfWeek;
                DateTime valtmp = itemSchoolWeek.FromDate.Value;
                while (valtmp <= itemSchoolWeek.ToDate)
                {

                    var objDate = new DateOfWeekModel();
                    objDate.Date = input.AddDays(delta);
                    objDate.DateOfWeek = this.GetDateOfWeek(input.AddDays(delta).DayOfWeek.ToString());
                    lstDate.Add(objDate);
                    valtmp = valtmp.AddDays(1);
                    delta++;
                };
                #endregion

                #region [lstDate Loop]
                //bool isSave = false;
                for (int i = 0; i < lstDate.Count; i++)
                {
                    var objDate = lstDate[i];

                    for (int s = 0; s < lstSectionID.Count; s++)
                    {
                        for (int subjectOrder = 1; subjectOrder <= 5; subjectOrder++)
                        {
                            var objCalendar = lstCalendar.Where(c => c.DayOfWeek == objDate.DateOfWeek
                                                                   && c.Section == lstSectionID[s]
                                                                   && c.SubjectOrder == subjectOrder)
                                                            .FirstOrDefault();
                            if (objCalendar != null)
                            {
                                #region [Distribute Program Search]
                                DistributeProgram objDisProgram = null;

                                if (objCalendar.AssignmentSubjectID.HasValue && objCalendar.AssignmentSubjectID.Value > 0)
                                {
                                    objDisProgram = lstDistriProgram.Where(d => d.ClassID == objCalendar.ClassID
                                                                        && d.SchoolWeekID == (int)itemSchoolWeek.SchoolWeekID
                                                                        && d.SubjectID == objCalendar.SubjectID
                                                                        && d.AssignSubjectID == objCalendar.AssignmentSubjectID)
                                                                    .OrderBy(D => D.AssignSubjectID)
                                                                    .ThenBy(d => d.DistributeProgramOrder)
                                                                    .FirstOrDefault();
                                }
                                else
                                {
                                    objDisProgram = lstDistriProgram.Where(d => d.ClassID == objCalendar.ClassID
                                                                        && d.SchoolWeekID == (int)itemSchoolWeek.SchoolWeekID
                                                                        && d.SubjectID == objCalendar.SubjectID)
                                                                    .OrderBy(D => D.AssignSubjectID)
                                                                    .ThenBy(d => d.DistributeProgramOrder)
                                                                    .FirstOrDefault();
                                }


                                if (objDisProgram != null)
                                {
                                    #region [Insert To Teaching Schedule]
                                    var objTS = new TeachingSchedule();
                                    objTS.SchoolID = _globalInfo.SchoolID.Value;
                                    objTS.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                    objTS.SchoolWeekID = (int)itemSchoolWeek.SchoolWeekID;
                                    objTS.ScheduleDate = objDate.Date;
                                    objTS.DayOfWeek = objCalendar.DayOfWeek;
                                    objTS.SectionID = lstSectionID[s];
                                    objTS.TeacherID = TeacherID;
                                    objTS.SubjectOrder = objCalendar.SubjectOrder ?? 0;
                                    objTS.TeachingScheduleOrder = objDisProgram.DistributeProgramOrder ?? 0;
                                    objTS.ClassID = objDisProgram.ClassID;
                                    objTS.SubjectID = objCalendar.SubjectID;
                                    objTS.Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100;
                                    objTS.CreatedTime = DateTime.Now;
                                    objTS.AssignSubjectID = objDisProgram.AssignSubjectID;

                                    TeachingScheduleBusiness.Insert(objTS);
                                    #endregion

                                    lstDistriProgram.Remove(objDisProgram);
                                }
                                #endregion
                            }
                        }

                        #region [Something Tobe Remove]
                        #endregion
                    }
                }
                #endregion
            }

            #endregion

            return 0;
        }

        public PartialViewResult GetScheduleAutoTeachingSchedule(string WeekID, int TeacherID)
        {
            List<string> lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];

            return PartialView("_ScheduleAutoTeachingSchedule",
                new ScheduleAutoTeachingScheduleModel()
                {
                    WeekIDDate = WeekID,
                    StrFromDate = fromDate,
                    SchoolWeekName = string.Format("({0} - {1})", fromDate, toDate),
                    EmployeeID = TeacherID,
                    IsPresent = true
                });
        }

        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult ScheduleAutoTeachingSchedule(ScheduleAutoTeachingScheduleModel model)
        {
            //B1: xoa data cu
            //B2: insert data moi vao
            #region [Begin]
            TeachingScheduleBusiness.SetAutoDetectChangesEnabled(false);
            AcademicYearBusiness.SetAutoDetectChangesEnabled(false);
            CalendarBusiness.SetAutoDetectChangesEnabled(false);
            DistributeProgramBusiness.SetAutoDetectChangesEnabled(false);
            SchoolWeekBusiness.SetAutoDetectChangesEnabled(false);
            #endregion
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<WeekDefaultViewModel> ListWeekDefault = AutoGenericWeekDefaul(academicYear);

            int result = 0;
            List<WeekDefaultViewModel> lstWeekDefaultViewModel = new List<WeekDefaultViewModel>();
            List<string> lstFromDate = new List<string>();
            #region [B1]
            #region [Check Week Approvaled]

            #endregion
            #region [Check IsPresent]
            if (model.IsPresent)
            {
                // searchInfoTeachingSchedule["FromDate"] = model.StrFromDate;
                var objWD = ListWeekDefault.Where(x => x.FromDate == model.StrFromDate).FirstOrDefault();
                if (objWD != null)
                {
                    if (GetTeachingScheduleApprovalCustom(model.StrFromDate, model.EmployeeID) == true)
                    {
                        return Json(new JsonMessage(string.Format("Tuần {0} đã được phê duyệt", model.SchoolWeekName), "error"));
                    }

                    lstFromDate.Add(model.StrFromDate);
                    lstWeekDefaultViewModel.Add(objWD);
                }
            }
            else
            {
                for (int i = 0; i < ListWeekDefault.Count(); i++)
                {
                    if (GetTeachingScheduleApprovalCustom(ListWeekDefault[i].FromDate, model.EmployeeID) == false)
                    {
                        lstFromDate.Add(ListWeekDefault[i].FromDate);
                        lstWeekDefaultViewModel.Add(ListWeekDefault[i]);
                    }
                }

                // searchInfoTeachingSchedule["lstFromDate"] = lstFromDate;
            }
            #endregion

            #endregion

            #region [B2] // insert
            if (lstFromDate.Count > 0)
            {
                result = ScheduleAutoByWeekDefault(lstWeekDefaultViewModel, lstFromDate, model.IsPresent, model.EmployeeID);
            }
            #endregion

            #region [End]
            TeachingScheduleBusiness.SetAutoDetectChangesEnabled(true);
            AcademicYearBusiness.SetAutoDetectChangesEnabled(true);
            CalendarBusiness.SetAutoDetectChangesEnabled(true);
            DistributeProgramBusiness.SetAutoDetectChangesEnabled(true);
            SchoolWeekBusiness.SetAutoDetectChangesEnabled(true);
            #endregion

            if (result == 0)
            {
                TeachingScheduleBusiness.Save();
                return Json(new JsonMessage("Lên lịch tự động thành công"));
            }

            return Json(new JsonMessage(result.ToString(), "error"));
        }
        #endregion

        public PartialViewResult GetViewDetailApproval(string WeekID, int TeacherID)
        {
            CheckPermissionButtonApproval(TeacherID);

            List<string> lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];

            IDictionary<string, object> SearchInfoTeachingScheduleApproval = new Dictionary<string, object>() {
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"TeacherID",TeacherID},
                    {"FromDate", fromDate},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };
            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);
            var TeachingScheduleApprovalEntities = TeachingScheduleApprovalBusiness.Search(SearchInfoTeachingScheduleApproval)

                .OrderByDescending(x => x.TeachingScheduleApprovalID).ToList();

            var employeeIDs = TeachingScheduleApprovalEntities.Where(x => x.EmployeeID > 0).Select(x => x.EmployeeID).ToList();

            var models = new List<ViewDetailApprovalViewModel>();
            if (employeeIDs.Count > 0)
            {
                var employeeEntities = EmployeeBusiness.All.Where(x => employeeIDs.Contains(x.EmployeeID)).ToList();
                models = TeachingScheduleApprovalEntities.Select(x => new ViewDetailApprovalViewModel
                {
                    TeachingScheduleApprovalID = x.TeachingScheduleApprovalID,
                    Status = x.Status,
                    EmployeeID = x.EmployeeID,
                    EmployeeName = employeeEntities.Where(e => e.EmployeeID == x.EmployeeID).Select(e => e.FullName).FirstOrDefault(),
                    CreatedDate = x.CreateDate,
                    Note = x.Note
                }).ToList();
            }
            else
            {
                models = TeachingScheduleApprovalEntities.Select(x => new ViewDetailApprovalViewModel
                {
                    TeachingScheduleApprovalID = x.TeachingScheduleApprovalID,
                    Status = x.Status,
                    EmployeeID = x.EmployeeID,
                    CreatedDate = x.CreateDate,
                    Note = x.Note
                }).ToList();
            }

            return PartialView("_ViewDetailApproval", models);
        }

        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult UpdateApprovalTeachingSchedule(List<ViewDetailApprovalViewModel> models)
        {
            if (models == null)
            {
                models = new List<ViewDetailApprovalViewModel>();
            }

            if (models.Count > 0)
            {
                bool isSave = false;
                TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);
                var listModelId = models.Where(x => x.TeachingScheduleApprovalID > 0).Select(x => x.TeachingScheduleApprovalID).ToList();
                var teachingScheduleApprovalEntities = TeachingScheduleApprovalBusiness.All.Where(x => listModelId.Contains(x.TeachingScheduleApprovalID)).AsEnumerable();
                foreach (var item in teachingScheduleApprovalEntities)
                {
                    var modelUpdate = models.Where(x => x.TeachingScheduleApprovalID == item.TeachingScheduleApprovalID).FirstOrDefault();
                    if (modelUpdate != null)
                    {
                        item.UpdateDate = DateTime.Now;
                        item.Note = modelUpdate.Note.Length > 510 ? modelUpdate.Note.Substring(0, 510) : modelUpdate.Note;
                        TeachingScheduleApprovalBusiness.Update(item);
                        isSave = true;
                    }
                }
                TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(true);
                if (isSave)
                {
                    TeachingScheduleApprovalBusiness.Save();
                }

            }

            return Json(new JsonMessage("Lưu thành công"));
        }

        /// <summary>
        /// Xóa một lịch báo giảng
        /// </summary>
        /// <param name="teachingScheduleID"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteOneTeachingSchedule(int? teachingScheduleID, string weekID)
        {
            if (teachingScheduleID == null || teachingScheduleID < 0)
            {
                return Json(new JsonMessage(Res.Get("Teaching_Schedule_KHONG_CO_LICH"), "error"));
            }
            TeachingSchedule ts = TeachingScheduleBusiness.Find(teachingScheduleID);
            // Null hoặc không phải của người dùng hiện tại
            if (ts == null || ts.TeacherID != _globalInfo.EmployeeID)
            {
                return Json(new JsonMessage(Res.Get("Teaching_Schedule_KHONG_CO_LICH"), "error"));
            }

            List<string> lstDate = weekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            if (lstDate.Count == 0)
            {
                return Json(new JsonMessage("Tuần không tồn tại", "error"));
            }

            string fromDate = lstDate[0];
            if (GetTeachingScheduleApprovalCustom(fromDate, ts.TeacherID) == true)
            {
                return Json(new JsonMessage("Phân phối chương trình đã được phê duyệt", "error"));
            }

            this.TeachingScheduleBusiness.Delete(ts);
            return Json(new JsonMessage(Res.Get("Teaching_Schedule_XOA_OK"), "success"));
        }

        /// <summary>
        /// Xóa toàn bộ lịch báo giảng của tuần
        /// </summary>
        /// <param name="WeekID"></param>
        /// <param name="SectionID"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteTeachingSchedule(string WeekID, int? SectionID, int TeacherID)
        {
            List<string> lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];

            //? có kiểm tra điều kiện không
            if (GetTeachingScheduleApprovalCustom(fromDate, TeacherID) == true)
            {
                return Json(new JsonMessage("Phân phối chương trình đã được phê duyệt", "error"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"lstFromTo",lstFromTo},
                {"TeacherID",TeacherID},
                {"SectionID",SectionID}
            };

            List<TeachingSchedule> lstResult = TeachingScheduleBusiness.Search(dic).ToList();
            TeachingScheduleBusiness.DeleteAll(lstResult);
            TeachingScheduleBusiness.Save();
            return Json(new JsonMessage(Res.Get("Teaching_Schedule_XOA_OK"), "success"));
        }

        #region Import Export
        /// <summary>
        /// Xuất excel
        /// </summary>
        /// <param name="WeekID"></param>
        /// <param name="SectionID"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        public FileResult ExportExcel(string WeekID, int? SectionID, int TeacherID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "GV", TeachingScheduleConstants.TEMPLATE_FILE);
            List<int> lstSectionID = new List<int>();
            List<ClassProfile> lstCP = ListClassProfile().OrderBy(p => p.OrderNumber).ToList();
            if (SectionID.HasValue)
            {
                lstSectionID.Add(SectionID.Value);
            }
            else
            {
                //tinh ra so buoi lon nhat
                //int maxSectionID = lstCP.Count > 0 ? lstCP.Max(p => p.Section).Value : 0;
                lstSectionID = UtilsBusiness.GetListSectionID(lstCP);
            }
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //SchoolWeek objSchoolWeek = SchoolWeekBusiness.Find(WeekID);
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<WeekDefaultViewModel> ListWeekDefault = AutoGenericWeekDefaul(academicYear);

            WeekDefaultViewModel objWeekDefault = ListWeekDefault.Where(x => x.WeekDefaultID == WeekID).FirstOrDefault();
            #region fill du lieu
            //fill thong tin chung
            string WeekName = string.Empty;
            WeekName = string.Format("Từ ngày {0} đến {1} ", (objWeekDefault != null ? objWeekDefault.FromDate : "---"), (objWeekDefault != null ? objWeekDefault.ToDate : "---"));
            sheet.SetCellValue("A3", WeekName);
            Employee objE = EmployeeBusiness.Find(TeacherID);
            string EmployeeName = string.Format("{0}: ", Res.Get("Teaching_Schedule_Excel_GIAO_VIEN")) + objE.FullName;
            sheet.SetCellValue("A4", EmployeeName);
            sheet.SetCellValue("AA6", WeekID);
            sheet.SetCellValue("AB6", SectionID.HasValue ? SectionID.Value : 0);
            //fill list
            IDictionary<string, object> dicSubjectAssign = new Dictionary<string, object>()
            {
                {"TeacherID",TeacherID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel}
            };
            List<TeachingAssignmentBO> lstTeachingAssign = TeachingAssignmentBusiness.GetSubjectTeacher(_globalInfo.SchoolID.Value, dicSubjectAssign).ToList();
            List<string> lstCLassNameAssign = lstTeachingAssign.OrderBy(p => p.ClassName).Select(p => p.ClassName).Distinct().ToList();
            var lstSubjectNameAssign = lstTeachingAssign.OrderBy(p => p.OrderInSubject).Select(p => new { p.SubjectName, p.SubjectID }).ToList();
            IDictionary<string, object> dicS = new Dictionary<string, object>()
            {
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID}
            };
            var lstSubjectCat = (from s in SchoolSubjectBusiness.Search(dicS)
                                 join sc in SubjectCatBusiness.All on s.SubjectID equals sc.SubjectCatID
                                 where s.SectionPerWeekFirstSemester.HasValue && s.SectionPerWeekFirstSemester.Value > 0
                                 && s.SectionPerWeekSecondSemester.HasValue && s.SectionPerWeekSecondSemester.Value > 0
                                 && sc.AppliedLevel == _globalInfo.AppliedLevel
                                 select sc).Distinct().OrderBy(p => p.OrderInSubject).Select(p => new { p.SubjectName, p.SubjectCatID }).ToList();
            List<string> lstClassName = lstCP.Select(p => p.DisplayName).ToList();
            List<string> lstSubjecttmp = new List<string>();
            List<string> lstClasstmp = new List<string>();
            lstSubjecttmp.AddRange(lstSubjectNameAssign.Select(x => x.SubjectName).Distinct().ToList());
            lstSubjecttmp.AddRange(lstSubjectCat.Select(x => x.SubjectName).Distinct().ToList());
            lstSubjecttmp = lstSubjecttmp.Distinct().ToList();

            var subjectIDs = lstSubjectNameAssign.Where(x => x.SubjectID != null).Select(x => (int)x.SubjectID).Distinct().ToList();
            subjectIDs.AddRange(lstSubjectCat.Select(x => x.SubjectCatID).Distinct().ToList());
            var lstAssignSubjectConfig = GetAssignSubjectListBySubjects(subjectIDs);

            lstClasstmp.AddRange(lstCLassNameAssign);
            lstClasstmp.AddRange(lstClassName);
            lstClasstmp = lstClasstmp.Distinct().ToList();
            //fill thong tin mon va lop
            IVTWorksheet sheetRef = oBook.GetSheet(2);
            for (int i = 0; i < lstSubjecttmp.Count; i++)
            {
                sheetRef.SetCellValue(2 + i, 1, lstSubjecttmp[i]);
            }

            for (int i = 0; i < lstClasstmp.Count; i++)
            {
                sheetRef.SetCellValue(2 + i, 2, lstClasstmp[i]);
            }
            var __itemIndex = 0;
            sheetRef.SetCellValue(2 + (__itemIndex++), 3, Res.Get("ItSelf"));
            foreach (var item in lstAssignSubjectConfig)
            {
                sheetRef.SetCellValue(2 + (__itemIndex++), 3, item.AssignSubjectName);
            }

            List<string> lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"lstFromTo",lstFromTo},
                {"TeacherID",TeacherID},
                {"SectionID",SectionID}
            };

            IQueryable<TeachingSchedule> queryTeachingSchedule = TeachingScheduleBusiness.Search(dic);
            List<TeachingScheduleViewModel> lstResult = ListTeachingScheduleVM(queryTeachingSchedule);
            TeachingScheduleViewModel objTSVM = null;

            IDictionary<string, object> dicSW = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"fromDateToDate", WeekID}
            };
            //List<SchoolWeek> lstSchoolWeek = SchoolWeekBusiness.Search(dicSW).ToList();
            //List<int> lstSchoolWeekID = lstSchoolWeek.Select(x => (int)x.SchoolWeekID).Distinct().ToList();

            IDictionary<string, object> dicDP = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                //{"lstSchoolWeekID", lstSchoolWeekID}
            };
            List<DistributeProgram> lstDistributeProgram = new List<DistributeProgram>();
            DistributeProgram objDP = null;
            lstDistributeProgram = DistributeProgramBusiness.Search(dicDP).ToList();
            DateTime startDate = Convert.ToDateTime(objWeekDefault.FromDate);
            sheet.SetComment(5, 8, Res.Get("Teaching_Schedule_Excel_PPCT_LAY_TU"));
            sheet.SetComment(6, 11, Res.Get("Teaching_Schedule_Excel_X_PTB"));
            sheet.SetComment(6, 12, Res.Get("Teaching_Schedule_Excel_X_TL"));
            string dateName = string.Empty;
            string sectionName = string.Empty;
            int StartRow = 7;
            for (int i = 1; i <= 7; i++)//for 1 tuan 7 ngay co dinh
            {
                sheet.GetRange(StartRow, 1, StartRow + lstSectionID.Count * 5 - 1, 1).Merge();
                dateName = this.GetDateName(startDate.DayOfWeek.ToString()) + "\r\n" + " (" + startDate.ToString("dd/MM/yyyy") + ")";
                sheet.SetCellValue(StartRow, 1, dateName);
                for (int j = 0; j < lstSectionID.Count; j++)//for buoi hoc
                {
                    sheet.GetRange(StartRow, 2, StartRow + 4, 2).Merge();
                    sectionName = lstSectionID[j] == 1 ? Res.Get("Teaching_Schedule_Excel_SANG") : lstSectionID[j] == 2 ? Res.Get("Teaching_Schedule_Excel_CHIEU") : Res.Get("Teaching_Schedule_Excel_TOI");
                    sheet.SetCellValue(StartRow, 2, sectionName);
                    for (int k = 1; k < 6; k++)//for tiet hoc co dinh 5 tiet
                    {
                        sheet.SetCellValue(StartRow, 3, k);
                        objTSVM = lstResult.FirstOrDefault(p => p.ScheduleDate == startDate && p.Section == lstSectionID[j] && p.SubjectOrder == k);
                        if (objTSVM != null)
                        {
                            //Mon
                            sheet.SetCellValue(StartRow, 4, objTSVM.SubjectName);
                            //Phan mon
                            sheet.SetCellValue(StartRow, 5, !string.IsNullOrEmpty(objTSVM.AssignSubjectName) ? objTSVM.AssignSubjectName : "");
                            //Lop
                            sheet.SetCellValue(StartRow, 6, objTSVM.ClassName);
                            //Tiet PPCT
                            sheet.SetCellValue(StartRow, 7, objTSVM.TeachingScheduleOrder);
                            //Ten bai day
                            if (objTSVM.AssignSubjectID.HasValue)
                            {
                                objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTSVM.SubjectID
                                && p.AssignSubjectID == objTSVM.AssignSubjectID
                                && p.EducationLevelID == objTSVM.EducationLevelID
                                && p.ClassID == objTSVM.ClassID
                                //&& p.SchoolWeekID == objTSVM.SchoolWeekID
                                && p.DistributeProgramOrder == objTSVM.TeachingScheduleOrder);
                            }
                            else
                            {
                                objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTSVM.SubjectID
                                && p.AssignSubjectID.HasValue == false
                                && p.ClassID == objTSVM.ClassID
                                && p.EducationLevelID == objTSVM.EducationLevelID
                                //&& p.SchoolWeekID == objTSVM.SchoolWeekID
                                && p.DistributeProgramOrder == objTSVM.TeachingScheduleOrder);
                            }
                            sheet.SetCellValue(StartRow, 8, objDP != null ? objDP.LessonName : "");
                            //Ten do dung
                            sheet.SetCellValue(StartRow, 9, objTSVM.TeachingUtensil);
                            //So luong
                            sheet.SetCellValue(StartRow, 10, objTSVM.Number.HasValue && objTSVM.Number.Value > 0 ? objTSVM.Number.Value.ToString() : "");
                            //Co o phong TB
                            sheet.SetCellValue(StartRow, 11, objTSVM.InRoom.HasValue && objTSVM.InRoom.Value ? "x" : "");
                            //Tu lam
                            sheet.SetCellValue(StartRow, 12, objTSVM.IsSelfMade.HasValue && objTSVM.IsSelfMade.Value ? "x" : "");

                            if (objTSVM.Status != 0)
                            {
                                sheet.GetRange(StartRow, 3, StartRow, 12).SetFontStyle(false, System.Drawing.Color.Red, false, 10, false, false);
                                sheet.GetRange(StartRow, 3, StartRow, 12).IsLock = true;
                            }

                            sheet.SetCellValue("Y" + StartRow, objTSVM.Status);

                        }
                        StartRow++;
                    }
                }
                startDate = startDate.AddDays(1);
            }
            //ke khung
            sheet.GetRange(7, 1, 7 + 7 * lstSectionID.Count * 5 - 1, 13).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.HideColumn(13);
            sheet.SetColumnWidth('Y', 0.0);
            sheet.Name = string.Format("{0}-{1}", objWeekDefault.FromDate, objWeekDefault.ToDate);
            sheet.ProtectSheet();
            #endregion
            string fileName = string.Format("PhieuBaoGiang[{0}]_[{1}].xls", Utils.Utils.StripVNSignAndSpace(objE.FullName), sheet.Name);
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        /// <summary>
        /// Xuất sổ lịch báo giảng
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <param name="CurrentWeekID"></param>
        /// <param name="TypeExport"></param>
        /// <returns></returns>
        public FileResult ExportExcelBookTeachingSchedule(int TeacherID, string CurrentWeekID, int TypeExport)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<WeekDefaultViewModel> lstWeekDefault = AutoGenericWeekDefaul(academicYear);
            List<string> lstFromTo = new List<string>();
            string fromDateToDate = string.Empty;
            // theo tuần
            if (TypeExport == 1)
            {
                lstWeekDefault = lstWeekDefault.Where(x => x.WeekDefaultID == CurrentWeekID).ToList();
                lstFromTo = CurrentWeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
                fromDateToDate = CurrentWeekID;
            }
            else
            {// tất cả tuần        
                lstFromTo.Add(lstWeekDefault[0].FromDate);
                lstFromTo.Add(lstWeekDefault[lstWeekDefault.Count() - 1].ToDate);
                fromDateToDate = lstFromTo[0] + "*" + lstFromTo[1];
            }

            Employee objEmployee = EmployeeBusiness.Find(TeacherID);
            Stream excel = Export(TeacherID, objEmployee, lstFromTo, fromDateToDate, lstWeekDefault, TypeExport);

            string fileName = string.Format("LichBaoGiang_[{0}]_[{1}].xls", Utils.Utils.StripVNSignAndSpace(objEmployee.FullName), lstFromTo[0].Replace("/", ""));
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        public FileResult ExportPDFBookTeachingSchedule(int TeacherID, string CurrentWeekID, int TypeExport)
        {
            //ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(TeachingScheduleConstants.REPORT_CODE);
            Employee objEmployee = EmployeeBusiness.Find(TeacherID);
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<WeekDefaultViewModel> lstWeekDefault = AutoGenericWeekDefaul(academicYear);

            List<string> lstFromTo = new List<string>();
            string fromDateToDate = string.Empty;
            string fromDate = string.Empty;
            // theo tuần
            if (TypeExport == 1)
            {
                lstWeekDefault = lstWeekDefault.Where(x => x.WeekDefaultID == CurrentWeekID).ToList();
                lstFromTo = CurrentWeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
                fromDateToDate = CurrentWeekID;
                fromDate = lstFromTo[0];

            }
            else
            {// tất cả tuần        
                lstFromTo.Add(lstWeekDefault[0].FromDate);
                lstFromTo.Add(lstWeekDefault[lstWeekDefault.Count() - 1].ToDate);
                fromDateToDate = lstFromTo[0] + "*" + lstFromTo[1];
                fromDate = lstWeekDefault[0].FromDate;
            }

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"TeacherID",objEmployee.EmployeeID},
                {"TeacherName",objEmployee.FullName},
                {"FromDate", fromDate}
            };

            Stream excel = Export(TeacherID, objEmployee, lstFromTo, fromDateToDate, lstWeekDefault, TypeExport);
            //ProcessedReport processedReport = TeachingScheduleBusiness.InsertProcessReport(dic, excel, TeachingScheduleConstants.REPORT_CODE);
            string fileName = string.Format("LichBaoGiang_{0}_{1}.xls", objEmployee.FullName, lstFromTo[0]);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, fileName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream Export(
            int TeacherID,
            Employee objEmployee,
            List<string> lstFromTo,
            string fromDateToDate,
            List<WeekDefaultViewModel> lstWeekDefault,
            int TypeExport)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "GV", TeachingScheduleConstants.TEMPLATE_FILE_BOOK_TS);

            IDictionary<string, object> dicCP = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevel",_globalInfo.AppliedLevel}
            };

            List<int> lstSectionID = new List<int>();
            lstSectionID.Add(1);
            lstSectionID.Add(2);
            lstSectionID.Add(3);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            // sheet bìa
            IVTWorksheet sheetFirst = oBook.GetSheet(1);
            // sheet thông tin giảng dạy
            IVTWorksheet sheetInfo = oBook.GetSheet(2);
            // sheet nội dung
            IVTWorksheet sheetContent = oBook.GetSheet(3);

            // Get data
            #region
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            AcademicYear objYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            IDictionary<string, object> dicTS = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"lstFromTo",lstFromTo},
                {"TeacherID",TeacherID},
            };
            IQueryable<TeachingSchedule> lstTeachingSchedule = TeachingScheduleBusiness.Search(dicTS);

            IDictionary<string, object> dicSW = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"fromDateToDate", fromDateToDate}
            };
            //List<SchoolWeek> lstSchoolWeek = SchoolWeekBusiness.Search(dicSW).ToList();
            //List<int> lstSchoolWeekID = lstSchoolWeek.Select(x => (int)x.SchoolWeekID).Distinct().ToList();

            IDictionary<string, object> dicDP = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                //{"lstSchoolWeekID", lstSchoolWeekID}
            };
            List<DistributeProgram> lstDistributeProgram = new List<DistributeProgram>();
            DistributeProgram objDP = null;
            lstDistributeProgram = DistributeProgramBusiness.Search(dicDP).ToList();

            DateTime fromDate = Convert.ToDateTime(lstFromTo[0]);
            DateTime toDate = Convert.ToDateTime(lstFromTo[1]);

            // phê duyệt    
            IDictionary<string, object> dicTSA = new Dictionary<string, object>() {
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"TeacherID",TeacherID},
                    //{"FromDate", fromDate},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };

            var lstTeachScheduleApproval = this.TeachingScheduleApprovalBusiness.Search(dicTSA)
                                                .OrderByDescending(x => x.TeachingScheduleApprovalID).ToList();
            List<int> lstEmployeeID = lstTeachScheduleApproval.Where(x => x.EmployeeID != 0).Select(x => x.EmployeeID).ToList();
            List<Employee> lstEmployee = EmployeeBusiness.All.Where(x => lstEmployeeID.Contains(x.EmployeeID)).ToList();

            lstTeachingSchedule = lstTeachingSchedule.Where(p => p.ScheduleDate >= fromDate && p.ScheduleDate <= toDate);
            List<TeachingScheduleViewModel> lstResult = ListTeachingScheduleVM(lstTeachingSchedule);

            List<TeachingScheduleViewModel> lstSubjectFlowClass = lstTeachingSchedule
                .Select(x => new TeachingScheduleViewModel
                {
                    ClassID = x.ClassID,
                    SubjectID = x.SubjectID
                }).Distinct().ToList();
            List<TeachingScheduleViewModel> lstSubjectName = lstResult
                .Select(x => new TeachingScheduleViewModel
                {
                    SubjectName = x.SubjectName,
                    SubjectID = x.SubjectID
                }).Distinct().ToList();
            List<int> lstSubjectID = lstSubjectName.Select(x => x.SubjectID).Distinct().ToList();
            #endregion

            if (TypeExport != 1)
            {
                // Fill thông tin bìa 
                #region
                // sở
                sheetFirst.SetCellValue("A5", school.SupervisingDept.SupervisingDeptName.ToUpper());
                // phòng
                sheetFirst.SetCellValue("A6", "");

                string schoolName = school.SchoolName;
                List<string> arraySchoolName = schoolName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
                if (arraySchoolName[0].ToUpper().Equals("TRƯỜNG"))
                {
                    schoolName = schoolName.Substring(7);
                }

                sheetFirst.SetCellValue("C27", string.Format("Họ và tên giáo viên: {0}", objEmployee.FullName));
                sheetFirst.SetCellValue("C28", string.Format("Môn dạy: {0}", objEmployee.FullName));
                sheetFirst.SetCellValue("C29", string.Format("Trường: {0}", schoolName));
                sheetFirst.SetCellValue("A38", string.Format("Năm học: {0}", objYear.DisplayTitle));
                #endregion

                // Fill thông tin giảng dạy
                #region
                sheetInfo.SetCellValue("F2", string.Format("{0}", objEmployee.FullName));
                sheetInfo.SetCellValue("F3", string.Format("'{0}", objEmployee.BirthDate.HasValue ? objEmployee.BirthDate.Value.ToString("dd/MM/yyyy") : ""));
                sheetInfo.SetCellValue("F4", string.Format("'{0}", objEmployee.StartingDate.HasValue ? objEmployee.StartingDate.Value.ToString("dd/MM/yyyy") : ""));
                sheetInfo.SetCellValue("F5", string.Format("{0}", objEmployee.QualificationTypeID.HasValue ? objEmployee.QualificationType.Resolution : ""));

                for (int i = 0; i < lstSubjectID.Count(); i++)
                {
                    sheetInfo.SetCellValue(("B" + (i + 8)), lstSubjectName.Where(x => x.SubjectID == lstSubjectID[i]).FirstOrDefault().SubjectName);
                    List<int> lstClassID = lstSubjectFlowClass.Where(x => x.SubjectID == lstSubjectID[i]).Select(x => x.ClassID).Distinct().ToList();
                    List<string> lstClassName = lstResult.Where(x => lstClassID.Contains(x.ClassID)).Select(x => x.ClassName).Distinct().ToList();

                    var strClassName = string.Join(", ", lstClassName.ToArray());

                    sheetInfo.SetCellValue(("G" + (i + 8)), strClassName);
                    if (i >= 4)
                        break;
                }

                List<string> subjectName = lstSubjectName.Select(x => x.SubjectName).Distinct().ToList();
                sheetInfo.SetCellValue("M28", string.Format("Ngày {0} tháng {1} năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year));
                sheetInfo.SetCellValue("M34", string.Format("{0}", school.HeadMasterName));
                sheetInfo.SetCellValue("N5", string.Join(", ", subjectName.ToArray()));

                sheetFirst.SetCellValue("C28", string.Format("Môn dạy: {0}", string.Join(", ", subjectName.ToArray())));
                #endregion
            }

            //Fill Nội dung tuần
            #region
            List<int> lstSectionApplied = new List<int>();

            int mergeThu = 5;
            int StartRow = 5;
            IVTWorksheet sheetCopy = null;
            string dateName = string.Empty;
            string description = string.Empty;

            bool IsMorning = true;
            bool IsAfternoon = true;
            bool IsNight = true;
            string userApproval = string.Empty;
            string dateApproval = "Ngày.... tháng.... năm....";
            TeachingScheduleViewModel objTSVM = null;
            for (int i = 0; i < lstWeekDefault.Count(); i++)
            {
                DateTime startDate = lstWeekDefault[i].StartDate;

                lstSectionApplied = lstResult.Where(x => x.ScheduleDate >= startDate && x.ScheduleDate <= startDate.AddDays(6))
                                            .Select(x => x.Section).Distinct()
                                            .OrderBy(x => x).ToList();
                if (lstSectionApplied.Count() == 0)
                {
                    continue;
                }

                DateTime valFromDateConvert = Convert.ToDateTime(lstWeekDefault[i].FromDate);
                sheetCopy = oBook.CopySheetToLast(sheetContent);
                sheetCopy.SetCellValue("A3", string.Format("Giáo viên: {0} - Từ ngày {1} đến ngày {2}", objEmployee.FullName, lstWeekDefault[i].FromDate, lstWeekDefault[i].ToDate));

                var objApproval = lstTeachScheduleApproval.Where(x => x.FromDate == valFromDateConvert).FirstOrDefault();
                if (objApproval != null && objApproval.Status == 1)
                {
                    if (objApproval.EmployeeID != 0)
                    {
                        var objEm = lstEmployee.Where(x => x.EmployeeID == objApproval.EmployeeID).FirstOrDefault();
                        if (objEm != null)
                        {
                            userApproval = objEm.FullName;
                        }
                    }
                    else
                    {
                        userApproval = "Quản Trị Trường";
                    }
                    dateApproval = string.Format("Ngày {0} tháng {1} năm {2}", objApproval.CreateDate.Day, objApproval.CreateDate.Month, objApproval.CreateDate.Year);
                    sheetCopy.SetCellValue("A111", objApproval.Note);
                }
                sheetCopy.SetCellValue("G110", dateApproval);
                sheetCopy.SetCellValue("G114", userApproval);
                //sheetCopy.SetCellValue("H115", userApproval);
                userApproval = string.Empty;
                dateApproval = "Ngày.... tháng.... năm....";

                // kiểm tra trong tuần GV có dạy buổi sáng - chiều - tối hay không?
                var checkIsMorning = lstSectionApplied.Where(x => x == lstSectionID[0]).ToList();
                var checkIsAfternoon = lstSectionApplied.Where(x => x == lstSectionID[1]).ToList();
                var checkIsNight = lstSectionApplied.Where(x => x == lstSectionID[2]).ToList();

                if (checkIsMorning.Count <= 0)
                    IsMorning = false; // không dạy buổi sáng
                if (checkIsAfternoon.Count <= 0)
                    IsAfternoon = false; // không dạy buổi chiều
                if (checkIsNight.Count <= 0)
                    IsNight = false; // không dạy buổi tối

                for (int n = 1; n <= 7; n++)//for 1 tuan 7 ngay co dinh
                {
                    dateName = this.GetNameToDay(startDate.DayOfWeek.ToString());

                    #region
                    if (IsMorning && IsAfternoon && IsNight)// Dạy 3 buổi
                    {
                        sheetCopy.SetCellValue(("A" + mergeThu), dateName);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 7), 1).Merge();
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 7), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 7), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 8)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 14), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 14), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 14), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (IsMorning && IsAfternoon && !IsNight) // Dạy sáng - chiều
                    {
                        sheetCopy.SetCellValue(("A" + mergeThu), dateName);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).Merge();
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);
                        //sheetCopy.GetRange((mergeThu + 4), 1, (mergeThu + 4), 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 5)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (IsMorning && !IsAfternoon && IsNight) // Dạy sáng - tối
                    {
                        sheetCopy.SetCellValue(("A" + mergeThu), dateName);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).Merge();
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 10)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (!IsMorning && IsAfternoon && IsNight) // Dạy Chiều - tối
                    {
                        sheetCopy.SetCellValue(("A" + (mergeThu + 5)), dateName);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 10)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (IsMorning && !IsAfternoon && !IsNight) // Dạy Sáng
                    {
                        sheetCopy.SetCellValue(("A" + mergeThu), dateName);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 2), 1).Merge();
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 2), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 2), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 3)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 3), 1, (mergeThu + 4), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 3), 1, (mergeThu + 4), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 3), 1, (mergeThu + 4), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (!IsMorning && IsAfternoon && !IsNight) // Dạy Chiều
                    {
                        sheetCopy.SetCellValue(("A" + (mergeThu + 5)), dateName);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 7), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 7), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 7), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);
                        sheetCopy.GetRange((mergeThu + 9), 1, (mergeThu + 9), 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 8)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 9), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 9), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 9), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (!IsMorning && !IsAfternoon && IsNight) // Dạy Tối
                    {
                        sheetCopy.SetCellValue(("A" + (mergeThu + 10)), dateName);
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 12), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 12), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 12), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 13)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 13), 1, (mergeThu + 14), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 13), 1, (mergeThu + 14), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 13), 1, (mergeThu + 14), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    #endregion


                    if (n == 7 && dateName.Contains("CN"))
                    {
                        var checkSunday = lstResult.Where(x => x.ScheduleDate == startDate).Select(x => x.Section)
                                                    .Distinct().OrderBy(x => x).ToList();
                        if (checkSunday.Count() == 0)
                        {
                            for (int p = 95; p <= 109; p++)
                            {
                                sheetCopy.SetRowHeight(p, 0);
                            }
                            continue;
                        }
                    }

                    #region // fill buoi hoc
                    for (int j = 0; j < lstSectionID.Count; j++)//for buoi hoc
                    {
                        if (IsMorning == false && lstSectionID[j] == 1)
                        {
                            for (int p = 1; p <= 5; p++)
                            {
                                sheetCopy.SetRowHeight(StartRow, 0);
                                StartRow++;
                            }
                            continue;
                        }

                        if (IsAfternoon == false && lstSectionID[j] == 2)
                        {
                            for (int p = 1; p <= 5; p++)
                            {
                                sheetCopy.SetRowHeight(StartRow, 0);
                                StartRow++;
                            }
                            continue;
                        }

                        if (IsNight == false && lstSectionID[j] == 3)
                        {
                            for (int p = 1; p <= 5; p++)
                            {
                                sheetCopy.SetRowHeight(StartRow, 0);
                                StartRow++;
                            }
                            continue;
                        }

                        for (int k = 1; k < 6; k++)//for tiet hoc co dinh 5 tiet
                        {
                            objTSVM = lstResult.FirstOrDefault(p => p.ScheduleDate == startDate
                                                            && p.Section == lstSectionID[j]
                                                            && p.SubjectOrder == k);
                            if (objTSVM != null)
                            {
                                //Mon
                                sheetCopy.SetCellValue(StartRow, 4, !string.IsNullOrEmpty(objTSVM.AssignSubjectName) ? objTSVM.AssignSubjectName : objTSVM.SubjectName);
                                //Lop
                                sheetCopy.SetCellValue(StartRow, 5, objTSVM.ClassName);
                                //Tiet PPCT
                                sheetCopy.SetCellValue(StartRow, 6, objTSVM.TeachingScheduleOrder);
                                //Ten bai day
                                if (objTSVM.AssignSubjectID.HasValue)
                                {
                                    objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTSVM.SubjectID
                                    && p.AssignSubjectID == objTSVM.AssignSubjectID
                                    && p.EducationLevelID == objTSVM.EducationLevelID
                                    && p.ClassID == objTSVM.ClassID
                                    && p.DistributeProgramOrder == objTSVM.TeachingScheduleOrder);
                                }
                                else
                                {
                                    objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTSVM.SubjectID
                                    && p.AssignSubjectID.HasValue == false
                                    && p.ClassID == objTSVM.ClassID
                                    && p.EducationLevelID == objTSVM.EducationLevelID
                                    && p.DistributeProgramOrder == objTSVM.TeachingScheduleOrder);
                                }
                                sheetCopy.SetCellValue(StartRow, 7, objDP != null ? objDP.LessonName : "");
                                //Chuan bi

                                description = Description(objTSVM.TeachingUtensil, objTSVM.Number, objTSVM.InRoom, objTSVM.IsSelfMade, objTSVM.Status, ref description);
                                sheetCopy.SetCellValue(StartRow, 8, description);
                                description = string.Empty;
                            }
                            StartRow++;
                        }
                    }
                    #endregion


                    startDate = startDate.AddDays(1);
                }

                sheetCopy.Name = lstWeekDefault[i].WeekDefaultID.Replace("/", "").Replace("*", "-");

                IsMorning = true;
                IsAfternoon = true;
                IsNight = true;
                StartRow = 5;
                mergeThu = 5;
            }

            if (TypeExport == 1)
            {
                sheetFirst.Delete();
                sheetInfo.Delete();
            }

            sheetContent.Delete();
            #endregion

            string fileName = string.Format("LichBaoGiang_[{0}]_[{1}].xls", Utils.Utils.StripVNSignAndSpace(objEmployee.FullName), lstFromTo[0].Replace("/", ""));
            return oBook.ToStream();
        }

        private string Description(string TeachingUtensil, int? Number, bool? InRoom, bool? IsSelfMade, int Status, ref string description)
        {
            if (!string.IsNullOrEmpty(TeachingUtensil))
                description += TeachingUtensil;

            if (Number.HasValue && Number > 0)
            {
                description = !string.IsNullOrEmpty(description) ? (description + ". Số lượng: " + Number) : ("Số lượng: " + Number);
            }

            if (InRoom.HasValue && InRoom.Value == true)
            {
                description = !string.IsNullOrEmpty(description) ? (description + ". Có ở phòng TB") : ("Có ở phòng TB");
            }

            if (IsSelfMade.HasValue && IsSelfMade.Value == true)
            {
                description = !string.IsNullOrEmpty(description) ? (description + ". Tự làm") : ("Tự làm");
            }

            if (Status != 0)
            {
                if (Status == 1)
                    description = !string.IsNullOrEmpty(description) ? (description + ". " + Res.Get("Offset_Teaching")) : (Res.Get("Offset_Teaching"));
                else
                    description = !string.IsNullOrEmpty(description) ? (description + ". " + Res.Get("Cease_Teaching")) : (Res.Get("Cease_Teaching"));
            }

            return description;
        }


        /// <summary>
        /// Đưa dữ liệu từ excel vào hệ thống
        /// </summary>
        /// <param name="attachments"></param>
        /// <param name="SchoolWeekID"></param>
        /// <param name="SectionID"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments,
            string SchoolWeekID, int? SectionID, int TeacherID)
        {
            List<string> lstFromTo = SchoolWeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];

            if (GetTeachingScheduleApprovalCustom(fromDate, TeacherID) == true)
            {
                return Json(new JsonMessage("Phân phối chương trình đã được phê duyệt", "error"));
            }

            if (attachments == null || !attachments.Any()) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res1.ContentType = "text/plain";
                    return res1;
                }
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                List<int> lstSectionID = new List<int>();

                //SchoolWeek objSchoolWeek = SchoolWeekBusiness.Find(SchoolWeekID);

                if (SectionID.HasValue)
                {
                    lstSectionID.Add(SectionID.Value);
                }
                else
                {
                    //tinh ra so buoi lon nhat
                    List<ClassProfile> lstCP = ListClassProfile();
                    lstSectionID = UtilsBusiness.GetListSectionID(lstCP);
                }
                IVTRange vtRange = sheet.GetUsedRange();
                int totalRow = vtRange.TotalRow;
                int currentRow = 7 + 7 * lstSectionID.Count * 5 - 1;
                if (totalRow != currentRow)
                {
                    int SectionImportID = sheet.GetCellValue("AB6") != null ? int.Parse(sheet.GetCellValue("AB6").ToString()) : 0;
                    int tmp = SectionID.HasValue ? SectionID.Value : 0;
                    if (SectionImportID != tmp)
                    {
                        return Json(new JsonMessage(Res.Get("Teaching_Schedule_Excel_NHAP_KO_DUNG_BUOI"), "FileErr"));
                    }
                    else
                    {
                        return Json(new JsonMessage(Res.Get("Teaching_Schedule_Excel_NHAP_KHL"), "FileErr"));
                    }
                }

                string SchoolWeekIDImport = sheet.GetCellValue("AA6") != null ? sheet.GetCellValue("AA6").ToString() : "";
                if (SchoolWeekID != SchoolWeekIDImport)
                {
                    string strSchoolWeekName = "Từ ngày" + fromDate + " đến ngày " + toDate;
                    return Json(new JsonMessage(string.Format("{0}: {1}", Res.Get("Teaching_Schedule_Excel_NHAP_TUAN_SAI"), strSchoolWeekName), "FileErr"));
                }

                List<TeachingScheduleViewModel> lstResult = this.GetDataToFileExcel(sheet, lstFromTo, fromDate, toDate, lstSectionID, TeacherID);
                if (lstResult.Any(p => p.isErr))//co loi return ra trang loi
                {
                    Session["lstTeachingScheduleErr"] = lstResult;
                    return Json(new JsonMessage(Res.Get("Teaching_Schedule_Excel_NHAP_OK"), "Error"));
                }
                else
                {
                    IDictionary<string, object> dicSW = new Dictionary<string, object>();
                    dicSW.Add("fromDateToDate", SchoolWeekID);
                    dicSW.Add("AcademicYearID", _globalInfo.AcademicYearID);
                    dicSW.Add("SchoolID", _globalInfo.SchoolID);
                    List<SchoolWeek> lstSchoolWeek = SchoolWeekBusiness.Search(dicSW).ToList();
                    List<int> lstSchoolWeekID = lstSchoolWeek.Select(x => (int)x.SchoolWeekID).Distinct().ToList();

                    List<TeachingSchedule> lstTeachingScheduleNew = new List<TeachingSchedule>();
                    TeachingSchedule objNew = null;
                    TeachingScheduleViewModel objTSVM = null;
                    for (int i = 0; i < lstResult.Count; i++)
                    {
                        objTSVM = lstResult[i];
                        var objSW = lstSchoolWeek.Where(x => x.ClassID == objTSVM.ClassID).FirstOrDefault();
                        if (objSW != null)
                        {
                            objNew = new TeachingSchedule();
                            objNew.ScheduleDate = objTSVM.ScheduleDate;
                            objNew.DayOfWeek = GetDateOfWeek(objTSVM.ScheduleDate.DayOfWeek.ToString());
                            objNew.SectionID = objTSVM.Section;
                            objNew.SubjectID = objTSVM.SubjectID;
                            objNew.SubjectOrder = objTSVM.SubjectOrder;
                            objNew.AssignSubjectID = objTSVM.AssignSubjectID;
                            objNew.ClassID = objTSVM.ClassID;
                            objNew.TeachingScheduleOrder = objTSVM.TeachingScheduleOrder;
                            objNew.TeachingUtensil = objTSVM.TeachingUtensil;
                            objNew.Quantity = objTSVM.Number;
                            objNew.InRoom = objTSVM.InRoom;
                            objNew.IsSelfMade = objTSVM.IsSelfMade;
                            objNew.TeacherID = TeacherID;
                            objNew.SchoolWeekID = (int)objSW.SchoolWeekID;
                            objNew.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objNew.SchoolID = _globalInfo.SchoolID.Value;
                            objNew.Last2DigitNumberSchool = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                            lstTeachingScheduleNew.Add(objNew);
                        }
                    }
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"SchoolID",_globalInfo.SchoolID},
                        {"lstFromTo",lstFromTo},
                        {"lstSectionID",lstSectionID},
                        {"TeacherID",TeacherID}
                    };
                    TeachingScheduleBusiness.InsertOrUpdateList(lstTeachingScheduleNew, dic);
                    return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage"), "Success"));
                }
            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage"), "Success"));
        }

        /// <summary>
        /// Tải tệp excel sau khi đã export dữ liệu
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public FileResult DownloadFileTemplate(FormCollection frm)
        {
            string WeekID = !string.IsNullOrEmpty(frm["hdfImportWeekID"]) ? frm["hdfImportWeekID"] : string.Empty;
            int SectionID = !string.IsNullOrEmpty(frm["hdfImportSectionID"]) ? Int32.Parse(frm["hdfImportSectionID"]) : 0;
            int TeacherID = !string.IsNullOrEmpty(frm["hdfImportTeacherID"]) ? Int32.Parse(frm["hdfImportTeacherID"]) : 0;
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "GV", TeachingScheduleConstants.TEMPLATE_FILE);
            List<int> lstSectionID = new List<int>();

            List<ClassProfile> lstCP = ListClassProfile().OrderBy(p => p.OrderNumber).ToList();
            if (SectionID > 0)
            {
                lstSectionID.Add(SectionID);
            }
            else
            {
                //tinh ra so buoi lon nhat
                lstSectionID = UtilsBusiness.GetListSectionID(lstCP);
            }
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);

            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<WeekDefaultViewModel> ListWeekDefault = AutoGenericWeekDefaul(academicYear);

            WeekDefaultViewModel objWeekDefault = ListWeekDefault.Where(x => x.WeekDefaultID == WeekID).FirstOrDefault();

            sheet.SetComment(5, 8, Res.Get("Teaching_Schedule_Excel_PPCT_LAY_TU"));
            sheet.SetComment(6, 11, Res.Get("Teaching_Schedule_Excel_X_PTB"));
            sheet.SetComment(6, 12, Res.Get("Teaching_Schedule_Excel_X_TL"));
            #region fill du lieu
            //fill thong tin chung
            string WeekName = string.Empty;
            WeekName = string.Format("Từ ngày {0} đến {1} ", (objWeekDefault != null ? objWeekDefault.FromDate : "---"), (objWeekDefault != null ? objWeekDefault.ToDate : "---"));
            sheet.SetCellValue("A3", WeekName);
            Employee objE = EmployeeBusiness.Find(TeacherID);
            string EmployeeName = string.Format("{0}: ", Res.Get("Teaching_Schedule_Excel_GIAO_VIEN")) + objE.FullName;
            sheet.SetCellValue("A4", EmployeeName);
            sheet.SetCellValue("AA6", WeekID);
            sheet.SetCellValue("AB6", SectionID);
            //fill list
            IDictionary<string, object> dicSubjectAssign = new Dictionary<string, object>()
            {
                {"TeacherID",TeacherID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel}
            };
            List<TeachingAssignmentBO> lstTeachingAssign = TeachingAssignmentBusiness.GetSubjectTeacher(_globalInfo.SchoolID.Value, dicSubjectAssign).ToList();
            List<string> lstCLassNameAssign = lstTeachingAssign.Select(p => p.ClassName).Distinct().ToList();
            var lstSubjectNameAssign = lstTeachingAssign.Select(p => new { p.SubjectName, p.SubjectID }).Distinct().ToList();
            IDictionary<string, object> dicS = new Dictionary<string, object>()
            {
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID}
            };
            var lstSubjectCat = (from s in SchoolSubjectBusiness.Search(dicS)
                                 join sc in SubjectCatBusiness.All on s.SubjectID equals sc.SubjectCatID
                                 where s.SectionPerWeekFirstSemester.HasValue && s.SectionPerWeekFirstSemester.Value > 0
                                 && s.SectionPerWeekSecondSemester.HasValue && s.SectionPerWeekSecondSemester.Value > 0
                                 && sc.AppliedLevel == _globalInfo.AppliedLevel
                                 select sc).Distinct().OrderBy(p => p.OrderInSubject).Select(p => new { p.SubjectName, p.SubjectCatID }).ToList();

            List<string> lstClassName = lstCP.Select(p => p.DisplayName).ToList();
            List<string> lstSubjecttmp = new List<string>();
            List<string> lstClasstmp = new List<string>();
            lstSubjecttmp.AddRange(lstSubjectNameAssign.Select(x => x.SubjectName).ToList());
            lstSubjecttmp.AddRange(lstSubjectCat.Select(x => x.SubjectName).ToList());
            lstSubjecttmp = lstSubjecttmp.Distinct().ToList();

            lstClasstmp.AddRange(lstCLassNameAssign);
            lstClasstmp.AddRange(lstClassName);
            lstClasstmp = lstClasstmp.Distinct().ToList();
            //fill thong tin mon va lop
            IVTWorksheet sheetRef = oBook.GetSheet(2);
            for (int i = 0; i < lstSubjecttmp.Count; i++)
            {
                sheetRef.SetCellValue(2 + i, 1, lstSubjecttmp[i]);
            }

            for (int i = 0; i < lstClasstmp.Count; i++)
            {
                sheetRef.SetCellValue(2 + i, 2, lstClasstmp[i]);
            }

            var subjectIDs = lstSubjectNameAssign.Where(x => x.SubjectID != null).Select(x => (int)x.SubjectID).Distinct().ToList();
            subjectIDs.AddRange(lstSubjectCat.Select(x => x.SubjectCatID).Distinct().ToList());
            var lstAssignSubjectConfig = GetAssignSubjectListBySubjects(subjectIDs);
            var __itemIndex = 0;
            sheetRef.SetCellValue(2 + (__itemIndex++), 3, Res.Get("ItSelf"));
            foreach (var item in lstAssignSubjectConfig)
            {
                sheetRef.SetCellValue(2 + (__itemIndex++), 3, item.AssignSubjectName);
            }

            DateTime startDate = Convert.ToDateTime(objWeekDefault.FromDate);
            string dateName = string.Empty;
            string sectionName = string.Empty;
            int StartRow = 7;
            for (int i = 1; i <= 7; i++)//for 1 tuan 7 ngay co dinh
            {
                sheet.GetRange(StartRow, 1, StartRow + lstSectionID.Count * 5 - 1, 1).Merge();
                dateName = this.GetDateName(startDate.DayOfWeek.ToString()) + "\r\n" + " (" + startDate.ToString("dd/MM/yyyy") + ")";
                sheet.SetCellValue(StartRow, 1, dateName);
                for (int j = 0; j < lstSectionID.Count; j++)//for buoi hoc
                {
                    sheet.GetRange(StartRow, 2, StartRow + 4, 2).Merge();
                    sectionName = lstSectionID[j] == 1 ? Res.Get("Teaching_Schedule_Excel_SANG") : lstSectionID[j] == 2 ? Res.Get("Teaching_Schedule_Excel_CHIEU") : Res.Get("Teaching_Schedule_Excel_TOI");
                    sheet.SetCellValue(StartRow, 2, sectionName);
                    for (int k = 1; k < 6; k++)//for tiet hoc co dinh 5 tiet
                    {
                        sheet.SetCellValue(StartRow, 3, k);
                        StartRow++;
                    }

                }
                startDate = startDate.AddDays(1);
            }
            //ke khung
            sheet.GetRange(7, 1, 7 + 7 * lstSectionID.Count * 5 - 1, 13).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.HideColumn(13);
            sheet.Name = string.Format("{0}-{1}", objWeekDefault.FromDate, objWeekDefault.ToDate);
            #endregion
            string fileName = string.Format("PhieuBaoGiang_[{0}]_[{1}].xls", Utils.Utils.StripVNSignAndSpace(objE.FullName), sheet.Name);
            sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        /// <summary>
        /// Trả về tệp excel thông báo lỗi không import được dữ liệu vào hệ thống
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public FileResult DowloadFileImportErr(FormCollection frm)
        {
            string WeekID = !string.IsNullOrEmpty(frm["hdfImportErrWeekID"]) ? frm["hdfImportErrWeekID"] : "";
            int SectionID = !string.IsNullOrEmpty(frm["hdfImportErrSectionID"]) ? Int32.Parse(frm["hdfImportErrSectionID"]) : 0;
            int TeacherID = !string.IsNullOrEmpty(frm["hdfImportErrTeacherID"]) ? Int32.Parse(frm["hdfImportErrTeacherID"]) : 0;

            List<string> lstFromTo = new List<string>();
            string fromDate = string.Empty;
            string toDate = string.Empty;
            if (!string.IsNullOrEmpty(WeekID))
            {
                lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
                fromDate = lstFromTo[0];
                toDate = lstFromTo[1];
            }

            List<TeachingScheduleViewModel> lstResultErr = (List<TeachingScheduleViewModel>)Session["lstTeachingScheduleErr"];
            TeachingScheduleViewModel objResultErr = null;

            Employee objE = EmployeeBusiness.Find(TeacherID);
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "GV", TeachingScheduleConstants.TEMPLATE_FILE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            sheet.SetComment(5, 8, Res.Get("Teaching_Schedule_Excel_PPCT_LAY_TU"));
            sheet.SetComment(6, 11, Res.Get("Teaching_Schedule_Excel_X_PTB"));
            sheet.SetComment(6, 12, Res.Get("Teaching_Schedule_Excel_X_TL"));
            List<int> lstSectionID = new List<int>();

            List<ClassProfile> lstCP = ListClassProfile().OrderBy(p => p.OrderNumber).ToList();
            if (SectionID > 0)
            {
                lstSectionID.Add(SectionID);
            }
            else
            {
                //tinh ra so buoi lon nhat
                lstSectionID = UtilsBusiness.GetListSectionID(lstCP);
            }
            //fill thong tin chung
            string WeekName = string.Empty;
            WeekName = string.Format("Từ ngày {0} đến {1}", fromDate, toDate);
            sheet.SetCellValue("A3", WeekName);
            string EmployeeName = string.Format("{0}: ", Res.Get("Teaching_Schedule_Excel_GIAO_VIEN")) + objE.FullName;
            sheet.SetCellValue("A4", EmployeeName);
            sheet.SetCellValue("AA6", WeekID);
            sheet.SetCellValue("AB6", SectionID);
            //fill list
            IDictionary<string, object> dicSubjectAssign = new Dictionary<string, object>()
            {
                {"TeacherID",TeacherID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel}
            };
            List<TeachingAssignmentBO> lstTeachingAssign = TeachingAssignmentBusiness.GetSubjectTeacher(_globalInfo.SchoolID.Value, dicSubjectAssign).ToList();
            List<string> lstCLassNameAssign = lstTeachingAssign.Select(p => p.ClassName).Distinct().ToList();
            var lstSubjectNameAssign = lstTeachingAssign.Select(p => new { p.SubjectName, p.SubjectID }).Distinct().ToList();
            IDictionary<string, object> dicS = new Dictionary<string, object>()
            {
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID}
            };
            var lstSubjectCat = (from s in SchoolSubjectBusiness.Search(dicS)
                                 join sc in SubjectCatBusiness.All on s.SubjectID equals sc.SubjectCatID
                                 where s.SectionPerWeekFirstSemester.HasValue && s.SectionPerWeekFirstSemester.Value > 0
                                 && s.SectionPerWeekSecondSemester.HasValue && s.SectionPerWeekSecondSemester.Value > 0
                                 && sc.AppliedLevel == _globalInfo.AppliedLevel
                                 select sc).Distinct().OrderBy(p => p.OrderInSubject).Select(p => new { p.SubjectName, p.SubjectCatID }).ToList();
            List<string> lstClassName = lstCP.Select(p => p.DisplayName).ToList();
            List<string> lstSubjecttmp = new List<string>();
            List<string> lstClasstmp = new List<string>();
            lstSubjecttmp.AddRange(lstSubjectNameAssign.Select(x => x.SubjectName).ToList());
            lstSubjecttmp.AddRange(lstSubjectCat.Select(x => x.SubjectName).ToList());
            lstSubjecttmp = lstSubjecttmp.Distinct().ToList();

            lstClasstmp.AddRange(lstCLassNameAssign);
            lstClasstmp.AddRange(lstClassName);
            lstClasstmp = lstClasstmp.Distinct().ToList();
            //fill thong tin mon va lop
            IVTWorksheet sheetRef = oBook.GetSheet(2);
            for (int i = 0; i < lstSubjecttmp.Count; i++)
            {
                sheetRef.SetCellValue(2 + i, 1, lstSubjecttmp[i]);
            }

            for (int i = 0; i < lstClasstmp.Count; i++)
            {
                sheetRef.SetCellValue(2 + i, 2, lstClasstmp[i]);
            }
            var subjectIDs = lstSubjectNameAssign.Where(x => x.SubjectID != null).Select(x => (int)x.SubjectID).Distinct().ToList();
            subjectIDs.AddRange(lstSubjectCat.Select(x => x.SubjectCatID).Distinct().ToList());
            var lstAssignSubjectConfig = GetAssignSubjectListBySubjects(subjectIDs);
            var __itemIndex = 0;
            sheetRef.SetCellValue(2 + (__itemIndex++), 3, Res.Get("ItSelf"));
            foreach (var item in lstAssignSubjectConfig)
            {
                sheetRef.SetCellValue(2 + (__itemIndex++), 3, item.AssignSubjectName);
            }

            DateTime startDate = Convert.ToDateTime(fromDate);
            string dateName = string.Empty;
            string sectionName = string.Empty;
            int StartRow = 7;
            for (int i = 1; i <= 7; i++)//for 1 tuan 7 ngay co dinh
            {
                sheet.GetRange(StartRow, 1, StartRow + lstSectionID.Count * 5 - 1, 1).Merge();
                dateName = this.GetDateName(startDate.DayOfWeek.ToString()) + "\r\n" + " (" + startDate.ToString("dd/MM/yyyy") + ")";
                sheet.SetCellValue(StartRow, 1, dateName);
                for (int j = 0; j < lstSectionID.Count; j++)//for buoi hoc
                {
                    sheet.GetRange(StartRow, 2, StartRow + 4, 2).Merge();
                    sectionName = lstSectionID[j] == 1 ? Res.Get("Teaching_Schedule_Excel_SANG") : lstSectionID[j] == 2 ? Res.Get("Teaching_Schedule_Excel_CHIEU") : Res.Get("Teaching_Schedule_Excel_TOI");
                    sheet.SetCellValue(StartRow, 2, sectionName);
                    for (int k = 1; k < 6; k++)//for tiet hoc co dinh 5 tiet
                    {
                        sheet.SetCellValue(StartRow, 3, k);
                        objResultErr = lstResultErr.FirstOrDefault(p => p.ScheduleDate == startDate && p.Section == lstSectionID[j] && p.SubjectOrder == k);
                        if (objResultErr != null)
                        {
                            sheet.SetCellValue(StartRow, 4, objResultErr.SubjectName);
                            sheet.SetCellValue(StartRow, 5, string.IsNullOrEmpty(objResultErr.AssignSubjectName) ? Res.Get("ItSelf") : objResultErr.AssignSubjectName);
                            sheet.SetCellValue(StartRow, 6, objResultErr.ClassName);
                            sheet.SetCellValue(StartRow, 7, objResultErr.TeachingScheduleOrder);
                            sheet.SetCellValue(StartRow, 9, objResultErr.TeachingUtensil);
                            sheet.SetCellValue(StartRow, 10, objResultErr.Number);
                            sheet.SetCellValue(StartRow, 11, objResultErr.InRoom.HasValue && objResultErr.InRoom.Value ? "x" : "");
                            sheet.SetCellValue(StartRow, 12, objResultErr.IsSelfMade.HasValue && objResultErr.IsSelfMade.Value ? "x" : "");
                            sheet.SetCellValue(StartRow, 13, objResultErr.ErrorMessage);
                        }
                        StartRow++;
                    }
                }
                startDate = startDate.AddDays(1);
            }
            sheet.GetRange(7, 1, 7 + 7 * lstSectionID.Count * 5 - 1, 13).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.Name = WeekID.Replace("/", "").Replace("*", "-");
            sheet.ProtectSheet();

            string fileName = string.Format("PhieuBaoGiang[{0}]_[{1}].xls", Utils.Utils.StripVNSignAndSpace(objE.FullName), sheet.Name);
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        /// <summary>
        /// Lấy dữ liệu từ file excel để đưa vào hệ thống
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="WeekID"></param>
        /// <param name="lstSectionID"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>      
        [ValidateInput(true)]
        private List<TeachingScheduleViewModel> GetDataToFileExcel(IVTWorksheet sheet, List<string> lstFromTo, string fromDate, string toDate, List<int> lstSectionID, int TeacherID)
        {
            List<TeachingScheduleViewModel> lstResult = new List<TeachingScheduleViewModel>();
            TeachingScheduleViewModel objTeachingSchedule = null;
            DateTime FromDate = Convert.ToDateTime(fromDate);
            int startRow = 7;
            IDictionary<string, object> dicSC = new Dictionary<string, object>()
            {
                {"AppliedLevel",_globalInfo.AppliedLevel},
                {"IsActive",true}
            };
            List<SubjectCat> lstSubjectCat = SubjectCatBusiness.Search(dicSC).ToList();
            SubjectCat objSC = null;

            var lstAssignSubjectConfig = GetAssignSubjectListBySubjects(lstSubjectCat.Select(x => x.SubjectCatID).ToList());
            AssignSubjectConfig objASC = null;

            List<ClassProfile> lstCP = ListClassProfile();
            ClassProfile objCP = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"lstFromTo", lstFromTo},
                {"lstSectionID",lstSectionID}
            };
            List<TeachingScheduleViewModel> lstTeachingScheduleDB = (from t in TeachingScheduleBusiness.Search(dic)
                                                                     join c in ClassProfileBusiness.All on t.ClassID equals c.ClassProfileID
                                                                     join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                                                                     where c.IsActive.Value && t.Status == 0
                                                                     select new TeachingScheduleViewModel
                                                                     {
                                                                         ScheduleDate = t.ScheduleDate,
                                                                         Section = t.SectionID,
                                                                         SubjectOrder = t.SubjectOrder,
                                                                         SubjectID = t.SubjectID,
                                                                         SubjectName = sc.SubjectName,
                                                                         ClassID = t.ClassID,
                                                                         ClassName = c.DisplayName,
                                                                         TeachingScheduleOrder = t.TeachingScheduleOrder,
                                                                         TeachingUtensil = t.TeachingUtensil,
                                                                         Number = t.Quantity,
                                                                         InRoom = t.InRoom,
                                                                         IsSelfMade = t.IsSelfMade,
                                                                         TeacherID = t.TeacherID,
                                                                         AssignSubjectID = t.AssignSubjectID
                                                                     }).ToList();
            TeachingScheduleViewModel objTS = new TeachingScheduleViewModel();

            string subjectName = string.Empty;
            string assignSubjectName = string.Empty;
            string className = string.Empty;
            string distributeProgramOrder = string.Empty;
            string ErrorMessage = string.Empty;
            string TeachingUtensilName = string.Empty;
            string InRoom = string.Empty;
            string IsSelfMade = string.Empty;
            bool isErr = false;
            int Number = 0;
            List<ObjImportErr> lstScheduleOrder = new List<ObjImportErr>();

            bool isAssignSubject = false;
            int ScheduleOrder = 0;
            for (int i = 1; i <= 7; i++)
            {
                for (int j = 0; j < lstSectionID.Count; j++)
                {
                    for (int k = 1; k < 6; k++)
                    {
                        ErrorMessage = string.Empty;
                        subjectName = sheet.GetCellValue(startRow, 4) != null ? sheet.GetCellValue(startRow, 4).ToString() : "";
                        assignSubjectName = sheet.GetCellValue(startRow, 5) != null ? sheet.GetCellValue(startRow, 5).ToString() : "";
                        className = sheet.GetCellValue(startRow, 6) != null ? sheet.GetCellValue(startRow, 6).ToString() : "";

                        if (string.IsNullOrEmpty(subjectName) || string.IsNullOrEmpty(className))
                        {
                            startRow++;
                            continue;
                        }

                        if (sheet.GetCellValue("Y" + startRow) != null)
                        {
                            string val = sheet.GetCellValue("Y" + startRow).ToString();
                            if ((Int32.Parse(val)) != 0)
                            {
                                startRow++;
                                continue;
                            }
                        }

                        objTeachingSchedule = new TeachingScheduleViewModel();
                        objTeachingSchedule.ScheduleDate = FromDate;
                        objTeachingSchedule.Section = lstSectionID[j];
                        objTeachingSchedule.SubjectOrder = k;

                        objSC = lstSubjectCat.FirstOrDefault(p => p.SubjectName.Equals(subjectName));

                        //Kiem tra mon hoc
                        if (objSC != null)
                        {
                            objTeachingSchedule.SubjectID = objSC.SubjectCatID;
                            objTeachingSchedule.SubjectName = objSC.SubjectName;
                        }
                        else
                        {
                            objTeachingSchedule.SubjectName = subjectName;
                            ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_MON_KO_HL"));
                            isErr = true;
                        }

                        // kiem tra phan mon                 
                        // Chọn phâm môn, không chọn [Chính]
                        if (!string.IsNullOrEmpty(assignSubjectName) && !assignSubjectName.Equals(Res.Get("ItSelf")))
                        {
                            var lstAssignSubjectBySubjectID = lstAssignSubjectConfig.Where(x => x.SubjectID == objTeachingSchedule.SubjectID).ToList();

                            objASC = lstAssignSubjectBySubjectID.Where(x => x.AssignSubjectName.Equals(assignSubjectName)).FirstOrDefault();
                            if (objASC != null)
                            {
                                objTeachingSchedule.AssignSubjectID = objASC.AssignSubjectConfigID;
                                objTeachingSchedule.AssignSubjectName = objASC.AssignSubjectName;
                                isAssignSubject = true;
                            }
                            else
                            {
                                objTeachingSchedule.AssignSubjectName = assignSubjectName;
                                ErrorMessage += string.Format("{0}\n\r", "Phân môn không hợp lệ.");
                                isErr = true;
                            }
                        }// Để trống hoặc chọn [Chính]
                        else if (string.IsNullOrEmpty(assignSubjectName) || assignSubjectName.Equals(Res.Get("ItSelf")))
                        {
                            //objASC = lstAssignSubjectBySubjectID.FirstOrDefault();
                        }

                        //Kiem tra lop hoc
                        objCP = lstCP.FirstOrDefault(p => p.DisplayName.Equals(className));
                        if (objCP != null)
                        {
                            objTeachingSchedule.ClassID = objCP.ClassProfileID;
                            objTeachingSchedule.ClassName = objCP.DisplayName;
                        }
                        else
                        {
                            objTeachingSchedule.ClassName = className;
                            ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_LOP_KO_HL"));
                            isErr = true;
                        }

                        // Kiem tra tiet PPCT
                        if (sheet.GetCellValue(startRow, 7) == null)
                        {
                            ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_PPCT_CHUA_NHAP"));
                            isErr = true;
                        }
                        else
                        {
                            if (SMAS.Business.Common.Utils.IsInt32(sheet.GetCellValue(startRow, 7).ToString()))
                            {
                                Int32.TryParse(sheet.GetCellValue(startRow, 7).ToString(), out ScheduleOrder);
                                if (objCP != null && objASC != null)
                                {
                                    // là phân môn
                                    if (isAssignSubject)
                                    {
                                        objTS = lstTeachingScheduleDB.Where(p => p.ClassID == objCP.ClassProfileID
                                                          && p.SubjectID == objSC.SubjectCatID
                                                          && p.TeachingScheduleOrder == ScheduleOrder
                                                          && p.AssignSubjectID == objASC.AssignSubjectConfigID
                                                          && (p.ScheduleDate != FromDate || p.ScheduleDate == FromDate)
                                                          && p.Section != lstSectionID[j]
                                                          && p.SubjectOrder != k).FirstOrDefault();

                                        if (objTS != null)
                                        {
                                            ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_PPCT_DA_DK"));
                                            isErr = true;
                                        }
                                        else
                                        {
                                            if (lstScheduleOrder.Any(p => p.ClassID == objCP.ClassProfileID
                                                               && p.SubjectID == objSC.SubjectCatID
                                                               && p.AssignSubjectID == objASC.AssignSubjectConfigID
                                                               && p.ScheduleOrder == ScheduleOrder))
                                            {
                                                ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_PPCT_DA_DK"));
                                                isErr = true;
                                            }
                                            else
                                            {
                                                ObjImportErr objScheduleOrder = new ObjImportErr();
                                                objScheduleOrder.ClassID = objCP.ClassProfileID;
                                                objScheduleOrder.SubjectID = objSC.SubjectCatID;
                                                objScheduleOrder.AssignSubjectID = objASC.AssignSubjectConfigID;
                                                objScheduleOrder.ScheduleOrder = ScheduleOrder;
                                                lstScheduleOrder.Add(objScheduleOrder);
                                            }
                                        }
                                    }
                                    else
                                    {// là môn chính
                                        objTS = lstTeachingScheduleDB.Where(p => p.ClassID == objCP.ClassProfileID
                                                         && p.SubjectID == objSC.SubjectCatID
                                                         && p.TeachingScheduleOrder == ScheduleOrder
                                                         && p.AssignSubjectID == null
                                                         && (p.ScheduleDate != FromDate || p.ScheduleDate == FromDate)
                                                         && p.Section != lstSectionID[j]
                                                         && p.SubjectOrder != k).FirstOrDefault();

                                        if (objTS != null)
                                        {
                                            ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_PPCT_DA_DK"));
                                            isErr = true;
                                        }
                                        else
                                        {
                                            if (lstScheduleOrder.Any(p => p.ClassID == objCP.ClassProfileID
                                                               && p.SubjectID == objSC.SubjectCatID
                                                               && p.AssignSubjectID == null
                                                               && p.ScheduleOrder == ScheduleOrder))
                                            {
                                                ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_PPCT_DA_DK"));
                                                isErr = true;
                                            }
                                            else
                                            {
                                                ObjImportErr objScheduleOrder = new ObjImportErr();
                                                objScheduleOrder.ClassID = objCP.ClassProfileID;
                                                objScheduleOrder.SubjectID = objSC.SubjectCatID;
                                                objScheduleOrder.AssignSubjectID = null;
                                                objScheduleOrder.ScheduleOrder = ScheduleOrder;
                                                lstScheduleOrder.Add(objScheduleOrder);
                                            }
                                        }
                                    }
                                }

                                objTeachingSchedule.TeachingScheduleOrder = ScheduleOrder;
                            }
                            else
                            {
                                ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_PPCT_KO_HL"));
                                isErr = true;
                            }
                        }

                        if (sheet.GetCellValue(startRow, 9) != null)
                        {
                            TeachingUtensilName = sheet.GetCellValue(startRow, 9).ToString();
                            if (TeachingUtensilName.Length > 300)
                            {
                                TeachingUtensilName = TeachingUtensilName.Substring(0, 300);
                            }
                            objTeachingSchedule.TeachingUtensil = TeachingUtensilName;
                            if (sheet.GetCellValue(startRow, 10) != null)
                            {
                                if (int.TryParse(sheet.GetCellValue(startRow, 10).ToString(), out Number))
                                {
                                    if (Number > 99)
                                    {
                                        ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_SL_0_99"));
                                        isErr = true;
                                    }
                                    else
                                    {
                                        objTeachingSchedule.Number = Number;
                                    }
                                }
                                else
                                {
                                    ErrorMessage += string.Format("{0}\n\r", Res.Get("Teaching_Schedule_Excel_SL_0_99"));
                                    isErr = true;
                                }
                            }

                            InRoom = sheet.GetCellValue(startRow, 11) != null ? sheet.GetCellValue(startRow, 11).ToString() : "";
                            IsSelfMade = sheet.GetCellValue(startRow, 12) != null ? sheet.GetCellValue(startRow, 12).ToString() : "";

                            if (!string.IsNullOrEmpty(InRoom) && InRoom.Equals("x") && !string.IsNullOrEmpty(IsSelfMade))
                            {
                                objTeachingSchedule.InRoom = true;
                            }
                            else if (!string.IsNullOrEmpty(InRoom) && InRoom.Equals("x") && string.IsNullOrEmpty(IsSelfMade))
                            {
                                objTeachingSchedule.InRoom = true;
                            }
                            else if (string.IsNullOrEmpty(InRoom) && IsSelfMade.Equals("x") && !string.IsNullOrEmpty(IsSelfMade))
                            {
                                objTeachingSchedule.IsSelfMade = true;
                            }
                            else
                            {
                                objTeachingSchedule.InRoom = true;
                            }
                        }
                        objTeachingSchedule.ErrorMessage = ErrorMessage;
                        objTeachingSchedule.isErr = isErr;
                        lstResult.Add(objTeachingSchedule);
                        startRow++;
                        isAssignSubject = false;
                    }
                }
                FromDate = FromDate.AddDays(1);
            }
            return lstResult;
        }
        #endregion

        /// <summary>
        /// Đưa dữ liệu vào viewdata để hiển thị dưới cshtml(view)
        /// </summary>
        private void SetViewData()
        {
            //tinh ra so buoi lon nhat

            List<ClassProfile> lstCP = ListClassProfile();
            if (lstCP.Count > 0)
            {
                AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                List<WeekDefaultViewModel> ListWeekDefault = AutoGenericWeekDefaul(academicYear);

                DateTime dateNow = DateTime.Now;

                var objWeekNow = ListWeekDefault.Where(x => x.StartDate.AddDays(6) >= dateNow && x.StartDate <= dateNow).FirstOrDefault();
                if (objWeekNow != null)
                {
                    ViewData[TeachingScheduleConstants.LIST_WEEKNAME] = new SelectList(ListWeekDefault, "WeekDefaultID", "WeekDefaultName", objWeekNow.WeekDefaultID);
                }
                else
                {
                    ViewData[TeachingScheduleConstants.LIST_WEEKNAME] = new SelectList(ListWeekDefault, "WeekDefaultID", "WeekDefaultName", false);
                }

                ViewData[TeachingScheduleConstants.LIST_SECTION] = new SelectList(this.GetSectionName(lstCP), "key", "value");

                Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
                dicToGetTeacher["CurrentSchoolID"] = _globalInfo.SchoolID;
                //dicToGetTeacher["EmployeeID"] = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0;
                IQueryable<TeacherComboboxViewModel> query = (from q in EmployeeBusiness.Search(dicToGetTeacher)
                                                              where (q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)
                                                              select new TeacherComboboxViewModel
                                                              {
                                                                  EmployeeID = q.EmployeeID,
                                                                  DisplayName = q.FullName + " - " + q.EmployeeCode + " - " + q.SchoolFaculty.FacultyName
                                                              });

                ViewData[TeachingScheduleConstants.LIST_TEACHER] = query;
                ViewData[TeachingScheduleConstants.IS_TEACHER] = _globalInfo.EmployeeID.HasValue && _globalInfo.EmployeeID.Value > 0 && query.Any(o => o.EmployeeID.Equals((int)_globalInfo.EmployeeID));
                ViewData[TeachingScheduleConstants.IS_ADMIN] = _globalInfo.IsAdmin;
                ViewData[TeachingScheduleConstants.IS_ENABLED] = true;
            }
            else
            {
                ViewData[TeachingScheduleConstants.IS_ENABLED] = false;
            }
            ViewData[TeachingScheduleConstants.LOGGED_TEACHER_ID] = _globalInfo.EmployeeID;
        }

        private List<TeachingScheduleViewModel> ListTeachingScheduleVM(IQueryable<TeachingSchedule> lstTeachingSchedule)
        {
            List<TeachingScheduleViewModel> lstResult = (from t in lstTeachingSchedule
                                                         join c in ClassProfileBusiness.All on t.ClassID equals c.ClassProfileID
                                                         join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                                                         join asc in AssignSubjectConfigBusiness.All on t.AssignSubjectID equals asc.AssignSubjectConfigID
                                                         into l
                                                         from l1 in l.Where(xx => xx.SubjectID == t.SubjectID).DefaultIfEmpty()
                                                         where c.IsActive.Value && sc.AppliedLevel == _globalInfo.AppliedLevel
                                                            && c.SchoolID == _globalInfo.SchoolID.Value
                                                         select new TeachingScheduleViewModel
                                                         {
                                                             ScheduleDate = t.ScheduleDate,
                                                             SchoolWeekID = t.SchoolWeekID,
                                                             Section = t.SectionID,
                                                             SubjectOrder = t.SubjectOrder,
                                                             SubjectID = t.SubjectID,
                                                             SubjectName = sc.SubjectName,
                                                             ClassID = t.ClassID,
                                                             ClassName = c.DisplayName,
                                                             TeachingScheduleOrder = t.TeachingScheduleOrder,
                                                             TeachingUtensil = t.TeachingUtensil,
                                                             Number = t.Quantity,
                                                             InRoom = t.InRoom,
                                                             IsSelfMade = t.IsSelfMade,
                                                             EducationLevelID = c.EducationLevelID,
                                                             TeachingScheduleID = t.TeachingScheduleID,
                                                             AssignSubjectID = t.AssignSubjectID,
                                                             AssignSubjectName = l1 != null ? l1.AssignSubjectName : "",
                                                             Status = t.Status
                                                         }).ToList();
            return lstResult;
        }

        private List<ClassProfile> ListClassProfile()
        {
            IDictionary<string, object> dicCP = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevel",_globalInfo.AppliedLevel}
            };
            List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicCP).ToList();
            return lstCP;
        }

        // Hàm lấy obj đã phê/hủy duyệt hay chưa
        private TeachingScheduleApproval CheckApproval(IDictionary<string, object> dic)
        {
            this.TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);
            var obj = this.TeachingScheduleApprovalBusiness.Search(dic)
                            .OrderByDescending(x => x.TeachingScheduleApprovalID)
                            .FirstOrDefault();
            return obj;
        }

        private string GetDateName(string strInput)
        {
            string strOutput = string.Empty;
            switch (strInput)
            {
                case "Monday":
                    strOutput = Res.Get("Teaching_Schedule_THU_2");
                    break;
                case "Tuesday":
                    strOutput = Res.Get("Teaching_Schedule_THU_3");
                    break;
                case "Wednesday":
                    strOutput = Res.Get("Teaching_Schedule_THU_4");
                    break;
                case "Thursday":
                    strOutput = Res.Get("Teaching_Schedule_THU_5");
                    break;
                case "Friday":
                    strOutput = Res.Get("Teaching_Schedule_THU_6");
                    break;
                case "Saturday":
                    strOutput = Res.Get("Teaching_Schedule_THU_7");
                    break;
                case "Sunday":
                    strOutput = Res.Get("Message_Sunday_Label");
                    break;
            };
            return strOutput;
        }

        private List<ComboObject> GetSectionName(List<ClassProfile> lstcp)
        {
            List<int> lstSectionID = UtilsBusiness.GetListSectionID(lstcp);
            List<ComboObject> listSectionName = new List<ComboObject>();
            for (int i = 0; i < lstSectionID.Count; i++)
            {
                if (lstSectionID[i] == 1)
                {
                    listSectionName.Add(new ComboObject(SystemParamsInFile.SECTION_MORNING.ToString(), Res.Get("Home_Label_Morning")));

                }
                else if (lstSectionID[i] == 2)
                {
                    listSectionName.Add(new ComboObject(SystemParamsInFile.SECTION_AFTERNOON.ToString(), Res.Get("Home_Label_Afternoon")));
                }
                else
                {
                    listSectionName.Add(new ComboObject(SystemParamsInFile.SECTION_EVENING.ToString(), Res.Get("Home_Label_Evening")));
                }
            }
            return listSectionName;
        }

        public class Week
        {
            public string WeekName { get; set; }
            public long WeekID { get; set; }
        }
        public class DateName
        {
            public DateTime Date { get; set; }
            public string Name { get; set; }
        }
        private class ObjImportErr
        {
            public int ClassID { get; set; }
            public int SubjectID { get; set; }
            public int? AssignSubjectID { get; set; }
            public int ScheduleOrder { get; set; }
        }
        public class DateOfWeekModel
        {
            public DateTime Date { get; set; }
            public int DateOfWeek { get; set; }
        }
        private int GetDateOfWeek(string strInput)
        {
            switch (strInput)
            {
                case "Monday":
                    return SystemParamsInFile.DAY_OF_WEEK_MONDAY;
                case "Tuesday":
                    return SystemParamsInFile.DAY_OF_WEEK_TUESDAY;
                case "Wednesday":
                    return SystemParamsInFile.DAY_OF_WEEK_WEDNESDAY;
                case "Thursday":
                    return SystemParamsInFile.DAY_OF_WEEK_THURSDAY;
                case "Friday":
                    return SystemParamsInFile.DAY_OF_WEEK_FRIDAY;
                case "Saturday":
                    return SystemParamsInFile.DAY_OF_WEEK_SATURDAY;
                case "Sunday":
                    return SystemParamsInFile.DAY_OF_WEEK_SUNDAY;
            };
            return 0;
        }

        private string GetNameToDay(string strInput)
        {
            switch (strInput)
            {
                case "Monday":
                    return "Hai";
                case "Tuesday":
                    return "Ba";
                case "Wednesday":
                    return "Tư";
                case "Thursday":
                    return "Năm";
                case "Friday":
                    return "Sáu";
                case "Saturday":
                    return "Bảy";
                case "Sunday":
                    return "CN";
            };
            return "";
        }

        private List<WeekDefaultViewModel> AutoGenericWeekDefaul(AcademicYear objAcademicYear)
        {
            // lay ngay thu 2 cua tuan bat dau nam hoc
            DateTime? startDayOfAcademic = objAcademicYear.FirstSemesterStartDate;
            int delta = DayOfWeek.Monday - startDayOfAcademic.Value.DayOfWeek;
            DateTime mondaystartDayOfAcademic = startDayOfAcademic.Value.AddDays(delta);

            //Lay ngay CN cua tuan ket thuc nam hoc
            DateTime? endDayOfAcademic = objAcademicYear.SecondSemesterEndDate;//SMAS.Business.Common.Utils.GetDateTime(dic, "SecondSemesterEndDate");
            int delta2 = DayOfWeek.Monday - endDayOfAcademic.Value.DayOfWeek;
            DateTime sundayEndDayOfAcademic = endDayOfAcademic.Value.AddDays(delta2 + 6);

            //Tao tuan tu dong
            TimeSpan diffDay = (sundayEndDayOfAcademic - mondaystartDayOfAcademic);
            List<WeekDefaultViewModel> lstDefault = new List<WeekDefaultViewModel>();
            WeekDefaultViewModel obj = null;

            string fromDate = "";
            string toDate = "";
            for (int i = 0; i <= diffDay.TotalDays; i += 7)
            {
                fromDate = mondaystartDayOfAcademic.AddDays(i).ToString("dd/MM/yyyy");
                toDate = mondaystartDayOfAcademic.AddDays(i + 6).AddHours(23).AddMinutes(59).AddSeconds(55).ToString("dd/MM/yyyy");

                obj = new WeekDefaultViewModel();
                obj.WeekDefaultName = "Từ " + fromDate + " đến " + toDate;
                obj.WeekDefaultID = fromDate + "*" + toDate;
                obj.FromDate = fromDate;
                obj.ToDate = toDate;
                obj.StartDate = mondaystartDayOfAcademic.AddDays(i);
                obj.ToDateTime = mondaystartDayOfAcademic.AddDays(i + 6);
                lstDefault.Add(obj);
                fromDate = "";
                toDate = "";
            }

            return lstDefault;
        }
    }
}