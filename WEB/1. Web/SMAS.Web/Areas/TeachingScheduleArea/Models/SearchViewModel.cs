﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.TeachingScheduleArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("DistributeProgram_Label_Week")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TeachingScheduleConstants.LIST_WEEKNAME)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public string WeekID { get; set; }

        [ResourceDisplayName("DistributeProgram_Label_Section")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TeachingScheduleConstants.LIST_SECTION)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public string SectionID { get; set; }
    }
}