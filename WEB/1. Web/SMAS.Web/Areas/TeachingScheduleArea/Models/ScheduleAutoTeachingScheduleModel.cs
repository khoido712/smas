﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeachingScheduleArea.Models
{
    public class ScheduleAutoTeachingScheduleModel
    {
        public int SchoolWeekID { get; set; }
        public string SchoolWeekName { get; set; }
        public int EmployeeID { get; set; }
        public bool IsPresent { get; set; }
        public DateTime FromDate { get; set; }
        public string WeekIDDate { get; set; }
        public string DisplayNote { get; set; }
        public string StrFromDate { get; set; }
    }
}