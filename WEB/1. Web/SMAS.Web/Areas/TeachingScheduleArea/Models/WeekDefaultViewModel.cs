﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeachingScheduleArea.Models
{
    public class WeekDefaultViewModel
    {
        public string WeekDefaultName { get; set; }
        public string WeekDefaultID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ToDateTime { get; set; }
    }
}