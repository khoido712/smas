﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeachingScheduleArea.Models
{
    public class TeachingScheduleViewModel
    {
        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Employee")]
        public int? EmployeeID { get; set; }
        public DateTime ScheduleDate { get; set; }
        public int Section { get; set; }
        public int SubjectOrder { get; set; }
        public string SubjectName { get; set; }
        public int SubjectID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int TeachingScheduleOrder { get; set; }
        public String LessonName { get; set; }
        public string TeachingUtensil { get; set; }
        public int? Number { get; set; }
        public bool? InRoom { get; set; }
        public bool? IsSelfMade { get; set; }
        public bool isErr { get; set; }
        public string ErrorMessage { get; set; }
        public int EducationLevelID { get; set; }
        public int SchoolWeekID { get; set; }

        public long TeachingScheduleID { get; set; }

        public int TeacherID { get; set; }

        public int? AssignSubjectID { get; set; }
        public string AssignSubjectName { get; set; }
        public int Status { get; set; }
    }
}