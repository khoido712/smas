﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeachingScheduleArea.Models
{
    public class ApprovalTeachingScheduleModel
    {
        public int SchoolWeekID { get; set; }
        public int SchoolWeekOrder { get; set; }
        public int TeacherID { get; set; }
        public string TeacherName { get; set; }
        public string Note { get; set; }
        public DateTime FromDate { get; set; }
        public string WeekIDDate { get; set; }
        public string DisplayNote { get; set; }
        public string StrFromDate { get; set; }
    }
}