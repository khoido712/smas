﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea
{
    public class RegistrationOfSpecializationAndElectivesConstants
    {
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string LIST_STATUS = "listStatus";
        public const string LIST_RESULT = "lstRegistrationSpec";
        public const string LIST_REGISTER_SUBJECT = "lstRegisterSubject";
        public const string SubjectID = "subjectID";
        public const string SubjectTypeID = "subjectTypeID";
        public const string CLASS_ID = "ClassID";
        public const string EDUCATION_LEVEL_ID = "EduactionLevelID";
        public const string ACADEMIC_YEAR = "AcademicYear";
        public const string CHECK_AUTHORIZATION = "CheckAuthorization";
        public const int GENERAL_JOBS = 4;
    }
}