﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea.Models
{
    public class RegisterPupilSubjectViewModel
    {
        public int SubjectID { get; set; }       
        public int ClassID { get; set; }
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public DateTime Birthday { get; set; }
        public int Genre { get; set; }
        public int? SubjectTypeID { get; set; }
       
        public int? AppliedType { get; set; }

        /// <summary>
        /// Trang thai hoc sinh: dang hoc, chuyen truong ...
        /// </summary>
        public int Status { get; set; }

        public bool IsActive { get; set; }

        public int Semester { get; set; }
        public int SemesterI { get; set; }
        public int SemesterII { get; set; }

        /// <summary>
        /// STT hoc sinh trong lop
        /// </summary>
        public int? OrderInClass { get; set; }

        public decimal? SectionPerWeekFirstSemester { get; set; }
        public decimal? SectionPerWeekSecondSemester { get; set; }

        /// <summary>
        /// Mien giam hoc ky 1
        /// </summary>
        public int? FirstSemesterExempt { get; set; }

        /// <summary>
        /// Mien giam hoc ky 2
        /// </summary>
        public int? SecondSemesterExempt { get; set; }
    }
}