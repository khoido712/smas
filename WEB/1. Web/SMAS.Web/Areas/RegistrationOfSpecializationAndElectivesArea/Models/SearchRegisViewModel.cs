﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea.Models
{
    public class SearchRegisViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", RegistrationOfSpecializationAndElectivesConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadClassRegis(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("Calendar_Label_ClassID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", RegistrationOfSpecializationAndElectivesConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadGridRegis()")]
        public int? Class { get; set; }

        [ResourceDisplayName("Trạng thái")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey",RegistrationOfSpecializationAndElectivesConstants.LIST_STATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadGridRegis()")]
        public int? Status { get; set; }     

    }
}