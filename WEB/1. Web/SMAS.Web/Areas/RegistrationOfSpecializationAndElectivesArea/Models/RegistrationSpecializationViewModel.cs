﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea.Models
{
    public class RegistrationSpecializationViewModel
    {        
        public long SubjectClassID { get; set; }
        public int SubjectCatID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("Calendar_Label_ClassID")]
        public string ClassName { get; set; }

        [ResourceDisplayName("SubjectCat_Label_SubjectName")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("SubjectCat_Label_SubjectName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// ký hiệu môn học
        /// </summary>
        [ResourceDisplayName("SubjectCat_Label_Abbreviation1")]
        public string Abbreviation { get; set; }

        /// <summary>
        /// Môn chuyên
        /// </summary>
        [ResourceDisplayName("ClassSubject_Label_IsSpecializedSubject")]
        public bool? IsSpecializedSubject { get; set; }

        /// <summary>
        /// Loại môn (môn tự chọn)
        /// </summary>
        [ResourceDisplayName("ClassSubject_Label_AppliedType")]
        public int? AppliedType { get; set; }

        /// <summary>
        /// Số học sinh đăng ký
        /// </summary>
        [ResourceDisplayName("RegistrationSpecialization_Label_Number")]
        public string NumberRegistration { get; set; }

        /// <summary>
        /// So thu tu lop
        /// </summary>
        public int? OrderNumber { get; set; }


        /// <summary>
        /// So thu tu cua mon 
        /// </summary>
        public int? OrderInSubject { get; set; }
    }
}