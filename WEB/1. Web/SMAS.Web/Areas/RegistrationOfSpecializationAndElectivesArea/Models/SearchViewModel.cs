﻿using Resources;
using SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", RegistrationOfSpecializationAndElectivesConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("Calendar_Label_ClassID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", RegistrationOfSpecializationAndElectivesConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Class { get; set; }
    }
}