﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea
{
    public class RegistrationOfSpecializationAndElectivesAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RegistrationOfSpecializationAndElectivesArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RegistrationOfSpecializationAndElectivesArea_default",
                "RegistrationOfSpecializationAndElectivesArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
