﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.MarkRecordArea;
using SMAS.Business.BusinessObject;
using System.Threading;
using System.Globalization;
using System.IO;
using SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea.Models;

namespace SMAS.Web.Areas.RegistrationOfSpecializationAndElectivesArea.Controllers
{
    public class RegistrationOfSpecializationAndElectivesController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IHeadTeacherSubstitutionBusiness HeadTeacherSubstitutionBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;

        public RegistrationOfSpecializationAndElectivesController(IClassProfileBusiness ClassProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IExemptedSubjectBusiness ExemptedSubjectBusiness,
            IHeadTeacherSubstitutionBusiness HeadTeacherSubstitutionBusiness,
            IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.RegisterSubjectSpecializeBusiness = RegisterSubjectSpecializeBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.HeadTeacherSubstitutionBusiness = HeadTeacherSubstitutionBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public void SetViewData()
        {
            List<EducationLevel> lsEducationLevel = _globalInfo.EducationLevels;

            if (lsEducationLevel != null)
            {
                ViewData[RegistrationOfSpecializationAndElectivesConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[RegistrationOfSpecializationAndElectivesConstants.LIST_EDUCATIONLEVEL] = new SelectList(new string[] { });
            }
        }

        private List<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;

            if (!_globalInfo.IsRolePrincipal && !_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager && !_globalInfo.IsViewAll)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            if (_globalInfo.IsViewAll)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
            }
            List<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.EducationLevelID == EducationLevelID).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            return lsCP;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                List<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClassRegis(int? EducationLevelID)
        {
            int subjectID = SMAS.Business.Common.Utils.GetInt(Request["subjectID"]);
            int subjectTypeID = SMAS.Business.Common.Utils.GetInt(Request["subjectTypeID"]);
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            if (EducationLevelID.HasValue)
            {
                //Load class of subject 
                List<EducationLevelBO> lsEduClassOfSubject = (from cs in ClassSubjectBusiness.All
                                                              join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                              where cs.AppliedType == subjectTypeID && cs.SubjectID == subjectID
                                                              && cp.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                              && cp.SchoolID == _globalInfo.SchoolID.Value
                                                              && cp.EducationLevelID == EducationLevelID
                                                              && cs.Last2digitNumberSchool == partitionId
                                                              && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                              select new EducationLevelBO
                                                              {
                                                                  EducationLevelID = cp.EducationLevelID,
                                                                  ClassProfileID = cs.ClassID,
                                                                  DisplayName = cp.DisplayName,
                                                                  IsSpecializedSubject = cs.IsSpecializedSubject
                                                              }).Distinct().OrderBy(o => o.ClassProfileID).ToList();

                if (subjectTypeID == 0)
                {
                    lsEduClassOfSubject = lsEduClassOfSubject.Where(o => o.IsSpecializedSubject == true).ToList();
                }

                if (lsEduClassOfSubject.Count() != 0)
                {
                    return Json(new SelectList(lsEduClassOfSubject, "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [HttpPost]
        public PartialViewResult AjaxLoadSearchRegis(int subjectID, int classID, int educationlevelID, int subjectTypeID)
        {
            //list education level
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<EducationLevelBO> lsEducationLevel = (from cs in ClassSubjectBusiness.All
                                                       join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                       join el in EducationLevelBusiness.All on cp.EducationLevelID equals el.EducationLevelID
                                                       where cs.AppliedType == subjectTypeID && cs.SubjectID == subjectID
                                                       && cp.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                       && cp.SchoolID == _globalInfo.SchoolID.Value
                                                       && cs.Last2digitNumberSchool == partitionId
                                                       && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                       select new EducationLevelBO
                                                       {
                                                           EducationLevelID = el.EducationLevelID,
                                                           Resolution = el.Resolution,
                                                           IsSpecializedSubject = cs.IsSpecializedSubject
                                                       }).Distinct().OrderBy(o => o.EducationLevelID).ToList();

            //Load class of subject 
            List<EducationLevelBO> lsEduClassOfSubject = (from cs in ClassSubjectBusiness.All
                                                          join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                          where cs.AppliedType == subjectTypeID && cs.SubjectID == subjectID
                                                          && cp.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                          && cp.SchoolID == _globalInfo.SchoolID.Value
                                                          && cp.EducationLevelID == educationlevelID
                                                          && cs.Last2digitNumberSchool == partitionId
                                                          && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                          select new EducationLevelBO
                                                          {
                                                              EducationLevelID = cp.EducationLevelID,
                                                              ClassProfileID = cs.ClassID,
                                                              DisplayName = cp.DisplayName,
                                                              IsSpecializedSubject = cs.IsSpecializedSubject
                                                          }).Distinct().OrderBy(o => o.ClassProfileID).ToList();

            if (subjectTypeID == 0)
            {
                lsEducationLevel = lsEducationLevel.Where(o => o.IsSpecializedSubject == true).ToList();
                lsEduClassOfSubject = lsEduClassOfSubject.Where(o => o.IsSpecializedSubject == true).ToList();
            }

            if (lsEducationLevel != null)
            {
                ViewData[RegistrationOfSpecializationAndElectivesConstants.LIST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution", educationlevelID);
            }
            else
            {
                ViewData[RegistrationOfSpecializationAndElectivesConstants.LIST_EDUCATIONLEVEL] = new SelectList(new string[] { });
            }

            ViewData[RegistrationOfSpecializationAndElectivesConstants.LIST_CLASS] = new SelectList(lsEduClassOfSubject, "ClassProfileID", "DisplayName", classID);

            //load status
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Đã đăng ký", Value = "1", Selected = false });
            items.Add(new SelectListItem() { Text = "Chưa đăng ký", Value = "2", Selected = true });
            SelectList lstStatus = new SelectList(items, "Text", "Value");
            ViewData[RegistrationOfSpecializationAndElectivesConstants.LIST_STATUS] = new SelectList(lstStatus, "Text", "Value", "");
            ViewData[RegistrationOfSpecializationAndElectivesConstants.SubjectID] = subjectID;
            ViewData[RegistrationOfSpecializationAndElectivesConstants.SubjectTypeID] = subjectTypeID;

            return PartialView("_SearchRegis");
        }

        
        public PartialViewResult Search()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            int? educationLevelID = SMAS.Business.Common.Utils.GetInt(Request["EducationLevel"]); ;
            int? classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]); ;

            //danh sach cac mon cua mot lop hoac tat ca
            List<RegistrationSpecializationViewModel> lstRegistrationSpec = new List<RegistrationSpecializationViewModel>();
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            lstRegistrationSpec = (from cs in ClassSubjectBusiness.All
                                   join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                   join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                   where s.IsActive == true && s.IsApprenticeshipSubject == false
                                   && cp.SchoolID == _globalInfo.SchoolID.Value && s.AppliedLevel.Value == _globalInfo.AppliedLevel.Value
                                   && cp.AcademicYearID == _globalInfo.AcademicYearID.Value
                                   && cs.Last2digitNumberSchool == partitionId
                                   && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                   select new RegistrationSpecializationViewModel
                                   {
                                       SubjectClassID = cs.ClassSubjectID,
                                       SubjectCatID = s.SubjectCatID,
                                       ClassID = cs.ClassID,
                                       EducationLevelID = cp.EducationLevelID,
                                       ClassName = cp.DisplayName,
                                       SubjectName = s.SubjectName,
                                       DisplayName = s.DisplayName,
                                       Abbreviation = s.Abbreviation,
                                       IsSpecializedSubject = cs.IsSpecializedSubject,
                                       AppliedType = cs.AppliedType,
                                       OrderNumber = cp.OrderNumber.Value,
                                       OrderInSubject = s.OrderInSubject.Value
                                   }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInSubject).ToList();

            //danh sach cac mon bat buoc 
            List<RegistrationSpecializationViewModel> lstSubjcetCompulsory = lstRegistrationSpec.Where(o => o.AppliedType == 0
                && (o.IsSpecializedSubject == false || o.IsSpecializedSubject == null) || o.AppliedType == RegistrationOfSpecializationAndElectivesConstants.GENERAL_JOBS).ToList();
            for (int i = 0; i < lstSubjcetCompulsory.Count; i++)
            {
                lstRegistrationSpec = lstRegistrationSpec.Where(o => o.SubjectClassID != lstSubjcetCompulsory[i].SubjectClassID).ToList();
            }

            if (educationLevelID != 0)
            {
                lstRegistrationSpec = lstRegistrationSpec.Where(o => o.EducationLevelID == educationLevelID.Value).ToList();
            }

            if (classID != 0)
            {
                lstRegistrationSpec = lstRegistrationSpec.Where(o => o.ClassID == classID.Value).ToList();
            }

            #region list danh sach su dung cho dem so hoc sinh dang ky
            //lay danh sach hoc sinh
            IDictionary<string, object> dicPupil = new Dictionary<string, object>()
            {
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"SchoolID", _globalInfo.SchoolID},
                {"ApplyLevel", _globalInfo.AppliedLevel.Value},   
                {"ClassID", classID},
                {"EducationLevelID", educationLevelID}              
            };

            List<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicPupil).ToList();

            // danh sach hoc sinh cua mot lop co dang ky mon hoc la mon tu chon/mon chuyen 
            List<RegisterSubjectSpecializeBO> lstRegisSubjectSpec = (from re in RegisterSubjectSpecializeBusiness.All
                                                                     join pp in PupilProfileBusiness.All on re.PupilID equals pp.PupilProfileID
                                                                     where re.SchoolID == _globalInfo.SchoolID && pp.IsActive == true
                                                                     && re.AcademicYearID == _globalInfo.AcademicYearID
                                                                     select new RegisterSubjectSpecializeBO
                                                                     {
                                                                         SubjectID = re.SubjectID,
                                                                         ClassID = re.ClassID,
                                                                         PupilID = re.PupilID,
                                                                         SubjectTypeID = re.SubjectTypeID
                                                                     }).ToList();

            //danh sach hoc sinh khac dang hoc
            List<PupilOfClass> listPupilNotStudying = listPupil.Where(o => o.Status != 1).ToList();
            for (int i = 0; i < listPupilNotStudying.Count; i++)
            {
                lstRegisSubjectSpec = lstRegisSubjectSpec.Where(o => o.PupilID != listPupilNotStudying[i].PupilID
                    || o.ClassID != listPupilNotStudying[i].ClassID).ToList();
            }

            #endregion list danh sach su dung cho dem so hoc sinh dang ky

            #region Dem so hoc sinh dang ky
            int countClass = 0;
            int countPupil = 0;
            RegistrationSpecializationViewModel objregisSpec = null;

            for (int i = 0; i < lstRegistrationSpec.Count; i++)
            {
                objregisSpec = lstRegistrationSpec[i];

                countClass = listPupil.Where(o => o.ClassID == objregisSpec.ClassID && o.Status == 1).Count();
                if (objregisSpec.IsSpecializedSubject == true && objregisSpec.AppliedType == 0)
                {
                    countPupil = lstRegisSubjectSpec.Where(o => o.SubjectID == objregisSpec.SubjectCatID && o.SubjectTypeID == objregisSpec.AppliedType
                        && o.ClassID == objregisSpec.ClassID).Select(o => o.PupilID).Distinct().Count();
                }
                if (objregisSpec.AppliedType == 1 || objregisSpec.AppliedType == 2 || objregisSpec.AppliedType == 3)
                {
                    countPupil = lstRegisSubjectSpec.Where(o => o.SubjectID == objregisSpec.SubjectCatID && o.SubjectTypeID == objregisSpec.AppliedType
                        && o.ClassID == objregisSpec.ClassID).Select(o => o.PupilID).Distinct().Count();
                }

                objregisSpec.NumberRegistration = countPupil + "/" + countClass;
                countPupil = 0;
                countClass = 0;
            }
            #endregion Dem so hoc sinh dang ky

            ViewData[RegistrationOfSpecializationAndElectivesConstants.LIST_RESULT] = lstRegistrationSpec;
            return PartialView("_List");
        }

        public PartialViewResult AjaxLoadGridRegistrationSubject()
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int subjectID = SMAS.Business.Common.Utils.GetInt(Request["subjectID"]);
            int educationlevelID = SMAS.Business.Common.Utils.GetInt(Request["educationLevelID"]);
            int classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]);
            int subjectTypeID = SMAS.Business.Common.Utils.GetInt(Request["subjectTypeID"]);
            int? stt = SMAS.Business.Common.Utils.GetInt(Request["status"]);
            int statusLoad = stt == null ? 0 : stt.Value;
            int partitionId = UtilsBusiness.GetPartionId(academicYear.SchoolID);
            // Phan quyen GVCN, GVBM
            bool isGVCN = false;
            if (classID != 0)
            {
                isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            }
            ViewData[RegistrationOfSpecializationAndElectivesConstants.CHECK_AUTHORIZATION] = isGVCN;

            List<RegisterPupilSubjectViewModel> lstRegisterSubject = new List<RegisterPupilSubjectViewModel>();

            //danh sach hoc sinh cua mot lop hoc mon 
            List<RegisterPupilSubjectViewModel> lstPupilOfClass = (from cs in ClassSubjectBusiness.All
                                                                   join poc in PupilOfClassBusiness.All on cs.ClassID equals poc.ClassID
                                                                   join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                                   where poc.SchoolID == _globalInfo.SchoolID && poc.AcademicYearID == _globalInfo.AcademicYearID
                                                                   && poc.ClassID == classID && cs.SubjectID == subjectID
                                                                   && cs.Last2digitNumberSchool==partitionId
                                                                   select new RegisterPupilSubjectViewModel
                                                                   {
                                                                       SubjectID = cs.SubjectID,
                                                                       ClassID = poc.ClassID,
                                                                       PupilID = pp.PupilProfileID,
                                                                       PupilCode = pp.PupilCode,
                                                                       Name = pp.Name,
                                                                       FullName = pp.FullName,
                                                                       Birthday = pp.BirthDate,
                                                                       Genre = pp.Genre,
                                                                       SubjectTypeID = -1,
                                                                       Status = poc.Status,
                                                                       IsActive = pp.IsActive,
                                                                       SectionPerWeekFirstSemester = cs.SectionPerWeekFirstSemester,
                                                                       SectionPerWeekSecondSemester = cs.SectionPerWeekSecondSemester,
                                                                       Semester = 0,
                                                                       SemesterI = 0,
                                                                       SemesterII = 0,
                                                                       OrderInClass = poc.OrderInClass
                                                                   }).ToList();

            // danh sach hoc sinh cua mot lop co dang ky mon hoc la mon tu chon/mon chuyen 
            List<RegisterSubjectSpecializeBO> lstRegisSubjectSpec = (from re in RegisterSubjectSpecializeBusiness.All
                                                                     where re.ClassID == classID && re.SchoolID == _globalInfo.SchoolID
                                                                     && re.AcademicYearID == _globalInfo.AcademicYearID && re.SubjectID == subjectID
                                                                     && re.SubjectTypeID == subjectTypeID
                                                                     select new RegisterSubjectSpecializeBO
                                                                     {
                                                                         PupilID = re.PupilID,
                                                                         SubjectTypeID = re.SubjectTypeID,
                                                                         SemesterID = re.SemesterID
                                                                     }).ToList();

            //lay danh sach hoc sinh co mon hoc duoc mien giam
            List<ExemptedSubject> lstExemptSubject = ExemptedSubjectBusiness.All
                                                        .Where(o => o.ClassID == classID && o.SchoolID == _globalInfo.SchoolID
                                                            && o.AcademicYearID == _globalInfo.AcademicYearID && o.SubjectID == subjectID).ToList();
            for (int i = 0; i < lstPupilOfClass.Count; i++)
            {
                RegisterPupilSubjectViewModel objRegis = lstPupilOfClass[i];
                List<ExemptedSubject> lst = lstExemptSubject.Where(p => p.PupilID == objRegis.PupilID && p.SubjectID == objRegis.SubjectID).ToList();
                if (lst != null && lst.Count > 0)
                {
                    for (int j = 0; j < lst.Count; j++)
                    {
                        if (lst[j].FirstSemesterExemptType != null)
                        {
                            objRegis.FirstSemesterExempt = lst[j].FirstSemesterExemptType;
                        }
                        if (lst[j].SecondSemesterExemptType != null)
                        {
                            objRegis.SecondSemesterExempt = lst[j].SecondSemesterExemptType;
                        }
                    }
                }
            }

            int count = 0;
            int countExempt = 0;
            RegisterPupilSubjectViewModel obj = null;
            if (statusLoad == 0)
            {
                #region all
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    obj = lstPupilOfClass[i];
                    List<RegisterSubjectSpecializeBO> lst = lstRegisSubjectSpec.Where(p => p.PupilID == obj.PupilID && obj.SubjectID == subjectID).ToList();
                    if (lst != null && lst.Count > 0)
                    {
                        for (int j = 0; j < lst.Count; j++)
                        {
                            if (lst[j].SubjectTypeID == 0)
                            {
                                obj.SubjectTypeID = lst[j].SubjectTypeID;
                            }
                            else
                            {
                                obj.SubjectTypeID = lst[j].SubjectTypeID;
                                if (lst[j].SemesterID == 1)
                                {
                                    obj.SemesterI = 1;
                                }
                                else
                                {
                                    obj.SemesterII = 2;
                                }
                            }
                        }
                    }
                    lstRegisterSubject.Add(obj);
                }
                #endregion all
            }
            if (statusLoad == 1)
            {
                #region da dang ky
                List<RegisterPupilSubjectViewModel> lstPupilOfClassAction = new List<RegisterPupilSubjectViewModel>();
                //lay danh hoc sinh co ton tai trong table RegisterSubjectSpecialize cua PupilOfClass
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    obj = lstPupilOfClass[i];
                    count = lstRegisSubjectSpec.Where(p => p.PupilID == obj.PupilID).Count();
                    if (count > 0)
                    {
                        lstPupilOfClassAction.Add(obj);
                    }
                }

                //duyet danh sach dang ky
                for (int i = 0; i < lstPupilOfClassAction.Count; i++)
                {
                    obj = lstPupilOfClassAction[i];
                    if (obj.Status == 1)
                    {
                        List<RegisterSubjectSpecializeBO> lst = lstRegisSubjectSpec.Where(p => p.PupilID == obj.PupilID && obj.SubjectID == subjectID).ToList();
                        if (lst != null && lst.Count > 0)
                        {
                            for (int j = 0; j < lst.Count; j++)
                            {
                                if (lst[j].SubjectTypeID == 0)
                                {
                                    obj.SubjectTypeID = lst[j].SubjectTypeID;
                                }
                                else
                                {
                                    obj.SubjectTypeID = lst[j].SubjectTypeID;
                                    if (lst[j].SemesterID == 1)
                                    {
                                        obj.SemesterI = 1;
                                    }
                                    else
                                    {
                                        obj.SemesterII = 2;
                                    }
                                }
                            }
                        }
                        lstRegisterSubject.Add(obj);
                    }
                }
                #endregion da dang ky
            }
            if (statusLoad == 2)
            {
                #region chua dang ky
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    obj = lstPupilOfClass[i];
                    if (obj.Status == 1)
                    {
                        count = lstRegisSubjectSpec.Where(p => p.PupilID == obj.PupilID).Count();

                        if (obj.SubjectTypeID == 0)
                        {
                            if (count == 0)
                            {
                                countExempt = lstExemptSubject.Where(p => p.PupilID == obj.PupilID && p.SubjectID == obj.SubjectID).Count();
                                if (countExempt == 0)
                                {
                                    lstRegisterSubject.Add(obj);
                                }
                            }
                        }
                        else
                        {
                            if (count == 0)
                            {
                                countExempt = lstExemptSubject.Where(p => p.PupilID == obj.PupilID && p.SubjectID == obj.SubjectID && p.FirstSemesterExemptType != null && p.SecondSemesterExemptType != null).Count();
                                if (countExempt == 0)
                                {
                                    lstRegisterSubject.Add(obj);
                                }
                            }
                        }

                    }
                }
                #endregion chua dang ky
            }

            ViewData[RegistrationOfSpecializationAndElectivesConstants.LIST_REGISTER_SUBJECT] = lstRegisterSubject.Where(o => o.IsActive == true)
                                            .OrderBy(o => o.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName));
            ViewData[RegistrationOfSpecializationAndElectivesConstants.SubjectTypeID] = subjectTypeID;
            ViewData[RegistrationOfSpecializationAndElectivesConstants.SubjectID] = subjectID;
            ViewData[RegistrationOfSpecializationAndElectivesConstants.CLASS_ID] = classID;
            ViewData[RegistrationOfSpecializationAndElectivesConstants.EDUCATION_LEVEL_ID] = educationlevelID;
            ViewData[RegistrationOfSpecializationAndElectivesConstants.ACADEMIC_YEAR] = academicYear;
            ViewData["_Status_"] = statusLoad;

            return PartialView("_ListRegis");
        }

        [ValidateAntiForgeryToken]
        public JsonResult InsertOrUpdateRegisterSubject(FormCollection frm)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            int ClassID = frm["HfClassID"] != null ? int.Parse(frm["HfClassID"]) : 0;
            int EducationLevelID = frm["HfEducationLevelID"] != null ? int.Parse(frm["HfEducationLevelID"]) : 0;
            int SubjectTypeID = frm["HfSubjectTypeID"] != null ? int.Parse(frm["HfSubjectTypeID"]) : 0;
            int SubjectID = frm["HfSubjectID"] != null ? int.Parse(frm["HfSubjectID"]) : 0;
            int StatusLoad = frm["Status"] != null ? int.Parse(frm["Status"]) : 0;
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            if (ClassID <= 0)
            {
                return Json(new JsonMessage(Res.Get("ApprenticeshipTraining_Label_ClassID")), "error");
            }
            string ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;

            //Check quyen nguoi dung
            bool isAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
            if (!(isAdmin || isGVCN))
            {
                return Json(new JsonMessage(Res.Get("RewardCommentFinal_Label_ErrorMesage_Permision") + ClassName), "error");
            }

            //khoi tao list RegisterSubjectSpecialize (ON) 
            List<RegisterSubjectSpecialize> lstRegisterSubjectSpecializeON = new List<RegisterSubjectSpecialize>();

            //khoi tao list RegisterSubjectSpecialize (OFF) 
            List<RegisterSubjectSpecialize> lstRegisterSubjectSpecializeOFF = new List<RegisterSubjectSpecialize>();


            RegisterSubjectSpecialize objRegisterSubjectSpec = null;
            PupilOfClass objPupilOfClass = null;

            //lay danh sach hoc sinh
            IDictionary<string, object> dicPupil = new Dictionary<string, object>()
            {
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"SchoolID", schoolId},
                {"ClassID", ClassID},
                {"EducationLevelID", EducationLevelID}
            };

            List<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(schoolId, dicPupil).ToList();


            for (int i = 0; i < listPupil.Count; i++)
            {
                objPupilOfClass = listPupil[i];
                if (objPupilOfClass.Status == 1)
                {
                    if (SubjectTypeID == 0)
                    {
                        objRegisterSubjectSpec = new RegisterSubjectSpecialize();
                        if (!"on".Equals(frm["chk_" + objPupilOfClass.PupilID]))
                        {
                            objRegisterSubjectSpec.SchoolID = schoolId;
                            objRegisterSubjectSpec.AcademicYearID = academicYearId;
                            objRegisterSubjectSpec.PupilID = objPupilOfClass.PupilID;
                            objRegisterSubjectSpec.ClassID = ClassID;
                            objRegisterSubjectSpec.SubjectID = SubjectID;
                            objRegisterSubjectSpec.SubjectTypeID = SubjectTypeID;
                            objRegisterSubjectSpec.SemesterID = 0;
                            objRegisterSubjectSpec.CreateTime = DateTime.Now;
                            objRegisterSubjectSpec.LastDigitYearID = objAcademicYear.Year;
                            lstRegisterSubjectSpecializeOFF.Add(objRegisterSubjectSpec);
                        }
                        else
                        {
                            //objRegisterSubjectSpec.RegisterSubjectSpecializeID = RegisterSubjectSpecializeBusiness.GetNextSeq<long>("REGISTER_SS_SEQ");
                            objRegisterSubjectSpec.SchoolID = _globalInfo.SchoolID.Value;
                            objRegisterSubjectSpec.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objRegisterSubjectSpec.PupilID = objPupilOfClass.PupilID;
                            objRegisterSubjectSpec.ClassID = ClassID;
                            objRegisterSubjectSpec.SubjectID = SubjectID;
                            objRegisterSubjectSpec.SubjectTypeID = SubjectTypeID;
                            objRegisterSubjectSpec.SemesterID = 0;
                            objRegisterSubjectSpec.CreateTime = DateTime.Now;
                            objRegisterSubjectSpec.LastDigitYearID = objAcademicYear.Year;
                            lstRegisterSubjectSpecializeON.Add(objRegisterSubjectSpec);
                        }

                    }
                    else
                    {
                        if ("on".Equals(frm["chkI_" + objPupilOfClass.PupilID]))
                        {
                            objRegisterSubjectSpec = new RegisterSubjectSpecialize();
                            //objRegisterSubjectSpec.RegisterSubjectSpecializeID = RegisterSubjectSpecializeBusiness.GetNextSeq<long>("REGISTER_SS_SEQ");
                            objRegisterSubjectSpec.SchoolID = _globalInfo.SchoolID.Value;
                            objRegisterSubjectSpec.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objRegisterSubjectSpec.PupilID = objPupilOfClass.PupilID;
                            objRegisterSubjectSpec.ClassID = ClassID;
                            objRegisterSubjectSpec.SubjectID = SubjectID;
                            objRegisterSubjectSpec.SubjectTypeID = SubjectTypeID;
                            objRegisterSubjectSpec.SemesterID = 1;
                            objRegisterSubjectSpec.CreateTime = DateTime.Now;
                            objRegisterSubjectSpec.LastDigitYearID = objAcademicYear.Year;
                            lstRegisterSubjectSpecializeON.Add(objRegisterSubjectSpec);
                        }
                        else
                        {
                            objRegisterSubjectSpec = new RegisterSubjectSpecialize();
                            objRegisterSubjectSpec.SchoolID = schoolId;
                            objRegisterSubjectSpec.AcademicYearID = academicYearId;
                            objRegisterSubjectSpec.PupilID = objPupilOfClass.PupilID;
                            objRegisterSubjectSpec.ClassID = ClassID;
                            objRegisterSubjectSpec.SubjectID = SubjectID;
                            objRegisterSubjectSpec.SubjectTypeID = SubjectTypeID;
                            objRegisterSubjectSpec.SemesterID = 1;
                            objRegisterSubjectSpec.CreateTime = DateTime.Now;
                            objRegisterSubjectSpec.LastDigitYearID = objAcademicYear.Year;
                            lstRegisterSubjectSpecializeOFF.Add(objRegisterSubjectSpec);
                        }

                        if ("on".Equals(frm["chkII_" + objPupilOfClass.PupilID]))
                        {
                            objRegisterSubjectSpec = new RegisterSubjectSpecialize();
                            //objRegisterSubjectSpec.RegisterSubjectSpecializeID = RegisterSubjectSpecializeBusiness.GetNextSeq<long>("REGISTER_SS_SEQ");
                            objRegisterSubjectSpec.SchoolID = _globalInfo.SchoolID.Value;
                            objRegisterSubjectSpec.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objRegisterSubjectSpec.PupilID = objPupilOfClass.PupilID;
                            objRegisterSubjectSpec.ClassID = ClassID;
                            objRegisterSubjectSpec.SubjectID = SubjectID;
                            objRegisterSubjectSpec.SubjectTypeID = SubjectTypeID;
                            objRegisterSubjectSpec.SemesterID = 2;
                            objRegisterSubjectSpec.CreateTime = DateTime.Now;
                            objRegisterSubjectSpec.LastDigitYearID = objAcademicYear.Year;
                            lstRegisterSubjectSpecializeON.Add(objRegisterSubjectSpec);
                        }
                        else
                        {
                            objRegisterSubjectSpec = new RegisterSubjectSpecialize();
                            objRegisterSubjectSpec.SchoolID = schoolId;
                            objRegisterSubjectSpec.AcademicYearID = academicYearId;
                            objRegisterSubjectSpec.PupilID = objPupilOfClass.PupilID;
                            objRegisterSubjectSpec.ClassID = ClassID;
                            objRegisterSubjectSpec.SubjectID = SubjectID;
                            objRegisterSubjectSpec.SubjectTypeID = SubjectTypeID;
                            objRegisterSubjectSpec.SemesterID = 2;
                            objRegisterSubjectSpec.CreateTime = DateTime.Now;
                            objRegisterSubjectSpec.LastDigitYearID = objAcademicYear.Year;
                            lstRegisterSubjectSpecializeOFF.Add(objRegisterSubjectSpec);
                        }
                    }
                }
            }

            if (StatusLoad == 2)
            {
                List<RegisterSubjectSpecialize> lstRegisSubjectDB = (from regis in RegisterSubjectSpecializeBusiness.All
                                                                     where regis.SchoolID == schoolId
                                                                     && regis.AcademicYearID == academicYearId
                                                                     && regis.ClassID == ClassID
                                                                     && regis.SubjectID == SubjectID
                                                                     && regis.SubjectTypeID == SubjectTypeID
                                                                     select regis).ToList();
                for (int i = 0; i < lstRegisSubjectDB.Count; i++)
                {
                    lstRegisterSubjectSpecializeOFF = lstRegisterSubjectSpecializeOFF.Where(p => p.PupilID != lstRegisSubjectDB[i].PupilID).ToList();
                }
            }


            if ((lstRegisterSubjectSpecializeON != null && lstRegisterSubjectSpecializeON.Count > 0) ||
                (lstRegisterSubjectSpecializeOFF != null && lstRegisterSubjectSpecializeOFF.Count > 0))
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID}                        
                };
                RegisterSubjectSpecializeBusiness.InsertOrUpdate(dic, lstRegisterSubjectSpecializeON, lstRegisterSubjectSpecializeOFF);
            }
            else
            {
                return Json(new JsonMessage(Res.Get("RewardCommentFinal_Label_ValidateEmpty"), "error"));
            }

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }

    }
}
