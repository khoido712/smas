﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.ChildrenMoneyPerMonthArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.ChildrenMoneyPerMonthArea.Controllers
{
    public class ChildrenMoneyPerMonthController : BaseController
    {
        private readonly INotEatingChildrenBusiness NotEatingChildrenBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;


        public ChildrenMoneyPerMonthController(IEducationLevelBusiness EducationLevelBusiness, ISchoolProfileBusiness SchoolProfileBusiness, IProcessedReportBusiness ProcessedReportBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, INotEatingChildrenBusiness noteatingchildrenBusiness,
            IAcademicYearBusiness academicYearBusiness, IClassProfileBusiness classProfileBusiness)
        {
            this.NotEatingChildrenBusiness = noteatingchildrenBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
        }

        //
        // GET: /ChildrenMoneyPerMonth/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();

            //Get view data here
            // Lay du lieu khoi hoc
            List<EducationLevel> lsEducationLevel = globalInfo.EducationLevels;
            if (lsEducationLevel == null)
            {
                lsEducationLevel = new List<EducationLevel>();
            }
            SelectList SelectListEducationLevel =
                new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            ViewData[ChildrenMoneyPerMonthConstants.LIST_EDUCATIONLEVEL] = SelectListEducationLevel;
            
            // Lay danh sach lop hoc dau tien theo khoi
            if (lsEducationLevel != null && lsEducationLevel.Count > 0)
            {
                int EducationLevelID = lsEducationLevel[0].EducationLevelID;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = EducationLevelID;
                dic["AcademicYearID "] = _globalInfo.AcademicYearID;
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["IsActive"] = true;
                List<ClassProfile> listClass = this.ClassProfileBusiness.Search(dic).OrderBy(p => p.DisplayName).ToList();
                listClass.Insert(0, new ClassProfile() {
                    DisplayName = Res.Get("CHOICE")
                });
                ViewData[ChildrenMoneyPerMonthConstants.LIST_CLASS] = new SelectList(listClass, "ClassProfileID", "DisplayName");
            }
            else
            {
                ViewData[ChildrenMoneyPerMonthConstants.LIST_CLASS] = new SelectList(new string[] { });
            }

            // Lay thoi gian nam hoc
            int? AcademicYearID = globalInfo.AcademicYearID;
            List<ComboObject> listYear = new List<ComboObject>();
            List<ComboObject> listMonth = new List<ComboObject>();

            // Phai co thong tin nam hoc trong session
            if (AcademicYearID.HasValue)
            {
                AcademicYear AcademicYear = AcademicYearBusiness.Find(AcademicYearID.Value);

                // Thong tin nam hoc phai co trong CSDL
                if (AcademicYear != null)
                {
                    DateTime? FromDate = AcademicYear.FirstSemesterStartDate;
                    DateTime? ToDate = AcademicYear.SecondSemesterEndDate;

                    // Thoi gian bat dau va ket thuc nam hoc phai co gia tri
                    if (FromDate.HasValue && ToDate.HasValue)
                    {
                        int FromMonth = FromDate.Value.Month;
                        int FromYear = FromDate.Value.Year;
                        int ToMonth = ToDate.Value.Month;
                        int ToYear = ToDate.Value.Year;

                        // Ngay ket thuc nam hoc phai lon hon ngay bat dau nam hoc
                        if (ToYear >= FromYear)
                        {
                            // Lay danh sach thang
                            int countMonth = 0;
                            if (ToYear > FromYear)
                            {
                                countMonth = 12;
                            }
                            countMonth = countMonth + ToMonth;
                            for (int i = FromMonth; i <= countMonth; i++)
                            {
                                int month = i;
                                if (i > 12)
                                {
                                    month = i - 12;
                                }
                                ComboObject comboObj = new ComboObject(month.ToString(), month.ToString());
                                listMonth.Add(comboObj);
                            }

                            // Lay danh sach nam
                            for (int i = FromYear; i <= ToYear; i++)
                            {
                                ComboObject comboObj = new ComboObject(i.ToString(), i.ToString());
                                listYear.Add(comboObj);
                            }
                        }
                        //ViewData[ChildrenMoneyPerMonthConstants.Year] = FromYear.ToString();
                        //ViewData[ChildrenMoneyPerMonthConstants.Month] = FromMonth.ToString();
                    }
                }
            }

            SelectList SelectListYear =
                new SelectList(listYear, "key", "value");
            ViewData[ChildrenMoneyPerMonthConstants.LIST_YEAR] = SelectListYear;

            SelectList SelectListMonth =
                new SelectList(listMonth, "key", "value");
            ViewData[ChildrenMoneyPerMonthConstants.LIST_MONTH] = SelectListMonth;

            //ViewData[ChildrenMoneyPerMonthConstants.ClassID] = "0";
            //ViewData[ChildrenMoneyPerMonthConstants.EducationLevelID] = lsEducationLevel[0].EducationLevelID.ToString();
            return View();
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingClassByEducation(int? EducationLevelId)
        {
            List<ClassProfile> lst = new List<ClassProfile>().ToList();
            
            if (EducationLevelId.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = EducationLevelId.Value;
                dic["AcademicYearID "] = _globalInfo.AcademicYearID;
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["IsActive"] = true;
                lst = this.ClassProfileBusiness.Search(dic).OrderBy(p => p.DisplayName).ToList();
            }
            lst.Insert(0, new ClassProfile()
            {
                DisplayName = Res.Get("CHOICE")
            });
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="frm">The FRM.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/12/2012</date>
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["Year"] = frm.Year;
            SearchInfo["Month"] = frm.Month;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["ClassID"] = frm.ClassID;

            Session["ClassID"] = frm.ClassID;
            Session["Year"] = frm.Year;
            Session["Month"] = frm.Month;
            Session["EducationLevel"] = frm.EducationLevel;

            IEnumerable<ChildrenMoneyPerMonthViewModel> lst = this._Search(SearchInfo);
            Session["lst"] = lst;
            ViewData[ChildrenMoneyPerMonthConstants.LIST_CHILDRENMONEYPERMONTH] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ChildrenMoneyPerMonthViewModel> _Search(IDictionary<string, object> searchInfo)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            string pupilCode = SMAS.Business.Common.Utils.GetString(searchInfo, "PupilCode");
            string fullName = SMAS.Business.Common.Utils.GetString(searchInfo, "FullName");
            int year = SMAS.Business.Common.Utils.GetInt(searchInfo, "Year");
            int month = SMAS.Business.Common.Utils.GetInt(searchInfo, "Month");
            int educationLevelID = SMAS.Business.Common.Utils.GetInt(searchInfo, "EducationLevelID");
            int classID = SMAS.Business.Common.Utils.GetInt(searchInfo, "ClassID");

            List<MoneyPerChildren> query = this.NotEatingChildrenBusiness.GetListChildren(globalInfo.AcademicYearID.Value, globalInfo.SchoolID.Value,
                educationLevelID, classID, pupilCode, fullName, month, year);
            List<ChildrenMoneyPerMonthViewModel> lst = query.Select(o => new ChildrenMoneyPerMonthViewModel
            {
                PupilID = o.PupilID,
                PupilCode = o.PupilCode,
                FullName = o.FullName,
                BirthDate = o.BirthDate,
                Genre = o.Genre,
                GenreName = CommonConvert.Genre((int)o.Genre),
                MonthAndYear = o.Date,
                NumberEating = o.NumberDayOfMonth,
                Cost = o.DailyDishCost,
                SumMoneyPerMonth = o.MoneyPerMonth,
                ClassID = o.CurrentClassID,
                ClassName = o.CurrentClassName
            }).ToList();

            return lst;
        }

        #region Export Excel

        public FileResult DownloadReport(FormCollection form)
        {
            int ClassID = (int)Session["ClassID"] ;
            int Year = (int) Session["Year"] ;
            int Month = (int)Session["Month"];
            int EducationLevelID = (int)Session["EducationLevel"] ;
            List<ChildrenMoneyPerMonthViewModel> lst = (List<ChildrenMoneyPerMonthViewModel>)Session["lst"];

            string reportCode = SystemParamsInFile.REPORT_TIEN_AN_HANG_THANG;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet phiếu điểm
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value);

            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = school.Province.ProvinceName;
            string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            string tittle = "Bảng tính tiền ăn tháng " + Month + "/" + Year;
            string className = "LỚP: " + ClassProfileBusiness.Find(ClassID).DisplayName.ToUpper();
            string educationName = EducationLevelBusiness.Find(EducationLevelID).Resolution;
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("E3", provinceAndDate);
            firstSheet.SetCellValue("A7", tittle);
            firstSheet.SetCellValue("E8", className);

            int startRow = 13;
            IVTRange range = firstSheet.GetRange("A13", "H13");

            decimal sumOfMoney = 0;
            for (int i = 0; i < lst.Count; i++)
            {
                //Copy row style
                if (startRow + i > 13)
                {
                    firstSheet.CopyPasteSameRowHeigh(range, startRow);
                }

                //STT
                firstSheet.SetCellValue(startRow, 1, i + 1);

                //Ma tre
                firstSheet.SetCellValue(startRow, 2, lst[i].PupilCode);

                //Ho ten
                firstSheet.SetCellValue(startRow, 3, lst[i].FullName);

                //Ngay sinh
                firstSheet.SetCellValue(startRow, 4, lst[i].BirthDate);

                //Gioi tinh
                firstSheet.SetCellValue(startRow, 5, lst[i].GenreName);

                //So buoi an
                firstSheet.SetCellValue(startRow, 6, lst[i].NumberEating);

                //Tien an/ngay
                firstSheet.SetCellValue(startRow, 7, lst[i].Cost);

                //Tong tien an
                firstSheet.SetCellValue(startRow, 8, lst[i].SumMoneyPerMonth);
                sumOfMoney += lst[i].SumMoneyPerMonth;
                startRow++;
            }
            firstSheet.CopyPasteSameRowHeigh(range, startRow);
            firstSheet.MergeRow(startRow, 1, 7);
            firstSheet.SetCellValue("A" + startRow, "Tổng: ");
            firstSheet.SetCellValue(startRow, 8, sumOfMoney);

            Stream excel = oBook.ToStream();

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = SystemParamsInFile.REPORT_TIEN_AN_HANG_THANG + "_" + ReportUtils.StripVNSign(educationName) + "_" + ReportUtils.StripVNSign(className) + "_Thang" + Month + "_Nam" + Year + ".xls";

            return result;
        }

        #endregion Export Excel
    }
}
