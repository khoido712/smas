/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ChildrenMoneyPerMonthArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ChildrenMoneyPerMonthConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "AjaxClassLoading(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ChildrenMoneyPerMonthConstants.LIST_CLASS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_FullName")]
        public string FullName { get; set; }

        [ScaffoldColumn(false)]
        public int Month { get; set; }
        [ScaffoldColumn(false)]
        public int Year { get; set; }
    }
}