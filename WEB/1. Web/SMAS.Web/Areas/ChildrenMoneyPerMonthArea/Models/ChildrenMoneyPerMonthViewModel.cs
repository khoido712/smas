/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ChildrenMoneyPerMonthArea.Models
{
    public class ChildrenMoneyPerMonthViewModel
    {

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_NotEatingChildrenID")]
        public int NotEatingChildrenID { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_PupilID")]
        public int PupilID { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_BirthDate")]
        public DateTime BirthDate { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_Genre")]
        public int Genre { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_Genre")]
        public string GenreName { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_ClassID")]
        public int ClassID { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_MonthAndYear")]
        public string MonthAndYear { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_NumberEating")]
        public int NumberEating { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_Cost")]
        public decimal Cost { get; set; }

        [ResourceDisplayName("ChildrenMoneyPerMonth_Label_SumMoneyPerMonth")]
        public decimal SumMoneyPerMonth { get; set; }
    }
}


