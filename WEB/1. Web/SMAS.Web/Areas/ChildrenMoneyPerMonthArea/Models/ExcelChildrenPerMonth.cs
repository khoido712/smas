/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.ChildrenMoneyPerMonthArea.Models
{
    public class ExcelChildrenPerMonth
    {
        public int? EducationLevelID { get; set; }

        public int? ClassID { get; set; }

        public string PupilCode { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public int SchoolID { get; set; }
    }
}