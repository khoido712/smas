﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ChildrenMoneyPerMonthArea
{
    public class ChildrenMoneyPerMonthAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ChildrenMoneyPerMonthArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ChildrenMoneyPerMonthArea_default",
                "ChildrenMoneyPerMonthArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
