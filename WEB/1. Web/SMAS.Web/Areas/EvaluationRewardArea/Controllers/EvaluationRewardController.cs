﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.EvaluationRewardArea;
using SMAS.Web.Areas.EvaluationRewardArea.Models;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.EvaluationRewardArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class EvaluationRewardController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IBookmarkedFunctionBusiness BookmarkedFunctionBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IEvaluationRewardBusiness EvaluationRewardBusiness;
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private readonly IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;
        private readonly IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        private readonly IQuickHintBusiness QuickHintBusiness;
        public EvaluationRewardController(IAcademicYearBusiness academicYearBusiness,
         IClassProfileBusiness classProfileBusiness, IClassSubjectBusiness classSubjectBusiness, ISubjectCatBusiness subjectCatBusiness,
         IClassSupervisorAssignmentBusiness classSupervisorAssignmentBusiness, IUserAccountBusiness userAccountBusiness,
         ITeachingAssignmentBusiness teachingAssignmentBusiness, IBookmarkedFunctionBusiness bookmarkedFunctionBusiness,
         IPupilOfClassBusiness pupilOfClassBusiness, ISchoolProfileBusiness schoolProfileBusiness, IEmployeeBusiness employeeBusiness,
         IEvaluationRewardBusiness evaluationRewardBusiness, IRatedCommentPupilBusiness ratedCommentPupilBusiness,
         IRatedCommentPupilHistoryBusiness ratedCommentPupilHistoryBusiness, IEvaluationCriteriaBusiness evaluationCriteriaBusiness,
            IQuickHintBusiness QuickHintBusiness)
        {
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassSupervisorAssignmentBusiness = classSupervisorAssignmentBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.BookmarkedFunctionBusiness = bookmarkedFunctionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.EvaluationRewardBusiness = evaluationRewardBusiness;
            this.RatedCommentPupilBusiness = ratedCommentPupilBusiness;
            this.RatedCommentPupilHistoryBusiness = ratedCommentPupilHistoryBusiness;
            this.EvaluationCriteriaBusiness = evaluationCriteriaBusiness;
            this.QuickHintBusiness = QuickHintBusiness;
        }
        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }
        public ActionResult _index(int? ClassID)
        {
            this.SetViewData(ClassID);
            return PartialView();
        }
        public PartialViewResult AjaxLoadGrid(int ClassID, int SemesterID)
        {
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            //Lay danh sach hoc sinh theo lop
            List<EvaluationRewardViewModel> lstResult = new List<EvaluationRewardViewModel>();
            EvaluationRewardViewModel objResult = null;
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
            if (isNotShowPupil)
            {
                iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            lstPOC = iquery.ToList();
            List<int> lstPupilID = lstPOC.Select(p => p.PupilID).Distinct().ToList();
            //Lay danh sach khen thuong cua hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID},
                {"lstPupilID",lstPupilID}
            };
            List<EvaluationReward> lstER = EvaluationRewardBusiness.Search(dic).OrderBy(p => p.OrderID).ToList();
            List<EvaluationReward> lstERtmp = null;
            PupilOfClassBO objPOC = null;
            List<RewardPupilContent> lstRewardContent = new List<RewardPupilContent>();
            RewardPupilContent objRewardContent = null;
            int? rewardID = 0;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objResult = new EvaluationRewardViewModel();
                objResult.PupilID = objPOC.PupilID;
                objResult.FullName = objPOC.PupilFullName;
                objResult.ClassID = objPOC.ClassID;
                objResult.StatusID = objPOC.Status;
                lstERtmp = lstER.Where(p => p.PupilID == objPOC.PupilID).ToList();
                lstRewardContent = new List<RewardPupilContent>();
                for (int j = 0; j < lstERtmp.Count; j++)
                {
                    rewardID = lstERtmp[j].RewardID;
                    objRewardContent = new RewardPupilContent();
                    objRewardContent.PupilID = objPOC.PupilID;
                    objRewardContent.RewardID = rewardID;
                    objRewardContent.EvaluationRewardID = lstERtmp[j].EvaluationRewardID;
                    if (lstERtmp[j].RewardID == GlobalConstants.EVALUATION_REWARD_FORTUITY)
                    {
                        objRewardContent.RewardName = Res.Get("Evaluation_Reward_Fortuity");
                    }
                    else
                    {
                        objRewardContent.RewardName = Res.Get("Evaluation_Reward_Outstanding_Achievement");
                    }
                    objRewardContent.Content = lstERtmp[j].Content;
                    lstRewardContent.Add(objRewardContent);
                }
                objResult.lstReward = lstRewardContent;
                lstResult.Add(objResult);
            }

            ViewData[EvaluationRewardConstants.LIST_EVALUATION_REWARD] = lstResult;
            bool isEnabled = false;
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
            if (_globalInfo.IsAdmin || _globalInfo.IsAdminSchoolRole || isGVCN)
            {
                isEnabled = true;
            }
            ViewData[EvaluationRewardConstants.IS_ENABLED] = isEnabled;
            List<RewardCommentViewModel> lstCommentAutocomplete = (from q in QuickHintBusiness.All
                                                                   where q.FunctionCode.Equals("EvaluationReward")
                                                                   && !q.IsSystemHint
                                                                   && q.UserAccountID == _globalInfo.UserAccountID
                                                                   select new RewardCommentViewModel
                                                                   {
                                                                       CommentCode = q.HintCode,
                                                                       Comment = q.Content,
                                                                       TypeID = q.TypeID
                                                                   }).OrderBy(p => p.CommentCode).ThenBy(p => p.Comment).ToList();
            List<RewardCommentViewModel> lstCommentAutocomplete1 = (from s in lstCommentAutocomplete
                                                                    select new RewardCommentViewModel
                                                                    {
                                                                        CommentCode = UtilsBusiness.RemoveSpecialCharacter(s.CommentCode),
                                                                        Comment = UtilsBusiness.RemoveSpecialCharacter(s.Comment),
                                                                        TypeID = s.TypeID
                                                                    }).OrderBy(p => p.CommentCode).ThenBy(p => p.Comment).ToList();
            ViewData[EvaluationRewardConstants.LIST_AUTOCOMPLETE] = lstCommentAutocomplete1;

            return PartialView("_GridEvaluationReward");
        }
        [ValidateAntiForgeryToken]
        public JsonResult SaveEvaluationReward(List<RewardPupilContent> lstRewardPupilContent, int ClassID, int Semester)
        {
            //check quyen GVCN
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
            if (!(_globalInfo.IsAdmin || _globalInfo.IsAdminSchoolRole || isGVCN))
            {
                return Json(new JsonMessage(string.Format("Thầy cô không có quyền cập khen thưởng cho lớp {0}", objCP.DisplayName), "error"));
            }
            List<EvaluationReward> lstEvaluationReward = new List<EvaluationReward>();
            EvaluationReward objER = null;
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            PupilOfClassBO objPOC = null;
            lstRewardPupilContent = lstRewardPupilContent.Where(p => p.isChecked.HasValue && p.isChecked.Value && p.RewardID > 0).ToList();
            RewardPupilContent objRPC = null;
            int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<int> lstPupilID = new List<int>();
            List<long> lstEvaluationRewardID = new List<long>();
            int countErr = 0;
            for (int i = 0; i < lstRewardPupilContent.Count; i++)
            {
                objRPC = lstRewardPupilContent[i];
                objPOC = lstPOC.Where(p => p.PupilID == objRPC.PupilID).FirstOrDefault();
                if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                objER = new EvaluationReward();
                objER.SchoolID = _globalInfo.SchoolID.Value;
                objER.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objER.PupilID = objRPC.PupilID;
                objER.ClassID = ClassID;
                objER.SemesterID = Semester;
                objER.RewardID = objRPC.RewardID;
                objER.OrderID = objRPC.OrderID;
                objER.LastDigitSchoolID = partitionID;
                if (string.IsNullOrEmpty(objRPC.Content))
                {
                    countErr = 1;
                    break;
                }
                objER.Content = objRPC.Content.Trim();
                lstPupilID.Add(objRPC.PupilID);
                lstEvaluationRewardID.Add(objRPC.EvaluationRewardID);
                lstEvaluationReward.Add(objER);
            }
            if (countErr > 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_DescriptionRepuired"), "error"));
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",Semester},
                {"lstPupilID",lstPupilID},
                {"lstEvaluationRewardID",lstEvaluationRewardID}
            };
            EvaluationRewardBusiness.InsertEvaluationReward(lstEvaluationReward, dic);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEvaluation(string arrDeleteID, int ClassID, int SemesterID)
        {
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
            if (!(_globalInfo.IsAdmin || _globalInfo.IsAdminSchoolRole || isGVCN))
            {
                return Json(new JsonMessage(string.Format("Thầy cô không có quyền xóa khen thưởng của học sinh lớp {0}", objCP.DisplayName), "error"));
            }
            List<long> lstEvaluationRewardID = arrDeleteID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => long.Parse(p)).Where(p => p > 0).ToList();
            if (lstEvaluationRewardID.Count == 0)
            {
                return Json(new JsonMessage(Res.Get("success_del"), "success"));
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"lstEvaluationRewardID",lstEvaluationRewardID}
            };
            EvaluationRewardBusiness.DeleteEvaluationReward(dic);
            return Json(new JsonMessage(Res.Get("success_del"), "success"));
        }
        public FileResult ExportExcel(int ClassID, int SemesterID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", EvaluationRewardConstants.TEMPLATE_FILE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //Fill thong tin chung
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            //DANH SÁCH KHEN THƯỞNG LỚP 4A HỌC KỲ I NĂM HỌC 2016 - 2017
            string title = "DANH SÁCH KHEN THƯỞNG LỚP " + objCP.DisplayName;
            string SemesterName = SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? " Học kỳ I" : " Học kỳ II";
            title += SemesterName + " năm học " + objAy.DisplayTitle;
            sheet.SetCellValue("A4", title.ToUpper());
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
            if (isNotShowPupil)
            {
                iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            lstPOC = iquery.ToList();
            List<int> lstPupilID = lstPOC.Select(p => p.PupilID).Distinct().ToList();
            //Lay danh sach khen thuong cua hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID},
                {"lstPupilID",lstPupilID}
            };
            List<EvaluationReward> lstER = EvaluationRewardBusiness.Search(dic).OrderBy(p => p.OrderID).ToList();
            List<EvaluationReward> lstERtmp = null;
            EvaluationReward objER = null;
            PupilOfClassBO objPOC = null;
            int startRow = 7;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                lstERtmp = lstER.Where(p => p.PupilID == objPOC.PupilID).ToList();
                //STT
                sheet.GetRange(startRow, 1, startRow + (lstERtmp.Count > 0 ? lstERtmp.Count - 1 : 0), 1).Merge();
                sheet.GetRange(startRow, 1, startRow + (lstERtmp.Count > 0 ? lstERtmp.Count - 1 : 0), 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(startRow, 1, (i + 1));
                //Ma HS
                sheet.GetRange(startRow, 2, startRow + (lstERtmp.Count > 0 ? lstERtmp.Count - 1 : 0), 2).Merge();
                sheet.GetRange(startRow, 2, startRow + (lstERtmp.Count > 0 ? lstERtmp.Count - 1 : 0), 2).SetHAlign(VTHAlign.xlHAlignLeft);
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                //Ho ten
                sheet.GetRange(startRow, 3, startRow + (lstERtmp.Count > 0 ? lstERtmp.Count - 1 : 0), 3).Merge();
                sheet.GetRange(startRow, 3, startRow + (lstERtmp.Count > 0 ? lstERtmp.Count - 1 : 0), 3).SetHAlign(VTHAlign.xlHAlignLeft);
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);
                if (lstERtmp.Count > 0)
                {
                    for (int j = 0; j < lstERtmp.Count; j++)
                    {
                        objER = lstERtmp[j];
                        sheet.SetCellValue(startRow, 4, objER.RewardID == GlobalConstants.EVALUATION_REWARD_FORTUITY ? Res.Get("Evaluation_Reward_Fortuity")
                                            : Res.Get("Evaluation_Reward_Outstanding_Achievement"));
                        sheet.SetCellValue(startRow, 5, objER.Content);
                        if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                        {
                            sheet.GetRange(startRow, 1, startRow + lstERtmp.Count, 5).SetFontStyle(false, System.Drawing.Color.Red, false, 12, true, false);
                        }
                        startRow++;
                    }
                }
                else
                {
                    if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 1, startRow + lstERtmp.Count, 5).SetFontStyle(false, System.Drawing.Color.Red, false, 12, true, false);
                    }
                    startRow++;
                }
            }
            sheet.GetRange(7, 1, startRow - 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.Name = objCP.DisplayName;
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = string.Format("HS_TH_Quanlykhenthuong_{0}_{1}.xls", SMAS.Web.Utils.Utils.StripVNSignAndSpace(objCP.DisplayName), "HK" + SemesterID);
            return result;
        }
        public PartialViewResult AjaxLoadGirdRatedComment(int ClassID, int SemesterID, int PupilID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID},
                {"PupilID",PupilID}
            };
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID,
                                        }).ToList();
            }
            else
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            CapacityEndingEvaluationName = rh.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                                        : rh.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : rh.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "",
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            QualityEndingEvaluationName = rh.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                                        : rh.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : rh.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "",
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID
                                        }).ToList();
            }
            List<RatedCommentPupilBO> lstSubjectRatedComment = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP).ToList();
            List<RatedCommentPupilBO> lstCapQuaRatedComment = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY).ToList();
            RatedCommentPupilBO objtmp = null;
            List<SubjectRatedCommentViewModel> lstSubjectViewModel = new List<SubjectRatedCommentViewModel>();
            SubjectRatedCommentViewModel objSubjectViewModel = new SubjectRatedCommentViewModel();
            List<CapacityQualityViewModel> lstCapQuaViewModel = new List<CapacityQualityViewModel>();
            CapacityQualityViewModel objCapQuaViewModel = null;

            //lay danh sach mon hoc cua lop
            bool ViewAll = false;

            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            if (ClassID > 0 && SemesterID > 0)
            {
                lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SemesterID, ClassID, ViewAll).Where(u => SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                  join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                  where s.IsActive == true
                                  && s.IsApprenticeshipSubject == false
                                  select new SubjectCatBO()
                                  {
                                      SubjectCatID = s.SubjectCatID,
                                      DisplayName = s.DisplayName,
                                      IsCommenting = cs.IsCommenting,
                                      OrderInSubject = s.OrderInSubject
                                  }
                                  ).OrderBy(p => p.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
            }
            int location = 0;
            int subjectID = 0;
            SubjectCatBO objSC = null;
            int lengthCount = lsClassSubject.Count / 2;
            int maxcount = lsClassSubject.Count % 2 == 0 ? lengthCount - 1 : lengthCount;
            for (int i = 0; i < lsClassSubject.Count; i++)
            {
                objSC = lsClassSubject[i];
                subjectID = objSC.SubjectCatID;
                objtmp = lstSubjectRatedComment.Where(p => p.SubjectID == subjectID).FirstOrDefault();
                if (i > maxcount)
                {
                    objSubjectViewModel = lstSubjectViewModel[location];
                    objSubjectViewModel.SubjectID2 = objSC.SubjectCatID;
                    objSubjectViewModel.SubjectName2 = objSC.DisplayName;
                    objSubjectViewModel.isCommenting2 = objSC.IsCommenting;
                    if (objtmp != null)
                    {
                        objSubjectViewModel.EndingSubject2 = objtmp.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                        : objtmp.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objtmp.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        objSubjectViewModel.MarkSubject2 = objtmp.PeriodicEndingMark;
                    }
                    location++;
                }
                else
                {
                    objSubjectViewModel = new SubjectRatedCommentViewModel();
                    objSubjectViewModel.SubjectID1 = objSC.SubjectCatID;
                    objSubjectViewModel.SubjectName1 = objSC.DisplayName;
                    objSubjectViewModel.isCommenting1 = objSC.IsCommenting;
                    if (objtmp != null)
                    {
                        objSubjectViewModel.EndingSubject1 = objtmp.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                        : objtmp.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objtmp.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        objSubjectViewModel.MarkSubject1 = objtmp.PeriodicEndingMark;

                    }
                    lstSubjectViewModel.Add(objSubjectViewModel);
                }
            }

            List<EvaluationCriteiaBO> lstEGBO = EvaluationCriteriaBusiness.All.Select(p => new EvaluationCriteiaBO
                                                            {
                                                                EvaluationCriteriaID = p.EvaluationCriteriaID,
                                                                CriteriaName = p.CriteriaName,
                                                                TypeID = p.TypeID
                                                            }).OrderBy(p => p.EvaluationCriteriaID).ToList();
            EvaluationCriteiaBO objEGBO = null;
            for (int i = 0; i < lstEGBO.Count; i++)
            {
                objEGBO = lstEGBO[i];
                objCapQuaViewModel = new CapacityQualityViewModel();
                objCapQuaViewModel.CriteriaID = objEGBO.EvaluationCriteriaID;
                objCapQuaViewModel.CriteriaName = objEGBO.CriteriaName;
                objCapQuaViewModel.TypeID = objEGBO.TypeID;
                objtmp = lstRatedCommentPupil.Where(p => p.EvaluationID == objEGBO.TypeID && p.SubjectID == objEGBO.EvaluationCriteriaID).FirstOrDefault();
                if (objtmp != null)
                {
                    objCapQuaViewModel.RatedName = objEGBO.TypeID == GlobalConstants.EVALUATION_CAPACITY ? objtmp.CapacityEndingEvaluationName
                                                        : objtmp.QualityEndingEvaluationName;
                }
                lstCapQuaViewModel.Add(objCapQuaViewModel);
            }
            ViewData[EvaluationRewardConstants.LIST_SUBJECT_VIEW_MODEL] = lstSubjectViewModel;
            ViewData[EvaluationRewardConstants.LIST_CAPQUA_VIEW_MODEL] = lstCapQuaViewModel;
            return PartialView("_ListRatedComment");
        }
        #region Khen thuong nhanh
        public PartialViewResult AjaxLoadSystemReward(int TypeSystemID, int TypeEmployeeID)
        {
            List<RewardCommentViewModel> lstSystemReward = new List<RewardCommentViewModel>();
            lstSystemReward = (from q in QuickHintBusiness.All
                               where q.FunctionCode.Equals("EvaluationReward")
                               && q.IsSystemHint
                               && q.TypeID == TypeSystemID
                               select new RewardCommentViewModel
                               {
                                   CommentCode = q.HintCode,
                                   Comment = q.Content,
                                   CollectionCommentsID = q.QuickHintID
                               }).OrderBy(p => p.CommentCode).ThenBy(p => p.Comment).ToList();
            List<string> lstCommentAccount = QuickHintBusiness.All.Where(p => p.FunctionCode.Equals("EvaluationReward") && p.TypeID == TypeEmployeeID && p.UserAccountID == _globalInfo.UserAccountID
                                                                        && !p.IsSystemHint).Select(p => p.Content).Distinct().ToList();
            lstSystemReward.ForEach(p =>
            {
                p.isEnable = !lstCommentAccount.Contains(p.Comment);
            });

            ViewData[EvaluationRewardConstants.LIST_COLLECTION_COMMENT] = lstSystemReward;
            return PartialView("_GridSystemReward");
        }
        public PartialViewResult AjaxLoadEmployeeReward(int TypeID)
        {
            List<RewardCommentViewModel> lstSEmployeeReward = new List<RewardCommentViewModel>();
            lstSEmployeeReward = (from q in QuickHintBusiness.All
                                  where q.FunctionCode.Equals("EvaluationReward")
                                  && !q.IsSystemHint
                                  && q.UserAccountID == _globalInfo.UserAccountID
                                  && q.TypeID == TypeID
                                  select new RewardCommentViewModel
                                  {
                                      CommentCode = q.HintCode,
                                      Comment = q.Content,
                                      CollectionCommentsID = q.QuickHintID
                                  }).OrderBy(p=>p.CommentCode).ThenBy(p=>p.Comment).ToList();
            ViewData[EvaluationRewardConstants.LIST_COLLECTION_COMMENT] = lstSEmployeeReward;
            return PartialView("_GridEmployeeReward");
        }
        [ValidateAntiForgeryToken]
        public JsonResult SaveRewardComment(List<RewardCommentViewModel> lstRewardComment, int TypeID)
        {
            lstRewardComment = lstRewardComment.Where(p => p.isChecked.HasValue && p.isChecked.Value).ToList();
            RewardCommentViewModel objRewardComment = new RewardCommentViewModel();
            List<QuickHint> lstQuictHint = new List<QuickHint>();
            QuickHint objQH = null;
            for (int i = 0; i < lstRewardComment.Count; i++)
            {
                objRewardComment = lstRewardComment[i];
                objQH = new QuickHint();
                objQH.QuickHintID = objRewardComment.CollectionCommentsID;
                objQH.FunctionCode = "EvaluationReward";
                objQH.Content = objRewardComment.Comment;
                objQH.HintCode = objRewardComment.CommentCode;
                objQH.IsSystemHint = false;
                objQH.UserAccountID = _globalInfo.UserAccountID;
                objQH.TypeID = TypeID;
                lstQuictHint.Add(objQH);
            }
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"TypeID",TypeID},
                {"FunctionCode","EvaluationReward"},
                {"UserAccountID",_globalInfo.UserAccountID}
            };
            QuickHintBusiness.InsertOrUpdate(dicInsert, lstQuictHint);
            return Json(new JsonMessage("Cập nhật thành công", "success"));
        }
        public PartialViewResult AjaxLoadMoveRigth(List<RewardCommentViewModel> lstRewardComment, string ArrSystemID, int TypeSystemID,int TypeEmployeeID)
        {
            List<int> lstQuickHintID = new List<int>();
            List<int> lstAccountID = (lstRewardComment != null && lstRewardComment.Count > 0) ? lstRewardComment.Select(p => p.CollectionCommentsID).ToList() : new List<int>();
            List<int> lstSystemID = ArrSystemID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            lstQuickHintID = lstAccountID.Union(lstSystemID).ToList();
            string RewardCode = TypeEmployeeID == 1 ? "DX" : "VT";
            IQueryable<RewardCommentViewModel> lsttmp = (from cc in QuickHintBusiness.All
                                                         where lstQuickHintID.Contains(cc.QuickHintID)
                                                         && cc.TypeID == TypeSystemID
                                                         select new RewardCommentViewModel
                                                             {
                                                                 CollectionCommentsID = cc.QuickHintID,
                                                                 Comment = cc.Content,
                                                                 CommentCode = cc.HintCode
                                                             });
            List<RewardCommentViewModel> lstRewardSystem = lsttmp.Where(p => lstSystemID.Contains(p.CollectionCommentsID) && !lstAccountID.Contains(p.CollectionCommentsID)).ToList();

            string maxCode = (lstRewardComment != null && lstRewardComment.Count > 0) ? lstRewardComment.Where(p => p.CommentCode.StartsWith(RewardCode)).Max(p => p.CommentCode) : "";
            int maxNumber = !string.IsNullOrEmpty(maxCode) ? Convert.ToInt32(maxCode.Substring(RewardCode.Length)) : 0;
            for (int i = 0; i < lstRewardSystem.Count; i++)
            {
                var objSystem = lstRewardSystem[i];
                maxNumber++;
                objSystem.CommentCode = RewardCode + maxNumber.ToString();
            }
            List<RewardCommentViewModel> lstResult = (lstRewardComment != null && lstRewardComment.Count > 0) ? lstRewardComment.Union(lstRewardSystem).ToList() : lstRewardSystem;

            ViewData[EvaluationRewardConstants.LIST_COLLECTION_COMMENT] = lstResult;
            return PartialView("_GridEmployeeReward");
        }
        [ValidateAntiForgeryToken]
        public JsonResult DeleteRewardComment(string ArrID, int TypeID)
        {
            try
            {
                List<int> lstQuickHintID = ArrID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                List<QuickHint> lstQuickHint = QuickHintBusiness.All.Where(p => p.FunctionCode.Equals("EvaluationReward") && p.UserAccountID == _globalInfo.UserAccountID
                                                                    && p.TypeID == TypeID && lstQuickHintID.Contains(p.QuickHintID)).ToList();
                QuickHintBusiness.DeleteAll(lstQuickHint);
                QuickHintBusiness.Save();
                return Json(new JsonMessage("Xóa nhận xét thành công", "success"));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Json(new JsonMessage("Có lỗi trong quá trình xử lý Thầy/cô vui lòng kiểm tra lại.", "error"));
            }
        }
        public FileResult ExportRewardComment(int TypeID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "GV", EvaluationRewardConstants.TEMPLATE_FILE_COLLECTION_COMMENTS);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //fill thong tin chung
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            sheet.SetCellValue("A5", "LOẠI: " + (TypeID == 1 ? "KHEN THƯỞNG ĐỘT XUẤT" : "HỌC SINH CÓ THÀNH TÍCH VƯỢT TRỘI"));
            var lstCC = (from cc in QuickHintBusiness.All
                                                where cc.FunctionCode.Equals("EvaluationReward")
                                                && cc.UserAccountID == _globalInfo.UserAccountID
                                                && cc.TypeID == TypeID
                                                && !cc.IsSystemHint
                                                select new 
                                                {
                                                    QuickHintID = cc.QuickHintID,
                                                    Comment = cc.Content,
                                                    CommentCode = cc.HintCode
                                                }).OrderBy(p => p.CommentCode).ToList();
            int startRow = 8;
            for (int i = 0; i < lstCC.Count; i++)
            {
                var objCC = lstCC[i];
                sheet.SetCellValue(startRow, 1, (i + 1));
                sheet.SetCellValue(startRow, 2, objCC.CommentCode);
                sheet.SetCellValue(startRow, 3, objCC.Comment);
                startRow++;
            }
            sheet.GetRange(8, 1, startRow - 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "GV_NganHangKhenThuong.xls";
            return result;
        }
        [ValidateAntiForgeryToken]
        public JsonResult ImportRewardComment(IEnumerable<HttpPostedFileBase> attachmentsCC, int TypeID)
        {
            if (attachmentsCC == null || attachmentsCC.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachmentsCC.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                List<QuickHint> lstQuickHint = new List<QuickHint>();
                QuickHint objQuickHint = null;
                List<long> lstCollectionCommentID = new List<long>();
                int startRow = 8;
                if (sheet.GetCellValue(startRow - 1, 1) != null && sheet.GetCellValue(startRow - 1, 2) != null && sheet.GetCellValue(startRow - 1, 3) != null)
                {
                    if (!sheet.GetCellValue(startRow - 1, 1).Equals("STT") || !sheet.GetCellValue(startRow - 1, 2).Equals("Mã") || !sheet.GetCellValue(startRow - 1, 3).Equals("Nội dung nhận xét"))
                    {
                        return Json(new JsonMessage("File import không đúng mẫu", "error"));
                    }
                }
                else
                {
                    return Json(new JsonMessage("File import không đúng mẫu", "error"));
                }

                while (sheet.GetCellValue(startRow, 1) != null && sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
                {
                    objQuickHint = new QuickHint();
                    objQuickHint.UserAccountID = _globalInfo.UserAccountID;
                    objQuickHint.FunctionCode = "EvaluationReward";
                    objQuickHint.TypeID = TypeID;
                    objQuickHint.IsSystemHint = false;
                    string strval = string.Empty;
                    strval = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : "";
                    objQuickHint.HintCode = strval;
                    strval = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString() : "";
                    objQuickHint.Content = strval;
                    lstQuickHint.Add(objQuickHint);
                    startRow++;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"TypeID",TypeID},
                    {"FunctionCode","EvaluationReward"},
                    {"UserAccountID",_globalInfo.UserAccountID}
                };
                QuickHintBusiness.InsertOrUpdate(dic, lstQuickHint,true);
            }
            return Json(new JsonMessage("Import thành công", "success"));
        }
        #endregion
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddMenuUsual(string url)
        {
            Menu objMenu = SMAS.Business.Common.StaticData.ListMenu.Where(c => c.URL.Contains(url)).FirstOrDefault();

            List<BookmarkedFunction> lstBm = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.GetValueOrDefault(),
                _globalInfo.AcademicYearID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID);

            BookmarkedFunction objDB = lstBm.Where(p => p.MenuID == objMenu.MenuID).FirstOrDefault();

            if (objDB == null)
            {
                BookmarkedFunction bf = new BookmarkedFunction();
                bf.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bf.AcademicYearID = _globalInfo.AcademicYearID;
                bf.LevelID = _globalInfo.AppliedLevel;
                bf.UserID = _globalInfo.UserAccountID;
                bf.MenuID = objMenu.MenuID;
                bf.Semester = _globalInfo.Semester;
                string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", objMenu.MenuName);
                bf.FunctionName = string.IsNullOrEmpty(name) ? objMenu.MenuName : name;
                bf.Order = lstBm.Count + 1;
                bf.CreatedDate = DateTime.Now;

                BookmarkedFunctionBusiness.Insert(bf);
                BookmarkedFunctionBusiness.Save();
            }
            return Json(new JsonMessage("Đã thêm vào danh sách chức năng thường dùng.", "success"));
        }
        private void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                {
                    semester1 = true;
                }
                else
                {
                    semester2 = true;
                }
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[EvaluationRewardConstants.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[EvaluationRewardConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[EvaluationRewardConstants.LIST_CLASS] = listClass;
                var listCP = listClass.ToList();
                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            // Ưu tiên trước đối với giáo viên bộ môn
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                            List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                            {
                                cp = listSubjectTeacher.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                        .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listHead != null && listHead.Count > 0)
                                {
                                    cp = listHead.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }
        }
    }
}