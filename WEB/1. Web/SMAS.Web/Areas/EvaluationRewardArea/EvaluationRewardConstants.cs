﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationRewardArea
{
    public class EvaluationRewardConstants
    {
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string CLASS_NULL = "classnull";
        public const string ClassID = "ClassID";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EVALUATION_REWARD = "listEvaluationReward";
        public const string LIST_RATED_COMMENT_PUPIL = "lstRatedCommentPupil";
        public const string LIST_SUBJECT_VIEW_MODEL = "lstSubjectViewModel";
        public const string LIST_CAPQUA_VIEW_MODEL = "lstCapQuaViewModel";
        public const string TEMPLATE_FILE = "HS_TH_Quanlykhenthuong.xls";
        public const string IS_ENABLED = "IsEnabled";
        public const string LIST_COLLECTION_COMMENT = "lstCollectionComment";
        public const string TEMPLATE_FILE_COLLECTION_COMMENTS = "GV_NganHangNhanXet.xls";
        public const string LIST_AUTOCOMPLETE = "lstAutocomplete";
    }
}