﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EvaluationRewardArea
{
    public class EvaluationRewardAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EvaluationRewardArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EvaluationRewardArea_default",
                "EvaluationRewardArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
