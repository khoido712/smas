﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationRewardArea.Models
{
    public class CapacityQualityViewModel
    {
        public int CriteriaID { get; set; }
        public string CriteriaName { get; set; }
        public string RatedName { get; set; }
        public int TypeID { get; set; }
    }
}