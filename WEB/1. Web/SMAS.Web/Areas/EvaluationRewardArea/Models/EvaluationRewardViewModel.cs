﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationRewardArea.Models
{
    public class EvaluationRewardViewModel
    {
        public long EvaluationRewardID { get; set; }
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public int ClassID { get; set; }
        public int StatusID { get; set; }
        public List<RewardPupilContent> lstReward { get; set; }
    }
    public class RewardPupilContent
    {
        public int PupilID { get; set; }
        public int? RewardID { get; set; }
        public long EvaluationRewardID { get; set; }
        public string RewardName { get; set; }
        public string Content { get; set; }
        public bool? isChecked { get; set; }
        public int OrderID { get; set; }
    }
}