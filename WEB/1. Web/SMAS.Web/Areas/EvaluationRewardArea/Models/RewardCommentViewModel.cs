﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationRewardArea.Models
{
    public class RewardCommentViewModel
    {
        public int CollectionCommentsID { get; set; }
        public string Comment { get; set; }
        public string CommentCode { get; set; }
        public bool? isChecked { get; set; }
        public bool isEnable { get; set; }
        public int? TypeID { get; set; }
    }
}