﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamineeCodeNameArea.Models
{
    public class ExamineeCodeNameViewModel
    {
        public long ExamPupilID { get; set; }
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public int EducationLevelID { get; set; }
        public int SchoolID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public long PupilID { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_PupilID")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_ExamineeNumber")]
        public string ExamineeNumber { get; set; }

        public long? ExamRoomID { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }

        public string Name { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_Birthday")]
        public DateTime Birthday { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_Gender")]
        public int? Genre { get; set; }

        public int ClassID { get; set; }

        [ResourceDisplayName("ExamineeCodeName_Label_ClassName")]
        public string ClassName { get; set; }

        public string EthnicCode { get; set; }

        public int Status { get; set; }
    }
}