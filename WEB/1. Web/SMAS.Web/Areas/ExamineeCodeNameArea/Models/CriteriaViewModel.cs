﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamineeCodeNameArea.Models
{
    public class CriteriaViewModel
    {
        public string CriteriaDisplay { get; set; }
        public string CriteriaValue { get; set; }
    }
}