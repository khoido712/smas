﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamineeCodeNameArea.Models
{
    public class CountExamPupil
    {
        public long ExamGroupID { get; set; }
        public string ExamGroupName { get; set; }
        public int Count { get; set; }
    }
}