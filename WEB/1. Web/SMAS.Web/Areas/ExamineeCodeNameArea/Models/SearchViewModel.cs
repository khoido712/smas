﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamineeCodeNameArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamineeCodeNameConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamineeCode_Error_Examinations")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamGroup(this)")]
        public long Examinations { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamineeCodeNameConstants.LIST_EXAM_GROUP)]
        [AdditionalMetadata("Placeholder", "null")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamineeCode_Error_ExamGroup")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamineeCodeName(this)")]
        public long ExamGroup { get; set; }
    }
}