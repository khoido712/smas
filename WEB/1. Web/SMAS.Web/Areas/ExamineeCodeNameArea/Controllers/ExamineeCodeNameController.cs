﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Business.Common;
using SMAS.Web.Controllers;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ExamineeCodeNameArea.Models;
using Telerik.Web.Mvc;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Web.Areas.ExamineeCodeNameArea.Controllers
{
    public class ExamineeCodeNameController : BaseController
    {
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;

        public ExamineeCodeNameController(IExamPupilBusiness ExamPupilBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            IExaminationsBusiness ExaminationsBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness)
        {
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ExaminationsBusiness = ExaminationsBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetViewData

        public void SetViewData()
        {
            SetViewDataPermission("ExamineeCodeName", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            ViewData[ExamineeCodeNameConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExamineeCodeNameConstants.CURRENT_SEMESTER] = _globalInfo.Semester;

            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamineeCodeNameConstants.LIST_EXAMINATIONS] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");

            if (listExam != null && listExam.Count > 0)
            {
                Examinations exam = listExam.FirstOrDefault();
                List<ExamGroup> listGroup = this.GetExamGroupFromExaminations(exam.ExaminationsID);
                ViewData[ExamineeCodeNameConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");

                search["ExaminationsID"] = exam.ExaminationsID;

                if (listGroup != null && listGroup.Count > 0)
                {
                    search["ExamGroupID"] = listGroup.FirstOrDefault().ExamGroupID;
                    // Kiem tra co ky thi va nhom thi duoc chon
                    ViewData[ExamineeCodeNameConstants.IS_CHOOSE] = true;
                }
                else
                {
                    ViewData[ExamineeCodeNameConstants.IS_CHOOSE] = false;
                }

                IEnumerable<ExamineeCodeNameViewModel> list = _Search(search);
                List<ExamineeCodeNameViewModel> listResult = null;
                int totalRecord = list.Count();
                ViewData[ExamineeCodeNameConstants.TOTAL] = totalRecord;
                listResult = list.Take(ExamineeCodeNameConstants.PAGE_SIZE).ToList();
                ViewData[ExamineeCodeNameConstants.LIST_EXAM_PUPIL] = listResult;

                if (_globalInfo.IsCurrentYear == true && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
                {
                    ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = true;
                }
                else
                {
                    ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = false;
                }
            }
            else
            {
                ViewData[ExamineeCodeNameConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = false;
                ViewData[ExamineeCodeNameConstants.IS_CHOOSE] = false;
            }

            ExamGroup group = new ExamGroup();
            group.ExamGroupID = 0;
            group.ExamGroupName = string.Empty;
            ViewData[ExamineeCodeNameConstants.GROUP_INFO] = group;
            ViewData[ExamineeCodeNameConstants.TOTAL_EXAM_PUPIL] = 0;
            ViewData[ExamineeCodeNameConstants.IS_CHECK_CODE_EXAM] = false;
            ViewData[ExamineeCodeNameConstants.IS_CHECK_CODE_GROUP] = false;
            ViewData[ExamineeCodeNameConstants.MAX_LENGTH_EXAM] = string.Empty;
            ViewData[ExamineeCodeNameConstants.MAX_LENGTH_GROUP] = 0;
        }

        #endregion SetViewData

        #region LoadComboBox

        private List<ExamGroup> GetExamGroupFromExaminations(long ExaminationsID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID", ExaminationsID},
            };

            List<ExamGroup> listGroup = ExamGroupBusiness.Search(dic).OrderBy(o => o.ExamGroupCode.ToUpper()).ToList();
            return listGroup;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamGroup(long? ExaminationsID)
        {
            if (ExaminationsID.HasValue)
            {
                IEnumerable<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID.Value);
                return Json(new SelectList(listGroup, "ExamGroupID", "ExamGroupName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion LoadComboBox

        #region Search

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadExamineeCodeName(long? ExaminationsID, long? ExamGroupID)
        {
            ViewData[ExamineeCodeNameConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            SetViewDataPermission("ExamineeCodeName", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            if (_globalInfo.IsCurrentYear == true && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = true;
            }
            else
            {
                ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = false;
            }

            List<ExamineeCodeNameViewModel> list = null;
            if (ExaminationsID.HasValue && ExamGroupID.HasValue && ExaminationsID > 0 && ExamGroupID > 0)
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
                search.Add("ExaminationsID", ExaminationsID);
                search.Add("ExamGroupID", ExamGroupID);
                search.Add("SchoolID", _globalInfo.SchoolID);
                search.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

                IEnumerable<ExamineeCodeNameViewModel> query = _Search(search);
                int totalRecord = query.Count();
                ViewData[ExamineeCodeNameConstants.TOTAL] = totalRecord;
                list = query.Take(ExamineeCodeNameConstants.PAGE_SIZE).ToList();
                ViewData[ExamineeCodeNameConstants.LIST_EXAM_PUPIL] = list;
            }
            else
            {
                list = new List<ExamineeCodeNameViewModel>();
                ViewData[ExamineeCodeNameConstants.LIST_EXAM_PUPIL] = list;
                ViewData[ExamineeCodeNameConstants.IS_CHOOSE] = false;
            }

            return PartialView("_List");
        }


        public PartialViewResult Search(SearchViewModel form)
        {
            ViewData[ExamineeCodeNameConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            SetViewDataPermission("ExamineeCodeName", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            Examinations exam = ExaminationsBusiness.Find(form.Examinations);
            if (_globalInfo.IsCurrentYear == true && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = true;
            }
            else
            {
                ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = false;
            }

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("ExaminationsID", form.Examinations);
            search.Add("ExamGroupID", form.ExamGroup);
            search.Add("SchoolID", _globalInfo.SchoolID);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

            List<ExamineeCodeNameViewModel> list = null;
            IEnumerable<ExamineeCodeNameViewModel> query = _Search(search);
            int totalRecord = query.Count();
            ViewData[ExamineeCodeNameConstants.TOTAL] = totalRecord;
            list = query.Take(ExamineeCodeNameConstants.PAGE_SIZE).ToList();
            ViewData[ExamineeCodeNameConstants.LIST_EXAM_PUPIL] = list;

            return PartialView("_List");
        }

        private List<ExamineeCodeNameViewModel> _Search(IDictionary<string, object> search)
        {
            ViewData[ExamineeCodeNameConstants.IS_CHOOSE] = true;
            var lstExamPupilBO = ExamPupilBusiness.GetListExamineeNumber(search).ToList();
            if (lstExamPupilBO == null || lstExamPupilBO.Count == 0)
            {
                return new List<ExamineeCodeNameViewModel>();
            }
            List<ExamineeCodeNameViewModel> listExamineeCodeName = lstExamPupilBO.Select(o => new ExamineeCodeNameViewModel
            {
                ExamPupilID = o.ExamPupilID,
                ExaminationsID = o.ExaminationsID,
                ExamGroupID = o.ExamGroupID,
                EducationLevelID = o.EducationLevelID,
                SchoolID = o.SchoolID,
                PupilID = o.PupilID,
                PupilCode = o.PupilCode,
                ExamineeNumber = o.ExamineeNumber,
                ExamRoomID = o.ExamRoomID,
                Name = o.Name,
                EthnicCode = o.EthnicCode,
                FullName = o.PupilFullName,
                Birthday = o.BirthDate,
                Genre = o.Genre,
                ClassName = o.ClassName

            }).OrderBy(o => o.ExamineeNumber).ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName.getOrderingName(o.EthnicCode))).ToList();

            // Kiem tra danh sach hoc sinh da duoc xep phong thi chua?
            int countRoomId = listExamineeCodeName.Where(o => o.ExamRoomID.HasValue).Select(o => o.ExamRoomID.Value).Count();
            ViewData[ExamineeCodeNameConstants.IS_CHECK_ROOM] = countRoomId > 0 ? true : false;

            return listExamineeCodeName;
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(SearchViewModel form, GridCommand command)
        {
            ViewData[ExamineeCodeNameConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            SetViewDataPermission("ExamineeCodeName", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            Examinations exam = ExaminationsBusiness.Find(form.Examinations);
            if (_globalInfo.IsCurrentYear == true && exam != null && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = true;
            }
            else
            {
                ViewData[ExamineeCodeNameConstants.BTN_COMMON_ENABLE] = false;
            }

            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);

            List<ExamineeCodeNameViewModel> listResult;
            if (form.Examinations == 0)
            {
                listResult = new List<ExamineeCodeNameViewModel>();
                return View(new GridModel<ExamineeCodeNameViewModel>
                {
                    Total = 0,
                    Data = listResult
                });
            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            SearchInfo["ExaminationsID"] = form.Examinations;
            SearchInfo["ExamGroupID"] = form.ExamGroup;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;

            //Add search info - Navigate to Search function in business
            IEnumerable<ExamineeCodeNameViewModel> iq = _Search(SearchInfo);
            int totalRecord = iq.Count();
            listResult = iq.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

            return View(new GridModel<ExamineeCodeNameViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        #endregion Search

        #region CheckCodeName

        public PartialViewResult GetDataInsert(long? ExaminationsID, long? ExamGroupID)
        {
            if (ExaminationsID.HasValue && ExamGroupID.HasValue)
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
                search.Add("SchoolID", _globalInfo.SchoolID);
                search.Add("ExaminationsID", ExaminationsID.Value);
                search.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

                // Danh sach nhom cua ky thi
                List<ExamGroup> listGroup = ExamGroupBusiness.Search(search).OrderBy(o => o.ExamGroupCode.ToUpper()).ToList();

                // Danh sach so bao danh khac rong cua ki thi
                IQueryable<long> listSBDExam = ExamPupilBusiness.Search(search).Where(o => o.ExamineeNumber != null || o.ExamineeNumber != string.Empty).Select(o => o.ExamGroupID);
                ViewData[ExamineeCodeNameConstants.IS_CHECK_CODE_GROUP] = listSBDExam.Count() > 0 ? true : false;
                ViewData[ExamineeCodeNameConstants.IS_CHECK_CODE_EXAM] = listSBDExam.Contains(ExamGroupID.Value) ? true : false;

                // Tim max lenght
                string listLength = string.Empty;
                // Danh sach cac thi sinh cua ky thi
                IQueryable<ExamPupil> listExamPupil = ExamPupilBusiness.Search(search);
                // Danh sach nhom va so luong thi sinh trong moi nhom
                var noticesGrouped = listExamPupil.GroupBy(o => o.ExamGroupID).
                                                Select(grp =>
                                                new
                                                {
                                                    ExamGroupID = grp.Key,
                                                    Count = grp.Count()
                                                }).OrderBy(o => o.ExamGroupID);

                foreach (var item in noticesGrouped)
                {
                    string name = listGroup.Where(o => o.ExamGroupID == item.ExamGroupID).FirstOrDefault().ExamGroupName;
                    string length = name + "-" + item.Count.ToString().Length;
                    listLength = listLength + length + ",";
                }

                ViewData[ExamineeCodeNameConstants.MAX_LENGTH_EXAM] = listLength;

                // Tim thong tin cho nhom
                search["ExamGroupID"] = ExamGroupID.Value;

                ExamGroup group = ExamGroupBusiness.Find(ExamGroupID);
                ViewData[ExamineeCodeNameConstants.GROUP_INFO] = group;

                IQueryable<long> listPupil = ExamPupilBusiness.GetListExamineeNumber(search).Select(o => o.ExamPupilID);
                int count = listPupil.Count();
                ViewData[ExamineeCodeNameConstants.TOTAL_EXAM_PUPIL] = count;
                ViewData[ExamineeCodeNameConstants.MAX_LENGTH_GROUP] = count.ToString().Length;
            }

            return PartialView("_CheckCodeName");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult CheckCodeName(FormCollection frm)
        {
            if (GetMenupermission("ExamineeCodeName", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            long HiddenExamGroup = !string.IsNullOrEmpty(frm["HiddenExamGroup"]) ? long.Parse(frm["HiddenExamGroup"]) : 0;
            long HiddenExaminations = !string.IsNullOrEmpty(frm["HiddenExaminations"]) ? long.Parse(frm["HiddenExaminations"]) : 0;
            int HiddenTotalExamPupil = !string.IsNullOrEmpty(frm["HiddenTotalExamPupil"]) ? int.Parse(frm["HiddenTotalExamPupil"]) : 0;
            int ValueCheckCurrent = !string.IsNullOrEmpty(frm["ValueCheckCurrent"]) ? int.Parse(frm["ValueCheckCurrent"]) : 0;
            string valCriteria = !string.IsNullOrEmpty(frm["valueCriteria"]) ? frm["valueCriteria"] : "";
            string valOrder = !string.IsNullOrEmpty(frm["valueOrderSelect"]) ? frm["valueOrderSelect"] : "";
            int checkAll = 0;

            if (HiddenExaminations != 0 && HiddenExamGroup != 0)
            {
                string CriteriaIIIText = "";
                string CriteriaIVText = "";
                List<int> lstCheckCriteria = valCriteria.Split(new string[]{","}, StringSplitOptions.RemoveEmptyEntries).Select(x=> int.Parse(x)).ToList();
                List<int> lstOrder = valOrder.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList();

                if (lstCheckCriteria.Contains(3))
                {
                    CriteriaIIIText = !string.IsNullOrEmpty(frm["txtCriteriaIII"]) ? frm["txtCriteriaIII"] : "";
                }

                if (lstCheckCriteria.Contains(4))
                {
                    CriteriaIVText = !string.IsNullOrEmpty(frm["txtCriteriaIV"]) ? frm["txtCriteriaIV"] : "";
                }

                string userDescription = string.Format(Res.Get("ExamineeCode_WriteLog_Description_CurrentGroup"), HiddenExaminations, HiddenExamGroup, HiddenTotalExamPupil);             
                
                // Tất cả nhóm - không chọn sử dụng chung SBD
                if (ValueCheckCurrent == 1)
                {
                    checkAll = 1;
                    userDescription = string.Format(Res.Get("ExamineeCode_WriteLog_Description_AllGroup"), HiddenExaminations);
                }

                // Tất cả nhóm - chọn sử dụng chung SBD
                int usingEXCode = 0;
                if (ValueCheckCurrent == 2)
                {
                    checkAll = 2;
                    userDescription = string.Format(Res.Get("ExamineeCode_WriteLog_Description_AllGroup"), HiddenExaminations);
                    usingEXCode = (!string.IsNullOrEmpty(frm["usingEXCode"]) ? int.Parse(frm["usingEXCode"]) : 1);
                }

                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
                search.Add("AppledLevelID", _globalInfo.AppliedLevel.Value);
                search.Add("SchoolID", _globalInfo.SchoolID);
                search.Add("ExaminationsID", HiddenExaminations);
                search.Add("ExamGroupID", HiddenExamGroup);

                ExamPupilBusiness.UpdateExamineeCodeNameCustom(search, lstCheckCriteria, lstOrder, CriteriaIIIText, CriteriaIVText, checkAll, usingEXCode);
                // Ghi log hành động cập nhật số báo danh
                SetViewDataActionAudit(String.Empty, String.Empty
                    , HiddenExamGroup.ToString()
                    , Res.Get("ExamineeCodeName_Title_Page")
                    , "Examinations: " + HiddenExaminations + ", ExamGroup: " + HiddenExamGroup + ", CountExamPupil: " + HiddenTotalExamPupil
                    , Res.Get("ExamineeCodeName_Title_Page")
                    , SMAS.Business.Common.GlobalConstants.ACTION_UPDATE
                    , userDescription);

                return Json(new JsonMessage(Res.Get("Common_Label_CheckCodeName")));
            }

            return Json(new JsonMessage(Res.Get("Common_Label_CheckCodeName_Failed")), "error");
        }

        #endregion CheckCodeName

        #region Delete


        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DeleteCheck(long ExaminationsID, long ExamGroupID, string stringExamPupil)
        {
            if (GetMenupermission("ExamineeCodeName", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            string[] listDelete = stringExamPupil.Split(',');
            List<long> listExamPupil = new List<long>();

            string listWriteLog = string.Empty;
            for (int i = 0; i < listDelete.Length - 1; i++)
            {
                listExamPupil.Add(long.Parse(listDelete[i]));
                listWriteLog = listWriteLog + " " + listDelete[i];
            }

            IDictionary<string, object> search = new Dictionary<string, object>()
            {
                { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                { "SchoolID", _globalInfo.SchoolID.Value },
                { "ExaminationsID", ExaminationsID },
                { "ExamGroupID", ExamGroupID }
            };

            ExamPupilBusiness.DeleteCheckCodeName(search, listExamPupil);

            // Ghi log hanh dong xoa
            string userDescription = string.Format(Res.Get("ExamineeCode_WriteLog_Description_Delete"), listWriteLog, ExaminationsID, ExamGroupID);
            SetViewDataActionAudit(String.Empty, String.Empty
                , ExamGroupID.ToString()
                , Res.Get("ExamineeCode_WriteLog_Delete")
                , Res.Get("ExamineeCode_WriteLog_Delete") + " - ExaminationsID: " + ExaminationsID + ", ExamGroupID: " + ExamGroupID + ", Count: " + (listDelete.Count() - 1).ToString()
                , Res.Get("ExamineeCodeName_Title_Page")
                , SMAS.Business.Common.GlobalConstants.ACTION_DELETE
                , userDescription);

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion Delete

        #region xuat excel
        public FileResult ExportCheckCode(int ExaminationsID, int ExamGroupID)
        {
            string templatePath = string.Empty;
            string fileName = string.Empty;
            string fileTemplate = string.Format("{0}.xls", SystemParamsInFile.THI_THPT_DSThisinh_LHS);
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "THI", fileTemplate);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            #region
            string examinationName = string.Empty;
            string examGroupCode = string.Empty;

            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            List<ExamGroup> listGroup = this.GetExamGroupFromExaminations(exam.ExaminationsID);
            ExamGroup objGroup = listGroup.Where(x => x.ExamGroupID == ExamGroupID).FirstOrDefault();
            examinationName = exam.ExaminationsName;
            examGroupCode = objGroup.ExamGroupCode;

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("ExaminationsID", ExaminationsID);
            search.Add("ExamGroupID", ExamGroupID);
            search.Add("SchoolID", _globalInfo.SchoolID);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

            List<ExamineeCodeNameViewModel> listResult = _Search(search).ToList();

            firstSheet.SetCellValue("A2", "KỲ THI: " + examinationName.ToUpper());
            firstSheet.SetCellValue("A3", "NHÓM THI: " + examGroupCode.ToUpper());
            this.SetDataToFile(firstSheet, listResult);

            string appliedLevel = string.Empty;
            if (_globalInfo.AppliedLevel == 1)
                appliedLevel = "TH";
            else if (_globalInfo.AppliedLevel == 2)
                appliedLevel = "THCS";
            else
                appliedLevel = "THPT";
            fileName = string.Format("THI_{0}_DSThiSinh_{1}.xls", appliedLevel, SMAS.Web.Utils.Utils.StripVNSignAndSpace(examGroupCode, true));

            #endregion
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        #region SetDataToFile
        private void SetDataToFile(
            IVTWorksheet sheet,
            List<ExamineeCodeNameViewModel> listResult)
        {
            //fill thong tin
            int startRow = 7;
            int index = 1;
            int indexBoder = 0;
            ExamineeCodeNameViewModel objVM = null;
            for (int i = 0; i < listResult.Count; i++)
            {
                indexBoder++;
                objVM = listResult[i];
                // STT
                sheet.SetCellValue(startRow, 1, index);
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                // SBD
                sheet.SetCellValue(startRow, 2, objVM.ExamineeNumber);
                // Ma HS
                sheet.SetCellValue(startRow, 3, objVM.PupilCode);
                // Ho ten
                sheet.SetCellValue(startRow, 4, objVM.FullName);
                sheet.GetRange(startRow, 2, startRow, 4).SetHAlign(VTHAlign.xlHAlignLeft);
                // Ngay sinh
                sheet.SetCellValue(startRow, 5, ("'" + objVM.Birthday.ToString("dd/MM/yyyy")));
                // Gioi tinh
                sheet.SetCellValue(startRow, 6, objVM.Genre == 1 ? "Nam" : "Nữ");
                // Lop
                sheet.SetCellValue(startRow, 7, objVM.ClassName);
                sheet.GetRange(startRow, 5, startRow, 7).SetHAlign(VTHAlign.xlHAlignCenter);

                if (indexBoder != 5)
                {
                    for (int k = 1; k <= 7; k++)
                    {
                        sheet.GetRange(startRow, k, startRow, k).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                }
                else if (indexBoder == 5)
                {
                    indexBoder = 0;
                    sheet.GetRange(startRow, 1, startRow, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thick, VTBorderIndex.EdgeBottom);
                }

                if (i == (listResult.Count - 1))
                {
                    sheet.GetRange(startRow, 1, startRow, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thick, VTBorderIndex.EdgeBottom);
                }

                for (int h = 1; h <= 8; h++)
                {
                    sheet.GetRange(startRow, h, startRow, h).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                }

                index++;
                startRow++;
            }

            sheet.SetFontName("Times New Roman", 12);
        }
        #endregion

        #endregion

        #region Import Excel

        #region //RenderPartialViewToString
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        #region // UploadFileImportSubject
        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImport(IEnumerable<HttpPostedFileBase> attachments, int examinationsID, int examGroupID)
        {
            if (attachments == null || attachments.Count() <= 0)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;
                List<int> listTypeReportExcel = new List<int>();
                List<string> listSheetNameExcel = new List<string>();
                string strValue = string.Empty;
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 5120)
                {
                    return Json(new JsonMessage(Res.Get("Common_Max_Size_FileExcel"), "error"));
                }

                string sheetInfo = string.Empty;
                if (lstSheets.Count > 0)
                {
                    for (int i = 0; i < lstSheets.Count; i++)
                    {
                        sheet = oBook.GetSheet(i + 1);
                        listSheetNameExcel.Add(sheet.Name);
                    }

                    if (listSheetNameExcel.Count <= 0)
                        return Json(new JsonMessage(Res.Get("Import dữ liệu thất bại"), "error"));
                    else
                    {
                        Session[ExamineeCodeNameConstants.PHISICAL_PATH] = physicalPath;
                        return Json(new { strOption = "success" });
                    }

                }
            }
            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }
        #endregion

        #region // ImportExcel
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int examinationsID, int examGroupID)
        {
            if (Session[ExamineeCodeNameConstants.PHISICAL_PATH] == null)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            string physicalPath = (string)Session[ExamineeCodeNameConstants.PHISICAL_PATH];
            IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);
            string messageSuccess = "Nhập dữ liệu từ Excel thành công";
            string Error = string.Empty;
            List<int> lstError = new List<int>();

            List<ListErrorExamBO> lstErrorBO = new List<ListErrorExamBO>();

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID);
            search.Add("ExaminationsID", examinationsID);
            search.Add("ExamGroupID", examGroupID);

            ExamGroup group = ExamGroupBusiness.Find(examGroupID);
            string examinationName = string.Empty;
            string examGroupCode = string.Empty;

            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            List<ExamGroup> listGroup = this.GetExamGroupFromExaminations(exam.ExaminationsID);
            ExamGroup objGroup = listGroup.Where(x => x.ExamGroupID == examGroupID).FirstOrDefault();
            examinationName = exam.ExaminationsName;
            examGroupCode = objGroup.ExamGroupCode;

            List<ExamPupilBO> listExamPupil = ExamPupilBusiness.GetListExamineeNumber(search).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
            List<ExamPupil> listSBD = listExamPupil.Select(o => new ExamPupil
            {
                ExamPupilID = o.ExamPupilID,
                ExaminationsID = o.ExaminationsID,
                ExamGroupID = o.ExamGroupID,
                ExamRoomID = o.ExamRoomID,
                EducationLevelID = o.EducationLevelID,
                SchoolID = o.SchoolID,
                LastDigitSchoolID = o.LastDigitSchoolID,
                PupilID = o.PupilID,
                ExamineeNumber = o.ExamineeNumber,
                CreateTime = o.CreateTime,
                UpdateTime = DateTime.Now
            }).Distinct().ToList();

            List<ExamPupil> lstSBD = new List<ExamPupil>();

            lstSBD = this.GetDataToFileExcel(obook, ref lstSBD, listExamPupil, ref lstErrorBO,
                    examinationName, examGroupCode, ref Error, ref lstError);

            if (!string.IsNullOrEmpty(Error))
            {
                return Json(new JsonMessage(Error, "error"));
            }

            if (lstError.Count > 0)
            {
                List<ListErrorExamBO> LstErrorMessage = new List<ListErrorExamBO>();
                ListErrorExamBO objError = null;
                foreach (var item in lstErrorBO)
                {
                    objError = new ListErrorExamBO();
                    objError.PupilCode = item.PupilCode;
                    objError.PupilName = item.PupilName;
                    objError.Description = item.Description;
                    LstErrorMessage.Add(objError);
                }

                if (LstErrorMessage.Count > 0)
                {
                    ViewData[ExamineeCodeNameConstants.LIST_ERROR_IMPORT] = LstErrorMessage;
                    return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "CreateViewError"));
                }
            }

            this.ExamPupilBusiness.UpdateList(lstSBD);         
            return Json(new JsonMessage(messageSuccess, "success"));
        }
        #endregion
       
        #region // GetDataToFileExcel
        private List<ExamPupil> GetDataToFileExcel(
            IVTWorkbook oBook,
            ref List<ExamPupil> lstSBD,
            List<ExamPupilBO> listResult,
            ref List<ListErrorExamBO> lstErrorBO,
            string examinationName,
            string examGroupCode,
            ref string Error,
            ref List<int> lstError)
        {
            IVTWorksheet sheetDefault = oBook.GetSheet(1);
            #region Validate du lieu
            string TitleInFile = (string)sheetDefault.GetCellValue("A4");
            string Title1 = string.Empty;
            string Title2 = string.Empty;
            var titleExam = sheetDefault.GetCellValue("A2");
            var titleExamGroup = sheetDefault.GetCellValue("A3");

            if (titleExam != null)
                Title1 = titleExam.ToString();
            if (titleExam != null)
                Title2 = titleExamGroup.ToString();

            if (string.IsNullOrEmpty(Title1) || string.IsNullOrEmpty(Title2) || TitleInFile == null)
            {
                Error = "File excel không đúng định dạng";
            }

            if (!Title1.Contains(examinationName.ToUpper()))
            {
                if (string.IsNullOrEmpty(Error))
                {
                    Error = "File excel không phải của kỳ thi " + examinationName;
                }
            }

            if (!Title2.Contains(examGroupCode.ToUpper()))
            {
                if (string.IsNullOrEmpty(Error))
                {
                    Error = "File excel không phải của nhóm thi " + examGroupCode;
                }
            }

            if (!TitleInFile.Equals("DANH SÁCH THÍ SINH"))
            {
                if (string.IsNullOrEmpty(Error))
                {
                    Error = "File excel không đúng định dạng";
                }
            }

            if (!string.IsNullOrEmpty(Error))
            {
                return lstSBD;
            }
            #endregion
            #region lay du lieu tu file
            lstSBD = this.GetData(sheetDefault, ref lstSBD, listResult, ref lstErrorBO, ref Error, ref lstError);

            if (!string.IsNullOrEmpty(Error))
            {
                return lstSBD;
            }
            #endregion

            return lstSBD;
        }
        #endregion

        #region //GetData
        private List<ExamPupil> GetData(
            IVTWorksheet sheet,
            ref List<ExamPupil> lstSBD,
            List<ExamPupilBO> listResultPOC,
            ref List<ListErrorExamBO> lstErrorBO,
            ref string Error,
            ref List<int> lstErrorIndex)
        {
            ExamPupil objExam = null;
            List<string> lstNewPupilCode = new List<string>();
            List<ListErrorExamBO> lstError = new List<ListErrorExamBO>();
            ListErrorExamBO objError = null;
            List<string> lstExamineeNumber = new List<string>();

            ExamPupilBO objPOC = null;
            string pupilCode = string.Empty;
            long pupilID = 0;
            int indexError = 0;
            int startRow = 7;
            int LastDigitSchoolID = _globalInfo.SchoolID.Value % 100;
            //duyệt nội dung danh sách thí sinh
            while (sheet.GetCellValue(startRow, 3) != null && sheet.GetCellValue(startRow, 4) != null)
            {
                objExam = new ExamPupil();
                pupilCode = sheet.GetCellValue(startRow, 3).ToString();
                objPOC = listResultPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    objError = new ListErrorExamBO();
                    objError.PupilName = sheet.GetCellValue(startRow, 4).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = "Mã học sinh không tồn tại";
                    lstError.Add(objError);

                    //objExam.ListErrorMessage = lstError;
                    //lstSBD.Add(objExam);
                    indexError++;
                    startRow++;
                    continue;
                }
                else
                {
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    }

                    pupilID = objPOC.PupilID;
                    lstNewPupilCode.Add(objPOC.PupilCode);
                    if (lstNewPupilCode.Where(p => p == objPOC.PupilCode).Count() > 1)
                    {
                        objError = new ListErrorExamBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 4).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "Mã học sinh bị trùng lập";
                        lstError.Add(objError);
                        indexError++;

                    }
                }

                string newExamineeNumber = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : string.Empty;              

                if (string.IsNullOrEmpty(newExamineeNumber))
                {
                    objError = new ListErrorExamBO();
                    objError.PupilName = sheet.GetCellValue(startRow, 4).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = "Số báo danh không được để trống";
                    lstError.Add(objError);
                    indexError++;

                    startRow++;
                    continue;
                }
                else
                {
                    if (newExamineeNumber.Length > 20)
                    {
                        objError = new ListErrorExamBO();
                        objError.PupilName = sheet.GetCellValue(startRow, 4).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "Số báo danh không được vượt quá 20 ký tự";
                        lstError.Add(objError);
                        indexError++;

                        startRow++;
                        continue;
                    }
                    else
                    {
                       
                        lstExamineeNumber.Add(newExamineeNumber);
                        if (lstExamineeNumber.Where(p => p.ToUpper() == newExamineeNumber.ToUpper()).Count() > 1)
                        {
                            objError = new ListErrorExamBO();
                            objError.PupilName = sheet.GetCellValue(startRow, 4).ToString();
                            objError.PupilCode = pupilCode;
                            objError.Description = "Số báo danh bị trùng lập";
                            lstError.Add(objError);
                            indexError++;

                            startRow++;
                            continue;

                        }
                        else
                        {
                            objExam.PupilID = Int32.Parse(objPOC.PupilID.ToString());
                            objExam.ExamPupilID = objPOC.ExamPupilID;
                            objExam.ExamGroupID = objPOC.ExamGroupID;
                            objExam.ExaminationsID = objPOC.ExaminationsID;
                            objExam.SchoolID = _globalInfo.SchoolID.Value;
                            objExam.LastDigitSchoolID = LastDigitSchoolID;
                            objExam.EducationLevelID = objPOC.EducationLevelID;
                            objExam.CreateTime = objPOC.CreateTime;
                            objExam.UpdateTime = DateTime.Now;
                            objExam.ExamRoomID = objPOC.ExamRoomID;
                            objExam.ExamineeNumber = newExamineeNumber;
                        }
                    }
                }

                lstSBD.Add(objExam);
                startRow++;
            }

            if (indexError != 0)
            {
                lstErrorIndex.Add(indexError);
            }

            lstErrorBO = lstError;

            return lstSBD;
        }
        #endregion

        #endregion
    }
}
