﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamineeCodeNameArea
{
    public class ExamineeCodeNameConstants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";
        public const string BTN_COMMON_ENABLE = "BtnCommonEnable";
        public const string IS_CHECK_ROOM = "IsCheckRoom";
        public const string IS_CHOOSE = "IsChoose";

        public const string LIST_EXAM_PUPIL = "ListExamPupil";
        public const string LIST_EXAMINATIONS = "ListExaminations";
        public const string LIST_EXAM_GROUP = "ListExamGroup";

        public const string GROUP_INFO = "GroupInfo";
        public const string TOTAL_EXAM_PUPIL = "TotalExamPupil";
        public const string LIST_CRITERIA = "ListCriteria";

        public const string MAX_LENGTH_GROUP = "MaxLengthGroup";
        public const string MAX_LENGTH_EXAM = "MaxLengthExam";

        public const string IS_CHECK_CODE_GROUP = "IsCheckCodeGroup";
        public const string IS_CHECK_CODE_EXAM = "IsCheckCodeExam";

        public const int PAGE_SIZE = 20;
        public const string TOTAL = "Total";
        public const string PAGE = "Page";

        public const string PHISICAL_PATH = "PhisicalPath";
        public const string LIST_ERROR_IMPORT = "ListError";
        
    }
}