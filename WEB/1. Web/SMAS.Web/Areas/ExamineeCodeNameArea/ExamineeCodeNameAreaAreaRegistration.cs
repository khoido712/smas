﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamineeCodeNameArea
{
    public class ExamineeCodeNameAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamineeCodeNameArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamineeCodeNameArea_default",
                "ExamineeCodeNameArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
