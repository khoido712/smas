﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HistorySMSArea
{
    public class HistorySMSAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HistorySMSArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HistorySMSArea_default",
                "HistorySMSArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
