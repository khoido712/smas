/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.HistorySMSArea
{
    public class HistorySMSConstants
    {
        public const string LIST_HISTORYSMS = "listHistorySMS";
        public const string LIST_STATUS = "listStatus";
        public const string LIST_ACADEMIC_YEAR = "listAcademicYear";
        public const string LIST_SMS_TYPE = "listSMSType";
        public const string LIST_RECEIVE_TYPE = "listReceiveType";

        public const string SCHOOL_NAME = "SchoolName";
    }
}