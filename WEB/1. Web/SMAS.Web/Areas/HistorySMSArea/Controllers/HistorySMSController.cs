﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.HistorySMSArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.HistorySMSArea.Controllers
{
    public class HistorySMSController : BaseController
    {        
        private readonly IHistorySMSBusiness HistorySMSBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
		
		public HistorySMSController (IHistorySMSBusiness historysmsBusiness,
            IAcademicYearBusiness AcademicYearBusiness)
		{
			this.HistorySMSBusiness = historysmsBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
		}
		
		//
        // GET: /HistorySMS/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

		//
        // GET: /HistorySMS/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            //add search info
            SearchInfo["SchoolID"] = global.SchoolID.Value;
            SearchInfo["Year"] = frm.Year;
            SearchInfo["Mobile"] = frm.Mobile;
            SearchInfo["TypeID"] = frm.TypeID;
            SearchInfo["ReceiveType"] = frm.ReceiveType;
            SearchInfo["Status"] = frm.Status;
            SearchInfo["FromDate"] = frm.FromDate;
            SearchInfo["ToDate"] = frm.ToDate;

            IEnumerable<HistorySMSViewModel> lst = this._Search(SearchInfo);
            ViewData[HistorySMSConstants.LIST_HISTORYSMS] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            HistorySMS historysms = new HistorySMS();
            TryUpdateModel(historysms); 
            Utils.Utils.TrimObject(historysms);

            this.HistorySMSBusiness.Insert(historysms);
            this.HistorySMSBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int HistorySMSID)
        {
            HistorySMS historysms = this.HistorySMSBusiness.Find(HistorySMSID);
            TryUpdateModel(historysms);
            Utils.Utils.TrimObject(historysms);
            this.HistorySMSBusiness.Update(historysms);
            this.HistorySMSBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.HistorySMSBusiness.Delete(id);
            this.HistorySMSBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<HistorySMSViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<HistorySMS> query = this.HistorySMSBusiness.Search(SearchInfo);
            IQueryable<HistorySMSViewModel> lst = query.Select(o => new HistorySMSViewModel {               
						HistorySMSID = o.HistorySMSID,								
						Mobile = o.Mobile,								
						FullName = o.FullName,								
						TypeID = o.TypeID,								
						ReceiveType = o.ReceiveType,								
						CreateDate = o.CreateDate,								
						SchoolID = o.SchoolID,								
						Year = o.Year,								
						Content = o.Content,								
						Status = o.Status,								
						Sender = o.Sender,								
						FlagID = o.FlagID								
					
				
            });

            List<HistorySMSViewModel> lsHistorySMSViewModel = lst.ToList();
            foreach (HistorySMSViewModel item in lsHistorySMSViewModel)
            {
                if (item.TypeID.HasValue)
                {
                    if (item.TypeID == SystemParamsInFile.SMS_TYPE_FOR_PARENT)
                    {
                        item.SMSType = Res.Get("Common_Label_SMS_ForParent");
                    }
                    else if (item.TypeID == SystemParamsInFile.SMS_TYPE_FOR_TEACHER)
                    {
                        item.SMSType = Res.Get("Common_Label_SMS_ForTeacher");
                    }
                    else if (item.TypeID == SystemParamsInFile.SMS_TYPE_CAPACITY)
                    {
                        item.SMSType = Res.Get("Common_Label_SMS_Capacity");
                    }
                    else if (item.TypeID == SystemParamsInFile.SMS_TYPE_CONDUCT)
                    {
                        item.SMSType = Res.Get("Common_Label_SMS_Conduct");
                    }
                }

                if (item.ReceiveType.HasValue)
                {
                    if (item.ReceiveType == SystemParamsInFile.HISTORY_RECEIVE_TYPE_PUPIL)
                    {
                        item.SMSReceiverType = Res.Get("Common_Label_SMS_Receiver_Pupil");
                    }
                    else if (item.ReceiveType == SystemParamsInFile.HISTORY_RECEIVE_TYPE_PARENTS)
                    {
                        item.SMSReceiverType = Res.Get("Common_Label_SMS_Receiver_Parent");
                    }
                    else if (item.ReceiveType == SystemParamsInFile.HISTORY_RECEIVE_TYPE_EMPLOYEE)
                    {
                        item.SMSReceiverType = Res.Get("Common_Label_SMS_Receiver_Teacher");
                    }
                }

                if (item.Status.HasValue)
                {
                    if (item.Status.Value)
                    {
                        item.SMSStatus = Res.Get("Common_Label_SMS_Sending_Succeeded");
                    }
                    else
                    {
                        item.SMSStatus = Res.Get("Common_Label_SMS_Sending_Failed");
                    }
                }
            }

            return lsHistorySMSViewModel;
        }

        #region setviewdata

        public void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            GlobalInfo global = new GlobalInfo();
            
            SearchInfo["SchoolID"] = global.SchoolID.Value;
            IEnumerable<HistorySMSViewModel> lst = this._Search(SearchInfo);
            ViewData[HistorySMSConstants.LIST_HISTORYSMS] = lst;
            ViewData[HistorySMSConstants.SCHOOL_NAME] = global.SchoolName;

            List<EducationLevel> lsEducationLevel = global.EducationLevels;
            IQueryable<AcademicYear> lsAcademicYear = AcademicYearBusiness.SearchBySchool(global.SchoolID.Value);

            ViewData[HistorySMSConstants.LIST_ACADEMIC_YEAR] = new SelectList(lsAcademicYear, "Year", "DisplayTitle");
            ViewData[HistorySMSConstants.LIST_SMS_TYPE] = new SelectList(CommonList.SMSType(), "key", "value");
            ViewData[HistorySMSConstants.LIST_RECEIVE_TYPE] = new SelectList(CommonList.ReceiveType(), "key", "value");
            ViewData[HistorySMSConstants.LIST_STATUS] = new SelectList(CommonList.SMSStatus(), "key", "value");
        }

        #endregion setviewdata
    }
}





