/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.HistorySMSArea.Models
{
    public class HistorySMSViewModel
    {
		public System.Int64 HistorySMSID { get; set; }

        [ResourceDisplayName("HistorySMS_Label_MobileReceive")]			
		public System.String Mobile { get; set; }

        [ResourceDisplayName("HistorySMS_Label_Receiver")]
		public System.String FullName { get; set; }								
		public System.Nullable<System.Int32> TypeID { get; set; }								
		public System.Nullable<System.Int32> ReceiveType { get; set; }

        [ResourceDisplayName("HistorySMS_Label_CreateDate")]
        public System.Nullable<System.DateTime> CreateDate { get; set; }	
							
		public System.Int32 SchoolID { get; set; }								
		public System.Int32 Year { get; set; }								
		public System.String Content { get; set; }								
		public System.Nullable<System.Boolean> Status { get; set; }

        [ResourceDisplayName("HistorySMS_Label_Sender")]			
		public System.String Sender { get; set; }								
		public System.Nullable<System.Int32> FlagID { get; set; }

        [ResourceDisplayName("HistorySMS_Label_SMSType")]
        public System.String SMSType { get; set; }

        [ResourceDisplayName("HistorySMS_Label_ReceiveType")]
        public System.String SMSReceiverType { get; set; }

        [ResourceDisplayName("HistorySMS_Label_Status")]
        public System.String SMSStatus { get; set; }	
	       
    }
}


