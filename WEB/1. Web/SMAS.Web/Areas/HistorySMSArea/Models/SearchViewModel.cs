/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.HistorySMSArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("HistorySMS_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HistorySMSConstants.LIST_ACADEMIC_YEAR)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Year { get; set; }

        [ResourceDisplayName("HistorySMS_Label_MobileReceive")]
        [UIHint("Textbox")]
        public string Mobile { get; set; }

        [ResourceDisplayName("HistorySMS_Label_SMSType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HistorySMSConstants.LIST_SMS_TYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? TypeID { get; set; }

        [ResourceDisplayName("HistorySMS_Label_ReceiveType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HistorySMSConstants.LIST_RECEIVE_TYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? ReceiveType { get; set; }

        [ResourceDisplayName("HistorySMS_Label_Status")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HistorySMSConstants.LIST_STATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Status { get; set; }

        [ResourceDisplayName("Message_Label_FromDate")]
        [UIHint("DateTimePicker")]
        public DateTime? FromDate { get; set; }

        [ResourceDisplayName("Message_Label_ToDate")]
        [UIHint("DateTimePicker")]
        public DateTime? ToDate { get; set; }
    }
}