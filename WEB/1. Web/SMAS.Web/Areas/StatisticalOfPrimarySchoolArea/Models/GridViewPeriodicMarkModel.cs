﻿using System.Collections.Generic;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.StatisticalOfPrimarySchoolArea.Models
{
    public class GridViewPeriodicMarkModel
    {
        public int SchoolID { get; set; }

        public int DistrictID { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_School")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_District")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_TotalSchool")]
        public int TotalSchool { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_TotalTest")]
        public int TotalTest { get; set; }

        public List<int> MarkAll { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_BelowAverage")]
        public int BelowAverage { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_PercentBelowAverage")]
        public double PercentBelowAverage { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_AboveAverage")]
        public int AboveAverage { get; set; }

        [ResourceDisplayName("StatisticalPeriodicMarkOfPrimarySchool_Label_PercentAboveAverage")]
        public double PercentAboveAverage { get; set; }
    }
}