﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.StatisticalOfPrimarySchoolArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("StatisticalOfPrimarySchool_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalOfPrimarySchoolContant.CBO_ACADEMIC_YEAR)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "onYearChange()")]
        public int? Year { get; set; }

        [ResourceDisplayName("SStatisticalOfPrimarySchool_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalOfPrimarySchoolContant.CBO_SEMESTER)]
        [AdditionalMetadata("Placeholder", "null")]
        public int? Semester { get; set; }

        [ResourceDisplayName("StatisticalOfPrimarySchool_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalOfPrimarySchoolContant.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("StatisticalOfPrimarySchool_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalOfPrimarySchoolContant.CBO_SUBJECT_ID)]
        [AdditionalMetadata("Placeholder", "Choice")]
        public int? SubjectID { get; set; }

        [ResourceDisplayName("StatisticalOfPrimarySchool_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticalOfPrimarySchoolContant.CBO_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? DistrictID { get; set; }

        public bool IsFemale { get; set; }
        public bool IsEthnic { get; set; }
        public bool IsFemaleEthnic { get; set; }
        public int ReportType { get; set; }

    }
}