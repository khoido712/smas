﻿using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.StatisticalOfPrimarySchoolArea.Models
{
    public class GridViewJudgeRecordModel
    {
        public int SchoolID { get; set; }

        public int DistrictID { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_School")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_District")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_TotalSchool")]
        public int TotalSchool { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_TotalPupil")]
        public int TotalPupil { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_TotalAPlus")]
        public int TotalAPlus { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_PercentAPlus")]
        public double PercentAPlus { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_TotalA")]
        public int TotalA { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_PercentA")]
        public double PercentA { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_TotalB")]
        public int TotalB { get; set; }

        [ResourceDisplayName("StatisticalJudgeRecordOfPrimarySchool_Label_PercentB")]
        public double PercentB { get; set; }
    }
}