﻿using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.StatisticalOfPrimarySchoolArea.Models
{
    public class GridViewMarkRecordModel
    {
        public int SchoolID { get; set; }

        public int DistrictID { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_School")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_District")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalSchool")]
        public int TotalSchool { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalPupil")]
        public int TotalPupil { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalExcellent")]
        public int TotalExcellent { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_PercentExcellent")]
        public double PercentExcellent { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalGood")]
        public int TotalGood { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_PercentGood")]
        public double PercentGood { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalNormal")]
        public int TotalNormal { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_PercentNormal")]
        public double PercentNormal { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalWeak")]
        public int TotalWeak { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_PercentWeak")]
        public double PercentWeak { get; set; }
    }
}