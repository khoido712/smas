﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticalOfPrimarySchoolArea
{
    public class StatisticalOfPrimarySchoolContant
    {
        public const string CBO_ACADEMIC_YEAR = "CBO_ACADEMIC_YEAR";
        public const string CBO_SEMESTER = "CBO_SEMESTER";
        public const string CBO_EDUCATION_LEVEL = "CBO_EDUCATION_LEVEL";
        public const string CBO_SUBJECT_ID = "CBO_SUBJECT_ID";
        public const string CBO_DISTRICT = "CBO_DISTRICT";


    }
}