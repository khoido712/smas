﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticalOfPrimarySchoolArea
{
    public class StatisticalOfPrimarySchoolAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticalOfPrimarySchoolArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticalOfPrimarySchoolArea_default",
                "StatisticalOfPrimarySchoolArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
