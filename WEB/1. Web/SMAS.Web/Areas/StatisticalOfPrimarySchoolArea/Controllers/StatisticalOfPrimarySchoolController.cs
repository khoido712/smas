﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.StatisticalOfPrimarySchoolArea.Models;
using SMAS.Web.Utils;
using SMAS.Web.Areas.StatisticalOfPrimarySchoolArea;
using System.Configuration;
using System;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.StatisticalOfPrimarySchoolArea.Controllers
{
    public class StatisticalOfPrimarySchoolController : BaseController
    {
        #region properties
        private readonly IEducationLevelBusiness EducationLevelBusiness ;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IProvinceSubjectBusiness ProvinceSubjectBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        #endregion

        #region Constructor
        public StatisticalOfPrimarySchoolController(IEducationLevelBusiness EducationLevelBusiness, 
            ISchoolProfileBusiness SchoolProfileBusiness, 
            IAcademicYearBusiness AcademicYearBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness,
            IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness,
            IProvinceSubjectBusiness ProvinceSubjectBusiness,
            IDistrictBusiness DistrictBusiness,
            IMarkStatisticBusiness MarkStatisticBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.ProvinceSubjectBusiness = ProvinceSubjectBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.MarkStatisticBusiness = MarkStatisticBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }
        #endregion

        #region Actions
       
        public ActionResult Index()
        {
            SetViewData();

            return View();
        }

        [HttpPost]
        public JsonResult AjaxLoadSubject(int year, int educationLevel)
        {

            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();

            MarkStatisticsearchInfo["Year"] = year;
            MarkStatisticsearchInfo["EducationLevelID"] = educationLevel;
            MarkStatisticsearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            MarkStatisticsearchInfo["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;
            MarkStatisticsearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            //MarkStatisticsearchInfo["IsCommenting"] = 0;
            List<SubjectCatBO> ListSubject = SupervisingDeptBusiness.SearchSubject(MarkStatisticsearchInfo);

            return Json(new SelectList(ListSubject, "SubjectCatID", "DisplayName"), JsonRequestBehavior.AllowGet);

        }

        [ValidateAntiForgeryToken]
        public JsonResult CreatedReport(int reportType, int year, int semester, int educationLevel, int subjectId, int? districtId, bool isFemale, bool isEthnic, bool isFemaleEthnic)
        {
            int semesterID = 0;
            if (reportType == 2 || reportType == 3)
            {
                if (semester == 1 || semester == 3)
                {
                    semesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                }
                else if (semester == 2 || semester == 4)
                {
                    semesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                }
            }
            else
            {
                semesterID = GlobalConstants.EVALUATION_RESULT;
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            SearchInfo["Year"] = year;
            SearchInfo["MarkType"] = reportType;
            SearchInfo["Semester"] = semesterID;
            SearchInfo["SubjectID"] = subjectId;
            SearchInfo["EducationLevelID"] = educationLevel;
            SearchInfo["DistrictID"] = districtId;
            SearchInfo["IsFemale"] = isFemale;
            SearchInfo["IsEthnic"] = isEthnic;
            SearchInfo["IsFemaleEthnic"] = isFemaleEthnic;
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            SearchInfo["UnitId"] = _globalInfo.SupervisingDeptID;
            SearchInfo["ReportTypeID"] = semester;

            int superVisingDeptId = 0;
            if (!_globalInfo.IsSuperVisingDeptRole)
            {
                SearchInfo["SuperVisingDeptID"] = 1;
                SearchInfo["DistrictID"] = _globalInfo.DistrictID;
                superVisingDeptId = 1;
            }
            else
            {
                SearchInfo["SuperVisingDeptID"] = 0;
                superVisingDeptId = 0;
            }
            string reportCode = GetReportCode(superVisingDeptId, reportType);
            SearchInfo["ReportCode"] = reportCode;

            MarkStatisticBusiness.CreatePrimaryStatistic(SearchInfo);
            return Json(new { Type = "success" });
        }

        public FileResult ExportReport(int reportType, int year, int semester, int educationLevel, int subjectId, int? districtId, bool isFemale, bool isEthnic, bool isFemaleEthnic)
        {
           
            GlobalInfo global = GlobalInfo.getInstance();
            int semesterID = 0;
            if (reportType == 2 || reportType == 3)
            {
                if (semester == 1 || semester == 3)
                {
                    semesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                }
                else if (semester == 2 || semester == 4)
                {
                    semesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                }
            }
            else
            {
                semesterID = GlobalConstants.EVALUATION_RESULT;
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = global.ProvinceID;
            SearchInfo["MarkType"] = semester;
            SearchInfo["Semester"] = semesterID;
            SearchInfo["SubjectID"] = subjectId;
            SearchInfo["EducationLevelID"] = educationLevel;
            SearchInfo["DistrictID"] = districtId;
            SearchInfo["IsFemale"] = isFemale;
            SearchInfo["IsEthnic"] = isEthnic;
            SearchInfo["IsFemaleEthnic"] = isFemaleEthnic;
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            SearchInfo["ReportType"] = reportType;
            SearchInfo["UnitId"] = global.SupervisingDeptID;
            SearchInfo["Year"] = year;
            int superVisingDeptId = 0;
            if (!global.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                SearchInfo["SuperVisingDeptID"] = 1;
                SearchInfo["DistrictID"] = _globalInfo.DistrictID;
                superVisingDeptId = 1;
            }
            else
            {
                //so
                SearchInfo["SuperVisingDeptID"] = 0;
                superVisingDeptId = 0;
            }
            string reportCode = GetReportCode(superVisingDeptId, reportType);
            SearchInfo["ReportCode"] = reportCode;
            
            Stream excel = MarkStatisticBusiness.ExportPrimaryStatistic(SearchInfo);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SubjectCat sc = SubjectCatBusiness.Find(subjectId);
            string strSemester = string.Empty;
            if (semester == 1 || semester == 2)
            {
                strSemester = "Giua" + (semesterID == 1 ? "HKI" : "HKII");
            }
            else 
            {
                strSemester = "Cuoi" + (semesterID == 3 ? "HKI" : "HKII");
            }
            string fileName = string.Empty;
            if (reportType == 2)
            {
                fileName = reportDef.OutputNamePattern.Replace("[SemesterName]", strSemester).Replace("[Subject]", ReportUtils.StripVNSign(sc.DisplayName));    
            }
            else if (reportType == 3)
            {
                fileName = reportDef.OutputNamePattern.Replace("[SemesterName]", strSemester);
            }
            else if (reportType == 4)
            {
                fileName = reportDef.OutputNamePattern;
            }
            result.FileDownloadName = fileName + ".xls";
            return result;
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            List<int> lstAca = this.AcademicYearBusiness.GetListYearForSupervisingDept_THCS(_globalInfo.SupervisingDeptID.Value);
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            for (int i = 0; i < lstAca.Count(); i++)
            {
                AcademicYear AcademicYear = new AcademicYear();
                AcademicYear.Year = lstAca[i];
                AcademicYear.DisplayTitle = lstAca[i] + "-" + (lstAca[i] + 1);
                lstAcademicYear.Add(AcademicYear);
            }
            List<AcademicYear> ListAcademicYear = lstAcademicYear.ToList();
            // Lay nam hoc hien tai
            string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
            int curSemester = 1;
            int curYear = AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(_globalInfo.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);
            ViewData[StatisticalOfPrimarySchoolContant.CBO_ACADEMIC_YEAR] = new SelectList(ListAcademicYear, "Year", "DisplayTitle", curYear);

            ViewData[StatisticalOfPrimarySchoolContant.CBO_SEMESTER] = new SelectList(CommonList.Semester_GeneralSummed(), "key", "value", curSemester);

            //Khoi
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(1).ToList();
            ViewData[StatisticalOfPrimarySchoolContant.CBO_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            //Mon hoc
            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();

            MarkStatisticsearchInfo["Year"] = curYear;
            MarkStatisticsearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_FIRST;
            MarkStatisticsearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            MarkStatisticsearchInfo["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;
            MarkStatisticsearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            //MarkStatisticsearchInfo["IsCommenting"] = 0;
            List<SubjectCatBO> ListSubject = SupervisingDeptBusiness.SearchSubject(MarkStatisticsearchInfo);
            ViewData[StatisticalOfPrimarySchoolContant.CBO_SUBJECT_ID] = new SelectList(ListSubject, "SubjectCatID", "DisplayName");
            

            //Quan huyen
            List<District> lstDistrict = new List<District>();
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                lstDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == _globalInfo.ProvinceID).ToList();
                ViewData[StatisticalOfPrimarySchoolContant.CBO_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName");
            }
            else if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                lstDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == _globalInfo.ProvinceID
                    && o.DistrictID == _globalInfo.DistrictID).ToList();
                ViewData[StatisticalOfPrimarySchoolContant.CBO_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName", _globalInfo.DistrictID);
            }

            
        }

        private string GetReportCode(int supervisingDeptID, int reportType)
        {
            if (reportType == 1)
            {

                if (supervisingDeptID == 0)
                {
                    return SystemParamsInFile.REPORT_SGD_ThongKeTieuHoc;
                }
                else
                {
                    return SystemParamsInFile.REPORT_PGD_ThongKeTieuHoc;
                }
            }
            else if (reportType == 2)//Mon hoc va HDGD
            {
                return SystemParamsInFile.REPORT_SGD_SUBJECT_AND_EDUCATION_GROUP;
            }
            else if (reportType == 3)//Nang luc pham chat
            {
                return SystemParamsInFile.REPORT_SGD_CAPACITY_QUALITY;
            }
            else if (reportType == 4)//Ket qua cuoi nam
            {
                return SystemParamsInFile.REPORT_SGD_SUMMED_ENDING_EVALUATION;
            }
            return "";
        }
        #endregion
    }
}
