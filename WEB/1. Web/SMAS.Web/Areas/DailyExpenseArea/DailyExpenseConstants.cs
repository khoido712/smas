/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.DailyExpenseArea
{
    public class DailyExpenseConstants
    {
        public const string LIST_DAILYEXPENSE = "listDailyExpense";
        public const string LIST_YEAR = "listYear";
        public const string LIST_MONTH = "listMonth";
        public const string LIST_DATECALMODEL = "LIST_DATECALMODEL";
        //da tinh
        public const int STATUS_OK = 1;
        //de trang
        public const int STATUS_WHITE = 0;
        //tinh
        public const int STATUS_CAL = 2;

        public const string LIST_DISHINSPECTION = "listDishInspection";
        public const string LIST_EATINGGROUP = "listEatingGroup";
        public const string LIST_OTHERSERVICE = "listOtherService";
        public const string MAX_PRICE = "maxPrice";
        public const string NUMBER_CHILD = "numberOfChildren";
        public const string COUNT_FOOD_MONEY = "countFoodMoney";
        public const string COUNT_SERVICE_MONEY = "countServiceMoney";
        public const string NUMBERMODSTARTDAY = "NUMBERMODSTARTDAY";

        public const string CHECK_STATUS = "CHECK_STATUS";
        public const string STATUS_BUTTON_NUMBERMOD = "STATUS_BUTTON_NUMBERMOD";
        public const string STATUS_BUTTON_MONTHLOCK = "STATUS_BUTTON_MONTHLOCK";
        public const string STATUS_BUTTON_EXPORT = "STATUS_BUTTON_EXPORT";

    }
}