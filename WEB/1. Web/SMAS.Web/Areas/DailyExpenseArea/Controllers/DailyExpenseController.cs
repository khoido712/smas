﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DailyExpenseArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Web.Areas.DailyExpenseArea.Controllers
{
    public class DailyExpenseController : BaseController
    {
        private readonly IDailyExpenseBusiness DailyExpenseBusiness;
        private readonly IDishInspectionBusiness DishInspectionBusiness;
        private readonly IMonthlyLockBusiness MonthlyLockBusiness;
        private readonly IEatingGroupBusiness EatingGroupBusiness;
        private readonly IDailyDishCostBusiness DailyDishCostBusiness;
        private readonly IMonthlyBalanceBusiness MonthlyBalanceBusiness;
        private int startYear = 1900;
        private int endYear = 2200;
        private readonly GlobalInfo globalInfo = new GlobalInfo();

        public DailyExpenseController(IDailyExpenseBusiness dailyexpenseBusiness, IDishInspectionBusiness DishInspectionBusiness, IMonthlyLockBusiness MonthlyLockBusiness, IEatingGroupBusiness eatingGroupBusiness,
            IDailyDishCostBusiness dailyDishCostBusiness, IMonthlyBalanceBusiness MonthlyBalanceBusiness)
        {
            this.DailyExpenseBusiness = dailyexpenseBusiness;
            this.DishInspectionBusiness = DishInspectionBusiness;
            this.MonthlyLockBusiness = MonthlyLockBusiness;
            this.EatingGroupBusiness = eatingGroupBusiness;
            this.DailyDishCostBusiness = dailyDishCostBusiness;
            this.MonthlyBalanceBusiness = MonthlyBalanceBusiness;
        }

        //
        // GET: /DailyExpense/

        public ActionResult Index()
        {
            int schoolID = _globalInfo.SchoolID.Value;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            MonthPickerPanel();
            LoadgrdDateCalculator(DateTime.Now.Month, DateTime.Now.Year);
            //SetViewData(DateTime.Now.Date);
            ViewData[DailyExpenseConstants.CHECK_STATUS] = false;

            DateTime startMonth = Business.Common.Utils.StartOfMonth(DateTime.Now);
            DateTime endMonth = Business.Common.Utils.EndOfMonth(startMonth);

            ViewData[DailyExpenseConstants.STATUS_BUTTON_NUMBERMOD] = "disabled";
            ViewData[DailyExpenseConstants.STATUS_BUTTON_MONTHLOCK] = "disabled";
            ViewData[DailyExpenseConstants.STATUS_BUTTON_EXPORT] = "disabled";

            if (DailyExpenseBusiness.CheckExpenseDate(schoolID, startMonth, endMonth))
                ViewData[DailyExpenseConstants.STATUS_BUTTON_NUMBERMOD] = "";
            if (!DailyExpenseBusiness.CheckExpenseDate(schoolID, startMonth, endMonth) && !MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, startMonth, endMonth))
                ViewData[DailyExpenseConstants.STATUS_BUTTON_MONTHLOCK] = "";

            

            return View();
        }

        //
        // GET: /DailyExpense/Search

        /// <summary>
        /// MonthPickerPanel
        /// </summary>
        ///<author>Nam ta</author>
        ///<Date>11/17/2012</Date>
        public void MonthPickerPanel()
        {

            //Get view data here

            #region  TÌM CÁC NĂM CÓ NGÀY ĐÃ TÍNH TIỀN ĐỂ ĐIỀN VÀO COMBOBOX
            int schoolID = _globalInfo.SchoolID.Value;
            // Lấy tất cả các ngày đã được tính tiền
            IEnumerable<DishInspection> lstDishInspection = DishInspectionBusiness.GetInspectedDate(schoolID, new DateTime(1990, 1, 1), DateTime.Now);
            // Chọn ra các năm từ danh sách các ngày tính tiền
            IEnumerable<int> lstYear = lstDishInspection.Select(p => p.CreatedDate.Year).Distinct();
            // Tạo danh sách để đưa vào View
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            foreach (int year in lstYear)
            {
                if (year == DateTime.Now.Year)
                    lstSelectListItem.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString(), Selected = true });
                else
                    lstSelectListItem.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString(), Selected = false });
            }
            ViewData[DailyExpenseConstants.LIST_YEAR] = lstSelectListItem;
            #endregion

            #region LẤY CÁC THÁNG TRONG NĂM
            List<SelectListItem> ListMonth = new List<SelectListItem>();
            for (int month = 1; month <= 12; month++)
            {
                if (month == DateTime.Now.Month)
                    ListMonth.Add(new SelectListItem { Text = month.ToString(), Value = month.ToString(), Selected = true });
                else
                    ListMonth.Add(new SelectListItem { Text = month.ToString(), Value = month.ToString(), Selected = false });
            }
            ViewData[DailyExpenseConstants.LIST_MONTH] = ListMonth;
            #endregion
            ViewData[DailyExpenseConstants.STATUS_BUTTON_NUMBERMOD] = "disabled";
            ViewData[DailyExpenseConstants.STATUS_BUTTON_MONTHLOCK] = "disabled";
            ViewData[DailyExpenseConstants.STATUS_BUTTON_EXPORT] = "disabled";
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult ChangeMonthYearPicker(int? month, int? year)
        {
            if (month == null || year == null)
                return null;
            else
                LoadgrdDateCalculator(month.Value, year.Value);
            return PartialView("_DateCalGrid");
        }


        [HttpPost]
        public PartialViewResult showDte()
        {
            String date = Request["dte"];
            DateTime dt = new DateTime();
            bool test = DateTime.TryParse(date,out dt);

            if (test )
            {
                SetViewData(dt);
                ViewData[DailyExpenseConstants.CHECK_STATUS] = true;
            }

            return PartialView("_ListAll");
        }

        [HttpPost]
        public PartialViewResult calByDte()
        {
            String date = Request["date"];
            DateTime dt = new DateTime();
            bool test = DateTime.TryParse(date, out dt);

            if (test)
            {
                SetViewData(dt);
                ViewData[DailyExpenseConstants.CHECK_STATUS] = true;
            }

            return PartialView("_ListAll");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(string dte)
        {
            String date = dte;
            DateTime dt = new DateTime();
            bool test = DateTime.TryParse(date, out dt);

         
            DateTime startMonth = Business.Common.Utils.StartOfMonth(dt);
            DateTime endMonth = Business.Common.Utils.EndOfMonth(startMonth);
            if (MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, startMonth, endMonth))
            {
                return Json(new JsonMessage(Res.Get("DailyExpense_Label_ErrorDelete"), "error"));
            }
            else
            {
                ViewData[DailyExpenseConstants.CHECK_STATUS] = false;
                DailyExpenseBusiness.Delete(new GlobalInfo().SchoolID.Value, dt);
                DailyExpenseBusiness.Save();
                ViewData["CheckDel"] = true;
                return Json(new JsonMessage(Res.Get("DailyExpense_Label_DeleteSuccess") + " "+date));
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult NumberModLoad()
        {
            IDictionary<string,object> dic =new Dictionary<string,object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;

            IEnumerable<DailyExpense> lstDailyExpense  = DailyExpenseBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic);
            int NumberMod = 0;
            if (lstDailyExpense != null && lstDailyExpense.Count() > 0)
            {
                DateTime dt = lstDailyExpense.Max(o => o.ExpenseDate);
                NumberMod = lstDailyExpense.Where(o => o.ExpenseDate == dt).FirstOrDefault().BalanceEnd;
            }
            else
            {
                NumberMod = 0;
            }
            return Json(NumberMod);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult NumberModSave(int? year, int? month, int? balance)
        {


            DateTime dt = new DateTime(year.Value,month.Value,1);
            if (dt.Month == 1)
                dt = new DateTime(dt.Year - 1, 12, 1);
            else
                dt = new DateTime(dt.Year , dt.Month - 1, 1);

            if ( balance!=null)
            {
                IQueryable<MonthlyBalance> listMonthlyBalance = MonthlyBalanceBusiness.All.Where(o => o.BalanceMonth.Month == dt.Month);
                if (listMonthlyBalance != null && listMonthlyBalance.Count() > 0)
                {
                    MonthlyBalanceBusiness.DeleteAll(listMonthlyBalance.ToList());
                    MonthlyBalanceBusiness.Save();
                }

                MonthlyBalance MonthlyBalance = new MonthlyBalance();
                MonthlyBalance.SchoolID = new GlobalInfo().SchoolID.Value;
                
                MonthlyBalance.BalanceMonth = dt;
                MonthlyBalance.Balance = balance.Value;

                MonthlyBalanceBusiness.Insert(MonthlyBalance);
                MonthlyBalanceBusiness.Save();
                return (Json(new JsonMessage(Res.Get("DailyExpense_Label_NewModUpdatesuccess"))));
            }



            return (Json(new JsonMessage(Res.Get("DailyExpense_Label_NotChoiceDate"), "error")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult MonthyLock(int? year, int? month)
        {
            if (year != null && month != null)
            {
                MonthlyLock MonthlyLock = new MonthlyLock();
                MonthlyLock.SchoolID = new GlobalInfo().SchoolID.Value;
                MonthlyLock.LockedMonth = new DateTime(year.Value, month.Value, 1);
                MonthlyLockBusiness.Insert(MonthlyLock);
                MonthlyLockBusiness.Save();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;

                IEnumerable<DailyExpense> lstDailyExpense = DailyExpenseBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic);
                int NumberMod = 0;
                if (lstDailyExpense != null)
                {
                    MonthlyBalance MonthlyBalance = new MonthlyBalance();
                    DateTime dt = lstDailyExpense.Max(o => o.ExpenseDate);
                    NumberMod = lstDailyExpense.Where(o => o.ExpenseDate == dt).FirstOrDefault().BalanceEnd;
                    dt = new DateTime(year.Value, month.Value, 1);

                    IQueryable<MonthlyBalance> listMonthlyBalance = MonthlyBalanceBusiness.All.Where(o => o.BalanceMonth.Month == dt.Month);
                    if (listMonthlyBalance != null && listMonthlyBalance.Count() > 0)
                    {
                        MonthlyBalanceBusiness.DeleteAll(listMonthlyBalance.ToList());
                    }

                    MonthlyBalance.SchoolID = new GlobalInfo().SchoolID.Value;
                    MonthlyBalance.BalanceMonth  = dt;
                    MonthlyBalance.Balance = NumberMod;
                    MonthlyBalanceBusiness.Insert(MonthlyBalance);
                    MonthlyBalanceBusiness.Save();
                }
            }


            return (Json(new JsonMessage("Common_Label_UpdateSuccessMessage")));
        }
        /// <summary>
        /// LoadgrdDateCalculator
        /// </summary>
        /// <param name="month">The month.</param>
        /// <param name="year">The year.</param>
        ///<author>Nam ta</author>
        ///<Date>11/17/2012</Date>
        public void LoadgrdDateCalculator(int month, int year)
        {
            List<DateCalViewModel> listDateCalViewModel = new List<DateCalViewModel>();
            DateTime dteStartMonth = new DateTime(year, month, 1);
            DateTime dteEndMonth = Business.Common.Utils.EndOfMonth(dteStartMonth);
            DateTime lastStartMonth = new DateTime();
            if (dteStartMonth.Month != 1)
            {
                lastStartMonth = new DateTime(dteStartMonth.Year, dteStartMonth.Month - 1, 1);

            }
            else
            {
                lastStartMonth = new DateTime(dteStartMonth.Year - 1, 12, 1);
            }
            IEnumerable<DishInspection> lstDishInspection = DishInspectionBusiness.GetInspectedDate(new GlobalInfo().SchoolID.Value, dteStartMonth, dteEndMonth);
            IEnumerable<DailyExpense> lstDailyExpense = DailyExpenseBusiness.GetExpenseDate(new GlobalInfo().SchoolID.Value, dteStartMonth, dteEndMonth);
            bool lstMonthlyLock = MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, dteStartMonth, dteEndMonth);
            for (DateTime date = dteStartMonth; date <= dteEndMonth; date = date.AddDays(1))
            {
                DateCalViewModel dt = new DateCalViewModel();
                dt.Date = date.ToString("dd/MM/yyyy");

                IEnumerable<DishInspection> lDishInspection = lstDishInspection.Where(o => o.InspectedDate == date);
                if (lDishInspection != null && lDishInspection.Count() > 0)
                {
                    dt.DishInspection = DailyExpenseConstants.STATUS_OK;
                }
                else
                {
                    dt.DishInspection = DailyExpenseConstants.STATUS_WHITE;
                }

                IEnumerable<DailyExpense> lDailyExpense = lstDailyExpense.Where(o => o.ExpenseDate == date);
                if (lDailyExpense != null && lDailyExpense.Count() > 0)
                {
                    dt.DailyExpense = DailyExpenseConstants.STATUS_OK;
                }
                else if ((dt.DishInspection == DailyExpenseConstants.STATUS_OK) && (!MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, dteStartMonth, dteEndMonth)) )
                {
                    dt.DailyExpense = DailyExpenseConstants.STATUS_CAL;
                }
                else
                {
                    dt.DailyExpense = DailyExpenseConstants.STATUS_WHITE;
                }

                if (dt.DailyExpense == DailyExpenseConstants.STATUS_OK && (!MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, dteStartMonth, dteEndMonth)))
                {
                    dt.Delete = true;
                }
                else dt.Delete = false;

                listDateCalViewModel.Add(dt);
            }


            ViewData[DailyExpenseConstants.STATUS_BUTTON_NUMBERMOD] = "disabled";
            ViewData[DailyExpenseConstants.STATUS_BUTTON_MONTHLOCK] = "disabled";
            ViewData[DailyExpenseConstants.STATUS_BUTTON_EXPORT] = "disabled";
            ViewData[DailyExpenseConstants.LIST_DATECALMODEL] = listDateCalViewModel;
            if (DailyExpenseBusiness.CheckExpenseDate(new GlobalInfo().SchoolID.Value,dteStartMonth,dteEndMonth) )
                ViewData[DailyExpenseConstants.STATUS_BUTTON_NUMBERMOD] = "";
            if (!DailyExpenseBusiness.CheckExpenseDate(new GlobalInfo().SchoolID.Value, dteStartMonth, dteEndMonth) && !MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, dteStartMonth, dteEndMonth))
                ViewData[DailyExpenseConstants.STATUS_BUTTON_MONTHLOCK] = "";
            //ViewData[DailyExpenseConstants.STATUS_BUTTON_EXPORT] = "";
        }


        private void SetViewData(DateTime date)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            int SchoolID = globalInfo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = globalInfo.AcademicYearID.Value;
            IQueryable<EatingGroupBO> ListEatingGroup ;
            ListEatingGroup = EatingGroupBusiness.SearchBySchool(SchoolID, SearchInfo);

            if (ListEatingGroup != null)
            {
                ViewData[DailyExpenseConstants.LIST_EATINGGROUP] = ListEatingGroup.ToList();
            }
            //List<EatingGroupBO> ListEatingGroup = EatingGroupBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();
            //ViewData[DailyExpenseConstants.LIST_EATINGGROUP] = ListEatingGroup;
            // Tinh tong so tre theo truong
            int NumberOfChildren = DishInspectionBusiness.GetNumberOfChildren(SchoolID, date);
            ViewData[DailyExpenseConstants.NUMBER_CHILD] = NumberOfChildren;

            IEnumerable<DishInspectionViewModel> lstDishInspectionViewModel = this._Search(date, NumberOfChildren);
            ViewData[DailyExpenseConstants.LIST_DISHINSPECTION] = lstDishInspectionViewModel;
            // Lay thong tin tien thuc pham
            decimal countFoodMoney = 0;
            foreach (DishInspectionViewModel item in lstDishInspectionViewModel)
            {
                countFoodMoney += item.TotalMoney;
            }
            ViewData[DailyExpenseConstants.COUNT_FOOD_MONEY] = countFoodMoney;

            List<DishInspectionBO> ListOtherService = DishInspectionBusiness.GetListOtherService(SchoolID, date).ToList();
            ViewData[DailyExpenseConstants.LIST_OTHERSERVICE] = ListOtherService;

            // Tien dich vu
            decimal countServiceMoney = 0;
            foreach (DishInspectionBO item in ListOtherService)
            {
                countServiceMoney += item.PriceService * NumberOfChildren;
            }
            ViewData[DailyExpenseConstants.COUNT_SERVICE_MONEY] = countServiceMoney;

            int DailyPrice = DailyDishCostBusiness.MaxDailyDishCost(date, SchoolID);
            ViewData[DailyExpenseConstants.MAX_PRICE] = DailyPrice;


            //-	Số dư đầu ngày

            DateTime startMonth = Business.Common.Utils.StartOfMonth(date);
            DateTime endMonth = Business.Common.Utils.EndOfMonth(startMonth);
            IEnumerable<DailyExpense> lstDailyExpense = DailyExpenseBusiness.GetExpenseDate(new GlobalInfo().SchoolID.Value, startMonth, endMonth);
            IEnumerable<DishInspection> lstDishInspection = DishInspectionBusiness.GetInspectedDate(new GlobalInfo().SchoolID.Value, startMonth, endMonth);
            bool status = DailyExpenseBusiness.CheckExpenseDate(new GlobalInfo().SchoolID.Value, startMonth, endMonth);
            int numberModStartDay = 0;
            //DateTime startDte = date;
            DateTime startLastDte = new DateTime();
            if (date.Month != 1)
            {
                startLastDte = new DateTime(date.Year, date.Month - 1, 1);

            }
            else
            {
                startLastDte = new DateTime(date.Year - 1, 12, 1);
            }
            bool statusMonthLock = MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, startLastDte, Business.Common.Utils.EndOfMonth(startLastDte));

            ///Code phuc vu chuc nang xem

            if (lstDailyExpense != null && lstDailyExpense.Where(o => o.ExpenseDate == date).Count() > 0)
            {

                numberModStartDay = lstDailyExpense.Where(o => o.ExpenseDate == date).FirstOrDefault().BalanceStart;

                ViewData[DailyExpenseConstants.NUMBERMODSTARTDAY] = numberModStartDay;
            }
            else
            {
                // tinh so du dau ngay chuc nang tinh
                if (status)
                {
                    if (!statusMonthLock)
                    {
                        IEnumerable<DailyExpense> lstDailyExpenseLastMonth = DailyExpenseBusiness.GetExpenseDate(new GlobalInfo().SchoolID.Value, startLastDte, Business.Common.Utils.EndOfMonth(startLastDte));
                        IEnumerable<DailyExpense> list = lstDailyExpenseLastMonth.OrderByDescending(o => o.ExpenseDate);
                        if (list != null && list.Count()>0)
                            numberModStartDay = list.FirstOrDefault().BalanceEnd;

                    }
                    else
                    {
                        numberModStartDay = MonthlyBalanceBusiness.GetMonthylyBalance(new GlobalInfo().SchoolID.Value, startLastDte, Business.Common.Utils.EndOfMonth(startLastDte));
                    }
                    

                }
                else{
                    IEnumerable<DailyExpense> list = lstDailyExpense.Where(o => o.ExpenseDate < date).OrderByDescending(o => o.ExpenseDate);
                    if (list == null)
                    {

                        numberModStartDay = MonthlyBalanceBusiness.GetMonthylyBalance(new GlobalInfo().SchoolID.Value, startLastDte, Business.Common.Utils.EndOfMonth(startLastDte));
                    }
                    else if (list.Count() > 0)
                    {
                        numberModStartDay = list.FirstOrDefault().BalanceEnd;
                    }
                }
                

                ViewData[DailyExpenseConstants.NUMBERMODSTARTDAY] = numberModStartDay;
            }
            IEnumerable<DailyExpense> DailyExpenseNow =  lstDailyExpense.Where(o => o.ExpenseDate == date);
            if (DailyExpenseNow == null || DailyExpenseNow.Count()<=0)
            {
                if (((lstDishInspection != null)))
                {
                    if (lstDishInspection.Where(o => o.InspectedDate == date).Count() > 0 )
                    {

                        DailyExpense DailyExpense = new DailyExpense();

                        DailyExpense.SchoolID = new GlobalInfo().SchoolID.Value;
                        DailyExpense.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                        DailyExpense.ExpenseDate = date;
                        DailyExpense.Expense = (int)(countServiceMoney + countFoodMoney);
                        DailyExpense.Receipt = DailyPrice * NumberOfChildren;
                        DailyExpense.BalanceStart = numberModStartDay;
                        DailyExpense.BalanceEnd = DailyExpense.Receipt + numberModStartDay - DailyExpense.Expense;
                        DailyExpenseBusiness.Insert(DailyExpense);
                        DailyExpenseBusiness.Save();
                    }
                }
            }
            if (DailyExpenseBusiness.CheckExpenseDate(new GlobalInfo().SchoolID.Value, startMonth, endMonth))
                ViewData[DailyExpenseConstants.STATUS_BUTTON_NUMBERMOD] = "";
            else ViewData[DailyExpenseConstants.STATUS_BUTTON_NUMBERMOD] = "disabled";
            if (!DailyExpenseBusiness.CheckExpenseDate(new GlobalInfo().SchoolID.Value, startMonth, endMonth) && !MonthlyLockBusiness.CheckMonthlyLock(new GlobalInfo().SchoolID.Value, startMonth, endMonth))
                ViewData[DailyExpenseConstants.STATUS_BUTTON_MONTHLOCK] = "";
            else ViewData[DailyExpenseConstants.STATUS_BUTTON_MONTHLOCK] = "disabled";
            ViewData[DailyExpenseConstants.STATUS_BUTTON_EXPORT] = "";

        }

        public int calNumberModStartDay(int type)
        {
            int numberModStartDay = 0;

            return numberModStartDay;
        }


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DishInspectionViewModel> _Search(DateTime date, int NumberOfChildren)
        {
            int SchoolID = globalInfo.SchoolID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = globalInfo.AcademicYearID.Value;

            List<DishInspectionBO> LstFood = DishInspectionBusiness.GetListFood(SchoolID, date).ToList();

            // Danh sach hien thi tren grid
            List<DishInspectionViewModel> ListDishInspectionViewModel = new List<DishInspectionViewModel>();
            foreach (DishInspectionBO DishInspectionBO in LstFood)
            {
                int FoodID = DishInspectionBO.FoodID;
                DishInspectionViewModel item = ListDishInspectionViewModel.Find(o => o.FoodID == FoodID);
                // Lay thong tin cho danh sach con
                DishInspectionViewModel child = new DishInspectionViewModel();
                child.EatingGroupID = DishInspectionBO.EatingGroupID;
                child.EatingGroupName = DishInspectionBO.EatingGroupName;
                child.MenuType = DishInspectionBO.MenuType;
                child.SumWeight = DishInspectionBO.Weight * DishInspectionBO.NumberOfChildren / 1000;
                child.SumPrice = DishInspectionBO.PricePerOnce;
                child.TotalMoney = DishInspectionBO.PricePerOnce * DishInspectionBO.Weight * DishInspectionBO.NumberOfChildren / 1000;
                List<DishInspectionViewModel> ListChild = new List<DishInspectionViewModel>();
                if (item != null)
                {
                    ListChild = item.ListDishInspectionViewModel;
                    // Tinh tong khoi luong va thanh tien
                    item.SumWeight += child.SumWeight;
                    item.TotalMoney += DishInspectionBO.PricePerOnce * DishInspectionBO.Weight * NumberOfChildren / 1000;
                }
                else
                {
                    // Lay thong tin ve thuc pham
                    item = new DishInspectionViewModel();
                    item.FoodID = DishInspectionBO.FoodID;
                    item.FoodName = DishInspectionBO.FoodName;
                    item.SumWeight = DishInspectionBO.Weight * DishInspectionBO.NumberOfChildren / 1000;
                    item.TotalMoney = DishInspectionBO.PricePerOnce * DishInspectionBO.Weight * NumberOfChildren / 1000;
                    ListDishInspectionViewModel.Add(item);
                }
                ListChild.Add(child);
                item.ListDishInspectionViewModel = ListChild;

            }

            return ListDishInspectionViewModel;
        }


        public FileResult Export(FormCollection col)
        {
            GlobalInfo global = new GlobalInfo();
            DateTime inspectionDate = Convert.ToDateTime(col["datechoice"]);
            Stream excel = DailyExpenseBusiness.CreateReportDailyExpense(global.SchoolID.Value, global.AcademicYearID.Value, inspectionDate);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            string ReportName = "TienCho_ReportDate.xls";

            result.FileDownloadName = ReportName;
            return result;
        }
    }
}





