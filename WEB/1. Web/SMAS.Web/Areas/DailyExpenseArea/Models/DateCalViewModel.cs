﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.DailyExpenseArea.Models
{
    public class DateCalViewModel
    {
        public string Date { get; set; }
        public int DishInspection { get; set; }
        public int DailyExpense { get; set; }
        public bool Delete { get; set; }
    }
}