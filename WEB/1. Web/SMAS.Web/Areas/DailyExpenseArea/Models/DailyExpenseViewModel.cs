/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.DailyExpenseArea.Models
{
    public class DailyExpenseViewModel
    {
		public System.Int32 DailyExpenseID { get; set; }								
		public System.DateTime ExpenseDate { get; set; }								
		public System.Int32 Receipt { get; set; }								
		public System.Int32 Expense { get; set; }								
		public System.Int32 Balance { get; set; }								
		public System.Int32 SchoolID { get; set; }								
		public System.Int32 AcademicYearID { get; set; }								
	       
    }
}


