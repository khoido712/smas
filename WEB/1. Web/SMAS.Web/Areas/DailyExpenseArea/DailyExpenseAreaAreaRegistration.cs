﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DailyExpenseArea
{
    public class DailyExpenseAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DailyExpenseArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DailyExpenseArea_default",
                "DailyExpenseArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
