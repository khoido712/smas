﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ExamSubjectArea.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ExamSubjectArea.Controllers
{
    public class ExamSubjectController : BaseController
    {
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness;
        private readonly IExamBagBusiness ExamBagBusiness;
        private readonly IExamCandenceBagBusiness ExamCandenceBagBusiness;
        private readonly IExamDetachableBagBusiness ExamDetachableBagBusiness;
        private readonly IExamPupilViolateBusiness ExamPupilViolateBusiness;
        private readonly IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness;
        private readonly IExamInputMarkBusiness ExamInputMarkBusiness;
        private readonly IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness;

        public ExamSubjectController(IExaminationsBusiness ExaminationsBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness,
            IExamBagBusiness ExamBagBusiness,
            IExamCandenceBagBusiness ExamCandenceBagBusiness,
            IExamDetachableBagBusiness ExamDetachableBagBusiness,
            IExamPupilViolateBusiness ExamPupilViolateBusiness,
            IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness,
            IExamInputMarkBusiness ExamInputMarkBusiness,
            IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.ExamSupervisoryAssignmentBusiness = ExamSupervisoryAssignmentBusiness;
            this.ExamBagBusiness = ExamBagBusiness;
            this.ExamCandenceBagBusiness = ExamCandenceBagBusiness;
            this.ExamDetachableBagBusiness = ExamDetachableBagBusiness;
            this.ExamPupilViolateBusiness = ExamPupilViolateBusiness;
            this.ExamInputMarkAssignedBusiness = ExamInputMarkAssignedBusiness;
            this.ExamInputMarkBusiness = ExamInputMarkBusiness;
            this.ExamPupilAbsenceBusiness = ExamPupilAbsenceBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetViewData

        public void SetViewData()
        {
            long examinationsID = 0;
            SetViewDataPermission("ExamSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SemesterID", _globalInfo.Semester.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            IDictionary<string, object> searchExamminations = new Dictionary<string, object>();
            searchExamminations.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            searchExamminations.Add("SchoolID", _globalInfo.SchoolID.Value);
            searchExamminations.Add("AppliedLevel", _globalInfo.AppliedLevel);

            ViewData[ExamSubjectConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExamSubjectConstants.CURRENT_SEMESTER] = _globalInfo.Semester;

            IEnumerable<Examinations> listExam = ExaminationsBusiness.Search(searchExamminations).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamSubjectConstants.LIST_EXAMINATIONS] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");
            if (listExam != null && listExam.Count() > 0)
            {
                examinationsID = listExam.FirstOrDefault().ExaminationsID;
                search.Add("ExaminationsID", listExam.FirstOrDefault().ExaminationsID);

                IEnumerable<ExamGroup> examGroupList = GetExamGroupFromExaminations(examinationsID);
                if (examGroupList != null && examGroupList.Count() > 0)
                {
                    ViewData[ExamSubjectConstants.LIST_EXAMGROUP] = new SelectList(examGroupList, "ExamGroupID", "ExamGroupName"); ;
                }
                else
                {
                    ViewData[ExamSubjectConstants.LIST_EXAMGROUP] = new SelectList(new string[] { });
                }

                IEnumerable<ExamSubjectViewModel> listSubject = this._Search(search);
                ViewData[ExamSubjectConstants.LIST_EXAMSUBJECT] = listSubject;
            }
            else
            {
                ViewData[ExamSubjectConstants.LIST_EXAMINATIONS] = new SelectList(new string[] { });
                ViewData[ExamSubjectConstants.LIST_EXAMGROUP] = new SelectList(new string[] { });
                ViewData[ExamSubjectConstants.LIST_EXAMSUBJECT] = new List<ExamSubjectViewModel>();
            }

            ExamSubjectViewModel exam = new ExamSubjectViewModel();
            ViewData[ExamSubjectConstants.EXAMSUBJECT_UPDATE] = exam;
        }

        #endregion SetViewData

        #region Search


        public PartialViewResult Search(SearchViewModel form)
        {
            ViewData[ExamSubjectConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;

            IDictionary<string, object> dicsearch = new Dictionary<string, object>();
            dicsearch.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dicsearch.Add("SemesterID", _globalInfo.Semester.Value);
            dicsearch.Add("SchoolID", _globalInfo.SchoolID.Value);
            dicsearch.Add("AppliedLevel", _globalInfo.AppliedLevel);
            Examinations listExam = ExaminationsBusiness.Search(dicsearch).OrderByDescending(o => o.CreateTime).FirstOrDefault();
            if (form.Examinations == 0)
            {
                form.Examinations = listExam.ExaminationsID;
            }
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("ExaminationsID", form.Examinations);
            search.Add("ExamGroupID", form.ExamGroup);

            List<ExamSubjectViewModel> list = this._Search(search);
            ViewData[ExamSubjectConstants.LIST_EXAMSUBJECT] = list;

            return PartialView("_List");
        }

        private List<ExamSubjectViewModel> _Search(IDictionary<string, object> search)
        {
            SetViewDataPermission("ExamSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            long examinationsID = SMAS.Business.Common.Utils.GetLong(search, "ExaminationsID");
            long examGroupID = SMAS.Business.Common.Utils.GetLong(search, "ExamGroupID");

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;

            //Danh sach phan cong giam thi theo ky thi nhom thi
            List<int> supervisoryList = ExamSupervisoryAssignmentBusiness.Search(dic).Select(p => p.SubjectID).ToList();
            //Danh sach ma tui thi
            List<int> examBagList = ExamBagBusiness.Search(dic).Select(p => p.SubjectID).ToList();
            //Danh sach phach
            List<int> examCandenceBagList = ExamCandenceBagBusiness.Search(dic).Select(p => p.SubjectID).ToList();
            // Dan sach danh phach
            List<int> examdetachableBagList = ExamDetachableBagBusiness.Search(dic).Select(p => p.SubjectID).ToList();
            //Danh sach thi sinh vi pham
            List<int> examPupilViolateList = ExamPupilViolateBusiness.Search(dic).Select(p => p.SubjectID).ToList();
            // Danh sach phan cong nhap diem
            List<int> examInputMarkAssignList = ExamInputMarkAssignedBusiness.Search(dic).Select(p => p.SubjectID).ToList();
            // Danh sach vao diem thi
            List<int> examInputMarkList = ExamInputMarkBusiness.Search(dic).Select(p => p.SubjectID).ToList();
            //Danh sach thi sinh vang thi
            List<int> ExamPupilAbsenceList = ExamPupilAbsenceBusiness.Search(dic).Select(p => p.SubjectID).ToList();

            // Check chua vao diem thi va chua chot diem thi
            ExaminationsBO ExaminationsObj = this.GetExaminationsByID(examinationsID);
            ViewData[ExamSubjectConstants.DATA_EXAMINATIONS] = ExaminationsObj;
            //Danh sach mon thi
            List<ExamSubjectBO> examSubjectList = ExamSubjectBusiness.GetListExamSubject(search).ToList();
            List<ExamSubjectViewModel> resultList = new List<ExamSubjectViewModel>();
            ExamSubjectViewModel resultObj = null;
            ExamSubjectBO examSubjectBO = null;
            bool isCurrentYear = _globalInfo.IsCurrentYear;

            if (examSubjectList != null && examSubjectList.Count > 0)
            {
                for (int i = 0; i < examSubjectList.Count; i++)
                {
                    examSubjectBO = examSubjectList[i];
                    resultObj = new ExamSubjectViewModel();
                    resultObj.ExamSubjectID = examSubjectBO.ExamSubjectID;
                    resultObj.ExaminationsID = examSubjectBO.ExaminationsID;
                    resultObj.ExamGroupID = examSubjectBO.ExamGroupID;
                    resultObj.SubjectID = examSubjectBO.SubjectID;
                    resultObj.SubjectCode = examSubjectBO.SubjectCode;
                    resultObj.SchedulesExam = examSubjectBO.SchedulesExam;
                    resultObj.CreateTime = examSubjectBO.CreateTime;
                    resultObj.UpdateTime = examSubjectBO.UpdateTime;
                    resultObj.ExaminationsName = examSubjectBO.ExaminationsName;
                    resultObj.ExamGroupName = examSubjectBO.ExamGroupName;
                    resultObj.SubjectName = examSubjectBO.SubjectName;
                    resultObj.Abbreviation = examSubjectBO.Abbreviation;
                    if (supervisoryList.Exists(p => p == examSubjectBO.SubjectID)
                            || examBagList.Exists(p => p == examSubjectBO.SubjectID)
                            || examCandenceBagList.Exists(p => p == examSubjectBO.SubjectID)
                            || examdetachableBagList.Exists(p => p == examSubjectBO.SubjectID)
                            || examPupilViolateList.Exists(p => p == examSubjectBO.SubjectID)
                            || examInputMarkAssignList.Exists(p => p == examSubjectBO.SubjectID)
                            || examInputMarkList.Exists(p => p == examSubjectBO.SubjectID)
                            || ExamPupilAbsenceList.Exists(p => p == examSubjectBO.SubjectID)
                            || (ExaminationsObj != null && (ExaminationsObj.MarkInput.HasValue && ExaminationsObj.MarkClosing.HasValue
                                && ExaminationsObj.MarkInput.Value && ExaminationsObj.MarkClosing.Value))
                            || !isCurrentYear
                        )
                    {
                        resultObj.CheckExist = true;
                    }
                    else
                    {
                        resultObj.CheckExist = false;
                    }

                    resultList.Add(resultObj);
                }
            }
            return resultList;
        }

        #endregion Search

        #region LoadCombobox

        private IEnumerable<ExamGroup> GetExamGroupFromExaminations(long ExaminationsID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ExaminationsID", ExaminationsID},
                };

            List<ExamGroup> listGroup = ExamGroupBusiness.Search(dic).ToList().OrderBy(p => p.ExamGroupCode.ToUpper()).ToList();
            return listGroup;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamGroup(long? ExaminationsID)
        {
            if (ExaminationsID.HasValue)
            {
                IEnumerable<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID.Value);
                return Json(new SelectList(listGroup, "ExamGroupID", "ExamGroupName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion LoadCombobox

        #region Create

        public PartialViewResult GetDataInsert(long ExaminationsID, long ExamGroupID)
        {
            SetViewDataPermission("ExamSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            if (ExaminationsID != 0 && ExamGroupID != 0)
            {
                ExamGroup examGroup = ExamGroupBusiness.Find(ExamGroupID);

                IDictionary<string, object> searchSubject = new Dictionary<string, object>();
                searchSubject.Add("ExaminationsID", ExaminationsID);
                searchSubject.Add("ExamGroupID", ExamGroupID);

                List<ExamSubject> listExamSubject = ExamSubjectBusiness.Search(searchSubject).ToList();
                List<ExamSubjectViewModel> listSubjectInsert = (from ss in SchoolSubjectBusiness.All
                                                                join sc in SubjectCatBusiness.All on ss.SubjectID equals sc.SubjectCatID
                                                                where sc.AppliedLevel == _globalInfo.AppliedLevel.Value
                                                                && ss.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                                && ss.SchoolID == _globalInfo.SchoolID
                                                                select new ExamSubjectViewModel
                                                                {
                                                                    ExaminationsID = ExaminationsID,
                                                                    ExamGroupID = ExamGroupID,
                                                                    ExamGroupName = examGroup.ExamGroupName,
                                                                    SubjectID = ss.SubjectID,
                                                                    SubjectName = sc.SubjectName,
                                                                    OrderInSubject = sc.OrderInSubject,
                                                                    Abbreviation = sc.Abbreviation

                                                                }).Distinct().OrderBy(o => o.OrderInSubject).ToList();

                for (int i = 0; i < listExamSubject.Count; i++)
                {
                    listSubjectInsert = listSubjectInsert.Where(o => o.SubjectID != listExamSubject[i].SubjectID).ToList();
                }
                ViewData[ExamSubjectConstants.LIST_EXAMSUBJECT_INSERT] = listSubjectInsert;
            }
            else
            {
                List<ExamSubjectViewModel> listSubjectInsert = new List<ExamSubjectViewModel>();
                ViewData[ExamSubjectConstants.LIST_EXAMSUBJECT_INSERT] = listSubjectInsert;
            }

            return PartialView("_ListInsert");
        }

        private bool CheckContains(int[] ListSubject, int SubjectID)
        {
            for (int i = 0; i < ListSubject.Length; i++)
            {
                if (ListSubject[i] == SubjectID)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(FormCollection frm, long? HiddenExaminationsID, long? ExamGroupID, int? CheckAllExamGroup, int[] CheckExamSubjectID)
        {
            if (GetMenupermission("ExamSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                return Json(new JsonMessage(Res.Get("Validate_Permission_Teacher"), "error"));
            }

            if (CheckExamSubjectID == null || CheckExamSubjectID.Length == 0)
            {
                return Json(new JsonMessage(Res.Get("Message_Error_Exam_Subject_Empty"), "error"));
            }

            int subjectID = 0;
            List<ExamSubject> listExamSubject = new List<ExamSubject>();
            ExamSubject insert = null;
            int checkAll = 0;
            if (CheckAllExamGroup.HasValue)
            {
                checkAll = 1;
            }
            if (HiddenExaminationsID.HasValue && ExamGroupID.HasValue)
            {
                for (int i = 0; i < CheckExamSubjectID.Length; i++)
                {
                    subjectID = CheckExamSubjectID[i];
                    insert = new ExamSubject();
                    insert.ExamSubjectID = ExamSubjectBusiness.GetNextSeq<long>();
                    insert.ExaminationsID = HiddenExaminationsID.Value;
                    insert.ExamGroupID = ExamGroupID.Value;
                    insert.SubjectID = subjectID;
                    insert.SubjectCode = frm["Abbreviation" + subjectID];
                    insert.SchedulesExam = frm["SchedulesExam" + subjectID];
                    insert.CreateTime = DateTime.Now;
                    listExamSubject.Add(insert);
                }
                if (listExamSubject != null && listExamSubject.Count > 0)
                {
                    // Kiem tra trung ma mon thi trong list client submit
                    List<string> listSubjectCode = listExamSubject.Select(p => p.SubjectCode.ToLower()).ToList();
                    List<string> listDuplicate = listSubjectCode.GroupBy(p => p).Where(p => p.Count() > 1).Select(p => p.Key).ToList();
                    if (listDuplicate != null && listDuplicate.Count > 0)
                    {
                        return Json(new JsonMessage(Res.Get("Validate_Duplicate_Exam_Subject"), "error"));
                    }
                    //Kiem tra ma mon thi da co trong DB theo nhom thi
                    List<long> listGroupID = new List<long>();
                    IDictionary<string, object> search = new Dictionary<string, object>();
                    search.Add("ExaminationsID", HiddenExaminationsID.Value);
                    if (checkAll == 0)
                    {
                        search.Add("ExamGroupID", ExamGroupID.HasValue ? ExamGroupID.Value : 0);
                        //Danh sach nhom thi cua ki thi
                        listGroupID = ExamGroupBusiness.All.Where(p => p.ExaminationsID == HiddenExaminationsID.Value && p.ExamGroupID == ExamGroupID.Value).Select(p => p.ExamGroupID).ToList();
                    }
                    else
                    {
                        //Danh sach nhom thi cua ki thi
                        listGroupID = ExamGroupBusiness.All.Where(p => p.ExaminationsID == HiddenExaminationsID.Value).Select(p => p.ExamGroupID).ToList();
                    }

                    // Danh sach cac mon hoc theo ky thi trong DB
                    List<ExamSubject> listExamSubjectSource = ExamSubjectBusiness.Search(search).ToList();
                    // So sanh cac nhom thi khong trung ma mon thi
                    long groupID = 0;
                    List<string> listExamSubjectSourceByGroupID = null;
                    List<string> listIntersect = null;
                    List<string> listSubjectCodeGrid = listExamSubject.Select(p => p.SubjectCode.ToLower()).ToList();
                    for (int i = 0; i < listGroupID.Count; i++)
                    {
                        listIntersect = new List<string>();
                        listExamSubjectSourceByGroupID = new List<string>();
                        groupID = listGroupID[i];
                        for (int j = 0; j < listExamSubject.Count; j++)
                        {
                            subjectID = listExamSubject[j].SubjectID;
                            listExamSubjectSourceByGroupID = listExamSubjectSource.Where(p => p.ExamGroupID == groupID && p.SubjectID != subjectID).Select(p => p.SubjectCode.ToLower()).ToList();
                            listIntersect = listSubjectCodeGrid.Intersect(listExamSubjectSourceByGroupID).ToList();
                            if (listIntersect.Count > 0)
                            {
                                return Json(new JsonMessage(Res.Get("Validate_Duplicate_Exam_Subject"), "error"));
                            }
                        }
                    }
                    //Neu check dieu kien khong return ve thi tieo tuc insert
                    ExamSubjectBusiness.InsertExamSubject(listExamSubject, checkAll);
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Message_Error_Exam_Subject_Empty"), "error"));
            }

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        #endregion Create

        #region Update

        public PartialViewResult GetDataUpdate(long? ExamSubjectID)
        {
            if (ExamSubjectID.HasValue)
            {
                ExamSubjectViewModel exam = ExamSubjectBusiness.FindExamSubject(ExamSubjectID.Value)
                    .Select(o => new ExamSubjectViewModel
                    {
                        ExamSubjectID = o.ExamSubjectID,
                        ExaminationsID = o.ExaminationsID,
                        ExamGroupID = o.ExamGroupID,
                        SubjectID = o.SubjectID,
                        SubjectCode = o.SubjectCode,
                        SchedulesExam = o.SchedulesExam,
                        CreateTime = o.CreateTime,
                        UpdateTime = o.UpdateTime,
                        ExaminationsName = o.ExaminationsName,
                        ExamGroupName = o.ExamGroupName,
                        SubjectName = o.SubjectName,
                        Abbreviation = o.Abbreviation

                    }).ToList().FirstOrDefault();

                ViewData[ExamSubjectConstants.EXAMSUBJECT_UPDATE] = exam;
            }
            return PartialView("_Edit");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Update(long HiddenExamSubject, string ExamSubjectCode, string SchedulesExam, int? CheckAllExamGroup, long HiddenExaminations, long HiddenExamGroupID, int HiddenSubject)
        {
            if (GetMenupermission("ExamSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            int checkAll = 0;
            if (CheckAllExamGroup.HasValue)
            {
                checkAll = 1;
            }

            //Check validte
            // Du lieu co trung voi DB theo tung nhom 1
            //Kiem tra ma mon thi da co trong DB theo nhom thi
            List<long> listGroupID = new List<long>();
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("ExaminationsID", HiddenExaminations);
            if (checkAll == 0)
            {
                search.Add("ExamGroupID", HiddenExamGroupID);
                //Danh sach nhom thi cua ki thi
                listGroupID = ExamGroupBusiness.All.Where(p => p.ExaminationsID == HiddenExaminations && p.ExamGroupID == HiddenExamGroupID).Select(p => p.ExamGroupID).ToList();
            }
            else
            {
                //Danh sach nhom thi cua ki thi
                listGroupID = ExamGroupBusiness.All.Where(p => p.ExaminationsID == HiddenExaminations).Select(p => p.ExamGroupID).ToList();
            }

            // Danh sach cac mon hoc theo ky thi trong DB
            List<ExamSubject> listExamSubjectSource = ExamSubjectBusiness.Search(search).ToList();
            // So sanh cac nhom thi khong trung ma mon thi
            long groupID = 0;
            List<ExamSubject> listExamSubject = null;
            List<ExamSubject> listExamSubjectCode = null;
            for (int i = 0; i < listGroupID.Count; i++)
            {
                groupID = listGroupID[i];
                //list mon hoc trung nhau
                listExamSubject = listExamSubjectSource.Where(p => p.ExamGroupID == groupID && p.SubjectID == HiddenSubject).ToList();
                // list khac mon nhung trung ma mon thi
                listExamSubjectCode = listExamSubjectSource.Where(p => p.ExamGroupID == groupID && p.SubjectID != HiddenSubject && p.SubjectCode.ToLower().Contains(ExamSubjectCode.ToLower())).ToList();
                if (listExamSubject.Count > 1 || listExamSubjectCode.Count > 0)
                {
                    return Json(new JsonMessage(Res.Get("Validate_Duplicate_Exam_Subject"), "error"));
                }
            }

            ExamSubject exam = ExamSubjectBusiness.Find(HiddenExamSubject);
            exam.SubjectCode = ExamSubjectCode;
            exam.SchedulesExam = SchedulesExam;
            ExamSubjectBusiness.UpdateExamSubject(exam, checkAll);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        #endregion Update

        #region Delete

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult Delete(string strExamSubjectID)
        {
            if (GetMenupermission("ExamSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            string[] arraySubjectID = strExamSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            List<string> strexamSubjectIDList = arraySubjectID.ToList();
            List<long> examSubjectIDList = strexamSubjectIDList.Select(long.Parse).ToList();
            if (examSubjectIDList != null && examSubjectIDList.Count > 0)
            {
                ExamSubjectBusiness.DeleteExamSubject(examSubjectIDList);
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion Delete

        private ExaminationsBO GetExaminationsByID(long examinationsID)
        {
            ExaminationsBO examinationsObj = (from a in ExaminationsBusiness.All
                                              where a.ExaminationsID == examinationsID
                                              select new ExaminationsBO
                                              {
                                                  ExaminationsID = a.ExaminationsID,
                                                  ExaminationsName = a.ExaminationsName,
                                                  AcademicYearID = a.AcademicYearID,
                                                  AppliedLevel = a.AppliedLevel,
                                                  SemesterID = a.SemesterID,
                                                  SchoolID = a.SchoolID,
                                                  MarkInputType = a.MarkInputType,
                                                  MarkInput = a.MarkInput,
                                                  MarkClosing = a.MarkClosing,
                                                  DataInheritance = a.DataInheritance,
                                                  InheritanceExam = a.InheritanceExam,
                                                  InheritanceData = a.InheritanceData,
                                                  CreateTime = a.CreateTime,
                                                  UpdateTime = a.UpdateTime
                                              }).FirstOrDefault();
            return examinationsObj;
        }
    }
}
