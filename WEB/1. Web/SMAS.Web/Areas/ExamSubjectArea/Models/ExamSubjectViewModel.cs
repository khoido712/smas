﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSubjectArea.Models
{
    public class ExamSubjectViewModel
    {
        public long ExamSubjectID { get; set; }

        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamSubjectConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        public long ExaminationsID { get; set; }

        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        public string ExaminationsName { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamSubjectConstants.LIST_EXAMGROUP)]
        [AdditionalMetadata("Placeholder", "0")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamSubjectInsert(this)")]
        public long ExamGroupID { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        public string ExamGroupName { get; set; }

        public int SubjectID { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamSubjectName")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamSubjectID")]
        public string SubjectCode { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamSubjectID")]
        public string Abbreviation { get; set; }

        [ResourceDisplayName("ExamSubject_Label_SchedulesExam")]        
        public string SchedulesExam { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public int? OrderInSubject { get; set; }

        /// <summary>
        /// de kiem tra co can hien check box xoa hay khong
        /// </summary>
        public bool CheckExist { get; set; }
    }
}