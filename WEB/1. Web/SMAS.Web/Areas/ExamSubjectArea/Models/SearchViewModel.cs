﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSubjectArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamSubjectConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamGroup(this)")]
        public long Examinations { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamSubjectConstants.LIST_EXAMGROUP)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamSubject(this)")]
        public long? ExamGroup { get; set; }
    }
}