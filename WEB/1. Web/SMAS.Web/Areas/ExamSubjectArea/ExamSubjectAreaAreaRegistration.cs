﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSubjectArea
{
    public class ExamSubjectAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamSubjectArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamSubjectArea_default",
                "ExamSubjectArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
