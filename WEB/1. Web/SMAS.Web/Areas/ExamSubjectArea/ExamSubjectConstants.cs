﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSubjectArea
{
    public class ExamSubjectConstants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";

        public const string LIST_EXAMINATIONS = "ListExaminations";
        public const string LIST_EXAMGROUP = "ListExamGroup";
        public const string LIST_EXAMSUBJECT = "ListExamSubject";

        public const string LIST_EXAMSUBJECT_INSERT = "ListExamSubjectInsert";
        public const string EXAMSUBJECT_UPDATE = "ExamSubjectUpdate";

        public const string DATA_EXAMINATIONS = "DataExaminations";
    }
}