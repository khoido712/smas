﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ListReligionArea.Models;

using SMAS.Models.Models;
using SMAS.Web.Areas.ListReligionArea;

namespace SMAS.Web.Areas.ListReligionArea.Controllers
{
    public class ListReligionController : BaseController
    {        
        private readonly IReligionBusiness ReligionBusiness;

        public ListReligionController(IReligionBusiness religionBusiness)
		{
			this.ReligionBusiness = religionBusiness;
		}
		
		//
        // GET: /Religion/

        public ActionResult Index()
        {
            SetViewDataPermission("ListReligion", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;

            IEnumerable<ListReligionViewModel> lst = this._Search(SearchInfo);
            ViewData[ListReligionConstants.LIST_RELIGION] = lst;
            return View();
        }

		//
        // GET: /Religion/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["Resolution"] = frm.Resolution;
            SearchInfo["IsActive"] = true;

            IEnumerable<ListReligionViewModel> lst = this._Search(SearchInfo);
            ViewData[ListReligionConstants.LIST_RELIGION] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ListReligion", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Religion religion = new Religion();
            TryUpdateModel(religion); 
            Utils.Utils.TrimObject(religion);

            religion.IsActive = true;
            this.ReligionBusiness.Insert(religion);
            this.ReligionBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int ReligionID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ListReligion", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Religion religion = this.ReligionBusiness.Find(ReligionID);
            TryUpdateModel(religion);
            Utils.Utils.TrimObject(religion);
            this.ReligionBusiness.Update(religion);
            this.ReligionBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }



        #region Detail
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult Detail(int ReligionID)
        {
            Religion religion = this.ReligionBusiness.Find(ReligionID);
            ListReligionViewModel frm = new ListReligionViewModel();
            Utils.Utils.BindTo(religion, frm, true);
            return PartialView("_Detail", frm);
        }

        #endregion

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ListReligion", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.ReligionBusiness.Delete(id);
            this.ReligionBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ListReligionViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("ListReligion", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<Religion> query = this.ReligionBusiness.Search(SearchInfo);
            IQueryable<ListReligionViewModel> lst = query.Select(o => new ListReligionViewModel
            {               
						ReligionID = o.ReligionID,								
						Resolution = o.Resolution,								
						Description = o.Description
				
            });

            return lst.OrderBy(o=>o.Resolution).ToList();
        }        
    }
}





