﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ListReligionArea
{
    public class ListReligionAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ListReligionArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ListReligionArea_default",
                "ListReligionArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
