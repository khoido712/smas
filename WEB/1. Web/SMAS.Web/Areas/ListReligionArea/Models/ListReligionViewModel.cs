/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ListReligionArea.Models
{
    public class ListReligionViewModel
    {


        [ScaffoldColumn(false)]
        [ResourceDisplayName("Religion_Label_ReligionID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ReligionID { get; set; }

        [ResourceDisplayName("Religion_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Resolution { get; set; }

        [ResourceDisplayName("Religion_Label_Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }			
	       
    }
}


