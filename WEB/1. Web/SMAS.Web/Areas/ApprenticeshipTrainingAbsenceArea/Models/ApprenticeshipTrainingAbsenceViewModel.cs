/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ApprenticeshipTrainingAbsenceArea.Models
{
    public class ApprenticeshipTrainingAbsenceViewModel
    {
		public System.Int32 ApprenticeshipTrainingAbsenceID { get; set; }								
		public System.Nullable<System.Int32> SchoolID { get; set; }								
		public System.Int32 ClassID { get; set; }
        public System.Int32 ApprenticeshipClassID { get; set; }					
		public System.Int32 AcademicYearID { get; set; }								
		public System.Nullable<System.Int32> EducationLevelID { get; set; }								
		public System.Int32 PupilID { get; set; }
        [ResourceDisplayName("ApprenticeshipTrainingAbsence_Control_PupilCode")]
        public System.String PupilCode { get; set; }								
		public System.Nullable<System.Int32> SectionInDay { get; set; }								
		public System.DateTime AbsentDate { get; set; }								
		public System.Boolean IsAccepted { get; set; }
        [ResourceDisplayName("ApprenticeshipTrainingAbsence_Control_FullName")]
        public System.String FullName { get; set; }
        public System.String ScheduleDetail { get; set; }

        public int? PositionError { get; set; }

        public System.Int32 SumP { get; set; }
        public System.Int32 SumK { get; set; }

        public string Accepted { get; set; }
        public bool? Pass { get; set; }
        public string Note { get; set; }
    }
}


