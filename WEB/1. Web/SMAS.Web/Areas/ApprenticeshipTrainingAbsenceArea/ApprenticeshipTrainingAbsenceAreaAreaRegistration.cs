﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ApprenticeshipTrainingAbsenceArea
{
    public class ApprenticeshipTrainingAbsenceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ApprenticeshipTrainingAbsenceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ApprenticeshipTrainingAbsenceArea_default",
                "ApprenticeshipTrainingAbsenceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
