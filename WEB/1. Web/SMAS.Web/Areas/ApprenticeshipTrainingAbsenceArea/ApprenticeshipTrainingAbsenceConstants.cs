/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ApprenticeshipTrainingAbsenceArea
{
    public class ApprenticeshipTrainingAbsenceConstants
    {
        public const string LIST_APPRENTICESHIPTRAININGABSENCE = "listApprenticeshipTrainingAbsence";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_CLASS = "listClass";
        public const string LIST_MONTH = "listMonth";
        public const string LIST_SECTIONINDAY = "listSectionInDay";
        public const string LIST_ACCEPTED = "listAccepted";
        public const string LIST_DAYOFF = "listDayOff";
    }
}