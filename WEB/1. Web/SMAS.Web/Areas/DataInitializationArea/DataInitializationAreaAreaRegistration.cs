﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DataInitializationArea
{
    public class DataInitializationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DataInitializationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DataInitializationArea_default",
                "DataInitializationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
