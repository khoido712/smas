﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DataInitializationArea.Controllers
{
    public class DataInitializationController : Controller
    {
        //
        // GET: /DataInitializationArea/DataInitialization/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /DataInitializationArea/DataInitialization/Details/5


        [ValidateAntiForgeryToken]
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /DataInitializationArea/DataInitialization/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /DataInitializationArea/DataInitialization/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /DataInitializationArea/DataInitialization/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /DataInitializationArea/DataInitialization/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /DataInitializationArea/DataInitialization/Delete/5
 

        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /DataInitializationArea/DataInitialization/Delete/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult getUrl(int? menuOrder)
        {
            string url = "";
            if (menuOrder == 1)
            {
                url = "/AcademicYearArea/AcademicYear/";
            }
            else if (menuOrder == 2)
            {
                url = "/SchoolFacultyArea/SchoolFaculty/";
            }
            else if (menuOrder == 3)
            {
                url = "/SchoolSubjectArea/SchoolSubject/";
            }
            else if (menuOrder == 4)
            {
                url = "/ClassProfileArea/ClassProfile/";
            }
            else if (menuOrder == 5)
            {
                url = "/ClassSubjectArea/ClassSubject/";
            }
            ViewData["url"] = url;
            return PartialView("_List");
        }
    }
}
