﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ReportMarkSubjectByTeacherArea.Models;
using System.IO;
using SMAS.Web.Areas.ReportMarkSubjectByTeacherArea;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.ReportMarkSubjectByTeacherArea.Controllers
{
     
    public class ReportMarkSubjectByTeacherController : BaseController
    {
        IClassProfileBusiness ClassProfileBusiness;
        ISchoolFacultyBusiness SchoolFacultyBusiness;
        IEmployeeBusiness EmployeeBusiness;
        ISchoolProfileBusiness SchoolProfileBusiness;
        ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        ISubjectCatBusiness SubjectCatBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IReportMarkSubjectByTeacherBusiness ReportMarkSubjectByTeacherBusiness;
        ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        public ReportMarkSubjectByTeacherController(ISchoolFacultyBusiness schoolFacultyBusiness, IEmployeeBusiness employeeBusiness, 
            ISchoolProfileBusiness schoolProfileBusiness, ITeachingAssignmentBusiness teachingAssignmentBusiness,
            ISubjectCatBusiness subjectCatBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness, IReportMarkSubjectByTeacherBusiness reportMarkSubjectByTeacherBusiness, IAcademicYearBusiness academicYearBusiness, ISemeterDeclarationBusiness semeterDeclarationBusiness)
        {
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ReportMarkSubjectByTeacherBusiness = reportMarkSubjectByTeacherBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SemeterDeclarationBusiness = semeterDeclarationBusiness;
        }
        public ActionResult Index()
        {
            GetViewData();
            return View();
        }
        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();         

            //Lấy ra tổ bộ môn của nhà trường
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = glo.AcademicYearID;
            dic["SchoolID"] = glo.SchoolID;
            List<SchoolFaculty> lstSchoolFaculty = new List<SchoolFaculty>();
            if (glo.SchoolID != null)
            {
                lstSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(glo.SchoolID.Value, dic).OrderBy(o => o.FacultyName).ToList();
            }
            lstSchoolFaculty = lstSchoolFaculty.OrderBy(o => o.FacultyName).ToList();
            ViewData[ReportMarkSubjectByTeacherConstants.ListSchoolFaculty] = lstSchoolFaculty;
            //tìm kiếm giáo viên
            List<TeacherInfoViewModel> ListEmployee = EmployeeBusiness.SearchWorkingTeacherByFaculty(glo.SchoolID.Value).Select(u => new TeacherInfoViewModel {TeacherID = u.EmployeeID,
                                                                                                                                                               Name = u.Name,
                                                                                                                                                               FullName = u.FullName,
                                                                                                                                                               SchoolFacultyID = u.SchoolFacultyID.Value
            }).ToList();
            ViewData["LIST_TEACHER_INFO"] = ListEmployee;
            SchoolProfile sp = SchoolProfileBusiness.Find(glo.SchoolID.Value);
            ViewData["SCHOOL_NAME"] = sp.SchoolName;
            if (lstSchoolFaculty.Count() > 0)
            {
                ViewData["SCHOOLFACULTYID"] = lstSchoolFaculty.FirstOrDefault().SchoolFacultyID;
            }
            
            List<SubjectCat> lstSubject = new List<SubjectCat>();
            ViewData[ReportMarkSubjectByTeacherConstants.LIST_SUBJECT] = new SelectList(lstSubject, "SubjectID", "SubjectName");

            var lstSemesterCommon = CommonList.Semester();
            ViewData[ReportMarkSubjectByTeacherConstants.LIST_SEMESTER] = new SelectList(lstSemesterCommon, "key", "value");

            var semesterCommon = lstSemesterCommon.FirstOrDefault();

            ViewData["DateHK"] = null;
              string Day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();
            string date = Year + "/" + month + "/" + Day;
            DateTime datenow = Convert.ToDateTime(date);
            AcademicYear academicYear = AcademicYearBusiness.Find(glo.AcademicYearID.Value);
            DateTime? FirstStarDate = academicYear.FirstSemesterStartDate.Value;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value;
            if ((datenow >= FirstStarDate && datenow <= FirstEndDate) || datenow < FirstStarDate)
            {
                ViewData["DateHK"] = "1";
            }
            else
            {
                ViewData["DateHK"] = "2";
            }
            IDictionary<string, object> dicSes = new Dictionary<string, object>();
            dicSes["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value; ;
            dicSes["SchoolID"] = new GlobalInfo().SchoolID.Value;

            SemeterDeclaration semester = SemeterDeclarationBusiness.Search(dicSes).FirstOrDefault();
            ViewData["isAvaible"] = "true";
            if (semester == null)
            {
                ViewData["isAvaible"] = "false";
            }
            
            
            //ViewData["NUMBER_SUBJECT"] = "1";
            //var semesterSubject = lstSemesterSubject.FirstOrDefault();


        }

        public PartialViewResult SearchTreeView(string name)
        {
            if (name.Trim().ToUpper() == "HỌ VÀ TÊN GIÁO VIÊN..." || name.Trim() == "")
            {
                GlobalInfo glo = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = glo.AcademicYearID;
                dic["SchoolID"] = glo.SchoolID;
                List<SchoolFaculty> lstSchoolFaculty = new List<SchoolFaculty>();
                if (glo.SchoolID != null)
                {
                    lstSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(glo.SchoolID.Value, dic).OrderBy(o => o.FacultyName).ToList();
                }
                lstSchoolFaculty = lstSchoolFaculty.OrderBy(o => o.FacultyName).ToList();
                ViewData[ReportMarkSubjectByTeacherConstants.ListSchoolFaculty] = lstSchoolFaculty;
                List<TeacherInfoViewModel> ListEmployee = EmployeeBusiness.SearchWorkingTeacherByFaculty(glo.SchoolID.Value).Select(u => new TeacherInfoViewModel
                {
                    TeacherID = u.EmployeeID,
                    Name = u.Name,
                    FullName = u.FullName,
                    SchoolFacultyID = u.SchoolFacultyID.Value
                }).ToList();
                ViewData["LIST_TEACHER_INFO"] = ListEmployee;
                SchoolProfile sp = SchoolProfileBusiness.Find(glo.SchoolID.Value);
                ViewData["SCHOOL_NAME"] = sp.SchoolName;
                if (lstSchoolFaculty.Count() > 0)
                {
                    ViewData["SCHOOLFACULTYID"] = lstSchoolFaculty.FirstOrDefault().SchoolFacultyID;
                }
               
            }
            else
            {
                GlobalInfo glo = new GlobalInfo();
                List<TeacherInfoViewModel> ListEmployee = EmployeeBusiness.SearchWorkingTeacherByFaculty(glo.SchoolID.Value).Select(u => new TeacherInfoViewModel
                {
                    TeacherID = u.EmployeeID,
                    Name = u.Name,
                    FullName = u.FullName,
                    SchoolFacultyID = u.SchoolFacultyID.Value
                }).ToList();
                ListEmployee = ListEmployee.Where(o => o.FullName.ToUpper().Contains(name.Trim().ToUpper())).ToList();
                ViewData["LIST_TEACHER_INFO"] = ListEmployee;
                IEnumerable<int> lstSchoolFacultyID = new List<int>();
                List<SchoolFaculty> lstSchoolFaculty = new List<SchoolFaculty>();
                lstSchoolFacultyID = ListEmployee.Select(u => u.SchoolFacultyID).Distinct().ToList();
                foreach (var p in lstSchoolFacultyID)
                {
                    SchoolFaculty sf = SchoolFacultyBusiness.Find(p);
                    lstSchoolFaculty.Add(sf);
                }
                lstSchoolFaculty = lstSchoolFaculty.OrderBy(o => o.FacultyName).ToList();
                ViewData[ReportMarkSubjectByTeacherConstants.ListSchoolFaculty] = lstSchoolFaculty;
                SchoolProfile sp = SchoolProfileBusiness.Find(glo.SchoolID.Value);
                ViewData["SCHOOL_NAME"] = sp.SchoolName;
                //if (lstSchoolFaculty.Count() > 0)
                //{
                //    ViewData["SCHOOLFACULTYID"] = lstSchoolFaculty.FirstOrDefault().SchoolFacultyID;
                //}
            }
            return PartialView("_TrvFaculty");
        }
        [ValidateAntiForgeryToken]
        public JsonResult SearchSubjectByTeacher(int? teacherID, int? semester)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = glo.AcademicYearID.Value;
            dicClass["TeacherID"] = teacherID;
            dicClass["Semester"] = semester;
            dicClass["AppliedLevel"] = glo.AppliedLevel.Value;
            
            List<TeachingAssignmentBO> lstTA = TeachingAssignmentBusiness.SearchByTeacher(glo.SchoolID.Value, dicClass).OrderBy(o => o.OrderInSubject).ThenBy(o => o.SubjectName).ToList();
            List<ComboObject> lstTeachingAssignment = lstTA
                                                .Select(u => new ComboObject(u.SubjectID.ToString(), u.SubjectName))
                                                .ToList();
            ViewData["NUMBER_SUBJECT"] = lstTA.Count().ToString();
            return Json(new SelectList(lstTeachingAssignment, "key", "value"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReportBySemester(FormCollection form)
        {

            ReportMarkSubjectBO reportMarkSubject = new ReportMarkSubjectBO();

            reportMarkSubject.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            reportMarkSubject.Semester = int.Parse(form["Semester"]);
            reportMarkSubject.SchoolID = new GlobalInfo().SchoolID.Value;
            reportMarkSubject.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            reportMarkSubject.SubjectID = int.Parse(form["SubjectID"]);
            reportMarkSubject.TeacherID = int.Parse(form["TeacherID"]);
            string reportCode = SystemParamsInFile.REPORT_SODIEMBOMONTHEOGIAOVIEN;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = this.ReportMarkSubjectByTeacherBusiness.GetReportMarkSubjectByTeacher(reportMarkSubject);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportMarkSubjectByTeacherBusiness.CreateReportMarkSubjectByTeacher(reportMarkSubject);
                processedReport = this.ReportMarkSubjectByTeacherBusiness.InsertReportMarkSubjectByTeacher(reportMarkSubject, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportBySemester(FormCollection form)
        {
            ReportMarkSubjectBO reportMarkSubject = new ReportMarkSubjectBO();
            reportMarkSubject.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            reportMarkSubject.AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            reportMarkSubject.Semester = int.Parse(form["Semester"]);

            reportMarkSubject.SchoolID = new GlobalInfo().SchoolID.Value;
            reportMarkSubject.SubjectID = int.Parse(form["SubjectID"]);
            reportMarkSubject.TeacherID = int.Parse(form["TeacherID"]);
            
            Stream excel = this.ReportMarkSubjectByTeacherBusiness.CreateReportMarkSubjectByTeacher(reportMarkSubject);
            ProcessedReport processedReport = this.ReportMarkSubjectByTeacherBusiness.InsertReportMarkSubjectByTeacher(reportMarkSubject, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}