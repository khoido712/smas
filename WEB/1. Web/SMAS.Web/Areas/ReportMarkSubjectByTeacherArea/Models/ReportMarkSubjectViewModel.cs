﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportMarkSubjectByTeacherArea.Models
{
    public class ReportMarkSubjectViewModel
    {
        [ResourceDisplayName("TranscriptsBySemester_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ReportMarkSubjectByTeacherConstants.LIST_SEMESTER)]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public Nullable<int> Semester { get; set; }

        [ResourceDisplayName("TranscriptsBySemester_Label_Subject")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("PlaceHolder", "CHOICE")]
        [AdditionalMetadata("ViewDataKey", ReportMarkSubjectByTeacherConstants.LIST_SUBJECT)]
        public int SubjectID { get; set; }

    }
}