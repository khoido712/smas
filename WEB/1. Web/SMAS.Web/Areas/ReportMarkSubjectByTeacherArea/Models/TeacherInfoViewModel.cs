﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMarkSubjectByTeacherArea.Models
{
    public class TeacherInfoViewModel
    {
        public System.Int32 TeacherID { get; set; }
        public System.String Name { get; set; }
        public System.String FullName { get; set; }
        public System.Int32 SchoolFacultyID { get; set; }
    }
}