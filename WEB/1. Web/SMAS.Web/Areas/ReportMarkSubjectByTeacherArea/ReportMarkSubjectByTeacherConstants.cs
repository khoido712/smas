﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportMarkSubjectByTeacherArea
{
    public class ReportMarkSubjectByTeacherConstants
    {
        public const string SCHOOLFACULTYID = "SCHOOLFACULTYID";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_SUBJECT = "LIST_SUBJECT";
        public const string ListSchoolFaculty = "ListSchoolFaculty";
        public const string LIST_TEACHER_INFO = "LIST_TEACHER_INFO";
        public const string SCHOOL_NAME = "SCHOOL_NAME";
        public const string NUMBER_SUBJECT = "NUMBER_SUBJECT";
        public const string DateHK = "DateHK";
        public const string isAvaible = "isAvaible";
    }
}