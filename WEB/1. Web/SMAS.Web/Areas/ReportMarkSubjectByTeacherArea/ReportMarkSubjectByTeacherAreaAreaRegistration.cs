﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportMarkSubjectByTeacherArea
{
    public class ReportMarkSubjectByTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportMarkSubjectByTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportMarkSubjectByTeacherArea_default",
                "ReportMarkSubjectByTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
