﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ReportSupervisingDeptArea.Models
{
    public class ReportSupervisingDeptViewModel
    {
        #region Grid DK
        public string[] lstEduOfSchool { get; set; }
        public string[,] lstSubjectOfEdu { get; set; }
        [ResourceDisplayName("ReportSupervisingDept_Label_Capacity")]
        public string DisplayHL { get; set; }
        [ResourceDisplayName("ReportSupervisingDept_Label_Conduct")]
        public string DisplayHK { get; set; }
        public string[] lstSubject { get; set; }
        #endregion
        public int? Semester { get; set; }
        [ResourceDisplayName("ReportSupervisingDept_Label_SchoolName")]
        public string SchoolName { get; set; }
        [ResourceDisplayName("ReportSupervisingDept_Label_EducationName")]
        public string EducationName { get; set; }
        [ResourceDisplayName("ReportSupervisingDept_Label_SubjectName")]
        public string SubjectName { get; set; }
        public int SubjectID { get; set; }
        public int SchoolID { get; set; }
        public int? EducationLevelID { get; set; }
    }
}