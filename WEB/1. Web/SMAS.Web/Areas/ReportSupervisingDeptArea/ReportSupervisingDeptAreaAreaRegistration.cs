﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportSupervisingDeptArea
{
    public class ReportSupervisingDeptAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportSupervisingDeptArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportSupervisingDeptArea_default",
                "ReportSupervisingDeptArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
