﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportSupervisingDeptArea
{
    public class ReportSupervisingDeptConstants 
    {

        public const string LIST_DISTRICT = "list_district";
        public const string LIST_ACADEMIC_YEAR = "list_academic_year";
        public const string LIST_SEMESTER = "list_semester";
        public const string LIST_EDUCATION_LEVEL = "list_education_level";

        public const string LIST_DINHKY = "list_capacitystatistic";
        public const string LIST_HOCKY = "list_capacitystatisticdistrict";
        public const string LIST_TBM = "listSchool";

        public const string GRID_REPORT_DATA = "grid_report_data";
        public const string GRID_REPORT_COMITTEE = "grid_report_comittee";
        public const string GRID_REPORT_COMITTEE_EDUCATIONLEVEL = "grid_report_comittee_educationlevel";
        public const string GRID_REPORT_ID = "grid_report_id";
        
        public const string FILE_ID = "fileid";
        public const string DISABLE_COMBOBOX = "disablecbb";
        public const string LIST_Applied_Level = "list_AL";
        public const string LIST_TYPESTATICS = "list";
        public const string LIST_SUBJECT = "listSubject";
    }
}
