﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.ReportSupervisingDeptArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.ReportSupervisingDeptArea.Controllers
{
    public class ReportSupervisingDeptController : Controller
    {
        IDistrictBusiness DistrictBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        ITrainingTypeBusiness TrainingTypeBusiness;
        ISubCommitteeBusiness SubCommitteeBusiness;
        ICapacityStatisticBusiness CapacityStatisticBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IConductStatisticBusiness ConductStatisticBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        IProvinceSubjectBusiness ProvinceSubjectBusiness;
        ISupervisingDeptBusiness SupervisingDeptBusiness;
        ISchoolProfileBusiness SchoolProfileBusiness;
        IMarkStatisticBusiness MarkStatisticBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        GlobalInfo global = new GlobalInfo();
        public ReportSupervisingDeptController(IMarkStatisticBusiness MarkStatisticBusiness, ISchoolProfileBusiness SchoolProfileBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness, IProvinceSubjectBusiness ProvinceSubjectBusiness, IDistrictBusiness districtBusiness,
                                                            IAcademicYearBusiness academicYearBusiness,
                                                            ITrainingTypeBusiness trainingTypeBusiness,
                                                            IReportDefinitionBusiness ReportDefinitionBusiness,
                                                            IEducationLevelBusiness educationlevelBusiness,
                                                            ISubCommitteeBusiness subCommitteeBusiness,
            ICapacityStatisticBusiness capacityStatisticBusiness, IProcessedReportBusiness processedReportBusiness,
            IConductStatisticBusiness conductStatisticBusiness)
        {
            this.DistrictBusiness = districtBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.EducationLevelBusiness = educationlevelBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
            this.ProvinceSubjectBusiness = ProvinceSubjectBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.MarkStatisticBusiness = MarkStatisticBusiness;
        }


        private void GetViewData()
        {
            ViewData[ReportSupervisingDeptConstants.DISABLE_COMBOBOX] = false;
            GlobalInfo glo = new GlobalInfo();
            //Loại báo cáo:
            //- Thống kê điểm kiểm tra định kỳ. Mặc định
            //- Thống kê điểm kiểm tra học kỳ
            //- Thống kê TBM, học lực, hạnh kiểm. Chỉ hiển thị nếu là account Sở

            List<ViettelCheckboxList> lsType = new List<ViettelCheckboxList>();
            lsType.Add(new ViettelCheckboxList { Label = @Res.Get("ReportSupervisingDept_Label_DK"), Value = 1, cchecked = true, disabled = false });
            lsType.Add(new ViettelCheckboxList { Label = @Res.Get("ReportSupervisingDept_Label_HK"), Value = 2, cchecked = false, disabled = false });

            List<int> listAcademicYear = AcademicYearBusiness.GetListYearForSupervisingDept(glo.SupervisingDeptID.Value);
            ViewData[ReportSupervisingDeptConstants.LIST_ACADEMIC_YEAR] = listAcademicYear.Select(u => new SelectListItem { Text = u + " - " + (u + 1), Value = u.ToString(), Selected = false }).ToList();

            ViewData[ReportSupervisingDeptConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");

            List<ComboObject> ListAppliedLevel = new List<ComboObject>();
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("Common_Label_Primary")));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("Common_Label_Secondary")));


            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_PRIMARY).ToList();
            ViewData[ReportSupervisingDeptConstants.LIST_EDUCATION_LEVEL] = new SelectList(listEducationLevel, "EducationLevelID", "Resolution");
            // cboAppliedLevel: 
            //- Tiểu học (Mặc định)
            //- THCS
            //- THPT. Chỉ hiển thị nếu là account Sở. UserInfo.IsSupervisingDeptRole() = true

            if (glo.IsSuperVisingDeptRole)
            {
                List<District> listDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "ProvinceID", glo.ProvinceID.Value }, { "IsActive", true } }).ToList();
                ViewData[ReportSupervisingDeptConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName");
                lsType.Add(new ViettelCheckboxList { Label = @Res.Get("ReportSupervisingDept_Label_TBM"), Value = 3, cchecked = false, disabled = false });
                ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("Common_Label_Tertiary")));
            }
            //Neu la account phong thi disable cbb district
            else if (glo.IsSubSuperVisingDeptRole)
            {
                District District = DistrictBusiness.Find(glo.DistrictID);
                List<District> listDistrict = new List<SMAS.Models.Models.District>();
                listDistrict.Add(District);
                ViewData[ReportSupervisingDeptConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName", glo.DistrictID);
                ViewData[ReportSupervisingDeptConstants.DISABLE_COMBOBOX] = true;
            }
            ViewData[ReportSupervisingDeptConstants.LIST_TYPESTATICS] = lsType;
            ViewData[ReportSupervisingDeptConstants.LIST_Applied_Level] = new SelectList(ListAppliedLevel, "key", "value", SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString());
            ViewData[ReportSupervisingDeptConstants.LIST_DINHKY] = new List<ReportSupervisingDeptViewModel>();
            ViewData[ReportSupervisingDeptConstants.LIST_HOCKY] = new List<ReportSupervisingDeptViewModel>();
            ViewData[ReportSupervisingDeptConstants.LIST_TBM] = new List<ReportSupervisingDeptViewModel>();
            ViewData[ReportSupervisingDeptConstants.LIST_SUBJECT] = new List<SubjectCat>();
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        public JsonResult _LoadAppliedLevel()
        {
            List<ComboObject> ListAppliedLevel = new List<ComboObject>();
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("Common_Label_Secondary")));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("Common_Label_Tertiary")));
            return Json(new SelectList(ListAppliedLevel, "key", "value", SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString()));
        }

        public JsonResult _LoadAppliedLevelFor23()
        {
            List<ComboObject> ListAppliedLevel = new List<ComboObject>();
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("Common_Label_Primary")));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("Common_Label_Secondary")));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("Common_Label_Tertiary")));
            return Json(new SelectList(ListAppliedLevel, "key", "value", SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString()));
        }


        [ValidateAntiForgeryToken]
        public JsonResult _LoadEducationLevel(int AppliedLevel)
        {
            List<EducationLevel> ls = EducationLevelBusiness.GetByGrade(AppliedLevel).ToList();
            return Json(new SelectList(ls, "EducationLevelID", "Resolution"));
        }


        [ValidateAntiForgeryToken]
        public JsonResult _LoadSemester(int AppliedLevel)
        {
            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                return Json(new SelectList(CommonList.Semester(), "key", "value"));
            }
            else
            {
                return Json(new SelectList(CommonList.SemesterAndAll(), "key", "value"));
            }

        }

        public ActionResult Search(SearchViewModel model, FormCollection col)
        {
            List<ReportSupervisingDeptViewModel> lst = new List<ReportSupervisingDeptViewModel>();
            SupervisingDept sd = SupervisingDeptBusiness.Find(global.SupervisingDeptID);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = model.AcademicYearID;
            dic["AppliedLevel"] = model.AppliedLevel;
            dic["EducationLevelID"] = model.EducationLevelID;
            IQueryable<ProvinceSubject> iqSubject = ProvinceSubjectBusiness.SearchByProvince(global.ProvinceID.Value, dic);
            List<SubjectCat> lstSubject = iqSubject.Select(o => o.SubjectCat).Distinct().ToList();
            ViewData[ReportSupervisingDeptConstants.LIST_SUBJECT] = lstSubject;
            Dictionary<string, object> dicSchool = new Dictionary<string, object>();
            dicSchool["TraversalPath"] = sd.TraversalPath;
            dicSchool["ProvinceID"] = global.ProvinceID;
            dicSchool["DistrictID"] = model.DistrictID;
            dicSchool["AppliedLevel"] = model.AppliedLevel;
            List<SchoolProfile> lstSchool = SchoolProfileBusiness.Search(dicSchool).ToList();
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            if (model.EducationLevelID != null && model.EducationLevelID != 0)
            {
                EducationLevel edu = EducationLevelBusiness.Find(model.EducationLevelID);
                lstEducationLevel.Add(edu);
            }
            else
            {
                lstEducationLevel = EducationLevelBusiness.GetByGrade(model.AppliedLevel).ToList();
            }
            int reportType = int.Parse(col["ReportType"]);
            Session["AppliedLevel"] = model.AppliedLevel;
            Session["Year"] = model.AcademicYearID;
            Session["EducationLevelID"] = model.EducationLevelID;
            Session["Semester"] = model.Semester;
            Session["DistrictID"] = model.DistrictID != 0 ? 0 : model.DistrictID.Value;
            Session["reportType"] = reportType;
            Session["lstSchool"] = lstSchool;
            Session["lstEducationLevel"] = lstEducationLevel;
            Session["lstSubject"] = lstSubject;
            if (reportType == 1)
            {
                Dictionary<string, object> dicMark = new Dictionary<string, object>();
                dicMark["Year"] = model.AcademicYearID;
                dicMark["AppliedLevel"] = model.AppliedLevel;
                dicMark["EducationLevelID"] = model.EducationLevelID;
                dicMark["Semester"] = model.Semester;
                dicMark["ReportCode"] = "ThongKeDiemKiemTraDinhKy";
                List<MarkStatistic> lstMarkStatistic = MarkStatisticBusiness.Search(dicMark).ToList();
                List<EducationLevel> qEdu = EducationLevelBusiness.All.ToList();
                List<SchoolProfile> qSchool = SchoolProfileBusiness.All.ToList();
                for (int i = 0; i < lstSchool.Count; i++)
                {
                    int schoolID = lstSchool[i].SchoolProfileID;
                    string SchoolName = qSchool.Where(o => o.SchoolProfileID == schoolID).FirstOrDefault().SchoolName;
                    for (int j = 0; j < lstEducationLevel.Count; j++)
                    {
                        int eduID = lstEducationLevel[j].EducationLevelID;
                        ReportSupervisingDeptViewModel item = new ReportSupervisingDeptViewModel();
                        item.SchoolID = lstSchool[i].SchoolProfileID;
                        item.SchoolName = SchoolName;
                        item.EducationName = qEdu.Where(o => o.EducationLevelID == eduID).FirstOrDefault().Resolution;
                        item.lstSubject = new string[lstSubject.Count];
                        for (int k = 0; k < lstSubject.Count; k++)
                        {
                            int subjectID = lstSubject[k].SubjectCatID;
                            MarkStatistic mark = lstMarkStatistic.Where(o => o.SubjectID == subjectID && o.EducationLevelID == eduID && o.SchoolID == schoolID).FirstOrDefault();
                            if (mark != null)
                            {
                                item.lstSubject[k] = mark.ProcessedDate.ToString().Substring(0, 10);
                            }
                            else
                            {
                                item.lstSubject[k] = "x";
                            }
                        }
                        lst.Add(item);
                    }
                }
                ViewData[ReportSupervisingDeptConstants.LIST_DINHKY] = lst;
                Session["lstViewModel"] = lst;
               
                return PartialView("_ListDK");
            }
            else if (reportType == 2)
            {
                Dictionary<string, object> dicMark = new Dictionary<string, object>();
                dicMark["Year"] = model.AcademicYearID;
                dicMark["AppliedLevel"] = model.AppliedLevel;
                dicMark["EducationLevelID"] = model.EducationLevelID;
                dicMark["Semester"] = model.Semester;
                dicMark["ReportCode"] = "ThongKeDiemKiemTraHocKy";
                List<MarkStatistic> lstMarkStatistic = MarkStatisticBusiness.Search(dicMark).ToList();
                List<EducationLevel> qEdu = EducationLevelBusiness.All.ToList();
                List<SchoolProfile> qSchool = SchoolProfileBusiness.All.ToList();
                if (model.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || model.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    for (int i = 0; i < lstSchool.Count; i++)
                    {
                        int schoolID = lstSchool[i].SchoolProfileID;
                        string SchoolName = qSchool.Where(o => o.SchoolProfileID == schoolID).FirstOrDefault().SchoolName;
                        for (int j = 0; j < lstEducationLevel.Count; j++)
                        {
                            int eduID = lstEducationLevel[j].EducationLevelID;
                            ReportSupervisingDeptViewModel item = new ReportSupervisingDeptViewModel();
                            item.SchoolID = lstSchool[i].SchoolProfileID;
                            item.SchoolName = SchoolName;
                            item.EducationName = qEdu.Where(o => o.EducationLevelID == eduID).FirstOrDefault().Resolution;
                            item.lstSubject = new string[lstSubject.Count];
                            for (int k = 0; k < lstSubject.Count; k++)
                            {
                                int subjectID = lstSubject[k].SubjectCatID;
                                MarkStatistic mark = lstMarkStatistic.Where(o => o.SubjectID == subjectID && o.EducationLevelID == eduID && o.SchoolID == schoolID).FirstOrDefault();
                                if (mark != null)
                                {
                                    item.lstSubject[k] = mark.ProcessedDate.ToString().Substring(0, 10);
                                }
                                else
                                {
                                    item.lstSubject[k] = "x";
                                }
                            }
                            lst.Add(item);
                        }
                    }
                    ViewData[ReportSupervisingDeptConstants.LIST_HOCKY] = lst;
                    Session["lstViewModel"] = lst;
                    return PartialView("_ListHK23");
                }
                else
                {
                    Dictionary<string, object> dicHL = new Dictionary<string, object>();
                    dicHL["Year"] = model.AcademicYearID;
                    dicHL["AppliedLevel"] = model.AppliedLevel;
                    dicHL["EducationLevelID"] = model.EducationLevelID;
                    dicHL["Semester"] = model.Semester;
                    dicHL["ReportCode"] = "ThongKeHocLucMon";
                    List<CapacityStatistic> lstCapacitySubjectStatistic = CapacityStatisticBusiness.Search(dic).ToList();
                    for (int i = 0; i < lstSchool.Count; i++)
                    {
                        int schoolID = lstSchool[i].SchoolProfileID;
                        string SchoolName = qSchool.Where(o => o.SchoolProfileID == schoolID).FirstOrDefault().SchoolName;
                        for (int j = 0; j < lstEducationLevel.Count; j++)
                        {
                            int eduID = lstEducationLevel[j].EducationLevelID;
                            ReportSupervisingDeptViewModel item = new ReportSupervisingDeptViewModel();
                            item.SchoolID = lstSchool[i].SchoolProfileID;
                            item.SchoolName = SchoolName;
                            item.EducationName = qEdu.Where(o => o.EducationLevelID == eduID).FirstOrDefault().Resolution;

                            item.lstSubject = new string[lstSubject.Count * 2];
                            int index = 0;
                            for (int k = 0; k < lstSubject.Count; k++)
                            {
                                int subjectID = lstSubject[k].SubjectCatID;
                                MarkStatistic mark = lstMarkStatistic.Where(o => o.SubjectID == subjectID && o.EducationLevelID == eduID && o.SchoolID == schoolID).FirstOrDefault();
                                if (mark != null)
                                {
                                    item.lstSubject[index] = mark.ProcessedDate.ToString().Substring(0, 10);
                                }
                                else
                                {
                                    item.lstSubject[index] = "x";
                                }
                                index++;
                                CapacityStatistic CapacityStatistic = lstCapacitySubjectStatistic.Where(o => o.SubjectID == subjectID && o.EducationLevelID == eduID && o.SchoolID == schoolID).FirstOrDefault();
                                if (CapacityStatistic != null)
                                {
                                    item.lstSubject[index] = CapacityStatistic.SentDate.ToString().Substring(0, 10);
                                }
                                else
                                {
                                    item.lstSubject[index] = "x";
                                }
                                index++;
                            }
                            lst.Add(item);
                        }
                    }
                    ViewData[ReportSupervisingDeptConstants.LIST_HOCKY] = lst;
                    Session["lstViewModel"] = lst;
                    return PartialView("_ListHK");
                }
            }
            else
            {
                Dictionary<string, object> dicMark = new Dictionary<string, object>();
                dicMark["Year"] = model.AcademicYearID;
                dicMark["AppliedLevel"] = model.AppliedLevel;
                dicMark["EducationLevelID"] = model.EducationLevelID;
                dicMark["Semester"] = model.Semester;
                dicMark["ReportCode"] = "ThongKeHocLucMon";
                List<CapacityStatistic> lstCapacity = CapacityStatisticBusiness.Search(dicMark).ToList();

                Dictionary<string, object> dicMarkHL = new Dictionary<string, object>();
                dicMarkHL["Year"] = model.AcademicYearID;
                dicMarkHL["AppliedLevel"] = model.AppliedLevel;
                dicMarkHL["EducationLevelID"] = model.EducationLevelID;
                dicMarkHL["Semester"] = model.Semester;
                dicMarkHL["ReportCode"] = "ThongKeHocLuc";
                List<CapacityStatistic> lstCapacityHL = CapacityStatisticBusiness.Search(dicMarkHL).ToList();

                Dictionary<string, object> dicMarkHK = new Dictionary<string, object>();
                dicMarkHK["Year"] = model.AcademicYearID;
                dicMarkHK["AppliedLevel"] = model.AppliedLevel;
                dicMarkHK["EducationLevelID"] = model.EducationLevelID;
                dicMarkHK["Semester"] = model.Semester;
                dicMarkHK["ReportCode"] = "ThongKeHanhKiem";
                List<ConductStatistic> lstConduct = ConductStatisticBusiness.Search(dicMarkHK).ToList();
                List<EducationLevel> qEdu = EducationLevelBusiness.All.ToList();
                List<SchoolProfile> qSchool = SchoolProfileBusiness.All.ToList();
                for (int i = 0; i < lstSchool.Count; i++)
                {
                    int schoolID = lstSchool[i].SchoolProfileID;
                    string SchoolName = qSchool.Where(o => o.SchoolProfileID == schoolID).FirstOrDefault().SchoolName;
                    for (int j = 0; j < lstEducationLevel.Count; j++)
                    {
                        int eduID = lstEducationLevel[j].EducationLevelID;
                        ReportSupervisingDeptViewModel item = new ReportSupervisingDeptViewModel();
                        item.SchoolID = lstSchool[i].SchoolProfileID;
                        item.SchoolName = SchoolName;
                        item.EducationName = qEdu.Where(o => o.EducationLevelID == eduID).FirstOrDefault().Resolution;
                        item.lstSubject = new string[lstSubject.Count];
                        //TBM
                        for (int k = 0; k < lstSubject.Count; k++)
                        {
                            int subjectID = lstSubject[k].SubjectCatID;

                            CapacityStatistic mark = lstCapacity.Where(o => o.SubjectID == subjectID && o.EducationLevelID == eduID && o.SchoolID == schoolID).FirstOrDefault();
                            if (mark != null)
                            {
                                item.lstSubject[k] = mark.SentDate.ToString().Substring(0, 10);
                            }
                            else
                            {
                                item.lstSubject[k] = "x";
                            }
                        }

                        //Hoc Luc
                        CapacityStatistic capacity = lstCapacityHL.Where(o => o.EducationLevelID == eduID && o.SchoolID == schoolID).FirstOrDefault();
                        if (capacity != null)
                        {
                            item.DisplayHL = capacity.SentDate.ToString().Substring(0, 10);
                        }
                        else
                        {
                            item.DisplayHL = "x";
                        }
                        //Hanh Kiem
                        ConductStatistic conduct = lstConduct.Where(o => o.EducationLevelID == eduID && o.SchoolID == schoolID).FirstOrDefault();
                        if (conduct != null)
                        {
                            item.DisplayHK = conduct.SentDate.ToString().Substring(0, 10);
                        }
                        else
                        {
                            item.DisplayHK = "x";
                        }
                        lst.Add(item);
                    }
                }
                ViewData[ReportSupervisingDeptConstants.LIST_TBM] = lst;
                Session["lstViewModel"] = lst;
                return PartialView("_ListTBM");
            }
        }

        public FileResult DownloadReport()
        {
            List<SchoolProfile> lstSchool = (List<SchoolProfile >)Session["lstSchool"];
            List<EducationLevel> lstEducationLevel = (List<EducationLevel>)Session["lstEducationLevel"];
            List<SubjectCat> lstSubject = (List<SubjectCat>)Session["lstSubject"];
            List<ReportSupervisingDeptViewModel> lstViewModel = (List<ReportSupervisingDeptViewModel>)Session["lstViewModel"];
            int reportType =  (int)Session["reportType"] ;
            int AppliedLevel =  (int)Session["AppliedLevel"];
            int EducationLevelID = Session["EducationLevelID"] == null ? 0 : (int)Session["EducationLevelID"];
            int DistrictID = (int)Session["DistrictID"];
            int Year = (int)Session["Year"];
            int Semester = (int)Session["Semester"];
            GlobalInfo Global = new GlobalInfo();

            AcademicYear ay = AcademicYearBusiness.All.Where(o => o.Year == Year && o.IsActive == true).FirstOrDefault();
            SupervisingDept sd = SupervisingDeptBusiness.Find(Global.SupervisingDeptID);
            string DistrictName = "";
            if (DistrictID == 0)
            {
                DistrictName = "Tất Cả";

            }
            else
            {
                DistrictName = DistrictBusiness.Find(DistrictID).DistrictName;
            }

            string AppliedLevelName = "";
            if (AppliedLevel == 1)
            {
                AppliedLevelName = "Tiểu học";
            }
            if (AppliedLevel == 2)
            {
                AppliedLevelName = "THCS";
            }
            if (AppliedLevel == 3)
            {
                AppliedLevelName = "THPT";
            }

            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string educationlevel = EducationLevelID != 0 ? ("Khối " + EducationLevelID) : "Tất Cả";
           
            string SemesterName = "";
            if(Semester == 1)
            {
            SemesterName = "Học kỳ I";
            }
            if(Semester == 2)
            {
            SemesterName = "Học kỳ II";
            }
            if(Semester == 1)
            {
            SemesterName = "Cả Năm";
            }

            if (reportType == 1)
            {
                #region Cấp 1
                if (AppliedLevel == 1)
                {

                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_TK_DIEMKTDK_TH);
                    //Tạo tên file [SoGD/PGD]_TH_TKDonViBCDiemKTDinhKy _[Khối]_[Học kỳ]
                    string outputNamePattern = reportDef.OutputNamePattern;
                    outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                    outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationlevel));
                    string ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                    string templatePath = ReportUtils.GetTemplatePath(reportDef);
                    IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                    IVTWorksheet Template = oBook.GetSheet(1);

                    IVTWorksheet Sheet = oBook.GetSheet(2);
                    Sheet.CopyPasteSameSize(Template.GetRange("A1", "N6"), 1, 1);

                    Sheet.SetCellValue("A2", sd.SupervisingDeptName.ToUpper());
                    Sheet.SetCellValue("A3", "TÌNH HÌNH BÁO CÁO ĐIỂM KIẾM TRA ĐỊNH KỲ CỦA CÁC ĐƠN VỊ " + SemesterName.ToUpper() + " NĂM HỌC " + ay.DisplayTitle.ToUpper());
                    Sheet.SetCellValue("A4", "Quận/huyện: " + DistrictName + ", cấp trường: " + AppliedLevelName + ", khối: " + educationlevel);

                    // Tạo header template
                    IVTRange rangeHeader = Template.GetRange("D6", "D6");
                    int startrow = 7;
                    int startcolhearder = 4;

                    foreach (var itemSubject in lstSubject)
                    {

                        Sheet.CopyPasteSameSize(rangeHeader, 6, startcolhearder);
                        Sheet.SetCellValue(6, startcolhearder, itemSubject.SubjectName);
                        startcolhearder++;
                    }

                    // fill dữ liệu vào excel
                    IVTRange range = Template.GetRange("C8", "C8");
                    IVTRange rangelast = Template.GetRange("C11", "C11");
                    IVTRange rangeSchool = Template.GetRange("B8", "B8");
                    IVTRange rangelastSchool = Template.GetRange("B11", "B11");

                    int n = 0;
                    int startrowSchool = 0;
                    foreach (var item in lstSchool)
                    {
                        int schoolid = item.SchoolProfileID;
                        startrowSchool = startrow;
                        List<ReportSupervisingDeptViewModel> lstviewmodel = lstViewModel.Where(o => o.SchoolID == schoolid).ToList();
                        Sheet.CopyPasteSameSize(rangeSchool, startrow, 2);
                        Sheet.SetCellValue(startrow, 2, item.SchoolName);

                        for (int i = 0; i < lstviewmodel.Count(); i++)
                        {
                            n++;
                            IVTRange rangeExcel = null;
                            if (i == lstviewmodel.Count() - 1)
                            {

                                rangeExcel = rangelast;
                                Sheet.CopyPasteSameSize(rangelastSchool, startrow, 2);
                            }
                            else
                            {

                                rangeExcel = range;
                            }
                            Sheet.CopyPasteSameSize(rangeExcel, startrow, 1);
                            Sheet.SetCellValue(startrow, 1, n);
                            Sheet.CopyPasteSameSize(rangeExcel, startrow, 3);
                            Sheet.SetCellValue(startrow, 3, lstviewmodel[i].EducationName);
                            int startcol = 4;
                            for (int j = 0; j < lstSubject.Count(); j++)
                            {

                                Sheet.CopyPasteSameSize(rangeExcel, startrow, startcol);
                                Sheet.SetCellValue(startrow, startcol, lstviewmodel[i].lstSubject[j]);
                                startcol++;
                            }
                            startrow++;
                        }

                        Sheet.MergeColumn(2, startrowSchool, startrow -1);

                    }

                    Sheet.SetCellValue(startrow + 1, 1, "Ghi chú:  ô có chữ x là khối học không học môn học đó, ô trống là đơn vị chưa báo cáo, ô ghi ngày tháng là ngày gần nhất đơn vị báo cáo");

                    Template.Delete();
                    Stream excel = oBook.ToStream();
                    FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                    result.FileDownloadName = ReportName;
                    return result;

                }

                #endregion

                #region Câp 2 3
                else
                {

                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_TK_DIEMKTDK_THCS);
                    //Tạo tên file  [SoGD/PGD]_[SchoolLevel]_TKDonViBCDiemKTDinhKy _[Khối]_[Học kỳ]
                    string outputNamePattern = reportDef.OutputNamePattern;
                    outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                    outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationlevel));
                    outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.StripVNSign(AppliedLevelName));
                    string ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                    string templatePath = ReportUtils.GetTemplatePath(reportDef);
                    IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                    IVTWorksheet Template = oBook.GetSheet(1);

                    IVTWorksheet Sheet = oBook.GetSheet(2);
                    Sheet.CopyPasteSameSize(Template.GetRange("A1", "C6"), 1, 1);

                    Sheet.SetCellValue("A2", sd.SupervisingDeptName.ToUpper());
                    Sheet.SetCellValue("A3", "TÌNH HÌNH BÁO CÁO ĐIỂM KIẾM TRA ĐỊNH KỲ CỦA CÁC ĐƠN VỊ " + SemesterName.ToUpper() + " NĂM HỌC " + ay.DisplayTitle.ToUpper());
                    Sheet.SetCellValue("A4", "Quận/huyện: " + DistrictName + ", cấp trường: " + AppliedLevelName + ", khối: " + educationlevel);

                    // Tạo header template
                    IVTRange rangeHeader = Template.GetRange("D6", "D6");
                    int startrow = 7;
                    int startcolhearder = 4;

                    foreach (var itemSubject in lstSubject)
                    {

                        Sheet.CopyPasteSameSize(rangeHeader, 6, startcolhearder);
                        Sheet.SetCellValue(6, startcolhearder, itemSubject.SubjectName);
                        startcolhearder++;
                    }

                    // fill dữ liệu vào excel
                    IVTRange range = Template.GetRange("C8", "C8");
                    IVTRange rangelast = Template.GetRange("C11", "C11");
                    IVTRange rangeSchool = Template.GetRange("B8", "B8");
                    IVTRange rangelastSchool = Template.GetRange("B11", "B11");

                    int n = 0;
                    int startrowSchool = 0;
                    foreach (var item in lstSchool)
                    {
                        int schoolid = item.SchoolProfileID;
                        startrowSchool = startrow;
                        List<ReportSupervisingDeptViewModel> lstviewmodel = lstViewModel.Where(o => o.SchoolID == schoolid).ToList();
                        Sheet.CopyPasteSameSize(rangeSchool, startrow, 2);
                        Sheet.SetCellValue(startrow, 2, item.SchoolName);

                        for (int i = 0; i < lstviewmodel.Count(); i++)
                        {
                            n++;
                            IVTRange rangeExcel = null;
                            if (i == lstviewmodel.Count() - 1)
                            {

                                rangeExcel = rangelast;
                                Sheet.CopyPasteSameSize(rangelastSchool, startrow, 2);
                            }
                            else
                            {

                                rangeExcel = range;
                            }
                            Sheet.CopyPasteSameSize(rangeExcel, startrow, 1);
                            Sheet.SetCellValue(startrow, 1, n);
                            Sheet.CopyPasteSameSize(rangeExcel, startrow, 3);
                            Sheet.SetCellValue(startrow, 3, lstviewmodel[i].EducationName);
                            int startcol = 4;
                            for (int j = 0; j < lstSubject.Count(); j++)
                            {

                                Sheet.CopyPasteSameSize(rangeExcel, startrow, startcol);
                                Sheet.SetCellValue(startrow, startcol, lstviewmodel[i].lstSubject[j]);
                                startcol++;
                            }
                            startrow++;
                        }

                        Sheet.MergeColumn(2, startrowSchool, startrow - 1);

                    }

                    Sheet.SetCellValue(startrow + 1, 1, "Ghi chú:  ô có chữ x là khối học không học môn học đó, ô trống là đơn vị chưa báo cáo, ô ghi ngày tháng là ngày gần nhất đơn vị báo cáo");


                    Template.Delete();
                    Stream excel = oBook.ToStream();
                    FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                    result.FileDownloadName = ReportName;
                    return result;
                }

                #endregion

            }

            if (reportType == 2)
            {

                #region Cấp 1
                if (AppliedLevel == 1)
                {

                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_TK_DIEMKTHK_TH);
                    //Tạo tên file SoGD/PGD]_TH_ TKDonViBCDiemKTHocKy _[Khối]_[Học kỳ]
                    string outputNamePattern = reportDef.OutputNamePattern;
                    outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                    outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationlevel));
                    string ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                    string templatePath = ReportUtils.GetTemplatePath(reportDef);
                    IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                    IVTWorksheet Template = oBook.GetSheet(1);

                    IVTWorksheet Sheet = oBook.GetSheet(2);
                    Sheet.CopyPasteSameSize(Template.GetRange("A1", "E7"), 1, 1);

                    Sheet.SetCellValue("A2", sd.SupervisingDeptName.ToUpper());
                    Sheet.SetCellValue("A3", "TÌNH HÌNH BÁO CÁO ĐIỂM KIẾM TRA HỌC KỲ CỦA CÁC ĐƠN VỊ " + SemesterName.ToUpper() + " NĂM HỌC " + ay.DisplayTitle.ToUpper());
                    Sheet.SetCellValue("A4", "Quận/huyện: " + DistrictName + ", cấp trường: " + AppliedLevelName + ", khối: " + educationlevel);

                    // Tạo header template
                    IVTRange rangeHeader = Template.GetRange("D6", "D6");
                    IVTRange rangeHeaderLast = Template.GetRange("D7", "D7");
                    IVTRange rangeHeaderButtom = Template.GetRange("D7", "D7");
                    int startrow = 8;
                    int startcolhearder = 4;

                    foreach (var itemSubject in lstSubject)
                    {

                        Sheet.CopyPasteSameSize(rangeHeader, 6, startcolhearder);
                        Sheet.CopyPasteSameSize(rangeHeaderLast, 6, startcolhearder + 1);
                        Sheet.SetCellValue(6, startcolhearder, itemSubject.SubjectName);
                        Sheet.CopyPasteSameSize(rangeHeaderButtom, 7, startcolhearder);
                        Sheet.SetCellValue(7, startcolhearder, "KTHK");
                        Sheet.CopyPasteSameSize(rangeHeaderButtom, 7, startcolhearder + 1);
                        Sheet.SetCellValue(7, startcolhearder + 1, "HLM");
                        startcolhearder = startcolhearder + 2;
                    }

                    // fill dữ liệu vào excel
                    IVTRange range = Template.GetRange("C9", "C9");
                    IVTRange rangelast = Template.GetRange("C12", "C12");
                    IVTRange rangeSchool = Template.GetRange("B9", "B9");
                    IVTRange rangelastSchool = Template.GetRange("B12", "B12");

                    int n = 0;
                    int startrowSchool = 0;
                    foreach (var item in lstSchool)
                    {
                        int schoolid = item.SchoolProfileID;
                        startrowSchool = startrow;
                        List<ReportSupervisingDeptViewModel> lstviewmodel = lstViewModel.Where(o => o.SchoolID == schoolid).ToList();
                        Sheet.CopyPasteSameSize(rangeSchool, startrow, 2);
                        Sheet.SetCellValue(startrow, 2, item.SchoolName);

                        for (int i = 0; i < lstviewmodel.Count(); i++)
                        {
                            n++;
                            IVTRange rangeExcel = null;
                            if (i == lstviewmodel.Count() - 1)
                            {

                                rangeExcel = rangelast;
                                Sheet.CopyPasteSameSize(rangelastSchool, startrow, 2);
                            }
                            else
                            {

                                rangeExcel = range;
                            }
                            Sheet.CopyPasteSameSize(rangeExcel, startrow, 1);
                            Sheet.SetCellValue(startrow, 1, n);
                            Sheet.CopyPasteSameSize(rangeExcel, startrow, 3);
                            Sheet.SetCellValue(startrow, 3, lstviewmodel[i].EducationName);
                            int startcol = 4;
                            for (int j = 0; j < lstSubject.Count() * 2; j++)
                            {

                                Sheet.CopyPasteSameSize(rangeExcel, startrow, startcol);
                                Sheet.SetCellValue(startrow, startcol, lstviewmodel[i].lstSubject[j]);
                                startcol++;
                            }
                            startrow++;
                        }

                        Sheet.MergeColumn(2, startrowSchool, startrow - 1);

                    }

                    Sheet.SetCellValue(startrow + 1, 1, "Ghi chú:  ô có chữ x là khối học không học môn học đó, ô trống là đơn vị chưa báo cáo, ô ghi ngày tháng là ngày gần nhất đơn vị báo cáo");

                    Template.Delete();
                    Stream excel = oBook.ToStream();
                    FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                    result.FileDownloadName = ReportName;
                    return result;

                }

                #endregion

                #region Câp 2 3
                else
                {

                    ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_TK_DIEMKTHK_THCS);
                    //Tạo tên file : [SoGD/PGD]_[SchoolLevel]_TKDonViBCDiemKTHocKy _[Khối]_[Học kỳ]
                    string outputNamePattern = reportDef.OutputNamePattern;
                    outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                    outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationlevel));
                    outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.StripVNSign(AppliedLevelName));
                    string ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                    string templatePath = ReportUtils.GetTemplatePath(reportDef);
                    IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                    IVTWorksheet Template = oBook.GetSheet(1);

                    IVTWorksheet Sheet = oBook.GetSheet(2);
                    Sheet.CopyPasteSameSize(Template.GetRange("A1", "C6"), 1, 1);

                    Sheet.SetCellValue("A2", sd.SupervisingDeptName.ToUpper());
                    Sheet.SetCellValue("A3", "TÌNH HÌNH BÁO CÁO ĐIỂM KIẾM TRA ĐỊNH KỲ CỦA CÁC ĐƠN VỊ " + SemesterName.ToUpper() + " NĂM HỌC " + ay.DisplayTitle.ToUpper());
                    Sheet.SetCellValue("A4", "Quận/huyện: " + DistrictName + ", cấp trường: " + AppliedLevelName + ", khối: " + educationlevel);

                    // Tạo header template
                    IVTRange rangeHeader = Template.GetRange("D6", "D6");
                    int startrow = 7;
                    int startcolhearder = 4;

                    foreach (var itemSubject in lstSubject)
                    {

                        Sheet.CopyPasteSameSize(rangeHeader, 6, startcolhearder);
                        Sheet.SetCellValue(6, startcolhearder, itemSubject.SubjectName);
                        startcolhearder++;
                    }

                    // fill dữ liệu vào excel
                    IVTRange range = Template.GetRange("C8", "C8");
                    IVTRange rangelast = Template.GetRange("C10", "C10");
                    IVTRange rangeSchool = Template.GetRange("B8", "B8");
                    IVTRange rangelastSchool = Template.GetRange("B10", "B10");

                    int n = 0;
                    int startrowSchool = 0;
                    foreach (var item in lstSchool)
                    {
                        int schoolid = item.SchoolProfileID;
                        startrowSchool = startrow;
                        List<ReportSupervisingDeptViewModel> lstviewmodel = lstViewModel.Where(o => o.SchoolID == schoolid).ToList();
                        Sheet.CopyPasteSameSize(rangeSchool, startrow, 2);
                        Sheet.SetCellValue(startrow, 2, item.SchoolName);

                        for (int i = 0; i < lstviewmodel.Count(); i++)
                        {
                            n++;
                            IVTRange rangeExcel = null;
                            if (i == lstviewmodel.Count() - 1)
                            {

                                rangeExcel = rangelast;
                                Sheet.CopyPasteSameSize(rangelastSchool, startrow, 2);
                            }
                            else
                            {

                                rangeExcel = range;
                            }
                            Sheet.CopyPasteSameSize(rangeExcel, startrow, 1);
                            Sheet.SetCellValue(startrow, 1, n);
                            Sheet.CopyPasteSameSize(rangeExcel, startrow, 3);
                            Sheet.SetCellValue(startrow, 3, lstviewmodel[i].EducationName);
                            int startcol = 4;
                            for (int j = 0; j < lstSubject.Count(); j++)
                            {

                                Sheet.CopyPasteSameSize(rangeExcel, startrow, startcol);
                                Sheet.SetCellValue(startrow, startcol, lstviewmodel[i].lstSubject[j]);
                                startcol++;
                            }
                            startrow++;
                        }

                        Sheet.MergeColumn(2, startrowSchool, startrow - 1);

                    }

                    Sheet.SetCellValue(startrow + 1, 1, "Ghi chú:  ô có chữ x là khối học không học môn học đó, ô trống là đơn vị chưa báo cáo, ô ghi ngày tháng là ngày gần nhất đơn vị báo cáo");


                    Template.Delete();
                    Stream excel = oBook.ToStream();
                    FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                    result.FileDownloadName = ReportName;
                    return result;
                }

                #endregion

            }

            else 
            {

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_TK_DIEMTBM_THCS);
                //Tạo tên file [SoGD/PGD]_[SchoolLevel]_ TKDonViBCDiemTBM _[Khối]_[Học kỳ]
                string outputNamePattern = reportDef.OutputNamePattern;
                outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationlevel));
                outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.StripVNSign(AppliedLevelName));
                string ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                string templatePath = ReportUtils.GetTemplatePath(reportDef);
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet Template = oBook.GetSheet(1);

                IVTWorksheet Sheet = oBook.GetSheet(2);
                Sheet.CopyPasteSameSize(Template.GetRange("A1", "C6"), 1, 1);

                Sheet.SetCellValue("A2", sd.SupervisingDeptName.ToUpper());
                Sheet.SetCellValue("A3", "TÌNH HÌNH BÁO CÁO ĐIỂM TBM, HỌC LỰC, HẠNH KIỂM CỦA CÁC ĐƠN VỊ  " + SemesterName.ToUpper() + " NĂM HỌC " + ay.DisplayTitle.ToUpper());
                Sheet.SetCellValue("A4", "Quận/huyện: " + DistrictName + ", cấp trường: " + AppliedLevelName + ", khối: " + educationlevel);

                // Tạo header template
                IVTRange rangeHeader = Template.GetRange("D6", "D6");
                int startrow = 7;
                int startcolhearder = 4;

                foreach (var itemSubject in lstSubject)
                {

                    Sheet.CopyPasteSameSize(rangeHeader, 6, startcolhearder);
                    Sheet.SetCellValue(6, startcolhearder, itemSubject.SubjectName);
                    startcolhearder++;
                }

                Sheet.CopyPasteSameSize(rangeHeader, 6, startcolhearder);
                Sheet.SetCellValue(6, startcolhearder, "Học lực");
                startcolhearder++;
                Sheet.CopyPasteSameSize(rangeHeader, 6, startcolhearder);
                Sheet.SetCellValue(6, startcolhearder, "Hạnh kiểm");
                startcolhearder++;

                // fill dữ liệu vào excel
                IVTRange range = Template.GetRange("C8", "C8");
                IVTRange rangelast = Template.GetRange("C10", "C10");
                IVTRange rangeSchool = Template.GetRange("B8", "B8");
                IVTRange rangelastSchool = Template.GetRange("B10", "B10");

                int n = 0;
                int startrowSchool = 0;
                foreach (var item in lstSchool)
                {
                    int schoolid = item.SchoolProfileID;
                    startrowSchool = startrow;
                    List<ReportSupervisingDeptViewModel> lstviewmodel = lstViewModel.Where(o => o.SchoolID == schoolid).ToList();
                    Sheet.CopyPasteSameSize(rangeSchool, startrow, 2);
                    Sheet.SetCellValue(startrow, 2, item.SchoolName);

                    for (int i = 0; i < lstviewmodel.Count(); i++)
                    {
                        n++;
                        IVTRange rangeExcel = null;
                        if (i == lstviewmodel.Count() - 1)
                        {

                            rangeExcel = rangelast;
                            Sheet.CopyPasteSameSize(rangelastSchool, startrow, 2);
                        }
                        else
                        {

                            rangeExcel = range;
                        }
                        Sheet.CopyPasteSameSize(rangeExcel, startrow, 1);
                        Sheet.SetCellValue(startrow, 1, n);
                        Sheet.CopyPasteSameSize(rangeExcel, startrow, 3);
                        Sheet.SetCellValue(startrow, 3, lstviewmodel[i].EducationName);
                        int startcol = 4;
                        for (int j = 0; j < lstSubject.Count(); j++)
                        {

                            Sheet.CopyPasteSameSize(rangeExcel, startrow, startcol);
                            Sheet.SetCellValue(startrow, startcol, lstviewmodel[i].lstSubject[j]);
                            startcol++;
                        }
                        Sheet.CopyPasteSameSize(rangeExcel, startrow, startcol);
                        Sheet.SetCellValue(startrow, startcol, lstviewmodel[i].DisplayHL);
                        startcol++;
                        Sheet.CopyPasteSameSize(rangeExcel, startrow, startcol);
                        Sheet.SetCellValue(startrow, startcol, lstviewmodel[i].DisplayHK);
                        startcol++;

                        startrow++;
                    }

                    Sheet.MergeColumn(2, startrowSchool, startrow - 1);

                }

                Sheet.SetCellValue(startrow + 1, 1, "Ghi chú:  ô có chữ x là khối học không học môn học đó, ô trống là đơn vị chưa báo cáo, ô ghi ngày tháng là ngày gần nhất đơn vị báo cáo");


                Template.Delete();
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = ReportName;
                return result;

            }

            
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            if (model.ReportType == 3)
            {

                return PartialView("_ListTBM");

            }

            if (model.ReportType == 2)
            {
                if (glo.IsSuperVisingDeptRole)
                {

                }

                return PartialView("_ListHK");

            }

            #region
            if (model.ReportType == 1)
            {

                return PartialView("_ListDK");
            }
            #endregion

            return PartialView("");
        }



    }
}
