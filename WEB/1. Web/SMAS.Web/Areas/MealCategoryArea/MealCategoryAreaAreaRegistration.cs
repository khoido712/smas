﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MealCategoryArea
{
    public class MealCategoryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MealCategoryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MealCategoryArea_default",
                "MealCategoryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
