﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MealCategoryArea
{
    public class MealCategoryConstants
    {
        public const string LIST_RESULT = "list_result";
        public const string TOTAL = "total";
        public const string PAGE = "page";
        public const int PageSize = 20;
        public const string PERMISSION_CREATE = "PERMISSION_CREATE";
        public const string PERMISSION_UPDATE = "PERMISSION_UPDATE";
        public const string PERMISSION_DELETE = "PERMISSION_DELETE";
    }
}