﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MealCategoryArea.Models
{
    public class ListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DishesNum { get; set; }
        public string Note { get; set; }
        public int? OrderNumber { get; set; }
    }
}