﻿using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.MealCategoryArea.Models;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.MealCategoryArea.Controllers
{
    public class MealCategoryController:BaseController
    {
        #region properties
        private readonly IMealCategoryBusiness MealCategoryBusiness;
        private readonly IWeeklyMealMenuBusiness WeeklyMealMenuBusiness;
        #endregion

        #region Constructor
        public MealCategoryController(IMealCategoryBusiness MealCategoryBusiness, IWeeklyMealMenuBusiness WeeklyMealMenuBusiness)
        {
            this.MealCategoryBusiness = MealCategoryBusiness;
            this.WeeklyMealMenuBusiness = WeeklyMealMenuBusiness;
        }
        #endregion

        #region Actions
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public PartialViewResult Search(string mealName)
        {
            CheckCommandPermision();

            int currentPage = 1;

            List<MealCategory> listMc = MealCategoryBusiness.GetMealCategoriesBySchool(_globalInfo.SchoolID.Value);
            if (!string.IsNullOrWhiteSpace(mealName))
            {
                listMc = listMc.Where(o => o.MealName.ToUpper().Contains(mealName.Trim().ToUpper())).ToList();
            }
            List<ListViewModel> listResult = listMc
                 .Select(o => new ListViewModel
                 {
                     Id = o.MealCategoryID,
                     Name = o.MealName,
                     Note = o.Note,
                     DishesNum = o.DishesNum,
                     OrderNumber = o.OrderNumber
                 }).Skip((currentPage - 1) * MealCategoryConstants.PageSize).Take(MealCategoryConstants.PageSize).ToList();

            ViewData[MealCategoryConstants.TOTAL] = listMc.Count;
            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(string mealName, GridCommand command)
        {
            int currentPage = command.Page;

            List<MealCategory> listMc = MealCategoryBusiness.GetMealCategoriesBySchool(_globalInfo.SchoolID.Value);
            if (!string.IsNullOrWhiteSpace(mealName))
            {
                listMc = listMc.Where(o => o.MealName.ToUpper().Contains(mealName.Trim().ToUpper())).ToList();
            }
            List<ListViewModel> listResult = listMc
                 .Select(o => new ListViewModel
                 {
                     Id = o.MealCategoryID,
                     Name = o.MealName,
                     Note = o.Note,
                     DishesNum = o.DishesNum,
                     OrderNumber = o.OrderNumber
                 }).Skip((currentPage - 1) * MealCategoryConstants.PageSize).Take(MealCategoryConstants.PageSize).ToList();

            //Kiem tra button
            return View(new GridModel<ListViewModel>
            {
                Total = listMc.Count,
                Data = listResult
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(string mealName, int? dishesNum, string note)
        {
            if (GetMenupermission("MealCategory", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (string.IsNullOrWhiteSpace(mealName))
            {
                return Json(new JsonMessage("Thầy/cô chưa nhập tên bữa ăn", "error"));
            }

            if (!dishesNum.HasValue)
            {
                return Json(new JsonMessage("Thầy/cô chưa nhập số món ăn", "error"));
            }

            if (dishesNum.Value == 0)
            {
                return Json(new JsonMessage("Số món ăn phải lớn hơn 0", "error"));
            }

            int nextOrderNumber = MealCategoryBusiness.GetMealCategoriesBySchool(_globalInfo.SchoolID.Value).OrderByDescending(o => o.OrderNumber)
                .Select(o => o.OrderNumber).FirstOrDefault() + 1;

            MealCategory mc = new MealCategory();
            mc.SchoolID = _globalInfo.SchoolID.Value;
            mc.MealName = mealName.Trim();
            mc.Note = note.Trim();
            mc.DishesNum = dishesNum.Value;
            mc.CreateDate = DateTime.Now;
            mc.OrderNumber = nextOrderNumber;

            MealCategoryBusiness.Insert(mc);
            MealCategoryBusiness.Save();

            return Json(new JsonMessage("Thêm mới thành công"));
          
        }

        public PartialViewResult OpenEdit(int id)
        {
            MealCategory mc = MealCategoryBusiness.Find(id);
            ListViewModel model = new ListViewModel();
            if (mc != null)
            {
                model.Id = mc.MealCategoryID;
                model.Name = mc.MealName;
                model.Note = mc.Note;
                model.DishesNum = mc.DishesNum;
            }

            return PartialView("_Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int id,string mealName, int? dishesNum, string note)
        {
            if (GetMenupermission("MealCategory", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (string.IsNullOrWhiteSpace(mealName))
            {
                return Json(new JsonMessage("Thầy/cô chưa nhập tên bữa ăn", "error"));
            }

            if (!dishesNum.HasValue)
            {
                return Json(new JsonMessage("Thầy/cô chưa nhập số món ăn", "error"));
            }

            if (dishesNum.Value == 0)
            {
                return Json(new JsonMessage("Số món ăn phải lớn hơn 0", "error"));
            }

            MealCategory mc = MealCategoryBusiness.Find(id);

            if (WeeklyMealMenuBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value && o.MealCategoryID == id).Count() > 0)
            {
                if (dishesNum.Value < mc.DishesNum)
                {
                    return Json(new JsonMessage("Số bữa ăn không được nhỏ hơn số bữa ăn đã nhập trong hệ thống", "error"));
                }
            }

            mc.MealName = mealName.Trim();
            mc.Note = note.Trim();
            mc.DishesNum = dishesNum.Value;
            mc.ModifiedDate = DateTime.Now;

            MealCategoryBusiness.Update(mc);
            MealCategoryBusiness.Save();

            return Json(new JsonMessage("Cập nhật thành công"));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            if (GetMenupermission("MealCategory", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (WeeklyMealMenuBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value && o.MealCategoryID == id).Count() > 0)
            {
                return Json(new JsonMessage("Bữa ăn đã được sử dụng", "error"));
            }

            MealCategoryBusiness.Delete(id);
            MealCategoryBusiness.Save();

            return Json(new JsonMessage("Xóa thành công"));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateOrderNumber(List<ListViewModel> listModel)
        {
            if (GetMenupermission("MealCategory", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (listModel.Any(o => !o.OrderNumber.HasValue))
            {
                return Json(new JsonMessage("Số thứ tự không được để trống", "error"));
            }

            if (listModel.Any(o => o.OrderNumber == 0))
            {
                return Json(new JsonMessage("Số thứ tự phải lớn hơn 0", "error"));
            }

            List<MealCategory> lstMc = this.MealCategoryBusiness.GetMealCategoriesBySchool(_globalInfo.SchoolID.Value);

            for (int i = 0; i < listModel.Count; i++)
            {
                ListViewModel model = listModel[i];
                MealCategory mc = lstMc.FirstOrDefault(o => o.MealCategoryID == model.Id);
                mc.OrderNumber = model.OrderNumber.Value;
            }

            if (lstMc.Select(o => o.OrderNumber).Distinct().Count() < lstMc.Count)
            {
                return Json(new JsonMessage("Số thứ tự không được trùng nhau", "error"));
            }

            List<MealCategory> listMc = MealCategoryBusiness.GetMealCategoriesBySchool(_globalInfo.SchoolID.Value);

            for (int i = 0; i < listModel.Count; i++)
            {
                ListViewModel model = listModel[i];
                MealCategory mc = listMc.FirstOrDefault(o => o.MealCategoryID == model.Id);

                mc.OrderNumber = model.OrderNumber.Value;
                mc.ModifiedDate = DateTime.Now;

                MealCategoryBusiness.Update(mc);
            }

            MealCategoryBusiness.Save();
            return Json(new JsonMessage("Cập nhật thành công"));
        }


        #endregion

        #region Private methods
        private void SetViewData()
        {
        }
        private void CheckCommandPermision()
        {
            SetViewDataPermission("MealCategory", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            //Kiem tra quyen them, sua, xoa

            ViewData[MealCategoryConstants.PERMISSION_CREATE] = (bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE];
            ViewData[MealCategoryConstants.PERMISSION_UPDATE] = (bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_UPDATE];
            ViewData[MealCategoryConstants.PERMISSION_DELETE] = (bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE];
          
        }

        #endregion
    }
}