﻿namespace SMAS.Web.Areas.StatisticalMarkRecordOfPrimarySchoolArea
{
    public class StatisticalMarkRecordOfPrimarySchoolConstant
    {
        public const string LISTACADEMICYEAR = "LISTACADEMICYEAR";
        public const string LISTSEMESTER = "LISTSEMESTER";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTTRAININGTYPE = "LISTTRAININGTYPE";
        public const string LISTSUBJECT = "LISTSUBJECT";

        public const string LIST_GRIDVIEWMODEL = "LIST_GRIDVIEWMODEL";

        public const string SHOW_EXCEL = "SHOW_EXCEL";
        public const string ACCOUNTPHONG = "ACCOUNTPHONG";
        public const string ACCOUNTSO = "ACCOUNTSO";
    }
}