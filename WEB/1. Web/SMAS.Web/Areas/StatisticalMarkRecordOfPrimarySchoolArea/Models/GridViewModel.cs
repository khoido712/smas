﻿using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.StatisticalMarkRecordOfPrimarySchoolArea.Models
{
    public class GridViewModel
    {
        public int SchoolID { get; set; }

        public int DistrictID { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_School")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_District")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalSchool")]
        public int TotalSchool { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalPupil")]
        public int TotalPupil { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalExcellent")]
        public int TotalExcellent { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_PercentExcellent")]
        public string PercentExcellent { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalGood")]
        public int TotalGood { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_PercentGood")]
        public string PercentGood { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalNormal")]
        public int TotalNormal { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_PercentNormal")]
        public string PercentNormal { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_TotalWeak")]
        public int TotalWeak { get; set; }

        [ResourceDisplayName("StatisticalMarkRecordOfPrimarySchool_Label_PercentWeak")]
        public string PercentWeak { get; set; }
    }
}