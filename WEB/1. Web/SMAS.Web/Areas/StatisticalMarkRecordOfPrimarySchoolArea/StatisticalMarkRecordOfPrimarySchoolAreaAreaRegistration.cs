﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticalMarkRecordOfPrimarySchoolArea
{
    public class StatisticalMarkRecordOfPrimarySchoolAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticalMarkRecordOfPrimarySchoolArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticalMarkRecordOfPrimarySchoolArea_default",
                "StatisticalMarkRecordOfPrimarySchoolArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}