﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ExamInputMarkAssignedArea.Models;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
namespace SMAS.Web.Areas.ExamInputMarkAssignedArea.Controllers
{
    public class ExamInputMarkAssignedController : BaseController
    {

        #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        #endregion

        #region Constructor
        public ExamInputMarkAssignedController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness, IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness, IEmployeeBusiness EmployeeBusiness,
            IExamRoomBusiness ExamRoomBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamInputMarkAssignedBusiness = ExamInputMarkAssignedBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
        }
        #endregion


        #region Action
        //
        // GET: /ExamInputMarkAssignedArea/ExamInputMarkAssigned/
        public ActionResult Index()
        {

            SetViewData(null,null);

            //Lay ky thi mac dinh
            long? defaultExamID = null;
            if (listExaminations.Count > 0)
            {
                defaultExamID = listExaminations.First().ExaminationsID;
            }

            //Lay nhom thi mac dinh
            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0)
            {
                defaultExamGroupID = listExamGroup.First().ExamGroupID;
            }

            if (defaultExamID == null || defaultExamGroupID == null)
            {
                ViewData[ExamInputMarkAssignedConstants.LIST_RESULT] = new List<ListViewModel>();
                CheckCommandPermision(null);
                return View();
            }

            List<ListViewModel> listResult = this._Search(defaultExamID.Value, defaultExamGroupID.Value, null).ToList();

            ViewData[ExamInputMarkAssignedConstants.LIST_RESULT] = listResult;

            CheckCommandPermision(this.ExaminationsBusiness.Find(defaultExamID));

            return View();
        }

        //
        // GET: /ExamInputMarkAssignedArea/Search/
        public PartialViewResult Search(SearchViewModel form)
        {
            Utils.Utils.TrimObject(form);

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;

            if (examinationsID == null || examGroupID==null)
            {
                CheckCommandPermision(this.ExaminationsBusiness.Find(null));
                return PartialView("_List", new List<ListViewModel>());
            }
            List<ListViewModel> listResult = this._Search(examinationsID.Value, examGroupID.Value, subjectID).ToList();
            CheckCommandPermision(this.ExaminationsBusiness.Find(examinationsID));

            return PartialView("_List", listResult);

        }


        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(long id)
        {
            if (GetMenupermission("ExamInputMarkAssigned", _globalInfo.UserAccountID, _globalInfo.IsAdmin,_globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ExamInputMarkAssigned assigned = this.ExamInputMarkAssignedBusiness.Find(id);

            this.ExamInputMarkAssignedBusiness.Delete(id);
            this.ExamInputMarkAssignedBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        //
        // GET: /ExamInputMarkAssignedArea/Assign/
        public ActionResult Assign(long examinationsID, long? examGroupID, int? subjectID)
        {
            SetViewData(examinationsID, examGroupID);
            //Lay danh sach can bo
            List<ListViewModel> listEmployee = EmployeeBusiness.GetActiveEmployee(_globalInfo.SchoolID.GetValueOrDefault())
                                                                .OrderBy(o=>o.FullName)
                                                                .Select(o=>new ListViewModel
                                                                {
                                                                    EmployeeCode=o.EmployeeCode,
                                                                    EmployeeName=o.FullName,
                                                                    SchoolFacultyID=o.SchoolFacultyID,
                                                                    SchoolFacultyName=o.SchoolFacultyName,
                                                                    TeacherID=o.EmployeeID,
                                                                    EthnicCode = o.EthnicCode
                                                                }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode))).ToList(); 

            AssignContentViewModel model = new AssignContentViewModel();
            model.topList = listEmployee;
            model.botList = new List<ListViewModel>();

            ViewData[ExamInputMarkAssignedConstants.ASSIGN_CONTENT] = model;
            AssignFormModel form = new AssignFormModel();
            form._ExaminationsID = examinationsID;
            form._ExamGroupID = examGroupID;
            form._SubjectID = subjectID;

            //Lay danh sach phong thi
            List<ExamRoomViewModel> lstExamRoom = new List<ExamRoomViewModel>();
            if (examGroupID != null)
            {
                lstExamRoom = ExamRoomBusiness.GetListExamRoomBO(examinationsID, new List<long>() { examGroupID.Value })
                    .Select(o => new ExamRoomViewModel
                    {
                        ExamGroupName = o.ExamGroupName,
                        ExamRoomCode = o.ExamRoomCode,
                        ExamRoomID = o.ExamRoomID
                    }).ToList();
            }

            form.lstExamRoom = lstExamRoom;
            ViewData[ExamInputMarkAssignedConstants.ASSIGN_FORM] = form;

            return PartialView("_Assign");
        }

        public ActionResult AssignSearch(string AssignSearchInfo, string hiddenChoosedID)
        {
            List<long> listChoosedID = GetIDsFromString(hiddenChoosedID);
            //Lay danh sach can bo
            List<ListViewModel> listEmployee = EmployeeBusiness.GetActiveEmployee(_globalInfo.SchoolID.GetValueOrDefault())
                                                                .OrderBy(o => o.FullName)
                                                                .Select(o => new ListViewModel
                                                                {
                                                                    EmployeeCode = o.EmployeeCode,
                                                                    EmployeeName = o.FullName,
                                                                    SchoolFacultyID = o.SchoolFacultyID,
                                                                    SchoolFacultyName = o.SchoolFacultyName,
                                                                    TeacherID = o.EmployeeID,
                                                                    EthnicCode = o.EthnicCode
                                                                }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode))).ToList();

            List<ListViewModel> topList = listEmployee.Where(o => !listChoosedID.Contains(o.TeacherID)).ToList(); 
            if (AssignSearchInfo.Trim().Length > 0)
            {
                topList = topList.Where(o => o.EmployeeCode.ToUpper().Contains(AssignSearchInfo.ToUpper())
                                                    || o.EmployeeName.ToUpper().Contains(AssignSearchInfo.ToUpper())
                                                    || o.SchoolFacultyName.ToUpper().Contains(AssignSearchInfo.ToUpper())).ToList();
            }
            return PartialView("_AssignList", topList);
        }

        [HttpPost]
        public ActionResult GetExamRoomList(long examinationsId, long? examGroupId, int scale)
        {
            List<ExamRoomViewModel> lstExamRoom = new List<ExamRoomViewModel>();
            if (scale == ExamInputMarkAssignedConstants.ASSIGN_SCALE_CURRENT_SUBJECT || scale == ExamInputMarkAssignedConstants.ASSIGN_SCALE_ALL_SUBJECT)
            {
                if (examGroupId.HasValue)
                {
                    lstExamRoom = ExamRoomBusiness.GetListExamRoomBO(examinationsId, new List<long>() {examGroupId.Value })
                    .Select(o => new ExamRoomViewModel
                    {
                        ExamGroupName = o.ExamGroupName,
                        ExamRoomCode = o.ExamRoomCode,
                        ExamRoomID = o.ExamRoomID
                    }).ToList();
                }
            }
            else
            {
                //Lay danh sach nhom thi cua ky thi
                List<long> lstExamGroupId = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsId)
                                                        .OrderBy(o => o.ExamGroupCode)
                                                        .Select(o => o.ExamGroupID).ToList();
                lstExamRoom = ExamRoomBusiness.GetListExamRoomBO(examinationsId, lstExamGroupId)
                    .Select(o => new ExamRoomViewModel
                    {
                        ExamGroupName = o.ExamGroupName,
                        ExamRoomCode = o.ExamRoomCode,
                        ExamRoomID = o.ExamRoomID
                    }).ToList();
            }

            return PartialView("_ExamRoomList", lstExamRoom);
        }
        //
        // GET: /ExamInputMarkAssignedArea/AssignAddOrCancel/
        public ActionResult AssignAddOrCancel(string searchInfo, string listIDchecked, string checkedID)
        {
            AssignContentViewModel content = new AssignContentViewModel();
            List<long> listChoosedID = new List<long>();
            if (!string.IsNullOrEmpty(listIDchecked))
            {
                listChoosedID = listIDchecked.Split(new[] { ',', '\"' }, StringSplitOptions.RemoveEmptyEntries).Select(p => Convert.ToInt64(p)).ToList();
            }             
            List<long> listCheckedID = this.GetIDsFromString(checkedID);

            //Lay danh sach can bo
             List<ListViewModel> listEmployee = EmployeeBusiness.GetActiveEmployee(_globalInfo.SchoolID.GetValueOrDefault())
                                                                .Select(o=>new ListViewModel
                                                                {
                                                                    TeacherID=o.EmployeeID,
                                                                    EmployeeCode=o.EmployeeCode,
                                                                    EmployeeName=o.FullName,
                                                                    SchoolFacultyID=o.SchoolFacultyID,
                                                                    SchoolFacultyName=o.SchoolFacultyName,
                                                                    EthnicCode = o.EthnicCode
                                                                }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode))).ToList(); 

            //Lay danh sach can bo o tren
             List<ListViewModel> topList = listEmployee.Where(o => !listChoosedID.Contains(o.TeacherID)).ToList();
             if (searchInfo.Trim().Length > 0)
             {
                 topList = topList.Where(o => o.EmployeeCode.ToUpper().Contains(searchInfo.ToUpper())
                                                    || o.EmployeeName.ToUpper().Contains(searchInfo.ToUpper())
                                                    || o.SchoolFacultyName.ToUpper().Contains(searchInfo.ToUpper())).ToList();
             }
            //Lay danh sach can bo o duoi
             List<ListViewModel> botList = listEmployee.Where(o => listChoosedID.Contains(o.TeacherID)).ToList();

             content.topList = topList;
             content.botList = botList;

            ViewData[ExamInputMarkAssignedConstants.CHECKED_ID] = listCheckedID;
            return PartialView("_AssignContent", content);
        }

        [ValidateAntiForgeryToken]
        
        [HttpPost]
        
        public JsonResult AutoAssign(AssignFormModel form)
        {
            if (ModelState.IsValid)
            {
                if (GetMenupermission("ExamInputMarkAssigned", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }

                if (String.IsNullOrEmpty(form.ChoosedTeacherID))
                {
                    throw new BusinessException("ExamInputMarkAssigned_Validate_NotTeacherChoosen");
                }
                if (form._ExaminationsID == null)
                {
                    throw new BusinessException("ExamInputMarkAssigned_Validate_Required_ExaminationsID");
                }
                if (form._ExamGroupID == null)
                {
                    throw new BusinessException("ExamInputMarkAssigned_Validate_Required_ExamGroupID");
                }
                if (form._SubjectID == null)
                {
                    throw new BusinessException("ExamInputMarkAssigned_Validate_Required_SubjectID");
                }

                Utils.Utils.TrimObject(form);
                int assignScale = form.AssignScale;
                long examGroupID = form._ExamGroupID.Value;
                long examinationsID = form._ExaminationsID.Value;
                int subjectID = form._SubjectID.Value;
                List<long> lstExamRoomID = new List<long>();
                if(form.lstExamRoomID != null)
                {
                    lstExamRoomID = form.lstExamRoomID;
                }
                if (lstExamRoomID.Count == 0)
                {
                    throw new BusinessException("ExamInputMarkAssigned_Validate_Required_ExamRoomID");
                }

                List<long> listChoosedID = GetIDsFromString(form.ChoosedTeacherID);

                //Lay danh sach can bo duoc chon tham gia nhap diem
                List<EmployeeBO> listChoosedEmployee = EmployeeBusiness.GetActiveEmployee(_globalInfo.SchoolID.GetValueOrDefault())
                                                                .Where(o => listChoosedID.Contains(o.EmployeeID))
                                                                .OrderBy(o=>o.FullName).ToList();
                //Lay danh sach phan cong nhap diem hien co cua ky thi
                List<ExamInputMarkAssigned> listAssignedOfExaminations = ExamInputMarkAssignedBusiness.All.Where(o => o.ExaminationsID == examinationsID).ToList();

                List<ExamInputMarkAssigned> listToDelete = new List<ExamInputMarkAssigned>();
                List<ExamInputMarkAssigned> listToInsert = new List<ExamInputMarkAssigned>();

                if (assignScale == ExamInputMarkAssignedConstants.ASSIGN_SCALE_CURRENT_SUBJECT)
                {
                    AutoAssignHandle(examinationsID, examGroupID, subjectID, listChoosedEmployee, listToInsert, listToDelete, listAssignedOfExaminations, lstExamRoomID);
                }
                else if(assignScale==ExamInputMarkAssignedConstants.ASSIGN_SCALE_ALL_SUBJECT)
                {
                    //Lay danh sach mon thi cua nhom thi hien tai
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["ExaminationsID"] = examinationsID;
                    dic["ExamGroupID"] = examGroupID;
                    List<ExamSubjectBO> listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).ToList();
                    for (int i = 0; i < listExamSubject.Count; i++)
                    {
                        ExamSubjectBO entity = listExamSubject[i];
                        AutoAssignHandle(examinationsID, examGroupID, entity.SubjectID, listChoosedEmployee, listToInsert, listToDelete, listAssignedOfExaminations, lstExamRoomID);
                    }

                }
                else if (assignScale == ExamInputMarkAssignedConstants.ASSIGN_SCALE_ALL_GROUP)
                {
                    //Lay danh sach nhom thi cua ky thi
                    List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
                    List<ExamRoom> lstExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID).ToList();
                    for (int i = 0; i < listExamGroup.Count; i++)
                    {
                        ExamGroup entity = listExamGroup[i];
                        //Lay cac phong thi duoc chon cua nhom thi
                        List<long> lstExamRoomIDOfGroup = lstExamRoom.Where(o => o.ExamGroupID == entity.ExamGroupID).Select(o => o.ExamRoomID).ToList();
                        List<long> lstSelectedExamRoomID = lstExamRoomID.Where(o => lstExamRoomIDOfGroup.Contains(o)).ToList();
                        if (lstSelectedExamRoomID.Count == 0)
                        {
                            throw new BusinessException("ExamInputMarkAssigned_Validate_Required_ExamRoomID");
                        }

                        AutoAssignHandle(examinationsID, entity.ExamGroupID, subjectID, listChoosedEmployee, listToInsert, listToDelete, listAssignedOfExaminations, lstSelectedExamRoomID);
                    }
                }

                ExamInputMarkAssignedBusiness.DeleteList(listToDelete);
                ExamInputMarkAssignedBusiness.InsertList(listToInsert);
                
                return Json(new JsonMessage(String.Format(Res.Get("ExamInputMarkAssigned_Message_AssignSuccess"),listChoosedEmployee.Count.ToString())));
            }
            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }

        //
        // GET: /ExamInputMarkAssignedArea/Create/
        public ActionResult Create(long examinationsID, long examGroupID, int subjectID)
        {
            //Lay danh sach can bo da duoc phan cong nhap diem cho mon thi va nhom thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SubjectID"] = subjectID;

            //Lay danh sach da duoc phan cong cho nhom thi va mon thi
            List<ExamInputMarkAssignedBO> listExamInputMarkAssign = ExamInputMarkAssignedBusiness.Search(dic).ToList();
            //Lay danh sach toan bo giao vien
            List<EmployeeBO> lstActiveEmployee = EmployeeBusiness.GetActiveEmployee(_globalInfo.SchoolID.GetValueOrDefault()).ToList();

            List<ListViewModel> listModel = (from ee in lstActiveEmployee
                                             join eim in listExamInputMarkAssign on ee.EmployeeID equals eim.TeacherID into des
                                             from x in des.DefaultIfEmpty()
                                             select new ListViewModel
                                             {
                                                 EmployeeCode = ee.EmployeeCode,
                                                 EmployeeName = ee.FullName,
                                                 SchoolFacultyID = ee.SchoolFacultyID,
                                                 SchoolFacultyName = ee.SchoolFacultyName,
                                                 TeacherID = ee.EmployeeID,
                                                 EthnicCode = ee.EthnicCode,
                                                 ExamRoomID = x != null ? x.ExamRoomID : null,
                                                 ExamInputMarkAssignedID = x!=null?x.ExamInputMarkAssignedID : 0,
                                                 IsCheck = x != null ? true : false
                                             }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode))).ToList();

            //Danh sach phong thi cua nhom thi
            List<ExamRoom> lstExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID).ToList();
            
            for (int i = 0; i < listModel.Count; i++)
            {
                ListViewModel model = listModel[i];
                string examRoomId = model.ExamRoomID;
                List<long> lstExamRoomID = GetIDsFromString(examRoomId);

                string strExamRoom = String.Empty;
                if (lstExamRoomID.Count == 0 || lstExamRoom.Count == 0)
                {
                    strExamRoom = "Chọn phòng thi";
                }
                else if (lstExamRoomID.Count == lstExamRoom.Count)
                {
                    strExamRoom = "Tất cả";
                }
                else
                {
                    for (int j = 0; j < lstExamRoomID.Count; j++)
                    {
                        ExamRoom er = lstExamRoom.Where(o => o.ExamRoomID == lstExamRoomID[j]).FirstOrDefault();
                        if (er != null)
                        {
                            strExamRoom = strExamRoom + er.ExamRoomCode;
                            if (j < lstExamRoomID.Count - 1)
                            {
                                strExamRoom = strExamRoom + ", ";
                            }
                        }
                    }
                }
                model.StrExamRoom = strExamRoom;
                model.LstExamRoomID = lstExamRoomID;
            }

            ViewData[ExamInputMarkAssignedConstants.CREATE_LIST] = listModel;
            ViewData[ExamInputMarkAssignedConstants.CREATE_EXAMINATIONS_ID] = examinationsID;
            ViewData[ExamInputMarkAssignedConstants.CREATE_EXAMGROUP_ID] = examGroupID;
            ViewData[ExamInputMarkAssignedConstants.CREATE_SUBJECT_ID] = subjectID;
            ViewData[ExamInputMarkAssignedConstants.CREATE_LIST_EXAM_ROOM] = lstExamRoom;

            return PartialView("_Create");
        }

        public ActionResult CreateSearch(string CreateSearchInfo, long examinationsID, long examGroupID, int subjectID)
        {
            //Lay danh sach can bo da duoc phan cong nhap diem cho mon thi va nhom thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SubjectID"] = subjectID;

            //Lay danh sach da duoc phan cong cho nhom thi va mon thi
            List<ExamInputMarkAssignedBO> listExamInputMarkAssign = ExamInputMarkAssignedBusiness.Search(dic).ToList();
            //Lay danh sach toan bo giao vien
            List<EmployeeBO> lstActiveEmployee = EmployeeBusiness.GetActiveEmployee(_globalInfo.SchoolID.GetValueOrDefault()).ToList();

            List<ListViewModel> listModel = (from ee in lstActiveEmployee
                                             join eim in listExamInputMarkAssign on ee.EmployeeID equals eim.TeacherID into des
                                             from x in des.DefaultIfEmpty()
                                             select new ListViewModel
                                             {
                                                 EmployeeCode = ee.EmployeeCode,
                                                 EmployeeName = ee.FullName,
                                                 SchoolFacultyID = ee.SchoolFacultyID,
                                                 SchoolFacultyName = ee.SchoolFacultyName,
                                                 TeacherID = ee.EmployeeID,
                                                 EthnicCode = ee.EthnicCode,
                                                 ExamRoomID = x != null ? x.ExamRoomID : null,
                                                 ExamInputMarkAssignedID = x != null ? x.ExamInputMarkAssignedID : 0,
                                                 IsCheck = x != null ? true : false
                                             }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode))).ToList();

            //Danh sach phong thi cua nhom thi
            List<ExamRoom> lstExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID).ToList();

            for (int i = 0; i < listModel.Count; i++)
            {
                ListViewModel model = listModel[i];
                string examRoomId = model.ExamRoomID;
                List<long> lstExamRoomID = GetIDsFromString(examRoomId);

                string strExamRoom = String.Empty;
                if (lstExamRoomID.Count == 0 || lstExamRoom.Count == 0)
                {
                    strExamRoom = "Chọn phòng thi";
                }
                else if (lstExamRoomID.Count == lstExamRoom.Count)
                {
                    strExamRoom = "Tất cả";
                }
                else
                {
                    for (int j = 0; j < lstExamRoomID.Count; j++)
                    {
                        ExamRoom er = lstExamRoom.Where(o => o.ExamRoomID == lstExamRoomID[j]).FirstOrDefault();
                        if (er != null)
                        {
                            strExamRoom = strExamRoom + er.ExamRoomCode;
                            if (j < lstExamRoomID.Count - 1)
                            {
                                strExamRoom = strExamRoom + ", ";
                            }
                        }
                    }
                }
                model.StrExamRoom = strExamRoom;
                model.LstExamRoomID = lstExamRoomID;
            }

            if (CreateSearchInfo.Trim().Length > 0)
            {
                listModel = listModel.Where(o => o.EmployeeCode.ToUpper().Contains(CreateSearchInfo.ToUpper())
                                                    || o.EmployeeName.ToUpper().Contains(CreateSearchInfo.ToUpper())
                                                    || o.SchoolFacultyName.ToUpper().Contains(CreateSearchInfo.ToUpper())).ToList();
            }

            ViewData[ExamInputMarkAssignedConstants.CREATE_EXAMINATIONS_ID] = examinationsID;
            ViewData[ExamInputMarkAssignedConstants.CREATE_EXAMGROUP_ID] = examGroupID;
            ViewData[ExamInputMarkAssignedConstants.CREATE_SUBJECT_ID] = subjectID;
            ViewData[ExamInputMarkAssignedConstants.CREATE_LIST_EXAM_ROOM] = lstExamRoom;
            return PartialView("_CreateList", listModel);
        }

        [ValidateAntiForgeryToken]
        
        [HttpPost]
        public JsonResult Create(long examinationsID, long examGroupID, int subjectID, List<ListViewModel> lstViewModel)
        {
            if (GetMenupermission("ExamInputMarkAssigned", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }



            List<ExamInputMarkAssigned> listExamInputMarkAssign = ExamInputMarkAssignedBusiness.All.Where(o => o.ExaminationsID == examinationsID
                                                                                                && o.ExamGroupID == examGroupID
                                                                                                && o.SubjectID == subjectID).ToList();

            if (lstViewModel != null)
            {
                List<ExamInputMarkAssigned> listToInsert = new List<ExamInputMarkAssigned>();
                List<ExamInputMarkAssigned> listToUpdate = new List<ExamInputMarkAssigned>();
                List<ExamInputMarkAssigned> listToDelete = new List<ExamInputMarkAssigned>();
                ExamInputMarkAssigned entity;
                for (int i = 0; i < lstViewModel.Count; i++)
                {
                    ListViewModel model = lstViewModel[i];
                    if (model.IsCheck)
                    {
                        if (model.LstExamRoomID == null || model.LstExamRoomID.Count == 0)
                        {
                            throw new BusinessException("Tồn tại cán bộ chưa được phân phòng thi để nhập điểm. Xin vui lòng kiểm tra lại");
                        }
                        entity = null;
                        if (model.ExamInputMarkAssignedID != 0)
                        {
                            entity = listExamInputMarkAssign.Where(o => o.ExamInputMarkAssignedID == model.ExamInputMarkAssignedID).FirstOrDefault();
                        }
                        //Neu chua co thi insert
                        if (entity == null)
                        {
                            entity = new ExamInputMarkAssigned();
                            entity.CreateTime = DateTime.Now;
                            entity.ExamGroupID = examGroupID;
                            entity.ExaminationsID = examinationsID;
                            entity.ExamInputMarkAssignedID = ExamInputMarkAssignedBusiness.GetNextSeq<long>("EXAM_INPUTMARK_ASSIGNED_SEQ");
                            entity.SchooFacultyID = model.SchoolFacultyID;
                            entity.SubjectID = subjectID;
                            entity.ExamRoomID = GetStringFromIDs(model.LstExamRoomID);
                            entity.TeacherID = model.TeacherID;
                            entity.UpdateTime = null;

                            listToInsert.Add(entity);
                        }
                        else
                        {
                            entity.ExamRoomID = GetStringFromIDs(model.LstExamRoomID);
                            entity.UpdateTime = DateTime.Now;

                            listToUpdate.Add(entity);
                        }
                    }
                    else
                    {
                        entity = null;
                        if (model.ExamInputMarkAssignedID != 0)
                        {
                            entity = ExamInputMarkAssignedBusiness.Find(model.ExamInputMarkAssignedID);
                        }
                        //Neu co thi xoa
                        if (entity != null)
                        {
                            listToDelete.Add(entity);
                        }
                    }
                }

                ExamInputMarkAssignedBusiness.InsertList(listToInsert);
                ExamInputMarkAssignedBusiness.UpdateList(listToUpdate);
                ExamInputMarkAssignedBusiness.DeleteList(listToDelete);
            }                                              
            
            
            return Json(new JsonMessage(Res.Get("ExamInputMarkAssigned_Message_CreateSuccess")));
        }

        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamSubject(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamSubjectBO>(), "SubjectID", "SubjectName"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            List<ExamSubjectBO> listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o=>o.OrderInSubject).ToList();

            return Json(new SelectList(listExamSubject, "SubjectID", "SubjectName"));
        }
        #endregion

        #region Private method
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData(long? selectedExamID, long? selectedExamGroupID)
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamInputMarkAssignedConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (selectedExamID == null)
            {
                if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;
            }
            else
            {
                defaultExamID = selectedExamID;
            }

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamInputMarkAssignedConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (selectedExamGroupID == null)
            {
                if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;
            }
            else
            {
                defaultExamGroupID = selectedExamGroupID;
            }


            //Lay danh sach mon thi
            List<ExamSubjectBO> listExamSubject;
            //Lay danh sach mon thi
            if (defaultExamGroupID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = defaultExamID;
                dic["ExamGroupID"] = defaultExamGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o=>o.OrderInSubject).ToList();
            }
            else
            {
                listExamSubject = new List<ExamSubjectBO>();
            }
            ViewData[ExamInputMarkAssignedConstants.CBO_EXAM_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");
        }

        /// <summary>
        /// Ham tim kiem
        /// </summary>
        /// <param name="examinationsID"></param>
        /// <param name="examGroupID"></param>
        /// <param name="ExamRoomID"></param>
        /// <returns></returns>
        private List<ListViewModel> _Search(long examinationsID, long examGroupID, long? subjectID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SubjectID"] = subjectID;

            List<ExamInputMarkAssignedBO> listAssignment = ExamInputMarkAssignedBusiness.Search(dic).ToList();


            //Lay danh sach mon thi cua nhom thi
            dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            List<ExamSubjectBO> listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).ToList();
            int _subjectID=SMAS.Business.Common.Utils.GetInt(subjectID);
            if (_subjectID > 0)
            {
                listExamSubject = listExamSubject.Where(o => o.SubjectID == _subjectID).ToList();
            }

            List<ListViewModel> ret = (from es in listExamSubject
                                       join la in listAssignment
                                           on es.SubjectID equals la.SubjectID into des
                                       from x in des.DefaultIfEmpty()
                                       select new ListViewModel
                                        {
                                            EmployeeName = x != null ? x.EmployeeName : String.Empty,
                                            EmployeeCode=x!=null?x.EmployeeCode:String.Empty,
                                            ExamGroupID = es.ExamGroupID,
                                            ExaminationsID = es.ExaminationsID,
                                            ExamInputMarkAssignedID = x != null ? x.ExamInputMarkAssignedID : 0,
                                            SchoolFacultyID = x != null ? x.SchoolFacultyID : 0,
                                            SchoolFacultyName = x != null ? x.SchoolFacultyName : String.Empty,
                                            SubjectID = es.SubjectID,
                                            SubjectName = es.SubjectName,
                                            TeacherID = x != null ? x.TeacherID : 0,
                                            SubjectOrder=es.OrderInSubject,
                                            EthnicCode=x!=null?x.EthnicCode:null,
                                            ExamRoomID = x != null ? x.ExamRoomID : null
                                        }).OrderBy(o => o.SubjectOrder).ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode))).ToList();

            List<ExamRoom> lstExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID).ToList();
            for (int i = 0; i < ret.Count; i++)
            {
                ListViewModel model = ret[i];
                string examRoomId = model.ExamRoomID;
                List<long> lstExamRoomID = GetIDsFromString(examRoomId);

                string strExamRoom = String.Empty;
                if (lstExamRoomID.Count == lstExamRoom.Count)
                {
                    strExamRoom = "Tất cả";
                }
                else
                {
                    for (int j = 0; j < lstExamRoomID.Count; j++)
                    {
                        ExamRoom er = lstExamRoom.Where(o => o.ExamRoomID == lstExamRoomID[j]).FirstOrDefault();
                        if (er != null)
                        {
                            strExamRoom = strExamRoom + er.ExamRoomCode;
                            if (j < lstExamRoomID.Count - 1)
                            {
                                strExamRoom = strExamRoom + ", ";
                            }
                        }
                    }
                }
                model.StrExamRoom = strExamRoom;
            }

            return ret;

        }

        private void AutoAssignHandle(long examinationsID, long examGroupID, int subjectID, List<EmployeeBO> listEmployee, List<ExamInputMarkAssigned> listToInsert,
            List<ExamInputMarkAssigned> listToDelete, List<ExamInputMarkAssigned> listInputMarkAssignedOfExaminations,
            List<long> lstExamRoomID)
        {
            //Lay ra danh sach phan cong nhap diem cu
            IDictionary<string,object> dic=new Dictionary<string,object>();
            List<ExamInputMarkAssigned> oldData = listInputMarkAssignedOfExaminations.Where(o => o.ExamGroupID == examGroupID)
                                                                                   .Where(o => o.SubjectID == subjectID).ToList();
            listToDelete.AddRange(oldData);

            string strExamRoomID = GetStringFromIDs(lstExamRoomID);
            //Tao phan cong nhap diem
            ExamInputMarkAssigned entity;
            for (int i = 0; i < listEmployee.Count; i++)
            {
                EmployeeBO employee=listEmployee[i];

                entity=new ExamInputMarkAssigned();
                entity.CreateTime=DateTime.Now;
                entity.ExamGroupID=examGroupID;
                entity.ExaminationsID=examinationsID;
                entity.ExamInputMarkAssignedID = ExamInputMarkAssignedBusiness.GetNextSeq<long>("EXAM_INPUTMARK_ASSIGNED_SEQ");
                entity.SchooFacultyID = employee.SchoolFacultyID;
                entity.SubjectID = subjectID;
                entity.TeacherID = employee.EmployeeID;
                entity.ExamRoomID = strExamRoomID;
                entity.UpdateTime = null;

                listToInsert.Add(entity);
            }                                                                            
        }

        private List<long> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                IDArr = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                IDArr = new string[] { };
            }
            List<long> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList() :
                                            new List<long>();

            return listID;
        }

        private string GetStringFromIDs(List<long> list)
        {
            string str = null;
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    
                    str = str + list[i];
                    if (i < list.Count - 1)
                    {
                        str = str + ",";
                    }
                }
            }
            return str;
        }
        /// <summary>
        /// Kiem tra disable/enable
        /// </summary>
        /// <param name="listResult"></param>
        private void CheckCommandPermision(Examinations examinations)
        {
            SetViewDataPermission("ExamInputMarkAssigned", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            bool checkButton = false;
            bool checkDelete = false;
            if (examinations != null)
            {
                if(examinations.MarkClosing.GetValueOrDefault() != true && _globalInfo.IsCurrentYear)
                {
                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE])
                    {
                        checkButton = true;
                    }
                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE])
                    {
                        checkDelete=true;
                    }
                }
            
            }

            ViewData[ExamInputMarkAssignedConstants.PER_CHECK_BUTTON] = checkButton;
            ViewData[ExamInputMarkAssignedConstants.PER_CHECK_DELETE] = checkDelete;
        }
        #endregion
    }
}
