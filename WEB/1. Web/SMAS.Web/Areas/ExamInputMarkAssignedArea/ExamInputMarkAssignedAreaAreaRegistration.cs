﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamInputMarkAssignedArea
{
    public class ExamInputMarkAssignedAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamInputMarkAssignedArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamInputMarkAssignedArea_default",
                "ExamInputMarkAssignedArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
