﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamInputMarkAssignedArea
{
    public class ExamInputMarkAssignedConstants
    {
        //ViewData
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string CBO_EXAM_SUBJECT = "cbo_exam_subject";
        public const string LIST_RESULT = "list_result";
        public const string ASSIGN_CONTENT = "assign_content";
        public const string ASSIGN_FORM = "assign_form";
        public const string CHECKED_ID = "checked_id";
        public const string CREATE_LIST = "create_list";
        public const string CREATE_EXAMINATIONS_ID = "create_examinations_id";
        public const string CREATE_EXAMGROUP_ID = "create_exam_group_id";
        public const string CREATE_SUBJECT_ID = "create_subject_id";
        public const string CREATE_LIST_EXAM_ROOM = "create_list_exam_room";
        public const string PER_CHECK_BUTTON = "per_check_button";
        public const string PER_CHECK_DELETE = "per_check_delete";

        public const int ASSIGN_SCALE_CURRENT_SUBJECT = 1;
        public const int ASSIGN_SCALE_ALL_SUBJECT = 2;
        public const int ASSIGN_SCALE_ALL_GROUP = 3;


    }
}