﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamInputMarkAssignedArea.Models
{
    public class ExamRoomViewModel
    {
        public long ExamRoomID { get; set; }
        [ResourceDisplayName("ExamInputMarkAssigned_Label_ExamRoom")]
        public string ExamRoomCode { get; set; }
        [ResourceDisplayName("ExamInputMarkAssigned_Label_ExamGroupID")]
        public string ExamGroupName { get; set; }
    }
}