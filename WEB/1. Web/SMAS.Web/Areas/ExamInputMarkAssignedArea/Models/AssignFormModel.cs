﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamInputMarkAssignedArea.Models
{
    public class AssignFormModel
    {
        [ResourceDisplayName("ExamInputMarkAssigned_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkAssignedConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "_onExaminationsChange()")]
        public long? _ExaminationsID { get; set; }

        [ResourceDisplayName("ExamInputMarkAssigned_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkAssignedConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "_onExamGroupChange()")]
        public long? _ExamGroupID { get; set; }

        [ResourceDisplayName("ExamInputMarkAssigned_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkAssignedConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("OnChange", "_onExamSubjectChange()")]
        public int? _SubjectID { get; set; }

        public List<ExamRoomViewModel> lstExamRoom { get; set; }

        public List<long> lstExamRoomID { get; set; }

        public int AssignScale { get; set; }

        public string ChoosedTeacherID { get; set; }
    }
}