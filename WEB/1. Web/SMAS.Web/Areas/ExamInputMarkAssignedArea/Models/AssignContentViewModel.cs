﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Areas.ExamInputMarkAssignedArea.Models;

namespace SMAS.Web.Areas.ExamInputMarkAssignedArea.Models
{
    public class AssignContentViewModel
    {
        public List<ListViewModel> topList { get; set; }
        public List<ListViewModel> botList { get; set; }
    }
}