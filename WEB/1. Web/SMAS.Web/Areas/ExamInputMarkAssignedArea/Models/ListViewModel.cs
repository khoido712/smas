﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamInputMarkAssignedArea.Models
{
    public class ListViewModel
    {
        //PK
        public long ExamInputMarkAssignedID { get; set; }

        //FK
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public int SubjectID { get; set; }
        public int TeacherID { get; set; }
        public int? SchoolFacultyID { get; set; }
        public string ExamRoomID { get; set; }
        

        public string SubjectName { get; set; }
        [ResourceDisplayName("ExamInputMarkAssigned_Label_EmployeeName")]
        public string EmployeeName { get; set; }
        [ResourceDisplayName("ExamInputMarkAssigned_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }
        [ResourceDisplayName("ExamInputMarkAssigned_Label_SchoolFacultyName")]
        public string SchoolFacultyName { get; set; }
        [ResourceDisplayName("ExamInputMarkAssigned_Label_ExamRoom")]
        public string StrExamRoom { get; set; }
        public int? SubjectOrder { get; set; }
        public string EthnicCode { get; set; }
        public List<long> LstExamRoomID { get; set; }
        public bool IsCheck { get; set; }

    }
}