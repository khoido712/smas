﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETExamPupilArea
{
    public class DOETExamPupilConstants
    {
        public const string LIST_YEAR = "listYear";
        public const string LIST_EXAMINATIONS = "listExaminations";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_EXAMGROUP = "listExamGroup";
        public const string LIST_EXAMPUPIL = "listExamPupil";
        public const string PAGE = "Page";
        public const string TOTAL = "Total";
        public const int PageSize = 15;
    }
}