﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamPupilArea.Controllers
{
    public class DOETExamPupilController : BaseController
    {
        #region contructor
        private readonly IDOETExamPupilBusiness DOETExamPupilBusiness;
        private readonly IDOETExamMarkBusiness DOETExamMarkBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IDOETExaminationsBusiness DOETExaminationsBusiness;
        private readonly IDOETExamGroupBusiness DOETExamGroupBusiness;
        private readonly IDOETExamSubjectBusiness DOETExamSubjectBusiness;
        private readonly IDOETSubjectBusiness DOETSubjectBusiness;
        public DOETExamPupilController(IDOETExamPupilBusiness DOETExamPupilBusiness, IDOETExamMarkBusiness DOETExamMarkBusiness,
            IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness, IDOETExaminationsBusiness DOETExaminationsBusiness,
            IDOETExamGroupBusiness DOETExamGroupBusiness, IDOETExamSubjectBusiness DOETExamSubjectBusiness, IDOETSubjectBusiness DOETSubjectBusiness)
        {
            this.DOETExamPupilBusiness = DOETExamPupilBusiness;
            this.DOETExamMarkBusiness = DOETExamMarkBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.DOETExaminationsBusiness = DOETExaminationsBusiness;
            this.DOETExamGroupBusiness = DOETExamGroupBusiness;
            this.DOETExamSubjectBusiness = DOETExamSubjectBusiness;
            this.DOETSubjectBusiness = DOETSubjectBusiness;
        }
        #endregion
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        private void SetViewData()
        {
            //var json=DoetHcmApi.GetKiThiTheoTruong(2017, "79767504", 2);

            
            //var monhoc = DoetHcmApi.GetMonThi(93);

            //var thisinh = DoetHcmApi.GetThiSinh(93, "79767504", 464);
            List<ComboObject> lstComboboxYear = this.GetListYear();
            ViewData[DOETExamPupilConstants.LIST_YEAR] = new SelectList(lstComboboxYear, "key", "value");
            List<DOETExaminations> lstExaminations = new List<DOETExaminations>();

            ViewData[DOETExamPupilConstants.LIST_EXAMINATIONS] = new SelectList(new List<SelectListItem>());
            ViewData[DOETExamPupilConstants.LIST_SUBJECT] = new SelectList(new List<SelectListItem>());
            ViewData[DOETExamPupilConstants.LIST_EXAMGROUP] = new SelectList(new List<SelectListItem>());
        }
        #region Ajax load Commbobox
        [HttpPost]
        public JsonResult AjaxLoadExaminations(int YearID)
        {
            List<DOETExaminations> lstExaminations = new List<DOETExaminations>();
            
            lstExaminations = DOETExaminationsBusiness.All.Where(p => p.UnitID == _globalInfo.SupervisingDeptID && p.Year == YearID).OrderByDescending(p=>p.CreateTime).ToList();
            return Json(new SelectList(lstExaminations, "ExaminationsID", "ExaminationsName"));
        }

        [HttpPost]
        public JsonResult AjaxLoadExamGroup(int ExaminationsID)
        {
            List<DOETExamGroup> lstExamGroup = new List<DOETExamGroup>();
            lstExamGroup = DOETExamGroupBusiness.All.Where(p => p.ExaminationsID == ExaminationsID).OrderBy(p=>p.ExamGroupCode).ToList();
            return Json(new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        [HttpPost]
        public JsonResult AjaxLoadExamSubject(int ExaminationsID,int ExamGroupID)
        {
            List<DOETExamSubjectBO> lstExamSubject = new List<DOETExamSubjectBO>();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID",ExaminationsID},
                {"ExamGroupID",ExamGroupID}
            };
            lstExamSubject = DOETExamSubjectBusiness.GetListExamSubject(dic).OrderBy(p => p.DOETSubjectName).ToList(); ;
            return Json(new SelectList(lstExamSubject, "DOETSubjectID", "DOETSubjectName"));
        }
        #endregion
        public PartialViewResult Search(FormCollection frm,GridCommand command)
        {
            int ExaminationsID = !string.IsNullOrEmpty(frm["cboExaminations"]) ? Int32.Parse(frm["cboExaminations"]) : 0;
            int ExamGroupID = !string.IsNullOrEmpty(frm["cboExamGroupID"]) ? Int32.Parse(frm["cboExamGroupID"]) : 0;
            int SubjectID = !string.IsNullOrEmpty(frm["cboSubjectID"]) ? Int32.Parse(frm["cboSubjectID"]) : 0;
            int YearID = !string.IsNullOrEmpty(frm["cboYear"]) ? Int32.Parse(frm["cboYear"]) : 0;
            string SchoolName = frm["SchoolName"];
            string SchoolCode = frm["SchoolCode"];
            string FullName = frm["FullName"];
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID",ExaminationsID},
                {"ExamGroupID",ExamGroupID},
                {"SubjectID",SubjectID},
                {"YearID",YearID},
                {"SchoolName",SchoolName},
                {"SchoolCode",SchoolCode},
                {"PupilName",FullName}
            };

            IQueryable<DOETExamPupilBO> iqExamPupilBO = DOETExamPupilBusiness.GetListExamPupil(dic);

            List<DOETExamPupilBO> lstResult = iqExamPupilBO.OrderBy(p => p.ExamineeNumber).ThenBy(p => p.Name).ThenBy(p => p.FullName).Skip((command.Page - 1) * DOETExamPupilConstants.PageSize).Take(DOETExamPupilConstants.PageSize).ToList();

            ViewData[DOETExamPupilConstants.LIST_EXAMPUPIL] = lstResult;
            ViewData[DOETExamPupilConstants.PAGE] = command.Page;
            ViewData[DOETExamPupilConstants.TOTAL] = iqExamPupilBO.Count();
            return PartialView("_GridExamPupil");
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchExamPupilAjax(FormCollection frm, GridCommand command)
        {
            int ExaminationsID = !string.IsNullOrEmpty(frm["cboExaminations"]) ? Int32.Parse(frm["cboExaminations"]) : 0;
            int ExamGroupID = !string.IsNullOrEmpty(frm["cboExamGroupID"]) ? Int32.Parse(frm["cboExamGroupID"]) : 0;
            int SubjectID = !string.IsNullOrEmpty(frm["cboSubjectID"]) ? Int32.Parse(frm["cboSubjectID"]) : 0;
            int YearID = !string.IsNullOrEmpty(frm["cboYear"]) ? Int32.Parse(frm["cboYear"]) : 0;
            string SchoolName = frm["SchoolName"];
            string SchoolCode = frm["SchoolCode"];
            string FullName = frm["FullName"];
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID",ExaminationsID},
                {"ExamGroupID",ExamGroupID},
                {"SubjectID",SubjectID},
                {"YearID",YearID},
                {"SchoolName",SchoolName},
                {"SchoolCode",SchoolCode},
                {"PupilName",FullName}
            };
            IQueryable<DOETExamPupilBO> iqExamPupilBO = DOETExamPupilBusiness.GetListExamPupil(dic);
            List<DOETExamPupilBO> lstResult = iqExamPupilBO.OrderBy(p => p.ExamineeNumber).ThenBy(p => p.Name).ThenBy(p => p.FullName).Skip((command.Page - 1) * DOETExamPupilConstants.PageSize).Take(DOETExamPupilConstants.PageSize).ToList();
            return View(new GridModel<DOETExamPupilBO>()
            {
                Data = lstResult,
                Total = iqExamPupilBO.Count()
            }); ;
        }
        #region Excel
        public FileResult ExportExcel()
        {
            int ExaminationsID = SMAS.Business.Common.Utils.GetInt(Request["ExaminationsID"]);
            int ExamGroupID = SMAS.Business.Common.Utils.GetInt(Request["ExamGroupID"]); ;
            int SubjectID = SMAS.Business.Common.Utils.GetInt(Request["SubjectID"]);
            int YearID = SMAS.Business.Common.Utils.GetInt(Request["YearID"]);
            string SchoolName = SMAS.Business.Common.Utils.GetString(Request["SchoolName"]);
            string SchoolCode = SMAS.Business.Common.Utils.GetString(Request["SchoolCode"]);
            string FullName = SMAS.Business.Common.Utils.GetString(Request["FullName"]);
            string DisplayYear = string.Empty;
            string SubjectName = string.Empty;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID",ExaminationsID},
                {"ExamGroupID",ExamGroupID},
                {"SubjectID",SubjectID},
                {"YearID",YearID},
                {"SchoolName",SchoolName},
                {"SchoolCode",SchoolCode},
                {"PupilName",FullName}
            };
            DOETSubject objDOETSubject = DOETSubjectBusiness.Find(SubjectID);
            List<DOETExamPupilBO> lstExamPupilBO = DOETExamPupilBusiness.GetListExamPupil(dic).OrderBy(p => p.ExamineeNumber).ThenBy(p => p.Name).ThenBy(p=>p.FullName).ToList();
            DOETExamPupilBO objExamPupilBO = null;
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Phong_So" + "/" + "DanhSachThiSinh.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //Fill dữ liệu chung
            string Title = "Kỳ thi {0} - Năm học {1}";
            SubjectName = objDOETSubject != null ? objDOETSubject.DOETSubjectName : "";
            DOETExaminations objExaminations = DOETExaminationsBusiness.Find(ExaminationsID);
            DisplayYear = objExaminations.Year + "-" + (objExaminations.Year + 1);
            Title = string.Format(Title, objExaminations != null ? objExaminations.ExaminationsName : "", DisplayYear);
            sheet.SetCellValue("A3", Title);
            sheet.SetCellValue("A4", "Môn " + SubjectName);
            //fill thông tin học sinh
            int startRow = 7;
            for (int i = 0; i < lstExamPupilBO.Count; i++)
            {
                objExamPupilBO = lstExamPupilBO[i];
                //STT
                sheet.SetCellValue(startRow, 1, i + 1);
                //Mã thí sinh
                sheet.SetCellValue(startRow, 2, objExamPupilBO.PupilCode);
                sheet.GetRange(startRow, 2, startRow, 2).WrapText();
                //Họ tên
                sheet.SetCellValue(startRow, 3, objExamPupilBO.FullName);
                //Ngày sinh
                sheet.SetCellValue(startRow, 4, objExamPupilBO.BirthDay.ToString("dd/MM/yyyy"));
                //Mã trường
                sheet.SetCellValue(startRow, 5, objExamPupilBO.SchoolCode);
                //Môn thi
                sheet.SetCellValue(startRow, 6, SubjectName);
                //SBD
                sheet.SetCellValue(startRow, 7, objExamPupilBO.ExamineeNumber);
                //Phòng thi
                sheet.SetCellValue(startRow, 8, objExamPupilBO.ExamRoomName);
                //Địa điểm thi
                sheet.SetCellValue(startRow, 9, objExamPupilBO.Location);
                //Điểm
                sheet.SetCellValue(startRow, 10, objExamPupilBO.Mark);
                sheet.GetRange(startRow, 7, startRow, 10).IsLock = false;
                startRow++;
            }
            sheet.GetRange(7, 1, lstExamPupilBO.Count + 6, 10).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "DanhSachThiSinh.xls";
            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int ExaminationsID, int ExamGroupID, int SubjectID,int YearID,string SchoolName,string SchoolCode,string FullName)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID",ExaminationsID},
                {"ExamGroupID",ExamGroupID},
                {"SubjectID",SubjectID},
                {"YearID",YearID},
                {"SchoolName",SchoolName},
                {"SchoolCode",SchoolCode},
                {"PupilName",FullName}
            };
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                string physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);

                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = obook.GetSheet(1);
                
                #region Doc du lieu tu file
                DOETExaminations objExaminations = DOETExaminationsBusiness.Find(ExaminationsID);
                int startRow = 7;
                #region check file
                string Title = string.Empty;
                Title = sheet.GetCellValue("A3").ToString();
                string DisplayYear = string.Empty;
                DisplayYear = objExaminations.Year + "-" + (objExaminations.Year + 1);
                string TitleSubject = string.Empty;
                TitleSubject = sheet.GetCellValue("A4").ToString();
                DOETSubject objSubject = DOETSubjectBusiness.Find(SubjectID);
                string SubjectName = objSubject != null ? objSubject.DOETSubjectName : string.Empty;
                if (!Title.ToUpper().Contains(objExaminations.ExaminationsName.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("File Excel không phải là file của kỳ thi " + objExaminations.ExaminationsName), "error"));
                }
                if (!Title.ToUpper().Contains(DisplayYear.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("File Excel không phải là file của năm học " + DisplayYear), "error"));
                }
                if (!TitleSubject.ToUpper().Contains(SubjectName.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("File Excel không phải là file của môn " + SubjectName), "error"));
                }

                #endregion
                #region doc du lieu
                List<DOETExamPupilBO> lstExamPupilBO = DOETExamPupilBusiness.GetListExamPupil(dic).OrderBy(p=>p.OrderInClass).ToList();
                DOETExamPupilBO objExamPupilBO = null;
                List<DOETExamPupilBO> lstResult = new List<DOETExamPupilBO>();
                DOETExamPupilBO objtmp = null;
                List<string> lstPupilCode = new List<string>();
                string PupilCode = string.Empty;
                string SchoolCodeFile = string.Empty;
                bool isError = false;
                string ErrorMessage = string.Empty;

                while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null)
                {
                    ErrorMessage = string.Empty;
                    isError = false;
                    objtmp = new DOETExamPupilBO();
                    PupilCode = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : "";
                    if (string.IsNullOrEmpty(PupilCode))
                    {
                        ErrorMessage += "- Mã học sinh không được để trống.<br/>";
                        isError = true;
                    }
                    else
                    {
                        objExamPupilBO = lstExamPupilBO.Where(p => p.PupilCode.Equals(PupilCode)).FirstOrDefault();
                        if (objExamPupilBO == null)
                        {
                            ErrorMessage += "- Mã học sinh không tồn tại.<br/>";
                            isError = true;
                        }
                        else
                        {
                            objtmp.PupilID = objExamPupilBO.PupilID;
                        }
                    }

                    if (!lstPupilCode.Contains(PupilCode))
                    {
                        lstPupilCode.Add(PupilCode);
                    }
                    else
                    {
                        ErrorMessage += "- Trùng mã học sinh.<br/>";
                        isError = true;
                    }


                    SchoolCodeFile = sheet.GetCellValue(startRow, 5) != null ? sheet.GetCellValue(startRow, 5).ToString() : "";
                    if (string.IsNullOrEmpty(SchoolCodeFile))
                    {
                        ErrorMessage += "- Mã trường không được để trống.<br/>";
                        isError = true;
                    }
                    else
                    {
                        objExamPupilBO = lstExamPupilBO.Where(p => p.SchoolCode.Equals(SchoolCodeFile)).FirstOrDefault();
                        if (objExamPupilBO == null)
                        {
                            ErrorMessage += "- Mã trường không tồn tại.<br/>";
                            isError = true;
                        }
                        else
                        {
                            objtmp.SchoolID = objExamPupilBO.SchoolID;
                        }
                    }
                    


                    objtmp.ExamineeNumber = sheet.GetCellValue(startRow, 7) != null ? sheet.GetCellValue(startRow, 7).ToString() : "";//SBD
                    objtmp.ExamRoomName = sheet.GetCellValue(startRow, 8) != null ? sheet.GetCellValue(startRow, 8).ToString() : "";//Phong thi
                    objtmp.Location = sheet.GetCellValue(startRow, 9) != null ? sheet.GetCellValue(startRow, 9).ToString() : "";//Dia diem thi
                    decimal Mark = 0;
                    string tmpMark = sheet.GetCellValue(startRow, 10) != null ? sheet.GetCellValue(startRow, 10).ToString() : "";
                    if (!string.IsNullOrEmpty(tmpMark))
                    {
                        if (decimal.TryParse(tmpMark, out Mark))
                        {
                            if (Mark < 0 || Mark > 10)
                            {
                                ErrorMessage += "- Điểm không hợp lệ.<br/>";
                                isError = true;
                                objtmp.DisplayMark = tmpMark;
                            }
                            else
                            {
                                if (Mark == 10 || Mark == 0)
                                {
                                    objtmp.DisplayMark = tmpMark;
                                }
                                else
                                {
                                    objtmp.DisplayMark = Mark.ToString("0.0");
                                }
                                objtmp.Mark = Mark;
                            }
                        }
                        else
                        {
                            ErrorMessage += "- Điểm không hợp lệ.<br/>";
                            objtmp.DisplayMark = tmpMark;
                            isError = true;
                        }
                    }
                    
                    
                    objtmp.PupilCode = PupilCode;
                    objtmp.SchoolCode = SchoolCodeFile;
                    objtmp.FullName = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString() : "";
                    objtmp.BirthDay = sheet.GetCellValue(startRow, 4) != null ? DateTime.Parse(sheet.GetCellValue(startRow, 4).ToString()) : new DateTime();
                    objtmp.ExamSubjectName = SubjectName;
                    objtmp.isErr = isError;
                    objtmp.ErrorMessage = ErrorMessage;
                    objtmp.ExamSubjectID = objSubject.DOETSubjectID;
                    lstResult.Add(objtmp);
                    startRow++;
                }
                #endregion
                #endregion
                #region tao du lieu luu vao DB
                if (lstResult.Where(p => p.isErr).Count() == 0)
                {
                    DOETExamPupilBusiness.UpdateExamPupil(lstResult, dic);
                }
                else
                {
                    ViewData[DOETExamPupilConstants.LIST_EXAMPUPIL] = lstResult;
                    Session["listExamPupil"] = lstResult.Where(p => p.isErr == false).ToList();
                    return Json(new JsonMessage(RenderPartialViewToString("_ViewDataError", null), "Grid"));
                }
                #endregion
                
            }

            return Json(new JsonMessage("Import thành công", "success"));
        }

         [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveErrImport(FormCollection frm)
        {
            int ExaminationsID = !string.IsNullOrEmpty(frm["hdfExaminationsID"]) ? Int32.Parse(frm["hdfExaminationsID"]) : 0;
            int ExamGroupID = !string.IsNullOrEmpty(frm["hdfExamGroupID"]) ? Int32.Parse(frm["hdfExamGroupID"]) : 0;
            int SubjectID = !string.IsNullOrEmpty(frm["hdfExamSubjectID"]) ? Int32.Parse(frm["hdfExamSubjectID"]) : 0;
            int YearID = !string.IsNullOrEmpty(frm["hdfYearID"]) ? Int32.Parse(frm["hdfYearID"]) : 0;
            string SchoolName = frm["hdfSchoolName"];
            string SchoolCode = frm["hdfSchoolCode"];
            string FullName = frm["hdfFullName"];
            List<DOETExamPupilBO> lstExamPupilBO = (List<DOETExamPupilBO>)Session["listExamPupil"];
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID",ExaminationsID},
                {"ExamGroupID",ExamGroupID},
                {"SubjectID",SubjectID},
                {"YearID",YearID},
                {"SchoolName",SchoolName},
                {"SchoolCode",SchoolCode},
                {"PupilName",FullName}
            };
            DOETExamPupilBusiness.UpdateExamPupil(lstExamPupilBO, dic);
            return Json(new JsonMessage("Import thành công", "success"));
        }
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}