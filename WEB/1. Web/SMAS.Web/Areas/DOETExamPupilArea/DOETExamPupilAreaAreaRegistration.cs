﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamPupilArea
{
    public class DOETExamPupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DOETExamPupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DOETExamPupilArea_default",
                "DOETExamPupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
