﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.UserAccountArea
{
    public class UserAccountAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "UserAccountArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "UserAccountArea_default",
                "UserAccountArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
