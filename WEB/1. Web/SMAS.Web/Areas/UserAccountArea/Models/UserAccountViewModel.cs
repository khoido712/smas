﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.UserAccountArea.Models
{
    public class UserAccountViewModel
    {
        [DisplayName("User Account")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int UserAccountID { get; set; }

        [DisplayName("G U")]
        public Nullable<System.Guid> GUID { get; set; }

        [DisplayName("Employee")]
        public Nullable<int> EmployeeID { get; set; }

        [DisplayName("Created User")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int CreatedUserID { get; set; }

        [DisplayName("Is Admin")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsAdmin { get; set; }

        [DisplayName("Is Active")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsActive { get; set; }

        [DisplayName("Created Date")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [DisplayName("Is Operational User")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsOperationalUser { get; set; }	
    }
}