﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.Web.Models;
using SMAS.Web.Constants;
using SMAS.Web.Filter;
using System.Transactions;
namespace SMAS.Web.Areas.UserAccountArea.Controllers
{   
    public class UserAccountController : BaseController
    {        
        private readonly IUserAccountBusiness userAccountBusiness;
        private readonly IRoleBusiness roleBusiness;
        private readonly IUserRoleBusiness userRoleBusiness;
        private readonly IEmployeeBusiness employeeBusiness;
        private readonly IGroupCatBusiness groupCatBusiness;
        private readonly IUserGroupBusiness userGroupBusiness;

        public UserAccountController(IUserAccountBusiness useraccountBusiness, 
            IRoleBusiness roleBusiness, 
            IUserRoleBusiness userRoleBusiness,
            IEmployeeBusiness employeeBusiness,
            IGroupCatBusiness groupCatBusiness,
            IUserGroupBusiness usergroupBusiness)
		{
			this.userAccountBusiness = useraccountBusiness;
            this.roleBusiness = roleBusiness;
            this.userRoleBusiness = userRoleBusiness;
            this.employeeBusiness = employeeBusiness;
            this.groupCatBusiness = groupCatBusiness;
            this.userGroupBusiness = usergroupBusiness;
		}
        #region CRUD
        //
        // GET: /UserAccount/

        public ViewResult Index()
        {
            
			return View(this.userAccountBusiness.All.ToList());
        }

        //
        // GET: /UserAccount/Details/5

        public ViewResult Details(int id)
        {            
            UserAccount useraccount = this.userAccountBusiness.Find(id);
			return View(useraccount);
        }
        #region Create
        //
        // GET: /UserAccount/Create

        public ActionResult Create()
        {

            return View();
        } 

        //
        // POST: /UserAccount/Create

        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Create(UserAccount useraccount)
        {
            if (ModelState.IsValid)
            {				
				this.userAccountBusiness.Insert(useraccount);
                return RedirectToAction("Index");  
            }
            return View(useraccount);
        }
        #endregion 
        #region CreateAcc
        //
        // GET: /UserAccount/Create
        
        public ActionResult CreateUser()
        {
            return View();
        }

        //
        // POST: /UserAccount/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomErrorHandler]
        public ActionResult CreateUser(CustomUserAcc model)
        {
            MembershipCreateStatus createStatus;
            if (ModelState.IsValid)
            {
                /*
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);
                
                if (createStatus == MembershipCreateStatus.Success)
                {
                    //Sau khi tao thanh cong trong bang aspnet_User thi tao UserAccount
                    MembershipUser createdUser = Membership.GetUser(model.UserName);
                    Guid aspnet_userID = (Guid) createdUser.ProviderUserKey;// lay id cua aspnet_User vua duoc tao
                    UserAccount userAccount = new UserAccount();
                    userAccount.GUID = aspnet_userID;
                    userAccount.EmployeeID = null;
                    userAccount.CreatedUserID = (int) HttpContext.Session[GlobalConstants.USERACCID]; //cho nay can xem lai
                    userAccount.IsAdmin = model.IsAdmin;
                    userAccount.IsActive = true;
                    userAccount.CreatedDate = DateTime.Now;
                    this.userAccountBusiness.Insert(userAccount); 
                 */
                    //FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    int createdUserID = (int) HttpContext.Session[GlobalConstants.USERACCID];
                    //using (TransactionScope scope = new TransactionScope())
                    //{
                    this.userAccountBusiness.CreateUser(model.UserName, createdUserID, model.IsAdmin, model.EmployeeID,out createStatus, model.Password);
                        //this.userAccountBusiness.Save();
                        //scope.Complete();
                    //}
                    if(createStatus == MembershipCreateStatus.Success)
                    {
                        if (model.IsAdmin == true) return RedirectToAction("AssignRoles", "UserAccount");
                            else return RedirectToAction("AssignGroups", "UserAccount");
                        
                    } else
                    {
                        ModelState.AddModelError("", ErrorCodeToString(createStatus)); 
                    }
                }
            // If we got this far, something failed, redisplay form           
            return View();
        }
        #endregion
        #region Edit
        //
        // GET: /UserAccount/Edit/5
 
        public ActionResult Edit(int id)
        {
            UserAccount useraccount = this.userAccountBusiness.Find(id);

            return View(useraccount);
        }

        //
        // POST: /UserAccount/Edit/5

        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Edit(UserAccount useraccount)
        {
            if (ModelState.IsValid)
            {
				this.userAccountBusiness.Update(useraccount);                
                return RedirectToAction("Index");
            }

            return View(useraccount);
        }
        #endregion
        //
        // GET: /UserAccount/Delete/5
 

        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            UserAccount useraccount = this.userAccountBusiness.Find(id);
            return View(useraccount);
        }

        //
        // POST: /UserAccount/Delete/5

        [HttpPost, ActionName("Delete")]		

        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this.userAccountBusiness.Delete(id);
            return RedirectToAction("Index");
        }
        #endregion
        
        #region AssignRoles
        //Gan Role cho toan bo UserAccount do nguoi dang Log on tao
        [HttpGet]
        public ActionResult AssignRoles()
        {
            //admin create acc
            //get user from session
            UserAccount userAdmin = Session[GlobalConstants.USERACCOUNT] as UserAccount;
            if (!userAdmin.IsAdmin) RedirectToAction("Index", "Home");                      
            //list account admin created by userAdmin
            List<UserAccount> lsAcc = userAccountBusiness.GetUsersByCreator(userAdmin.UserAccountID, true);//Neu list account rong thi hien thong bao va chuyen ve trang chu
            if (!lsAcc.Any() )
            {
                //hien thong bao 
                RedirectToAction("Index", "Home");
            }
            // Neu list account khong rong thi map voi Model CustomUserAcc
            List<CustomUserAcc> listCustomUserAcc = userAccountBusiness.MapFromUserAccountToCustomUserAcc(lsAcc);

            ViewData[GlobalConstants.ACCOUNTS] = new SelectList(listCustomUserAcc, "UserAccountID", "UserName");           

            List<Role> lsRoles = new List<Role>();
            List<RoleCheck> lsRoleChecks = new List<RoleCheck>();

            if (userAdmin.IsAdmin == true)
            { 
                //get role of admin from session 
                //Role selectedRole = HttpContext.Session[GlobalConstants.SELECTED_ROLE] as Role;
                int selectedAdminRole = (int)Session[GlobalConstants.ROLEID];
                // get direct childs of roleAdmin
                lsRoles = roleBusiness.All.Where(r => r.ParentID == selectedAdminRole && r.IsActive == true).ToList();                

                foreach (var rItem in lsRoles)
                {
                    RoleCheck rc = new RoleCheck { Role = rItem, IsChecked = false };
                    lsRoleChecks.Add(rc);
                }                        
            }
            return View("AssignRoles", lsRoleChecks);            
        }   

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignRoles(List<RoleCheck> collection, string UserAccountID)
        {
            //admin create acc
            //get user from session
            //UserAccount userAdmin = this.ControllerContext.HttpContext.Session[GlobalConstants.USERACCOUNT] as UserAccount;
            

            //list account created by userAdmin
            //List<UserAccount> lsAcc = userAccountBusiness.All.Where(ur => ur.CreatedUserID == userAdmin.UserAccountID).ToList();
            //ViewData[GlobalConstants.ACCOUNTS] = new SelectList(userAccountBusiness.All.Where(ur => ur.CreatedUserID == userAdmin.UserAccountID).ToList(), "UserAccountID");

            List<Role> lsRoleResults = new List<Role>();
            int userID = Convert.ToInt32(UserAccountID);
            if (userRoleBusiness.All.Where(ur => ur.UserID == userID).Count() != 0)
            {
                var lsOldUserRole = userRoleBusiness.All.Where(ur => ur.UserID == userID).ToList();
                foreach (var rc in collection.Where(rc => rc.IsChecked == true))
                {
                    lsRoleResults.Add(roleBusiness.Find(rc.Role.RoleID));
                    if (lsOldUserRole.Where(ur => ur.RoleID == rc.Role.RoleID).Count() == 0)
                    {
                        UserRole ur = new UserRole { RoleID = rc.Role.RoleID, UserID = userID };
                        userRoleBusiness.Insert(ur);
                    }
                }
            }
            else
            {
                foreach (var rc in collection.Where(rc => rc.IsChecked==true))
                {
                    lsRoleResults.Add(roleBusiness.Find(rc.Role.RoleID));
                    UserRole ur = new UserRole { RoleID = rc.Role.RoleID, UserID = userID };
                    userRoleBusiness.Insert(ur);
                }
            }            

            ViewData["UserAccountID"] = userID;
            //return View("AssignRolesConfirm");
            return View("UserRoleDetails",lsRoleResults);
        }
        #endregion
        //
        #region AssignGroups
        [HttpGet]
        public ActionResult AssignGroups()
        {
            //get user from session
            UserAccount userAdmin = Session[GlobalConstants.USERACCOUNT] as UserAccount;
            //Role selectedRole = HttpContext.Session[GlobalConstants.SELECTED_ROLE] as Role;
            int selectedRoleID = (int) Session[GlobalConstants.ROLEID];
            if (!userAdmin.IsAdmin) RedirectToAction("Index", "Home");
            //list normal account created by userAdmin
            List<UserAccount> lsAcc = userAccountBusiness.GetUsersByCreator(userAdmin.UserAccountID, false);//Neu list account rong thi hien thong bao va chuyen ve trang chu
            if (!lsAcc.Any() )
            {
                //hien thong bao 
                RedirectToAction("Index", "Home");
            }
            List<CustomUserAcc> listCustomUserAcc = userAccountBusiness.MapFromUserAccountToCustomUserAcc(lsAcc);
            ViewData[GlobalConstants.NORMAL_ACCOUNTS] = new SelectList(listCustomUserAcc, "UserAccountID", "UserName"); 
            //
            //lay danh sach cac Group do nguoi dung tao va theo Role chon luc dang nhap
            List<GroupCat> groups = groupCatBusiness.GetGroupByRoleAndCreatedUser(selectedRoleID, userAdmin.UserAccountID).ToList();
            List<GroupCheck> groupChecks = new List<GroupCheck>();
            if (groups.Any())
            {
                foreach (GroupCat g in groups)
                {
                    GroupCheck gc = new GroupCheck { group = g, isChecked = false };
                    groupChecks.Add(gc);
                }
            }
            return View("AssignGroups",groupChecks);
        }

        //
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignGroups(List<GroupCheck> groups, string userAccountID)
        {
            int userID = Convert.ToInt32(userAccountID);
            userGroupBusiness.AssignGroupsForUser(groups, userID);
            return RedirectToAction("Index", "UserGroup", new { area = "UserGroupArea" });
        }
       
        #endregion

        #region AssignEmployee
        //Get
        public ActionResult AssignEmployees()
        {
            //Admin create acc
            //Get userID from session
            UserAccount userAdmin = this.ControllerContext.HttpContext.Session[GlobalConstants.USERACCOUNT] as UserAccount;

            //List acc created by Admin
            List<UserAccount> lsAcc = userAccountBusiness.All.Where(ur => ur.CreatedUserID == userAdmin.UserAccountID).ToList();
            //ViewData[GlobalConstants.ACCOUNTS] = new SelectList(lsAcc, "UserAccountID", "UserAccountID");
            List<UserAccountCheck> lsUserAccCheck = new List<UserAccountCheck>();
            foreach (var ua in lsAcc)
            { 
                lsUserAccCheck.Add(new UserAccountCheck{UserAccount=ua,IsChecked=false});
            }


            //comboBox Employee
            ViewData[GlobalConstants.EMPLOYEES] = new SelectList(employeeBusiness.All.ToList(), "EmployeeID", "EmployeeName");

            //List<Role> lsRoles = new List<Role>();
            //List<RoleCheck> lsRoleChecks = new List<RoleCheck>();

            //if (userAdmin.IsAdmin == true)
            //{
            //    //get role of admin from session 
            //    int selectedAdminRole = 1;
            //    // get direct childs of roleAdmin
            //    lsRoles = roleBusiness.All.Where(r => r.ParentID == selectedAdminRole).ToList();

            //    foreach (var rItem in lsRoles)
            //    {
            //        RoleCheck rc = new RoleCheck { Role = rItem, IsChecked = false };
            //        lsRoleChecks.Add(rc);
            //    }

            //}            
            //return View("AssignRoles", lsRoleChecks);
            return View("AssignEmployees",lsUserAccCheck);
        }

        ////Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignEmployees(List<UserAccountCheck> lsUserAccCheck, string EmployeeID)
        {
            int employeeId = Convert.ToInt16(EmployeeID);
            //set userAcc.EmployeeID = employeeId 
            //(to bussiness: no savechange in loop)
            //foreach (var uc in lsUserAccCheck)
            //{
            //    UserAccount acc = userAccountBusiness.Find(uc.UserAccount.UserAccountID);
            //    acc.EmployeeID = employeeId;
            //    userAccountBusiness.Update(acc);
            //}
            return View();
        }

        #endregion

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }

}
