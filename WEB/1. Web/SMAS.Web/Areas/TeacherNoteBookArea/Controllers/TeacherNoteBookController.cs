﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Web.Areas.TeacherNoteBookArea.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text.RegularExpressions;
using System.Text;
using System.Globalization;
using SMAS.Web.Areas.ConfigDataLockArea;

namespace SMAS.Web.Areas.TeacherNoteBookArea.Controllers
{
    public class TeacherNoteBookController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness;
        private readonly ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        private readonly ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness;

        public TeacherNoteBookController(IClassProfileBusiness classProfileBusiness, IAcademicYearBusiness academicYearBusiness, IClassSubjectBusiness classSubjectBusiness,
            ISubjectCatBusiness subjectCatBusiness, IUserAccountBusiness userAccountBusiness, IClassSupervisorAssignmentBusiness classSupervisorAssignmentBusiness,
            ITeachingAssignmentBusiness teachingAssignmentBusiness, ITeacherNoteBookMonthBusiness teacherNoteBookMonthBusiness, ITeacherNoteBookSemesterBusiness teacherNoteBookSemesterBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness, IExemptedSubjectBusiness exemptedSubjectBusiness,
            IRestoreDataBusiness RestoreDataBusiness,
            IRestoreDataDetailBusiness RestoreDataDetailBusiness,
            ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.ClassSupervisorAssignmentBusiness = classSupervisorAssignmentBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.TeacherNoteBookMonthBusiness = teacherNoteBookMonthBusiness;
            this.TeacherNoteBookSemesterBusiness = teacherNoteBookSemesterBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
            this.RestoreDataDetailBusiness = RestoreDataDetailBusiness;
            this.LockRatedCommentPupilBusiness = LockRatedCommentPupilBusiness;
        }
        public ActionResult Index(int? ClassID)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string checkedSemester = string.Empty;
            DateTime datetimeNow = DateTime.Now.Date;
            if (!_globalInfo.IsCurrentYear)
            {
                if (objAcademicYear.FirstSemesterStartDate < datetimeNow)
                {
                    checkedSemester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND.ToString();
                }
                else
                {
                    checkedSemester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST.ToString();
                }
            }
            else
            {
                if (_globalInfo.Semester.HasValue && _globalInfo.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    checkedSemester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST.ToString();
                }
                else
                {
                    checkedSemester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND.ToString();
                }
            }
            ViewData[TeacherNoteBookConstant.SELECTED_SEMESTER] = checkedSemester;
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, false, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), SystemParamsInFile.SEMESTER_OF_YEAR_SECOND, false, false));
            ViewData[TeacherNoteBookConstant.LIST_SEMESTER] = listSemester;
            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();
            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.IsVnenClass == true).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[TeacherNoteBookConstant.LIST_CLASS] = listClass;

                var listCP = listClass.ToList();
                ClassProfile cp = null;
                if (listCP != null && listCP.Count() > 0)
                {
                    ViewData[TeacherNoteBookConstant.isClass] = true;
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            // Ưu tiên trước đối với giáo viên bộ môn
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                            List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                            {
                                cp = listSubjectTeacher.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                        .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listHead != null && listHead.Count > 0)
                                {
                                    cp = listHead.First();
                                }
                            }
                        }
                    }
                    if (listClass.Any(p => cp.ClassProfileID == p.ClassProfileID))
                    {
                        ViewData[TeacherNoteBookConstant.CLASS_PROFILE] = cp;
                    }
                    else
                    {
                        ViewData[TeacherNoteBookConstant.CLASS_PROFILE] = listCP.OrderBy(p => p.EducationLevelID).First();
                    }
                }
                else
                {
                    ViewData[TeacherNoteBookConstant.isClass] = false;
                }
            }
            List<ListMonth> lstMonth = new List<ListMonth>();
            ViewData[TeacherNoteBookConstant.LIST_MONTH] = new SelectList(lstMonth, "MonthID", "MonthName");

            List<SubjectCat> lstSubjectCat = new List<SubjectCat>();
            ViewData[TeacherNoteBookConstant.LIST_SUBJECT] = new SelectList(lstSubjectCat, "SubjectCatID", "DislayName");

            return View();
        }

        [HttpPost]
        public JsonResult AjaxLoadMonth(int SemesterID)
        {
            List<ListMonth> lstMonth = new List<ListMonth>();
            lstMonth = this.GetListMonth(SemesterID);
            string selected = string.Empty;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (!_globalInfo.IsCurrentYear)
            {
                if (objAca.Year < DateTime.Now.Date.Year)
                {
                    selected = SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? TeacherNoteBookConstant.MONTH_DGCK_HKI.ToString() : TeacherNoteBookConstant.MONTH_DGCK_HKII.ToString();
                }
                else
                {
                    selected = lstMonth.Select(p => p.MonthID).FirstOrDefault().ToString();
                }
            }
            else
            {
                selected = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString();
            }
            return Json(new SelectList(lstMonth, "MonthID", "MonthName", selected));
        }
        [HttpPost]
        public PartialViewResult AjaxLoadGrid(int ClassID, int MonthID, int? SubjectID, int SemesterID)
        {
            List<TeacherNoteBookViewModel> lstResult = new List<TeacherNoteBookViewModel>();
            TeacherNoteBookViewModel objResult = null;
            List<TeacherNoteBookMonthBO> lstTNMBO = new List<TeacherNoteBookMonthBO>();
            TeacherNoteBookMonthBO objTNBM = null;
            List<TeacherNoteBookSemester> lstTNBSAll = new List<TeacherNoteBookSemester>();
            List<TeacherNoteBookSemester> lstTNBS = new List<TeacherNoteBookSemester>();
            TeacherNoteBookSemester objTNBS = null;
            TeacherNoteBookSemester objTNBSYear = null;
            TeacherNoteBookSemester objTNBSOther = null;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAca.IsShowPupil.HasValue && objAca.IsShowPupil.Value;

            List<SubjectCatBO> lstSCBO = this.GetlistSubjectBO(ClassID, SemesterID); // Get list mon hoc
            ViewData[TeacherNoteBookConstant.LIST_SUBJECT_IMPORT] = lstSCBO;

            if (SubjectID.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SubjectID",SubjectID},
                    {"MonthID",MonthID},
                    {"SemesterID",SemesterID}
                };

                lstTNMBO = TeacherNoteBookMonthBusiness.GetListTeacherNoteBookMonth(dic);

                dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SubjectID",SubjectID}
                };
                lstTNBSAll = TeacherNoteBookSemesterBusiness.Search(dic).ToList();
                lstTNBS = lstTNBSAll.Where(o => o.SemesterID == SemesterID).ToList();
                //lay danh sach hoc sinh trong lop
                List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
                IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName);
                if (isNotShowPupil)
                {
                    iquery = iquery.Where(p => p.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_GRADUATED);
                }
                lstPOC = iquery.ToList();
                PupilOfClassBO objPOC = null;

                Dictionary<int, string> dicNoteMonthSubject = new Dictionary<int, string>();
                Dictionary<int, string> dicNoteMonthCQ = new Dictionary<int, string>();
                if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
                {
                    List<int> lstListMonthID = this.GetListMonth(SemesterID).Select(p => p.MonthID).ToList();

                    //lay nhan xet toan bo cac thang cua hoc sinh
                    dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SubjectID",SubjectID},
                    {"lstMonthID",lstListMonthID}
                };
                    List<TeacherNoteBookMonth> lstNoteMonthAllBO = TeacherNoteBookMonthBusiness.Search(dic).Where(p => p.MonthID != TeacherNoteBookConstant.MONTH_DGCK_HKI
                                                                   && p.MonthID != TeacherNoteBookConstant.MONTH_DGCK_HKII).OrderBy(p => p.MonthID).ToList();

                    string strCommentSubject = string.Empty;
                    string strCommentCQ = string.Empty;
                    int tmpPupilID = 0;
                    string tmpMonthID = string.Empty;
                    List<TeacherNoteBookMonth> lstTMP = new List<TeacherNoteBookMonth>();
                    for (int i = 0; i < lstPOC.Count; i++)
                    {
                        tmpPupilID = lstPOC[i].PupilID;
                        lstTMP = lstNoteMonthAllBO.Where(p => p.PupilID == tmpPupilID).ToList();
                        strCommentSubject = string.Empty;
                        strCommentCQ = string.Empty;
                        for (int j = 0; j < lstTMP.Count; j++)
                        {
                            tmpMonthID = this.GetMonthID(lstTMP[j].MonthID);
                            strCommentSubject += !string.IsNullOrEmpty(lstTMP[j].CommentSubject) ? tmpMonthID + ": " + lstTMP[j].CommentSubject + (j == lstTMP.Count - 1 ? "" : "\r\n") : "";
                            strCommentCQ += !string.IsNullOrEmpty(lstTMP[j].CommentCQ) ? tmpMonthID + ": " + lstTMP[j].CommentCQ + (j == lstTMP.Count - 1 ? "" : "\r\n") : "";
                        }

                        dicNoteMonthSubject[tmpPupilID] = strCommentSubject;
                        dicNoteMonthCQ[tmpPupilID] = strCommentCQ;
                    }
                }
                //lay danh sach hoc sinh mien giam
                IDictionary<string, object> dicExem = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SubjectID",SubjectID},
                    {"SemesterID",SemesterID}
                };
                List<ExemptedSubject> lstExem = ExemptedSubjectBusiness.Search(dicExem).ToList();

                dicExem = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SubjectID",SubjectID},
                    {"SemesterID",1}
                };
                List<ExemptedSubject> lstExemSem1 = ExemptedSubjectBusiness.Search(dicExem).ToList();

                IDictionary<string, object> dicClass = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };
                ClassSubject objCS = ClassSubjectBusiness.SearchByClass(ClassID, dicClass).Where(p => p.SubjectID == SubjectID).FirstOrDefault();


                bool isExempted = false;
                bool isExemptedSem1 = false;
                int pupilID = 0;
                for (int i = 0; i < lstPOC.Count; i++)
                {
                    isExempted = false;
                    isExemptedSem1 = false;
                    objPOC = lstPOC[i];
                    pupilID = objPOC.PupilID;
                    //Exempted
                    if (lstExem.Any(p => p.PupilID == pupilID))
                    {
                        isExempted = true;
                    }

                    if (lstExemSem1.Any(p => p.PupilID == pupilID))
                    {
                        isExemptedSem1 = true;
                    }

                    if (objCS.SectionPerWeekFirstSemester <= 0)
                    {
                        isExemptedSem1 = true;
                    }

                    objResult = new TeacherNoteBookViewModel();
                    objResult.PupilID = objPOC.PupilID;
                    objResult.FullName = objPOC.PupilFullName;
                    objResult.Status = objPOC.Status;
                    objTNBM = lstTNMBO.Where(p => p.PupilID == pupilID).FirstOrDefault();
                    objTNBS = lstTNBS.Where(p => p.PupilID == pupilID).FirstOrDefault();
                    objTNBSYear = lstTNBSAll.Where(p => p.PupilID == pupilID && p.SemesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                    if (SemesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        objTNBSOther = lstTNBSAll.Where(p => p.PupilID == pupilID && p.SemesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                    }
                    else
                    {
                        objTNBSOther = lstTNBSAll.Where(p => p.PupilID == pupilID && p.SemesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                    }
                    if (!isExempted)
                    {
                        if (objTNBM != null)
                        {
                            if (!string.IsNullOrEmpty(objTNBM.CommentSubject))
                            {
                                objResult.CommentSubjectHDGD = objTNBM.CommentSubject;
                            }
                            else
                            {
                                if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
                                {
                                    objResult.CommentSubjectHDGD = dicNoteMonthSubject[pupilID].Length > 1500 ? dicNoteMonthSubject[pupilID].Substring(0, 1500) : dicNoteMonthSubject[pupilID];
                                }
                                else
                                {
                                    objResult.CommentSubjectHDGD = objTNBM.CommentSubject;
                                }
                            }

                            if (!string.IsNullOrEmpty(objTNBM.CommentCQ))
                            {
                                objResult.CommentCQ = objTNBM.CommentCQ;
                            }
                            else
                            {
                                if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
                                {
                                    objResult.CommentCQ = dicNoteMonthCQ[pupilID].Length > 1500 ? dicNoteMonthCQ[pupilID].Substring(0, 1500) : dicNoteMonthCQ[pupilID];
                                }
                                else
                                {
                                    objResult.CommentCQ = objTNBM.CommentCQ;
                                }
                            }
                        }
                        else if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
                        {
                            objResult.CommentSubjectHDGD = dicNoteMonthSubject[pupilID];
                            objResult.CommentCQ = dicNoteMonthCQ[pupilID];
                        }

                        if (objTNBS != null)
                        {
                            if (objTNBS.PERIODIC_SCORE_MIDDLE.HasValue)
                            {
                                if ("10".Equals(objTNBS.PERIODIC_SCORE_MIDDLE.Value.ToString()))
                                {
                                    objResult.KTDKGK = "10";
                                }
                                else if ("0".Equals(objTNBS.PERIODIC_SCORE_MIDDLE.Value.ToString()))
                                {
                                    objResult.KTDKGK = "0";
                                }
                                else
                                {
                                    objResult.KTDKGK = objTNBS.PERIODIC_SCORE_MIDDLE.Value.ToString("0.0");
                                }
                            }

                            if (objTNBS.PERIODIC_SCORE_END.HasValue)
                            {
                                if ("10".Equals(objTNBS.PERIODIC_SCORE_END.Value.ToString()))
                                {
                                    objResult.KTDKCK = "10";
                                }
                                else if ("0".Equals(objTNBS.PERIODIC_SCORE_END.Value.ToString()))
                                {
                                    objResult.KTDKCK = "0";
                                }
                                else
                                {
                                    objResult.KTDKCK = objTNBS.PERIODIC_SCORE_END.Value.ToString("0.0");
                                }
                            }

                            if (objTNBS.AVERAGE_MARK.HasValue)
                            {
                                if ("10".Equals(objTNBS.AVERAGE_MARK.Value.ToString()))
                                {
                                    objResult.TBM = "10";
                                }
                                else if ("0".Equals(objTNBS.AVERAGE_MARK.Value.ToString()))
                                {
                                    objResult.TBM = "0";
                                }
                                else
                                {
                                    objResult.TBM = objTNBS.AVERAGE_MARK.Value.ToString("0.0");
                                }
                            }

                            objResult.KTDKGK_JUDGE = objTNBS.PERIODIC_SCORE_MIDDLE_JUDGE;
                            objResult.KTDKCK_JUDGE = objTNBS.PERIODIC_SCORE_END_JUDGLE;
                            objResult.TBM_JUDGE = objTNBS.AVERAGE_MARK_JUDGE;
                            objResult.Rate = objTNBS.Rate;
                        }

                        if (objTNBSYear != null)
                        {
                            if (objTNBSYear.AVERAGE_MARK.HasValue)
                            {
                                if ("10".Equals(objTNBSYear.AVERAGE_MARK.Value.ToString()))
                                {
                                    objResult.TBM_YEAR = "10";
                                }
                                else if ("0".Equals(objTNBSYear.AVERAGE_MARK.Value.ToString()))
                                {
                                    objResult.TBM_YEAR = "0";
                                }
                                else
                                {
                                    objResult.TBM_YEAR = objTNBSYear.AVERAGE_MARK.Value.ToString("0.0");
                                }
                            }

                            objResult.TBM_YEAR_JUDGE = objTNBSYear.AVERAGE_MARK_JUDGE;
                        }

                        if (objTNBSOther != null)
                        {
                            if (objTNBSOther.AVERAGE_MARK.HasValue)
                            {
                                if ("10".Equals(objTNBSOther.AVERAGE_MARK.Value.ToString()))
                                {
                                    objResult.TBM_OTHER = "10";
                                }
                                else if ("0".Equals(objTNBSOther.AVERAGE_MARK.Value.ToString()))
                                {
                                    objResult.TBM_OTHER = "0";
                                }
                                else
                                {
                                    objResult.TBM_OTHER = objTNBSOther.AVERAGE_MARK.Value.ToString("0.0");
                                }
                            }

                            objResult.TBM_OTHER_JUDGE = objTNBSOther.AVERAGE_MARK_JUDGE;
                        }
                    }
                    objResult.IsExempted = isExempted;
                    objResult.IsExemptedSem1 = isExemptedSem1;
                    lstResult.Add(objResult);
                }

                ViewData[TeacherNoteBookConstant.LIST_TEACHER_NOTE_BOOK] = lstResult;
                ViewData[TeacherNoteBookConstant.MONTH_ID] = MonthID;
                ViewData[TeacherNoteBookConstant.SEMESTERID] = SemesterID;

                ViewData[TeacherNoteBookConstant.IS_COMMENTING] = objCS.IsCommenting;
                bool permission = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, (int)ClassID, (int)SubjectID, SemesterID);

                if (_globalInfo.IsCurrentYear == false)
                {
                    ViewData[TeacherNoteBookConstant.LOCK_IMPORT] = "disabled";
                }
                else
                {
                    if (!permission)
                    {
                        ViewData[TeacherNoteBookConstant.LOCK_IMPORT] = "disabled";
                    }
                    else
                    {

                        if (_globalInfo.IsAdminSchoolRole == true)
                        {
                            ViewData[TeacherNoteBookConstant.LOCK_IMPORT] = "";
                        }
                        else
                        {
                            bool timeSemester = SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                  ? (objAca != null && objAca.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= objAca.FirstSemesterEndDate)
                                                                                  : (objAca != null && objAca.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= objAca.SecondSemesterEndDate);
                            if (timeSemester)
                            {
                                ViewData[TeacherNoteBookConstant.LOCK_IMPORT] = "";
                            }
                            else
                            {
                                ViewData[TeacherNoteBookConstant.LOCK_IMPORT] = "disabled";
                            }
                        }
                    }
                }
            }
            else
            {
                ViewData[TeacherNoteBookConstant.LIST_TEACHER_NOTE_BOOK] = null;
                ViewData[TeacherNoteBookConstant.MONTH_ID] = null;
                ViewData[TeacherNoteBookConstant.SEMESTERID] = null;
                ViewData[TeacherNoteBookConstant.IS_COMMENTING] = null;
                ViewData[TeacherNoteBookConstant.LOCK_IMPORT] = "";
            }

            ViewData["CurrentYear"] = objAca.Year;
            //check xem so co khoa nhap lieu hay khong
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle))
            {
                isLockInput = objLockInput.LockTitle.Contains("HK" + SemesterID);
            }
            ViewData[TeacherNoteBookConstant.IS_LOCK_INPUT] = isLockInput;
            if (isLockInput)
            {
                string LockUserName = string.Empty;
                int LockUserID = SemesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST ? objLockInput.UserLock1ID.Value : objLockInput.UserLock2ID.Value;
                int HierachyLevelID = this.GetHierachyLevelIDByUserAccountID(LockUserID);
                LockUserName = HierachyLevelID == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE ? "Sở giáo dục" : HierachyLevelID == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE ? "Phòng giáo dục" : "";
                ViewData[TeacherNoteBookConstant.LOCK_USER_NAME] = "Sổ tay giáo viên đã bị khóa nhập liệu bởi " + LockUserName;
            }

            IDictionary<string, object> dicLock = new Dictionary<string, object>();
            dicLock["SchoolID"] = _globalInfo.SchoolID.Value;
            dicLock["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicLock["ClassID"] = ClassID;
            dicLock["SubjectID"] = SubjectID;
            dicLock["lstEvaluationID"] = ListEvaluation();
            List<LockRatedCommentPupil> lstDataLock = LockRatedCommentPupilBusiness.Search(dicLock).ToList();
            this.IsDataLockComment(MonthID, SemesterID, lstDataLock);
            this.IsDataLockKTDK(SemesterID, lstDataLock);
           
            return PartialView("_GridTeacherNoteBook");
        }

        private void IsDataLockKTDK(int SemesterID, List<LockRatedCommentPupil> lstDataLock)
        {
            // KTĐK Giữa kỳ
            ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTGK] = false;
            // KTĐK cuối kỳ
            ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTCK] = false;

            string lockTitle_GK = string.Empty;
            string lockTitle_CK = string.Empty;

            if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {// Học kỳ 1            
                lockTitle_GK = (ConfigDataLockConstant.KTDK_GK1);
                lockTitle_CK = (ConfigDataLockConstant.KTDK_CK1);
            }
            else
            {// Học kỳ 2
                lockTitle_GK = (ConfigDataLockConstant.KTDK_GK2);
                lockTitle_CK = (ConfigDataLockConstant.KTDK_CK2);
            }

            var check_GK = lstDataLock.Where(x => x.LockTitle.Contains(lockTitle_GK)
                                      && x.EvaluationID == 4).FirstOrDefault();
            if (check_GK != null)
            {
                ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTGK] = true;
            }

            var check_CK = lstDataLock.Where(x => x.LockTitle.Contains(lockTitle_CK)
                                     && x.EvaluationID == 4).FirstOrDefault();
            if (check_CK != null)
            {
                ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTCK] = true;
            }
        }

        private void IsDataLockComment(int MonthID, int SemesterID, List<LockRatedCommentPupil> lstDataLock)
        {
            ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_COMMENT] = false;            
            string lockTitle_Comment = string.Empty;

            // Đánh giá cuối kỳ
            if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
            {
                if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI)
                {
                    lockTitle_Comment = ("C" + ConfigDataLockConstant.HK1);
                }
                else
                {
                    lockTitle_Comment = ("C" + ConfigDataLockConstant.HK2);
                }

                var check = lstDataLock.Where(x => x.LockTitle.Contains(lockTitle_Comment)
                                              && x.EvaluationID == 4).FirstOrDefault();
                if (check != null)
                {
                    ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_COMMENT] = true; 
                }
            }
            else
            {// Lựa chọn tháng
                int _length = MonthID.ToString().Length;
                string month = MonthID.ToString();
                string _strMonth = string.Empty;

                if (_length == 5)
                    _strMonth = month.Substring(4, 1).ToString();
                else
                    _strMonth = month.Substring(4, 2).ToString();

                if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    lockTitle_Comment = ("T" + _strMonth + ConfigDataLockConstant.HK1);                 
                }
                else
                {
                    lockTitle_Comment = ("T" + _strMonth + ConfigDataLockConstant.HK2);                                   
                }

                var check_Comment = lstDataLock.Where(x => x.LockTitle.Contains(lockTitle_Comment)
                                             && x.EvaluationID == 4).FirstOrDefault();
                if (check_Comment != null)
                {
                    ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_COMMENT] = true;
                }  
            }        
        }

        private List<int> ListEvaluation()
        {
            List<int> lstEvalutionID = new List<int>();
            lstEvalutionID.Add(4);
            lstEvalutionID.Add(5);
            lstEvalutionID.Add(6);
            lstEvalutionID.Add(7);
            return lstEvalutionID;
        }

        private List<ListMonth> GetListMonth(int semesterID)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime FDate = new DateTime();
            DateTime EDate = new DateTime();
            if (semesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                FDate = objAcademicYear.FirstSemesterStartDate.Value;
                EDate = objAcademicYear.FirstSemesterEndDate.Value;
            }
            else if (semesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                FDate = objAcademicYear.SecondSemesterStartDate.Value;
                EDate = objAcademicYear.SecondSemesterEndDate.Value;
            }
            FDate = new DateTime(FDate.Date.Year, FDate.Date.Month, 1);
            EDate = new DateTime(EDate.Date.Year, EDate.Date.Month, 1);
            List<ListMonth> lstListMonth = new List<ListMonth>();
            ListMonth objMonth = null;
            while (FDate <= EDate)
            {
                objMonth = new ListMonth();
                objMonth.MonthID = Int32.Parse(FDate.Date.Year.ToString() + FDate.Date.Month.ToString());
                objMonth.MonthName = "Tháng " + (FDate.Date.Month > 9 ? FDate.Date.Month.ToString() : "0" + FDate.Date.Month);
                lstListMonth.Add(objMonth);
                FDate = FDate.AddMonths(1);
            }
            objMonth = new ListMonth();
            objMonth.MonthID = (semesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? TeacherNoteBookConstant.MONTH_DGCK_HKI :
                (semesterID == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND) ? TeacherNoteBookConstant.MONTH_DGCK_HKII : 0;
            objMonth.MonthName = "Đánh giá cuối kỳ";
            lstListMonth.Add(objMonth);
            return lstListMonth;
        }

        [HttpPost]
        public JsonResult AjaxLoadSubject(int ClassID, int SemesterID)
        {
            List<SubjectCatBO> lstSCBO = this.GetlistSubjectBO(ClassID, SemesterID);
            return Json(new SelectList(lstSCBO, "SubjectCatID", "DisplayName"));
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult SaveTeacherNoteBook(FormCollection frm)
        {
            int ClassID = Int32.Parse(frm["ClassID"]);
            int MonthID = Int32.Parse(frm["MonthID"]);
            int SemesterID = Int32.Parse(frm["SemesterID"]);
            int SubjectID = Int32.Parse(frm["SubjectID"]);
            int iscommenting = Int32.Parse(frm["IsCommenting"]);
            int SchoolID = _globalInfo.SchoolID.Value;
            int AcademicYearID = _globalInfo.AcademicYearID.Value;

            // Kiem tra du lieu khoa
            IDictionary<string, object> dicLock = new Dictionary<string, object>();          
            dicLock["SchoolID"] = _globalInfo.SchoolID.Value;
            dicLock["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicLock["ClassID"] = ClassID;
            dicLock["SubjectID"] = SubjectID;
            dicLock["lstEvaluationID"] = ListEvaluation();
            List<LockRatedCommentPupil> lstDataLock = LockRatedCommentPupilBusiness.Search(dicLock).ToList();
            this.IsDataLockComment(MonthID, SemesterID, lstDataLock);
            this.IsDataLockKTDK(SemesterID, lstDataLock);
            bool lockComment = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_COMMENT];
            bool lockGK = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTGK];
            bool lockCK = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTCK];

            //Lay danh sach hoc sinh trong lop
            List<PupilOfClassBO> lstPupilOfClass = new List<PupilOfClassBO>();
            PupilOfClassBO objPupilOfClass = null;
            lstPupilOfClass = PupilOfClassBusiness.GetPupilInClass(ClassID).ToList();

            List<int> lstSubjectID = new List<int>();
            lstSubjectID.Add(SubjectID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"MonthID",MonthID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID},
                {"lstSubjectID",lstSubjectID}
            };
            List<TeacherNoteBookMonth> lstTeacherNoteBookMonthDB = TeacherNoteBookMonthBusiness.Search(dic).ToList();
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDB = TeacherNoteBookSemesterBusiness.Search(dic).ToList();

            List<TeacherNoteBookMonth> lstTeacherNoteBookMonth = new List<TeacherNoteBookMonth>();
            TeacherNoteBookMonth objTeacherNoteBookMonth = null;
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemester = new List<TeacherNoteBookSemester>();
            TeacherNoteBookSemester objTeacherNoteBookSemester = null;
            List<ActionAuditDataBO> lstActionAuditData = new List<ActionAuditDataBO>();
            int pupilID = 0;
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();

            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsmeterStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();

            StringBuilder oldObject = new StringBuilder();
            StringBuilder newObject = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            string comment = string.Empty;
            for (int i = 0; i < lstPupilOfClass.Count; i++)
            {
                objPupilOfClass = lstPupilOfClass[i];
                pupilID = objPupilOfClass.PupilID;
                comment = string.Empty;
                if (!"on".Equals(frm["chk_" + pupilID]) && objPupilOfClass.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                else
                {
                    #region Tao doi tuong TeacherNoteBookMonth
                    objTeacherNoteBookMonth = new TeacherNoteBookMonth();
                    objTeacherNoteBookMonth.PupilID = pupilID;
                    objTeacherNoteBookMonth.SchoolID = SchoolID;
                    objTeacherNoteBookMonth.AcademicYearID = AcademicYearID;
                    objTeacherNoteBookMonth.MonthID = MonthID;
                    objTeacherNoteBookMonth.SubjectID = SubjectID;
                    objTeacherNoteBookMonth.ClassID = ClassID;

                    var objTNBMonth = lstTeacherNoteBookMonthDB.Where(x => x.PupilID == pupilID).FirstOrDefault();
                    if (objTNBMonth != null)
                    {
                        if (lockComment)
                        {
                            objTeacherNoteBookMonth.CommentSubject = objTNBMonth.CommentSubject;
                            objTeacherNoteBookMonth.CommentCQ = objTNBMonth.CommentCQ;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(frm["CommentSubjectHDGD_" + pupilID]))
                            {
                                comment = frm["CommentSubjectHDGD_" + pupilID];
                                if (comment.Length > 1500)
                                {
                                    comment = comment.Substring(0, 1500);
                                }
                                objTeacherNoteBookMonth.CommentSubject = comment;
                            }
                            comment = string.Empty;
                            if (!string.IsNullOrWhiteSpace(frm["CommentCQ_" + pupilID]))
                            {
                                comment = frm["CommentCQ_" + pupilID];
                                if (comment.Length > 1500)
                                {
                                    comment = comment.Substring(0, 1500);
                                }
                                objTeacherNoteBookMonth.CommentCQ = comment;
                            }
                        }
                    }
                    else
                    {
                        if (lockComment)
                        {
                            objTeacherNoteBookMonth.CommentSubject = null;
                            objTeacherNoteBookMonth.CommentCQ = null;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(frm["CommentSubjectHDGD_" + pupilID]))
                            {
                                comment = frm["CommentSubjectHDGD_" + pupilID];
                                if (comment.Length > 1500)
                                {
                                    comment = comment.Substring(0, 1500);
                                }
                                objTeacherNoteBookMonth.CommentSubject = comment;
                            }
                            comment = string.Empty;
                            if (!string.IsNullOrWhiteSpace(frm["CommentCQ_" + pupilID]))
                            {
                                comment = frm["CommentCQ_" + pupilID];
                                if (comment.Length > 1500)
                                {
                                    comment = comment.Substring(0, 1500);
                                }
                                objTeacherNoteBookMonth.CommentCQ = comment;
                            }
                        }
                    }
                                     
                    lstTeacherNoteBookMonth.Add(objTeacherNoteBookMonth);
                    #endregion

                    #region Tao doi tuong TeacherNoteBookSemester
                    objTeacherNoteBookSemester = new TeacherNoteBookSemester();
                    objTeacherNoteBookSemester.PupilID = pupilID;
                    objTeacherNoteBookSemester.SchoolID = SchoolID;
                    objTeacherNoteBookSemester.AcademicYearID = AcademicYearID;
                    objTeacherNoteBookSemester.ClassID = ClassID;
                    objTeacherNoteBookSemester.SubjectID = SubjectID;
                    objTeacherNoteBookSemester.SemesterID = SemesterID;

                    var objTNBSemester = lstTeacherNoteBookSemsterDB.Where(x => x.PupilID == pupilID).FirstOrDefault();

                    if (objTNBSemester != null)
                    {
                        if (iscommenting == 0)
                        {
                            if (lockGK)
                            {
                                objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE = objTNBSemester.PERIODIC_SCORE_MIDDLE;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(frm["GK_" + pupilID]))
                                {
                                    objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE = decimal.Parse(frm["GK_" + pupilID].Replace(".", ","));
                                }
                            }

                            if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
                            {
                                if (lockCK)
                                {
                                    objTeacherNoteBookSemester.PERIODIC_SCORE_END = objTNBSemester.PERIODIC_SCORE_END;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(frm["CK_" + pupilID]))
                                    {
                                        objTeacherNoteBookSemester.PERIODIC_SCORE_END = decimal.Parse(frm["CK_" + pupilID].Replace(".", ","));
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (lockGK)
                            {
                                objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = objTNBSemester.PERIODIC_SCORE_MIDDLE_JUDGE;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(frm["JUDGE_GK_" + pupilID]))
                                {
                                    objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = frm["JUDGE_GK_" + pupilID];
                                }
                            }

                            if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
                            {
                                if (lockCK)
                                {
                                    objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = objTNBSemester.PERIODIC_SCORE_END_JUDGLE;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(frm["JUDGE_CK_" + pupilID]))
                                    {
                                        objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = frm["JUDGE_CK_" + pupilID];
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (iscommenting == 0)
                        {
                            if (!string.IsNullOrEmpty(frm["GK_" + pupilID]) && !lockGK)
                            {
                                objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE = decimal.Parse(frm["GK_" + pupilID].Replace(".", ","));
                            }

                            if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
                            {
                                if (!string.IsNullOrEmpty(frm["CK_" + pupilID]) && !lockCK)
                                {
                                    objTeacherNoteBookSemester.PERIODIC_SCORE_END = decimal.Parse(frm["CK_" + pupilID].Replace(".", ","));
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(frm["JUDGE_GK_" + pupilID]) && !lockGK)
                            {
                                objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = frm["JUDGE_GK_" + pupilID];
                            }
                            if (MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKI || MonthID == TeacherNoteBookConstant.MONTH_DGCK_HKII)
                            {
                                if (!string.IsNullOrEmpty(frm["JUDGE_CK_" + pupilID]) && !lockCK)
                                {
                                    objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = frm["JUDGE_CK_" + pupilID];
                                }
                            }
                        } 
                    }                                

                    //viethd31: Thuc hien tinh diem TBM
                    //Tinh diem TBM hoc ky
                    AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);

                    if (iscommenting == 0)
                    {
                        if (SemesterID == 1 && aca.Year == 2016)
                        {
                            objTeacherNoteBookSemester.AVERAGE_MARK = objTeacherNoteBookSemester.PERIODIC_SCORE_END;
                        }
                        else
                        {
                            objTeacherNoteBookSemester.AVERAGE_MARK = CalculateAverageMark(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE, objTeacherNoteBookSemester.PERIODIC_SCORE_END);
                        }
                    }
                    else
                    {
                        objTeacherNoteBookSemester.AVERAGE_MARK_JUDGE = objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE;
                    }

                    //Tinh diem TBM ca nam
                    lstTeacherNoteBookSemester.Add(objTeacherNoteBookSemester);
                    #endregion
                }
            }
            
            //try
            //{
            IDictionary<string, object> dicLogAction = new Dictionary<string, object>();
            ActionAuditDataBO objActionAuditBO = new ActionAuditDataBO();
            if (lstTeacherNoteBookMonth.Count > 0)
            {
                TeacherNoteBookMonthBusiness.InsertOrUpdate(lstTeacherNoteBookMonth, dic, lstTeacherNoteBookMonthDB, ref lstActionAuditData);
            }

            if (lstTeacherNoteBookSemester.Count > 0)
            {
                TeacherNoteBookSemesterBusiness.InsertOrUpdate(lstTeacherNoteBookSemester, dic,
                    lstActionAuditData, lstTeacherNoteBookSemsterDB, ref objActionAuditBO);
                // Tạo dữ liệu ghi log
                dicLogAction = new Dictionary<string, object>()
                    {
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,objActionAuditBO.OldData.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,objActionAuditBO.NewData.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objActionAuditBO.ObjID.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,objActionAuditBO.Description.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,objActionAuditBO.Parameter.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,objActionAuditBO.UserAction.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,objActionAuditBO.UserFunction.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,objActionAuditBO.UserDescription.ToString()}
                    };
            }
            else
            {
                ActionAuditDataBO objAC = null;
                lstActionAuditData = lstActionAuditData.Where(p => p.isInsertLog).ToList();
                for (int i = 0; i < lstActionAuditData.Count; i++)
                {
                    objAC = lstActionAuditData[i];

                    string tmpOld = string.Empty;
                    string tmpNew = string.Empty;
                    tmpOld = objAC.OldData.Length > 0 ? objAC.OldData.ToString().Substring(0, objAC.OldData.Length - 2) : "";
                    oldObjectStrtmp.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                    tmpNew = objAC.NewData.Length > 0 ? objAC.NewData.ToString().Substring(0, objAC.NewData.Length - 2) : "";
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                    userDescriptionsStrtmp.Append(objAC.UserDescription).Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");

                    objectIDStr.Append(objAC.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsmeterStr.Append(objAC.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append(objAC.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append(objAC.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(objAC.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectStr.Append(oldObjectStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectStr.Append(newObjectStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsStr.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);


                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                }
                dicLogAction = new Dictionary<string, object>()
                    {
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsmeterStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                    };
            }
            SetViewDataActionAudit(dicLogAction);
            //}
            //catch
            //{
            //    return Json(new JsonMessage("Có lỗi trong quá trình cập nhật.", "error"));
            //}

            return Json(new JsonMessage("Cập nhật thành công", "success"));
        }

        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult DeleteTeacherNoteBook(string strPupilID, int ClassID, int MonthID, int SubjectID, int SemesterID, int IsCommenting)
        {
            List<int> lstPupilID = !string.IsNullOrEmpty(strPupilID) ? strPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList() : new List<int>();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"MonthID",MonthID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID},
                {"lstPupilID",lstPupilID},
                {"IsCommenting",IsCommenting}
            };
            List<ActionAuditDataBO> lstActionAuditData = new List<ActionAuditDataBO>();
            ActionAuditDataBO objAC = new ActionAuditDataBO();
            //Luu thong tin de phuc hoi du lieu
            SubjectCat objJubject = SubjectCatBusiness.Find(SubjectID);
            ClassProfile objClass = ClassProfileBusiness.Find(ClassID);
            int year = objClass.AcademicYear.Year;
            string monthName = string.Empty;
            if (MonthID != 15 && MonthID != 16)
            {
                int tmp = Int32.Parse(MonthID.ToString().Substring(4, MonthID.ToString().Length - 4));
                monthName = "Tháng " + MonthID.ToString().Substring(4, MonthID.ToString().Length - 4);
            }
            else
            {
                monthName = "Đánh giá cuối kỳ";
            }

            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value,
                SCHOOL_ID = _globalInfo.SchoolID.Value,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_MARK
            };

            objRes.SHORT_DESCRIPTION = string.Format(" Sổ tay giáo viên lớp: {0}, học kỳ:{1},{2}, môn: {3}, năm học: {4} ",
                                                        objClass.DisplayName,
                                                        SemesterID,
                                                        monthName,
                                                        objJubject.DisplayName,
                                                        year + "-" + (year + 1)
                                                        );

            dic.Add("RESTORE_DATA", objRes);

            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();

            IDictionary<string, object> dicLock = new Dictionary<string, object>();
            dicLock["SchoolID"] = _globalInfo.SchoolID.Value;
            dicLock["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicLock["ClassID"] = ClassID;
            dicLock["SubjectID"] = SubjectID;
            dicLock["lstEvaluationID"] = ListEvaluation();
            List<LockRatedCommentPupil> lstDataLock = LockRatedCommentPupilBusiness.Search(dicLock).ToList();
            this.IsDataLockComment(MonthID, SemesterID, lstDataLock);
            this.IsDataLockKTDK(SemesterID, lstDataLock);
            bool lockComment = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_COMMENT];
            bool lockGK = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTGK];
            bool lockCK = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTCK];

            dic["lockComment"] = lockComment;
            if (!lockComment)
            {
                TeacherNoteBookMonthBusiness.DeleteTeacherNoteBookMonth(dic, ref lstActionAuditData, ref lstRestoreDetail);
            }


            if (!lockGK || !lockCK)
            {
                dic["lockGK"] = lockGK;
                dic["lockCK"] = lockCK;
                TeacherNoteBookSemesterBusiness.DeleteTeacherNoteBookSemester(dic, lstActionAuditData, ref objAC, ref lstRestoreDetail);
            }
                   
            IDictionary<string, object> dicLogAction = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,objAC.OldData.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objAC.ObjID.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,objAC.Description.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,objAC.Parameter.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,objAC.UserAction.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,objAC.UserFunction.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,objAC.UserDescription.ToString()}
            };
            SetViewDataActionAudit(dicLogAction);

            if (lstRestoreDetail != null && lstRestoreDetail.Count > 0)
            {
                objRes.SHORT_DESCRIPTION += objAC.RestoreMsgDescription;
                if (objRes.SHORT_DESCRIPTION.Length > 1000)
                {
                    objRes.SHORT_DESCRIPTION = objRes.SHORT_DESCRIPTION.ToString().Substring(0, 1000) + "...";
                }
                RestoreDataBusiness.Insert(objRes);
                RestoreDataBusiness.Save();
                RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
            }
            return Json(new JsonMessage("Xóa nhận xét thành công", "success"));
        }

        #region ImportExcel & ExportExcel
        [ActionAudit]
        public FileResult ExportExcel()
        {
            int ClassID = SMAS.Business.Common.Utils.GetInt(Request["ClassID"]);
            int MonthID = SMAS.Business.Common.Utils.GetInt(Request["MonthID"]); ;
            int SemesterID = SMAS.Business.Common.Utils.GetInt(Request["SemesterID"]); ;
            string className = ClassProfileBusiness.Find(ClassID).DisplayName;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            Stream excel = null;
            excel = this.SetDataToFileExcel(ClassID, SemesterID, MonthID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = string.Format("Sotay_{0}_{1}.xls", Utils.Utils.StripVNSignAndSpace(className), SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII");
            result.FileDownloadName = ReportName;
            ViewData[CommonKey.AuditActionKey.ObjectID] = ClassID;
            ViewData[CommonKey.AuditActionKey.Description] = "Xuất Excel Sổ tay giáo viên";
            ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_EXPORT;
            ViewData[CommonKey.AuditActionKey.userFunction] = "Sổ tay giáo viên";
            ViewData[CommonKey.AuditActionKey.userDescription] = "Xuất excel Sổ tay giáo viên, Lớp " + className + ", Học kỳ " + SemesterID + ", năm học " + objAca.Year + "-" + (objAca.Year + 1);
            return result;
        }
        [ActionAudit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int ClassID, int SemesterID, string arrayID)
        {
            StringBuilder errMsg;
            string[] ArrayID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (ArrayID == null || ArrayID.Count() == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn môn học"));
            }
            List<string> IDStrList = ArrayID.ToList();
            List<int> lstSubjectImport = IDStrList.Select(Int32.Parse).ToList();
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            List<ClassSubject> lstClassSubject = new List<ClassSubject>();
            if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && ClassID > 0)
            {
                lstClassSubject = GetSubjectImport(SemesterID, ClassID).Where(s => lstSubjectImport.Contains(s.SubjectID)).ToList();
            }
            if (lstClassSubject == null || lstClassSubject.Count == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa được phân công phụ trách môn học đã chọn"), JsonMessage.ERROR);
            }
            int schoolId = _globalInfo.SchoolID.Value;
            int partitionNumber = UtilsBusiness.GetPartionId(schoolId);

            var file = attachments.FirstOrDefault();  //The Name of the Upload component is "attachments"
            if (file == null)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format")));
            }

            Regex objHtml = new Regex(@"</?\w+((\s+\w+(\s*=\s*(?:""[^""]*""|'[^']*'|[^'"">\s]+))?)+\s*|\s*)/?>");
            bool IsMatch = false;
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            List<string> excelExtension = new List<string>();
            excelExtension.Add(".XLS");
            excelExtension.Add(".XLSX");
            var extension = Path.GetExtension(file.FileName);
            if (!excelExtension.Contains(extension.ToUpper()))
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
            }

            //luu ra dia
            var fileName = Path.GetFileNameWithoutExtension(file.FileName);
            fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
            string physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);

            file.SaveAs(physicalPath);
            Session["FilePath"] = physicalPath;
            IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);
            StringBuilder validateSubjectName = new StringBuilder();
            string subjectSheetName;
            IVTWorksheet sheet = null;
            List<IVTWorksheet> lstSheet = obook.GetSheets();

            #region Validate sheet name
            string msgTitle = string.Empty;
            string sheetName;
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                sheetName = Utils.Utils.StripVNSignAndSpace(lstClassSubject[i].SubjectCat.DisplayName);
                sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(sheetName));
                if (sheet == null)
                {
                    continue;
                }
                msgTitle = this.ErrValidateFileExcel(sheet, SemesterID, ClassID, lstClassSubject[i].ClassProfile.DisplayName, academicYear, lstClassSubject[i].SubjectID, sheetName);
                if (string.IsNullOrEmpty(msgTitle))
                {
                    continue;
                }
                break;
            }
            if (!String.IsNullOrEmpty(msgTitle))
            {
                string msg = String.Format("Import dữ liệu không thành công. {0} Thầy cô vui lòng kiểm tra lại.", msgTitle);
                return Json(new JsonMessage(msg, JsonMessage.ERROR));
            }

            //Kiem tra sheet mon hoc co trong danh sach mon hoc duoc import
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                subjectSheetName = lstClassSubject[i].SubjectCat.DisplayName;
                if (!lstSheet.Select(s => s.Name).Contains(Utils.Utils.StripVNSignAndSpace(subjectSheetName)))
                {
                    if (String.IsNullOrEmpty(validateSubjectName.ToString()))
                    {
                        validateSubjectName.Append(subjectSheetName);
                    }
                    else
                    {
                        validateSubjectName.Append(", " + subjectSheetName);
                    }
                }
            }
            if (!string.IsNullOrEmpty(validateSubjectName.ToString()))
            {
                return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
            }
            #endregion

            // Kiểm tra xem đã khóa cột dữ liệu nào
            bool lockComment = false;
            bool lockGK = false;
            bool lockCK = false;
            IDictionary<string, object> dicLock = new Dictionary<string, object>();
            dicLock["SchoolID"] = _globalInfo.SchoolID.Value;
            dicLock["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicLock["ClassID"] = ClassID;
            //dicLock["SubjectID"] = SubjectID;
            dicLock["lstEvaluationID"] = ListEvaluation();
            List<LockRatedCommentPupil> lstDataLock = LockRatedCommentPupilBusiness.Search(dicLock).ToList();

            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch["SchoolID"] = _globalInfo.SchoolID.Value;
            dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSearch["ClassID"] = ClassID;
            dicSearch["SemesterID"] = SemesterID;
            dicSearch["lstSubjectID"] = lstClassSubject.Select(x => x.SubjectID).Distinct().ToList();

            List<TeacherNoteBookMonth> lstTeacherNoteBookMonthDB = TeacherNoteBookMonthBusiness.Search(dicSearch).ToList();
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDB = TeacherNoteBookSemesterBusiness.Search(dicSearch).ToList();

            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>(); //lay danh sach hoc sinh trong lop
            lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            PupilOfClassBO objPOC = null;
            List<ImportViewModel> lstImportViewModel = new List<ImportViewModel>();
            ImportViewModel objImportViewModel = null;

            string subjectName = string.Empty;
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            string className = objCP.DisplayName;
            string semesterName = string.Empty;
            string Title = string.Empty;
            List<string> lstPupilCode = new List<string>();
            string PupilCode = string.Empty;
            if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            { semesterName = "Học kỳ 1"; }
            else { semesterName = "Học kỳ 2"; }

            int startRow = 0;
            string error = string.Empty;
            bool isErr = false;
            string DKT = string.Empty;
            float KT = 0;
            bool fileErr = false;
            int colComment = 0;
            int colExpression = 0;
            int startColumnTitle = 4;

            #region  Chi doc du lieu nhung Sheet Excel duoc chon
            StringBuilder strSubjectName = new StringBuilder();
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                sheetName = Utils.Utils.StripVNSignAndSpace(lstClassSubject[i].SubjectCat.DisplayName);
                strSubjectName.Append(lstClassSubject[i].SubjectCat.DisplayName);
                if (i < lstClassSubject.Count - 1)
                {
                    strSubjectName.Append(",");
                }
                sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(sheetName));
                if (sheet == null)
                {
                    fileErr = true;
                    continue;
                }
                else
                {
                    fileErr = false;
                }

                var lstDataLockBySubject = lstDataLock.Where(x => x.SubjectID == lstClassSubject[i].SubjectID).ToList();
                this.IsDataLockKTDK(SemesterID, lstDataLockBySubject);
                lockGK = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTGK];
                lockCK = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_KTCK];

                startRow = 8;
                lstPupilCode = new List<string>();
                string strCommentSubject = string.Empty;
                string strCommentCQ = string.Empty;
                List<ListMonth> lstListMonth = this.GetListMonth(SemesterID).ToList();
                while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null)
                {
                    //error = string.Empty;
                    errMsg = new StringBuilder();
                    errMsg.Append("Sheet: " + sheet.Name);
                    isErr = false;
                    objImportViewModel = new ImportViewModel();

                    #region Validate pupil code
                    PupilCode = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : "";
                    objImportViewModel.IsCommenting = lstClassSubject[i].IsCommenting;
                    if (string.IsNullOrEmpty(PupilCode))
                    {
                        //error += "Mã học sinh không được để trống,";
                        objImportViewModel.ErrorMessage = String.Format(" - Dòng {0} cột {1}: Mã học sinh không được để trống.\r\n", startRow, 2);
                        isErr = true;
                    }
                    else
                    {
                        objPOC = lstPOC.Where(p => p.PupilCode.Equals(PupilCode)).FirstOrDefault();
                        if (objPOC == null)
                        {
                            //error += "Mã học sinh không tồn tại,";
                            objImportViewModel.ErrorMessage = String.Format(" - Dòng {0} cột {1}: Mã học sinh không tồn tại.\r\n", startRow, 2);
                            isErr = true;
                        }
                        else
                        {
                            if (!lstPupilCode.Contains(PupilCode))
                            {
                                lstPupilCode.Add(PupilCode);
                                objImportViewModel.PupilID = objPOC.PupilID;
                            }
                            else
                            {
                                //error += "Trùng mã học sinh,"; 
                                objImportViewModel.ErrorMessage = String.Format("- Dòng {0} cột {1}: Trùng mã học sinh.\r\n", startRow, 2);
                                isErr = true;
                            }
                        }
                    }
                    #endregion

                    objImportViewModel.PupilCode = PupilCode;
                    objImportViewModel.FullName = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString() : "";
                    objImportViewModel.SubjectID = lstClassSubject[i].SubjectCat.SubjectCatID;
                    objImportViewModel.ClassID = objCP.ClassProfileID;
                    objImportViewModel.SemesterID = SemesterID;
                    objImportViewModel.SubjectName = lstClassSubject[i].SubjectCat.DisplayName;
                    objImportViewModel.lstMonthModel = new List<ImportMonthModel>();

                    #region Read Nhận xét về môn học & HĐGD - Biểu hiện nổi bật, mức độ hình thành phẩm chất, năng lực trong tất cả các tháng
                    for (int m = 0; m < lstListMonth.Count; m++)
                    {
                        colComment = startColumnTitle + 2 * m;
                        colExpression = startColumnTitle + 1 + 2 * m;

                        ImportMonthModel objMonth = new ImportMonthModel();
                        objMonth.MonthID = lstListMonth[m].MonthID;

                        this.IsDataLockComment(objMonth.MonthID, SemesterID, lstDataLockBySubject);
                        lockComment = (bool)ViewData[TeacherNoteBookConstant.IS_DATA_LOCK_COMMENT];

                        var objTNBMonth = lstTeacherNoteBookMonthDB.Where(x => x.MonthID == objMonth.MonthID
                                                                            && x.SubjectID == lstClassSubject[i].SubjectID
                                                                            && x.PupilID == objImportViewModel.PupilID).FirstOrDefault();
                        if (objTNBMonth != null)
                        {
                            #region // Tồn tại đối tượng
                            if (!lockComment)
                            {
                                strCommentSubject = sheet.GetCellValue(startRow, colComment) != null ? sheet.GetCellValue(startRow, colComment).ToString() : "";

                                IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(strCommentSubject));
                                if (IsMatch)
                                {
                                    //error += "Nhận xét về môn học & HĐGD chứa ký tự đặc biệt,";
                                    objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Nhận xét về môn học & HĐGD chứa ký tự đặc biệt.\r\n", startRow, colComment);
                                    isErr = true;
                                }
                                else
                                {
                                    if (strCommentSubject.Length > TeacherNoteBookConstant.MAXLENGTH)
                                    {
                                        strCommentSubject = strCommentSubject.Substring(0, TeacherNoteBookConstant.MAXLENGTH);
                                    }
                                }
                                objMonth.CommentSubject = strCommentSubject;

                                strCommentCQ = sheet.GetCellValue(startRow, colExpression) != null ? sheet.GetCellValue(startRow, colExpression).ToString() : "";
                                IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(strCommentCQ));
                                if (IsMatch)
                                {
                                    //error += "Biểu hiện chứa ký tự đặc biệt,";
                                    objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Biểu hiện chứa ký tự đặc biệt.\r\n", startRow, colExpression);
                                    isErr = true;
                                }
                                else
                                {
                                    if (strCommentCQ.Length > TeacherNoteBookConstant.MAXLENGTH)
                                    {
                                        strCommentCQ = strCommentCQ.Substring(0, TeacherNoteBookConstant.MAXLENGTH);
                                    }
                                }
                                objMonth.CommentCQ = strCommentCQ;
                            }
                            else
                            {
                                objMonth.CommentSubject = objTNBMonth.CommentSubject;
                                objMonth.CommentCQ = objTNBMonth.CommentCQ;
                            }
                            #endregion
                        }
                        else
                        {
                            #region // Thêm mới đối tượng
                            if (!lockComment)
                            {
                                strCommentSubject = sheet.GetCellValue(startRow, colComment) != null ? sheet.GetCellValue(startRow, colComment).ToString() : "";

                                IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(strCommentSubject));
                                if (IsMatch)
                                {
                                    //error += "Nhận xét về môn học & HĐGD chứa ký tự đặc biệt,";
                                    objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Nhận xét về môn học & HĐGD chứa ký tự đặc biệt.\r\n", startRow, colComment);
                                    isErr = true;
                                }
                                else
                                {
                                    if (strCommentSubject.Length > TeacherNoteBookConstant.MAXLENGTH)
                                    {
                                        strCommentSubject = strCommentSubject.Substring(0, TeacherNoteBookConstant.MAXLENGTH);
                                    }
                                }
                                objMonth.CommentSubject = strCommentSubject;

                                strCommentCQ = sheet.GetCellValue(startRow, colExpression) != null ? sheet.GetCellValue(startRow, colExpression).ToString() : "";
                                IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(strCommentCQ));
                                if (IsMatch)
                                {
                                    //error += "Biểu hiện chứa ký tự đặc biệt,";
                                    objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Biểu hiện chứa ký tự đặc biệt.\r\n", startRow, colExpression);
                                    isErr = true;
                                }
                                else
                                {
                                    if (strCommentCQ.Length > TeacherNoteBookConstant.MAXLENGTH)
                                    {
                                        strCommentCQ = strCommentCQ.Substring(0, TeacherNoteBookConstant.MAXLENGTH);
                                    }
                                }
                                objMonth.CommentCQ = strCommentCQ;
                            }
                            else
                            {
                                objMonth.CommentSubject = null;
                                objMonth.CommentCQ = null;
                            }
                            #endregion
                        }
                        objImportViewModel.lstMonthModel.Add(objMonth);
                    }
                    #endregion

                    #region Read Điểm KTĐK GKII	Điểm KTĐK CKII	Đánh giá
                    bool tmp = true;

                    var objTBNSemester = lstTeacherNoteBookSemsterDB.Where(x => x.SubjectID == lstClassSubject[i].SubjectID
                                                                    && x.PupilID == objImportViewModel.PupilID).FirstOrDefault();
                    if (objTBNSemester != null)
                    {
                        #region // Tồn tại đối tượng
                        #region // Kiểm tra khóa GK
                        if (lockGK)
                        {
                            objImportViewModel.KTGK = objTBNSemester.PERIODIC_SCORE_MIDDLE.HasValue ? objTBNSemester.PERIODIC_SCORE_MIDDLE.Value.ToString() : string.Empty;
                            objImportViewModel.KTDK_GK_JUDGE = objTBNSemester.PERIODIC_SCORE_MIDDLE_JUDGE;
                        }
                        else
                        {
                            DKT = sheet.GetCellValue(startRow, colExpression + 1) != null ? sheet.GetCellValue(startRow, colExpression + 1).ToString().Replace(".", ",") : "";
                            if (lstClassSubject[i].IsCommenting == 0)
                            {
                                if (!string.IsNullOrEmpty(DKT))
                                {
                                    tmp = float.TryParse(DKT, out KT);
                                    if (KT > 10 || KT < 0)
                                    {
                                        tmp = false;
                                    }
                                    if (tmp)
                                    {
                                        objImportViewModel.KTGK = KT.ToString();
                                    }
                                    else
                                    {
                                        objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Điểm KTĐK GK" + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ.\r\n", startRow, colExpression + 1);
                                        isErr = true;
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(DKT))
                                {
                                    if (!"Đ".Equals(DKT) && !"CĐ".Equals(DKT))
                                    {
                                        objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Điểm KTĐK GK" + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ.\r\n", startRow, colExpression + 1);
                                        isErr = true;
                                    }

                                    objImportViewModel.KTDK_GK_JUDGE = DKT;
                                }
                            }
                        }
                        #endregion

                        #region // Kiểm tra khóa CK
                        if (lockCK)
                        {
                            objImportViewModel.KTCK = objTBNSemester.PERIODIC_SCORE_END.HasValue ? objTBNSemester.PERIODIC_SCORE_END.Value.ToString() : string.Empty;
                            objImportViewModel.KTDK_CK_JUDGE = objTBNSemester.PERIODIC_SCORE_END_JUDGLE;
                        }
                        else
                        {
                            DKT = sheet.GetCellValue(startRow, colExpression + 2) != null ? sheet.GetCellValue(startRow, colExpression + 2).ToString().Replace(".", ",") : "";
                            if (lstClassSubject[i].IsCommenting == 0)
                            {
                                if (!string.IsNullOrEmpty(DKT))
                                {
                                    tmp = float.TryParse(DKT, out KT);
                                    if (KT > 10 || KT < 0)
                                    {
                                        tmp = false;
                                    }
                                    if (tmp)
                                    {
                                        objImportViewModel.KTCK = KT.ToString();
                                    }
                                    else
                                    {
                                        objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Điểm KTĐK CK" + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ.\r\n", startRow, colExpression + 2);
                                        isErr = true;
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(DKT))
                                {
                                    if (!"Đ".Equals(DKT) && !"CĐ".Equals(DKT))
                                    {
                                        objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Điểm KTĐK CK" + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ.\r\n", startRow, colExpression + 2);
                                        isErr = true;
                                    }
                                    objImportViewModel.KTDK_CK_JUDGE = DKT;
                                }
                            }
                        }
                        #endregion
                        #endregion
                    }
                    else
                    {
                        #region // Thêm mới đối tượng
                        #region // Kiểm tra khóa GK
                        if (lockGK)
                        {
                            objImportViewModel.KTGK =  string.Empty;
                            objImportViewModel.KTDK_GK_JUDGE = string.Empty;
                        }
                        else
                        {
                            DKT = sheet.GetCellValue(startRow, colExpression + 1) != null ? sheet.GetCellValue(startRow, colExpression + 1).ToString().Replace(".", ",") : "";
                            if (lstClassSubject[i].IsCommenting == 0)
                            {
                                if (!string.IsNullOrEmpty(DKT))
                                {
                                    tmp = float.TryParse(DKT, out KT);
                                    if (KT > 10 || KT < 0)
                                    {
                                        tmp = false;
                                    }
                                    if (tmp)
                                    {
                                        objImportViewModel.KTGK = KT.ToString();
                                    }
                                    else
                                    {
                                        //error += "Điểm KTĐK GK " + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ,";
                                        objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Điểm KTĐK GK" + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ.\r\n", startRow, colExpression + 1);
                                        isErr = true;
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(DKT))
                                {
                                    if (!"Đ".Equals(DKT) && !"CĐ".Equals(DKT))
                                    {
                                        //error += "Điểm KTĐK GK " + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ,";
                                        objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Điểm KTĐK GK" + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ.\r\n", startRow, colExpression + 1);
                                        isErr = true;
                                    }

                                    objImportViewModel.KTDK_GK_JUDGE = DKT;
                                }

                            }
                        }
                        #endregion

                        #region // Kiểm tra khóa CK
                        if (lockCK)
                        {
                            objImportViewModel.KTCK = string.Empty;
                            objImportViewModel.KTDK_CK_JUDGE = string.Empty;
                        }
                        else
                        {
                            DKT = sheet.GetCellValue(startRow, colExpression + 2) != null ? sheet.GetCellValue(startRow, colExpression + 2).ToString().Replace(".", ",") : "";
                            if (lstClassSubject[i].IsCommenting == 0)
                            {
                                if (!string.IsNullOrEmpty(DKT))
                                {
                                    tmp = float.TryParse(DKT, out KT);
                                    if (KT > 10 || KT < 0)
                                    {
                                        tmp = false;
                                    }
                                    if (tmp)
                                    {
                                        objImportViewModel.KTCK = KT.ToString();
                                    }
                                    else
                                    {
                                        //error += "Điểm KTĐK CK " + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ,";
                                        objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Điểm KTĐK CK" + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ.\r\n", startRow, colExpression + 2);
                                        isErr = true;
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(DKT))
                                {
                                    if (!"Đ".Equals(DKT) && !"CĐ".Equals(DKT))
                                    {
                                        //error += "Điểm KTĐK CK " + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ,";
                                        objImportViewModel.ErrorMessage += String.Format(" - Dòng {0} cột {1}: Điểm KTĐK CK" + (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II") + " không hợp lệ.\r\n", startRow, colExpression + 2);
                                        isErr = true;
                                    }
                                    objImportViewModel.KTDK_CK_JUDGE = DKT;
                                }
                            }
                        }
                        #endregion
                        #endregion
                    }                                                    
                    #endregion

                    if (!string.IsNullOrEmpty(objImportViewModel.ErrorMessage))
                    {
                        errMsg.Append(objImportViewModel.ErrorMessage + "\r\n");
                        objImportViewModel.ErrorMessage = errMsg.ToString();
                    }
                    objImportViewModel.IsError = isErr;
                    lstImportViewModel.Add(objImportViewModel);
                    startRow++;
                }
            }
            if (fileErr)
            {
                return Json(new JsonMessage(Res.Get("File import không đúng mẫu"), "error"));
            }
            #endregion

            #region Tao doi tuong cho bang TeacherNoteBookMonth and TeacherNoteBookSemester
            List<ImportViewModel> excelDataErrorList = lstImportViewModel.Where(p => p.IsError == true && p.ErrorMessage != null).ToList();
            if (excelDataErrorList != null && excelDataErrorList.Count > 0)
            {
                ViewData[TeacherNoteBookConstant.LIST_TEACHER_NOTE_BOOK] = excelDataErrorList;
                return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "ViewPopupError"));
            }
            else
            {
                List<TeacherNoteBookMonth> lstTeacherNoteBookMonth = new List<TeacherNoteBookMonth>();
                TeacherNoteBookMonth objTeacherNoteBookMonth = null;
                List<TeacherNoteBookSemester> lstTeacherNoteBookSemester = new List<TeacherNoteBookSemester>();
                TeacherNoteBookSemester objTeacherNoteBookSemester = null;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                for (int i = 0; i < lstImportViewModel.Count; i++)
                {
                    objImportViewModel = lstImportViewModel[i];
                    for (int j = 0; j < objImportViewModel.lstMonthModel.Count; j++)
                    {
                        ImportMonthModel imm = objImportViewModel.lstMonthModel[j];

                        objTeacherNoteBookMonth = new TeacherNoteBookMonth();
                        objTeacherNoteBookMonth.PupilID = objImportViewModel.PupilID;
                        objTeacherNoteBookMonth.SchoolID = _globalInfo.SchoolID.Value;
                        objTeacherNoteBookMonth.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objTeacherNoteBookMonth.ClassID = objImportViewModel.ClassID;
                        objTeacherNoteBookMonth.SubjectID = objImportViewModel.SubjectID;
                        objTeacherNoteBookMonth.MonthID = imm.MonthID;
                        objTeacherNoteBookMonth.CommentSubject = imm.CommentSubject;
                        objTeacherNoteBookMonth.CommentCQ = imm.CommentCQ;
                        lstTeacherNoteBookMonth.Add(objTeacherNoteBookMonth);
                    }

                    //doi tuong TeacherNoteSemester
                    objTeacherNoteBookSemester = new TeacherNoteBookSemester();
                    objTeacherNoteBookSemester.SchoolID = _globalInfo.SchoolID.Value;
                    objTeacherNoteBookSemester.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objTeacherNoteBookSemester.PupilID = objImportViewModel.PupilID;
                    objTeacherNoteBookSemester.ClassID = objImportViewModel.ClassID;
                    objTeacherNoteBookSemester.SemesterID = SemesterID;
                    objTeacherNoteBookSemester.SubjectID = objImportViewModel.SubjectID;

                    if (!string.IsNullOrEmpty(objImportViewModel.KTGK))
                    {
                        objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE = decimal.Parse(objImportViewModel.KTGK);
                    }
                    if (!string.IsNullOrEmpty(objImportViewModel.KTCK))
                    {
                        objTeacherNoteBookSemester.PERIODIC_SCORE_END = decimal.Parse(objImportViewModel.KTCK);
                    }
                    if (!string.IsNullOrEmpty(objImportViewModel.KTDK_GK_JUDGE))
                    {
                        objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = objImportViewModel.KTDK_GK_JUDGE;
                    }
                    if (!string.IsNullOrEmpty(objImportViewModel.KTDK_CK_JUDGE))
                    {
                        objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = objImportViewModel.KTDK_CK_JUDGE;
                    }
                    //objTeacherNoteBookSemester.Rate = objImportViewModel.Rate;

                    if (objImportViewModel.IsCommenting == 0)
                    {
                        if (SemesterID == 1 && academicYear.Year == 2016)
                        {
                            objTeacherNoteBookSemester.AVERAGE_MARK = objTeacherNoteBookSemester.PERIODIC_SCORE_END;
                        }
                        else
                        {
                            objTeacherNoteBookSemester.AVERAGE_MARK = CalculateAverageMark(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE, objTeacherNoteBookSemester.PERIODIC_SCORE_END);
                        }
                    }
                    else
                    {
                        objTeacherNoteBookSemester.AVERAGE_MARK_JUDGE = objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE;
                    }

                    lstTeacherNoteBookSemester.Add(objTeacherNoteBookSemester);
                }
                if (lstTeacherNoteBookMonth.Count > 0)
                {
                    dic = new Dictionary<string, object>();
                    dic.Add("SchoolID", _globalInfo.SchoolID);
                    dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
                    dic.Add("ClassID", ClassID);
                    dic.Add("SemesterID", SemesterID);
                    dic.Add("AppliedLevelID", _globalInfo.AppliedLevel);
                    dic.Add("lstSubjectID", lstTeacherNoteBookMonth.Select(p => p.SubjectID).Distinct().ToList());
                    TeacherNoteBookMonthBusiness.InsertOrUpdateForImport(lstTeacherNoteBookMonth, dic);
                }
                if (lstTeacherNoteBookSemester.Count > 0)
                {
                    ActionAuditDataBO objAC = new ActionAuditDataBO();
                    dic = new Dictionary<string, object>();
                    dic.Add("SchoolID", _globalInfo.SchoolID);
                    dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
                    dic.Add("ClassID", ClassID);
                    dic.Add("MonthID", 15);
                    dic.Add("SemesterID", SemesterID);
                    dic.Add("lstSubjectID", lstTeacherNoteBookSemester.Select(p => p.SubjectID).Distinct().ToList());
                    TeacherNoteBookSemesterBusiness.InsertOrUpdate(lstTeacherNoteBookSemester, dic, new List<ActionAuditDataBO>(), lstTeacherNoteBookSemsterDB, ref objAC);
                }
            }
            #endregion
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,""},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,ClassID},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,"Import sổ tay giáo viên"},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,ClassID},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,SMAS.Business.Common.GlobalConstants.ACTION_IMPORT},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,"Sổ tay giáo viên"},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,"Import Sổ tay giáo viên, Lớp " + objCP.DisplayName + ", Môn {" + strSubjectName + "}, " + "Học kỳ " + SemesterID + ", năm học " + academicYear.Year + "-" + (academicYear.Year + 1) + "."}
            };
            SetViewDataActionAudit(dicLog);
            return Json(new JsonMessage("Import thành công", "success"));
        }

        //#region Theo nghiep vu moi thi ko dung ham nay nua
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[ValidateInput(true)]
        //public JsonResult SaveErrImport(FormCollection frm)
        //{
        //    int ClassID = frm["hdfClassID"] != null ? Int32.Parse(frm["hdfClassID"]) : 0;
        //    int SemesterID = frm["hdfSemesterID"] != null ? Int32.Parse(frm["hdfSemesterID"]) : 0;
        //    int MonthID = frm["hdfMonthID"] != null ? Int32.Parse(frm["hdfMonthID"]) : 0;
        //    List<ImportViewModel> lstImportViewModel = (List<ImportViewModel>)Session["listTeacherNoteBook"];
        //    ImportViewModel objImportViewModel = null;
        //    List<TeacherNoteBookMonth> lstTeacherNoteBookMonth = new List<TeacherNoteBookMonth>();
        //    TeacherNoteBookMonth objTeacherNoteBookMonth = null;
        //    List<TeacherNoteBookSemester> lstTeacherNoteBookSemester = new List<TeacherNoteBookSemester>();
        //    TeacherNoteBookSemester objTeacherNoteBookSemester = null;
        //    IDictionary<string, object> dic = new Dictionary<string, object>();
        //    for (int i = 0; i < lstImportViewModel.Count; i++)
        //    {
        //        objImportViewModel = lstImportViewModel[i];
        //        //doi tuong TeacherNoteMonth
        //        objTeacherNoteBookMonth = new TeacherNoteBookMonth();
        //        objTeacherNoteBookMonth.PupilID = objImportViewModel.PupilID;
        //        objTeacherNoteBookMonth.SchoolID = _globalInfo.SchoolID.Value;
        //        objTeacherNoteBookMonth.AcademicYearID = _globalInfo.AcademicYearID.Value;
        //        objTeacherNoteBookMonth.ClassID = objImportViewModel.ClassID;
        //        objTeacherNoteBookMonth.SubjectID = objImportViewModel.SubjectID;
        //        objTeacherNoteBookMonth.MonthID = objImportViewModel.MonthID;
        //        objTeacherNoteBookMonth.CommentSubject = objImportViewModel.CommentSubject;
        //        objTeacherNoteBookMonth.CommentCQ = objImportViewModel.CommentCQ;
        //        lstTeacherNoteBookMonth.Add(objTeacherNoteBookMonth);
        //        //doi tuong TeacherNoteSemester
        //        objTeacherNoteBookSemester = new TeacherNoteBookSemester();
        //        objTeacherNoteBookSemester.SchoolID = _globalInfo.SchoolID.Value;
        //        objTeacherNoteBookSemester.AcademicYearID = _globalInfo.AcademicYearID.Value;
        //        objTeacherNoteBookSemester.PupilID = objImportViewModel.PupilID;
        //        objTeacherNoteBookSemester.ClassID = objImportViewModel.ClassID;
        //        objTeacherNoteBookSemester.SemesterID = SemesterID;
        //        objTeacherNoteBookSemester.SubjectID = objImportViewModel.SubjectID;
        //        if (!string.IsNullOrEmpty(objImportViewModel.KTGK))
        //        {
        //            objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE = decimal.Parse(objImportViewModel.KTGK);
        //        }
        //        if (!string.IsNullOrEmpty(objImportViewModel.KTCK))
        //        {
        //            objTeacherNoteBookSemester.PERIODIC_SCORE_END = decimal.Parse(objImportViewModel.KTCK);
        //        }
        //        objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = objImportViewModel.KTDK_GK_JUDGE;
        //        objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = objImportViewModel.KTDK_CK_JUDGE;
        //        objTeacherNoteBookSemester.Rate = objImportViewModel.Rate;
        //        lstTeacherNoteBookSemester.Add(objTeacherNoteBookSemester);
        //    }
        //    List<ActionAuditDataBO> lstActionAuditData = new List<ActionAuditDataBO>();
        //    ActionAuditDataBO objAc = new ActionAuditDataBO();
        //    if (lstTeacherNoteBookMonth.Count > 0)
        //    {

        //        dic = new Dictionary<string, object>();
        //        dic.Add("SchoolID", _globalInfo.SchoolID);
        //        dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
        //        dic.Add("ClassID", ClassID);
        //        dic.Add("MonthID", MonthID);
        //        dic.Add("lstSubjectID", lstTeacherNoteBookMonth.Select(p => p.SubjectID).Distinct().ToList());
        //        TeacherNoteBookMonthBusiness.InsertOrUpdate(lstTeacherNoteBookMonth, dic, ref lstActionAuditData);
        //    }
        //    if (lstTeacherNoteBookSemester.Count > 0)
        //    {
        //        dic = new Dictionary<string, object>();
        //        dic.Add("SchoolID", _globalInfo.SchoolID);
        //        dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
        //        dic.Add("ClassID", ClassID);
        //        dic.Add("MonthID", MonthID);
        //        dic.Add("SemesterID", SemesterID);
        //        dic.Add("lstSubjectID", lstTeacherNoteBookSemester.Select(p => p.SubjectID).Distinct().ToList());
        //        TeacherNoteBookSemesterBusiness.InsertOrUpdate(lstTeacherNoteBookSemester, dic, lstActionAuditData, ref objAc);
        //    }
        //    return Json(new JsonMessage("Import dữ liệu thành công", "success"));
        //}
        //#endregion

        #endregion

        private List<SubjectCatBO> GetlistSubjectBO(int ClassID, int SemesterID, bool isImport = false)
        {
            //cho phep tai khoan xem tat ca sanh sach mon hoc
            bool ViewAll = false;
            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lstSubjectCat = new List<SubjectCatBO>();
            lstSubjectCat = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value,
                                                                SemesterID, ClassID, ViewAll)
                                                                .Where(u => u.IsSubjectVNEN.HasValue && u.IsSubjectVNEN.Value)
                             join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                             where s.IsActive == true
                             && s.IsApprenticeshipSubject == false
                             select new SubjectCatBO()
                             {
                                 SubjectCatID = s.SubjectCatID,
                                 DisplayName = s.DisplayName,
                                 IsCommenting = cs.IsCommenting,
                                 OrderInSubject = s.OrderInSubject,
                                 SubjectIdInCrease = cs.SubjectIDIncrease
                             }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();

            int UserAccountID = _globalInfo.UserAccountID;
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
            // Neu la admin truong thi hien thi het
            if (_globalInfo.IsAdminSchoolRole || (ViewAll && !isImport) || (isGVCN && !isImport))
            {
                return lstSubjectCat;
            }
            else
            {
                int? TeacherID = UserAccountBusiness.Find(UserAccountID).EmployeeID;
                List<int> listPermitSubjectID = null; ;
                if (TeacherID.HasValue)
                {
                    // Phan cong giao vu la giao vien bo mon
                    if (ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == ClassID &&
                                                            o.AcademicYearID == _globalInfo.AcademicYearID &&
                                                            o.SchoolID == _globalInfo.SchoolID &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER).Count() > 0
                                                            )
                    {
                        listPermitSubjectID = lstSubjectCat.Select(o => o.SubjectCatID).ToList();
                    }
                    else
                    {
                        // Khong phai lai giao vien phan cong giao vu thi kiem tra la giao vien bo mon
                        IDictionary<string, object> searchInfo = new Dictionary<string, object>();
                        searchInfo["ClassID"] = ClassID;
                        searchInfo["TeacherID"] = TeacherID;
                        searchInfo["Semester"] = SemesterID;
                        searchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                        searchInfo["IsActive"] = true;
                        listPermitSubjectID = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfo).Select(o => o.SubjectID).ToList();
                    }
                }
                else
                {
                    listPermitSubjectID = new List<int>();
                }
                lstSubjectCat = lstSubjectCat.Where(p => listPermitSubjectID.Contains(p.SubjectCatID)).ToList();
                return lstSubjectCat;
            }
        }
        private Stream SetDataToFileExcel(int ClassID, int SemesterID, int MonthID)
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/HS" + "/" + "Sotay_Lop6A_HKI.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = null;
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            //lay danh sach mon hoc
            List<SubjectCatBO> lstSubjectCatBO = this.GetlistSubjectBO(ClassID, SemesterID);
            int SubjectID = 0;

            List<int> lstListMonthID = this.GetListMonth(SemesterID).Select(p => p.MonthID).ToList();
            //lay nhan xet toan bo cac thang cua hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"lstMonthID",lstListMonthID}
                };
            List<TeacherNoteBookMonth> lstNoteMonth = TeacherNoteBookMonthBusiness.Search(dic).OrderBy(p => p.MonthID).ToList();

            dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID}
                };
            List<TeacherNoteBookSemester> lstNoteSemester = TeacherNoteBookSemesterBusiness.Search(dic).ToList();

            dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SemesterID",SystemParamsInFile.SEMESTER_OF_YEAR_ALL}
                };
            List<TeacherNoteBookSemester> lstNoteSemesterYear = TeacherNoteBookSemesterBusiness.Search(dic).ToList();

            for (int i = 0; i < lstSubjectCatBO.Count; i++)
            {
                SubjectID = lstSubjectCatBO[i].SubjectCatID;
                if (lstSubjectCatBO[i].IsCommenting == SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK) // Mon tinh diem
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(1), "AZ100");
                }
                else // Mon nhan xet
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(2), "AZ100");
                }
                this.fillData(sheet, lstNoteMonth, lstNoteSemester, ClassID, SubjectID, SemesterID, MonthID, lstSubjectCatBO[i].IsCommenting, i + 1, ref lstValidation);
                sheet.SetFontName("Times New Roman", 11);
            }
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            return oBook.ToStreamValidationData(lstValidation);
        }
        private void fillData(IVTWorksheet sheet, List<TeacherNoteBookMonth> lstTeacherNoteBookMonth, List<TeacherNoteBookSemester> lstTeacherNoteBookSemester
            , int ClassID, int SubjectID, int SemesterID, int MonthID, int? isCommenting, int sheetIndex, ref List<VTDataValidation> lstValidation)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            //lay danh sach hoc sinh trong lop
            TeacherNoteBookMonth objTNBM = null;
            TeacherNoteBookSemester objTNBS = null;
            TeacherNoteBookSemester objTNBS1 = null;
            TeacherNoteBookSemester objTNBS2 = null;
            TeacherNoteBookSemester objTNBSYear = null;
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAcademicYear.IsShowPupil.HasValue && objAcademicYear.IsShowPupil.Value;
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName);
            if (isNotShowPupil)
            {
                iquery = iquery.Where(p => p.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            lstPOC = iquery.ToList();
            lstTeacherNoteBookMonth = lstTeacherNoteBookMonth.Where(p => p.SubjectID == SubjectID).ToList();
            lstTeacherNoteBookSemester = lstTeacherNoteBookSemester.Where(p => p.SubjectID == SubjectID).ToList();

            /// Fill Thong tin truong
            string SubjectName = SubjectCatBusiness.Find(SubjectID).DisplayName;
            string ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            string strTitle = string.Format("SỔ TAY GIÁO VIÊN MÔN {0} HỌC KỲ {1} - LỚP {2}", SubjectName.ToUpper(), SemesterID, ClassName.ToUpper());
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            sheet.SetCellValue("A3", strTitle);
            sheet.SetCellValue("A4", "NĂM HỌC " + objAcademicYear.DisplayTitle.ToUpper());

            //lay danh sach hoc sinh mien giam
            IDictionary<string, object> dicExem = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SubjectID",SubjectID},
                    {"SemesterID",SemesterID}
                };
            List<ExemptedSubject> lstExem = ExemptedSubjectBusiness.Search(dicExem).ToList();

            dicExem = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SubjectID",SubjectID},
                    {"SemesterID",1}
                };
            List<ExemptedSubject> lstExemSem1 = ExemptedSubjectBusiness.Search(dicExem).ToList();

            IDictionary<string, object> dicClass = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            ClassSubject objCS = ClassSubjectBusiness.SearchByClass(ClassID, dicClass).Where(p => p.SubjectID == SubjectID).FirstOrDefault();

            int startColumnTitle = TeacherNoteBookConstant.COLUMN_TITLE;
            int startRowTitle = TeacherNoteBookConstant.ROW_TILTE;
            IVTRange rgMonthTemp = sheet.GetRange("J6", "K8");
            PupilOfClassBO objPOC = null;
            bool isExempted = false;
            bool isExemptedSem1 = false;

            int pupilID = 0;
            int colComment = 0;
            int colExpression = 0;
            int tmpPupilID = 0;
            int startRow = 9;

            ///Fill cac thang tieu de
            List<ListMonth> lstListMonth = this.GetListMonth(SemesterID).ToList();
            for (int m = 0; m < lstListMonth.Count; m++)
            {

                colComment = startColumnTitle + 2 * m;
                colExpression = startColumnTitle + 1 + 2 * m;
                sheet.CopyPasteSameSize(rgMonthTemp, 6, colComment);

                sheet.SetCellValue(startRowTitle, colComment, lstListMonth[m].MonthName);
                sheet.SetCellValue(7, colComment, "Nhận xét về môn học & HĐGD");
                sheet.SetCellValue(7, colExpression, "Biểu hiện nổi bật, mức độ hình thành phẩm chất, năng lực");
                sheet.GetRange(6, colComment, 6, colExpression).Merge();
                int stRow = 9;
                List<TeacherNoteBookMonth> lstTMP = new List<TeacherNoteBookMonth>();
                for (int i = 0; i < lstPOC.Count; i++)//Duyet du lieu tung hoc sinh
                {
                    isExempted = false;
                    objPOC = lstPOC[i];
                    pupilID = objPOC.PupilID;
                    if (lstExem.Any(p => p.PupilID == pupilID))
                    {
                        isExempted = true;
                    }

                    if (!isExempted) // Fill Nhan xet va bieu hien
                    {
                        tmpPupilID = lstPOC[i].PupilID;
                        lstTMP = lstTeacherNoteBookMonth.Where(p => p.PupilID == tmpPupilID && p.MonthID == lstListMonth[m].MonthID).ToList();
                        for (int j = 0; j < lstTMP.Count; j++)
                        {
                            if (!string.IsNullOrEmpty(lstTMP[j].CommentSubject) || !string.IsNullOrEmpty(lstTMP[j].CommentCQ))
                            {
                                sheet.SetCellValue(stRow, colComment, lstTMP[j].CommentSubject);
                                sheet.SetCellValue(stRow, colExpression, lstTMP[j].CommentCQ);
                            }
                            else
                            {
                                sheet.SetCellValue(stRow, colComment, "");
                                sheet.SetCellValue(stRow, colExpression, "");
                                sheet.GetRange(stRow, colComment, stRow, colComment).SetHAlign(VTHAlign.xlHAlignLeft);
                                sheet.GetRange(stRow, colExpression, stRow, colExpression).SetHAlign(VTHAlign.xlHAlignLeft);
                            }
                        }
                    }
                    stRow++;
                }
            }
            IVTRange rgRateTemp = sheet.GetRange("D6", "I8");
            sheet.CopyPasteSameSize(rgRateTemp, 6, colComment + 2);
            //tao duong ke o, WrapText, can trai
            int lastRow = startRow + lstPOC.Count - 1;
            sheet.GetRange(startRow, 1, lastRow, colExpression + 6).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            sheet.GetRange(startRow, 3, lastRow, colExpression).WrapText();
            sheet.GetRange(startRow, colExpression + 1, lastRow, colExpression + 3).SetHAlign(VTHAlign.xlHAlignCenter);

            //Fill du lieu tung hoc sinh
            for (int i = 0; i < lstPOC.Count; i++)
            {
                isExempted = false;
                isExemptedSem1 = false;
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                if (lstExem.Any(p => p.PupilID == pupilID))
                {
                    isExempted = true;
                }

                if (lstExemSem1.Any(p => p.PupilID == pupilID))
                {
                    isExemptedSem1 = true;
                }

                if (objCS.SectionPerWeekFirstSemester <= 0)
                {
                    isExemptedSem1 = true;
                }


                objTNBM = lstTeacherNoteBookMonth.Where(p => p.PupilID == pupilID && p.MonthID == MonthID).FirstOrDefault();
                objTNBS1 = lstTeacherNoteBookSemester.Where(p => p.SemesterID == 1 && p.PupilID == pupilID).FirstOrDefault();
                objTNBS2 = lstTeacherNoteBookSemester.Where(p => p.SemesterID == 2 && p.PupilID == pupilID).FirstOrDefault();
                objTNBSYear = lstTeacherNoteBookSemester.Where(p => p.SemesterID == 3 && p.PupilID == pupilID).FirstOrDefault();
                objTNBS = SemesterID == 1 ? objTNBS1 : objTNBS2;

                sheet.SetCellValue(startRow, 1, i + 1);  //STT
                sheet.SetCellValue(startRow, 2, objPOC.PupilCode);  //Ma HS
                sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);  //Ho ten

                //  KTĐK GKI - KTĐK CKI - Đánh giá
                if (!isExempted)
                {
                    if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) // Ky 1
                    {
                        sheet.SetCellValue(6, colComment + 2, "Điểm KTĐK GK1");
                        sheet.SetCellValue(6, colExpression + 2, "Điểm KTĐK CK1");
                    }
                    else
                    {
                        sheet.SetCellValue(6, colComment + 2, "Điểm KTĐK GK2");
                        sheet.SetCellValue(6, colExpression + 2, "Điểm KTĐK CK2");
                    }
                    if (isCommenting == 0) // Mon tinh diem
                    {
                        sheet.SetCellValue(startRow, colComment + 2, objTNBS != null ? objTNBS.PERIODIC_SCORE_MIDDLE : (object)"");
                        sheet.SetCellValue(startRow, colExpression + 2, objTNBS != null ? objTNBS.PERIODIC_SCORE_END : (object)"");
                        sheet.GetRange(startRow, colComment + 2, startRow, colComment + 2).SetHAlign(VTHAlign.xlHAlignRight);
                        sheet.GetRange(startRow, colExpression + 2, startRow, colExpression + 2).SetHAlign(VTHAlign.xlHAlignRight);
                        sheet.SetCellValue(startRow, colExpression + 3, objTNBS1 != null ? objTNBS1.AVERAGE_MARK : (object)"");
                        sheet.SetCellValue(startRow, colExpression + 4, objTNBS2 != null ? objTNBS2.AVERAGE_MARK : (object)"");
                        sheet.SetCellValue(startRow, colExpression + 5, isExemptedSem1 ? "1" : string.Empty);

                        //Fill lai cong thuc
                        if (SemesterID == 1)
                        {
                            if (objAcademicYear.Year == 2016)
                            {
                                sheet.SetCellValue(startRow, colExpression + 3, string.Format("=IF({0}=\"\";\"\";{0})", UtilsBusiness.GetExcelColumnName(colExpression + 2) + startRow));

                            }
                            else
                            {
                                sheet.SetCellValue(startRow, colExpression + 3, string.Format("=IF(OR({0}=\"\";{1}=\"\");\"\";ROUND(({0}+2*{1})/3;1))", UtilsBusiness.GetExcelColumnName(colComment + 2) + startRow, UtilsBusiness.GetExcelColumnName(colExpression + 2) + startRow));
                            }

                        }
                        else
                        {
                            sheet.SetCellValue(startRow, colExpression + 4, string.Format("=IF(OR({0}=\"\";{1}=\"\");\"\";ROUND(({0}+2*{1})/3;1))", UtilsBusiness.GetExcelColumnName(colComment + 2) + startRow, UtilsBusiness.GetExcelColumnName(colExpression + 2) + startRow));
                            //sheet.SetCellValue(startRow, colExpression + 4, "=IF(OR(P8=\"\";Q8=\"\");\"\";ROUND((P8+2*Q8)/3;1))");
                            sheet.SetCellValue(startRow, colExpression + 6, string.Format("=IF({0}=\"\";IF(OR({1}=\"\";{2}=\"\");\"\";ROUND(({1}+2*{2})/3;1));{2})",
                                UtilsBusiness.GetExcelColumnName(colExpression + 5) + startRow,
                                UtilsBusiness.GetExcelColumnName(colExpression + 3) + startRow,
                                UtilsBusiness.GetExcelColumnName(colExpression + 4) + startRow));
                            ;

                        }
                    }
                    else
                    {
                        sheet.SetCellValue(startRow, colComment + 2, objTNBS != null ? objTNBS.PERIODIC_SCORE_MIDDLE_JUDGE : "");
                        sheet.SetCellValue(startRow, colExpression + 2, objTNBS != null ? objTNBS.PERIODIC_SCORE_END_JUDGLE : "");

                        sheet.SetCellValue(startRow, colExpression + 3, objTNBS1 != null ? objTNBS1.AVERAGE_MARK_JUDGE : "");
                        sheet.SetCellValue(startRow, colExpression + 4, objTNBS2 != null ? objTNBS2.AVERAGE_MARK_JUDGE : "");
                        sheet.SetCellValue(startRow, colExpression + 5, isExemptedSem1 ? 1 : 0);

                        //Fill lai cong thuc
                        if (SemesterID == 1)
                        {
                            sheet.SetCellValue(startRow, colExpression + 3, string.Format("=IF({0}=\"\";\"\";{0})", UtilsBusiness.GetExcelColumnName(colExpression + 2) + startRow));
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, colExpression + 4, string.Format("=IF({0}=\"\";\"\";{0})", UtilsBusiness.GetExcelColumnName(colExpression + 2) + startRow));
                            //sheet.SetCellValue(startRow, colExpression + 4, "=IF(OR(P8=\"\";Q8=\"\");\"\";ROUND((P8+2*Q8)/3;1))");
                            sheet.SetCellValue(startRow, colExpression + 6, string.Format("=IF({0}=\"\";\"\";{0})", UtilsBusiness.GetExcelColumnName(colExpression + 4) + startRow));
                            ;
                        }

                        sheet.GetRange(startRow, colExpression + 3, startRow, colExpression + 3).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, colExpression + 4, startRow, colExpression + 4).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, colExpression + 6, startRow, colExpression + 6).SetHAlign(VTHAlign.xlHAlignCenter);
                    }
                    //sheet.GetRange(startRow, 6, startRow, colExpression + 2).SetHAlign(VTHAlign.xlHAlignCenter);
                    //if (objTNBS != null)
                    //{
                    //    if (objTNBS.Rate == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT)
                    //    {
                    //        sheet.SetCellValue(startRow, colExpression + 3, "Hoàn thành");
                    //    }
                    //    else if (objTNBS.Rate == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT)
                    //    {
                    //        sheet.SetCellValue(startRow, colExpression + 3, "Chưa hoàn thành");
                    //    }
                    //}
                }

                if (objPOC.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING || isExempted)
                {
                    sheet.GetRange(1, startRow, 1, colExpression + 6).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                }
                if ((i + 1) % 5 == 0 || i == (lstPOC.Count - 1))
                {
                    sheet.GetRange(startRow, 1, startRow, colExpression + 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thick, VTBorderIndex.EdgeBottom);
                }
                if (objPOC.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && objPOC.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED || isExempted)
                {
                    sheet.GetRange(startRow, 1, startRow, colExpression + 6).FillColor(System.Drawing.Color.Yellow);
                    sheet.GetRange(startRow, 1, startRow, colExpression + 6).IsLock = true;
                }
                else
                {
                    sheet.GetRange(startRow, 4, startRow, colExpression + 6).IsLock = false;
                }
                startRow++;
            }
            for (int i = 0; i < 6; i++) // Xoa 3 cot KTĐK GKI - KTĐK CKI - Đánh giá
            {
                sheet.DeleteColumn(4);
            }
            string[] cd = new string[] { "Hoàn thành", "Chưa hoàn thành" };
            string[] str = new string[] { "Đ", "CĐ" };
            //VTDataValidation objValidation = new VTDataValidation()
            //{
            //    Contrains = cd,
            //    SheetIndex = sheetIndex,
            //    FromRow = 8,
            //    ToRow = 8 + lstPOC.Count,
            //    FromColumn = colExpression,
            //    ToColumn = colExpression,
            //    Type = 3
            //};
            VTDataValidation objValidation;
            if (isCommenting == 0)
            {
                objValidation = new VTDataValidation()
                {

                    FromRow = 8,
                    ToRow = 8 + lstPOC.Count,
                    FromColumn = colComment - 4,
                    ToColumn = colExpression - 4,
                    Type = VTValidationType.DECIMAL,
                    FromValue = "0",
                    ToValue = "10",
                    SheetIndex = sheetIndex
                };
            }
            else
            {
                objValidation = new VTDataValidation()
                {
                    Contrains = str,
                    SheetIndex = sheetIndex,
                    FromRow = 8,
                    ToRow = 8 + lstPOC.Count,
                    FromColumn = colComment - 4,
                    ToColumn = colExpression - 4,
                    Type = 3
                };
            }
            lstValidation.Add(objValidation);
            sheet.Name = Utils.Utils.StripVNSignAndSpace(SubjectName);
            sheet.ProtectSheet();

            sheet.DeleteRow(8);
            sheet.HideColumn(colExpression - 1);
            if (SemesterID == 1)
            {
                sheet.HideColumn(colExpression);
                sheet.HideColumn(colExpression - 2);

            }
            else
            {
                sheet.HideColumn(colExpression - 3);
            }


        }
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        private class ListMonth
        {
            public int MonthID { get; set; }
            public string MonthName { get; set; }
        }
        private List<ClassSubject> GetSubjectImport(int Semester, int ClassID)
        {
            List<ClassSubject> lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value,
                                                                                                   _globalInfo.SchoolID.Value, Semester, ClassID, 1);
            return lsClassSubject;
        }
        private string ErrValidateFileExcel(IVTWorksheet sheet, int semesterID, int classID, string className, AcademicYear objAcademicYear, int? subjectID = 0, string subjectName = "")
        {
            string ErrMessage = "";
            string Title = "";
            string semesterName = "";
            string academicYearName = objAcademicYear.DisplayTitle.Replace("-", "").Replace(" ", "");
            string TitleYear = "";
            if (semesterID == 1)
            {
                semesterName = "HOCKY1";
            }
            else if (semesterID == 2)
            {
                semesterName = "HOCKY2";
            }

            if (!String.IsNullOrEmpty(className))
            {
                className = Utils.Utils.StripVNSignAndSpace(className).ToUpper();
            }
            if (sheet.GetCellValue(3, 1) != null)
            {
                Title = Utils.Utils.StripVNSign(sheet.GetCellValue(3, 1).ToString().Replace(" ", "")).ToUpper();
            }

            if (sheet.GetCellValue(4, 1) != null)
            {
                TitleYear = Utils.Utils.StripVNSign(sheet.GetCellValue(4, 1).ToString().Replace("-", "").Replace(" ", ""));
            }

            if (!string.IsNullOrEmpty(subjectName))
            {
                subjectName = subjectName.ToUpper().Replace(" ", "");
            }
            if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(TitleYear) || !Title.Contains("SOTAYGIAOVIEN"))
            {
                return "File excel không đúng mẫu.";
            }
            else if (!Title.Contains(semesterName))
            {
                return "Học kỳ trong file excel không đúng.";
            }
            else if (!Title.Contains(className))
            {
                return "Lớp học trong file excel không đúng.";
            }
            else if (!Title.Contains(subjectName))
            {
                return "Môn học trong file excel không đúng.";
            }
            else if (!TitleYear.Contains(academicYearName))
            {
                return "Năm học trong file excel không đúng.";
            }
            return ErrMessage;
        }
        private string GetMonthID(int Month)
        {
            string Result = string.Empty;
            int M = Int32.Parse(Month.ToString().Substring(4, Month.ToString().Length - 4));
            if (M > 9)
            {
                Result = "Tháng thứ " + M;
            }
            else
            {
                Result = "Tháng thứ 0" + M;
            }
            return Result;
        }

        private decimal? CalculateAverageMark(decimal? coef1Mark, decimal? coef2Mark)
        {
            if (!coef1Mark.HasValue || !coef2Mark.HasValue)
            {
                return null;
            }
            return System.Math.Round((coef1Mark.Value + (coef2Mark.Value * 2)) / 3, 1);
        }
    }
}