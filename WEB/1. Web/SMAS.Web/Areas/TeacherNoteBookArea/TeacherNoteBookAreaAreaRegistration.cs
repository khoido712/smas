﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TeacherNoteBookArea
{
    public class TeacherNoteBookAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TeacherNoteBookArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TeacherNoteBookArea_default",
                "TeacherNoteBookArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
