﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeacherNoteBookArea
{
    public class TeacherNoteBookConstant
    {
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_MONTH = "listMonth";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_CLASS = "listClass";
        public const string CLASS_PROFILE = "ClassProfile";
        public const string LIST_TEACHER_NOTE_BOOK = "lstTeacherNoteBook";
        public const string SEMESTERID = "SemesterID";
        public const int MONTH_DGCK_HKI = 15;
        public const int MONTH_DGCK_HKII = 16;
        public const string MONTH_ID = "MonthID";
        public const string IS_COMMENTING = "IsCommenting";
        public const string LOCK_IMPORT = "LOCK_IMPORT";
        public const string SELECTED_SEMESTER = "SelectedSemester";
        public const int MAXLENGTH = 450;
        public const string isClass = "IsClass";
        public const string IS_LOCK_INPUT = "IsLockInput";
        public const string LOCK_USER_NAME = "LockUserName";

        // Date: 16-12-2015
        public const int FILE_UPLOAD_MAX_SIZE = 5120;//5MB
        public const string LIST_SUBJECT_IMPORT = "ListSubject_IMPORT";
        public const int COLUMN_TITLE = 10;
        public const int ROW_TILTE = 6;
        public const int ROW_START = 8;
        public const string IS_DATA_LOCK_KTGK = "IS_DATA_LOCK_KTGK";
        public const string IS_DATA_LOCK_KTCK = "IS_DATA_LOCK_KTCK";
        public const string IS_DATA_LOCK_CUOI_HK = "IS_DATA_LOCK_CUOI_HK";
        public const string IS_DATA_LOCK_COMMENT = "IS_DATA_LOCK_COMMENT";
    }
}