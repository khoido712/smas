﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.TeacherNoteBookArea.Models
{
    public class ImportViewModel
    {
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public int ClassID { get; set; }
        public int SemesterID { get; set; }
        public int MonthID { get; set; }
        public int SubjectID { get; set; }
        public string CommentSubject { get; set; }
        public string CommentCQ { get; set; }
        public string KTGK { get; set; }
        public string KTCK { get; set; }
        public int? Rate { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public string RateName { get; set; }
        public string SubjectName { get; set; }
        public string KTDK_GK_JUDGE { get; set; }
        public string KTDK_CK_JUDGE { get; set; }
        public int? IsCommenting { get; set; }
        public List<ImportMonthModel> lstMonthModel { get; set; }
    }
    public class ImportMonthModel
    {
        public int MonthID { get; set; }
        public string CommentSubject { get; set; }
        public string CommentCQ { get; set; }
    }
}