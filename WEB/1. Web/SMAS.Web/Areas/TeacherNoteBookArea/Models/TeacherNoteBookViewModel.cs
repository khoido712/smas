﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeacherNoteBookArea.Models
{
    public class TeacherNoteBookViewModel
    {
        public int PupilID { get; set; }
        public int Status { get; set; }
        public string FullName { get; set; }
        public string CommentSubjectHDGD { get; set; }
        public string CommentCQ { get; set; }
        public string KTDKGK { get; set; }
        public string KTDKCK { get; set; }
        public string KTDKGK_JUDGE { get; set; }
        public string KTDKCK_JUDGE { get; set; }
        public string TBM { get; set; }
        public string TBM_JUDGE { get; set; }
        public string TBM_YEAR { get; set; }
        public string TBM_YEAR_JUDGE { get; set; }
        public string TBM_OTHER { get; set; }
        public string TBM_OTHER_JUDGE { get; set; }
        public int? Rate { get; set; }
        public bool IsExempted { get; set; }
        public bool IsExemptedSem1 { get; set; }
    }
}