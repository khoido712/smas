﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.FamilyTypeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.FamilyTypeArea.Controllers
{
    public class FamilyTypeController : BaseController
    {        
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
		
		public FamilyTypeController (IFamilyTypeBusiness familytypeBusiness)
		{
			this.FamilyTypeBusiness = familytypeBusiness;
		}
		
		//
        // GET: /FamilyType/

        public ActionResult Index()
        {
            SetViewDataPermission("FamilyType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here

            IEnumerable<FamilyTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[FamilyTypeConstants.LIST_FAMILYTYPE] = lst;
            return View();
        }

		//
        // GET: /FamilyType/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;

            IEnumerable<FamilyTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[FamilyTypeConstants.LIST_FAMILYTYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            FamilyType familytype = new FamilyType();
            familytype.IsActive = true;
            TryUpdateModel(familytype); 
            Utils.Utils.TrimObject(familytype);

            this.FamilyTypeBusiness.Insert(familytype);
            this.FamilyTypeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int FamilyTypeID)
        {
            FamilyType familytype = this.FamilyTypeBusiness.Find(FamilyTypeID);
            TryUpdateModel(familytype);
            Utils.Utils.TrimObject(familytype);
            this.FamilyTypeBusiness.Update(familytype);
            this.FamilyTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            FamilyTypeBusiness.Delete(id);
            this.FamilyTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<FamilyTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<FamilyType> query = this.FamilyTypeBusiness.Search(SearchInfo);
            IQueryable<FamilyTypeViewModel> lst = query.Select(o => new FamilyTypeViewModel {               
						FamilyTypeID = o.FamilyTypeID,								
						Resolution = o.Resolution,								
						Description = o.Description											
            });

            return lst.ToList();
        }        
    }
}





