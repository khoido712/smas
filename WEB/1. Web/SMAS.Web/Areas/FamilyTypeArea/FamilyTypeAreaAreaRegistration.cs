﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.FamilyTypeArea
{
    public class FamilyTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FamilyTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FamilyTypeArea_default",
                "FamilyTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
