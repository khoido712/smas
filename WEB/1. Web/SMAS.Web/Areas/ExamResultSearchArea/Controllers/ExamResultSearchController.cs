﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.ExamResultSearchArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultSearchArea.Controllers
{
    public class ExamResultSearchController : BaseController
    {
        public int totalRecord = 0;
        #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IExamInputMarkBusiness ExamInputMarkBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExamPupilViolateBusiness ExamPupilViolateBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamSubjectBO> listExamSubject;
        private List<ExamRoom> listExamRoom;
        #endregion

        #region Constructor
        public ExamResultSearchController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness, IExamRoomBusiness ExamRoomBusiness, IExamInputMarkBusiness ExamInputMarkBusiness,
             IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IExamPupilViolateBusiness ExamPupilViolateBusiness, ISchoolSubjectBusiness SchoolSubjectBusiness, ISubjectCatBusiness SubjectCatBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ExamInputMarkBusiness = ExamInputMarkBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ExamPupilViolateBusiness = ExamPupilViolateBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;

        }
        #endregion

        #region Actions
        //
        // GET: /ExamResultSearchArea/ExamResultSearch/

        public ActionResult Index()
        {
            SetViewData();

            ViewData[ExamResultSearchConstants.LIST_RESULT] = new List<ListViewModel>();
            ViewData[ExamResultSearchConstants.TOTAL] = 0;
            return View();
        }

        // GET: /ExamSupervisoryViolate/Search
        public PartialViewResult Search(SearchViewModel form, GridCommand command)
        {
            Utils.Utils.TrimObject(form);

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;
            long? examRoomID = form.ExamRoomID;
            string examineeNumber = form.ExamineeNumber;
            string pupilName = form.PupilName;

            if (examinationsID == null || examGroupID == null || subjectID == null)
            {
                ViewData[ExamResultSearchConstants.TOTAL] = 0;
                return PartialView("_List", new List<ListViewModel>());
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();

            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["LastDigitSchoolID"] = partition;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SubjectID"] = subjectID;
            dic["ExamRoomID"] = examRoomID;
            dic["ExamineeNumber"] = examineeNumber;
            dic["PupilName"] = pupilName;
            dic["Page"] = 1;

            //Lay danh sach loi vi pham
            List<ExamPupilViolateBO> listViolate = ExamPupilViolateBusiness.GetListExamPupilViolate(dic);
            List<ListViewModel> listResult = _Search(dic, listViolate, ref totalRecord);
            ViewData[ExamResultSearchConstants.TOTAL] = totalRecord;

            ViewData[ExamResultSearchConstants.HIDDEN_EXAMINATIONS_ID] = examinationsID;
            ViewData[ExamResultSearchConstants.HIDDEN_EXAM_GROUP_ID] = examGroupID;
            ViewData[ExamResultSearchConstants.HIDDEN_SUBJECT_ID] = subjectID;
            ViewData[ExamResultSearchConstants.HIDDEN_EXAM_ROOM_ID] = examRoomID;
            ViewData[ExamResultSearchConstants.HIDDEN_EXAMINEE_NUMBER] = examineeNumber;
            ViewData[ExamResultSearchConstants.HIDDEN_PUPIL_NAME] = pupilName;
            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(SearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;
            long? examRoomID = form.ExamRoomID;
            string examineeNumber = form.ExamineeNumber;
            string pupilName = form.PupilName;

            if (examinationsID == null || examGroupID == null || subjectID == null)
            {
                return View(new GridModel<ListViewModel>
                {
                    Total = 0,
                    Data = new List<ListViewModel>()
                });
            }

            //Add search info - Navigate to Search function in business
            IDictionary<string, object> dic = new Dictionary<string, object>();
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["LastDigitSchoolID"] = partition;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SubjectID"] = subjectID;
            dic["ExamRoomID"] = examRoomID;
            dic["ExamineeNumber"] = examineeNumber;
            dic["PupilName"] = pupilName;
            dic["Page"] = currentPage;

            //Lay danh sach loi vi pham
            List<ExamPupilViolateBO> listViolate = ExamPupilViolateBusiness.GetListExamPupilViolate(dic);
            List<ListViewModel> listResult = _Search(dic, listViolate, ref totalRecord);
            //Kiem tra button
            return View(new GridModel<ListViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }


        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamSubject(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamSubjectBO>(), "SubjectID", "SubjectName"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            List<ExamSubjectBO> listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).ToList();

            return Json(new SelectList(listExamSubject, "SubjectID", "SubjectName"));
        }

        public JsonResult AjaxLoadExamRoom(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamRoom>(), "ExamRoomID", "ExamRoomCode"));
            }

            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                  .Where(o => o.ExamGroupID == examGroupID)
                                                  .OrderBy(o => o.ExamRoomCode)
                                                  .ToList();

            return Json(new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode"));
        }
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["SubjectID"] = form.SubjectID;
            dic["ExamRoomID"] = form.ExamRoomID;
            dic["PupilName"] = form.PupilName;
            dic["ExamineeNumber"] = form.ExamineeNumber;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_EXAM_INPUT_MARK;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ExamInputMarkBusiness.GetExamInputMarkReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ExamInputMarkBusiness.CreateExamInputMarkReport(dic);
                processedReport = ExamInputMarkBusiness.InsertExamInputMarkReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel form)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["SubjectID"] = form.SubjectID;
            dic["ExamRoomID"] = form.ExamRoomID;
            dic["PupilName"] = form.PupilName;
            dic["ExamineeNumber"] = form.ExamineeNumber;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            Stream excel = ExamInputMarkBusiness.CreateExamInputMarkReport(dic);
            ProcessedReport processedReport = ExamInputMarkBusiness.InsertExamInputMarkReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_EXAM_INPUT_MARK,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamResultSearchConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamResultSearchConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;

            //Lay danh sach mon thi
            if (defaultExamGroupID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = defaultExamID;
                dic["ExamGroupID"] = defaultExamGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();
            }
            else
            {
                listExamSubject = new List<ExamSubjectBO>();
            }
            ViewData[ExamResultSearchConstants.CBO_EXAM_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");

            //Lay danh sach phong thi
            if (defaultExamGroupID != null)
            {
                listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == defaultExamID.Value)
                                                 .Where(o => o.ExamGroupID == defaultExamGroupID.Value)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            }
            else
            {
                listExamRoom = new List<ExamRoom>();
            }
            ViewData[ExamResultSearchConstants.CBO_EXAM_ROOM] = new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode");
        }


        private List<ListViewModel> _Search(IDictionary<string, object> dic, List<ExamPupilViolateBO> listViolate, ref int totalRecord)
        {
            List<ListViewModel> listResult = new List<ListViewModel>();
            List<ExamInputMarkBO> listExamResult = new List<ExamInputMarkBO>();
            int currentPage = SMAS.Business.Common.Utils.GetInt(dic, "Page");
            IQueryable<ExamInputMarkBO> iquery = this.ExamInputMarkBusiness.SearchForResultSearch(dic).OrderBy(o => o.ExamineeNumber);
            totalRecord = iquery.Count();

            int subjectID = SMAS.Business.Common.Utils.GetInt(dic, "SubjectID");
            //Lay ten mon
            string subjectName = SubjectCatBusiness.Find(subjectID).SubjectName;
            //Lay kieu mon
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = _globalInfo.AcademicYearID;
            tmpDic["SubjectID"] = subjectID;
            tmpDic["AppliedLevel"] = _globalInfo.AppliedLevel;
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), tmpDic).FirstOrDefault();
            int isCommentSubject = GlobalConstants.ISCOMMENTING_TYPE_MARK;
            if (schoolSubject != null && schoolSubject.IsCommenting!=null)
            {
                isCommentSubject = schoolSubject.IsCommenting.Value;
            }
            if (totalRecord > 0)
            {
                listExamResult = iquery.Skip((currentPage - 1) * ExamResultSearchConstants.PageSize).Take(ExamResultSearchConstants.PageSize).ToList();
                listResult = (from o in listExamResult
                              select new ListViewModel
                              {
                                  ExamPupilID = o.ExamPupilID,
                                  ActualMark = _globalInfo.AppliedLevel.GetValueOrDefault() == GlobalConstants.APPLIED_LEVEL_PRIMARY ? String.Format("{0:0}", o.ActualMark)
                                            : isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK ? (o.ActualMark != 0 && o.ActualMark != 10 ? String.Format("{0:0.0}", o.ActualMark) : String.Format("{0:0}", o.ActualMark)) : o.ExamJudgeMark
                                            ,
                                  ExamineeNumber = o.ExamineeNumber,
                                  ExamMark =_globalInfo.AppliedLevel.GetValueOrDefault() == GlobalConstants.APPLIED_LEVEL_PRIMARY ? String.Format("{0:0}", o.ExamMark)
                                            : isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK ? (o.ExamMark != 0 && o.ExamMark != 10 ? String.Format("{0:0.0}", o.ExamMark) : String.Format("{0:0}", o.ExamMark)) : o.ExamJudgeMark
                                            ,
                                  ExamRoomCode = o.ExamRoomCode,
                                  PupilCode = o.PupilCode,
                                  PupilName = o.PupilName,
                                  SubjectName = subjectName,
                                  ExamInputMarkID = o.ExamInputMarkID,
                              }).ToList();
                //Lay danh sach vi pham quy che thi
                if (listViolate != null && listViolate.Count > 0)
                {
                    for (int i = 0; i < listResult.Count(); i++)
                    {
                        ListViewModel model = listResult[i];
                        model.listViolate = listViolate.Where(o => o.ExamPupilID == model.ExamPupilID).ToList();
                    }
                }
            }           
            return listResult;                       
        }

        #endregion
    }
}