﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamResultSearchArea
{
    public class ExamResultSearchConstants
    {
        //Viewdata
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string CBO_EXAM_SUBJECT = "cbo_exam_subject";
        public const string CBO_EXAM_ROOM = "cbo_exam_room";
        public const string LIST_RESULT ="list_result";
        public const string TOTAL = "total";
        public const string PAGE = "page";
        public const string HIDDEN_EXAMINATIONS_ID = "hidden_examinations_id";
        public const string HIDDEN_EXAM_GROUP_ID = "hidden_exam_group_id";
        public const string HIDDEN_SUBJECT_ID = "hidden_subject_id";
        public const string HIDDEN_EXAM_ROOM_ID = "hidden_exam_room_id";
        public const string HIDDEN_EXAMINEE_NUMBER = "hidden_examinee_number";
        public const string HIDDEN_PUPIL_NAME = "hidden_pupil_name";
        public const int PageSize = 20;
    }
}