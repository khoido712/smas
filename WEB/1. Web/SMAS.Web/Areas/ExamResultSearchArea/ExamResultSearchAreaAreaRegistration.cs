﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultSearchArea
{
    public class ExamResultSearchAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamResultSearchArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamResultSearchArea_default",
                "ExamResultSearchArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
