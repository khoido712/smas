﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Business.BusinessObject;
namespace SMAS.Web.Areas.ExamResultSearchArea.Models
{
    public class ListViewModel
    {
        [ResourceDisplayName("ExamResultSearch_Label_PupilCode")]
        public string PupilCode { get; set; }
        [ResourceDisplayName("ExamResultSearch_Label_PupilName")]
        public string PupilName { get; set; }
        [ResourceDisplayName("ExamResultSearch_Label_Grid_ExamineeNumber")]
        public string ExamineeNumber { get; set; }
        [ResourceDisplayName("ExamResultSearch_Label_ExamRoomCode")]
        public string ExamRoomCode { get; set; }
        [ResourceDisplayName("ExamResultSearch_Label_SubjectName")]
        public string SubjectName { get; set; }
        [ResourceDisplayName("ExamResultSearch_Label_ExamMark")]
        public object ExamMark { get; set; }
        [ResourceDisplayName("ExamResultSearch_Label_ActualMark")]
        public object ActualMark { get; set; }
        public List<ExamPupilViolateBO> listViolate;
        [ResourceDisplayName("ExamResultSearch_Label_Note")]
        public string Note
        {
            get
            {
                string note = String.Empty;
                if (listViolate != null && listViolate.Count > 0)
                {

                    for (int i = 0; i < listViolate.Count; i++)
                    {
                        note = note + listViolate[i].Note;
                        if (i < listViolate.Count - 1)
                        {
                            note = note + ", ";
                        }
                    }
                }
                return note;
            }
        }

        public long ExamInputMarkID { get; set; }

        public long ExamPupilID { get; set; }
    }
}