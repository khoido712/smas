﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ExamResultSearchArea;
using Resources;

namespace SMAS.Web.Areas.ExamResultSearchArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamResultSearch_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamResultSearchConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public long? ExaminationsID { get; set; }

        [ResourceDisplayName("ExamResultSearch_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamResultSearchConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public long? ExamGroupID { get; set; }

        [ResourceDisplayName("ExamResultSearch_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamResultSearchConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("OnChange", "onExamSubjectChange()")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? SubjectID { get; set; }

        [ResourceDisplayName("ExamResultSearch_Label_ExamRoomID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamResultSearchConstants.CBO_EXAM_ROOM)]
        [AdditionalMetadata("OnChange", "onExamRoomChange()")]
        public long? ExamRoomID { get; set; }

        [ResourceDisplayName("ExamResultSearch_Label_ExamineeNumber")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ExamineeNumber { get; set; }

        [ResourceDisplayName("ExamResultSearch_Label_PupilName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilName { get; set; }
    }
}