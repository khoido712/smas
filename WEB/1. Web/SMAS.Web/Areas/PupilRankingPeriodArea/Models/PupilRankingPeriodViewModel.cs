using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.PupilRankingPeriodArea.Models
{
    public class PupilRankingPeriodViewModel
    {
        [ResourceDisplayName("ExemptedSubject_Label_EducationLevelID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("ListRadio")]
        [AdditionalMetadata("ViewDataKey", PupilRankingPeriodContants.LIST_SEMESTER)]
        public int Semester { get; set; }

        [ResourceDisplayName("EducationLevel_Label_EducationLevelID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [AdditionalMetadata("ViewDataKey", PupilRankingPeriodContants.LIST_EDUCATION_LEVEL)]
        [UIHint("ListRadio")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("PeriodDeclaration_Label_PeriodDeclarationID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PeriodDeclarationID { get; set; }

        [ResourceDisplayName("ClassProfile_Label_ClassProfileID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassProfileID { get; set; }

        public int RankingType { get; set; }
    }
}
