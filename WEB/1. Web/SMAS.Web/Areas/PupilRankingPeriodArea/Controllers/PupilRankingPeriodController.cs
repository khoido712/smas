﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.PupilRankingPeriodArea.Models;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Controllers;
using System.IO;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.PupilRankingPeriodArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilRankingPeriodController : BaseController
    {
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBu;
        private readonly IClassProfileBusiness ClassProfileBu;
        private readonly IPupilProfileBusiness PupilProfileBu;
        private readonly IPupilRankingBusiness PupilRankingBu;
        private readonly IConductLevelBusiness ConductLevelBu;
        private readonly IConductConfigByCapacityBusiness ConductConfigByCapacityBu;
        private readonly IConductConfigByViolationBusiness ConductConfigByViolationBu;
        private readonly IAcademicYearBusiness AcademicYearBu;
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IConfigConductRankingBusiness ConfigConductRankingBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilRankingHistoryBusiness PupilRankingHistoryBusiness;
        private readonly ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        public PupilRankingPeriodController(IPeriodDeclarationBusiness periodDeclarationBu,
                                            IClassProfileBusiness classProfileBu,
                                            IPupilProfileBusiness pupilProfileBu,
                                            IPupilRankingBusiness pupilRankingBu,
                                            IConductLevelBusiness conductLevelBusiness,
                                            IConfigConductRankingBusiness configConductRankingBusiness,
                                            IConductLevelBusiness conductLevelBu,
                                            IConductConfigByCapacityBusiness conductConfigByCapacityBu,
                                            IConductConfigByViolationBusiness conductConfigByViolationBu,
                                            IAcademicYearBusiness academicYearBu, ISchoolProfileBusiness SchoolProfileBusiness,
                                            IPupilRankingHistoryBusiness _PupilRankingHistoryBusiness,
                                            ISummedUpRecordClassBusiness _SummedUpRecordClassBusiness)
        {
            this.PeriodDeclarationBu = periodDeclarationBu;
            this.ClassProfileBu = classProfileBu;
            this.PupilProfileBu = pupilProfileBu;
            this.PupilRankingBu = pupilRankingBu;
            this.ConductLevelBu = conductLevelBu;
            this.ConductLevelBusiness = conductLevelBusiness;
            this.ConductConfigByCapacityBu = conductConfigByCapacityBu;
            this.ConductConfigByViolationBu = conductConfigByViolationBu;
            this.AcademicYearBu = academicYearBu;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ConfigConductRankingBusiness = configConductRankingBusiness;
            this.PupilRankingHistoryBusiness = _PupilRankingHistoryBusiness;
            this.SummedUpRecordClassBusiness = _SummedUpRecordClassBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eid)
        {
            if (eid <= 0)
                return Json(new List<ComboObject>());

            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", glo.AcademicYearID);
            dicClass.Add("EducationLevelID", eid);

            if (!glo.IsAdminSchoolRole)
            {
                dicClass.Add("UserAccountID", glo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEADTEACHER);
            }

            var lstClass = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, dicClass).ToList()
                                                .Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName))
                                                .ToList();
            return Json(lstClass);
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadPeriod(int sid)
        {
            if (sid <= 0)
                return Json(new List<ComboObject>());

            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> dicPeriod = new Dictionary<string, object>();
            dicPeriod.Add("AcademicYearID", glo.AcademicYearID.Value);
            dicPeriod.Add("Semester", sid);
            var lstPeriod = PeriodDeclarationBu.SearchBySchool(glo.SchoolID.Value, dicPeriod);
            int PeriodDeclarationIDSelected = 0;
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            DateTime nowDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            PeriodDeclarationIDSelected = lstPeriod.FirstOrDefault().PeriodDeclarationID;
            List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
            foreach (var item in lstPeriod)
            {
                PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                periodDeclaration = item;
                if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                {
                    periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + Res.Get("Common_Label_From") + " " + periodDeclaration.FromDate.Value.ToShortDateString() + " " + Res.Get("Common_Label_To") + " " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                    startDate = new DateTime(periodDeclaration.FromDate.Value.Year, periodDeclaration.FromDate.Value.Month, periodDeclaration.FromDate.Value.Day, 0, 0, 0);
                    endDate = new DateTime(periodDeclaration.EndDate.Value.Year, periodDeclaration.EndDate.Value.Month, periodDeclaration.EndDate.Value.Day, 0, 0, 0);
                    if (startDate <= nowDate && nowDate <= endDate)
                    {
                        PeriodDeclarationIDSelected = periodDeclaration.PeriodDeclarationID;
                    }
                }
                listPeriodDeclaration.Add(periodDeclaration);
            }
            var listPeriod = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", PeriodDeclarationIDSelected);
            return Json(listPeriod);
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(int sid, int pid, int cid, int? ranking)
        {
            GlobalInfo glo = new GlobalInfo();
            PeriodDeclaration period = PeriodDeclarationBu.Find(pid);
            // DungVA 05/05
            string className = ClassProfileBu.Find(cid).DisplayName;
            ViewData[PupilRankingPeriodContants.TITLE_PAGE] = Res.Get("PupilRankingPeriod_List") + " đợt " + period.Resolution + " (" + period.FromDate.Value.ToShortDateString() + " - " + period.EndDate.Value.ToShortDateString() + ")" + " lớp " + className;
            // end DungVA

            ViewData[PupilRankingPeriodContants.PERIOD] = period;

            ViewData[PupilRankingPeriodContants.SEMESTER_END_DATE] = period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? period.AcademicYear.FirstSemesterEndDate : period.AcademicYear.SecondSemesterEndDate;
            int rankingType = GetRankingType();

            //check enable export, ranking, save button
            bool timePeriod = period != null && (period.FromDate < DateTime.Now && DateTime.Now < period.EndDate.Value.AddDays(1)) && (!period.IsLock.HasValue || !period.IsLock.Value);

            bool enabled = UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, cid) && (glo.IsAdminSchoolRole || timePeriod);
            if (timePeriod && UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, cid))
            {
                enabled = true;
            }
            else if (glo.IsAdminSchoolRole && glo.IsCurrentYear)
            {
                enabled = true;
            }
            else
            {
                enabled = false;
            }

            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(glo.SchoolID.Value);
            bool checkGDTX = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
            ViewData[PupilRankingPeriodContants.IS_GDTX] = checkGDTX ? "true" : "false";

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",cid},
                {"PeriodID",pid}
            };
            ViewData[PupilRankingPeriodContants.IS_CONFIG_RANKING] = ConfigConductRankingBusiness.GetAllConfigConductRanking(dic).Count() > 0 ? true : false;

            if (rankingType < 0)
                return PartialView("_gridCapacity", null);

            //Get data for combobox in grid
            ViewData[PupilRankingPeriodContants.LIST_CONDUCT_LEVEL] = this.ConductLevelBu.Search(new Dictionary<string, object>() { { "IsActive", true }, { "AppliedLevel", glo.AppliedLevel.Value } }).ToList();
            ViewData[PupilRankingPeriodContants.CLASS_ID] = cid;
            ViewData[PupilRankingPeriodContants.IS_RETRANING] = sid == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
            int appliedLevel = glo.AppliedLevel.Value;
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle))
            {
                isLockInput = objLockInput.LockTitle.Contains("HK" + sid);
            }
            ViewData[PupilRankingPeriodContants.IS_LOCK_INPUT] = isLockInput;
            if (rankingType == 0)
            {
                var model = this.PupilRankingBu.GetPupilToRankConductByCapacity(glo.SchoolID.Value, glo.AcademicYearID.Value, cid, sid, pid);
                ViewData[PupilRankingPeriodContants.ENABLE_EDITING] = enabled && model.Any(u => u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING && !PupilProfileBu.IsNotConductRankingPupil(checkGDTX, _globalInfo.AppliedLevel.Value, u.LearningType, u.BirthDate));
                if (ranking.HasValue && ranking.Value > 0 && enabled)
                    foreach (var item in model)
                        if (item.CapacityLevelID.HasValue && item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            item.ConductLevelID = GetPupilRankingByCapacityLevel(glo.AppliedLevel.Value, item.CapacityLevelID.Value);
                return PartialView("_gridCapacity", model);
            }

            if (rankingType == 1)
            {
                var model = this.PupilRankingBu.GetPupilToRankConductByFault(glo.SchoolID.Value, glo.AcademicYearID.Value, cid, sid, pid);
                ViewData[PupilRankingPeriodContants.ENABLE_EDITING] = enabled && model.Any(u => u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING && !PupilProfileBu.IsNotConductRankingPupil(checkGDTX, _globalInfo.AppliedLevel.Value, u.LearningType, u.BirthDate));
                var lstConductConfig = this.ConductConfigByViolationBu.Search(new Dictionary<string, object>() { { "SchoolID", glo.SchoolID.Value }, { "AppliedLevel", glo.AppliedLevel.Value } }).ToList();
                if (ranking.HasValue && ranking.Value > 0 && enabled)
                    foreach (var item in model)
                        if (item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING && item.TotalMark.HasValue)
                            item.ConductLevelID = GetPupilRankingByViolation(lstConductConfig, item.TotalMark.Value);
                return PartialView("_gridFault", model);
            }

            return null;
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult Create(PupilRankingPeriodViewModel model, FormCollection form, int? ClassID, int? Semester, int? EducationlevelID, int? PeriodID)
        {
            //DungVA - Truyền tham số từ chức năng gộp mới
            ClassProfile classProfile= ClassProfileBu.Find(ClassID);
            if (ClassID != null && ClassID != 0)
            {
                model.ClassProfileID = ClassID.Value;
                model.EducationLevelID = classProfile.EducationLevelID;
            }
            if (Semester != null && Semester != 0)
            {
                model.Semester = Semester.Value;
            }

            if (PeriodID != null && PeriodID != 0)
            {
                model.PeriodDeclarationID = PeriodID.Value;
            }
            if (!ModelState.IsValid)
            {
                logger.Info("Dang co lop thuc hien tong ket/xep loai ClassID="+ClassID);
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter"), JsonMessage.ERROR));
            }
            GlobalInfo glo = new GlobalInfo();
            PeriodDeclaration period = PeriodDeclarationBu.Find(model.PeriodDeclarationID);
            #region Check quyen xep hang
            if (UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, model.ClassProfileID))
            {
                if (!glo.IsAdminSchoolRole)
                {
                    bool timePeriod = period != null && (period.FromDate < DateTime.Now && DateTime.Now < period.EndDate.Value.AddDays(1)) && (!period.IsLock.HasValue || !period.IsLock.Value);
                    if (!timePeriod)
                        return Json(new JsonMessage(Res.Get("PupilRankingPeriod_Label_PeriodLockOrEnd"), JsonMessage.ERROR));
                }
            }
            else
                return Json(new JsonMessage(Res.Get("Common_Label_HasHeadTeacherPermissionError"), JsonMessage.ERROR));
            #endregion
            if (!glo.HasHeadTeacherPermission(model.ClassProfileID))
            {
                return Json(new JsonMessage(Res.Get("Common_Label_HasHeadTeacherPermissionError"), JsonMessage.ERROR));
            }
            int rankingType = GetRankingType();
            if (rankingType == -1)
            {
                return Json(new JsonMessage(Res.Get("ConductConfig_Label_NotConfigYet"), JsonMessage.ERROR));
            }

            AcademicYear acaYear = this.AcademicYearBu.Find(glo.AcademicYearID.Value);


            List<PupilRankingBO> lstPupils = new List<PupilRankingBO>();
            List<PupilRanking> lstPupilRanking = new List<PupilRanking>();
            bool isCheckAll = "on".Equals(form["chkAll"]);
            bool checkGDTX = acaYear.SchoolProfile.TrainingTypeID.HasValue && acaYear.SchoolProfile.TrainingType.Resolution == "GDTX";
            lstPupils = rankingType == 0 ? this.PupilRankingBu.GetPupilToRankConductByCapacity(glo.SchoolID.Value, glo.AcademicYearID.Value, model.ClassProfileID, model.Semester, model.PeriodDeclarationID)
                                                : this.PupilRankingBu.GetPupilToRankConductByFault(glo.SchoolID.Value, glo.AcademicYearID.Value, model.ClassProfileID, model.Semester, model.PeriodDeclarationID);
            lstPupils = lstPupils.Where(u => u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
            int pupilID = 0;


            SummedUpRecordClass objSummedUpRecordClass = new SummedUpRecordClass
            {
                AcademicYearID = acaYear.AcademicYearID,
                ClassID = ClassID.Value,
                PeriodID = PeriodID,
                Semester = Semester.Value,
                CreatedDate = DateTime.Now,
                EducationLevelID = classProfile.EducationLevelID,
                SchoolID = acaYear.SchoolID,
                NumberOfPupil = lstPupils.Count,
                Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1,
                Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING,
                SynchronizeID = 0,
            };
            //12.02.2018 Kiem tra lop co dang thuc hien xep loai HK, tong ket, xep loai khong

            if (SummedUpRecordClassBusiness.CheckExistsSummedExcuting(objSummedUpRecordClass))
            {
                return Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_Summeding"), JsonMessage.ERROR));
            }
            //Danh dau lop dang thuc hien tinh tong ket, xep loai
            SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);


            foreach (var pupil in lstPupils)
            {
                PupilRanking pr = new PupilRanking();
                pr.ConductLevelID = PupilProfileBu.IsNotConductRankingPupil(checkGDTX, glo.AppliedLevel.Value, pupil.LearningType, pupil.BirthDate) ? null : GetConductorIDInForm(pupil.PupilID.Value, form);
                pr.SchoolID = glo.SchoolID.Value;
                pr.AcademicYearID = glo.AcademicYearID.Value;
                pr.ClassID = model.ClassProfileID;
                pupilID = pupil.PupilID.Value;
                pr.PupilID = pupilID;
                pr.TotalAbsentDaysWithPermission = pupil.TotalAbsentDaysWithPermission;
                pr.TotalAbsentDaysWithoutPermission = pupil.TotalAbsentDaysWithoutPermission;
                pr.CreatedAcademicYear = acaYear.Year;
                pr.Last2digitNumberSchool = glo.SchoolID.Value % 100;
                pr.Semester = model.Semester;
                pr.PeriodID = model.PeriodDeclarationID;
                pr.EducationLevelID = model.EducationLevelID;
                pr.AverageMark = pupil.AverageMark;
                pr.CapacityLevelID = pupil.CapacityLevelID;
                pr.MSourcedb = "0";//truong phan biet voi tool TKD tu dong                            
                if (isCheckAll)
                {
                    pr.PupilRankingComment = form["hdfComment"];
                }
                else
                {
                    pr.PupilRankingComment = form["PupilCommentPeriod_" + pupilID];
                }
                lstPupilRanking.Add(pr);
            }
            try
            {
                if (UtilsBusiness.IsMoveHistory(acaYear)) //Du lieu lich su
                {
                    this.PupilRankingHistoryBusiness.RankingPupilConductHistory(glo.UserAccountID, lstPupilRanking, (int)model.Semester, false);
                }
                else
                {
                    this.PupilRankingBu.RankingPupilConduct(glo.UserAccountID, lstPupilRanking, (int)model.Semester, false);
                }
            }
            finally
            {
                objSummedUpRecordClass.ModifiedDate = DateTime.Now;
                objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                objSummedUpRecordClass.NumberOfPupil = lstPupilRanking.Count;
                SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);
            }
           
            //this.PupilRankingBu.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }


        public FileStreamResult ExportFault(PupilRankingPeriodViewModel model)
        {
            if (model.ClassProfileID != 0)
            {
                model.EducationLevelID = ClassProfileBu.Find(model.ClassProfileID).EducationLevelID;
            }
            GlobalInfo glo = new GlobalInfo();
            if (ModelState.IsValid && glo.HasHeadTeacherPermission(model.ClassProfileID))
            {
                int rankingType = GetRankingType();
                if (rankingType != -1)
                {
                    string fileName = string.Empty;
                    Stream stream = this.PupilRankingBu.CreatePupilToRankConductByFaultReport(glo.AppliedLevel.Value, glo.SchoolID.Value, glo.AcademicYearID.Value, model.ClassProfileID, model.Semester, model.PeriodDeclarationID, out fileName);
                    FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                    result.FileDownloadName = fileName;
                    return result;
                }
            }
            return null;
        }


        public FileStreamResult ExportCapacity(PupilRankingPeriodViewModel model)
        {
            if (model.ClassProfileID != 0)
            {
                model.EducationLevelID = ClassProfileBu.Find(model.ClassProfileID).EducationLevelID;
            }
            GlobalInfo glo = new GlobalInfo();
            if (ModelState.IsValid && glo.HasHeadTeacherPermission(model.ClassProfileID))
            {
                int rankingType = GetRankingType();
                if (rankingType != -1)
                {
                    string fileName = string.Empty;
                    Stream stream = this.PupilRankingBu.CreatePupilToRankConductByCapacityReport(glo.AppliedLevel.Value, glo.SchoolID.Value, glo.AcademicYearID.Value, model.ClassProfileID, model.Semester, model.PeriodDeclarationID, out fileName);
                    FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                    result.FileDownloadName = fileName;
                    return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Lấy kiểu xét hạnh kiểm
        /// </summary>
        /// <returns>0: Theo học lực, 1 : theo lỗi vi phạm. -1 : Chưa cấu hình.</returns>
        private int GetRankingType()
        {
            GlobalInfo glo = GlobalInfo.getInstance();
            AcademicYear academicYear = this.AcademicYearBu.Find(glo.AcademicYearID.Value);
            if (academicYear.ConductEstimationType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY)
            {
                return 0;
            }
            else if (academicYear.ConductEstimationType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION)
            {
                return 1;
            }
            else
            {
                return -1;
            }
            //IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic.Add("SchoolID", glo.SchoolID.Value);
            //dic.Add("AppliedLevel", glo.AppliedLevel.Value);
            //dic.Add("IsActive", true);

            //var configByCapacity = this.ConductConfigByCapacityBu.Search(dic).FirstOrDefault();
            //if (configByCapacity != null) return 0;

            //var configByViolation = this.ConductConfigByViolationBu.Search(dic).FirstOrDefault();
            //if (configByViolation != null) return 1;

            //return -1;
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            var lstSemester = CommonList.Semester();

            ViewData[PupilRankingPeriodContants.LIST_SEMESTER] = lstSemester
                                                                    .Select(u => new ViettelCheckboxList()
                                                                    {
                                                                        Value = u.key,
                                                                        Label = u.value,
                                                                        cchecked = glo.Semester.Value.ToString() == u.key,
                                                                        disabled = false
                                                                    }).ToList();
            var cblEducation = glo.EducationLevels
                                .Select(u => new ViettelCheckboxList()
                                {
                                    Value = u.EducationLevelID,
                                    Label = u.Resolution,
                                    cchecked = false,
                                    disabled = false
                                }).ToList();
            cblEducation.First().cchecked = true;
            ViewData[PupilRankingPeriodContants.LIST_EDUCATION_LEVEL] = cblEducation;

            ViewData[PupilRankingPeriodContants.RANKING_TYPE] = GetRankingType();
        }

        private List<ConductConfigByCapacity> mapCapacityToConduct = null;
        /// <summary>
        /// Lấy hạnh kiểm của học sinh theo học lực
        /// </summary>
        /// <param name="AppliedLevel"></param>
        /// <param name="CapacityLevelID"></param>
        /// <returns></returns>
        private int? GetPupilRankingByCapacityLevel(int AppliedLevel, int CapacityLevelID)
        {
            int SchoolID = GlobalInfo.getInstance().SchoolID.Value;
            // Tim du lieu cau hinh xep loai hanh kiem theo hoc luc cua truong
            if (this.mapCapacityToConduct == null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AppliedLevel"] = AppliedLevel;
                this.mapCapacityToConduct = this.ConductConfigByCapacityBu.Search(dic).ToList();
            }
            var Conduct = mapCapacityToConduct.FirstOrDefault(p => p.SchoolID == SchoolID && p.AppliedLevel == AppliedLevel && p.CapacityLevelID == CapacityLevelID);
            if (Conduct != null)
            {
                return Conduct.ConductLevelID;
            }
            // Neu khong tim thay cau hinh thi thuc hien xep loai theo mac dinh
            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT || CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD)
                    return SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL)
                    return SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK)
                    return SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                    return SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY;
            }

            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT || CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD)
                    return SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL)
                    return SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK)
                    return SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY;

                if (CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                    return SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY;
            }

            return null;
        }

        /// <summary>
        /// Lấy hạnh kiểm của học sinh theo lỗi vi phạm
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private int? GetPupilRankingByViolation(List<ConductConfigByViolation> lstConductConfig, decimal total)
        {
            foreach (var item in lstConductConfig)
                if (total >= item.MinValue.Value)
                    return item.ConductLevelID;

            ConductConfigByViolation lowestLevel = lstConductConfig.Where(u => u.MinValue == lstConductConfig.Min(v => v.MinValue)).SingleOrDefault();

            if (lowestLevel != null && (total < 0 || (lowestLevel.MinValue.HasValue && total <= lowestLevel.MinValue.Value)))
                return lowestLevel.ConductLevelID;

            return null;
        }

        private int? GetConductorIDInForm(int pupilID, FormCollection form)
        {
            string conductLevel = form.Get("ConductLevel_" + pupilID);
            if (string.IsNullOrEmpty(conductLevel))
            {
                return null;
            }
            else
            {
                int result = 0;
                if (!string.IsNullOrEmpty(conductLevel) && int.TryParse(conductLevel, out result))
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
