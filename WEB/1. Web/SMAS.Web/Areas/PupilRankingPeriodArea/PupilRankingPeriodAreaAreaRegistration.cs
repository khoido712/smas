﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilRankingPeriodArea
{
    public class PupilRankingPeriodAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilRankingPeriodArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilRankingPeriodArea_default",
                "PupilRankingPeriodArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
