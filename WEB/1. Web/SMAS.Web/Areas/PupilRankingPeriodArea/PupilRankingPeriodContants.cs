﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilRankingPeriodArea
{
    public class PupilRankingPeriodContants
    {
        public const string LIST_EDUCATION_LEVEL = "List_Education_Level";

        public const string LIST_SEMESTER = "List_Semester";

        public const string LIST_CONDUCT_LEVEL = "List_Conduct_Level";

        public const string RANKING_TYPE = "Ranking_Type";

        public const string CLASS_ID = "Class_ID";

        public const string ENABLE_EDITING = "Enable_Editing";

        public const string SEMESTER_END_DATE = "semester_end_date";

        public const string TITLE_PAGE = "TITLE_PAGE";

        public const string IS_GDTX = "IS_GDTX";

        public const string IS_RETRANING = "is_retraning";

        public const string IS_CONFIG_RANKING = "IsConfigRanking";

        public const string PERIOD = "Period";

        public const string IS_LOCK_INPUT = "IsLockInput";
    }
}