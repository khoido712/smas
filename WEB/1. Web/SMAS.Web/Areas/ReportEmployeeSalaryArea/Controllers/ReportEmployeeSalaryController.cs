﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ReportEmployeeSalaryArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
using SMAS.Web.Areas.ReportEmployeeByGraduationLevelArea;

namespace SMAS.Web.Areas.ReportEmployeeSalaryArea.Controllers
{
    public class ReportEmployeeSalaryController : BaseController
    {
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportEmployeeSalaryBusiness ReportEmployeeSalaryBusiness;
        public ReportEmployeeSalaryController(IReportEmployeeSalaryBusiness ReportEmployeeSalaryBusiness, IProcessedReportBusiness ProcessedReportBusiness, ISchoolFacultyBusiness SchoolFacultyBusiness)
        {
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ReportEmployeeSalaryBusiness = ReportEmployeeSalaryBusiness;
        }

        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }

        public ActionResult _Index()
        {
            this.SetViewData();
            return PartialView();
        }

        public PartialViewResult loadFormSearch(int id)
        {
            if (id == 1)
            {
                SetViewData();
                return PartialView("_SearchEmployeeSalary");
            }
            else
            {
                return PartialView("_SearchEmployeeIncreaseSalary");
            }
        }

        #region SetViewData
        public void SetViewData()
        {
            ViewData[ReportEmployeeSalaryConstants.CBO_FACULTY] = new List<SchoolFaculty>();
            ViewData[ReportEmployeeSalaryConstants.LIST_EMPLOYEESALARY] = new List<ReportEmployeeSalaryViewModel>();



            //-	cboFaculty: : lấy danh sách từ SchoolFacultyBusiness.SearchBySchool(Dictionary) 
            //    với Dictionary[“SchoolID”] = UserInfo.SchoolID và Dictionary[“IsActive”] = 1. Thêm giá trị mặc định [Tất cả]
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["IsActive"] = true;

            IQueryable<SchoolFaculty> lsFaculty = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary);
            if (lsFaculty.Count() > 0)
            {
                List<SchoolFaculty> lstFaculty = lsFaculty.ToList();
                lstFaculty = lstFaculty.OrderBy(o => o.FacultyName).ToList();
                ViewData[ReportEmployeeSalaryConstants.CBO_FACULTY] = lstFaculty;
            }
        }
        #endregion
        #region Search
        private IEnumerable<ReportEmployeeSalaryViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ReportEmployeeSalaryBO> query = ReportEmployeeSalaryBusiness.GetListEmployeeSalaryBySchool(SearchInfo);
            if (query != null && query.Count() > 0)
            {
                IQueryable<ReportEmployeeSalaryViewModel> lst = query.Select(o => new ReportEmployeeSalaryViewModel
                {
                    AppliedDate = o.AppliedDate,
                    BirthDate = o.BirthDate,
                    Coefficent = o.Coefficent,
                    FacultyName = o.FacultyName,
                    FullName = o.FullName,
                    Name = o.Name,
                    Genre = o.Genre == true ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female"),
                    Resolution = o.EmployeeScale,
                    SalaryAmount = o.SalaryAmount,
                    SalaryResolution = o.SalaryResolution,
                    SubLevel = o.SubLevel != null ? o.SubLevel.ToString() : ""
                });

                return lst.OrderBy(o=>o.Name).ThenBy(o => o.FullName).ToList();
            }
            else return new List<ReportEmployeeSalaryViewModel>();
        }
        public PartialViewResult Search(SearchViewModel svm)
        {
            ViewData[ReportEmployeeSalaryConstants.ReportDate] = svm.ReportDate;
            ViewData[ReportEmployeeSalaryConstants.FacultyID] = svm.SchoolFaculty != null ? svm.SchoolFaculty : 0;
            ViewData[ReportEmployeeSalaryConstants.FromDate] = svm.FromDate;
            ViewData[ReportEmployeeSalaryConstants.ToDate] = svm.ToDate;

            if (svm.rdoEmployeeSalary == 1)
            {
                if (svm.ReportDate == null)
                {
                    ViewData["Error"] = Res.Get("ReportEmployeeSalary_Validate_ReportDate");
                    return PartialView("_ListEmployeeSalary");
                }
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["ReportDate"] = svm.ReportDate;
                Dictionary["FacultyID"] = svm.SchoolFaculty;
                Dictionary["SchoolID"] = _globalInfo.SchoolID;
                Dictionary["AppliedLevel"] = _globalInfo.AppliedLevel;
                IEnumerable<ReportEmployeeSalaryViewModel> lsEmp = this._Search(Dictionary);
                if (lsEmp.Count() > 0)
                {
                    ViewData[ReportEmployeeSalaryConstants.LIST_EMPLOYEESALARY] = lsEmp.ToList();
                }
                else
                {
                    ViewData[ReportEmployeeSalaryConstants.LIST_EMPLOYEESALARY] = new List<ReportEmployeeSalaryViewModel>();
                }
                
                return PartialView("_ListEmployeeSalary");
            }
            else
            {
                if (svm.FromDate == null)
                {
                    ViewData["Error"] = Res.Get("ReportEmployeeSalary_Validate_FromDate");
                    return PartialView("_ListEmployeeIncreaseSalary");
                }
                if (svm.ToDate == null)
                {
                    ViewData["Error"] = Res.Get("ReportEmployeeSalary_Validate_ToDate");
                    return PartialView("_ListEmployeeIncreaseSalary");
                }
                if (svm.FromDate > svm.ToDate)
                {
                    ViewData["Error"] = Res.Get("ReportEmployeeSalary_Validate_FromDate_ToDate");
                    return PartialView("_ListEmployeeIncreaseSalary");
                }
                DateTime? fromDate = svm.FromDate;
                DateTime? toDate = svm.ToDate;
                List<ReportEmployeeSalaryBO> lst = ReportEmployeeSalaryBusiness.GetListEmployeeIncreaseSalaryBySchool(fromDate.Value, toDate.Value, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value);
                List<ReportEmployeeSalaryViewModel> lstViewModel = new List<ReportEmployeeSalaryViewModel>();
                if (lst!= null && lst.Count > 0)
                {
                    foreach(ReportEmployeeSalaryBO empSalary in lst)
                    {
                        ReportEmployeeSalaryViewModel rp = new ReportEmployeeSalaryViewModel();
                        rp.AppliedDate = empSalary.AppliedDate;
                        rp.BirthDate = empSalary.BirthDate;
                        rp.Coefficent = empSalary.Coefficent;
                        rp.FacultyName = empSalary.FacultyName;
                        rp.FullName = empSalary.FullName;
                        rp.Genre = empSalary.Genre == true ? "Nam" : "Nữ";
                        rp.Resolution = empSalary.EmployeeScale;
                        rp.SalaryAmount = empSalary.SalaryAmount;
                        rp.SalaryResolution = empSalary.SalaryResolution;
                        rp.SubLevel = empSalary.SubLevel != null ? empSalary.SubLevel.ToString() : "";
                        rp.NextAppliedDate = empSalary.AppliedDate != null ? empSalary.AppliedDate.Value.AddYears(empSalary.DurationInYear.Value) : DateTime.Now;
                        lstViewModel.Add(rp);
                    }
                    ViewData[ReportEmployeeSalaryConstants.LIST_EMPLOYEEINCREASESALARY] = lstViewModel.ToList();
                }
                else
                {
                    ViewData[ReportEmployeeSalaryConstants.LIST_EMPLOYEEINCREASESALARY] = lstViewModel;
                }

                return PartialView("_ListEmployeeIncreaseSalary");
            }
        }
        #endregion

        [HttpPost]
        public FileResult ExportExcel(FormCollection col)
        {
            int reportType = Convert.ToInt32(col["rdoEmployeeSalary"]);
            if (reportType == 1)
            {
                DateTime reportDate = DateTime.Now;
                if (col["ReportDate"] != null)
                {
                    reportDate = Convert.ToDateTime(col["ReportDate"]);
                }
                int FacultyID = 0;
                if (col["FacultyID"] != null)
                {
                    FacultyID = Convert.ToInt32(col["FacultyID"]);
                }
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["ReportDate"] = reportDate;
                Dictionary["FacultyID"] = FacultyID;
                Dictionary["SchoolID"] = _globalInfo.SchoolID;
                Dictionary["AppliedLevel"] = _globalInfo.AppliedLevel;

                Stream excel = ReportEmployeeSalaryBusiness.CreateReportEmployeeSalary(Dictionary);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                //reportName GV_[SchoolLevel]_BangLuongCanBo
                string ReportName = "GV_[SchoolLevel]_BangLuongCanBo_[FacultyName].xls";
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(_globalInfo.AppliedLevel.Value);
                string facultyName = "";
                if (FacultyID != 0)
                {
                    SchoolFaculty sf = SchoolFacultyBusiness.Find(FacultyID);
                    if (sf != null)
                    {
                        facultyName = SchoolFacultyBusiness.Find(FacultyID).FacultyName;
                    }
                }
                if (facultyName != "")
                {
                    ReportName = ReportName.Replace("[FacultyName]", facultyName);
                }
                else
                {
                    ReportName = ReportName.Replace("_[FacultyName]", "");
                }
                ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
                result.FileDownloadName = ReportName;
                return result;
            }
            else
            {
                DateTime fromDate = DateTime.Now;
                if (col["FromDate"] != null)
                {
                    fromDate = Convert.ToDateTime(col["FromDate"]);
                }
                DateTime toDate = DateTime.Now;
                if (col["ToDate"] != null)
                {
                    toDate = Convert.ToDateTime(col["ToDate"]);
                }
                Stream excel = ReportEmployeeSalaryBusiness.CreateReportEmployeeIncreaseSalary(fromDate, toDate, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                //reportName GV_[SchoolLevel]_DSCanBoDenKyTangLuong
                string ReportName = "GV_[SchoolLevel]_DSCanBoDenKyTangLuong.xls";
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(_globalInfo.AppliedLevel.Value);
                ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
                result.FileDownloadName = ReportName;
                return result;
            }
        }
    }
}
