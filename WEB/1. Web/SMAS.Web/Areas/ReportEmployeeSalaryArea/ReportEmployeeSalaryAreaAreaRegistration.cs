﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportEmployeeSalaryArea
{
    public class ReportEmployeeSalaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportEmployeeSalaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportEmployeeSalaryArea_default",
                "ReportEmployeeSalaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
