﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportEmployeeSalaryArea
{
    public class ReportEmployeeSalaryConstants
    {
        public const string CBO_REPORTDATE = "CboReportDate";
        public const string CBO_FACULTY = "CboFaculty";
        public const string CBO_MATH = "CboMath";
        public const string LIST_EMPLOYEESALARY = "List_EmployeeSalary";
        public const string LIST_EMPLOYEEINCREASESALARY = "List_EmployeeIncreaseSalary";
        public const string ReportDate = "ReportDate";
        public const string FromDate = "FromDate";
        public const string ToDate = "ToDate";
        public const string FacultyID = "FacultyID";
        public const string Error = "Error";
    }
}