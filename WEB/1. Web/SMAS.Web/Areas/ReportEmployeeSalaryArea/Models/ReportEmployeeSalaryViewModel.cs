using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;

namespace SMAS.Web.Areas.ReportEmployeeSalaryArea.Models
{
    public class ReportEmployeeSalaryViewModel
    {
        

        #region Grid
        public int EmployeeID { get; set; }
        [ResourceDisplayName("Employee_Label_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("Employee_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }
        [ResourceDisplayName("Employee_Label_Genre")]
        public string Genre { get; set; }
        [ResourceDisplayName("SchoolFaculty_Label_FacultyName")]
        public string FacultyName { get; set; }
        [ResourceDisplayName("EmployeeScale_Label_Resolution")]
        public string Resolution { get; set; }
        [ResourceDisplayName("SalaryLevel_Label_SubLevel")]
        public string SubLevel { get; set; }
        [ResourceDisplayName("SalaryLevel_Label_Coefficent")]
        public decimal? Coefficent { get; set; }
        [ResourceDisplayName("EmployeeSalary_Label_SalaryAmount")]
        public decimal? SalaryAmount { get; set; }
        [ResourceDisplayName("EmployeeSalary_Label_AppliedDate")]
        public DateTime? AppliedDate { get; set; }
        [ResourceDisplayName("EmployeeSalary_Label_SalaryResolution")]
        public string SalaryResolution { get; set;}
        [ResourceDisplayName("EmployeeSalary_Label_NextAppliedDate")]
        public DateTime? NextAppliedDate { get; set; }
        [ScaffoldColumn(false)]
        public string Name { get; set; }        
        #endregion
    }
}
