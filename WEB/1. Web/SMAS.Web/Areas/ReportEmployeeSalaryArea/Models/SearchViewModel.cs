﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportEmployeeSalaryArea.Models
{
    public class SearchViewModel
    {
        public DateTime? ReportDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? SchoolFaculty { get; set; }
        public int rdoEmployeeSalary { get; set; }
    }
}