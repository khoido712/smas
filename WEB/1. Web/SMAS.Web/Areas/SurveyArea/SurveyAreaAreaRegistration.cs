﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SurveyArea
{
    public class SurveyAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SurveyArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SurveyArea_default",
                "SurveyArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
