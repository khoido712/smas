﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SurveyArea.Models
{
    public class SurveyViewModel
    {
        [ScaffoldColumn(false)]
        public int SurveyID { get; set; }

        [ResourceDisplayName("Survey_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Survey_Validate_Required_Title")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Title { get; set; }

        [ResourceDisplayName("Survey_Label_Content")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Survey_Validate_Required_Content")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Content { get; set; }

        [ResourceDisplayName("Survey_Label_URL")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Survey_Validate_Required_URL")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string URL { get; set; }

        [ResourceDisplayName("Survey_Label_StartDate")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Survey_Validate_Required_StartDate")]
        public DateTime StartDate { get; set; }

        [ResourceDisplayName("Survey_Label_EndDate")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Survey_Validate_Required_EndDate")]
        public DateTime EndDate { get; set; }
    }
}