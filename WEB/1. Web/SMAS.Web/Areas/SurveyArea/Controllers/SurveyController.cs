﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SurveyArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SurveyArea.Controllers
{
    [SkipCheckRole]
    public class SurveyController:BaseController
    {
        #region Properties
        private readonly ISurveyBusiness SurveyBusiness;
        #endregion

        #region Constructor
        public SurveyController(ISurveyBusiness SurveyBusiness)
        {
            this.SurveyBusiness = SurveyBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamPupilArea/ExamPupil/

        public ActionResult Index()
        {
            //Search du lieu
            List<SurveyViewModel> listResult = this.SurveyBusiness.All.Select(o => new SurveyViewModel
                {
                    Content = o.Content,
                    EndDate = o.EndDate,
                    StartDate = o.StartDate,
                    SurveyID = o.SurveyID,
                    Title = o.Title,
                    URL = o.URL
                }).ToList();
            ViewData["ListSurvey"] = listResult;
            
            return View();
        }

        //
        // GET: /ExamGroup/Search
        
        public PartialViewResult Search(long? examinationsID)
        {
            //Search du lieu
            List<SurveyViewModel> listResult = this.SurveyBusiness.All.Select(o => new SurveyViewModel
            {
                Content = o.Content,
                EndDate = o.EndDate,
                StartDate = o.StartDate,
                SurveyID = o.SurveyID,
                Title = o.Title,
                URL = o.URL
            }).ToList();

            return PartialView("_List", listResult);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {

            Survey survey = new Survey();
            TryUpdateModel(survey);
            Utils.Utils.TrimObject(survey);
            this.SurveyBusiness.Insert(survey);
            this.SurveyBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(int SurveyID)
        {
            Survey oriSurvey = this.SurveyBusiness.Find(SurveyID);
           
            TryUpdateModel(oriSurvey);
            Utils.Utils.TrimObject(oriSurvey);

            this.SurveyBusiness.Update(oriSurvey);
            this.SurveyBusiness.Save();

             return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Delete(long id)
        {
            this.SurveyBusiness.Delete(id);
            this.SurveyBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public ActionResult ViewSurvey(int id)
        {
            Survey sv = this.SurveyBusiness.Find(id);
            return PartialView("_SurveyView", sv);
        }
    }
}