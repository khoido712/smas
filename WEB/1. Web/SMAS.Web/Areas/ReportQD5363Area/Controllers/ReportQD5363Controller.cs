﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.ReportMenuDirectionArea;
using SMAS.Web.Areas.ReportQD5363Area.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportQD5363Area.Controllers
{
    public class ReportQD5363Controller:BaseController
    {
        #region properties
        private readonly IEducationLevelBusiness EducationLevelBusiness ;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IProvinceSubjectBusiness ProvinceSubjectBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IProcessedCellDataBusiness ProcessedCellDataBusiness;
        #endregion

        #region Constructor
        public ReportQD5363Controller(IEducationLevelBusiness EducationLevelBusiness, 
            ISchoolProfileBusiness SchoolProfileBusiness, 
            IAcademicYearBusiness AcademicYearBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness,
            IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness,
            IProvinceSubjectBusiness ProvinceSubjectBusiness,
            IDistrictBusiness DistrictBusiness,
            IMarkStatisticBusiness MarkStatisticBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            IProcessedCellDataBusiness ProcessedCellDataBusiness)
        {
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.ProvinceSubjectBusiness = ProvinceSubjectBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.MarkStatisticBusiness = MarkStatisticBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.ProcessedCellDataBusiness = ProcessedCellDataBusiness;
        }
        #endregion

        #region Actions
       
        public ActionResult Index()
        {
            SetViewData();

            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult Synthetic(ReportViewModel model)
        {
            if (!model.Year.HasValue)
            {
                throw new BusinessException("Thầy cô chưa chọn năm báo cáo");
            }
            int StatusReport = 0;
            string reportCode = SystemParamsInFile.REPORT_QD5363_CODE;

            int? ProvinceID = _globalInfo.ProvinceID ;
            int? DistrictID = null;
            if (!_globalInfo.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                DistrictID = _globalInfo.DistrictID.Value;
            }

            ProcessedCellDataBusiness.InsertOrUpdateQD5363ForSupervisingDeptRequest(ProvinceID, DistrictID, model.AppliedLevel, model.Year.Value, model.Period, reportCode);
            return Json(new { StatusReport = StatusReport });
        }

        public JsonResult CheckSynthetic(ReportViewModel model)
        {
            if (!model.Year.HasValue)
            {
                throw new BusinessException("Thầy cô chưa chọn năm báo cáo");
            }
            int? ProvinceID = _globalInfo.ProvinceID;
            int? DistrictID = null;
            if (!_globalInfo.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                DistrictID = _globalInfo.DistrictID.Value;
            }

            int StatusReport = ProcessedCellDataBusiness.GetStatusRequestForSupervisingDept(ProvinceID, DistrictID, model.Year.Value, SystemParamsInFile.REPORT_QD5363_CODE, 1, model.AppliedLevel);
            return Json(new { StatusReport = StatusReport });
        }

        public FileResult ExportExcelQD5363(ReportViewModel model)
        {
            int ReportTime = model.Period;
            int? ProvinceID = _globalInfo.ProvinceID;
            int? DistrictID = null;
            if (!_globalInfo.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                DistrictID = _globalInfo.DistrictID.Value;
            }
            int StatusReport = ProcessedCellDataBusiness.GetStatusRequestForSupervisingDept(ProvinceID, DistrictID, model.Year.Value, SystemParamsInFile.REPORT_QD5363_CODE, 1, model.AppliedLevel);
            
            if (StatusReport == 0 || StatusReport == -1 )
            {
                throw new BusinessException("Báo cáo chưa được tổng hợp");
            }

            string TEMPLATE_NAME = "";
            string templatePath = "";
            string ExportTemplateName = "";
            String pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";

            int appliedlevel = model.AppliedLevel; // cap truong
            int Year = model.Year.Value;
            string YearDisplay = Year.ToString() + " - " + (Year + 1).ToString();
            DateTime nowDate = DateTime.Now;
            string StrDate = String.Format("……, ngày {0} tháng {1} năm {2}", nowDate.Date, nowDate.Month, nowDate.Year);

            IVTWorkbook oBook = null;
            #region Bao cao EMIS
            #region EMIS dau ki
            if (ReportTime == 1)// Nếu là chọn EMIS đầu kì
            {
                   
                if (model.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    TEMPLATE_NAME = "Bieu_C1_TH_D.xls";
                    ExportTemplateName = "Bieu_C1_TH_D.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "QD5363", TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);

                    IVTWorksheet sheet1 = oBook.GetSheet(1);
                    sheet1.SetCellValue("E3", YearDisplay);

                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmisForSupervisingDept(ProvinceID, DistrictID, Year, ReportMenuDirectionConstant.QD5363_CODE_C1);
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.QD5363_C1_SHEET1);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.QD5363_C1_SHEET2);
                    FillSheet(oBook, 3, list, ReportMenuDirectionConstant.QD5363_C1_SHEET3);
                    FillSheet(oBook, 4, list, ReportMenuDirectionConstant.QD5363_C1_SHEET4);

                }
                else if (model.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY) // truong 1 cap va la cap 2
                {
                    TEMPLATE_NAME = "Bieu_C2_THCS_D.xls";
                    ExportTemplateName = "Bieu_C2_THCS_D.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "QD5363", TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);

                    IVTWorksheet sheet1 = oBook.GetSheet(1);
                    sheet1.SetCellValue("D3", YearDisplay);

                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmisForSupervisingDept(ProvinceID, DistrictID, Year, ReportMenuDirectionConstant.QD5363_CODE_C2);
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.QD5363_C2_SHEET1);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.QD5363_C2_SHEET2);
                    FillSheet(oBook, 3, list, ReportMenuDirectionConstant.QD5363_C2_SHEET3);
                    FillSheet(oBook, 4, list, ReportMenuDirectionConstant.QD5363_C2_SHEET4);

                }
                else if (model.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)//truong 1 cap va la cap 3
                {
                    TEMPLATE_NAME = "Bieu_C3_THPT_D.xls";
                    ExportTemplateName = "Bieu_C3_THPT_D.xls";
                    templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "1.Dau Nam", "QD5363", TEMPLATE_NAME);
                    oBook = VTExport.OpenWorkbook(templatePath);

                    IVTWorksheet sheet1 = oBook.GetSheet(1);
                    sheet1.SetCellValue("E3", YearDisplay);

                    List<ProcessedCellData> list = ProcessedCellDataBusiness.GetListReportEmisForSupervisingDept(ProvinceID, DistrictID, Year, ReportMenuDirectionConstant.QD5363_CODE_C3);
                    FillSheet(oBook, 1, list, ReportMenuDirectionConstant.QD5363_C3_SHEET1);
                    FillSheet(oBook, 2, list, ReportMenuDirectionConstant.QD5363_C3_SHEET2);
                    FillSheet(oBook, 3, list, ReportMenuDirectionConstant.QD5363_C3_SHEET3);
                    FillSheet(oBook, 4, list, ReportMenuDirectionConstant.QD5363_C3_SHEET4);
                }

            }
            #endregion

            //to stream xuat ra file
            Stream excel = oBook.ToStream();
            return File(excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ExportTemplateName);
            #endregion

        }
        #endregion

        #region Private methods
        private void FillSheet(IVTWorkbook oBooks, int sheetIndex, List<ProcessedCellData> lstInfoOriginal, string SheetName)
        {
            //condition là tên từng sheet muốn fill ra
            IVTWorksheet oSheet = oBooks.GetSheet(sheetIndex);
            if (lstInfoOriginal == null || lstInfoOriginal.Count == 0) { return; }
            List<ProcessedCellData> lstInfo = lstInfoOriginal.Where(p => p.SheetName == SheetName).ToList();
            string cell = string.Empty;
            int sum;
            int val;
            List<string> filledCell = new List<string>();
            for (int i = 0; i < lstInfo.Count; i++)
            {
                cell = lstInfo[i].CellAddress;
                if (filledCell.Contains(cell))
                {
                    continue;
                }

                List<ProcessedCellData> lstSumInfo = lstInfo.Where(o => o.CellAddress == cell).ToList();
                sum = lstSumInfo.Sum(o => Int32.TryParse(o.CellValue, out val) ? val : 0);
                if (sum > 0)
                {
                    oSheet.SetCellValue(cell, sum);
                }
                filledCell.Add(cell); 
            }
        }

        private void SetViewData()
        {
            List<int> lstAca = this.AcademicYearBusiness.GetListYearForSupervisingDept_THCS(_globalInfo.SupervisingDeptID.Value);
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            for (int i = 0; i < lstAca.Count(); i++)
            {
                AcademicYear AcademicYear = new AcademicYear();
                AcademicYear.Year = lstAca[i];
                AcademicYear.DisplayTitle = lstAca[i] + "-" + (lstAca[i] + 1);
                lstAcademicYear.Add(AcademicYear);
            }
            List<AcademicYear> ListAcademicYear = lstAcademicYear.ToList();
            // Lay nam hoc hien tai
            string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
            int curSemester = 1;
            int curYear = AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(_globalInfo.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);
            ViewData[ReportQD5363Constants.CBO_ACADEMIC_YEAR] = new SelectList(ListAcademicYear, "Year", "DisplayTitle", curYear);

            List<ComboObject> lstPeriod = new List<ComboObject>() { new ComboObject { key = (1).ToString(), value = "Kỳ đầu năm" } };
            ViewData[ReportQD5363Constants.CBO_PERIOD] = new SelectList(lstPeriod, "key", "value");

            List<ComboObject> lstAppliedLevel = new List<ComboObject>() { new ComboObject { key = (2).ToString(), value = "Cấp 2" }, new ComboObject { key = (3).ToString(), value = "Cấp 3" } };
            ViewData[ReportQD5363Constants.CBO_APPLIED_LEVEL] = new SelectList(lstAppliedLevel, "key", "value");

            int? ProvinceID = _globalInfo.ProvinceID;
            int? DistrictID = null;
            if (!_globalInfo.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                DistrictID = _globalInfo.DistrictID.Value;
            }

            int StatusReport = ProcessedCellDataBusiness.GetStatusRequestForSupervisingDept(ProvinceID, DistrictID, curYear, SystemParamsInFile.REPORT_QD5363_CODE, 1, GlobalConstants.APPLIED_LEVEL_SECONDARY);
            ViewData[ReportQD5363Constants.STATUS_REQUEST] = StatusReport;
          
        }
        #endregion
    }
}