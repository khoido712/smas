﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportQD5363Area
{
    public class ReportQD5363AreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportQD5363Area";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportQD5363Area_default",
                "ReportQD5363Area/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
