﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ReportQD5363Area.Models
{
    public class ReportViewModel
    {
        [ResourceDisplayName("ReportViewModel_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportQD5363Constants.CBO_ACADEMIC_YEAR)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "CheckStatusSyntheticForCb()")]
        public int? Year { get; set; }

        [ResourceDisplayName("ReportViewModel_Label_Period")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportQD5363Constants.CBO_PERIOD)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "CheckStatusSyntheticForCb()")]
        public int Period { get; set; }

        [ResourceDisplayName("ReportViewModel_Label_AppliedLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportQD5363Constants.CBO_APPLIED_LEVEL)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "CheckStatusSyntheticForCb()")]
        public int AppliedLevel { get; set; }
    }
}