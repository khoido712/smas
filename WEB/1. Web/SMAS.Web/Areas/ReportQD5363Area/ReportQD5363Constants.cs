﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportQD5363Area
{
    public class ReportQD5363Constants
    {
        public const string CBO_ACADEMIC_YEAR = "CBO_ACADEMIC_YEAR";
        public const string CBO_PERIOD = "CBO_PERIOD";
        public const string CBO_APPLIED_LEVEL = "CBO_APPLIED_LEVEL";
        public const string STATUS_REQUEST = "STATUS_REQUEST";
    }
}