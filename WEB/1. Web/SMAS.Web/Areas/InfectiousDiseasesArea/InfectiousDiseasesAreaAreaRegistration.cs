﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.InfectiousDiseasesArea
{
    public class InfectiousDiseasesAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "InfectiousDiseasesArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "InfectiousDiseasesArea_default",
                "InfectiousDiseasesArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}