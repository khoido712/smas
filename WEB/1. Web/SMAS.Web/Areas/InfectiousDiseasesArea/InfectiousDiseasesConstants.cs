﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.InfectiousDiseasesArea
{
    public class InfectiousDiseasesConstants
    {
        public const string LIST_INFECTIOUSDISEASES = "listInfectiousDiseases";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTCLASS = "LISTCLASS";
        public const string LISTPROVINCE = "LISTPROVINCE";
        public const string LISTDISTRICT = "LISTDISTRICT";
        public const string LISTCOMMUNE = "LISTCOMMUNE";
        public const string DATEFIRSTACAD = "DATEFIRSTACAD";
        public const string DATEENDACAD = "DATEENDACAD";

        public const string AUTOCOMPLETE_PUPILNAME = "AUTOCOMPLETE_PUPILNAME";
        public const string AUTOCOMPLETE_DISEASESNAME = "AUTOCOMPLETE_DISEASESNAME";
        public const string VALUE_PUPILNAME = "VALUE_PUPILNAME";
        public const string VALUE_DISEASESNAME = "VALUE_DISEASESNAME";

        public const string PERMISSION_ALLOW = "PERMISSION_ALLOW";
        public const string GRID_EMPTY = "GRID_EMPTY";
        public const string LISTDISEASES = "LIST_DISEASES";
        public const int NOTOTHERDISEASES = 1; // không phải bệnh khác
        public const int OTHERDISEASES = 0; // bệnh khác
        public const string GETOTHERDISEASES = "GETOTHERDISEASES";// biến để lấy thông tin tên bệnh khác
        public const string GETDISEASES = "GETDISEASES"; // biến lấy tên bệnh khác tên bệnh khác
        public const string GETKEYDISEASES = "GETKEYDISEASES";//lấy mã của tên bệnh
        public const string TEMPLATE_DICHBENH = "Template_Dichbenh.xls";
        public const string TEMPLATE_DICHBENH_FORMAT = "HS_MacBenhTruyenNhiem_{0}.xls";//KHOI,LOP,TRUONG
        public const string TEMPLATE_TITLE = "SỔ QUẢN LÝ BỆNH TRUYỀN NHIỄM KHỐI:{0}-LỚP:{1}";
        public const string ALL_SCHOOL = "ToanTruong";
        public const string YEAR_FORMAT = "Năm học {0} - {1}";
        public const string PROVINCE_FORMAT = ",ngày {0} tháng {1} năm {2}";
        public const string ALL = "TẤT CẢ";
        public const int OTHER_DISEASES_TYPE = 40;

        public const string USER_PERMISSION = "permissiom of user";
        public const string EMPLOYEE_MEDICAL = "Nhân viên Y Tế";
    }
}