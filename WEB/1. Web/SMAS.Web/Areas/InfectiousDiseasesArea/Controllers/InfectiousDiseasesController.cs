﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.InfectiousDiseasesArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.InfectiousDiseasesArea.Controllers
{
    public class InfectiousDiseasesController : BaseController
    {
        private readonly IInfectiousDiseasBusiness InfectiousDiseasesBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IDiseasesTypeBusiness DiseasesTypeBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        public InfectiousDiseasesController(IInfectiousDiseasBusiness infectiousdiseasesBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IPupilProfileBusiness PupilProfileBusiness
            , IDistrictBusiness DistrictBusiness
            , ICommuneBusiness CommuneBusiness
            , IProvinceBusiness ProvinceBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IDiseasesTypeBusiness DiseasesTypeBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IEmployeeBusiness EmployeeBusiness)
        {
            this.InfectiousDiseasesBusiness = infectiousdiseasesBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.CommuneBusiness = CommuneBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.DiseasesTypeBusiness = DiseasesTypeBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        //
        // GET: /InfectiousDiseases/

        public ActionResult Index()
        {
            SetViewData();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //add search info
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            SearchInfo["FromInfectionDate"] = objAcademicYear.FirstSemesterStartDate;            
            ViewData[InfectiousDiseasesConstants.DATEFIRSTACAD] = objAcademicYear.FirstSemesterStartDate;
            if (_globalInfo.IsCurrentYear)
            {
                SearchInfo["ToInfectionDate"] = DateTime.Now.Date;
                ViewData[InfectiousDiseasesConstants.DATEENDACAD] = DateTime.Now.Date;
            }
            else
            {
                SearchInfo["ToInfectionDate"] = objAcademicYear.SecondSemesterEndDate;
                ViewData[InfectiousDiseasesConstants.DATEENDACAD] = objAcademicYear.SecondSemesterEndDate;
            }            
            IEnumerable<InfectiousDiseasesViewModel> lst = this._Search(SearchInfo).OrderByDescending(o => o.CreateDate).ThenBy(o => o.InfectionDate).ThenBy(o => o.PupilNameForCoE);
            ViewData[InfectiousDiseasesConstants.LIST_INFECTIOUSDISEASES] = lst;
            ViewData[InfectiousDiseasesConstants.PERMISSION_ALLOW] = _globalInfo.IsCurrentYear;
            ViewData[InfectiousDiseasesConstants.GRID_EMPTY] = (lst.Count() != 0);
            //ViewData[InfectiousDiseasesConstants.USER_PERMISSION] = GetPermissionOfUser();
            GetPermission();
            return View();
        }

        //
        // GET: /InfectiousDiseases/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            if(!string.IsNullOrEmpty(frm.PupilName))
            {
                string[] FullName = frm.PupilName.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (FullName.Count() > 1)
                {
                    frm.PupilName = FullName[0];
                }
            }
            //add search info
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;

            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["ClassID"] = frm.Class;
            SearchInfo["FullName"] = frm.PupilName;

            SearchInfo["FromInfectionDate"] = frm.FromDate;
            SearchInfo["ToInfectionDate"] = frm.ToDate;
            SearchInfo["DiseasesID"] = frm.DiseasesName;

            IEnumerable<InfectiousDiseasesViewModel> lst = this._Search(SearchInfo).OrderByDescending(o => o.CreateDate).ThenBy(o => o.InfectionDate).ThenBy(o => o.PupilNameForCoE);
            ViewData[InfectiousDiseasesConstants.LIST_INFECTIOUSDISEASES] = lst;

            //Get view data here
            ViewData[InfectiousDiseasesConstants.PERMISSION_ALLOW] = _globalInfo.IsCurrentYear;
            ViewData[InfectiousDiseasesConstants.GRID_EMPTY] = (lst.Count() != 0);
            //ViewData[InfectiousDiseasesConstants.USER_PERMISSION] = GetPermissionOfUser();
            GetPermission();
            return PartialView("_List");
        }

        public ActionResult GetEdit(int id)
        {
            InfectiousDiseas infectiousdiseases = this.InfectiousDiseasesBusiness.Find(id);
            InfectiousDiseasesViewModel idvm = new InfectiousDiseasesViewModel();
            idvm.InfectiousDiseasesID = infectiousdiseases.InfectiousDiseasesID;
            idvm.EducationLevel = infectiousdiseases.EducationLevelID;
            idvm.PupilID = infectiousdiseases.PupilID;
            idvm.Class = infectiousdiseases.ClassID;
            idvm.PupilNameForCoEAuto = infectiousdiseases.PupilProfile.FullName;
            idvm.Address = infectiousdiseases.Address;
            idvm.ProvinceID = infectiousdiseases.Commune.District.ProvinceID;
            idvm.DistrictID = infectiousdiseases.Commune.DistrictID;
            idvm.CommuneID = infectiousdiseases.CommuneID;
            idvm.InfectionDate = infectiousdiseases.InfectionDate;
            idvm.OffDate = infectiousdiseases.OffDate;
            idvm.DiseasesNameForCoE = infectiousdiseases.DiseasesName;
            idvm.Note = infectiousdiseases.Note;
            idvm.DiseasesNameOther = infectiousdiseases.Other.ToString();
            idvm.CreateDate = infectiousdiseases.CreatedDate;
            if (infectiousdiseases.CreatedDate.HasValue)
            {
                ViewData["CREATE_DATE"] = infectiousdiseases.CreatedDate.Value.ToString("dd/MM/yyyy");
            }
            //Set lại viewdata
            if (_globalInfo.EducationLevels != null)
                ViewData[InfectiousDiseasesConstants.LISTEDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            else ViewData[InfectiousDiseasesConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });

            ViewData[InfectiousDiseasesConstants.DATEFIRSTACAD] = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).FirstSemesterStartDate.Value;
            ViewData[InfectiousDiseasesConstants.DATEENDACAD] = DateTime.Now.Date;

            //var schooProfile = this.SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            //if (schooProfile == null) throw new BusinessException("Dữ liệu không hợp lệ.");

            ViewData["provinceid"] = idvm.ProvinceID;
            ViewData["districtid"] = idvm.DistrictID;

            IQueryable<Province> lsP = ProvinceBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
            ViewData[InfectiousDiseasesConstants.LISTPROVINCE] = new SelectList(lsP.OrderBy(o => o.ProvinceName).ToList(), "ProvinceID", "ProvinceName");
            IQueryable<DiseasesType> lstDiseasesType = DiseasesTypeBusiness.Search(new Dictionary<string, object>());
            ViewData[InfectiousDiseasesConstants.LISTDISEASES] = new SelectList(lstDiseasesType.OrderBy(p => p.DiseasesTypeID).ToList(), "DiseasesTypeID", "Resolution");
            ViewData[InfectiousDiseasesConstants.AUTOCOMPLETE_DISEASESNAME] = getAutoCompleteDiseasesTypeData().Select(o => o.value);
            ViewData[InfectiousDiseasesConstants.VALUE_DISEASESNAME] = "";
            // End Set

            ViewData[InfectiousDiseasesConstants.AUTOCOMPLETE_PUPILNAME] = getAutoCompletePupilData(infectiousdiseases.EducationLevelID, infectiousdiseases.ClassID);
            ViewData[InfectiousDiseasesConstants.VALUE_PUPILNAME] = "";
            ViewData[InfectiousDiseasesConstants.GETOTHERDISEASES] = infectiousdiseases.Other.ToString();//biến xác định có phải bị bệnh khác không
            ViewData[InfectiousDiseasesConstants.GETDISEASES] = string.IsNullOrEmpty(infectiousdiseases.DiseasesName) ? string.Empty : infectiousdiseases.DiseasesName; // biến chứa tên bệnh

            // LỚP
            IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(infectiousdiseases.EducationLevelID);
            if (lsCP.Count() != 0)
                ViewData[InfectiousDiseasesConstants.LISTCLASS] = new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName", infectiousdiseases.ClassID);
            else ViewData[InfectiousDiseasesConstants.LISTCLASS] = new SelectList(new string[] { });


            // Quận/Huyện
            IQueryable<District> lsD = getDistrictFromProvide(idvm.ProvinceID.Value);
            if (lsD.Count() != 0)
            {
                ViewData[InfectiousDiseasesConstants.LISTDISTRICT] = new SelectList(lsD.OrderBy(p => p.DistrictName).ToList(), "DistrictID", "DistrictName");
            }
            else
            {
                ViewData[InfectiousDiseasesConstants.LISTDISTRICT] = new SelectList(new string[] { });
            }

            // Xã/Phường
            IQueryable<Commune> lsC = getCommuneFromDistrict(idvm.DistrictID.Value);
            if (lsC.Count() != 0)
            {
                ViewData[InfectiousDiseasesConstants.LISTCOMMUNE] = new SelectList(lsC.ToList(), "CommuneID", "CommuneName", idvm.CommuneID)
                    .OrderBy(p => p.Text);
            }
            else
            {
                ViewData[InfectiousDiseasesConstants.LISTCOMMUNE] = new SelectList(new string[] { });
            }

            return View("_Edit", idvm);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            InfectiousDiseas infectiousdiseases = new InfectiousDiseas();
            InfectiousDiseasesViewModel idvm = new InfectiousDiseasesViewModel();
            List<ComboObject> lsDiseasesName = getAutoCompleteDiseasesTypeData();
            int DiseasesID = 0;
            GlobalInfo global = new GlobalInfo();

            TryUpdateModel(idvm);
            infectiousdiseases.AcademicYearID = global.AcademicYearID.Value;
            infectiousdiseases.Address = idvm.Address;
            infectiousdiseases.ClassID = idvm.Class;
            infectiousdiseases.CommuneID = idvm.CommuneID;
            int pupilProfileID = Int32.Parse(idvm.PupilNameForCoE);
            try
            {
                PupilProfile objPupil = PupilProfileBusiness.Find(pupilProfileID);
                if (objPupil != null)
                {
                    infectiousdiseases.PupilID = objPupil.PupilProfileID;
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ErrorAddNewMessage")));
                }

            }
            catch (Exception ex)
            {
                throw new BusinessException("InfectiousDiseases_Label_NoPupilFound", ex.Message);
            }
            //infectiousdiseases.DiseasesName = idvm.DiseasesNameForCoE;
            if (idvm.DiseasesNameForCoE == "40")// lưu tên bệnh khác
            {
                infectiousdiseases.DiseasesName = idvm.DiseasesNameOther;
                infectiousdiseases.Other = Convert.ToBoolean(InfectiousDiseasesConstants.OTHERDISEASES);
                DiseasesID = 40;//nếu chọn bệnh khác thì ID = 40
            }
            else// lưu tên bệnh có trong combox bệnh khác
            {
                string nameDiseases = "";
                if (DiseasesTypeBusiness.Find(Convert.ToInt32(idvm.DiseasesNameForCoE)) != null)
                {
                    nameDiseases = DiseasesTypeBusiness.Find(Convert.ToInt32(idvm.DiseasesNameForCoE)).Resolution;
                }
                infectiousdiseases.DiseasesName = nameDiseases;
                infectiousdiseases.Other = Convert.ToBoolean(InfectiousDiseasesConstants.NOTOTHERDISEASES);
            }
            try
            {
                if (idvm.DiseasesNameForCoE != "40")
                {
                    ComboObject obj = new ComboObject();

                    for (int i = 0; i < lsDiseasesName.Count; i++)
                    {
                        obj = lsDiseasesName[i];
                        if (obj.key == idvm.DiseasesNameForCoE)
                        {
                            DiseasesID = int.Parse(obj.key);
                            break;
                        }
                    }
                }
                infectiousdiseases.DiseasesTypeID = DiseasesID;
            }
            catch (Exception ex)
            {
                infectiousdiseases.DiseasesTypeID = null;
                throw new BusinessException(ex.Message);
            }
            infectiousdiseases.EducationLevelID = idvm.EducationLevel;
            infectiousdiseases.InfectionDate = idvm.InfectionDateForCreate;
            infectiousdiseases.Note = idvm.Note;
            infectiousdiseases.OffDate = idvm.OffDateForCreate;
            infectiousdiseases.SchoolID = global.SchoolID.Value;
            Utils.Utils.TrimObject(infectiousdiseases);
            this.InfectiousDiseasesBusiness.Insert(infectiousdiseases);
            this.InfectiousDiseasesBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(FormCollection frm, int InfectiousDiseasesID, int PupilID)
        {
            InfectiousDiseas infectiousdiseases = this.InfectiousDiseasesBusiness.Find(InfectiousDiseasesID);
            PupilProfile objPupil = PupilProfileBusiness.Find(PupilID);
            if (objPupil != null)
                infectiousdiseases.PupilID = objPupil.PupilProfileID;
            else return Json(new JsonMessage(Res.Get("InfectiousDiseases_Label_NoPupilFound")));           

            if (frm.AllKeys.Contains("EducationLevel"))
            {
                int EducationLevelID = 0;
                Int32.TryParse(frm["EducationLevel"].Trim(), out EducationLevelID);
                if (EducationLevelID > 0)
                    infectiousdiseases.EducationLevelID = EducationLevelID;
            }

            if (frm.AllKeys.Contains("Class"))
            {
                int ClassID = 0;
                Int32.TryParse(frm["Class"].Trim(), out ClassID);
                if (ClassID > 0)
                    infectiousdiseases.ClassID = ClassID;
            }

            if (frm.AllKeys.Contains("Address"))
            {
                string Address = frm["Address"].Trim();
                if (!string.IsNullOrEmpty(Address))
                    infectiousdiseases.Address = Address;
            }

            if (frm.AllKeys.Contains("CommuneID"))
            {
                int CommuneID = 0;
                Int32.TryParse(frm["CommuneID"].Trim(), out CommuneID);
                if (CommuneID > 0)
                    infectiousdiseases.CommuneID = CommuneID;
            }

            if (frm.AllKeys.Contains("DiseasesNameForCoE"))
                {
                int DiseasesTypeID = 0;
                Int32.TryParse(frm["DiseasesNameForCoE"].Trim(), out DiseasesTypeID);
                if (DiseasesTypeID == InfectiousDiseasesConstants.OTHER_DISEASES_TYPE) // Loại bệnh chưa có trong danh sách
                {
                    if (frm.AllKeys.Contains("DiseasesNameOther"))
            {
                        string DiseasesNameOther = frm["DiseasesNameOther"].Trim();
                        if (!string.IsNullOrEmpty(DiseasesNameOther))
            {
                            infectiousdiseases.DiseasesName = DiseasesNameOther;
            }
                }                
                    infectiousdiseases.DiseasesTypeID = InfectiousDiseasesConstants.OTHER_DISEASES_TYPE;
                    infectiousdiseases.Other = false;
            }
                else if (DiseasesTypeID > 0) // lưu tên bệnh có trong combox bệnh
            {
                    DiseasesType obj = DiseasesTypeBusiness.Find(DiseasesTypeID);
                    if (obj != null)
                        {
                        infectiousdiseases.DiseasesTypeID = DiseasesTypeID;
                        infectiousdiseases.DiseasesName = obj.Resolution;
                        }
                    infectiousdiseases.Other = true;
                    }
                }

            if (frm.AllKeys.Contains("InfectionDate"))
                {
                DateTime InfectionDate = new DateTime();
                if (DateTime.TryParse(frm["InfectionDate"].Trim(), out InfectionDate))
                    infectiousdiseases.InfectionDate = InfectionDate;
                }

            if (frm.AllKeys.Contains("OffDate"))
            {
                DateTime OffDate;
                if (DateTime.TryParse(frm["OffDate"].Trim(), out OffDate))
                {
                    infectiousdiseases.OffDate = OffDate;
                }
                else
                {
                    infectiousdiseases.OffDate = null;
                }

            }

            if (frm.AllKeys.Contains("Note"))
            {
                string Note = frm["Note"].Trim();
                    infectiousdiseases.Note = Note;
            }

            this.InfectiousDiseasesBusiness.Update(infectiousdiseases);
            this.InfectiousDiseasesBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.InfectiousDiseasesBusiness.Delete(id);
            this.InfectiousDiseasesBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<InfectiousDiseasesViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");

            //lấy trạng thái học sinh đang học đã tốt nghiệp theo năm học được chọn
            AcademicYear academicYearObj = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic), academicYearObj, _globalInfo.Semester.Value);
            //End
            IQueryable<InfectiousDiseas> query = from a in this.InfectiousDiseasesBusiness.Search(SearchInfo)
                                                 join b in ls on a.PupilID equals b.PupilID
                                                 select a;

            IQueryable<InfectiousDiseasesViewModel> lst = query.OrderByDescending(o => o.CreatedDate).Select(o => new InfectiousDiseasesViewModel
            {
                InfectiousDiseasesID = o.InfectiousDiseasesID,
                DiseasesTypeID = o.DiseasesTypeID,
                Class = o.ClassID,
                AcademicYearID = o.AcademicYearID,
                EducationLevel = o.EducationLevelID,
                SchoolID = o.SchoolID,
                PupilID = o.PupilID,
                InfectionDate = o.InfectionDate,
                OffDate = o.OffDate,
                Address = o.Address,
                CommuneID = o.CommuneID,
                DiseasesNameForCoE = o.DiseasesName,
                DistrictName = o.Commune.District.DistrictName,
                CommuneName = o.Commune.CommuneName,
                ProvinceName = o.Commune.District.Province.ProvinceName,
                Note = o.Note,
                CreateDate = o.CreatedDate
            });
            List<InfectiousDiseasesViewModel> lsIDVM = lst.Distinct().ToList();
            foreach (var item in lsIDVM)
            {
                ClassProfile cp = ClassProfileBusiness.Find(item.Class);
                item.ClassName = cp!=null?cp.DisplayName:"";

                PupilProfile pp = PupilProfileBusiness.Find(item.PupilID);
                item.PupilNameForCoE = pp!=null?pp.FullName:"";
                item.GenreName = (pp.Genre == (int)SystemParamsInFile.GENRE_MALE) ? male : female;
                item.PupilAndCodeName = pp.FullName + "-" + pp.PupilCode;
            }

            return lsIDVM;
        }

        #region setViewData

        private void SetViewData()
        {
            SetViewDataStatic();
            ViewData[InfectiousDiseasesConstants.LISTCLASS] = new SelectList(new string[] { });
            ViewData[InfectiousDiseasesConstants.LISTCOMMUNE] = new SelectList(new string[] { });
            ViewData[InfectiousDiseasesConstants.LISTDISTRICT] = new SelectList(new string[] { });

            //ViewData[InfectiousDiseasesConstants.AUTOCOMPLETE_PUPILNAME] = getAutoCompletePupilData(null, null).Select(o => o.value);
            ViewData[InfectiousDiseasesConstants.VALUE_PUPILNAME] = "";
        }

        private void SetViewDataStatic()
        {
            if (_globalInfo.EducationLevels != null)
            {
                ViewData[InfectiousDiseasesConstants.LISTEDUCATIONLEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[InfectiousDiseasesConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
            }


            DateTime dateStart = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value).FirstSemesterStartDate.Value;
            ViewData[InfectiousDiseasesConstants.DATEFIRSTACAD] = dateStart;
            ViewData[InfectiousDiseasesConstants.DATEENDACAD] = DateTime.Now.Date;

            IQueryable<Province> lsP = ProvinceBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });

            var schooProfile = this.SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            if (schooProfile == null) throw new BusinessException("Dữ liệu không hợp lệ.");
            ViewData["provinceid"] = schooProfile.ProvinceID;
            ViewData["districtid"] = schooProfile.DistrictID;
            ViewData[InfectiousDiseasesConstants.LISTPROVINCE] = new SelectList(lsP, "ProvinceID", "ProvinceName").OrderBy(p=>p.Text);
            IQueryable<DiseasesType> lstDiseasesType = DiseasesTypeBusiness.Search(new Dictionary<string, object>());
            ViewData[InfectiousDiseasesConstants.LISTDISEASES] = new SelectList(lstDiseasesType.OrderBy(p=>p.DiseasesTypeID).ToList(), "DiseasesTypeID", "Resolution");
            ViewData[InfectiousDiseasesConstants.AUTOCOMPLETE_DISEASESNAME] = getAutoCompleteDiseasesTypeData().Select(o => o.value);
            ViewData[InfectiousDiseasesConstants.VALUE_DISEASESNAME] = "";
        }

        #endregion setViewData

        #region ho tro

        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            //: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary)
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"SchoolID",global.SchoolID.Value}
                };

            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return lsCP;
        }

        private IQueryable<District> getDistrictFromProvide(int ProvinceID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",ProvinceID},
                };
            IQueryable<District> lsD = DistrictBusiness.Search(dic);
            return lsD;
        }

        private IQueryable<Commune> getCommuneFromDistrict(int DistrictID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"DistrictID",DistrictID},
                };
            IQueryable<Commune> lsC = CommuneBusiness.Search(dic);
            return lsC;
        }

        private List<string> getAutoCompletePupilData(int? EducationLevelID, int? ClassID)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<PupilOfClass> iqPoC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"ClassID",ClassID}
            });
            if (iqPoC.Count() > 0)
            {
                iqPoC = iqPoC.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING);
            }
            List<string> lsCO = new List<string>();
            string fullName = string.Empty;
            foreach (var o in iqPoC)
            {
                fullName = o.PupilProfile.FullName + "<span style=\"display:none;\">©" + o.PupilProfile.PupilProfileID + "</span>";
                lsCO.Add(fullName);
            }
            return lsCO;
        }

        private List<ComboObject> getAutoCompleteDiseasesTypeData()
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<DiseasesType> iqDT = DiseasesTypeBusiness.Search(new Dictionary<string, object>()
            {
                {"IsActive",true}
            });
            List<ComboObject> lsCO = new List<ComboObject>();
            foreach (var o in iqDT)
            {
                ComboObject co = new ComboObject(o.DiseasesTypeID.ToString(), o.Resolution);
                lsCO.Add(co);
            }
            return lsCO;
        }

        #endregion ho tro

        #region ajax load combobox


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadDistrict(int? ProvinceID)
        {
            if (ProvinceID.HasValue)
            {
                IQueryable<District> lsD = getDistrictFromProvide(ProvinceID.Value);
                if (lsD.Count() != 0)
                {
                    return Json(new SelectList(lsD.ToList(), "DistrictID", "DistrictName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadCommune(int? DistrictID)
        {
            if (DistrictID.HasValue)
            {
                IQueryable<Commune> lsC = getCommuneFromDistrict(DistrictID.Value);
                if (lsC.Count() != 0)
                {
                    return Json(new SelectList(lsC.ToList(), "CommuneID", "CommuneName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion ajax load combobox

        #region ajax load auto complete

        public PartialViewResult LoadAutoCompletePupilNameInSearch(int? EducationLevelID, int? ClassID)
        {
            ViewData[InfectiousDiseasesConstants.AUTOCOMPLETE_PUPILNAME] = getAutoCompletePupilData(EducationLevelID, ClassID);
            ViewData[InfectiousDiseasesConstants.VALUE_PUPILNAME] = "";
            return PartialView("_AutoCompletePupilName");
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadAutoCompletePupilNameInCoE(int? EducationLevelID, int? ClassID)
        {
            ViewData[InfectiousDiseasesConstants.AUTOCOMPLETE_PUPILNAME] = getAutoCompletePupilData(EducationLevelID, ClassID);
            ViewData[InfectiousDiseasesConstants.VALUE_PUPILNAME] = "";
            return PartialView("_AutoCompletePupilNameForCoE");
        }

        #endregion ajax load auto complete

        #region export Excel

        public FileResult ExportExcel(SearchViewModel frm)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", InfectiousDiseasesConstants.TEMPLATE_DICHBENH);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTRange templateRange = sheet.GetRange("A1", "M75");

            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            List<object> QueenDic = new List<object>();

            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;

            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["ClassID"] = frm.Class;
            SearchInfo["FullName"] = frm.PupilName;

            var provider = new System.Globalization.CultureInfo("vi-VN");
            string sFromDate = this.HttpContext.Request.QueryString["FromDate"];
            DateTime FromDate;
            if (DateTime.TryParse(sFromDate, provider, System.Globalization.DateTimeStyles.None, out FromDate))
            {
                SearchInfo["FromInfectionDate"] = FromDate;
            }
            string sToDate = this.HttpContext.Request.QueryString["ToDate"];
            DateTime ToDate;
            if (DateTime.TryParse(sToDate, provider, System.Globalization.DateTimeStyles.None, out ToDate))
            {
                SearchInfo["ToInfectionDate"] = ToDate;
            }
            //SearchInfo["FromInfectionDate"] = frm.FromDate;
            //SearchInfo["ToInfectionDate"] = frm.ToDate;
            SearchInfo["DiseasesID"] = frm.DiseasesName;

            List<InfectiousDiseasesViewModel> lst = this._Search(SearchInfo).OrderByDescending(o => o.CreateDate)
                                    .ThenBy(o => o.InfectionDate).ThenBy(o => o.PupilNameForCoE).ToList();
            for (int i = 0; i < lst.Count(); i++)
            {
                InfectiousDiseasesViewModel thisModel = lst[i];
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["FullName"] = thisModel.PupilNameForCoE;
                princeDic["Genre"] = thisModel.GenreName;
                princeDic["Class"] = thisModel.ClassName;

                Commune commune = CommuneBusiness.Find(thisModel.CommuneID);
                if (commune != null)
                {
                    princeDic["Commune"] = commune.CommuneName;
                    princeDic["District"] = commune.District.DistrictName;
                    princeDic["Province"] = commune.District.Province.ProvinceName;
                }
                else
                {
                    princeDic["Commune"] = "";
                    princeDic["District"] = "";
                    princeDic["Province"] = "";
                }

                princeDic["Address"] = thisModel.Address;
                princeDic["InfectionDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.InfectionDate);
                princeDic["DiseasesName"] = thisModel.DiseasesNameForCoE;
                princeDic["OffDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.OffDate);
                princeDic["Note"] = thisModel.Note;

                QueenDic.Add(princeDic);
            }
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string year = InfectiousDiseasesConstants.YEAR_FORMAT;
            string date = sp.Province.ProvinceName + InfectiousDiseasesConstants.PROVINCE_FORMAT;
            string dateTitle = string.Format(date, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string yearTitle = string.Format(year, ay.Year, (ay.Year + 1));


            EducationLevel el = EducationLevelBusiness.Find(frm.EducationLevel);
            ClassProfile cp = ClassProfileBusiness.Find(frm.Class);
            string templateTitle = InfectiousDiseasesConstants.TEMPLATE_TITLE;

            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SupervisingDeptName", sp.SupervisingDept.SupervisingDeptName);
            KingDic.Add("SchoolName", sp.SchoolName);
            KingDic.Add("TitleYear", yearTitle);
            KingDic.Add("ProvinceNameAndDate", dateTitle);
            KingDic.Add("TitleTemplate", string.Format(templateTitle, (el != null) ? el.Resolution.ToUpper() : InfectiousDiseasesConstants.ALL, (cp != null) ? cp.DisplayName.ToUpper() : InfectiousDiseasesConstants.ALL));

            templateRange.FillVariableValue(KingDic);
            string fileName = "HS";//THCS_DSHSMacBenhTruyenNhiem_Khoi6.xls";
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Noi them TH neu la cap 1
            {
                fileName += "_TH";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY) // Noi them THCS neu la cap 2
            {
                fileName += "_THCS";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY) // Noi them THPT neu la cap 3
            {
                fileName += "_THPT";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN) // Noi them NT neu la nha tre
            {
                fileName += "_MG";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE) // Noi them MG neu la mau giao
            {
                fileName += "_NT";
            }
            fileName += "_DSHSMacBenhTruyenNhiem";
            if (cp != null) // Noi them ten lop neu chon 1 lop
            {
                fileName += "_" + SMAS.Business.Common.Utils.StripVNSign(cp.DisplayName);
            }
            else if (el != null) // Noi them ten khoi neu chon ca khoi
            {
                fileName += "_" + SMAS.Business.Common.Utils.StripVNSign(el.Resolution.Replace(" ", ""));
            }
            fileName += ".xls";
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        #endregion export Excel

        #region get Detail

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetDetail(int InfectiousDiseasesID)
        {
            InfectiousDiseas infectiousdiseases = this.InfectiousDiseasesBusiness.Find(InfectiousDiseasesID);
            InfectiousDiseasesViewModel idvm = new InfectiousDiseasesViewModel();
            idvm.InfectiousDiseasesID = infectiousdiseases.InfectiousDiseasesID;
            idvm.EducationLevel = infectiousdiseases.EducationLevelID;
            idvm.Class = infectiousdiseases.ClassID;
            idvm.PupilNameForCoE = infectiousdiseases.PupilProfile.FullName;
            idvm.Address = infectiousdiseases.Address;
            idvm.ProvinceID = infectiousdiseases.Commune.District.ProvinceID;
            idvm.DistrictID = infectiousdiseases.Commune.DistrictID;
            idvm.CommuneID = infectiousdiseases.CommuneID;
            idvm.InfectionDate = infectiousdiseases.InfectionDate;
            idvm.OffDate = infectiousdiseases.OffDate;
            idvm.DiseasesNameForCoE = infectiousdiseases.DiseasesName;
            idvm.Note = infectiousdiseases.Note;

            EducationLevel edu = EducationLevelBusiness.Find(idvm.EducationLevel);
            idvm.EducationLevelName = edu != null ? edu.Resolution : "";

            ClassProfile cp = ClassProfileBusiness.Find(idvm.Class);
            idvm.ClassName = cp != null ? cp.DisplayName : "";

            Province province = ProvinceBusiness.Find(idvm.ProvinceID);
            idvm.ProvinceName = province != null ? province.ProvinceName : "";

            District dis = DistrictBusiness.Find(idvm.DistrictID);
            idvm.DistrictName = dis != null ? dis.DistrictName : "";

            Commune c = CommuneBusiness.Find(idvm.CommuneID);
            idvm.CommuneName = c != null ? c.CommuneName : "";

            return View("_Detail", idvm);
        }

        #endregion


        #region Role permission
        /// <summary>
        /// Lấy quyền của người dùng
        /// </summary>
        /// <returns></returns>
        private int GetPermissionOfUser()
        {
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsEmployeeManager)
                return SystemParamsInFile.PER_DELETE;
            else
            {
                // Kiem tra co la Nhan vien Y te
                Employee emp = EmployeeBusiness.Find(_globalInfo.EmployeeID);
                if (emp.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE &&
                        emp.WorkType.Resolution.ToLower().Equals(InfectiousDiseasesConstants.EMPLOYEE_MEDICAL.ToLower()))
                    return SystemParamsInFile.PER_DELETE;

                // Nếu ko chỉ có quyền xem/xuất excel
                return SystemParamsInFile.PER_VIEW;
            }
        }
        #endregion

        private void GetPermission()
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
        }
    }
}
