/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.InfectiousDiseasesArea.Models
{
    public class InfectiousDiseasesViewModel
    {
        [ResourceDisplayName("InfectiousDiseas_Label_InfectiousDiseaseID")]
        public int InfectiousDiseasesID { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_DiseasesType")]
        public Nullable<int> DiseasesTypeID { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int Class { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_Class")]
        public string ClassName { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_AcademicYear")]
        public int AcademicYearID { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EducationLevel { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_EducationLevel")]
        public string EducationLevelName { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_SchoolID")]
        public int SchoolID { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_PupilID")]
        public int PupilID { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_PupilID")]
        public string PupilNameForCoE { get; set; }

        public string PupilNameForCoEAuto { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_InfectionDate")]
        public Nullable<System.DateTime> InfectionDate { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_InfectionDate")]
        public string Date { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_OffDate")]
        public Nullable<System.DateTime> OffDate { get; set; }

        //them truong vi loi datetimepicker cua telerik
        //an dc khi them moi nhung ko an dc khi update
        [ResourceDisplayName("InfectiousDiseas_Label_InfectionDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public Nullable<System.DateTime> InfectionDateForCreate { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_OffDate")]
        public Nullable<System.DateTime> OffDateForCreate { get; set; }

        //
        [ResourceDisplayName("InfectiousDiseas_Label_Address")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Address { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_CommuneID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int CommuneID { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_CommuneID")]
        public string CommuneName { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_DistrictID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? DistrictID { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_DistrictID")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_ProvinceID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? ProvinceID { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_ProvinceID")]
        public string ProvinceName { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_DiseasesName")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string DiseasesNameForCoE { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_Note")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Note { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_PupilID")]
        public string PupilAndCodeName { get; set; }

        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string DiseasesNameOther { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? CreateDate { get; set; }
    }
}
