/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.InfectiousDiseasesArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", InfectiousDiseasesConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxEducationLevelChange(this)")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", InfectiousDiseasesConstants.LISTCLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Class { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [UIHint("Textbox")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilName { get; set; }

        [ResourceDisplayName("Examination_Label_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("ViewDataKey", InfectiousDiseasesConstants.DATEFIRSTACAD)]
        [AdditionalMetadata("Placeholder", "All")]
        //[DataConstraint(DataConstraintAttribute.GREATER, "FirstDayOfSemester", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        //[DataConstraint(DataConstraintAttribute.LESS, "ToDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? FromDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("MonitoringBook_Label_FirstDayOfSemester")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FirstDayOfSemester { get; set; }

        [ResourceDisplayName("Examination_Label_ToDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("ViewDataKey", InfectiousDiseasesConstants.DATEENDACAD)]
        [AdditionalMetadata("Placeholder", "All")]
        //[DataConstraint(DataConstraintAttribute.LESS_EQUALS, "EndDayOfSemester", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        //[DataConstraint(DataConstraintAttribute.GREATER, "FromDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? ToDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("MonitoringBook_Label_EndDayOfSemester")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDayOfSemester { get; set; }

        [ResourceDisplayName("InfectiousDiseas_Label_DiseasesName")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", InfectiousDiseasesConstants.LISTDISEASES)]
        [AdditionalMetadata("Placeholder", "All")]
        public string DiseasesName { get; set; }

    }
}