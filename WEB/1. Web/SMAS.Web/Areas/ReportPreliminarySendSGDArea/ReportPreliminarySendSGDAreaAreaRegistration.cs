﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportPreliminarySendSGDArea
{
    public class ReportPreliminarySendSGDAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportPreliminarySendSGDArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportPreliminarySendSGDArea_default",
                "ReportPreliminarySendSGDArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
