﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportPreliminarySendSGDArea.Controllers
{
    public class ReportPreliminarySendSGDController : BaseController
    {
        //
        // GET: /ReportPreliminarySendSGDArea/ReportPreliminarySendSGD/
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IReportPreliminarySendSGDBusiness ReportPreliminarySendSGDBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public ReportPreliminarySendSGDController(IReportDefinitionBusiness ReportDefinitionBusiness, IReportPreliminarySendSGDBusiness ReportPreliminarySendSGDBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportPreliminarySendSGDBusiness = ReportPreliminarySendSGDBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        public ActionResult _Index()
        {
            this.SetViewData();
            return PartialView();
        }

        private void SetViewData()
        {
            ViewData[ReportPreliminarySendSGDConstants.LIST_SEMESTER] = CommonList.Semester_SK();
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime? FirstStartDate = academicYear.FirstSemesterStartDate.Value.Date;
            DateTime? FirstEndDate = academicYear.FirstSemesterEndDate.Value.Date;
            int defaultSemester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            if (DateTime.Now.Date >= FirstStartDate && DateTime.Now.Date <= FirstEndDate)
            {
                defaultSemester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
            }
            ViewData[ReportPreliminarySendSGDConstants.DF_SEMESTER] = defaultSemester;
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportPreliminaryBO reportPreliminary)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            reportPreliminary.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            reportPreliminary.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            reportPreliminary.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
            string reportCode = "";
            if (GlobalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                 reportCode = SystemParamsInFile.REPORT_SO_KET_GUI_SOGD;
            }
            else
            {
                 reportCode = SystemParamsInFile.REPORT_SO_KET_GUI_SOGD_THPT;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                string inputParameterHashKey = ReportPreliminarySendSGDBusiness.GetHashKey(reportPreliminary);
                processedReport = ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportPreliminarySendSGDBusiness.ExcelCreateReportPreliminarySendSGD(reportPreliminary);
                processedReport = ReportPreliminarySendSGDBusiness.ExcelInsertReportPreliminarySendSGD(reportPreliminary, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportPreliminaryBO reportPreliminary)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            reportPreliminary.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            reportPreliminary.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            reportPreliminary.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();

            Stream excel = ReportPreliminarySendSGDBusiness.ExcelCreateReportPreliminarySendSGD(reportPreliminary);
            ProcessedReport processedReport = ReportPreliminarySendSGDBusiness.ExcelInsertReportPreliminarySendSGD(reportPreliminary, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID},
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string>();
            if (GlobalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                listRC = new List<string> { 
                SystemParamsInFile.REPORT_SO_KET_GUI_SOGD
            };
            }
            else
            {
                listRC = new List<string> { 
                SystemParamsInFile.REPORT_SO_KET_GUI_SOGD_THPT
            };
            }
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;
            return result;


        }

        public ActionResult ViewHtmTemplate()
        {
            return PartialView("_HtmlPattern");
        }
    }
}
