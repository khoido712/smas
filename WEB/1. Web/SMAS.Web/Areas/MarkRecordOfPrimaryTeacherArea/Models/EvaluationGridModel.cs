﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea.Models
{
    public class EvaluationGridModel
    {
        public long EvaluationCommentsID { get; set; }
        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public DateTime BirthDay { get; set; }
        public int OrderInClass { get; set; }
        public int PupilID { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public string Comment4 { get; set; }
        public string Comment5 { get; set; }
        public int? PeriodicEndingMark { get; set; }
        public string EvaluationEnding { get; set; }
        public int? RetestMark { get; set; }
        public string EvaluationReTraining { get; set; }
        public int Status { get; set; }
        public string CommentsEnding { get; set; }
        public int MonthID_1 { get; set; }
        public int MonthID_2 { get; set; }
        public int MonthID_3 { get; set; }
        public int MonthID_4 { get; set; }
        public int MonthID_5 { get; set; }
        public int EvaluationCriteriaID { get; set; }
        public bool Error { get; set; }
        public string MessageError { get; set; }
        public Dictionary<string, object> arrComment { get; set; }        
        public int EvaluationId { get; set; }
        public int IsCommenting { get; set; }
    }
}