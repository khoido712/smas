/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea.Models
{
    public class SearchViewModel
    {
        public int? SemesterID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? ClassProfileID { get; set; }
        public int? SubjectID { get; set; }
    }
}