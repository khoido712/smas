﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea.Models
{
    public class MarkRecorkTXDK
    {
        public System.Int32 ClassID { get; set; }
        public Nullable<System.Int32> SubjectID { get; set; }
        public System.Int32 PupilID { get; set; }
        public System.String PupilCode { get; set; }
        public System.String FullName { get; set; }
        public System.DateTime BirthDate { get; set; }
        public System.String Month1 { get; set; }              
        public System.String Month2 { get; set; }
        public System.String Month3 { get; set; }
        public System.String Month4 { get; set; }
        public System.String Month5 { get; set; }
        public System.String Month6 { get; set; }
        public System.String Month7 { get; set; }
        public System.String Month8 { get; set; }
        public System.String Month9 { get; set; }

        public Nullable<System.Decimal> DGK1 { get; set; }
        public Nullable<System.Decimal> VGK1 { get; set; }
        public Nullable<System.Decimal> GK1 { get; set; }
        public Nullable<System.Decimal> DCK1 { get; set; }
        public Nullable<System.Decimal> VCK1 { get; set; }
        public Nullable<System.Decimal> CK1 { get; set; }    
        public Nullable<System.Decimal> DGK2 { get; set; }
        public Nullable<System.Decimal> VGK2 { get; set; }
        public Nullable<System.Decimal> GK2 { get; set; }        
        public Nullable<System.Decimal> DCK2 { get; set; }
        public Nullable<System.Decimal> VCK2 { get; set; }
        public Nullable<System.Decimal> CK2 { get; set; }        
        public bool Disable { get; set; }
        public bool? IsShow { get; set; }
        public bool? Pass { get; set; }
        public bool? PassDK { get; set; }
        public string Note { get; set; }
        public string NoteDK { get; set; }
        public string SubjectName { get; set; }

        public Nullable<System.Decimal> HLHK1 { get; set; }
        public Nullable<System.Decimal> HLCN { get; set; }

        public System.String XLKH1 { get; set; }
        public System.String XLCN { get; set; }

        public string LockTitle { get; set; }
        
    }
}