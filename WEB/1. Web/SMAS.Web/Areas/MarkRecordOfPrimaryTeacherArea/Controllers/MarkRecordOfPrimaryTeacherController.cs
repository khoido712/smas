﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea.Models;
using SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Text;
using System.Configuration;
using System.Text.RegularExpressions;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea.Controllers
{
    public class MarkRecordOfPrimaryTeacherController : BaseController
    {
        //
        // GET: /MarkRecordOfPrimaryTeacherArea/MarkRecordOfPrimaryTeacher/
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IVMarkRecordBusiness VMarkRecordBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly ILockMarkPrimaryBusiness LockMarkPrimaryBusiness;
        private readonly IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IMonthCommentsBusiness MonthCommentsBusiness;
        private readonly IEvaluationCommentsBusiness EvaluationCommentsBusiness;
        private readonly IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness;

        public MarkRecordOfPrimaryTeacherController(IPupilLeavingOffBusiness PupilLeavingOffBusiness, IClassMovementBusiness ClassMovementBusiness, ISchoolMovementBusiness SchoolMovementBusiness, IUserAccountBusiness UserAccountBusiness, IMarkRecordBusiness markrecordBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness, IClassProfileBusiness ClassProfileBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            IEducationLevelBusiness educationLevelBusiness, ITeachingAssignmentBusiness teachingAssignmentBusiness, ISubjectCatBusiness subjectCatBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness, IMarkTypeBusiness markTypeBusiness, IAcademicYearBusiness academicYearBusiness,
            ISummedUpRecordBusiness summedUpRecordBusiness, IExemptedSubjectBusiness exemptedSubjectBusiness, ISchoolProfileBusiness schoolProfileBusiness,
            IPupilProfileBusiness pupilProfileBusiness, IVMarkRecordBusiness VMarkRecordBusiness, IVSummedUpRecordBusiness VSummedUpRecordBusiness, ILockMarkPrimaryBusiness LockMarkPrimaryBusiness,
            IMarkRecordHistoryBusiness MarkRecordHistoryBusiness, ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness, IMonthCommentsBusiness MonthCommentsBusiness,
            IEvaluationCommentsBusiness EvaluationCommentsBusiness, IEvaluationCriteriaBusiness EvaluationCriteriaBusiness, ISummedEvaluationBusiness SummedEvaluationBusiness
            )
        {
            this.PupilLeavingOffBusiness = PupilLeavingOffBusiness;
            this.ClassMovementBusiness = ClassMovementBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.MarkRecordBusiness = markrecordBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.MarkTypeBusiness = markTypeBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.VSummedUpRecordBusiness = VSummedUpRecordBusiness;
            this.VMarkRecordBusiness = VMarkRecordBusiness;
            this.LockMarkPrimaryBusiness = LockMarkPrimaryBusiness;
            this.MarkRecordHistoryBusiness = MarkRecordHistoryBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
            this.MonthCommentsBusiness = MonthCommentsBusiness;
            this.EvaluationCommentsBusiness = EvaluationCommentsBusiness;
            this.EvaluationCriteriaBusiness = EvaluationCriteriaBusiness;
            this.SummedEvaluationBusiness = SummedEvaluationBusiness;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetSubjectAndEducationGroup()
        {
            
            int evalutionID = MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION;
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;//giao vien bo mon
            int classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]);
            int semester = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int subjectID = SMAS.Business.Common.Utils.GetInt(Request["subjectID"]);
            int monthID = SMAS.Business.Common.Utils.GetInt(Request["monthID"]);
            bool isGetFirstSubject = !string.IsNullOrEmpty(SMAS.Business.Common.Utils.GetString(Request["isGetFirstSubject"])) ? Convert.ToBoolean(SMAS.Business.Common.Utils.GetString(Request["isGetFirstSubject"])) : false;
            string subjectName = string.Empty;
            //lay list mon hoc
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && classID > 0)
            {
                lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semester, classID, _globalInfo.IsRolePrincipal ? true : _globalInfo.IsViewAll)
                .Where(o => o.SubjectCat.IsActive == true && o.SubjectCat.AppliedLevel == _globalInfo.AppliedLevel)
                    //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                .ToList();
            }
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_SUBJECT] = lsClassSubject;
            if ((isGetFirstSubject || subjectID == 0) && lsClassSubject.Count > 0)
            {
                subjectID = lsClassSubject.Select(p=>p.SubjectID).FirstOrDefault();
            }

            if (subjectID > 0)
            {
                subjectName = SubjectCatBusiness.Find(subjectID).SubjectName;
            }
            //lay list thang
            List<MonthComments> monthList = GetMonthBySemester(semester);
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_MONTH] = monthList;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_SUBJECT_IMPORT] = GetSubjectImport(semester, classID);
            
            List<EvaluationGridModel> listEvaluationResult = new List<EvaluationGridModel>();
            if (subjectID > 0 && lsClassSubject.Count > 0)
            {
                listEvaluationResult = this.SearchEvaluation(classID, semester, subjectID, typeOfTeacher, evalutionID);
            }
            //check quyen GVBM
            SetVisbileButton(semester, classID, subjectID);

            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID}
            };

            //lay danh sach nhan xet cua tat ca cac mon theo thang
            List<string> lstComment = new List<string>();
            IDictionary<string, object> dicComment = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",classID},
                //{"SemesterID",semester},
                {"TypeOfTeacherID",typeOfTeacher},
                {"EvaluationID",evalutionID},
                {"isAdmin",_globalInfo.IsAdmin}
            };
            lstComment = EvaluationCommentsBusiness.getListCommentAutoComplete(dicComment);

            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
            ViewData[MarkRecordOfPrimaryTeacherConstants.LOCK_MARK_TITLE] = LockMarkTitle;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_PUPIL_EVALUATION] = listEvaluationResult;
            ViewData[MarkRecordOfPrimaryTeacherConstants.MONTH_SELECT] = monthID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.SUBJECT_ID] = subjectID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.CHECK_ISCOMMENTING] = CheckSubjectCommenting(subjectID, classID);
            ViewData[MarkRecordOfPrimaryTeacherConstants.SEMESTER_ID] = semester;
            ViewData[MarkRecordOfPrimaryTeacherConstants.CLASS_ID] = classID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.ERROR_IMPORT] = true;
            ViewData[MarkRecordOfPrimaryTeacherConstants.ERROR_BASIC_DATA] = false;
            ViewData[MarkRecordOfPrimaryTeacherConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[MarkRecordOfPrimaryTeacherConstants.IS_CURENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_COMMENT] = lstComment;
            return PartialView("_GetSubjectAndEducationGroup");
        }

        [HttpPost]
        public PartialViewResult GetCapacities()
        {
            int evalutionID = MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES;// tab thu 2
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;//giao vien bo mon
            int classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]);
            int semester = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int monthID = SMAS.Business.Common.Utils.GetInt(Request["monthID"]);
            int subjectID = SMAS.Business.Common.Utils.GetInt(Request["subjectID"]);
            bool isGetFirstSubject = !string.IsNullOrEmpty(SMAS.Business.Common.Utils.GetString(Request["isGetFirstSubject"])) ? Convert.ToBoolean(SMAS.Business.Common.Utils.GetString(Request["isGetFirstSubject"])) : false;
            //lay list thang
            List<MonthComments> monthList = GetMonthBySemester(semester).Where(p => p.MonthID != MarkRecordOfPrimaryTeacherConstants.MONTH_EVALUATION_ENDING).ToList();
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_MONTH] = monthList;
            List<EvaluationGridModel> listEvaluationResult = new List<EvaluationGridModel>();
            //lay list mon hoc
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && classID > 0)
            {
                lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semester, classID, _globalInfo.IsRolePrincipal ? true : _globalInfo.IsViewAll)
                .Where(o => o.SubjectCat.IsActive == true)
                    //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                .ToList();
            }
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_SUBJECT] = lsClassSubject;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_SUBJECT_IMPORT] = GetSubjectImport(semester, classID);
            if (( isGetFirstSubject || subjectID == 0) && lsClassSubject.Count > 0)
            {
                subjectID = lsClassSubject.Select(p=>p.SubjectID).FirstOrDefault();
            }

            if (subjectID > 0 && lsClassSubject.Count > 0)
            {
                listEvaluationResult = this.SearchEvaluation(classID, semester, subjectID, typeOfTeacher, evalutionID);
            }
            SetVisbileButton(semester, classID, subjectID);
            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID}
            };
            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            //lay danh sach nhan xet cua tat ca cac mon theo thang
            List<string> lstComment = new List<string>();
            IDictionary<string, object> dicComment = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",classID},
               // {"SemesterID",semester},
                {"TypeOfTeacherID",typeOfTeacher},
                {"EvaluationID",evalutionID}
            };
            lstComment = EvaluationCommentsBusiness.GetListComment(dicComment);
            string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_PUPIL_EVALUATION] = listEvaluationResult;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LOCK_MARK_TITLE] = LockMarkTitle;
            ViewData[MarkRecordOfPrimaryTeacherConstants.MONTH_SELECT] = monthID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.SEMESTER_ID] = semester;
            ViewData[MarkRecordOfPrimaryTeacherConstants.CLASS_ID] = classID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.SUBJECT_ID] = subjectID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.CHECK_ISCOMMENTING] = CheckSubjectCommenting(subjectID, classID);
            ViewData[MarkRecordOfPrimaryTeacherConstants.IS_CURENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_COMMENT] = lstComment;

            return PartialView("_GetCapacities");
        }
        [HttpPost]
        public PartialViewResult GetQualities()
        {
            int evalutionID = MarkRecordOfPrimaryTeacherConstants.TAB_QUALITIES;// tab thu 3
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;//giao vien bo mon
            int classID = SMAS.Business.Common.Utils.GetInt(Request["classID"]);
            int semester = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int monthID = SMAS.Business.Common.Utils.GetInt(Request["monthID"]);
            int subjectID = SMAS.Business.Common.Utils.GetInt(Request["subjectID"]);
            bool isGetFirstSubject = !string.IsNullOrEmpty(SMAS.Business.Common.Utils.GetString(Request["isGetFirstSubject"])) ? Convert.ToBoolean(SMAS.Business.Common.Utils.GetString(Request["isGetFirstSubject"])) : false;
            //lay list thang
            List<MonthComments> monthList = GetMonthBySemester(semester).Where(p => p.MonthID != MarkRecordOfPrimaryTeacherConstants.MONTH_EVALUATION_ENDING).ToList();
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_MONTH] = monthList;
            //lay thong tin cac tieu chi GV Cn moi load tieu chi
            //List<EvaluationCriteria> listEvaluationCriteria = EvaluationCriteriaBusiness.getEvaluationCriteria().Where(p=>p.TypeID == evalutionID).ToList();

            //ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_EVALUATION_CRITERIA] = listEvaluationCriteria;
            List<EvaluationGridModel> listEvaluationResult = new List<EvaluationGridModel>();
            //lay list mon hoc
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && classID > 0)
            {
                lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semester, classID, _globalInfo.IsRolePrincipal ? true : _globalInfo.IsViewAll)
                .Where(o => o.SubjectCat.IsActive == true)
                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                .ToList();
            }
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_SUBJECT] = lsClassSubject;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_SUBJECT_IMPORT] = GetSubjectImport(semester, classID);
            if ((isGetFirstSubject || subjectID == 0) && lsClassSubject.Count > 0)
            {
                subjectID = lsClassSubject.Select(c => c.SubjectID).FirstOrDefault();
            }

            if (subjectID > 0 && lsClassSubject.Count > 0)
            {
                listEvaluationResult = this.SearchEvaluation(classID, semester, subjectID, typeOfTeacher, evalutionID);
            }
            //Kiem tra quyen hien thi cac button
            SetVisbileButton(semester, classID, subjectID);
            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID}
            };
            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            //lay danh sach nhan xet cua tat ca cac mon theo thang
            List<string> lstComment = new List<string>();
            IDictionary<string, object> dicComment = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",classID},
                //{"SemesterID",semester},
                {"TypeOfTeacherID",typeOfTeacher},
                {"EvaluationID",evalutionID}
            };
            lstComment = EvaluationCommentsBusiness.GetListComment(dicComment);
            string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_PUPIL_EVALUATION] = listEvaluationResult;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LOCK_MARK_TITLE] = LockMarkTitle;
            ViewData[MarkRecordOfPrimaryTeacherConstants.MONTH_SELECT] = monthID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.SEMESTER_ID] = semester;
            ViewData[MarkRecordOfPrimaryTeacherConstants.CLASS_ID] = classID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.SUBJECT_ID] = subjectID;
            ViewData[MarkRecordOfPrimaryTeacherConstants.CHECK_ISCOMMENTING] = CheckSubjectCommenting(subjectID, classID);
            ViewData[MarkRecordOfPrimaryTeacherConstants.IS_CURENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_COMMENT] = lstComment;

            return PartialView("_GetQualities");
        }


        /// <summary>
        /// Luu diem thang cua hoc sinh
        /// </summary>
        /// <param name="fc"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public JsonResult SaveCommentsbyMonth(FormCollection frm)
        {
            //var objWatch = new System.Diagnostics.Stopwatch();
            //objWatch.Start();
            int schoolId = _globalInfo.SchoolID.Value;
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int academicYearID = _globalInfo.AcademicYearID.Value;
            int evaluationID = Convert.ToInt32(frm["Tab"]);
            int semesterID = Convert.ToInt32(frm["HfSemesterID"]);
            int classID = Convert.ToInt32(frm["HfClassID"]);
            int subjectID = Convert.ToInt32(frm["Subject_ID"]);
            int educationLevelID = Convert.ToInt32(frm["HfEducationLevelID"]);
            int MonthID = Convert.ToInt32(frm["Month"]);
            #region check permision
            bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isSubjectTeacher = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, classID, subjectID, semesterID);
            if (!(isSchoolAdmin || isSubjectTeacher))
            {
                return Json(new JsonMessage("Thầy/cô không có quyền thao tác chức năng này"));
            }
            #endregion
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;
            List<EvaluationCommentsBO> evaluationList = EvaluationCommentsBusiness.getListEvaluationCommentsBySchool(schoolId, academicYearID, classID, semesterID, subjectID, typeOfTeacher, evaluationID).ToList(); ;

            List<SummedEvaluation> evaluationCommentsList = SummedEvaluationBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionId
                                 && p.ClassID == classID
                                 && p.AcademicYearID == academicYearID
                                 && p.SemesterID == semesterID
                                 && p.EvaluationID == evaluationID
                                 && (p.EvaluationCriteriaID == subjectID || subjectID == 0)
                                 ).ToList();

            List<EvaluationComments> list = new List<EvaluationComments>();
            EvaluationComments evaluationCommentsObj;
            int pupilID;
            string strInsert;
            EvaluationCommentsBO objEC;
            SummedEvaluation objSummedEvaluation;
            SummedEvaluation objSEtmp;
            List<SummedEvaluation> listSummedEvaluation = new List<SummedEvaluation>();
            List<ActionAuditDataBO> lstActionAuditData = new List<ActionAuditDataBO>();
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();

            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsmeterStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr= new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();

            StringBuilder oldObject = new StringBuilder();
            StringBuilder newObject = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            for (int i = 0; i < evaluationList.Count; i++)
            {
                objEC = evaluationList[i];
                if (objEC.Status != SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                pupilID = objEC.PupilID;
                strInsert = string.Empty;
                if (!"on".Equals(frm["ChkCLGD_" + pupilID]))
                {
                    continue;
                }
                objSEtmp = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();
                evaluationCommentsObj = new EvaluationComments
                {
                    PupilID = pupilID,
                    ClassID = classID,
                    EducationLevelID = educationLevelID,
                    AcademicYearID = _globalInfo.AcademicYearID.Value,
                    LastDigitSchoolID = partitionId,
                    SchoolID = _globalInfo.SchoolID.Value,
                    TypeOfTeacher = typeOfTeacher,
                    SemesterID = semesterID,
                    EvaluationCriteriaID = subjectID,
                    EvaluationID = evaluationID,
                    EvaluationCommentsID = objEC.EvaluationCommentsID
                };

                //Luu danh gia nhan xet cuoi ky
                objSummedEvaluation = new SummedEvaluation
                {
                    PupilID = pupilID,
                    ClassID = classID,
                    EducationLevelID = educationLevelID,
                    AcademicYearID = _globalInfo.AcademicYearID.Value,
                    LastDigitSchoolID = partitionId,
                    SchoolID = _globalInfo.SchoolID.Value,
                    SemesterID = semesterID,
                    EvaluationCriteriaID = subjectID,
                    EvaluationID = evaluationID
                };
                //truong hop mon tinh diem
                if (!string.IsNullOrEmpty(frm["txtPiriodicEndingMark_" + pupilID]))
                {
                    objSummedEvaluation.PeriodicEndingMark = Convert.ToInt32(frm["txtPiriodicEndingMark_" + pupilID]);
                }

                if (MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_1 || MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_6)
                {
                    if (frm["AreaCommentsM16_" + pupilID] != null)
                    {
                        evaluationCommentsObj.CommentsM1M6 = frm["AreaCommentsM16_" + pupilID].Trim();
                    }
                }
                else if (MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_2 || MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_7)
                {
                    if (frm["AreaCommentsM27_" + pupilID] != null)
                    {
                        evaluationCommentsObj.CommentsM2M7 = frm["AreaCommentsM27_" + pupilID].Trim();
                    }
                }
                else if (MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_3 || MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_8)
                {
                    strInsert += (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "T3," : "T8,";
                    if (frm["AreaCommentsM38_" + pupilID] != null)
                    {
                        evaluationCommentsObj.CommentsM3M8 = frm["AreaCommentsM38_" + pupilID].Trim();
                    }
                }
                else if (MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_4 || MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_9)
                {
                    if (frm["AreaCommentsM49_" + pupilID] != null)
                    {
                        evaluationCommentsObj.CommentsM4M9 = frm["AreaCommentsM49_" + pupilID].Trim();
                    }
                }
                else if (MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_5 || MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_10)
                {
                    if (frm["AreaCommentsM510_" + pupilID] != null)
                    {
                        evaluationCommentsObj.CommentsM5M10 = frm["AreaCommentsM510_" + pupilID].Trim();
                    }
                }


                if (MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_11)
                {
                    objSummedEvaluation.EndingComments = frm["AreaCommentsEnding_" + pupilID].Trim();
                }

                if (evaluationID == MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION)
                {
                    objSummedEvaluation.EndingEvaluation = frm["txtEvaluation_" + pupilID];
                }

                //if (isChange)
                //{
                    evaluationCommentsObj.NoteInsert = strInsert + DateTime.Now.ToString();
                    evaluationCommentsObj.CreateTime = DateTime.Now;
                    list.Add(evaluationCommentsObj);
                //}
                
                objSummedEvaluation.CreateTime = DateTime.Now;
                listSummedEvaluation.Add(objSummedEvaluation);
            }
            ActionAuditDataBO objActionAuditBO = new ActionAuditDataBO();
            if ((list != null && list.Count > 0) || (listSummedEvaluation != null && listSummedEvaluation.Count > 0))
            {
                IDictionary<string, object> dicLogAction = new Dictionary<string, object>();
                if (list != null && list.Count > 0)
                {
                    EvaluationCommentsBusiness.InsertOrUpdate(MonthID, schoolId, academicYearID, classID, educationLevelID, semesterID, evaluationID, subjectID, typeOfTeacher, list, ref  lstActionAuditData);
                }
                //Thuc hien them moi cap nhat danh gia nhan xet cuoi ky
                if (evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("PartitionId", partitionId);
                    dic.Add("SchoolId", schoolId);
                    dic.Add("AcademicYearId", academicYearID);
                    dic.Add("ClassId", classID);
                    dic.Add("EducationLevelId", educationLevelID);
                    dic.Add("SemesterId", semesterID);
                    dic.Add("EvaluationId", evaluationID);
                    dic.Add("EvaluationCriteriaID", subjectID);
                    dic.Add("IsRetest", false);//Co cap nhat diem thi lai
                    dic.Add("MonthId", MonthID);
                    SummedEvaluationBusiness.InsertOrUpdate(dic, listSummedEvaluation, lstActionAuditData, ref objActionAuditBO);
                    // Tạo dữ liệu ghi log
                    dicLogAction = new Dictionary<string, object>()
                    {
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,objActionAuditBO.OldData.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,objActionAuditBO.NewData.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objActionAuditBO.ObjID.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,objActionAuditBO.Description.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,objActionAuditBO.Parameter.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,objActionAuditBO.UserAction.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,objActionAuditBO.UserFunction.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,objActionAuditBO.UserDescription.ToString()}
                    };
                }
                else
                {
                    ActionAuditDataBO objAC = null;
                    lstActionAuditData = lstActionAuditData.Where(p => p.isInsertLog).ToList();
                    for (int i = 0; i < lstActionAuditData.Count; i++)
                    {
                        objAC = lstActionAuditData[i];

                        string tmpOld = string.Empty;
                        string tmpNew = string.Empty;
                        tmpOld = objAC.OldData.Length > 0 ? objAC.OldData.ToString().Substring(0, objAC.OldData.Length - 2) : "";
                        oldObjectStrtmp.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                        tmpNew = objAC.NewData.Length > 0 ? objAC.NewData.ToString().Substring(0, objAC.NewData.Length - 2) : "";
                        newObjectStrtmp.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                        userDescriptionsStrtmp.Append(objAC.UserDescription).Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                        userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");

                        objectIDStr.Append(objAC.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsmeterStr.Append(objAC.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionStr.Append(objAC.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsStr.Append(objAC.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsStr.Append(objAC.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectStr.Append(oldObjectStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        newObjectStr.Append(newObjectStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userDescriptionsStr.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);


                        oldObjectStrtmp = new StringBuilder();
                        newObjectStrtmp = new StringBuilder();
                        userDescriptionsStrtmp = new StringBuilder();
                    }
                    dicLogAction = new Dictionary<string, object>()
                    {
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsmeterStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                        {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                    };
                }
                // Tạo dữ liệu ghi log
                SetViewDataActionAudit(dicLogAction);
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_RequiredPrintSchool")));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveImportErr(FormCollection frm)
        {
           
            int schoolId = _globalInfo.SchoolID.Value;
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int academicYearID = _globalInfo.AcademicYearID.Value;
            int evaluationID = Convert.ToInt32(frm["Tab"]);
            int semesterID = Convert.ToInt32(frm["HfSemesterID"]);
            int classID = Convert.ToInt32(frm["HfClassID"]);
            int subjectID = Convert.ToInt32(frm["HfSubjectID"]);
            int educationLevelID = Convert.ToInt32(frm["HfEducationLevelIDErr"]);
            int MonthID = Convert.ToInt32(frm["HfMonth"]);
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;
            #region check permision
            bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isSubjectTeacher = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, classID, subjectID, semesterID);
            if (!isSchoolAdmin && !isSubjectTeacher)
            {
                return Json(new JsonMessage("Thầy/cô không có quyền Import chức năng này"));
            }
            #endregion

            List<EvaluationCommentsBO> evaluationList = EvaluationCommentsBusiness.getListEvaluationCommentsBySchool(schoolId, academicYearID, classID, semesterID, subjectID, typeOfTeacher, evaluationID).ToList(); ;
            List<EvaluationComments> list = new List<EvaluationComments>();
            EvaluationComments evaluationCommentsObj;
            int pupilID;
            string strInsert;
            EvaluationCommentsBO objEC;
            SummedEvaluation objSummedEvaluation;
            List<SummedEvaluation> listSummedEvaluation = new List<SummedEvaluation>();
            for (int i = 0; i < evaluationList.Count; i++)
            {
                objEC = evaluationList[i];
                if (objEC.Status != SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                pupilID = objEC.PupilID;
                strInsert = string.Empty;
                if (!"on".Equals(frm["ChkErr_" + pupilID]))
                {
                    continue;
                }
                evaluationCommentsObj = new EvaluationComments
                {
                    PupilID = pupilID,
                    ClassID = classID,
                    EducationLevelID = educationLevelID,
                    AcademicYearID = _globalInfo.AcademicYearID.Value,
                    LastDigitSchoolID = partitionId,
                    SchoolID = _globalInfo.SchoolID.Value,
                    TypeOfTeacher = typeOfTeacher,
                    SemesterID = semesterID,
                    EvaluationCriteriaID = subjectID,
                    EvaluationID = evaluationID,
                    EvaluationCommentsID = objEC.EvaluationCommentsID
                };

                //Luu danh gia nhan xet cuoi ky
                objSummedEvaluation = new SummedEvaluation
                {
                    PupilID = pupilID,
                    ClassID = classID,
                    EducationLevelID = educationLevelID,
                    AcademicYearID = _globalInfo.AcademicYearID.Value,
                    LastDigitSchoolID = partitionId,
                    SchoolID = _globalInfo.SchoolID.Value,
                    SemesterID = semesterID,
                    EvaluationCriteriaID = subjectID,
                    EvaluationID = evaluationID
                };
                int? MarkEnding = null;
                objSummedEvaluation.EndingComments = frm["hdfCommentEnding_" + pupilID];
                objSummedEvaluation.PeriodicEndingMark = !string.IsNullOrEmpty(frm["hdfPeriodicEndingMark_" + pupilID]) ? int.Parse(frm["hdfPeriodicEndingMark_" + pupilID]) : MarkEnding;

                //luu danh gia: TODO-----

                //truong hop mon nhan xet khong lay PeriodicEndingMark (chua lam)

                //lay thong tin commnet trong thang
                if (!string.IsNullOrEmpty(frm["hdfcomment1_" + pupilID]))
                {
                    strInsert += (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "T1," : "T6,";
                    evaluationCommentsObj.CommentsM1M6 = frm["hdfcomment1_" + pupilID];
                }

                if (!string.IsNullOrEmpty(frm["hdfcomment2_" + pupilID]))
                {
                    strInsert += (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "T2," : "T7,";
                    evaluationCommentsObj.CommentsM2M7 = frm["hdfcomment2_" + pupilID];
                }

                if (!string.IsNullOrEmpty(frm["hdfcomment3_" + pupilID]))
                {
                    strInsert += (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "T3," : "T8,";
                    evaluationCommentsObj.CommentsM3M8 = frm["hdfcomment3_" + pupilID];
                }

                if (!string.IsNullOrEmpty(frm["hdfcomment4_" + pupilID]))
                {
                    strInsert += (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "T4," : "T9,";
                    evaluationCommentsObj.CommentsM4M9 = frm["hdfcomment4_" + pupilID];
                }

                if (!string.IsNullOrEmpty(frm["hdfcomment5_" + pupilID]))
                {
                    strInsert += (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "T5," : "T10,";
                    evaluationCommentsObj.CommentsM5M10 = frm["hdfcomment5_" + pupilID];
                }

                evaluationCommentsObj.NoteInsert = strInsert + DateTime.Now.ToString();
                evaluationCommentsObj.CreateTime = DateTime.Now;
                list.Add(evaluationCommentsObj);

                listSummedEvaluation.Add(objSummedEvaluation);
            }

            if ((list != null && list.Count > 0) || (listSummedEvaluation != null && listSummedEvaluation.Count > 0))
            {
                if (list != null && list.Count > 0)
                {
                    EvaluationCommentsBusiness.InsertOrUpdateEducationQualityImport(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classID, educationLevelID, semesterID, evaluationID, subjectID, typeOfTeacher, list);
                }
                //Thuc hien them moi cap nhat danh gia nhan xet cuoi ky
                if (listSummedEvaluation != null && listSummedEvaluation.Count > 0)
                {
                    if (evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                    {
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        dic.Add("PartitionId", partitionId);
                        dic.Add("SchoolId", schoolId);
                        dic.Add("AcademicYearId", academicYearID);
                        dic.Add("ClassId", classID);
                        dic.Add("EducationLevelId", educationLevelID);
                        dic.Add("SemesterId", semesterID);
                        dic.Add("EvaluationId", evaluationID);
                        dic.Add("EvaluationCriteriaID", subjectID);
                        dic.Add("MonthId", MonthID);
                        dic.Add("IsRetest", false);//Co cap nhat diem thi lai
                        ActionAuditDataBO objActionAuditDataBO = new ActionAuditDataBO();
                        SummedEvaluationBusiness.InsertOrUpdate(dic, listSummedEvaluation,new List<ActionAuditDataBO>(),ref objActionAuditDataBO);
                    }
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_RequiredPrintSchool")));
            }

           
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")), "success");
        }

        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEvaluationCommments(string arrayID, int semesterID, int classID, int evaluationCriteriaID, int educationLevelID, int evaluationID, int monthId)
        {

            string[] ArrayID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (ArrayID == null || ArrayID.Count() == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn học sinh để xóa dữ liệu"));
            }
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();

            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsmeterStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            List<string> IDStrList = ArrayID.ToList();
            List<long> idList = IDStrList.Select(long.Parse).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("ClassId", classID);
            dic.Add("EducationLevelId", educationLevelID);
            dic.Add("SemesterId", semesterID);
            dic.Add("EvaluationId", evaluationID);
            dic.Add("EvaluationCriteriaID", evaluationCriteriaID);
            dic.Add("TypeOfTeacher", typeOfTeacher);
            dic.Add("MonthId", monthId);
            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"ClassID",classID}
            };
            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
            dic.Add("LockMarkTitle", LockMarkTitle);
            List<ActionAuditDataBO> lstActionAuditData = new List<ActionAuditDataBO>();
            EvaluationCommentsBusiness.BulkDeleteEvaluationComments(idList, dic, false, ref lstActionAuditData);

            ActionAuditDataBO objAC = null;
            for (int i = 0; i < lstActionAuditData.Count; i++)
            {
                objAC = lstActionAuditData[i];

                string tmpOld = string.Empty;
                tmpOld = objAC.OldData.Length > 0 ? objAC.OldData.ToString().Substring(0, objAC.OldData.Length - 2) : "";
                oldObjectStrtmp.Append("(Giá trị trước khi xóa): ").Append(tmpOld);
                userDescriptionsStrtmp.Append(objAC.UserDescription).Append("Giá trị cũ {").Append(tmpOld).Append("}");

                objectIDStr.Append(objAC.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                paramsmeterStr.Append(objAC.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                descriptionStr.Append(objAC.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userFuntionsStr.Append(objAC.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userActionsStr.Append(objAC.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                oldObjectStr.Append(oldObjectStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                newObjectStr.Append(newObjectStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userDescriptionsStr.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
            }
            IDictionary<string,object> dicLogAction = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsmeterStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
            };
            //Ghi log du lieu:
            SetViewDataActionAudit(dicLogAction);
            return Json(new JsonMessage("Xóa dữ liệu nhận xét đánh giá thành công ", "success"));
        }

        
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult SaveOneComments(string comments, int pupilID, int monthID, int subjectID, int classID, int educationLevelID, int semester, int tab)
        {
            EvaluationComments obj = new EvaluationComments();
            obj.PupilID = pupilID;
            obj.ClassID = classID;
            obj.EducationLevelID = educationLevelID;
            obj.AcademicYearID = _globalInfo.AcademicYearID.Value;
            obj.LastDigitSchoolID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            obj.SchoolID = _globalInfo.SchoolID.Value;
            obj.TypeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;
            obj.SemesterID = semester;
            obj.EvaluationCriteriaID = subjectID;

            EvaluationComments objDB = new EvaluationComments();
            objDB = (from e in EvaluationCommentsBusiness.All
                     where e.AcademicYearID == _globalInfo.AcademicYearID
                     && e.SchoolID == _globalInfo.SchoolID
                     && e.ClassID == classID
                     && e.EducationLevelID == educationLevelID
                     && e.EvaluationCriteriaID == subjectID
                     && e.TypeOfTeacher == MarkRecordOfPrimaryTeacherConstants.GVBM
                     && e.SemesterID == semester
                     && e.PupilID == pupilID
                     select e).FirstOrDefault();

            if (tab == MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION)
            {
                obj.EvaluationID = MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION;//danh gia CLGD
                obj.EvaluationCriteriaID = subjectID;
            }
            else if (tab == MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES) //nang luc
            {
                obj.EvaluationID = MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES;
            }
            else //Pham chat
            {
                obj.EvaluationID = MarkRecordOfPrimaryTeacherConstants.TAB_QUALITIES;
            }

            if (monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_1 || monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_6)
            {
                obj.CommentsM1M6 = comments.Trim();
            }
            else if (monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_2 || monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_7)
            {
                obj.CommentsM2M7 = comments.Trim();
            }
            else if (monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_3 || monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_8)
            {
                obj.CommentsM3M8 = comments.Trim();
            }
            else if (monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_4 || monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_9)
            {
                obj.CommentsM4M9 = comments.Trim();
            }
            else if (monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_5 || monthID == MarkRecordOfPrimaryTeacherConstants.MONTH_10)
            {
                obj.CommentsM5M10 = comments.Trim();
            }
            ActionAuditDataBO objAc = new ActionAuditDataBO();
            EvaluationCommentsBusiness.InsertOrUpdateEvaluationCommentsByPupilID(monthID, obj,ref objAc);
            if (objAc != null && objAc.isInsertLog)
            {
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLogAction = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,objAc.OldData.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,objAc.NewData.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objAc.ObjID.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,objAc.Description.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,objAc.Parameter.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,objAc.UserAction.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,objAc.UserFunction.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,objAc.UserDescription.ToString()}
                };
                //Ghi log du lieu:
                SetViewDataActionAudit(dicLogAction);
            }
            return Json(new JsonMessage("Lưu thành công"));
        }

        [ActionAudit]
        public FileResult ExportExcel(int classID, int semester, string className)
        {
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            string templatePath = string.Empty;
            string OutPutName = string.Empty;
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", MarkRecordTeacherExcelConstants.TEMPLATE_FILE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = null;

            //Lay list mon hoc
            List<ClassSubject> lstClassSubject = new List<ClassSubject>();
            if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && classID > 0)
            {
                lstClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semester, classID, _globalInfo.IsRolePrincipal ? true : _globalInfo.IsViewAll)
                .Where(o => o.SubjectCat.IsActive == true)
                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                .ToList();
            }

            ClassSubject objClassSubject;
            if (lstClassSubject == null || lstClassSubject.Count == 0)
            {
                throw new BusinessException("Lớp chưa được khai báo môn học. Thầy/cô vui lòng kiểm tra lại");
            }

            //Lay danh sach cac cot da khoa
            List<MonthComments> monthCommentsList = this.GetMonthBySemester(semester).ToList();
            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID}
            };
            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch.Add("AcademicYearId", academicYear.AcademicYearID);
            dicSearch.Add("SemesterId", semester);
            dicSearch.Add("TypeOfTeacher", typeOfTeacher);
            dicSearch.Add("SchoolId", academicYear.SchoolID);
            List<int> lstSubject = lstClassSubject.Select(c => c.SubjectID).ToList();


            //Lay du lieu tat ca cac mon hoc cua lop
            List<EvaluationCommentsReportBO> lstEvaluationComents = EvaluationCommentsBusiness.GetAllEvaluationCommentsbyClass(classID, dicSearch, lstSubject).ToList();
            List<EvaluationCommentsReportBO> lstEvaluationComentsbySubject = new List<EvaluationCommentsReportBO>();
            int endRow = 0;
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation;
            VTDataValidation objValidationNumber;
            string[] s = new string[] { MarkRecordOfPrimaryTeacherConstants.FINISH, MarkRecordOfPrimaryTeacherConstants.NOTFINISH };
            for (int c = 0; c < lstClassSubject.Count; c++)
            {
                objClassSubject = lstClassSubject[c];
                objValidation = new VTDataValidation();
                lstEvaluationComentsbySubject = lstEvaluationComents.Where(e => e.EvaluationCriteriaID == objClassSubject.SubjectID).OrderBy(e => e.OrderInClass).ThenBy(e => e.Name).ThenBy(e => e.FullName).ToList();
                endRow = MarkRecordTeacherExcelConstants.ROW_START + lstEvaluationComentsbySubject.Count;
                if (objClassSubject.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)//Mon tinh diem
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                    objValidation.ToColumn = MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION;
                    objValidation.FromColumn = MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION;
                    objValidationNumber = new VTDataValidation
                    {
                        FromColumn = MarkRecordTeacherExcelConstants.COLUMN_END_MARK,
                        ToColumn = MarkRecordTeacherExcelConstants.COLUMN_END_MARK,
                        FromRow = MarkRecordTeacherExcelConstants.ROW_START,
                        ToRow = endRow,
                        Type = VTValidationType.INTEGER,
                        FromValue = "1",
                        ToValue = "10",
                        SheetIndex = c + 1
                    };
                    lstValidation.Add(objValidationNumber);
                }
                else
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(2));
                    objValidation.ToColumn = MarkRecordTeacherExcelConstants.COLUMN_END_MARK;
                    objValidation.FromColumn = MarkRecordTeacherExcelConstants.COLUMN_END_MARK;
                }
                sheet.Name = Utils.Utils.StripVNSignAndSpace(objClassSubject.SubjectCat.DisplayName);
                FillExcelEducationQuality(sheet, lstEvaluationComentsbySubject, semester, className, academicYear, objClassSubject, monthCommentsList, LockMarkTitle);
                //Tao du lieu validation

                objValidation.FromRow = MarkRecordTeacherExcelConstants.ROW_START;
                objValidation.ToRow = endRow;
                objValidation.SheetIndex = c + 1;
                objValidation.Contrains = s;
                objValidation.Type = VTValidationType.LIST;
                lstValidation.Add(objValidation);
                sheet.SetFontName("Times New Roman", 11);
            }

            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();

            className = className.Replace("Lớp", "");
            className = className.Replace("lớp", "");
            className = className.Replace("LỚP", "");
            className = Utils.Utils.StripVNSignAndSpace(className.Replace(" ", ""));
            string fileName = string.Format("BangDanhGiaThangLop{0}_HK{1}_GVBM.xls", className, semester);
            Stream excel = oBook.ToStreamValidationData(lstValidation);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            //lưu log
            ViewData[CommonKey.AuditActionKey.ObjectID] = classID;
            ViewData[CommonKey.AuditActionKey.Description] = "Xuất Excel Sổ TD CLGD (GVBM)";
            ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_EXPORT;
            ViewData[CommonKey.AuditActionKey.userFunction] = "Sổ TD CLGD (GVBM)";
            ViewData[CommonKey.AuditActionKey.userDescription] = "Xuất excel Sổ theo dõi CLGD (GVBM), " + className + ", Học kỳ" + semester + ", năm học " + academicYear.Year + "-" + (academicYear.Year + 1);
            return result;
        }
        /// <summary>
        /// Fill du lieu chat luong giao duc vao file excel
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="listresult"></param>
        /// <param name="semester"></param>
        /// <param name="className"></param>
        /// <param name="objAcademicYear"></param>
        /// <param name="isCommeting"></param>
        /// <returns></returns>
        private void FillExcelEducationQuality(IVTWorksheet sheet, List<EvaluationCommentsReportBO> lstEvaluationComments, int semester, string className, AcademicYear objAcademicYear, ClassSubject objClassSubject, List<MonthComments> monthCommentsList, string LockMarkTitle)
        {

            className = className.Replace("Lớp", "");
            className = className.Replace("lớp", "");
            className = className.Replace("LỚP", "");
            className = className.Trim();
            ///Thong tin truong
            string strTitle = string.Format("BẢNG THEO DÕI CHẤT LƯỢNG GIÁO DỤC MÔN {0} HỌC KỲ {1} - LỚP {2}", objClassSubject.SubjectCat.DisplayName.ToUpper(), semester, className.ToUpper());
            string strAcademicYear = Res.Get("AcademicYear_Control_Year") + " " + objAcademicYear.DisplayTitle;
            sheet.SetCellValue("A2", objAcademicYear.SchoolProfile.SchoolName.ToUpper());
            sheet.SetCellValue("A3", strTitle);
            sheet.SetCellValue("A4", strAcademicYear.ToUpper());

            //fill cac thang tieu de
            int startColumnTitle = MarkRecordTeacherExcelConstants.COLUMN_TITLE;
            int startRowTitle = MarkRecordTeacherExcelConstants.ROW_TILTE;
            monthCommentsList = monthCommentsList.Where(p => p.MonthID != MarkRecordOfPrimaryTeacherConstants.MONTH_11).ToList();
            for (int m = 0; m < monthCommentsList.Count; m++)
            {
                sheet.SetCellValue(startRowTitle, (startColumnTitle + 3 * m), monthCommentsList[m].Name);
            }
            if (objClassSubject.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                sheet.SetCellValue(startRowTitle, MarkRecordTeacherExcelConstants.COLUMN_END_MARK, "KTĐK CKII");
            }
            // Fill du lieu nhan xet trong thang
            int startRow = MarkRecordTeacherExcelConstants.ROW_START;
            int startColumn = 1;
            EvaluationCommentsReportBO obj;
            EvaluationComments objEducation;
            EvaluationComments objCapcities;
            EvaluationComments objQualities;
            SummedEvaluation objSummedEvaluation;

            if (lstEvaluationComments == null || lstEvaluationComments.Count == 0)
            {
                return;
            }
            string LockCK = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "CK1" : "CK2";
            string endingEvaluation = string.Empty;
            for (int i = 0; i < lstEvaluationComments.Count; i++)
            {
                obj = lstEvaluationComments[i];
                //Lay nhan xet danh gia mon hoc, nang luc, pham chat cua
                objEducation = obj.DicEvaluation[MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION];
                objCapcities = obj.DicEvaluation[MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES];
                objQualities = obj.DicEvaluation[MarkRecordOfPrimaryTeacherConstants.TAB_QUALITIES];
                sheet.SetCellValue(startRow, startColumn, i + 1);//STT
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_CODE, obj.PupilCode);//ma hoc sinh
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_CODE, startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_CODE).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_NAME, obj.FullName);// ho ten hoc sinh
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_NAME, startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_NAME).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_BIRTHDAY, obj.Birthday.ToString("dd/MM/yyyy"));// ngay thang nam sinh
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_BIRTHDAY, startRow, MarkRecordTeacherExcelConstants.COLUMN_BIRTHDAY).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_BIRTHDAY, startRow, MarkRecordTeacherExcelConstants.COLUMN_BIRTHDAY).SetHAlign(VTHAlign.xlHAlignLeft);

                //sheet.GetRange(startRow, startColumn + 3, startRow, startColumn + 3).FillColor(System.Drawing.Color.Red);
                //Nhan xet thang thu nhat
                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION, (objEducation != null) ? objEducation.CommentsM1M6 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_CAPACITIES, (objCapcities != null) ? objCapcities.CommentsM1M6 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_CAPACITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_CAPACITIES).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_QUALITIES, (objQualities != null) ? objQualities.CommentsM1M6 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_QUALITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_QUALITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_QUALITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_QUALITIES).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T1", ""))
                {
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_QUALITIES).IsLock = true;
                }

                //Nhan xet thang thu 2
                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_EDUCATION, (objEducation != null) ? objEducation.CommentsM2M7 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_EDUCATION).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_EDUCATION).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_CAPACITIES, (objCapcities != null) ? objCapcities.CommentsM2M7 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_CAPACITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_CAPACITIES).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_QUALITIES, (objQualities != null) ? objQualities.CommentsM2M7 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_QUALITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_QUALITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_QUALITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_QUALITIES).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T2", ""))
                {
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_QUALITIES).IsLock = true;
                }

                //Nhan xet thang thu 3
                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_EDUCATION, (objEducation != null) ? objEducation.CommentsM3M8 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_EDUCATION).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_EDUCATION).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_CAPACITIES, (objCapcities != null) ? objCapcities.CommentsM3M8 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_CAPACITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_CAPACITIES).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_QUALITIES, (objQualities != null) ? objQualities.CommentsM3M8 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_QUALITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_QUALITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_QUALITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_QUALITIES).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T3", ""))
                {
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_QUALITIES).IsLock = true;
                }

                //Nhan xet thang thu 4
                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION, (objEducation != null) ? objEducation.CommentsM4M9 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_CAPACITIES, (objCapcities != null) ? objCapcities.CommentsM4M9 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_CAPACITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_CAPACITIES).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_QUALITIES, (objQualities != null) ? objQualities.CommentsM4M9 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_QUALITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_QUALITIES).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T4", ""))
                {
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_QUALITIES).IsLock = true;
                }

                //Nhan xet thang thu 5
                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION, (objEducation != null) ? objEducation.CommentsM5M10 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_CAPACITIES, (objCapcities != null) ? objCapcities.CommentsM5M10 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_CAPACITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_CAPACITIES, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_CAPACITIES).WrapText();

                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_QUALITIES, (objQualities != null) ? objQualities.CommentsM5M10 : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_QUALITIES).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_QUALITIES).WrapText();
                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, "T5", ""))
                {
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_QUALITIES).IsLock = true;
                }

                objSummedEvaluation = obj.DicSummedEvaluation[MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION];
                endingEvaluation = (objSummedEvaluation != null) ? objSummedEvaluation.EndingEvaluation : "";
                //Nhan xet cuoi ky
                sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING, (objSummedEvaluation != null) ? objSummedEvaluation.EndingComments : "");
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING, startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING, startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING).WrapText();

                if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, LockCK, "CK"))
                {
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING, startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING).IsLock = true;
                }
                //Neu la mon tinhs diem thi dien cot KTCK
                if (objClassSubject.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK) //mon tinh diem
                {
                    sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK, (objSummedEvaluation != null) ? objSummedEvaluation.PeriodicEndingMark : null);

                    //Dien diem thi lai hoac diem cuoi nam
                    /*if (obj.RetestMark.HasValue && obj.RetestMark.Value > 0)
                    {
                        sheet.SetCellValue(startRow, startColumn + 10, obj.RetestMark);
                    }
                    else
                    {
                        sheet.SetCellValue(startRow, startColumn + 10, obj.PiriodicEndingMark);
                    }*/

                    if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, LockCK, "CK"))
                    {
                        sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK, startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).IsLock = true;
                    }
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK, startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION, "HT".Equals(endingEvaluation) ? MarkRecordOfPrimaryTeacherConstants.FINISH : "CHT".Equals(endingEvaluation) ? MarkRecordOfPrimaryTeacherConstants.NOTFINISH : endingEvaluation);
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    //sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION, startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION).IsLock = true;
                    /*//Danh gia sau thi lai hoac danh gia cuoi nam
                    if (!string.IsNullOrEmpty(obj.EvaluationEnding))
                    {
                        sheet.SetCellValue(startRow, startColumn + 11, obj.EvaluationEnding);
                    }
                    else
                    {
                        sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION, "HT".Equals(obj.EvaluationEnding) ? MarkRecordOfPrimaryTeacherConstants.FINISH : "CHT".Equals(obj.EvaluationEnding) ? MarkRecordOfPrimaryTeacherConstants.NOTFINISH : obj.EvaluationEnding);
                    }*/

                    if ((i + 1) % 5 == 0)
                    {
                        sheet.GetRange(startRow, startColumn, startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                    else
                    {
                        sheet.GetRange(startRow, startColumn, startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    if ((i + 1) == lstEvaluationComments.Count())
                    {
                        sheet.GetRange(startRow, startColumn, startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                }
                else //Mon nhat xet
                {
                    sheet.SetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK, "HT".Equals(endingEvaluation) ? MarkRecordOfPrimaryTeacherConstants.FINISH : "CHT".Equals(endingEvaluation) ? MarkRecordOfPrimaryTeacherConstants.NOTFINISH : endingEvaluation);

                    /*if (!string.IsNullOrEmpty(obj.EvaluationReTraining))
                    {
                        sheet.SetCellValue(startRow, startColumn + 10, "HT".Equals(obj.EvaluationReTraining) ? MarkRecordOfPrimaryTeacherConstants.FINISH : "CHT".Equals(obj.EvaluationReTraining) ? MarkRecordOfPrimaryTeacherConstants.NOTFINISH : obj.EvaluationReTraining);
                    }
                    else
                    {
                        sheet.SetCellValue(startRow, startColumn + 10, "HT".Equals(obj.EvaluationEnding) ? MarkRecordOfPrimaryTeacherConstants.FINISH : "CHT".Equals(obj.EvaluationEnding) ? MarkRecordOfPrimaryTeacherConstants.NOTFINISH : obj.EvaluationEnding);
                    }*/
                    sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK, startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    //sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK, startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).IsLock = true;
                    /*if (UtilsBusiness.CheckLockMarkBySemester(semester, LockMarkTitle, LockCK, "CK"))
                    {
                        sheet.GetRange(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK, startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).IsLock = true;
                    }*/

                    if ((i + 1) % 5 == 0)
                    {
                        sheet.GetRange(startRow, startColumn, startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                    else
                    {
                        sheet.GetRange(startRow, startColumn, startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    if ((i + 1) == lstEvaluationComments.Count())
                    {
                        sheet.GetRange(startRow, startColumn, startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                }
                //học sinh khác đang học tô đỏ ghạch ngang
                if (obj.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, startColumn, startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, startColumn, startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION).IsLock = true;
                }
                startRow += 1;
            }
            //sheet.ProtectSheet();
            sheet.SetFontName("Times New Roman", 11);

        }

        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int classID, int semester, int educationLevelID, string arrayID, bool importCapacities, bool importQualities)
        {
            //var objWatch = new System.Diagnostics.Stopwatch();
            //objWatch.Start();
            string[] ArrayID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (ArrayID == null || ArrayID.Count() == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn môn học"));
            }
            List<string> IDStrList = ArrayID.ToList();
            List<int> lstSubjectImport = IDStrList.Select(Int32.Parse).ToList();
            //Lay danh sach mon hoc theo quyen nguoi dung

            List<ClassSubject> lstClassSubject = new List<ClassSubject>();
            if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && classID > 0)
            {
                lstClassSubject = GetSubjectImport(semester, classID).Where(s => lstSubjectImport.Contains(s.SubjectID)).ToList();
            }

            if (lstClassSubject == null || lstClassSubject.Count == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa được phân công phụ trách môn học đã chọn"), JsonMessage.ERROR);
            }
            int schoolId = _globalInfo.SchoolID.Value;
            int partitionNumber = UtilsBusiness.GetPartionId(schoolId);
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;
            if (attachments == null || attachments.Count() <= 0)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError")));

            //The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();
            if (file == null)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format")));
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            //kiem tra file extension lan nua
            List<string> excelExtension = new List<string>();
            excelExtension.Add(".XLS");
            excelExtension.Add(".XLSX");
            var extension = Path.GetExtension(file.FileName);
            if (!excelExtension.Contains(extension.ToUpper()))
            {
                return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format")));
            }
            if (file.ContentLength / 1024 > MarkRecordOfPrimaryTeacherConstants.FILE_UPLOAD_MAX_SIZE)
            {
                return Json(new JsonMessage("Dung lượng file excel không được lớn hơn 5MB", JsonMessage.ERROR));
            }

            //luu ra dia
            var fileName = Path.GetFileNameWithoutExtension(file.FileName);
            fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
            var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
            file.SaveAs(physicalPath);

            IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
            StringBuilder validateSubjectName = new StringBuilder();
            string subjectName;
            //Doc du lieu va validate tung sheet
            IVTWorksheet sheet = null;
            List<IVTWorksheet> lstSheet;
            try
            {
                lstSheet = oBook.GetSheets();
            }
            catch
            {
                return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format")));
            }

            #region "Validate file excel"

            //Validate sheet name
            string msgTitle = string.Empty;
            string sheetName;
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                sheetName = Utils.Utils.StripVNSignAndSpace(lstClassSubject[i].SubjectCat.DisplayName);
                sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(sheetName));
                if (sheet == null)
                {
                    continue;
                }
                msgTitle = this.ErrValidateFileExcel(sheet, MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION, semester, classID,
                                                     lstClassSubject[i].ClassProfile.DisplayName, academicYear, lstClassSubject[i].SubjectID, sheetName);
                if (string.IsNullOrEmpty(msgTitle))
                {
                    continue;
                }
                break;

            }

            if (!String.IsNullOrEmpty(msgTitle))
            {
                string msg = String.Format("Import dữ liệu không thành công. {0} Thầy cô vui lòng kiểm tra lại.", msgTitle);
                return Json(new JsonMessage(msg, JsonMessage.ERROR));
            }

            //Kiem tra sheet mon hoc co trong danh sach mon hoc duoc import
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                subjectName = lstClassSubject[i].SubjectCat.DisplayName;
                if (!lstSheet.Select(s => s.Name).Contains(Utils.Utils.StripVNSignAndSpace(subjectName)))
                {
                    if (String.IsNullOrEmpty(validateSubjectName.ToString()))
                    {
                        validateSubjectName.Append(subjectName);
                    }
                    else
                    {
                        validateSubjectName.Append(", " + subjectName);
                    }
                }
            }
            if (!string.IsNullOrEmpty(validateSubjectName.ToString()))
            {
                //string msg = string.Format("Thầy/cô không có quyền import các môn học: {0}", validateSubjectName.ToString());
                //return Json(new JsonMessage(msg, JsonMessage.ERROR));
                return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format"), JsonMessage.ERROR));
            }
            #endregion

            List<PupilOfClassBO> lstPupilOfClass = PupilOfClassBusiness.GetPupilInClass(classID).ToList().OrderBy(c => c.OrderInClass).ThenBy(c => SMAS.Business.Common.Utils.SortABC(c.Name + " " + c.PupilFullName)).ToList();
            List<EvaluationCommentsReportBO> lstOutPut = new List<EvaluationCommentsReportBO>();
            ClassSubject objClassSubject;
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            //Kiem tra du lieu dung dong cua cac mon hoc
            #region "Dien du lieu vao object"
            try
            {
                for (int i = 0; i < lstClassSubject.Count; i++)
                {
                    objClassSubject = lstClassSubject[i];
                    sheetName = Utils.Utils.StripVNSignAndSpace(lstClassSubject[i].SubjectCat.DisplayName);
                    sheet = lstSheet.FirstOrDefault(c => c.Name.Equals(sheetName));
                    if (sheet == null)
                    {
                        continue;
                    }
                    GetDataFromFileExcel(sheet, lstPupilOfClass, academicYear, partitionId, educationLevelID, classID, objClassSubject.SubjectID, semester, objClassSubject.IsCommenting.Value, lstOutPut);
                }
            }
            catch
            {
                return Json(new JsonMessage(Res.Get("Common_Label_File_Upload_Incorrect_format")));
            }
            if (lstOutPut == null || lstOutPut.Count == 0)
            {
                return Json(new JsonMessage("Không có dữ liệu để import"));

            }


            List<EvaluationCommentsReportBO> excelDataErrorList = lstOutPut.Where(p => p.Error == false && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING && p.MessageError != null).ToList();
            if (excelDataErrorList != null && excelDataErrorList.Count > 0)
            {
                //return Json(new JsonMessage("File dữ liệu không hợp lệ. Thầy cô vui lòng kiểm tra lại"), "error");
                ViewData[MarkRecordOfPrimaryTeacherConstants.SEMESTER_ID] = semester;
                ViewData[MarkRecordOfPrimaryTeacherConstants.CLASS_ID] = classID;
                ViewData[MarkRecordOfPrimaryTeacherConstants.ERROR_IMPORT] = false;
                ViewData[MarkRecordOfPrimaryTeacherConstants.ERROR_BASIC_DATA] = false;
                ViewData[MarkRecordOfPrimaryTeacherConstants.ERROR_IMPORT_MESSAGE] = "";
                ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_PUPIL_EVALUATION] = excelDataErrorList;
                //return ve trang loi
                return Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "ViewPopupError"));
            }

            List<EvaluationComments> lstEvaluationInsertOrUpdate = new List<EvaluationComments>();
            List<SummedEvaluation> lstSummedEvaluation = new List<SummedEvaluation>();
            SummedEvaluation objSummedEvaluation = null;
            EvaluationCommentsReportBO sourceData = null;
            EvaluationComments sourceEvaluation;
            for (int i = 0; i < lstOutPut.Count; i++)
            {
                //InsertOrUpdateObj = new EvaluationComments();
                sourceData = lstOutPut[i];
                //Chi import cac hoc sinh co trang thai dang hoc
                if (sourceData.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                sourceEvaluation = sourceData.DicEvaluation[MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION];
                if (sourceEvaluation != null)
                {
                    lstEvaluationInsertOrUpdate.Add(sourceEvaluation);
                }

                sourceEvaluation = sourceData.DicEvaluation[MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES];
                if (sourceEvaluation != null && importCapacities)
                {
                    lstEvaluationInsertOrUpdate.Add(sourceEvaluation);
                }
                sourceEvaluation = sourceData.DicEvaluation[MarkRecordOfPrimaryTeacherConstants.TAB_QUALITIES];
                if (sourceEvaluation != null && importQualities)
                {
                    lstEvaluationInsertOrUpdate.Add(sourceEvaluation);
                }
                objSummedEvaluation = sourceData.DicSummedEvaluation[MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION];
                if (objSummedEvaluation != null)
                {
                    lstSummedEvaluation.Add(objSummedEvaluation);
                }
            }
            #endregion

            if (lstEvaluationInsertOrUpdate != null && lstEvaluationInsertOrUpdate.Count > 0)
            {
                EvaluationCommentsBusiness.InsertOrUpdatebyClass(schoolId, academicYear.AcademicYearID, classID, educationLevelID, semester, typeOfTeacher, lstEvaluationInsertOrUpdate);
            }
            if (lstSummedEvaluation != null && lstSummedEvaluation.Count > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("PartitionId", partitionNumber);
                dic.Add("SchoolId", schoolId);
                dic.Add("AcademicYearId", academicYear.AcademicYearID);
                dic.Add("ClassId", classID);
                dic.Add("EducationLevelId", educationLevelID);
                dic.Add("SemesterId", semester);
                dic.Add("IsRetest", false);//Co cap nhat diem thi lai
                SummedEvaluationBusiness.InsertOrUpdatebyClass(dic, lstSummedEvaluation);
            }

            // Tạo dữ liệu ghi log
            ClassProfile objCP = ClassProfileBusiness.Find(classID);
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,""},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,classID},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,"Import sổ TD CLGD GVBM"},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,classID},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,SMAS.Business.Common.GlobalConstants.ACTION_IMPORT},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,"Sổ TD CLGD (GVBM)"},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,"Import Sổ theo dõi CLGD (GVBM), " + objCP.DisplayName + ", Học kỳ " + semester + ", năm học " + academicYear.Year + "-" + (academicYear.Year + 1) + "."}
                };
            SetViewDataActionAudit(dicLog);

            return Json(new JsonMessage("Import dữ liệu thành công", "success"));
        }
        
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        private void GetDataFromFileExcel(IVTWorksheet sheet, List<PupilOfClassBO> lstPupilOfClass, AcademicYear objAcademicYear, int partitionId, int educationLevelId, int classId, int subjectId, int semester, int isCommenting, List<EvaluationCommentsReportBO> lstOutPut)
        {
            StringBuilder errMsg;

            Regex objHtml = new Regex(@"</?\w+((\s+\w+(\s*=\s*(?:""[^""]*""|'[^']*'|[^'"">\s]+))?)+\s*|\s*)/?>");
            bool IsMatch = false;
            EvaluationCommentsReportBO objEvaluation;
            EvaluationComments objEvaluationEducation = null;
            EvaluationComments objEvaluationCapacities = null;
            EvaluationComments objEvaluationQualities = null;
            IDictionary<int, EvaluationComments> dicEvaluation;
            IDictionary<int, SummedEvaluation> dicSummedEvaluation;
            SummedEvaluation objSummedEvaluationEducation = null;
            List<string> lstPupilCode = new List<string>();
            bool checkDuplicate = false;
            bool pass = true;
            int comment = 0;
            int mark;
            int startRow = MarkRecordTeacherExcelConstants.ROW_START;

            //for qua file excel
            int startColumn = MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION;
            int endColumn = 0;
            if (isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
            {
                endColumn = MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION;
            }
            else
            {
                endColumn = MarkRecordTeacherExcelConstants.COLUMN_END_MARK;
            }
            string pupilCode;
            while (sheet.GetCellValue(startRow, 1) != null ||
                   sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_CODE) != null ||
                   sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_NAME) != null ||
                   sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_BIRTHDAY) != null)
            {
                errMsg = new StringBuilder();
                errMsg.Append("Sheet: " + sheet.Name);
                pass = true;
                pupilCode = (string)sheet.GetCellValue(startRow, 2);
                objEvaluation = new EvaluationCommentsReportBO();
                if (lstPupilCode.Count() == 0)
                {
                    // add vao list ma hoc sinh:
                    lstPupilCode.Add(pupilCode);
                }
                else
                {
                    if (lstPupilCode.Contains(pupilCode))
                    {
                        checkDuplicate = true;
                    }
                    else
                    {
                        checkDuplicate = false;
                        lstPupilCode.Add(pupilCode);
                    }
                }
                var poc = lstPupilOfClass.SingleOrDefault(u => u.PupilCode == pupilCode);
                if (poc == null)
                {
                    objEvaluation.MessageError = "- " + Res.Get("MarkRecord_Label_PupilIDError") + "\r\n";
                    pass = false;
                }
                else
                {
                    objEvaluation.PupilID = poc.PupilID;
                    objEvaluation.Status = poc.Status;
                }

                // Check trùng dữ liệu:
                if (checkDuplicate)
                {
                    pass = false;
                    objEvaluation.MessageError = String.Format("- Dòng {0} cột {1}: trùng mã học sinh.\r\n", startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_CODE);
                }
                objEvaluation.PupilCode = pupilCode;
                //Kiem tra cot ho va ten hoc sinh
                string pupilname = (string)sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_NAME);
                if (poc != null && !poc.PupilFullName.Equals(pupilname, StringComparison.InvariantCultureIgnoreCase))
                {
                    objEvaluation.MessageError = String.Format("- Dòng {0} cột {1}" + Res.Get("MarkRecord_Label_NameError") + "\r\n", startRow, MarkRecordTeacherExcelConstants.COLUMN_PULIL_NAME);
                    pass = false;
                }

                objEvaluation.FullName = pupilname;
                //Mon hoc va hoat dong giao duc
                objEvaluationEducation = new EvaluationComments
                {
                    AcademicYearID = objAcademicYear.AcademicYearID,
                    ClassID = classId,
                    CommentsM1M6 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION) == null) ? "" :
                                                     sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_EDUCATION).ToString().Trim(),
                    CommentsM2M7 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_EDUCATION) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_EDUCATION).ToString().Trim(),
                    CommentsM3M8 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_EDUCATION) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_EDUCATION).ToString().Trim(),
                    CommentsM4M9 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_EDUCATION).ToString().Trim(),
                    CommentsM5M10 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_EDUCATION).ToString().Trim(),
                    CreateTime = DateTime.Now,
                    EducationLevelID = educationLevelId,
                    EvaluationCriteriaID = subjectId,
                    EvaluationID = MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION,
                    LastDigitSchoolID = partitionId,
                    PupilID = poc.PupilID,
                    SchoolID = objAcademicYear.SchoolID,
                    SemesterID = semester,
                    TypeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM,
                    UpdateTime = DateTime.Now
                };

                //Nang luc
                objEvaluationCapacities = new EvaluationComments
                {
                    AcademicYearID = objAcademicYear.AcademicYearID,
                    ClassID = classId,
                    CommentsM1M6 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_CAPACITIES) == null) ? "" :
                                                     sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_CAPACITIES).ToString(),
                    CommentsM2M7 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_CAPACITIES) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_CAPACITIES).ToString(),
                    CommentsM3M8 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_CAPACITIES) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_CAPACITIES).ToString(),
                    CommentsM4M9 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_CAPACITIES) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_CAPACITIES).ToString(),
                    CommentsM5M10 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_CAPACITIES) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_CAPACITIES).ToString(),
                    CreateTime = DateTime.Now,
                    EducationLevelID = educationLevelId,
                    EvaluationCriteriaID = subjectId,
                    EvaluationID = MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES,
                    LastDigitSchoolID = partitionId,
                    PupilID = poc.PupilID,
                    SchoolID = objAcademicYear.SchoolID,
                    SemesterID = semester,
                    TypeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM,
                    UpdateTime = DateTime.Now
                };

                //Pham chat
                objEvaluationQualities = new EvaluationComments
                {
                    AcademicYearID = objAcademicYear.AcademicYearID,
                    ClassID = classId,
                    CommentsM1M6 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_QUALITIES) == null) ? "" :
                                                     sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_1_QUALITIES).ToString().Trim(),
                    CommentsM2M7 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_QUALITIES) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_2_QUALITIES).ToString().Trim(),
                    CommentsM3M8 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_QUALITIES) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_3_QUALITIES).ToString().Trim(),
                    CommentsM4M9 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_QUALITIES) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_4_QUALITIES).ToString().Trim(),
                    CommentsM5M10 = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_QUALITIES) == null) ? "" :
                                                      sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_MONTH_5_QUALITIES).ToString().Trim(),
                    CreateTime = DateTime.Now,
                    EducationLevelID = educationLevelId,
                    EvaluationCriteriaID = subjectId,
                    EvaluationID = MarkRecordOfPrimaryTeacherConstants.TAB_QUALITIES,
                    LastDigitSchoolID = partitionId,
                    PupilID = poc.PupilID,
                    SchoolID = objAcademicYear.SchoolID,
                    SemesterID = semester,
                    TypeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM,
                    UpdateTime = DateTime.Now
                };
                dicEvaluation = new Dictionary<int, EvaluationComments>();
                dicEvaluation.Add(MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION, objEvaluationEducation);
                dicEvaluation.Add(MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES, objEvaluationCapacities);
                dicEvaluation.Add(MarkRecordOfPrimaryTeacherConstants.TAB_QUALITIES, objEvaluationQualities);
                objEvaluation.DicEvaluation = dicEvaluation;

                dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                objSummedEvaluationEducation = new SummedEvaluation
                {
                    AcademicYearID = objAcademicYear.AcademicYearID,
                    ClassID = classId,
                    CreateTime = DateTime.Now,
                    EducationLevelID = educationLevelId,
                    EndingComments = (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING) == null) ? "" :
                                    sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING).ToString().Trim(),
                    EvaluationCriteriaID = subjectId,
                    EvaluationID = MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION,
                    LastDigitSchoolID = partitionId,
                    PupilID = poc.PupilID,
                    SchoolID = objAcademicYear.SchoolID,
                    SemesterID = semester,
                    UpdateTime = DateTime.Now
                };
                if (isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK) //Mon tinh diem
                {
                    if (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK) != null)
                    {
                        int.TryParse(sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK).ToString().Trim(), out mark);
                        objSummedEvaluationEducation.PeriodicEndingMark = mark;
                    }
                    else
                    {
                        objSummedEvaluationEducation.PeriodicEndingMark = null;
                    }

                    //Cot danh gia HT, CHT cho mon tinh diem
                    if (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION) != null)
                    {
                        if (MarkRecordOfPrimaryTeacherConstants.FINISH.Equals(sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION)))
                        {
                            objSummedEvaluationEducation.EndingEvaluation = MarkRecordOfPrimaryTeacherConstants.SHORTFINISH;
                        }
                        else if (MarkRecordOfPrimaryTeacherConstants.NOTFINISH.Equals(sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_ENDDING_EVALUATION)))
                        {
                            objSummedEvaluationEducation.EndingEvaluation = MarkRecordOfPrimaryTeacherConstants.SHORTNOTFINISH;
                        }
                    }
                    else
                    {
                        objSummedEvaluationEducation.EndingEvaluation = "";
                    }

                }
                else
                {
                    //Cot danh gia HT, CHT cho mon tinh nhan xet
                    if (sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK) != null)
                    {
                        if (MarkRecordOfPrimaryTeacherConstants.FINISH.Equals(sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK)))
                        {
                            objSummedEvaluationEducation.EndingEvaluation = MarkRecordOfPrimaryTeacherConstants.SHORTFINISH;
                        }
                        else if (MarkRecordOfPrimaryTeacherConstants.NOTFINISH.Equals(sheet.GetCellValue(startRow, MarkRecordTeacherExcelConstants.COLUMN_END_MARK)))
                        {
                            objSummedEvaluationEducation.EndingEvaluation = MarkRecordOfPrimaryTeacherConstants.SHORTNOTFINISH;
                        }
                    }
                    else
                    {
                        objSummedEvaluationEducation.EndingEvaluation = "";
                    }
                }
                dicSummedEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, objSummedEvaluationEducation);
                objEvaluation.DicSummedEvaluation = dicSummedEvaluation;

                //Kiem tra du lieu tung cot du lieu
                for (int i = startColumn; i <= endColumn; i++)
                {
                    comment++;
                    if (i == MarkRecordTeacherExcelConstants.COLUMN_END_MARK && isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        if (sheet.GetCellValue(startRow, i) != null)
                        {
                            if (int.TryParse(sheet.GetCellValue(startRow, i).ToString().Trim(), out mark))
                            {
                                //evaluationObj.PeriodicEndingMark = mark;
                                if (mark <= 0 || mark > 10)
                                {
                                    objEvaluation.MessageError += String.Format("- Dòng {0} cột {1}: Điểm kiểm tra định kỳ phải được nhập từ 1-10.\r\n", startRow, i);
                                    pass = false;
                                }
                            }
                            else
                            {
                                objEvaluation.MessageError += String.Format("- Dòng {0} cột {1}: Điểm kiểm tra định kỳ không phải là số.\r\n", startRow, i);
                                pass = false;
                            }
                        }
                    }
                    else if (sheet.GetCellValue(startRow, i) != null)
                    {
                        IsMatch = objHtml.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, i).ToString()));
                        if (IsMatch)
                        {
                            objEvaluation.MessageError += String.Format("- Dòng {0} cột {1}: Nội dung nhận xét chứa các ký tự đặc biệt.\r\n", startRow, i);
                            pass = false;
                        }
                        else if (i == MarkRecordTeacherExcelConstants.COLUMN_COMMENTS_ENDDING)
                        {
                            if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryTeacherConstants.LIMIT_COMMENTS)
                            {
                                objEvaluation.MessageError += String.Format("- Dòng {0} cột {1}: Số kí tự nhận xét cuối kỳ bé hơn hoặc bằng 500 kí tự.\r\n", startRow, i);
                                pass = false;
                            }
                        }
                        else
                        {
                            if (sheet.GetCellValue(startRow, i).ToString().Length > MarkRecordOfPrimaryTeacherConstants.LIMIT_COMMENTS)
                            {
                                objEvaluation.MessageError += String.Format("- Dòng {0} cột {1}: Số kí tự nhận xét tháng thứ " + (semester == 1 ? comment : comment + 5) + " bé hơn hoặc bằng 500 kí tự.\r\n", startRow, i);
                                pass = false;
                            }
                        }
                    }

                }
                if (!string.IsNullOrEmpty(objEvaluation.MessageError))
                {
                    errMsg.Append(objEvaluation.MessageError + "\r\n");
                    objEvaluation.MessageError = errMsg.ToString();
                }
                objEvaluation.EvaluationCriteriaID = subjectId;
                objEvaluation.IsCommenting = isCommenting;
                objEvaluation.Error = pass;
                lstOutPut.Add(objEvaluation);
                startRow++;
            }

        }

        private List<EvaluationGridModel> SearchEvaluation(int classID, int semester, int subjectID, int typeOfTeacher, int evalutionID)
        {
            List<EvaluationCommentsBO> listEvaluation = EvaluationCommentsBusiness.getListEvaluationCommentsBySchool(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classID, semester, subjectID, typeOfTeacher, evalutionID).ToList();
            List<EvaluationGridModel> listEvaluationResult = (from list in listEvaluation
                                                              select new EvaluationGridModel
                                                              {
                                                                  FullName = list.FullName,
                                                                  Name = list.Name,
                                                                  PupilID = list.PupilID,
                                                                  Comment1 = list.Comment1,
                                                                  Comment2 = list.Comment2,
                                                                  Comment3 = list.Comment3,
                                                                  Comment4 = list.Comment4,
                                                                  Comment5 = list.Comment5,
                                                                  MonthID_1 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryTeacherConstants.MONTH_1 : MarkRecordOfPrimaryTeacherConstants.MONTH_6),
                                                                  MonthID_2 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryTeacherConstants.MONTH_2 : MarkRecordOfPrimaryTeacherConstants.MONTH_7),
                                                                  MonthID_3 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryTeacherConstants.MONTH_3 : MarkRecordOfPrimaryTeacherConstants.MONTH_8),
                                                                  MonthID_4 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryTeacherConstants.MONTH_4 : MarkRecordOfPrimaryTeacherConstants.MONTH_9),
                                                                  MonthID_5 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? MarkRecordOfPrimaryTeacherConstants.MONTH_5 : MarkRecordOfPrimaryTeacherConstants.MONTH_10),
                                                                  PeriodicEndingMark = list.PiriodicEndingMark,
                                                                  EvaluationEnding = list.EvaluationEnding,
                                                                  RetestMark = list.RetestMark,
                                                                  EvaluationReTraining = list.EvaluationReTraining,
                                                                  Status = list.Status,
                                                                  EvaluationCommentsID = list.EvaluationCommentsID,
                                                                  CommentsEnding = list.CommentEnding,
                                                                  PupilCode = list.PupilCode,
                                                                  BirthDay = list.Birthday
                                                              }).ToList();
            return listEvaluationResult;
        }

        /// <summary>
        /// Lay thang theo hoc ki
        /// </summary>
        /// <param name="semesterID"></param>
        /// <returns></returns>
        private List<MonthComments> GetMonthBySemester(int semesterID)
        {
            List<MonthComments> monthList = new List<MonthComments>();
            if (semesterID == 1)
            {
                monthList = MonthCommentsBusiness.getMonthCommentsList().Where(p => (p.MonthID >= MarkRecordOfPrimaryTeacherConstants.MONTH_1 && p.MonthID <= MarkRecordOfPrimaryTeacherConstants.MONTH_5) || p.MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_11).ToList();
            }
            else
            {
                monthList = MonthCommentsBusiness.getMonthCommentsList().Where(p => (p.MonthID >= MarkRecordOfPrimaryTeacherConstants.MONTH_6 && p.MonthID <= MarkRecordOfPrimaryTeacherConstants.MONTH_10) || p.MonthID == MarkRecordOfPrimaryTeacherConstants.MONTH_11).ToList();
            }
            return monthList;
        }

        private int CheckSubjectCommenting(int subjectID, int ClassID)
        {
            int isCommenting = -1;
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            if (subjectID > 0 && ClassID > 0)
            {
                ClassSubject obj = ClassSubjectBusiness.All.Where(p => p.ClassID == ClassID && p.SubjectID == subjectID && p.Last2digitNumberSchool == partitionId).FirstOrDefault();
                if (obj != null)
                {
                    return obj.IsCommenting.HasValue ? obj.IsCommenting.Value : -1;
                }
            }
            return isCommenting;
        }

        private string ErrValidateFileExcel(IVTWorksheet sheet, int evaluationID, int semesterID, int classID, string className, AcademicYear objAcademicYear, int? subjectID = 0, string subjectName = "")
        {
            string ErrMessage = "";
            string Title = "";
            string semesterName = "";
            string academicYearName = objAcademicYear.DisplayTitle.Replace("-", "").Replace(" ", "");
            string TitleYear = "";
            if (semesterID == 1)
            {
                semesterName = "HOCKY1";
            }
            else if (semesterID == 2)
            {
                semesterName = "HOCKY2";
            }

            if (!String.IsNullOrEmpty(className))
            {
                //className = Utils.Utils.StripVNSign(ClassProfileBusiness.Find(classID).DisplayName.ToUpper().Replace(" ", ""));
                className = Utils.Utils.StripVNSignAndSpace(className).ToUpper();
            }
            if (sheet.GetCellValue(3, 1) != null)
            {
                Title = Utils.Utils.StripVNSign(sheet.GetCellValue(3, 1).ToString().Replace(" ", "")).ToUpper();
            }

            if (sheet.GetCellValue(4, 1) != null)
            {
                TitleYear = Utils.Utils.StripVNSign(sheet.GetCellValue(4, 1).ToString().Replace("-", "").Replace(" ", ""));
            }

            if (evaluationID == MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION)
            {
                if (!string.IsNullOrEmpty(subjectName))
                {
                    //string subjectName = Utils.Utils.StripVNSign(SubjectCatBusiness.Find(subjectID).SubjectName.ToUpper().Replace(" ", ""));
                    subjectName = subjectName.ToUpper().Replace(" ", "");
                }
                if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(TitleYear) || !Title.Contains("BANGTHEODOICHATLUONGGIAODUC"))
                {
                    return "File excel không đúng mẫu.";
                }
                else if (!Title.Contains(semesterName))
                {
                    return "Học kỳ trong file excel không đúng.";
                }
                else if (!Title.Contains(className))
                {
                    return "Lớp học trong file excel không đúng.";
                }
                else if (!Title.Contains(subjectName))
                {
                    return "Môn học trong file excel không đúng.";
                }
                else if (!TitleYear.Contains(academicYearName))
                {
                    return "Năm học trong file excel không đúng.";
                }
            }
            else if (evaluationID == MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES)
            {
                if (!Title.Contains(semesterName))
                {
                    return "Học kỳ trong file excel không đúng.";
                }
                else if (!Title.Contains(className))
                {
                    return "Lớp học trong file excel không đúng.";
                }
                else if (!TitleYear.Contains(academicYearName))
                {
                    return "Năm học trong file excel không đúng.";
                }
                else if (!Title.Contains("NANGLUC"))
                {
                    return "File excel không đúng mẫu.";
                }
            }
            else
            {
                if (!Title.Contains(semesterName))
                {
                    return "Học kỳ trong file excel không đúng.";
                }
                else if (!Title.Contains(className))
                {
                    return "Lớp học trong file excel không đúng.";
                }
                else if (!TitleYear.Contains(academicYearName))
                {
                    return "Năm học trong file excel không đúng.";
                }
            }

            return ErrMessage;
        }

        private void SetVisbileButton(int semester, int classID, int subjectID)
        {
            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVBM = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, classID, subjectID, semester);
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (!(isAdminShcool || isGVBM))
            {
                ViewData[MarkRecordOfPrimaryTeacherConstants.IS_VISIBLED_BUTTON] = false;
            }
            else if (isAdminShcool)
            {
                ViewData[MarkRecordOfPrimaryTeacherConstants.IS_VISIBLED_BUTTON] = true;
            }
            else if (isGVBM)
            {
                if (semester == 1)
                {
                    if (dateTimeNow < objAca.FirstSemesterStartDate.Value || dateTimeNow > objAca.FirstSemesterEndDate)
                    {
                        ViewData[MarkRecordOfPrimaryTeacherConstants.IS_VISIBLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[MarkRecordOfPrimaryTeacherConstants.IS_VISIBLED_BUTTON] = true;
                    }
                }
                else if (semester == 2)
                {
                    if (dateTimeNow < objAca.SecondSemesterStartDate.Value || dateTimeNow > objAca.SecondSemesterEndDate.Value)
                    {
                        ViewData[MarkRecordOfPrimaryTeacherConstants.IS_VISIBLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[MarkRecordOfPrimaryTeacherConstants.IS_VISIBLED_BUTTON] = true;
                    }
                }
            }
        }
        private string GetEvaluationName(int evaluationID)
        {
            string retName = "";
            switch (evaluationID)
            {
                case MarkRecordOfPrimaryTeacherConstants.TAB_EDUCATION:
                    retName = "Môn học và HÐGD GVBM";
                    break;
                case MarkRecordOfPrimaryTeacherConstants.TAB_CAPACTIES:
                    retName = "Năng lực GVBM";
                    break;
                case MarkRecordOfPrimaryTeacherConstants.TAB_QUALITIES:
                    retName = "Phẩm chất GVBM";
                    break;
            }
            return retName;
        }


        /// <summary>
        /// Dua cac thong tin vao log
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="evaluationID"></param>
        /// <param name="semester"></param>
        /// <param name="monthId"></param>
        /// <param name="action"></param>
        /// <param name="functionName"></param>
        private void SetLog(List<EvaluationComments> lstChange, List<SummedEvaluation> lstSummedEvaluation)
        {
            #region source cũ
            /*
            string monthName = "";
            string subjectName = "";
            if (monthId > 0)
            {
                MonthComments objMonth = MonthCommentsBusiness.Find(monthId);
                monthName = (objMonth == null) ? "" : objMonth.Name;
            }
            if (subjectId > 0)
            {
                SubjectCat objSubject = SubjectCatBusiness.Find(subjectId);
                subjectName = (objSubject == null) ? "" : objSubject.DisplayName;
            }
            ClassProfile objClass = ClassProfileBusiness.Find(classId);
            string evaluationName = GetEvaluationName(evaluationID);

            string desCription = "";
            if (action == SMAS.Business.Common.GlobalConstants.ACTION_ADD || action == SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)
            {
                desCription = String.Format("Lưu nhận xét đánh giá lớp: {0}, môn học:  {1}, học kỳ: {2}, tháng: {3}, mặt đánh giá: {4}", objClass.DisplayName, subjectName, semester, monthName, evaluationName);
            }
            else if (action == SMAS.Business.Common.GlobalConstants.ACTION_DELETE)
            {
                desCription = String.Format("Xóa nhận xét đánh giá lớp: {0}, môn học: {1}, học kỳ: {2}, tháng: {3}, mặt đánh giá: {4}", objClass.DisplayName, subjectName, semester, monthName, evaluationName);
            }
            else if (action == SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)
            {

                desCription = String.Format("Import nhận xét đánh giá lớp: {0}, học kỳ: {1}", objClass.DisplayName, semester);
            }
            string parameter = String.Format("Class_id:{0}, subject_id:{1} Semester:{2}, Thang:{3}, Tab:{4}", classId, subjectId, semester, monthId, evaluationID);
            SetViewDataActionAudit(String.Empty, String.Empty, classId.ToString(), desCription, parameter, "Sổ theo dõi CLGD GVBM", action, desCription);
            */
            #endregion
        }

        /// <summary>
        /// Lay danh sach mon hoc nguoi dung duoc phep import
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="TyOfTeacher"></param>
        /// <returns></returns>
        private List<ClassSubject> GetSubjectImport(int Semester, int ClassID)
        {
            List<ClassSubject> lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value,
                                                                                                   _globalInfo.SchoolID.Value, Semester, ClassID, MarkRecordOfPrimaryTeacherConstants.GVBM);
            return lsClassSubject;
        }
        [HttpPost]
        public PartialViewResult LoadCopyData(int ClassID,int SemesterID,int SubjectID,int EvaluationID)
        {
            List<SubjectCatBO> lstSCBO = (from sc in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value,
                                         _globalInfo.SchoolID.Value, SemesterID, ClassID, MarkRecordOfPrimaryTeacherConstants.GVBM)
                                          where sc.SubjectID != SubjectID
                                          select new SubjectCatBO
                                          {
                                              SubjectCatID = sc.SubjectCat.SubjectCatID,
                                              SubjectName = sc.SubjectCat.DisplayName
                                          }).ToList();
            ViewData[MarkRecordOfPrimaryTeacherConstants.LIST_SUBJECT] = lstSCBO;
            ViewData[MarkRecordOfPrimaryTeacherConstants.EVALUATION_ID] = EvaluationID;
            bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isSubjectTeacher = UtilsBusiness.HasSubjectTeacherPermission_BM(_globalInfo.UserAccountID, ClassID, SemesterID);
            ViewData[MarkRecordOfPrimaryTeacherConstants.IS_GVBM] = (!isSchoolAdmin && isSubjectTeacher);
            return PartialView("_CopyData");
        }

        [ActionAudit]
        public JsonResult CopyData(FormCollection frm)
        {
            string arrSubject = frm["ArrSubjectID"];
            int ObjectID = !string.IsNullOrEmpty(frm["ObjectID"]) ? Int32.Parse(frm["ObjectID"]) : 0;
            int LimitID = !string.IsNullOrEmpty(frm["LimitID"]) ? Int32.Parse(frm["LimitID"]) : 0;
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? Int32.Parse(frm["ClassID"]) : 0;
            int SemesterID = !string.IsNullOrEmpty(frm["Semester"]) ? Int32.Parse(frm["Semester"]) : 0;
            int EducationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? Int32.Parse(frm["EducationLevelID"]) : 0;
            int Month = !string.IsNullOrEmpty(frm["Month"]) ? Int32.Parse(frm["Month"]) : 0;
            int SubjectSelectID = !string.IsNullOrEmpty(frm["SubjectIDSelect"]) ? Int32.Parse(frm["SubjectIDSelect"]) : 0;
            int EvaluationID = !string.IsNullOrEmpty(frm["EvaluationID"]) ? Int32.Parse(frm["EvaluationID"]) : 0;
            int typeOfTeacher = MarkRecordOfPrimaryTeacherConstants.GVBM;
            #region check permision
            bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isSubjectTeacher = UtilsBusiness.HasSubjectTeacherPermission_BM(_globalInfo.UserAccountID, ClassID, SemesterID);
            if (!isSchoolAdmin && !isSubjectTeacher)
            {
                return Json(new JsonMessage("Thầy/cô không có quyền thao tác chức năng này"));
            }
            #endregion


            if (string.IsNullOrEmpty(arrSubject)) return Json(new { Type = "error", Message = "Thầy cô chưa chọn môn học để sao chép." });
            if (ObjectID != MarkRecordOfPrimaryTeacherConstants.COPY_OVER_WRITE_ALL_PUPIL
                && ObjectID != MarkRecordOfPrimaryTeacherConstants.COPY_OVER_WRITE_ALL_PUPIL_NOT_COMMENT
                && LimitID != MarkRecordOfPrimaryTeacherConstants.COPY_COMMNET_OF_MONTH
                && LimitID != MarkRecordOfPrimaryTeacherConstants.COPY_COMMNET_ALL_MONTH_IN_SEMESTER)
                return Json(new { Type = "error", Message = "Dữ liệu không hợp lệ." });

            List<int> listSubjectID = arrSubject.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolId",_globalInfo.SchoolID},
                {"AcademicYearId",_globalInfo.AcademicYearID},
                {"ClassId",ClassID},
                {"SemesterId",SemesterID},
                {"EvaluationId", EvaluationID},
                {"TypeOfTeacher", typeOfTeacher},
                {"EduationLevelId", EducationLevelID},
                {"listSubjectId", listSubjectID},
                {"Status", GlobalConstants.PUPIL_STATUS_STUDYING},                
                {"Month", Month},                
            };
            List<ActionAuditDataBO> lstActionAuditData = new List<ActionAuditDataBO>();
            EvaluationCommentsBusiness.CopyDataEvaluation(objDic, ObjectID, LimitID, Month, SubjectSelectID,ref lstActionAuditData);
            //luu log du lieu
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsmeterStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();

            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();

            ActionAuditDataBO objAC = null;
            for (int i = 0; i < lstActionAuditData.Count; i++)
            {
                objAC = lstActionAuditData[i];

                string tmpOld = string.Empty;
                string tmpNew = string.Empty;
                tmpOld = objAC.OldData.Length > 0 ? objAC.OldData.ToString().Substring(0, objAC.OldData.Length - 2) : "";
                oldObjectStrtmp.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                tmpNew = objAC.NewData.Length > 0 ? objAC.NewData.ToString().Substring(0, objAC.NewData.Length - 2) : "";
                newObjectStrtmp.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                userDescriptionsStrtmp.Append(objAC.UserDescription).Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");

                objectIDStr.Append(objAC.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                paramsmeterStr.Append(objAC.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                descriptionStr.Append(objAC.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userFuntionsStr.Append(objAC.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userActionsStr.Append(objAC.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                oldObjectStr.Append(oldObjectStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                newObjectStr.Append(newObjectStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                userDescriptionsStr.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);


                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
            }
            IDictionary<string,object> dicLogAction = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsmeterStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
            };
            SetViewDataActionAudit(dicLogAction);
            return Json(new JsonMessage("Sao chép thành công","success"));
        }
    }
}
