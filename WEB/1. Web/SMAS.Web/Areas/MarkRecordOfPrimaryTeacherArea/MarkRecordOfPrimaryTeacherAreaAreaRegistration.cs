﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea
{
    public class MarkRecordOfPrimaryTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkRecordOfPrimaryTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkRecordOfPrimaryTeacherArea_default",
                "MarkRecordOfPrimaryTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
