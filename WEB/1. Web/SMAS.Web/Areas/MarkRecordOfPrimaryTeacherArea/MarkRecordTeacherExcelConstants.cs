﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea
{
    public class MarkRecordTeacherExcelConstants
    {
        public const string TEMPLATE_FILE = "BangDanhGiaThangLopK1_1HK1_GVBM.xls";
        public const int COLUMN_TITLE = 5;
        public const int ROW_TILTE = 6;
        public const int ROW_START = 8;
        public const int COLUMN_START = 1;
        public const int COLUMN_PULIL_CODE = 2;
        public const int COLUMN_PULIL_NAME = 3;
        public const int COLUMN_BIRTHDAY = 4;
        public const int COLUMN_MONTH_1_EDUCATION = 5;
        public const int COLUMN_MONTH_1_CAPACITIES = 6;
        public const int COLUMN_MONTH_1_QUALITIES = 7;

        public const int COLUMN_MONTH_2_EDUCATION = 8;
        public const int COLUMN_MONTH_2_CAPACITIES = 9;
        public const int COLUMN_MONTH_2_QUALITIES = 10;

        public const int COLUMN_MONTH_3_EDUCATION = 11;
        public const int COLUMN_MONTH_3_CAPACITIES = 12;
        public const int COLUMN_MONTH_3_QUALITIES = 13;

        public const int COLUMN_MONTH_4_EDUCATION = 14;
        public const int COLUMN_MONTH_4_CAPACITIES = 15;
        public const int COLUMN_MONTH_4_QUALITIES = 16;

        public const int COLUMN_MONTH_5_EDUCATION = 17;
        public const int COLUMN_MONTH_5_CAPACITIES = 18;
        public const int COLUMN_MONTH_5_QUALITIES = 19;

        public const int COLUMN_COMMENTS_ENDDING = 20;
        public const int COLUMN_END_MARK = 21;
        public const int COLUMN_ENDDING_EVALUATION = 22;

    }
}