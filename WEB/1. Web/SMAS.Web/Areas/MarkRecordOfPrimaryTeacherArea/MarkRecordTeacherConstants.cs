﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.MarkRecordOfPrimaryTeacherArea
{
    public class MarkRecordOfPrimaryTeacherConstants
    {
        public const string LIST_MARKRECORD = "listMarkRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "ListEducationLevel";
        public const string LIST_CLASS = "ListClass";
        public const string LIST_SUBJECT = "ListSubject";
        public const string LIST_SUBJECT_IMPORT = "ListSubject_IMPORT";
        public const string LABEL_MES = "LabelMessage";
        public const string BOOL_TOAN = "BoolToan";
        public const string BOOL_MONKHAC = "BoolMonKhac";
        public const string BOOL_TIENGVIET = "BoolTiengViet";
        public const string BOOL_HKI = "BoolSemesterI";
        public const string BOOL_HKII = "BoolSemesterII";


        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";
        public const string DISABLED_BUTTON = "DISABLED_BUTTON";
        public const string LOCK_MARK_TITLE = "LOCK_MARK_TITLE";
        public const string ERROR_IMPORT = "ERROR_IMPORT";
        public const string IS_VISIBLED_BUTTON = "IsVisibledButton";

        public const string LIST_MONTH = "ListMonth";
        public const string LIST_PUPIL_EVALUATION = "ListPupilProfileEvaluation";
        public const string LIST_EVALUATION_CRITERIA = "ListEvaluationcriteria";
        public const string FINISH = "Hoàn thành";

        public const string NOTFINISH = "Chưa hoàn thành";  

        public const int GVBM = 1;
        public const int GVCN = 2;

        public const int MONTH_EVALUATION_ENDING = 11;

        public const string MONTH_SELECT = "MONTH_SELECT";
        public const int LIMIT_COMMENTS = 500;

        public const int MONTH_1 = 1;
        public const int MONTH_2 = 2;
        public const int MONTH_3 = 3;
        public const int MONTH_4 = 4;
        public const int MONTH_5 = 5;
        public const int MONTH_6 = 6;
        public const int MONTH_7 = 7;
        public const int MONTH_8 = 8;
        public const int MONTH_9 = 9;
        public const int MONTH_10 = 10;
        public const int MONTH_11 = 11;

        public const string SUBJECT_ID = "SubjectID";
        public const string CHECK_ISCOMMENTING = "CheckIsCommenting";
        public const string IS_CURENT_YEAR = "IsCurentYear";
        public const string SEMESTER_ID = "SemesterID";
        public const string CLASS_ID = "ClassId";
        public const string CLASS_NAME = "ClassName";
        public const string SUBJECT_NAME = "SubjectName";
        public const string EVALUATION_ID = "EvaluationID";
        public const string LIST_CLASS_SUBJECT = "lstClassSubject";
        public const string LIST_COMMENT = "lstComment";

        public const int TAB_EDUCATION = 1;
        public const int TAB_CAPACTIES = 2;
        public const int TAB_QUALITIES = 3;
        public const int FILE_UPLOAD_MAX_SIZE = 5120;//5MB

        public const string SHORTFINISH = "HT";
        public const string SHORTNOTFINISH = "CHT";

        public const int COPY_OVER_WRITE_ALL_PUPIL = 1;
        public const int COPY_OVER_WRITE_ALL_PUPIL_NOT_COMMENT = 2;
        public const int COPY_COMMNET_OF_MONTH = 1;
        public const int COPY_COMMNET_ALL_MONTH_IN_SEMESTER = 2;
        public const string IS_GVBM = "is_GVBM";
    }
}