﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.MarkRecordRetestArea.Models;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System.Globalization;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using System.Transactions;
using System.Configuration;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Web.Areas.MarkRecordRetestArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class MarkRecordRetestController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBu;
        private readonly IPupilRetestRegistrationBusiness PupilRetestRegistrationBu;
        private readonly ISummedUpRecordBusiness SummedUpRecordBu;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        public MarkRecordRetestController(IClassProfileBusiness classProfileBu, IPupilRetestRegistrationBusiness pupilRetestRegistrationBu,
            ISummedUpRecordBusiness summedUpRecordBu,
           ISummedUpRecordHistoryBusiness summedUpRecordHistoryBusiness,
            IClassSubjectBusiness classSubjectBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
           IAcademicYearBusiness academicYearBusiness,
            ISubjectCatBusiness subjectCatBusiness)
        {
            this.ClassProfileBu = classProfileBu;
            this.PupilRetestRegistrationBu = pupilRetestRegistrationBu;
            this.SummedUpRecordBu = summedUpRecordBu;
            this.SummedUpRecordHistoryBusiness = summedUpRecordHistoryBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo glo = new GlobalInfo();
            List<EducationLevel> lstEducationLevel = glo.EducationLevels;
            lstEducationLevel = lstEducationLevel.Where(o => o.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_NINTH && o.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_TWELFTH && o.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_FIFTH).ToList();
            ViewData[MarkRecordRetestContants.LIST_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eid)
        {
            if (eid <= 0) return Json(new List<ComboObject>());
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", eid);
            dicClass.Add("IsVNEN", true);

            // AnhVD 20140814
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsViewAll || _globalInfo.IsEmployeeManager) // nếu là QTHT: cho phép load ra tất cả các lớp
            {
                var lstClass = this.ClassProfileBu.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(o => o.OrderNumber).ThenBy(u => u.DisplayName).ToList()
                                                .Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName))
                                                .ToList();
                return Json(lstClass);
            }
            else // load ra lớp theo phân quyền giáo viên bộ môn hoặc giáo vụ có quyền giáo viên bộ môn hoac gvcn hoac giao vu co quyen gvcn
            {
                dicClass.Add("UserAccountID", _globalInfo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                var lstClass = this.ClassProfileBu.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(o => o.OrderNumber).ThenBy(u => u.DisplayName).ToList()
                                                    .Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName))
                                                    .ToList();
                return Json(lstClass);
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadSubject(int eid, int? cid)
        {
            var lstClass = this.LoadClassSubject(eid, cid)
                                        .Select(u => new
                                        {
                                            Label = u.SubjectCat.DisplayName,
                                            Value = u.SubjectID,
                                            cchecked = false,
                                            disabled = false,
                                            IsComment = u.IsCommenting,
                                            OrderInSubject = u.SubjectCat.OrderInSubject
                                        })
                                        .Distinct()
                                        .OrderBy(u => u.OrderInSubject).ThenBy(o => o.Label)
                                        .ToList();
            return Json(lstClass);
        }

        /// <summary>
        /// Load danh sach hoc sinh thi lai
        /// </summary>
        /// <param name="eid">EducationLevelID</param>
        /// <param name="cid">ClassID</param>
        /// <param name="sid">SubjectID</param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(int eid, int? cid, int sid)
        {
            bool isHeadTeacher = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, cid.HasValue ? cid.Value : 0);
            ViewData[MarkRecordRetestContants.ENABLE_BUTTON] = "false";
            if (isHeadTeacher)
            {
                ViewData[MarkRecordRetestContants.ENABLE_BUTTON] = "true";
            }
            if (!_globalInfo.IsAdminSchoolRole && (cid == null || cid == 0))
            {
                ViewData[MarkRecordRetestContants.ENABLE_BUTTON] = "false";
            }

            if (eid < 0 || sid <= 0 || !_globalInfo.AcademicYearID.HasValue || !_globalInfo.SchoolID.HasValue || !_globalInfo.AppliedLevel.HasValue)
                return PartialView("_gridMarkRecordRetest", null);
            
            //AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("EducationLevelID", eid);
            dic.Add("ClassID", cid);
            dic.Add("SubjectID", sid);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dic.Add("IsVNEN", true);

            // AnhVD 20140814 - Neu la Giao vien - Kiem tra chi lay lop theo phan cong
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
            }
            else 
            {
                dic["EmployeeID"] = _globalInfo.EmployeeID;
                List<int> ListClassID = ClassProfileBu.SearchByTeacherPermission(dic).Select(o => o.ClassID).ToList();
                if (ListClassID == null || ListClassID.Count == 0)
                {
                    ListClassID.Add(0); // Tìm với Ds rỗng
                }
                dic["ListClassID"] = ListClassID;
            }
            
            List<PupilRetestRegistrationSubjectBO> lstPupilRetest =PupilRetestRegistrationBu.GetListPupilRetestRegistrationBySubject(dic).ToList();
            return PartialView("_gridMarkRecordRetest", lstPupilRetest);
        }


        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_ADD)]
        [ValidateAntiForgeryToken]
        public JsonResult Create(MarkRecordRetestViewModel model, FormCollection form)
        {
            
            GlobalInfo glo = new GlobalInfo();
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            if (ModelState.IsValid && glo.AcademicYearID.HasValue && glo.SchoolID.HasValue && glo.AppliedLevel.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("AcademicYearID", glo.AcademicYearID.Value);
                dic.Add("SchoolID", glo.SchoolID.Value);
                dic.Add("EducationLevelID", model.EducationLevelID);
                dic.Add("ClassID", model.ClassID);
                dic.Add("SubjectID", model.SubjectID);
                dic.Add("AppliedLevel", glo.AppliedLevel.Value);
                dic.Add("IsVNEN", true);
                // AnhVD 20140814 - Neu la Giao vien - Kiem tra chi lay lop theo phan cong
                if (!glo.IsAdminSchoolRole && !glo.IsViewAll && !glo.IsEmployeeManager)
                {
                    dic["EmployeeID"] = glo.EmployeeID;
                    List<int> ListClassID = ClassProfileBu.SearchByTeacherPermission(dic).Select(o => o.ClassID).ToList();
                    dic["ListClassID"] = ListClassID;
                }
                IEnumerable<PupilRetestRegistrationSubjectBO> lstPupilRetest = this.PupilRetestRegistrationBu.GetListPupilRetestRegistrationBySubject(dic).ToList(); //Kiem tra lai ham search

                List<PupilRetestRegistrationSubjectBO> lstInsertOrUpdateSumup = new List<PupilRetestRegistrationSubjectBO>();

                //Chi lay cac hoc sinh duoc nhap diem thi lai
                lstPupilRetest = lstPupilRetest.Where(u => !"on".Equals(form.Get("chkAbsentContent_" + u.PupilRetestRegistrationID))).ToList();
                //lấy danh sách điểm thi lại theo lớp, môn:
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = glo.AcademicYearID;
                SearchInfo["ClassID"] = model.ClassID;
                SearchInfo["SubjectID"] = model.SubjectID;
                SearchInfo["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_RETEST;
                List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>(); // luu cac hoc sinh da duoc nhap diem thi lai
                List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                if (isMovedHistory)
                {
                    lstSummedUpRecordHistory = SummedUpRecordHistoryBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo).ToList();
                    lstSummedUpRecordHistory = lstSummedUpRecordHistory.Where(u => u.SummedUpRecordID == lstSummedUpRecordHistory.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.SummedUpRecordID)).ToList();
                }
                else
                {
                    lstSummedUpRecord = SummedUpRecordBu.SearchBySchool(glo.SchoolID.Value, SearchInfo).ToList();
                    lstSummedUpRecord = lstSummedUpRecord.Where(u => u.SummedUpRecordID == lstSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.SummedUpRecordID)).ToList();
                }

                //SummedUpRecordBu.DeleteAll(lstSummedUpRecord);
                //Update thông tin trong bảng PupilRetestRegistration, lấy những học sinh không chứa trong lstPupilRetest
                SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = glo.AcademicYearID;
                SearchInfo["EducationLevelID"] = model.EducationLevelID;
                SearchInfo["ClassID"] = model.ClassID;
                SearchInfo["SubjectID"] = model.SubjectID;
                // lstPupilRetestUpdate se chua cac hoc sinh bi xoa diem
                List<PupilRetestRegistration> lstPupilRetestUpdate = PupilRetestRegistrationBu.SearchBySchool(glo.SchoolID.Value, SearchInfo).ToList();
                lstPupilRetestUpdate = lstPupilRetestUpdate.Where(u => u.PupilRetestRegistrationID == lstPupilRetestUpdate.Where(v => v.PupilID == u.PupilID).Max(v => v.PupilRetestRegistrationID)).ToList();
                List<PupilRetestRegistration> lst2 = new List<PupilRetestRegistration>();
                string reason = string.Empty;
                PupilRetestRegistration objPRU = null;
                for (int i = 0; i < lstPupilRetestUpdate.Count; i++)
                {
                    objPRU = lstPupilRetestUpdate[i];
                    reason = string.Empty;
                    if ("on".Equals(form.Get("chkAbsentContent_" + objPRU.PupilRetestRegistrationID)))
                    {
                        objPRU.IsAbsentContest = true;
                        reason = form.Get("txtReason_" + objPRU.PupilRetestRegistrationID);
                    }
                    else
                    {
                        objPRU.IsAbsentContest = false;
                    }
                    objPRU.Reason = reason;
                }

                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (var item in lstPupilRetest)
                    {
                        string newMark = form.Get("txt_RetestMark_" + item.PupilRetestRegistrationID).Replace(".",",");
                        if (item.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon nhan xet
                        {
                            Business.Common.UtilsBusiness.CheckValidateMark(glo.AppliedLevel.Value, item.IsCommenting, newMark);
                            item.ReTestJudgement = newMark;
                            item.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_RETEST;
                        }
                        else
                        {
                            newMark = newMark.Replace(".", ",");
                            Business.Common.UtilsBusiness.CheckValidateMark(glo.AppliedLevel.Value, item.IsCommenting, newMark);
                            if (!string.IsNullOrEmpty(newMark))
                            {
                                item.ReTestMark = decimal.Parse(newMark);
                                item.ReTestMark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(item.ReTestMark.Value));    
                            }
                            else
                            {
                                item.ReTestMark = null;
                            }
                            item.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_RETEST;
                        }
                        //lstPupilRetestUpdate.RemoveAll(o => o.PupilID == item.PupilID);
                        lstInsertOrUpdateSumup.Add(item);
                    }
                    if (isMovedHistory) // Xu ly cac nam hoc truoc
                    {
                        List<SummedUpRecordHistory> lstSummedUpRecordHistoryDelete;
                        lstSummedUpRecordHistoryDelete = (from p in lstInsertOrUpdateSumup
                                                          join q in lstSummedUpRecordHistory on p.PupilID equals q.PupilID
                                                          select q).ToList();

                        // Xoa ban ghi diem thi lai cua cac hoc sinh khong duoc nhap diem thi lai
                        SummedUpRecordHistoryBusiness.DeleteAll(lstSummedUpRecordHistoryDelete);
                        // Them hoac update ban ghi diem thi lai cho cac hoc sinh duoc nhap diem thi lai
                        //chi cap nhap cho nhung HS co nhap diem thi lai
                        lstInsertOrUpdateSumup = lstInsertOrUpdateSumup.Where(p => p.ReTestMark.HasValue || !string.IsNullOrEmpty(p.ReTestJudgement)).ToList();
                        this.SummedUpRecordHistoryBusiness.InsertOrUpdateSummedUpRecordAfterRetest(glo.SchoolID.Value, glo.AcademicYearID.Value, lstInsertOrUpdateSumup);
                        this.SummedUpRecordHistoryBusiness.Save();
                    }
                    else
                    {
                        //Lấy danh sách các con điểm cần xoá trong bảng SummedUpRecord
                        List<SummedUpRecord> lstSummedUpRecordDelete;
                        lstSummedUpRecordDelete = (from p in lstInsertOrUpdateSumup
                                                   join q in lstSummedUpRecord on p.PupilID equals q.PupilID
                                                   select q).ToList();
                        // Xoa ban ghi diem thi lai cua cac hoc sinh khong duoc nhap diem thi lai
                        SummedUpRecordBu.DeleteAll(lstSummedUpRecordDelete);
                        // Them hoac update ban ghi diem thi lai cho cac hoc sinh duoc nhap diem thi lai
                        //chi cap nhap cho nhung HS co nhap diem thi lai
                        lstInsertOrUpdateSumup = lstInsertOrUpdateSumup.Where(p => p.ReTestMark.HasValue || !string.IsNullOrEmpty(p.ReTestJudgement)).ToList();
                        this.SummedUpRecordBu.InsertOrUpdateSummedUpRecordAfterRetest(glo.SchoolID.Value, glo.AcademicYearID.Value, lstInsertOrUpdateSumup);
                        // Luu cac thay doi
                        this.SummedUpRecordBu.Save();
                    }

                    // Cap nhap truong ModifiedDate trong bang PupilRetestRegistration thanh null
                    //lstPupilRetestUpdate.ForEach(pru => pru.ModifiedDate = null);
                    //this.PupilRetestRegistrationBu.Save();

                    //foreach (var pupilRetestRegistration in lstPupilRetestUpdate)
                    //{
                    //    pupilRetestRegistration.ModifiedDate = null; // AnhVD 20140707
                    //    PupilRetestRegistration prrInsert = new PupilRetestRegistration();
                    //    Utils.Utils.BindTo(pupilRetestRegistration, prrInsert);
                    //    PupilRetestRegistrationBu.Insert(prrInsert);
                    //}

                    scope.Complete();
                }
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));

            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter")));
            }
        }

        public FileResult ExportExcel(int EducationLevelID,int? ClassID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", "HS_THPT_DiemThiLai_Khoi10.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = null;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("EducationLevelID", EducationLevelID);
            dic.Add("ClassID", ClassID);
            dic.Add("IsVNEN", true);
            //dic.Add("SubjectID", sid);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
            }
            else
            {
                dic["EmployeeID"] = _globalInfo.EmployeeID;
                List<int> ListClassID = ClassProfileBu.SearchByTeacherPermission(dic).Select(o => o.ClassID).ToList();
                if (ListClassID == null || ListClassID.Count == 0)
                {
                    ListClassID.Add(0); // Tìm với Ds rỗng
                }
                dic["ListClassID"] = ListClassID;
            }            
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);            
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            List<PupilRetestRegistrationSubjectBO> lstPupilRetest = new List<PupilRetestRegistrationSubjectBO>();
            
            List<PupilRetestRegistrationSubjectBO> lsttmp = new List<PupilRetestRegistrationSubjectBO>();
            var lstClassSubject = (this.LoadClassSubject(EducationLevelID, ClassID)
                                        .Select(u => new
                                        {
                                            Label = u.SubjectCat.DisplayName,
                                            Value = u.SubjectID,
                                            OrderInSubject = u.SubjectCat.OrderInSubject
                                        }).Distinct().OrderBy(u => u.OrderInSubject).ThenBy(o => o.Label).ToList());
            List<int> lstSubjectID = lstClassSubject.Select(p => p.Value).ToList();
            dic.Add("ListSubjectID", lstSubjectID);
            lstPupilRetest = this.PupilRetestRegistrationBu.GetListPupilRetestRegistrationBySubject(dic).ToList();

            int subjectID = 0;
            for (int i = 0; i < lstSubjectID.Count; i++)
            {
                subjectID = lstSubjectID[i];
                sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                lsttmp = lstPupilRetest.Where(p => p.SubjectID == subjectID).ToList();
                this.FillDataExport(sheet, lsttmp, subjectID, EducationLevelID);
            }
            oBook.GetSheet(1).Delete();
            Stream excel = oBook.ToStream();
            // Fix chrome file type
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            ClassProfile objClassProfile = ClassProfileBu.Find(ClassID);

            string AppliedName = _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY ? "TH"
                                : _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY ? "THCS"
                                : _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY ? "THPT"
                                : "";
            string EducationName = ClassID > 0 ? ReportUtils.StripVNSign(objClassProfile.DisplayName.Replace(" ","")) : "Khoi" + EducationLevelID;

            string FileResult = string.Format(MarkRecordRetestContants.FILE_RESULT, AppliedName, EducationName);

            result.FileDownloadName = ReportUtils.StripVNSign(FileResult);
            return result;
        }

        private IQueryable<ClassSubject> LoadClassSubject(int eid, int? cid)
        {
            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            searchPOC["AppliedLevel"] = _globalInfo.AppliedLevel;
            searchPOC["AcademicYearID"] = _globalInfo.AcademicYearID;
            searchPOC["EducationLevelID"] = eid;
            searchPOC["ClassID"] = cid;

            List<int> ListClassID = new List<int>();
            if (cid > 0)
            {
                searchPOC["Check"] = "Check";
            }
            else // tim kiem theo khoi
            {
                searchPOC["CheckWithClass"] = "Check";

                // AnhVD 20140814 - Neu la Giao vien - Kiem tra chi lay lop theo phan cong
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsEmployeeManager)
                {
                    searchPOC["EmployeeID"] = _globalInfo.EmployeeID;
                    ListClassID = ClassProfileBu.SearchByTeacherPermission(searchPOC).Select(o => o.ClassID).ToList();
                    if (ListClassID != null && ListClassID.Count == 0)
                    {
                        ListClassID.Add(0); // Tìm với Ds rỗng
                    }
                    searchPOC["ListClassID"] = ListClassID;
                }
            }

            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchPOC);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("EducationLevelID", eid);
            dic.Add("ClassID", cid);
            dic.Add("ListClassID", ListClassID);
            dic.Add("IsVNEN", true);
            var lstPupilRetest = this.PupilRetestRegistrationBu.SearchBySchool(_globalInfo.SchoolID.Value, dic);


            lstPupilRetest = from su in lstPupilRetest
                             join poc in iqPupilOfClass on new { su.PupilID, su.ClassID } equals new { poc.PupilID, poc.ClassID }
                             where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                             select su;
            return ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(o => lstPupilRetest.Any(x => x.SubjectID == o.SubjectID && x.ClassID == o.ClassID));

        }
        private void FillDataExport(IVTWorksheet sheet, List<PupilRetestRegistrationSubjectBO> lstPupilRetest,int SubjectID,int EducationLevelID)
        {
            string SubjectName = SubjectCatBusiness.Find(SubjectID).DisplayName;
            //Fill thong tin
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            sheet.SetCellValue("A3", string.Format("ĐIỂM THI LẠI MÔN {0} KHỐI {1}", SubjectName.ToUpper(), EducationLevelID));
            sheet.SetCellValue("A4", "NĂM HỌC " + AcademicYearBusiness.Find(_globalInfo.AcademicYearID).DisplayTitle);

            int startRow = 7;
            PupilRetestRegistrationSubjectBO objPupilRetest = null;

            for (int i = 0; i < lstPupilRetest.Count; i++)
            {
                objPupilRetest = lstPupilRetest[i];
                sheet.SetCellValue(startRow, 1, i + 1);
                sheet.SetCellValue(startRow, 2, objPupilRetest.PupilCode);
                sheet.SetCellValue(startRow, 3, objPupilRetest.FullName);
                sheet.SetCellValue(startRow, 4, objPupilRetest.ClassName);
                sheet.SetCellValue(startRow, 5, objPupilRetest.IsCommenting == 1 ? objPupilRetest.JudgementResult : objPupilRetest.SummedUpMark.HasValue ? objPupilRetest.SummedUpMark.Value.ToString("0.#").Replace(",",".") : "");
                sheet.SetCellValue(startRow, 6, objPupilRetest.IsCommenting == 1 ? objPupilRetest.ReTestJudgement 
                                                : (objPupilRetest.ReTestMark.HasValue ? objPupilRetest.ReTestMark.Value.ToString("0.#").Replace(",",".") : ""));
                sheet.SetCellValue(startRow, 7, objPupilRetest.isAbsentContest.HasValue && objPupilRetest.isAbsentContest.Value ? "Có" : "");
                sheet.GetRange(startRow, 7, startRow, 7).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(startRow, 8, objPupilRetest.Reason);
                sheet.GetRange(startRow, 8, startRow, 8).WrapText();
                sheet.GetRange(startRow, 5, startRow, 8).FillColor(System.Drawing.Color.Yellow);
                startRow++;
            }
            startRow = 7;
            sheet.Name = SubjectName;
            sheet.SetFontName("Times New Roman", 12);
            sheet.GetRange(startRow, 1, startRow + lstPupilRetest.Count - 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
        }
    }
}
