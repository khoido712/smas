﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkRecordRetestArea
{
    public class MarkRecordRetestAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkRecordRetestArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkRecordRetestArea_default",
                "MarkRecordRetestArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
