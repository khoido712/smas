﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkRecordRetestArea
{
    public class MarkRecordRetestContants
    {
        public const string LIST_EDUCATION_LEVEL = "List_Education_Level";

        public const string LIST_CLASS_PROFILE = "List_Class_Profile";

        public const string LIST_SUBJECT_CAT = "List_Subject_Cat";
        public const string ENABLE_BUTTON = "ENABLE_BUTTON";

        public const string FILE_RESULT = "HS_{0}_DiemThiLai_{1}.xls";
    }
}