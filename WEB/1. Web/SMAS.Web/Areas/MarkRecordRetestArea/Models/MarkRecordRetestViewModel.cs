using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Areas.PupilRankingPeriodArea;

namespace SMAS.Web.Areas.MarkRecordRetestArea.Models
{
    public class MarkRecordRetestViewModel
    {
        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("ComboBox")]
        [AdditionalMetadata("ViewDataKey", MarkRecordRetestContants.LIST_EDUCATION_LEVEL)]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("ClassProfile_Label_AllTitle")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? ClassID {get;set;}

        [ResourceDisplayName("Candidate_Label_ExaminationSubject")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SubjectID {get;set;}
    }
}
