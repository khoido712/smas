﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.WeeklyMealMenuArea.Models
{
    public class ListViewModel
    {
        public int MealCategoryID { get; set; }
        public string MealCategoryName { get; set; }
        public int DishesNum { get; set; }
        public List<DailyDishViewModel> ListDishesMon { get; set; }
        public List<DailyDishViewModel> ListDishesTue { get; set; }
        public List<DailyDishViewModel> ListDishesWed { get; set; }
        public List<DailyDishViewModel> ListDishesThu { get; set; }
        public List<DailyDishViewModel> ListDishesFri { get; set; }
        public List<DailyDishViewModel> ListDishesSat { get; set; }
    }
}