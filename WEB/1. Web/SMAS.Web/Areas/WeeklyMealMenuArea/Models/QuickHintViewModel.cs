﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.WeeklyMealMenuArea.Models
{
    public class QuickHintViewModel
    {
        public int QuickHintID { get; set; }
        public int UserAccountID { get; set; }
        public string HintCode { get; set; }
        public string Content { get; set; }
        public bool IsChecked { get; set; }
    }
}