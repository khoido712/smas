﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.WeeklyMealMenuArea.Models
{
    public class WeekViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string ToolTip
        {
            get
            {
                return string.Format("Từ ngày {0} - {1}", this.FromDate, this.ToDate);
            }
        }
    }
}