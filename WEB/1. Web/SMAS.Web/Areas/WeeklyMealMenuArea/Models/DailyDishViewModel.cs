﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.WeeklyMealMenuArea.Models
{
    public class DailyDishViewModel
    {
        public int MealCategoryID { get; set; }
        public int WeeklyMealMenuID { get; set; }
        public string DishName { get; set; }
        public string DateOfMeal { get; set; }
        public int OrderNumber { get; set; }
    }
}