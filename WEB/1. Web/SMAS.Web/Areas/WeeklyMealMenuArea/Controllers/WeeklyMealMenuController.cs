﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.WeeklyMealMenuArea.Models;
using System.Globalization;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.WeeklyMealMenuArea.Controllers
{
    public class WeeklyMealMenuController:BaseController
    {
        #region Properties
        private readonly IMealCategoryBusiness MealCategoryBusiness;
        private readonly IWeeklyMealMenuBusiness WeeklyMealMenuBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IQuickHintBusiness QuickHintBusiness;
        #endregion

        #region Constructor
        public WeeklyMealMenuController(IMealCategoryBusiness MealCategoryBusiness, IWeeklyMealMenuBusiness WeeklyMealMenuBusiness, IClassProfileBusiness ClassProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IQuickHintBusiness QuickHintBusiness)
        {
            this.MealCategoryBusiness = MealCategoryBusiness;
            this.WeeklyMealMenuBusiness = WeeklyMealMenuBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.QuickHintBusiness = QuickHintBusiness;
        }
        #endregion

        #region Actions
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        [HttpPost]
        public PartialViewResult Search(int classId, string monthAndYear, int week)
        {
            SetViewDataPermission("WeeklyMealMenu", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            //Kiem tra quyen them, sua, xoa
            bool checkCreate = false;
            bool checkDelete = false;
            if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE]
                && _globalInfo.IsCurrentYear)
            {
                checkCreate = true;
            }

            if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE]
                && _globalInfo.IsCurrentYear)
            {
                checkDelete = true;
            }

            ViewData["CheckCreate"] = checkCreate;
            ViewData["CheckDelete"] = checkDelete;

            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);
            //Lay ra danh sach bua an
            List<MealCategory> lstMealCategory = this.MealCategoryBusiness.GetMealCategoriesBySchool(_globalInfo.SchoolID.Value);

            //Danh sach thuc don
            DateTime fromDate;
            DateTime toDate;
            this.GetDateRangeOfWeek(year, month, week, out fromDate, out toDate);

            List<ListViewModel> lstModel = this._Search(classId, monthAndYear, week);

            List<KeyValuePair<string, string>> lstDayOfWeek = new List<KeyValuePair<string, string>>();
            for (DateTime date = fromDate; DateTime.Compare(date, toDate) <= 0; date = date.AddDays(1))
            {
                KeyValuePair<string, string> kvp = new KeyValuePair<string, string>(GetDayOfWeekString(date.DayOfWeek), date.ToString("dd/MM/yyyy"));
                lstDayOfWeek.Add(kvp);
            }
            ViewData[WeeklyMealMenuConstants.LIST_DAY_OF_WEEK] = lstDayOfWeek;
            ViewData[WeeklyMealMenuConstants.CURRENT_CLASS_ID] = classId;

            //Lay du lieu hint
            List<QuickHint> lstCommentAutocomplete = this.QuickHintBusiness.GetListQuickHintByUserAccount(_globalInfo.UserAccountID, "WeeklyMealMenu");
            ViewData[WeeklyMealMenuConstants.LIST_AUTOCOMPLETE] = lstCommentAutocomplete;

            return PartialView("_List", lstModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(List<ListViewModel> lstModel, int classId, List<int> lstCheckClasses, bool applyMultiClasses, string listIdAndOrder)
        {
            if (GetMenupermission("WeeklyMealMenu", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            List<int> lstClassId = applyMultiClasses ? lstCheckClasses : new List<int> { classId };

            var lstObjIdAndOrder = new List<KeyValuePair<int, int>>();

            if (!applyMultiClasses)
            {
                string[] arrListAndOrder = listIdAndOrder.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                List<string> lstIdAndOrder = arrListAndOrder.ToList();
                for (int i = 0; i < lstIdAndOrder.Count; i++)
                {
                    string[] idAndOrder = lstIdAndOrder[i].Split(new char[] { '_' });
                    int id = int.Parse(idAndOrder[0]);
                    int order = int.Parse(idAndOrder[1]);

                    lstObjIdAndOrder.Add(new KeyValuePair<int, int>(id, order));
                }
            }

            List<WeeklyMealMenu> lstInsert = new List<WeeklyMealMenu>();
            List<WeeklyMealMenu> lstUpdate = new List<WeeklyMealMenu>();
            List<WeeklyMealMenu> lstDelete = new List<WeeklyMealMenu>();

            for (int iClass = 0; iClass < lstClassId.Count; iClass++)
            {
                int curClassId = lstClassId[iClass];

                //Lay ra thuc don da nhap
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["ClassID"] = curClassId;

                List<WeeklyMealMenu> lstWmm = this.WeeklyMealMenuBusiness.Search(dic).ToList();

                for (int i = 0; i < lstModel.Count; i++)
                {
                    ListViewModel model = lstModel[i];

                    //Thu 2
                    this.SaveOneDay(curClassId, model.ListDishesMon.Where(o => applyMultiClasses || lstObjIdAndOrder.Any(u => model.MealCategoryID == u.Key && o.OrderNumber == u.Value)).ToList(), model, lstWmm, lstInsert, lstUpdate, lstDelete);

                    //Thu 3
                    this.SaveOneDay(curClassId, model.ListDishesTue.Where(o => applyMultiClasses || lstObjIdAndOrder.Any(u => model.MealCategoryID == u.Key && o.OrderNumber == u.Value)).ToList(), model, lstWmm, lstInsert, lstUpdate, lstDelete);

                    //Thu 4
                    this.SaveOneDay(curClassId, model.ListDishesWed.Where(o => applyMultiClasses || lstObjIdAndOrder.Any(u => model.MealCategoryID == u.Key && o.OrderNumber == u.Value)).ToList(), model, lstWmm, lstInsert, lstUpdate, lstDelete);

                    //Thu 5
                    this.SaveOneDay(curClassId, model.ListDishesThu.Where(o => applyMultiClasses || lstObjIdAndOrder.Any(u => model.MealCategoryID == u.Key && o.OrderNumber == u.Value)).ToList(), model, lstWmm, lstInsert, lstUpdate, lstDelete);

                    //Thu 6
                    this.SaveOneDay(curClassId, model.ListDishesFri.Where(o => applyMultiClasses || lstObjIdAndOrder.Any(u => model.MealCategoryID == u.Key && o.OrderNumber == u.Value)).ToList(), model, lstWmm, lstInsert, lstUpdate, lstDelete);

                    //Thu 7
                    this.SaveOneDay(curClassId, model.ListDishesSat.Where(o => applyMultiClasses || lstObjIdAndOrder.Any(u => model.MealCategoryID == u.Key && o.OrderNumber == u.Value)).ToList(), model, lstWmm, lstInsert, lstUpdate, lstDelete);
                }
            }

            for (int i = 0; i < lstInsert.Count; i++)
            {
                this.WeeklyMealMenuBusiness.Insert(lstInsert[i]);
            }

            for (int i = 0; i < lstUpdate.Count; i++)
            {
                this.WeeklyMealMenuBusiness.Update(lstUpdate[i]);
            }

            this.WeeklyMealMenuBusiness.DeleteAll(lstDelete);

            this.WeeklyMealMenuBusiness.Save();

            return Json(new JsonMessage("Lưu thành công"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(string ListIdAndOrder, int classId, string monthAndYear, int week)
        {
            if (GetMenupermission("WeeklyMealMenu", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            string[] arrListAndOrder = ListIdAndOrder.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            List<string> listIdAndOrder = arrListAndOrder.ToList();
            if (listIdAndOrder.Count == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn bữa ăn", "error"));
            }

            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);

            //Danh sach thuc don
            DateTime fromDate;
            DateTime toDate;
            this.GetDateRangeOfWeek(year, month, week, out fromDate, out toDate);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = classId;
            dic["DateFrom"] = fromDate;
            dic["DateTo"] = toDate;

            List<WeeklyMealMenu> lstWmm = this.WeeklyMealMenuBusiness.Search(dic).ToList();

            for (int i = 0; i < listIdAndOrder.Count; i++)
            {
                string[] idAndOrder = listIdAndOrder[i].Split(new char[] { '_' });
                int id = int.Parse(idAndOrder[0]);
                int order = int.Parse(idAndOrder[1]);

                List<WeeklyMealMenu> lstWmmByCat = lstWmm.Where(o => o.MealCategoryID == id && o.OrderNumber == order).ToList();

                this.WeeklyMealMenuBusiness.DeleteAll(lstWmmByCat);
            }

            this.WeeklyMealMenuBusiness.Save();

            return Json(new JsonMessage("Xóa thành công"));
        }

        public PartialViewResult OpenApplyClassesPopup(int classId, string monthAndYear, int week)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["SchoolID"] = _globalInfo.SchoolID;

            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                //dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }

            List<ClassProfile> lstCp = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            ViewData[WeeklyMealMenuConstants.LIST_CLASS] = lstCp;

            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);
            DateTime fromDate;
            DateTime toDate;
            this.GetDateRangeOfWeek(year, month, week, out fromDate, out toDate);
            ViewData[WeeklyMealMenuConstants.FROM_DATE] = fromDate.ToString("dd/MM/yyyy");
            ViewData[WeeklyMealMenuConstants.TO_DATE] = toDate.ToString("dd/MM/yyyy");
            ViewData[WeeklyMealMenuConstants.CURRENT_CLASS_ID] = classId;

            return PartialView("_ApplyClasses");
        }

        public PartialViewResult OpenQuickHintPopup()
        {
            List<QuickHintViewModel> lstQuickHint = this.QuickHintBusiness.GetListQuickHintByUserAccount(_globalInfo.UserAccountID, "WeeklyMealMenu")
                .Select(o => new QuickHintViewModel
                {
                    Content = o.Content,
                    HintCode = o.HintCode,
                    QuickHintID = o.QuickHintID,
                    UserAccountID = o.UserAccountID
                }).ToList();

            return PartialView("_QuickHint", lstQuickHint);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveQuickHint(List<QuickHintViewModel> listQuickHint)
        {
            if (GetMenupermission("WeeklyMealMenu", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            listQuickHint = listQuickHint.Where(o => o.IsChecked).ToList();

            if (listQuickHint.Count == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn món ăn", "error"));
            }

            if (listQuickHint.Where(o => string.IsNullOrWhiteSpace(o.HintCode)).Count() > 0)
            {
                return Json(new JsonMessage("Mã món ăn không được để trống", "error"));
            }

            if (listQuickHint.Where(o => string.IsNullOrWhiteSpace(o.Content)).Count() > 0)
            {
                return Json(new JsonMessage("Tên món ăn không được để trống", "error"));
            }

            //Xoa het mon an cu
            List<QuickHint> lstDelete = this.QuickHintBusiness.GetListQuickHintByUserAccount(_globalInfo.UserAccountID, "WeeklyMealMenu");
            this.QuickHintBusiness.DeleteAll(lstDelete);

            for (int i = 0; i < listQuickHint.Count; i++)
            {
                QuickHintViewModel model = listQuickHint[i];
                QuickHint q = new QuickHint();

                q.Content = model.Content.Trim();
                q.FunctionCode = "WeeklyMealMenu";
                q.HintCode = model.HintCode.Trim();
                q.IsSystemHint = false;
                q.UserAccountID = _globalInfo.UserAccountID;

                this.QuickHintBusiness.Insert(q);
            }

            this.QuickHintBusiness.Save();

            return Json(new JsonMessage("Lưu thành công"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteQuickHint(List<QuickHintViewModel> listQuickHint)
        {
            if (GetMenupermission("WeeklyMealMenu", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (listQuickHint==null || listQuickHint.Where(o => o.IsChecked).Count() == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn món ăn", "error"));
            }

            List<int> lstQuickHintID = listQuickHint.Where(o => o.IsChecked && o.QuickHintID != 0).Select(o => o.QuickHintID).ToList();
            List<QuickHint> lstDelete = this.QuickHintBusiness.GetListQuickHintByUserAccount(_globalInfo.UserAccountID, "WeeklyMealMenu").Where(o => lstQuickHintID.Contains(o.QuickHintID)).ToList();

            this.QuickHintBusiness.DeleteAll(lstDelete);
            this.QuickHintBusiness.Save();

            return Json(new JsonMessage("Xóa thành công"));
        }

        public FileResult Export(int classId, string monthAndYear, int week)
        {
            List<ListViewModel> lstModel = this._Search(classId, monthAndYear, week);
            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            //Danh sach thuc don
            DateTime fromDate;
            DateTime toDate;
            this.GetDateRangeOfWeek(year, month, week, out fromDate, out toDate);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/HT" + "/" + "ThucDonTuan.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet sheet = oBook.GetSheet(1);

            sheet.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            sheet.SetCellValue("A6", string.Format("Thời gian thực hiện: từ {0} - {1}", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy")));
            sheet.SetCellValue("A5", "THỰC ĐƠN TUẦN LỚP " + cp.DisplayName.ToUpper());

            List<KeyValuePair<string, string>> lstDayOfWeek = new List<KeyValuePair<string, string>>();
            for (DateTime date = fromDate; DateTime.Compare(date, toDate) <= 0; date = date.AddDays(1))
            {
                KeyValuePair<string, string> kvp = new KeyValuePair<string, string>(GetDayOfWeekString(date.DayOfWeek), date.ToString("dd/MM/yyyy"));
                lstDayOfWeek.Add(kvp);
            }
            //Fill header
            sheet.SetCellValue("C8", "Thứ 2\n" + lstDayOfWeek[0].Value);
            sheet.SetCellValue("D8", "Thứ 3\n" + lstDayOfWeek[1].Value);
            sheet.SetCellValue("E8", "Thứ 4\n" + lstDayOfWeek[2].Value);
            sheet.SetCellValue("F8", "Thứ 5\n" + lstDayOfWeek[3].Value);
            sheet.SetCellValue("G8", "Thứ 6\n" + lstDayOfWeek[4].Value);
            sheet.SetCellValue("H8", "Thứ 7\n" + lstDayOfWeek[5].Value);

            int startRow = 9;
            int lastRow = startRow + lstModel.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 8;
            int curColumn = startColumn;

            for (int i = 0; i < lstModel.Count; i++)
            {
                ListViewModel model = lstModel[i];

                for (int j = 0; j < model.DishesNum; j++)
                {
                    if (j == 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, i + 1);
                        curColumn++;

                        sheet.SetCellValue(curRow, curColumn, model.MealCategoryName);
                        curColumn++;
                    }
                    else
                    {
                        curColumn = curColumn + 2;
                    }
                    

                    sheet.SetCellValue(curRow, curColumn, model.ListDishesMon[j].DishName);
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, model.ListDishesTue[j].DishName);
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, model.ListDishesWed[j].DishName);
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, model.ListDishesThu[j].DishName);
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, model.ListDishesFri[j].DishName);
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, model.ListDishesSat[j].DishName);
                    curColumn++;

                    curColumn = startColumn;
                    curRow++;
                }

                sheet.GetRange(curRow - 1, 1, curRow - model.DishesNum, 1).Merge();
                sheet.GetRange(curRow - 1, 2, curRow - model.DishesNum, 2).Merge();
            }

            if (lstModel.Count > 0)
            {
                IVTRange globalRange = sheet.GetRange(startRow, startColumn, curRow - 1, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "ThucDonTuan.xls";

            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Import(IEnumerable<HttpPostedFileBase> attachment, int classId, string monthAndYear, int week)
        {
            if (GetMenupermission("WeeklyMealMenu", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);
            //Lay ra danh sach bua an
            List<MealCategory> lstMealCategory = this.MealCategoryBusiness.GetMealCategoriesBySchool(_globalInfo.SchoolID.Value);

            if (attachment == null || attachment.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachment.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                List<QuickHint> lstQuickHint = new List<QuickHint>();

                int startRow = 9;
                if (sheet.GetCellValue(startRow - 1, 1) != null && sheet.GetCellValue(startRow - 1, 2) != null)
                {
                    if (!sheet.GetCellValue(startRow - 1, 1).Equals("TT") || !sheet.GetCellValue(startRow - 1, 2).Equals("Bữa ăn"))
                    {
                        return Json(new JsonMessage("File import không đúng mẫu", "error"));
                    }
                }
                else
                {
                    return Json(new JsonMessage("File import không đúng mẫu", "error"));
                }

                //Kiem tra tên bữa ăn và số lượng các bữa ăn
                int index = 9;
                for (int i = 0; i < lstMealCategory.Count; i++)
                {
                    MealCategory mc = lstMealCategory[i];
                    if (!sheet.GetCellValue(index, 2).Equals(mc.MealName))
                    {
                        return Json(new JsonMessage("Thông tin bữa ăn không hợp lệ", "error"));
                    }
                    index = index + mc.DishesNum;
                }

                //Xoa het du lieu cu
                DateTime fromDate;
                DateTime toDate;
                this.GetDateRangeOfWeek(year, month, week, out fromDate, out toDate);

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["ClassID"] = classId;
                dic["DateFrom"] = fromDate;
                dic["DateTo"] = toDate;

                List<WeeklyMealMenu> lstWmm = this.WeeklyMealMenuBusiness.Search(dic).ToList();

                this.WeeklyMealMenuBusiness.DeleteAll(lstWmm);

                //Doc du lieu
                int curRow = 9;
                List<WeeklyMealMenu> lstInsert = new List<WeeklyMealMenu>();
                for (int i = 0; i < lstMealCategory.Count; i++)
                {
                    MealCategory mc = lstMealCategory[i];
                    
                    for (int j = 0; j < mc.DishesNum; j++)
                    {
                        List<string> lstDishes = new List<string>();
                        lstDishes.Add(sheet.GetCellValue(curRow, 3) != null ? sheet.GetCellValue(curRow, 3).ToString() : string.Empty);
                        lstDishes.Add(sheet.GetCellValue(curRow, 4) != null ? sheet.GetCellValue(curRow, 4).ToString() : string.Empty);
                        lstDishes.Add(sheet.GetCellValue(curRow, 5) != null ? sheet.GetCellValue(curRow, 5).ToString() : string.Empty);
                        lstDishes.Add(sheet.GetCellValue(curRow, 6) != null ? sheet.GetCellValue(curRow, 6).ToString() : string.Empty);
                        lstDishes.Add(sheet.GetCellValue(curRow, 7) != null ? sheet.GetCellValue(curRow, 7).ToString() : string.Empty);
                        lstDishes.Add(sheet.GetCellValue(curRow, 8) != null ? sheet.GetCellValue(curRow, 8).ToString() : string.Empty);

                        int iDay = 0;
                        for (DateTime date = fromDate; DateTime.Compare(date, toDate) <= 0; date = date.AddDays(1))
                        {
                            WeeklyMealMenu wmm = this.SaveOneDay(classId, date, lstDishes[iDay++], mc.MealCategoryID, j + 1);

                            if (!string.IsNullOrEmpty(wmm.DishName))
                            {
                                lstInsert.Add(wmm);
                            }
                        }

                        curRow++;
                    }
                }

                for (int i = 0; i < lstInsert.Count; i++)
                {
                    this.WeeklyMealMenuBusiness.Insert(lstInsert[i]);
                }

                this.QuickHintBusiness.Save();
            }
            return Json(new JsonMessage("Import thành công", "success"));
        }

        public FileResult ExportQuickHint()
        {
            List<QuickHint> lstQuickHint = this.QuickHintBusiness.GetListQuickHintByUserAccount(_globalInfo.UserAccountID, "WeeklyMealMenu").ToList();

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/HT" + "/" + "NganHangMonAn.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            firstSheet.SetCellValue("A2", _globalInfo.SchoolName);

            int startRow = 8;
            int lastRow = startRow + lstQuickHint.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 3;
            int curColumn = startColumn;

            for (int i = 0; i < lstQuickHint.Count; i++)
            {
                QuickHint q = lstQuickHint[i];

                firstSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;

                firstSheet.SetCellValue(curRow, curColumn, q.HintCode);
                curColumn++;

                firstSheet.SetCellValue(curRow, curColumn, q.Content);
                curColumn++;

                curRow++;
                curColumn = startColumn;
            }

            if (lstQuickHint.Count > 0)
            {
                IVTRange globalRange = firstSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "NganHangMonAn.xls";

            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportQuickHint(IEnumerable<HttpPostedFileBase> attachmentQ)
        {
            if (GetMenupermission("WeeklyMealMenu", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (attachmentQ == null || attachmentQ.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachmentQ.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                List<QuickHint> lstQuickHint = new List<QuickHint>();

                int startRow = 8;
                if (sheet.GetCellValue(startRow - 1, 1) != null && sheet.GetCellValue(startRow - 1, 2) != null && sheet.GetCellValue(startRow - 1, 3) != null)
                {
                    if (!sheet.GetCellValue(startRow - 1, 1).Equals("STT") || !sheet.GetCellValue(startRow - 1, 2).Equals("Mã") || !sheet.GetCellValue(startRow - 1, 3).Equals("Món ăn"))
                    {
                        return Json(new JsonMessage("File import không đúng mẫu", "error"));
                    }
                }
                else
                {
                    return Json(new JsonMessage("File import không đúng mẫu", "error"));
                }

                //Xoa het mon an cu
                List<QuickHint> lstDelete = this.QuickHintBusiness.GetListQuickHintByUserAccount(_globalInfo.UserAccountID, "WeeklyMealMenu");
                this.QuickHintBusiness.DeleteAll(lstDelete);

                while (sheet.GetCellValue(startRow, 1) != null && sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
                {
                    QuickHint q = new QuickHint();

                    //Kiem tra du lieu
                    string code = sheet.GetCellValue(startRow, 2).ToString().Trim();
                    if (string.IsNullOrEmpty(code))
                    {
                        continue;
                    }

                    string content = sheet.GetCellValue(startRow, 3).ToString().Trim();
                    if (string.IsNullOrEmpty(content))
                    {
                        continue;
                    }

                    q.Content = content;
                    q.FunctionCode = "WeeklyMealMenu";
                    q.HintCode = code;
                    q.IsSystemHint = false;
                    q.UserAccountID = _globalInfo.UserAccountID;

                    this.QuickHintBusiness.Insert(q);

                    startRow++;
                }

                this.QuickHintBusiness.Save();
            }
            return Json(new JsonMessage("Import thành công", "success"));
        }

        public PartialViewResult AjaxLoadWeek(string monthAndYear, int week)
        {
            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);

            List<WeekViewModel> lstWeek = this.GetListWeek(month, year);
            ViewData[WeeklyMealMenuConstants.LIST_WEEK] = lstWeek;
            ViewData[WeeklyMealMenuConstants.DEFAULT_WEEK] = week;

            return PartialView("_ViewWeek", lstWeek);
        }
        #endregion

        #region Private methods
        private List<ListViewModel> _Search(int classId, string monthAndYear, int week)
        {
            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);
            //Lay ra danh sach bua an
            List<MealCategory> lstMealCategory = this.MealCategoryBusiness.GetMealCategoriesBySchool(_globalInfo.SchoolID.Value);

            //Danh sach thuc don
            DateTime fromDate;
            DateTime toDate;
            this.GetDateRangeOfWeek(year, month, week, out fromDate, out toDate);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = classId;
            dic["DateFrom"] = fromDate;
            dic["DateTo"] = toDate;

            List<WeeklyMealMenu> lstWmm = this.WeeklyMealMenuBusiness.Search(dic).ToList();

            List<ListViewModel> lstModel = new List<ListViewModel>();
            for (int i = 0; i < lstMealCategory.Count; i++)
            {
                MealCategory mc = lstMealCategory[i];
                ListViewModel model = new ListViewModel();
                lstModel.Add(model);

                model.MealCategoryID = mc.MealCategoryID;
                model.MealCategoryName = mc.MealName;
                model.DishesNum = mc.DishesNum;
                model.ListDishesMon = new List<DailyDishViewModel>();
                model.ListDishesTue = new List<DailyDishViewModel>();
                model.ListDishesWed = new List<DailyDishViewModel>();
                model.ListDishesThu = new List<DailyDishViewModel>();
                model.ListDishesFri = new List<DailyDishViewModel>();
                model.ListDishesSat = new List<DailyDishViewModel>();

                int dishesNum = mc.DishesNum;
                DailyDishViewModel dailyModel;
                for (int j = 0; j < dishesNum; j++)
                {
                    int orderNumber = j + 1;

                    //Lay mon an cac tuan
                    List<WeeklyMealMenu> lstWmmByOrder = lstWmm.Where(o => o.MealCategoryID == mc.MealCategoryID && o.OrderNumber == orderNumber).ToList();

                    //Thu 2
                    dailyModel = this.GetDishOfDay(lstWmmByOrder, year, month, week, DayOfWeek.Monday, mc.MealCategoryID, orderNumber);
                    model.ListDishesMon.Add(dailyModel);

                    //Thu 3
                    dailyModel = this.GetDishOfDay(lstWmmByOrder, year, month, week, DayOfWeek.Tuesday, mc.MealCategoryID, orderNumber);
                    model.ListDishesTue.Add(dailyModel);

                    //Thu 4
                    dailyModel = this.GetDishOfDay(lstWmmByOrder, year, month, week, DayOfWeek.Wednesday, mc.MealCategoryID, orderNumber);
                    model.ListDishesWed.Add(dailyModel);

                    //Thu 5
                    dailyModel = this.GetDishOfDay(lstWmmByOrder, year, month, week, DayOfWeek.Thursday, mc.MealCategoryID, orderNumber);
                    model.ListDishesThu.Add(dailyModel);

                    //Thu 6
                    dailyModel = this.GetDishOfDay(lstWmmByOrder, year, month, week, DayOfWeek.Friday, mc.MealCategoryID, orderNumber);
                    model.ListDishesFri.Add(dailyModel);

                    //Thu 7
                    dailyModel = this.GetDishOfDay(lstWmmByOrder, year, month, week, DayOfWeek.Saturday, mc.MealCategoryID, orderNumber);
                    model.ListDishesSat.Add(dailyModel);
                }
            }

            return lstModel;
        }

        private void SetViewData()
        {
            //Lay danh sach lop
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["SchoolID"] = _globalInfo.SchoolID;

            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                //dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }

            List<ClassProfile> lstCp = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            ViewData[WeeklyMealMenuConstants.LIST_CLASS] = lstCp;

            //Lay danh sach thang
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime dateFrom = new DateTime(aca.FirstSemesterStartDate.Value.Year, aca.FirstSemesterStartDate.Value.Month, 1);
            DateTime dateTo = new DateTime(aca.SecondSemesterEndDate.Value.Year, aca.SecondSemesterEndDate.Value.Month, 1);

            List<DateTime> lstMonth = new List<DateTime>();
            List<KeyValuePair<string, string>> lstKVMonth = new List<KeyValuePair<string, string>>();

            for (DateTime d = dateFrom; DateTime.Compare(d, dateTo) <= 0; d = d.AddMonths(1))
            {
                KeyValuePair<string, string> kv = new KeyValuePair<string, string>(d.ToString("MM/yyyy"), "Tháng " + d.Month);
                lstKVMonth.Add(kv);
                lstMonth.Add(d);
            }

            //lstKVMonth.Reverse();
            ViewData[WeeklyMealMenuConstants.LIST_MONTH] = lstKVMonth;
            ViewData[WeeklyMealMenuConstants.DEFAULT_MONTH] = DateTime.Now.ToString("MM/yyyy");

            //Lay danh sach tuan
            DateTime curMonth = DateTime.Now;
            DateTime defaultMont = lstMonth.Any(o=>o.Month == curMonth.Month) ? curMonth : lstMonth[0];
            List<WeekViewModel> lstWeek = this.GetListWeek(curMonth.Month, curMonth.Year);
            ViewData[WeeklyMealMenuConstants.LIST_WEEK] = lstWeek;
            ViewData[WeeklyMealMenuConstants.DEFAULT_WEEK] = this.GetCurrentWeek();
        }

        private List<WeekViewModel> GetListWeek(int month, int year)
        {
            DateTime dateMonth = new DateTime(year, month, 1);
            int mondayNum = this.GetMondaysInMonth(dateMonth);
            List<WeekViewModel> lstWeek = new List<WeekViewModel>();
            DateTime firstMonday = dateMonth;
            while(firstMonday.DayOfWeek != DayOfWeek.Monday) firstMonday = firstMonday.AddDays(1); 

            for (int i = 0; i < mondayNum; i++)
            {
                WeekViewModel model = new WeekViewModel();
                model.Id = i + 1;
                model.Name = "Tuần " + (i + 1);
                model.FromDate = firstMonday.ToString("dd/MM/yyyy");
                model.ToDate = firstMonday.AddDays(5).ToString("dd/MM/yyyy");

                lstWeek.Add(model);
                firstMonday = firstMonday.AddDays(7);
            }

            return lstWeek;
        }

        private int GetMondaysInMonth(DateTime thisMonth)
        {
            int mondays = 0;
            int month = thisMonth.Month;
            int year = thisMonth.Year;
            int daysThisMonth = DateTime.DaysInMonth(year, month);
            DateTime beginingOfThisMonth = new DateTime(year, month, 1);
            for (int i = 0; i < daysThisMonth; i++)
                if (beginingOfThisMonth.AddDays(i).DayOfWeek == DayOfWeek.Monday)
                    mondays++;

            return mondays;
        }

        private void GetDateRangeOfWeek(int year, int month, int week, out DateTime fromDate, out DateTime toDate)
        {
            DateTime dateMonth = new DateTime(year, month, 1);
            DateTime firstMonday = dateMonth;
            while (firstMonday.DayOfWeek != DayOfWeek.Monday) firstMonday = firstMonday.AddDays(1);

            fromDate = new DateTime(year, month, firstMonday.Day + (7 * (week - 1)));
            toDate = fromDate.AddDays(5);
        }

        private DateTime GetDateByDayOfWeek(int year, int month, int week, DayOfWeek dayOfWeek)
        {
            DateTime fromDate;
            DateTime toDate;
            DateTime curDate;
            this.GetDateRangeOfWeek(year, month, week, out fromDate, out toDate);
            curDate = fromDate;

            while (curDate.DayOfWeek != dayOfWeek)
            {
                curDate = curDate.AddDays(1);
            }

            return curDate;

        }

        private int GetCurrentWeek()
        {
            int curMonth = DateTime.Now.Month;
            int curYear = DateTime.Now.Year;
            DateTime curDate = DateTime.Now.Date;

            List<WeekViewModel> lstWeek = this.GetListWeek(curMonth, curYear);
            for (int i = 0; i < lstWeek.Count; i++)
            {
                WeekViewModel week = lstWeek[i];
                DateTime fromDate, toDate;
                this.GetDateRangeOfWeek(curYear, curMonth, week.Id, out fromDate, out toDate);

                if (DateTime.Compare(curDate, fromDate.Date) >= 0 && DateTime.Compare(curDate, toDate.Date) <= 0)
                {
                    return week.Id;
                }
            }

            return 0;
        }

        private DailyDishViewModel GetDishOfDay(List<WeeklyMealMenu> lstWmm, int year, int month, int week, DayOfWeek dayOfWeek, int mealCategoryId, int orderNumber)
        {
            DailyDishViewModel model = new DailyDishViewModel();
            model.MealCategoryID = mealCategoryId;
            model.OrderNumber = orderNumber;
            DateTime date = this.GetDateByDayOfWeek(year, month, week, dayOfWeek);
            model.DateOfMeal = date.ToString("dd/MM/yyyy");
            WeeklyMealMenu wmm = lstWmm.FirstOrDefault(o => DateTime.Compare(o.DateOfMeal, date) == 0);

            if (wmm != null)
            {
                model.WeeklyMealMenuID = wmm.WeeklyMealMenuID;
                model.DishName = wmm.DishName;
            }
            return model;
        }

        private void SaveOneDay(int classId, List<DailyDishViewModel> lstDailyModel, ListViewModel model, List<WeeklyMealMenu> lstWmm, List<WeeklyMealMenu> lstInsert, List<WeeklyMealMenu> lstUpdate, List<WeeklyMealMenu> lstDelete)
        {
            for (int j = 0; j < lstDailyModel.Count; j++)
            {
                DailyDishViewModel dailyModel = lstDailyModel[j];

                WeeklyMealMenu wwm = lstWmm.FirstOrDefault(o => o.MealCategoryID == model.MealCategoryID && o.OrderNumber == dailyModel.OrderNumber && o.DateOfMeal.ToString("dd/MM/yyyy").Equals(dailyModel.DateOfMeal));
                if (wwm != null)
                {
                    if (!string.IsNullOrWhiteSpace(dailyModel.DishName))
                    {
                        wwm.DishName = dailyModel.DishName;
                        wwm.ModifiedDate = DateTime.Now.Date;

                        lstUpdate.Add(wwm);
                    }
                    else
                    {
                        lstDelete.Add(wwm);
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(dailyModel.DishName))
                    {
                        wwm = new WeeklyMealMenu
                        {
                            AcademicYearID = _globalInfo.AcademicYearID.Value,
                            ClassID = classId,
                            CreateDate = DateTime.Now,
                            DateOfMeal = DateTime.ParseExact(dailyModel.DateOfMeal, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                            DishName = dailyModel.DishName.Trim(),
                            Last2digitNumberSchool = _globalInfo.SchoolID.Value % 100,
                            MealCategoryID = model.MealCategoryID,
                            OrderNumber = dailyModel.OrderNumber,
                            SchoolID = _globalInfo.SchoolID.Value,
                        };

                        lstInsert.Add(wwm);
                    }
                }
            }
        }

        private WeeklyMealMenu SaveOneDay(int classId, DateTime date, string dishName, int mealCategoryId, int orderNumber)
        {
            WeeklyMealMenu wwm = new WeeklyMealMenu
            {
                AcademicYearID = _globalInfo.AcademicYearID.Value,
                ClassID = classId,
                CreateDate = DateTime.Now,
                DateOfMeal = date,
                DishName = dishName.Trim(),
                Last2digitNumberSchool = _globalInfo.SchoolID.Value % 100,
                MealCategoryID = mealCategoryId,
                OrderNumber = orderNumber,
                SchoolID = _globalInfo.SchoolID.Value,
            };

            return wwm;
        }

        private string GetDayOfWeekString(DayOfWeek dow)
        {
            switch (dow)
            {
                case DayOfWeek.Monday:
                    return "Thứ 2";
                case DayOfWeek.Tuesday:
                    return "Thứ 3";
                case DayOfWeek.Wednesday:
                    return "Thứ 4";
                case DayOfWeek.Thursday:
                    return "Thứ 5";
                case DayOfWeek.Friday:
                    return "Thứ 6";
                case DayOfWeek.Saturday:
                    return "Thứ 7";
                case DayOfWeek.Sunday:
                    return "Chủ nhật";
                default:
                    return string.Empty;

            }
        }
        #endregion
    }
}