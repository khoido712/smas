﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.WeeklyMealMenuArea
{
    public class WeeklyMealMenuConstants
    {
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_MONTH = "LIST_MONTH";
        public const string DEFAULT_MONTH = "DEFAULT_MONTH";
        public const string LIST_WEEK = "LIST_WEEK";
        public const string DEFAULT_WEEK = "DEFAULT_WEEK";
        public const string LIST_DAY_OF_WEEK = "LIST_DAY_OF_WEEK";
        public const string CURRENT_CLASS_ID = "CURRENT_CLASS_ID";
        public const string FROM_DATE = "FROM_DATE";
        public const string TO_DATE = "TO_DATE";
        public const string LIST_AUTOCOMPLETE = "LIST_AUTOCOMPLETE";
    }
}