﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.WeeklyMealMenuArea
{
    public class WeeklyMealMenuAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "WeeklyMealMenuArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "WeeklyMealMenuArea_default",
                "WeeklyMealMenuArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
