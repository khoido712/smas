/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.TypeOfDishArea.Models
{
    public class TypeOfDishViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 TypeOfDishID { get; set; }
        [ScaffoldColumn(false)]
		public System.DateTime CreatedDate { get; set; }
        [ScaffoldColumn(false)]
		public System.Boolean IsActive { get; set; }
        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }	
							
        [ResourceDisplayName("TypeOfDish_Label_TypeOfDishName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string TypeOfDishName { get; set; }

        [ResourceDisplayName("TypeOfDish_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [ResourceDisplayName("TypeOfDish_Label_Note")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }
    }
}


