﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.TypeOfDishArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.TypeOfDishArea.Controllers
{
    public class TypeOfDishController : BaseController
    {        
        private readonly ITypeOfDishBusiness TypeOfDishBusiness;
        private readonly IDishCatBusiness DishCatBusiness;
		
		public TypeOfDishController (ITypeOfDishBusiness typeofdishBusiness,IDishCatBusiness dishcatBusiness)
		{
			this.TypeOfDishBusiness = typeofdishBusiness;
            this.DishCatBusiness = dishcatBusiness;
		}

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            IEnumerable<TypeOfDishViewModel> lst = this._Search(SearchInfo);
            ViewData[TypeOfDishConstants.LIST_TYPEOFDISH] = lst.ToList();
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            if (frm.TypeOfDishName != null && frm.TypeOfDishName.Length > 300)
            {
                throw new BusinessException("TypeOfDish_Validate_TypeOfDishName");
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["TypeOfDishName"] = frm.TypeOfDishName;
            IEnumerable<TypeOfDishViewModel> lst = this._Search(SearchInfo);
            ViewData[TypeOfDishConstants.LIST_TYPEOFDISH] = lst.ToList();
            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            TypeOfDish typeofdish = new TypeOfDish();
            TryUpdateModel(typeofdish); 
            Utils.Utils.TrimObject(typeofdish);
            typeofdish.IsActive = true;
            this.TypeOfDishBusiness.Insert(typeofdish);
            this.TypeOfDishBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int TypeOfDishID)
        {
            TypeOfDish typeofdish = this.TypeOfDishBusiness.Find(TypeOfDishID);
            TryUpdateModel(typeofdish);
            Utils.Utils.TrimObject(typeofdish);
            typeofdish.ModifiedDate = DateTime.Now;
            this.TypeOfDishBusiness.Update(typeofdish);
            this.TypeOfDishBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.TypeOfDishBusiness.Delete(id);
            this.TypeOfDishBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<TypeOfDishViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<TypeOfDish> query = this.TypeOfDishBusiness.Search(SearchInfo);
            IQueryable<TypeOfDishViewModel> lst = query.Select(o => new TypeOfDishViewModel {               
						TypeOfDishID = o.TypeOfDishID,								
						Note = o.Note,								
						Description = o.Description,								
						CreatedDate = o.CreatedDate,								
						IsActive = o.IsActive,								
						ModifiedDate = o.ModifiedDate,								
						TypeOfDishName = o.TypeOfDishName													
            });

            List<DishCat> listDishCat = DishCatBusiness.All.ToList();
            List<TypeOfDishViewModel> list =  new List<TypeOfDishViewModel>();
            foreach (TypeOfDishViewModel obj in lst)
            {
                var listContraint = listDishCat.Where(o => o.TypeOfDishID == obj.TypeOfDishID).FirstOrDefault();
                if (listContraint != null)
                {
                    obj.DeleteAble = false;
                }
                else
                {
                    obj.DeleteAble = true;
                }
                list.Add(obj);
            }
            return list.OrderBy(o=>o.TypeOfDishName);
        }        
    }
}





