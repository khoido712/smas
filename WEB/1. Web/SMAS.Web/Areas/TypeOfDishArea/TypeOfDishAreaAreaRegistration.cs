﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TypeOfDishArea
{
    public class TypeOfDishAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TypeOfDishArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TypeOfDishArea_default",
                "TypeOfDishArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
