﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Models.Models;
using SMAS.Web.Areas.LookupInfoReportArea.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using SMAS.Web.Controllers;
namespace SMAS.Web.Areas.LookupInfoReportArea.Controllers
{
    
    public class LookupInfoReportController : BaseController
    {
        #region Khởi tạo biến
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ILookupInfoReportBusiness LookupInfoReportBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        public LookupInfoReportController(IDistrictBusiness DistrictBusiness, IContractTypeBusiness ContractTypeBusiness, IWorkTypeBusiness WorkTypeBusiness, ISchoolProfileBusiness SchoolProfileBusiness, ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness, ILookupInfoReportBusiness LookupInfoReportBusiness, IProcessedReportBusiness ProcessedReportBusiness, IAcademicYearBusiness AcademicYearBusiness, IEducationLevelBusiness EducationLevelBusiness, IClassProfileBusiness ClassProfileBusiness, IPupilProfileBusiness PupilProfileBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.DistrictBusiness = DistrictBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.WorkTypeBusiness = WorkTypeBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.LookupInfoReportBusiness = LookupInfoReportBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }
    #endregion
        //
        // GET: /LookupInfoReportArea/LookupInfoReport/
        #region Trang Index
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }
        public ActionResult IndexForPupil()
        {
            SetViewPupilData();
            return View();
        }
        #endregion
        #region Load dữ liệu đầu vào
        // Dữ liệu cho cán bộ
        public void SetViewData() {
            GlobalInfo global = new GlobalInfo();
            if(global.IsSuperVisingDeptRole==true){
                ViewData[LookupInfoReportConstants.LIST_ACCOUNTTYPE] = true;
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID}
                };
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName);
                ViewData[LookupInfoReportConstants.LIST_DISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName");
                ViewData[LookupInfoReportConstants.LIST_EDUCATIONGRADE] = new SelectList(CommonList.AppliedLevelForProvince(), "key", "value");
                ViewData[LookupInfoReportConstants.LIST_SEX] = new SelectList(CommonList.GenreAndAll(), "key", "value");
                ViewData[LookupInfoReportConstants.LIST_EMPLOYMENTSTATUS] = new SelectList(CommonList.EmployeeStatus(), "key", "value");
                IQueryable<ContractType> lstContract =ContractTypeBusiness.Search(new Dictionary<string,object>());
                ViewData[LookupInfoReportConstants.LIST_CONTRACTTYPE] = new SelectList(lstContract.ToList(), "ContractTypeID", "Resolution");
                IQueryable<WorkType> lstWorkType = WorkTypeBusiness.Search(new Dictionary<string, object>());
                ViewData[LookupInfoReportConstants.LIST_WORKTYPE] = new SelectList(lstWorkType.ToList(), "WorkTypeID", "Resolution");

                IDictionary<string, object> searchSchool = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID},
                    {"IsActive",true }
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(searchSchool).OrderBy(o => o.SchoolName);
                if (lstSP.Count() != 0)
                {
                    ViewData[LookupInfoReportConstants.LIST_SCHOOL] = new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName");
                }
            }
            else if(global.IsSubSuperVisingDeptRole==true){
                ViewData[LookupInfoReportConstants.LIST_ACCOUNTTYPE] = false;
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID}
                };
                int? DistrictID = global.DistrictID;
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic);
                ViewData[LookupInfoReportConstants.LIST_DISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName");
                ViewData[LookupInfoReportConstants.LIST_DEFAULT_DISTRICT] = DistrictID;
                ViewData[LookupInfoReportConstants.LIST_EDUCATIONGRADE] = new SelectList(CommonList.AppliedLevelForDistrict(), "key", "value");
                ViewData[LookupInfoReportConstants.LIST_SEX] = new SelectList(CommonList.GenreAndAll(), "key", "value");
                ViewData[LookupInfoReportConstants.LIST_EMPLOYMENTSTATUS] = new SelectList(CommonList.EmployeeStatus(), "key", "value");
                IQueryable<ContractType> lstContract1 = ContractTypeBusiness.Search(new Dictionary<string, object>());
                ViewData[LookupInfoReportConstants.LIST_CONTRACTTYPE] = new SelectList(lstContract1.ToList(), "ContractTypeID", "Resolution");
                IQueryable<WorkType> lstWorkType1 = WorkTypeBusiness.Search(new Dictionary<string, object>());
                ViewData[LookupInfoReportConstants.LIST_WORKTYPE] = new SelectList(lstWorkType1.ToList(), "WorkTypeID", "Resolution");
                SupervisingDept su = SupervisingDeptBusiness.Find(global.SupervisingDeptID);
                int SupervisingDeptID = global.SupervisingDeptID.Value;
                if (su != null && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = su.ParentID.Value;
                }
                IDictionary<string, object> searchSchool = new Dictionary<string, object>()
                {
                    {"SupervisingDeptID", SupervisingDeptID},
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(searchSchool).OrderBy(o => o.SchoolName);
                if (lstSP.Count() != 0)
                {
                    ViewData[LookupInfoReportConstants.LIST_SCHOOL] = new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName");
                }
            }



        }

        // Dữ liệu cho học sinh
        public void SetViewPupilData() {
            GlobalInfo global = new GlobalInfo();
            int currentYear = DateTime.Now.Year - 1;
            if (DateTime.Now.Month >= 9)
            {
                currentYear++;
            }
            ViewData[LookupInfoReportConstants.PAGING] = true;
            if (global.IsSuperVisingDeptRole == true)
            {
                ViewData[LookupInfoReportConstants.LIST_ACCOUNTTYPE] = true;
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID}
                };
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic).OrderBy(o =>o.DistrictName);
                ViewData[LookupInfoReportConstants.LIST_PDISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName");

                ViewData[LookupInfoReportConstants.LIST_PEDUCATIONGRADE] = new SelectList(CommonList.AppliedLevelForProvince(), "key", "value");

                ViewData[LookupInfoReportConstants.LIST_PSEX] = new SelectList(CommonList.GenreAndAll(), "key", "value");
                ViewData[LookupInfoReportConstants.LIST_PSTATUS] = new SelectList(CommonList.PupilStatus(), "key", "value");
                //ViewData[LookupInfoReportConstants.LIST_PEDUCATIONLEVEL] = new SelectList(global.EducationLevels, "EducationLevelID", "Resolution");
                
                List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
                if (lsYear != null && lsYear.Count > 0 && !lsYear.Contains(currentYear))
                {
                    currentYear = lsYear.FirstOrDefault();
                }
                List<ComboObject> lscbYear = new List<ComboObject>();
                if (lsYear != null && lsYear.Count > 0)
                {
                    foreach (var year in lsYear)
                    {
                        string value = year.ToString() + "-" + (year + 1).ToString();
                        ComboObject cb = new ComboObject(year.ToString(), value);
                        lscbYear.Add(cb);
                    }
                }
                ViewData[LookupInfoReportConstants.LIST_PACADEMICYEAR] = new SelectList(lscbYear, "key", "value", currentYear);

                IDictionary<string, object> searchSchool = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID},
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(searchSchool).OrderBy(o => o.SchoolName);
                if (lstSP.Count() != 0)
                {
                    ViewData[LookupInfoReportConstants.LIST_PSCHOOL] = new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName");
                }

            }
            else if (global.IsSubSuperVisingDeptRole == true)
            {
                ViewData[LookupInfoReportConstants.LIST_ACCOUNTTYPE] = false;
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID}
                };
                int? DistrictID = global.DistrictID;
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic);
                ViewData[LookupInfoReportConstants.LIST_PDISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName");
                ViewData[LookupInfoReportConstants.LIST_DEFAULT_PDISTRICT] = DistrictID;
                ViewData[LookupInfoReportConstants.LIST_PEDUCATIONGRADE] = new SelectList(CommonList.AppliedLevelForDistrict(), "key", "value");
                ViewData[LookupInfoReportConstants.LIST_PSEX] = new SelectList(CommonList.GenreAndAllPS(), "key", "value");
                ViewData[LookupInfoReportConstants.LIST_PSTATUS] = new SelectList(CommonList.PupilStatus(), "key", "value");
                List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
                if (lsYear != null && lsYear.Count > 0 && !lsYear.Contains(currentYear))
                {
                    currentYear = lsYear.FirstOrDefault();
                }

                List<ComboObject> lscbYear = new List<ComboObject>();
                if (lsYear != null && lsYear.Count > 0)
                {
                    foreach (var year in lsYear)
                    {
                        string value = year.ToString() + "-" + (year + 1).ToString();
                        ComboObject cb = new ComboObject(year.ToString(), value);
                        lscbYear.Add(cb);
                    }
                }
                ViewData[LookupInfoReportConstants.LIST_PACADEMICYEAR] = new SelectList(lscbYear, "key", "value", currentYear);
                SupervisingDept su = SupervisingDeptBusiness.Find(global.SupervisingDeptID);
                int SupervisingDeptID = global.SupervisingDeptID.Value;
                if (su != null && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = su.ParentID.Value;
                }
                IDictionary<string, object> searchSchool = new Dictionary<string, object>()
                {
                    {"SupervisingDeptID", SupervisingDeptID},
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(searchSchool).OrderBy(o => o.SchoolName);
                if (lstSP.Count() != 0)
                {
                    ViewData[LookupInfoReportConstants.LIST_PSCHOOL] = new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName");
                }

            }
        }

        #endregion
        #region Load Combo Box

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSchool(int? EducationGradeID, int? DistrictID)
        {
            GlobalInfo global = new GlobalInfo();
            //Neu la So
            if (global.IsSuperVisingDeptRole == true)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID},
                    {"DistrictID",DistrictID},
                    {"AppliedLevel",EducationGradeID}
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName);
                if (lstSP.Count() != 0)
                {
                    return Json(new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else //Neu la Phong
            {
                SupervisingDept su = SupervisingDeptBusiness.Find(global.SupervisingDeptID);
                int SupervisingDeptID = global.SupervisingDeptID.Value;
                if (su != null && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = su.ParentID.Value;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SupervisingDeptID", SupervisingDeptID},
                    //{"DistrictID",DistrictID},
                    {"AppliedLevel",EducationGradeID}
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName);
                if (lstSP.Count() != 0)
                {
                    return Json(new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPupilSchool(int? EducationGradeID, int? DistrictID)
        {
            
            GlobalInfo global = new GlobalInfo();
            //Neu la So
            if (global.IsSuperVisingDeptRole == true)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID},
                    {"DistrictID",DistrictID},
                    {"AppliedLevel",EducationGradeID}
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName);
                if (lstSP.Count() != 0)
                {
                    return Json(new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else //Neu la Phong
            {
                SupervisingDept su = SupervisingDeptBusiness.Find(global.SupervisingDeptID);
                int SupervisingDeptID = global.SupervisingDeptID.Value;
                if (su != null && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = su.ParentID.Value;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",SupervisingDeptID},
                    //{"DistrictID",DistrictID},
                    {"AppliedLevel",EducationGradeID}
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName);
                if (lstSP.Count() != 0)
                {
                    return Json(new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }

        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadSchoolFaculty(int? SchoolID)
        {
            if (SchoolID != 0 && SchoolID!= null)
            {
                IQueryable<SchoolFaculty> lstSF = SchoolFacultyBusiness.SearchBySchool(SchoolID.Value, new Dictionary<string, object>());
                if (lstSF.Count() != 0)
                {
                    return Json(new SelectList(lstSF.ToList(), "SchoolFacultyID", "FacultyName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEducationLevel(int? EducationGradeID) {
            //ViewData[LookupInfoReportConstants.LIST_PEDUCATIONLEVEL] = EducationLevelBusiness.GetByGrade(
            if (EducationGradeID!= null && EducationGradeID != 0)
            {
                IQueryable<EducationLevel> lstSF = EducationLevelBusiness.GetByGrade(EducationGradeID.Value);
                if (lstSF.Count() != 0)
                {
                    return Json(new SelectList(lstSF.ToList(), "EducationLevelID", "Resolution"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? SchoolID, int? EducationLevelID, int? AcademicYearID, int? AppliedLevel)
        {
            if (SchoolID.HasValue && AcademicYearID.HasValue && AppliedLevel.HasValue)
            {
                GlobalInfo global = new GlobalInfo();
                Dictionary<string, object> Dictionary = new Dictionary<string, object>()
                {
                    {"Year",AcademicYearID}
                };
                AcademicYear ac = AcademicYearBusiness.SearchBySchool(SchoolID.Value, Dictionary).FirstOrDefault();
                if (ac != null)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    if (EducationLevelID == 0 || EducationLevelID == null)
                    {
                        dic["AcademicYearID"] = ac.AcademicYearID;
                        dic["AppliedLevel"] = AppliedLevel;
                    }
                    else
                    {
                        dic["EducationLevelID"] = EducationLevelID;
                        dic["AcademicYearID"] = ac.AcademicYearID;

                    }

                    IQueryable<ClassProfile> lstCP = ClassProfileBusiness.SearchBySchool(SchoolID.Value, dic);
                    if (lstCP.Count() != 0)
                    {
                        return Json(new SelectList(lstCP.ToList(), "ClassProfileID", "DisplayName"));
                    }
                    else
                    {
                        return Json(new SelectList(new string[] { }));
                    }
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }
        #endregion
        #region Tìm kiếm cán bộ
        public PartialViewResult Search(SearchViewModel frm, FormCollection form1, int page = 1, string orderBy = "")
        {
            #region List checkbox
            IDictionary<string, object> lstVisible = new Dictionary<string, object>();
            if (form1["chkFullName"] != null)
                lstVisible["chkFullName"] = true;
            else lstVisible["chkFullName"] = true;
            if (form1["chkEmployeeCode"] != null)
                lstVisible["chkEmployeeCode"] = true;
            else lstVisible["chkEmployeeCode"] = true;
            if (form1["chkBirthDate"] != null)
                lstVisible["chkBirthDate"] = true;
            else lstVisible["chkBirthDate"] = false;
            if (form1["chkSex"] != null)
                lstVisible["chkSex"] = true;
            else lstVisible["chkSex"] = false;
            if (form1["chkEthnic"] != null)
                lstVisible["chkEthnic"] = true;
            else lstVisible["chkEthnic"] = false;
            if (form1["chkReligion"] != null)
                lstVisible["chkReligion"] = true;
            else lstVisible["chkReligion"] = false;
            if (form1["chkIdentifyNumber"] != null)
                lstVisible["chkIdentifyNumber"] = true;
            else lstVisible["chkIdentifyNumber"] = false;
            if (form1["chkIdentifyIssuedDate"] != null)
                lstVisible["chkIdentifyIssuedDate"] = true;
            else lstVisible["chkIdentifyIssuedDate"] = false;
            if (form1["chkIdentifyIssuedPlace"] != null)
                lstVisible["chkIdentifyIssuedPlace"] = true;
            else lstVisible["chkIdentifyIssuedPlace"] = false;
            if (form1["chkMobile"] != null)
                lstVisible["chkMobile"] = true;
            else lstVisible["chkMobile"] = false;
            if (form1["chkEmail"] != null)
                lstVisible["chkEmail"] = true;
            else lstVisible["chkEmail"] = false;
            if (form1["chkHomeTown"] != null)
                lstVisible["chkHomeTown"] = true;
            else lstVisible["chkHomeTown"] = false;
            if (form1["chkPermanentResidentalAddress"] != null)
                lstVisible["chkPermanentResidentalAddress"] = true;
            else lstVisible["chkPermanentResidentalAddress"] = false;
            if (form1["chkYouthLeageMember"] != null)
                lstVisible["chkYouthLeageMember"] = true;
            else lstVisible["chkYouthLeageMember"] = false;
            if (form1["chkYouthLeagueJoinedDate"] != null)
                lstVisible["chkYouthLeagueJoinedDate"] = true;
            else lstVisible["chkYouthLeagueJoinedDate"] = false;
            if (form1["chkCommunistPartyMember"] != null)
                lstVisible["chkCommunistPartyMember"] = true;
            else lstVisible["chkCommunistPartyMember"] = false;
            if (form1["chkCommunistPartyJoinedDate"] != null)
                lstVisible["chkCommunistPartyJoinedDate"] = true;
            else lstVisible["chkCommunistPartyJoinedDate"] = false;
            if (form1["chkFatherFullName"] != null)
                lstVisible["chkFatherFullName"] = true;
            else lstVisible["chkFatherFullName"] = false;
            if (form1["chkFatherBirthDate"] != null)
                lstVisible["chkFatherBirthDate"] = true;
            else lstVisible["chkFatherBirthDate"] = false;
            if (form1["chkMotherFullName"] != null)
                lstVisible["chkMotherFullName"] = true;
            else lstVisible["chkMotherFullName"] = false;
            if (form1["chkMotherBirthDate"] != null)
                lstVisible["chkMotherBirthDate"] = true;
            else lstVisible["chkMotherBirthDate"] = false;
            if (form1["chkSpouseFullName"] != null)
                lstVisible["chkSpouseFullName"] = true;
            else lstVisible["chkSpouseFullName"] = false;
            if (form1["chkSpouseBirthDate"] != null)
                lstVisible["chkSpouseBirthDate"] = true;
            else lstVisible["chkSpouseBirthDate"] = false;
            if (form1["chkGraduationLevel"] != null)
                lstVisible["chkGraduationLevel"] = true;
            else lstVisible["chkGraduationLevel"] = false;
            if (form1["chkQualificationLevel"] != null)
                lstVisible["chkQualificationLevel"] = true;
            else lstVisible["chkQualificationLevel"] = false;
            if (form1["chkForeignLanguageGrade"] != null)
                lstVisible["chkForeignLanguageGrade"] = true;
            else lstVisible["chkForeignLanguageGrade"] = false;
            if (form1["chkITQualificationLevel"] != null)
                lstVisible["chkITQualificationLevel"] = true;
            else lstVisible["chkITQualificationLevel"] = false;
            if (form1["chkPoliticalGrade"] != null)
                lstVisible["chkPoliticalGrade"] = true;
            else lstVisible["chkPoliticalGrade"] = false;
            if (form1["chkSpecialityCat"] != null)
                lstVisible["chkSpecialityCat"] = true;
            else lstVisible["chkSpecialityCat"] = false;
            if (form1["chkContractType"] != null)
                lstVisible["chkContractType"] = true;
            else lstVisible["chkContractType"] = false;
            if (form1["chkWorkType"] != null)
                lstVisible["chkWorkType"] = true;
            else lstVisible["chkWorkType"] = false;
            if (form1["chkSchoolFaculty"] != null)
                lstVisible["chkSchoolFaculty"] = true;
            else lstVisible["chkSchoolFaculty"] = false;

            if (form1["chkSchool"] != null)
                lstVisible["chkSchool"] = true;
            else lstVisible["chkSchool"] = false;
            ViewData[LookupInfoReportConstants.LIST_VISIBLE] = lstVisible; 
            #endregion
            Utils.Utils.TrimObject(frm);

            GlobalInfo global = new GlobalInfo();

            IQueryable<LookupInfoReportViewModel> query = this.SearchPaging(frm, true, false, page, orderBy);
            List<LookupInfoReportViewModel> lstResult = query.Skip((page - 1) * LookupInfoReportConstants.PAGE_SIZE).Take(LookupInfoReportConstants.PAGE_SIZE).ToList();
            foreach (var item in lstResult)
            {
                item.FatherYearOld = item.FatherBirthDate.HasValue ? item.FatherBirthDate.Value.Year.ToString() : "";
                item.MotherYearOld = item.MotherBirthDate.HasValue ? item.MotherBirthDate.Value.Year.ToString() : "";
                item.SpouseYearOld = item.SpouseBirthDate.HasValue ? item.SpouseBirthDate.Value.Year.ToString() : "";
            }
            ViewData[LookupInfoReportConstants.PAGE] = page;
            ViewData[LookupInfoReportConstants.TOTAL] = query.Count();

            ViewData[LookupInfoReportConstants.LIST_LOOKUPINFOREPORT] = lstResult;
            return PartialView("_List");
        }
        #endregion
        #region Tìm kiếm học sinh

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchPupil(SearchPupilViewModel frm, FormCollection form1, int page = 1, string orderBy = "")
        {
            #region list checkbox
                IDictionary<string, object> lstVisible = new Dictionary<string, object>();
                if (form1["chkPFullName"] != null)
                    lstVisible["chkPFullName"] = true;
                else lstVisible["chkPFullName"] = true;
                if (form1["chkPPupilCode"] != null)
                    lstVisible["chkPPupilCode"] = true;
                else lstVisible["chkPPupilCode"] = true;
                if (form1["chkPClass"] != null)
                    lstVisible["chkPClass"] = true;
                else lstVisible["chkPClass"] = true;
                if (form1["chkPBirthDate"] != null)
                    lstVisible["chkPBirthDate"] = true;
                else lstVisible["chkPBirthDate"] = false;
                if (form1["chkPSex"] != null)
                    lstVisible["chkPSex"] = true;
                else lstVisible["chkPSex"] = false;
                if (form1["chkPStatus"] != null)
                    lstVisible["chkPStatus"] = true;
                else lstVisible["chkPStatus"] = false;
                if (form1["chkPEthnic"] != null)
                    lstVisible["chkPEthnic"] = true;
                else lstVisible["chkPEthnic"] = false;
                if (form1["chkPReligion"] != null)
                    lstVisible["chkPReligion"] = true;
                else lstVisible["chkPReligion"] = false;
                if (form1["chkPHomeTown"] != null)
                    lstVisible["chkPHomeTown"] = true;
                else lstVisible["chkPHomeTown"] = false;
                if (form1["chkPPermanentResidentalAddress"] != null)
                    lstVisible["chkPPermanentResidentalAddress"] = true;
                else lstVisible["chkPPermanentResidentalAddress"] = false;
                if (form1["chkPFamilyType"] != null)
                    lstVisible["chkPFamilyType"] = true;
                else lstVisible["chkPFamilyType"] = false;
                if (form1["chkPPolicyTarget"] != null)
                    lstVisible["chkPPolicyTarget"] = true;
                else lstVisible["chkPPolicyTarget"] = false;
                if (form1["chkPFatherFullName"] != null)
                    lstVisible["chkPFatherFullName"] = true;
                else lstVisible["chkPFatherFullName"] = false;
                if (form1["chkPFatherBirthDate"] != null)
                    lstVisible["chkPFatherBirthDate"] = true;
                else lstVisible["chkPFatherBirthDate"] = false;
                if (form1["chkPFatherMobile"] != null)
                    lstVisible["chkPFatherMobile"] = true;
                else lstVisible["chkPFatherMobile"] = false;
                if (form1["chkPMotherFullName"] != null)
                    lstVisible["chkPMotherFullName"] = true;
                else lstVisible["chkPMotherFullName"] = false;
                if (form1["chkPMotherBirthDate"] != null)
                    lstVisible["chkPMotherBirthDate"] = true;
                else lstVisible["chkPMotherBirthDate"] = false;
                if (form1["chkPMotherMobile"] != null)
                    lstVisible["chkPMotherMobile"] = true;
                else lstVisible["chkPMotherMobile"] = false;
                if (form1["chkPSchool"] != null)
                    lstVisible["chkPSchool"] = true;
                else lstVisible["chkPSchool"] = false;
                if (form1["chkPProvince"] != null)
                    lstVisible["chkPProvince"] = true;
                else lstVisible["chkPProvince"] = false;

                if (form1["chkPEducationLevel"] != null)
                    lstVisible["chkPEducationLevel"] = true;
                else lstVisible["chkPEducationLevel"] = false;
                if (form1["chkPBirthPlace"] != null)
                    lstVisible["chkPBirthPlace"] = true;
                else lstVisible["chkPBirthPlace"] = false;
                if (form1["chkPDistrict"] != null)
                    lstVisible["chkPDistrict"] = true;
                else lstVisible["chkPDistrict"] = false;
                if (form1["chkPCommune"] != null)
                    lstVisible["chkPCommune"] = true;
                else lstVisible["chkPCommune"] = false;
                if (form1["chkPFatherJob"] != null)
                    lstVisible["chkPFatherJob"] = true;
                else lstVisible["chkPFatherJob"] = false;
                if (form1["chkPMotherJob"] != null)
                    lstVisible["chkPMotherJob"] = true;
                else lstVisible["chkPMotherJob"] = false;

                if (form1["chkPSponsorFullName"] != null)
                    lstVisible["chkPSponsorFullName"] = true;
                else lstVisible["chkPSponsorFullName"] = false;
                if (form1["chkPSponsorBirthDate"] != null)
                    lstVisible["chkPSponsorBirthDate"] = true;
                else lstVisible["chkPSponsorBirthDate"] = false;
                if (form1["chkPSponsorJob"] != null)
                    lstVisible["chkPSponsorJob"] = true;
                else lstVisible["chkPSponsorJob"] = false;
                if (form1["chkPSponsorMobile"] != null)
                    lstVisible["chkPSponsorMobile"] = true;
                else lstVisible["chkPSponsorMobile"] = false;


                ViewData[LookupInfoReportConstants.LIST_VISIBLE] = lstVisible; 
                #endregion
            Utils.Utils.TrimObject(frm);

            GlobalInfo global = new GlobalInfo();

            Paginate<LookupInfoReportPupilViewModel> lst = this.SearchPagingPupil(frm, true, false, page, orderBy);
            ViewData[LookupInfoReportConstants.LIST_LOOKUPINFOREPORTPUPIL] = lst;
       

            return PartialView("_ListPupil");
        }
        #endregion
        [SkipCheckRole]
        [GridAction(EnableCustomBinding = true)]

        [ValidateAntiForgeryToken]
        public ActionResult _GridSelectPupil(SearchPupilViewModel frm, FormCollection form1, int page = 1, string orderBy = "")
        {
            GlobalInfo global = new GlobalInfo();

            Paginate<LookupInfoReportPupilViewModel> paging = this.SearchPagingPupil(frm, true, false, page, orderBy);
            GridModel<LookupInfoReportPupilViewModel> gm = new GridModel<LookupInfoReportPupilViewModel>(paging.List);
            gm.Total = paging.total;

            return View(gm);
        }


        [SkipCheckRole]
        [GridAction(EnableCustomBinding = true)]

        [ValidateAntiForgeryToken]
        public ActionResult _GridSelect(SearchViewModel frm, FormCollection form1, int page = 1, string orderBy = "")
        {
            GlobalInfo global = new GlobalInfo();

            IQueryable<LookupInfoReportViewModel> query = this.SearchPaging(frm, true, false, page, orderBy);
            List<LookupInfoReportViewModel> lstResult = query.Skip((page - 1) * LookupInfoReportConstants.PAGE_SIZE).Take(LookupInfoReportConstants.PAGE_SIZE).ToList();
            foreach (var item in lstResult)
            {
                item.FatherYearOld = item.FatherBirthDate.HasValue ? item.FatherBirthDate.Value.Year.ToString() : "";
                item.MotherYearOld = item.MotherBirthDate.HasValue ? item.MotherBirthDate.Value.Year.ToString() : "";
                item.SpouseYearOld = item.SpouseBirthDate.HasValue ? item.SpouseBirthDate.Value.Year.ToString() : "";
            }
            return View(new GridModel<LookupInfoReportViewModel>()
            {
                Data = lstResult,
                Total = query.Count()
            });
        }


        private Paginate<LookupInfoReportPupilViewModel> SearchPagingPupil(SearchPupilViewModel frm, bool allowPaging, bool sort, int page = 1, string orderBy = "")
        {
            GlobalInfo global = new GlobalInfo();
            Paginate<LookupInfoReportPupilViewModel> Paging = null;

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            string no = Res.Get("Common_Label_NoChoice");
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ProvinceID"] = global.ProvinceID.Value;
            search["AppliedLevel"] = frm.EducationGrade;
            if (global.IsSuperVisingDeptRole == true)
            {
                search["DistrictID"] = frm.District;
            }
            else
            {
                search["DistrictID"] = global.DistrictID;
            }
            search["SupervisingDeptID"] = global.SupervisingDeptID; //0
            search["SchoolID"] = frm.School;
            search["PupilCode"] = frm.PupilCode;
            search["FullName"] = frm.FullName;
            search["Genre"] = frm.Sex != null ? frm.Sex : -1;
            search["Status"] = frm.Status;
            search["Year"] = frm.AcademicYear;
            search["EducationLevelID"] = frm.EducationLevel;
            search["ClassID"] = frm.Class;
            if (global.IsSuperVisingDeptRole == true)
            {
                search["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE;
                search["IsSub"] = true;
            }
            else
            {
                search["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE;
                search["IsSub"] = false;
            }
            IQueryable<LookupInfoReportPupilViewModel> lsPupilProfileViewModel = this.PupilProfileBusiness.SearchPupilBySupervisingDept(search)
                                                                        .Select(o => new LookupInfoReportPupilViewModel
                                                                        {
                                                                            Fullname = o.FullName,
                                                                            PupilCode = o.PupilCode,
                                                                            Class = o.CurrentClassName,
                                                                            BirthDate = o.BirthDate,
                                                                            Genre = o.Genre,
                                                                            GenreName = (o.Genre == (int)SystemParamsInFile.GENRE_MALE) ? male : female,
                                                                            Status = o.ProfileStatus,
                                                                            EthnicName = o.EthnicName,
                                                                            ReligionResolution = o.ReligionName,
                                                                            HomeTown = o.HomeTown,
                                                                            PermanentResidentalAddress = o.PermanentResidentalAddress,
                                                                            FamilyType = o.FamilyName,
                                                                            PolicyTarget = o.PolicyTargetName,
                                                                            FatherFullName = o.FatherFullName,
                                                                            FatherBirthDate = o.FatherBirthDate,
                                                                            FatherMobile = o.FatherMobile,
                                                                            MotherFullName = o.MotherFullName,
                                                                            MotherBirthDate = o.MotherBirthDate,
                                                                            MotherMobile = o.MotherMobile,
                                                                            School = o.SchoolName,
                                                                            Province = o.ProvinceName,
                                                                            PupilID = o.PupilProfileID,
                                                                            EducationLevelID = o.EducationLevelID,
                                                                            EducationLevel = o.EducationLevel,
                                                                            BirthPlace = o.BirthPlace,
                                                                            District = o.DistrictName,
                                                                            Commune = o.CommuneName,
                                                                            FatherJob = o.FatherJob,
                                                                            MotherJob = o.MotherJob,
                                                                            SponsorFullName = o.SponsorFullName,
                                                                            SponsorBirthDate = o.SponsorBirthDate,
                                                                            SponsorJob = o.SponsorJob,
                                                                            SponsorMobile = o.SponsorMobile,
                                                                            OrderInClass = o.OrderInClass,
                                                                            Name = o.Name,
                                                                            ClassOrderNumber = o.ClassOrderNumber
                                                                        });
            List<LookupInfoReportPupilViewModel> lsPupilProfileViewModelResult = lsPupilProfileViewModel.ToList();

            if (frm.Class > 0)
            {
                lsPupilProfileViewModelResult = lsPupilProfileViewModelResult.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(o => o.Fullname).ToList();
                ViewData[LookupInfoReportConstants.PAGING] = false;
                allowPaging = false;
            }
            else
            {
                lsPupilProfileViewModelResult = lsPupilProfileViewModelResult.OrderBy(o => o.School).ThenBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber).ThenBy(o => o.Class).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.Fullname).ToList() ;
            }


            Paging = new Paginate<LookupInfoReportPupilViewModel>(lsPupilProfileViewModelResult.AsQueryable());

            if (!allowPaging)
                Paging.size = lsPupilProfileViewModelResult.Count();

            Paging.page = page;
            Paging.paginate();
            
            #region set data

            EducationLevel edu = new EducationLevel();
            foreach (var item in Paging.List)
            {
                #region switch case data
                switch (item.Status)
                {
                    case SystemParamsInFile.PUPIL_STATUS_STUDYING:
                        item.StatusName = Res.Get("Common_Label_PupilStatusStuding");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_GRADUATED:
                        item.StatusName = Res.Get("Common_Label_PupilStatusGraduated");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMoved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF:
                        item.StatusName = Res.Get("Common_Label_PupilStatusLeaved");
                        break;

                    case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS:
                        item.StatusName = Res.Get("Common_Label_PupilStatusMovedToOtherClass");
                        break;
                }

               
                #endregion
                item.FatherYearOld = item.FatherBirthDate.HasValue ? item.FatherBirthDate.Value.Year.ToString() : "";
                item.MotherYearOld = item.MotherBirthDate.HasValue ? item.MotherBirthDate.Value.Year.ToString() : "";
                item.SponsorYearOld = item.SponsorBirthDate.HasValue ? item.SponsorBirthDate.Value.Year.ToString() : "";
            }
            #endregion

            return Paging;
        }


        private IQueryable<LookupInfoReportViewModel> SearchPaging(SearchViewModel frm, bool allowPaging, bool sort, int page = 1, string orderBy = "")
        {
            GlobalInfo global = new GlobalInfo();
            List<LookupInfoReportViewModel> Paging = new List<LookupInfoReportViewModel>();

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            string no = Res.Get("Common_Label_NoChoice");
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ProvinceID"] = global.ProvinceID.Value;
            search["AppliedLevel"] = frm.EducationGrade;
            if (global.IsSuperVisingDeptRole == true)
            {
                search["DistrictID"] = frm.District;
                search["IsSub"] = true;
            }
            else
            {
                search["DistrictID"] = global.DistrictID;
                search["IsSub"] = false;
            }
            search["SupervisingDeptID"] = global.SupervisingDeptID; //0
            search["SchoolID"] = frm.School;
            search["EmployeeCode"] = frm.EmployeeCode;
            search["FullName"] = frm.FullName;
            search["Genre"] = frm.Sex != null ? frm.Sex : -1;
            search["EmployeeStatus"] = frm.EmploymentStatus;
            search["BirthDate"] = frm.Birthdate;
            search["SchoolFacultyID"] = frm.SchoolFaculty;
            search["WorkTypeID"] = frm.WorkType;
            search["ContractTypeID"] = frm.ContractType;
            search["AppliedLevel"] = frm.EducationGrade;
            if (global.IsSuperVisingDeptRole == true)
            {
                search["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE;
            }
            else
            {
                search["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE;
            }

            //
            string youthLeageMember = Res.Get("Common_Label_IsYouthLeageMember");
            string communistPartyMember = Res.Get("Common_Label_IsCommunistPartyMember");
            string noMember = Res.Get("Common_Label_No");
            IQueryable<LookupInfoReportViewModel> lsTeacherViewModel = EmployeeBusiness.SearchTeacherBySupervisingDept(search)
                                                                        .Where(o=>o.SchoolProfile.IsActive)
                                                                        .Select(o => new LookupInfoReportViewModel
                                                                        {
                                                                            Fullname = o.FullName,
                                                                            EmployeeCode = o.EmployeeCode,
                                                                            BirthDate = o.BirthDate,
                                                                            Genre = o.Genre,
                                                                            GenreName = (o.Genre) ? male : female,
                                                                            EthnicName = o.Ethnic.EthnicName,
                                                                            ReligionResolution = o.Religion.Resolution,
                                                                            IdentifyNumber = o.IdentityNumber,
                                                                            IdentifyIssuedDate = o.IdentityIssuedDate,
                                                                            IdentifyIssuedPlace = o.IdentityIssuedPlace,
                                                                            Mobile = o.Mobile,
                                                                            Email = o.Email,
                                                                            HomeTown = o.HomeTown,
                                                                            PermanentResidentalAddress = o.PermanentResidentalAddress,
                                                                            IsYouthLeageMember = o.IsYouthLeageMember,
                                                                            IsYouthLeageMemberName = o.IsYouthLeageMember.HasValue ? o.IsYouthLeageMember.Value ? youthLeageMember : noMember : noMember,
                                                                            YouthLeagueJoinedDate = o.YouthLeagueJoinedDate,
                                                                            IsCommunistPartyMember = o.IsCommunistPartyMember,
                                                                            IsCommunistPartyMemberName = o.IsCommunistPartyMember.HasValue ? o.IsCommunistPartyMember.Value ? communistPartyMember : noMember : noMember,
                                                                            CommunistPartyJoinedDate = o.CommunistPartyJoinedDate,
                                                                            FatherFullName = o.FatherFullName,
                                                                            FatherBirthDate = o.FatherBirthDate,
                                                                            MotherFullName = o.MotherFullName,
                                                                            MotherBirthDate = o.MotherBirthDate,
                                                                            SpouseFullName = o.SpouseFullName,
                                                                            SpouseBirthDate = o.SpouseBirthDate,
                                                                            GraduationLevelResolution = o.TrainingLevel.Resolution,
                                                                            QualificationLevelResolution = o.QualificationLevel.Resolution,
                                                                            ForeignLanguageGradeResolution = o.ForeignLanguageGrade.Resolution,
                                                                            ITQualificationLevelResolution = o.ITQualificationLevel.Resolution,
                                                                            PoliticalGradeResolution = o.PoliticalGrade.Resolution,
                                                                            SpecialityCatResolution = o.SpecialityCat.Resolution,
                                                                            ContractTypeResolution = o.ContractType.Resolution,
                                                                            WorkTypeResolution = o.WorkType.Resolution,
                                                                            SchoolFacultyFacultyName = o.SchoolFaculty.FacultyName,
                                                                            SchoolName = o.SchoolProfile.SchoolName,
                                                                            Name = o.Name
                                                                        }).OrderBy(p=>p.SchoolName).ThenBy(p=>p.Name);

            //List<LookupInfoReportViewModel> lstTeacher = lsTeacherViewModel.ToList();
            //lstTeacher = lstTeacher.OrderBy(o => o.SchoolName).ThenBy(o => o.Name).ToList();
            //lsTeacherViewModel = lstTeacher.AsQueryable();
            //lsTeacherViewModel = lsTeacherViewModel.OrderBy(o => o.SchoolName).ThenBy(o => o.Name);
            return lsTeacherViewModel;
        }


        #region Download Report
        [HttpPost]
        public int ExportFile(FormCollection form1,FormCollection form2)
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            //dua tat ca thong tin tu form vao dictionary
            foreach (String key in form1.AllKeys)
            {
                dic.Add(key, form1[key]);

            }
            //Them vao de lay ten So phong SupervisingDeptID1
            dic.Add("form1[SupervisingDeptID1]", new GlobalInfo().SupervisingDeptID.Value);
            dic.Add("form1[ProvinceID]", new GlobalInfo().ProvinceID.Value);
            dic.Add("form1[chkFullName]", true);
            dic.Add("form1[chkEmployeeCode]", true);
            
            GlobalInfo global = new GlobalInfo();
            bool IsSuperRole = global.IsSuperVisingDeptRole;
            bool IsSubSuperRole = global.IsSubSuperVisingDeptRole;
            
            if (IsSuperRole)
            {
                dic.Add("form1[SupervisingDeptID]", 0);
            }
            if (IsSubSuperRole)
            {
                dic.Add("form1[SupervisingDeptID]", global.SupervisingDeptID.Value);
                dic["form1[District]"] = global.DistrictID;
            }

            if (global.IsSuperVisingDeptRole == true)
            {
                dic["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE;
                dic["IsSub"] = true;
            }
            else
            {
                dic["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE;
                dic["IsSub"] = false;
            }


            String filename = "";
            
            Stream excel = this.LookupInfoReportBusiness.ExportLookupInfo(dic, out filename);


            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;


            ProcessedReport ProcessedReport = new ProcessedReport();
            ProcessedReport.ReportData = ReportUtils.Compress(excel);
            ProcessedReport.ReportCode = "TempReport";
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.ReportName = ReportName;
            ProcessedReportBusiness.Insert(ProcessedReport);
            ProcessedReportBusiness.Save();

            int IDFile = ProcessedReport.ProcessedReportID;
            return IDFile;
        }
        [HttpPost]
        public int ExportFilePupil(FormCollection form1, FormCollection form2)
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            //dua tat ca thong tin tu form vao dictionary
            foreach (String key in form1.AllKeys)
            {
                dic.Add(key, form1[key]);
            }
            //Them vao de lay ten So phong SupervisingDeptID1
            dic.Add("form1[SupervisingDeptID1]", new GlobalInfo().SupervisingDeptID.Value);

            dic.Add("form1[ProvinceID]", new GlobalInfo().ProvinceID.Value);
            dic.Add("form1[chkPFullName]", true);
            dic.Add("form1[chkPPupilCode]", true);
            dic.Add("form1[chkPClass]", true);
            GlobalInfo global = new GlobalInfo();
            bool IsSuperRole = global.IsSuperVisingDeptRole;
            bool IsSubSuperRole = global.IsSubSuperVisingDeptRole;
            if (IsSuperRole)
            {
                dic.Add("form1[SupervisingDeptID]", 0);
            }
            if (IsSubSuperRole)
            {
                dic.Add("form1[SupervisingDeptID]", global.SupervisingDeptID.Value);
                dic["form1[District]"] = global.DistrictID;
            }
            String filename = "";
            
            if (global.IsSuperVisingDeptRole == true)
            {
                dic["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE;
                dic["IsSub"] = true;
            }
            else
            {
                dic["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE;
                dic["IsSub"] = false;
            }
            
            Stream excel = this.LookupInfoReportBusiness.ExportLookupInfoPupil(dic, out filename);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;


            ProcessedReport ProcessedReport = new ProcessedReport();
            ProcessedReport.ReportData = ReportUtils.Compress(excel);
            ProcessedReport.ReportCode = "TempReport";
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.ReportName = ReportName;
            ProcessedReportBusiness.Insert(ProcessedReport);
            ProcessedReportBusiness.Save();

            int IDFile = ProcessedReport.ProcessedReportID;
            return IDFile;
        }
        public FileResult DownloadReport(int IdFile)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(IdFile, _globalInfo.SchoolID.GetValueOrDefault(), _globalInfo.AcademicYearID.GetValueOrDefault());

            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;
            return result;
        }

        public void DeleteReport(int IdFile)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(IdFile, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            ProcessedReportBusiness.Delete(processedReport.ProcessedReportID);
            ProcessedReportBusiness.Save();
        }
        #endregion
    }
}
