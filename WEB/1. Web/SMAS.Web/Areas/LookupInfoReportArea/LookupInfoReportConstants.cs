﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookupInfoReportArea
{
    public class LookupInfoReportConstants
    {
        //Loại account đăng nhập
        public const string LIST_ACCOUNTTYPE = "LIST_ACCOUNTTYPE";
        //Dành cho cán bộ
        public const string LIST_LOOKUPINFOREPORT = "listSchoolReport";
        public const string LIST_DISTRICT = "LIST_DISTRICT";
        public const string LIST_DEFAULT_DISTRICT = "LIST_DEFAULT_PDISTRICT";
        public const string LIST_EMPLOYEECODE = "LIST_EMPLOYEECODE";
        public const string LIST_SCHOOLFACULTY = "LIST_SCHOOLFACULTY";
        public const string LIST_EDUCATIONGRADE = "LIST_EDUCATIONGRADE";
        public const string LIST_FULLNAME = "LIST_FULLNAME";
        public const string LIST_WORKTYPE = "LIST_WORKTYPE";
        public const string LIST_SCHOOL = "LIST_SCHOOL";
        public const string LIST_SEX = "LIST_SEX";
        public const string LIST_CONTRACTTYPE = "LIST_CONTRACTTYPE";
        public const string LIST_BIRTHDATE = "LIST_BIRTHDATE";
        public const string LIST_EMPLOYMENTSTATUS = "LIST_EMPLOYMENTSTATUS";
        public const string LIST_EDUCATION = "LIST_EDUCATION";
        public const string LIST_PUPIL = "LIST_PUPIL";
        public const string LIST_VISIBLE = "LIST_VISIBLE";
        
        //Dành cho học sinh
        public const string LIST_LOOKUPINFOREPORTPUPIL = "listSchoolReport";
        public const string LIST_PDISTRICT = "LIST_PDISTRICT";
        public const string LIST_DEFAULT_PDISTRICT = "LIST_DEFAULT_PDISTRICT";
        public const string LIST_PACADEMICYEAR = "LIST_PACADEMICYEAR";
        public const string LIST_PPUPILCODE = "LIST_PPUPILCODE";
        public const string LIST_PEDUCATIONGRADE = "LIST_PEDUCATIONGRADE";
        public const string LIST_PEDUCATIONLEVEL = "LIST_PEDUCATIONLEVEL";
        public const string LIST_PFULLNAME = "LIST_PFULLNAME";
        public const string LIST_PSCHOOL = "LIST_PSCHOOL";
        public const string LIST_PCLASS = "LIST_PCLASS";
        public const string LIST_PSEX = "LIST_PSEX";
        public const string LIST_PSTATUS = "LIST_PSTATUS";
        public const string PAGING = "PAGING";
        public const int PAGE_SIZE = 20;
        public const string TOTAL = "TOTAL";
        public const string PAGE = "PAGE";
    }
}