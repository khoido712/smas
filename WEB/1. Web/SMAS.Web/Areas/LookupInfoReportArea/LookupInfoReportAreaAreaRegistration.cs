﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LookupInfoReportArea
{
    public class LookupInfoReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LookupInfoReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LookupInfoReportArea_default",
                "LookupInfoReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
