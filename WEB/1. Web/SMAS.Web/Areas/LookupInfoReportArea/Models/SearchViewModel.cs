using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.LookupInfoReportArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Employee_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadDistrict(this)")]
        public int District { get; set; }
        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string EmployeeCode { get; set; }
        [ResourceDisplayName("Employee_Label_SchoolFaculty")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_SCHOOLFACULTY)]
        [AdditionalMetadata("Placeholder", "All")]
        public int SchoolFaculty { get; set; }
        [ResourceDisplayName("Employee_Label_EducationGrade")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_EDUCATIONGRADE)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadGrade(this)")]
        public string EducationGrade { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string FullName { get; set; }

        [ResourceDisplayName("Employee_Label_WorkType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_WORKTYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int WorkType { get; set; }

        [ResourceDisplayName("Employee_Label_School")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_SCHOOL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "OnCboSchoolChange(this)")]
        public int? School { get; set; }
        [ResourceDisplayName("Employee_Label_Sex")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_SEX)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? Sex { get; set; }
        [ResourceDisplayName("Employee_Label_ContractType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_CONTRACTTYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int ContractType { get; set; }
        [ResourceDisplayName("Employee_Label_BirthDate")]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_BIRTHDATE)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public DateTime? Birthdate { get; set; }
        [ResourceDisplayName("Employee_Label_EmploymentStatus")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_EMPLOYMENTSTATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int EmploymentStatus { get; set; }

        
        
        //public bool chkFullName { get; set; }
        
        //public bool chkEmployeeCode { get; set; }
        
        //public bool chkBirthDate { get; set; }
        
        //public bool chkSex { get; set; }
        
        //public bool chkEthnic { get; set; }
        
        //public bool chkReligion { get; set; }
        
        //public bool chkIdentifyNumber { get; set; }
        
        //public bool chkIdentifyIssuedDate { get; set; }
        
        //public bool chkIdentifyIssuedPlace { get; set; }
        
        //public bool chkMobile { get; set; }
        
        //public bool chkEmail { get; set; }
        
        //public bool chkHomeTown { get; set; }
        
        //public bool chkPermanentResidentalAddress { get; set; }
        
        //public bool chkYouthLeageMember { get; set; }
        
        //public bool chkYouthLeagueJoinedDate { get; set; }
        
        //public bool chkCommunistPartyMember { get; set; }
        
        //public bool chkCommunistPartyJoinedDate { get; set; }
        
        //public bool chkFatherFullName { get; set; }
        
        //public bool chkFatherBirthDate { get; set; }
        
        //public bool chkMotherFullName { get; set; }
        
        //public bool chkMotherBirthDate { get; set; }
        
        //public bool chkSpouseFullName { get; set; }
        
        //public bool chkSpouseBirthDate { get; set; }
        
        //public bool chkGraduationLevel { get; set; }
        
        //public bool chkQualificationLevel { get; set; }
        
        //public bool chkForeignLanguageGrade { get; set; }
        
        //public bool chkITQualificationLevel { get; set; }
        
        //public bool chkPoliticalGrade { get; set; }
        
        //public bool chkSpecialityCat { get; set; }
        
        //public bool chkContractType { get; set; }
        
        //public bool chkWorkType { get; set; }
        
        //public bool chkSchoolFaculty { get; set; }
    }
}
