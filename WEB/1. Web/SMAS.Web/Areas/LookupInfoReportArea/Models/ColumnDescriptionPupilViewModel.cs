﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookupInfoReportArea.Models
{
    public class ColumnDescriptionPupilViewModel
    {
        public int ColumnDescriptionId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string ColExcel { get; set; }
        public string ExcelName { get; set; }
        public string Resolution { get; set; }
        public int ColumnOrder { get; set; }
        public int ColumnIndex { get; set; }
        public bool RequiredColumn { get; set; }
        public bool CheckValue { get; set; }
        public string Description { get; set; }
        public int? ReportType { get; set; }
    }

    public class VTVectorColumnTemplateViewModel
    {
        public int ColDesId { get; set; }
        public string ColExcel { get; set; }
        public string ExcelName { get; set; }
    }
}