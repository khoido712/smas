using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.LookupInfoReportArea.Models
{
    public class SearchPupilViewModel
    {
        [ResourceDisplayName("LookupInfo_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_PDISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadDistrict(this)")]
        public int District { get; set; }
        [ResourceDisplayName("LookupInfo_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_PACADEMICYEAR)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "OnCboAcademicYearChange(this)")]
        public int AcademicYear { get; set; }
        [ResourceDisplayName("LookupInfo_Label_PupilCode")]
        [UIHint("Textbox")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }
        [ResourceDisplayName("LookupInfo_Label_EducationGrade")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_PEDUCATIONGRADE)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadGrade(this)")]
        public string EducationGrade { get; set; }

        [ResourceDisplayName("LookupInfo_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_PEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "OnCboEducationLevelChange(this)")]
        public int EducationLevel { get; set; }

        [ResourceDisplayName("LookupInfo_Label_FullName")]
        [UIHint("Textbox")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }

        [ResourceDisplayName("LookupInfo_Label_School")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_PSCHOOL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "OnCboSchoolChange(this)")]
        public int School { get; set; }

        [ResourceDisplayName("LookupInfo_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_PCLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int Class { get; set; }
                
        [ResourceDisplayName("LookupInfo_Label_Sex")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_PSEX)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? Sex { get; set; }

        [ResourceDisplayName("LookupInfo_Label_Status")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoReportConstants.LIST_PSTATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        //[AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int Status { get; set; }
        
    }
}
